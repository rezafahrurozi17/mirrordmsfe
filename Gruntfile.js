// Generated on 2016-11-11 using generator-angular 0.15.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn',
    injector: 'grunt-injector',
    htmlbuild: 'grunt-html-build',
    'sails-linker': 'grunt-sails-linker',

  });

  // Configurable paths for the application
  var appConfig = {
    app: 'assets',
    dist: 'dist'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      // bower: {
      //   files: ['bower.json'],
      //   tasks: ['wiredep']
      // },
      js: {
        files: [
          '<%= yeoman.app %>/**/*.*',
          '<%= yeoman.app %>/styles/**/*.*',
          // '<%= yeoman.app %>/app/**/*.js',
          // '<%= yeoman.app %>/app/**/*.css',
          // '<%= yeoman.app %>/app/**/*.html',
          // '<%= yeoman.app %>/styles/**/*.css',
          // '<%= yeoman.app %>/module/**/*.*',
          // '<%= yeoman.app %>/images/**/*.*',

        ],
        tasks: [
          // 'newer:jshint:all',
          // 'newer:jscs:all'
          'clean:dist',
          'ngtemplates',
          'concat:sysauth_css', 'concat:sysdashboard_css', 'concat:sysapp_css', 'concat:app_css', 'concat:uistyle_css',
          'concat:sysauth1', 'concat:sysauth2dep', 'concat:sysauth2app',
          'concat:sysdashboard', 'concat:appdashboard', 'concat:sysappdep', 'concat:sysapp', 'concat:appdep', //'concat:app',
          'concat:app_gate',
          'concat:app_gate_towing',
          'concat:app_delivery_warranty',
          'concat:app_reception_wogr',
          'concat:app_reception_wobp',
          'concat:app_reception_wolistgr',
          'concat:app_reception_wolistbp',
          'concat:app_reception_bstklist',
          'concat:envJS',
          //CR4
          'concat:app_estimasiBP',
          //End Of CR4
          'concat:asbBoards',
          'concat:lib',
          'concat:uiscript',
          'concat:amcharts', 'concat:serial', 'concat:gantt', 'concat:light', 'concat:exportmin', 'concat:export_css', 'concat:print_min_css', 'concat:print_min',
          'concat:boards_css', 'concat:boards_js',
          'concat:app_sales', 'concat:app_sales1', 'concat:app_sales2', 'concat:app_sales3', 'concat:app_sales4', 'concat:app_sales5', 'concat:app_sales6', 'concat:app_sales7', 'concat:app_sales8', 'concat:app_sales9',
          'concat:sounds_js',
          'concat:signature', 'concat:signature_pad', 'concat:uigantt', 'concat:pie', 'concat:gauge', 'concat:funnel',
          'concat:uiknob', 'concat:uisparkline', 'concat:uifullcalendar', 'concat:uimoment',
          'concat:uitimelinestyle_css', 'concat:uiboxstyle_css', 'concat:uifullcalendarstyle_css', 'concat:uitogglestyle_css',
          'concat:uiganttstyle_css',
          'copy:dist',
          'sails-linker:distJS',
          'sails-linker:distCSS',
          'sails-linker:distTemplate',
        ],
        options: {
          interval: 5007,
          livereload: '<%= connect.options.livereload %>'
        }
      },
      // jsTest: {
      //   files: ['test/spec/{,*/}*.js'],
      //   tasks: ['newer:jshint:test', 'newer:jscs:test', 'karma']
      // },
      // compass: {
      //   files: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
      //   tasks: ['compass:server', 'postcss:server']
      // },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/**/*.html',
          '.tmp/styles/**/*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        // hostname: 'localhost',
        hostname: '0.0.0.0',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              // connect().use(
              //   '/bower_components',
              //   connect.static('./bower_components')
              // ),
              connect().use(
                '/app/styles',
                connect.static('./app/styles')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/app/**/*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Make sure code styles are up to par
    jscs: {
      options: {
        config: '.jscsrc',
        verbose: true
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/app/**/*.js'
        ]
      },
      test: {
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/{,*/}*',
            '!<%= yeoman.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },

    htmlbuild: {
      dist: {
        src: '<%= yeoman.app %>/index.html',
        dest: '<%= yeoman.dist %>',
        options: {
          //beautify: true,
          //prefix: '//some-cdn',
          relative: true,
          basePath: false,
          scripts: {
            template: [
              '<%= yeoman.dist %>/template/*.js',
              '!**/main.js',
            ],
            js: '<%= yeoman.dist %>/js/*.js'
          },
          styles: {
            css: [
              '<%= yeoman.dist %>/css/*.css',
              // '<%= yeoman.app %>/css/dev.css'
            ],
            // test: '<%= fixturesPath %>/css/inline.css'
          },
        }
      }
    },

    'sails-linker': {
      // DIST ENVIRONMENT
      distJS: {
        options: {
          startTag: '<!--SCRIPTS-->',
          endTag: '<!--SCRIPTS END-->',
          fileTmpl: '<script src="%s"></script>',
          appRoot: '<%= yeoman.dist %>/',
          removeStartEndTags: true,
          relative: true
        },
        files: {
          '<%= yeoman.dist %>/index.html': ['<%= yeoman.dist %>/js/sysauth1.js'],
          '<%= yeoman.dist %>/main.html': ['<%= yeoman.dist %>/js/sysauth1.js',
            '<%= yeoman.dist %>/js/sysauth2dep.js',
            '<%= yeoman.dist %>/js/sysauth2app.js'
          ],
          //'views/**/*.html': ['.tmp/public/js/sysauth1.js'],
          //'views/**/login.ejs': ['.tmp/public/js/sysauth1.js'],
        }
      },
      distCSS: {
        options: {
          startTag: '<!--STYLES-->',
          endTag: '<!--STYLES END-->',
          fileTmpl: '<link rel="stylesheet" href="%s">',
          appRoot: '<%= yeoman.dist %>/',
          removeStartEndTags: true,
          relative: true
        },
        files: {
          '<%= yeoman.dist %>/index.html': ['<%= yeoman.dist %>/css/sysauth.css'],
          '<%= yeoman.dist %>/main.html': ['<%= yeoman.dist %>/css/sysauth.css'],
          // 'views/**/*.html': ['.tmp/public/css/sysauth.css'],
          // 'views/**/*.ejs': ['.tmp/public/css/sysauth.css']
        }
      },
      distTemplate: {
        options: {
          startTag: '<!--TEMPLATES-->',
          endTag: '<!--TEMPLATES END-->',
          fileTmpl: '<script src="%s"></script>',
          appRoot: '<%= yeoman.dist %>/',
          removeStartEndTags: true,
          relative: true
        },
        files: {
          //'<%= yeoman.dist %>/index.html': ['<%= yeoman.dist %>/css/sysauth.css'],
          '<%= yeoman.dist %>/main.html': ['<%= yeoman.dist %>/templates/templates_sysauth.js'],
          // 'views/**/*.html': ['.tmp/public/css/sysauth.css'],
          // 'views/**/*.ejs': ['.tmp/public/css/sysauth.css']
        }
      }
    },

    cssmin: {
      dist: {
        files: {
          '<%= yeoman.dist %>/css/sysauth.css': [
            '.tmp/css/sysauth.css'
          ],
          '<%= yeoman.dist %>/css/sysdashboard.css': [
            '.tmp/css/sysdashboard.css'
          ],
          '<%= yeoman.dist %>/css/sysapp.css': [
            '.tmp/css/sysapp.css'
          ],
          '<%= yeoman.dist %>/css/app.css': [
            '.tmp/css/app.css'
          ],
          '<%= yeoman.dist %>/css/boards.css': [
            '.tmp/css/boards.css'
          ],
          '<%= yeoman.dist %>/css/uistyle.css': [
            '.tmp/css/uistyle.css'
          ],
          '<%= yeoman.dist %>/css/uifullcalendarstyle.css': [

            '.tmp/css/uifullcalendarstyle.css'
          ],
          '<%= yeoman.dist %>/css/uitimelinestyle.css': [
            '.tmp/css/uitimelinestyle.css'
          ],
          '<%= yeoman.dist %>/css/uitogglestyle.css': [
            '.tmp/css/uitogglestyle.css'
          ],
          '<%= yeoman.dist %>/css/uiboxstyle.css': [
            '.tmp/css/uiboxstyle.css'
          ],
          '<%= yeoman.dist %>/css/uiganttstyle.css': [
            '.tmp/css/uiganttstyle.css'
          ],
        }
      }
    },
    uglify: {
      options: {
        compress: {
          drop_console: true
        }
      },
      dist: {
        files: {
          '<%= yeoman.dist %>/js/sysauth1.js': [
            '<%= yeoman.dist %>/js/sysauth1.js'
          ],
          '<%= yeoman.dist %>/js/sysauth2dep.js': [
            '<%= yeoman.dist %>/js/sysauth2dep.js'
          ],
          '<%= yeoman.dist %>/js/sysauth2app.js': [
            '<%= yeoman.dist %>/js/sysauth2app.js'
          ],
          '<%= yeoman.dist %>/js/env.js': [
            '<%= yeoman.dist %>/js/env.js'
          ],
          '<%= yeoman.dist %>/js/sysdashboard.js': [
            '<%= yeoman.dist %>/js/sysdashboard.js'
          ],
          '<%= yeoman.dist %>/js/appdashboard.js': [
            '<%= yeoman.dist %>/js/appdashboard.js'
          ],
          '<%= yeoman.dist %>/js/sysappdep.js': [
            '<%= yeoman.dist %>/js/sysappdep.js'
          ],
          '<%= yeoman.dist %>/js/sysapp.js': [
            '<%= yeoman.dist %>/js/sysapp.js'
          ],
          '<%= yeoman.dist %>/js/appdep.js': [
            '<%= yeoman.dist %>/js/appdep.js'
          ],
          '<%= yeoman.dist %>/js/app.js': [
            '<%= yeoman.dist %>/js/app.js'
          ],
          '<%= yeoman.dist %>/js/lib.js': [
            '<%= yeoman.dist %>/js/lib.js'
          ],
          '<%= yeoman.dist %>/js/app_gate.js': [
            '<%= yeoman.dist %>/js/app_gate.js'
          ],
          '<%= yeoman.dist %>/js/app_gate_towing.js': [
            '<%= yeoman.dist %>/js/app_gate_towing.js'
          ],
          '<%= yeoman.dist %>/js/app_delivery_warranty.js': [
            '<%= yeoman.dist %>/js/app_delivery_warranty.js'
          ],
          '<%= yeoman.dist %>/js/app_reception_wogr.js': [
            '<%= yeoman.dist %>/js/app_reception_wogr.js'
          ],
          '<%= yeoman.dist %>/js/app_reception_wobp.js': [
            '<%= yeoman.dist %>/js/app_reception_wobp.js'
          ],
          '<%= yeoman.dist %>/js/app_reception_wolistgr.js': [
            '<%= yeoman.dist %>/js/app_reception_wolistgr.js'
          ],
          '<%= yeoman.dist %>/js/app_reception_wolistbp.js': [
            '<%= yeoman.dist %>/js/app_reception_wolistbp.js'
          ],
          '<%= yeoman.dist %>/js/app_reception_bstklist.js': [
            '<%= yeoman.dist %>/js/app_reception_bstklist.js'
          ],
          //CR4
          '<%= yeoman.dist %>/js/app_estimasiBP.js': [
            '<%= yeoman.dist %>/js/app_estimasiBP.js'
          ],
          // end CR4	  
          '<%= yeoman.dist %>/js/app_boards.js': [
            '<%= yeoman.dist %>/js/app_boards.js'
          ],
          '<%= yeoman.dist %>/js/app_branchorder.js': [
            '<%= yeoman.dist %>/js/app_branchorder.js'
          ],
          '<%= yeoman.dist %>/js/app_crm.js': [
            '<%= yeoman.dist %>/js/app_crm.js'
          ],
          '<%= yeoman.dist %>/js/app_finance.js': [
            '<%= yeoman.dist %>/js/app_finance.js'
          ],
          '<%= yeoman.dist %>/js/app_master.js': [
            '<%= yeoman.dist %>/js/app_master.js'
          ],
          '<%= yeoman.dist %>/js/app_parameter.js': [
            '<%= yeoman.dist %>/js/app_parameter.js'
          ],
          '<%= yeoman.dist %>/js/app_parts.js': [
            '<%= yeoman.dist %>/js/app_parts.js'
          ],
          '<%= yeoman.dist %>/js/app_report.js': [
            '<%= yeoman.dist %>/js/app_report.js'
          ],
          '<%= yeoman.dist %>/js/app_sales.js': [
            '<%= yeoman.dist %>/js/app_sales.js'
          ],
          '<%= yeoman.dist %>/js/app_sales1.js': [
            '<%= yeoman.dist %>/js/app_sales1.js'
          ],
          '<%= yeoman.dist %>/js/app_sales2.js': [
            '<%= yeoman.dist %>/js/app_sales2.js'
          ],
          '<%= yeoman.dist %>/js/app_sales3.js': [
            '<%= yeoman.dist %>/js/app_sales3.js'
          ],
          '<%= yeoman.dist %>/js/app_sales4.js': [
            '<%= yeoman.dist %>/js/app_sales4.js'
          ],
          '<%= yeoman.dist %>/js/app_sales5.js': [
            '<%= yeoman.dist %>/js/app_sales5.js'
          ],
          '<%= yeoman.dist %>/js/app_sales6.js': [
            '<%= yeoman.dist %>/js/app_sales6.js'
          ],
          '<%= yeoman.dist %>/js/app_sales7.js': [
            '<%= yeoman.dist %>/js/app_sales7.js'
          ],
          '<%= yeoman.dist %>/js/app_sales8.js': [
            '<%= yeoman.dist %>/js/app_sales8.js'
          ],
          '<%= yeoman.dist %>/js/app_sales9.js': [
            '<%= yeoman.dist %>/js/app_sales9.js'
          ],
          '<%= yeoman.dist %>/js/app_services.js': [
            '<%= yeoman.dist %>/js/app_services.js'
          ],
          '<%= yeoman.dist %>/js/uiscript.js': [
            '<%= yeoman.dist %>/js/uiscript.js'
          ],

          '<%= yeoman.dist %>/js/print_min.js': [
            '<%= yeoman.dist %>/js/print_min.js'
          ],

          '<%= yeoman.dist %>/js/amcharts.js': [
            '<%= yeoman.dist %>/js/amcharts.js'
          ],

          '<%= yeoman.dist %>/js/serial.js': [
            '<%= yeoman.dist %>/js/serial.js'
          ],

          '<%= yeoman.dist %>/js/gantt.js': [
            '<%= yeoman.dist %>/js/gantt.js'
          ],

          '<%= yeoman.dist %>/js/light.js': [
            '<%= yeoman.dist %>/js/light.js'
          ],

          '<%= yeoman.dist %>/js/exportmin.js': [
            '<%= yeoman.dist %>/js/exportmin.js'
          ],

          '<%= yeoman.dist %>/js/uiknob.js': [
            '<%= yeoman.dist %>/js/uiknob.js'
          ],

          '<%= yeoman.dist %>/js/uisparkline.js': [
            '<%= yeoman.dist %>/js/uisparkline.js'
          ],

          '<%= yeoman.dist %>/js/uifullcalendar.js': [
            '<%= yeoman.dist %>/js/uifullcalendar.js'
          ],

          '<%= yeoman.dist %>/js/uimoment.js': [
            '<%= yeoman.dist %>/js/uimoment.js'
          ],
          '<%= yeoman.dist %>/js/signature.js': [
            '<%= yeoman.dist %>/js/signature.js'
          ],
          '<%= yeoman.dist %>/js/signature_pad.js': [
            '<%= yeoman.dist %>/js/signature_pad.js'
          ],
          '<%= yeoman.dist %>/js/uigantt.js': [
            '<%= yeoman.dist %>/js/uigantt.js'
          ],
          '<%= yeoman.dist %>/js/pie.js': [
            '<%= yeoman.dist %>/js/pie.js'
          ],
          '<%= yeoman.dist %>/js/gauge.js': [
            '<%= yeoman.dist %>/js/gauge.js'
          ],
          '<%= yeoman.dist %>/js/funnel.js': [
            '<%= yeoman.dist %>/js/funnel.js'
          ],
          '<%= yeoman.dist %>/templates/templates_lib.js': [
            '<%= yeoman.dist %>/templates/templates_lib.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app.js': [
            '<%= yeoman.dist %>/templates/templates_app.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app_gate.js': [
            '<%= yeoman.dist %>/templates/templates_app_gate.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app_gate_towing.js': [
            '<%= yeoman.dist %>/templates/templates_app_gate_towing.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app_delivery_warranty.js': [
            '<%= yeoman.dist %>/templates/templates_app_delivery_warranty.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app_reception_wogr.js': [
            '<%= yeoman.dist %>/templates/templates_app_reception_wogr.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app_reception_wobp.js': [
            '<%= yeoman.dist %>/templates/templates_app_reception_wobp.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app_reception_wolistgr.js': [
            '<%= yeoman.dist %>/templates/templates_app_reception_wolistgr.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app_reception_wolistbp.js': [
            '<%= yeoman.dist %>/templates/templates_app_reception_wolistbp.js'
          ],
          '<%= yeoman.dist %>/templates/templates_app_reception_bstklist.js': [
            '<%= yeoman.dist %>/templates/templates_app_reception_bstklist.js'
          ],
          //CR4
          '<%= yeoman.dist %>/templates/templates_app_estimasiBP.js': [
            '<%= yeoman.dist %>/templates/templates_app_estimasiBP.js'
          ],
          //end CR4  
          '<%= yeoman.dist %>/templates/templates_appboards.js': [
            '<%= yeoman.dist %>/templates/templates_appboards.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appbranchorder.js': [
            '<%= yeoman.dist %>/templates/templates_appbranchorder.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appcrm.js': [
            '<%= yeoman.dist %>/templates/templates_appcrm.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appfinance.js': [
            '<%= yeoman.dist %>/templates/templates_appfinance.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appmaster.js': [
            '<%= yeoman.dist %>/templates/templates_appmaster.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appparameter.js': [
            '<%= yeoman.dist %>/templates/templates_appparameter.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appparts.js': [
            '<%= yeoman.dist %>/templates/templates_appparts.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appreport.js': [
            '<%= yeoman.dist %>/templates/templates_appreport.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appsales.js': [
            '<%= yeoman.dist %>/templates/templates_appsales.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appsales1.js': [
            '<%= yeoman.dist %>/templates/templates_appsales1.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appservices.js': [
            '<%= yeoman.dist %>/templates/templates_appservices.js'
          ],
          '<%= yeoman.dist %>/templates/templates_appdashboard.js': [
            '<%= yeoman.dist %>/templates/templates_appdashboard.js'
          ],
          '<%= yeoman.dist %>/templates/templates_sysapp.js': [
            '<%= yeoman.dist %>/templates/templates_sysapp.js'
          ],
          '<%= yeoman.dist %>/templates/templates_sysauth.js': [
            '<%= yeoman.dist %>/templates/templates_sysauth.js'
          ],
          '<%= yeoman.dist %>/templates/templates_sysdashboard.js': [
            '<%= yeoman.dist %>/templates/templates_sysdashboard.js'
          ],
        }
      }
    },
    concat: {
      //--------------------- CSS
      sysauth_css: {
        src: [
          '<%= yeoman.app %>/styles/sysauth.css',
        ],
        dest: '.tmp/css/sysauth.css'
      },
      sysdashboard_css: {
        src: [
          '<%= yeoman.app %>/styles/sysdashboard.css',
          '<%= yeoman.app %>/styles/ui-listView.css',
        ],
        dest: '.tmp/css/sysdashboard.css'
      },
      sysapp_css: {
        src: [
          '<%= yeoman.app %>/styles/sysapp.css',
        ],
        dest: '.tmp/css/sysapp.css'
      },
      app_css: {
        src: [
          '<%= yeoman.app %>/styles/app/**/*.css',
        ],
        dest: '.tmp/css/app.css'
      },
      boards_css: {
        src: [
          '<%= yeoman.app %>/styles/boards/**/*.css',
        ],
        dest: '.tmp/css/boards.css'
      },

      uistyle_css: {
        src: [
          '<%= yeoman.app %>/styles/uistyle.css',
        ],
        dest: '.tmp/css/uistyle.css'
      },
      print_min_css: {
        src: [
          '<%= yeoman.app %>/styles/print_min.css',
        ],
        dest: '.tmp/css/print_min.css'
      },
      export_css: {
        src: [
          '<%= yeoman.app %>/styles/export.css',
        ],
        dest: '.tmp/css/export.css'
      },
      uiboxstyle_css: {
        src: [
          '<%= yeoman.app %>/styles/uiboxstyle.css',
        ],
        dest: '.tmp/css/uiboxstyle.css'
      },
      uifullcalendarstyle_css: {
        src: [
          '<%= yeoman.app %>/styles/uifullcalendarstyle.css',
        ],
        dest: '.tmp/css/uifullcalendarstyle.css'
      },
      uitimelinestyle_css: {
        src: [
          '<%= yeoman.app %>/styles/uitimelinestyle.css',
        ],
        dest: '.tmp/css/uitimelinestyle.css'
      },
      uitogglestyle_css: {
        src: [
          '<%= yeoman.app %>/styles/uitogglestyle.css',
        ],
        dest: '.tmp/css/uitogglestyle.css'
      },
      uiganttstyle_css: {
        src: [
          '<%= yeoman.app %>/styles/uiganttstyle.css',
        ],
        dest: '.tmp/css/uiganttstyle.css'
      },
      //--------------------- JS
      sysauth1: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/sysauth1.js',
          '<%= yeoman.app %>/app/_directives/_common/**/*.js',
          '<%= yeoman.app %>/app/auth_interceptor.js',
          '<%= yeoman.app %>/app/_dependencies/mobile-detect.min.js'
        ],
        dest: '<%= yeoman.dist %>/js/sysauth1.js'
      },
      sysauth2dep: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/sysauth2dep.js',
        ],
        dest: '<%= yeoman.dist %>/js/sysauth2dep.js'
      },
      sysauth2app: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/sysauth2app.js',
          '<%= yeoman.app %>/app/_filters/**/*.js',
          '<%= yeoman.app %>/app/app.config.js',
          '<%= yeoman.app %>/app/routes.sys.js',
          '<%= yeoman.app %>/app/routes.ds.js',
          '<%= yeoman.app %>/app/routes.as.js',
          '<%= yeoman.app %>/app/routes.as.master.js',
          '<%= yeoman.app %>/app/routes.js',
          '<%= yeoman.app %>/app/board_routes.js',
          '<%= yeoman.app %>/app/part_routes.js',
          '<%= yeoman.app %>/app/sales_routes.js',
          '<%= yeoman.app %>/app/finance_routes.js',
        ],
        dest: '<%= yeoman.dist %>/js/sysauth2app.js'
      },
      envJS: {
        src: [
          '<%= yeoman.app %>/app/env.js',
        ],
        dest: '<%= yeoman.dist %>/js/env.js'
      },
      sysdashboard: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/sysdashboard.js',
        ],
        dest: '<%= yeoman.dist %>/js/sysdashboard.js'
      },
      appdashboard: {
        src: [
          '<%= yeoman.app %>/app/app_dashboard/**/*.js',
          '<%= yeoman.app %>/app/_dependencies/firebase-app.js',
          '<%= yeoman.app %>/app/_dependencies/firebase-auth.js',
          '<%= yeoman.app %>/app/_dependencies/firebase-messaging.js',
          '<%= yeoman.app %>/app/_dependencies/SmartNotification.min.js'
        ],
        dest: '<%= yeoman.dist %>/js/appdashboard.js'
      },
      sysappdep: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/sysappdep.js',
          '<%= yeoman.app %>/app/_directives/_common/**/*.js',
          // '<%= yeoman.app %>/app/_directives/_common2/**/*.js',
          // '<%= yeoman.app %>/app/_dependencies/ag-grid-enterprise.min.js',
          // '<%= yeoman.app %>/app/_dependencies/ag-grid.directive.js'

        ],
        dest: '<%= yeoman.dist %>/js/sysappdep.js'
      },
      sysapp: {
        src: [
          '<%= yeoman.app %>/app/_sys/role/**/*.js',
          '<%= yeoman.app %>/app/_sys/user/**/*.js',
          '<%= yeoman.app %>/app/_sys/org/**/*.js',
          '<%= yeoman.app %>/app/_sys/menu/**/*.js',
          '<%= yeoman.app %>/app/_sys/appl/**/*.js',
          '<%= yeoman.app %>/app/_sys/service/**/*.js',
          '<%= yeoman.app %>/app/_sys/files/**/*.js',
          '<%= yeoman.app %>/app/_sys/jobs/**/*.js',
          '<%= yeoman.app %>/app/_sys/_demo/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/sysapp.js'
      },
      appdep: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/appdep.js',
          '<%= yeoman.app %>/app/_dependencies/print_min.js',
        ],
        dest: '<%= yeoman.dist %>/js/appdep.js'
      },
      app: {
        src: [
          '<%= yeoman.app %>/app/app/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app.js'
      },
      lib: {
        src: [
          '<%= yeoman.app %>/app/app/services/lib/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/lib.js'
      },
      app_gate: {
        src: [
          '<%= yeoman.app %>/app/app/services/gate/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_gate.js'
      },
      app_gate_towing: {
        src: [
          '<%= yeoman.app %>/app/app/services/reception/towing/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_gate_towing.js'
      },
      app_delivery_warranty: {
        src: [
          '<%= yeoman.app %>/app/app/services/delivery/warranty/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_delivery_warranty.js'
      },
      app_reception_wogr: {
        src: [
          '<%= yeoman.app %>/app/app/services/reception/wo/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_reception_wogr.js'
      },
      app_reception_wobp: {
        src: [
          '<%= yeoman.app %>/app/app/services/reception/wo_bp/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_reception_wobp.js'
      },
      app_reception_wolistgr: {
        src: [
          '<%= yeoman.app %>/app/app/services/reception/woHistoryGR/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_reception_wolistgr.js'
      },
      app_reception_wolistbp: {
        src: [
          '<%= yeoman.app %>/app/app/services/reception/woHistoryBP/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_reception_wolistbp.js'
      },
      app_reception_bstklist: {
        src: [
          '<%= yeoman.app %>/app/app/services/reception/bstkList/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_reception_bstklist.js'
      },
      // CR4
      app_estimasiBP: {
        src: [
          '<%= yeoman.app %>/app/app/services/estimasiBP/**/*.js',
          '<%= yeoman.app %>/app/app/master/AfterSales/MasterDiscount/MasterDiscountFactory.js',
          '<%= yeoman.app %>/app/app/services/appointment/appointmentServiceBp/appointmentBpServiceFactory.js',
          '<%= yeoman.app %>/app/app/services/reception/wo_bp/wobpFactory.js',
          '<%= yeoman.app %>/app/app/services/gmaster/gmasterFactory.js',
          '<%= yeoman.app %>/app/app/services/appointment/appointmentServiceGr/appointmentGrServiceFactory.js',
          '<%= yeoman.app %>/app/app/services/mrs/mrsList/mrsListFactory.js',
          '<%= yeoman.app %>/app/app/services/reception/wo/woFactory.js'

        ],
        dest: '<%= yeoman.dist %>/js/app_estimasiBP.js'
      },
      //End CR4	
      asbBoards: {
        src: [
          '<%= yeoman.app %>/app/app/boards/factories/**/*.js',
          '<%= yeoman.app %>/app/app/boards/jpcbgrview/**/*.js',
          '<%= yeoman.app %>/app/app/boards/asb/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/asbBoards.js'

      },
      // added 2017-12-06
      //       app_boards: {
      //                 src: [
      //                     '<%= yeoman.app %>/app/app/boards/**/*.js',
      //                 ],
      //                 dest: '<%= yeoman.dist %>/js/app_boards.js'
      //         },
      app_branchorder: {
        src: [
          '<%= yeoman.app %>/app/app/branchOrder/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_branchorder.js'
      },
      //     app_crm: {
      //                 src: [
      //                     '<%= yeoman.app %>/app/app/crm/**/*.js',
      //                 ],
      //                 dest: '<%= yeoman.dist %>/js/app_crm.js'
      //       },
      app_finance: {
        src: [
          '<%= yeoman.app %>/app/app/finance/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_finance.js'
      },
      app_master: {
        src: [
          '<%= yeoman.app %>/app/app/master/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_master.js'
      },
      //     app_parameter: {
      //                 src: [
      //                     '<%= yeoman.app %>/app/app/parameter/**/*.js',
      //                 ],
      //                 dest: '<%= yeoman.dist %>/js/app_parameter.js'
      //       },
      //     app_parts: {
      //                 src: [
      //                     '<%= yeoman.app %>/app/app/parts/**/*.js',
      //                 ],
      //                 dest: '<%= yeoman.dist %>/js/app_parts.js'
      //       },
      app_report: {
        src: [
          '<%= yeoman.app %>/app/app/report/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_report.js'
      },
      app_sales: {
        src: [
          '<%= yeoman.app %>/app/app/sales/report/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales.js'
      },
      app_sales1: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales1/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales1.js'
      },
      app_sales2: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales2/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales2.js'
      },
      app_sales3: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales3/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales3.js'
      },
      app_sales4: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales4/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales4.js'
      },
      app_sales5: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales5/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales5.js'
      },
      app_sales6: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales6/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales6.js'
      },
      app_sales7: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales7/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales7.js'
      },
      app_sales8: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales8/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales8.js'
      },
      app_sales9: {
        src: [
          '<%= yeoman.app %>/app/app/sales/sales9/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_sales9.js'
      },
      app_services: {
        src: [
          '<%= yeoman.app %>/app/app/boards/**/*.js',
          '<%= yeoman.app %>/app/app/crm/**/*.js',
          '<%= yeoman.app %>/app/app/services/**/*.js',
          '<%= yeoman.app %>/app/app/parts/**/*.js',
          '<%= yeoman.app %>/app/app/parameter/**/*.js',
        ],
        dest: '<%= yeoman.dist %>/js/app_services.js'
      },
      // 
      boards_js: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/boards/element-resize-detector.js',
          '<%= yeoman.app %>/app/_dependencies/boards/konva.js',
          '<%= yeoman.app %>/app/_dependencies/boards/jboard.js',
          '<%= yeoman.app %>/app/_dependencies/boards/layouts.js',
          '<%= yeoman.app %>/app/_dependencies/boards/layouts_jpcb.js',
          '<%= yeoman.app %>/app/_dependencies/boards/layouts_kanban.js',
          '<%= yeoman.app %>/app/_dependencies/boards/layouts_saMainMenu.js',
          '<%= yeoman.app %>/app/_dependencies/boards/layouts_status.js',
          '<%= yeoman.app %>/app/_dependencies/boards/layouts_grid.js',
          '<%= yeoman.app %>/app/_dependencies/boards/layouts_queue.js',
          '<%= yeoman.app %>/app/_dependencies/boards/chips*.js',
        ],
        dest: '<%= yeoman.dist %>/js/boards.js'

      },
      sounds_js: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/boards/howler.js',
          '<%= yeoman.app %>/app/_dependencies/boards/jsound.js',
        ],
        dest: '<%= yeoman.dist %>/js/sounds.js'
      },

      uiscript: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/uiscript.js',
        ],
        dest: '<%= yeoman.dist %>/js/uiscript.js'
      },

      print_min: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/print_min.js',
        ],
        dest: '<%= yeoman.dist %>/js/print_min.js'
      },
      amcharts: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/amcharts.js',
        ],
        dest: '<%= yeoman.dist %>/js/amcharts.js'
      },
      serial: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/serial.js',
        ],
        dest: '<%= yeoman.dist %>/js/serial.js'
      },
      gantt: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/gantt.js',
        ],
        dest: '<%= yeoman.dist %>/js/gantt.js'
      },
      light: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/light.js',
        ],
        dest: '<%= yeoman.dist %>/js/light.js'
      },
      exportmin: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/exportmin.js',
        ],
        dest: '<%= yeoman.dist %>/js/exportmin.js'
      },
      uigantt: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/uigantt.js',
        ],
        dest: '<%= yeoman.dist %>/js/uigantt.js'
      },
      pie: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/pie.js',
        ],
        dest: '<%= yeoman.dist %>/js/pie.js'
      },
      gauge: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/gauge.js',
        ],
        dest: '<%= yeoman.dist %>/js/gauge.js'
      },
      funnel: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/funnel.js',
        ],
        dest: '<%= yeoman.dist %>/js/funnel.js'
      },
      signature: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/signature.js',
        ],
        dest: '<%= yeoman.dist %>/js/signature.js'
      },
      signature_pad: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/signature_pad.js',
        ],
        dest: '<%= yeoman.dist %>/js/signature_pad.js'
      },
      uiknob: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/uiknob.js',
        ],
        dest: '<%= yeoman.dist %>/js/uiknob.js'
      },
      uisparkline: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/uisparkline.js',
        ],
        dest: '<%= yeoman.dist %>/js/uisparkline.js'
      },
      uifullcalendar: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/uifullcalendar.js',
        ],
        dest: '<%= yeoman.dist %>/js/uifullcalendar.js'
      },
      uimoment: {
        src: [
          '<%= yeoman.app %>/app/_dependencies/uimoment.js',
        ],
        dest: '<%= yeoman.dist %>/js/uimoment.js'
      }
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: ['*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    ngtemplates: {
      appdashboard: {
        options: {
          module: 'templates_appdashboard',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          'app_dashboard/**/*.html'
        ],
        dest: '.tmp/templates/templates_appdashboard.js'
      },
      sysapp: {
        options: {
          module: 'templates_sysapp',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          '_sys/role/**/*.html',
          '_sys/user/**/*.html',
          '_sys/org/**/*.html',
          '_sys/appl/**/*.html',
          '_sys/service/**/*.html',
          '_sys/files/**/*.html',
          '_sys/job/**/*.html',
          '_sys/menu/**/*.html',
          '_sys/_demo/**/*.html',
        ],
        dest: '.tmp/templates/templates_sysapp.js'
      },
      // app: {
      //   options: {
      //     module: 'templates_app',
      //     standalone:true,
      //     htmlmin: '<%= htmlmin.dist.options %>',
      //   },
      //   cwd: '<%= yeoman.app %>/app/',
      //   src: [
      //   //render
      //           'app/**/*.html' ,
      //   //'app/sales/master/**/*.html',
      //   //'app/sales/pdd/**/*.html',
      //   //'app/sales/registration/**/*.html',
      //   //'app/sales/deliveryRequest/**/*.html',
      //   //'app/sales/sampleListOption/*.html',
      //   //'app/sales/spk/**/*.html',
      //   //'app/sales/prospecting/**/*.html',
      //   //'app/sales/negotiation/**/*.html'
      //   ],
      //   dest: '.tmp/templates/templates_app.js'
      // },
      lib: {
        options: {
          module: 'templates_lib',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/lib/**/*.html',
        ],
        dest: '.tmp/templates/templates_lib.js'
      },
      appboards: {
        options: {
          module: 'templates_appboards',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/boards/**/*.html',
        ],
        dest: '.tmp/templates/templates_appboards.js'
      },
      appbrachorder: {
        options: {
          module: 'templates_appbranchorder',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/branchorder/**/*.html',
        ],
        dest: '.tmp/templates/templates_appbranchorder.js'
      },
      appcrm: {
        options: {
          module: 'templates_appcrm',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/crm/**/*.html',
        ],
        dest: '.tmp/templates/templates_appcrm.js'
      },
      appfinance: {
        options: {
          module: 'templates_appfinance',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/finance/**/*.html',
        ],
        dest: '.tmp/templates/templates_appfinance.js'
      },
      appmaster: {
        options: {
          module: 'templates_appmaster',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/master/**/*.html',
        ],
        dest: '.tmp/templates/templates_appmaster.js'
      },
      appparameter: {
        options: {
          module: 'templates_appparameter',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/parameter/**/*.html',
        ],
        dest: '.tmp/templates/templates_appparameter.js'
      },
      appparts: {
        options: {
          module: 'templates_appparts',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/parts/**/*.html',
        ],
        dest: '.tmp/templates/templates_appparts.js'
      },
      app_gate: {
        options: {
          module: 'templates_app_gate',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/gate/*.html',
        ],
        dest: '.tmp/templates/templates_app_gate.js'
      },
      app_gate_towing: {
        options: {
          module: 'templates_app_gate_towing',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/reception/towing/*.html',
        ],
        dest: '.tmp/templates/templates_app_gate_towing.js'
      },
      app_delivery_warranty: {
        options: {
          module: 'templates_app_delivery_warranty',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/delivery/warranty/*.html',
        ],
        dest: '.tmp/templates/templates_app_delivery_warranty.js'
      },
      app_reception_wogr: {
        options: {
          module: 'templates_app_reception_wogr',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/reception/wo/**/*.html',
          'app/boards/asb/**/*.html',
          'app/boards/factories/**/*.html',
          'app/boards/jpcbgrview/**/*.html',
        ],
        dest: '.tmp/templates/templates_app_reception_wogr.js'
      },
      app_reception_wobp: {
        options: {
          module: 'templates_app_reception_wobp',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/reception/wo_bp/**/*.html',
          'app/services/reception/woHistoryGR/modalFormMasterOpl.html',
        ],
        dest: '.tmp/templates/templates_app_reception_wobp.js'
      },
      app_reception_wolistgr: {
        options: {
          module: 'templates_app_reception_wolistgr',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/reception/woHistoryGR/**/*.html',
          'app/boards/asb/**/*.html',
          'app/boards/factories/**/*.html',
          'app/boards/jpcbgrview/**/*.html',
          'app/services/reception/wo/includeForm/*.html',
        ],
        dest: '.tmp/templates/templates_app_reception_wolistgr.js'
      },
      app_reception_wolistbp: {
        options: {
          module: 'templates_app_reception_wolistbp',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/reception/woHistoryBP/**/*.html',
          'app/services/reception/woHistoryGR/modalFormMasterOpl.html',
          'app/services/reception/wo_bp/includeForm/*.html',
        ],
        dest: '.tmp/templates/templates_app_reception_wolistbp.js'
      },
      app_reception_bstklist: {
        options: {
          module: 'templates_app_reception_bstklist',
          standalone:true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
        //render
                'app/services/reception/bstkList/**/*.html' ,
                'app/services/reception/woHistoryGR/modalFormMasterOpl.html' ,
                'app/services/reception/wo_bp/includeForm/*.html' ,
                'app/services/reception/wo_bp/includeForm/estimationFormInclude/*.html' ,
        ],
        dest: '.tmp/templates/templates_app_reception_bstklist.js'
      },
      // CR4
      app_estimasiBP: {
        options: {
          module: 'templates_app_estimasiBP',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/estimasiBP/**/*.html',

        ],
        dest: '.tmp/templates/templates_app_estimasiBP.js'
      },
      // end CR4	
      appreport: {
        options: {
          module: 'templates_appreport',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/report/**/*.html',
        ],
        dest: '.tmp/templates/templates_appreport.js'
      },
      appsales: {
        options: {
          module: 'templates_appsales',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/report/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales.js'
      },
      appsales1: {
        options: {
          module: 'templates_appsales1',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales1/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales1.js'
      },
      appsales2: {
        options: {
          module: 'templates_appsales2',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales2/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales2.js'
      },
      appsales3: {
        options: {
          module: 'templates_appsales3',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales3/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales3.js'
      },
      appsales4: {
        options: {
          module: 'templates_appsales4',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales4/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales4.js'
      },
      appsales5: {
        options: {
          module: 'templates_appsales5',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales5/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales5.js'
      },
      appsales6: {
        options: {
          module: 'templates_appsales6',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales6/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales6.js'
      },
      appsales7: {
        options: {
          module: 'templates_appsales7',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales7/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales7.js'
      },
      appsales8: {
        options: {
          module: 'templates_appsales8',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales8/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales8.js'
      },
      appsales9: {
        options: {
          module: 'templates_appsales9',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/sales/sales9/**/*.html',
        ],
        dest: '.tmp/templates/templates_appsales9.js'
      },

      appservices: {
        options: {
          module: 'templates_appservices',
          standalone: true,
          htmlmin: '<%= htmlmin.dist.options %>',
        },
        cwd: '<%= yeoman.app %>/app/',
        src: [
          //render
          'app/services/**/*.html',
        ],
        dest: '.tmp/templates/templates_appservices.js'
      },
    },

    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>/js',
          src: '*.js',
          dest: '<%= yeoman.dist %>/js'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dev: {
        files: [{
          expand: true,
          cwd: '.tmp/templates',
          dest: '<%= yeoman.app %>/templates',
          src: ['*.js']
        }, ]
      },
      dist: {
        files: [{
            expand: true,
            dot: true,
            cwd: '<%= yeoman.app %>',
            dest: '<%= yeoman.dist %>',
            src: [
              '*.{ico,png,txt}',
              '*.html',
              'images/**/*.*',
              'sound/boards/*.*',
              'media/**/*.*',
              'fonts/**/*.*',
              'module/**/*.*',
              'styles/fonts/{,*/}*.*',
              'firebase-messaging-sw.js',
            ]
          },
          {
            expand: true,
            cwd: '<%= yeoman.app %>/app/_templates',
            dest: '<%= yeoman.dist %>/templates',
            src: ['*.js']
          },
          {
            expand: true,
            cwd: '.tmp/css',
            dest: '<%= yeoman.dist %>/css',
            src: ['*.css']
          },
          {
            expand: true,
            cwd: '<%= yeoman.app %>/styles',
            dest: '<%= yeoman.dist %>/css',
            src: ['*.eot', '*.svg', '*.woff', '*.ttf']
          },
          {
            expand: true,
            cwd: '.tmp/templates',
            dest: '<%= yeoman.dist %>/templates',
            src: ['*.js']
          },
          // {
          //   expand: true,
          //   cwd: '.tmp/images',
          //   dest: '<%= yeoman.dist %>/images',
          //   src: ['generated/*']
          // },
          // {
          //   expand: true,
          //   cwd: 'bower_components/bootstrap/dist',
          //   src: 'fonts/*',
          //   dest: '<%= yeoman.dist %>'
          // }
        ]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'compass:server'
      ],
      test: [
        'compass'
      ],
      dist: [
        'compass:dist',
        'imagemin',
        'svgmin'
      ]
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true
      }
    }
  });


  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:livereload', 'watch']);
    }
    if (target === 'prod') {
      return grunt.task.run(['buildprod']); //build prod 
      // return grunt.task.run(['buildprod', 'connect:dist:livereload', 'watch']); //build prod and open browser using prod
    }
    grunt.task.run([
      'clean:server',
      //'wiredep',
      //'concurrent:server',
      //'postcss:server',
      'ngtemplates',
      'copy:dev',
      'sails-linker:devJS',
      'sails-linker:devCSS',
      'sails-linker:devTemplate',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  grunt.registerTask('test', [
    'clean:server',
    //'wiredep',
    //'concurrent:test',
    //'postcss',
    'connect:test',
    'karma'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    //'wiredep',
    //'useminPrepare',
    //'concurrent:dist',
    //'postcss',
    'ngtemplates',
    'concat:sysauth_css', 'concat:sysdashboard_css', 'concat:sysapp_css', 'concat:app_css', 'concat:uistyle_css',
    'concat:sysauth1', 'concat:sysauth2dep', 'concat:sysauth2app',
    'concat:sysdashboard', 'concat:appdashboard', 'concat:sysappdep', 'concat:sysapp', 'concat:appdep', //'concat:app',
    //'concat:app_boards',
    'concat:app_branchorder', // 'concat:app_crm',
    'concat:app_finance', 'concat:app_master',
    //'concat:app_parameter',// 'concat:app_parts',
    'concat:app_report',
    'concat:app_sales', 'concat:app_sales1', 'concat:app_sales2', 'concat:app_sales3', 'concat:app_sales4', 'concat:app_sales5', 'concat:app_sales6', 'concat:app_sales7', 'concat:app_sales8', 'concat:app_sales9',
    'concat:app_services',
    'concat:uiscript',
    'concat:amcharts', 'concat:serial', 'concat:gantt', 'concat:light', 'concat:exportmin', 'concat:export_css', 'concat:print_min_css', 'concat:print_min',
    'concat:boards_css', 'concat:boards_js',
    'concat:sounds_js',
    'concat:app_gate',
    'concat:app_gate_towing',
    'concat:app_delivery_warranty',
    'concat:app_reception_wogr',
    'concat:app_reception_wobp',
    'concat:app_reception_wolistgr',
    'concat:app_reception_wolistbp',
    'concat:app_reception_bstklist',
    // Cache Controll
    'concat:envJS',
    // end cache controll
    // CR4
    'concat:app_estimasiBP',
    // end CR4			   
    'concat:asbBoards',
    'concat:lib',
    'concat:signature', 'concat:signature_pad', 'concat:uigantt', 'concat:pie', 'concat:gauge', 'concat:funnel',
    'concat:uiknob', 'concat:uisparkline', 'concat:uifullcalendar', 'concat:uimoment',
    'concat:uitimelinestyle_css', 'concat:uiboxstyle_css', 'concat:uifullcalendarstyle_css', 'concat:uitogglestyle_css',
    'concat:uiganttstyle_css',
    //'ngAnnotate',
    'copy:dist',
    //'cdnify',
    //'cssmin',
    //'uglify',
    //'filerev',
    //'usemin',
    //'htmlmin',
    //'htmlbuild'
    'sails-linker:distJS',
    'sails-linker:distCSS',
    'sails-linker:distTemplate',
    //'watch'
  ]);

  grunt.registerTask('buildprod', [
    'clean:dist',
    //'wiredep',
    //'useminPrepare',
    //'concurrent:dist',
    //'postcss',
    'ngtemplates',
    'concat:sysauth_css', 'concat:sysdashboard_css', 'concat:sysapp_css', 'concat:app_css', 'concat:uistyle_css',
    'concat:sysauth1', 'concat:sysauth2dep', 'concat:sysauth2app',
    'concat:sysdashboard', 'concat:appdashboard', 'concat:sysappdep', 'concat:sysapp', 'concat:appdep', // 'concat:app',
    'concat:app_gate',
    'concat:app_gate_towing',
    'concat:app_delivery_warranty',
    'concat:app_reception_wogr',
    'concat:app_reception_wobp',
    'concat:app_reception_wolistgr',
    'concat:app_reception_wolistbp',
    'concat:app_reception_bstklist',
    // Cache Controll
    'concat:envJS',
    // end cache controll
    // CR4
    'concat:app_estimasiBP',
    // end CR4	  
    'concat:asbBoards',
    'concat:uiscript',
    'concat:lib',
    //'concat:app_boards',
    'concat:app_branchorder', // 'concat:app_crm',
    'concat:app_finance', 'concat:app_master',
    //'concat:app_parameter',// 'concat:app_parts',
    'concat:app_report',
    'concat:app_sales', 'concat:app_sales1', 'concat:app_sales2', 'concat:app_sales3', 'concat:app_sales4', 'concat:app_sales5', 'concat:app_sales6', 'concat:app_sales7', 'concat:app_sales8', 'concat:app_sales9',
    'concat:app_services',
    'concat:uiscript',
    'concat:amcharts', 'concat:serial', 'concat:gantt', 'concat:light', 'concat:exportmin', 'concat:export_css', 'concat:print_min_css', 'concat:print_min',
    'concat:boards_css', 'concat:boards_js',
    'concat:sounds_js',
    'concat:signature', 'concat:signature_pad', 'concat:uigantt', 'concat:pie', 'concat:gauge', 'concat:funnel',
    'concat:uiknob', 'concat:uisparkline', 'concat:uifullcalendar', 'concat:uimoment',
    'concat:uitimelinestyle_css', 'concat:uiboxstyle_css', 'concat:uifullcalendarstyle_css', 'concat:uitogglestyle_css',
    'concat:uiganttstyle_css',
    'ngAnnotate',
    'copy:dist',
    //'cdnify',
    'cssmin',
    'uglify',
    //'filerev',
    //'usemin',
    'htmlmin',
    //'htmlbuild'
    'sails-linker:distJS',
    'sails-linker:distCSS',
    'sails-linker:distTemplate',
    //'watch'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'newer:jscs',
    'test',
    'build'
  ]);
};
