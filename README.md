## Prerequisites
- Install Node.js v4.4.x or above

## Documentation
- Gitlab Getting Started is available at project wiki: https://gitlab.com/BeSmartGit/DMS_AfterSales_FrontEnd/wikis/home
- Download: GitLab_-_Getting_Started.pdf

## After pulling from master
1. run: npm install --only=dev
2. run: npm install

## Build & development

Run `grunt serve:dist` for development mode

## Build & production

Run `grunt serve:prod` for production mode

