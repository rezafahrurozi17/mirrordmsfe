angular.module('templates_app_reception_wogr', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/services/reception/wo/Technicalcompletion.html',
    "<div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <fieldset style=\"border:2px solid #e2e2e2\"> <legend style=\"margin-left:10px;width:inherit;border-bottom:0px\"> WO Information </legend> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-xs-12 col-sm-12 col-md-12\"> <div class=\"col-xs-6 col-sm-6 col-md-6\"> <div class=\"form-group\"> <label class=\"col-xs-4\">Nomor WO </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.WoNo}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-4\">Nomor Polisi </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.PoliceNumber}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-4\">Model </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.ModelType}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-4\">NPWP </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.Npwp.Npwp}}</label> </div> </div> <div class=\"col-xs-6 col-sm-6 col-md-6\"> <div class=\"form-group\"> <label class=\"col-xs-6\">Tanggal WO </label> <label class=\"col-xs-1\">:</label> <!-- <label class=\"col-xs-4\">{{mData.WoCreatedDate | date: 'dd-MM-yyyy h:mma'}}</label> --> <label class=\"col-xs-4\">{{mData.WoCreatedDate | date: 'dd-MM-yyyy HH:mm'}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-6\">Janji Penyerahan</label> <label class=\"col-xs-1\">:</label> <!-- <label class=\"col-xs-4\">{{mData.FixedDeliveryTime1 | date: 'dd-MM-yyyy h:mma'}}</label> --> <label class=\"col-xs-4\">{{mData.FixedDeliveryTime2 | date: 'dd-MM-yyyy HH:mm'}}</label> <!-- <label class=\"col-xs-4\">{{mData.FixedDeliveryTime1 == mData.FixedDeliveryTime2 ? mData.FixedDeliveryTime1 : mData.FixedDeliveryTime2 }}</label> --> </div> <div class=\"form-group\"> <label class=\"col-xs-6\">Status Kendaraan </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-4\">{{mData.MStatusJob.Description}} &nbsp;</label> </div> <div class=\"form-group\"> <label class=\"col-xs-6\">Status Washing </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-4\">{{mData.StatusWashing}} &nbsp;</label> </div> </div> </div> </div> </fieldset> </div> <div class=\"col-md-12\"> <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Job Request\" list-title=\"Permintaan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "              </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" show-errors> <bsreqlabel>Job Request</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Job Request\" name=\"reqdesc\" ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "              </textarea> <bserrmsg field=\"reqdesc\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> <div class=\"col-md-6\"> <!--     <pre>{{vm.appScope.ComplaintCategory}}</pre>\r" +
    "\n" +
    "      <pre>{{appScope.ComplaintCategory}}</pre>\r" +
    "\n" +
    "      <pre>{{ComplaintCategory}}</pre> --> <bslist data=\"JobComplaint\" m-id=\"JobComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Complaint\" list-title=\"Keluhan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Complaint Type: </bsreqlabel> <!--                     <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Complaint Category\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                      </bsselect> --> <input type=\"text\" skip-enable class=\"form-control\" ng-model=\"lmItem.ComplaintCatg\" name=\"\" ng-disabled=\"true\"> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Complaint Text\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                  </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div style=\"margin-bottom:90px\"> <div class=\"form-group\"> <bsreqlabel>Complaint Type</bsreqlabel> <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Complaint Category\" required> </bsselect> <bserrmsg field=\"ctype\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom: 90px\"> <bsreqlabel>Job Complaint</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\" required>\r" +
    "\n" +
    "                                  </textarea> <bserrmsg field=\"ctext\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> <div ng-if=\"haveParts || showOPLlabel\" class=\"col-md-6\"> <div style=\"text-align:center\">Status</div> <div ng-if=\"haveParts\" style=\"height:181px;padding:30px;text-align:center\"> <label> GI Part</label> <h1 style=\"color:red\">{{statusGI}}</h1> </div> <div ng-show=\"showOPLlabel == 1\" style=\"height:181px;padding:5px 5px 5px 5px; text-align:center\"> <label>OPL</label> <h1 style=\"color:red\">{{statusOPL}}</h1> </div> </div> <div class=\"col-md-12\"> <!-- listView --> <bslist data=\"gridWork\" m-id=\"tmpTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApiJob\" confirm-save=\"false\" list-title=\"Job List\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-8 col-xs-8\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe: {{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-4 col-xs-4\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align:right\"><span style=\"float: left\">Rp. </span>{{ totalWork | currency:\"\":0}}</td> <td style=\"text-align:right\"><span style=\"float: left\">Rp. </span>{{ totalWorkDiscounted | currency:\"\":0}}</td> <td style=\"text-align:right\"><span style=\"float: left\">Rp. </span>{{ (totalWork - totalWorkDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts & Bahan)</td> <td style=\"text-align:right\"><span style=\"float: left\">Rp. </span>{{totalMaterial | currency:\"\":0}}</td> <td style=\"text-align:right\"><span style=\"float: left\">Rp. </span>{{ totalMaterialDiscounted | currency:\"\":0}}</td> <td style=\"text-align:right\"><span style=\"float: left\">Rp. </span>{{ (totalMaterial - totalMaterialDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalPPN | currency:\"\":0}}</td> </tr> <tr> <td style=\"font-weight:bold\" class=\"text-right\" colspan=\"3\">Total</td> <td style=\"text-align:right;font-weight:bold\"><span style=\"float: left\">Rp. </span> {{ totalEstimasi| currency:\"\":0}}</td> </tr> <tr> <td style=\"font-weight:bold\" colspan=\"3\" class=\"text-right\">DP</td> <td style=\"text-align:right;font-weight:bold\"><span style=\"float: left\">Rp. </span>{{totalDp | currency:\"\":0}}</td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <!-- OPL BSLIST DISINI! --> <bslist data=\"oplData\" m-id=\"Id\" list-model=\"lmModelOpl\" on-before-save=\"onBeforeSaveOpl\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <strong>{{lmItem.OPLName}}</strong> </div> <div class=\"form-group\"> {{lmItem.VendorName}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Estimasi : </label> {{lmItem.TargetFinishDate | date:'dd-MM-yyyy'}} </div> <div class=\"form-group\"> <label>Quantity : </label> {{lmItem.QtyPekerjaan}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}} </div> </div> </div> </bslist-template> <bslist-modal-form> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMasterOpl.html'\"> </div></bslist-modal-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <tbody> <tr> <td width=\"20%\">Total</td> <td>{{sumWorkOpl * (1.1) | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> <div class=\"col-md-12\" ng-show=\"checkIsWarranty == 1\"> <!-- <fieldset style=\"border:2px solid #e2e2e2;\"> --> <!-- <legend style=\"margin-left:10px;width:inherit;border-bottom:0px;\">\r" +
    "\n" +
    "                    Warranty Info\r" +
    "\n" +
    "                </legend> --> <!-- <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-12\">\r" +
    "\n" +
    "                        <div class=\"col-md-6\">\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>No. Claim :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>No. TWC :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>VDS :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>VIS :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>Total Amount :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>Approved :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"col-md-6\">\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>Tipe Claim :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>WMI :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>CD :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>Status Claim :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <label>Error code :</label>\r" +
    "\n" +
    "                                <label>{{}}</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div> --> <bslist data=\"getWarrantyInfo\" m-id=\"tmpTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModelWarr\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApiJob\" confirm-save=\"false\" list-title=\"Warranty Info\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Claim :</label> <label>{{lmItem.ClaimNo}}</label> </div> <div class=\"form-group\"> <label>No. TWC :</label> <label>{{lmItem.TwcNo}}</label> </div> <div class=\"form-group\"> <label>VDS :</label> <label>{{lmItem.VDS}}</label> </div> <div class=\"form-group\"> <label>VIS :</label> <label>{{lmItem.VIS}}</label> </div> <div class=\"form-group\"> <label>Total Amount :</label> <label>{{lmItem.totAmount | currency:\"Rp. \":0}}</label> </div> <div class=\"form-group\"> <label>Approved :</label> <label>{{lmItem.Approved}}</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Tipe Claim :</label> <label>{{lmItem.ClaimType}}</label> </div> <div class=\"form-group\"> <label>WMI :</label> <label>{{lmItem.WMI}}</label> </div> <div class=\"form-group\"> <label>CD :</label> <label>{{lmItem.CD}}</label> </div> <div class=\"form-group\"> <label>Status Claim :</label> <label>{{lmItem.StatusClaim}}</label> </div> <div class=\"form-group\"> <label>Error code :</label> <label>{{lmItem.ErrorCode}}</label> </div> </div> </div> </div> </bslist-template> </bslist> <!-- <div class=\"\">\r" +
    "\n" +
    "          <div class=\"col-md-12\">\r" +
    "\n" +
    "            <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "              <thead>\r" +
    "\n" +
    "                <tr>\r" +
    "\n" +
    "                  <th>Nomor LT</th>\r" +
    "\n" +
    "                  <th>Description</th>\r" +
    "\n" +
    "                  <th>Claim Amount</th>\r" +
    "\n" +
    "                  <th>Status</th>\r" +
    "\n" +
    "                  <th>Warranty Number</th>\r" +
    "\n" +
    "                  <th>Acceptable Amount</th>\r" +
    "\n" +
    "                  <th>Error Code</th>\r" +
    "\n" +
    "                </tr>\r" +
    "\n" +
    "              </thead>\r" +
    "\n" +
    "              <tbody>\r" +
    "\n" +
    "                <tr>\r" +
    "\n" +
    "                  <td>Nomor LT</td>\r" +
    "\n" +
    "                  <td>Description</td>\r" +
    "\n" +
    "                  <td>Claim Amount</td>\r" +
    "\n" +
    "                  <td>Status</td>\r" +
    "\n" +
    "                  <td>Warranty Number</td>\r" +
    "\n" +
    "                  <td>Acceptable Amount</td>\r" +
    "\n" +
    "                  <td>Error Code</td>\r" +
    "\n" +
    "                </tr>\r" +
    "\n" +
    "              </tbody>\r" +
    "\n" +
    "            </table>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </div> --> <!-- </fieldset> --> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Status TECO : </label> <input type=\"text\" class=\"form-control\" placeholder=\"Status TECO\" name=\"totalDp\"> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> </div> <div class=\"col-md-9\"> <div class=\"btn-group pull-right\"> <!-- {{btnComplete}} - {{checkIsWarranty}} - {{showCompleteTeco}} {{(btnComplete || checkIsWarranty == 0 && showCompleteTeco != 1) }} --> <button type=\"button\" ng-disabled=\"(btnComplete || checkIsWarranty == 1) || holdLoadingSubmit\" skip-enable ng-show=\"showSubmitTeco > 0\" class=\"rbtn btn\" ng-click=\"SubmitApprovalTecho(mData,gridWork)\"> <!--  ng-show=\"showSubmitTeco > 0\" --> Submit </button> <button type=\"button\" ng-disabled=\"(((checkIsWarranty == 0 || checkIsWarranty == 2) && showSubmitTeco > 0 || btnComplete || btnCompleteOPL))\" skip-enable ng-if=\"(mData.Status >= 12 && mData.Status <=13) && showCompleteTeco > 0\" ng-click=\"completeTeco(mData)\" class=\"rbtn btn\"> Complete </button> <!-- <button type=\"button\" ng-disabled=\"(btnComplete) || (checkIsWarranty == 0)\" skip-enable ng-if=\"(mData.Status >= 12 && mData.Status <=13) && showSubmitTeco >= 0\" ng-click=\"completeTeco(mData)\" class=\"rbtn btn\">\r" +
    "\n" +
    "                  Complete 2!\r" +
    "\n" +
    "                  </button> --> <button type=\"button\" ng-disabled=\"btnComplete\" ng-if=\"mData.Status == 14\" ng-click=\"UncompleteTeco(mData)\" class=\"rbtn btn\"> UnTeco </button> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/callCustomer.html',
    "<div> <div ng-hide=\"isService\" class=\"row\"> <div class=\"col-md-12\"> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <legend style=\"margin-left:10px;width:inherit;border-bottom:0px\"> WO Information </legend> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nomor WO :</label> <label>{{mData.WoNo}}</label> </div> <div class=\"form-group\"> <label>Nomor Polisi :</label> <label>{{mData.PoliceNumber}}</label> </div> <div class=\"form-group\"> <label>Model :</label> <label>{{mData.ModelType}}</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Tanggal WO :</label> <label>{{mData.WoCreatedDate | date:'MM/dd/yyyy'}}</label> </div> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Category</bsreqlabel> <bsselect name=\"Category\" ng-model=\"mData.Category\" data=\"woCategory\" item-text=\"Name\" item-value=\"Category\" placeholder=\"Pilih Kategori\" ng-disabled=\"true\" skip-enabled required> </bsselect> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"TaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" list-api=\"listApi\" confirm-save=\"false\" list-title=\"Job List\" modal-title=\"Job Data\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.paidName}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> </div> <div class=\"col-md-12\"> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Nomor LT</th> <th>Description</th> <th>Claim Amount</th> <th>Status</th> <th>Warranty Number</th> <th>Acceptable Amount</th> <th>Error Code</th> </tr> </thead> <tbody> <tr ng-repeat=\"a in dataWarranty\"> <td>{{a.LTNo}}</td> <!-- belum --> <td>{{a.Description}}</td> <!-- belum  --> <td>{{a.ClaimAmount}}</td> <td>{{a.Lama}}</td> <!-- belum --> <td>{{a.claimNo}}</td> <td>{{a.ClaimGrandTotal}}</td> <!-- belum --> <td>{{a.ErrorCode}}</td> <!-- belum --> </tr> </tbody> </table> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Lama Pekerjaan : </label> <input type=\"text\" class=\"form-control\" placeholder=\"0\" name=\"LamaPekerjaan\" ng-model=\"mData.LamaPekerjaan\" ng-disabled=\"true\" skip-enable> </div> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Total DP:</label> <input type=\"text\" name=\"dp\" class=\"form-control\" placeholder=\"Total DP\" ng-model=\"mData.TotalDP\" ng-disabled=\"true\" skip-enable> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Part Bekas</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radio1\" ng-model=\"isParts\" name=\"radio1\" value=\"1\" checked ng-disabled=\"true\" skip-enabled> <label for=\"radio1\">Kembalikan</label> <input type=\"radio\" id=\"radio2\" ng-model=\"isParts\" name=\"radio1\" value=\"0\" ng-disabled=\"true\" skip-enabled> <label for=\"radio2\">Tinggal</label> </div> <!-- {{isParts}} --> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Waiting / Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"wait1\" ng-model=\"isWaiting\" name=\"radio2\" value=\"1\" checked ng-disabled=\"true\" skip-enabled> <label for=\"wait1\">Waiting</label> <input type=\"radio\" id=\"wait2\" ng-model=\"isWaiting\" name=\"radio2\" value=\"0\" ng-disabled=\"true\" skip-enabled> <label for=\"wait2\">Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Car Wash</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"washing1\" ng-model=\"isWashing\" name=\"radio3\" value=\"1\" checked ng-disabled=\"true\" skip-enabled> <label for=\"washing1\">Wash</label> <input type=\"radio\" id=\"washing2\" ng-model=\"isWashing\" name=\"radio3\" value=\"0\" ng-disabled=\"true\" skip-enabled> <label for=\"washing2\">No Wash</label> </div> <!-- {{isWashing}} --> </div> <div class=\"row\"> <div class=\"col-md-3\"> <label>Scheduled Service Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.firstDate\" ng-disabled=\"true\" skip-enabled> </bsdatepicker> </div> <div class=\"form-group text-center\" style=\"width:20px\"> <b>-</b> </div> <div class=\"form-group\"> <bsdatepicker name=\"lastDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.lastDate\" ng-disabled=\"true\" skip-enabled> </bsdatepicker> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <label>Estimation Delivery Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.Estimate\" ng-disabled=\"true\" skip-enabled> </bsdatepicker> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <label>Adjusment Delivery Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.Adjusment\" ng-disabled=\"true\" skip-enabled> </bsdatepicker> </div> <div class=\"form-group\" style=\"width:200px\"> <bscheckbox ng-model=\"mData.customerRequest\" label=\"Customer Request\" ng-click=\"\" true-value=\"2\" false-value=\"0\" ng-disabled=\"true\" skip-enabled> </bscheckbox> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"btn-group pull-right\"> <button class=\"rbtn btn\" ng-click=\"callingCustomer(mData)\">Call Customer</button> </div> </div> </fieldset> </div> <bsui-modal show=\"show_modal.show\" title=\"Customer Notification\" data=\"modal_model\" on-save=\"saveCustomer\" on-cancel=\"cancelCustomer\" button-settings=\"buttonSettingModal2\" mode=\"modalMode\"> <div ng-form name=\"customerForm\"> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <div class=\"col-md-12\" style=\"text-align:center\"> <h2>Lakukan proses notifikasi sekarang?</h2> <h3>Silahkan tekan nomor telepon dibawah untuk menelepon</h3> <p> <h3>{{modal_model.ContactPerson}}</h3> </p> <p> <h3>{{modal_model.PhoneContactPerson1}}&nbsp;&nbsp;<a href=\"Tel:{{modal_model.PhoneContactPerson1}}\"><i style=\"color:green\" class=\"fa fa-lg fa-phone\"></i></a></h3> </p> <p> <h3>{{modal_model.PhoneContactPerson2}}&nbsp;&nbsp;<a href=\"Tel:{{modal_model.PhoneContactPerson2}}\"><i style=\"color:green\" class=\"fa fa-lg fa-phone\"></i></a></h3> </p> </div> </fieldset> </div> </bsui-modal> </div> <div ng-show=\"isService\"> <div smart-include=\"app/services/reception/wo/serviceExplanation.html\"></div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/customerInfo.html',
    "hello hello"
  );


  $templateCache.put('app/services/reception/wo/dialogApproveWarranty.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"><h3 class=\"panel-title\">Approval Warranty</h3> </div> <div class=\"panel-body\"> <form method=\"post\"> <!-- <div class=\"row\">\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-10\">\r" +
    "\n" +
    "\t\t\t\t<p><i>Permohonan Warranty</i></p>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</div> --> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label class=\"control-label\">Permohonan </label> </div> </div> <div class=\"col-md-7\"> <div class=\"form-group\"> <label class=\"control-label\">Pengajuan Warranty </label> </div> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-5\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\">\r" +
    "\n" +
    "\t\t\t\t\t<label class=\"control-label\">Nomor Request\r" +
    "\n" +
    "\t\t\t\t\t</label>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-7\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\">\r" +
    "\n" +
    "\t\t\t\t\t<label class=\"control-label\">\r" +
    "\n" +
    "\t\t\t\t\t</label>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</div>  --> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label class=\"control-label\">Tanggal dan Jam </label> </div> </div> <div class=\"col-md-7\"> <div class=\"form-group\"> <label class=\"control-label\">{{CurrentDate | date:'dd/MM/yyyy HH:mm'}} </label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label class=\"control-label\">Pemohon</label> </div> </div> <div class=\"col-md-7\"> <textarea ng-model=\"DataSelect.Requester\" class=\"form-control\" rows=\"4\"></textarea> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label class=\"control-label\">Keterangan </label> </div> </div> <div class=\"col-md-7\"> <textarea class=\"form-control\" rows=\"4\" ng-model=\"DataSelect.Requestmessage\">\r" +
    "\n" +
    "\t\t\t\t</textarea> </div> </div> <br> <!-- <div class=\"row\">\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-5\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\">\r" +
    "\n" +
    "\t\t\t\t\t<label class=\"control-label\">\r" +
    "\n" +
    "\t\t\t\t\t</label>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-7\">\r" +
    "\n" +
    "\t\t\t\t<textarea class=\"form-control\" ng-model=\"ApproveData.CancelReasonDesc\" rows=\"4\">\r" +
    "\n" +
    "\t\t\t\t\tMohon untuk approve list dari item Sales Order berikut. Terima kasih.\r" +
    "\n" +
    "\t\t\t\t</textarea>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</div>  --> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div> <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0)\">Batal</button> <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"confirm()\" style=\"color:white; background-color:#D53337\">Permohonan Approval</button> </div> </div> </div> </div> </form> </div> </div> <!-- <pre>{{ApproveData}}</pre> -->"
  );


  $templateCache.put('app/services/reception/wo/includeForm/CPPKInfo.html',
    "<div class=\"panel panel-default\"> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-xs-6 col-xm-6 col-md-6\"> <div class=\"form-group\"> <label>List Kontak Person</label> <bsselect name=\"ContactPerson\" ng-disabled=\"disableCPPK\" skip-enable ng-model=\"mDataDM.CP\" data=\"cpList\" item-text=\"Name\" item-value=\"Name\" placeholder=\"Pilih Kontak Person\" on-select=\"onChangeCP(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.CP.Name\" placeholder=\"Nama\" maxlength=\"50\"> </div> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataDM.CP.HP1\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataDM.CP.HP2\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> </div> <!-- ================= --> <div class=\"col-xs-6 col-xm-6 col-md-6\"> <div class=\"form-group\"> <label>List Pengambil Keputusan</label> <bsselect name=\"PengambilKeputusan\" ng-disabled=\"disableCPPK\" skip-enable ng-model=\"mDataDM.PK\" data=\"pkList\" item-text=\"Name\" item-value=\"Name\" placeholder=\"Pilih Penanggung Jawab\" on-select=\"onChangePK(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.PK.Name\" placeholder=\"Nama\" maxlength=\"50\"> </div> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataDM.PK.HP1\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataDM.PK.HP2\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/OPLNew.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Order Pekerjaan Luar</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-6 col-xs-6\"> <button class=\"ui icon inverted grey button\" style=\"font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px\" ng-click=\"deleteAllDataGridViewWork()\"><i class=\"fa fa-trash\"></i> </button> </div> <div class=\"col-md-6 col-xs-6\"> <button ng-click=\"plushOrderOpl()\" class=\"rbtn btn pull-right\" data-target=\"#ModalOP\" on-deny=\"denied\" data-toggle=\"modal\" data-backdrop=\"static\" data-keyboard=\"false\"><i class=\"fa fa-plus\"></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridWorkView\" class=\"ag-theme-balham\" style=\"height: 400px\"></div> </fieldset> <div class=\"ui modal ModalOPL\" role=\"dialog\" style=\"left:0% !important; width:75%; margin:-284px auto 0px; background-color: transparent !important\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Order Pekerjaan Luar</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <label>Kode Vendor</label> <input type=\"text\" class=\"form-control\" name=\"VendorCode\" ng-model=\"mOpl.VendorCode\" ng-disabled=\"true\" skip-enable> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <bsreqlabel>Vendor</bsreqlabel> <select name=\"vendor\" convert-to-number class=\"form-control\" ng-model=\"mOpl.vendor\" data=\"OplVendor\" placeholder=\"Pilih Vendor\" required ng-disabled=\"viewMode\" skip-enable ng-change=\"chooseVendor()\"> <option ng-repeat=\"sat in OplVendor\" value=\"{{sat.VendorId}}\">{{sat.Name}}</option> </select> <bserrmsg field=\"Vendor\"></bserrmsg> </div> </div> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">OPL</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-12 col-xs-12\"> <button ng-disabled=\"viewMode\" skip-enable ng-click=\"plushOPL()\" class=\"rbtn btn pull-right\"><i class=\"fa fa-plus\" n></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridDetailOPL\" class=\"ag-theme-balham\" style=\"height: 400px\" id=\"gridParts\"></div> </fieldset> </div> <!-- <label>{{OplVendor}}</label> --> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important;background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"wbtn btn ng-binding\" style=\"width:100px\" ng-click=\"closeModalOpl()\">Cancel</div> <div class=\"rbtn btn ng-binding ng-scope\" ng-disabled=\"viewMode || disableSimpan || masterOPL.length == 0\" skip-enable ng-click=\"saveFinalOpl()\" style=\"width:100px\">Simpan</div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpl.html\"><a>\r" +
    "\n" +
    "                    <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                    <span ng-bind-html=\"match.model.Name | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                    <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                    <span ng-bind-html=\"match.model.isExternalDesc | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                    <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "                </a></script> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/customer-01.html',
    "<div class=\"panel panel-default\"> <!-- <div class=\"panel-body\"> --> <div class=\"panel-body\"> <div id=\"rowCustDisableCompleted\" class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Pelanggan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <select ng-show=\"mode == 'search' || mode == 'form'\" class=\"form-control\" ng-model=\"action\" ng-change=\"changeOwner(selected)\">\r" +
    "\n" +
    "                        <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                        <option value=\"2\">Input Pemilik Data Baru</option>\r" +
    "\n" +
    "                    </select>\r" +
    "\n" +
    "                    <select ng-show=\"mode=='view'\" class=\"form-control\" ng-model=\"action\">\r" +
    "\n" +
    "                        <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                        <option value=\"2\">input Pemilik Data Baru</option>\r" +
    "\n" +
    "                    </select> --> <!-- <bsselect ng-show=\"mode == 'search' || mode == 'form'\" on-select=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                    </bsselect> --> <select class=\"form-control\" convert-to-number ng-show=\"mode == 'search' || mode == 'form'\" ng-change=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"act1 in actionCdata\" value=\"{{act1.Id}}\">{{act1.Value}}</option> </select> <!-- <bsselect ng-show=\"mode=='view'\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                    </bsselect> --> <select class=\"form-control\" convert-to-number ng-show=\"mode=='view'\" data=\"actionCdata\" ng-change=\"changeInfo(mFilterCust.action)\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"act2 in actionCdata\" value=\"{{act2.Id}}\">{{act2.Value}}</option> </select> </div> </div> <div ng-show=\"mode == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-disabled=\"disabledEdit || (mFilterCust.action == null || mFilterCust.action == undefined)\" skip-enable ng-show=\"editenable && mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <!-- <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-disabled=\"mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> </div> </div> </div> <div class=\"row\" ng-show=\"mode == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null || mFilterCust.action == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-3\"> <label>Toyota ID</label> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" ng-model=\"filterSearch.TID\" placeholder=\"Input Toyota ID\" disallow-space> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelected}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterField\"> <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag != 6)\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" ng-disabled=\"filterSearch.choice\" disallow-space> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag == 6)\"> <bsdatepicker date-options=\"XFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null\" ng-click=\"searchCustomer(mFilterCust.action, filterSearch)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div ng-show=\"mode == 'search'\" class=\"panel-body\"> <div class=\"row\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\" ng-show=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Pelanggan</bsreqlabel> <!-- <select class=\"form-control\" ng-model=\"mDataCrm.Customer.CustomerTypeId\" ng-change=\"changeType(mDataCrm.Customer.CustomerTypeId)\">\r" +
    "\n" +
    "                        <option value=\"3\">Individu</option>\r" +
    "\n" +
    "                        <option value=\"4\">Pemerintah</option>\r" +
    "\n" +
    "                        <option value=\"5\">Yayasan</option>\r" +
    "\n" +
    "                        <option value=\"6\">Perusahaan</option>\r" +
    "\n" +
    "                    </select> --> <select class=\"form-control\" convert-to-number ng-model=\"mDataCrm.Customer.CustomerTypeId\" ng-change=\"changeType(mDataCrm.Customer.CustomerTypeId)\"> <!-- <option value=\"3\">Individu</option>\r" +
    "\n" +
    "                        <option value=\"4\">Pemerintah</option>\r" +
    "\n" +
    "                        <option value=\"5\">Yayasan</option>\r" +
    "\n" +
    "                        <option value=\"6\">Perusahaan</option> --> <option ng-repeat=\"item in tipePemilik\" value=\"{{item.Id}}\">{{item.Name}}</option> </select> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldInst\" ng-form=\"listInst\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"Name\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\" required> <em ng-messages=\"listInst.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>Nama PIC</bsreqlabel> --> <bsreqlabel>Nama Penanggung Jawab</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"NamePic\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\" required maxlength=\"50\"> <em ng-messages=\"listInst.NamePic.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>PIC Department</bsreqlabel> --> <bsreqlabel>Department Penanggung Jawab</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" name=\"PICDepartment\" placeholder=\"PIC Department\" required> --> <select name=\"PICDepartmentId\" convert-to-number class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\" required> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> <em ng-messages=\"listInst.PICDepartmentId.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>PIC HP</bsreqlabel> --> <bsreqlabel>HP Penanggung Jawab</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\" name=\"PICHp\" maxlength=\"15\" required> </div> <em ng-messages=\"listInst.PICHp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" maxlength=\"20\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" name=\"SIUP\" style=\"text-transform: uppercase\" required> <em ng-messages=\"listInst.SIUP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>PIC Email</bsreqlabel> --> <bsreqlabel>Email Penanggung Jawab</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\" required name=\"PICEmail\"> <em ng-messages=\"listInst.PICEmail.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No. NPWP\" name=\"NpwpInst\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listInst.NpwpInst.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" on-before-cancel=\"onListCancel\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> <option ng-repeat=\"cat1 in categoryContact\" value=\"{{cat1.AddressCategoryId}}\">{{cat1.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Provinsi...\" ng-change=\"selectProvince(lmModelAddress)\" required> <option ng-repeat=\"prov in provinsiData\" value=\"{{prov.ProvinceId}}\">{{prov.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" required> <option ng-repeat=\"kab in kabupatenData\" value=\"{{kab.CityRegencyId}}\">{{kab.CityRegencyName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.CityRegencyId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" required> <option ng-repeat=\"kec in kecamatanData\" value=\"{{kec.DistrictId}}\">{{kec.DistrictName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.DistrictId\" hidden required> </div> <div class=\"form-group\"> <label>Kelurahan</label> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" required> <option ng-repeat=\"kel in kelurahanData\" value=\"{{kel.VillageId}}\">{{kel.VillageName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.VillageId\" hidden required> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                            <bsreqlabel>Alamat Utama</bsreqlabel>\r" +
    "\n" +
    "                                            <bsselect name=\"contact\" ng-model=\"lmModelAddress.MainAddress\" data=\"dataMainAddress\" item-text=\"Description\" item-value=\"Id\" placeholder=\"Pilih Informasi\" skip-disable=\"true\" on-select=\"selectMainAddress(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect>\r" +
    "\n" +
    "                                        </div> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagToyotaID(mDataCrm)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"flToyota in flagToyotaID\" ng-selected=\"mDataCrm.Customer.ToyotaIDFlag == flToyota.Id\" value=\"{{flToyota.Id}}\">{{flToyota.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <!-- <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                            </bsselect> --> <select convert-to-number name=\"FlagAfco\" class=\"form-control\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagAfco(mDataCrm)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"FlagAfco in FlagAfco\" ng-selected=\"mDataCrm.Customer.AFCOIdFlag == FlagAfco.Id\" value=\"{{FlagAfco.Id}}\">{{FlagAfco.Desc}}</option> </select> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"(mode=='form' || mode=='search')\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listInst.$invalid || CustomerAddress.length == 0\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldPersonal\" ng-form=\"listPersonal\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" placeholder=\"Gelar Depan\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <!-- style=\"text-transform: uppercase;\"  --> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama\" required name=\"Name\" maxlength=\"50\"> <!-- style=\"text-transform: uppercase;\" --> <em ng-messages=\"listPersonal.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" placeholder=\"Gelar Belakang\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Lahir</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"BirthDate\" ng-model=\"mDataCrm.Customer.BirthDate\" date-options=\"dateOptionsBirth\" max-date=\"maxDateOption.maxDate\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" required> </bsdatepicker> <!-- <bserrmsg field=\"BirthDate\"></bserrmsg> --> <em ng-messages=\"listPersonal.BirthDate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" ng-maxlength=\"14\" maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" placeholder=\"Handphone 1\" numbers-only required name=\"HandPhone1\"> <em ng-messages=\"listPersonal.HandPhone1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" ng-maxlength=\"14\" maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" placeholder=\"Handphone 2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.KTPKITAS\" minlength=\"16\" maxlength=\"16\" placeholder=\"No. KTP/KITAS\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> <!-- required --> <em ng-messages=\"listPersonal.KTPKITAS.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No. NPWP\" name=\"Npwp\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listPersonal.Npwp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-show-detail=\"onShowDetailAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" custom-button-settings-detail=\"listCustomButtonSettingsDetailAdress\" on-after-save=\"onAfterSaveAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" ng-change=\"selectPendapatan(lmModelAddress)\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> <!--  numbers-only --> </div> <div class=\"form-group\"> <label>No. Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Provinsi...\" ng-change=\"selectProvince(lmModelAddress)\" required> <option ng-repeat=\"provinsiData in provinsiData\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" required> <option ng-repeat=\"kabupatenData in kabupatenData\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.CityRegencyId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" required> <option ng-repeat=\"kecamatanData in kecamatanData\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.DistrictId\" hidden required> </div> <div class=\"form-group\"> <label>Kelurahan</label> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" required> <option ng-repeat=\"kelurahanData in kelurahanData\" value=\"{{kelurahanData.VillageId}}\" postalcode=\"{{kelurahanData.PostalCode}}\">{{kelurahanData.VillageName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.VillageId\" hidden required> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagToyotaID(mDataCrm)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"flagToyotaID in flagToyotaID\" ng-selected=\"mDataCrm.Customer.ToyotaIDFlag == flagToyotaID.Id\" value=\"{{flagToyotaID.Id}}\">{{flagToyotaID.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listPersonal.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"FlagAfco\" class=\"form-control\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagAfco(mDataCrm)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"FlagAfco in FlagAfco\" ng-selected=\"mDataCrm.Customer.AFCOIdFlag == FlagAfco.Id\" value=\"{{FlagAfco.Id}}\">{{FlagAfco.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listPersonal.$invalid || CustomerAddress.length == 0\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> </div> </div> <!-- untuk tampilan depan --> <div class=\"panel-body\" ng-hide=\"mode =='search'\"> <div class=\"row\" ng-show=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <!-- <input type=\"text\" ng-readonly='true' class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" placeholder=\"PIC Department\"> --> <select name=\"picDepartment\" convert-to-number ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\"> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" maxlength=\"15\" placeholder=\"PIC HP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" style=\"text-transform: uppercase\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\"> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" ng-change=\"selectPendapatan(lmModelAddress)\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> </div> <div class=\"form-group\"> <label>No. Telepon 2 </label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Provinsi...\" ng-change=\"selectProvince(lmModelAddress)\" required> <option ng-repeat=\"provinsiData in provinsiData\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceData}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kabupaten\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.CityRegencyName\" required> <!-- <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kabupatenData in kabupatenData\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kecamatan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.DistrictName\" required> <!-- <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kecamatanData in kecamatanData\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <label>Kelurahan</label> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kelurahan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.VillageName\" required> <!-- <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kelurahanData in kelurahanData\" value=\"{{kelurahanData.VillageId}}\">{{kelurahanData.VillageName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagToyotaID(mDataCrm)\" icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"flagToyotaID in flagToyotaID\" value=\"{{flagToyotaID.Id}}\">{{flagToyotaID.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <!-- <bsselect name=\"FlagAfco\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                required=\"true\">\r" +
    "\n" +
    "                            </bsselect> --> <select convert-to-number name=\"FlagAfco\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagAfco(mDataCrm)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"FlagAfco in FlagAfco\" value=\"{{FlagAfco.Id}}\">{{FlagAfco.Desc}}</option> </select> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> </div> <div class=\"row\" ng-hide=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" ng-readonly=\"true\" placeholder=\"Gelar Depan\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" ng-readonly=\"true\" placeholder=\"Nama\"> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" ng-readonly=\"true\" placeholder=\"Gelar Belakang\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" ng-readonly=\"true\" numbers-only> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" ng-readonly=\"true\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.KTPKITAS\" ng-readonly=\"true\" minlength=\"16\" maxlength=\"16\" placeholder=\"No. KTP/KITAS\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" ng-readonly=\"true\" placeholder=\"No. NPWP\" name=\"Npwp\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address1\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\" ng-disabled=\"true\" skip-enabled> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" ng-change=\"selectPendapatan(lmModelAddress)\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Provinsi...\" ng-change=\"selectProvince(lmModelAddress)\" required> <option ng-repeat=\"provinsiData in provinsiData\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kabupaten\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.CityRegencyName\" required> <!-- <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kabupatenData in kabupatenData\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kecamatan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.DistrictName\" required> <!-- <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kecamatanData in kecamatanData\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <label>Kelurahan</label> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kelurahan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.VillageName\" required> <!-- <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kelurahanData in kelurahanData\" value=\"{{kelurahanData.VillageId}}\">{{kelurahanData.VillageName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagToyotaID(mDataCrm)\"\r" +
    "\n" +
    "                                icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"flagToyotaID in flagToyotaID\" value=\"{{flagToyotaID.Id}}\">{{flagToyotaID.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect convert-to-number name=\"FlagAfco\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"FlagAfco\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagAfco(mDataCrm)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"FlagAfco in FlagAfco\" value=\"{{FlagAfco.Id}}\">{{FlagAfco.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> </div> </div> <hr width=\"95%\" style=\"height:3px;background-color:grey\"> <div class=\"panel-body\"> <div id=\"rowVehicDisableCompleted\" class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Kendaraan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <bsselect ng-show=\"modeV == 'search' || modeV == 'form'\" on-select=\"changeVehic(mFilterVehic.actionV)\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                    </bsselect> --> <select class=\"form-control\" convert-to-number ng-show=\"modeV == 'search' || modeV == 'form'\" ng-change=\"changeVehic(mFilterVehic.actionV)\" data=\"actionVdata\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"actionVdata in actionVdata\" value=\"{{actionVdata.Id}}\">{{actionVdata.Value}}</option> </select> <!-- <bsselect ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                    </bsselect>                                         --> <select class=\"form-control\" convert-to-number ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-change=\"changeKendaraan(mFilterVehic.actionV)\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"actionVdata in actionVdata\" value=\"{{actionVdata.Id}}\">{{actionVdata.Value}}</option> </select> </div> </div> <div ng-show=\"modeV == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" ng-show=\"editenable && modeV=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" skip-enable ng-disabled=\"disabledEditV || (mFilterVehic.actionV == null || mFilterVehic.actionV == undefined) \" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> </div> <div class=\"row\" ng-show=\"modeV == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null || mFilterVehic.actionV == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelectedV}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterFieldV\"> <a href=\"#\" ng-click=\"filterFieldChangeV(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearchV.filterValue\" ng-keypress=\"allowPatternFilterSrc($event,mFilterVehic.actionV,filterSearchV)\" ng-disabled=\"filterSearchV.choice\" disallow-space> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null\" ng-click=\"searchVehicle(mFilterVehic.actionV, filterSearchV)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div class=\"panel-body\" ng-form=\"listVehicle\" ng-hide=\"!hideVehic\" novalidate> <div class=\"col-md-12\" ng-show=\"!disVehic\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tipe Kendaraan</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changeTypeVehic1\" ng-change=\"checkTam(mFilterTAM.isTAM)\" ng-disabled=\"disableCheckTam\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"1\" checked> <label for=\"changeTypeVehic1\">TAM</label> <input ng-change=\"checkTam(mFilterTAM.isTAM)\" type=\"radio\" id=\"changeTypeVehic2\" ng-disabled=\"disableCheckTam\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"0\"> <label for=\"changeTypeVehic2\">Non TAM</label> </div> </div> </div> </div> <div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Polisi</bsreqlabel> <input name=\"noPolice\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-model=\"mDataCrm.Vehicle.LicensePlate\" ng-maxlength=\"10\" ng-keypress=\"allowPatternFilter($event,3,filter.noPolice)\" placeholder=\"No Polisi\" required> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No Rangka</bsreqlabel> <input name=\"VIN\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)) || mData.Status >= 4\" skip-enable ng-model=\"mDataCrm.Vehicle.VIN\" placeholder=\"No Rangka\" required ng-maxlength=\"17\" ng-minlength=\"12\" maxlength=\"17\"> <em ng-messages=\"listVehicle.VIN.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>Model Code</bsreqlabel>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ModelCode\" placeholder=\"Model Code\">\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div> --> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <!-- <bsselect data=\"VehicM\" name=\"vehicModel\" on-select=\"selectedModel(selected)\" item-value=\"VehicleModelId\" item-text=\"VehicleModelName\" ng-model=\"mDataCrm.Vehicle.Model\" placeholder=\"Vehicle Model\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </bsselect> --> <ui-select name=\"vehicModel\" ng-if=\"mFilterTAM.isTAM == 1 && mDataCrm.Vehicle.SourceDataId !== null \" ng-model=\"mDataCrm.Vehicle.Model\" data=\"VehicM\" on-select=\"selectedModel($item)\" ng-required=\"true\" search-enabled=\"false\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Model\"> {{$select.selected.VehicleModelName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicM\"> <span ng-bind-html=\"item.VehicleModelName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelName\" maxlength=\"18\" required name=\"vehicModel\" placeholder=\"Model Type\"> <input type=\"text\" ng-disabled=\"disVehic || mDataCrm.Vehicle.SourceDataId == null || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 1 && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.Model.VehicleModelName\" required name=\"vehicModel\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <!--<bsselect name=\"vehicType\" data=\"VehicT\" on-select=\"selectedType(selected)\" item-value=\"VehicleTypeId\" item-text=\"NewDescription\" ng-model=\"mDataCrm.Vehicle.Type\" placeholder=\"Vehicle Type\" ng-readonly=\"enableType\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </bsselect> --> <ui-select data=\"VehicT\" name=\"vehicType\" ng-if=\"mFilterTAM.isTAM == 1 && mDataCrm.Vehicle.SourceDataId !== null\" ng-model=\"mDataCrm.Vehicle.Type\" on-select=\"selectedType($item)\" ng-required=\"true\" ng-disabled=\"disVehic || mData.Status >= 4\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.NewDescription}} </ui-select-match> <ui-select-choices repeat=\"item in VehicT\"> <span ng-bind-html=\"item.NewDescription\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelType\" required name=\"vehicType\" placeholder=\"Model Type\"> <input type=\"text\" ng-disabled=\"disVehic || mDataCrm.Vehicle.SourceDataId == null || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 1 && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.Type.NewDescription\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicType.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-readonly=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Warna\"> --> <!-- <bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"ColorName\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </bsselect> --> <ui-select name=\"colorMobil\" ng-model=\"mDataCrm.Vehicle.Color\" data=\"VehicColor\" on-select=\"selectedColor($item.MColor)\" ng-if=\"mFilterTAM.isTAM == 1\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Warna\"> {{$select.selected.ColorName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicColor\"> <span ng-bind-html=\"item.MColor.ColorName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ColorName\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\" ng-show=\"isCustomColor\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna Lain</bsreqlabel> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable class=\"form-control\" ng-model=\"mData.VehicleColor\" placeholder=\"Warna\"> <!--<bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"VehicleColor\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </bsselect> --> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <!-- <div class=\"col-md-6\" ng-if=\"mFilterTAM.isTAM == 1\"> --> <div class=\"form-group\" show-errors> <bsreqlabel ng-if=\"mFilterTAM.isTAM == 1\">Kode Warna</bsreqlabel> <label ng-if=\"mFilterTAM.isTAM == 0\">Kode Warna</label> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCode\" placeholder=\"Kode Warna\" required> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCodeNonTAM\" placeholder=\"Kode Warna\"> <em ng-messages=\"listVehicle.colorCode.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bahan Bakar</bsreqlabel> <bsselect data=\"VehicGas\" on-select=\"selectedGas(selected)\" item-value=\"VehicleGasTypeId\" name=\"fuel\" item-text=\"VehicleGasTypeName\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required> </bsselect> <!-- <select class=\"form-control\" data=\"VehicGas\" on-select=\"selectedGas(selected)\" item-value=\"VehicleGasTypeId\" name=\"fuel\" item-text=\"VehicleGasTypeName\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </select>     --> <!-- <ui-select name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" on-select=\"selectedGas($item)\" item-text=\"VehicleGasTypeName\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\">\r" +
    "\n" +
    "                            <ui-select-match allow-clear placeholder=\"Vehicle Gas Type\">\r" +
    "\n" +
    "                                {{$select.selected.VehicleGasTypeName}}\r" +
    "\n" +
    "                            </ui-select-match>\r" +
    "\n" +
    "                            <ui-select-choices repeat=\"item in VehicGas\">\r" +
    "\n" +
    "                                <span ng-bind-html=\"item.VehicleGasTypeName\"></span>\r" +
    "\n" +
    "                            </ui-select-choices>\r" +
    "\n" +
    "                        </ui-select> --> <!-- <select class=\"form-control\" name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" on-select=\"selectedGas(selected)\" item-text=\"VehicleGasTypeName\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable>\r" +
    "\n" +
    "                            <option ng-repeat=\"item in VehicGas\" value=\"{{item.VehicleGasTypeId}}\">{{item.VehicleGasTypeName}}</option>\r" +
    "\n" +
    "                        </select> --> <em ng-messages=\"listVehicle.fuel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tahun Rakit</bsreqlabel> <input name=\"tahunRakit\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable numbers-only ng-model=\"mDataCrm.Vehicle.AssemblyYear\" placeholder=\"Tahun Rakit\" required> <em ng-messages=\"listVehicle.tahunRakit.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Full Model</bsreqlabel> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCode\" placeholder=\"Katashiki Code\" required> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" ng-disabled=\"disVehic\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCodeNonTAM\" placeholder=\"Katashiki Code\" required> <em ng-messages=\"listVehicle.fullModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Mesin</bsreqlabel> <input type=\"text\" maxlength=\"20\" name=\"NoMesin\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable ng-model=\"mDataCrm.Vehicle.EngineNo\" placeholder=\"No. Mesin\" required> <em ng-messages=\"listVehicle.NoMesin.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Asuransi</label> <input type=\"text\" name=\"Asuransi\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Insurance\" placeholder=\"Asuransi\"> <em ng-messages=\"listVehicle.Asuransi.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Polis Asuransi</label> <input type=\"text\" name=\"noSPK\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable ng-model=\"mDataCrm.Vehicle.SPK\" placeholder=\"No. Polis Asuransi\"> <em ng-messages=\"listVehicle.noSPK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal DEC</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!--<input type=\"text\" class=\"form-control\"  ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal DEC\">--> <!-- {{disVehic}} (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null) --> <bsdatepicker name=\"DateDEC\" ng-model=\"mDataCrm.Vehicle.DECDate\" ng-if=\"disVehic == false\" date-options=\"dateOptionsBirth\" max-date=\"maxDateS\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" ng-disabled=\"disVehic\" skip-enable required> </bsdatepicker> <input name=\"DateDEC\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal Lahir\" required> <!--\r" +
    "\n" +
    "                        <bsdatepicker name=\"DateDEC\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" max-date='maxDateOptionDECLC.maxDate' date-options=\"dateDECOptions\" ng-change=\"dateChange(dt)\" required>\r" +
    "\n" +
    "                        </bsdatepicker> --> <em ng-messages=\"listVehicle.DateDEC.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama STNK</bsreqlabel> <input type=\"text\" name=\"nameStnk\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKName\" placeholder=\"Nama STNK\" required> <em ng-messages=\"listVehicle.nameStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat STNK</bsreqlabel> <input name=\"addressStnk\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKAddress\" placeholder=\"Alamat STNK\" required> <em ng-messages=\"listVehicle.addressStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Masa Berlaku STNK</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!-- Enable for SALES --> <bsdatepicker name=\"dateSTNK\" ng-disabled=\"disVehic\" ng-if=\"disVehic == false\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" date-options=\"dateDECOptions\" placeholder=\"Masa Berlaku STNK\" required> </bsdatepicker> <input name=\"dateSTNK\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" placeholder=\"Masa Berlaku STNK\" required> <em ng-messages=\"listVehicle.dateSTNK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"modeV=='form' || modeV=='search'\" ng-click=\"saveVehic(mFilterVehic.actionV, mDataCrm, mFilterTAM.isTAM);\" ng-disabled=\"listVehicle.$invalid\" skip-enable class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <button type=\"button\" ng-click=\"cancelSearchV()\" ng-show=\"modeV=='form' || modeV=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> <!-- </div> --> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/customer-02.html',
    "<div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPnj()\" ng-hide=\"!disPnj\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"savePnj()\" ng-show=\"!disPnj\" class=\"rbtn btn\">Save</button> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Lengkap</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" ng-readonly=\"disPnj\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" ng-readonly=\"disPnj\" numbers-only> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/customer-03.html',
    "<div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPng(mData.US)\" ng-hide=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelPng(mData.US)\" ng-show=\"!disPng\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"savePng(mData.US, mDataCrm, CustomerVehicleId)\" ng-show=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" ng-show=\"!disPng\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-show=\"!hiddenUserList\"> <label>List Pengguna</label> <!-- <bsselect ng-model=\"mData.US\" data=\"pngList\" item-text=\"name\" item-value=\"VehicleUserId\" placeholder=\"Pilih Pengguna\" on-select=\"onChangePng(selected)\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" ng-model=\"mData.US\" data=\"pngList\" placeholder=\"Pilih Pengguna\" ng-change=\"onChangePng(mData.US)\"> <option ng-repeat=\"item in pngList\" ng-selected=\"mData.US.VehicleUserId == item.VehicleUserId\" value=\"{{item.VehicleUserId}}\">{{item.name}}</option> </select> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Lengkap</label> <input type=\"text\" name=\"nameLengkap\" class=\"form-control\" ng-model=\"mData.US.name\" ng-readonly=\"disPng\"> <input type=\"text\" class=\"form-control\" ng-model=\"mData.US.VehicleUserId\" ng-hide=\"true\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 1</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" name=\"HP1\" numbers-only ng-maxlength=\"15\" maxlength=\"15\" class=\"form-control\" ng-model=\"mData.US.phoneNumber1\" ng-readonly=\"disPng\"> <!--  numbers-only --> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" name=\"HP2\" numbers-only ng-maxlength=\"15\" maxlength=\"15\" class=\"form-control\" ng-model=\"mData.US.phoneNumber2\" ng-readonly=\"disPng\"> <!-- numbers-only --> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Hubungan dengan Pemilik</label> <!-- <bsselect data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" item-value=\"RelationId\" item-text=\"RelationDesc\"\r" +
    "\n" +
    "                    ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable>\r" +
    "\n" +
    "                </bsselect> --> <select class=\"form-control\" id=\"RelationshipID\" data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable> <option ng-repeat=\"dataRelationship in dataRelationship\" ng-selected=\"mData.US.Relationship == dataRelationship.RelationId\" value=\"{{dataRelationship.RelationId}}\">{{dataRelationship.RelationDesc}}</option> </select> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/customerIndex.html',
    "<div class=\"col-md-12\"> <div class=\"row\"> <uib-accordion close-others=\"oneAtATime\"> <form> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Pelanggan & Kendaraan </uib-accordion-heading> <div class=\"panel panel-default\"> <!-- <div class=\"panel-body\"> --> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Pelanggan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <select ng-show=\"mode == 'search' || mode == 'form'\" class=\"form-control\" ng-model=\"action\" ng-change=\"changeOwner(selected)\">\r" +
    "\n" +
    "                                            <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                                            <option value=\"2\">Input Pemilik Data Baru</option>\r" +
    "\n" +
    "                                        </select>\r" +
    "\n" +
    "                                        <select ng-show=\"mode=='view'\" class=\"form-control\" ng-model=\"action\">\r" +
    "\n" +
    "                                            <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                                            <option value=\"2\">input Pemilik Data Baru</option>\r" +
    "\n" +
    "                                        </select> --> <bsselect ng-show=\"mode == 'search' || mode == 'form'\" on-select=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> </bsselect> <bsselect ng-show=\"mode=='view'\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> </bsselect> </div> </div> <div ng-show=\"mode == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-show=\"editenable && mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-disabled=\"mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> <div class=\"row\" ng-show=\"mode == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null || mFilterCust.action == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-3\"> <label>Toyota ID</label> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" ng-model=\"filterSearch.TID\" placeholder=\"Input Toyota ID\" disallow-space> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelected}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterField\"> <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag != 6)\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" ng-disabled=\"filterSearch.choice\" disallow-space> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag == 6)\"> <bsdatepicker date-options=\"XFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null\" ng-click=\"searchCustomer(mFilterCust.action, filterSearch)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div ng-show=\"mode == 'search'\" class=\"panel-body\"> <div class=\"row\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\" ng-show=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Pelanggan</bsreqlabel> <select class=\"form-control\" ng-model=\"mDataCrm.Customer.CustomerTypeId\" ng-change=\"changeType(mDataCrm.Customer.CustomerTypeId)\"> <option value=\"3\">Individu</option> <option value=\"4\">Pemerintah</option> <option value=\"5\">Yayasan</option> <option value=\"6\">Perusahaan</option> </select> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldInst\" ng-form=\"listInst\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"Name\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\" required> <em ng-messages=\"listInst.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"NamePic\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\" required> <em ng-messages=\"listInst.NamePic.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" name=\"PICDepartment\" placeholder=\"PIC Department\" required> <em ng-messages=\"listInst.PICDepartment.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\" name=\"PICHp\" required> </div> <em ng-messages=\"listInst.PICHp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" name=\"SIUP\"> <em ng-messages=\"listInst.SIUP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\" required name=\"PICEmail\"> <em ng-messages=\"listInst.PICEmail.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" on-before-cancel=\"onListCancel\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone1\" numbers-only> </div> <div class=\"form-group\"> <label>No Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone2\" numbers-only> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" numbers-only> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" numbers-only> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <bsreqlabel>Alamat Utama</bsreqlabel>\r" +
    "\n" +
    "                                                                <bsselect name=\"contact\" ng-model=\"lmModelAddress.MainAddress\" data=\"dataMainAddress\" item-text=\"Description\" item-value=\"Id\" placeholder=\"Pilih Informasi\" skip-disable=\"true\" on-select=\"selectMainAddress(selected)\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                </bsselect>\r" +
    "\n" +
    "                                                            </div> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listInst.$invalid\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldPersonal\" ng-form=\"listPersonal\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" placeholder=\"Gelar Depan\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <!-- style=\"text-transform: uppercase;\"  --> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama\" required name=\"Name\"> <!-- style=\"text-transform: uppercase;\" --> <em ng-messages=\"listPersonal.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" placeholder=\"Gelar Belakang\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Lahir</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"BirthDate\" ng-model=\"mDataCrm.Customer.BirthDate\" date-options=\"dateOptionsBirth\" max-date=\"maxDateOption.maxDate\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" required> </bsdatepicker> <!-- <bserrmsg field=\"BirthDate\"></bserrmsg> --> <em ng-messages=\"listPersonal.BirthDate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" placeholder=\"Handphone 1\" numbers-only required name=\"HandPhone1\"> <em ng-messages=\"listPersonal.HandPhone1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" placeholder=\"Handphone 2\" numbers-only> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.KTPKITAS\" minlength=\"16\" maxlength=\"16\" placeholder=\"No. KTP/KITAS\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> <!-- required --> <em ng-messages=\"listPersonal.KTPKITAS.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No. NPWP\" name=\"Npwp\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listPersonal.Npwp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-show-detail=\"onShowDetailAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" custom-button-settings-detail=\"listCustomButtonSettingsDetailAdress\" on-after-save=\"onAfterSaveAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone1\" numbers-only> <!--  numbers-only --> </div> <div class=\"form-group\"> <label>No. Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone2\" numbers-only> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" numbers-only> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" numbers-only> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listPersonal.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listPersonal.$invalid\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> </div> </div> <!-- untuk tampilan depan --> <div class=\"panel-body\" ng-hide=\"mode =='search'\"> <div class=\"row\" ng-show=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" placeholder=\"PIC Department\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\"> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" numbers-only> </div> <div class=\"form-group\"> <label>No. Telepon 2 </label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" numbers-only> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" numbers-only> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" numbers-only> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> </div> <div class=\"row\" ng-hide=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" ng-readonly=\"true\" placeholder=\"Gelar Depan\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" ng-readonly=\"true\" placeholder=\"Nama\"> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" ng-readonly=\"true\" placeholder=\"Gelar Belakang\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" ng-readonly=\"true\" numbers-only> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" ng-readonly=\"true\" numbers-only> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address1\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\" ng-disabled=\"true\" skip-enabled> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" numbers-only maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" numbers-only maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" numbers-only> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" numbers-only> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> </div> </div> <hr width=\"95%\" style=\"height:3px;background-color:grey\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Kendaraan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <bsselect ng-show=\"modeV == 'search' || modeV == 'form'\" on-select=\"changeVehic(mFilterVehic.actionV)\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> </bsselect> <bsselect ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> </bsselect> </div> </div> <div ng-show=\"modeV == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" ng-show=\"editenable && modeV=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" ng-disabled=\"modeV=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> </div> <div class=\"row\" ng-show=\"modeV == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null || mFilterVehic.actionV == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelectedV}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterFieldV\"> <a href=\"#\" ng-click=\"filterFieldChangeV(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearchV.filterValue\" ng-keypress=\"allowPatternFilterSrc($event,mFilterVehic.actionV,filterSearchV)\" ng-disabled=\"filterSearchV.choice\" ng-maxlength=\"17\" disallow-space> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null\" ng-click=\"searchVehicle(mFilterVehic.actionV, filterSearchV)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div class=\"panel-body\" ng-form=\"listVehicle\" ng-hide=\"!hideVehic\" novalidate> <div class=\"col-md-12\" ng-show=\"!disVehic\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tipe Kendaraan</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changeTypeVehic1\" ng-change=\"checkTam(mFilterTAM.isTAM)\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"1\" checked> <label for=\"changeTypeVehic1\">TAM</label> <input ng-change=\"checkTam(mFilterTAM.isTAM)\" type=\"radio\" id=\"changeTypeVehic2\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"0\"> <label for=\"changeTypeVehic2\">Non TAM</label> </div> </div> </div> </div> <div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Polisi</bsreqlabel> <input name=\"noPolice\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.LicensePlate\" ng-maxlength=\"10\" ng-keypress=\"allowPatternFilter($event,3,filter.noPolice)\" placeholder=\"No Polisi\" required> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No Rangka</bsreqlabel> <input name=\"VIN\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.VIN\" placeholder=\"No Rangka\" required ng-maxlength=\"17\" ng-minlength=\"8\" maxlength=\"20\"> <em ng-messages=\"listVehicle.VIN.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                <bsreqlabel>Model Code</bsreqlabel>\r" +
    "\n" +
    "                                                <input type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ModelCode\" placeholder=\"Model Code\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div> --> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <!-- <bsselect data=\"VehicM\" name=\"vehicModel\" on-select=\"selectedModel(selected)\" item-value=\"VehicleModelId\" item-text=\"VehicleModelName\" ng-model=\"mDataCrm.Vehicle.Model\" placeholder=\"Vehicle Model\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select name=\"vehicModel\" ng-if=\"mFilterTAM.isTAM == 1\" ng-model=\"mDataCrm.Vehicle.Model\" data=\"VehicM\" on-select=\"selectedModel($item)\" ng-required=\"true\" search-enabled=\"false\" ng-disabled=\"disVehic\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Model\"> {{$select.selected.VehicleModelName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicM\"> <span ng-bind-html=\"item.VehicleModelName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelName\" required name=\"vehicModel\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <!--<bsselect name=\"vehicType\" data=\"VehicT\" on-select=\"selectedType(selected)\" item-value=\"VehicleTypeId\" item-text=\"NewDescription\" ng-model=\"mDataCrm.Vehicle.Type\" placeholder=\"Vehicle Type\" ng-readonly=\"enableType\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select data=\"VehicT\" name=\"vehicType\" ng-if=\"mFilterTAM.isTAM == 1\" ng-model=\"mDataCrm.Vehicle.Type\" on-select=\"selectedType($item)\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.NewDescription}} </ui-select-match> <ui-select-choices repeat=\"item in VehicT\"> <span ng-bind-html=\"item.NewDescription\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelType\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicType.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-readonly=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Warna\"> --> <!-- <bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"ColorName\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select name=\"colorMobil\" ng-model=\"mDataCrm.Vehicle.Color\" data=\"VehicColor\" on-select=\"selectedColor($item.MColor)\" ng-if=\"mFilterTAM.isTAM == 1\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Warna\"> {{$select.selected.ColorName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicColor\"> <span ng-bind-html=\"item.MColor.ColorName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ColorName\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\" ng-show=\"isCustomColor\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna Lain</bsreqlabel> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable class=\"form-control\" ng-model=\"mData.VehicleColor\" placeholder=\"Warna\"> <!--<bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"VehicleColor\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <!-- <div class=\"col-md-6\" ng-if=\"mFilterTAM.isTAM == 1\"> --> <div class=\"form-group\" show-errors> <bsreqlabel ng-if=\"mFilterTAM.isTAM == 1\">Kode Warna</bsreqlabel> <label ng-if=\"mFilterTAM.isTAM == 0\">Kode Warna</label> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCode\" placeholder=\"Kode Warna\" required> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCodeNonTAM\" placeholder=\"Kode Warna\"> <em ng-messages=\"listVehicle.colorCode.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bahan Bakar</bsreqlabel> <bsselect data=\"VehicGas\" on-select=\"selectedGas(selected)\" item-value=\"VehicleGasTypeId\" name=\"fuel\" item-text=\"VehicleGasTypeName\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required> </bsselect> <!-- <ui-select name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" on-select=\"selectedGas($item)\" item-text=\"VehicleGasTypeName\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\">\r" +
    "\n" +
    "                                                <ui-select-match allow-clear placeholder=\"Vehicle Gas Type\">\r" +
    "\n" +
    "                                                    {{$select.selected.VehicleGasTypeName}}\r" +
    "\n" +
    "                                                </ui-select-match>\r" +
    "\n" +
    "                                                <ui-select-choices repeat=\"item in VehicGas\">\r" +
    "\n" +
    "                                                    <span ng-bind-html=\"item.VehicleGasTypeName\"></span>\r" +
    "\n" +
    "                                                </ui-select-choices>\r" +
    "\n" +
    "                                            </ui-select> --> <em ng-messages=\"listVehicle.fuel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tahun Rakit</bsreqlabel> <input name=\"tahunRakit\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable numbers-only ng-model=\"mDataCrm.Vehicle.AssemblyYear\" placeholder=\"Tahun Rakit\" required> <em ng-messages=\"listVehicle.tahunRakit.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Full Model</bsreqlabel> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCode\" placeholder=\"Katashiki Code\" required> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCodeNonTAM\" placeholder=\"Katashiki Code\" required> <em ng-messages=\"listVehicle.fullModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Mesin</bsreqlabel> <input type=\"text\" name=\"NoMesin\" maxlength=\"20\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.EngineNo\" placeholder=\"No. Mesin\" required> <em ng-messages=\"listVehicle.NoMesin.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Asuransi</label> <input type=\"text\" name=\"Asuransi\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Insurance\" placeholder=\"Asuransi\"> <em ng-messages=\"listVehicle.Asuransi.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. SPK</label> <input type=\"text\" name=\"noSPK\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.SPK\" placeholder=\"No. SPK\"> <em ng-messages=\"listVehicle.noSPK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal DEC</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!--<input type=\"text\" class=\"form-control\"  ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal DEC\">--> <!-- {{disVehic}} --> <bsdatepicker name=\"DateDEC\" ng-model=\"mDataCrm.Vehicle.DECDate\" ng-if=\"disVehic == false\" date-options=\"dateOptionsBirth\" max-date=\"maxDateS\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" ng-disabled=\"disVehic\" skip-enable required> </bsdatepicker> <input name=\"DateDEC\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal Lahir\" required> <!--\r" +
    "\n" +
    "                                            <bsdatepicker name=\"DateDEC\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" max-date='maxDateOptionDECLC.maxDate' date-options=\"dateDECOptions\" ng-change=\"dateChange(dt)\" required>\r" +
    "\n" +
    "                                            </bsdatepicker> --> <em ng-messages=\"listVehicle.DateDEC.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama STNK</bsreqlabel> <input type=\"text\" name=\"nameStnk\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKName\" placeholder=\"Nama STNK\" required> <em ng-messages=\"listVehicle.nameStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat STNK</bsreqlabel> <input name=\"addressStnk\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKAddress\" placeholder=\"Alamat STNK\" required> <em ng-messages=\"listVehicle.addressStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Masa Berlaku STNK</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"dateSTNK\" ng-disabled=\"disVehic\" ng-if=\"disVehic == false\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" date-options=\"dateDECOptions\" placeholder=\"Masa Berlaku STNK\" required> </bsdatepicker> <input name=\"dateSTNK\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" placeholder=\"Masa Berlaku STNK\" required> <em ng-messages=\"listVehicle.dateSTNK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"modeV=='form' || modeV=='search'\" ng-click=\"saveVehic(mFilterVehic.actionV, mDataCrm, mFilterTAM.isTAM);\" ng-disabled=\"listVehicle.$invalid\" skip-enable class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <button type=\"button\" ng-click=\"cancelSearchV()\" ng-show=\"modeV=='form' || modeV=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> <!-- </div> --> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" ng-show=\"showInst\"> <uib-accordion-heading> Penanggung Jawab </uib-accordion-heading> <div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPnj()\" ng-hide=\"!disPnj\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"savePnj()\" ng-show=\"!disPnj\" class=\"rbtn btn\">Save</button> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Lengkap</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" ng-readonly=\"disPnj\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" ng-readonly=\"disPnj\" numbers-only> </div> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Pengguna </uib-accordion-heading> <div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPng(mData.US)\" ng-hide=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelPng(mData.US)\" ng-show=\"!disPng\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"savePng(mData.US, mDataCrm, CustomerVehicleId)\" ng-show=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" ng-show=\"!disPng\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-show=\"!hiddenUserList\"> <label>List Pengguna</label> <bsselect ng-model=\"mData.US\" data=\"pngList\" item-text=\"name\" item-value=\"VehicleUserId\" placeholder=\"Pilih Pengguna\" on-select=\"onChangePng(selected)\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Lengkap</label> <input type=\"text\" name=\"nameLengkap\" class=\"form-control\" ng-model=\"mData.US.name\" ng-readonly=\"disPng\"> <input type=\"text\" class=\"form-control\" ng-model=\"mData.US.VehicleUserId\" ng-hide=\"true\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 1</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" name=\"HP1\" numbers-only ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mData.US.phoneNumber1\" ng-readonly=\"disPng\"> <!--  numbers-only --> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" name=\"HP2\" numbers-only ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mData.US.phoneNumber2\" ng-readonly=\"disPng\"> <!-- numbers-only --> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Hubungan dengan Pemilik</label> <bsselect data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" item-value=\"RelationId\" item-text=\"RelationDesc\" ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable> </bsselect> </div> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Kontak Person & Pengambil Keputusan </uib-accordion-heading> <div class=\"col-md-12\"> <div class=\"row\"> <div smart-include=\"app/services/reception/wo/includeForm/CPPKInfo.html\"> </div> </div> </div> </div> </form> </uib-accordion> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/decisionMaker.html',
    "<div class=\"form-group\" show-errors> <bsreqlabel>Pengambil Keputusan</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"decisionMaker\" placeholder=\"\" ng-model=\"mData.decisionMaker.name\" required> <bserrmsg field=\"contactPerson\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"numbers-only\" class=\"form-control\" name=\"decisionMakerPhoneNumber1\" placeholder=\"\" ng-model=\"mData.decisionMaker.phoneNumber1\" required> <bserrmsg field=\"decisionMakerPhoneNumber1\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>No. Handphone 2</bsreqlabel> <input type=\"numbers-only\" class=\"form-control\" name=\"decisionMakerPhoneNumber2\" placeholder=\"\" ng-model=\"mData.decisionMaker.phoneNumber2\"> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/modalFormDetail.html',
    "<div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Material</bsreqlabel> <!-- <bsselect name=\"tipe\" ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" item-text=\"Name\" item-value=\"MaterialTypeId\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeParts(selected)\">\r" +
    "\n" +
    "            </bsselect> --> <select ng-disabled=\"ldModel.PartsId !== undefined && ldModel.PartsId !== null\" required ng-required=\"true\" name=\"tipe\" class=\"form-control\" convert-to-number ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" placeholder=\"Pilih Tipe\" ng-change=\"selectTypeParts(ldModel)\"> <option ng-repeat=\"mtr in materialCategory\" value=\"{{mtr.MaterialTypeId}}\">{{mtr.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"ldModel.MaterialTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.tipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"typeMaterial\"></bserrmsg> --> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label>No. Material</label> <bscheckbox ng-change=\"checkIsOPB(ldModel.IsOPB, ldModel)\" ng-disabled=\"!(ldModel.MaterialTypeId == 2) || (ldModel.PartsCode !== undefined && ldModel.PartsCode !== '' &&  ldModel.PartsCode !== null) \" class=\"pull-right\" ng-model=\"ldModel.IsOPB\" label=\"OPB\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div class=\"col-xs-8\" style=\"padding-left:0\"> <bstypeahead placeholder=\"No. Material\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" ng-change=\"ubahPartsCode(ldModel)\" get-data=\"getParts\" item-text=\"PartsCode\" ng-disabled=\"(ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null) || ldModel.IsOPB == 1\" ng-maxlength=\"250\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectParts\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\" template-url=\"customTemplate.html\" icon=\"fa-id-card-o\"> </bstypeahead> <em ng-show=\"ldModel.PartsCode.length < 4\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p>Minimal 4 karakter untuk pencarian</p> </em> <em ng-show=\"ldModel.PartsCode.length >= 4\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p>&nbsp;</p> </em> </div> <!-- <div class=\"col-md-1 col-sm-12\">\r" +
    "\n" +
    "                    <div class=\"form-group\" > --> <div class=\"col-xs-4\"> <button class=\"rbtn btn\" ng-disabled=\"!(ldModel.Qty !== null && ldModel.Qty !== undefined && partsAvailable)\" ng-click=\"checkAvailabilityParts(ldModel)\">Cek</button> <!-- </div> --> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Material</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"namaMaterial\" ng-model=\"ldModel.PartsName\" ng-maxlength=\"64\" ng-disabled=\"ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null\" ng-change=\"checkNameParts(ldModel.PartsName)\" required> <bserrmsg field=\"namaMaterial\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input step=\"1\" type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" required min=\"1\" class=\"form-control\" name=\"qty\" ng-model=\"ldModel.Qty\"> <em ng-messages=\"row1.qty.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\"> Nilai Qty Harus Satuan</p> </em> <!-- <bserrmsg field=\"qty\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-model=\"ldModel.SatuanId\" on-select=\"selectTypeUnitParts(selected)\" data=\"unitData\" item-text=\"Name\" required item-value=\"MasterId\" placeholder=\"Pilih Tipe\">\r" +
    "\n" +
    "            </bsselect> --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-model=\"ldModel.SatuanId\" ng-change=\"selectTypeUnitParts(ldModel)\" data=\"unitData\" placeholder=\"Pilih Tipe\"> <option ng-repeat=\"unt in unitData\" value=\"{{unt.MasterId}}\">{{unt.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"ldModel.SatuanId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Tipe Order</label> <!-- <bsselect name=\"satuan\" ng-model=\"ldModel.Type\" data=\"typeMData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Tipe\" ng-disabled = \"DisTipeOrder\" skip-enable>\r" +
    "\n" +
    "            </bsselect> --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-model=\"ldModel.OrderType\" data=\"typeMData\" placeholder=\"Pilih Tipe\" ng-disabled=\"DisTipeOrder\" skip-enable> <option ng-repeat=\"tdata in typeMData\" value=\"{{tdata.Id}}\">{{tdata.Name}}</option> </select> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\"> <label>ETA</label><span style=\"font-size: 10px\"> (dd/mm/yyyy)</span> <!-- <bsdatepicker name=\"DateAppoint\" ng-disabled=\"partsAvailable\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\">\r" +
    "\n" +
    "            </bsdatepicker> --> <bsdatepicker name=\"DateAppoint\" ng-disabled=\"true\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\"> </bsdatepicker> </div> <div class=\"form-group\" show-errors> <label>Ketersediaan</label> <input type=\"text\" min=\"0\" class=\"form-control\" ng-disabled=\"true\" name=\"ketersedian\" ng-model=\"ldModel.Availbility\"> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypePaidParts(selected)\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select name=\"pembayaran\" ng-init=\"ldModel.PaidById=''\" convert-to-number class=\"form-control\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" ng-change=\"selectTypePaidParts(ldModel.PaidById)\" ng-disabled=\"JobIsWarranty == 1\" placeholder=\"Pilih Pembayaran\" required> <option value=\"\"></option> <option ng-repeat=\"pyt in paymentData\" value=\"{{pyt.MasterId}}\">{{pyt.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"ldModel.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Harga</label> <!-- <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=0 class=\"form-control\" name=\"harga\" ng-maxlength=\"10\" ng-disabled=\"ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0\" ng-model=\"ldModel.RetailPrice\"> --> <input type=\"text\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"0\" class=\"form-control\" name=\"harga\" ng-if=\"ldModel.IsOPB != 1\" ng-maxlength=\"10\" ng-required=\"ldModel.IsOPB == 1\" ng-disabled=\"(ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\" currency-formating number-only> <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"1\" class=\"form-control\" name=\"hargaOPB\" ng-if=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-required=\"ldModel.IsOPB == 1\" ng-disabled=\"(ldModel.IsOPB == 1) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\"> <em ng-messages=\"row1.harga.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <em ng-messages=\"row1.hargaOPB.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Harga Tidak Boleh 0</p> </em> <!-- <bserrmsg field=\"harga\"></bserrmsg> --> </div> <!--     <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "      <label>Diskon</label>\r" +
    "\n" +
    "      <input type=\"number\" min=0 max=\"100\" class=\"form-control\" name=\"harga\" ng-model=\"ldModel.Discount\">\r" +
    "\n" +
    "      <bserrmsg field=\"harga\"></bserrmsg>\r" +
    "\n" +
    "    </div> --> <div class=\"form-group\" show-errors> <label>Sub Total</label> <!-- <input type=\"number\" ng-disabled=\"true\" min=0 class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotal\"> --> <input type=\"text\" ng-disabled=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotal\" currency-formating number-only> <!-- <input type=\"text\" ng-disabled=\"true\" min=0 class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.xsubTotal\" currency-formating number-only> --> <bserrmsg field=\"subTotal\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Dp</label> <!-- <input type=\"text\" currency-formating number-only min=0 ng-maxlength=\"10\" ng-disabled=\"disableDP || (ldModel.PaidById == 2277 || ldModel.PaidById == 30 || ldModel.PaidById == 31 || ldModel.PaidById == 2458) || ldModel.Availbility == 'Tersedia'\" skip-enable class=\"form-control\" name=\"Dp\" ng-change=\"checkDp(ldModel.DownPayment, ldModel)\" ng-model=\"ldModel.DownPayment\"> --> <input type=\"text\" currency-formating number-only min=\"0\" ng-maxlength=\"10\" ng-disabled=\" (disableDP && ldModel.PaidById != 28) || ldModel.Availbility == 'Tersedia'\" skip-enable class=\"form-control\" name=\"Dp\" ng-change=\"checkDp(ldModel.DownPayment, ldModel)\" ng-model=\"ldModel.DownPayment\"> <bserrmsg field=\"Dp\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectDiscountTypeIdParts(selected)\" placeholder=\"Pilih Tipe Diskon\" required>\r" +
    "\n" +
    "            </bsselect> --> <select name=\"diskon\" convert-to-number class=\"form-control\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonDataParts\" ng-change=\"selectTypeDiskonParts(ldModel.DiscountTypeId)\" ng-disabled=\"ldModel.PaidById == 2277 || ldModel.PaidById == 30 || ldModel.PaidById == 31 || ldModel.PaidById == 32 || JobIsWarranty == 1 || (ldModel.IsOPB != 1 && (ldModel.PartsCode == '' || ldModel.PartsCode == null || ldModel.PartsCode == undefined) )\" placeholder=\"Pilih Tipe Diskon\" required> <option value=\"\"></option> <option ng-repeat=\"disc1 in diskonDataParts\" value=\"{{disc1.MasterId}}\">{{disc1.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.diskon.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>List Diskon</bsreqlabel> <!-- <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonPart(selected,ldModel.DiscountTypeId)\" placeholder=\"Pilih List Diskon\"\r" +
    "\n" +
    "                ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\">\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number name=\"diskon\" class=\"form-control\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1 || ldModel.DiscountTypeId == 10 || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonPart(ldModel,ldModel.DiscountTypeId)\" placeholder=\"Pilih List Diskon\" ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\"> <option ng-repeat=\"disclist in diskonListDataParts\" value=\"{{disclist.Id}}\">{{disclist.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-hide=\"true\" ng-required=\"ldModel.typeDiskon !== undefined && ldModel.typeDiskon !== null && ldModel.typeDiskon > 0\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-change=\"changeDiscount(ldModel.subTotal,ldModel.Discount)\" ng-required=\"isCustomTask\" ng-disabled=\"(!isCustomPart) || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1 || ldModel.DiscountTypeId == -1\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"ldModel.Discount\"> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.PartsName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> <div style=\"margin-top:50px; float: left\"> &nbsp; </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/modalFormMaster.html',
    "<div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-xs-6\"> <div class=\"row\"></div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <bsselect name=\"tipe\" ng-model=\"lmModel.JobTypeId\" data=\"taskCategory\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"isRelease\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeWork(selected,lmModel.JobTypeId)\"> </bsselect> <!-- <select name=\"tipe\" ng-model=\"lmModel.JobTypeId\" data=\"taskCategory\" ng-disabled=\"isRelease\" placeholder=\"Pilih Tipe\"\r" +
    "\n" +
    "                    ng-change=\"selectTypeWork(lmModel,lmModel)\" class=\"form-control\">\r" +
    "\n" +
    "                    <option ng-repeat=\"task in taskCategory\" value=\"{{task.MasterId}}\">{{task.Name}}</option>\r" +
    "\n" +
    "                </select> --> <em ng-messages=\"row1.tipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pekerjaan</bsreqlabel> <bstypeahead ng-if=\"tmpIsTAM != 0 && mFilterTAM.isTAM == 1\" placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"lmModel.TaskName\" get-data=\"getWork\" ng-disabled=\"isRelease\" item-text=\"TaskName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWork\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" icon=\"fa-id-card-o\" required> </bstypeahead> <input ng-if=\"tmpIsTAM == 0 || mFilterTAM.isTAM != 1\" placeholder=\"Nama Pekerjaan\" type=\"text\" required ng-disabled=\"isRelease\" class=\"form-control\" name=\"jobName\" ng-model=\"lmModel.TaskName\"> <em ng-messages=\"row1.jobName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"jobName\"></bserrmsg> --> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label class=\"pull-left\">Additional OpeNo</label> <bscheckbox ng-change=\"getDataOpeNo(lmModel.TaskName,lmModel.isOpe,lmModel)\" class=\"pull-right\" ng-model=\"lmModel.isOpe\" label=\"Ope No\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\" show-errors> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div style=\"padding-left:0;padding-right:0;padding-bottom:15px\"> <!-- <bstypeahead placeholder=\"Task Code\" name=\"noMaterial\" ng-model=\"lmModel.TaskCode\" get-data=\"getTaskOpe\"\r" +
    "\n" +
    "                    item-text=\"TaskCode\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected=\"selected\"\r" +
    "\n" +
    "                    on-select=\"onSelectOpeNo\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\"\r" +
    "\n" +
    "                    ng-disabled=\"lmModel.isOpe == 0\" template-url=\"customTemplateOpe.html\" icon=\"fa-id-card-o\">\r" +
    "\n" +
    "                    </bstypeahead> --> <select name=\"OpeNo\" ng-change=\"selectTypeOpeNo(lmModel.TaskCode,lmModel)\" class=\"form-control\" ng-disabled=\"lmModel.isOpe == 0\" ng-model=\"lmModel.TaskCode\" data=\"DataOpeNo\" placeholder=\"Pilih Tipe\"> <option ng-repeat=\"sat in DataOpeNo\" value=\"{{sat.TaskCode}}\">{{sat.TaskName}}</option> </select> </div> <em ng-messages=\"row1.noMaterial.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"noMaterial\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div class=\"row\"> <div class=\"col-xs-6\"> <bsreqlabel>FR</bsreqlabel> <input type=\"number\" required step=\"0.0000000001\" min=\"0\" class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"FlatRateAvailable\" ng-change=\"checkServiceRate(lmModel.FlatRate,lmModel.JobTypeId)\"> <em ng-messages=\"row1.Fr.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Angka harus lebih besar atau sama dengan 0 </p> </em> </div> <!-- <div class=\"form-group\" show-errors> --> <div class=\"col-xs-6\"> <bsreqlabel>Satuan</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-disabled=\"isRelease\" ng-model=\"lmModel.ProcessId\" data=\"unitData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" required>\r" +
    "\n" +
    "                    </bsselect> --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-disabled=\"isRelease\" ng-model=\"lmModel.ProcessId\" data=\"unitData\" placeholder=\"Pilih Tipe\" required> <option ng-repeat=\"sat in unitData\" value=\"{{sat.MasterId}}\">{{sat.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"lmModel.ProcessId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> <!-- </div> --> <!-- <bserrmsg field=\"Fr\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div class=\"col-md-12\" style=\"padding-right: 0px;padding-left: 0px\"> <bsreqlabel>Harga</bsreqlabel> <div ng-if=\"PriceAvailable == false && (isRelease == false || isRelease == true)\"> <!-- <input type=\"number\" ng-maxlength=\"10\" required min=0 class=\"form-control\" name=\"price\" ng-model=\"lmModel.Fare\"\r" +
    "\n" +
    "                    ng-disabled=\"\"> --> <input type=\"text\" ng-maxlength=\"10\" required min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Fare\" ng-disabled=\"\" currency-formating number-only> </div> <div ng-if=\"PriceAvailable == true && (isRelease == false || isRelease == true)\"> <!-- <input type=\"number\" required ng-maxlength=\"10\" min=0 class=\"form-control\" name=\"price\" ng-model=\"lmModel.Summary\"\r" +
    "\n" +
    "                    ng-disabled=\"PriceAvailable\"> --> <input type=\"text\" required ng-maxlength=\"10\" min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Summary\" ng-change=\"changeHarga(lmModel, mFilterTAM.isTAM)\" ng-disabled=\"PriceAvailable && mFilterTAM.isTAM == 1\" currency-formating number-only> </div> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> <!-- <bserrmsg field=\"price\"></bserrmsg> --> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Job Plan / SAR</bsreqlabel> <input type=\"number\" ng-disabled=\"isRelease\" step=\"0.25\" min=\"0.25\" class=\"form-control\" name=\"jobplan\" ng-model=\"lmModel.ActualRate\" required ng-change=\"onSARtype(lmModel.ActualRate)\"> <em ng-messages=\"row1.jobplan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Jobplan / SAR harus kelipatan 0.25 </p> <p ng-message=\"min\">Jobplan / SAR harus lebih dari sama dengan 0.25 </p> </em> <!-- <bserrmsg field=\"jobplan\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-disabled=\"isRelease\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected,lmModel)\" required>\r" +
    "\n" +
    "            </bsselect> --> <select ng-init=\"lmModel.PaidById=''\" name=\"pembayaran\" convert-to-number ng-disabled=\"disablePembayaran || JobIsWarranty == 1\" class=\"form-control\" ng-model=\"lmModel.PaidById\" placeholder=\"Pilih Pembayaran\" ng-change=\"selectTypeCust(lmModel,lmModel.PaidById)\" ng-required=\"true\" required> <option value=\"\"></option> <option ng-repeat=\"pay in paymentData\" value=\"{{pay.MasterId}}\">{{pay.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModel.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors ng-hide=\"hideT1customer\"> <label>T1 <span ng-show=\"!disT1\" style=\"color:#b94a48\"> *</span> </label> <!-- <bsselect name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" item-text=\"Name\" item-value=\"Name\" ng-disabled=\"disT1\" placeholder=\"T1\">\r" +
    "\n" +
    "            </bsselect> --> <!-- ng-required=\"!disT1\"  ng-disabled=\"disT1\" --> <select name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" placeholder=\"T1\" class=\"form-control\" ng-disabled=\"disT1\" ng-required=\"!disT1\"> <option value=\"\"></option> <option ng-repeat=\"tf1 in TFirst\" value=\"{{tf1.T1Code}} - {{tf1.Name}}\">{{tf1.T1Code}} - {{tf1.Name}}</option> </select> <em ng-messages=\"row1.T1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\" ng-if=\"disablePembayaran\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- <bsselect name=\"diskonTipe\" ng-model=\"lmModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectDiscountTypeIdTask(selected,lmModel)\" placeholder=\"Pilih Tipe Diskon\" required>\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number name=\"diskonTipe\" ng-model=\"lmModel.DiscountTypeId\" class=\"form-control\" data=\"diskonData\" ng-change=\"selectTypeDiskonTask(lmModel.DiscountTypeId,lmModel)\" ng-disabled=\"lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.PaidById == 31 || lmModel.PaidById == 32 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" placeholder=\"Pilih Tipe Diskon\" required> <option value=\"\"></option> <option ng-repeat=\"disc in diskonData\" value=\"{{disc.MasterId}}\">{{disc.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.diskonTipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div ng-if=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2|| lmModel.DiscountTypeId == null|| lmModel.DiscountTypeId == undefined\"><bsreqlabel>List Diskon</bsreqlabel></div> <div ng-if=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10\"><label>List Diskon</label></div> <!-- <bsselect name=\"diskonList\" ng-model=\"lmModel.DiscountTypeListId\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonTask(selected,lmModel.DiscountTypeId,lmModel)\"\r" +
    "\n" +
    "                    data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonTask(selected,lmModel.DiscountTypeId,lmModel)\"\r" +
    "\n" +
    "                    placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\">\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number name=\"diskonList\" ng-model=\"lmModel.DiscountTypeListId\" class=\"form-control\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10 || lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonTask(lmModel.DiscountTypeListId,lmModel.DiscountTypeId,lmModel)\" placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\"> <option ng-repeat=\"discl in diskonListData\" value=\"{{discl.Id}}\">{{discl.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-hide=\"true\" ng-required=\"lmModel.DiscountTypeId !== undefined && lmModel.DiscountTypeId !== null && lmModel.DiscountTypeId != 0 && lmModel.DiscountTypeId != -1 && lmModel.DiscountTypeId != 10\"> <em ng-messages=\"row1.diskonList.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-required=\"isCustomTask\" ng-disabled=\"(!isCustomTask) || lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60 || lmModel.DiscountTypeId == -1\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"lmModel.Discount\"> <em ng-messages=\"row1.Discount.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpe.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.TaskName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/modalFormMasterOpl.html',
    "<div class=\"row\" ng-form=\"row1\"> <div class=\"col-md-12\" ng-if=\"(mData.Status <= 3 ||  mData.Status == undefined) && (lmModelOpl.Status <= 1 || lmModelOpl.Status == undefined)\"> <div class=\"form-group\"> <bsreqlabel>Nama OPL</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" ng-disabled=\"mData.Status > 3 && !isNew\" name=\"oplName\" ng-model=\"lmModelOpl.OPLName\" get-data=\"getOpl\" item-text=\"OPLWorkName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkOPL\" on-noresult=\"onNoResultOpl\" on-gotresult=\"onGotResult\" template-url=\"customTemplateOpl.html\" icon=\"fa-id-card-o\" required> </bstypeahead> <em ng-messages=\"row1.oplName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModelOpl.OplName\"> --> <!-- <bserrmsg field=\"oplName\"></bserrmsg> --> </div> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <!-- <bstypeahead placeholder=\"Nama Vendor\" name=\"vendorName\"\r" +
    "\n" +
    "                ng-model=\"lmModelOpl.VendorName\"\r" +
    "\n" +
    "                get-data=\"getOpl\"\r" +
    "\n" +
    "                model-key=\"Opl\"\r" +
    "\n" +
    "                ng-minlength=\"2\" ng-maxlength=\"10\" disallow-space\r" +
    "\n" +
    "                loading=\"loadingUsers\"\r" +
    "\n" +
    "                noresult=\"noResults\"\r" +
    "\n" +
    "                selected=\"selected\"\r" +
    "\n" +
    "                on-select=\"onSelectUser\"\r" +
    "\n" +
    "                on-noresult=\"onNoResult\"\r" +
    "\n" +
    "                on-gotresult=\"onGotResult\",\r" +
    "\n" +
    "                ta-minlength=\"1\"\r" +
    "\n" +
    "                icon=\"fa-id-card-o\"\r" +
    "\n" +
    "            >\r" +
    "\n" +
    "            </bstypeahead> --> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModelOpl.VendorName\" required> <!-- <bserrmsg field=\"oplName\"></bserrmsg> --> <em ng-messages=\"row1.vendorName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"OPLPaymentData\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"paymentData in OPLPaymentData\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <bsreqlabel>Estimasi Selesai</bsreqlabel> <bsdatepicker name=\"RequiredDate\" min-date=\"dateOptionsOPL.minDate\" date-options=\"dateOptionsOPL\" ng-model=\"lmModelOpl.TargetFinishDate\" ng-change=\"DateChange(dt)\" ng-required=\"true\"> </bsdatepicker> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" required name=\"ReceiveBy\" ng-model=\"lmModelOpl.ReceiveBy\">\r" +
    "\n" +
    "            <bserrmsg field=\"ReceiveBy\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input type=\"number\" min=\"0\" ng-change=\"QtyOPL(lmModelOpl.QtyPekerjaan,lmModelOpl)\" class=\"form-control\" ng-disabled=\"false\" required name=\"qty\" ng-model=\"lmModelOpl.QtyPekerjaan\"> <!-- <bserrmsg field=\"qty\"></bserrmsg> --> <em ng-messages=\"row1.qty.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Harga</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> --> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <!-- <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "                <p ng-message=\"required\">Wajib diisi</p>\r" +
    "\n" +
    "            </em>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Beli</bsreqlabel> <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"PurchasePrice\" ng-model=\"lmModelOpl.OPLPurchasePrice\"> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Jual</bsreqlabel> <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <label>Notes</label> <textarea class=\"form-control\" name=\"notes\" ng-model=\"lmModelOpl.Notes\" style=\"height: 100px\"></textarea> </div> <div class=\"form-group\" show-errors> <label>Discount (%)</label> <input type=\"number\" min=\"0\" max=\"100\" class=\"form-control\" name=\"discount\" ng-model=\"lmModelOpl.Discount\" ng-disabled=\"DiscountAvailable\"> <bserrmsg field=\"discount\"></bserrmsg> </div> </div> <div class=\"col-md-12\" ng-if=\"mData.Status > 3 && lmModelOpl.Status > 1\"> <div class=\"form-group\"> <bsreqlabel>Nama OPL</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" ng-disabled=\"true\" name=\"oplName\" ng-model=\"lmModelOpl.OPLName\" get-data=\"getOpl\" item-text=\"OPLWorkName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkOPL\" on-noresult=\"onNoResultOpl\" on-gotresult=\"onGotResult\" template-url=\"customTemplateOpl.html\" icon=\"fa-id-card-o\" required> </bstypeahead> <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModelOpl.OplName\"> --> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModelOpl.VendorName\"> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"true\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"OPLPaymentData\" ng-disabled=\"true\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"paymentData in OPLPaymentData\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Estimasi Selesai</bsreqlabel> <bsdatepicker name=\"RequiredDate\" ng-disabled=\"true\" min-date=\"dateOptionsOPL.minDate\" date-options=\"dateOptionsOPL\" ng-model=\"lmModelOpl.TargetFinishDate\" ng-change=\"DateChange(dt)\" ng-required=\"true\"> </bsdatepicker> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" required name=\"ReceiveBy\" ng-model=\"lmModelOpl.ReceiveBy\">\r" +
    "\n" +
    "            <bserrmsg field=\"ReceiveBy\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input type=\"number\" ng-change=\"QtyOPL(lmModelOpl.QtyPekerjaan,lmModelOpl)\" class=\"form-control\" ng-disabled=\"true\" required name=\"qty\" ng-model=\"lmModelOpl.QtyPekerjaan\"> <bserrmsg field=\"price\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Harga</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"number\" ng-disabled=\"true\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\">\r" +
    "\n" +
    "            <bserrmsg field=\"price\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Beli</bsreqlabel> <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"PurchasePrice\" ng-model=\"lmModelOpl.OPLPurchasePrice\"> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Jual</bsreqlabel> <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <label>Notes</label> <textarea ng-disabled=\"true\" class=\"form-control\" name=\"notes\" ng-model=\"lmModelOpl.Notes\" style=\"height: 100px\"></textarea> </div> <div class=\"form-group\" show-errors> <label>Discount (%)</label> <input type=\"number\" min=\"0\" max=\"100\" ng-disabled=\"true\" class=\"form-control\" name=\"discount\" ng-model=\"lmModelOpl.Discount\" ng-disabled=\"DiscountAvailable\"> <bserrmsg field=\"discount\"></bserrmsg> </div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpl.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.Vendor.Name | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.isExternalDesc | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wac.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-01.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Pemilik</label> <input ng-model=\"mDataCustomer.owner\" type=\"text\" skip-enable disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Pengguna</label> <input ng-model=\"mDataCustomer.user\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Alamat</label> <input ng-model=\"mDataCustomer.address\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>On Time/Late</label> <input ng-model=\"mDataCustomer.late\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Waiting Time</label> <input ng-model=\"mDataCustomer.waitTime\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Model</label> <input ng-model=\"mDataCustomer.model\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-12\"> <div class=\"well\"> <div class=\"form-group\"> <label>Job Suggest</label> <textarea class=\"form-control\" skip-enable ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\"></textarea> </div> </div> </div> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Field Action\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Operation No.</font> </td> <td> <font color=\"white\">Description</font> </td> </tr> <tr ng-repeat=\"a in GetDataTowass\"> <td>{{a.OpeNo}}</td> <td>{{a.Description}}</td> </tr> </table> </div> </uib-tab> <uib-tab index=\"1\" heading=\"PSFU\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table id=\"QA\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>No.</th> <th>Pertanyaan</th> <th>Jawaban Customer</th> <!-- <th>Alasan Customer</th> --> </tr> </thead> <tbody> <tr ng-repeat=\"item in Question track by $index\"> <td width=\"5%\">{{$index + 1}}</td> <td width=\"40%\">{{item.Description}}</td> <td width=\"25%\"> <select class=\"form-control\" name=\"yesOrNo\" ng-disabled=\"true\" skip-enable> <option ng-disabled=\"true\" skip-enable ng-repeat=\"itemAnswer in item.Answer track by $index\" value=\"{{itemAnswer.AnswerId}}\">{{itemAnswer.Description}}</option> </select> </td> </tr> </tbody> </table> </div> </uib-tab> <uib-tab index=\"2\" heading=\"CS Survey\"> </uib-tab> <uib-tab index=\"3\" heading=\"MRS\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Tanggal Reminder</font> </td> <td> <font color=\"white\">Tipe Pekerjaan</font> </td> <td> <font color=\"white\">Status</font> </td> </tr> <tr ng-repeat=\"a in GetDataMRS\"> <td>{{a.reminderDate}}</td> <td>{{a.Name}}</td> <td>{{a.StatusName}}</td> </tr> </table> </div> </uib-tab> </uib-tabset> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-02.html',
    "<div class=\"row\" id=\"coeg\"> <div class=\"col-md-12\"> <div class=\"col-md-9\"> <p id=\"demo\" hidden>aa</p> </div> <div class=\"col-md-3\"> <div class=\"form-group form-inline\"> <label>Estimasi Durasi</label> <input type=\"number\" min=\"0.1\" step=\"0.1\" class=\"form-control\" style=\"width:100px\" name=\"EstimateHour\" ng-disabled=\"true\" skip-enable ng-change=\"estimationDuration(mData.EstimateHour)\" ng-model=\"mData.EstimateHour\"> <label>Jam</label> </div> <div class=\"form-group\"> <bsdatepicker name=\"tes\" date-options=\"DateOptionstes\" ng-model=\"asb.startDate\" ng-change=\"reloadBoard(asb.startDate)\" min-date=\"minDateASB3\"></bsdatepicker> </div> </div> </div> <div class=\"col-md-12\" ontouchmove=\"myFunction(event)\" ontouchend=\"myFunctionStop()\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"plottingStallWO\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> </div> </div> </div> </div> <script>function myFunction(event) {\r" +
    "\n" +
    "        var x = event.touches[0].clientX;\r" +
    "\n" +
    "        var y = event.touches[0].clientY;\r" +
    "\n" +
    "        document.getElementById(\"demo\").innerHTML = x + \", \" + y;\r" +
    "\n" +
    "        var h = document.documentElement.scrollHeight;\r" +
    "\n" +
    "\r" +
    "\n" +
    "        //   ======\r" +
    "\n" +
    "        var tmpheight = $(\"#coeg\").height() + 100;\r" +
    "\n" +
    "        var targetScrollTop = tmpheight - $('#layoutContainer_SAMainMenuForm > ui-layout').height();\r" +
    "\n" +
    "        var targetScrollTopStart = 0;\r" +
    "\n" +
    "        var distanceToTravel = targetScrollTop - $('#layoutContainer_SAMainMenuForm > ui-layout').scrollTop();\r" +
    "\n" +
    "        var animationDuration = (distanceToTravel / 1000) * 1000;\r" +
    "\n" +
    "        h = 0.8 * h;\r" +
    "\n" +
    "        console.log(h);\r" +
    "\n" +
    "        if (y >= h) {\r" +
    "\n" +
    "            console.log('woi');\r" +
    "\n" +
    "            $(\"#layoutContainer_SAMainMenuForm > ui-layout\").animate({\r" +
    "\n" +
    "                scrollTop: targetScrollTop\r" +
    "\n" +
    "            }, 1500, 'linear');\r" +
    "\n" +
    "        } else if (y <= (0.25 * h)) {\r" +
    "\n" +
    "            $(\"#layoutContainer_SAMainMenuForm > ui-layout\").animate({\r" +
    "\n" +
    "                scrollTop: targetScrollTopStart\r" +
    "\n" +
    "            }, 1500, 'linear');\r" +
    "\n" +
    "        } else {\r" +
    "\n" +
    "            $(\"#layoutContainer_SAMainMenuForm > ui-layout\").clearQueue();\r" +
    "\n" +
    "            $(\"#layoutContainer_SAMainMenuForm > ui-layout\").stop();\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        //   if (y >= h ){\r" +
    "\n" +
    "        //   alert('bawah');\r" +
    "\n" +
    "        //   } else if ( y <= (h*0.25)){\r" +
    "\n" +
    "        //       alert('atas');\r" +
    "\n" +
    "        //   }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    function myFunctionStop() {\r" +
    "\n" +
    "        $(\"#layoutContainer_SAMainMenuForm > ui-layout\").clearQueue();\r" +
    "\n" +
    "        $(\"#layoutContainer_SAMainMenuForm > ui-layout\").stop();\r" +
    "\n" +
    "    }</script>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-03.html',
    "<div class=\"col-md-9\"> <div class=\"col-md-12\"> <label class=\"control-label\">Riwayat Servis</label> </div> <div class=\"col-md-3\" style=\"padding-left: 0px\"> <bsdatepicker name=\"Tanggal1\" date-options=\"DateOptionsHistory\" ng-model=\"MRS.startDate\"> </bsdatepicker> </div> <div class=\"col-md-1\" style=\"margin-top: 5px;padding-left: 0px; text-align: center\"> <label>S/D</label> </div> <div class=\"col-md-3\" style=\"padding-left: 0px\"> <bsdatepicker name=\"Tanggal2\" date-options=\"DateOptionsHistory\" ng-model=\"MRS.endDate\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <bsreqlabel class=\"control-label\">Kategori : </bsreqlabel> <!-- ng-change=\"change()\" --> <select ng-model=\"MRS.category\" class=\"form-control\" convert-to-number> <option value=\"2\">ALL</option> <option value=\"1\">GR</option> <option value=\"0\">BP</option> </select> </div> <div class=\"col-md-12\" style=\"margin-top: 10px;text-align: right\"> <button class=\"rbtn btn\" ng-click=\"change(MRS.startDate,MRS.endDate,MRS.category,mDataCrm.Vehicle.VIN)\">Cari</button> </div> <div class=\"col-md-12\" style=\"margin-top:20px\"> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"item in mDataVehicleHistory\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <table style=\"width:100%\"> <tr> <td>{{item.VehicleJobService[0].JobDescription}}</td> <th class=\"text-right\">{{item.ServiceNumber}} | {{item.ServiceDate |date:'dd-MMM-yyyy'}}</th> <!-- <th colspan=\"2\">{{item.ServiceNumber}} | {{item.ServiceDate |date:dd/mm/yyyy}}</th> --> </tr> <tr> <td>Lokasi services : {{item.ServiceLocation}}</td> <td class=\"text-right\">SA : {{item.ServiceAdvisor}}</td> </tr> <tr> <td>KM : {{item.Km}}</td> <!-- <td class=\"text-right\">Group : {{item.Group}}</td> --> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Teknisi : {{item.VehicleJobService[0].EmployeeName}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Group : {{item.namaGroup}}</td> <!-- <td class=\"text-right\">Tanggal Servis : {{item.ServiceDate |date:dd/mm/yyyy}}</td> --> </tr> <tr> <td>Job Suggest : {{item.JobSuggest}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Kategori : GR</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Kategori : BP</td> </tr> </table> </uib-accordion-heading> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"itemJob in item.VehicleJobService\" is-open=\"status.open\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> {{itemJob.JobDescription}} <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> </uib-accordion-heading> <table class=\"table table-striped\"> <tr> <th>No. Material</th> <th>Material</th> <!-- <th>Nama Material</th> --> <th>Qty</th> <th>Satuan</th> </tr> <tr ng-repeat=\"tr in itemJob.VehicleServiceHistoryDetail\"> <!-- <td>{{tr.PartsCode}}</td> --> <td>{{tr.MaterialCode}}</td> <td>{{tr.Material}}</td> <td>{{tr.Quantity | number:2}}</td> <td>{{tr.Satuan}}</td> </tr> </table> </div> </uib-accordion> </div> </uib-accordion> <div class=\"row\" ng-hide=\"isPSFU\"> <div class=\"col-md-12\"> <bscheckbox class=\"pull-right\" ng-model=\"mRiwayatService.isNeedPrint\" label=\"Print Riwayat Service\" ng-show=\"mDataVehicleHistory.length > 0\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-04.html',
    "<!-- Request --> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-12\"> <bslist data=\"JobRequest\" m-id=\"JobRequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" on-before-save=\"onBeforeSaveJobRequest\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Permintaan\" list-title=\"Permintaan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Permintaan\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea ng-maxtlength=\"200\" maxlength=\"200\" class=\"form-control\" placeholder=\"Permintaan\" name=\"reqdesc\" ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "                </textarea> <bserrmsg field=\"reqdesc\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <!-- Keluhan --> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-12\" style=\"text-align:right\"> <div class=\"btn-group\" style=\"margin-top:25px\"> <button type=\"button\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\"> Pre Diagnose </button> </div> </div> </div> <br> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-12\"> <bslist data=\"JobComplaint\" m-id=\"JobComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Keluhan\" list-title=\"Keluhan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Tipe Keluhan\" ng-disabled=\"true\" skip-enable> </bsselect> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Deskripsi\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div style=\"margin-bottom:90px\"> <div class=\"form-group\"> <bsreqlabel>Tipe Keluhan</bsreqlabel> <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Tipe Keluhan\" required> </bsselect> <bserrmsg field=\"ctype\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom: 100px\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\" required>\r" +
    "\n" +
    "                                            </textarea> <bserrmsg field=\"ctext\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <br> <!-- JobList --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nomor WO Estimasi</label> <input type=\"text\" min=\"0\" class=\"form-control\" ng-disabled=\"true\" skip-enable name=\"NomorWo\" ng-model=\"mData.EstimationNo\"> <bserrmsg field=\"NomorWo\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Kategori WO</bsreqlabel> <select class=\"form-control\" name=\"categoryWO\" convert-to-number ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\" ng-change=\"changeSelectedWoCategory(mData.WoCategoryId)\"> <option ng-repeat=\"cat in woCategory\" value=\"{{cat.MasterId}}\">{{cat.Name}}</option> </select> <!-- <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\">\r" +
    "\n" +
    "            </bsselect> --> <!-- <bserrmsg field=\"ctype\"></bserrmsg> --> <bserrmsg field=\"categoryWO\"></bserrmsg> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"tmpTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-edit=\"onBeforeEditJob\" on-before-save=\"onBeforeSave\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApiJob\" confirm-save=\"false\" button-settings=\"ButtonList\" list-title=\"Order Pekerjaan\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe : {{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate : {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran : {{lmItem.PaidBy}}</p> <p>Harga : {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> </div> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWork | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(totalWork - totalWorkDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterial | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ (totalMaterial - totalMaterialDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi OPL</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl - sumWorkOplDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOplDiscounted | currency:\"\":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalPPN + (sumWorkOplDiscounted*(10/100)) | currency:\"\":0}} </td></tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalEstimasi + sumWorkOplDiscounted + (sumWorkOplDiscounted*(10/100)) | currency:\"\":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDp | currency:\"\":0}}</td> </tr> </tbody> </table> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-05.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <bslist data=\"oplData\" form-name=\"oplWoGR\" m-id=\"Id\" list-model=\"lmModelOpl\" on-before-new=\"onBeforeNewOpl\" on-after-delete=\"onAfterDeleteOpl\" on-before-save=\"onBeforeSaveOpl\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <strong>{{lmItem.OPLName}}</strong> </div> <div class=\"form-group\"> {{lmItem.VendorName}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Estimasi : </label> {{lmItem.TargetFinishDate | date:'dd-MM-yyyy'}} </div> <div class=\"form-group\"> <label>Quantity : </label> {{lmItem.QtyPekerjaan}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}} </div> </div> </div> </bslist-template> <bslist-modal-form> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMasterOpl.html'\"> </div></bslist-modal-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <tbody> <tr> <td width=\"20%\">Total</td> <td>{{sumWorkOpl * (1.1) | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> </div> "
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-06.html',
    "<div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">INTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>KM</bsreqlabel> <input type=\"text\" step=\"1\" min=\"0\" class=\"form-control\" name=\"KM\" placeholder=\"Km\" ng-model=\"mData.Km\" ng-maxlength=\"7\" maxlength=\"7\" ng-keyup=\"givePattern(mData.Km,$event)\"> <bserrmsg field=\"km\"></bserrmsg> <p ng-if=\"isKmHistory >= 1 && nilaiKM < KMTerakhir\" style=\"font-style:italic;margin-top:5px; color:  rgb(185, 74, 72)\">Tidak boleh kurang dari {{KMTerakhir | number}} KM</p> </div> </div> <div id=\"slide\" class=\"col-md-6\"> <label for=\"slider\">Fuel</label> <div class=\"scaleWarp\"> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> <garis>|</garis> <garis style=\"font-size: 20px;font-weight:bold\">|</garis> <garis>|</garis> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> </div> <rzslider class=\"custom-slider\" rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.BanSerep\" label=\"Ban Serep\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.CDorKaset\" label=\"CD/Kaset\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Payung\" label=\"Payung\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Dongkrak\" label=\"Dongkrak\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.STNK\" label=\"STNK\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.P3k\" label=\"P3K\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.KunciSteer\" label=\"Kunci Steer\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ToolSet\" label=\"Tool Set\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ClipKarpet\" label=\"Clip Karpet\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.BukuService\" label=\"Buku Service\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Alarm Condition</label> <!-- <bsselect name=\"alarmCondition\" ng-model=\"mData.AlarmCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Alarm Condition\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"alarmCondition\" ng-model=\"mData.AlarmCondition\" data=\"conditionData\" placeholder=\"Pilih Alarm Condition\"> <option ng-repeat=\"conditionData in conditionData\" value=\"{{conditionData.Id}}\">{{conditionData.Name}}</option> </select> <bserrmsg field=\"alarmCondition\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>AC Condition</label> <!-- <bsselect name=\"AcCondition\" ng-model=\"mData.AcCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih AC Condition\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"AcCondition\" ng-model=\"mData.AcCondition\" data=\"conditionData\" placeholder=\"Pilih AC Condition\"> <option ng-repeat=\"conditionData in conditionData\" value=\"{{conditionData.Id}}\">{{conditionData.Name}}</option> </select> <bserrmsg field=\"AcCondition\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Power Window</label> <!-- <bsselect name=\"PowerWindow\" ng-model=\"mData.PowerWindow\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Power Window Condition\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"PowerWindow\" ng-model=\"mData.PowerWindow\" data=\"conditionData\" placeholder=\"Pilih Power Window Condition\"> <option ng-repeat=\"conditionData in conditionData\" value=\"{{conditionData.Id}}\">{{conditionData.Name}}</option> </select> <bserrmsg field=\"PowerWindow\"></bserrmsg> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Other</label> <input type=\"text\" class=\"form-control\" class=\"form-control\" placeholder=\"Other\" name=\"Other\" ng-model=\"mData.OtherDescription\"> <!-- <textarea class=\"form-control\" placeholder=\"Other\" name='Other' ng-model=\"mData.OtherDescription\">\r" +
    "\n" +
    "                                </textarea> --> <bserrmsg field=\"Other\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <label>Money Amount</label> <input type=\"text\" class=\"form-control\" class=\"form-control\" placeholder=\"Money Amount\" name=\"moneyAmount\" ng-model=\"mData.moneyAmount\" ng-keyup=\"givePatternMoney(mData.moneyAmount,$event)\"> <!-- <textarea class=\"form-control\" placeholder=\"Money Amount\" name='moneyAmount' ng-model=\"mData.moneyAmount\" ng-keyup=\"givePatternMoney(mData.moneyAmount,$event)\"> \r" +
    "\n" +
    "                            </textarea> --> <bserrmsg field=\"moneyAmount\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">EXTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <label>Tipe Kendaraan</label> --> <bsreqlabel>Tipe Kendaraan</bsreqlabel> <bsselect name=\"filterType\" ng-model=\"filter.vType\" data=\"VTypeData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe Kendaraan\" on-select=\"chooseVType(selected)\" icon=\"fa fa-car\"> </bsselect> <!-- <select class=\"form-control\" name=\"filterType\" ng-model=\"filter.vType\" data=\"VTypeData\" placeholder=\"Pilih Tipe Kendaraan\" ng-change=\"chooseVType(filter)\" icon=\"fa fa-car\">\r" +
    "\n" +
    "                        <option ng-repeat=\"VTypeData in VTypeData\" value=\"{{VTypeData.MasterId}}\">{{VTypeData.Name}}</option>\r" +
    "\n" +
    "                    </select> --> </div> </div> </div> <div class=\"row\"> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "                <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"750\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\"\r" +
    "\n" +
    "                    ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\">\r" +
    "\n" +
    "            </div> --> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"680\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"!isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"750\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> </div> <div class=\"row\" ng-show=\"!disableWACExt\"> <div class=\"col-md-12\"> <!-- <div ui-grid=\"grid\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellNav ui-grid-pinning ui-grid-auto-resize ui-grid-edit class=\"grid\" ng-style=\"getTableHeight()\">\r" +
    "\n" +
    "                    <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>Data Belum Tersedia</div>\r" +
    "\n" +
    "                    <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div>\r" +
    "\n" +
    "                </div> --> <table id=\"wacGrid\" width=\"100%\" class=\"table table-bordered table-hover table-responsive\"> <thead> <tr> <th ng-style=\"{'width':'{{col.width}}'}\" ng-hide=\"col.visible == false\" ng-repeat=\"col in grid.columnDefs\">{{col.name}}</th> </tr> </thead> <tbody> <tr style=\"height:40px\" ng-repeat=\"column in grid.data\"> <td ng-hide=\"true\" style=\"width:5%\">{{column.JobWACExtId}}</td> <td style=\"width:55%\">{{column.ItemName}}</td> <td style=\"width:45%\" style=\"padding:0px\"> <select class=\"form-control\" name=\"status\" convert-to-number ng-change=\"onDropdownChange(column)\" ng-model=\"column.Status\" placeholder=\"Pilih Kerusakan\" style=\"height:30px\"> <option ng-repeat=\"item in DamageVehicle\" value=\"{{item.Id}}\">{{item.Name}}</option> </select> </td> <td ng-hide=\"true\" style=\"width:5%\">{{column.typeName}}</td> </tr> <tr ng-if=\"!grid.data.length\"> <td colspan=\"{{grid.columnDefs.length}}\"> <div style=\"opacity: .25;font-size: 3em;width: 100%;text-align: center;z-index: 1000\">No Data Available</div> </td> </tr> </tbody> </table> </div> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"col-md-12\"> <div class=\"col-xs-2\"> <!-- <button class=\"btn rbtn\" ng-click=\"disableExt\">Clear</button> --> <!-- <button type=\"button\" class=\"btn rbtn\" ng-model=\"disableWACExt\" uib-btn-checkbox btn-checkbox-true=\"1\" btn-checkbox-false=\"0\">Clear</button> --> <!-- <button class=\"btn rbtn\" ng-disabled=\"disableBPCenter\" skip-enable ng-click=\"\">BP Center</button> --> <!-- added by sss on 2017-09-07 --> <bscheckbox ng-model=\"disableWACExt\" label=\"Clear\" true-value=\"true\" false-value=\"false\" ng-change=\"chgChk()\"> </bscheckbox> <!-- <div class=\"\" style=\"margin-top:50px;\">\r" +
    "\n" +
    "                <bscheckbox ng-model=\"mData.isBPToCenter\" ng-disabled=\"disableBPCenter\" ng-change=\"triggerBPC()\" label=\"BP Center\" true-value=\"1\" false-value=\"0\">\r" +
    "\n" +
    "                </bscheckbox>\r" +
    "\n" +
    "            </div> --> </div> <div class=\"col-xs-8\"> <div class=\"row\" style=\"height: 300px\"> <div class=\"containeramk\" ng-style=\"{'max-width': boundingBox.width + 'px', 'max-height': boundingBox.height + 'px'}\"> <signature-pad accept=\"accept\" clear=\"clear\" style=\"height:165px\" height=\"165\" width=\"500\" dataurl=\"signature.dataUrl\"></signature-pad> <div class=\"buttonsamk\"> <button class=\"btn wbtn\" ng-disabled=\"disableReset\" skip-enable ng-click=\"clear()\">Clear</button> <button class=\"btn rbtn\" ng-click=\"signature = accept(); open(signature)\">Sign</button> </div> </div> </div> </div> <div class=\"col-xs-2\"> <image-control max-upload=\"4\" is-image=\"1\" img-upload=\"uploadFiles\" multiple> <form-image-control> <input capture=\"camera\" type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"file\" ng-init=\"mData.UploadPhoto = uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"col-md-12\"> <div class=\"col-xs-2\"> <div class=\"\" style=\"margin-top:50px\"> <bscheckbox ng-model=\"mData.isBPToCenter\" ng-disabled=\"disableBPCenter\" ng-change=\"triggerBPC()\" label=\"BP Center\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"col-xs-10\"> <div ng-show=\"mData.isBPToCenter == 1\" class=\"\" style=\"margin-top:50px\"> <div class=\"form-group\"> <label>Request Appointment Date</label> <bsdatepicker name=\"bpCenterDate\" date-options=\"DateOptions\" ng-model=\"bpCenterDate.date\"> </bsdatepicker> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-xs-6\"> <select class=\"form-control\" ng-show=\"mData.isBPToCenter == 1\" ng-change=\"changeBPCenter(mData.BPCenter)\" data=\"BPCenterdata\" ng-model=\"mData.BPCenter\" placeholder=\"\"> <option ng-repeat=\"bp in BPCenterdata\" value=\"{{bp.OutletIdBPCenter}}\">{{bp.Name}}</option> </select> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-07.html',
    "<div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 10px\"> <div class=\"row\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editSuggest()\" ng-hide=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"SimpanJobSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" style=\"margin-top: 25px\"> <div class=\"col-md-12\"> <!-- <input  ng-readonly=\"disSgs\" type=\"text\" class=\"form-control\" class=\"form-control\" placeholder=\"Job Suggestion\" name='Suggestion' ng-model=\"mData.Suggestion\" >  --> <textarea ng-readonly=\"disSgs\" ng-model=\"mData.Suggestion\" maxlength=\"300\" style=\"width: 100%;height: 100px\"></textarea> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-08.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label>No. Polisi</label> </div> <div class=\"form-group\"> <label>No. WO</label> </div> <div class=\"form-group\"> <label>Kategori WO</label> </div> </div> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label style=\"text-transform: uppercase\">{{mDataCrm.Vehicle.LicensePlate}}</label> <!-- <label>{{NopoliceSummary}}</label> --> </div> <div class=\"form-group\"> <label ng-if=\"mData.WoNo != Null\">{{mData.WoNo}}</label> <label ng-if=\"mData.WoNo == Null\">-</label> </div> <div class=\"form-group\"> <!-- <bsselect ng-disabled=\"true\" data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\">\r" +
    "\n" +
    "                </bsselect> --> <select class=\"form-control\" ng-disabled=\"true\" convert-to-number data=\"woCategory\" name=\"categoryWOSUM\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\"> <option ng-repeat=\"woCategory in woCategory\" value=\"{{woCategory.MasterId}}\">{{woCategory.Name}}</option> </select> </div> </div> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWorkSummary\" m-id=\"TaskId\" d-id=\"PartsId\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetailView\" confirm-save=\"false\" list-title=\"Order Pekerjaan\" modal-title=\"Job Data\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMaster.html'\">\r" +
    "\n" +
    "                </div> --> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\">\r" +
    "\n" +
    "                </div> --> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWork | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(totalWork - totalWorkDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterial | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ (totalMaterial - totalMaterialDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi OPL</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOplDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl - sumWorkOplDiscounted | currency:\"\":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalPPN + (sumWorkOplDiscounted*(10/100)) | currency:\"\":0}} </td></tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalEstimasi + sumWorkOplDiscounted + (sumWorkOplDiscounted*(10/100)) | currency:\"\":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDp | currency:\"\":0}}</td> </tr> </tbody> </table> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Lama Pekerjaan : </label> <input type=\"text\" ng-model=\"tmpActRate\" class=\"form-control\" placeholder=\"0\" name=\"\" disabled skip-enable> Jam </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Part Bekas</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radio1Create\" ng-model=\"isParts\" name=\"radio1Create\" value=\"1\" checked> <label for=\"radio1Create\">Kembalikan</label> <input type=\"radio\" id=\"radio2Create\" ng-model=\"isParts\" name=\"radio1Create\" value=\"0\"> <label for=\"radio2Create\">Tinggal</label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Waiting / Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"wait1CreateWO\" ng-model=\"isWaiting\" name=\"radio2CreateWo\" value=\"1\" checked> <label for=\"wait1CreateWO\">Waiting</label> <input type=\"radio\" id=\"wait2CreateWo\" ng-model=\"isWaiting\" name=\"radio2CreateWo\" value=\"0\"> <label for=\"wait2CreateWo\">Drop Off</label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Car Wash</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"washing1CreateWo\" ng-model=\"isWashing\" name=\"radio3CreateWo\" value=\"1\" checked> <label for=\"washing1CreateWo\">Wash</label> <input type=\"radio\" id=\"washing2CreateWo\" ng-model=\"isWashing\" name=\"radio3CreateWo\" value=\"0\"> <label for=\"washing2CreateWo\">No Wash</label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Penggantian Parts</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changepart1CreateWo\" ng-model=\"PermissionPartChange\" name=\"radio4Createwo\" value=\"1\" checked> <label for=\"changepart1CreateWo\">Langsung</label> <input type=\"radio\" id=\"changepart2CreateWo\" ng-model=\"PermissionPartChange\" name=\"radio4Createwo\" value=\"0\"> <label for=\"changepart2CreateWo\">Izin</label> <!-- {{PermissionPartChange}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Metode Pembayaran</label> </div> </div> <div class=\"col-md-6\"> <!-- <bsselect name=\"paymentType\" ng-model=\"mData.PaymentMethod\" data=\"paymentTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Type Pembayaran\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" convert-to-number name=\"paymentType\" ng-model=\"mData.PaymentMethod\" data=\"paymentTypeData\" placeholder=\"Pilih Type Pembayaran\"> <option ng-repeat=\"paymentTypeData in paymentTypeData\" value=\"{{paymentTypeData.Id}}\">{{paymentTypeData.Name}}</option> </select> <bserrmsg field=\"paymentType\"></bserrmsg> </div> </div> <div class=\"row\" style=\"margin-top:25px; margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Scheduled Service Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"scheduled\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.firstDate\" ng-disabled=\"true\"> </bsdatepicker> </div> <div class=\"form-group text-center\" style=\"width:20px\"> <b>-</b> </div> <div class=\"form-group\"> <bsdatepicker name=\"scheduled2\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.lastDate\" ng-disabled=\"true\"> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Estimation Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"estDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.EstimateDate\" ng-disabled=\"true\" skip-enable> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Adjusment Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"adjDate\" min-date=\"minDate\" date-options=\"DateOptions\" ng-model=\"mData.AdjusmentDate\" ng-disabled=\"adjustDate\" ng-change=\"adjustTime(mData.AdjusmentDate)\" skip-enable> </bsdatepicker> </div> <div class=\"form-group\" style=\"width:200px\"> <div uib-timepicker ng-model=\"mData.AdjusmentTime\" ng-change=\"changedTime(mData.AdjusmentTime)\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" ng-disabled=\"adjustDate\"></div> </div> </div> <div class=\"col-md-3 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bscheckbox ng-model=\"mData.customerRequest\" label=\"Customer Request\" ng-click=\"adjRequest(mData.customerRequest)\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <!-- <div class=\"col-md-2 form-inline\">\r" +
    "\n" +
    "       \r" +
    "\n" +
    "    </div> --> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Kode Transaksi</label> </div> <div class=\"col-md-9 form-inline\"> <!-- <bsselect ng-disabled=\"false\" data=\"codeTransaction\" name=\"codeTrans\" item-value=\"Code\" item-text=\"Description\" ng-model=\"mData.CodeTransaction\" placeholder=\"Kode Transaksi\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" ng-disabled=\"false\" data=\"codeTransaction\" name=\"codeTrans\" ng-model=\"mData.CodeTransaction\" placeholder=\"Kode Transaksi\"> <option ng-repeat=\"codeTransaction in codeTransaction\" value=\"{{codeTransaction.Code}}\">{{codeTransaction.Description}}</option> </select> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\"> <button type=\"button\" name=\"saveWO\" class=\"rbtn btn no-animate\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" ng-click=\"saveNewWO(isParts, isWashing, isWaiting, PermissionPartChange)\"> Simpan </button> <!-- ng-disabled=\"disableRelease\" --> <button type=\"button\" name=\"releaseWO\" class=\"rbtn btn no-animate\" ng-click=\"onValidateRelease(isParts, isWashing, isWaiting, PermissionPartChange)\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" skip-enable> Release WO </button> <!-- ng-show=\"mData.JobId >= 0\" --> <button type=\"button\" name=\"CloseWO\" ng-click=\"cancelWo(mData)\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" class=\"rbtn btn no-animate\"> Batal WO </button> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/mainmenu.html',
    "<style type=\"text/css\">.radioCustomer input[type=radio] {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio]+label {\r" +
    "\n" +
    "        background-color: grey;\r" +
    "\n" +
    "        color: #fff;\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        -webkit-border-radius: 2px;\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "        font-weight: 400;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        width: 100px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio]:checked+label {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .grey {\r" +
    "\n" +
    "        color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*\r" +
    "\n" +
    "    .table-fixed{\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        background-color: #f3f3f3;\r" +
    "\n" +
    "        tbody{\r" +
    "\n" +
    "            height:200px;\r" +
    "\n" +
    "            overflow-y:auto;\r" +
    "\n" +
    "            width: 100%;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        thead,tbody,tr,td,th{\r" +
    "\n" +
    "            display:block;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        tbody{\r" +
    "\n" +
    "            td{\r" +
    "\n" +
    "                float:left;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        thead {\r" +
    "\n" +
    "            tr{\r" +
    "\n" +
    "                th{\r" +
    "\n" +
    "                    float:left;\r" +
    "\n" +
    "                    background-color: #f39c12;\r" +
    "\n" +
    "                    border-color:#e67e22;\r" +
    "\n" +
    "                }\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    */\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed {\r" +
    "\n" +
    "        width: 100%\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>thead,\r" +
    "\n" +
    "    .header-fixed>tbody,\r" +
    "\n" +
    "    .header-fixed>thead>tr,\r" +
    "\n" +
    "    .header-fixed>tbody>tr,\r" +
    "\n" +
    "    .header-fixed>thead>tr>th,\r" +
    "\n" +
    "    .header-fixed>tbody>tr>td {\r" +
    "\n" +
    "        display: block;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>tbody>tr>td {\r" +
    "\n" +
    "        border-top: 0;\r" +
    "\n" +
    "        border-bottom: 0;\r" +
    "\n" +
    "        border-left: 0;\r" +
    "\n" +
    "        border-right: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>tbody>tr:after,\r" +
    "\n" +
    "    .header-fixed>thead>tr:after {\r" +
    "\n" +
    "        content: ' ';\r" +
    "\n" +
    "        display: block;\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "        clear: both;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>tbody {\r" +
    "\n" +
    "        overflow-y: auto;\r" +
    "\n" +
    "        height: 300px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>tbody>tr>td,\r" +
    "\n" +
    "    .header-fixed>thead>tr>th {\r" +
    "\n" +
    "        width: 20%;\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .call-red {\r" +
    "\n" +
    "        color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .customCell {\r" +
    "\n" +
    "        width: 20%;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        font-size: 12pt;\r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "        color: #fff;\r" +
    "\n" +
    "        height: 61px;\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /* .fa {\r" +
    "\n" +
    "        color: black !important;\r" +
    "\n" +
    "    } */\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #example .fa,\r" +
    "\n" +
    "    #waiting .fa {\r" +
    "\n" +
    "        color: black !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (max-width: 1025px) {\r" +
    "\n" +
    "        .customCell {\r" +
    "\n" +
    "            text-align: center;\r" +
    "\n" +
    "            font-size: 10.2pt;\r" +
    "\n" +
    "            font-weight: bold;\r" +
    "\n" +
    "            height: 61px;\r" +
    "\n" +
    "            padding-left: 5px !important;\r" +
    "\n" +
    "            padding-right: 5px !important;\r" +
    "\n" +
    "            padding-top: 10px !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .panel-default>.panel-heading {\r" +
    "\n" +
    "        color: #fff !important;\r" +
    "\n" +
    "        background-color: #d53337 !important;\r" +
    "\n" +
    "        border-color: #d53337 !important;\r" +
    "\n" +
    "        font-size: 18px !important;\r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "    }</style> <bsform-base ng-form=\"SAMainMenuForm\" loading=\"loading\" get-data=\"getData\" action-button-settings=\"actionButtonSettings\" action-button-settings-detail=\"actionButtonSettingsDetail\" form-name=\"SAMainMenuForm\" form-title=\"SA Main Menu\" icon=\"fa fa-fw fa-child\" form-api=\"formApi\" linksref=\"app.preDiagnose\" linkview=\"preDiagnose@app\"> <bsform-base-main id=\"lebihAtas\"> <!-- <button ng-click=\"TestingTanggal()\">AAA</button> --> <div id=\"palingAtas\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom: 10px;padding-left: 0px\"> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <i class=\"fa fa-search\"></i> <!-- <input class=\"form-control\" type=\"text\" name=\"Filter\" ng-model=\"mData.Filter2\" placeholder=\"Filter No.Polisi\">  ng-show=\"showFilter\" ng-keyup=\"Tes($event)\" --> <input class=\"form-control\" type=\"text\" name=\"Filter\" id=\"tesFilter\" ng-model=\"mData.Filter\" placeholder=\"Filter No.Polisi\" style=\"text-transform: uppercase\"> </div> </div> </div> <div class=\"col-md-12\"> <table id=\"example\" class=\"table table-striped table-bordered header-fixed\"> <thead> <tr> <th style=\"width:20%;text-align: center;font-size: 12pt;font-weight: bold; height:61px; background-color: #d53337; color: #fff\">SA Assignment <br> ({{CountDataSa}})</th> <th class=\"customCell\">Waiting for Production <br> ({{CountWaitProd}})</th> <th style=\"width:20%;text-align: center;font-size: 12pt;font-weight: bold;height:61px; background-color: #d53337; color: #fff\">In Production <br> ({{CountProd}})</th> <th style=\"width:20%;text-align: center;font-size: 12pt;font-weight: bold;height:61px; background-color: #d53337; color: #fff\">Waiting TECO <br> ({{CountarWaitTecho}})</th> <th style=\"width:20%;text-align: center;font-size: 12pt;font-weight: bold;height:61px; background-color: #d53337; color: #fff\">Delivery <br> ({{CountDeliv}})</th> </tr> </thead> <tbody> <tr> <td> <!-- SA Assignment --> <!-- | filter : {JobId : JbId} --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.SA | filter : {PoliceNumber : mData.Filter} track by $index\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.PlanStart\" time-finish=\"item.PlanFinish\" midcolor=\"color\" place=\"\"></chip-samainmenu> </td> <td> <!-- waiting --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.wap | filter : {PoliceNumber : mData.Filter}\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.PlanStart\" time-finish=\"item.PlanFinish\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\" iswaiting=\"item.IsWaiting\" ismultijob=\"item.jmlTask\"></chip-samainmenu> </td> <td> <!-- Production --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.ip | filter : {PoliceNumber : mData.Filter}\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.PlanStart\" time-finish=\"item.PlanFinish\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\" iswaiting=\"item.IsWaiting\" ismultijob=\"item.jmlTask\"></chip-samainmenu> </td> <td> <!-- Teco --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.wt | filter : {PoliceNumber : mData.Filter}\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.PlanStart\" time-finish=\"item.PlanFinish\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\" iswaiting=\"item.IsWaiting\" ismultijob=\"item.jmlTask\"></chip-samainmenu> </td> <td> <!-- Delivery --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.deliv | filter : {PoliceNumber : mData.Filter}\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.PlanStart\" time-finish=\"item.PlanFinish\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\" iswaiting=\"item.IsWaiting\" ismultijob=\"item.jmlTask\"></chip-samainmenu> </td> </tr> </tbody> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-12\" style=\"background-color: #d53337\"> <center><label style=\"margin-top:5px; color: white;font-size: 23px; font-weight: bold\">Waiting</label></center> </div> <table id=\"waiting\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th style=\"width:33%\">Cust Approval</th> <th style=\"width:33%\">Parts</th> <th style=\"width:33%\">OPL</th> </tr> </thead> <tbody> <tr> <td> <chip-samainmenu ng-repeat=\"item in DataChipsWaiting.arr39 | filter : {PoliceNumber : mData.Filter}\" status=\"item.PauseReasonId\" policenumber=\"item.PoliceNumber\" modelname=\"item.JobTask.Job.ModelType\" time-start=\"item.JobTask.Job.PlanStart\" time-finish=\"item.JobTask.Job.PlanFinish\" employeesa=\"item.JobTask.Job.UserSa.EmployeeName\" midcolor=\"item.Color\"></chip-samainmenu> </td> <td> <chip-samainmenu ng-repeat=\"item in DataChipsWaiting.arr38 | filter : {PoliceNumber : mData.Filter}\" status=\"item.PauseReasonId\" policenumber=\"item.PoliceNumber\" modelname=\"item.JobTask.Job.ModelType\" time-start=\"item.JobTask.Job.PlanStart\" time-finish=\"item.JobTask.Job.PlanFinish\" employeesa=\"item.JobTask.Job.UserSa.EmployeeName\" midcolor=\"item.Color\"></chip-samainmenu> </td> <td> <chip-samainmenu ng-repeat=\"item in DataWaitingOPLs | filter : {PoliceNumber : mData.Filter}\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.Job.ModelType\" time-start=\"item.Job.PlanStart\" time-finish=\"item.Job.PlanFinish\" employeesa=\"item.Job.UserSa.EmployeeName\" midcolor=\"item.Color\"></chip-samainmenu> </td> </tr> </tbody> </table> </div> </div> </div> </div> </div> <bsui-modal show=\"showCallCustomer.show\" title=\"Customer Notification\" data=\"modal_modelCallCustomer\" on-save=\"saveCallCustomer\" on-cancel=\"cancelCallCustomer\" button-settings=\"buttonSettingModal3\" mode=\"modalModeCallCustomer\"> <div ng-form name=\"customerCallForm\"> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <div class=\"col-md-12\" style=\"text-align:center\"> <h2>Lakukan proses notifikasi sekarang?</h2> <h3>Silahkan tekan nomor telepon dibawah untuk menelepon</h3> <p> <h3>{{modal_modelCallCustomer.ContactPerson}}</h3> </p> <p ng-show=\"modal_modelCallCustomer.PhoneContactPerson1 !== null\"> <h3 ng-show=\"modal_modelCallCustomer.PhoneContactPerson1 !== null\">{{modal_modelCallCustomer.PhoneContactPerson1}}&nbsp;&nbsp;<a href=\"Tel:{{modal_modelCallCustomer.PhoneContactPerson1}}\"><i style=\"color:green\" class=\"fa fa-lg fa-phone\"></i></a></h3> </p> <p ng-show=\"modal_modelCallCustomer.PhoneContactPerson2 !== null\"> <h3 ng-show=\"modal_modelCallCustomer.PhoneContactPerson2 !== null\">{{modal_modelCallCustomer.PhoneContactPerson2}}&nbsp;&nbsp;<a href=\"Tel:{{modal_modelCallCustomer.PhoneContactPerson2}}\"><i style=\"color:green\" class=\"fa fa-lg fa-phone\"></i></a></h3> </p> </div> </fieldset> </div> </bsui-modal> </bsform-base-main> <bsform-base-detail> <div ng-if=\"modeQueue == 2\"> <div ng-include=\"'app/services/reception/wo/queueTaking.html'\"></div> </div> <div ng-if=\"modeQueue == 3\"> <div smart-include=\"app/services/reception/wo/wo.html\"></div> </div> <div ng-if=\"modeQueue == 4\"> <div smart-include=\"app/services/reception/wo/wo.html\"></div> </div> <!-- untuk Teco --> <div ng-if=\"modeQueue == 13\"> <div smart-include=\"app/services/reception/wo/Technicalcompletion.html\"></div> </div> <!-- untuk CallCustomer --> <!--         <div ng-if=\"modeQueue == 16\">\r" +
    "\n" +
    "            <div smart-include=\"app/services/reception/wo/callCustomer.html\"></div>\r" +
    "\n" +
    "        </div> --> <!-- Service Explanation --> <div ng-if=\"modeQueue == 16\"> <div smart-include=\"app/services/reception/wo/serviceExplanation.html\"></div> </div> <div ng-if=\"modeQueue == 20\"> <!-- <div ng-include=\"'app/services/production/repairProcessGR/repairProcessGR.html'\"></div> --> <div ng-include=\"'app/services/reception/wo/repairProcessGR.html'\"></div> <!-- <div smart-include=\"app/services/production/rapairProcessGR/repairProcessGR.html\"></div> --> </div> </bsform-base-detail> </bsform-base>"
  );


  $templateCache.put('app/services/reception/wo/queueTaking.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">th.active {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .table_font thead tr th{\r" +
    "\n" +
    "        font-size: 24px !important;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .table_font tbody tr td{\r" +
    "\n" +
    "        font-size: 24px !important; \r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .refreshIcon{\r" +
    "\n" +
    "        color:#444;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .refreshIcon:hover{\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        color:#d53337;\r" +
    "\n" +
    "    }</style> <div ng-show=\"pageMode=='view'\" class=\"row\"> <div class=\"col-md-12\" style=\"display: flex;\r" +
    "\n" +
    "        justify-content: flex-end\"> <i ng-click=\"refreshData()\" style=\"width: 40px;height: 40px;font-size: 30px;margin-top: 20px\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" class=\"fa fa-refresh refreshIcon\"></i> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">EM On Time</th> </tr> </table> </div> <div class=\"col-md-12\"> <!--         <div id=\"gridEMOnTime\" ui-grid=\"{ data: mDataEMOnTime, columnDefs: columnDefs }\" class=\"grid\"></div> --> <!-- <div ui-grid=\"gridEMOnTime\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav ui-grid-pinning\r" +
    "\n" +
    "                ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridEMOnTimeStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"emontime in gridEMOnTime.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{emontime.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{emontime.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"  ng-class=\"{ 'grey':emontime.WaitingTime > '0:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((emontime.WaitingTimeHour == 0 && emontime.WaitingTimeMinute >= 10) || (emontime.WaitingTimeHour > 0)) }\"> {{emontime.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"emontime.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(emontime)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((emontime.WaitingTimeHour == 0 && emontime.WaitingTimeMinute >= 10) || (emontime.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(emontime)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWo(emontime,1)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">EM</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div id=\"gridEM\" ui-grid=\"{ data: mDataEM, columnDefs: columnDefs }\" class=\"grid\"></div> --> <!-- <div ui-grid=\"gridEM\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav ui-grid-pinning ui-grid-cellNav\r" +
    "\n" +
    "                ui-grid-auto-resize ng-style=\"gridEMStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"EM in gridEM.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{EM.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{EM.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':EM.WaitingTime > '00:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((EM.WaitingTimeHour == 0 && EM.WaitingTimeMinute >= 10) || (EM.WaitingTimeHour > 0)) }\"> {{EM.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"EM.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(EM)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((EM.WaitingTimeHour == 0 && EM.WaitingTimeMinute >= 10) || (EM.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(EM)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWo(EM,1)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Booking On Time</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div id=\"gridBookingOnTime\" ui-grid=\"{ data: mDataBookingOnTime, columnDefs: columnDefs }\" class=\"grid\"></div> --> <!-- <div ui-grid=\"gridBookingOnTime\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav ui-grid-pinning\r" +
    "\n" +
    "                ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridBookingOnTimeStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"BookingOnTime in gridBookingOnTime.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{BookingOnTime.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{BookingOnTime.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':BookingOnTime.WaitingTime > '00:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((BookingOnTime.WaitingTimeHour == 0 && BookingOnTime.WaitingTimeMinute >= 10) || (BookingOnTime.WaitingTimeHour > 0)) }\"> {{BookingOnTime.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"BookingOnTime.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(BookingOnTime)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((BookingOnTime.WaitingTimeHour == 0 && BookingOnTime.WaitingTimeMinute >= 10) || (BookingOnTime.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(BookingOnTime)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWo(BookingOnTime,1)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Booking</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- \r" +
    "\n" +
    "            <div id=\"gridBooking\" ui-grid=\"{ data: mDataBooking, columnDefs: columnDefs }\" class=\"grid\"></div> --> <!-- <div ui-grid=\"gridBooking\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav ui-grid-pinning\r" +
    "\n" +
    "                ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridBookingStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"Booking in gridBooking.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{Booking.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{Booking.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':Booking.WaitingTime > '00:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((Booking.WaitingTimeHour == 0 && Booking.WaitingTimeMinute >= 10) || (Booking.WaitingTimeHour > 0)) }\"> {{Booking.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"Booking.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(Booking)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((Booking.WaitingTimeHour == 0 && Booking.WaitingTimeMinute >= 10) || (Booking.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(Booking)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWo(Booking,1)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Walk In</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div id=\"gridWalkIn\" ui-grid=\"{ data: mDataWalkIn, columnDefs: columnDefs }\" class=\"grid\"></div> --> <!-- <div ui-grid=\"gridWalkIn\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav ui-grid-pinning\r" +
    "\n" +
    "                ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridWalkInStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-6 col-sm-6 col-md-6\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"WalkIn in gridWalkIn.data\"> <td class=\"col-xs-6 col-sm-6 col-md-6\"> {{WalkIn.LicensePlate}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':WalkIn.WaitingTime > '00:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((WalkIn.WaitingTimeHour == 0 && WalkIn.WaitingTimeMinute >= 10) || (WalkIn.WaitingTimeHour > 0)) }\"> {{WalkIn.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"WalkIn.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(WalkIn)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((WalkIn.WaitingTimeHour == 0 && WalkIn.WaitingTimeMinute >= 10) || (WalkIn.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(WalkIn)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWo(WalkIn,2)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Other</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div id=\"gridOthers\" ui-grid=\"{ data: mDataOthers, columnDefs: columnDefs }\" class=\"grid\"></div> --> <!-- <div ui-grid=\"gridOthers\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav ui-grid-pinning\r" +
    "\n" +
    "                ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridOthersStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-6 col-sm-6 col-md-6\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"Others in gridOthers.data\"> <td class=\"col-xs-6 col-sm-6 col-md-6\"> {{Others.LicensePlate}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':Others.WaitingTime > '00:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((Others.WaitingTimeHour == 0 && Others.WaitingTimeMinute >= 10) || (Others.WaitingTimeHour > 0)) }\"> {{Others.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"Others.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(Others)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((Others.WaitingTimeHour == 0 && Others.WaitingTimeMinute >= 10) || (Others.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(Others)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWo(Others,2)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <button ng-click=\"callCustomer()\" type=\"button\" class=\"btn btn-block btn-success\"><span style=\"font-size:26px;font-weight: bold\">Next Call</span></button> </div> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "            <button ng-click=\"stop()\">stop</button>\r" +
    "\n" +
    "        </div> --> </div> <bsui-modal show=\"show_modal.show\" title=\"Edit\" data=\"modal_model\" on-save=\"editSave\" on-cancel=\"editCancel\" button-settings=\"buttonSettingMdl\" mode=\"modalMode\"> <div class=\"col-md-12\"> <label>No Polisi</label> </div> <div class=\"col-md-6\"> <input type=\"text\" name=\"LicensePlate\" ng-model=\"mData.LicensePlate\" class=\"form-control\" style=\"text-transform:uppercase\"> </div> </bsui-modal> <div ng-show=\"pageMode=='call'\" class=\"row\"> <!-- <div style=\"position:fixed;left:25%\"> --> <div class=\"page\" ng-controller=\"AppCtrl\"> <div ng-swipe-up=\"swipeup($event)\" ng-swipe-down=\"swipedown($event)\" ng-swipe-left=\"swipeleft($event)\" ng-swipe-right=\"swiperight($event)\"> <div style=\"text-align: center; font-size: 48pt; font-weight: bold\" class=\"col-md-12\"> <!--No. Polisi :-->{{dataAuto.LicensePlate}} </div> <div class=\"row\"> <div class=\"col-md-12 text-center\"> <div class=\"col-md-4 text-left\"></div> <div class=\"col-md-4 text-center\">Panggil Ulang</div> <div class=\"col-md-4 text-right\"></div> </div> </div> <div class=\"row\"> <div class=\"col-md-12 text-center\"> <div class=\"col-md-4 text-left\"></div> <!-- up --> <div class=\"col-md-4 text-center\"> <img src=\"images/swipeImage/swipe.gif\" height=\"30px\" style=\"transform:rotate(180deg)\"> </div> <div class=\"col-md-4 text-right\"></div> </div> </div> <div class=\"row\"> <div class=\"col-xs-12 col-sm-12 col-md-12 text-center\"> <!-- left --> <div class=\"col-xs-4 col-sm-4 col-md-4 text-left\">Berikutnya &nbsp; <img src=\"images/swipeImage/swipe.gif\" height=\"30px\" style=\"transform:rotate(90deg)\"> </div> <div class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <h1>{{callTimer}}</h1> </div> <!-- right --> <div class=\"col-xs-4 col-sm-4 col-md-4 text-right\"> <img src=\"images/swipeImage/swipe.gif\" height=\"30px\" style=\"transform:rotate(270deg)\"> &nbsp; Start Reception </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-12 col-sm-12 col-md-12 text-center\"> <div class=\"col-xs-4 col-sm-4 col-md-4 text-center\">&nbsp;</div> <!-- down --> <div class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <img src=\"images/swipeImage/swipe.gif\" height=\"30px\"> </div> <!-- <div class=\"col-md-4 text-center\"><button ng-click=\"callCancel()\" type=\"button\" class=\"btn btn-block btn-default\">Kembali</button></div> --> <div class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> &nbsp; </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-12 col-sm-12 col-md-12 text-center\"> <div class=\"col-xs-4 col-sm-4 col-md-4 text-center\"></div> <div class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Kembali</div> <div class=\"col-xs-4 col-sm-4 col-md-4 text-center\"></div> </div> </div> </div> </div> <br> <br> <!-- </div> --> <div class=\"col-md-12\"> <form class=\"form-horizontal well\" style=\"margin-top:20px\"> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">General Info</label> </div> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">Pemilik</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.owner\" type=\"text\" disabled class=\"form-control\"> </div> <label class=\"col-sm-2 control-label\">On Time/Late</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.late\" type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">Pengguna</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.user\" type=\"text\" disabled class=\"form-control\"> </div> <label class=\"col-sm-2 control-label\">Waiting Time</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.waitTime\" type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">Alamat</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.address\" type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">Model</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.model\" type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"form-group\" style=\"text-align: right\"> <button class=\"rbtn btn\" ng-click=\"gateOut(mData,mDataCustomer)\">Gate Out</button> </div> </form> </div> <div class=\"col-md-12\"> <div class=\"well\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"\"> <div class=\"form-group\"> <label>Job Suggest</label> <textarea class=\"form-control\" ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\"></textarea> </div> </div> <!-- <textarea ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\" style=\"height: 100px;width: 400px;\"></textarea> --> </div> </div> </div> </div> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Field Action\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">OpeNo</font> </td> <td> <font color=\"white\">Description</font> </td> </tr> <tr ng-repeat=\"a in GetDataTowass\"> <td>{{a.OpeNo}}</td> <td>{{a.Description}}</td> </tr> </table> </div> </uib-tab> <uib-tab index=\"1\" heading=\"PSFU\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table id=\"QA\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>No.</th> <th>Pertanyaan</th> <th>Jawaban Customer</th> <!-- <th>Alasan Customer</th> --> </tr> </thead> <tbody> <!-- track by $index --> <tr ng-repeat=\"item in Question track by $index\"> <td width=\"5%\">{{$index + 1}}</td> <td width=\"40%\">{{item.Description}}</td> <td width=\"25%\"> <select class=\"form-control\" name=\"yesOrNo\" ng-disabled=\"true\" skip-enable> <option ng-disabled=\"true\" skip-enable ng-repeat=\"itemAnswer in item.Answer track by $index\" value=\"{{itemAnswer.AnswerId}}\">{{itemAnswer.Description}}</option> </select> <!--<div ng-repeat=\"itemAnswer in item.Answer track by $index\">\r" +
    "\n" +
    "                                            <bsselect data=\"item.Answer\" on-select=\"selectedAnswer(selected)\" item-value=\"itemAnswer.AnswerId\" item-text=\"Description\" ng-model=\"mFService.arrQuestion.AnswerId[$index]\" placeholder=\"Vehicle Model\">\r" +
    "\n" +
    "                                            </bsselect>\r" +
    "\n" +
    "                                        </div>--> </td> <!-- <td width=\"30%\">\r" +
    "\n" +
    "                                <input style=\"width : 100%;\" type=\"text\" class=\"form-control\" name=\"Reason\" placeholder=\"Reason\" ng-model=\"item.Reason\" ng-hide=\"item.ReasonStatus\" ng-disabled=\"true\" skip-enableng-maxlength=\"30\">\r" +
    "\n" +
    "                            </td> --> </tr> </tbody> </table> <!-- <table class=\"table table-bordered\">\r" +
    "\n" +
    "                        <tr style=\"background-color: grey;\">\r" +
    "\n" +
    "                        <Td>\r" +
    "\n" +
    "                        <font color=\"white\">Pernyataan</font>\r" +
    "\n" +
    "                        </Td>\r" +
    "\n" +
    "                        <Td>\r" +
    "\n" +
    "                        <font color=\"white\">Jawaban Customer</font>\r" +
    "\n" +
    "                        </Td>\r" +
    "\n" +
    "                         <Td>\r" +
    "\n" +
    "                        <font color=\"white\">Alasan Customer</font>\r" +
    "\n" +
    "                        </Td>\r" +
    "\n" +
    "                        </tr>\r" +
    "\n" +
    "                     <tr>\r" +
    "\n" +
    "                    <Td></Td>\r" +
    "\n" +
    "                    <Td></Td>\r" +
    "\n" +
    "                    <Td></Td>\r" +
    "\n" +
    "                    </tr>\r" +
    "\n" +
    "                    </table> --> </div> </uib-tab> <!--           <uib-tab index=\"1\" heading=\"TPSS\">\r" +
    "\n" +
    "                    </uib-tab>\r" +
    "\n" +
    "                     <uib-tab index=\"2\" heading=\"Follow Up\">\r" +
    "\n" +
    "                     </uib-tab>\r" +
    "\n" +
    "                     <uib-tab index=\"3\" heading=\"PKS\">\r" +
    "\n" +
    "                        </uib-tab> --> <uib-tab index=\"2\" heading=\"CS Survey\"> </uib-tab> <uib-tab index=\"3\" heading=\"MRS\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Tanggal Reminder</font> </td> <td> <font color=\"white\">Tipe Pekerjaan</font> </td> <td> <font color=\"white\">Status</font> </td> </tr> <tr ng-repeat=\"a in GetDataMRS\"> <td>{{a.ReminderDate | date : \"dd/MM/y\"}}</td> <td>{{a.Name}}</td> <td>{{a.StatusName}}</td> </tr> </table> </div> </uib-tab> <!--           <uib-tab index=\"5\" heading=\"MRS\">\r" +
    "\n" +
    "          </uib-tab> --> </uib-tabset> <!-- Nav tabs --> <!--         <ul class=\"nav nav-tabs\" role=\"tablist\" style=\"margin-top:0px;\">\r" +
    "\n" +
    "            <li role=\"presentation\" class=\"active\"><a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Field Action</a></li>\r" +
    "\n" +
    "            <li role=\"presentation\"><a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">TPSS</a></li>\r" +
    "\n" +
    "            <li role=\"presentation\"><a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Follow Up</a></li>\r" +
    "\n" +
    "            <li role=\"presentation\"><a href=\"#settings\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">PKS</a></li>\r" +
    "\n" +
    "            <li role=\"presentation\"><a href=\"#settings\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">CS Survey</a></li>\r" +
    "\n" +
    "            <li role=\"presentation\"><a href=\"#settings\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">MRS</a></li>\r" +
    "\n" +
    "        </ul> --> <!-- Tab panes --> <!-- <div class=\"tab-content\">\r" +
    "\n" +
    "            <div role=\"tabpanel\" class=\"tab-pane active\" id=\"home\">wew</div>\r" +
    "\n" +
    "            <div role=\"tabpanel\" class=\"tab-pane\" id=\"profile\">...</div>\r" +
    "\n" +
    "            <div role=\"tabpanel\" class=\"tab-pane\" id=\"messages\">...</div>\r" +
    "\n" +
    "            <div role=\"tabpanel\" class=\"tab-pane\" id=\"settings\">...</div>\r" +
    "\n" +
    "        </div> --> </div> </div> <div ng-show=\"pageMode=='form'\" class=\"row\"> <div class=\"col-md-12\">gg</div> </div>"
  );


  $templateCache.put('app/services/reception/wo/queueTakingPickEstimatesDialog.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <p>List estimasi untuk kendaraan {{mData.PoliceNumber}}</p> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <table class=\"table table-hover\"> <tr> <th>No. Estimasi</th> <th>Tgl. Release Estimasi</th> <th>SA</th> <th>Action</th> </tr> <tr ng-if=\"EstimateAppointment.length == 0\"> <td colspan=\"4\" align=\"center\">Data Tidak Ditemukan</td> </tr> <tr ng-if=\"EstimateAppointment.length > 0\" ng-repeat=\"item in EstimateAppointment\"> <td>{{item.EstimationNo}}</td> <td>{{item.EstimationDate | date:'dd-MM-yyyy'}}</td> <td>{{item.EmployeeName}}</td> <td> <button class=\"rbtn btn\" ng-click=\"confirm(EstimateAppointment[$index])\" type=\"button\">Pilih</button></td> </tr> </table> </div> </div> <!-- <bsui-modal show=\"show_modalPKS.show\" title=\"PKS\" mode=\"modalMode\" button-settings=\"buttonEmail\"> --> <div class=\"row\"> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "        <p>PKS</p>\r" +
    "\n" +
    "        <hr>\r" +
    "\n" +
    "    </div> --> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 25px\" ng-repeat=\"a in dataPKS\"> <label>Nama Perusahaan : {{a.CompanyName}}</label><br> <!-- <label ng-show=\"a.CustomerPKSTop < 0 || a.CustomerPKSTop == null\">Balances : {{a.Balances | currency:\"Rp.\"}}</label> --> <label>Saldo : {{a.AvailableBalance | currency:\"Rp. \":0}}</label><br> <!-- <label ng-show=\"a.CustomerPKSTop > 0 || a.CustomerPKSTop !== null\">TOP : {{a.CustomerPKSTop}} Hari</label><br> --> <label ng-hide=\"{{a.CustomerPKSTop == 0}}\">TOP : {{a.CustomerPKSTop}} Hari</label><br ng-hide=\"{{a.CustomerPKSTop == 0}}\"> <label>Periode Mulai : {{a.StartPeriod | date:'dd-MM-yyyy'}}</label><br> <label>Periode Selesai : {{a.EndPeriod | date:'dd-MM-yyyy'}}</label> </div> </div> <!-- </bsui-modal> --> <div class=\"row\"> <div class=\"col-md-12 text-right\"> <button ng-click=\"ngDialog.closeAll();\" class=\"wbtn btn\" type=\"button\">Batal</button> <button ng-click=\"confirm();\" class=\"rbtn btn\" type=\"button\">Buat WO</button> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/repairProcessGR.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.ChangeColor {\r" +
    "\n" +
    "        background-color: rgb(213, 51, 55);\r" +
    "\n" +
    "        border-color: rgb(213, 51, 55);\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .DefaultColor {\r" +
    "\n" +
    "        background-color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .StatusClocOn {\r" +
    "\n" +
    "        background-color: green;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .ngdialog.ngdialog-theme-plain.custom-width .ngdialog-content {\r" +
    "\n" +
    "        width: 70%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .bs-list > .panel-body > .ui-grid-nodata{\r" +
    "\n" +
    "        position: relative !important;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <!-- <bsform-grid\r" +
    "\n" +
    "        ng-form=\"NegotiationForm\"\r" +
    "\n" +
    "        factory-name=\"Negotiation\"\r" +
    "\n" +
    "        model=\"mRole\"\r" +
    "\n" +
    "        model-id=\"Id\"\r" +
    "\n" +
    "        loading=\"loading\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        get-data=\"getData\"\r" +
    "\n" +
    "        selected-rows=\"selectedRows\"\r" +
    "\n" +
    "        on-select-rows=\"onSelectRows\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        form-name=\"RoleForm\"\r" +
    "\n" +
    "        form-title=\"Role\"\r" +
    "\n" +
    "        modal-title=\"Role\"\r" +
    "\n" +
    "        modal-size=\"small\"\r" +
    "\n" +
    "        icon=\"fa fa-fw fa-child\"\r" +
    "\n" +
    "        >\r" +
    "\n" +
    " --> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- </bsform-grid> --> <!-- IF NOT USING FORMLY --> <!-- Form Filter View WO List --> <div class=\"row\" ng-hide=\"ShowRepair\" ng-init=\"getDataDefault()\"> <div class=\"col-md-12\"> <!-- Bagian FIlter --> <div class=\"col-md-3\" style=\"margin-top: 10px\" ng-init=\"dataTeknisi()\"> <label>Nama Teknisi/Foreman</label> <bsselect name=\"EmployeeId\" ng-model=\"mData.EmployeeId\" data=\"EmployeeId\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" placeholder=\"Teknisi\" ng-keypress=\"allowPatternFilter($event,2,mData)\"> </bsselect> </div> <div class=\"col-md-3\" style=\"margin-top: 32px\"> <bsselect name=\"ChoseFilter\" ng-model=\"mFilter.ChoseFilterId\" data=\"ChoseFilter\" item-text=\"ChoseFilter\" item-value=\"ChoseFilterId\" placeholder=\"Tipe Filter\" on-select=\"selectFilter(selected)\"> </bsselect> </div> <div class=\"col-md-3\" ng-show=\"mFilter.ChoseFilterId == 1\" style=\"margin-top: 15px\"> <bslabel>No Wo</bslabel> <input class=\"form-control\" type=\"text\" name=\"KodeWO\" ng-model=\"mData.NoWo\" placeholder=\"NO WO\" ng-keypress=\"allowPatternFilter($event,2,mData)\"> </div> <div class=\"col-md-3\" ng-show=\"mFilter.ChoseFilterId == 2\" style=\"margin-top: 15px\"> <bslabel>No Polisi</bslabel> <input class=\"form-control\" type=\"text\" name=\"NomorPolisi\" ng-model=\"mData.PoliceNumber\" placeholder=\"NO Polisi\" ng-keypress=\"allowPatternFilter($event,2,mData)\"> </div> <div class=\"col-md-3\" style=\"margin-top: 10px\"> <label>Tanggal</label> <bsdatepicker name=\"tglProcessGR\" date-options=\"DateOptions\" ng-model=\"mData.FilterDate\"> </bsdatepicker> <!-- <bserrmsg field=\"tglProcessGR\"></bserrmsg> --> </div> <div class=\"col-md-3\" style=\"margin-top: 32px\"> <!-- background-color:rgb(213,51,55); --> <button ng-click=\"getData(mData.PoliceNumber,mData.NoWo,mData.EmployeeId,mData.FilterDate)\" class=\"btn wbtn\"> <i class=\"large icons\"> <i class=\"database icon\"> </i> <i class=\"inverted corner filter icon\"> </i> </i> Search <!-- <span class=\"glyphicon glyphicon-search\"></span> --> </button> </div> <!--Akhir Bagian FIlter --> <!-- Bagian Table  --> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Menunggu Pengerjaan ({{CountProdFinal}})</font> </td> <td> <font color=\"white\">Dalam Pengerjaan ({{CountInProdFinal}})</font> </td> <td> <font color=\"white\">Menunggu FI ({{CountWaitFiFinal}})</font> </td> </tr> <tr> <td width=\"33%\"> <chip-technicianmainmenu ng-repeat=\"xDataWo1 in DataWoList  | filter:search0\" policenumber=\"xDataWo1.PoliceNumber\" date=\"xDataWo1.PlanStart\" modelname=\"xDataWo1.ModelType\" planfinish=\"xDataWo1.PlanDateFinish\" timeplanfinish=\"xDataWo1.PlanFinish\" employeesa=\"xDataWo1.EmployeeSa\" jobtype=\"xDataWo1.Process.Name\" ng-click=\"getDataDetail(xDataWo1.JobId, 1)\" midcolor=\"xDataWo1.color\" ng-hide=\"xDataWo1.StallId <= 0\" status=\"xDataWo1.Status\" iswaiting=\"xDataWo1.IsWaiting\" jmltask=\"xDataWo1.jmltask\" returnjob=\"xDataWo1.Revision\" jobtwc=\"xDataWo1.adaJobTWC\" jobpwc=\"xDataWo1.adaJobPWC\" waitingpart=\"xDataWo1.adaMenungguParts\"></chip-technicianmainmenu> </td> <td width=\"33%\"> <chip-technicianmainmenu ng-repeat=\"xDataWo1 in DataWoList  | filter:search\" policenumber=\"xDataWo1.PoliceNumber\" date=\"xDataWo1.PlanStart\" modelname=\"xDataWo1.ModelType\" planfinish=\"xDataWo1.PlanDateFinish\" timeplanfinish=\"xDataWo1.PlanFinish\" employeesa=\"xDataWo1.EmployeeSa\" jobtype=\"xDataWo1.Process.Name\" ng-click=\"getDataDetail(xDataWo1.JobId, 1)\" midcolor=\"xDataWo1.color\" ng-hide=\"xDataWo1.StallId <= 0\" status=\"xDataWo1.Status\" iswaiting=\"xDataWo1.IsWaiting\" jmltask=\"xDataWo1.jmltask\" returnjob=\"xDataWo1.Revision\" jobtwc=\"xDataWo1.adaJobTWC\" jobpwc=\"xDataWo1.adaJobPWC\" waitingpart=\"xDataWo1.adaMenungguParts\"></chip-technicianmainmenu> </td> <td width=\"33%\"> <chip-technicianmainmenu ng-repeat=\"xDataWo1 in DataWoList  | filter:search2\" policenumber=\"xDataWo1.PoliceNumber\" date=\"xDataWo1.PlanStart\" modelname=\"xDataWo1.ModelType\" planfinish=\"xDataWo1.PlanDateFinish\" timeplanfinish=\"xDataWo1.PlanFinish\" employeesa=\"xDataWo1.EmployeeSa\" jobtype=\"xDataWo1.Process.Name\" ng-click=\"getDataDetail(xDataWo1.JobId, 1)\" midcolor=\"xDataWo1.color\" ng-hide=\"xDataWo1.StallId <= 0\" status=\"xDataWo1.Status\" iswaiting=\"xDataWo1.IsWaiting\" jmltask=\"xDataWo1.jmltask\" returnjob=\"xDataWo1.Revision\" jobtwc=\"xDataWo1.adaJobTWC\" jobpwc=\"xDataWo1.adaJobPWC\" waitingpart=\"xDataWo1.adaMenungguParts\"></chip-technicianmainmenu> </td> </tr> </table> </div> <!-- Akhir Bagian Table  --> <!-- Bagian Keterangan Warna --> <div class=\"col-md-12\"> <div class=\"col-xs-12\"> <div class=\"col-md-3 col-xs-3\" style=\"margin-top: 5px\"> <div class=\"col-md-6 col-xs-6\" style=\"background-color: green;height: 20px;width: 40px\"></div> <div class=\"col-md-6 col-xs-6\"> Walk In </div> </div> <div class=\"col-md-3 col-xs-3\" style=\"margin-top: 5px\"> <div class=\"col-md-6 col-xs-6\" style=\"background-color: red;height: 20px;width: 40px\"></div> <div class=\"col-md-6 col-xs-6\"> Problem / Pause </div> </div> <div class=\"col-md-3 col-xs-3\" style=\"margin-top: 5px\"> <div class=\"col-md-6 col-xs-6\" style=\"background-color: yellow;height: 20px;width: 40px\"></div> <div class=\"col-md-6 col-xs-6\"> In Progress </div> </div> <div class=\"col-md-3 col-xs-3\" style=\"margin-top: 5px\"> <div class=\"col-md-6 col-xs-6\" style=\"background-color: blue;height: 20px;width: 40px\"></div> <div class=\"col-md-6 col-xs-6\"> Booking / Appoinment </div> </div> </div> </div> <!-- Akhir Bagian Keterangan Warna --> </div> </div> <!-- Akhir Form Filter View WO List --> <div class=\"row\"> <div class=\"col-xs-12\" style=\"text-align: right;margin-bottom: 20px\"> <button class=\"wbtn btn\" ng-click=\"Kembali()\" ng-show=\"ShowBtnKembali\">Kembali</button> </div> </div> <div class=\"row\" ng-show=\"ShowRepair\"> <div class=\"col-md-12 col-xs-12\"> <div class=\"col-md-12 col-xs-12\" style=\"padding-left: 0px;padding-right: 0px\"> <div class=\"col-md-7\"> <div class=\"col-md-12\" style=\"border:1px solid #ccc; background-color: #fff\"> <div class=\"col-md-12\"> <div class=\"col-md-12\" style=\"margin-top: 10px\"> <label>Nama Teknisi :</label><label ng-repeat=\"a in JobTaskTeknisi\">{{a.EmployeeName}} </label> </div> </div> <div class=\"col-md-12\"> <div ng-include=\"'app/services/production/repairProcessGR/BoardRepairProcessGR.html'\" style=\"margin-top: 35px\"> </div> </div> <!-- <h1>Info Terkait Data Stall</h1> --> </div> </div> <div class=\"col-md-5\"> <div class=\"col-xs-12\" style=\"border:1px solid #ccc;height: 290px\" ng-repeat=\"z in xDataWoList | filter : {JobId : JbId}\"> <div class=\"col-xs-12\" style=\"margin-top: 65px\"> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Full Model</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.KatashikiCode}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">W/O No.</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.WoNo}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">SA</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.UserSa.EmployeeName}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Nomor Polisi</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.PoliceNumber}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Model</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.ModelType}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Mulai</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.PlanDateStart | date : 'dd/MM/yyyy' }} {{z.PlanStart}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Selesai</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.PlanDateFinish | date : 'dd/MM/yyyy' }} {{z.PlanFinish}}</div> </div> <div class=\"col-xs-12\" ng-show=\"z.FixedDeliveryTime2 == null\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Janji Penyerahan</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\"> {{z.FixedDeliveryTime1 | date : 'dd/MM/yyyy'}} {{JanjiPenyerahan}}</div> </div> <div class=\"col-xs-12\" ng-show=\"z.FixedDeliveryTime2 !== null\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Janji Penyerahan</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.FixedDeliveryTime2 | date : 'dd/MM/yyyy'}} {{JanjiPenyerahan}}</div> </div> </div> </div> </div> </div> <!-- Permintaan dan keluhan pelanggan --> <!--             <div class=\"col-md-12 col-xs-12\" style=\"padding-left: 0px;margin-top: 15px;\">\r" +
    "\n" +
    "              \r" +
    "\n" +
    "                 <uib-accordion close-others=\"false\">\r" +
    "\n" +
    "                   <div uib-accordion-group class=\"panel-default\" is-open=\"status.open\">\r" +
    "\n" +
    "                     <uib-accordion-heading>\r" +
    "\n" +
    "                       <label>Permintaan dan Keluhan Pelanggan</label>\r" +
    "\n" +
    "                       <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i>\r" +
    "\n" +
    "                     </uib-accordion-heading>\r" +
    "\n" +
    "                     \r" +
    "\n" +
    "                     <div class=\"col-md-12\">\r" +
    "\n" +
    "                      <div class=\"col-md-6\">\r" +
    "\n" +
    "                        Keluhan\r" +
    "\n" +
    "                        <p  ng-repeat = \"a in xDataWoList\">\r" +
    "\n" +
    "                          {{$index + 1 }}. {{a.JobComplaint[$index].ComplaintDesc}}\r" +
    "\n" +
    "                        </p>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                      <div class=\"col-md-6\">\r" +
    "\n" +
    "                        Permintaan\r" +
    "\n" +
    "                        <p  ng-repeat = \"a in xDataWoList\">\r" +
    "\n" +
    "                          {{$index + 1}}. {{a.JobRequest[$index].RequestDesc}}\r" +
    "\n" +
    "                        </p>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                     </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                     <div class=\"col-md-12\" style=\"margin-top: 10px;\">\r" +
    "\n" +
    "                       <div class=\"col-md-12\">\r" +
    "\n" +
    "                          <textarea style=\"width: 100%; height: 150px;\" placeholder=\"Action Yang Dilakukan Teknisi\" ng-model=\"mData.TechnicianNotes\"></textarea>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                          <button class=\"rbtn btn\" ng-click=\"SimpanNote();\">Simpan</button>\r" +
    "\n" +
    "                       </div>\r" +
    "\n" +
    "                     </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                   </div>\r" +
    "\n" +
    "                 </uib-accordion>\r" +
    "\n" +
    "            </div> --> <!-- Permintaan dan keluhan pelanggan --> <div class=\"col-md-12 col-xs-12\" ng-show=\"ShowRepair\" ng-repeat=\"z in xDataWoList | filter : {JobId : JbId}\"> <uib-tabset active=\"activeJustified\" justified=\"true\" style=\"margin-top: 40px\"> <!-- <bstabwiz fulwidth=\"true\"> --> <uib-tab index=\"0\" heading=\"Job Instruction\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036 || user.RoleId == 1033\"> <!-- <bstabwiz-pane title=\"Job Instruction\"> --> <div ng-show=\"user.RoleId == 1 || user.RoleId == 1036 || user.RoleId == 1033\" ng-include=\"'app/services/production/repairProcessGR/repairProcessGR_JobInstruction.html'\"> </div> <!-- </bstabwiz-pane> --> </uib-tab> <uib-tab index=\"1\" heading=\"Final Inspection\" ng-show=\"showFi\" ng-click=\"getFIById(z.JobId,z.JobTask)\"> <!-- <bstabwiz-pane title=\"Final Inspection\"> --> <div ng-include=\"'app/services/production/repairProcessGR/repairProcessGR_FinalInspection.html'\" ng-show=\"showFi2\"> </div> <!-- </bstabwiz-pane> --> </uib-tab> <!-- \r" +
    "\n" +
    "                 <uib-tab index=\"2\" heading=\"Job Suggest\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036 || user.RoleId == 1033\">\r" +
    "\n" +
    "                    <div ng-include=\"'app/services/production/repairProcessGR/repairProcessGR_JobSuggest.html'\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036 || user.RoleId == 1033\"></div>\r" +
    "\n" +
    "                 </uib-tab> --> <uib-tab index=\"3\" heading=\"Wo Detail\" ng-click=\"WoDetail(z.JobId)\"> <div class=\"col-md-12\" ng-repeat=\"fDwo in DataWOTask\" style=\"margin-top: 10px;padding-right: 0px;padding-left: 0px\" ng-disabled=\"true\"> <uib-accordion close-others=\"oneAtATime\" ng-disabled=\"true\"> <div uib-accordion-group is-open=\"true\" class=\"panel-default\"> <uib-accordion-heading> <label>Nama Pekerjaan : {{fDwo.TaskName}}</label> <br> <label>Tipe : {{fDwo.JobType.Name}} </label> <!-- <label>Tipe : {{fDwo.JobTypeId}} </label> --> <br> <label>Flat Rate : {{fDwo.FlatRate}}</label> <br> <label>Act Rate : {{fDwo.ActualRate}}</label> <!-- <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> --> </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <table class=\"table table-striped table-bordered\"> <tr> <td>Nomor Material</td> <td>Nama Material</td> <td>Status</td> <td>ETA</td> <td>Permintaan Qty</td> <td>Issue Qty</td> <td>Satuan</td> </tr> <tr ng-repeat=\"fDParts in fDwo.JobParts\"> <td>{{fDParts.Part.PartsCode}}</td> <td>{{fDParts.PartsName}}</td> <!-- <td>{{fDParts.Status}}</td> --> <!-- <td ng-if=\"fDParts.Qty == fDParts.QtyGI\">Tersedia</td>\r" +
    "\n" +
    "                                                <td ng-if=\"fDParts.Qty != fDParts.QtyGI\">Tidak Tersedia</td> --> <td>{{fDParts.Availability}}</td> <td>{{fDParts.ETA | date:\"dd-MM-yyyy\"}}</td> <td>{{fDParts.Qty}}</td> <td>{{fDParts.QtyGI}}</td> <td>{{fDParts.Satuan.Name}}</td> </tr> </table> <!-- <div ui-grid=\"gridWODetail\" ui-grid-cellNav ui-grid-auto-resize class=\"grid\" ng-cloak style=\"height: 200px;\"></div> --> </div> </div> </div> </uib-accordion> </div> <div class=\"col-md-12\"> <button class=\"btn btn-danger\" style=\"background-color: rgb(213,51,55)\" ng-click=\"getDataPrediagnose()\">Diagnoses Detail</button> <button class=\"btn btn-danger\" style=\"background-color: rgb(213,51,55)\" ng-disabled=\"showBtnTambah\" ng-click=\"addPekerjaan()\">Tambah Pekerjaan</button> <button class=\"btn btn-danger\" style=\"background-color: rgb(213,51,55)\" ng-disabled=\"showBtnTambah\" ng-click=\"addMaterial()\">Tambah Parts</button> </div> </uib-tab> <!-- \r" +
    "\n" +
    "                 ini tadinya riwayat service yg ternyata harus dipindah\r" +
    "\n" +
    "                  <uib-tab index=\"3\" heading=\"Riwayat Service\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036\">\r" +
    "\n" +
    "                    <div class=\"col-md-12\" style=\"margin-top: 10px;\" ng-repeat = \"b in VinData\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036\">\r" +
    "\n" +
    "                      <vehicle-history vin=\"b.VIN\"></vehicle-history>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </uib-tab> --> <uib-tab index=\"4\" heading=\"WAC\" ng-click=\"chooseVType()\"> <!-- <bstabwiz-pane title=\"Riwayat Service\"> --> <div class=\"col-md-12\" id=\"dataWAC\" style=\"margin-top: 10px\"> <div ng-include=\"'app/services/production/repairProcessGR/repairProcessGR_WAC.html'\"></div> <!-- <vehicle-history vin=\"b.VIN\"></vehicle-history> --> </div> <!-- </bstabwiz-pane> --> </uib-tab> <!-- </bstabwiz> --> </uib-tabset> </div> </div> </div> <bsui-modal show=\"show_modal.show\" title=\"Tambah Material\" data=\"modal_model\" on-save=\"doAddMaterial\" on-cancel=\"rescheduleCancel\" mode=\"modalMode\"> <div class=\"col-md-12\"> <label>Nama Material</label> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <textarea style=\"width:100%\" ng-model=\"modal_model.Message\"></textarea> </div> </bsui-modal> <bsui-modal show=\"show_modalTask.show\" title=\"Tambah Pekerjaan\" data=\"modal_modelTask\" on-save=\"doAddPekerjaan\" on-cancel=\"rescheduleCancel\" mode=\"modalModeTask\"> <div class=\"col-md-12\"> <label>Nama Pekerjaan</label> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <textarea style=\"width:100%\" ng-model=\"modal_modelTask.Message\"></textarea> </div> </bsui-modal>"
  );


  $templateCache.put('app/services/reception/wo/serviceExplanation.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nomor WO :</label> <label>{{mData.WoNo}}</label> </div> <div class=\"form-group\"> <label>Nomor Polisi :</label> <label>{{mData.PoliceNumber}}</label> </div> </div> </div> </div> <div class=\"col-md-12\"> <uib-accordion close-others=\"oneAtATime\"> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Hasil Diagnosa </div> </uib-accordion-heading> <label>Hasil Diagnosa</label> <fieldset style=\"padding:10px;border:2px solid #e2e2e2; background-color: #e9e9e9\"> <!-- <legend style=\"margin-left:10px;width:inherit;border-bottom:0px;\">\r" +
    "\n" +
    "                            Hasil Diagnosa\r" +
    "\n" +
    "                        </legend> --> <label ng-repeat=\"x in dataPrediagnose\">{{x.PreDiagnoseTxt}}</label> <!-- <textarea class=\"form-control\"></textarea> --> </fieldset> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Nama Petugas Diagnosa</label> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mData.UserSa.EmployeeName\" placeholder=\"Nama Petugas\" name=\"namePetugas\" ng-disabled=\"true\" skip-enable> --> <input type=\"text\" class=\"form-control\" ng-model=\"mDataPrediagnose.PrediagnoseUser.EmployeeName\" placeholder=\"Nama Petugas\" name=\"namePetugas\" ng-disabled=\"true\" skip-enable> </div> </div> </div> <div id=\"hasilDiagnosa\" class=\"row\"> <div class=\"col-xs-6\"> <div class=\"form-group\"> <div class=\"col-xs-12\"> <label>Tanggal Awal</label> </div> <!-- <bsdatepicker\r" +
    "\n" +
    "                                name=\"DateAppoint\"\r" +
    "\n" +
    "                                date-options=\"DateOptions\"\r" +
    "\n" +
    "                                ng-model=\"mData.firstDate\"\r" +
    "\n" +
    "                                 ng-disabled=\"false\"\r" +
    "\n" +
    "                                 skip-enable\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  >\r" +
    "\n" +
    "                  </bsdatepicker> --> <div class=\"col-xs-6\"> <bsdatepicker disable=\"disabled\" name=\"DateAppoint\" ng-disabled=\"true\" min-date=\"minDateASB\" date-options=\"DateOptions\" ng-model=\"mDataPrediagnose.ScheduledTime\" ng-change=\"adjustTime(mDataPrediagnose.ScheduledTime)\" skip-enable> </bsdatepicker> </div> <div class=\"col-xs-6\"> <div uib-timepicker disable=\"disabled\" ng-disabled=\"true\" ng-model=\"mDataPrediagnose.ScheduledTime\" ng-change=\"changedTime(mDataPrediagnose.ScheduledTime)\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" skip-enable></div> </div> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\"> <div class=\"col-xs-12\"> <label>Tanggal Akhir</label> </div> <!-- <bsdatepicker\r" +
    "\n" +
    "                                name=\"DateAppoint\"\r" +
    "\n" +
    "                                date-options=\"DateOptions\"\r" +
    "\n" +
    "                                ng-model=\"mData.lastDate\"\r" +
    "\n" +
    "                                 ng-disabled=\"false\"\r" +
    "\n" +
    "                                 skip-enable\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  >\r" +
    "\n" +
    "                  </bsdatepicker> --> <div class=\"col-xs-6\"> <bsdatepicker name=\"DateAppoint\" ng-disabled=\"true\" min-date=\"minDateASB\" date-options=\"DateOptions\" ng-model=\"mDataPrediagnose.lastDatePrediagnose\" ng-change=\"adjustTime(mDataPrediagnose.lastDate)\" skip-enable disable=\"disabled\"> </bsdatepicker> </div> <div class=\"col-xs-6\"> <div uib-timepicker ng-disabled=\"true\" ng-model=\"mDataPrediagnose.lastDatePrediagnose\" ng-change=\"changedTime(mDataPrediagnose.lastDateTime)\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" disable=\"disabled\" show-spinners=\"false\" skip-enable></div> </div> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Hasil Pekerjaan </div> </uib-accordion-heading> <div class=\"col-md-12\"> <!-- bs list di bawah  di komen karena katanya hasil pekerjaan mau ngambil dr \"action ayng dilakukan teknisi\" dr repair proses --> <!-- <bslist data=\"gridWork\" m-id=\"tmpTaskId\" d-id=\"PartId\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" button-settings=\"listView\" selected-rows=\"listJoblistSelectedRows\" list-api='listApi'\r" +
    "\n" +
    "                            confirm-save=\"false\" list-title=\"Job List\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\">\r" +
    "\n" +
    "                            <bslist-template>\r" +
    "\n" +
    "                                <div class=\"col-md-6\" style=\"text-align:left;\">\r" +
    "\n" +
    "                                    <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p>\r" +
    "\n" +
    "                                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                                        <p>Tipe :{{lmItem.catName}}</p>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                                        <p>Flat Rate: {{lmItem.FlatRate}}</p>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-6\" style=\"text-align:right;\">\r" +
    "\n" +
    "                                    <p>Pembayaran: {{lmItem.PaidBy}}</p>\r" +
    "\n" +
    "                                    <p>Harga: {{lmItem.Summary | currency:\"Rp. \":0}}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </bslist-template>\r" +
    "\n" +
    "                            <bslist-detail-template>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <p><strong>{{ ldItem.PartsCode }}</strong></p>\r" +
    "\n" +
    "                                    <p>{{ ldItem.PartsName }}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <p>{{ ldItem.Availbility }}\r" +
    "\n" +
    "                                    </p> --> <!-- <p>Pembayaran : {{ ldItem.paidName}}</p> --> <!-- <p>Pembayaran : {{ ldItem.PaidBy}}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}\r" +
    "\n" +
    "                                    </p>\r" +
    "\n" +
    "                                    <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}\r" +
    "\n" +
    "                                    </p>\r" +
    "\n" +
    "                                    <p>Reserve : {{ ldItem.Qty }}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </bslist-detail-template>\r" +
    "\n" +
    "                            <bslist-modal-form>\r" +
    "\n" +
    "                                <div class=\"row\">\r" +
    "\n" +
    "                                    <div ng-include=\"'app/services/appointment/appointmentServiceGr/modalFormMaster.html'\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </bslist-modal-form>\r" +
    "\n" +
    "                            <bslist-modal-detail-form>\r" +
    "\n" +
    "                                <div class=\"row\">\r" +
    "\n" +
    "                                    <div ng-include=\"'app/services/appointment/appointmentServiceGr/modalFormDetail.html'\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </bslist-modal-detail-form>\r" +
    "\n" +
    "                        </bslist> --> <div class=\"row\"> <div class=\"col-md-12\"> <textarea ng-model=\"mData.TechnicianNotes\" class=\"form-control\" disabled></textarea> </div> </div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Mekanik</label> <input type=\"text\" name=\"namaMekanik\" class=\"form-control\" placeholder=\"Nama Mekanik\" ng-model=\"mData.Teknisi\" ng-disabled=\"true\" skip-enable> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Jam Mulai (Plan)</label> <!-- <br> {{PlanStart}} --> <input type=\"text\" name=\"PlanStart\" class=\"form-control\" placeholder=\"PlanStart\" ng-model=\"PlanStart\" ng-disabled=\"true\" skip-enable> <!-- <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.PlanStart\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\">\r" +
    "\n" +
    "                    </bsdatepicker> --> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Jam Selesai (Plan)</label> <input type=\"text\" name=\"PlanFinish\" class=\"form-control\" placeholder=\"PlanFinish\" ng-model=\"PlanFinish\" ng-disabled=\"true\" skip-enable> <!-- <br> {{PlanFinish}} --> <!-- <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.PlanFinish\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\">\r" +
    "\n" +
    "                  </bsdatepicker> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Jam Mulai (Aktual)</label> <!-- <br> {{ClockOn}} --> <input type=\"text\" name=\"ClockOn\" class=\"form-control\" placeholder=\"ClockOn\" ng-model=\"ClockOn\" ng-disabled=\"true\" skip-enable> <!-- <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.PlanStart\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                    </bsdatepicker> --> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Jam Selesai (Aktual)</label> <!-- <br> {{ClockOff}} --> <input type=\"text\" name=\"ClockOff\" class=\"form-control\" placeholder=\"ClockOff\" ng-model=\"ClockOff\" ng-disabled=\"true\" skip-enable> <!--  <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.PlanFinish\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                  </bsdatepicker> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <label>Hasil Final Inspection</label> <div class=\"form-group\"> <div class=\"radio-inline\" style=\"padding-left:20px\"> <label class=\"radio\" style=\"margin-top:5px;margin-bottom:6px\"> <input type=\"radio\" ng-click=\"clearModel()\" name=\"typeAppointmentGR\" class=\"radiobox style-0\" ng-model=\"filter.Type\" ng-change=\"cekType(filter.Type)\" ng-disabled=\"true\" skip-enable value=\"1\"> <span>Ok</span> </label> </div> <div class=\"radio-inline\" style=\"padding-left:20px\"> <label class=\"radio\" style=\"margin-top:5px;margin-bottom:6px\"> <input type=\"radio\" ng-click=\"clearModel()\" name=\"typeAppointmentGR\" class=\"radiobox style-0\" ng-model=\"filter.Type\" ng-change=\"cekType(filter.Type)\" ng-disabled=\"true\" skip-enable value=\"0\"> <span>Not Ok</span> </label> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Foreman</label> <input type=\"text\" name=\"namaMekanik\" class=\"form-control\" placeholder=\"Nama Mekanik\" ng-model=\"mData.foreMan\" ng-disabled=\"true\" skip-enable> </div> <!--<div class=\"form-group\">\r" +
    "\n" +
    "                  <label>Waktu Final Inspection</label>\r" +
    "\n" +
    "                  \r" +
    "\n" +
    "                   <div class=\"col-md-6 form-inline\">\r" +
    "\n" +
    "                              <div class=\"form-group\" style=\"width:200px;\">\r" +
    "\n" +
    "                                        <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.EstimateDeliveryTime\">\r" +
    "\n" +
    "                                            </bsdatepicker>--> <!--<bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.waktuInspection\" ng-change=\"DateChange(dt)\" ng-disabled=\"false\" skip-enable>\r" +
    "\n" +
    "                                    </bsdatepicker>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            <div class=\"form-group\" style=\"width:200px;\">\r" +
    "\n" +
    "                                <div uib-timepicker ng-model=\"mData.waktuInspection\" ng-change=\"changedTime(mData.waktuInspection)\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" ng-disabled=\"adjustDate\"></div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                </div>--> <div class=\"row\" style=\"margin-bottom:10px;margin-top:10px\"> <div class=\"col-md-12\"> <label>Waktu Final Inspection</label> </div> </div> <div id=\"waktuInspection\" class=\"row\" style=\"margin-bottom:10px;margin-top:10px\"> <div class=\"col-md-12\"> <div class=\"col-xs-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <!--<bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.EstimateDeliveryTime\">\r" +
    "\n" +
    "                                              </bsdatepicker>--> <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.waktuInspection\" ng-change=\"DateChange(dt)\" ng-disabled=\"false\" skip-enable> </bsdatepicker> </div> </div> <div class=\"col-xs-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <div uib-timepicker ng-model=\"mData.waktuInspection\" ng-change=\"changedTime(mData.waktuInspection)\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" ng-disabled=\"adjustDate\"></div> </div> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <fieldset style=\"padding:10px;padding-top:0px;border:2px solid #e2e2e2\"> <legend style=\"margin-bottom:0px;margin-left:10px;width:inherit;border-bottom:0px\"> Job Suggest </legend> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <textarea ng-model=\"mData.Suggestion\" class=\"form-control\"></textarea> </div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-md-3\"> <label>Kategori Job Suggest</label> </div> <div class=\"col-md-6\"> <div class=\"col-xs-6\" ng-disabled=\"true\" skip-enable> <bscheckbox ng-model=\"mData.CategoryJobSuggestKeamanan\" label=\"Safety\" ng-click=\"CountShift()\" true-value=\"1\" false-value=\"0\"> <!-- ng-disabled=\"true\"\r" +
    "\n" +
    "                                            skip-enable> --> </bscheckbox> <bscheckbox ng-model=\"mData.CategoryJobSuggestPeraturan\" label=\"Regulation\" ng-click=\"CountShift()\" true-value=\"2\" false-value=\"0\"> <!-- ng-disabled=\"true\"\r" +
    "\n" +
    "                                            skip-enable> --> </bscheckbox> </div> <div class=\"col-xs-6\"> <bscheckbox ng-model=\"mData.CategoryJobSuggestKenyamanan\" label=\"Comfortable\" ng-click=\"CountShift()\" true-value=\"4\" false-value=\"0\"> <!-- ng-disabled=\"true\"\r" +
    "\n" +
    "                                            skip-enable> --> </bscheckbox> <bscheckbox ng-model=\"mData.CategoryJobSuggestEfisiensi\" label=\"Efficiency\" ng-click=\"CountShift()\" true-value=\"8\" false-value=\"0\"> <!-- ng-disabled=\"true\"\r" +
    "\n" +
    "                                            skip-enable> --> </bscheckbox> </div> </div> </div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Tanggal Job Selanjutnya</label> <bsdatepicker name=\"DateAppoint\" date-options=\"DateOptions\" ng-model=\"mData.SuggestionDate\" ng-disabled=\"false\" skip-enable> </bsdatepicker> </div> </div> </div> <!-- <textarea class=\"form-control\"></textarea> --> </fieldset> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "                            <div class=\"col-md-12\">\r" +
    "\n" +
    "                                <fieldset style=\"padding:10px;padding-top:0px;border:2px solid #e2e2e2;\">\r" +
    "\n" +
    "                                    <legend style=\"margin-bottom:0px;margin-left:10px;width:inherit;border-bottom:0px;\">\r" +
    "\n" +
    "                                        Job Suggest\r" +
    "\n" +
    "                                    </legend>\r" +
    "\n" +
    "                                    <textarea ng-model=\"mDataJobSuggest.Suggestion\" class=\"form-control\"></textarea>\r" +
    "\n" +
    "                                </fieldset>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"row\" style=\"margin-top:10px;\">\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <label>Kategori Job Suggest</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-6\">\r" +
    "\n" +
    "                                <div class=\"col-md-6\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    <bscheckbox ng-model=\"mData.CategoryJobSuggestKeamanan\" label=\"Safety\" ng-click=\"CountShift()\" true-value=1 false-value=0 ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    </bscheckbox>\r" +
    "\n" +
    "                                    <bscheckbox ng-model=\"mData.CategoryJobSuggestPeraturan\" label=\"Regulation\" ng-click=\"CountShift()\" true-value=2 false-value=0 ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    </bscheckbox>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-6\">\r" +
    "\n" +
    "                                    <bscheckbox ng-model=\"mData.CategoryJobSuggestKenyamanan\" label=\"Comfortable\" ng-click=\"CountShift()\" true-value=4 false-value=0 ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    </bscheckbox>\r" +
    "\n" +
    "                                    <bscheckbox ng-model=\"mData.CategoryJobSuggestEfisiensi\" label=\"Efficiency\" ng-click=\"CountShift()\" true-value=8 false-value=0 ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    </bscheckbox>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"row\" style=\"margin-top:10px;\">\r" +
    "\n" +
    "                            <div class=\"col-md-6\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <label>Tanggal Job Selanjutnya</label>\r" +
    "\n" +
    "                                    <bsdatepicker name=\"DateAppoint\" date-options=\"DateOptions\" ng-model=\"mDataJobSuggest.SuggestionDate\" ng-disabled=\"false\" skip-enable>\r" +
    "\n" +
    "                                    </bsdatepicker>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div> --> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Pemeriksaan Akhir Sebelum Penyerahan </div> </uib-accordion-heading> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <!-- <textarea class=\"form-control\"></textarea> --> <div class=\"col-md-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.InsideCleaness\" label=\"{{mDataTemp.InsideCleanessLabel}}\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.OutsideCleaness\" label=\"{{mDataTemp.OutsideCleanessLabel}}\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.CompletenessVehicle\" label=\"{{mDataTemp.CompletenessVehicleLabel}}\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.UsedPart\" label=\"{{mDataTemp.UsedPartLabel}}\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-8\" ng-show=\"mData.UsedPart == 1\"> <input type=\"text\" class=\"form-control\" placeholder=\"Part Bekas\" ng-model=\"mData.OldPartKeepPlace\" name=\"\"> </div> <div class=\"col-md-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.DriverCarpetPosition\" label=\"{{mDataTemp.DriverCarpetPositionLabel}}\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </fieldset> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Penjelasan Saat Penyerahan </div> </uib-accordion-heading> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <div class=\"col-md-12\"> <label>{{mDataTemp.JobResultExplain}}</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel('JobResultExplainReason')\" name=\"IsJobResultExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsJobResultExplainOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.IsJobResultExplainOk\"\r" +
    "\n" +
    "                      label=\"Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel()\" name=\"IsJobResultExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsJobResultExplainOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Not Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" skip-enable placeholder=\"Alasan\" name=\"JobResultExplainReason\" ng-model=\"mData.JobResultExplainReason\" ng-disabled=\"mData.IsJobResultExplainOk == 1 || mData.IsJobResultExplainOk == undefined\"> </div> </div> </div> <div class=\"col-md-12\"> <label>{{mDataTemp.JobSparePartExplain}}</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel('SparePartExplainReason')\" name=\"IsSparePartExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsSparePartExplainOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel()\" name=\"IsSparePartExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsSparePartExplainOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Not Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" skip-enable placeholder=\"Alasan\" name=\"SparePartExplainReason\" ng-model=\"mData.SparePartExplainReason\" ng-disabled=\"mData.IsSparePartExplainOk==1 || mData.IsSparePartExplainOk == undefined\"> </div> </div> </div> <div class=\"col-md-12\"> <label>{{mDataTemp.JobPartPriceExplain}}</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel()\" name=\"IsConfirmPriceOk\" class=\"radiobox style-0\" ng-model=\"mData.IsConfirmPriceOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel('ConfirmPriceReason')\" name=\"IsConfirmPriceOk\" class=\"radiobox style-0\" ng-model=\"mData.IsConfirmPriceOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Not Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" skip-enable placeholder=\"Alasan\" name=\"ConfirmPriceReason\" ng-model=\"mData.ConfirmPriceReason\" ng-disabled=\"mData.IsConfirmPriceOk==1 || mData.IsConfirmPriceOk == undefined\"> </div> </div> </div> <div class=\"col-md-12\"> <label>{{mDataTemp.JobItemSuggestExplain}}</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel('JobSuggestExplainReason')\" name=\"IsJobSuggestExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsJobSuggestExplainOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel()\" name=\"IsJobSuggestExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsJobSuggestExplainOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Not Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" skip-enable placeholder=\"Alasan\" name=\"JobSuggestExplainReason\" ng-model=\"mData.JobSuggestExplainReason\" ng-disabled=\"mData.IsJobSuggestExplainOk==1 || mData.IsJobSuggestExplainOk == undefined\"> </div> </div> </div> <div class=\"col-md-12\"> <label>{{mDataTemp.JobConfirmCheckExplain}}</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel('ConfirmVehicleCheckReason')\" name=\"IsConfirmVehicleCheckOk\" class=\"radiobox style-0\" ng-model=\"mData.IsConfirmVehicleCheckOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" ng-click=\"clearModel()\" name=\"IsConfirmVehicleCheckOk\" class=\"radiobox style-0\" ng-model=\"mData.IsConfirmVehicleCheckOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox\r" +
    "\n" +
    "                      ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                      label=\"Not Ok\"\r" +
    "\n" +
    "                      true-value=1\r" +
    "\n" +
    "                      false-value=0>\r" +
    "\n" +
    "                </bscheckbox> --> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" skip-enable placeholder=\"Alasan\" name=\"ConfirmVehicleCheckReason\" ng-model=\"mData.ConfirmVehicleCheckReason\" ng-disabled=\"mData.IsConfirmVehicleCheckOk==1 || mData.IsConfirmVehicleCheckOk == undefined\"> </div> </div> </div> </fieldset> </div> <div ng-form=\"serviceExplanationWO\" novalidate uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Konfirmasi Rencana Follow Up </div> </uib-accordion-heading> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Follow Up</bsreqlabel> <bsdatepicker name=\"FollowUpDate\" date-options=\"DateOptions\" ng-model=\"mData.FollowUpDate\" required ng-required=\"true\"> </bsdatepicker> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Telepon</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-required=\"true\" required placeholder=\"Kontak Person\" name=\"PhoneNumber\" ng-model=\"mData.PhoneNumber\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Telepon</bsreqlabel> <ui-select name=\"phoneType\" required ng-model=\"mData.phoneType\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kategori\"> {{$select.selected.Name}} </ui-select-match> <ui-select-choices repeat=\"item.Id as item in phoneType\"> <span ng-bind-html=\"item.Name\"></span> </ui-select-choices> </ui-select> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Waktu Follow Up</bsreqlabel> <ui-select name=\"StatusFUTime\" required ng-model=\"mData.StatusFUTime\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kategori\"> {{$select.selected.Name}} </ui-select-match> <ui-select-choices repeat=\"item.Id as item in StatusFUTime\"> <span ng-bind-html=\"item.Name\"></span> </ui-select-choices> </ui-select> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Cetak Toyota ID </div> </uib-accordion-heading> <div class=\"panel panel-default\"> <div class=\"row\" style=\"max-width: 1000px; margin: auto\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <h3 style=\"color: red;margin-left: 5% ;font-size: 20pt\">ToyotaID<br> <h3> <div id=\"printSectionId\" style=\"-webkit-print-color-adjust:exact\"> <div style=\"position: relative;background-image: url('../images/logo_only.jpg');height: 250px;width: 500px;margin-left:50%;-webkit-print-color-adjust:exact\"> <div style=\"position: absolute;bottom:1px;text-align:left;margin-left:-30%\"> <a style=\"color: red;text-align:center;font-size: 50pt\">{{tmptoyotaid}}</a><br> <p style=\"color: black;margin-bottom: 5%; text-align:center;font-size: 35pt\">{{tmpCustomerName}}</p> </div> </div> </div> </h3></h3></div> <div class=\"col-md-12\"> <button type=\"button\" style=\"width: 300px; margin-right: 4px\" name=\"CetakToyota\" ng-click=\"CetakToyotaID(mData)\" class=\"rbtn btn; pull-right\"> Cetak Toyota ID </button> </div> </div> </div> </div> </div></uib-accordion> </div> <div class=\"row\"> <div class=\"btn-group pull-right\"> <button ng-disabled=\"serviceExplanationWO.$invalid\" skip-enable type=\"button\" name=\"simpanExplanation\" class=\"rbtn btn\" ng-click=\"saveExplanation(mData,mDataCustomer)\"> Simpan </button> <button ng-disabled=\"serviceExplanationWO.$invalid\" skip-enable type=\"button\" name=\"simpanExplanation\" class=\"rbtn btn\" ng-click=\"saveExplanationWithPrint(mData)\"> Simpan & Print </button> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/wo.html',
    "<div> <!-- <div ng-init=\"estimateAsb()\"> --> <script type=\"text/javascript\">//$('.containeramk').css('height','240px');</script> <!-- <script src=\"https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js\"></script>  test--> <!-- test push lagi --> <!-- <script type=\"text/javascript\" src=\"js/signature.js\"></script>\r" +
    "\n" +
    "    <script type=\"text/javascript\" src=\"js/signature_pad.js\"></script> --> <!--     <script type=\"text/javascript\" src=\"js/signature_pad.min.js\"></script>\r" +
    "\n" +
    "    <script type=\"text/javascript\" src=\"/dist/js/signature_pad.js\"></script>\r" +
    "\n" +
    "    <script type=\"text/javascript\" src=\"js/signature.js\"></script> --> <style type=\"text/css\">th.active {\r" +
    "\n" +
    "            background-color: red;\r" +
    "\n" +
    "            color: white;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .border-medium {\r" +
    "\n" +
    "            border-width: 2px;\r" +
    "\n" +
    "            /*border-style: solid;*/\r" +
    "\n" +
    "            border-radius: 5px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .border-thin {\r" +
    "\n" +
    "            border-width: 1px;\r" +
    "\n" +
    "            border-style: solid;\r" +
    "\n" +
    "            border-radius: 3px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .select-area-field-label {\r" +
    "\n" +
    "            font-size: 11px;\r" +
    "\n" +
    "            background-color: floralwhite;\r" +
    "\n" +
    "            padding: 2px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            top: -20px;\r" +
    "\n" +
    "            left: -40px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .select-areas-overlay {\r" +
    "\n" +
    "            /*background-color: #ccc;*/\r" +
    "\n" +
    "            background-color: #fff;\r" +
    "\n" +
    "            overflow: hidden;\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .select-areas-outline {\r" +
    "\n" +
    "            /*background: url('../images/outline.gif');*/\r" +
    "\n" +
    "            overflow: hidden;\r" +
    "\n" +
    "            padding: 2px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .choosen-area {\r" +
    "\n" +
    "            /*background-color: red;*/\r" +
    "\n" +
    "            border: 2px solid red;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .select-areas-resize-handler {\r" +
    "\n" +
    "            border: 2px #fff solid;\r" +
    "\n" +
    "            height: 10px;\r" +
    "\n" +
    "            width: 10px;\r" +
    "\n" +
    "            overflow: hidden;\r" +
    "\n" +
    "            background-color: #000;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .select-areas-delete-area {\r" +
    "\n" +
    "            background: url('../images/bt-delete.png');\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "            height: 20px;\r" +
    "\n" +
    "            width: 20px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .delete-area {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "            padding: 1px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk {\r" +
    "\n" +
    "            /* border: 1px solid red;\r" +
    "\n" +
    "            padding: 10px 10px 40px 10px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            margin: 0 auto 0 auto;\r" +
    "\n" +
    "            height: 100%;*/\r" +
    "\n" +
    "            border: 1px solid #8a8a8a;\r" +
    "\n" +
    "            /*padding: 20px 10px 10px 10px;*/\r" +
    "\n" +
    "            padding: 0px 0px 0px 0px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            margin: 0 auto 0 auto;\r" +
    "\n" +
    "            /*height: 100%;*/\r" +
    "\n" +
    "            background-color: #f5f5f5\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .signature {\r" +
    "\n" +
    "            /*border: 1px solid orange;*/\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .signature canvas {\r" +
    "\n" +
    "            /* border: 1px solid #999;\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;*/\r" +
    "\n" +
    "            /*border: 1px solid #999;*/\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "            background-color: #fff;\r" +
    "\n" +
    "            box-shadow: inset 0px 0px 12px 0px #c5c5c5;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .buttons {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            bottom: 10px;\r" +
    "\n" +
    "            left: 10px;\r" +
    "\n" +
    "            visibility: hidden;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .buttonsamk {\r" +
    "\n" +
    "            margin-top: 10px;\r" +
    "\n" +
    "            /*        position: absolute;\r" +
    "\n" +
    "            bottom: 2px;\r" +
    "\n" +
    "            left: 10px;*/\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .signature_container {\r" +
    "\n" +
    "            visibility: hidden;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .result {\r" +
    "\n" +
    "            border: 1px solid blue;\r" +
    "\n" +
    "            margin: 30px auto 0 auto;\r" +
    "\n" +
    "            height: 220px;\r" +
    "\n" +
    "            width: 568px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        /*====================*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containersign {\r" +
    "\n" +
    "            border: 1px solid blue;\r" +
    "\n" +
    "            padding: 10px 10px 40px 10px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            margin: 0 auto 0 auto;\r" +
    "\n" +
    "            height: 100%;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containersign .signature {\r" +
    "\n" +
    "            border: 1px solid dimgrey;\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .buttons {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .signature_container {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containersign .signature canvas {\r" +
    "\n" +
    "            border: 1px solid #999;\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .container .buttons {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            bottom: 10px;\r" +
    "\n" +
    "            left: 10px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .container .sizes {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            bottom: 10px;\r" +
    "\n" +
    "            right: 10px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containersign .sizes input {\r" +
    "\n" +
    "            width: 4em;\r" +
    "\n" +
    "            text-align: center;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        /* .result {\r" +
    "\n" +
    "            border: 1px solid blue;\r" +
    "\n" +
    "            margin: 30px auto 0 auto;\r" +
    "\n" +
    "            height: 220px;\r" +
    "\n" +
    "            width: 568px;\r" +
    "\n" +
    "        } */\r" +
    "\n" +
    "        /*====================*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .custom-slider.rzslider .rz-bar {\r" +
    "\n" +
    "            background: #ffe4d1;\r" +
    "\n" +
    "            height: 2px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .custom-slider.rzslider .rz-selection {\r" +
    "\n" +
    "            background: orange;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .custom-slider.rzslider .rz-pointer {\r" +
    "\n" +
    "            width: 8px;\r" +
    "\n" +
    "            height: 16px;\r" +
    "\n" +
    "            top: auto;\r" +
    "\n" +
    "            /* to remove the default positioning */\r" +
    "\n" +
    "            bottom: 0;\r" +
    "\n" +
    "            /*background-color: #333;*/\r" +
    "\n" +
    "            background-color: red;\r" +
    "\n" +
    "            border-top-left-radius: 3px;\r" +
    "\n" +
    "            border-top-right-radius: 3px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .custom-slider.rzslider .rz-pointer:after {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .custom-slider.rzslider .rz-bubble {\r" +
    "\n" +
    "            bottom: 14px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .custom-slider.rzslider .rz-limit {\r" +
    "\n" +
    "            font-weight: bold;\r" +
    "\n" +
    "            color: orange;\r" +
    "\n" +
    "            font-size: 20px;\r" +
    "\n" +
    "            bottom: 0;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .custom-slider.rzslider .rz-tick {\r" +
    "\n" +
    "            width: 1px;\r" +
    "\n" +
    "            height: 10px;\r" +
    "\n" +
    "            margin-left: 4px;\r" +
    "\n" +
    "            border-radius: 0;\r" +
    "\n" +
    "            background: #ffe4d1;\r" +
    "\n" +
    "            top: -1px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .custom-slider.rzslider .rz-tick.rz-selected {\r" +
    "\n" +
    "            background: orange;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .scaleWarp {\r" +
    "\n" +
    "            margin-bottom: -40px;\r" +
    "\n" +
    "            /*padding:16px;*/\r" +
    "\n" +
    "            padding-bottom: 0px;\r" +
    "\n" +
    "            padding-left: 1px;\r" +
    "\n" +
    "            letter-spacing: -10px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            /*float:left;*/\r" +
    "\n" +
    "            /*width:100%;*/\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        #slide {\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        /*.scaleWarp span+span{*/\r" +
    "\n" +
    "        /*margin-left:23%;*/\r" +
    "\n" +
    "        /*vertical-align:bottom;*/\r" +
    "\n" +
    "        /*bottom:0;*/\r" +
    "\n" +
    "        /*}*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .scaleWarp garis {\r" +
    "\n" +
    "            margin-left: 24.75%;\r" +
    "\n" +
    "            vertical-align: bottom;\r" +
    "\n" +
    "            bottom: 0;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        .ui.modal{\r" +
    "\n" +
    "            z-index: 10000 !important;\r" +
    "\n" +
    "            position: relative !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .panel-default>.panel-heading\r" +
    "\n" +
    "        {\r" +
    "\n" +
    "            color: #fff !important;\r" +
    "\n" +
    "            background-color: #d53337 !important;\r" +
    "\n" +
    "            border-color: #d53337 !important;\r" +
    "\n" +
    "            font-size: 18px !important;\r" +
    "\n" +
    "            font-weight: bold;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        .uib-time input{\r" +
    "\n" +
    "            width:100% !important;\r" +
    "\n" +
    "        }</style> <uib-accordion close-others=\"oneAtATime\"> <form> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(1)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Short Brief </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/wo-01.html'\" ng-if=\"includehtml1\"></div> </div> <div uib-accordion-group is-open=\"false\" class=\"panel-default\"> <uib-accordion-heading> <!-- <div ng-click=\"estimateAsbParam(1)\"> --> <div ng-click=\"includetheHtml(2);estimateAsbParam(1)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Plotting Stall </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/wo-02.html'\" ng-if=\"includehtml2\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Informasi Umum </div> </uib-accordion-heading> <!-- <div ng-include=\"'app/services/reception/wo/includeForm/customerIndex.html'\" ng-if=\"includehtml3\"></div> --> <div class=\"col-md-12\"> <div class=\"row\"> <uib-accordion close-others=\"oneAtATime\"> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(3)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Pelanggan & Kendaraan </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/customer-01.html'\" ng-if=\"includehtml3\"></div> </div> <!-- di komen dl penanggung jawab nya.. kata pa ari double sama yg info pelanggan di atas --> <!-- <div uib-accordion-group class=\"panel-default\" ng-show=\"showInst\">\r" +
    "\n" +
    "                                <uib-accordion-heading>\r" +
    "\n" +
    "                                    <div ng-click=\"includetheHtml(4)\" style=\"font-size: 26px; height:30px;font-weight: bold;\">\r" +
    "\n" +
    "                                        Penanggung Jawab\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </uib-accordion-heading>\r" +
    "\n" +
    "                                <div ng-include=\"'app/services/reception/wo/includeForm/customer-02.html'\" ng-if=\"includehtml4\"></div>\r" +
    "\n" +
    "                            </div> --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(5)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Pengguna </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/customer-03.html'\" ng-if=\"includehtml5\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(6)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Kontak Person & Pengambil Keputusan </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/CPPKInfo.html'\" ng-if=\"includehtml6\"></div> </div> </uib-accordion> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(7, mDataCrm.Vehicle.VIN)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Riwayat Servis </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/wo-03.html'\" ng-if=\"includehtml7\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(8)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Order Pekerjaan </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/wo-04.html'\" ng-if=\"includehtml8\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(9)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> OPL </div> </uib-accordion-heading> <!-- <div ng-include=\"'app/services/reception/wo/includeForm/wo-05.html'\" ng-if=\"includehtml9\"></div><hr> --> <div ng-include=\"'app/services/reception/wo/includeForm/OPLNew.html'\" ng-if=\"includehtml9\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(10);clickWAC(mDataCrm.Vehicle.VIN)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> WAC </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/wo-06.html'\" ng-if=\"includehtml10\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(11)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Job Suggest </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/wo-07.html'\" ng-if=\"includehtml11\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(12)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Summary </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/wo-08.html'\" ng-if=\"includehtml12\"></div> </div> </form> </uib-accordion> <bsui-modal show=\"bpCenter.show\" title=\"BP Center\" mode=\"modalMode\" on-save=\"bpCenterTrigger\" on-cancel=\"bpCtrTrgCancel\" button-settings=\"buttonSettingModal\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" style=\"margin-bottom: 250px\"> <label>Request Appointment Date</label> <!-- ng-change=\"onChange(selected)\" --> <bsdatepicker name=\"bpCenterDate\" date-options=\"DateOptions\" ng-model=\"bpCenterDate.date\"> </bsdatepicker> </div> </div> </div> </bsui-modal> <!-- <button ng-click=\"test()\" type=\"button\" class=\"rbtn btn pull-right\">Batal WO</button> --> <bsui-modal show=\"showPrediagnose\" title=\"PreDiagnose Form\" data=\"dataPrediagnose\" on-save=\"savePreDiagnose\" on-cancel=\"cancelPreDiagnose\" button-settings=\"listEstimationButton\" mode=\"modalModepreDiagnose\"> <div ng-form name=\"preDiagnoseForm\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <div ng-include=\"'app/services/preDiagnose/preDiagnose_CustomerComplaint.html'\"></div> --> <div class=\"form-group\"> <div class=\"col-md-12\" ng-hide=\"true\"> <div class=\"col-xs-12\"> <label>Job Id</label> </div> <div class=\"col-xs-4\"> <input type=\"text\" name=\"JobId\" ng-model=\"dataPrediagnose.JobId\" class=\"form-control\"> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-12\" ng-hide=\"true\"> <div class=\"col-xs-12\"> <label>Job Id</label> </div> <div class=\"col-xs-4\"> <input type=\"text\" name=\"PrediagnoseId\" ng-model=\"dataPrediagnose.PrediagnoseId\" class=\"form-control\"> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-12\"> <div class=\"col-xs-12\" style=\"padding-left: 0px\"> <bsreqlabel>Gejala Terdeteksi Pertama Kali</bsreqlabel> </div> </div> <div class=\"col-xs-4\"> <bsdatepicker name=\"FirstIndication\" date-options=\"DateOptions\" ng-required=\"true\" required ng-model=\"dataPrediagnose.FirstIndication\"> </bsdatepicker> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Frekuensi Kejadian</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"1\"> <span>Sekali</span> </label> </div> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"2\"> <span>Kadang - Kadang</span> </label> </div> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"3\"> <span>Selalu</span> </label> </div> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>MIL saat terjadi</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"1\"> <span>On</span> </label> </div> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"2\"> <span>Berkedip</span> </label> </div> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"3\"> <span>Off</span> </label> </div> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Kondisi Kendaraan Saat Gejala Terjadi</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"1\"> <span>Idling</span> </label> </div> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"2\"> <span>Starting</span> </label> </div> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"3\"> <span>Kecepatan Konstan</span> </label> </div> </div> <div class=\"col-xs-6\"></div> <div class=\"col-xs-6\" ng-show=\"dataPrediagnose.ConditionIndication == 3\" style=\"padding-left: 0px\"> <!-- <input type=\"number\" name=\"Kecepatan\" style=\"width: 100%\" placeholder=\"Kecepatan\" ng-model=\"mData.Speed\"> --> <!--  ng-keypress=\"allowPattern($event,1)\" --> <textarea name=\"Kecepatan\" ng-model=\"dataPrediagnose.notes\" maxlength=\"100\" style=\"width: 100%;height: 50px\"></textarea> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Kondisi Mesin</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"1\"> <span>Panas</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"2\"> <span>Dingin</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"3\"> <span>Lainnya</span> </label> </div> </div> <div class=\"col-xs-6\" ng-show=\"dataPrediagnose.MachineCondition == 3\" style=\"padding-left: 0px\"> <textarea name=\"lainnya\" ng-model=\"dataPrediagnose.MachineConditionTxt\" style=\"width: 100%;height: 70px\"></textarea> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Posisi Shift Lever</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftP\" label=\"P\" ng-click=\"CountShiftPrediagnose()\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftN\" label=\"N\" ng-click=\"CountShiftPrediagnose()\" true-value=\"2\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftD\" label=\"D\" ng-click=\"CountShiftPrediagnose()\" true-value=\"4\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftS\" label=\"S\" ng-click=\"CountShiftPrediagnose()\" true-value=\"8\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftR\" label=\"R\" ng-click=\"CountShiftPrediagnose()\" true-value=\"16\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftOne\" label=\"1\" ng-click=\"CountShiftPrediagnose()\" true-value=\"32\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftTwo\" label=\"2\" ng-click=\"CountShiftPrediagnose()\" true-value=\"64\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftThree\" label=\"3\" ng-click=\"CountShiftPrediagnose()\" true-value=\"128\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftFour\" label=\"4\" ng-click=\"CountShiftPrediagnose()\" true-value=\"256\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftLainnya\" ng-click=\"CountShiftPrediagnose()\" label=\"Lainnya\" true-value=\"512\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-12\" ng-hide=\"true\"> <input type=\"text\" name=\"PositionShiftLever\" ng-model=\"dataPrediagnose.PositionShiftLever\"> </div> </div> <div class=\"col-xs-12\" ng-show=\"dataPrediagnose.PosisiShiftLainnya == 512\"> <textarea name=\"PositionShiftLeverTxt\" ng-model=\"dataPrediagnose.PositionShiftLeverTxt\" style=\"width: 100%;height: 70px\"></textarea> </div> </div> <div class=\"col-xs-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"dataPrediagnose.IsNeedTest\" ng-change=\"testDrive(mData)\" label=\"Perlu Test Drive\" true-value=\"1\" false-value=\"0\" ng-hide=\"true\"> </bscheckbox> </div> </div> </div> </div> </bsui-modal> </div>"
  );


  $templateCache.put('app/services/reception/wo/woPickEstimatesDialog.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <p>List estimasi untuk kendaraan {{mDataCostumer.policeNumber}}</p> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <table class=\"table table-hover\"> <tr> <th>Nomor Estimasi</th> <th>Tanggal Pembuatan</th> <th>Nama SA</th> <th>Action</th> </tr> <tr ng-repeat=\"item in mDataCostumer.estimates\"> <td>{{item.number}}</td> <td>{{item.createDate}}</td> <td>{{item.sa}}</td> <td> <button class=\"btn btn-default\" type=\"button\">Pilih</button></td> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12 text-right\"> <button ng-click=\"ngDialog.closeAll();\" class=\"btn btn-default\" type=\"button\">Batal</button> <button ng-click=\"createWO();\" class=\"btn btn-default\" type=\"button\">Estimasi Baru</button> </div> </div>"
  );


  $templateCache.put('app/boards/asb/asbmaintest.html',
    "<div class=\"panel panel-default\" ng-show=\"asb.showMode=='main'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label for=\"usr\">No Polisi:</label> <input ng-model=\"asb.nopol\" type=\"text\" class=\"form-control\" id=\"usr\"> </div> <div class=\"form-group\"> <label for=\"usr\">Tipe:</label> <select ng-model=\"asb.tipe\" name=\"type\" class=\"form-control\"> <option value=\"Avanza\">Avanza</option> <option value=\"Yaris\">Yaris</option> <option value=\"Innova\">Innova</option> <option value=\"Innova Diesel\">Innova Diesel</option> <option value=\"Vios\">Vios</option> </select> </div> <div class=\"form-group\"> <label for=\"usr\">Durasi:</label> <input ng-model=\"mdata.durasi\" type=\"text\" class=\"form-control\" id=\"usr\"> </div> </div> </div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbEstimasi()\">ASB Estimasi</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbProduksi()\">ASB Produksi</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbGr()\">ASB GR</button> </div> </div> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='estimasi'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB Estimasi </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 70%; position: relative; right: 20px; top: 20px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"asb.currentDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div id=\"asb_estimasi_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.FullScreen()\" ng-show=\"false\">Full Screen</button> </div> </div> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='produksi'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB Produksi </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 15px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"asb.startDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 48px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"endDat\" date-options=\"dateOptions\" ng-model=\"asb.endDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 81px; z-index: 1; margin-bottom: -30px\"> <label> <input type=\"radio\" ng-model=\"asb.BpCategory\" ng-value=\"asb.TPS\" ng-change=\"asb.reloadBoard()\">TPS&nbsp;</label> <input type=\"radio\" ng-model=\"asb.BpCategory\" ng-value=\"asb.Light\" ng-change=\"asb.reloadBoard()\">Light </div> <div id=\"asb_produksi_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> </div> </div> <!-- {{asb}} --> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='asbgr'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB GR </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 70%; position: relative; right: 20px; top: 20px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"asb.currentDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div id=\"test_asb_asbgr_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.SaveData()\">Save Data</button> </div> </div> </div>"
  );


  $templateCache.put('app/boards/factories/showMessage.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">{{showMsg.title}}</h3></div> <div class=\"panel-body\"> <p> {{showMsg.message}}</p> <button type=\"button \" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0) \">{{showMsg.closeText}}</button> </div> </div>"
  );


  $templateCache.put('app/boards/jpcbgrview/jpcbgrview.html',
    "<div style=\"margin-left: 10px; margin-right: 10px\"> <div class=\"row\"> <button type=\"button\" class=\"btn btn-default panel-title pull-right\" ng-click=\"showOptions(true)\" style=\"margin-top: -4px; margin-left: 4px; padding-bottom: 4px;margin-bottom:10px\"> <i class=\"fa fa-fw fa-gear\"></i> </button> </div> <div class=\"row\"> <div ng-class=\"wcolumn\"> <div class=\"panel panel-default\" id=\"jpcb_gr_view_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> JPCB GR </h3> <h3 class=\"panel-title pull-right\" ng-show=\"jpcbgrview.isCurrentDate\"> &nbsp;&nbsp;{{ clock }} </h3> <div style=\"width: 200px; margin-top: -4px\" class=\"pull-right\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"jpcbgrview.currentDate\" ng-change=\"jpcbgrview.reloadBoard()\"></bsdatepicker> </div> <h3 class=\"panel-title text-center\" style=\"font-size: 18px; font-weight:bolder;margin:10px 0px 10px 0px\"><br><br> <!-- <i class=\"fa fa-group\" style=\"font-size:18px\"></i>&nbsp; --> <!-- <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Working Technician : {{countTechnician}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">In Progress : {{countInProgress}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Production Capacity : {{productionCapacity}} %</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">No Show : {{countNoShow}}</span> &nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Carry Over : {{countCarryOver}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Booking : {{countBookingA}}/{{countBookingB}}</span> &nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">WIP : {{countWIP}}</span> &nbsp; --> <span>Working Technician : {{countTechnician}}</span>&nbsp; <span>In Progress : {{countInProgress}}</span>&nbsp; <span>Production Capacity : {{countProductionCapacity}} %</span>&nbsp; <span>No Show : {{countNoShow}}</span> &nbsp; <span>Carry Over : {{countCarryOver}}</span>&nbsp; <span>Booking : {{countBookingA}}/{{countBookingB}}</span> &nbsp; <span>WIP : {{countWIP}}</span> &nbsp; </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"jpcb_gr_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div ng-class=\"ncolumn\" style=\"padding-left: 0\" ng-show=\"ncolvisible\"> <div class=\"panel panel-default\" id=\"jpcb_gr_edit_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <!-- Select Stall --> Options </h3> <h3 class=\"panel-title pull-right\"> <a href=\"#\"><i class=\"fa fa-fw fa-remove\" ng-click=\"showOptions(false)\">&nbsp;</i></a> </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\" style=\"padding: 8px\"> <div class=\"row\"> <div class=\"col-md-12\"> <h4 style=\"border-bottom: 2px solid #a5a5a5\">Hide Header / Footer</h4> <div ng-repeat=\"item in filter\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.name}}</label> </div> <hr> <h4 style=\"border-bottom: 2px solid #a5a5a5\">Select Stall</h4> <div ng-repeat=\"item in jpcbgrview.tempStallVisible\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.title}}</label> </div> </div> </div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-danger btn-sm\" ng-click=\"saveOptions(filter)\">Update Board</button> </div> </div> </div> </div> </div> </div> <br>"
  );


  $templateCache.put('app/boards/jpcbgrview/options.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337; margin: -10px\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">Select Stall</h3> </div> <div class=\"panel-body\"> {{jpcbgrview}} <form method=\"post\"> <div class=\"row\"> <div class=\"col-md-12\"> <div ng-repeat=\"item in jpcbgrview.tempStallVisible\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.Name}}</label> </div> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div> <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog(0)\" style=\"color:white; background-color:#D53337\">OK</button> <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0)\">Cancel</button> </div> </div> </div> </div> </form> </div> </div>"
  );

}]);
