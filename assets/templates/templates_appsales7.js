angular.module('templates_appsales7', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/sales/sales7/registration/approvalRevisiBatalAfi/approvalRevisiBatalAfi.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "        $('body .modals').remove();\r" +
    "\n" +
    "    }</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "        -webkit-border-radius: 4px;\r" +
    "\n" +
    "        -moz-border-radius: 4px;\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "        border: 0px;\r" +
    "\n" +
    "        background-color: transparent;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <!--<script type=\"text/javascript\" src=\"js/uiscript.js\"></script>--> <bsform-grid ng-show=\"ApprovalRevisiBatalAfiMain\" ng-form=\"ApprovalRevisiBatalAfiForm\" factory-name=\"ApprovalRevisiBatalAfiFactory\" model=\"mApprovalRevisiBatalAfi\" model-id=\"SpkId\" loading=\"loading\" hide-new-button=\"true\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ApprovalRevisiBatalAfiForm\" form-title=\"Approval Pengiriman Dengan Pengecualian\" modal-title=\"Approval Pengiriman Dengan Pengecualian\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> </bsform-grid> <div ng-show=\"ApprovalRevisiBatalAfiDetail\"> <div class=\"row\" style=\"margin: -30px 0 0 0\"> <button ng-show=\"UserIsCaoHoAdmin\" ng-click=\"SetujuApprovalRevisiBatalAfi()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Setuju </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"UserIsCaoHoAdmin\" ng-click=\"TidakSetujuApprovalRevisiBatalAfi()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Tolak </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float:right\" ng-click=\"BatalApprovalRevisiBatalAfi()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <fieldset ng-disabled=\"true\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <br> <!-- Baris 1 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <!-- Kiri --> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>No. SPK</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mApprovalRevisiBatalAfi.FormSPKNo\" class=\"form-control\" style=\"float: left\"> </div> <!-- Kanan --> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nomor Billing</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mApprovalRevisiBatalAfi.BillingCode\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 2 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nomor SO</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mApprovalRevisiBatalAfi.SoCode\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Tanggal Billing</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsdatepicker name=\"tanggal\" ng-model=\"mApprovalRevisiBatalAfi.BillingDate\" date-options=\"dateOptions\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </p> <!-- Baris 3 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nama Pelanggan</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mApprovalRevisiBatalAfi.NamaPelanggan\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nomor Rangka</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mApprovalRevisiBatalAfi.FrameNo\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 4 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Alamat</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mApprovalRevisiBatalAfi.AlamatPelanggan\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nomor Mesin</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mApprovalRevisiBatalAfi.EngineNo\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 5 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Model</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mApprovalRevisiBatalAfi.VehicleModelName\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Tahun Kendaraan</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mApprovalRevisiBatalAfi.AssemblyYear\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 6 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Tipe</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mApprovalRevisiBatalAfi.Description\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>CBU/CKD</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mApprovalRevisiBatalAfi.AssemblyType\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 7 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Warna</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mApprovalRevisiBatalAfi.ColorName\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>On/Off the Road</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mApprovalRevisiBatalAfi.OnOffTheRoadName\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> </div> </fieldset> <!-- Navigation Tab --> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px; background:linear-gradient(transparent 60%, rgba(0,0,0,0.03))\"> <li class=\"active\"><a style=\"padding: 9px 7px 10px 7px\" data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainerAfiApproval','infoPemilikAfiApproval')\"><i class=\"fa fa-user\"></i>&nbsp Info Pelanggan</a></li> <li><a style=\"padding: 9px 7px 10px 7px\" data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainerAfiApproval','dokumenAfiApproval')\" ng-click=\"loadImage()\"><i class=\"fa fa-folder-open\"></i>&nbsp Dokumen</a></li> </ul> <div id=\"tabContainerAfiApproval\" class=\"tab-content\"> <div id=\"infoPemilikAfiApproval\" class=\"tab-pane fade in active\" style=\"padding:10px 0 0 0;margin-left: 15px;margin-right: 10px\"> <!-- <div id=\"infoPerusahaan\" class=\"row\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin: 10px 0 0 0\"> --> <!-- <p style=\"width:120px; margin-top:-20px; margin-left:14px; background:white;\"><b>Informasi Pemilik</b></p> --> <div id=\"customerDetail\" style=\"margin: 0 0 0 0\"> <fieldset ng-disabled=\"actionAfi == 'lihatAfi' || actionAfi == 'batalAfi'\"> <form name=\"validasiafi\"> <!-- Baris 1 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Nama</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-user\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA'\" maxlength=\"255\" name=\"pemilikafi\" ng-model=\"mApprovalRevisiBatalAfi.NamaPemilik\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.pemilikafi.$touched == true) && (validasiafi.pemilikafi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.pemilikafi.$touched && validasiafi.pemilikafi.$invalid) || (validasiafi.pemilikafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 2 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>No Handphone</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-mobile\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' \r" +
    "\n" +
    "                                                                            || CodeRevisiAfi == 'REVB'\r" +
    "\n" +
    "                                                                            || CodeRevisiAfi == 'REVC' \r" +
    "\n" +
    "                                                                            || CodeRevisiAfi == 'REVD' \r" +
    "\n" +
    "                                                                            || CodeRevisiAfi == 'REVE' \r" +
    "\n" +
    "                                                                            || CodeRevisiAfi == 'REVF'\" maxlength=\"15\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"nohpafi\" ng-model=\"mApprovalRevisiBatalAfi.HpNumber\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.nohpafi.$touched == true) && (validasiafi.nohpafi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>Email</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-envelope\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB' || CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD'|| CodeRevisiAfi == 'REVE'|| CodeRevisiAfi == 'REVF'\" maxlength=\"50\" ng-model=\"mApprovalRevisiBatalAfi.Email\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.nohpafi.$touched && validasiafi.nohpafi.$invalid) || (validasiafi.nohpafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                    <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.pemilikafi.$touched && validasiafi.pemilikafi.$invalid\" aria-live=\"assertive\">This field is required.</i>\r" +
    "\n" +
    "                                                </div> --> </div> </p> <!-- Baris 3 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>No Telpon</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD' || CodeRevisiAfi == 'REVE' || CodeRevisiAfi == 'REVF'\" maxlength=\"15\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"mApprovalRevisiBatalAfi.PhoneNumber\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> <div ng-if=\"mApprovalRevisiBatalAfi.CustomerTypeId == 3\" class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>KTP/KITAS</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div ng-if=\"mApprovalRevisiBatalAfi.CustomerTypeId == 3\" class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-address-card\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"ktpafi\" ng-model=\"mApprovalRevisiBatalAfi.KTPNo\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.ktpafi.$touched == true) && (validasiafi.ktpafi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> <div ng-if=\"mApprovalRevisiBatalAfi.CustomerTypeId > 3\" class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>SIUP</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div ng-if=\"mApprovalRevisiBatalAfi.CustomerTypeId > 3\" class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-address-card\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"tdpsiup\" ng-model=\"mApprovalRevisiBatalAfi.SIUPNo\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <!-- <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.nohpafi.$touched && validasiafi.nohpafi.$invalid\" aria-live=\"assertive\">This field is required.</i> --> </div> <div class=\"col-md-2\"> </div> <div ng-if=\"mApprovalRevisiBatalAfi.CustomerTypeId == 3\" class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.ktpafi.$touched && validasiafi.ktpafi.$invalid) || (validasiafi.ktpafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <!-- <div ng-if=\"mApprovalRevisiBatalAfi.CustomerTypeId > 3\" class=\"col-md-4\">\r" +
    "\n" +
    "                                        <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.tdpsiup.$touched && validasiafi.tdpsiup.$invalid\" aria-live=\"assertive\">This field is required.</i>\r" +
    "\n" +
    "                                    </div> --> </div> </p> <!-- Baris 4 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>NPWP</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'\" name=\"npwpafi\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"mApprovalRevisiBatalAfi.NPWPNo\" ng-class=\"{invalidBox : (validasiafi.npwpafi.$touched == true) && (validasiafi.npwpafi.$invalid == true)}\" class=\"form-control\" style=\"float: left\" required> </div> </div> </div> <div ng-if=\"mApprovalRevisiBatalAfi.CustomerTypeId > 3\" class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>TDP</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div ng-if=\"mApprovalRevisiBatalAfi.CustomerTypeId > 3\" class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-address-card\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"tdpsiup\" ng-model=\"mApprovalRevisiBatalAfi.TDPNo\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.npwpafi.$touched && validasiafi.npwpafi.$invalid) || (validasiafi.npwpafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 5 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alamat 1</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-home\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'\" maxlength=\"30\" name=\"alamat1afi\" ng-model=\"mApprovalRevisiBatalAfi.AlamatPemilik\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.alamat1afi.$touched == true) && (validasiafi.alamat1afi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.alamat1afi.$touched && validasiafi.alamat1afi.$invalid) || (validasiafi.alamat1afi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 6 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alamat 2</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-home\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'\" maxlength=\"30\" name=\"alamat2afi\" ng-model=\"mApprovalRevisiBatalAfi.DistrictName\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.alamat2afi.$touched == true) && (validasiafi.alamat2afi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.alamat2afi.$touched && validasiafi.alamat2afi.$invalid) || (validasiafi.alamat2afi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 7 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alamat 3</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-home\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'\" maxlength=\"30\" name=\"alamat3afi\" ng-model=\"mApprovalRevisiBatalAfi.VillageName\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.alamat3afi.$touched == true) && (validasiafi.alamat3afi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.alamat3afi.$touched && validasiafi.alamat3afi.$invalid) || (validasiafi.alamat3afi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 8 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Provinsi</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"provinsiafi\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'\" placeholder=\"Provinsi\" ng-model=\"mApprovalRevisiBatalAfi.ProvinceId\" data=\"province\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" ng-change=\"filterKabupaten(mApprovalRevisiBatalAfi.ProvinceId)\" icon=\"fa fa-map-marker\" required> </bsselect> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Kode Pos</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"kodeposafi\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'\" placeholder=\"Kode Pos\" ng-model=\"mApprovalRevisiBatalAfi.AFIRegionCode\" data=\"regioncode\" item-text=\"PostalCode\" item-value=\"AFIRegionCode\" icon=\"fa fa-map-marker\" required> </bsselect> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.provinsiafi.$touched && validasiafi.provinsiafi.$invalid) || (validasiafi.provinsiafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.regioncodeafi.$touched && validasiafi.regioncodeafi.$invalid) || (validasiafi.regioncodeafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 9 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\" style=\"padding-right: 0px\"> <bsreqlabel>Kota/Kabupaten</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"kabkotaafi\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'\" placeholder=\"Kabupaten/Kota\" ng-disabled=\"mApprovalRevisiBatalAfi.ProvinceId == null || mApprovalRevisiBatalAfi.ProvinceId == undefined\" ng-model=\"mApprovalRevisiBatalAfi.CityRegencyId\" data=\"kabupaten\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" required icon=\"fa fa-map-marker \"> </bsselect> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Region Code AFI</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"regioncodeafi\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD' || CodeRevisiAfi == 'REVE'|| CodeRevisiAfi == 'REVF'\" placeholder=\"Region Code\" ng-model=\"mApprovalRevisiBatalAfi.AFIRegionCode\" data=\"regioncode\" item-text=\"AFIRegionCode,Name\" item-value=\"AFIRegionCode\" required icon=\"fa fa-map-marker\"> </bsselect> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.kabkotaafi.$touched && validasiafi.kabkotaafi.$invalid) || (validasiafi.kabkotaafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.regioncodeafi.$touched && validasiafi.regioncodeafi.$invalid) || (validasiafi.regioncodeafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 10 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\" style=\"padding-right: 0px\"> <bsreqlabel>Jenis</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"cartypeafi\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD' || CodeRevisiAfi == 'REVE'\" placeholder=\"Jenis\" ng-model=\"mApprovalRevisiBatalAfi.AFICarTypeCode\" data=\"cartype\" item-text=\"Jenis,Model,AFICarTypeCode\" item-value=\"AFICarTypeCode\" required icon=\"fa fa-car \"> </bsselect> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Model</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"cartypeafi\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD' || CodeRevisiAfi == 'REVE'\" placeholder=\"Model\" ng-model=\"mApprovalRevisiBatalAfi.AFICarTypeCode\" data=\"cartype\" item-text=\"Model,Jenis,AFICarTypeCode\" item-value=\"AFICarTypeCode\" placeholder=\"\" required icon=\"fa fa-car\"> </bsselect> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.cartypeafi.$touched && validasiafi.cartypeafi.$invalid) || (validasiafi.cartypeafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.cartypeafi.$touched && validasiafi.cartypeafi.$invalid) || (validasiafi.cartypeafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 11 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\" style=\"padding-right: 0px\"> <bsreqlabel>Tanggal Efektif Faktur</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsdatepicker name=\"tanggal\" name=\"efectiveDateAfi\" ng-disabled=\"kecuali\" ng-model=\"mApprovalRevisiBatalAfi.InvoiceStartEffectiveDate\" date-options=\"dateOptions\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>Warna Unit</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-mobile\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD'\" maxlength=\"50\" ng-model=\"mApprovalRevisiBatalAfi.CustomColorName\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.nohpafi.$touched == true) && (validasiafi.nohpafi.$invalid == true)}\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.efectiveDateAfi.$touched && validasiafi.efectiveDateAfi.$invalid) || (validasiafi.efectiveDateAfi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                    <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.cartypeafi.$touched && validasiafi.cartypeafi.$invalid\" aria-live=\"assertive\">This field is required.</i>\r" +
    "\n" +
    "                                                </div> --> </div> </p> <!-- Baris 12 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\" style=\"padding-right: 0px\"> <label>Permintaan Plat Nomor Khusus</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD' || CodeRevisiAfi == 'REVE' || CodeRevisiAfi == 'REVF'\" maxlength=\"50\" ng-model=\"mApprovalRevisiBatalAfi.SpecialReqNumberPlate\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.nohpafi.$touched == true) && (validasiafi.nohpafi.$invalid == true)}\" style=\"float: left\"> </div> </div> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>Status AFI</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa\" style=\"top:8px\"></i> <input type=\"text\" maxlength=\"50\" ng-disabled=\"true\" ng-model=\"mApprovalRevisiBatalAfi.StatusName\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <!-- <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.efectiveDateAfi.$touched && validasiafi.efectiveDateAfi.$invalid\" aria-live=\"assertive\">This field is required.</i> --> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                    <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                        <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.cartypeafi.$touched && validasiafi.cartypeafi.$invalid\" aria-live=\"assertive\">This field is required.</i>\r" +
    "\n" +
    "                                                    </div> --> </div> </p> </form> </fieldset> </div> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alasan Revisi / Batal AFI</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <textarea rows=\"5\" ng-disabled=\"actionAfi == 'lihatAfi'\" class=\"form-control\" ng-model=\"mApprovalRevisiBatalAfi.AFIRevisionCancelReason\" placeholder=\"Alasan\">\r" +
    "\n" +
    "                                                                    </textarea> </div> </div> </p> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alasan Reject dari ADH/CAO</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <textarea rows=\"5\" ng-disabled=\"actionAfi == 'lihatAfi'\" class=\"form-control\" ng-model=\"mApprovalRevisiBatalAfi.ADHCAORejectReason\" placeholder=\"Alasan\">\r" +
    "\n" +
    "                                                                    </textarea> </div> </div> </p> <!-- </div> --> </div> <div id=\"dokumenAfiApproval\" class=\"tab-pane fade\" style=\"padding:10px 0 0 0\"> <div class=\"row\" style=\"margin: 0 0 0 0; min-height: 400px;padding-top: 15px\"> <div class=\"row\" style=\"margin: 0 0 0 0\" ng-repeat=\"listTempDoc in mApprovalRevisiBatalAfi.listDokumen\"> <!-- <p> --> <div class=\"row\" style=\"margin: 0 0 0 0\"> <!-- <p> --> <div class=\"row\" style=\"margin: 0 0 10px 0;padding-left: 10px\"> <!-- <div --> <div class=\"col-md-1\" ng-hide=\"actionAfi == 'lihatAfi'\" ng-click=\"removeExtension($index)\" style=\"color:red;padding-top: 10px ; float: left; width: 20px !important\"> <i class=\"fa fa-times\" aria-hidden=\"true \" style=\"float:right\"></i> </div> <div class=\"col-md-3\" style=\"margin-top: 8px; width:22% !important\"> <div style=\"float: left\"><label><a><u ng-click=\"viewDocumentAFI(listTempDoc)\">{{listTempDoc.DocumentName}}</u></a></label></div> <div style=\"float: right\">:&nbsp&nbsp</div> </div> <div class=\"col-md-8\"> <div class=\"input-icon-right\"> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"listTempDoc.DocumentDataName\" ng-change=\"cekStatusUpload(listTempDoc, listTempDoc.UploadDokumenCustomer.FileName)\" name=\"fieldNoSpkEnd\" placeholder=\"\" ng-maxlength=\"50\" required> <!-- <input type=\"text\" style=\"display: none\" ng-model=\"listTempDoc.DocumentDataName = listTempDoc.UploadDokumenCustomer.FileName\"> --> </div> </div> <!-- <div ng-hide=\"actionAfi == 'lihatAfi'\" class=\"col-md-1\" style=\"padding-right: 0px;\">\r" +
    "\n" +
    "                                <label class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;width: 100% !important\">\r" +
    "\n" +
    "                                                                    Browse <input type=\"file\" bs-uploadsingle=\"onFileSelect($files)\" img-upload=\"listTempDoc.UploadDokumenCustomer\" style=\"display:none\">\r" +
    "\n" +
    "                                                                </label>\r" +
    "\n" +
    "                            </div> --> </div> <!-- </p> --> </div> <!-- </p> --> </div> <p> <div class=\"row\" ng-hide=\"actionAfi == 'lihatAfi'\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-11\" style=\"border-bottom: 1px solid #ccc; min-height: 20px\"> </div> <div class=\"col-md-1\"> <div class=\"row\" style=\"margin-left: 15px; margin-right: 14px; margin-top: 10px\"> <div style=\"float: right;color: #d53337\" ng-click=\"openDokumenlist()\"> <i class=\"fa fa-plus-circle\" aria-hidden=\"true\" style=\"font-size: 1.5em\"></i> </div> </div> </div> </div> </p> </div> </div> </div> </div> <div class=\"ui modal ModalAlasanPenolakanApprovalRevisiBatalAfi\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"BatalAlasanApprovalRevisiBatalAfi()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Alasan Penolakan</h4> </div> <br> <div class=\"modal-body\"> <textarea rows=\"10\" cols=\"80\" maxlength=\"512\" placeholder=\"input alasan...\" ng-model=\"mApprovalRevisiBatalAfi.ADHCAORejectReason\"></textarea> </div> <div class=\"modal-footer\"> <!--<button onclick=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"SubmitAlasanApprovalRevisiBatalAfi()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-disabled=\"mApprovalRevisiBatalAfi.ADHCAORejectReason==undefined\">Simpan</button> <button ng-click=\"BatalAlasanApprovalRevisiBatalAfi()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/goodReceiptSTNK/goodReceiptSTNK.html',
    "<script type=\"text/javascript\">function SetReceiveSTNKDateMinDate(TheDate) {\r" +
    "\n" +
    "        var Da_ReceiveSTNKDateMaxDate = TheDate;\r" +
    "\n" +
    "        var dd = Da_ReceiveSTNKDateMaxDate.getDate();\r" +
    "\n" +
    "        var mm = Da_ReceiveSTNKDateMaxDate.getMonth() + 1; //January is 0!\r" +
    "\n" +
    "        var yyyy = Da_ReceiveSTNKDateMaxDate.getFullYear();\r" +
    "\n" +
    "        if (dd < 10) {\r" +
    "\n" +
    "            dd = '0' + dd\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (mm < 10) {\r" +
    "\n" +
    "            mm = '0' + mm\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        Da_ReceiveSTNKDateMaxDate = yyyy + '-' + mm + '-' + dd;\r" +
    "\n" +
    "        document.getElementById(\"Min_Da_ReceiveSTNKDateMaxDate\").setAttribute(\"min\", Da_ReceiveSTNKDateMaxDate);\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }</script> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"ShowGoodReceiptMain\" class=\"row\" style=\"margin: 0 0 0 0\"> <button id=\"PengembalianSpk\" ng-click=\"TambahGoodReceiptSTNK()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px\" ng-disabled=\"btnPengembalianSpk\">Tambah</button> <bsform-grid ng-form=\"goodReceiptSTNKForm\" factory-name=\"goodReceiptSTNKFactory\" model=\"mgoodReceiptSTNK\" model-id=\"ReceiveId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" show-advsearch=\"on\" form-name=\"goodReceiptSTNKForm\" form-title=\"Terima STNK Biro Jasa\" modal-title=\"goodReceiptSTNK\" loading=\"loading\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Surat Jalan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nomor Surat Jalan\" ng-model=\"filterStatusSTNKMain.SuratJalanNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <bsselect name=\"vendor\" data=\"vendor\" item-text=\"ServiceBureauName, ServiceBureauCode\" ng-model=\"filterStatusSTNKMain.ServiceBureauId\" item-value=\"ServiceBureauId\" placeholder=\"Pilih Biro Jasa\" required icon=\"fa fa-search \"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status GR</label> <div class=\"input-icon-right\"> <bsselect name=\"filterSales\" ng-model=\"filterStatusSTNKMain.STNKGrStatusId\" data=\"statusGRSTNK\" item-text=\"STNKGrStatusName\" item-value=\"STNKGrStatusId\" placeholder=\"Status GR\" icon=\"fa fa-sitemap\"></bsselect> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nomor Rangka\" ng-model=\"filterStatusSTNKMain.FrameNo\" ng-maxlength=\"50\"> </div> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Good Receipt</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nomor Good Receipt\" ng-model=\"filterStatusSTNKMain.STNKGRCode\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tanggal Dokumen GR</label> <bsdatepicker name=\"JadwalPenerimaan\" ng-model=\"filterStatusSTNKMain.StartSTNKGRDate\" date-options=\"dateOption\" required> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Sampai Tanggal</label> <bsdatepicker name=\"JadwalPenerimaan\" ng-model=\"filterStatusSTNKMain.EndSTNKGRDate\" date-options=\"dateOption\" required> </bsdatepicker> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"ShowTambahGoodReceipt\" class=\"row\" style=\"margin: 0 10px 0 10px\"> <!-- button simpan dan kembali --> <div class=\"row\" style=\"margin: 0 0 10px 0\"> <button style=\"float: right\" ng-click=\"showKonfirmasiGoodReceipt()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" ng-disabled=\"selectRowgridTambahGRSTNK.length<1||selectRowgridTambahGRSTNK==null||selectRowgridTambahGRSTNK==undefined\"> <span class=\"ladda-label\"> Simpan </span> <span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliGoodReceiptSTNK()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> </div> <!-- <div class=\"col-md-12 advsearch\" style=\"padding:10px; margin-bottom:0px; border:1px solid #e2e2e2;background-color:rgba(213,51,55,0.02)\"> --> <div class=\"col-md-12 advsearch\" style=\"padding:10px; margin-bottom:0px; border:1px solid #e2e2e2; margin-right: 10px\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <bsselect name=\"vendor\" data=\"vendor\" item-text=\"ServiceBureauName, ServiceBureauCode\" ng-model=\"filterTambahStatusSTNK.ServiceBureauId\" ng-change=\"filterVendor()\" item-value=\"ServiceBureauId\" placeholder=\"Pilih Biro Jasa\" required icon=\"fa fa-search \"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status STNK</label> <select class=\"form-control\" disabled> <option selected>Terima STNK Dari Biro Jasa</option> <option>2</option> <option>3</option> </select> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tipe PO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" disabled value=\"STNK\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tanggal Terima STNK</label> <bsdatepicker name=\"tanggalFrom\" date-options=\"dateOption\" ng-model=\"filterTambahStatusSTNK.StartReceiveSTNKDate\" ng-change=\"tanggalmin(dt)\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Sampai Tanggal</label> <bsdatepicker name=\"tanggalTo\" date-options=\"dateOption\" ng-disabled=\"filterTambahStatusSTNK.StartReceiveSTNKDate == null || filterTambahStatusSTNK.StartReceiveSTNKDate == undefined\" ng-model=\"filterTambahStatusSTNK.EndReceiveSTNKDate\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> </div> </div> <div class=\"\" style=\"position:absolute;right:20px;margin-top:-30px\"> <div class=\"pull-right\"> <div class=\"btn-group\"> <button class=\"btn wbtn\" style=\"padding:4px\" ng-click=\"getDataTambahSTNK()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search\"> <i class=\"large icons\"> <i class=\"database icon\"></i> <i class=\"inverted corner filter icon\"></i> </i>Search</button> </div> </div> </div> </div> <div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 10px; margin-bottom: 10px; margin-right: 10px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Nomor Surat Jalan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" required name=\"model\" placeholder=\"Input No. Surat Jalan\" ng-model=\"fieldInput.SuratJalanNo\" ng-maxlength=\"50\"> </div> </div> <div class=\"form-group\"> <label>No. GR</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"No. Good Receipt Terisi Otomatis\" ng-model=\"fieldInput.STNKGRCode\" disabled ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-6\"> <label>Catatan Penerimaan</label> <textarea name=\"\" id=\"\" style=\"width: 100%; height: 100px\" ng-model=\"fieldInput.Note\" class=\"form-control\"></textarea> </div> </div> <div class=\"col-md-12\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridTambahGRSTNK\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> </div> <div ng-show=\"ShowLihatGoodReceipt\" class=\"row\" style=\"margin: 0 10px 0 10px\"> <!-- button kembali --> <div class=\"row\" style=\"margin: 0 0 10px 0\"> <button style=\"float: right\" ng-click=\"KembaliGoodReceiptSTNK()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> </div> <!-- <div class=\"col-md-12 advsearch\" style=\"padding:10px; margin-bottom:0px; border:1px solid #e2e2e2;background-color:rgba(213,51,55,0.02)\"> --> <div class=\"col-md-12 advsearch\" style=\"padding:10px; margin-bottom:0px; border:1px solid #e2e2e2; margin-right: 10px\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <!-- <bsselect   name=\"detailGRSTNK\"  data=\"detailGRSTNK.listFrameNo[0]\"\r" +
    "\n" +
    "                                item-text=\"ServiceBureauName, ServiceBureauCode\" ng-model=\"detailGRSTNK.listFrameNo[0].ServiceBureauName\" ng-disabled=\"true\"\r" +
    "\n" +
    "                                item-value=\"ServiceBureauId\" placeholder=\"Pilih Biro Jasa\" required=\"true\" icon=\"fa fa-search \">\r" +
    "\n" +
    "                    </bsselect>   --> <!-- hardcoded sementara --> <select class=\"form-control\" disabled> <option selected>{{detailGRSTNK.listFrameNo[0].ServiceBureauName}}</option> <option>2</option> <option>3</option> </select> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status STNK</label> <select class=\"form-control\" disabled> <option selected>Terima STNK Dari Biro Jasa</option> <option>2</option> <option>3</option> </select> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tipe PO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" disabled value=\"STNK\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Terima STNK</bsreqlabel> <bsdatepicker name=\"tanggalFrom\" date-options=\"dateOptionsStart\" ng-model=\"filterTambahStatusSTNK.StartReceiveSTNKDate\" ng-change=\"tanggalmin(dt)\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Sampai Tanggal</bsreqlabel> <bsdatepicker name=\"tanggalTo\" date-options=\"dateOptionsEnd\" ng-disabled=\"filterTambahStatusSTNK.StartReceiveSTNKDate == null || filterTambahStatusSTNK.StartReceiveSTNKDate == undefined\" ng-model=\"filterTambahStatusSTNK.EndReceiveSTNKDate\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> </div> </div> </div> <div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 10px; margin-bottom: 10px; margin-right: 10px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nomor Surat Jalan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. PO\" ng-model=\"detailGRSTNK.SuratJalanNo\" ng-disabled=\"detailGRSTNK.SuratJalanNo\" ng-maxlength=\"50\"> </div> </div> <div class=\"form-group\"> <label>No. GR</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" ng-model=\"detailGRSTNK.STNKGRCode\" ng-disabled=\"detailGRSTNK.STNKGRCode\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-6\"> <label>Catatan Penerimaan</label> <textarea name=\"\" id=\"\" style=\"width: 100%; height: 100px\" ng-model=\"detailGRSTNK.Note\" ng-disabled=\"detailGRSTNK.Note\" class=\"form-control\"></textarea> </div> </div> <div class=\"col-md-12\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridLihatGRSTNK\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> </div> <div class=\"ui modal modalKonfirmasi\" role=\"dialog\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Good Receipt STNK</h4> <button class=\"btn rbtn\" style=\"float: right;margin-top: -28px\" ng-click=\"simpanGRSTNK()\">Simpan</button> <button class=\"btn wbtn\" style=\"float: right;margin-top: -28px\" ng-click=\"keluarModal()\">Keluar</button> </div> <div class=\"modal-body\"> <div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 10px; margin-bottom: 10px; margin-right: 10px\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>No. GR</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"No. Good Receipt Terisi Otomatis\" disabled ng-model=\"fieldInput.STNKGRCode\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <select class=\"form-control\" disabled> <option selected>Terima STNK Dari Biro Jasa</option> <option>2</option> <option>3</option> </select> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tipe PO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" disabled value=\"STNK\"> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Nomor Surat Jalan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" disabled ng-model=\"fieldInput.SuratJalanNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-8\"> <label>Catatan Penerimaan</label> <textarea name=\"\" id=\"\" style=\"width: 100%; height: 100px\" disabled ng-model=\"fieldInput.Note\" class=\"form-control\"></textarea> </div> </div> <div class=\"col-md-12\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridSelectedGRSTNK\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> <br> </div> </div> </div> </div> <div class=\"ui modal modalPopUpBatal text-center\" role=\"dialog\"> <div class=\"modal-header\"> <h4 class=\"modal-title\">Konfirmasi</h4> </div> <br> <br> <div class=\"modal-body\"> <h1>Apakah Anda yakin ingin membatalkan GR ini?</h1> </div> <div class=\"modal-footer\"> <div class=\"row\"> <button ng-click=\"BatalGoodReceiptSTNK()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"keluarModalKonfirmasiBatal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/maintainAFI/maintainAfi.html',
    "<style type=\"text/css\">.ScrollStyle {\r" +
    "\n" +
    "        max-height: 400px;\r" +
    "\n" +
    "        overflow-y: scroll;\r" +
    "\n" +
    "        overflow-x: hidden;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .invalidBox {\r" +
    "\n" +
    "        border-color: rgb(185, 74, 72) !important;\r" +
    "\n" +
    "        margin-bottom: 5px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .switch {\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "        width: 60px;\r" +
    "\n" +
    "        height: 34px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .switch input {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* .row {\r" +
    "\n" +
    "        margin: 0 0 0 0;\r" +
    "\n" +
    "    } */\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .noppading {\r" +
    "\n" +
    "        padding: 0 0 0 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .noppading-left {\r" +
    "\n" +
    "        padding-left: 0px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .slider {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        top: 0;\r" +
    "\n" +
    "        left: 0;\r" +
    "\n" +
    "        right: 0;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "        background-color: #ccc;\r" +
    "\n" +
    "        -webkit-transition: .4s;\r" +
    "\n" +
    "        transition: .4s;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .slider:before {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        content: \"\";\r" +
    "\n" +
    "        height: 26px;\r" +
    "\n" +
    "        width: 26px;\r" +
    "\n" +
    "        left: 4px;\r" +
    "\n" +
    "        bottom: 4px;\r" +
    "\n" +
    "        background-color: white;\r" +
    "\n" +
    "        -webkit-transition: .4s;\r" +
    "\n" +
    "        transition: .4s;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    input:checked+.slider {\r" +
    "\n" +
    "        background-color: #2196F3;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    input:focus+.slider {\r" +
    "\n" +
    "        box-shadow: 0 0 1px #2196F3;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    input:checked+.slider:before {\r" +
    "\n" +
    "        -webkit-transform: translateX(26px);\r" +
    "\n" +
    "        -ms-transform: translateX(26px);\r" +
    "\n" +
    "        transform: translateX(26px);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* Rounded sliders */\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .slider.round {\r" +
    "\n" +
    "        border-radius: 34px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .slider.round:before {\r" +
    "\n" +
    "        border-radius: 50%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .button {\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "        padding: 15px 15px 15px 15px;\r" +
    "\n" +
    "        font-size: 14px;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        text-align: left;\r" +
    "\n" +
    "        text-decoration: none;\r" +
    "\n" +
    "        outline: none;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "        background-color: #fff;\r" +
    "\n" +
    "        border: none;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .button:hover {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "        color: #fff;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .button:active {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "        transform: translateY(4px);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /*th,\r" +
    "\n" +
    "    td {\r" +
    "\n" +
    "        padding-bottom: 10px;\r" +
    "\n" +
    "        font-family: \"Courier New\", Courier, monospace;\r" +
    "\n" +
    "    }*/</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <!-- <link rel=\"stylesheet\" href=\"css/uistyle.css\"> --> <!-- <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> --> <div ng-show=\"listContainer\" class=\"row\" style=\"margin: 0 0 0 0\"> <!--  --> <!-- <button id=\"sendEmailBtn\" ng-if=\"user.RoleName != 'CAO/HO'\" ng-disabled=\"selectedData.length == 0 || selectedData[0].StatusName != 'Belum Aju AFI' || selectedData[0].StatusName != 'Approve Batal By CAO' || selectedData[0].TombolAju == false\"\r" +
    "\n" +
    "        class=\"rbtn btn ng-binding ladda-button\" ng-click=\"tambahAFi()\" style=\"float: right; margin: -30px 0 0 7px\">&nbsp\r" +
    "\n" +
    "        <span>Tambah Aju AFI</span>\r" +
    "\n" +
    "    </button> --> <button id=\"sendEmailBtn\" ng-if=\"user.RoleName != 'CAO/HO'\" ng-disabled=\"selectedData.length == 0 || selectedData[0].TombolAju == false\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"tambahAFi()\" style=\"float: right; margin: -30px 0 0 7px\">&nbsp <span>Tambah Aju AFI</span> </button> <!-- <button id=\"sendEmailBtn\" ng-if=\"user.RoleName == 'CAO/HO'\" ng-disabled=\"selectedData.length == 0 || selectedData[0].StatusName == 'Aju AFI Ke TAM' || isHAveAjuAfiTAM\"\r" +
    "\n" +
    "        class=\"rbtn btn ng-binding ladda-button\" ng-click=\"ajuafiketam()\" style=\"float: right; margin: -30px 0 0 7px\">&nbsp\r" +
    "\n" +
    "        <span>Aju AFI Ke TAM</span>\r" +
    "\n" +
    "    </button> --> <button id=\"sendEmailBtn\" ng-if=\"user.RoleName == 'CAO/HO'\" ng-disabled=\"selectedData.length == 0 || selectedData[0].StatusName == 'Aju AFI Ke TAM' || selectedData[0].TombolAju == false\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"ajuafiketam()\" style=\"float: right; margin: -30px 0 0 7px\">&nbsp <span>Aju AFI Ke TAM</span> </button> <!--<button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"tambahAFi()\" style=\"float: right; margin: -30px 0 0 7px\">&nbsp<span>Tambah Aju AFI</span></button>--> <bsform-grid ng-form=\"MaintainAfiForm\" factory-name=\"MaintainAfiFactory\" model=\"mMaintainAfi\" model-id=\"MaintainAfiId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" loading=\"loading\" show-advsearch=\"on\" form-name=\"MaintainAfiForm\" form-title=\"Aju AFI\" modal-title=\"MaintainAfi\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status</label> <bsselect name=\"filterSales\" ng-model=\"filter.StatusAFIId\" data=\"statusafi\" item-text=\"StatusName\" item-value=\"StatusAFIId\" placeholder=\"Select\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" alpha-numeric class=\"form-control\" name=\"model\" placeholder=\"Nomor Rangka\" ng-model=\"filter.FrameNo\" maxlength=\"20\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>No SO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" alpha-numeric mask=\"SIUP\" class=\"form-control\" name=\"model\" placeholder=\"Nomor SO\" ng-model=\"filter.SONo\" maxlength=\"20\"> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" mask=\"Name\" class=\"form-control\" name=\"model\" placeholder=\"Nama Pelanggan\" ng-model=\"filter.CustomerName\" maxlength=\"250\"> </div> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"formAfi\" class=\"row\" style=\"margin: 0 0 0 0\"> <!-- <button ng-click=\"testdebug()\">test Debug</button> --> <bsbreadcrumb-custom action-label=\"breadcrums.title\" tab=\"tab\"></bsbreadcrumb-custom> <!-- <div class=\"row\"> --> <div class=\"row\" style=\"margin: -45px 10px 10px 0\"> <!--<a class=\"btn rbtn ng-binding ladda-button\">\r" +
    "\n" +
    "                        Simpan</a>--> <bs-print ng-if=\"viewAfi.TombolCetak == true\" cetakan-url=\"{{urlCetakan}}\"> <bsbutton-print> <button class=\"rbtn btn\" style=\"float:right\"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i>&nbsp&nbspCetak PPFA </button> </bsbutton-print> </bs-print> <!-- <button ng-show=\"actionAfi == 'lihatAfi'\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">&nbsp<span>Cetak PPFA</span></button> --> <button ng-if=\"actionAfi != 'lihatAfi'\" ng-disabled=\"validasiafi.$invalid || validasiKomen.$invalid || (mMaintainAfi.CustomerTypeId > 3 && mMaintainAfi.SIUPNo=='') || (mMaintainAfi.CustomerTypeId > 3 && mMaintainAfi.SIUPNo==null) || (mMaintainAfi.CustomerTypeId > 3 && mMaintainAfi.SIUPNo==undefined)\" ng-click=\"tambahAjuAfi()\" class=\"rbtn btn\" style=\"float:right\">&nbsp <span>Simpan</span> </button> <button id=\"saveBtn\" ng-click=\"cancle()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Kembali</button> </div> <!-- </div> --> <fieldset ng-disabled=\"true\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <br> <!-- Baris 1 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <!-- Kiri --> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>NO SPK</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mMaintainAfi.FormSPKNo\" class=\"form-control\" style=\"float: left\"> </div> <!-- Kanan --> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nomor Billing</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mMaintainAfi.BillingCode\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 2 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nomor SO</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mMaintainAfi.SoCode\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Tanggal Billing</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsdatepicker name=\"tanggal\" ng-model=\"mMaintainAfi.BillingDate\" date-options=\"dateOptions\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </p> <!-- Baris 3 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nama Pelanggan</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mMaintainAfi.NamaPelanggan\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nomor Rangka</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mMaintainAfi.FrameNo\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 4 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Alamat</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mMaintainAfi.AlamatPelanggan\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Nomor Mesin</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mMaintainAfi.EngineNo\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 5 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Model</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mMaintainAfi.VehicleModelName\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Tahun Kendaraan</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mMaintainAfi.AssemblyYear\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 6 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Tipe</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text\" ng-model=\"mMaintainAfi.Description\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>CBU/CKD</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mMaintainAfi.AssemblyType\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> <!-- Baris 7 --> <p> <div class=\"row\" style=\"margin-left: 0; margin-right: 0\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>Warna</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mMaintainAfi.ColorName\" class=\"form-control\" style=\"float: left\"> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading\"> <label>On/Off the Road</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <input type=\"text \" ng-model=\"mMaintainAfi.OnOffTheRoadName\" class=\"form-control\" style=\"float: left\"> </div> </div> </p> </div> </fieldset> <!-- Navigation Tab --> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px; background:linear-gradient(transparent 60%, rgba(0,0,0,0.03))\"> <li class=\"active\"> <a style=\"padding: 9px 7px 10px 7px\" data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainerAfi','infoPemilikAfi')\"> <i class=\"fa fa-user\"></i>&nbsp Info Pemilik</a> </li> <li> <a style=\"padding: 9px 7px 10px 7px\" data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainerAfi','dokumenAfi')\" ng-click=\"loadImage()\"> <i class=\"fa fa-folder-open\"></i>&nbsp Dokumen</a> </li> </ul> <div id=\"tabContainerAfi\" class=\"tab-content\"> <div id=\"infoPemilikAfi\" class=\"tab-pane fade in active\" style=\"padding:10px 0 0 0;margin-left: 15px;margin-right: 10px\"> <!-- <div id=\"infoPerusahaan\" class=\"row\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin: 10px 0 0 0\"> --> <!-- <p style=\"width:120px; margin-top:-20px; margin-left:14px; background:white;\"><b>Informasi Pemilik</b></p> --> <div id=\"customerDetail\" style=\"margin: 0 0 0 0\"> <fieldset ng-disabled=\"actionAfi == 'lihatAfi' || actionAfi == 'batalAfi'\"> <form name=\"validasiafi\"> <!-- Baris 1 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Nama</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-user\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A'\" alpha-numeric mask=\"Name\" maxlength=\"50\" name=\"pemilikafi\" ng-model=\"mMaintainAfi.NamaPemilik\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.pemilikafi.$touched == true) && (validasiafi.pemilikafi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.pemilikafi.$touched && validasiafi.pemilikafi.$invalid) || (validasiafi.pemilikafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 2 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>No Handphone</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-mobile\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' \r" +
    "\n" +
    "                                                                        || CodeRevisiAfi == 'REV.B'\r" +
    "\n" +
    "                                                                        || CodeRevisiAfi == 'REV.C' \r" +
    "\n" +
    "                                                                        || CodeRevisiAfi == 'REV.D' \r" +
    "\n" +
    "                                                                        || CodeRevisiAfi == 'REV.E' \r" +
    "\n" +
    "                                                                        || CodeRevisiAfi == 'REV.F'\" maxlength=\"15\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"nohpafi\" ng-model=\"mMaintainAfi.HpNumber\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.nohpafi.$touched == true) && (validasiafi.nohpafi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>Email</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-envelope\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB' || CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD'|| CodeRevisiAfi == 'REVE'|| CodeRevisiAfi == 'REVF'\" maxlength=\"50\" ng-model=\"mMaintainAfi.Email\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.nohpafi.$touched && validasiafi.nohpafi.$invalid) || (validasiafi.nohpafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                            <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.pemilikafi.$touched && validasiafi.pemilikafi.$invalid\" aria-live=\"assertive\">This field is required.</i>\r" +
    "\n" +
    "                                            </div> --> </div> </p> <!-- Baris 3 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>No Telpon</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'|| CodeRevisiAfi == 'REV.D' || CodeRevisiAfi == 'REV.E' || CodeRevisiAfi == 'REV.F'\" maxlength=\"15\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"mMaintainAfi.PhoneNumber\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> <div ng-if=\"mMaintainAfi.CustomerTypeId == 3\" class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>KTP/KITAS</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div ng-if=\"mMaintainAfi.CustomerTypeId == 3\" class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-address-card\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"ktpafi\" ng-model=\"mMaintainAfi.KTPNo\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.ktpafi.$touched == true) && (validasiafi.ktpafi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> <div ng-if=\"mMaintainAfi.CustomerTypeId > 3\" class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>SIUP/KTP</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div ng-if=\"mMaintainAfi.CustomerTypeId > 3\" class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-address-card\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'\" maxlength=\"20\" alpha-numeric mask=\"SIUP\" name=\"tdpsiup\" ng-model=\"mMaintainAfi.SIUPNo\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <!-- <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.nohpafi.$touched && validasiafi.nohpafi.$invalid\" aria-live=\"assertive\">This field is required.</i> --> </div> <div class=\"col-md-2\"> </div> <div ng-if=\"mMaintainAfi.CustomerTypeId == 3\" class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.ktpafi.$touched && validasiafi.ktpafi.$invalid) || (validasiafi.ktpafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <!-- <div ng-if=\"mMaintainAfi.CustomerTypeId > 3\" class=\"col-md-4\">\r" +
    "\n" +
    "                                    <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.tdpsiup.$touched && validasiafi.tdpsiup.$invalid\" aria-live=\"assertive\">This field is required.</i>\r" +
    "\n" +
    "                                </div> --> </div> </p> <!-- Baris 4 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>NPWP</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'\" name=\"npwpafi\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"mMaintainAfi.NPWPNo\" ng-class=\"{invalidBox : (validasiafi.npwpafi.$touched == true) && (validasiafi.npwpafi.$invalid == true)}\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> <div ng-if=\"mMaintainAfi.CustomerTypeId > 3\" class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>TDP</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div ng-if=\"mMaintainAfi.CustomerTypeId > 3\" class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-address-card\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"tdpsiup\" ng-model=\"mMaintainAfi.TDPNo\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.npwpafi.$touched && validasiafi.npwpafi.$invalid) || (validasiafi.npwpafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 5 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alamat 1</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-home\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'\" maxlength=\"30\" name=\"alamat1afi\" ng-model=\"mMaintainAfi.AlamatPemilik\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.alamat1afi.$touched == true) && (validasiafi.alamat1afi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.alamat1afi.$touched && validasiafi.alamat1afi.$invalid) || (validasiafi.alamat1afi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 6 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alamat 2</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-home\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'\" maxlength=\"30\" name=\"alamat2afi\" ng-model=\"mMaintainAfi.DistrictName\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.alamat2afi.$touched == true) && (validasiafi.alamat2afi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.alamat2afi.$touched && validasiafi.alamat2afi.$invalid) || (validasiafi.alamat2afi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 7 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alamat 3</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-home\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'\" maxlength=\"30\" name=\"alamat3afi\" ng-model=\"mMaintainAfi.VillageName\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.alamat3afi.$touched == true) && (validasiafi.alamat3afi.$invalid == true)}\" style=\"float: left\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-10\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.alamat3afi.$touched && validasiafi.alamat3afi.$invalid) || (validasiafi.alamat3afi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 8 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Provinsi</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"provinsiafi\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'\" placeholder=\"Provinsi\" ng-model=\"mMaintainAfi.ProvinceId\" data=\"province\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" on-select=\"filterKabupaten(mMaintainAfi.ProvinceId)\" icon=\"fa fa-map-marker\" required> </bsselect> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Kode Pos</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"kodeposafi\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'\" placeholder=\"Kode Pos\" ng-model=\"mMaintainAfi.AFIRegionId\" data=\"regioncode\" item-text=\"PostalCode\" item-value=\"AFIRegionId\" icon=\"fa fa-map-marker\" required> </bsselect> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.provinsiafi.$touched && validasiafi.provinsiafi.$invalid) || (validasiafi.provinsiafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.regioncodeafi.$touched && validasiafi.regioncodeafi.$invalid) || (validasiafi.regioncodeafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 9 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\" style=\"padding-right: 0px\"> <bsreqlabel>Kota/Kabupaten</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"kabkotaafi\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'\" placeholder=\"Kabupaten/Kota\" ng-disabled=\"mMaintainAfi.ProvinceId == null || mMaintainAfi.ProvinceId == undefined\" ng-change=\"filterKecamatan(mMaintainAfi.CityRegencyId)\" ng-model=\"mMaintainAfi.CityRegencyId\" data=\"kabupaten\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" required icon=\"fa fa-map-marker \"> </bsselect> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Region Code AFI</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"regioncodeafi\" ng-disabled=\"CodeRevisiAfi == 'REVA' || CodeRevisiAfi == 'REVB'|| CodeRevisiAfi == 'REVC'|| CodeRevisiAfi == 'REVD' || CodeRevisiAfi == 'REVE'|| CodeRevisiAfi == 'REVF'\" placeholder=\"Region Code\" ng-model=\"mMaintainAfi.AFIRegionId\" data=\"regioncode\" item-text=\"AFIRegionCode,Name,Kota\" item-value=\"AFIRegionId\" required icon=\"fa fa-map-marker\"> </bsselect> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.kabkotaafi.$touched && validasiafi.kabkotaafi.$invalid) || (validasiafi.kabkotaafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.regioncodeafi.$touched && validasiafi.regioncodeafi.$invalid) || (validasiafi.regioncodeafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 10 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\" style=\"padding-right: 0px\"> <bsreqlabel>Jenis</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"cartypeafi\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'|| CodeRevisiAfi == 'REV.D' || CodeRevisiAfi == 'REV.E'\" placeholder=\"Jenis\" ng-model=\"mMaintainAfi.AFICarTypeCode\" data=\"cartype\" item-text=\"Jenis,Model,AFICarTypeCode\" item-value=\"AFICarTypeCode\" required icon=\"fa fa-car \"> </bsselect> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Model</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsselect name=\"cartypeafi\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'|| CodeRevisiAfi == 'REV.D' || CodeRevisiAfi == 'REV.E'\" placeholder=\"Model\" ng-model=\"mMaintainAfi.AFICarTypeCode\" data=\"cartype\" item-text=\"Model,Jenis,AFICarTypeCode\" item-value=\"AFICarTypeCode\" placeholder=\"\" required icon=\"fa fa-car\"> </bsselect> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.cartypeafi.$touched && validasiafi.cartypeafi.$invalid) || (validasiafi.cartypeafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.cartypeafi.$touched && validasiafi.cartypeafi.$invalid) || (validasiafi.cartypeafi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> </div> </p> <!-- Baris 11 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\" style=\"padding-right: 0px\"> <bsreqlabel>Tanggal Efektif Faktur</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <bsdatepicker name=\"tanggal\" name=\"efectiveDateAfi\" ng-disabled=\"kecuali\" ng-model=\"mMaintainAfi.InvoiceStartEffectiveDate\" date-options=\"dateOptionsfaktur\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>Warna Unit</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa-mobile\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'|| CodeRevisiAfi == 'REV.D'\" maxlength=\"50\" ng-model=\"mMaintainAfi.CustomColorName\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.nohpafi.$touched == true) && (validasiafi.nohpafi.$invalid == true)}\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <i style=\"color: rgb(185, 74, 72)\" ng-show=\"(validasiafi.efectiveDateAfi.$touched && validasiafi.efectiveDateAfi.$invalid) || (validasiafi.efectiveDateAfi.$untouched && isValidate == true)\" aria-live=\"assertive\">This field is required.</i> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                            <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.cartypeafi.$touched && validasiafi.cartypeafi.$invalid\" aria-live=\"assertive\">This field is required.</i>\r" +
    "\n" +
    "                                            </div> --> </div> </p> <!-- Baris 12 --> <p> <div class=\"row\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\" style=\"padding-right: 0px\"> <label>Permintaan Plat Nomor Khusus</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa fa\" style=\"top:8px\"></i> <input type=\"text\" ng-disabled=\"CodeRevisiAfi == 'REV.A' || CodeRevisiAfi == 'REV.B'|| CodeRevisiAfi == 'REV.C'|| CodeRevisiAfi == 'REV.D' || CodeRevisiAfi == 'REV.E' || CodeRevisiAfi == 'REV.F'\" maxlength=\"50\" ng-model=\"mMaintainAfi.SpecialReqNumberPlate\" class=\"form-control\" ng-class=\"{invalidBox : (validasiafi.nohpafi.$touched == true) && (validasiafi.nohpafi.$invalid == true)}\" style=\"float: left\"> </div> </div> </div> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <label>Status AFI</label> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group form-input\"> <div class=\"input-icon-right\"> <i class=\"fa\" style=\"top:8px\"></i> <input type=\"text\" maxlength=\"50\" ng-disabled=\"true\" ng-model=\"mMaintainAfi.StatusName\" class=\"form-control\" style=\"float: left\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-2\"> </div> <div class=\"col-md-4\"> <!-- <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.efectiveDateAfi.$touched && validasiafi.efectiveDateAfi.$invalid\" aria-live=\"assertive\">This field is required.</i> --> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                    <i style=\"color: rgb(185, 74, 72)\" ng-show=\"validasiafi.cartypeafi.$touched && validasiafi.cartypeafi.$invalid\" aria-live=\"assertive\">This field is required.</i>\r" +
    "\n" +
    "                                                </div> --> </div> </p> </form> </fieldset> </div> <form name=\"validasiKomen\"> <p> <div class=\"row\" ng-if=\"actionAfi == 'lihatAfi' || actionAfi == 'revisiAfi' || actionAfi == 'batalAfi'\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alasan Revisi / Batal AFI</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <textarea rows=\"5\" maxlength=\"200\" name=\"AFIRevisionCancelReason\" ng-disabled=\"actionAfi == 'lihatAfi'\" class=\"form-control\" ng-model=\"mMaintainAfi.AFIRevisionCancelReason\" placeholder=\"Alasan\" required>\r" +
    "\n" +
    "                            </textarea> </div> </div> </p> <p> <div class=\"row\" ng-if=\"actionAfi == 'lihatAfi'\"> <div class=\"col-md-2\" style=\"padding-top: 8px\"> <div class=\"row\"> <div class=\"col-md-9 noppading-left\"> <bsreqlabel>Alasan Reject dari ADH/CAO</bsreqlabel> </div> <div class=\"col-md-3\"> : </div> </div> </div> <div class=\"col-md-10\"> <textarea rows=\"5\" maxlength=\"200\" name=\"ADHCAORejectReason\" ng-disabled=\"actionAfi == 'lihatAfi'\" class=\"form-control\" ng-model=\"mMaintainAfi.ADHCAORejectReason\" placeholder=\"Alasan\" required>\r" +
    "\n" +
    "                            </textarea> </div> </div> </p> </form> <!-- </div> --> </div> <div id=\"dokumenAfi\" class=\"tab-pane fade\" style=\"padding:10px 0 0 0\"> <div class=\"row\" style=\"margin: 0 0 0 0; min-height: 400px;padding-top: 15px\"> <div class=\"row\" style=\"margin: 0 0 0 0\" ng-repeat=\"listTempDoc in mMaintainAfi.listDokumen\"> <!-- <p> --> <div class=\"row\" style=\"margin: 0 0 0 0\"> <!-- <p> --> <div class=\"row\" style=\"margin: 0 0 10px 0;padding-left: 10px\"> <!-- <div --> <div class=\"col-md-1\" ng-hide=\"actionAfi == 'lihatAfi' || actionAfi == 'batalAfi'\" ng-click=\"removeExtension($index)\" style=\"color:red;padding-top: 10px ; float: left; width: 20px !important\"> <i class=\"fa fa-lg fa-times\" aria-hidden=\"true \" style=\"float:right\"></i> </div> <div class=\"col-md-4\" style=\"margin-top: 8px\"> <div style=\"float: left\"> <label> <a> <u ng-click=\"viewDocumentAFI(listTempDoc)\">{{listTempDoc.DocumentName}}</u> </a> </label> </div> <div style=\"float: right\">:&nbsp&nbsp</div> </div> <div class=\"col-md-6\" style=\"padding-right: 0px !important\"> <div class=\"input-icon-right\"> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"listTempDoc.UploadDokumenCustomer.FileName\" ng-change=\"cekStatusUpload(listTempDoc, listTempDoc.UploadDokumenCustomer.FileName)\" name=\"fieldNoSpkEnd\" placeholder=\"\" ng-maxlength=\"50\" required style=\"padding-right:0px !important\"> <!-- <input type=\"text\" style=\"display: none\" ng-model=\"listTempDoc.DocumentDataName = listTempDoc.UploadDokumenCustomer.FileName\"> --> </div> </div> <div ng-hide=\"actionAfi == 'lihatAfi' || actionAfi == 'batalAfi'\" class=\"col-md-1\" style=\"padding-right: 0px;padding-left: 0px !important\"> <label class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;width: 100% !important\"> <i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i> <input type=\"file\" loading=\"loadingImgAFI\" id=\"uploadAFI{{$index}}\" accepted=\"acceptvalue\" accept=\"acceptvalue\" formattype=\"['jpg','png','img']\" bs-uploadsingle=\"onFileSelect($files)\" img-upload=\"listTempDoc.UploadDokumenCustomer\" style=\"display:none\"> </label> </div> </div> <!-- </p> --> </div> <!-- </p> --> </div> <p> <div class=\"row\" ng-hide=\"actionAfi == 'lihatAfi' || actionAfi == 'batalAfi'\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-11\" style=\"border-bottom: 1px solid #ccc; min-height: 20px\"> </div> <div class=\"col-md-1\"> <div class=\"row\" style=\"margin-left: 15px; margin-right: 14px; margin-top: 10px\"> <div style=\"float: right;color: #d53337\" ng-click=\"openDokumenlist()\"> <i class=\"fa fa-plus-circle\" aria-hidden=\"true\" style=\"font-size: 1.5em\"></i> </div> </div> </div> </div> </p> </div> </div> </div> </div> <div class=\"ui modal dokumentListAFI\" style=\"background-color: transparent ;box-shadow :none !important\"> <div style=\"margin: auto; width: 600px\"> <div class=\"row\" style=\"margin: 0 0 0 0; background-color: white; border-radius:3px; box-shadow :1px 3px 3px 0 rgba(0,0,0,.2), 1px 3px 15px 2px rgba(0,0,0,.2) !important\"> <button class=\"btn btn-default\" type=\"button\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; ; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Dokumen </button> <div class=\"list-group ScrollStyle\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom: 10px !important\"> <div class=\"list-group-item\" ng-repeat=\"item in dokumentlist\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleCheckedAfiDoc(item);\"> <div class=\"row\"> <div class=\"col-md-12\"> <h7 style=\"float: left\">{{item.DocumentType}}</h7> <span style=\"float:right\"> <i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.checked == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i> </span> </div> <div class=\"col-md-12\" style=\"font-size:10px\">{{item.DocumentName}}</div> </div> </div> </div> <div class=\"listData\" id=\"addMode\" style=\"float:right; margin-bottom: 0px !important;padding-top: 0px!important;padding-bottom: 8px!important;padding-right: 8px!important\"> <button id=\"addBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"ApproveData()\">Pilih </button> <button id=\"addBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"RejectData()\">Kembali </button> </div> </div> </div> </div> <div class=\"ui modal loadingImgAFI\" style=\"height:200px; background-color:transparent; box-shadow: none !important\"> <div align=\"center\" class=\"list-group-item\" style=\"border: none !important;padding:10px 0px 10px 0px; background-color:transparent\"> <font color=\"white\"><i class=\"fa fa-spinner fa-spin fa-3x fa-lg\"></i> <b> Upload Image.... </b> </font> </div> </div> <div class=\"ui modal revisionAFI\" style=\"background-color: transparent ;box-shadow :none !important\"> <div style=\"margin: auto; width: 600px\"> <div class=\"row\" style=\"margin: 0 0 0 0; background-color: white; border-radius:3px; box-shadow :1px 3px 3px 0 rgba(0,0,0,.2), 1px 3px 15px 2px rgba(0,0,0,.2) !important\"> <button class=\"btn btn-default\" type=\"button\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; ; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Kategori Revisi </button> <div class=\"list-group ScrollStyle\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom: 10px !important\"> <div class=\"list-group-item\" ng-repeat=\"item in afirevision\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleAFICatChecked(item);\"> <div class=\"row\"> <div class=\"col-md-12\"> <h7 style=\"float: left\">{{item.RevisionAFICode}}</h7> <span style=\"float:right\"> <i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': ((item.RevisionAFICode == CategoryAfi[0].RevisionAFICode) && (item.RevisionAFIId == CategoryAfi[0].RevisionAFIId) ) ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i> </span> </div> <div class=\"col-md-12\" style=\"font-size:10px\">{{item.RevisionAFIName}}</div> </div> </div> </div> <div class=\"listData\" id=\"addMode\" style=\"float:right; margin-bottom: 0px !important;padding-top: 0px!important;padding-bottom: 8px!important;padding-right: 8px!important\"> <button id=\"addBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"PilihKategori()\">Pilih </button> <button id=\"addBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"BatalKategori()\">Kembali </button> </div> </div> </div> </div> <div class=\"ui modal dokumenAfiLihat\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4> <b>{{dokumenName}}</b> </h4> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> </div> <div class=\"modal-body\"> <!--<img ng-src=\"{{viewdocument}}\" image-set-aspect ng-class=\"{'wide': viewdocument.width/viewdocument.height > 1, 'tall': viewdocument.width/viewdocument.height <= 1}\">--> <p align=\"center\"> <img ng-src=\"{{dokumenData}}\" style=\"max-width: 860px; max-height: 526px; text-align: center\"> </p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <!-- <button ng-click=\"btnkehilanganSpk()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width:15%;\">Simpan</button> --> <button ng-click=\"KeluarDokumenAfi()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Keluar</button> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/maintainPOBirojasa/maintainPOBirojasa.html',
    "<script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">#MaintainPoBiroJasaModalDaftarNoRangkaGaDulu {\r" +
    "\n" +
    "        height: 625px !important;\r" +
    "\n" +
    "    }</style> <!--.modal{\r" +
    "\n" +
    "top: 0px !important;\r" +
    "\n" +
    "}--> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"daftarPO\" class=\"row\" style=\"margin: 0 0 0 0\"> <button id=\"PengembalianSpk\" ng-click=\"buatPO()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px\" ng-disabled=\"btnPengembalianSpk\">Tambah</button> <bsform-grid ng-form=\"MaintainPOBirojasaForm\" loading=\"loading\" show-advsearch=\"on\" factory-name=\"MaintainPOBirojasaFactory\" model=\"mMaintainPOBirojasa\" model-id=\"MaintainPOBirojasaId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" form-name=\"MaintainPOBirojasaForm\" form-title=\"PO Biro Jasa\" modal-title=\"MaintainPOBirojasa\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Cabang</label> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.OutletId\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Cabang\" icon=\"fa fa-sitemap\"></bsselect> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. Rangka\" ng-model=\"filter.FrameNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nama Pelanggan\" ng-model=\"filter.CustomerName\" ng-maxlength=\"50\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>No. PO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. PO\" ng-model=\"filter.POBiroJasaUnitNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status</label> <bsselect name=\"filterSales\" ng-model=\"filter.POJasaStatusId\" data=\"status\" item-text=\"POJasaStatusName\" item-value=\"POJasaStatusId\" placeholder=\"Select\"> </bsselect> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div class=\"row\" style=\"margin: 0 0 10px 0\" ng-show=\"detailBirojasa\"> <div class=\"row\" style=\"margin: -30px 0 20px 0 !important\"> <button type=\"button\" class=\"rbtn btn\" ng-disabled=\"(mMaintainPOBirojasa.TombolKirim == false ? true:false)\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"kirim()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-paper-plane\"></i>&nbsp;Kirim</span><span class=\"ladda-spinner\"></span> </button> <bs-print cetakan-url=\"{{printPO}}\"> <bsbutton-print> <button class=\"rbtn btn\" style=\"float:right\" ng-show=\"rightStatus != 'Batal PO'\" ng-disabled=\"(mMaintainPOBirojasa.TombolCetak == false ? true:false)\"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i>&nbsp&nbspCetak PO </button> </bsbutton-print> </bs-print> <button type=\"button\" class=\"rbtn btn\" ng-show=\"rightStatus != 'Batal PO'\" ng-disabled=\"(mMaintainPOBirojasa.TombolCetak == false ? true:false)\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"MaintainPoBiroJasaDownloadFile()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Download File</span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"rbtn btn\" ng-disabled=\"(mMaintainPOBirojasa.TombolUbah == false ? true:false)\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"ubah('ubah')\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Ubah</span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"rbtn btn\" ng-disabled=\"(mMaintainPOBirojasa.TombolBatal == false ? true:false)\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"batal()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Batal PO</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"awal()\" class=\"wbtn btn\" style=\"float: right\">Kembali</button> </div> <!--<div class=\"row\" style=\"margin-bottom:10px;margin-left:0px\">\r" +
    "\n" +
    "        <label class=\"col-md-2\">Nomor PO </label>\r" +
    "\n" +
    "        <label class=\"col-md-10\">: {{mMaintainPOBirojasa.POBiroJasaUnitNo}}</label>        \r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"row\" style=\"margin-bottom:10px;margin-left:0px\">\r" +
    "\n" +
    "        <label class=\"col-md-2\">Vendor Biro Jasa </label>\r" +
    "\n" +
    "        <label class=\"col-md-10\">: {{mMaintainPOBirojasa.ServiceBureauName}}</label>\r" +
    "\n" +
    "\t</div>--> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No PO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control ng-pristine ng-untouched ng-valid ng-not-empty\" name=\"NoPO\" ng-model=\"mMaintainPOBirojasa.POBiroJasaUnitNo\" ng-disabled=\"true\" disabled> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Vendor Biro Jasa</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control ng-pristine ng-untouched ng-valid ng-not-empty\" name=\"VendorBiroJasa\" ng-model=\"mMaintainPOBirojasa.ServiceBureauName\" ng-disabled=\"true\" disabled> </div> </div> </div> </div> <div class=\"grid\" ui-grid=\"gridDetailNoRangka\" ui-grid-auto-resize></div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <p align=\"center\"> <!-- <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"cetak()\" ng-disabled=\"selctedRow == null || selctedRow == undefined || selctedRow.length == 0\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\">\r" +
    "\n" +
    "                <span class=\"ladda-label\"><i class=\"fa fa-fw\"></i>&nbsp;Cetak PO</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "            </button> --> </p> </div> </div> <div class=\"ui modal pilihNoRangkaBiroJasa\"> <div> <div class=\"modal-header\"> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\"><i class=\"fa fa-list\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Pilih No Rangka</h4> </div> <div class=\"modal-body\"> <!-- <div class=\"row filtersearch\" style=\"margin-right:0px;padding-bottom:5px;\">\r" +
    "\n" +
    "                <div class=\"col-md-12\" style=\"padding-right:0px;\">\r" +
    "\n" +
    "                    <div class=\"input-group col-md-4\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control ng-pristine ng-untouched ng-valid ng-empty\" placeholder=\"Filter\" ng-model=\"gridApi.grid.columns[filterColIdx].filters[0].term\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <div class=\"input-group-btn dropdown\" uib-dropdown=\"\">\r" +
    "\n" +
    "                            <button id=\"filter\" type=\"button\" class=\"btn wbtn ng-binding dropdown-toggle\" uib-dropdown-toggle=\"\" data-toggle=\"dropdown\" tabindex=\"-1\" aria-haspopup=\"true\" aria-expanded=\"false\">CABANG&nbsp;<span class=\"caret\"></span>\r" +
    "\n" +
    "                            </button>\r" +
    "\n" +
    "                            <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu=\"\" role=\"menu\" aria-labelledby=\"filter\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <li role=\"menuitem\" ng-repeat=\"col in gridCols\" class=\"ng-scope\">\r" +
    "\n" +
    "                                    <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">CABANG<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter\" ng-show=\"gridApi.grid.columns[1].filters[0].term!='' &amp;&amp;gridApi.grid.columns[1].filters[0].term!=undefined\"></i></span></a>\r" +
    "\n" +
    "                                </li>\r" +
    "\n" +
    "                            </ul>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> <div class=\"grid\" ui-grid=\"gridNoRangka\" ui-grid-selection ui-grid-auto-resize></div> </div> <div class=\"modal-footer\" style=\"padding: 10px 10px 10px 10px !important\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"lanjut(selctedRow)\" ng-disabled=\"selctedRow == null || selctedRow == undefined || selctedRow.length == 0\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-arrow-right\"></i>&nbsp;Lanjut</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"keluarFrame()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </p> </div> </div> </div> <div id=\"MaintainPoBiroJasaModalDaftarNoRangka\" class=\"ui modal noRangkaSelectedBiroJasa\" ng-class=\"{'ui modal noRangkaSelectedBiroJasa transition visible active scrolling' : noRangkaSelectedBiroJasaStatus == true, 'ui modal noRangkaSelectedBiroJasa' : noRangkaSelectedBiroJasaStatus == false }\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\"><i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Daftar No Rangka</h4> </div> <div class=\"modal-body\" style=\"padding-bottom: 10px !important\"> <div class=\"row col-md-12\"> <div class=\"row col-md-4\" style=\"padding: 0 0 0 0; margin: 0 0 0 0\"> <bsreqlabel>Biro Jasa</bsreqlabel> <bsselect name=\"birojasa\" ng-change=\"getPriceJasa(mMaintainPOBirojasa.ServiceBureauId)\" data=\"birojasa\" item-text=\"ServiceBureauName, ServiceBureauCode\" ng-model=\"mMaintainPOBirojasa.ServiceBureauId\" item-value=\"ServiceBureauId\" placeholder=\"Pilih Biro Jasa\" required icon=\"fa fa-search \"> </bsselect> <!-- <bsselect name=\"birojasa\" data=\"birojasa\"\r" +
    "\n" +
    "                        item-text=\"ServiceBureauName, ServiceBureauCode\" ng-model=\"mMaintainPOBirojasa.ServiceBureauId\"\r" +
    "\n" +
    "                        item-value=\"ServiceBureauId\" placeholder=\"Pilih Biro Jasa\" required=\"true\" icon=\"fa fa-search \">\r" +
    "\n" +
    "                    </bsselect> --> </div> <div class=\"row col-md-4\" style=\"padding: 0 0 0 0; margin: 0 0 0 0\"> </div> <div class=\"row col-md-4\" style=\"padding: 0 0 0 0; margin: 0 0 0 0\"> <div style=\"font-size:10px;margin-top:40px\"> <p align=\"right\"> NOTE * : Klik 2 kali pada field Harga untuk mengubah harga. </p> </div> </div> </div> <div class=\"row\" style=\"margin: 30px 0 0 0\"> <div class=\"grid\" ui-grid=\"gridNoRangkaDocument\" style=\"margin: 30px 0 0 0; height: 300px\" ui-grid-edit ui-grid-auto-resize></div> </div> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"col-md-4\" style=\"float:right\"> <input type=\"tel\" style=\"text-align: right\" input-currency number-only class=\"form-control\" ng-model=\"TotalPriceJasa\" disabled> </div> <div class=\"col-md-2\" style=\"float:right;margin-top: 10px;text-align: right\"><b>Total Biaya :</b> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button ng-disabled=\"mMaintainPOBirojasa.ServiceBureauId==null || mMaintainPOBirojasa.ServiceBureauId==undefined\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"simpan()\" ng-disabled=\"mMaintainPOBirojasa.ServiceBureauId == null || mMaintainPOBirojasa.ServiceBureauId == undefined\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"back()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </p> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/maintainSuratDistribusi/maintainSuratDistribusi.html',
    "<script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">.context-menu {\r" +
    "\n" +
    "        cursor: context-menu;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"listContainer\" class=\"row\" style=\"margin: 0 0 0 0\"> <button style=\"float:right; margin:-30px 0 0 10px\" ng-click=\"buatSuratDistribusi()\" class=\"btn rbtn ladda-button\">Tambah</button> <bsform-grid ng-form=\"MaintainSuratDistribusiForm\" factory-name=\"MaintainSuratDistribusiFactory\" model=\"mMaintainSuratDistribusi\" model-id=\"MaintainSuratDistribusiId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" show-advsearch=\"on\" loading=\"loading\" form-name=\"MaintainSuratDistribusiForm\" form-title=\"Aju AFI\" modal-title=\"MaintainSuratDistribusi\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Tanggal Pengiriman</label> <bsdatepicker ng-model=\"filter.StartSentDate\" ng-change=\"tanggalmin(filter.StartSentDate)\" date-options=\"dateOptionsFrom\" name=\"tanggalfrom\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label> Sampai Tanggal</label> <bsdatepicker ng-model=\"filter.EndSentDate\" ng-disabled=\"filter.StartSentDate == null\" date-options=\"dateOptionLastDate\" name=\"tanggalto\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. Rangka\" ng-model=\"filter.FrameNo\" maxlength=\"17\" alpha-numeric> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Surat Distribusi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. Surat Distribusi\" ng-model=\"filter.SuratDistribusiNo\" maxlength=\"20\" alpha-numeric mask=\"SIUP\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nama Pelanggan\" ng-model=\"filter.CustomerName\" maxlength=\"50\" alpha-numeric mask=\"Huruf\"> </div> </div> </div> </div> </bsform-advsearch> </bsform-grid> <div class=\"ui modal noRangka\" modal=\"noRangka\" role=\"dialog\"> <div> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" ng-click=\"close()\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Pilih No. Rangka</h4> </div> <div class=\"modal-body\"> </div> <div class=\"modal-footer\"> <p align=\"center\"> </p> </div> </div> </div> <div class=\"ui small modal formDocumenUpload\"> <!-- <button ng-click=\"testing()\">test</button> --> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" class=\"close\" ng-click=\"close()\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Update Dokumen</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"fieldNoSpk\" placeholder=\"\" ng-model=\"rowsDocument.FrameNo\" ng-disabled=\"true\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-6\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"Input Nama Pelanggan\" placeholder=\"\" ng-model=\"rowsDocument.CustomerName\" ng-disabled=\"true\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <hr style=\"margin-top: 0 !important\"> <!--<button ng-click=\"test()\"> test </button>--> <!--Upload KTP Pelanggan--> <div class=\"row\" style=\"overflow-y: scroll; height: 300px\"> <div class=\"row\" style=\"margin: 0 0 10px 0\" ng-repeat=\"listTempDoc in rowsDocument.listDetail track by $index\"> <div class=\"col-md-1\" style=\"margin-top: 8px; padding-right: 0 !important\"> <i ng-show=\"listTempDoc.UploadDokumenCustomer.FileName == undefined\" class=\"fa fa-square-o\" style=\"font-size: 18px\" aria-hidden=\"true\"></i> <i ng-show=\"listTempDoc.UploadDokumenCustomer.FileName != undefined\" class=\"fa fa-check-square\" style=\"font-size: 18px\" aria-hidden=\"true\"></i> </div> <div class=\"col-md-3\" style=\"margin-top: 8px;margin-left: -15px ; margin-right: 15px ; padding: 0 0 0 0 !important\"> <label ng-click=\"viewImage(listTempDoc.DocumentURL)\">{{listTempDoc.DocumentName}}</label> </div> <div class=\"col-md-6\" style=\"padding: 0 0 0 0 !important\"> <div class=\"input-icon-right\"> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"listTempDoc.UploadDokumenCustomer.FileName\" name=\"fieldNoSpkEnd\" placeholder=\"\" ng-maxlength=\"50\" required> <!-- <input type=\"text\" style=\"display: none\" ng-model=\"listTempDoc.DocumentDataName = listTempDoc.UploadDokumenCustomer.FileName\"> --> </div> </div> <div class=\"col-md-2\" style=\"margin-left:-5px; padding: 0 0 0 0 !important\"> <label class=\"rbtn btn ng-binding ladda-button fa fa-times\" style=\"float: right\"> <!-- <input type=\"file\" bs-uploadsingleregis=\"onFileSelect($files)\" img-upload=\"listTempDoc.UploadDokumenCustomer\" style=\"display:none\"> --> <button style=\"display: none\" ng-click=\"removeImage(listTempDoc, $index)\"></button> </label> <label class=\"rbtn btn ng-binding ladda-button fa fa-camera\" style=\"float: right\"> <input type=\"file\" bs-uploadsingle=\"onFileSelect($files)\" img-upload=\"listTempDoc.UploadDokumenCustomer\" style=\"display:none\" formattype=\"['jpg','png','img']\"> </label> </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <div style=\"float: left\"> <input type=\"checkbox\" ng-checked=\"rowsDocument.SuratDistribusiStatusName == 'Lengkap'\" ng-model=\"rowsDocument.SuratDistribusiStatusChecked\"> <!--<i class=\"fa fa-check-square\"  aria-hidden=\"true\"></i>--> <label>&nbspKelengkapan Dokumen Sudah Diperiksa</label> </div> <button id=\"simpan\" ng-click=\"tempSave()\" class=\"rbtn btn\" style=\"float: right\"> <i class=\"fa fa-check\"></i>&nbspSimpan</button> <button id=\"kembali\" ng-click=\"kembali()\" class=\"wbtn btn\" style=\"float: right\"> <i class=\"fa fa-times\"></i>&nbspKembali</button> </p> </div> </div> </div> </div> <div ng-show=\"show.Wizard\" class=\"row\" style=\"margin: 10px 0 0 0; min-height: 100vh\"> <div class=\"row\" style=\"margin: -40px 0 0 0\" ng-if=\"button.FrameTab == true\"> <button id=\"reasignspk\" ng-click=\"lanjut(selctedRow)\" ng-disabled=\"selctedRow.length == 0 && status != 'ubah'\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Lanjut&nbsp&nbsp <span> <i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i> </span> </button> <button id=\"reasignspk\" ng-click=\"Batal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\"> <span> <i class=\"fa fa-times\" aria-hidden=\"true\"></i> </span>&nbsp&nbsp Batal</button> </div> <div class=\"row\" style=\"margin: -40px 0 0 0\" ng-if=\"button.DokumenTab == true\"> <button id=\"\" ng-disabled=\"gridNoRangkaDocumen.data.length == 0\" ng-click=\"simpandistribusi()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span> <i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i> </span>&nbsp&nbspSimpan</button> <!-- <button id=\"\" ng-if=\"\" ng-click=\"back()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Kembali</button> --> <button id=\"\" ng-click=\"back()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span> <i class=\"fa fa-arrow-circle-left\" aria-hidden=\"true\"></i> </span>&nbsp&nbspTambah No. Rangka</button> <button id=\"\" ng-click=\"Batal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\"> <span> <i class=\"fa fa-times\" aria-hidden=\"true\"></i> </span>&nbsp&nbspKembali</button> </div> <div class=\"ui top attached steps small\" style=\"margin: 10px 0 10px 0\"> <div id=\"tabHeader1\" class=\"ui step ng-scope active context-menu\" ng-click=\"goTo(step)\" role=\"button\" tabindex=\"0\"> <i class=\"\"></i> <div class=\"content\"> <div class=\"title ng-binding\" style=\"font-size:13px\">No. Rangka</div> <div class=\"description ng-binding\" style=\"font-size:11px\">Pilih No. Rangka (Step 1)</div> </div> </div> <div id=\"tabHeader2\" class=\"ui step ng-scope context-menu\" ng-click=\"goTo(step)\" role=\"button\" tabindex=\"0\"> <i class=\"\"></i> <div class=\"content\"> <div class=\"title ng-binding\" style=\"font-size:13px\">Dokumen</div> <div class=\"description ng-binding\" style=\"font-size:11px\">Ubah Dokumen (Step 2)</div> </div> </div> </div> <div class=\"ui attached segment\"> <div id=\"tabContent1\" class=\"ui segment ng-scope ng-isolate-scope\" ng-style=\"noMargin\" desc=\"Data Preferensi Pelanggan\" icon=\"info\"> <div class=\"grid\" ui-grid=\"gridNoRangka\" ui-grid-selection ui-grid-pagination ui-grid-auto-resize></div> </div> <div id=\"tabContent2\" class=\"ui segment ng-scope ng-isolate-scope ng-hide\" ng-style=\"noMargin\" desc=\"Data Preferensi Pelanggan\" icon=\"info\"> <div class=\"grid\" ui-grid=\"gridNoRangkaDocumen\" ui-grid-pagination ui-grid-auto-resize></div> </div> </div> </div> <div ng-show=\"listDetail\" class=\"row\" style=\"margin: 0 0 0 0;min-height: 100vh\"> <div class=\"row\" style=\"margin:-30px 0px 0px 0\"> <button style=\"float:right; margin: 0 5px 0 0px\" ng-disabled=\"headerDetail.TerimaDokumenStatusName == 'Dokumen dikirim ke CAO' || headerDetail.listFrameNo[0].TombolKirim == true? false:true\" ng-click=\"KirimSuratDistribusi()\" class=\"btn rbtn\"> <span> <i class=\"fa fa-paper-plane\"></i> </span>&nbsp&nbspKirim</button> <button ng-disabled=\"headerDetail.listFrameNo[0].TombolUbah == true? false:true\" style=\"float:right; margin:0 5px 0 0px\" ng-click=\"UbahSuratDistribusi()\" class=\"btn rbtn\"> <span> <i class=\"fa fa-pencil-square-o\"></i> </span>&nbsp&nbspUbah</button> <button ng-disabled=\"headerDetail.listFrameNo[0].TombolBatal == true? false:true\" style=\"float:right; margin:0 5px 0 0px\" ng-click=\"BatalSuratDistribusi()\" class=\"btn rbtn\"> <span> <i class=\"fa fa-times\"></i> </span>&nbsp&nbspBatal</button> <bs-print cetakan-url=\"{{ctkUrl}}\"> <bsbutton-print> <button ng-disabled=\"headerDetail.listFrameNo[0].TombolCetak == true? false:true\" class=\"rbtn btn\" style=\"float:right; bottom:2px\"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i>&nbsp&nbspCetak </button> </bsbutton-print> </bs-print> <button style=\"float:right; margin: 0 5px 0 0px\" ng-click=\"kembaliList()\" class=\"btn wbtn\">Kembali</button> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-4\" style=\"padding-left: 0px !important\"> <label> <b>No Surat Distribusi:</b> </label> <input type=\"text\" class=\"form-control\" name=\"fieldNoSpk\" placeholder=\"\" ng-model=\"noSurat\" ng-maxlength=\"50\" ng-disabled=\"true\"> </div> </div> <br> <div style=\"margin: 0 0px 0 0px\" class=\"grid\" ui-grid=\"detailList\" ui-grid-auto-resize ui-grid-pagination></div> <!-- <div class=\"row\" style=\"margin: 15px 10px 0 0\">\r" +
    "\n" +
    "        \r" +
    "\n" +
    "    </div> --> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/maintainSuratDistribusiBpkb/maintainSuratDistribusiBpkb.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "        $('body .modals').remove();\r" +
    "\n" +
    "    }</script> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">.context-menu {\r" +
    "\n" +
    "        cursor: context-menu;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <div ng-show=\"ShowMaintainSuratDistribusiBpkbMain\" class=\"\" style=\"width: 100%; min-height: 100vh\"> <button ng-click=\"KePilihData()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;Tambah</button> <bsform-grid ng-form=\"MaintainSuratDistribusiBpkbForm\" factory-name=\"MaintainSuratDistribusiBpkbFactory\" model=\"mMaintainSuratDistribusiBpkb\" model-id=\"SuratDistribusiBPKBHeaderId\" loading=\"loading\" hide-new-button=\"true\" show-advsearch=\"on\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" form-name=\"MaintainSuratDistribusiBpkbForm\" form-title=\"MaintainSuratDistribusiBpkb\" modal-title=\"Maintain Surat Distribusi Bpkb\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Cabang</label> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.OutletId\" data=\"outletlist\" item-text=\"OutletName\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Branch\" icon=\"fa fa-sitemap\"></bsselect> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Nomor Surat Distribusi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Surat Distribusi\" ng-model=\"filter.SuratDistribusiSTNKNo\" maxlength=\"20\" alpha-numeric mask=\"SIUP\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Nomor Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Rangka\" ng-model=\"filter.FrameNo\" maxlength=\"17\" alpha-numeric> </div> </div> </div> </div> <div class=\"row\" style=\"margin:  0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nama Pelanggan\" ng-model=\"filter.CustomerName\" maxlength=\"30\" alpha-numeric mask=\"Huruf\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tanggal Pengiriman</label> <bsdatepicker name=\"tanggalFrom\" date-options=\"dateOptionsStart\" ng-model=\"filter.StartSentDate\" ng-change=\"tanggalmin(filter.StartSentDate)\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Sampai Tanggal</label> <bsdatepicker name=\"tanggalTo\" date-options=\"dateOptionsEnd\" ng-disabled=\"filter.StartSentDate == null\" ng-model=\"filter.EndSentDate\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"show.Wizard\" class=\"row\" style=\"margin: 10px 0 0 0; min-block-size: 100vh\"> <div class=\"row\" style=\"margin: -40px 0 0 0\" ng-if=\"button.FrameTab == true\"> <button id=\"reasignspk\" ng-click=\"KeBuatSuratDistribusi()\" ng-disabled=\"(selctedRow.length == 0 && status == 'buat')\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Lanjut&nbsp&nbsp<span><i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i></span></button> <button id=\"reasignspk\" ng-click=\"KembaliKeMaintainSuratDistribusiStnkMain()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>&nbsp&nbspKembali</button> </div> <div class=\"row\" style=\"margin: -40px 0 0 0\" ng-if=\"button.DokumenTab == true\"> <button ng-click=\"SimpanUpdatePenerimaanDokumenKeDatabase()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i></span>&nbsp&nbspSimpan</button> <!-- <button id=\"\" ng-if=\"\" ng-click=\"back()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Kembali</button> --> <button ng-click=\"KembaliKePilihData()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-arrow-circle-left\" aria-hidden=\"true\"></i></span>&nbsp&nbspTambah Nomor Frame</button> <button ng-click=\"KembaliKeMaintainSuratDistribusiBpkbMain()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>&nbsp&nbspKembali</button> </div> <div class=\"ui top attached steps small\" style=\"margin: 10px 0 10px 0\"> <div id=\"tabHeader1DisbBPKB\" class=\"ui step ng-scope active context-menu\" ng-click=\"goTo(step)\" role=\"button\" tabindex=\"0\"> <i class=\"\"></i> <div class=\"content\"> <div class=\"title ng-binding\" style=\"font-size:13px\">Nomor Rangka</div> <div class=\"description ng-binding\" style=\"font-size:11px\">Pilih Nomor Rangka (Step 1)</div> </div> </div> <div id=\"tabHeader2DisbBPKB\" class=\"ui step ng-scope context-menu\" ng-click=\"goTo(step)\" role=\"button\" tabindex=\"0\"> <i class=\"\"></i> <div class=\"content\"> <div class=\"title ng-binding\" style=\"font-size:13px\">Dokumen</div> <div class=\"description ng-binding\" style=\"font-size:11px\">Update Dokumen (Step 2)</div> </div> </div> </div> <div class=\"ui attached segment\"> <div id=\"tabContent1DisbBPKB\" class=\"ui segment ng-scope ng-isolate-scope\" ng-style=\"noMargin\" title=\"Preferensi\" desc=\"Data Preferensi Pelanggan\" icon=\"info\"> <fieldset class=\"row\" style=\"margin: 0 0 0 0; padding: 15px 15px 0px 15px; border:1px solid #e2e2e2; background-color:rgba(213,51,55,0.02)\"> <div class=\"row\"> <div class=\"col-md-3\"> <bsreqlabel>Outlet Cabang</bsreqlabel> <div class=\"form-group\"> <bsselect name=\"\" data=\"outletlist\" placeholder=\"Outlet Cabang\" item-text=\"OutletName\" item-value=\"OutletId\" ng-model=\"Search.OutletId\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <label>Nomor Rangka</label> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nomor Rangka\" ng-model=\"Search.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-3\" style=\"margin: 23px 0 0 0\"> <button ng-click=\"SearchFrame()\" ng-disabled=\"Search.OutletId == null || Search.OutletId == '' || Search.OutletId == undefined\" class=\"rbtn btn no-animate ng-binding ladda-button\" style=\"float:left\">Cari</button> </div> </div> </fieldset> <div class=\"grid\" ui-grid=\"grid2\" ui-grid-selection ui-grid-pagination ui-grid-auto-resize></div> </div> <div id=\"tabContent2DisbBPKB\" class=\"ui segment ng-scope ng-isolate-scope ng-hide\" ng-style=\"noMargin\" title=\"Preferensi\" desc=\"Data Preferensi Pelanggan\" icon=\"info\"> <div class=\"grid\" ui-grid=\"grid3\" ui-grid-selection ui-grid-pagination ui-grid-auto-resize></div> </div> </div> </div> <!-- <div ng-show=\"ShowPilihData1\">\r" +
    "\n" +
    "    <button ng-click=\"KeBuatSuratDistribusi()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:12%\">Lanjut</button>\r" +
    "\n" +
    "    <button ng-click=\"KembaliKeMaintainSuratDistribusiBpkbMain()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:12%\">Kembali</button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"row\" style=\"display: block; padding: 14px\">\r" +
    "\n" +
    "        <div class=\"grid\" ui-grid=\"grid2\" ui-grid-selection ui-grid-auto-resize></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-show=\"ShowBuatSuratDistribusi1\">\r" +
    "\n" +
    "    <button ng-click=\"SimpanUpdatePenerimaanDokumenKeDatabase()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:12%\">Simpan</button>\r" +
    "\n" +
    "    <button ng-click=\"KembaliKePilihData()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:12%\">Kembali</button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"row\" style=\"display: block; padding: 14px\">\r" +
    "\n" +
    "        <div class=\"grid\" ui-grid=\"grid3\" ui-grid-selection ui-grid-auto-resize></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <div ng-show=\"ShowDetailSuratDistribusiDetail\" style=\"min-height: 100vh\"> <div class=\"row\" style=\"margin: -30px 0 5px 0\"> <button ng-click=\"KirimSuratDistribusi()\" class=\"rbtn btn\" ng-disabled=\"The_Result.listFrameNo[0].TombolKirim == true ? false:true\" style=\"float:right;margin: 0 5px 0 0px\"> <span><i class=\"fa fa-paper-plane\"></i></span> &nbsp&nbspKirim </button> <button ng-disabled=\"The_Result.listFrameNo[0].TombolUbah == true ? false:true\" ng-click=\"KembaliKeBuatSuratDistribusi()\" class=\"rbtn btn\" style=\"float:right;margin: 0 5px 0 0px\"><span><i class=\"fa fa-pencil-square-o\"></i></span>&nbsp&nbspUbah</button> <button ng-disabled=\"The_Result.listFrameNo[0].TombolBatal == true ? false:true\" ng-click=\"confirmModalBatalBpkb()\" class=\"rbtn btn\" style=\"float:right;margin: 0 5px 0 0px\"><span><i class=\"fa fa-times\"></i></span>&nbsp&nbsp Batal</button> <bs-print cetakan-url=\"{{printUrl}}\"> <bsbutton-print> <button ng-disabled=\"The_Result.listFrameNo[0].TombolCetak == true ? false:true\" class=\"rbtn btn\" style=\"float:right; bottom:2px\"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i>&nbsp&nbspCetak </button> </bsbutton-print> </bs-print> <button ng-click=\"KembaliKeMaintainSuratDistribusiBpkbMain()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Kembali</button> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"grid4\" ui-grid-auto-resize></div> </div> <div class=\"ui modal confirmBatalBpkb\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4><b>Batal BPKB</b></h4> </div> <div class=\"modal-body\"> <p align=\"center\" style=\"margin: 15px 0px 0px 0px !important\">Apakah Anda yakin ingin membatalkan data BPKB ini ?</p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb  ; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <button ng-click=\"BatalSuratDistribusi_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin-right:10px\">Kembali</button> <button ng-click=\"KeluarModalBatalStnk()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right\">Keluar</button> </div> </div> </div> </div> <div class=\"ui modal UpdatePenerimaanDokumenBPKB\" role=\"dialog\" style=\"width:40%\"> <div style=\"background-color: white;box-shadow: 1px 3px 3px 0 rgba(0,0,0,.2), 1px 3px 15px 2px rgba(0,0,0,.2)\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4><b>Update Pengiriman Dokumen</b></h4> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> </div> <div class=\"modal-body\"> <legend style=\"background:white;color:red\" class=\"scheduler-border\">Kirim Dokumen</legend> <!--<img ng-src=\"{{viewdocument}}\" image-set-aspect ng-class=\"{'wide': viewdocument.width/viewdocument.height > 1, 'tall': viewdocument.width/viewdocument.height <= 1}\">--> <!-- <ul class=\"list-group\"> --> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px\">Nomor Rangka</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NoRangka\" placeholder=\"No Rangka\" ng-model=\"EditDetilBuatSuratDistribusi.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px\">Nama Pelanggan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NamaPelanggan\" placeholder=\"Nama Pelanggan\" ng-model=\"EditDetilBuatSuratDistribusi.CustomerName\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px; border-top:1px solid grey; border-bottom:1px solid grey\"> <div class=\"col-md-1\"> <!-- <bscheckbox ng-model=\"CentangAllDokumen\" label=\"\" ng-change=\"CentangAllDokumenChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <!-- <input type=\"checkbox\" ng-change=\"CentangAllDokumenChanged()\" ng-model=\"CentangAllDokumen\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px; margin-bottom: 12px\"> <b> Dokumen </b> </div> <div class=\"col-md-7\" style=\"margin-top: 12px; margin-bottom: 12px\"> <b>Tanggal Dokumen Dikirim</b> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px\"> <div class=\"col-md-1\" style=\"padding-top:7px\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.BPKBSendDateChanged\" label=\"\" ng-change=\"BPKBSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.BPKBSendDateChanged\" ng-change=\"BPKBSendDateChanged(EditDetilBuatSuratDistribusi)\"> <!-- <input type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\" ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px\"> <bsreqlabel>BPKB</bsreqlabel> </div> <div class=\"col-md-7\"> <input type=\"datetime-local\" step=\"0\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.BPKBSendDate\" ng-change=\"BPKBTickBox(EditDetilBuatSuratDistribusi)\"> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px\"> <div class=\"col-md-1\" style=\"padding-top:7px\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.FakturTAMSendDateCekChanged\" label=\"\" ng-change=\"FakturTAMSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.FakturTAMSendDateCekChanged\" ng-change=\"FakturTAMSendDateChanged(EditDetilBuatSuratDistribusi)\"> <!-- <input type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang\"  ng-model=\"STNKSendDateCekChanged\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px\"> <bsreqlabel>Faktur TAM</bsreqlabel> </div> <div class=\"col-md-7\"> <input type=\"datetime-local\" min=\"{{TodayDate}}\" class=\"form-control\" ng-model=\"EditDetilBuatSuratDistribusi.FakturTAMSendDate\" ng-change=\"FakturTAMTickBox(EditDetilBuatSuratDistribusi)\"> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px\"> <div class=\"col-md-1\" style=\"padding-top:7px\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.FormStatusGesekSendDateCekChanged\" label=\"\" ng-change=\"FormStatusGesekSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.FormStatusGesekSendDateCekChanged\" ng-change=\"FormStatusGesekSendDateChanged(EditDetilBuatSuratDistribusi)\"> <!-- <input type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang\" ng-change=\"NoticePajakSendDateChanged(EditDetilBuatSuratDistribusi)\" ng-model=\"EditDetilBuatSuratDistribusi.NoticePajakSendDateCekChanged\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px\"> Form Status Gesek </div> <div class=\"col-md-7\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.FormStatusGesekSendDate\" ng-change=\"FormStatusGesekTickBox(EditDetilBuatSuratDistribusi)\"> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px\"> <div class=\"col-md-1\" style=\"padding-top:7px\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.CertificateNIKSendDateCekChanged\" label=\"\" ng-change=\"CertificateNIKSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.CertificateNIKSendDateCekChanged\" ng-change=\"CertificateNIKSendDateChanged(EditDetilBuatSuratDistribusi)\"> <!-- <input type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\" ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px\"> Certificate NIK </div> <div class=\"col-md-7\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.CertificateNIKSendDate\" ng-change=\"CertificateNIKTickBox(EditDetilBuatSuratDistribusi)\"> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px\"> <div class=\"col-md-1\" style=\"padding-top:7px\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.SuratUjiTipeSendDateCekChanged\" label=\"\" ng-change=\"SuratUjiTipeSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.SuratUjiTipeSendDateCekChanged\" ng-change=\"SuratUjiTipeSendDateChanged(EditDetilBuatSuratDistribusi)\"> <!-- <input type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\" ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px\"> Surat Uji Tipe </div> <div class=\"col-md-7\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.SuratUjiTipeSendDate\" ng-change=\"SuratUjiTipeTickBox(EditDetilBuatSuratDistribusi)\"> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px\"> <div class=\"col-md-1\" style=\"padding-top:7px\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.FormASendDateCekChanged\" label=\"\" ng-change=\"FormASendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.FormASendDateCekChanged\" ng-change=\"FormASendDateChanged(EditDetilBuatSuratDistribusi)\"> <!-- <input type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\" ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px\"> Form A </div> <div class=\"col-md-7\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.FormASendDate\" ng-change=\"FormATickBox(EditDetilBuatSuratDistribusi)\"> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px\"> <div class=\"col-md-1\" style=\"padding-top:7px\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.SuratRekomendasiSendDateCekChanged\" label=\"\" ng-change=\"SuratRekomendasiSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.SuratRekomendasiSendDateCekChanged\" ng-change=\"SuratRekomendasiSendDateChanged(EditDetilBuatSuratDistribusi)\"> <!-- <input type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\" ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px\"> Surat Rekomendasi </div> <div class=\"col-md-7\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.SuratRekomendasiSendDate\" ng-change=\"SuratRekomendasiTickBox\"> </div> </div> <div class=\"row\" style=\"margin: 10px 20px 0 20px\"> <div class=\"col-md-1\" style=\"padding-top:7px\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.PIBSendDateCekChanged\" label=\"\" ng-change=\"PIBSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                    </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.PIBSendDateCekChanged\" ng-change=\"PIBSendDateChanged(EditDetilBuatSuratDistribusi)\"> <!-- <input type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\" ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\"> --> </div> <div class=\"col-md-4\" style=\"margin-top: 12px\"> PIB </div> <div class=\"col-md-7\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.PIBSendDate\" ng-change=\"PIBTickBox(EditDetilBuatSuratDistribusi)\"> </div> </div> <!-- </ul> --> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin: 0 0 0 20px\"> <!-- <bscheckbox style=\"float: left\" ng-model=\"EditDetilBuatSuratDistribusi.DokumenLengkap\" ng-change=\"CentangAllDokumenChanged(EditDetilBuatSuratDistribusi.DokumenLengkap, EditDetilBuatSuratDistribusi)\" label=\" Dokumen Lengkap\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                </bscheckbox> --> <div style=\"float:left; padding-top:5px\"> <input type=\"checkbox\" style=\"float: left\" ng-model=\"EditDetilBuatSuratDistribusi.DokumenLengkap\" ng-change=\"CentangAllDokumenChanged(EditDetilBuatSuratDistribusi.DokumenLengkap, EditDetilBuatSuratDistribusi)\"> <label style=\"padding-left:5px; padding-top:5px\">Dokumen Lengkap</label> </div> <!-- <button ng-click=\"test()\"></button> --> <!-- <button ng-click=\"btnkehilanganSpk()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width:15%;\">Simpan</button> --> <!-- <input style=\"margin-left:10px;float:left\" type=\"checkbox\" ng-model=\"EditDetilMaintainSuratDistribusiSTNKKelengkapan\"> Dokumen Lengkap --> <button style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"SimpanUpdatePenerimaanDokumen(EditDetilBuatSuratDistribusi)\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" ng-disabled=\"EditDetilBuatSuratDistribusi.BPKBSendDate == null || EditDetilBuatSuratDistribusi.FakturTAMSendDate == null\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Lanjut </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"keluarModal()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/maintainSuratDistribusiSTNK/maintainSuratDistribusiSTNK.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "        $('body .modals').remove();\r" +
    "\n" +
    "    }</script> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">.context-menu {\r" +
    "\n" +
    "        cursor: context-menu;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <div ng-show=\"ShowMaintainSuratDistribusiStnkMain\" class=\"row\" style=\"margin: 0 0 0 0\"> <button ng-click=\"BuatDistribusiSTNK()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;Tambah</button> <bsform-grid ng-form=\"MaintainSuratDistribusiStnkForm\" factory-name=\"MaintainSuratDistribusiSTNKFactory\" model=\"mMaintainSuratDistribusiStnk\" model-id=\"SuratDistribusiSTNKHeaderId\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" loading=\"loading\" hide-new-button=\"true\" form-name=\"MaintainSuratDistribusiStnkForm\" form-title=\"MaintainSuratDistribusiStnk\" modal-title=\"Maintain Surat Distribusi Stnk\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Cabang</label> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.OutletId\" data=\"outletList\" item-text=\"OutletName\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Branch\" icon=\"fa fa-sitemap\"></bsselect> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Nomor Surat Distribusi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nomor Surat Distribusi\" ng-model=\"filter.SuratDistribusiSTNKNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. Rangka\" ng-model=\"filter.FrameNo\" maxlength=\"17\"> </div> </div> </div> </div> <div class=\"row\" style=\"margin:  0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nama Pelanggan\" ng-model=\"filter.CustomerName\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tanggal Pengiriman</label> <bsdatepicker name=\"tanggalFrom\" date-options=\"dateOptionsStart\" ng-model=\"filter.StartSentDate\" ng-change=\"tanggalmin(filter.StartSentDate)\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Sampai Tanggal</label> <bsdatepicker name=\"tanggalTo\" date-options=\"dateOptionsEnd\" ng-disabled=\"filter.StartSentDate == null\" ng-model=\"filter.EndSentDate\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </div> <!-- </div> --> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"show.Wizard\" class=\"row\" style=\"margin: 10px 0 0 0\"> <div class=\"row\" style=\"margin: -40px 0 0 0\" ng-if=\"button.FrameTab == true\"> <button id=\"reasignspk\" ng-click=\"Lanjut()\" ng-disabled=\"(selctedRow.length == 0 && status == 'buat')\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Lanjut&nbsp&nbsp<span><i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i></span></button> <button id=\"reasignspk\" ng-click=\"KembaliKeMaintainSuratDistribusiStnkMain()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>&nbsp&nbspKembali</button> </div> <div class=\"row\" style=\"margin: -40px 0 0 0\" ng-if=\"button.DokumenTab == true\"> <button ng-click=\"SimpanUpdatePenerimaanDokumenKeDatabase()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i></span>&nbsp&nbspSimpan</button> <!-- <button id=\"\" ng-if=\"\" ng-click=\"back()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Kembali</button> --> <button ng-click=\"back()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-arrow-circle-left\" aria-hidden=\"true\"></i></span>&nbsp&nbspTambah Nomor Frame</button> <button ng-click=\"KembaliKeMaintainSuratDistribusiStnkMain()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>&nbsp&nbspKembali</button> </div> <div class=\"ui top attached steps small\" style=\"margin: 10px 0 10px 0\"> <div id=\"tabHeader1DisbSTNK\" class=\"ui step ng-scope active context-menu\" ng-click=\"goTo(step)\" role=\"button\" tabindex=\"0\"> <i class=\"\"></i> <div class=\"content\"> <div class=\"title ng-binding\" style=\"font-size:13px\">Nomor Rangka</div> <div class=\"description ng-binding\" style=\"font-size:11px\">Pilih Nomor Rangka (Step 1)</div> </div> </div> <div id=\"tabHeader2DisbSTNK\" class=\"ui step ng-scope context-menu\" ng-click=\"goTo(step)\" role=\"button\" tabindex=\"0\"> <i class=\"\"></i> <div class=\"content\"> <div class=\"title ng-binding\" style=\"font-size:13px\">Dokumen</div> <div class=\"description ng-binding\" style=\"font-size:11px\">Update Dokumen (Step 2)</div> </div> </div> </div> <div class=\"ui attached segment\"> <div id=\"tabContent1DisbSTNK\" class=\"ui segment ng-scope ng-isolate-scope\" ng-style=\"noMargin\" title=\"Preferensi\" desc=\"Data Preferensi Pelanggan\" icon=\"info\"> <fieldset class=\"row\" style=\"margin: 0 0 0 0; padding: 15px 15px 0px 15px; border:1px solid #e2e2e2; background-color:rgba(213,51,55,0.02)\"> <div class=\"row\"> <form name=\"validoutletstnk\" novalidate> <div class=\"col-md-3\"> <bsreqlabel>Outlet</bsreqlabel> <div class=\"form-group\"> <!-- <bsreqlabel>Outlet Cabang</bsreqlabel> --> <bsselect required name=\"\" data=\"outletList\" placeholder=\"Outlet Cabang\" item-text=\"OutletName\" item-value=\"OutletId\" ng-model=\"Search.OutletId\"> </bsselect> </div> </div> </form> <div class=\"col-md-3\"> <label>No. Rangka</label> <div class=\"form-group\"> <!-- <label>Frame Number</label> --> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. Rangka\" ng-model=\"Search.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-3\" style=\"margin: 23px 0 0 0\"> <button ng-disabled=\"validoutletstnk.$invalid\" ng-click=\"SearchFrame()\" class=\"rbtn btn no-animate ng-binding ladda-button\" style=\"float:left\">Cari</button> </div> </div> </fieldset> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridPilihFrame\" ui-grid-selection ui-grid-pagination ui-grid-auto-resize></div> </div> <div id=\"tabContent2DisbSTNK\" class=\"ui segment ng-scope ng-isolate-scope ng-hide\" ng-style=\"noMargin\" title=\"Preferensi\" desc=\"Data Preferensi Pelanggan\" icon=\"info\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"STNKTerpilih\" ui-grid-selection ui-grid-auto-resize></div> </div> </div> </div> <div ng-show=\"ShowPilihData\"> <div class=\"row\" style=\"margin:-30px 0 30px 0\"> <button style=\"float: right\" ng-click=\"Lanjut()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Lanjut </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliKeMaintainSuratDistribusiStnkMain()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridPilihFrame\" ui-grid-selection ui-grid-pagination ui-grid-auto-resize></div> </div> <div ng-show=\"ShowBuatSuratDistribusi\"> <button ng-disabled=\"onLoading\" style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"SimpanUpdatePenerimaanDokumenKeDatabase()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Simpan </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"KembaliKePilihData()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> <br> <br> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridPilihFrame\" ui-grid-selection ui-grid-pagination ui-grid-auto-resize></div> <br> </div> <div ng-show=\"ShowDetailSuratDistribusi\"> <div class=\"row\" style=\"margin: -30px 0 30px 0\"> <button ng-disabled=\"Header.listFrameNo[0].TombolKirim == true ? false:true\" style=\"float: right ;margin-right:5px\" ng-click=\"KirimSuratDistribusi()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i></span>&nbspKirim </button> <bs-print cetakan-url=\"{{print}}\"> <bsbutton-print> <button ng-disabled=\"Header.listFrameNo[0].TombolCetak == true ? false:true\" class=\"rbtn btn\" style=\"float:right\"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i>&nbsp&nbspCetak </button> </bsbutton-print> </bs-print> <button ng-disabled=\"Header.listFrameNo[0].TombolUbah == true ? false:true\" style=\"float: right;margin-right:5px\" ng-click=\"KembaliKeBuatSuratDistribusi()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-pencil\"></i>Ubah </span><span class=\"ladda-spinner\"></span> </button> <button ng-disabled=\"Header.listFrameNo[0].TombolBatal == true ? false:true\" style=\"float: right;margin-right:5px\" ng-click=\"showModalConfirmBatalStnk()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-times\"></i>Batal </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right;margin-right:5px\" ng-click=\"KembaliKeMaintainSuratDistribusiStnkMain()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"grid4\" ui-grid-auto-resize></div> <br> <div class=\"row\" style=\"margin: 0 0 0 0\"> <!-- <button style=\"float: right;margin-left:5px;margin-right:5px;\" ng-click=\"CetakPengirimanSTNKKeCabang()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "                        Cetak\r" +
    "\n" +
    "\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t</button> --> </div> </div> <div class=\"ui modal confirmBatalStnk\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4><b>Batal STNK</b></h4> </div> <div class=\"modal-body\"> <p align=\"center\" style=\"margin: 15px 0px 0px 0px !important\">Apakah Anda yakin ingin membatalkan data STNK ini ?</p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb  ; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <button ng-click=\"BatalSuratDistribusi_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin-right:10px\">Simpan</button> <button ng-click=\"KeluarModalBatalStnk()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Kembali</button> </div> </div> </div> </div> <div class=\"ui modal formDocumenfakturTamSingle\" role=\"dialog\" style=\"background-color: transparent !important\"> <!-- <div> --> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Update Penerimaan Dokumen</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button style=\"float: right\" ng-click=\"simpanupdateSingle()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbspSimpan </span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\" keluar()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Keluar</span><span class=\"ladda-spinner\"></span> </button> <div style=\"float:left;padding-left: 15px\"> <bscheckbox ng-model=\"rowsDocument.DokumenLengkap\" label=\"Kelengkapan Dokumen Sudah Diperiksa\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </div> <!-- </div> --> </div></p> </div> <div class=\"ui modal UpdatePenerimaanDokumenSTNK\" style=\"background: none; box-shadow: none\"> <div class=\"vertical-alignment-helper\"> <div class=\"modal-dialog vertical-align-center\" style=\"margin-top: 0px; margin-bottom: 0px\"> <div class=\"modal-content\" style=\"border-bottom-width: 0px; width:700px; height:100%\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4><b>Update Pengiriman Dokumen</b></h4> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"><b>Kirim Dokumen</b></p> <ul class=\"list-group\"> <div style=\"margin-bottom: 10px;margin-left:25px;margin-right:25px\"> <div class=\"col-md-6\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px\">No. Rangka</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NoRangka\" placeholder=\"Input No. Rangka\" ng-model=\"EditDetilBuatSuratDistribusi.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px\">Nama Pelanggan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NamaPelanggan\" placeholder=\"Input Nama Pelanggan\" ng-model=\"EditDetilBuatSuratDistribusi.CustomerName\" ng-maxlength=\"50\" required> </div> </div> </div> <table class=\"table table-bordered table-responsive\" border=\"1\" style=\"border: none !important\"> <tbody> <tr> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">&nbspDokumen</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Dikirim</th> </tr> <tr> <td style=\"border: none !important\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.STNKSendDateCekChanged\" label=\"STNK *\" ng-change=\"STNKSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                                                                            </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.STNKSendDateCekChanged\" ng-change=\"STNKSendDateChanged(EditDetilBuatSuratDistribusi)\"> <bsreqlabel>STNK</bsreqlabel> </td> <td style=\"border: none !important\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.STNKSendDate\" ng-change=\"STNKFieldChanged(EditDetilBuatSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\" label=\"No. Plat Polisi *\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                                                                            </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\"> <bsreqlabel>No. Plat Polisi</bsreqlabel> </td> <td style=\"border: none !important\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.PoliceNumberSendDate\" ng-change=\"PoliceNumberFieldChanged(EditDetilBuatSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"> <!-- <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.NoticePajakSendDateCekChanged\" label=\"Notice Pajak *\" ng-change=\"NoticePajakSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                                                                            </bscheckbox> --> <input type=\"checkbox\" ng-model=\"EditDetilBuatSuratDistribusi.NoticePajakSendDateCekChanged\" ng-change=\"NoticePajakSendDateChanged(EditDetilBuatSuratDistribusi)\"> <bsreqlabel>Notice Pajak</bsreqlabel> </td> <td style=\"border: none !important\"> <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.NoticePajakSendDate\" ng-change=\"NoticePajakFieldChanged(EditDetilBuatSuratDistribusi)\"> </td> </tr> </tbody> </table> </ul> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin: 0 0 0 20px\"> <!-- <bscheckbox style=\"float: left\" ng-change=\"CentangAllDokumenChanged(EditDetilBuatSuratDistribusi.DokumenLengkap, EditDetilBuatSuratDistribusi)\" ng-model=\"EditDetilBuatSuratDistribusi.DokumenLengkap\" label=\" Dokumen Lengkap\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                                    </bscheckbox> --> <div style=\"float:left; padding-top:5px\"> <input type=\"checkbox\" style=\"float: left\" ng-model=\"EditDetilBuatSuratDistribusi.DokumenLengkap\" ng-change=\"CentangAllDokumenChanged(EditDetilBuatSuratDistribusi.DokumenLengkap, EditDetilBuatSuratDistribusi)\"> <label style=\"padding-left:5px; padding-top:5px\">Dokumen Lengkap</label> </div> <!-- <button ng-click=\"test()\">Test</button> --> <!-- <button ng-click=\"btnkehilanganSpk()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width:15%;\">Simpan</button> --> <!-- <input style=\"margin-left:10px;float:left\" type=\"checkbox\" ng-model=\"EditDetilMaintainSuratDistribusiSTNKKelengkapan\"> Dokumen Lengkap --> <button style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"SimpanUpdatePenerimaanDokumen(EditDetilBuatSuratDistribusi)\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" ng-disabled=\"EditDetilBuatSuratDistribusi.STNKSendDate == null || EditDetilBuatSuratDistribusi.PoliceNumberSendDate == null || EditDetilBuatSuratDistribusi.NoticePajakSendDate == null\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Lanjut </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"keluarModal()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> </div> </div> </div> <!-- <div class=\"ui modal UpdatePenerimaanDokumenSTNK\" role=\"dialog\" style=\"width:40%\">\r" +
    "\n" +
    "        <div style=\"background-color: white;box-shadow: 1px 3px 3px 0 rgba(0,0,0,.2), 1px 3px 15px 2px rgba(0,0,0,.2)\">\r" +
    "\n" +
    "            <div class=\"modal-header\" style=\" padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15);\">\r" +
    "\n" +
    "                <h4><b>Update Pengiriman Dokumen</b></h4>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"modal-body\">\r" +
    "\n" +
    "                <legend style=\"background:white;color:red;\" class=\"scheduler-border\">Terima Dokumen</legend>\r" +
    "\n" +
    "                <div class=\"row\" style=\"margin: 0 0 0 0;\">\r" +
    "\n" +
    "                    <div class=\"col-md-12\" style=\"margin-bottom: 10px;\">\r" +
    "\n" +
    "                        <bsreqlabel style=\"margin-bottom: 5px;\">Nomor Rangka</bsreqlabel>\r" +
    "\n" +
    "                        <div class=\"input-icon-right\">\r" +
    "\n" +
    "                            <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                            <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NoRangka\" placeholder=\"No Rangka\" ng-model=\"EditDetilBuatSuratDistribusi.FrameNo\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-12\" style=\"margin-bottom: 10px;\">\r" +
    "\n" +
    "                        <bsreqlabel style=\"margin-bottom: 5px;\">Nama Pelanggan</bsreqlabel>\r" +
    "\n" +
    "                        <div class=\"input-icon-right\">\r" +
    "\n" +
    "                            <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                            <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NamaPelanggan\" placeholder=\"Nama Pelanggan\" ng-model=\"EditDetilBuatSuratDistribusi.CustomerName\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\" style=\"margin: 10px 20px 0 20px; border-top:1px solid grey; border-bottom:1px solid grey;\">\r" +
    "\n" +
    "                    <div class=\"col-md-1\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-4\" style=\"margin-top: 12px; margin-bottom: 12px\">\r" +
    "\n" +
    "                        <b>Dokumen</b>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-7\" style=\"margin-top: 12px; margin-bottom: 12px\">\r" +
    "\n" +
    "                        <b>Tanggal Dokumen Dikirim</b>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\" style=\"margin: 10px 20px 0 20px\">\r" +
    "\n" +
    "                    <div class=\"col-md-1\">\r" +
    "\n" +
    "                        <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.STNKSendDateCekChanged\" label=\"\" ng-change=\"STNKSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                        </bscheckbox>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-4\">\r" +
    "\n" +
    "                        <bsreqlabel>STNK</bsreqlabel>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-7\">\r" +
    "\n" +
    "                        <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.STNKSendDate\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\" style=\"margin: 10px 20px 0 20px\">\r" +
    "\n" +
    "                    <div class=\"col-md-1\">\r" +
    "\n" +
    "                            <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.PlatNomorPolisiSendDateCekChanged\" label=\"\" ng-change=\"PlatNomorPolisiSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                        </bscheckbox>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-4\">\r" +
    "\n" +
    "                        <bsreqlabel>No. Plat Polisi</bsreqlabel>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-7\">\r" +
    "\n" +
    "                        <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.PoliceNumberSendDate\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\" style=\"margin: 10px 20px 0 20px\">\r" +
    "\n" +
    "                    <div class=\"col-md-1\">\r" +
    "\n" +
    "                        <bscheckbox ng-model=\"EditDetilBuatSuratDistribusi.NoticePajakSendDateCekChanged\" label=\"\" ng-change=\"NoticePajakSendDateChanged(EditDetilBuatSuratDistribusi)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                        </bscheckbox>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-4\">\r" +
    "\n" +
    "                        <bsreqlabel>Notice Pajak</bsreqlabel>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-7\">\r" +
    "\n" +
    "                        <input type=\"datetime-local\" class=\"form-control\" min=\"{{TodayDate}}\" ng-model=\"EditDetilBuatSuratDistribusi.NoticePajakSendDate\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <div class=\"ui modal lodingdistribusistnk\" style=\"height:200px; background-color:transparent; box-shadow: none !important\"> <div align=\"center\" class=\"list-group-item\" style=\"border: none !important;padding:10px 0px 10px 0px; background-color:transparent\"> <font color=\"white\"><i class=\"fa fa-spinner fa-spin fa-3x fa-lg\"></i> <b> Loading Menu... </b> </font> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/monitoringSTNKdanBPKB/monitoringSTNKdanBPKB.html',
    "<script type=\"text/javascript\">function SetReceiveBPKBDateMinDate(TheDate) {\r" +
    "\n" +
    "        var Da_ReceiveBPKBDateMinDate = TheDate;\r" +
    "\n" +
    "        var dd = Da_ReceiveBPKBDateMinDate.getDate();\r" +
    "\n" +
    "        var mm = Da_ReceiveBPKBDateMinDate.getMonth() + 1; //January is 0!\r" +
    "\n" +
    "        var yyyy = Da_ReceiveBPKBDateMinDate.getFullYear();\r" +
    "\n" +
    "        if (dd < 10) {\r" +
    "\n" +
    "            dd = '0' + dd\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (mm < 10) {\r" +
    "\n" +
    "            mm = '0' + mm\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        Da_ReceiveBPKBDateMinDate = yyyy + '-' + mm + '-' + dd;\r" +
    "\n" +
    "        document.getElementById(\"Max_Da_ReceiveBPKBDateMinDate\").setAttribute(\"min\", Da_ReceiveBPKBDateMinDate);\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    function SetReceiveSTNKDateMinDate(TheDate) {\r" +
    "\n" +
    "        var Da_ReceiveSTNKDateMaxDate = TheDate;\r" +
    "\n" +
    "        var dd = Da_ReceiveSTNKDateMaxDate.getDate();\r" +
    "\n" +
    "        var mm = Da_ReceiveSTNKDateMaxDate.getMonth() + 1; //January is 0!\r" +
    "\n" +
    "        var yyyy = Da_ReceiveSTNKDateMaxDate.getFullYear();\r" +
    "\n" +
    "        if (dd < 10) {\r" +
    "\n" +
    "            dd = '0' + dd\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (mm < 10) {\r" +
    "\n" +
    "            mm = '0' + mm\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        Da_ReceiveSTNKDateMaxDate = yyyy + '-' + mm + '-' + dd;\r" +
    "\n" +
    "        document.getElementById(\"Min_Da_ReceiveSTNKDateMaxDate\").setAttribute(\"min\", Da_ReceiveSTNKDateMaxDate);\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }</script>  <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"daftarPO\" class=\"row\" style=\"margin: 0 0 0 0; min-height: 100vh\"> <div class=\"row\" style=\"margin:-25px 0 0 0\"> <div class=\"row\"> <div class=\"col-md-3\" style=\"float:right\"> <label>Tanggal Penerimaan STNK : </label> </div> </div> <div style=\"float: right\"> <div style=\"float: right;margin-left: 10px;margin-top: 3px\"> <span> <i ng-click=\"tanggal('plus')\" style=\"color:red\" class=\"fa fa-2x fa-caret-square-o-right\" aria-hidden=\"true\"></i> </span> </div> <div style=\"width: 20%; float:right\"> <bsdatepicker style=\"float:right\" ng-model=\"filter.DateTimeJadwalPenerimaan\" ng-change=\"getAll(filter.DateTimeJadwalPenerimaan)\" date-options=\"dateOption\" name=\"tanggalfrom\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> <div style=\"float: right;margin-right: 10px;margin-top: 3px\"> <span> <i ng-click=\"tanggal('min')\" style=\"color: red\" class=\"fa fa-2x fa-caret-square-o-left\" aria-hidden=\"true\"></i> </span> </div> </div> </div> <div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 10px 10px 10px; margin-top: 15px; margin-bottom: 20px\"> <p style=\"width:150px; margin-top:-20px; margin-left:14px; background:white\"> <b>Jadwal Penerimaan STNK</b> </p> <fieldset class=\"row\" style=\"margin: 0 0 0 0; padding: 15px 15px 0px 15px; border:1px solid #e2e2e2; background-color:rgba(213,51,55,0.02)\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Filter</label> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Filter\" ng-model=\"filter.Search\"> </div> </div> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsselect name=\"\" data=\"\" placeholder=\"\" item-text=\"\" item-value=\"\" ng-model=\"\" icon=\"fa\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div> --> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Kategori</label> <select class=\"form-control\" name=\"SelectFilter\" ng-model=\"data.selectOption\"> <option ng-repeat=\"option in data.optionData\" value=\"{{option.id}}\">{{option.name}}</option> <!-- <option value=\"filter.POBiroJasaUnitNo\">No. PO</option>\r" +
    "\n" +
    "                            <option value=\"filter.ServiceBureauName\">Vendor</option>\r" +
    "\n" +
    "                            <option value=\"filter.OutletName\">Cabang</option>\r" +
    "\n" +
    "                            <option value=\"filter.FrameNo\">No. Rangka</option>\r" +
    "\n" +
    "                            <option value=\"filter.VehicleModelName\">Model</option> --> </select> </div> </div> <div class=\"col-md-3\"> <button ng-click=\"FilterGrid()\" class=\"rbtn btn no-animate ng-binding ladda-button\" style=\"float:left;margin: 23px 0 0 0\">Cari</button> </div> </div> </fieldset> <div ui-grid=\"gridStnk\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-auto-resize></div> </div> <!-- <div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 10px 10px 10px; margin-top: 10px\">\r" +
    "\n" +
    "        <p style=\"width:150px; margin-top:-20px; margin-left:14px; background:white;\"><b>Jadwal Penerimaan BPKB</b></p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div ui-grid=\"gridBpkb\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-auto-resize></div>\r" +
    "\n" +
    "    </div> --> <!-- /////////////////////////////////////////////////////////////////////////// --> <div class=\"ui modal terimaSTNKMonitoring\" role=\"dialog\" style=\"background-color: transparent !important; width: 600px\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Terima STNK dari Biro Jasa</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div filedset=\"false\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoRangka\" placeholder=\"\" ng-model=\"rowsDocument.FrameNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Model Kendaraan</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.Description\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Nama STNK</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.STNKName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"> <b>Item yang Diterima</b> </p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-1\" style=\"margin-top: 8px\"> <!--<span ng-click=\"toogle = !toogle\"><i  ng-class=\"{'fa fa-square-o' : asdf == false || !toogle, 'fa fa-check-square' : asdf == true || toogle}\"  style=\"font-size: 15px\" aria-hidden=\"true\"></i>--> <!--<input type=\"checkbox\" readonly ng-model=\"NoSTNKCekChanged\">--> <span> <i ng-class=\"{'fa fa-square-o' : NoSTNKCekChanged == false || !toogle, 'fa fa-check-square' : NoSTNKCekChanged == true || toogle}\" style=\"font-size: 15px\" aria-hidden=\"true\"></i> </span></div> <div class=\"col-xs-4\" style=\"margin-top: 8px; padding-left: 0px !important\"> <bsreqlabel>No. STNK</bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangSTNK()\" type=\"text\" class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.STNKNo\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-1\" style=\"margin-top: 8px\"> <!--<i class=\"fa fa-square-o\" style=\"font-size: 15px\" aria-hidden=\"true\"></i>--> <!--<input type=\"checkbox\" readonly ng-model=\"PoliceNumberCekChanged\">--> <span> <i ng-class=\"{'fa fa-square-o' : PoliceNumberCekChanged == false || !toogle, 'fa fa-check-square' : PoliceNumberCekChanged == true || toogle}\" style=\"font-size: 15px\" aria-hidden=\"true\"></i> </span></div> <div class=\"col-xs-4\" style=\"margin-top: 8px; padding-left: 0px !important\"> <bsreqlabel>Plat No Polisi</bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangPoliceNumber()\" type=\"text\" class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.PoliceNumber\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-1\" style=\"margin-top: 8px\"> <!--<i class=\"fa fa-square-o\" style=\"font-size: 15px\" aria-hidden=\"true\"></i>--> <!--<input type=\"checkbox\" readonly ng-model=\"NoticePajakCekChanged\">--> <span> <i ng-class=\"{'fa fa-square-o' : NoticePajakCekChanged == false || !toogle, 'fa fa-check-square' : NoticePajakCekChanged == true || toogle}\" style=\"font-size: 15px\" aria-hidden=\"true\"></i> </span></div> <div class=\"col-xs-4\" style=\"margin-top: 8px ;padding-left: 0px !important\"> <bsreqlabel>Notice Pajak</bsreqlabel> </div> </div> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"width:96%;border:1px solid lightgrey;padding: 15px 0 0 0;margin: 15px 10px 10px 10px\"> <p style=\"width:110px; margin-top:-21px; padding-left: 3px; margin-left:14px; background:white\"> <b>Item Notice Pajak</b> </p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <bsreqlabel>BBNKB </bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.BBNKB\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <bsreqlabel>PKB </bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.PKB\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <bsreqlabel>SWDKLLJ </bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.SWDKLLJ\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <bsreqlabel>Biaya Adm.STNK </bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.BiayaAdmSTNK\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <bsreqlabel>Biaya Adm.TNKB </bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.BiayaAdmTNKB\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\" style=\"padding: 15px 13px 0 0\"> <label>Tanggal Aju STNK ke Biro Jasa :</label> <bsdatepicker ng-disabled=\"true\" ng-change=\"changeDate(rowsDocument.POBiroJasaUnitDate)\" type=\"date\" date-options=\"dateOption\" ng-model=\"rowsDocument.POBiroJasaUnitDate\"></bsdatepicker> </div> <div class=\"col-md-6\" style=\"padding: 15px 0 0 13px\"> <bsreqlabel>Tanggal Terima STNK dari Biro Jasa :</bsreqlabel> <bsdatepicker type=\"date\" date-options=\"dateOptionMax\" ng-model=\"rowsDocument.ReceiveSTNKDate\"></bsdatepicker> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button ng-disabled=\"(rowsDocument.STNKNo == null || \r" +
    "\n" +
    "                                     rowsDocument.PoliceNumber == null ||\r" +
    "\n" +
    "                                     rowsDocument.BBNKB == null ||\r" +
    "\n" +
    "                                     rowsDocument.PKB == null ||\r" +
    "\n" +
    "                                     rowsDocument.SWDKLLJ == null ||\r" +
    "\n" +
    "                                     rowsDocument.BiayaAdmSTNK == null ||\r" +
    "\n" +
    "                                     rowsDocument.BiayaAdmTNKB == null) || \r" +
    "\n" +
    "                                     (rowsDocument.STNKNo == '' || \r" +
    "\n" +
    "                                     rowsDocument.PoliceNumber == '' ||\r" +
    "\n" +
    "                                     rowsDocument.BBNKB == '' ||\r" +
    "\n" +
    "                                     rowsDocument.PKB == '' ||\r" +
    "\n" +
    "                                     rowsDocument.SWDKLLJ == '' ||\r" +
    "\n" +
    "                                     rowsDocument.BiayaAdmSTNK == '' ||\r" +
    "\n" +
    "                                     rowsDocument.BiayaAdmTNKB == '') || (rowsDocument.ReceiveSTNKDate == null || rowsDocument.ReceiveSTNKDate == '')\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"simpanTerimaSTNK()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span> <span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\" backFromterimaSTNK()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-times\"></i>&nbsp;Kembali</span> <span class=\"ladda-spinner\"></span> </button> </p> </div> </div> <div class=\"ui small modal alasanterlambatstnkMonitoring\" modal=\"alasanterlambat\" role=\"dialog\"> <!-- <div> --> <div class=\"modal-header\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\"></button> --> <h4 class=\"modal-title\"> <i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Alasan Keterlambatan Penerimaan STNK dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 15px 0 15px; margin-top: 10px 10px 0 10px\"> <p style=\"width:50px; padding:0 5px 0 5px; margin-top:-18px; margin-bottom:15px; background:white\"> <b>Alasan</b> </p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\" style=\"padding-left: 0 !important\"> <label>PO Number</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"POBiroJasaUnitNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_POBiroJasaUnitNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important; padding-top: 10px !important\"> <label>Model</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_Model\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <!-- <bsreqlabel>Alasan Keterlambatan</bsreqlabel> --> <bsreqlabel>Alasan Keterlambatan</bsreqlabel> <div class=\"form-group\"> <textarea title=\"Please fill out this field.\" maxlength=\"200\" ng-model=\"SelectedDataTerlambat_AlasanKeterlambatanSTNK\" rows=\"6\" class=\"form-control\" cols=\"83\">\r" +
    "\n" +
    "                                                                    </textarea> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\" style=\"margin-top: 8px; padding:0 5px 0 0\"> <bsreqlabel>Lama Keterlambatan : </bsreqlabel> </div> <div class=\"col-md-2\" style=\"padding: 0 0 0 0\"> <div class=\"form-group\"> <div> <input required number-only ng-change=\"input(SelectedDataTerlambat_LamaKeterlambatanSTNK)\" class=\"form-control\" size=\"2\" name=\"LamaKeterlambatanSTNK\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_LamaKeterlambatanSTNK\"> </div> </div> </div> <div class=\"col-md-1\" style=\"margin-top: 8px\"> Hari </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"simpanAlasanRevisiSTNK()\" ng-disabled=\"(SelectedDataTerlambat_AlasanKeterlambatanSTNK == undefined || SelectedDataTerlambat_AlasanKeterlambatanSTNK == null || SelectedDataTerlambat_AlasanKeterlambatanSTNK == '') ||\r" +
    "\n" +
    "                                    (SelectedDataTerlambat_LamaKeterlambatanSTNK == undefined || SelectedDataTerlambat_LamaKeterlambatanSTNK == null || SelectedDataTerlambat_LamaKeterlambatanSTNK == '')\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan </span> <span class=\"ladda-spinner\"></span> </button> <button ng-click=\"backFromAlasanRevisiSTNK()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </p> </div> <!-- </div> --> </div> <div class=\"ui small modal revisiStnkMonitoring\" modal=\"revisiStnk\" role=\"dialog\"> <!-- <div> --> <div class=\"modal-body\"> <p> <h3 align=\"center\"> ALERT </h3> </p> <p align=\"center\"> Apakah Anda ingin Melakukan Revisi STNK ? </p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"right\" style=\"margin-bottom: 0px !important\"> <button ng-click=\"backFromRevisiSTNK()\" class=\"wbtn btn ng-binding ladda-button\">Tidak</button> <button ng-click=\"openSaveRevisiSTNK()\" class=\"rbtn btn ng-binding ladda-button\" style=\"width: 8%\">Ya</button> </p> </div> <!-- </div> --> </div> <div class=\"ui modal alasanrevisiSTNK\" modal=\"alasanterlambat\" role=\"dialog\"> <!-- <div> --> <div class=\"modal-header\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\"></button> <h4 class=\"modal-title\"><i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Alasan Revisi Penerimaan STNK dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 15px 0 15px; margin-top: 10px 10px 0 10px\"> <p style=\"width:50px; padding:0 5px 0 5px; margin-top:-18px; margin-bottom:15px; background:white\"><b>Alasan</b></p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"The_SelectedRevisiSTNK.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\" style=\"padding-right: 0 !important\"> <label>No. PO</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" placeholder=\"No. PO\" ng-model=\"The_SelectedRevisiSTNK.POBiroJasaUnitNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important; padding-top: 10px !important\"> <label>Model</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"The_SelectedRevisiSTNK.VehicleModelName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <bsreqlabel>Alasan Revisi</bsreqlabel> <div class=\"form-group\"> <textarea ng-model=\"The_SelectedRevisiSTNK.AlasanRevisiSTNK\" rows=\"6\" class=\"form-control\" cols=\"83\">\r" +
    "\n" +
    "                            </textarea> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\" style=\"margin-top: 8px; padding:0 5px 0 0\"> <bsreqlabel>Perkiraan Lama Revisi : </bsreqlabel> </div> <div class=\"col-md-2\" style=\"padding: 0 0 0 0\"> <div class=\"form-group\"> <div> <input type=\"text\" number-only maxlength=\"3\" class=\"form-control\" size=\"2\" name=\"LamaKeterlambatanSTNK\" placeholder=\"\" ng-model=\"The_SelectedRevisiSTNK.LamaKeterlambatanSTNK\"> </div> </div> </div> <div class=\"col-md-1\" style=\"margin-top: 8px\"> hari </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"SaveRevisiSTNK()\" ng-disabled=\"(The_SelectedRevisiSTNK.AlasanRevisiSTNK == undefined || The_SelectedRevisiSTNK.AlasanRevisiSTNK == null || The_SelectedRevisiSTNK.AlasanRevisiSTNK == '') ||\r" +
    "\n" +
    "                                                (The_SelectedRevisiSTNK.LamaKeterlambatanSTNK == undefined || The_SelectedRevisiSTNK.LamaKeterlambatanSTNK == null || The_SelectedRevisiSTNK.LamaKeterlambatanSTNK == '')\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"batalRevisiSTNK()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </p> </div> <!-- </div> --> </div> <!-- /////////////////////////////////////////////////////////////////////////// --> <div class=\"ui modal terimaBPKBMonitoring\" role=\"dialog\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaBPKB()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Terima BPBK dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div filedset=\"false\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoRangka\" placeholder=\"\" ng-model=\"rowsDocument.FrameNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Model Kendaraan</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.Description\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Nama BPKB</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.BPKBName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\" style=\"padding: 15px 13px 0 0\"> <label>No BPKB</label> <input type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.BPKBNo\" ng-maxlength=\"50\" required> </div> <div class=\"col-md-6\" style=\"padding: 15px 0 0 13px\"> <label>Nomor Polisi</label> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.PoliceNumber\" ng-maxlength=\"50\" required> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\" style=\"padding: 15px 13px 0 0\"> <label>Tanggal Aju BPKB ke Biro Jasa</label> <input ng-disabled=\"true\" type=\"date\" ng-model=\"rowsDocument.POBiroJasaUnitDate\"> </div> <div class=\"col-md-6\" style=\"padding: 15px 0 0 13px\"> <label>Tanggal Terima BPKB dari Biro Jasa</label> <input type=\"date\" id=\"Max_Da_ReceiveBPKBDateMinDate\" ng-model=\"rowsDocument.ReceiveBPKBDate\"> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"simpanterimaBPKB()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span> <span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\" backFromterimaBPKB()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-times\"></i>&nbsp;Kembali</span> <span class=\"ladda-spinner\"></span> </button> </p> </div> </div> </div> <div class=\"ui modal alasanterlambatbpkbMonitoring\" role=\"dialog\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\"></button> --> <h4 class=\"modal-title\"> <i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Alasan Keterlambatan Penerimaan BPKB dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 15px 0 15px; margin-top: 10px 10px 0 10px\"> <p style=\"width:40px; margin-top:-20px; margin-left:0px; background:white\"> <b>Alasan</b> </p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_FrameNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <label>Alasan Keterlambatan</label> <div class=\"form-group\"> <textarea ng-model=\"SelectedDataTerlambat_AlasanKeterlambatanBPKB\" rows=\"6\" class=\"form-control\" cols=\"83\">\r" +
    "\n" +
    "                                                    </textarea> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\" style=\"margin-top: 8px; padding:0 5px 0 0\"> <label>Lama Keterlambatan : </label> </div> <div class=\"col-md-2\" style=\"padding: 0 0 0 0\"> <div class=\"form-group\"> <div> <input type=\"number\" class=\"form-control\" size=\"2\" name=\"LamaKeterlambatanBPKB\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_LamaKeterlambatanBPKB\"> </div> </div> </div> <div class=\"col-md-1\" style=\"margin-top: 8px\"> Hari </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"simpanAlasanRevisiBPKB()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span> <span class=\"ladda-spinner\"></span> </button> <button ng-click=\"backFromAlasanRevisiBPKB()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </p> </div> </div> </div> <div class=\"ui modal revisibpkbMonitoring\" modal=\"revisibpkb\" role=\"dialog\"> <div> <div class=\"modal-body\"> <p> <h3 align=\"center\"> ALERT </h3> </p> <p align=\"center\"> Apakah Anda ingin Melakukan Revisi BPKB ? </p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"right\" style=\"margin-bottom: 0px !important\"> <button ng-click=\"backFromRevisiBPKB()\" class=\"wbtn btn ng-binding ladda-button\">Tidak</button> <button ng-click=\"SaveRevisiBPKB()\" class=\"rbtn btn ng-binding ladda-button\" style=\"width: 8%\">Ya</button> </p> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/revisiDokumenBiroJasa/revisiDokumenBiroJasa.html',
    "<script type=\"text/javascript\"></script> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"RevisiDokumenBiroJasaMain\" class=\"row\" style=\"margin: 0 0 0 0\"> <button class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:-30px;float:right\" ng-click=\"RevisiDokumenBiroJasaRevisiStnk()\">Revisi STNK</button> <button class=\"rbtn btn ng-binding ladda-button\" style=\"margin-left:15px;margin-top:-30px;float:right\" ng-click=\"RevisiDokumenBiroJasaRevisiBpkb()\">Revisi BPKB</button> <bsform-grid ng-form=\"RevisiDokumenBiroJasaForm\" factory-name=\"RevisiDokumenBiroJasaFactory\" model=\"mRevisiDokumenBiroJasa\" model-id=\"ReceiveId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" show-advsearch=\"on\" form-name=\"RevisiDokumenBiroJasaForm\" form-title=\"Terima STNK Biro Jasa\" modal-title=\"RevisiDokumenBiroJasa\" loading=\"loading\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bslabel>Cabang</bslabel> <div class=\"input-icon-right\"> <bsselect name=\"filterSales\" ng-model=\"filter.OutletId\" data=\"outletCabang\" item-text=\"Name\" item-value=\"OutletId\" placeholder=\"Branch\" icon=\"fa fa-sitemap\"></bsselect> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. PO</label> <div style=\"margin-top:-5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. PO\" ng-model=\"filter.POCode\" maxlength=\"60\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Rangka</label> <div style=\"margin-top:-5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. Rangka\" ng-model=\"filter.FrameNo\" maxlength=\"60\"> </div> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status</label> <bsselect ng-model=\"filter.StatusAFIId\" data=\"OptionsStatusafi\" item-text=\"StatusName\" item-value=\"StatusAFIId\" placeholder=\"Select\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Vendor</label> <bsselect name=\"filterSales\" placeholder=\"Vendor\" ng-model=\"filter.ServiceBureauId\" data=\"VendorRevisiDokumenBiroJasa\" item-text=\"ServiceBureauName,ServiceBureauCode\" item-value=\"ServiceBureauId\" placeholder=\"Select\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bslabel>Tanggal Penerimaan STNK</bslabel> <bsdatepicker style=\"margin-top:5px\" name=\"tanggalFrom\" date-options=\"dateOption\" ng-model=\"filter.SODateStart\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </div> </bsform-advsearch> </bsform-grid> <div class=\"ui modal ModalRevisiDokumenBiroJasaRevisiStnk\" role=\"dialog\" style=\"background-color: transparent !important\"> <!-- <div> --> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Alasan Revisi / Keterlambatan Penerimaan STNK Dari Biro Jasa</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 0 0\"> <p style=\"width:50px; margin-top:-20px; margin-left:14px; background:white\"> <b>Alasan</b> </p> <div> <div class=\"col-md-6\"> <label>No. Rangka</label> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. Rangka\" ng-model=\"FrameNo\" maxlength=\"60\"> </div> </div> <div class=\"col-md-6\"> <label>No. PO</label> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. PO\" ng-model=\"POCode\" maxlength=\"60\"> </div> </div> </div> <div style=\"margin-top:10px\"> <div class=\"col-md-6\"> <label>Unit Model Kendaraan</label> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. Rangka\" ng-model=\"FrameNo\" maxlength=\"60\"> </div> </div> </div> <div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <label>Alasan Revisi/Keterlambatan</label> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <textarea style=\"width:100%;resize: none\" rows=\"10\" cols=\"50\" maxlength=\"200\" placeholder=\" input alasan...\" ng-model=\"selected_data.ReasonNote\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t</textarea> </div> </div> </div> <div> <div class=\"col-md-6\" style=\"margin-top:10px\"> Lama Keterlambatan:<input ng-model=\"LamaKeterlambatan\" number-only type=\"text\" class=\"form-control\"> </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <!-- <button ng-click=\"testing()\">tes</button> --> <button type=\"button\" ng-disabled=\"validSTNKBirojasa.$invalid\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"SimpanModalRevisiDokumenBiroJasaRevisiStnk()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"KembaliModalRevisiDokumenBiroJasaRevisiStnk()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Kembali</span><span class=\"ladda-spinner\"></span> </button> </div> </p> <!-- </div> --> </div> <div class=\"ui modal ModalRevisiDokumenBiroJasaRevisiBpkb\" role=\"dialog\" style=\"background-color: transparent !important\"> <!-- <div> --> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Alasan Revisi BPKB Dari Biro Jasa</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 0 0\"> <p style=\"width:50px; margin-top:-20px; margin-left:14px; background:white\"> <b>Alasan</b> </p> <div> <div class=\"col-md-5\"> <label>No. Rangka</label> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. Rangka\" ng-model=\"FrameNo\" maxlength=\"60\"> </div> </div> <div class=\"col-md-5\"> <label>No. PO</label> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. PO\" ng-model=\"POCode\" maxlength=\"60\"> </div> </div> </div> <div style=\"margin-top:10px\"> <div class=\"col-md-5\"> <label>Unit Model Kendaraan</label> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. Rangka\" ng-model=\"FrameNo\" maxlength=\"60\"> </div> </div> </div> <div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <label>Alasan Keterlambatan</label> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <textarea style=\"width:100%;resize: none\" rows=\"10\" cols=\"50\" maxlength=\"200\" placeholder=\" input alasan...\" ng-model=\"selected_data.ReasonNote\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t</textarea> </div> </div> </div> <div> <div class=\"col-md-6\" style=\"margin-top:10px\"> Lama Keterlambatan:<input ng-model=\"LamaKeterlambatan\" number-only type=\"text\" class=\"form-control\"> </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <!-- <button ng-click=\"testing()\">tes</button> --> <button type=\"button\" ng-disabled=\"validSTNKBirojasa.$invalid\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"SimpanModalRevisiDokumenBiroJasaRevisiBpkb()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"KembaliModalRevisiDokumenBiroJasaRevisiBpkb()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Kembali</span><span class=\"ladda-spinner\"></span> </button> </div> </p> <!-- </div> --> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/serahTerimaFakturDanBpkbKePelanggan/serahTerimaFakturDanBpkbKePelanggan.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "        $('body .modals').remove();\r" +
    "\n" +
    "    }</script> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">.action-right {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "    }</style> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script src=\"js/uiscript.js\" type=\"text/javascript\"></script> <script src=\"/release/ui-grid.min.js\"></script> <div ng-show=\"ShowSerahTerimaFakturDanBpkbKePelangganMain\" class=\"container\" style=\"width: 100%\"> <button type=\"button\" class=\"rbtn btn\" ng-click=\"onLanjutClick()\" style=\"margin: -31px 0 0 8px; float: right\" tabindex=\"0\" ng-disabled=\"selected_rows.length<=0\"> <i class=\"fa fa-fw fa-arrow-right\"></i>&nbsp;Lanjut </button> <bsform-grid ng-form=\"SerahTerimaFakturDanBpkbKePelangganForm\" factory-name=\"SerahTerimaFakturDanBpkbKePelangganFactory\" model=\"mSerahTerimaFakturDanBpkbKePelanggan\" model-id=\"SuratDistribusiBPKBHeaderId\" loading=\"loading\" hide-new-button=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" grid-hide-action-column=\"true\" form-name=\"SerahTerimaFakturDanBpkbKePelangganForm\" form-title=\"SerahTerimaFakturDanBpkbKePelanggan\" modal-title=\"Serah Terima Faktur Dan Bpkb Ke Pelanggan\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Insurance Type Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"insuranceTypeName\" placeholder=\"Insurance Type Name\" ng-model=\"mSerahTerimaFakturDanBpkbKePelanggan.InsuranceTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"insuranceTypeName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Insurance Type Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"insuranceTypeValue\" placeholder=\"Insurance Type Value\" ng-model=\"mSerahTerimaFakturDanBpkbKePelanggan.InsuranceTypeValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"insuranceTypeValue\"></bserrmsg> </div> </div> </div> <bsform-advsearch> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama Pembeli / Leasing</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nama Pembeli / Leasing\" ng-model=\"filterSTBPKBMain.NamaPembeli\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nomor Rangka\" ng-model=\"filterSTBPKBMain.FrameNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. SO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nomor Sales Order\" ng-model=\"filterSTBPKBMain.SOno\" ng-maxlength=\"50\"> </div> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"ShowCetakSerahTerimaFakturDanBpkbKePelanggan\"> <div class=\"button-atas\" style=\"margin-bottom: 20px\"> <button type=\"button\" class=\"rbtn btn\" ng-click=\"onCetakClick()\" style=\"margin: -31px 0 0 8px; float: right\" tabindex=\"0\"> <i class=\"fa fa-fw fa-print\"></i>&nbsp; Cetak </button> <button type=\"button\" class=\"wbtn btn\" ng-click=\"onKembaliClick()\" style=\"margin: -31px 0 0 8px; float: right\" tabindex=\"0\"> Kembali </button> </div> <br> <div class=\"row\" style=\"margin-bottom: 15px\"> <div class=\"col-md-3\"> Tanggal Penyerahan Dokumen <span style=\"color: red\">*</span> </div> <div class=\"col-md-3\"> <bsdatepicker name=\"tanggalTo\" date-options=\"dateOption\" ng-model=\"TanggalPenyerahanDokumenByCustom\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> </div> <div ui-grid=\"gridBPKBCetak\" ui-grid-selection class=\"grid\" ui-grid-cellnav ui-grid-edit ui-grid-pagination ui-grid-auto-resize ng-show=\"ShowCetakSerahTerimaFakturDanBpkbKePelanggan\"></div> </div> <div class=\"ui modal UpdatePenyerahanDokumen\" role=\"dialog\" style=\"background-color: transparent !important; width: 600px\"> <!-- <div> --> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Penyerahan Dokumen BPKB Ke Pelanggan</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"><b>Terima Dokumen</b></p> <ul class=\"list-group\"> <!-- --- --> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">No. Rangka</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"No Rangka\" ng-model=\"EditDetilTandaTerima.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">Pembayaran</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" class=\"form-control\" placeholder=\"Jenis Penbayaran\" ng-model=\"EditDetilTandaTerima.FundSourceName\" maxlength=\"50\" required> </div> </div> </div> <!-- clear --> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px;margin-left: 10px;margin-right: 10px\">Model Tipe Kendaraan</bsreqlabel> <div style=\"margin-left: 10px;margin-right: 10px\" class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"Model Tipe Kendaraan\" ng-model=\"EditDetilTandaTerima.Description\" ng-maxlength=\"50\" required> </div> </div> <!-- -- clear-- --> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px;margin-left: 10px;margin-right: 10px\">Nama BPKB</bsreqlabel> <div style=\"margin-left: 10px;margin-right: 10px\" class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" type=\"text\" alpha-numeric mask=\"Huruf\" class=\"form-control\" name=\"NamaPembeli\" placeholder=\"Nama Penerima\" ng-model=\"EditDetilTandaTerima.listDetail[0].BPKBName\" maxlength=\"50\" required> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px;margin-left: 10px;margin-right: 10px\">Nama Penerima Pelanggan / Leasing</bsreqlabel> <div style=\"margin-left: 10px;margin-right: 10px\" class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" type=\"text\" alpha-numeric mask=\"Huruf\" class=\"form-control\" name=\"NamaPembeli\" placeholder=\"Nama Penerima\" ng-model=\"EditDetilTandaTerima.NamaPembeli\" maxlength=\"50\" required> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">No. KTP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" number-only type=\"text\" class=\"form-control\" name=\"NoKTP\" placeholder=\"No KTP\" ng-model=\"EditDetilTandaTerima.NoKTPPembeli\" maxlength=\"16\" required> </div> </div> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">No HandPhone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" number-only type=\"text\" class=\"form-control\" name=\"NoHandPhone\" placeholder=\"No HandPhone\" ng-model=\"EditDetilTandaTerima.NoHPPembeli\" maxlength=\"12\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">No. BPKB</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" class=\"form-control\" name=\"NoBpkb\" placeholder=\"No Bpkb\" ng-model=\"EditDetilTandaTerima.listDetail[0].BPKBNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">Nomor Polisi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" number-only type=\"text\" class=\"form-control\" name=\"NoHandPhone\" placeholder=\"Nomor Polisi\" ng-model=\"EditDetilTandaTerima.listDetail[0].PoliceNumber\" maxlength=\"12\" required> </div> </div> </div> <div class=\"col-md-12\"> <hr> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">Tanggal Penyerahan Dokumen</bsreqlabel> </div> <div class=\"col-md-6\"> <bsdatepicker name=\"tanggalTo\" date-options=\"dateOption\" ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" ng-model=\"EditDetilTandaTerima.listDetail[0].TanggalPenyerahanDokumen\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> </div> </ul> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <bs-print cetakan-url=\"{{printUrl}}\"> <bsbutton-print> <button class=\"rbtn btn\" style=\"float:right\" ng-disabled=\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.FrameNo == undefined || EditDetilTandaTerima.FrameNo == null || EditDetilTandaTerima.FrameNo =='') ||   \r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.listDetail[0].BPKBNo == undefined || EditDetilTandaTerima.listDetail[0].BPKBNo == null || EditDetilTandaTerima.listDetail[0].BPKBNo =='') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.FundSourceName == null || EditDetilTandaTerima.FundSourceName == '' || EditDetilTandaTerima.FundSourceName == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.listDetail[0].BPKBName == null || EditDetilTandaTerima.listDetail[0].BPKBName == '' || EditDetilTandaTerima.listDetail[0].BPKBName == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.NamaPembeli == undefined || EditDetilTandaTerima.NamaPembeli == null || EditDetilTandaTerima.NamaPembeli == '') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.NoKTPPembeli == undefined || EditDetilTandaTerima.NoKTPPembeli == null || EditDetilTandaTerima.NoKTPPembeli == '') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.Description == null || EditDetilTandaTerima.Description == '' || EditDetilTandaTerima.Description == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.NoHPPembeli == undefined || EditDetilTandaTerima.NoHPPembeli == null || EditDetilTandaTerima.NoHPPembeli == '') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.NoKTPPembeli == null || EditDetilTandaTerima.NoKTPPembeli == '' || EditDetilTandaTerima.NoKTPPembeli == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.listDetail[0].PoliceNumber == null || EditDetilTandaTerima.listDetail[0].PoliceNumber == '' || EditDetilTandaTerima.listDetail[0].PoliceNumber == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.listDetail[0].TanggalPenyerahanDokumen == null || EditDetilTandaTerima.listDetail[0].TanggalPenyerahanDokumen == '' || EditDetilTandaTerima.listDetail[0].TanggalPenyerahanDokumen == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                EditDetilTandaTerima.listDetail[0].SerahTerimaSTNKBPKBStatusName != 'Sudah Diterima Pelanggan' \r" +
    "\n" +
    "\r" +
    "\n" +
    "                    \"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i>&nbsp&nbspCetak Tanda Terima </button> </bsbutton-print> </bs-print> <button style=\"float: right;margin-bottom:5px;margin-left:5px;margin-right:5px\" ng-click=\"UpdatePenyerahanDokumen()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" ng-disabled=\"\r" +
    "\n" +
    "                                (EditDetilTandaTerima.FrameNo == undefined || EditDetilTandaTerima.FrameNo == null || EditDetilTandaTerima.FrameNo =='') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.listDetail[0].BPKBNo == undefined || EditDetilTandaTerima.listDetail[0].BPKBNo == null || EditDetilTandaTerima.listDetail[0].BPKBNo =='') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.FundSourceName == null || EditDetilTandaTerima.FundSourceName == '' || EditDetilTandaTerima.FundSourceName == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.listDetail[0].BPKBName == null || EditDetilTandaTerima.listDetail[0].BPKBName == '' || EditDetilTandaTerima.listDetail[0].BPKBName == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.NamaPembeli == undefined || EditDetilTandaTerima.NamaPembeli == null || EditDetilTandaTerima.NamaPembeli == '') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.NoKTPPembeli == undefined || EditDetilTandaTerima.NoKTPPembeli == null || EditDetilTandaTerima.NoKTPPembeli == '') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.Description == null || EditDetilTandaTerima.Description == '' || EditDetilTandaTerima.Description == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.NoHPPembeli == undefined || EditDetilTandaTerima.NoHPPembeli == null || EditDetilTandaTerima.NoHPPembeli == '') ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.NoKTPPembeli == null || EditDetilTandaTerima.NoKTPPembeli == '' || EditDetilTandaTerima.NoKTPPembeli == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.listDetail[0].PoliceNumber == null || EditDetilTandaTerima.listDetail[0].PoliceNumber == '' || EditDetilTandaTerima.listDetail[0].PoliceNumber == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                (EditDetilTandaTerima.listDetail[0].TanggalPenyerahanDokumen == null || EditDetilTandaTerima.listDetail[0].TanggalPenyerahanDokumen == '' || EditDetilTandaTerima.listDetail[0].TanggalPenyerahanDokumen == undefined) ||\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                EditDetilTandaTerima.listDetail[0].SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan' \r" +
    "\n" +
    "\r" +
    "\n" +
    "                            \"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Simpan </span><span class=\"ladda-spinner\"></span> </button> <!-- <button style=\"float: right;margin-bottom:5px;margin-left:5px;margin-right:5px;\" ng-click=\"debugConsole()\"  type=\"button\" class=\"rbtn btn ng-binding ladda-button\" >\r" +
    "\n" +
    "                <span class=\"ladda-label\">\r" +
    "\n" +
    "                    Debug !\r" +
    "\n" +
    "                </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "            </button> --> <button style=\"float: right;margin-bottom:5px;margin-left:5px;margin-right:5px\" ng-click=\"KembaliKeTerimaBPKBKePelangganMain()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> </p> <!-- </div> --> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/serahTerimaStnkKePelanggan/serahTerimaStnkKePelanggan.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "        $('body .modals').remove();\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\tfunction RestrictSpace() {\r" +
    "\n" +
    "\t\tif (event.keyCode == 32) {\r" +
    "\n" +
    "\t\t\treturn false;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}</script> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">.action-custom {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <div ng-show=\"ShowTerimaSTNKKePelangganMain\" style=\"width: 100%\"> <bsform-grid ng-form=\"SerahTerimaStnkKePelangganForm\" factory-name=\"SerahTerimaStnkKePelangganFactory\" model=\"mSerahTerimaStnkKePelanggan\" model-id=\"SuratDistribusiSTNKHeaderId\" loading=\"loading\" hide-new-button=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" show-advsearch=\"on\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" form-name=\"SerahTerimaStnkKePelangganForm\" form-title=\"SerahTerimaStnkKePelanggan\" modal-title=\"Serah Terima Stnk Ke Pelanggan\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin-bottom:25px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama STNK</label> <div style=\"margin-top:-5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. STNK\" ng-model=\"filter.STNKName\" maxlength=\"60\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Rangka</label> <div style=\"margin-top:-5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. Rangka\" ng-model=\"filter.FrameNo\" maxlength=\"60\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Billing</label> <div style=\"margin-top:-5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input No. Billing\" ng-model=\"filter.BillingCode\" maxlength=\"60\"> </div> </div> </div> </div> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Insurance Type Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"insuranceTypeName\" placeholder=\"Insurance Type Name\" ng-model=\"mSerahTerimaStnkKePelanggan.InsuranceTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"insuranceTypeName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Insurance Type Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"insuranceTypeValue\" placeholder=\"Insurance Type Value\" ng-model=\"mSerahTerimaStnkKePelanggan.InsuranceTypeValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"insuranceTypeValue\"></bserrmsg> </div> </div> </div> </bsform-grid> </div> <div class=\"ui modal UpdatePenerimaanDokumenstnkkepelanggan\" role=\"dialog\" style=\"background-color: transparent !important; width: 600px\"> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Penyerahan Dokumen STNK ke Pelanggan</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"><b>&nbsp;Terima Dokumen</b></p> <form name=\"validSerahTerimaStnkPelanggan\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">No. Rangka</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"No Rangka\" ng-model=\"EditDetilTandaTerima.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">Pembayaran</bsreqlabel> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"Pembayaran\" placeholder=\"Pembayaran\" ng-model=\"The_Result.FundSourceName\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px;margin-left: 10px;margin-right: 10px\">Model Tipe Kendaraan</bsreqlabel> <div style=\"margin-left: 10px;margin-right: 10px\" class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"ModelTipeKendaraan\" placeholder=\"Model Tipe Kendaraan\" ng-model=\"The_Result.Description\" required> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px;margin-left: 10px;margin-right: 10px\">Nama Pembeli</bsreqlabel> <div style=\"margin-left: 10px;margin-right: 10px\" class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NamaPembeli\" placeholder=\"Nama Pembeli\" ng-model=\"The_Result.NamaPembeli\" maxlength=\"50\" required> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">No KTP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input number-only ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"NoKTP\" placeholder=\"No KTP\" ng-model=\"The_Result.NoKTPPembeli\" required> </div> </div> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-bottom: 5px\">No Handphone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input number-only ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"NoHandPhone\" placeholder=\"No HandPhone\" ng-model=\"The_Result.NoHPPembeli\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <bsreqlabel style=\"margin-bottom: 5px;margin-left: 10px;margin-right: 10px\">Nama STNK</bsreqlabel> <div style=\"margin-left: 10px;margin-right: 10px\" class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"NamaSTNK\" placeholder=\"Nama STNK\" ng-model=\"The_Result.STNKName\" required> </div> </div> <table class=\"table table-bordered table-responsive\" border=\"1\" style=\"border: none !important\"> <tbody style=\"border: none !important\"> <tr> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\"> <input style=\"margin-right:5px\" ng-disabled=\"true\" ng-checked=\"EditDetilTandaTerima.PoliceNumber!='' && EditDetilTandaTerima.PoliceNumber!=undefined && EditDetilTandaTerima.STNKNo!='' && EditDetilTandaTerima.STNKNo!=undefined\" type=\"checkbox\" ng-model=\"SerahTerimaStnksKePelangganCheckAll\"> <label>Dokumen</label> </th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\"></th> </tr> <tr> <td style=\"border: none !important\"> <input style=\"margin-right:5px\" ng-disabled=\"true\" ng-checked=\"EditDetilTandaTerima.STNKNo!='' && EditDetilTandaTerima.STNKNo!=undefined\" type=\"checkbox\" ng-model=\"SerahTerimaStnkKePelangganCekBoxNomorStnk\"> <bsreqlabel>Nomor STNK</bsreqlabel> </td> <td style=\"border: none !important\"> <div style=\"margin-left: 10px;margin-right: 10px\" class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" type=\"text\" class=\"form-control\" name=\"NoStnk\" placeholder=\"No STNK\" ng-model=\"EditDetilTandaTerima.STNKNo\" ng-maxlength=\"50\" required> </div> </td> </tr> <tr> <td style=\"border: none !important\"> <input style=\"margin-right:5px\" ng-disabled=\"true\" ng-checked=\"EditDetilTandaTerima.PoliceNumber!='' && EditDetilTandaTerima.PoliceNumber!=undefined\" type=\"checkbox\" ng-model=\"SerahTerimaStnkKePelangganCekBoxNomorPolisi\"> <bsreqlabel>Plat Nomor Polisi</bsreqlabel> </td> <td style=\"border: none !important\"> <div style=\"margin-left: 10px;margin-right: 10px\" class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input onkeypress=\"return RestrictSpace()\" alpha-numeric ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" type=\"text\" class=\"form-control\" name=\"NoPolisi\" placeholder=\"No Polisi\" ng-model=\"EditDetilTandaTerima.PoliceNumber\" maxlength=\"10\" required> </div> </td> </tr> <tr> <td style=\"border: none !important\"> <input ng-change=\"SerahTerimaStnkKePelangganNoticePajakCentang()\" style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" ng-model=\"EditDetilTandaTerima.NoticePajakBit\"> <bsreqlabel>Notice Pajak</bsreqlabel> </td><td style=\"border: none !important\"> </td> </tr> </tbody> </table> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-20px; margin-left:14px; background:white\"> <b>&nbsp; Item Notice Pajak</b> </p> <table class=\"table table-bordered table-responsive\" border=\"1\" style=\"border: none !important\"> <tbody style=\"border: none !important\"> <tr> <td style=\"border: none !important\"> <bslabel>BBNKB</bslabel> </td> <td style=\"border: none !important\"> <input ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan' || EditDetilTandaTerima.NoticePajakBit==false\" type=\"text\" number-only class=\"form-control\" ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" ng-model=\"EditDetilTandaTerima.BBNKB\" required> </td> </tr> <tr> <td style=\"border: none !important\"> <bslabel>PKB</bslabel> </td> <td style=\"border: none !important\"> <input ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan' || EditDetilTandaTerima.NoticePajakBit==false\" type=\"text\" number-only class=\"form-control\" ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" ng-model=\"EditDetilTandaTerima.PKB\" required> </td> </tr> <tr> <td style=\"border: none !important\"> <bslabel>SWDKLJJ</bslabel> </td> <td style=\"border: none !important\"> <input ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan' || EditDetilTandaTerima.NoticePajakBit==false\" type=\"text\" number-only class=\"form-control\" ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" ng-model=\"EditDetilTandaTerima.SWDKLLJ\" required> </td> </tr> <tr> <td style=\"border: none !important\"> <bslabel>Biaya ADM STNK</bslabel> </td> <td style=\"border: none !important\"> <input ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan' || EditDetilTandaTerima.NoticePajakBit==false\" type=\"text\" number-only class=\"form-control\" ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" ng-model=\"EditDetilTandaTerima.BiayaAdmSTNK\" required> </td> </tr> <tr> <td style=\"border: none !important\"> <bslabel>Biaya ADM TNKB</bslabel> </td> <td style=\"border: none !important\"> <input ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan' || EditDetilTandaTerima.NoticePajakBit==false\" type=\"text\" number-only class=\"form-control\" ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" ng-model=\"EditDetilTandaTerima.BiayaAdmTNKB\" required> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px; margin-top: 10px\"> <div class=\"col-md-6\"> <label style=\"margin-top: 5px\">Tanggal Penyerahan Dokumen</label> </div> <div class=\"col-md-6\"> <div class=\"input-icon-right\"> <i class=\"fa fa-calendar\"></i> <input ng-disabled=\"EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" class=\"form-control\" ng-model=\"EditDetilTandaTerima.TanggalPenyerahanDokumen\" max=\"{{TodayDate}}\" type=\"datetime-local\"> </div> </div> </div> </ul> </form> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button style=\"float: right;margin-bottom:5px;margin-left:5px;margin-right:5px\" ng-click=\"SimpanUpdatePenerimaanDokumen()\" type=\"button\" ng-disabled=\"(EditDetilTandaTerima.STNKNo == undefined || EditDetilTandaTerima.STNKNo == null || EditDetilTandaTerima.STNKNo == '') || EditDetilTandaTerima.NoticePajakBit!=true ||\r" +
    "\n" +
    "                             (EditDetilTandaTerima.PoliceNumber == undefined || EditDetilTandaTerima.PoliceNumber == null || EditDetilTandaTerima.PoliceNumber == '') || EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Sudah Diterima Pelanggan'\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Simpan </span><span class=\"ladda-spinner\"></span> </button> <bs-print cetakan-url=\"{{printUrl}}\"> <bsbutton-print> <!-- <button class=\"rbtn btn\" style=\"float:right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-print\" aria-hidden=\"true\"></i>&nbsp&nbspCetak Tanda Terima\r" +
    "\n" +
    "                    </button> --> <button class=\"rbtn btn\" style=\"float:right\" ng-disabled=\"(EditDetilTandaTerima.STNKNo == undefined || EditDetilTandaTerima.STNKNo == null || EditDetilTandaTerima.STNKNo == '') ||\r" +
    "\n" +
    "                            (EditDetilTandaTerima.PoliceNumber == undefined || EditDetilTandaTerima.PoliceNumber == null || EditDetilTandaTerima.PoliceNumber == '') || EditDetilTandaTerima.SerahTerimaSTNKBPKBStatusName == 'Belum Diterima Pelanggan'\"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i>&nbsp&nbspCetak Tanda Terima </button> </bsbutton-print> </bs-print> <!-- <button style=\"float: right;margin-bottom:5px;margin-left:5px;margin-right:5px;\" ng-click=\"debugConsole()\" type=\"button\"\r" +
    "\n" +
    "            class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "                <span class=\"ladda-label\">\r" +
    "\n" +
    "                    Debug !\r" +
    "\n" +
    "                </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "            </button> --> <!-- <button style=\"float: right;margin-bottom:5px;margin-left:5px;margin-right:5px;\" ng-click=\"KeTerimaSTNKKePelangganCetak()\"  type=\"button\" class=\"rbtn btn ng-binding ladda-button\" >\r" +
    "\n" +
    "                <span class=\"ladda-label\">\r" +
    "\n" +
    "                    Cetak Tanda Terimas\r" +
    "\n" +
    "        </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "        </button> --> <button style=\"float: right;margin-bottom:5px;margin-left:5px;margin-right:5px\" ng-click=\"KembaliKeTerimaSTNKKePelangganMain()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> <!-- <button style=\"float: right;margin-bottom:5px;margin-left:5px;margin-right:5px;\" ng-click=\"DebugSTNK()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "                <span class=\"ladda-label\"> --> <!--<i class=\"fa fa-fw fa-refresh\"></i>--> <!-- </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "            </button> --> </div> </p> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/terimaBpkb/terimaBpkb.html',
    "<script type=\"text/javascript\">function SetReceiveBPKBDateMinDate(TheDate) {\r" +
    "\n" +
    "        var Da_ReceiveBPKBDateMinDate = TheDate;\r" +
    "\n" +
    "        var dd = Da_ReceiveBPKBDateMinDate.getDate();\r" +
    "\n" +
    "        var mm = Da_ReceiveBPKBDateMinDate.getMonth() + 1; //January is 0!\r" +
    "\n" +
    "        var yyyy = Da_ReceiveBPKBDateMinDate.getFullYear();\r" +
    "\n" +
    "        if (dd < 10) {\r" +
    "\n" +
    "            dd = '0' + dd\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (mm < 10) {\r" +
    "\n" +
    "            mm = '0' + mm\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        Da_ReceiveBPKBDateMinDate = yyyy + '-' + mm + '-' + dd;\r" +
    "\n" +
    "        document.getElementById(\"Max_Da_ReceiveBPKBDateMinDate\").setAttribute(\"min\", Da_ReceiveBPKBDateMinDate);\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    $(function() {\r" +
    "\n" +
    "    $('#bpkb').on('keypress', function(e) {\r" +
    "\n" +
    "        if (e.which == 32)\r" +
    "\n" +
    "            return false;\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "});</script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"daftarPO\" class=\"row\" style=\"margin: 0 0 0 0\"> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"terimaBPKB()\" style=\"float: right; margin: -30px 0 0 7px\">&nbsp <span>Terima BPKB</span> </button> <button id=\"sendEmailBtn\" class=\"wbtn btn ng-binding ladda-button\" ng-click=\"batalTerimaBPKB()\" style=\"float: right; margin: -30px 0 0 7px\">&nbsp <span>Batal Terima BPKB</span> </button> <bsform-grid ng-form=\"TerimaBpkbForm\" factory-name=\"TerimaBpkbFactory\" model=\"mTerimaBpkb\" model-id=\"ReceiveId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" form-name=\"TerimaBpkbForm\" show-advsearch=\"on\" form-title=\"Terima BPKB Biro Jasa\" loading=\"loading\" modal-title=\"TerimaBpkb\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Cabang</label> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                    <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Pilih Cabang\" ng-model=\"filter.SuratDistribusiNo\" ng-maxlength=\"50\">\r" +
    "\n" +
    "                                </div> --> <div class=\"input-icon-right\"> <bsselect name=\"filterSales\" ng-model=\"filter.OutletId\" data=\"outletCabang\" item-text=\"Name\" item-value=\"OutletId\" placeholder=\"Branch\" icon=\"fa fa-sitemap\"></bsselect> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. PO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. PO\" ng-model=\"filter.POBiroJasaUnitNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. Rangka\" ng-model=\"filter.FrameNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status</label> <bsselect name=\"filterSales\" ng-model=\"filter.StatusAFIId\" data=\"statusafi\" item-text=\"StatusName\" item-value=\"StatusAFIId\" placeholder=\"Select\"> </bsselect> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama STNK</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nama STNK\" ng-model=\"filter.STNKName\" maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Vendor</label> <bsselect name=\"filterSales\" placeholder=\"Vendor\" ng-model=\"filter.ServiceBureauId\" data=\"vendor\" item-text=\"ServiceBureauName,ServiceBureauCode\" item-value=\"ServiceBureauId\" placeholder=\"Select\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tanggal Penerimaan</label> <bsdatepicker name=\"tanggalFrom\" date-options=\"dateOptionsStart\" ng-model=\"filter.TanggalPenerimaan\" ng-change=\"tanggalmin(filter.StartSentDate)\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </div> </bsform-advsearch> </bsform-grid> <div class=\"ui small modal terimaBPKB\" role=\"dialog\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaBPKB()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Terima BPKB dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div filedset=\"false\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoRangka\" placeholder=\"\" ng-model=\"rowsDocument.FrameNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Model Kendaraan</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.Description\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Nama BPKB</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.BPKBName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <hr> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\" style=\"padding: 15px 13px 0 0\"> <bsreqlabel>No BPKB</bsreqlabel> <input id=\"bpkb\" alpha-numeric mask=\"BPKB\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.BPKBNo\" maxlength=\"10\" alpha-numeric required> </div> <div class=\"col-md-6\" style=\"padding: 15px 0 0 13px\"> <label>Nomor Polisi</label> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.PoliceNumber\" ng-maxlength=\"50\" required> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\" style=\"padding: 15px 13px 0 0\"> <label>Tanggal Aju BPKB ke Biro Jasa</label><br> <input class=\"form-control\" ng-disabled=\"true\" type=\"date\" ng-model=\"rowsDocument.POBiroJasaUnitDate\"> </div> <div class=\"col-md-6\" style=\"padding: 15px 0 0 13px\"> <label>Tanggal Terima BPKB dari Biro Jasa</label><br> <input class=\"form-control\" type=\"date\" id=\"Max_Da_ReceiveBPKBDateMinDate\" min=\"2000-13-13\" ng-model=\"rowsDocument.ReceiveBPKBDate\"> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-disabled=\"rowsDocument.ReceiveBPKBDate == null || rowsDocument.BPKBNo == undefined\" ng-click=\"simpanterimaBPKB()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"backFromterimaBPKB()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-time\"></i>&nbsp;Keluar</span><span class=\"ladda-spinner\"></span> </button> </p> </div> </div> </div> <div class=\"ui small modal alasanterlambatBPKB\" modal=\"alasanterlambat\" role=\"dialog\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\"></button> --> <h4 class=\"modal-title\"><i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Alasan Keterlambatan Penerimaan BPKB dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 15px 0 15px; margin-top: 10px 10px 0 10px\"> <p style=\"width:50px; padding:0 5px 0 5px; margin-top:-18px; margin-bottom:15px; background:white\"><b>Alasan</b></p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\" style=\"padding-right: 0 !important\"> <label>PO Number</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_PONo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important; padding-top: 10px !important\"> <label>Model</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_Model\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <label>Alasan Keterlambatan</label> <div class=\"form-group\"> <textarea ng-model=\"SelectedDataTerlambat_AlasanKeterlambatanBPKB\" rows=\"6\" class=\"form-control\" cols=\"83\">\r" +
    "\n" +
    "                                                        </textarea> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\" style=\"margin-top: 8px; padding:0 5px 0 0\"> <label>Perkiraan Lama Keterlambatan : </label> </div> <div class=\"col-md-2\" style=\"padding: 0 0 0 0\"> <div class=\"form-group\"> <div> <input type=\"number\" class=\"form-control\" size=\"2\" name=\"LamaKeterlambatanBPKB\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_LamaKeterlambatanBPKB\"> </div> </div> </div> <div class=\"col-md-1\" style=\"margin-top: 8px\"> Hari </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"simpanAlasanRevisi()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"backFromAlasanRevisi()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </p> </div> </div> </div> <div class=\"ui small modal alasanrevisiBPKB\" modal=\"alasanterlambat\" role=\"dialog\"> <!-- <div> --> <div class=\"modal-header\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\"></button> <h4 class=\"modal-title\"><i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Alasan Revisi Penerimaan BPKB dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 15px 0 15px; margin-top: 10px 10px 0 10px\"> <p style=\"width:50px; padding:0 5px 0 5px; margin-top:-18px; margin-bottom:15px; background:white\"><b>Alasan</b></p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataRevisiBPKB.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\" style=\"padding-right: 0 !important\"> <label>Nomor PO</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"SelectedDataRevisiBPKB.POBiroJasaUnitNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important; padding-top: 10px !important\"> <label>Model</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataRevisiBPKB.VehicleModelName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <bsreqlabel>Alasan Revisi</bsreqlabel> <div class=\"form-group\"> <textarea ng-model=\"SelectedDataRevisiBPKB.AlasanRevisiBPKB\" rows=\"6\" class=\"form-control\" cols=\"83\">\r" +
    "\n" +
    "                            </textarea> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\" style=\"margin-top: 8px; padding:0 5px 0 0\"> <bsreqlabel>Perkiraan Lama Revisi : </bsreqlabel> </div> <div class=\"col-md-2\" style=\"padding: 0 0 0 0\"> <div class=\"form-group\"> <div> <input type=\"text\" number-only maxlength=\"3\" class=\"form-control\" size=\"2\" name=\"LamaKeterlambatanSTNK\" placeholder=\"\" ng-model=\"SelectedDataRevisiBPKB.LamaKeterlambatanBPKB\"> </div> </div> </div> <div class=\"col-md-1\" style=\"margin-top: 8px\"> Hari </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"SaveRevisiBPKB()\" ng-disabled=\"(SelectedDataRevisiBPKB.AlasanRevisiBPKB == undefined || SelectedDataRevisiBPKB.AlasanRevisiBPKB == null || SelectedDataRevisiBPKB.AlasanRevisiBPKB == '') ||\r" +
    "\n" +
    "                                                (SelectedDataRevisiBPKB.LamaKeterlambatanBPKB == undefined || SelectedDataRevisiBPKB.LamaKeterlambatanBPKB == null || SelectedDataRevisiBPKB.LamaKeterlambatanBPKB == '')\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"batalRevisiBPKB()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </p> </div> <!-- </div> --> </div> <div class=\"ui small modal revisibpkb\" modal=\"revisibpkb\" role=\"dialog\"> <div> <div class=\"modal-body\"> <p> <h3 align=\"center\"> Peringatan </h3> </p> <p align=\"center\"> Apakah Anda ingin Melakukan Revisi BPKB ? </p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"right\" style=\"margin-bottom: 0px !important\"> <button ng-click=\"backFromRevisiBPKB()\" class=\"wbtn btn ng-binding ladda-button\">Tidak</button> <button ng-click=\"openRevisiBPKB()\" class=\"rbtn btn ng-binding ladda-button\" style=\"width: 8%\">Ya</button> </p> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/terimaDokumenBpkb/terimaDokumenBpkb.html',
    "<script type=\"text/javascript\">// function IfGotProblemWithModal() {\r" +
    "\n" +
    "\t// \t$('body .modals').remove();\r" +
    "\n" +
    "\t// }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\tfunction justNumber(key){\r" +
    "\n" +
    "        return key == 'ArrowLeft' || key == 'ArrowRight' || key == 'Delete' || key == 'Tab';\r" +
    "\n" +
    "    }</script> <style type=\"text/css\">input[type=\"date\"]::-webkit-inner-spin-button{\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "        -webkit-appearance: none;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <div ng-show=\"ShowTerimaDokumenBPKBMain\" class=\"row\" style=\"width: 100%; margin: 0 0 0 0\"> <bsform-grid ng-form=\"TerimaDokumenBpkbForm\" factory-name=\"TerimaDokumenBpkbFactory\" model=\"mTerimaDokumenBpkb\" model-id=\"SuratDistribusiBPKBHeaderId\" loading=\"loading\" show-advsearch=\"on\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" hide-new-button=\"true\" form-name=\"TerimaDokumenBpkbForm\" form-title=\"TerimaDokumenBpkb\" modal-title=\"Terima Dokumen Bpkb\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Status</label> <bsselect name=\"status\" placeholder=\"Status\" ng-model=\"filter.TerimaDokumenSTNKBPKBStatusId\" data=\"getStatusDokumen\" item-text=\"TerimaDokumenSTNKBPKBStatusName\" item-value=\"TerimaDokumenSTNKBPKBStatusId\" required icon=\"fa fa-map-marker \"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input alpha-numeric type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Rangka\" ng-model=\"filter.FrameNo\" maxlength=\"30\"> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nomor Surat Distribusi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Surat Distribusi\" ng-model=\"filter.SuratDistribusiBPKBNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input alpha-numeric mask=\"Huruf\" type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nama Pelanggan\" ng-model=\"filter.CustomerName\" ng-maxlength=\"50\"> </div> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"ShowDetilSuratDistribusiBpkb\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <button style=\"float: right;margin: -30px 0 0 0\" ng-click=\"KembaliKeTerimaDokumenBpkbMain()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"grid2\" ui-grid-pagination ui-grid-auto-resize></div> </div> </div> <div class=\"ui modal UpdatePenerimaanDokumen\" role=\"dialog\" style=\"background-color: transparent !important; width: 600px\"> <!-- <div> --> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Update Penerimaan Dokumen</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"><b>Terima Dokumen</b></p> <ul class=\"list-group\"> <div style=\"margin-bottom: 10px;margin-left:25px;margin-right:25px\"> <bsreqlabel>Nomor Rangka</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NoRangka\" placeholder=\"No Rangka\" ng-model=\"EditDetilSuratDistribusi.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div style=\"margin-bottom: 10px;margin-left:25px;margin-right:25px\"> <bsreqlabel>Nama Pelanggan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NamaPelanggan\" placeholder=\"Nama Pelanggan\" ng-model=\"EditDetilSuratDistribusi.CustomerName\" alpha-numeric mask=\"Huruf\" ng-maxlength=\"50\" required> </div> </div> <div ng-hide=\"true\" class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\"> <bsreqlabel>No BPKB</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NoBpkb\" placeholder=\"No BPKB\" ng-model=\"EditDetilSuratDistribusi.BPKBNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\"> <bsreqlabel>No. Polisi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NoPolisi\" placeholder=\"No. Polisi\" ng-model=\"EditDetilSuratDistribusi.PoliceNumber\" ng-maxlength=\"50\" required> </div> </div> </div> <table class=\"table table-bordered table-responsive\" border=\"1\" style=\"border: none !important\"> <tbody> <tr> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">&nbspDokumen</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Dikirim</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Diterima</th> </tr> <tr> <td style=\"border: none !important\"> <input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"BPKBReceiveDateChanged(EditDetilSuratDistribusi.BPKBReceiveDateCekChanged)\" ng-model=\"EditDetilSuratDistribusi.BPKBReceiveDateCekChanged\"> <bsreqlabel>BPKB</bsreqlabel> </td> <td style=\"border: none !important\"> <input ng-disabled=\"true\" class=\"form-control\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.BPKBSendDate\"> </td> <td style=\"border: none !important\"> <input type=\"date\" class=\"form-control\" min=\"{{ProcessedOldBPKBReceiveDate}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.BPKBReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"BPKBTickBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"> <input style=\"margin-right:5px\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" type=\"checkbox\" ng-change=\"FakturTAMReceiveDateChanged(EditDetilSuratDistribusi.FakturTAMReceiveCekChanged)\" ng-model=\"EditDetilSuratDistribusi.FakturTAMReceiveCekChanged\"> <bsreqlabel>Faktur TAM</bsreqlabel> </td> <td style=\"border: none !important\"> <input ng-disabled=\"true\" class=\"form-control\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.FakturTAMSendDate\"> </td> <td style=\"border: none !important\"> <input type=\"date\" class=\"form-control\" min=\"{{ProcessedOldFakturTAMReceiveDate}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.FakturTAMReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"FakturTAMTickBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"><input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"FormStatusGesekReceiveDateChanged(EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged)\" ng-model=\"EditDetilSuratDistribusi.FormStatusGesekReceiveDateCekChanged\">Faktur Status Gesek</td> <td style=\"border: none !important\"> <input ng-disabled=\"true\" class=\"form-control\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.FormStatusGesekSendDate\"> </td> <td style=\"border: none !important\"> <input type=\"date\" class=\"form-control\" min=\"{{ProcessedOldFormStatusGesekReceiveDate}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.FormStatusGesekReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"FormStatusGesekTickBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"><input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"CertificateNIKReceiveDateChanged(EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged)\" ng-model=\"EditDetilSuratDistribusi.CertificateNIKReceiveDateCekChanged\">Sertifikat NIK</td> <td style=\"border: none !important\"> <input ng-disabled=\"true\" class=\"form-control\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.CertificateNIKSendDate\"> </td> <td style=\"border: none !important\"> <input type=\"date\" class=\"form-control\" min=\"{{ProcessedOldCertificateNIKReceiveDate}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.CertificateNIKReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"CertificateNIKTickBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"><input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"SuratUjiTipeReceiveDateChanged(EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged)\" ng-model=\"EditDetilSuratDistribusi.SuratUjiTipeReceiveDateCekChanged\">Surat Uji Tipe</td> <td style=\"border: none !important\"> <input ng-disabled=\"true\" class=\"form-control\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.SuratUjiTipeSendDate\"> </td> <td style=\"border: none !important\"> <input type=\"date\" class=\"form-control\" min=\"{{ProcessedOldSuratUjiTipeReceiveDate}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.SuratUjiTipeReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"SuratUjiTipeTickBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"><input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"FormAReceiveDateChanged(EditDetilSuratDistribusi.FormAReceiveDateCekChanged)\" ng-model=\"EditDetilSuratDistribusi.FormAReceiveDateCekChanged\">Form A</td> <td style=\"border: none !important\"> <input ng-disabled=\"true\" class=\"form-control\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.FormASendDate\"> </td> <td style=\"border: none !important\"> <input type=\"date\" class=\"form-control\" min=\"{{ProcessedOldFormAReceiveDate}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.FormAReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"FormATickBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"><input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"SuratRekomendasiReceiveDateChanged(EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged)\" ng-model=\"EditDetilSuratDistribusi.SuratRekomendasiReceiveDateCekChanged\">Surat Rekomendasi</td> <td style=\"border: none !important\"> <input ng-disabled=\"true\" class=\"form-control\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.SuratRekomendasiSendDate\"> </td> <td style=\"border: none !important\"> <input type=\"date\" class=\"form-control\" min=\"{{ProcessedOldSuratRekomendasiReceiveDate}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.SuratRekomendasiReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"SuratRekomendasiTickBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"><input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"PIBReceiveDateChanged(EditDetilSuratDistribusi.PIBReceiveDateCekChanged)\" ng-model=\"EditDetilSuratDistribusi.PIBReceiveDateCekChanged\">PIB</td> <td style=\"border: none !important\"> <input ng-disabled=\"true\" class=\"form-control\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.PIBSendDate\"> </td> <td style=\"border: none !important\"> <input type=\"date\" class=\"form-control\" min=\"{{ProcessedOldPIBReceiveDate}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.PIBReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"PIBTickBox(EditDetilSuratDistribusi)\"> </td> </tr> </tbody> </table> </ul> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div id=\"bottomBox\"> <div style=\"float:left\"> <input style=\"margin-left:15px\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"CentangAllDokumenChanged(EditDetilSuratDistribusi.DokumenLengkap)\" type=\"checkbox\" ng-model=\"EditDetilSuratDistribusi.DokumenLengkap\">&nbspKelengkapan Dokumen Sudah Diperiksa </div> <div style=\"float:right\"> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" onclick=\"this.blur()\" ng-click=\" backFromterimaSTNK()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Keluar</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"SimpanUpdatePenerimaanDokumen()\" ng-disabled=\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.FrameNo == undefined || \r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.CustomerName == undefined || \r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.BPKBNo == undefined || \r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.PoliceNumber == undefined || \r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.BPKBReceiveDate == null || \r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.FakturTAMReceiveDate == null || \r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.TombolSimpan == false ||\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.BPKBSendDate > EditDetilSuratDistribusi.BPKBReceiveDate ||\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.FakturTAMSendDate > EditDetilSuratDistribusi.FakturTAMReceiveDate ||\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.FormStatusGesekSendDate > EditDetilSuratDistribusi.FormStatusGesekReceiveDate ||\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.CertificateNIKSendDate > EditDetilSuratDistribusi.CertificateNIKReceiveDate ||\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.SuratUjiTipeSendDate > EditDetilSuratDistribusi.SuratUjiTipeReceiveDate ||\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.FormASendDate > EditDetilSuratDistribusi.FormAReceiveDate ||\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.SuratRekomendasiSendDate > EditDetilSuratDistribusi.SuratRekomendasiReceiveDate ||\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tEditDetilSuratDistribusi.PIBSendDate > EditDetilSuratDistribusi.PIBReceiveDate\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> </p> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/terimaDokumenFakturTAM/terimaDokumenFakturTAM.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.vertical-alignment-helper {\r" +
    "\n" +
    "        display: table;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "        /* This makes sure that we can still click outside of the modal to close it */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .vertical-align-center {\r" +
    "\n" +
    "        /* To center vertically */\r" +
    "\n" +
    "        display: table-cell;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .modal-content {\r" +
    "\n" +
    "        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */\r" +
    "\n" +
    "        width: inherit;\r" +
    "\n" +
    "        height: inherit;\r" +
    "\n" +
    "        /* To center horizontally */\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        pointer-events: all;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .noppading {\r" +
    "\n" +
    "        padding-top: 0 !important;\r" +
    "\n" +
    "        padding-left: 0 !important;\r" +
    "\n" +
    "        padding-right: 0 !important;\r" +
    "\n" +
    "        padding-left: 0 !important;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"listContainer\"> <bsform-grid ng-form=\"TerimaDokumenFakturTAMForm\" ad factory-name=\"TerimaDokumenFakturTAMFactory\" model=\"mterimaDokumenFakturTAM\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" form-name=\"TerimaDokumenFakturTAMForm\" show-advsearch=\"on\" loading=\"loading\" form-title=\"Dokumen\" modal-title=\"terimadokumenfaktur\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status</label> <bsselect name=\"filterSales\" ng-model=\"filter.FakturTAMKeCAOStatusId\" data=\"statusterima\" item-text=\"FakturTAMKeCAOStatusName\" item-value=\"FakturTAMKeCAOStatusId\" placeholder=\"Select\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Rangka\" ng-model=\"filter.FrameNo\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Surat Distribusi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Surat\" ng-model=\"filter.NoSuratDistribusi\"> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nama Pelanggan\" ng-model=\"filter.CustomerName\"> </div> </div> </div> </div> </bsform-advsearch> </bsform-grid> <div class=\"ui modal formDocumenfaktur\" role=\"dialog\" style=\"background-color: transparent !important\"> <!-- <div> --> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Update Penerimaan Dokumen</h4> <label style=\"float:right; margin-top: -15px\" ng-hide=\"numview == len\" ng-click=\"next()\" class=\"fa fa-chevron-circle-right fa-lg\" aria-hidden=\"true\"></label> <label style=\"float:right; margin-top: -15px\">&nbsp{{numview}}/{{len}}&nbsp</label> <label style=\"float:right; margin-top: -15px\" ng-hide=\"numview == 1\" ng-click=\"prev()\" class=\"fa fa-chevron-circle-left fa-lg\" aria-hidden=\"true\"></label> </div> <!-- <div class=\"row\" style=\"margin: 0 0 -10px 0; float:right\">\r" +
    "\n" +
    "                                        </div> --> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"><b>Terima Dokumen</b></p> <ul class=\"list-group\"> <div style=\"margin-bottom: 10px;margin-left:25px;margin-right:25px\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoSpk\" placeholder=\"\" ng-model=\"rowsDocument[num].FrameNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument[num].CustomerName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </div> <table class=\"table table-bordered table-responsive\" border=\"1\" style=\"border: none !important\"> <tbody> <tr> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">&nbspNomor Dokumen</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">&nbspDokumen</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Dikirim</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Diterima</th> </tr> <tr ng-repeat=\"documen in rowsDocument[num].ListDetailDokumen\"> <td style=\"border: none !important\"> <bscheckbox label=\"{{documen.NoDokumen}}\" ng-disabled=\"DisablePasSemuaDicentang || documen.TombolSimpan == false\" ng-model=\"documen.ReceivedDateBit\" label=\"\" ng-change=\"checkdate(documen)\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </td> <td style=\"border: none !important; padding-top: 18px\"> {{documen.NamaDokumen}}</td> <td style=\"border: none !important\"> <input type=\"datetime-local\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"documen.SentDate\"> </td> <td style=\"border: none !important\"> <input type=\"datetime-local\" class=\"form-control\" ng-disabled=\"documen.ReceiveDate == null || documen.TombolSimpan == false \" ng-model=\"documen.ReceiveDate\"> </td> </tr> </tbody> </table> </ul> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button style=\"float: right\" ng-disabled=\"rowsDocument[0].ListDetailDokumen[0].TombolSimpan == false\" ng-click=\"simpanupdate()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbspSimpan </span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\" keluar()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Kembali</span><span class=\"ladda-spinner\"></span> </button> <div style=\"float:left;padding-left: 15px\"> <bscheckbox ng-disabled=\"rowsDocument[0].ListDetailDokumen[0].TombolSimpan == false\" ng-model=\"rowsDocument[num].DokumenLengkap\" label=\"Kelengkapan Dokumen Sudah Diperiksa\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </div> <!-- </div> --> </div></p> </div> <div class=\"ui modal formDocumenfakturTamSingle\" role=\"dialog\" style=\"background-color: transparent !important\"> <!-- <div> --> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Update Penerimaan Dokumen</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"><b>Terima Dokumen</b></p> <ul class=\"list-group\"> <div style=\"margin-bottom: 10px;margin-left:25px;margin-right:25px\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoSpk\" placeholder=\"\" ng-model=\"rowsDocument.FrameNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.CustomerName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </div> <table class=\"table table-bordered table-responsive\" border=\"1\" style=\"border: none !important\"> <tbody> <tr> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">&nbspNomor Dokumen</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">&nbspDokumen</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Dikirim</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Diterima</th> </tr> <tr ng-repeat=\"documen in rowsDocument.ListDetailDokumen\"> <td style=\"border: none !important\"> <bscheckbox label=\"{{documen.NoDokumen}}\" ng-disabled=\"documen.TombolSimpan == false\" ng-disabled=\"DisablePasSemuaDicentang\" ng-model=\"documen.ReceivedDateBit\" label=\"\" ng-change=\"checkdate(documen)\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </td> <td style=\"border: none !important; padding-top: 18px\"> {{documen.NamaDokumen}}</td> <td style=\"border: none !important\"> <input type=\"datetime-local\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"documen.SentDate\"> </td> <td style=\"border: none !important\"> <input type=\"datetime-local\" class=\"form-control\" ng-disabled=\"documen.ReceiveDate == null || documen.TombolSimpan == false\" ng-model=\"documen.ReceiveDate\" ng-change=\"ReceiveDateTicBox(documen)\"> </td> </tr> </tbody> </table> </ul> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button style=\"float: right\" ng-disabled=\"rowsDocument.ListDetailDokumen[0].TombolSimpan == false\" ng-click=\"simpanupdateSingle()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbspSimpan </span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\" keluar()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Kembali</span><span class=\"ladda-spinner\"></span> </button> <div style=\"float:left;padding-left: 15px\"> <bscheckbox ng-model=\"rowsDocument.DokumenLengkap\" ng-disabled=\"rowsDocument.ListDetailDokumen[0].TombolSimpan==false\" label=\"Kelengkapan Dokumen Sudah Diperiksa\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </div> <!-- </div> --> </div></p> </div> </div> <div ng-show=\"listDetail\" style=\"min-height:100vh\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <button id=\"PengembalianSpk\" ng-click=\"kembali()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 10px 10px\" ng-disabled=\"btnPengembalianSpk\">Kembali</button> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Surat Distribusi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NoSuratDistribusi\" ng-model=\"header.NoSuratDistribusi\" ng-disabled=\"true\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"StatusSurat\" ng-model=\"header.FakturTAMKeCAOStatusName\" ng-disabled=\"true\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Jumlah No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"QtyFrameNo\" ng-model=\"header.QtyFrameNo\" ng-disabled=\"true\"> </div> </div> </div> </div> <div class=\"grid\" ui-grid=\"gridDetailDokumen\" ui-grid-pagination ui-grid-auto-resize></div> </div> <!-- <div class=\"ui modal formDocumenfaktur\" style=\"width:1000px; background: none\">\r" +
    "\n" +
    "        <div ng-form>\r" +
    "\n" +
    "            <div class=\"vertical-alignment-helper\">\r" +
    "\n" +
    "                <div class=\"modal-dialog vertical-align-center\" style=\"margin-top: 0px; margin-bottom: 0px\">\r" +
    "\n" +
    "                    <div class=\"modal-content\" style=\"border-bottom-width: 0px; width:1000px; height:100%\">\r" +
    "\n" +
    "                       \r" +
    "\n" +
    "                            <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15);\">\r" +
    "\n" +
    "                            <h4 class=\"modal-title\">Update Dokumen</h4>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"modal-body\">\r" +
    "\n" +
    "                            <div class=\"row\" style=\"margin: 0 0 -10px 0; float:right\">\r" +
    "\n" +
    "                                <label class=\"fa fa-chevron-circle-left\" aria-hidden=\"true\"><button ng-disabled=\"numview == 1\" style=\"display:none\" ng-click=\"prev()\"></button></label> {{numview}}/{{len}}\r" +
    "\n" +
    "                                <label class=\"fa fa-chevron-circle-right\" aria-hidden=\"true\"><button ng-disabled=\"numview == len\" style=\"display:none\" ng-click=\"next()\"></button></label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>No Rangka</label>\r" +
    "\n" +
    "                                        <div class=\"input-icon-right \">\r" +
    "\n" +
    "                                            <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoSpk\" placeholder=\"\" ng-model=\"rowsDocument[num].FrameNo\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Nama Pelanggan</label>\r" +
    "\n" +
    "                                        <div class=\"input-icon-right \">\r" +
    "\n" +
    "                                            <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument[num].CustomerName\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <hr style=\"margin-top: 0px\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <div class=\"row\" style=\"margin: 5px 0 5px 0\">\r" +
    "\n" +
    "                                            <div class=\"col-md-4\" style=\"padding: 0 0 0 15px\">\r" +
    "\n" +
    "                                        <label><b>Nomor Dokumen</b></label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"col-md-2\" style=\"padding: 0 0 0 0\">\r" +
    "\n" +
    "                                        <label><b>Dokumen</b></label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"col-md-3 noppading\">\r" +
    "\n" +
    "                                        <label><b>Tanggal Dokumen Dikirim</b></label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"col-md-3 noppading\">\r" +
    "\n" +
    "                                        <label><b>Tanggal Dokumen Diterima</b></label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <hr style=\"margin-top: 0px\">\r" +
    "\n" +
    "                           \r" +
    "\n" +
    "                                <div class=\"row\" ng-repeat=\"documen in rowsDocument[num].ListDetailDokumen\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                                    <div class=\"col-md-1\" style=\"margin: 0 0 0 0; padding: 0 0 0 0\">\r" +
    "\n" +
    "                                        <bscheckbox ng-model=\"documen.ReceivedDateBit\" label=\"\" ng-change=\"checkdate(documen)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                                        </bscheckbox>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"col-md-3\" style=\"margin-top: 8px; padding: 0 0 0 0\">\r" +
    "\n" +
    "                                            <label style=\"margin-left: 5px\">{{documen.NoDokumen}}</label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"col-md-2\" style=\"margin-top: 8px; padding: 0 0 0 0\">\r" +
    "\n" +
    "                                        <label style=\"margin-left: 5px\">{{documen.NamaDokumen}}</label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                            <input type=\"datetime-local\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"documen.SentDate\">\r" +
    "\n" +
    "                                          \r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                            <input type=\"datetime-local\" class=\"form-control\" ng-disabled=\"documen.ReceiveDate\" ng-model=\"documen.ReceiveDate\">\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15);\">                            \r" +
    "\n" +
    "                            <div class=\"row\" style=\"float: left; margin:0 0 0 0\">\r" +
    "\n" +
    "                                <bscheckbox ng-model=\"rowsDocument[num].DokumenLengkap\" label=\" Kelengkapan Dokumen Sudah Diperiksa\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                                </bscheckbox>\r" +
    "\n" +
    "                              </div>\r" +
    "\n" +
    "                           \r" +
    "\n" +
    "                            <button class=\"btn rbtn\" style=\"float: right; \" ng-click=\"simpanupdate()\">Simpan</button>\r" +
    "\n" +
    "                            <button class=\"btn wbtn\" style=\"float: right; \" ng-click=\"keluar()\">Keluar</button>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <!-- <div class=\"ui modal formDocumenfakturTamSingle\" style=\"width:1000px; overflow-y: auto; background: none\">\r" +
    "\n" +
    "        <div class=\"vertical-alignment-helper\">\r" +
    "\n" +
    "            <div class=\"modal-dialog vertical-align-center\" style=\"margin-top: 0px; margin-bottom: 0px\">\r" +
    "\n" +
    "                <div class=\"modal-content\" style=\"border-bottom-width: 0px; width:1000px; height:100%\">\r" +
    "\n" +
    " \r" +
    "\n" +
    "                    <div class=\"modal-header\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <h4 class=\"modal-title\">Update Dokumen</h4>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"modal-body\">\r" +
    "\n" +
    "                        <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <label>No Rangka</label>\r" +
    "\n" +
    "                                    <div class=\"input-icon-right \">\r" +
    "\n" +
    "                                        <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoSpk\" placeholder=\"\" ng-model=\"rowsDocument.FrameNo\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <label>Nama Pelanggan</label>\r" +
    "\n" +
    "                                    <div class=\"input-icon-right \">\r" +
    "\n" +
    "                                        <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.CustomerName\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <hr style=\"margin-top: 0px\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"row\" style=\"margin: 5px 0 5px 0\">\r" +
    "\n" +
    "                                <div class=\"col-md-4\" style=\"padding: 0 0 0 15px\">\r" +
    "\n" +
    "                                    <label><b>Nomor Dokumen</b></label>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-2\" style=\"padding: 0 0 0 0\">\r" +
    "\n" +
    "                                    <label><b>Dokumen</b></label>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3 noppading\">\r" +
    "\n" +
    "                                    <label><b>Tanggal Dokumen Dikirim</b></label>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3 noppading\">\r" +
    "\n" +
    "                                    <label><b>Tanggal Dokumen Diterima</b></label>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <hr style=\"margin-top: 0px\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"row\" ng-repeat=\"documen in rowsDocument.ListDetailDokumen\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                                <div class=\"col-md-1\" style=\"margin: 0 0 0 0; padding: 0 0 0 0\">\r" +
    "\n" +
    "                                    <bscheckbox ng-model=\"documen.ReceivedDateBit\" label=\"\" ng-change=\"checkdate(documen)\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                                    </bscheckbox>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\" style=\"margin-top: 8px; padding: 0 0 0 0\">\r" +
    "\n" +
    "                                        <label style=\"margin-left: 5px\">{{documen.NoDokumen}}</label>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-2\" style=\"margin-top: 8px; padding: 0 0 0 0\">\r" +
    "\n" +
    "                                    <label style=\"margin-left: 5px\">{{documen.NamaDokumen}}</label>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <input type=\"datetime-local\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"documen.SentDate\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                            <input type=\"datetime-local\" class=\"form-control\" ng-disabled=\"documen.ReceiveDate == null\" ng-model=\"documen.ReceiveDate\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"modal-footer\" style=\"padding-bottom: 10px; padding-top: 10px\">\r" +
    "\n" +
    "                        <div class=\"row\" style=\"float: left; margin: 0 0 0 0\">\r" +
    "\n" +
    "                            <bscheckbox ng-model=\"rowsDocument.DokumenLengkap\" label=\" Kelengkapan Dokumen Sudah Diperiksa\" true-value=\"true\" false-value=\"false\">\r" +
    "\n" +
    "                            </bscheckbox>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <button class=\"btn rbtn\" style=\"float: right; \" ng-click=\"simpanupdateSingle()\">Simpan</button>\r" +
    "\n" +
    "                        <button class=\"btn wbtn\" style=\"float: right; \" ng-click=\"keluar()\">Keluar</button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> -->"
  );


  $templateCache.put('app/sales/sales7/registration/terimaDokumenPelanggan/terimaDokumenPelanggan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.switch {\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "        width: 60px;\r" +
    "\n" +
    "        height: 34px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .switch input {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .slider {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        top: 0;\r" +
    "\n" +
    "        left: 0;\r" +
    "\n" +
    "        right: 0;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "        background-color: #ccc;\r" +
    "\n" +
    "        -webkit-transition: .4s;\r" +
    "\n" +
    "        transition: .4s;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .slider:before {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        content: \"\";\r" +
    "\n" +
    "        height: 26px;\r" +
    "\n" +
    "        width: 26px;\r" +
    "\n" +
    "        left: 4px;\r" +
    "\n" +
    "        bottom: 4px;\r" +
    "\n" +
    "        background-color: white;\r" +
    "\n" +
    "        -webkit-transition: .4s;\r" +
    "\n" +
    "        transition: .4s;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    input:checked+.slider {\r" +
    "\n" +
    "        background-color: #2196F3;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    input:focus+.slider {\r" +
    "\n" +
    "        box-shadow: 0 0 1px #2196F3;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    input:checked+.slider:before {\r" +
    "\n" +
    "        -webkit-transform: translateX(26px);\r" +
    "\n" +
    "        -ms-transform: translateX(26px);\r" +
    "\n" +
    "        transform: translateX(26px);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /* Rounded sliders */\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .slider.round {\r" +
    "\n" +
    "        border-radius: 34px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .slider.round:before {\r" +
    "\n" +
    "        border-radius: 50%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .button {\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "        padding: 15px 15px 15px 15px;\r" +
    "\n" +
    "        font-size: 14px;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        text-align: left;\r" +
    "\n" +
    "        text-decoration: none;\r" +
    "\n" +
    "        outline: none;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "        background-color: #fff;\r" +
    "\n" +
    "        border: none;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .button:hover {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "        color: #fff;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .button:active {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "        transform: translateY(4px);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*th,\r" +
    "\n" +
    "    td {\r" +
    "\n" +
    "        padding-bottom: 10px;\r" +
    "\n" +
    "        font-family: \"Courier New\", Courier, monospace;\r" +
    "\n" +
    "    }*/</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"gridDokumenPelanggan\"> <bsform-grid ng-form=\"TerimaDokumenPelangganForm\" factory-name=\"TerimaDokumenPelangganFactory\" model=\"mterimaDokumenPelanggan\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" form-name=\"TerimaDokumenPelangganForm\" show-advsearch=\"on\" form-title=\"Dokumen\" modal-title=\"terimadokumen\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"from-group\"> <label>Cabang</label> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                            <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Pilih Cabang\" ng-model=\"filter.SuratDistribusiNo\" ng-maxlength=\"50\">\r" +
    "\n" +
    "                        </div> --> <bsselect name=\"filterSales\" placeholder=\"Pilih Cabang\" ng-model=\"filter.OutletId\" data=\"outletCabang\" item-text=\"Name\" item-value=\"OutletId\" placeholder=\"Select\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"from-group\"> <label>Nomor Surat Distribusi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Surat\" ng-model=\"filter.SuratDistribusiNo\" ng-maxlength=\"50\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Status</label> <bsselect name=\"filterSales\" placeholder=\"Pilih Status\" ng-model=\"filter.TerimaDokumenStatusId\" data=\"status\" item-text=\"TerimaDokumenStatusName\" item-value=\"TerimaDokumenStatusId\" placeholder=\"Select\"> </bsselect> </div> </div> </div> </bsform-advsearch> </bsform-grid> <div class=\"ui modal formDocumenUpload\" style=\"overflow-y: auto; background: none; box-shadow: none\"> <!-- <button ng-click=\"test()\">test</button> --> <div class=\"vertical-alignment-helper\"> <div class=\"modal-dialog vertical-align-center\" style=\"margin-top: 0px; margin-bottom: 0px\"> <div class=\"modal-content\" style=\"border-bottom-width: 0px; width:700px; height:100%\"> <!--<div style=\"width: 600px\">--> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" ng-click=\"close()\">&times;</button> <h4 class=\"modal-title\">Update Dokumen</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 -10px 0; float:right\"> <!--<label class=\"fa fa-chevron-circle-left\" ng-hide=\"numview == 1\" aria-hidden=\"true\"><button ng-disabled=\"numview == 1\" style=\"display:none\" ng-click=\"prev()\"></button></label> {{numview}}/{{len}}\r" +
    "\n" +
    "                            <label class=\"fa fa-chevron-circle-right\" ng-hide=\"numview == len\" aria-hidden=\"true\"><button ng-disabled=\"numview == len\" style=\"display:none\" ng-click=\"next()\"></button></label>--> <!--ini tadinya pake len kaya diatas--> <label class=\"fa fa-chevron-circle-left\" ng-hide=\"numview == 1\" aria-hidden=\"true\"><button ng-disabled=\"numview == 1\" style=\"display:none\" ng-click=\"prev()\"></button></label> {{numview}}/{{rowsDocument.length}} <label class=\"fa fa-chevron-circle-right\" ng-hide=\"numview == rowsDocument.length\" aria-hidden=\"true\"><button ng-disabled=\"numview == rowsDocument.length\" style=\"display:none\" ng-click=\"next()\"></button></label> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoSpk\" placeholder=\"\" ng-model=\"rowsDocument[num].FrameNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument[num].CustomerName\" ng-maxlength=\"50\" required> </div> </div> </div> <hr style=\"margin-top: 0px\"> <div class=\"row\" style=\"margin: 5px 0 5px 0\"> <div class=\"col-md-1\" style=\"margin: 0 0 0 0; padding: 0 0 0 0\"> <!--<i ng-if=\"documen.ReceivedDate == undefined\" class=\"fa fa-square-o\" style=\"font-size: 16px\" aria-hidden=\"true\"></i>\r" +
    "\n" +
    "                                        <i ng-if=\"documen.ReceivedDate != undefined\" class=\"fa fa-check-square\" style=\"font-size: 16px\" aria-hidden=\"true\"></i>--> </div> <div class=\"col-md-3\" style=\"padding: 0 0 0 0\"> <label><b>Jenis Dokumen</b><label> </label></label></div> <div class=\"col-md-4\"> <label><b>Tanggal Dokumen Dikirim</b><label> </label></label></div> <div class=\"col-md-4\"> <label><b>Tanggal Dokumen Diterima</b><label> </label></label></div> </div> <hr style=\"margin-top: 0px\"> <!--Upload KTP Pelanggan--> <div class=\"row\" ng-repeat=\"documen in rowsDocument[num].listDetail\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-1\" style=\"margin: 8px 0 0 0; padding: 0 0 0 0\"> <bscheckbox ng-model=\"documen.ReceivedDateBit\" label=\"\" ng-change=\"checkdate(documen)\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </div> <div class=\"col-md-3\" style=\"margin-top: 8px; padding: 0 0 0 0\"> <!-- <i ng-if=\"documen.ReceivedDate == undefined\" class=\"fa fa-square-o\" style=\"font-size: 16px\" aria-hidden=\"true\"></i>\r" +
    "\n" +
    "                                        <i ng-if=\"documen.ReceivedDate != undefined\" class=\"fa fa-check-square\" style=\"font-size: 16px\" aria-hidden=\"true\"></i> --> <label style=\"margin-left: 5px\"><a ng-click=\"\">{{documen.DocumentName}}</a><label> </label></label></div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsdatepicker name=\"reciveraDate\" ng-disabled=\"true\" date-options=\"\" ng-model=\"documen.SentDate\"> </bsdatepicker> <!--<bserrmsg field=\"reciveraDate\"></bserrmsg>--> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <!--<bsdatepicker name=\"LastStartDate\" date-options=\"\" ng-model=\"documen.ReceivedDate\">\r" +
    "\n" +
    "                                            </bsdatepicker>--> <input type=\"datetime-local\" class=\"form-control\" ng-model=\"documen.ReceivedDate\"> </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding-bottom: 10px; padding-top: 10px\"> <div class=\"row\" style=\"float: left; margin: 0 0 0 0\"> <bscheckbox ng-model=\"rowsDocument[num].DokumenLengkap\" label=\" Kelengkapan Dokumen Sudah Diperiksa\" true-value=\"true\" false-value=\"false\"> </bscheckbox> <!-- <input type=\"checkbox\" ng-model=\"rowsDocument[num].SuratDistribusiAjuAFIKeTAMStatusCheck\" ng-checked=\"rowsDocument[num].SuratDistribusiAjuAFIKeTAMStatusCheck\" > Kelengkapan Dokumen Sudah Diperiksa --> </div> <button class=\"btn rbtn\" style=\"float: right\" ng-click=\"simpanupdate()\">Simpan</button> </div> <!--</div>--> </div> </div> </div> </div> <div class=\"ui modal formDocumenUploadsingle\" style=\"overflow-y: auto; background: none; box-shadow: none\"> <div class=\"vertical-alignment-helper\"> <div class=\"modal-dialog vertical-align-center\" style=\"margin-top: 0px; margin-bottom: 0px\"> <div class=\"modal-content\" style=\"border-bottom-width: 0px; width:700px; height:100%\"> <!--<div style=\"width: 600px\">--> <div class=\"modal-header\"> <!-- <button ng-click=\"test()\">test</button> --> <!-- <button type=\"button\" class=\"close\" ng-click=\"close()\">&times;</button> --> <h4 class=\"modal-title\">Update Dokumen</h4> </div> <div class=\"modal-body\"> <!--<div class=\"row\" style=\"margin: 0 0 -10px 0; float:right\">\r" +
    "\n" +
    "                                <label class=\"fa fa-chevron-circle-left\" aria-hidden=\"true\"><button ng-disabled=\"numview == 1\" style=\"display:none\" ng-click=\"prev()\"></button></label> {{numview}}/{{len}}\r" +
    "\n" +
    "                                    <label class=\"fa fa-chevron-circle-right\" aria-hidden=\"true\"><button ng-disabled=\"numview == len\" style=\"display:none\" ng-click=\"next()\"></button></label>\r" +
    "\n" +
    "                                </div>--> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoSpk\" placeholder=\"\" ng-model=\"rowsDocuments.FrameNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocuments.CustomerName\" ng-maxlength=\"50\" required> </div> </div> </div> <hr style=\"margin-top: 0px\"> <div class=\"row\" style=\"margin: 5px 0 5px 0\"> <div class=\"col-md-1\" style=\"margin: 8px 0 0 0; padding: 0 0 0 0\"> <!--<i ng-if=\"documen.ReceivedDate == undefined\" class=\"fa fa-square-o\" style=\"font-size: 16px\" aria-hidden=\"true\"></i>\r" +
    "\n" +
    "                                        <i ng-if=\"documen.ReceivedDate != undefined\" class=\"fa fa-check-square\" style=\"font-size: 16px\" aria-hidden=\"true\"></i>--> </div> <div class=\"col-md-3\" style=\"padding: 0 0 0 0\"> <label><b>Jenis Dokumen</b><label> </label></label></div> <div class=\"col-md-4\"> <label><b>Tanggal Dokumen Dikirim</b><label> </label></label></div> <div class=\"col-md-4\"> <label><b>Tanggal Dokumen Diterima</b><label> </label></label></div> </div> <hr style=\"margin-top: 0px\"> <!--Upload KTP Pelanggan--> <div class=\"row\" ng-repeat=\"documen in rowsDocuments.listDetail\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-1\" style=\"padding: 0 0 0 0\"> <bscheckbox ng-model=\"documen.ReceivedDateBit\" label=\"\" ng-change=\"checkdate(documen)\" ng-disabled=\"documen.TombolSimpan == false\" true-value=\"true\" false-value=\"false\"> </bscheckbox> <!-- <i ng-if=\"documen.ReceivedDate == undefined\" class=\"fa fa-square-o\" style=\"font-size: 16px\" aria-hidden=\"true\"></i>\r" +
    "\n" +
    "                                        <i ng-if=\"documen.ReceivedDate != undefined\" class=\"fa fa-check-square\" style=\"font-size: 16px\" aria-hidden=\"true\"></i> --> </div> <div class=\"col-md-3\" style=\"margin-top: 8px; padding: 0 0 0 0\"> <label style=\"margin-left: 5px\" ng-disabled=\"documen.TombolSimpan == false\" ng-click=\"viewImage(documen)\"><a>{{documen.DocumentName}}</a><label> </label></label></div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsdatepicker name=\"reciveraDate\" ng-disabled=\"true\" date-options=\"\" ng-model=\"documen.SentDate\"> </bsdatepicker> <!--<bserrmsg field=\"reciveraDate\"></bserrmsg>--> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <!--<bsdatepicker name=\"LastStartDate\" date-options=\"\" ng-model=\"documen.ReceivedDate\">\r" +
    "\n" +
    "                                            </bsdatepicker>--> <input type=\"datetime-local\" class=\"form-control\" ng-disabled=\"documen.ReceivedDate == null || rowsDocuments.listDetail[0].TombolSimpan == false\" ng-change=\"checkifDate(documen)\" ng-model=\"documen.ReceivedDate\"> </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding-bottom: 10px; padding-top: 10px\"> <div class=\"row\" style=\"float: left; margin: 0 0 0 0\"> <!-- <input type=\"checkbox\" ng-model=\"\" ng-checked=\"rowsDocument.SuratDistribusiAjuAFIKeTAMStatusCheck\" >  --> <bscheckbox ng-model=\"rowsDocuments.DokumenLengkap\" label=\"Kelengkapan Dokumen Sudah Diperiksa\" true-value=\"true\" ng-disabled=\"rowsDocuments.listDetail[0].TombolSimpan == false\" false-value=\"false\"> </bscheckbox> </div> <!-- <button type=\"button\" style=\"float: right;\" class=\"close\" ng-click=\"close()\">Keluar</button> --> <button class=\"btn rbtn\" style=\"float: right\" ng-disabled=\"rowsDocuments.listDetail[0].TombolSimpan == false\" ng-click=\"simpanupdatesingle()\">Simpan</button> <button class=\"btn wbtn\" style=\"float: right\" ng-click=\"close()\">Keluar</button> </div> <!--</div>--> </div> </div> </div> </div> </div> <div ng-show=\"DetailDokumen\"> <div class=\"row\" style=\"margin: -45px 0 0 0; float:right\"> <button class=\"wbtn btn\" ng-click=\"back()\">Kembali</button> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <div style=\"margin-top: 10px 0 0 0\" class=\"grid\" ui-grid=\"gridDetailDokumen\" ui-grid-pagination ui-grid-auto-resize></div> </div> </div> <div class=\"ui modal dokumenTerimaLihat\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4> <b>{{dokumenName}}</b> </h4> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> </div> <div class=\"modal-body\"> <!--<img ng-src=\"{{viewdocument}}\" image-set-aspect ng-class=\"{'wide': viewdocument.width/viewdocument.height > 1, 'tall': viewdocument.width/viewdocument.height <= 1}\">--> <p align=\"center\"> <img ng-src=\"{{dokumenData}}\" style=\"max-width: 860px; max-height: 526px; text-align: center\"> </p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <!-- <button ng-click=\"btnkehilanganSpk()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width:15%;\">Simpan</button> --> <button ng-click=\"KeluarDokumenTerima()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Keluar</button> </div> </div> </div> </div> <!--<div class=\"row\"> \r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <button id=\"cancelBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\" ng-click=\"revisi()\">Batal AFI</button>\r" +
    "\n" +
    "                <button id=\"cancelBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\" ng-click=\"printAfibtn()\">Revisi AFI</button>\r" +
    "\n" +
    "                <button id=\"uploadBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\" ng-click=\"printAfi()\">Lihat Detai</button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>-->"
  );


  $templateCache.put('app/sales/sales7/registration/terimaDokumenStnk/terimaDokumenStnk.html',
    "<script type=\"text/javascript\">// function IfGotProblemWithModal() {\r" +
    "\n" +
    "    //     $('body .modals').remove();\r" +
    "\n" +
    "    // }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    function justNumber(key){\r" +
    "\n" +
    "        return key == 'ArrowLeft' || key == 'ArrowRight' || key == 'Delete' || key == 'Tab';\r" +
    "\n" +
    "    }</script> <style type=\"text/css\">.context-menu {cursor: context-menu;}\r" +
    "\n" +
    "    /* input[type=\"date\"]::-webkit-calendar-picker-indicator, */\r" +
    "\n" +
    "    input[type=\"date\"]::-webkit-inner-spin-button{\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "        -webkit-appearance: none;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <!-- <div ng-show=\"ShowTerimaDokumenSTNKMain\" class=\"formHeader container\" style=\"width:100%\">\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"container\" style=\"width: 100%\">\r" +
    "\n" +
    "            <div class=\"col-md-1\">\r" +
    "\n" +
    "                <label>Search </label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-4\">\r" +
    "\n" +
    "                <bsselect name=\"In\" ng-model=\"\" data=\"type\" item-text=\"type\" item-value=\"type\" on-select=\"type(selected)\" placeholder=\"Select\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-4\">\r" +
    "\n" +
    "                <button class=\"rbtn btn ng-binding ladda-button\">Filter </button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <div ng-show=\"ShowTerimaDokumenSTNKMain\" class=\"row\" style=\"width: 100%; margin: 0 0 0 0\"> <bsform-grid ng-form=\"TerimaDokumenStnkForm\" factory-name=\"TerimaDokumenStnkFactory\" model=\"mTerimaDokumenStnk\" model-id=\"SuratDistribusiSTNKHeaderId\" loading=\"loading\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" hide-new-button=\"true\" form-name=\"TerimaDokumenStnkForm\" form-title=\"TerimaDokumenStnk\" modal-title=\"Terima Dokumen Stnk\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label>Status</label> <bsselect name=\"status\" placeholder=\"Status\" ng-model=\"filter.TerimaDokumenSTNKBPKBStatusId\" data=\"getStatusDokumen\" item-text=\"TerimaDokumenSTNKBPKBStatusName\" item-value=\"TerimaDokumenSTNKBPKBStatusId\" required icon=\"fa fa-map-marker \"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nomor Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Rangka\" ng-model=\"filter.FrameNo\" maxlength=\"30\"> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nomor Surat Distribusi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nomor Surat Distribusi\" ng-model=\"filter.SuratDistribusiSTNKNo\" maxlength=\"30\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Nama Pelanggan\" ng-model=\"filter.CustomerName\" maxlength=\"30\"> </div> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"ShowDetilSuratDistribusiStnk\"> <bsbreadcrumb-custom action-label=\"breadcrums.title\" tab=\"tab\"></bsbreadcrumb-custom> <div class=\"row\" style=\"margin: -45px 0 10px 0\"> <button style=\"float: right\" ng-click=\"KembaliKeTerimaDokumenSTNKMain()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"grid2\" ui-grid-pagination ui-grid-auto-resize></div> </div> <!-- <div class=\"ui modal UpdatePenerimaanDokumenSTNK\" role=\"dialog\" style=\"background-color: transparent !important; width: 600px\">\r" +
    "\n" +
    "        <p style=\"text-align: center\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        </p>\r" +
    "\n" +
    "</div> --> <div class=\"ui modal UpdatePenerimaanDokumenSTNK\" style=\"background: none; box-shadow: none\"> <div class=\"vertical-alignment-helper\"> <div class=\"modal-dialog vertical-align-center\" style=\"margin-top: 0px; margin-bottom: 0px\"> <div class=\"modal-content\" style=\"border-bottom-width: 0px; width:600px; height:100%\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Update Penerimaan Dokumen</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"><b>Terima Dokumen</b></p> <ul class=\"list-group\"> <div style=\"margin-bottom: 20px;margin-left:25px;margin-right:25px\"> <bsreqlabel>Nomor Rangka</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"No Rangka\" ng-model=\"EditDetilSuratDistribusi.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div style=\"margin-bottom: 10px;margin-left:25px;margin-right:25px\"> <bsreqlabel>Nama Pelanggan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"NamaPelanggan\" placeholder=\"Nama Pelanggan\" ng-model=\"EditDetilSuratDistribusi.CustomerName\" ng-maxlength=\"50\" required> </div> </div> <div ng-hide=\"true\" class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\"> <bsreqlabel>No STNK</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NoStnk\" placeholder=\"No STNK\" ng-model=\"EditDetilSuratDistribusi.STNKNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\"> <bsreqlabel>No Polisi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"NoPolisi\" placeholder=\"No Polisi\" ng-model=\"EditDetilSuratDistribusi.PoliceNumber\" ng-maxlength=\"50\" required> </div> </div> </div> <table class=\"table table-bordered table-responsive\" border=\"1\" style=\"border: none !important\"> <tbody style=\"border: none !important\"> <tr> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Dokumen</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Dikirim</th> <th style=\"border-left: none !important;border-right: none !important;border-top: 1px solid #ddd !important;border-bottom: 1px solid #ddd !important\">Tanggal Dokumen Diterima</th> </tr> <tr> <td style=\"border: none !important\"> <input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"StnkTerimaChanged(EditDetilSuratDistribusi.StnkTerimaCekChanged)\" ng-model=\"EditDetilSuratDistribusi.StnkTerimaCekChanged\"> <bsreqlabel>STNK</bsreqlabel> </td> <td style=\"border: none !important\"> <input class=\"form-control\" ng-disabled=\"true\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.STNKSendDate\"> </td> <td style=\"border: none !important\"> <input class=\"form-control\" type=\"date\" min=\"{{ProcessedOldTanggalTerimaSTNK}}\" onkeydown=\"return justNumber(event.key)\" ng-model=\"EditDetilSuratDistribusi.STNKReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"STNKTicBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"> <input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"PoliceNumberTerimaChanged(EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged)\" ng-model=\"EditDetilSuratDistribusi.PoliceNumberTerimaCekChanged\"> <bsreqlabel>No. Plat Polisi</bsreqlabel> </td> <td style=\"border: none !important\"> <input class=\"form-control\" ng-disabled=\"true\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.PoliceNumberSendDate\"> </td> <td style=\"border: none !important\"> <input class=\"form-control\" type=\"date\" onkeydown=\"return justNumber(event.key)\" min=\"{{ProcessedOldTanggalTerimaPoliceNumber}}\" ng-model=\"EditDetilSuratDistribusi.PoliceNumberReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"PoliceNumberTicBox(EditDetilSuratDistribusi)\"> </td> </tr> <tr> <td style=\"border: none !important\"> <input style=\"margin-right:5px\" type=\"checkbox\" ng-disabled=\"DisablePasSemuaDicentang || EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"NoticePajakTerimaChanged(EditDetilSuratDistribusi.NoticePajakTerimaCekChanged)\" ng-model=\"EditDetilSuratDistribusi.NoticePajakTerimaCekChanged\"> <bsreqlabel>Notice Pajak</bsreqlabel> </td> <td style=\"border: none !important\"> <input class=\"form-control\" ng-disabled=\"true\" type=\"date\" ng-model=\"EditDetilSuratDistribusi.NoticePajakSendDate\"> </td> <td style=\"border: none !important\"> <input class=\"form-control\" type=\"date\" onkeydown=\"return justNumber(event.key)\" min=\"{{ProcessedOldTanggalTerimaNoticePajak}}\" ng-model=\"EditDetilSuratDistribusi.NoticePajakReceiveDate\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"NoticePajakTicBox(EditDetilSuratDistribusi)\"> </td> </tr> </tbody> </table> </ul> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div id=\"bottomBox\"> <div style=\"float:left\"> <input style=\"margin-left:15px\" type=\"checkbox\" ng-disabled=\"EditDetilSuratDistribusi.TombolSimpan == false\" ng-change=\"CentangAllDokumenChanged(EditDetilSuratDistribusi.DokumenLengkap)\" ng-model=\"EditDetilSuratDistribusi.DokumenLengkap\">&nbspKelengkapan Dokumen Sudah Diperiksa </div> <div style=\"float:right\"> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" onclick=\"this.blur()\" ng-click=\" backFromterimaSTNK()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Kembali</span><span class=\"ladda-spinner\"></span> </button> <button ng-disabled=\"EditDetilSuratDistribusi.FrameNo == undefined || \r" +
    "\n" +
    "                                                EditDetilSuratDistribusi.CustomerName == undefined || \r" +
    "\n" +
    "                                                EditDetilSuratDistribusi.STNKReceiveDate == null || \r" +
    "\n" +
    "                                                EditDetilSuratDistribusi.PoliceNumberReceiveDate == null || \r" +
    "\n" +
    "                                                EditDetilSuratDistribusi.NoticePajakReceiveDate == null || \r" +
    "\n" +
    "                                                EditDetilSuratDistribusi.TombolSimpan == false ||\r" +
    "\n" +
    "                                                EditDetilSuratDistribusi.STNKSendDate > EditDetilSuratDistribusi.STNKReceiveDate ||\r" +
    "\n" +
    "                                                EditDetilSuratDistribusi.PoliceNumberSendDate > EditDetilSuratDistribusi.PoliceNumberReceiveDate || \r" +
    "\n" +
    "                                                EditDetilSuratDistribusi.NoticePajakSendDate > EditDetilSuratDistribusi.NoticePajakReceiveDate\" ng-click=\"SimpanUpdatePenerimaanDokumen()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Simpan </span><span class=\"ladda-spinner\"></span> </button> <!-- <button  --> <!-- ng-disabled=\"EditDetilSuratDistribusi.FrameNo == undefined ||  --> <!-- EditDetilSuratDistribusi.CustomerName == undefined ||  --> <!-- EditDetilSuratDistribusi.STNKNo == undefined ||  --> <!-- EditDetilSuratDistribusi.PoliceNumber == undefined ||  --> <!-- EditDetilSuratDistribusi.STNKReceiveDate == null ||  --> <!-- EditDetilSuratDistribusi.PoliceNumberReceiveDate == null ||  --> <!-- EditDetilSuratDistribusi.NoticePajakReceiveDate == null ||  --> <!-- EditDetilSuratDistribusi.TombolSimpan == false || --> <!-- EditDetilSuratDistribusi.STNKSendDate > EditDetilSuratDistribusi.STNKReceiveDate || --> <!-- EditDetilSuratDistribusi.PoliceNumberSendDate > EditDetilSuratDistribusi.PoliceNumberReceiveDate ||  --> <!-- EditDetilSuratDistribusi.NoticePajakSendDate > EditDetilSuratDistribusi.NoticePajakReceiveDate\" --> <!-- ng-click=\"SimpanUpdatePenerimaanDokumen()\" --> <!-- type=\"button\" --> <!-- class=\"rbtn btn ng-binding ladda-button\"> --> <!-- <span class=\"ladda-label\"> --> <!-- Simpan --> <!-- </span><span class=\"ladda-spinner\"></span> --> <!-- </button> --> </div> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales7/registration/terimaSTNKdariBiroJasa/terimaSTNKdariBirojasa.html',
    "<script type=\"text/javascript\">function SetReceiveSTNKDateMinDate(TheDate) {\r" +
    "\n" +
    "        var Da_ReceiveSTNKDateMaxDate = TheDate;\r" +
    "\n" +
    "        var dd = Da_ReceiveSTNKDateMaxDate.getDate();\r" +
    "\n" +
    "        var mm = Da_ReceiveSTNKDateMaxDate.getMonth() + 1; //January is 0!\r" +
    "\n" +
    "        var yyyy = Da_ReceiveSTNKDateMaxDate.getFullYear();\r" +
    "\n" +
    "        if (dd < 10) {\r" +
    "\n" +
    "            dd = '0' + dd\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (mm < 10) {\r" +
    "\n" +
    "            mm = '0' + mm\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        Da_ReceiveSTNKDateMaxDate = yyyy + '-' + mm + '-' + dd;\r" +
    "\n" +
    "        document.getElementById(\"Min_Da_ReceiveSTNKDateMaxDate\").setAttribute(\"min\", Da_ReceiveSTNKDateMaxDate);\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }</script> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"daftarPO\" class=\"row\" style=\"margin: 0 0 0 0\"> <button ng-disabled=\"TerimaStnkDariBiroJasaYangDipilih.length<1 || TerimaStnkDariBiroJasaYangDipilih==null || TerimaStnkDariBiroJasaYangDipilih==undefined\" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:-30px;float:right\" ng-click=\"TerimaSTNKBirojasaTerimaLengkap()\">Terima Lengkap</button> <button ng-disabled=\"TerimaStnkDariBiroJasaYangDipilih.length<1 || TerimaStnkDariBiroJasaYangDipilih==null || TerimaStnkDariBiroJasaYangDipilih==undefined\" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:-30px;float:right\" ng-click=\"TerimaSTNKBirojasaTerimaStnk()\">Terima STNK</button> <button ng-disabled=\"TerimaStnkDariBiroJasaYangDipilih.length<1 || TerimaStnkDariBiroJasaYangDipilih==null || TerimaStnkDariBiroJasaYangDipilih==undefined\" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:-30px;float:right\" ng-click=\"TerimaSTNKBirojasaTerimaNotice()\">Terima Notice</button> <button ng-disabled=\"TerimaStnkDariBiroJasaYangDipilih.length<1 || TerimaStnkDariBiroJasaYangDipilih==null || TerimaStnkDariBiroJasaYangDipilih==undefined\" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:-30px;float:right\" ng-click=\"TerimaSTNKBirojasaTerimaPlat()\">Terima Plat</button> <button ng-disabled=\"TerimaStnkDariBiroJasaYangDipilih.length<1 || TerimaStnkDariBiroJasaYangDipilih==null || TerimaStnkDariBiroJasaYangDipilih==undefined || TerimaStnkDariBiroJasaBatalAman==false\" class=\"wbtn btn ng-binding ladda-button\" style=\"margin-left:15px;margin-top:-30px;float:right\" ng-click=\"TerimaSTNKBirojasaBatalTerima()\">Batal Terima</button> <bsform-grid ng-form=\"TerimaSTNKBirojasaForm\" factory-name=\"TerimaSTNKBirojasaFactory\" model=\"mTerimaSTNKBirojasa\" model-id=\"ReceiveId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" show-advsearch=\"on\" form-name=\"TerimaSTNKBirojasaForm\" form-title=\"Terima STNK Biro Jasa\" modal-title=\"TerimaSTNKBirojasa\" loading=\"loading\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Cabang</label> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Pilih Cabang\" ng-model=\"filter.SuratDistribusiNo\" ng-maxlength=\"50\">\r" +
    "\n" +
    "                            </div> --> <div class=\"input-icon-right\"> <bsselect name=\"filterSales\" ng-model=\"filter.OutletId\" data=\"outletCabang\" item-text=\"Name\" item-value=\"OutletId\" placeholder=\"Branch\" icon=\"fa fa-sitemap\"></bsselect> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. PO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. PO\" ng-model=\"filter.POBiroJasaUnitNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input No. Rangka\" ng-model=\"filter.FrameNo\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status</label> <div class=\"input-icon-right\"> <bsselect ng-model=\"filter.StatusAFIId\" data=\"OptionsStatusafiTerimaStnkDariBiroJasa\" item-text=\"StatusName\" item-value=\"StatusAFIId\" placeholder=\"Status\"> </bsselect> </div> </div> </div> </div> <div class=\"row\" style=\"margin-left:0px;margin-right:0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama Pemilik</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"model\" placeholder=\"Input Nama Pemilik\" ng-model=\"filter.NamaPemilik\" maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Vendor</label> <bsselect name=\"filterSales\" placeholder=\"Vendor\" ng-model=\"filter.ServiceBureauId\" data=\"vendor\" item-text=\"ServiceBureauName,ServiceBureauCode\" item-value=\"ServiceBureauId\" placeholder=\"Select\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tanggal Penerimaan</label> <bsdatepicker name=\"TanggalPenerimaan\" ng-model=\"filter.TanggalPenerimaan\" ng-change=\"valueStartDate()\" date-options=\"dateOption\" required> </bsdatepicker> </div> </div> </div> </bsform-advsearch> </bsform-grid> <div class=\"ui modal terimaSTNK\" role=\"dialog\" style=\"background-color: transparent !important; width: 600px\"> <!-- <div> --> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Terima STNK dari Biro Jasa</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div filedset=\"false\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldNoRangka\" placeholder=\"\" ng-model=\"rowsDocument.FrameNo\" maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Model Kendaraan</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.Description\" maxlength=\"50\" required> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <label>Nama STNK</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"rowsDocument.STNKName\" maxlength=\"50\" required> </div> </div> </div> </div> <form name=\"validSTNKBirojasa\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px\"> <p style=\"width:120px; margin-top:-17px; padding-left: 3px; margin-left:14px; background:white\"><b>Item yang Diterima</b></p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-1\" style=\"margin-top: 8px\"> <!--<span ng-click=\"toogle = !toogle\"><i  ng-class=\"{'fa fa-square-o' : asdf == false || !toogle, 'fa fa-check-square' : asdf == true || toogle}\"  style=\"font-size: 15px\" aria-hidden=\"true\"></i>--> <!--<input type=\"checkbox\" readonly ng-model=\"NoSTNKCekChanged\">--> <span><i ng-class=\"{'fa fa-square-o' : NoSTNKCekChanged == false || !toogle, 'fa fa-check-square' : NoSTNKCekChanged == true || toogle}\" style=\"font-size: 15px\" aria-hidden=\"true\"></i> </span></div> <div class=\"col-xs-4\" style=\"margin-top: 8px; padding-left: 0px !important\"> <bsreqlabel>No STNK</bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input alpha-numeric mask=\"TDP\" ng-change=\"CentangSTNK()\" type=\"text\" class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.STNKNo\" maxlength=\"20\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-1\" style=\"margin-top: 8px\"> <!--<i class=\"fa fa-square-o\" style=\"font-size: 15px\" aria-hidden=\"true\"></i>--> <!--<input type=\"checkbox\" readonly ng-model=\"PoliceNumberCekChanged\">--> <span><i ng-class=\"{'fa fa-square-o' : PoliceNumberCekChanged == false || !toogle, 'fa fa-check-square' : PoliceNumberCekChanged == true || toogle}\" style=\"font-size: 15px\" aria-hidden=\"true\"></i> </span></div> <div class=\"col-xs-4\" style=\"margin-top: 8px; padding-left: 0px !important\"> <bsreqlabel>Plat No Polisi</bsreqlabel> </div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangPoliceNumber()\" type=\"text\" class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.PoliceNumber\" maxlength=\"10\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-1\" style=\"margin-top: 8px\"> <!--<i class=\"fa fa-square-o\" style=\"font-size: 15px\" aria-hidden=\"true\"></i>--> <!--<input type=\"checkbox\" readonly ng-model=\"NoticePajakCekChanged\">--> <span><i ng-class=\"{'fa fa-square-o' : NoticePajakCekChanged == false || !toogle, 'fa fa-check-square' : NoticePajakCekChanged == true || toogle}\" style=\"font-size: 15px\" aria-hidden=\"true\"></i> </span></div> <div class=\"col-xs-4\" style=\"margin-top: 8px ;padding-left: 0px !important\"> <bsreqlabel>Notice Pajak</bsreqlabel> </div> </div> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"width:96%;border:1px solid lightgrey;padding: 15px 0 0 0;margin: 15px 10px 10px 10px\"> <p style=\"width:110px; margin-top:-21px; padding-left: 3px; margin-left:14px; background:white\"><b>Item Notice Pajak</b></p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <label>BBNKB<label> </label></label></div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.BBNKB\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <label>PKB<label> </label></label></div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.PKB\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <label>SWDKLLJ<label> </label></label></div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.SWDKLLJ\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <label>Biaya Adm.STNK<label> </label></label></div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.BiayaAdmSTNK\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-xs-5\" style=\"margin-top: 8px\"> <label>Biaya Adm.TNKB<label> </label></label></div> <div class=\"col-xs-7\"> <div style=\"margin-bottom: 5px\"> <div class=\"input-icon-right\"> <input ng-change=\"CentangNoticePajak()\" type=\"text\" input-currency number-only class=\"form-control\" name=\"fieldModel\" placeholder=\"\" ng-model=\"rowsDocument.BiayaAdmTNKB\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </div> </div> </form> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\" style=\"padding: 15px 13px 0 0\"> <label>Tanggal Aju STNK ke Biro Jasa :</label> <bsdatepicker ng-disabled=\"true\" type=\"date\" date-options=\"dateOption\" ng-model=\"rowsDocument.POBiroJasaUnitDate\"></bsdatepicker> </div> <div class=\"col-md-6\" style=\"padding: 15px 0 0 13px\"> <label>Tanggal Terima STNK dari Biro Jasa :</label> <bsdatepicker type=\"date\" id=\"Min_Da_ReceiveSTNKDateMaxDate\" date-options=\"dateOption\" min=\"2000-13-13\" ng-model=\"rowsDocument.ReceiveSTNKDate\"></bsdatepicker> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <!-- <button ng-click=\"testing()\">tes</button> --> <button type=\"button\" ng-disabled=\"validSTNKBirojasa.$invalid\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"simpanTerimaSTNK()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\" backFromterimaSTNK()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-times\"></i>&nbsp;Kembali</span><span class=\"ladda-spinner\"></span> </button> </div> </p> <!-- </div> --> </div> <div class=\"ui modal alasanterlambatSTNK\" modal=\"alasanterlambat\" role=\"dialog\"> <!-- <div> --> <div class=\"modal-header\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\"></button> <h4 class=\"modal-title\"><i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Alasan Keterlambatan Penerimaan STNK dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 15px 0 15px; margin-top: 10px 10px 0 10px\"> <p style=\"width:50px; padding:0 5px 0 5px; margin-top:-18px; margin-bottom:15px; background:white\"><b>Alasan</b></p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\" style=\"padding-right: 0 !important\"> <label>No. PO</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" placeholder=\"Input No.PO\" ng-model=\"SelectedDataTerlambat_POBiroJasaUnitNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important; padding-top: 10px !important\"> <label>Model</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_Model\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <bsreqlabel>Alasan Keterlambatan</bsreqlabel> <div class=\"form-group\"> <textarea ng-model=\"SelectedDataTerlambat_AlasanKeterlambatanSTNK\" rows=\"6\" class=\"form-control\" cols=\"83\">\r" +
    "\n" +
    "                            </textarea> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\" style=\"margin-top: 8px; padding:0 5px 0 0\"> <bsreqlabel>Perkiraan Lama Keterlambatan : </bsreqlabel> </div> <div class=\"col-md-2\" style=\"padding: 0 0 0 0\"> <div class=\"form-group\"> <div> <input type=\"text\" number-only maxlength=\"3\" class=\"form-control\" size=\"2\" name=\"LamaKeterlambatanSTNK\" placeholder=\"\" ng-model=\"SelectedDataTerlambat_LamaKeterlambatanSTNK\"> </div> </div> </div> <div class=\"col-md-1\" style=\"margin-top: 8px\"> Hari </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"simpanAlasanRevisi()\" ng-disabled=\"(SelectedDataTerlambat_AlasanKeterlambatanSTNK == undefined || SelectedDataTerlambat_AlasanKeterlambatanSTNK == null || SelectedDataTerlambat_AlasanKeterlambatanSTNK == '') ||\r" +
    "\n" +
    "                                                (SelectedDataTerlambat_LamaKeterlambatanSTNK == undefined || SelectedDataTerlambat_LamaKeterlambatanSTNK == null || SelectedDataTerlambat_LamaKeterlambatanSTNK == '')\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"backFromAlasanRevisi()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </p> </div> <!-- </div> --> </div> <div class=\"ui modal alasanrevisiSTNK\" modal=\"alasanterlambat\" role=\"dialog\"> <!-- <div> --> <div class=\"modal-header\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\"></button> <h4 class=\"modal-title\"><i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Alasan Revisi Penerimaan STNK dari Biro Jasa</h4> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"infoPembeli\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 15px 0 15px; margin-top: 10px 10px 0 10px\"> <p style=\"width:50px; padding:0 5px 0 5px; margin-top:-18px; margin-bottom:15px; background:white\"><b>Alasan</b></p> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataRevisi.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\" style=\"padding-right: 0 !important\"> <label>No. PO</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" placeholder=\"No. PO\" ng-model=\"SelectedDataRevisi.POBiroJasaUnitNo\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-6\" style=\"padding-left: 0 !important; padding-top: 10px !important\"> <label>Model</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"FrameNo\" placeholder=\"\" ng-model=\"SelectedDataRevisi.VehicleModelName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <bsreqlabel>Alasan Revisi</bsreqlabel> <div class=\"form-group\"> <textarea ng-model=\"SelectedDataRevisi.AlasanRevisiSTNK\" rows=\"6\" class=\"form-control\" cols=\"83\">\r" +
    "\n" +
    "                            </textarea> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-3\" style=\"margin-top: 8px; padding:0 5px 0 0\"> <bsreqlabel>Perkiraan Lama Revisi : </bsreqlabel> </div> <div class=\"col-md-2\" style=\"padding: 0 0 0 0\"> <div class=\"form-group\"> <div> <input type=\"text\" number-only maxlength=\"3\" class=\"form-control\" size=\"2\" name=\"LamaKeterlambatanSTNK\" placeholder=\"\" ng-model=\"SelectedDataRevisi.LamaKeterlambatanSTNK\"> </div> </div> </div> <div class=\"col-md-1\" style=\"margin-top: 8px\"> Hari </div> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"center\"> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" onclick=\"this.blur()\" ng-click=\"SaveRevisiSTNK()\" ng-disabled=\"(SelectedDataRevisi.AlasanRevisiSTNK == undefined || SelectedDataRevisi.AlasanRevisiSTNK == null || SelectedDataRevisi.AlasanRevisiSTNK == '') ||\r" +
    "\n" +
    "                                                (SelectedDataRevisi.LamaKeterlambatanSTNK == undefined || SelectedDataRevisi.LamaKeterlambatanSTNK == null || SelectedDataRevisi.LamaKeterlambatanSTNK == '')\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"batalRevisiSTNK()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </p> </div> <!-- </div> --> </div> <div class=\"ui modal revisiStnk\" modal=\"revisiStnk\" role=\"dialog\"> <!-- <div> --> <div class=\"modal-body\"> <p> <h3 align=\"center\"> Peringatan </h3> </p> <p align=\"center\"> Apakah Anda ingin Melakukan Revisi STNK ? </p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <p align=\"right\" style=\"margin-bottom: 0px !important\"> <button ng-click=\"backFromRevisiSTNK()\" class=\"wbtn btn ng-binding ladda-button\">Tidak</button> <button ng-click=\"openSaveRevisiSTNK()\" class=\"rbtn btn ng-binding ladda-button\" style=\"width: 8%\">Ya</button> </p> </div> <!-- </div> --> </div> </div>"
  );

}]);
