angular.module('templates_appboards', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/boards/asb/asbmaintest.html',
    "<div class=\"panel panel-default\" ng-show=\"asb.showMode=='main'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label for=\"usr\">No Polisi:</label> <input ng-model=\"asb.nopol\" type=\"text\" class=\"form-control\" id=\"usr\"> </div> <div class=\"form-group\"> <label for=\"usr\">Tipe:</label> <select ng-model=\"asb.tipe\" name=\"type\" class=\"form-control\"> <option value=\"Avanza\">Avanza</option> <option value=\"Yaris\">Yaris</option> <option value=\"Innova\">Innova</option> <option value=\"Innova Diesel\">Innova Diesel</option> <option value=\"Vios\">Vios</option> </select> </div> <div class=\"form-group\"> <label for=\"usr\">Durasi:</label> <input ng-model=\"mdata.durasi\" type=\"text\" class=\"form-control\" id=\"usr\"> </div> </div> </div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbEstimasi()\">ASB Estimasi</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbProduksi()\">ASB Produksi</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbGr()\">ASB GR</button> </div> </div> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='estimasi'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB Estimasi </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 70%; position: relative; right: 20px; top: 20px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"asb.currentDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div id=\"asb_estimasi_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.FullScreen()\" ng-show=\"false\">Full Screen</button> </div> </div> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='produksi'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB Produksi </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 15px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"asb.startDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 48px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"endDat\" date-options=\"dateOptions\" ng-model=\"asb.endDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 81px; z-index: 1; margin-bottom: -30px\"> <label> <input type=\"radio\" ng-model=\"asb.BpCategory\" ng-value=\"asb.TPS\" ng-change=\"asb.reloadBoard()\">TPS&nbsp;</label> <input type=\"radio\" ng-model=\"asb.BpCategory\" ng-value=\"asb.Light\" ng-change=\"asb.reloadBoard()\">Light </div> <div id=\"asb_produksi_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> </div> </div> <!-- {{asb}} --> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='asbgr'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB GR </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 70%; position: relative; right: 20px; top: 20px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"asb.currentDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div id=\"test_asb_asbgr_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.SaveData()\">Save Data</button> </div> </div> </div>"
  );


  $templateCache.put('app/boards/cstatus/cstatusbp.html',
    "<style>.statusBpBottomDiv{\r" +
    "\n" +
    "        width: 600px;\r" +
    "\n" +
    "        height: 400px;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        top: 0px;\r" +
    "\n" +
    "        left: 0px;\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "        background-color: #000; border-width:0px; border-color: black; border-style: solid; z-index: 20;\r" +
    "\n" +
    "    }</style> <div id=\"base-customer-status-bp\" style=\"background-color: #fff; border-width:0px; border-color: black; border-style: solid; padding: 0px\"> <div id=\"customer-status-bp\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid; height: 100%\"></div> </div> <div id=\"video-customer-status-bp\" class=\"statusBpBottomDiv\"> <video id=\"video-player-customer-status-bp\" style=\"width: 600px; height: 300px\" controls autoplay loop></video> </div> <input id=\"btn-video-customer-status-bp\" type=\"file\" accept=\"video/*\">"
  );


  $templateCache.put('app/boards/cstatus/cstatusgr.html',
    "<style>.statusBottomDiv{\r" +
    "\n" +
    "        width: 600px;\r" +
    "\n" +
    "        height: 385px;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        top: 0px;\r" +
    "\n" +
    "        left: 0px;\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "        background-color: #000; border-width:0px; border-color: black; border-style: solid; z-index: 20;\r" +
    "\n" +
    "    }</style> <div id=\"base-customer-status-gr\" style=\"background-color: #fff; border-width:0px; border-color: black; border-style: solid; padding: 0px\"> <div id=\"customer-status-gr\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid; height: 100%\"></div> </div> <div id=\"video-customer-status-gr\" class=\"statusBottomDiv\"> <video id=\"video-player-customer-status-gr\" style=\"width: 600px; height: 285px\" controls autoplay loop></video> </div> <div class=\"btn-group\"> <input id=\"btn-video-customer-status-gr\" style=\"float: left\" type=\"file\" accept=\"video/*\"> <button class=\"rbtn btn\" ng-click=\"toggleFullScreen()\">Full Screen</button> </div>"
  );


  $templateCache.put('app/boards/factories/showMessage.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">{{showMsg.title}}</h3></div> <div class=\"panel-body\"> <p> {{showMsg.message}}</p> <button type=\"button \" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0) \">{{showMsg.closeText}}</button> </div> </div>"
  );


  $templateCache.put('app/boards/irregularity/irregularity.html',
    "<div class=\"panel panel-default\" id=\"irregular-gr-main-panel\"> <div class=\"panel-heading\"> <!-- <h3 class=\"panel-title pull-left\">\r" +
    "\n" +
    "          <i>Auto Refresh until <b>{{counter}}</b> (s)</i>    \r" +
    "\n" +
    "        <h3 class=\"panel-title pull-left\">\r" +
    "\n" +
    "          Irregularity Monitoring Board (GR)\r" +
    "\n" +
    "        </h3>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <h3 class=\"panel-title pull-right\">\r" +
    "\n" +
    "              {{ clock | date:'dd/MMM/yyyy hh:mm:ss'}}\r" +
    "\n" +
    "        </h3>\r" +
    "\n" +
    "        <div class=\"clearfix\"></div> --> <div class=\"row\"> <div class=\"col-md-2\"> <center><i>Auto Refresh until <b>{{counter}}</b> (s)</i></center> </div> <div class=\"col-md-8\"> <center><h4><b>Irregularity Monitoring Board (GR)</b></h4></center> </div> <div class=\"col-md-2\"> <center>{{ clock | date:'dd/MMM/yyyy hh:mm:ss'}}</center> </div> </div> </div> <div class=\"panel-body\"> <div> <div class=\"row\"> <div class=\"col-xs-2 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading irregularity_head\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-file-text-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{count_reception}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Reception </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irggr-waiting_reception\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-2 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading irregularity_head\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-wrench\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{count_production}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Production </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irggr-waiting_production\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-2 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading irregularity_head\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-check-square-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{count_inspect}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Final Inspection </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irggr-waiting_inspection\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-2 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading irregularity_head\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-list-alt\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{count_teco}}) </h3> <h3 class=\"panel-title text-center\"> Waiting TECO </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irggr-waiting_teco\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-2 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading irregularity_head\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-phone\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{count_notif}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Notification </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irggr-waiting_notif\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-2 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading irregularity_head\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-truck\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{count_delivery}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Delivery </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irggr-waiting_delivery\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/boards/irregularitybp/irregularitybp.html',
    "<style type=\"text/css\">@media (max-width: 1025px) {\r" +
    "\n" +
    "      .panel-title{\r" +
    "\n" +
    "          font-size:12px !important;\r" +
    "\n" +
    "      }\r" +
    "\n" +
    "  }</style> <div class=\"panel panel-default\" id=\"irregular-bp-main-panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Irregularity Monitoring Board (BP) </h3> <!-- style=\"width: 20px;height: 20px;font-size: 20px;margin-top: 0px;\" --> <div class=\"panel-title pull-right\"> <i ng-click=\"refreshData()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" class=\"fa fa-refresh pull-right refreshIcon\"></i> </div> <h3 class=\"panel-title pull-right\"> {{ clock | date:'dd/MMM/yyyy hh:mm:ss'}} </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div> <!-- ===========================4 atas=========================================start --> <div class=\"row\" ng-show=\"showBoard == 'atas'\"> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-file-text-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{count['waiting_ins_approval']}}) </h3> <h3 class=\"panel-title text-center\"> Waiting Insurance Approval </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irgbp-waiting_ins_approval\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-file-text-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_reception']}}) --> ({{jumlahWRECEPTION}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Reception </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irgbp-waiting_reception\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-wrench\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_dispatch']}}) --> ({{jumlahWDISPATCH}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Dispatch </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irgbp-waiting_dispatch\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-wrench\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_production']}}) --> ({{jumlahWPROD}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Production </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irgbp-waiting_production\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> </div> <!-- ===========================4 atas=========================================end --> <!-- ===========================4 bawah=========================================start --> <div class=\"row\" ng-show=\"showBoard == 'bawah'\"> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-check-square-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_final_inspection']}}) --> ({{jumlahWFI}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Final Inspection </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irgbp-waiting_inspection\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-list-alt\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_teco']}}) --> ({{jumlahWTECO}}) </h3> <h3 class=\"panel-title text-center\"> Waiting TECO </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irgbp-waiting_teco\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-phone\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_notification']}}) --> ({{jumlahWFN}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Notification </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irgbp-waiting_notif\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-truck\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_delivery']}}) --> ({{jumlahWDELIV}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Delivery </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"irgbp-waiting_delivery\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> </div> <!-- ===========================4 bawah=========================================end --> </div> </div> </div>"
  );


  $templateCache.put('app/boards/jpcbbp/editChipBP.html',
    "<div class=\"panel-body\"> <form method=\"post\"> <div class=\"row\"> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Nomor WO</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.WoNo\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">No Polisi</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.PoliceNumber\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Tanggal WO</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.FmtWoCreatedDate\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Start Allocation</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.StartAllocation\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Delivery Time</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.DeliveryTime\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Time Ext. (jam)</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.Extension\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"!displayedChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">SA</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.UserSa.EmployeeName\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Stall</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.StallName\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Durasi (Menit)</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.Durasi\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Alasan Pause</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.AlasanPaused\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\" ng-show=\"displayedChip.isSplitable\"> <div class=\"col-md-12\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Untuk dapat memindahkan chip ini ke stall lain, harus dilakukan <b>Split Chip</b> terlebih dahulu.</label> </div> </div> </div> <div class=\"row\" ng-show=\"displayedChip.isSplitable\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\"></label> </div> </div> <div class=\"col-md-7\"> <button type=\"button\" class=\"btn btn-danger\" ng-click=\"execSplitChip(displayedChip.JobId)\">Split Chip</button> </div> </div> </div> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\" xstyle=\"color:white; background-color:#D53337;\"> <h3 class=\"panel-title\">Pekerjaan</h3> </div> <table class=\"panel-body\" width=\"100%\"> <tr> <th class=\"edit-chip-grid-item-title\" style=\"width:65%\">Pekerjaan</th> <th class=\"edit-chip-grid-item-title\" style=\"width:35%\">Metode Perbaikan</th> </tr> <tr ng-repeat-start=\"task in displayedChip.JobTask\"> <td class=\"edit-chip-grid-item-content\" style=\"width:40%\">{{task.TaskName}}</td> <td class=\"edit-chip-grid-item-content\" style=\"width:30%\">{{task.MetodePerbaikan}}</td> </tr> <tr ng-repeat-end> <td class=\"edit-chip-grid-item-content\" colspan=\"4\"> <table style=\"width: 100%\"> <tr> <th class=\"edit-chip-grid-item-title\" style=\"width:50%\">Nama Material</th> <th class=\"edit-chip-grid-item-title\" style=\"width:30%\">Ketersediaan</th> <th class=\"edit-chip-grid-item-title\" style=\"width:20%\">ETA</th> </tr> <tr ng-repeat=\"part in task.JobParts\"> <td class=\"edit-chip-grid-item-content\" style=\"width:50%\"> <input ng-model=\"part.PartsName\" type=\"text\" class=\"edit-chip-item-control\" style=\"width:100%\" ng-disabled=\"true\"> </td> <td class=\"edit-chip-grid-item-content\" style=\"width:30%\"> <input ng-model=\"part.Status\" type=\"text\" class=\"edit-chip-item-control\" style=\"width:100%\" ng-disabled=\"true\"> </td> <td class=\"edit-chip-grid-item-content\" style=\"width:20%\"> <input ng-model=\"part.ETA\" type=\"text\" class=\"edit-chip-item-control\" style=\"width:100%\" ng-disabled=\"true\"> </td> </tr> </table> </td> </tr> </table> </div> </div> </form> </div>"
  );


  $templateCache.put('app/boards/jpcbbp/jpcbbp.html',
    "<style type=\"text/css\">th.active {\r" +
    "\n" +
    "        background-color: red;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .border-medium {\r" +
    "\n" +
    "        border-width: 2px;\r" +
    "\n" +
    "        border-style: solid;\r" +
    "\n" +
    "        border-radius: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .border-thin {\r" +
    "\n" +
    "        border-width: 1px;\r" +
    "\n" +
    "        border-style: solid;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-area-field-label {\r" +
    "\n" +
    "        font-size: 11px;\r" +
    "\n" +
    "        background-color: floralwhite;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        top: -20px;\r" +
    "\n" +
    "        left: -40px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-overlay {\r" +
    "\n" +
    "        /*background-color: #ccc;*/\r" +
    "\n" +
    "        background-color: #fff;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .blurred {\r" +
    "\n" +
    "        /*filter: url(\"/filters.svg#blur3px\");\r" +
    "\n" +
    "        -webkit-filter: blur(3px);\r" +
    "\n" +
    "        -moz-filter: blur(3px);\r" +
    "\n" +
    "        -o-filter: blur(3px);\r" +
    "\n" +
    "        filter: blur(3px);*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-outline {\r" +
    "\n" +
    "        /*background: url('../images/outline.gif');*/\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .choosen-area {\r" +
    "\n" +
    "        /*background-color: red;*/\r" +
    "\n" +
    "        border: 2px solid red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-resize-handler {\r" +
    "\n" +
    "        border: 2px #fff solid;\r" +
    "\n" +
    "        height: 10px;\r" +
    "\n" +
    "        width: 10px;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        background-color: #000;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-delete-area {\r" +
    "\n" +
    "        background: url('../images/bt-delete.png');\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        height: 20px;\r" +
    "\n" +
    "        width: 20px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .delete-area {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        padding: 1px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk {\r" +
    "\n" +
    "        border: 1px solid red;\r" +
    "\n" +
    "        padding: 10px 10px 40px 10px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        margin: 0 auto 0 auto;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature {\r" +
    "\n" +
    "        border: 1px solid orange;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature canvas {\r" +
    "\n" +
    "        border: 1px solid #999;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .buttons {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        bottom: 10px;\r" +
    "\n" +
    "        left: 10px;\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .buttonsamk {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        bottom: 2px;\r" +
    "\n" +
    "        left: 10px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature_container {\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .result {\r" +
    "\n" +
    "        border: 1px solid blue;\r" +
    "\n" +
    "        margin: 30px auto 0 auto;\r" +
    "\n" +
    "        height: 220px;\r" +
    "\n" +
    "        width: 568px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio] {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio]+label {\r" +
    "\n" +
    "        background-color: grey;\r" +
    "\n" +
    "        color: #fff;\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        -webkit-border-radius: 2px;\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "        font-weight: 400;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        width: 100px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio]:checked+label {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*====================*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-bar {\r" +
    "\n" +
    "        background: #ffe4d1;\r" +
    "\n" +
    "        height: 2px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-selection {\r" +
    "\n" +
    "        background: orange;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-pointer {\r" +
    "\n" +
    "        width: 8px;\r" +
    "\n" +
    "        height: 16px;\r" +
    "\n" +
    "        top: auto;\r" +
    "\n" +
    "        /* to remove the default positioning */\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "        /*background-color: #333;*/\r" +
    "\n" +
    "        background-color: red;\r" +
    "\n" +
    "        border-top-left-radius: 3px;\r" +
    "\n" +
    "        border-top-right-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-pointer:after {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-bubble {\r" +
    "\n" +
    "        bottom: 14px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-limit {\r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "        color: orange;\r" +
    "\n" +
    "        font-size: 20px;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-tick {\r" +
    "\n" +
    "        width: 1px;\r" +
    "\n" +
    "        height: 10px;\r" +
    "\n" +
    "        margin-left: 4px;\r" +
    "\n" +
    "        border-radius: 0;\r" +
    "\n" +
    "        background: #ffe4d1;\r" +
    "\n" +
    "        top: -1px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-tick.rz-selected {\r" +
    "\n" +
    "        background: orange;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .scaleWarp {\r" +
    "\n" +
    "        margin-bottom: -40px;\r" +
    "\n" +
    "        /*padding:16px;*/\r" +
    "\n" +
    "        padding-bottom: 0px;\r" +
    "\n" +
    "        padding-left: 1px;\r" +
    "\n" +
    "        letter-spacing: -10px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        /*float:left;*/\r" +
    "\n" +
    "        /*width:100%;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #slide {\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*.scaleWarp span+span{*/\r" +
    "\n" +
    "    /*margin-left:23%;*/\r" +
    "\n" +
    "    /*vertical-align:bottom;*/\r" +
    "\n" +
    "    /*bottom:0;*/\r" +
    "\n" +
    "    /*}*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .scaleWarp garis {\r" +
    "\n" +
    "        margin-left: 24.75%;\r" +
    "\n" +
    "        vertical-align: bottom;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .radioCustomer input[type=radio][aria-checked=\"true\"] + label {\r" +
    "\n" +
    "        color: #333 !important;\r" +
    "\n" +
    "        background-color: red !important;\r" +
    "\n" +
    "        border-color: #adadad !important;\r" +
    "\n" +
    "    }</style> <div style=\"margin-left: 10px; margin-right: 10px\"> <div class=\"row\"> <div ng-class=\"wcolumn\"> <div class=\"panel panel-default\" id=\"jpcbbp-board-panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> JPCB BP </h3> <h3 class=\"panel-title pull-right\"> <!--img src=\"images/loading.gif\" style=\"width: 24px\" xng-show=\"isLoading\"--> </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row row-board\" ng-show=\"JpcbBpData.screen_step==1\"> <div class=\"col-xs-4 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title text-center\"> Menunggu Alokasi ({{countAllocatedNumber}}) </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"btn-group\" data-toggle=\"buttons\" style=\"width:80%\"> <label class=\"btn btn-default\" ng-repeat=\"(key, catitem) in BpCategory\" style=\"width:25%\" ng-click=\"filterDataByType(catitem.type)\"> <input type=\"radio\" name=\"options\" id=\"option5\">{{catitem.name}}<br>{{catitem.count}}&nbsp; </label> </div> <!-- <div class=\"btn-group\" style=\"width:18%\">\r" +
    "\n" +
    "                                        <label>\r" +
    "\n" +
    "                                            <input type=\"checkbox\" class=\"checkbox style-0 ng-pristine ng-untouched ng-valid ng-empty\" \r" +
    "\n" +
    "                                                ng-true-value=\"1\" ng-false-value=\"0\" ng-model=\"isEditing\" ng-change=\"filterDataByType(null)\">\r" +
    "\n" +
    "                                            <span class=\"ng-binding\">Edit</span>\r" +
    "\n" +
    "                                        </label>\r" +
    "\n" +
    "                                    </div> --> </div> <div class=\"panel-footer\" ng-form=\"filterBy\"> <div class=\"input-icon-right panel-title pull-left\" style=\"margin-top: -4px; margin-left: 0 px; width: 160px; height: 32px\"> <i class=\"icon-search ng-binding\" style=\"margin-top: -3px; font-size:12px\"></i> <input name=\"searchnopol\" type=\"search\" class=\"form-control ng-pristine ng-valid ng-empty ng-touched\" style=\"font-size: 13px; text-transform: uppercase\" ng-minlength=\"3\" ng-model=\"mFilter.NomorPolisi\" placeholder=\"Nomor Polisi\"> </div> <button type=\"button\" ng-disabled=\"filterBy.searchnopol.$error.minlength\" class=\"btn btn-default panel-title pull-left\" ng-click=\"searchNoPol(mFilter.NomorPolisi)\" style=\"margin-top: -4px; margin-right: 4px; margin-left: 6px; padding-bottom: 4px; height: 32px\" tabindex=\"0\"> <i class=\"fa fa-fw fa-search\"></i> </button> <div class=\"clearfix\"></div> <em ng-messages=\"filterBy.searchnopol.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"minlength\">Jumlah karakter kurang</p> </em> </div> </div> </div> <div class=\"col-xs-8 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-body irregularity_panel\"> <div id=\"jpcbbp-viewcontainer\" style=\"height: 122px; background-color: #ddd\"></div> </div> <div class=\"panel-footer\"> <button class=\"btn btn-default\" ng-click=\"UnselectChip()\">Unselect</button> <button class=\"btn btn-default\" ng-click=\"nextSelectChip()\">Next <i class=\"fa fa-arrow-right\"></i></button> </div> </div> </div> </div> <div class=\"row row-board\" ng-show=\"JpcbBpData.screen_step==2\"> <div class=\"col-xs-12 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <div class=\"pull-right\" style=\"width: 200px\"> <bsdatepicker name=\"startDate\" date-options=\"DateOptions\" ng-model=\"JpcbBpData.cuarrentDate\" ng-change=\"showProductionBoardByDate()\"></bsdatepicker> </div> <h3 class=\"panel-title pull-left\"> Please Select Group to Allocate Job </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row row-board\"> <div class=\"col-xs-4\" style=\"border-style: solid; border-width: 0.5px; border-color: #ddd; margin-top:  -4px;padding-top: 4px\"> <div class=\"row row-board\"> <div class=\"col-xs-5\" style=\"padding-left: 0px\">Nomor Polisi</div> <div class=\"col-xs-7\" style=\"padding-left: 0px\">{{selectedChip.nopol}}</div> </div> <div class=\"row row-board\"> <div class=\"col-xs-5\" style=\"padding-left: 0px\">Kedatangan</div> <div class=\"col-xs-7\" style=\"padding-left: 0px\">{{selectedChip.waktudtg}}</div> </div> <div class=\"row row-board\"> <div class=\"col-xs-5\" style=\"padding-left: 0px\">Janji Penyerahan</div> <div class=\"col-xs-7\" style=\"padding-left: 0px\">{{selectedChip.tgljanjiserah}} {{selectedChip.jamjanjiserah}}</div> </div> </div> <div class=\"col-xs-1\"> <div class=\"btn-group\" style=\"margin-left: 10%; width: 80%\"> <label class=\"btn btn-default\" style=\"width: 100%\" ng-click=\"backSelectChip()\"><i class=\"fa fa-arrow-left\"></i><br>Back</label> </div> </div> <div class=\"col-xs-7\"> <div class=\"btn-group radioCustomer\" data-toggle=\"buttons\"> <label ng-class=\"{'active':JpcbBpData.select == team.GroupBPId}\" class=\"btn btn-default\" for=\"option{{$index}}\" ng-repeat=\"team in JpcbBpData.teamList\" ng-click=\"showProductionBoard(team, JpcbBpData.startDate)\"> <input type=\"radio\" name=\"options\" id=\"option{{$index}}\" ng-value=\"{{team.GroupBPId}}\" ng-model=\"JpcbBpData.select\">{{team.name}} <br>({{team.count}}) </label> </div> </div> </div> </div> </div> </div> </div> <div class=\"row row-board\" ng-show=\"JpcbBpData.showProdBoard==true\"> <div class=\"col-xs-12 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-body irregularity_panel\"> <div id=\"jpcbbp-prod-container\" style=\"background-color: #f8f8f8\"></div> </div> </div> </div> </div> <div class=\"row row-board\" ng-show=\"JpcbBpData.screen_step==2\"> <div class=\"col-xs-12 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-footer col-md-12\"> <div class=\"btn-group col-md-12\"> <!-- <button type=\"button\" class=\"btn btn-default btn-lg col-md-3\" ng-click=\"additionalJob()\">Additional Job</button> --> <button type=\"button\" class=\"btn btn-default btn-lg col-md-3\" ng-click=\"additionalJob()\"><i class=\"fa fa-refresh\"></i> Redo</button> <button type=\"button\" class=\"btn btn-default btn-lg col-md-3\" ng-click=\"pausedJob()\"><i class=\"fa fa-pause\"></i> Pause</button> <button type=\"button\" class=\"btn btn-default btn-lg col-md-3\" ng-click=\"waitingFI()\"><img src=\"images/boards/button/iconFI.png\" alt=\"\" height=\"20\" width=\"20\"> Waiting for FI</button> <button type=\"button\" class=\"btn btn-danger btn-lg col-md-3\" ng-click=\"delayedJob()\"><i class=\"fa fa-exclamation-circle\"></i> Delay</button> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"testChip()\" ng-show=\"false\">Testing 1</button> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"testDialog()\" ng-show=\"false\">Testing 2</button> </div> </div> </div> </div> </div> </div> <!-- WO LIST START HERE --> <div class=\"panel-body\"> <div class=\"row row-board\" ng-show=\"JpcbBpData.screen_step==3\"> <div class=\"row\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"BackFromWO()\">Back</button> </div> <uib-accordion close-others=\"oneAtATime\"> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Short Brief </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Pemilik</label> <input ng-model=\"mDataCustomer.owner\" type=\"text\" skip-enable disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Pengguna</label> <input ng-model=\"mDataCustomer.user\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Alamat</label> <input ng-model=\"mDataCustomer.address\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>On Time/Late</label> <input ng-model=\"mDataCustomer.late\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <!--<div class=\"form-group\">\r" +
    "\n" +
    "                                                        <label>Waiting Time</label>\r" +
    "\n" +
    "                                                        <input ng-model=\"mDataCustomer.waitTime\" skip-enable type=\"text\" disabled class=\"form-control\">\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                                                    </div>--> <div class=\"form-group\"> <label>Model</label> <input ng-model=\"mDataCustomer.model\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-12\"> <div class=\"well\"> <div class=\"form-group\"> <label>Job Suggest</label> <textarea class=\"form-control\" skip-enable ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\"></textarea> </div> </div> </div> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Field Action\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Operation No.</font> </td> <td> <font color=\"white\">Description</font> </td> </tr> <tr ng-repeat=\"a in GetDataTowass\"> <td>{{a.OpeNo}}</td> <td>{{a.Description}}</td> </tr> </table> </div> </uib-tab> <uib-tab index=\"1\" heading=\"PSFU\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table id=\"QA\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>No.</th> <th>Pertanyaan</th> <th>Jawaban Customer</th> <!-- <th>Alasan Customer</th> --> </tr> </thead> <tbody> <tr ng-repeat=\"item in Question track by $index\"> <td width=\"5%\">{{$index + 1}}</td> <td width=\"40%\">{{item.Description}}</td> <td width=\"25%\"> <select class=\"form-control\" name=\"yesOrNo\" ng-disabled=\"true\" skip-enable> <option ng-disabled=\"true\" skip-enable ng-repeat=\"itemAnswer in item.Answer track by $index\" value=\"{{itemAnswer.AnswerId}}\">{{itemAnswer.Description}}</option> </select> </td> </tr> </tbody> </table> </div> </uib-tab> <uib-tab index=\"2\" heading=\"CS Survey\"> </uib-tab> </uib-tabset> </div> </div> </div> </div> </div> <div uib-accordion-group is-open=\"true\" class=\"panel-default\"> <uib-accordion-heading> Plotting Stall </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-9\"> </div> <div class=\"col-md-3\"> <div class=\"form-group form-inline\" ng-hide=\"true\"> <bsreqlabel>Estimasi Durasi</bsreqlabel> <input type=\"number\" min=\"0.1\" max=\"9\" step=\"0.1\" class=\"form-control\" style=\"width:100px\" name=\"EstimateHour\" ng-disabled=\"true\" skip-enable ng-change=\"estimationDuration(mData.EstimateHour)\" ng-model=\"mData.EstimateHour\"> <label>Jam</label> </div> <div class=\"form-group\"> <bsdatepicker name=\"startDate\" disabled skip-enable ng-disabled=\"true\" date-options=\"dateOptions\" ng-model=\"asb.startDate\" ng-change=\"reloadBoard(asb.startDate)\" min-date=\"minDateASB\" skip-enable></bsdatepicker> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"plottingStall\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> </div> </div> </div> </div> </div> <!--  --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Informasi Umum </uib-accordion-heading> <div id=\"CustIndex\" class=\"row\"> <div smart-include=\"app/services/reception/woHistoryGR/customerIndex.html\"> </div> </div> </div> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "                                    <uib-accordion-heading>\r" +
    "\n" +
    "                                        Kontak Person & Pengambil Keputusan\r" +
    "\n" +
    "                                    </uib-accordion-heading>\r" +
    "\n" +
    "                                    <div class=\"col-md-12\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                        <div id=\"CPPKInfo\" class=\"row\">\r" +
    "\n" +
    "                                            <div smart-include=\"app/services/reception/includeForm/CPPKInfo.html\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div> --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Riwayat Servis </uib-accordion-heading> <div id=\"RiwayatServis\"> <vehicle-history ng-disabled=\"true\" skip-enable vin=\"mDataCrm.Vehicle.VIN\"> </vehicle-history> </div> </div> <div uib-accordion-group class=\"panel-default\" id=\"OrderPekerjaan\"> <uib-accordion-heading> Order Pekerjaan </uib-accordion-heading> <div class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\"> <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Permintaan\" list-title=\"Permintaan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                                    </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Job Request\" name=\"reqdesc\" ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "                                                                    </textarea> <bserrmsg field=\"reqdesc\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\" style=\"text-align:right\"> <div class=\"btn-group\" style=\"margin-top:25px\"> <button type=\"button\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\" disabled> Pre Diagnose </button> </div> </div> <div class=\"col-md-12\"> <bslist data=\"JobComplaint\" m-id=\"ComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Keluhan\" list-title=\"Keluhan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Complaint Category\" ng-disabled=\"true\" skip-enable> </bsselect> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Complaint Text\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                                                </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\"> <bsreqlabel>Tipe Keluhan</bsreqlabel> <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Complaint Category\"> </bsselect> <bserrmsg field=\"ctype\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\">\r" +
    "\n" +
    "                                                                                </textarea> <bserrmsg field=\"ctext\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nomor WO Estimasi</label> <input type=\"text\" class=\"form-control\" name=\"NomorWo\" ng-model=\"mData.NomorWoEstimasi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Kategori WO</bsreqlabel> <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\"> <!--  ng-change=\"PDS(mData.WoCategoryId)\" --> </bsselect> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"JobTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" on-before-edit=\"onBeforeEdit\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApiJob\" confirm-save=\"false\" list-title=\"Job List\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\">{{totalWork | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalWork - totalWorkDiscounted | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalWorkDiscounted | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\">{{totalMaterial | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalMaterialDiscounted | currency:\"Rp. \":0}}</td> </tr> <!-- <tr>\r" +
    "\n" +
    "                                                        <td>Estimasi OPL</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                        <td></td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                    </tr> --> <tr> <td>PPN</td> <td style=\"text-align: right\"> <!-- {{((totalWork + totalMaterial) * 10)/100 | currency:\"Rp. \":0}} --> </td> <td></td> <td style=\"text-align: right\"> {{(totalMaterial + totalWork) + (((totalWork + totalMaterial) * 10)/100) | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"> <!-- {{totalWork + totalMaterial | currency:\"Rp. \":0}}</td> --> </td><td></td> <!-- <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                                                    {{(totalMaterial - totalMaterialDiscounted)+(totalWork - totalWorkDiscounted) | currency:\"Rp. \":0}}\r" +
    "\n" +
    "                                                                </td> --> <td style=\"text-align: right\">{{(totalMaterial + totalWork) + (((totalWork + totalMaterial) * 10)/100) | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\">{{totalDp | currency:\"Rp. \":0}}</td> <td></td> <td></td> </tr> </tbody> </table> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" id=\"OPL\"> <uib-accordion-heading> OPL </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <bslist data=\"oplData\" m-id=\"OPLId\" list-model=\"lmModelOpl\" on-before-new=\"onBeforeNewOPL\" on-before-save=\"onBeforeSaveOpl\" on-before-edit=\"onBeforeEditOPL\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <strong>{{lmItem.OPLName}}</strong> </div> <div class=\"form-group\"> {{lmItem.VendorName}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Estimasi : </label> {{lmItem.TargetFinishDate | date:'dd-MM-yyyy'}} </div> <div class=\"form-group\"> <label>Quantity : </label> {{lmItem.QtyPekerjaan}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}} </div> <div class=\"form-group\"> <label>Status : {{lmItem.xStatus}}</label> </div> </div> </div> </bslist-template> <bslist-modal-form> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMasterOpl.html'\"></div> </bslist-modal-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <tbody> <tr> <td width=\"20%\">Total</td> <td>{{sumWorkOpl | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" ng-click=\"dataForWAC()\" is-open=\"accordionWac.isOpen\" id=\"WAC\"> <uib-accordion-heading> WAC <span ng-click=\"dataForWAC()\"></span> </uib-accordion-heading> <!--<div ng-include=\"'app/services/reception/wo/includeForm/wac.html'\">\r" +
    "\n" +
    "                                         </div> --> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">INTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Km</bsreqlabel> <input type=\"text\" step=\"1\" min=\"0\" class=\"form-control\" name=\"Km\" placeholder=\"Km\" ng-model=\"mData.Km\" ng-maxlength=\"7\" maxlength=\"7\" ng-keyup=\"givePattern(mData.Km,$event)\"> <!-- <bserrmsg field=\"km\"></bserrmsg> --> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                                                <label for=\"slider\">Fuel</label>\r" +
    "\n" +
    "                                                <rzslider rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider>\r" +
    "\n" +
    "                                            </div> --> <div id=\"slide\" class=\"col-md-6\"> <label for=\"slider\">Fuel</label> <div class=\"scaleWarp\"> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> <garis>|</garis> <garis style=\"font-size: 20px;font-weight:bold\">|</garis> <garis>|</garis> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> </div> <rzslider class=\"custom-slider\" rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.BanSerep\" label=\"Ban Serep\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.CDorKaset\" label=\"CD/Kaset\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Payung\" label=\"Payung\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Dongkrak\" label=\"Dongkrak\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.STNK\" label=\"STNK\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.P3k\" label=\"P3K\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.KunciSteer\" label=\"Kunci Steer\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ToolSet\" label=\"Tool Set\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ClipKarpet\" label=\"Clip Karpet\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.BukuService\" label=\"Buku Service\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Alarm Condition</bsreqlabel> <bsselect name=\"alarmCondition\" ng-model=\"mData.AlarmCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition Alarm\"> </bsselect> <!-- <bserrmsg field=\"alarmCondition\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>AC Condition</bsreqlabel> <bsselect name=\"AcCondition\" ng-model=\"mData.AcCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition AC\"> </bsselect> <!-- <bserrmsg field=\"AcCondition\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Power Window</bsreqlabel> <bsselect name=\"PowerWindow\" ng-model=\"mData.PowerWindow\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition Power Window\"> </bsselect> <!-- <bserrmsg field=\"PowerWindow\"></bserrmsg> --> </div> </div> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "                                                <div class=\"form-group\">\r" +
    "\n" +
    "                                                    <bsreqlabel>Jenis Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                                                    <bsselect name=\"CarType\" ng-model=\"mData.CarType\" data=\"carTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Jenis Kendaraan\">\r" +
    "\n" +
    "                                                    </bsselect>\r" +
    "\n" +
    "                                                    <bserrmsg field=\"CarType\"></bserrmsg>\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div> --> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Other</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Other\" name=\"Other\" ng-model=\"mData.OtherDescription\">\r" +
    "\n" +
    "                                                    </textarea> <!-- <bserrmsg field=\"Other\"></bserrmsg> --> </div> </div> <div class=\"col-md-6\"> <bsreqlabel>Money Amount</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Money Amount\" name=\"moneyAmount\" ng-model=\"mData.moneyAmount\">\r" +
    "\n" +
    "                                                </textarea> <!-- <bserrmsg field=\"moneyAmount\"></bserrmsg> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">EXTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Type Kendaraan</label> <bsselect name=\"filterType\" ng-model=\"filter.vType\" data=\"VTypeData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Type Kendaraan\" on-select=\"chooseVType(selected)\" icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <!-- <div class=\"row\" ng-repeat=\"imageWAC in imageWACArr\">\r" +
    "\n" +
    "                                                            <img ng-if=\"imageSel==imageWAC.imgId\" ng-areas=\"areasArray\" \r" +
    "\n" +
    "                                                                ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\"\r" +
    "\n" +
    "                                                                ng-src=\"{{imageWAC.img}}\" style=\"width:700px;height:400px\"> --> <!-- <img ng-if=\"imageSel==imageWAC.imgId\" ng-src=\"{{imageWAC.img}}\" style=\"width:700px;height:400px\"> --> <!-- </div> --> <div class=\"row\"> <div class=\"col-md-12\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"800\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> <!-- <pre>{{logDots}}</pre>  --> </div> </div> <div class=\"row\" ng-show=\"!disableWACExt\"> <div class=\"col-md-12\"> <div ui-grid=\"gridWac\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize ui-grid-edit class=\"grid\" ng-style=\"getTableHeight()\"> <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </div> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                                <bsreqlabel>Jenis Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                                                <bsselect name=\"CarType\" ng-model=\"mData.CarType\" data=\"carTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Jenis Kendaraan\">\r" +
    "\n" +
    "                                                </bsselect>\r" +
    "\n" +
    "                                                <bserrmsg field=\"CarType\"></bserrmsg>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                        <div class=\"col-md-12\" style=\"text-align:center;\">\r" +
    "\n" +
    "                                            <pitch-canvas data='drawData' status=\"paramClosePopover\"></pitch-canvas>\r" +
    "\n" +
    "                                            <div id='myPopover' ng-show='showPopup' ng-style=\"popoverStyle\">\r" +
    "\n" +
    "                                                <div class=\"form-group\">\r" +
    "\n" +
    "                                                    <button ng-click=\"closePopover(1)\">Baret</button>\r" +
    "\n" +
    "                                                    <button ng-click=\"closePopover(2)\">Penyok</button>\r" +
    "\n" +
    "                                                    <button ng-click=\"closePopover(3)\">Rusak</button>\r" +
    "\n" +
    "                                                    <button ng-click=\"closePopover(0)\">Delete</button>\r" +
    "\n" +
    "                                                    <p><span>Y : {{coorY}} - X : {{coorX}}</span></p>\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div> --> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <!-- <button class=\"btn rbtn\" ng-click=\"disableExt\">Clear</button> --> <!-- <button type=\"button\" class=\"btn rbtn\" ng-model=\"disableWACExt\" uib-btn-checkbox btn-checkbox-true=\"1\" btn-checkbox-false=\"0\">Clear</button> --> <!-- <button class=\"btn rbtn\" ng-disabled=\"disableBPCenter\" skip-enable ng-click=\"\">BP Center</button> --> <!-- added by sss on 2017-09-07 --> <bscheckbox ng-model=\"disableWACExt\" label=\"Clear\" true-value=\"true\" false-value=\"false\" ng-change=\"chgChk()\"> </bscheckbox> <!-- <div class=\"row\">\r" +
    "\n" +
    "                                                    <bsdatepicker \r" +
    "\n" +
    "                                                            name=\"bpCenterDate\" \r" +
    "\n" +
    "                                                            date-options=\"DateOptions\" \r" +
    "\n" +
    "                                                            ng-model=\"bpCenterDate\" \r" +
    "\n" +
    "                                                            ng-change=\"setCenterDate()\">\r" +
    "\n" +
    "                                                    </bsdatepicker>\r" +
    "\n" +
    "                                                </div> --> <div class=\"row\" style=\"margin-top:10px\"> <button class=\"btn rbtn\" ng-disabled=\"disableBPCenter\" skip-enable ng-click=\"triggerBPC()\"> BP Center </button> </div> <!--  --> </div> <div class=\"col-md-8\"> <div class=\"containeramk\" ng-style=\"{'max-width': boundingBox.width + 'px', 'max-height': boundingBox.height + 'px'}\"> <signature-pad accept=\"accept\" clear=\"clear\" height=\"220\" width=\"568\" dataurl=\"dataurl\"></signature-pad> <div class=\"buttonsamk\"> <button class=\"btn wbtn\" ng-disabled=\"disableReset\" skip-enable ng-click=\"clear()\">Clear</button> <!-- <button class=\"btn rbtn\" ng-click=\"dataurl = signature.dataUrl\" ng-disabled=\"!signature\">Reset</button> --> <button class=\"btn rbtn\" ng-click=\"signature = accept(); open(signature)\">Sign</button> </div> </div> </div> <div class=\"col-md-2\"> <!-- <input type=\"button\" class=\"btn rbtn\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" isImage=\"1\" img-upload=\"uploadFiles\"><i class=\"fa fa-fw fa-lg fa-car\" style=\"font-size:30px; padding:8px 8px 8px 0px;margin-left:8px;margin-right:8px;\"></i>\r" +
    "\n" +
    "                                                <input type=\"file\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" isImage=\"1\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1;\"> --> <!-- <label class=\"input-group-btn\">\r" +
    "\n" +
    "                                                    <span class=\"btn rbtn\" style=\"padding:12px 10px 12px 10px;\">\r" +
    "\n" +
    "                                                        <i class=\"fa fa-fw fa-lg fa-car\" style=\"font-size:55px;\"></i>\r" +
    "\n" +
    "                                                        <input type=\"file\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" img-upload=\"fileKK\" style=\"display: none;\" uploadFiles=\"uploadWACImage\" multiple>\r" +
    "\n" +
    "                                                    </span>\r" +
    "\n" +
    "                                                </label> --> <image-control max-upload=\"4\" is-image=\"1\" img-upload=\"uploadFiles\" multiple> <form-image-control> <input capture=\"camera\" type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"file\" ng-init=\"mData.UploadPhoto = uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> <!-- <button ng-click=\"TestingImage(mData)\">Testing Image</button> --> <!-- <div class=\"custom-file-upload\">\r" +
    "\n" +
    "                                                    <input type=\"file\" id=\"file\" name=\"myfiles[]\" bs-upload=\"onFileSelect($files)\" img-upload=\"fileKK\" file-model=\"mData.Attachment\" upload />\r" +
    "\n" +
    "                                                </div>  --> </div> </div> </div> </div> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "                                    <uib-accordion-heading>\r" +
    "\n" +
    "                                        Permintaan\r" +
    "\n" +
    "                                    </uib-accordion-heading>\r" +
    "\n" +
    "                                    <div class=\"row\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                        <div class=\"col-md-12\">\r" +
    "\n" +
    "                                            <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Job Request\" list-title=\"Job Request\"\r" +
    "\n" +
    "                                                list-height=\"200\">\r" +
    "\n" +
    "                                                <bslist-template>\r" +
    "\n" +
    "                                                    <div>\r" +
    "\n" +
    "                                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                                            <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                            </textarea>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-template>\r" +
    "\n" +
    "                                                <bslist-modal-form>\r" +
    "\n" +
    "                                                    <div>\r" +
    "\n" +
    "                                                        <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                            <bsreqlabel>Job Request</bsreqlabel>\r" +
    "\n" +
    "                                                            <textarea class=\"form-control\" placeholder=\"Job Request\" name='reqdesc' ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "                                                    </textarea>\r" +
    "\n" +
    "                                                            <bserrmsg field=\"reqdesc\"></bserrmsg>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-modal-form>\r" +
    "\n" +
    "                                            </bslist>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div> --> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "                                    <uib-accordion-heading>\r" +
    "\n" +
    "                                        Keluhan\r" +
    "\n" +
    "                                    </uib-accordion-heading>\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                                    <div class=\"row\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                        <div class=\"col-md-12\" style=\"text-align:right;\">\r" +
    "\n" +
    "                                            <div class=\"btn-group\" style=\"margin-top:25px;\">\r" +
    "\n" +
    "                                                <button type=\"button\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\">\r" +
    "\n" +
    "                                              Pre Diagnose\r" +
    "\n" +
    "                                            </button>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                        <div class=\"col-md-12\">\r" +
    "\n" +
    "                                            <bslist data=\"JobComplaint\" m-id=\"ComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Complaint\"\r" +
    "\n" +
    "                                                list-title=\"Complaint\" list-height=\"200\">\r" +
    "\n" +
    "                                                <bslist-template>\r" +
    "\n" +
    "                                                    <div>\r" +
    "\n" +
    "                                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                                            <bsreqlabel>Complaint Type: </bsreqlabel>\r" +
    "\n" +
    "                                                            <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Complaint Category\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                            </bsselect>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                                            <textarea class=\"form-control\" placeholder=\"Complaint Text\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                                        </textarea>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-template>\r" +
    "\n" +
    "                                                <bslist-modal-form>\r" +
    "\n" +
    "                                                    <div>\r" +
    "\n" +
    "                                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                                            <bsreqlabel>Complaint Type</bsreqlabel>\r" +
    "\n" +
    "                                                            <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Complaint Category\">\r" +
    "\n" +
    "                                                            </bsselect>\r" +
    "\n" +
    "                                                            <bserrmsg field=\"ctype\"></bserrmsg>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                                            <bsreqlabel>Job Complaint</bsreqlabel>\r" +
    "\n" +
    "                                                            <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\">\r" +
    "\n" +
    "                                                                        </textarea>\r" +
    "\n" +
    "                                                            <bserrmsg field=\"ctext\"></bserrmsg>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-modal-form>\r" +
    "\n" +
    "                                            </bslist>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div> --> <div uib-accordion-group class=\"panel-default\" id=\"JobSuggest\"> <uib-accordion-heading> Job Suggest </uib-accordion-heading> <div class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\" style=\"margin-top: 10px\"> <div class=\"row\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editSuggest()\" ng-hide=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"SimpanJobSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" style=\"margin-top: 25px\"> <div class=\"col-md-12\"> <textarea ng-readonly=\"disSgs\" ng-model=\"mData.Suggestion\" maxlength=\"200\" style=\"width: 100%;height: 100px\"></textarea> </div> </div> </div> </div> </div> <!--         <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "                                    <uib-accordion-heading>\r" +
    "\n" +
    "                                        Job List\r" +
    "\n" +
    "                                    </uib-accordion-heading>\r" +
    "\n" +
    "                                    <div class=\"row\">\r" +
    "\n" +
    "                                        <div class=\"col-md-6\">\r" +
    "\n" +
    "                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                <label>Nomor WO Estimasi</label>\r" +
    "\n" +
    "                                                <input type=\"text\" min=0 class=\"form-control\" name=\"NomorWo\" ng-model=\"mData.NomorWoEstimasi\">\r" +
    "\n" +
    "                                                <bserrmsg field=\"NomorWo\"></bserrmsg>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                        <div class=\"col-md-6\">\r" +
    "\n" +
    "                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                <bsreqlabel>Kategori WO</bsreqlabel>\r" +
    "\n" +
    "                                                <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\">\r" +
    "\n" +
    "                                                </bsselect>\r" +
    "\n" +
    "                                                <bserrmsg field=\"categoryWO\"></bserrmsg>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                        <div class=\"col-md-12\">\r" +
    "\n" +
    "                                            <bslist data=\"gridWork\" m-id=\"JobTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" on-before-edit=\"onBeforeEdit\" list-model=\"lmModel\"\r" +
    "\n" +
    "                                                list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api='listApiJob' confirm-save=\"false\" list-title=\"Job List\" button-settings=\"ButtonList\" modal-title=\"Job Data\"\r" +
    "\n" +
    "                                                modal-title-detail=\"Parts Data\">\r" +
    "\n" +
    "                                                <bslist-template>\r" +
    "\n" +
    "                                                    <div class=\"col-md-6\" style=\"text-align:left;\">\r" +
    "\n" +
    "                                                        <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p>\r" +
    "\n" +
    "                                                        <div class=\"col-md-3\">\r" +
    "\n" +
    "                                                            <p>Tipe :{{lmItem.catName}}</p>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                        <div class=\"col-md-3\">\r" +
    "\n" +
    "                                                            <p>Flat Rate: {{lmItem.FlatRate}}</p>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                    <div class=\"col-md-6\" style=\"text-align:right;\">\r" +
    "\n" +
    "                                                        <p>Pembayaran: {{lmItem.PaidBy}}</p>\r" +
    "\n" +
    "                                                        <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-template>\r" +
    "\n" +
    "                                                <bslist-detail-template>\r" +
    "\n" +
    "                                                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                                                        <p><strong>{{ ldItem.PartsCode }}</strong></p>\r" +
    "\n" +
    "                                                        <p>{{ ldItem.PartsName }}</p>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                                                        <p>{{ ldItem.Availbility }}</p>\r" +
    "\n" +
    "                                                        <p>Pembayaran : {{ ldItem.paidName}}</p>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                                                        <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p>\r" +
    "\n" +
    "                                                        <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp.\"}}</p>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                                                        <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p>\r" +
    "\n" +
    "                                                        <p>Reserve : {{ ldItem.Qty }}</p>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-detail-template>\r" +
    "\n" +
    "                                                <bslist-modal-form>\r" +
    "\n" +
    "                                                    <div class=\"row\">\r" +
    "\n" +
    "                                                        <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\">\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-modal-form>\r" +
    "\n" +
    "                                                <bslist-modal-detail-form>\r" +
    "\n" +
    "                                                    <div class=\"row\">\r" +
    "\n" +
    "                                                        <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\">\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-modal-detail-form>\r" +
    "\n" +
    "                                            </bslist>\r" +
    "\n" +
    "                                            <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "                                                <thead>\r" +
    "\n" +
    "                                                    <tr>\r" +
    "\n" +
    "                                                        <th>Item</th>\r" +
    "\n" +
    "                                                        <th>Harga</th>\r" +
    "\n" +
    "                                                        <th>Disc</th>\r" +
    "\n" +
    "                                                        <th>Sub Total</th>\r" +
    "\n" +
    "                                                    </tr>\r" +
    "\n" +
    "                                                </thead>\r" +
    "\n" +
    "                                                <tbody>\r" +
    "\n" +
    "                                                    <tr>\r" +
    "\n" +
    "                                                        <td>Estimasi Service / Jasa</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{totalWork | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{totalWork - totalWorkDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{totalWorkDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                                    </tr>\r" +
    "\n" +
    "                                                    <tr>\r" +
    "\n" +
    "                                                        <td>Estimasi Material (Parts / Bahan)</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{totalMaterial | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{totalMaterialDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                                    </tr>\r" +
    "\n" +
    "                                                    <tr>\r" +
    "\n" +
    "                                                        <td>Estimasi OPL</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{sumWorkOpl - sumWorkOplDiscounted | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{sumWorkOplDiscounted | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                    </tr>\r" +
    "\n" +
    "                                                    <tr>\r" +
    "\n" +
    "                                                        <td>PPN</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                                        </td>\r" +
    "\n" +
    "                                                        <td></td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                                            {{(((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                    </tr>\r" +
    "\n" +
    "                                                    <tr>\r" +
    "\n" +
    "                                                        <td>Total Estimasi</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                                        </td>\r" +
    "\n" +
    "                                                        <td></td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{(totalMaterialDiscounted + totalWorkDiscounted) + (((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                    </tr>\r" +
    "\n" +
    "                                                    <tr>\r" +
    "\n" +
    "                                                        <td>Total DP</td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\"></td>\r" +
    "\n" +
    "                                                        <td></td>\r" +
    "\n" +
    "                                                        <td style=\"text-align: right;\">{{totalDp | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                                    </tr>\r" +
    "\n" +
    "                                                </tbody>\r" +
    "\n" +
    "                                            </table>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div> --> <!--         <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "                                    <uib-accordion-heading>\r" +
    "\n" +
    "                                        OPL\r" +
    "\n" +
    "                                    </uib-accordion-heading>\r" +
    "\n" +
    "                                    <div class=\"row\">\r" +
    "\n" +
    "                                        <div class=\"col-md-12\">\r" +
    "\n" +
    "                                            <bslist data=\"oplData\" m-id=\"oplId\" list-model=\"lmModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApi\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\"\r" +
    "\n" +
    "                                                list-title=\"Order Pekerjaan Luar\" list-height=\"200\">\r" +
    "\n" +
    "                                                <bslist-template>\r" +
    "\n" +
    "                                                    <div class=\"row\">\r" +
    "\n" +
    "                                                        <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                                {{lmItem.OplName}}\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                                {{lmItem.VendorName}}\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                        <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <label>Dibutuhkan : </label> {{lmItem.RequiredDate | date:'dd-MM-yyyy'}}\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <label>Harga : </label> {{lmItem.Price | currency:\"Rp.\"}}\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                        <div class=\"col-md-4\">\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-template>\r" +
    "\n" +
    "                                                <bslist-modal-form>\r" +
    "\n" +
    "                                                    <div class=\"row\">\r" +
    "\n" +
    "                                                        <div class=\"col-md-12\">\r" +
    "\n" +
    "                                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <bsreqlabel>Nama OPL</bsreqlabel>\r" +
    "\n" +
    "                                                                <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModel.OplName\">\r" +
    "\n" +
    "                                                                <bserrmsg field=\"oplName\"></bserrmsg>\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <bsreqlabel>Vendor</bsreqlabel>\r" +
    "\n" +
    "                                                                <input type=\"text\" class=\"form-control\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModel.VendorName\">\r" +
    "\n" +
    "                                                                <bserrmsg field=\"oplName\"></bserrmsg>\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                                <bsreqlabel>Pembayaran</bsreqlabel>\r" +
    "\n" +
    "                                                                <bsselect name=\"pembayaran\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected)\">\r" +
    "\n" +
    "                                                                </bsselect>\r" +
    "\n" +
    "                                                                <bserrmsg field=\"pembayaran\"></bserrmsg>\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <bsreqlabel>Dibutuhkan Tanggal</bsreqlabel>\r" +
    "\n" +
    "                                                                <bsdatepicker name=\"RequiredDate\" date-options=\"DateOptions\" ng-model=\"lmModel.RequiredDate\" ng-change=\"DateChange(dt)\">\r" +
    "\n" +
    "                                                                </bsdatepicker>\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                                <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "                                                                <input type=\"text\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.ReceiveBy\">\r" +
    "\n" +
    "                                                                <bserrmsg field=\"price\"></bserrmsg>\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                                <bsreqlabel>Harga</bsreqlabel>\r" +
    "\n" +
    "                                                                <input type=\"number\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Price\">\r" +
    "\n" +
    "                                                                <bserrmsg field=\"price\"></bserrmsg>\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <label>Notes</label>\r" +
    "\n" +
    "                                                                <textarea class=\"form-control\" name=\"Address\" ng-model=\"lmModel.Address\" style=\"height: 100px;\"></textarea>\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                            <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                                <label>Discount</label>\r" +
    "\n" +
    "                                                                <input type=\"number\" class=\"form-control\" name=\"discount\" ng-model=\"lmModel.Discount\">\r" +
    "\n" +
    "                                                                <bserrmsg field=\"discount\"></bserrmsg>\r" +
    "\n" +
    "                                                            </div>\r" +
    "\n" +
    "                                                        </div>\r" +
    "\n" +
    "                                                    </div>\r" +
    "\n" +
    "                                                </bslist-modal-form>\r" +
    "\n" +
    "                                            </bslist>\r" +
    "\n" +
    "                                            <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "                                                <tbody>\r" +
    "\n" +
    "                                                    <tr>\r" +
    "\n" +
    "                                                        <td width=\"20%\">Total</td>\r" +
    "\n" +
    "                                                        <td>{{sumWork | currency:\"Rp.\"}}</td>\r" +
    "\n" +
    "                                                    </tr>\r" +
    "\n" +
    "                                                </tbody>\r" +
    "\n" +
    "                                            </table>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div> --> <!--  --> <div uib-accordion-group class=\"panel-default\" id=\"Summary\"> <uib-accordion-heading> Summary </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label>No. Polisi</label> </div> <div class=\"form-group\"> <label>Nomor WO</label> </div> <div class=\"form-group\"> <label>Kategori WO</label> </div> </div> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label>{{mDataCrm.Vehicle.LicensePlate}}</label> </div> <div class=\"form-group\"> <label ng-if=\"mData.WoNo != Null\">{{mData.WoNo}}</label> <label ng-if=\"mData.WoNo == Null\">-</label> </div> <div class=\"form-group\"> <bsselect ng-disabled=\"true\" data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\"> </bsselect> </div> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"TaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-after-cancel=\"onAfterCancel\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridSumPartsDetail\" list-api=\"listApi\" confirm-save=\"false\" list-title=\"Job List\" modal-title=\"Job Data\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.Part.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.paidName}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp.\"}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\">{{totalWork | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalWork - totalWorkDiscounted | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalWorkDiscounted | currency:\"Rp.\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\">{{totalMaterial | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalMaterialDiscounted | currency:\"Rp.\":0}}</td> </tr> <tr> <td>Estimasi OPL</td> <td style=\"text-align: right\">{{sumWorkOpl | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{sumWorkOplDiscounted | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{sumWorkOpl - sumWorkOplDiscounted | currency:\"Rp. \":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"> <!-- {{((totalWork + totalMaterial) * 10)/100 | currency:\"Rp. \":0}} --> </td> <td></td> <td style=\"text-align: right\"> {{(((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"> <!-- {{totalWork + totalMaterial | currency:\"Rp. \":0}} --> </td> <td></td> <!-- <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                                            {{(totalMaterial - totalMaterialDiscounted)+(totalWork - totalWorkDiscounted) | currency:\"Rp.\":0}}\r" +
    "\n" +
    "                                                        </td> --> <td style=\"text-align: right\">{{(totalMaterialDiscounted + totalWorkDiscounted) + (((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\"></td> <td></td> <td style=\"text-align: right\">{{totalDp | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Lama Pekerjaan : </label> <input type=\"text\" ng-model=\"mData.ActualRate\" class=\"form-control\" placeholder=\"0\" name=\"\" ng-disabled=\"true\" skip-enable> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Part Bekas</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radio1\" ng-model=\"mData.UsedPart\" name=\"radio1\" value=\"1\" checked> <label for=\"radio1\">Kembalikan</label> <input type=\"radio\" id=\"radio2\" ng-model=\"mData.UsedPart\" name=\"radio1\" value=\"0\"> <label for=\"radio2\">Tinggal</label> <!-- {{mData.UsedPart}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Waiting / Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"wait1\" ng-model=\"mData.IsWaiting\" name=\"radio2\" value=\"1\" checked> <label for=\"wait1\">Waiting</label> <input type=\"radio\" id=\"wait2\" ng-model=\"mData.IsWaiting\" name=\"radio2\" value=\"0\"> <label for=\"wait2\">Drop Off</label> <!-- {{mData.IsWaiting}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Car Wash</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"washing1\" ng-model=\"mData.IsWash\" name=\"radio3\" value=\"1\" checked> <label for=\"washing1\">Wash</label> <input type=\"radio\" id=\"washing2\" ng-model=\"mData.IsWash\" name=\"radio3\" value=\"0\"> <label for=\"washing2\">No Wash</label> <!-- {{mData.IsWash}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Penggantian Parts</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changePart1\" ng-model=\"mData.PermissionPartChange\" name=\"radio4\" value=\"1\" checked> <label for=\"changePart1\">Langsung</label> <input type=\"radio\" id=\"changePart2\" ng-model=\"mData.PermissionPartChange\" name=\"radio4\" value=\"0\"> <label for=\"changePart2\">Izin</label> <!-- {{mData.PermissionPartChange}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Metode Pembayaran</label> </div> </div> <div class=\"col-md-6\"> <bsselect name=\"paymentType\" ng-model=\"mData.PaymentType\" data=\"paymentTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Type Pembayaran\"> </bsselect> <!-- <bserrmsg field=\"paymentType\"></bserrmsg> --> </div> </div> <div class=\"row\" style=\"margin-top:25px; margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Scheduled Service Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.firstDate\" ng-disabled=\"true\"> </bsdatepicker> </div> <div class=\"form-group text-center\" style=\"width:20px\"> <b>-</b> </div> <div class=\"form-group\"> <bsdatepicker name=\"lastDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.lastDate\" ng-disabled=\"true\"> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Estimation Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"estDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.EstimateDate\" ng-disabled=\"true\" skip-enable> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Adjusment Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"adjDate\" min-date=\"minDate\" date-options=\"DateOptions\" ng-model=\"mData.AdjusmentDate\" ng-disabled=\"adjustDate\" ng-change=\"adjustTime(mData.AdjusmentDate)\" skip-enable> </bsdatepicker> </div> <div class=\"form-group\" style=\"width:200px\"> <div uib-timepicker ng-model=\"mData.AdjusmentDate\" ng-change=\"changed()\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" ng-disabled=\"adjustDate\"></div> </div> </div> <div class=\"col-md-3 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bscheckbox ng-model=\"mData.customerRequest\" label=\"Customer Request\" ng-click=\"adjRequest()\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <!-- <div class=\"col-md-2 form-inline\">\r" +
    "\n" +
    "                                           \r" +
    "\n" +
    "                                        </div> --> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Kode Transaksi</label> </div> <div class=\"col-md-9 form-inline\"> <bsselect ng-disabled=\"false\" data=\"codeTransaction\" name=\"codeTrans\" item-value=\"Id\" item-text=\"Description\" ng-model=\"mData.CodeTransaction\" placeholder=\"Kode Transaksi\"> </bsselect> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" name=\"saveWO\" class=\"rbtn btn\" ng-click=\"saveNewWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                                                    Simpan\r" +
    "\n" +
    "                                                </button>\r" +
    "\n" +
    "                                                <button type=\"button\" name=\"releaseWO\" class=\"rbtn btn\" ng-click=\"releaseWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                                                    Release WO\r" +
    "\n" +
    "                                                </button> --> <!-- ng-show=\"mData.JobId >= 0\" --> <!-- <button type=\"button\" name=\"CloseWO\" ng-click=\"cancelWo(mData)\" class=\"rbtn btn\">\r" +
    "\n" +
    "                                                    Batal WO\r" +
    "\n" +
    "                                                </button> --> </div> </div> </div> <!--             <div class=\"row\">\r" +
    "\n" +
    "                                        <div class=\"btn-group pull-right\">\r" +
    "\n" +
    "                                            <button type=\"button\" name=\"saveWO\" class=\"rbtn btn\" ng-click=\"saveNewWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                                                Simpan\r" +
    "\n" +
    "                                            </button>\r" +
    "\n" +
    "                                            <button type=\"button\" name=\"printWO\" class=\"rbtn btn\">\r" +
    "\n" +
    "                                                Print Preview\r" +
    "\n" +
    "                                            </button>\r" +
    "\n" +
    "                                            <button type=\"button\" name=\"releaseWO\" class=\"rbtn btn\" ng-click=\"releaseWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                                                Release WO\r" +
    "\n" +
    "                                            </button>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div> --> </div> </uib-accordion> </div> </div> <!-- WO LIST END HERE --> </div> </div> <div ng-class=\"ncolumn\" style=\"padding-left: 0\" ng-show=\"ncolvisible\"> <div class=\"panel panel-default\" id=\"jpcb_gr_edit_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> {{displayedChip.ModelType}} {{displayedChip.PoliceNumber}} (#{{displayedChip.JobId}}.{{displayedChip.chipType}}) </h3> <h3 class=\"panel-title pull-right\"> <a href=\"#\"><i class=\"fa fa-fw fa-remove\" ng-click=\"showChipInfo(false)\">&nbsp;</i></a> </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\" style=\"padding: 8px\"> <div class=\"col-md-12\" style=\"padding-left: 0; padding-right: 0\"> <div ng-include=\"'app/boards/jpcbbp/editChipBP.html'\"></div> </div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"ShowWO()\">WO</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"showChipInfo(false)\">Close</button> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"dispatchChip(displayedChip, 1)\" ng-disabled=\"!displayedChip.editable\">Dispatch</button> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"dispatchChip(displayedChip, 0)\" ng-hide=\"statusupda\" ng-disabled=\"!displayedChip.editable\">Update</button> <!-- <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"SplitChip(displayedChip)\" ng-show=\"showSplit\">Split</button>  --> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"SplitChip(displayedChip)\">Split</button> </div> </div> </div> </div> </div> <br> </div>"
  );


  $templateCache.put('app/boards/jpcbbpview/jpcbbpview.html',
    "<style type=\"text/css\">/*.table-fixed thead {\r" +
    "\n" +
    "      width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .table-fixed tbody {\r" +
    "\n" +
    "      height: 230px;\r" +
    "\n" +
    "      overflow-y: auto;\r" +
    "\n" +
    "      width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {\r" +
    "\n" +
    "      display: block;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .table-fixed tbody td, .table-fixed thead > tr> th {\r" +
    "\n" +
    "      float: left;\r" +
    "\n" +
    "      border-bottom-width: 0;\r" +
    "\n" +
    "    }*/</style> <div style=\"margin-left: 10px; margin-right: 10px\"> <div class=\"row\"> <div class=\"col-xs-10\"> <div class=\"panel panel-default\" id=\"jpcb_gr_view_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Job progress </h3> <h3 class=\"panel-title pull-right\" ng-show=\"jpcbgrview.isCurrentDate\"> &nbsp;&nbsp;{{ clock }} </h3> <div style=\"width: 200px; margin-top: -4px\" class=\"pull-right\"> <!-- <bsdatepicker\r" +
    "\n" +
    "                            name=\"currentDate\"\r" +
    "\n" +
    "                            date-options=\"dateOptions\"\r" +
    "\n" +
    "                            ng-model=\"jpcbgrview.currentDate\",\r" +
    "\n" +
    "                            ng-change=\"jpcbgrview.reloadBoard()\"\r" +
    "\n" +
    "                        ></bsdatepicker> --> </div> <h3 class=\"panel-title text-center\"> <span>Working Technician: {{countTechnician}}</span>&nbsp; <span>In Progress: {{countInProgress}}</span>&nbsp; <span>No Show: {{countNoShow}}</span> &nbsp; <span>Carry Over: {{countCarryOver}}</span>&nbsp; <span>Booking: {{countBookingA}}/{{countBookingB}}</span> &nbsp; </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-heading\"> <div class=\"row\"> <div class=\"btn-group col-sm-7\" data-toggle=\"buttons\"> <label class=\"btn btn-default\" ng-class=\"{'active':bpCategorySelected.id == catitem.id}\" for=\"optionCat{{$index}}\" ng-repeat=\"catitem in BpCategory\" ng-click=\"jpcbgrview.updateCategory(catitem)\"> <input type=\"radio\" name=\"options\" id=\"optionCat{{$index}}\" ng-model=\"bpCategorySelected\">{{catitem.name}} </label> </div> <div class=\"col-xs-5\"> <div style=\"position:relative;height:40px; overflow-x:auto;overflow-y:hidden;float:right\"> <div class=\"btn-group\" data-toggle=\"buttons\"> <label class=\"btn btn-default\" ng-class=\"{'active':teamSelected.Id == team.Id}\" for=\"option{{$index}}\" ng-repeat=\"team in BpGroupList\" ng-click=\"updateTeam(team)\"> <input type=\"radio\" name=\"options\" id=\"option{{$index}}\" ng-value=\"{{team.GroupBPId}}\" ng-model=\"teamSelected\">{{team.name}} </label> </div> </div> </div> </div> </div> <div class=\"panel-body\" style=\"\"> <div class=\"col-xs-1\" style=\"height:637px\" id=\"labelForGroup\"> <h1 ng-repeat=\"item in BpGroupList\" ng-if=\"item.visible == 1\" style=\"transform:rotate(-90deg);position: absolute;top: 45%;width:200px;right:0;padding-top: 100px;text-align: center\">{{item.name}}</h1> </div> <div id=\"jpcbbp-prod-container2\" class=\"col-xs-11\" style=\"border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0\"> <div class=\"panel panel-default\" id=\"jpcb_gr_edit_panel\"> <div class=\"panel-heading\"> <h3 ng-repeat=\"item in BpGroupList\" ng-if=\"item.visible == 1\" class=\"panel-title pull-left\"> Nama Grup : {{item.name}} </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\" style=\"padding: 8px\"> <div class=\"row\"> <div class=\"col-md-12\"> <table class=\"table table-fixed table-bordered table-striped\" border=\"1\"> <thead> <tr> <th width=\"20%\">No</th> <th width=\"40%\">No. Polisi</th> <th width=\"40%\">Target Proses</th> </tr> </thead> <tbody> <tr ng-repeat=\"item in tmpDataTarget\"> <td>{{$index + 1}}</td> <td>{{item.name}}</td> <td>{{item.process}}</td> </tr> </tbody> <!-- <tbody>\r" +
    "\n" +
    "                                    <tr>\r" +
    "\n" +
    "                                        <td>1</td>\r" +
    "\n" +
    "                                        <td>F1234AN</td>\r" +
    "\n" +
    "                                        <td>Putty</td>\r" +
    "\n" +
    "                                    </tr>\r" +
    "\n" +
    "                                    <tr>\r" +
    "\n" +
    "                                        <td>2</td>\r" +
    "\n" +
    "                                        <td>F1235AN</td>\r" +
    "\n" +
    "                                        <td>Polishing</td>\r" +
    "\n" +
    "                                    </tr>\r" +
    "\n" +
    "                                    <tr>\r" +
    "\n" +
    "                                        <td>3</td>\r" +
    "\n" +
    "                                        <td>F1236AN</td>\r" +
    "\n" +
    "                                        <td>Surfacer</td>\r" +
    "\n" +
    "                                    </tr>\r" +
    "\n" +
    "                                </tbody> --> </table> </div> </div> </div> </div> </div> </div> </div> <br>"
  );


  $templateCache.put('app/boards/jpcbbpview/options.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337; margin: -10px\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">Select Stall</h3> </div> <div class=\"panel-body\"> {{jpcbgrview}} <form method=\"post\"> <div class=\"row\"> <div class=\"col-md-12\"> <div ng-repeat=\"item in jpcbgrview.tempStallVisible\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.Name}}</label> </div> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div> <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog(0)\" style=\"color:white; background-color:#D53337\">OK</button> <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0)\">Cancel</button> </div> </div> </div> </div> </form> </div> </div>"
  );


  $templateCache.put('app/boards/jpcbgr/editChip.html',
    "<div class=\"panel-body\"> <form method=\"post\"> <div class=\"row\"> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Nomor WO</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.WoNo\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">No Polisi</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.PoliceNumber\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Tanggal WO</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.FmtWoCreatedDate\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Plan Time</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.PlanDateStart\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Allocation Time</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.PlanDateStart\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Delivery Time</label> </div> </div> <div class=\"col-md-7\"> <!-- <input ng-model=\"displayedChip.PlanDateFinish\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"></input> --> <input ng-model=\"displayedChip.DelivTime\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Time Extension</label> </div> </div> <div class=\"col-md-5\"> <input ng-model=\"displayedChip.AdditionalTimeAmount\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"!displayedChip.editable\"> </div> <div class=\"col-md-2\" style=\"margin-top:7px\"> Menit </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">SA</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.UserSa.EmployeeName\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Stall</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"displayedChip.StallName\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"true\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Foreman</label> </div> </div> <div class=\"col-md-7\"> <select ng-change=\"selectedForeman(displayedChip.MProfileForemanId)\" ng-model=\"displayedChip.MProfileForemanId\" class=\"form-control edit-chip-item-control\" ng-options=\"item.id as item.name for item in ListForeman\" ng-disabled=\"!displayedChip.editable\"> <option value=\"\">-- Pilih Foreman --</option> </select> </div> </div> <!-- div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"col-md-5\">\r" +
    "\n" +
    "                            <div class=\"form-group form-group-edit-chip\">\r" +
    "\n" +
    "                                <label class=\"control-label edit-chip-item-label\">Editable</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"col-md-7\">\r" +
    "\n" +
    "                            <input ng-model=\"displayedChip.editable\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"!displayedChip.editable\"></input>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"col-md-5\">\r" +
    "\n" +
    "                            <div class=\"form-group form-group-edit-chip\">\r" +
    "\n" +
    "                                <label class=\"control-label edit-chip-item-label\">Status</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"col-md-7\">\r" +
    "\n" +
    "                            <input ng-model=\"displayedChip.Status\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"!displayedChip.editable\"></input>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div --> <!--div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"col-md-5\">\r" +
    "\n" +
    "                            <div class=\"form-group form-group-edit-chip\">\r" +
    "\n" +
    "                                <label class=\"control-label edit-chip-item-label\">Tipe Pengerjaan</label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"col-md-7\">\r" +
    "\n" +
    "                            <input ng-model=\"displayedChip.isWOBase\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"!displayedChip.editable\"></input>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div --> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Tipe Pengerjaan</label> </div> </div> <div class=\"col-md-7\"> <select ng-model=\"displayedChip.isWOBase\" name=\"type\" class=\"form-control edit-chip-item-control\" ng-disabled=\"!displayedChip.editable\"> <option value=\"1\">WO Based</option> <option value=\"0\">Job Based</option> </select> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Waktu Overtime</label> </div> </div> <div class=\"col-md-5\"> <input ng-model=\"displayedChip.Overtime\" type=\"text\" class=\"form-control edit-chip-item-control\" ng-disabled=\"!displayedChip.editable\"> </div> <div class=\"col-md-2\" style=\"margin-top:7px\"> Menit </div> </div> <div class=\"row\" ng-show=\"displayedChip.isSplitable\"> <div class=\"col-md-12\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\">Untuk dapat memindahkan chip ini ke stall lain, harus dilakukan <b>Split Chip</b> terlebih dahulu.</label> </div> </div> </div> <div class=\"row\" ng-show=\"displayedChip.isSplitable\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-edit-chip\"> <label class=\"control-label edit-chip-item-label\"></label> </div> </div> <div class=\"col-md-7\"> <button type=\"button\" class=\"btn btn-danger\" ng-click=\"execSplitChip(displayedChip.JobId)\">Split Chip</button> </div> </div> </div> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\" xstyle=\"color:white; background-color:#D53337;\"> <h3 class=\"panel-title\">Pekerjaan</h3> </div> <table class=\"panel-body\"> <tr> <th class=\"edit-chip-grid-item-title\" style=\"width:40%\">Pekerjaan</th> <th class=\"edit-chip-grid-item-title\" style=\"width:30%\">Ketersediaan</th> <th class=\"edit-chip-grid-item-title\" style=\"width:15%\">SAR</th> <th class=\"edit-chip-grid-item-title\" style=\"width:15%\">FR</th> </tr> <tr ng-repeat-start=\"task in displayedChip.JobTask\"> <td class=\"edit-chip-grid-item-content\" style=\"width:40%\">{{task.TaskName}}</td> <td class=\"edit-chip-grid-item-content\" style=\"width:30%\">-</td> <td class=\"edit-chip-grid-item-content\" style=\"width:15%\"> <input ng-model=\"task.ActualRate\" type=\"text\" class=\"edit-chip-item-control\" style=\"width:100%\" ng-disabled=\"!displayedChip.editable\" ng-change=\"updateTechRate(task)\"> </td> <td class=\"edit-chip-grid-item-content\" style=\"width:15%\">{{task.FlatRate}}</td> </tr> <tr ng-repeat-end> <td class=\"edit-chip-grid-item-content\" colspan=\"4\"> <table style=\"width: 100%\"> <tr> <th class=\"edit-chip-grid-item-title\" style=\"width:40%\">Technician</th> <th class=\"edit-chip-grid-item-title\" style=\"width:35%\">Technician Flat Rate</th> <th class=\"edit-chip-grid-item-title\" style=\"width:35%\">JT Adjustment</th> <th style=\"width:15%\"> <a href=\"#\" style=\"margin-left:10px\" ng-show=\"displayedChip.editable\"><i class=\"fa fa-fw fa-plus-circle\" style=\"font-size: 15px\" ng-click=\"newItemRow(task)\">&nbsp;</i></a> </th> </tr> <tr ng-repeat=\"technician in task.JobTaskTechnician\"> <td class=\"edit-chip-grid-item-content\" style=\"width:50%\"> <select ng-model=\"technician.MProfileTechnicianId\" ng-change=\"selectTechnician(technician.MProfileTechnicianId,task.JobTaskId)\" class=\"form-control edit-chip-item-control\" ng-options=\"item.id as item.name for item in ListTechnician\"> <option value=\"\">-- Pilih Teknisi --</option> </select> </td> <td class=\"edit-chip-grid-item-content\" style=\"width:35%\"> <input ng-model=\"technician.FlatRate\" type=\"text\" class=\"edit-chip-item-control\" style=\"width:100%\" ng-disabled=\"!displayedChip.editable\"> </td> <td class=\"edit-chip-grid-item-content\" style=\"width:35%\"> <input ng-model=\"technician.JTAdjustment\" type=\"text\" class=\"edit-chip-item-control\" style=\"width:100%\" ng-disabled=\"!displayedChip.editable\"> </td> <td style=\"width:15%\"> <a href=\"#\" style=\"margin-left:10px\" ng-show=\"displayedChip.editable\"><i class=\"fa fa-fw fa-trash-o\" style=\"font-size: 15px\" ng-click=\"deleteItem(task, technician)\">&nbsp;</i></a> </td> </tr> </table> </td> </tr> </table> </div> </div> </form> </div>"
  );


  $templateCache.put('app/boards/jpcbgr/jpcbGr.html',
    "<div style=\"margin-left: 10px; margin-right: 10px\"> <div class=\"row\"> <div ng-class=\"wcolumn\"> <div class=\"panel panel-default\" id=\"jpcb_gr_edit_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> JPCB GR </h3> <div class=\"col-md-7\"> <i ng-click=\"refreshData()\" style=\"width: 20px;height: 20px;font-size: 20px;margin-top: 0px\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" class=\"fa fa-refresh pull-right refreshIcon\"></i> </div> <h3 class=\"panel-title pull-right\"> <img src=\"images/loading.gif\" style=\"width: 20px\" ng-show=\"isLoading\"> {{ clock }} </h3> <div style=\"width: 200px; margin-top: -4px\" class=\"pull-right\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"jpcbgrview.currentDate\" ng-change=\"jpcbgrview.reloadBoard()\"></bsdatepicker> </div> <button type=\"button\" class=\"btn btn-default panel-title pull-right\" ng-click=\"jpcbgrview.searchNomorPolisi()(true)\" style=\"margin-top: -4px; margin-right: 4px; margin-left: 0px; padding-bottom: 4px; height: 32px\"> <i class=\"fa fa-fw fa-search\"></i> </button> <div class=\"input-icon-right panel-title pull-right\" style=\"margin-top: -4px; margin-left: 4px; width: 160px; height: 32px\"> <i class=\"icon-search\" style=\"margin-top: -3px; font-size:12px\">{{jpcbgrview.searchFound}}</i> <input type=\"search\" class=\"form-control\" ng-change=\"jpcbgrview.searchInputEdit()\" style=\"font-size: 13px\" ng-model=\"jpcbgrview.scNomorPolisi\" placeholder=\"Nomor Polisi\"> </div> <h3 class=\"panel-title text-center\" style=\"font-size: 18px; font-weight:bolder;margin:10px 0px 10px 0px\"><br><br> <!-- <i class=\"fa fa-group\" style=\"font-size:18px\"></i>&nbsp; --> <!-- <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Working Technician : {{countTechnician}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">In Progress : {{countInProgress}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Production Capacity : {{productionCapacity}} %</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">No Show : {{countNoShow}}</span> &nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Carry Over : {{countCarryOver}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Booking : {{countBookingA}}/{{countBookingB}}</span> &nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">WIP : {{countWIP}}</span> &nbsp; --> <span>Working Technician : {{countTechnician}}</span>&nbsp; <span>In Progress : {{countInProgress}}</span>&nbsp; <span>Production Capacity : {{countProductionCapacity}} %</span>&nbsp; <span>No Show : {{countNoShow}}</span> &nbsp; <span>Carry Over : {{countCarryOver}}</span>&nbsp; <span>Booking : {{countBookingA}}/{{countBookingB}}</span> &nbsp; <span>WIP : {{countWIP}}</span> &nbsp; </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"jpcb_gr_edit\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer col-md-12\"> <div class=\"btn-group col-md-12\"> <div class=\"input-icon-right panel-title pull-left\" style=\"margin-top: -4px; margin-left: 4px; width: 160px; height: 32px\"> <i class=\"icon-search\" style=\"margin-top: -3px; font-size:12px\"></i> <input type=\"search\" class=\"form-control\" ng-change=\"SearchStoppageInput()\" style=\"font-size: 13px\" ng-model=\"StoppageNomorPolisi\" placeholder=\"Nomor Polisi\"> </div> <button type=\"button\" class=\"btn btn-default panel-title pull-left\" ng-click=\"SearchStoppage()\" style=\"margin-top: -4px; margin-right: 4px; margin-left: 0px; padding-bottom: 4px; height: 32px\"> <i class=\"fa fa-fw fa-search\"></i> </button> </div> <div class=\"btn-group col-md-12\"> <button type=\"button\" class=\"btn btn-default btn-lg col-md-3\" ng-click=\"additionalJob()\">Additional Job</button> <button type=\"button\" class=\"btn btn-default btn-lg col-md-3\" ng-click=\"pausedJob()\">Pause</button> <button type=\"button\" class=\"btn btn-default btn-lg col-md-3\" ng-click=\"waitingFI()\">Waiting for FI</button> <button type=\"button\" class=\"btn btn-danger btn-lg col-md-3\" ng-click=\"delayedJob()\">Delay</button> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"testChip()\" ng-show=\"false\">Testing 1</button> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"testDialog()\" ng-show=\"false\">Testing 2</button> </div> </div> </div> </div> <div id=\"sideInfo\" ng-class=\"ncolumn\" style=\"padding-left: 0\" ng-show=\"ncolvisible\"> <div class=\"panel panel-default\" id=\"jpcb_gr_edit_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> {{displayedChip.ModelType}} {{displayedChip.PoliceNumber}} </h3> <h3 class=\"panel-title pull-right\"> <a href=\"#\"><i class=\"fa fa-fw fa-remove\" ng-click=\"showChipInfo(false)\">&nbsp;</i></a> </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\" style=\"padding: 8px; overflow-y:auto;overflow-x:hidden\"> <div class=\"col-md-12\" style=\"padding-left: 0; padding-right: 0\"> <div ng-include=\"'app/boards/jpcbgr/editChip.html'\"></div> </div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"showChipWO(displayedChip)\">WO</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"showChipInfo(false)\">Close</button> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"dispatchChip(displayedChip, 1)\" ng-hide=\"statusdispa\" ng-disabled=\"!displayedChip.editableDispatch\">Dispatch</button> <button type=\"button\" class=\"btn btn-danger btn-lg\" ng-click=\"dispatchChip(displayedChip, 0)\" ng-hide=\"statusupda\" ng-disabled=\"displayedChip.editableDispatch\">Update</button> <!-- <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"Split(displayedChip)\">Split</button> --> </div> </div> </div> </div> {{\"\"}} </div> </div> <div style=\"display: none\"> Job id: {{displayedChip.JobId}} Status: {{displayedChip.Status}} </div> <bsui-modal show=\"show_modal.show\" title=\"Wo Detail\" data=\"modal_model\" on-save=\"doAddMaterial\" on-cancel=\"rescheduleCancel\" mode=\"modalMode\"> <div class=\"col-md-12\" ng-repeat=\"item in gridWork\" style=\"margin-top: 10px;padding-right: 0px;padding-left: 0px\" ng-disabled=\"true\"> <uib-accordion close-others=\"oneAtATime\" ng-disabled=\"true\"> <div uib-accordion-group is-open=\"true\" class=\"panel-default\"> <uib-accordion-heading> <label>Nama Pekerjaan : {{item.TaskName}}</label> <br> <label>Tipe : {{item.JobType.Name}} </label> <!-- <label>Tipe : {{item.JobTypeId}} </label> --> <br> <label>Flat Rate : {{item.FlatRate}}</label> <br> <label>Act Rate : {{item.ActualRate}}</label> <!-- <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> --> </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <table class=\"table table-striped table-bordered\"> <tr> <td>Nomor Material</td> <td>Nama Material</td> <td>Status</td> <td>ETA</td> <td>Permintaan Qty</td> <td>Issue Qty</td> <td>Satuan</td> </tr> <tr ng-repeat=\"itemParts in item.child\"> <td>{{itemParts.PartsCode}}</td> <td>{{itemParts.PartsName}}</td> <!-- <td>{{itemParts.Status}}</td> --> <!-- <td ng-if=\"itemParts.Qty == itemParts.QtyGI\">Tersedia</td>\r" +
    "\n" +
    "                                    <td ng-if=\"itemParts.Qty != itemParts.QtyGI\">Tidak Tersedia</td> --> <td>{{itemParts.Availbility}}</td> <td>{{itemParts.ETA | date:\"dd-MM-yyyy\"}}</td> <td>{{itemParts.Qty}}</td> <td>{{itemParts.QtyGI}}</td> <td>{{itemParts.satuanName}}</td> </tr> </table> <!-- <div ui-grid=\"gridWODetail\" ui-grid-cellNav ui-grid-auto-resize class=\"grid\" ng-cloak style=\"height: 200px;\"></div> --> </div> </div> </div> </uib-accordion> </div> </bsui-modal> <script type=\"text/javascript\">$(window).on('scroll resize', function() {\r" +
    "\n" +
    "        console.log(\"scroll => \",$(this).scrollTop());        \r" +
    "\n" +
    "        if($(this).scrollTop() > 85 && $('body').hasClass('hidden-menu') == false){\r" +
    "\n" +
    "            $('#sideInfo').css('position','fixed');\r" +
    "\n" +
    "            $('#sideInfo').css('right','0');\r" +
    "\n" +
    "            $('#sideInfo').css('top','0');\r" +
    "\n" +
    "            $('#sideInfo').css('width','28%');\r" +
    "\n" +
    "            console.log(\"wide nya =>\",\"28%\")            \r" +
    "\n" +
    "        }else if($(this).scrollTop() > 85 && $('body').hasClass('hidden-menu') == true){ \r" +
    "\n" +
    "            $('#sideInfo').css('position','fixed');\r" +
    "\n" +
    "            $('#sideInfo').css('right','10px');\r" +
    "\n" +
    "            $('#sideInfo').css('top','0');\r" +
    "\n" +
    "            $('#sideInfo').css('width','33%');\r" +
    "\n" +
    "            console.log(\"wide nya =>\",\"33%\")            \r" +
    "\n" +
    "        }else{\r" +
    "\n" +
    "            $('#sideInfo').css('position','relative');\r" +
    "\n" +
    "            $('#sideInfo').css('right','0');\r" +
    "\n" +
    "            $('#sideInfo').css('top','0');\r" +
    "\n" +
    "            $('#sideInfo').css('width','');\r" +
    "\n" +
    "            console.log(\"wide => \", \"kosong\");\r" +
    "\n" +
    "        }        \r" +
    "\n" +
    "        // toggleAffix(ele, $(this), wrapper);\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "    var tmpTinggi = $(window).height();\r" +
    "\n" +
    "    var tmpHeader = $('#sideInfo .panel .panel-heading').height();\r" +
    "\n" +
    "    var tmpFooter = $('#sideInfo .panel .panel-footer').height();\r" +
    "\n" +
    "    var tmpPanelbody = tmpTinggi - (tmpHeader + tmpFooter);\r" +
    "\n" +
    "    $('#sideInfo .panel .panel-body').css('height',(tmpPanelbody - tmpFooter)+'px');</script> <br>"
  );


  $templateCache.put('app/boards/jpcbgr/viewChip.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">{{currentChip.type}} {{currentChip.nopol}}</h3> </div> <div class=\"panel-body\"> <form method=\"post\"> <div class=\"row\"> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">Nomor WO</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.wo_number\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">No Polisi</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.nopol\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">Tanggal WO</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.wo_time\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">Start Allocation</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.alloc_time\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">Delivery Time</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.normalDuration\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">Time Extension</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.extDuration\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">SA</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.sa_name\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">Stall</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.stall\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">Foreman</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.foreman\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group form-group-board\"> <label class=\"control-label\">Tipe Pengerjaan</label> </div> </div> <div class=\"col-md-7\"> <input ng-model=\"currentChip.pekerjaan\" type=\"text\" class=\"form-control\" ng-disabled=\"!currentChip.editable\"> </div> </div> </div> <div class=\"row\" style=\"margin-top: 10px\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\" xstyle=\"color:white; background-color:#D53337;\"> <h3 class=\"panel-title\">Pekerjaan</h3> </div> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div> <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog(0)\" style=\"color:white; background-color:#D53337\">Simpan</button> <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0)\">Close</button> <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0)\">WO</button> </div> </div> </div> </div> </form> </div> </div>"
  );


  $templateCache.put('app/boards/jpcbgrview/jpcbgrview.html',
    "<div style=\"margin-left: 10px; margin-right: 10px\"> <div class=\"row\"> <button type=\"button\" class=\"btn btn-default panel-title pull-right\" ng-click=\"showOptions(true)\" style=\"margin-top: -4px; margin-left: 4px; padding-bottom: 4px;margin-bottom:10px\"> <i class=\"fa fa-fw fa-gear\"></i> </button> </div> <div class=\"row\"> <div ng-class=\"wcolumn\"> <div class=\"panel panel-default\" id=\"jpcb_gr_view_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> JPCB GR </h3> <h3 class=\"panel-title pull-right\" ng-show=\"jpcbgrview.isCurrentDate\"> &nbsp;&nbsp;{{ clock }} </h3> <div style=\"width: 200px; margin-top: -4px\" class=\"pull-right\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"jpcbgrview.currentDate\" ng-change=\"jpcbgrview.reloadBoard()\"></bsdatepicker> </div> <h3 class=\"panel-title text-center\" style=\"font-size: 18px; font-weight:bolder;margin:10px 0px 10px 0px\"><br><br> <!-- <i class=\"fa fa-group\" style=\"font-size:18px\"></i>&nbsp; --> <!-- <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Working Technician : {{countTechnician}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">In Progress : {{countInProgress}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Production Capacity : {{productionCapacity}} %</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">No Show : {{countNoShow}}</span> &nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Carry Over : {{countCarryOver}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Booking : {{countBookingA}}/{{countBookingB}}</span> &nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">WIP : {{countWIP}}</span> &nbsp; --> <span>Working Technician : {{countTechnician}}</span>&nbsp; <span>In Progress : {{countInProgress}}</span>&nbsp; <span>Production Capacity : {{countProductionCapacity}} %</span>&nbsp; <span>No Show : {{countNoShow}}</span> &nbsp; <span>Carry Over : {{countCarryOver}}</span>&nbsp; <span>Booking : {{countBookingA}}/{{countBookingB}}</span> &nbsp; <span>WIP : {{countWIP}}</span> &nbsp; </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"jpcb_gr_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div ng-class=\"ncolumn\" style=\"padding-left: 0\" ng-show=\"ncolvisible\"> <div class=\"panel panel-default\" id=\"jpcb_gr_edit_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <!-- Select Stall --> Options </h3> <h3 class=\"panel-title pull-right\"> <a href=\"#\"><i class=\"fa fa-fw fa-remove\" ng-click=\"showOptions(false)\">&nbsp;</i></a> </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\" style=\"padding: 8px\"> <div class=\"row\"> <div class=\"col-md-12\"> <h4 style=\"border-bottom: 2px solid #a5a5a5\">Hide Header / Footer</h4> <div ng-repeat=\"item in filter\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.name}}</label> </div> <hr> <h4 style=\"border-bottom: 2px solid #a5a5a5\">Select Stall</h4> <div ng-repeat=\"item in jpcbgrview.tempStallVisible\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.title}}</label> </div> </div> </div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-danger btn-sm\" ng-click=\"saveOptions(filter)\">Update Board</button> </div> </div> </div> </div> </div> </div> <br>"
  );


  $templateCache.put('app/boards/jpcbgrview/options.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337; margin: -10px\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">Select Stall</h3> </div> <div class=\"panel-body\"> {{jpcbgrview}} <form method=\"post\"> <div class=\"row\"> <div class=\"col-md-12\"> <div ng-repeat=\"item in jpcbgrview.tempStallVisible\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.Name}}</label> </div> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div> <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog(0)\" style=\"color:white; background-color:#D53337\">OK</button> <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0)\">Cancel</button> </div> </div> </div> </div> </form> </div> </div>"
  );


  $templateCache.put('app/boards/jscb/jscb.html',
    "<div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Job Scheduling Control Board </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-heading\"> <div class=\"row\"> <div class=\"btn-group col-sm-3\" data-toggle=\"buttons\"> <label class=\"btn btn-default\" ng-repeat=\"(key, catitem) in BpCategory\" style=\"width:25%\" ng-click=\"jscb.updateCategory(catitem)\"> <input type=\"radio\" name=\"options\" id=\"option5\">{{catitem.name}} </label> </div> <div class=\"col-sm-2\"> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"jscb.startDate\" ng-change=\"jscb.reloadBoard()\"></bsdatepicker> </div> <div class=\"col-xs-7\"> <div style=\"position:relative;height:40px; overflow-x:auto;overflow-y:hidden\"> <div class=\"btn-group\" data-toggle=\"buttons\"> <label class=\"btn btn-default\" ng-repeat=\"team in BpGroupList\" ng-click=\"jscb.updateTeam(team)\"> <input type=\"radio\" name=\"options\" id=\"option5\">{{team.name}} </label> </div> </div> </div> </div> </div> <div class=\"panel-body\"> <div class=\"\" ng-show=\"jscb.displayBoard\"> <div id=\"jscb_tps_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div>"
  );


  $templateCache.put('app/boards/partsorder/detailEta.html',
    "<style>.tg  {border-collapse:collapse;border-spacing:0;}\r" +
    "\n" +
    ".tg td{font-family:Arial, sans-serif;font-size:10px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}\r" +
    "\n" +
    ".tg th{font-family:Arial, sans-serif;font-size:10px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}\r" +
    "\n" +
    ".tg .tg-yw4l{vertical-align:top}\r" +
    "\n" +
    ".header_color{background-color:#eee; color: #000}</style> <div class=\"panel panel-danger\" style=\"border-color:#D53337\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">Detail</h3> </div> <div class=\"panel-body\"> <form method=\"post\"> <div class=\"row\"> <div class=\"col-md-12\"> <table class=\"tg\"> <tr> <th class=\"tg-yw4l header_color\" rowspan=\"2\">No. Material</th> <th class=\"tg-yw4l header_color\" rowspan=\"2\">Nama Material</th> <th class=\"tg-yw4l header_color\" rowspan=\"2\">Qty PO</th> <th class=\"tg-yw4l header_color\" rowspan=\"2\">Qty GR</th> <th class=\"tg-yw4l header_color\" rowspan=\"2\">Qty Remain</th> <th class=\"tg-yw4l header_color\" colspan=\"2\">BO</th> <th class=\"tg-yw4l header_color\" colspan=\"3\">Original</th> <th class=\"tg-yw4l header_color\" colspan=\"3\">Sebelumnya</th> <th class=\"tg-yw4l header_color\" colspan=\"3\">Terakhir</th> <th class=\"tg-yw4l header_color\" rowspan=\"2\">Alasan Perubahan ETD &amp; ETA</th> <th class=\"tg-yw4l header_color\" rowspan=\"2\">Satuan</th> </tr> <tr> <td class=\"tg-yw4l header_color\">BO Date</td> <td class=\"tg-yw4l header_color\">BO Qty</td> <td class=\"tg-yw4l header_color\">ETD</td> <td class=\"tg-yw4l header_color\">ETA</td> <td class=\"tg-yw4l header_color\">Qty GR</td> <td class=\"tg-yw4l header_color\">ETD</td> <td class=\"tg-yw4l header_color\">ETA</td> <td class=\"tg-yw4l header_color\">Qty GR</td> <td class=\"tg-yw4l header_color\">ETD</td> <td class=\"tg-yw4l header_color\">ETA</td> <td class=\"tg-yw4l header_color\">Qty GR</td> </tr> <tr ng-repeat=\"item in dataDetailEta\"> <td class=\"tg-yw4l\">{{item.PartsCode}}</td> <td class=\"tg-yw4l\">{{item.PartsName}}</td> <td class=\"tg-yw4l\">{{item.QtyPO}}</td> <td class=\"tg-yw4l\">{{item.QtyGR}}</td> <td class=\"tg-yw4l\">{{item.QtyRemain}}</td> <td class=\"tg-yw4l\">{{item.BODate}}</td> <td class=\"tg-yw4l\">{{item.BOQty}}</td> <td class=\"tg-yw4l\">{{item.ETD}}</td> <td class=\"tg-yw4l\">{{item.ETA}}</td> <td class=\"tg-yw4l\">{{item.QtyGR}}</td> <td class=\"tg-yw4l\">{{item.ETDPrev}}</td> <td class=\"tg-yw4l\">{{item.ETAPrev}}</td> <td class=\"tg-yw4l\">{{item.QtyGR}}</td> <td class=\"tg-yw4l\">{{item.ETDLast}}</td> <td class=\"tg-yw4l\">{{item.ETALast}}</td> <td class=\"tg-yw4l\">{{item.QtyGR}}</td> <td class=\"tg-yw4l\">{{item.ETDETAChangeReason}}</td> <td class=\"tg-yw4l\">{{item.Satuan}}</td> </tr> <!-- <tr ng-show=\"currentChip.nopol=='B 6661 BCD'\">\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">09771-00827</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">ABSORBER A/S FR LHS</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">2</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">0</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">2</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">16/Jul/2016 15:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">16/Jul/2016 17:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\"></td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Pieces</td>\r" +
    "\n" +
    "  </tr>\r" +
    "\n" +
    "  <tr ng-show=\"currentChip.nopol=='B 6661 BCD'\">\r" +
    "\n" +
    "    <td class=\"tg-yw4l\" rowspan=\"3\">48651-18212</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\" rowspan=\"3\">COVER FR BUMPER</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\" rowspan=\"3\">10</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\" rowspan=\"3\">7</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\" rowspan=\"3\">3</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">14/Jul/2016 15:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 17:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">16/Jul/2016 08:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">16/Jul/2016 11:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">15/Jul/2016 07:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">15/Jul/2016 09:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Gagal Produksi</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Pieces</td>\r" +
    "\n" +
    "  </tr>\r" +
    "\n" +
    "  <tr ng-show=\"currentChip.nopol=='B 6661 BCD'\">\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 10:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 12:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 10:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 12:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Perubahan Dari Supplier</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Pieces</td>\r" +
    "\n" +
    "  </tr>\r" +
    "\n" +
    "  <tr ng-show=\"currentChip.nopol=='B 6661 BCD'\">\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">16/Jul/2016 15:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\"></td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Pieces</td>\r" +
    "\n" +
    "  </tr>\r" +
    "\n" +
    "  <tr ng-show=\"currentChip.nopol=='B 9918 BCD'\">\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">09095-077712</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">EXPANDER 123</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">2</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 09:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 11:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 10:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 14:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Perubahan Dari Supplier</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Pieces</td>\r" +
    "\n" +
    "  </tr>\r" +
    "\n" +
    "  <tr ng-show=\"currentChip.nopol=='B 9918 BCD'\">\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">09095-00012</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">EXPANDER</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">2</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 10:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 12:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">-</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 10:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">18/Jul/2016 12:00</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">1</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Perubahan Dari Supplier</td>\r" +
    "\n" +
    "    <td class=\"tg-yw4l\">Pieces</td>\r" +
    "\n" +
    "  </tr> --> </table> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div> <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog(0)\" style=\"color:white; background-color:#D53337\">Tutup</button> </div> </div> </div> </div> </form> </div> </div>"
  );


  $templateCache.put('app/boards/partsorder/partsorder.html',
    "<div class=\"panel panel-default\" id=\"parts-order-boards-panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i>Auto Refresh until <b>{{counter}}</b> (s)</i> </h3> <h3 class=\"panel-title pull-left\"> &nbsp; &nbsp; Special Parts Order Board </h3> <h3 class=\"panel-title pull-right\"> {{ clock | date:'dd/MMM/yyyy hh:mm:ss'}} <button type=\"button\" class=\"btn btn-default panel-title pull-right\" ng-click=\"showOptions()\" style=\"margin-top: -4px; margin-left: 4px; padding-bottom: 4px\"> <i class=\"fa fa-fw fa-gear\"></i> </button> </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row\" id=\"AdvSearch\"> <div class=\"col-sm-3 irregularity_col\"> <select ng-model=\"porderboard.category_filter\" name=\"type\" class=\"form-control\"> <option value=\"0\">Semua Category</option> <option value=\"1\">Menunggu Parts No Info</option> <option value=\"2\">Menunggu Order</option> <option value=\"3\">Status 'PO' Parts</option> <option value=\"4\">Status RPP & Parts Claim</option> </select> </div> <div class=\"col-sm-3 irregularity_col\"> <div class=\"input-group\"> <select ng-model=\"porderboard.field_filter\" name=\"type\" class=\"form-control input-group-addon\" style=\"width: 50%\"> <option value=\"all\">Semua Kolom</option> <option value=\"nopol\">No. Polisi</option> <option value=\"appointment\">No. Appointment</option> <option value=\"wo\">No. WO</option> <option value=\"po\">No. PO</option> <option value=\"rpp\">No. RPP</option> <option value=\"so\">No. SO</option> <option value=\"claim\">No. Parts Claim</option> </select> <input ng-model=\"porderboard.text_filter\" type=\"text\" class=\"form-control\" style=\"width: 50%\" placeholder=\"Filter:\"> </div> </div> <div class=\"col-sm-6 irregularity_col\" style=\"margin-bottom: 20px\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"porderboard.doFilter()\">Filter</button> <button type=\"button\" class=\"btn btn-default\" ng-click=\"porderboard.resetFilter()\">Hapus Filter</button> </div> </div> <div class=\"clearfix\"></div> <div> <div class=\"row\"> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-file-text-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_part_no_info']}}) --> ({{waiting_part_no_infoCount}}) <img src=\"images/loading.gif\" style=\"width: 20px\" ng-show=\"loading[0]\"> </h3> <h3 class=\"panel-title text-center\"> Menunggu Parts No. Info </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"parts-waiting_part_no_info\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\" style=\"margin-bottom: 6px\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-file-text-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['waiting_order']}}) --> ({{waiting_orderCount}}) <img src=\"images/loading.gif\" style=\"width: 20px\" ng-show=\"loading[1]\"> </h3> <h3 class=\"panel-title text-center\"> Menunggu Order </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"parts-waiting_order\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-wrench\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['status_order_parts']}}) --> ({{status_order_partsCount}}) <img src=\"images/loading.gif\" style=\"width: 20px\" ng-show=\"loading[2]\"> </h3> <h3 class=\"panel-title text-center\"> Status Order Parts </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"parts-status_order_parts\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-xs-3 irregularity_col\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-wrench\"></i> </h3> <h3 class=\"panel-title pull-right\"> <!-- ({{count['status_rpp_claim']}}) --> ({{status_rpp_claimCount}}) <img src=\"images/loading.gif\" style=\"width: 20px\" ng-show=\"loading[3]\"> </h3> <h3 class=\"panel-title text-center\"> Status RPP & Parts Claim </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"parts-status_rpp_claim\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/boards/partssupply/partssupply.html',
    "<div class=\"panel panel-default\" id=\"parts-order-supply-panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i>Auto Refresh until <b>{{counter}}</b> (s)</i> </h3> <h3 class=\"panel-title pull-left\"> Special Parts Supply Board </h3> <!-- <h3 class=\"panel-title pull-right\">\r" +
    "\n" +
    "              {{ clock | date:'dd/MMM/yyyy hh:mm:ss'}}\r" +
    "\n" +
    "        </h3> --> <h3 class=\"panel-title pull-right\"> {{ clock | date:'dd/MMM/yyyy hh:mm:ss'}} <button type=\"button\" class=\"btn btn-default panel-title pull-right\" ng-click=\"showOptions()\" style=\"margin-top: -4px; margin-left: 4px; padding-bottom: 4px\"> <i class=\"fa fa-fw fa-gear\"></i> </button> </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row\" id=\"AdvSearch\" hidden> <!--ng-hide=\"false\" --> <div class=\"col-sm-3 irregularity_col\"> <select ng-model=\"porderboard.category_filter\" name=\"type\" class=\"form-control\"> <option value=\"0\">Semua Category</option> <option value=\"1\">Menunggu Pre Picking</option> <option value=\"2\">Menunggu Issuing</option> </select> </div> <div class=\"col-sm-5 irregularity_col\"> <div class=\"input-group\" style=\"width:100%\"> <select ng-model=\"porderboard.field_filter\" name=\"type\" class=\"form-control input-group-addon\" style=\"width: 50%\"> <option value=\"all\">Semua Kolom</option> <option value=\"nopol\">No. Polisi</option> <option value=\"appointment\">No. Appointment</option> <option value=\"wo\">No. WO</option> </select> <input ng-model=\"porderboard.text_filter\" type=\"text\" class=\"form-control\" style=\"width: 50%\" placeholder=\"Filter:\"> </div> </div> <div class=\"col-sm-4 irregularity_col\" style=\"margin-bottom: 20px\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"porderboard.doFilter()\">Filter</button> <button type=\"button\" class=\"btn btn-default\" ng-click=\"porderboard.resetFilter()\">Hapus Filter</button> </div> </div> <div class=\"clearfix\"></div> <div> <div class=\"row\"> <div class=\"col-sm-6 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-file-text-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{jumlahPrePick}}) <!-- ({{count['waiting_pre_picking']}}) --> <!-- ({{TotalDataPrePicking}}) --> <img src=\"images/loading.gif\" style=\"width: 20px\" ng-show=\"loading[0]\"> </h3> <h3 class=\"panel-title text-center\"> Menunggu Pre Picking (Appointment) </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"parts-waiting_pre_picking\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-sm-6 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-file-text-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{jumlahIssuing}}) <!-- ({{count['waiting_issuing']}}) --> <!-- ({{TotalDataIssuing}}) --> <img src=\"images/loading.gif\" style=\"width: 20px\" ng-show=\"loading[1]\"> </h3> <h3 class=\"panel-title text-center\"> Menunggu Issuing </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"parts-waiting_issuing\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/boards/queue/queuebp.html',
    "<div id=\"base-customer-queue-bp\" style=\"background-color: #fff; border-width:0px; border-color: black; border-style: solid; padding: 0px\"> <div id=\"customer-queue-bp\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid; height: 100%\"></div> </div>"
  );


  $templateCache.put('app/boards/queue/queuegr.html',
    "<style type=\"text/css\">div.NoScroll.ng-scope {\r" +
    "\n" +
    "        overflow-y: hidden;\r" +
    "\n" +
    "    }</style> <div class=\"NoScroll\"> <div id=\"base-customer-queue-gr\" style=\"background-color: #fff; border-width:0px; border-color: black; border-style: solid; padding: 0px\"> <div id=\"customer-queue-gr\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid; height: 100%\"></div> </div> </div>"
  );


  $templateCache.put('app/boards/queue/soundtest.html',
    "<div style=\"margin-left: 10px; margin-right: 10px\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Sound Testing </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <p>Ini adalah screen untuk melakukan testing terhadap sound library. Penggunaan sound library adalah pada screen Queue Board. Bila tombol-tombol berikut di-click, maka suara harus muncul secara seketika (tanpa delay) sesuai nama tombolnya.</p> <div class=\"row\"> <div class=\"col-xs-6\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Angka </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-repeat=\"angka in soundTest.angkaList\" ng-click=\"soundTest.playSprite(angka)\">{{angka}}</button> </div> </div> </div> </div> <div class=\"col-xs-6\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Text </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"soundTest.playSprite('@')\">Pemilik Kendaraan</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"soundTest.playSprite('#')\">Harap Datang</button> </div> </div> </div> </div> </div> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Abjad </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-repeat=\"abjad in soundTest.abjadList\" ng-click=\"soundTest.playSprite(abjad)\">{{abjad}}</button> </div> </div> </div> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Pemanggilan Kendaraan </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-sm-3 irregularity_col\"> <input ng-model=\"soundTest.nopol\" type=\"text\" class=\"form-control\" placeholder=\"Nomor Polisi\"> </div> <div class=\"col-sm-3 irregularity_col\"> <select ng-model=\"soundTest.konter\" name=\"type\" class=\"form-control\"> <option ng-repeat=\"item in soundTest.counterList\" value=\"{{item.value}}\">{{item.caption}}</option> </select> </div> <div class=\"col-sm-6 irregularity_col\" style=\"margin-bottom: 20px\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"soundTest.panggil()\">Panggil</button> </div> </div> <div class=\"clearfix\"></div> </div> </div> </div> </div> </div> </div> <br> </div>"
  );


  $templateCache.put('app/boards/sacapacity/sacapacity.html',
    "<div style=\"margin-left: 10px; margin-right: 10px\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> SA Capacity Board </h3> <h3 class=\"panel-title pull-right\"> {{ clock }} </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row row-board\"> <div class=\"col-xs-12 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-body\"> <div> <div class=\"pull-right\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"sac.doFilter()\">Filter</button> </div> <div class=\"pull-right text-center\" style=\"margin-top: 8px\">&nbsp;&nbsp;</div> <div class=\"pull-right\" style=\"width: 200px\"> <bsdatepicker name=\"endDate\" date-options=\"dateOptions\" ng-model=\"sac.endDate\"></bsdatepicker> </div> <div class=\"pull-right text-center\" style=\"margin-top: 8px\">&nbsp;&nbsp; - &nbsp;&nbsp;</div> <div class=\"pull-right\" style=\"width: 200px\"> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"sac.startDate\"></bsdatepicker> </div> <div class=\"clearfix\"></div> </div> </div> </div> </div> </div> <div class=\"row row-board\"> <div class=\"col-xs-12 irregularity_col\"> <div class=\"panel panel-default\"> <div class=\"panel-body irregularity_panel\"> <div id=\"sacapacityboard\" style=\"background-color: #ddd\"></div> </div> </div> </div> </div> </div> </div> </div> </div> <br> </div>"
  );


  $templateCache.put('app/boards/satelitemon/satelitemon.html',
    "<div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Satelite Monitoring Board </h3> <h3 class=\"panel-title pull-right\"> {{ clock | date:'dd/MMM/yyyy hh:mm:ss'}} </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 13px; margin-right: -13px\"> <div class=\"row\"> <div class=\"col-md-6 irregularity_col\" style=\"margin-bottom: 6px\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Satelite </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-sm-6 irregularity_col\" style=\"margin-bottom: 6px\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-handshake-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{smoncount['ready_to_pickup']}}) </h3> <h3 class=\"panel-title text-center\"> Ready For Pickup </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"satmon-ready_for_pickup\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-sm-6 irregularity_col\" style=\"margin-bottom: 6px\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-truck\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{smoncount['waiting_for_delivery']}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Delivery </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"satmon-waitingdelivery\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> </div> </div> </div> </div> <div class=\"col-md-6 irregularity_col\" style=\"margin-bottom: 6px\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> Center </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-sm-6 irregularity_col\" style=\"margin-bottom: 6px\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-file-text-o\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{smoncount['wait_reception']}}) </h3> <h3 class=\"panel-title text-center\"> Waiting For Reception Center </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"stmon-waitingreception-center\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div class=\"col-sm-6 irregularity_col\" style=\"margin-bottom: 6px\"> <div class=\"panel panel-default irregularity_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <i class=\"fa fa-truck\"></i> </h3> <h3 class=\"panel-title pull-right\"> ({{smoncount['ready_for_delivery']}}) </h3> <h3 class=\"panel-title text-center\"> Ready For Delivery </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body irregularity_panel\"> <div id=\"stmon-readydelivery-center\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/boards/tab/tabMessage.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">{{tab.title}}</h3></div> <div class=\"panel-body\"> <p> {{tab.message}}</p> <button type=\"button \" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"confirm(1)\" style=\"color:white; background-color:#D53337\">{{tab.okText}}</button> <button type=\"button \" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0) \">{{tab.cancelText}}</button> </div> </div>"
  );


  $templateCache.put('app/boards/tab/tabgr.html',
    "<bsform-base ng-form=\"TABGr\" factory-name=\"TABGr\" model=\"mData\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" form-name=\"TABGrForm\" form-title=\"TAB\" modal-title=\"TAB\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" form-api=\"formApi\"> <bsform-base-main> <div id=\"base-tab-board-gr\" style=\"background-color: #fff; border-width:0px; border-color: black; border-style: solid; padding: 0px\"> <div id=\"tab-board-gr\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid; height: 100%\"></div> </div> </bsform-base-main> <bsform-base-detail> </bsform-base-detail> </bsform-base>"
  );


  $templateCache.put('app/boards/wolist/wolistgr.html',
    "<div class=\"row\"> <div class=\"col-sm-3\"> <div class=\"form-group\"> <label for=\"usr\">Nama Teknisi / Foreman</label> <select ng-model=\"wolist.filterTeknisi\" name=\"type\" class=\"form-control\"> <option value=\"0\">ALL</option> <option value=\"1\">ANDI</option> <option value=\"2\">ADRI</option> <option value=\"3\">ANTI</option> <option value=\"4\">ALDI</option> <option value=\"5\">ARDI</option> </select> </div> </div> <div class=\"col-sm-3\"> <div class=\"form-group\"> <label for=\"usr\">No. Polisi / No. WO</label> <input ng-model=\"wolist.filterWo\" type=\"text\" class=\"form-control\" id=\"usr\"> </div> </div> <div class=\"col-sm-6\" style=\"margin-top: 24px\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"porderboard.doFilter()\">Filter</button> </div> </div> <div class=\"clearfix\"></div> <div id=\"base-wo-list-board-gr\" style=\"background-color: #fff; border-width:0px; border-color: black; border-style: solid; padding: 0px\"> <div id=\"wo-list-board-gr\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid; height: 100%\"></div> </div>"
  );

}]);
