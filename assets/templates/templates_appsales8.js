angular.module('templates_appsales8', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/sales/sales8/boardSales/boardSales.html',
    "<script type=\"text/javascript\">function WarnainBoardSalesPageAktif(da_page) {\r" +
    "\n" +
    "\tdocument.getElementById(\"BoardSalesSelectedPage\"+da_page).style.color = \"#FF0000\";\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function WarnainBoardSalesPageGaAktif(da_page) {\r" +
    "\n" +
    "\tdocument.getElementById(\"BoardSalesSelectedPage\"+da_page).style.color = \"#3276b1\";\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function StartTimeBoardSales() {\r" +
    "\n" +
    "    var today = new Date();\r" +
    "\n" +
    "    var h = today.getHours();\r" +
    "\n" +
    "    var m = today.getMinutes();\r" +
    "\n" +
    "    var s = today.getSeconds();\r" +
    "\n" +
    "    m = CheckTimeBoardSales(m);\r" +
    "\n" +
    "    s = CheckTimeBoardSales(s);\r" +
    "\n" +
    "    document.getElementById('ClockBoardSales').innerHTML =\r" +
    "\n" +
    "    h + \":\" + m + \":\" + s;\r" +
    "\n" +
    "    var t = setTimeout(StartTimeBoardSales, 500);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function CheckTimeBoardSales(i) {\r" +
    "\n" +
    "    if (i < 10) {i = \"0\" + i};  // add zero in front of numbers < 10\r" +
    "\n" +
    "    return i;\r" +
    "\n" +
    "}</script> <style type=\"text/css\">.marquee {\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "\tline-height: 20px;\r" +
    "\n" +
    "\tcolor: black;\r" +
    "\n" +
    "    white-space: nowrap;\r" +
    "\n" +
    "    overflow: hidden;\r" +
    "\n" +
    "    box-sizing: border-box;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".marquee p {\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    padding-left: 100%;\r" +
    "\n" +
    "    animation: marquee 23s linear infinite;<!--aslinya 15s-->\r" +
    "\n" +
    "}\r" +
    "\n" +
    "@keyframes marquee {\r" +
    "\n" +
    "    0%   { transform: translate(0, 0); }\r" +
    "\n" +
    "    100% { transform: translate(-100%, 0); }\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".Monitoring_Stock_Bagi_15_head\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:15%;\r" +
    "\n" +
    "        height:40px;\r" +
    "\n" +
    "\t\tbackground-color:yellow;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tfont-size: 24px;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align: middle;\r" +
    "\n" +
    "\t\tfont-weight:bold;\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Monitoring_Stock_Bagi_12_head\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:12%;\r" +
    "\n" +
    "        height:40px;\r" +
    "\n" +
    "\t\tbackground-color:yellow;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tfont-size: 24px;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align: middle;\r" +
    "\n" +
    "\t\tfont-weight:bold;\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    ".Monitoring_Stock_Bagi_43_head\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:43%;\r" +
    "\n" +
    "        height:40px;\r" +
    "\n" +
    "\t\tbackground-color:yellow;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tfont-size: 24px;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align: middle;\r" +
    "\n" +
    "\t\tfont-weight:bold;\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Monitoring_Stock_Bagi_18_head\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:18%;\r" +
    "\n" +
    "        height:40px;\r" +
    "\n" +
    "\t\tbackground-color:yellow;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tfont-size: 24px;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align: middle;\r" +
    "\n" +
    "\t\tfont-weight:bold;\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    "\t\r" +
    "\n" +
    ".Monitoring_Stock_Bagi_15\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:15%;\r" +
    "\n" +
    "        height:30px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tfont-size: 16px;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tfont-weight:bold;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Monitoring_Stock_Bagi_12\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:12%;\r" +
    "\n" +
    "        height:30px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tfont-size: 16px;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tfont-weight:bold;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Monitoring_Stock_Bagi_43\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:43%;\r" +
    "\n" +
    "        height:30px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tfont-size: 16px;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tfont-weight:bold;\r" +
    "\n" +
    "\t\ttext-transform: uppercase;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Monitoring_Stock_Bagi_18\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:18%;\r" +
    "\n" +
    "        height:30px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tfont-size: 16px;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tfont-weight:bold;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uiboxstyle.css\"> <link rel=\"stylesheet\" href=\"css/uifullcalendarstyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/uimoment.js\"></script> <script type=\"text/javascript\" src=\"js/uifullcalendar.js\"></script> <script type=\"text/javascript\" src=\"js/uisparkline.js\"></script> <script type=\"text/javascript\" src=\"js/uiknob.js\"></script> <script type=\"text/javascript\" src=\"js/amcharts.js\"></script> <script type=\"text/javascript\" src=\"js/pie.js\"></script> <script type=\"text/javascript\" src=\"js/gauge.js\"></script> <script type=\"text/javascript\" src=\"js/funnel.js\"></script> <script type=\"text/javascript\" src=\"js/serial.js\"></script> <script type=\"text/javascript\" src=\"js/gantt.js\"></script> <script type=\"text/javascript\" src=\"js/light.js\"></script> <script type=\"text/javascript\" src=\"js/exportmin.js\"></script> <link rel=\"stylesheet\" href=\"css/export.css\" type=\"text/css\" media=\"all\"> <div class=\"row\"> <div> <div style=\"width:20%;float:left\"> <img style=\"height:100px\" src=\"../images/app/board/logo_toyota_board.png\" alt=\"Calendar\" align=\"middle\"> </div> <div style=\"width:80%;float:left\"> <div style=\"padding-left:20%\"> <b style=\"font-size:35px\">Monitoring Stock</b> </div> <div style=\"text-align:right\"> <b style=\"font-size:20px;margin-right:10px\">{{TodayDateBoardSales| date:'d MMMM y'}} <label id=\"ClockBoardSales\"></label></b> </div> <div style=\"text-align:right\"> <b style=\"font-size:20px;margin-right:10px\">(Last Update {{BoardSales_DataMonitoringStock_LastModifiedDate| date:'h:mm a'}})</b> </div> </div> </div> <div class=\"col-xs-12\"> <div> <div class=\"Monitoring_Stock_Bagi_15_head\"> Model </div> <div class=\"Monitoring_Stock_Bagi_43_head\"> Type </div> <div class=\"Monitoring_Stock_Bagi_18_head\"> Color </div> <div class=\"Monitoring_Stock_Bagi_12_head\"> Matching </div> <div class=\"Monitoring_Stock_Bagi_12_head\"> On Hand </div> </div> <div style=\"min-height:330px;margin-bottom:10px\"> <div ng-repeat=\"BoardSales_DataMonitoringStock in BoardSales_DataMonitoringStock_Data\"> <div class=\"Monitoring_Stock_Bagi_15\"> {{BoardSales_DataMonitoringStock.VehicleModelName}} </div> <div class=\"Monitoring_Stock_Bagi_43\"> {{BoardSales_DataMonitoringStock.Description}} </div> <div class=\"Monitoring_Stock_Bagi_18\"> {{BoardSales_DataMonitoringStock.ColorName}} </div> <div class=\"Monitoring_Stock_Bagi_12\"> {{BoardSales_DataMonitoringStock.Matching}} </div> <div class=\"Monitoring_Stock_Bagi_12\"> {{BoardSales_DataMonitoringStock.FreeStock}} </div> </div> </div> <div style=\"margin-top:20px\" class=\"marquee\"> <p id=\"BoardSales_MonitoringStockModelBawah\"></p> </div> <div style=\"margin-top:10px;font-size:18px;max-height:25px;max-width:90%;overflow-x:hidden;overflow-y: auto\"> <div style=\"float:left\">Pages:&nbsp;</div><div style=\"float:left\" id=\"BoardSales_MonitoringStockPage\"></div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales8/boardSalesGlobalParameter/boardSalesGlobalParameter.html',
    "<script type=\"text/javascript\">function BoardSales_GlobalParameter_Max100_CegahInput(elem){\r" +
    "\n" +
    "    if (elem.value > 100) {\r" +
    "\n" +
    "        elem.value = 100; \r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "for (i = 0; i < 21; i++) \r" +
    "\n" +
    "{\r" +
    "\n" +
    "    var number = document.getElementById('BoardSalesGlobalParameter_PreventNegative_'+i);\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\tif(i==4||i==5||i==6||i==7)\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t// Listen for input event on numInput.\r" +
    "\n" +
    "\t\tnumber.onkeydown = function(e) {\r" +
    "\n" +
    "\t\t\tif(!((e.keyCode >= 48 && e.keyCode <= 57)\r" +
    "\n" +
    "\t\t\t  || e.keyCode == 190|| e.keyCode == 8|| e.keyCode == 46)) {\r" +
    "\n" +
    "\t\t\t\t//titik tu 190 buat decimal\r" +
    "\n" +
    "\t\t\t\treturn false;\r" +
    "\n" +
    "\t\t\t}\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\telse\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t// Listen for input event on numInput.\r" +
    "\n" +
    "\t\tnumber.onkeydown = function(e) {\r" +
    "\n" +
    "\t\t\tif(!((e.keyCode >= 48 && e.keyCode <= 57)\r" +
    "\n" +
    "\t\t\t  || e.keyCode == 8|| e.keyCode == 46)) {\r" +
    "\n" +
    "\t\t\t\t//titik tu 190 buat decimal\r" +
    "\n" +
    "\t\t\t\treturn false;\r" +
    "\n" +
    "\t\t\t}\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\tdocument.getElementById('BoardSalesGlobalParameter_PreventNegative_'+i).addEventListener('keyup', function(){\r" +
    "\n" +
    "\t\t\tif(this.value.charAt(0) === '0')\r" +
    "\n" +
    "\t\t\t\tthis.value = this.value.slice(1);\r" +
    "\n" +
    "\t\t});\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "}</script> <style type=\"text/css\"></style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uiboxstyle.css\"> <link rel=\"stylesheet\" href=\"css/uifullcalendarstyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/uimoment.js\"></script> <script type=\"text/javascript\" src=\"js/uifullcalendar.js\"></script> <script type=\"text/javascript\" src=\"js/uisparkline.js\"></script> <script type=\"text/javascript\" src=\"js/uiknob.js\"></script> <link rel=\"stylesheet\" href=\"css/export.css\" type=\"text/css\" media=\"all\"> <div class=\"row\"> <button ng-disabled=\"BoardSalesGlobalParamBoardPds.OnTimeRatio==null||BoardSalesGlobalParamBoardPds.OnTimeRatio==undefined||BoardSalesGlobalParameter_Prospecting_AgingAngkaGaAman||BoardSalesGlobalParameter_Ar_AgingAngkaGaAman||BoardSalesGlobalParameter_SpkOutstanding_AgingAngkaGaAman||BoardSalesGlobalParameter_Stock_AgingAngkaGaAman||BoardSalesGlobalParameter_LebihDari100||BoardSalesGlobalParameter_StockModelSelected_UniqueCheck==false || BoardSalesGlobalParameter_TargetPenjualanSelected_UniqueCheck==false || BoardSalesGlobalParamSalesTarget.VehicleModelId==undefined||BoardSalesGlobalParamSalesTarget.VehicleModelId==null\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-bottom:10px;margin-right:10px\" ng-click=\"BoardSalesGlobalParameterSaveSetting()\"> Simpan </button> <div class=\"listData\" style=\"margin-left:10px;margin-right:10px\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_Board_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Board <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_Board\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_Board\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_Board\" class=\"formContent\"> <div class=\"listData\" style=\"text-align: center\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_Board_BoardPds_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 95%; text-align:left; background-color:#D3D3D3; color:black\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Board PDS <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_Board_BoardPds\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_Board_BoardPds\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_Board_BoardPds\" class=\"formContent\"> <div style=\"margin-left:25px\"><b>Monitoring Driver</b></div> <div style=\"margin-left:27px\"> <bsreqlabel style=\"width:152px\">Target On Time</bsreqlabel> <input style=\"width:48px\" id=\"BoardSalesGlobalParameter_PreventNegative_0\" oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" min=\"0\" ng-model=\"BoardSalesGlobalParamBoardPds.OnTimeRatio\" type=\"number\"> </div> </div> </div> <div class=\"listData\" style=\"margin-left:10px;margin-right:10px\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_DashBoard_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Dash Board <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_DashBoard\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_DashBoard\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_DashBoard\" class=\"formContent\"> <div class=\"listData\" style=\"text-align: center\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_DashBoard_PengaturanTampilan_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 95%; text-align:left; background-color:#D3D3D3; color:black\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Pengaturan Tampilan <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_DashBoard_PengaturanTampilan\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_DashBoard_PengaturanTampilan\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_DashBoard_PengaturanTampilan\" class=\"formContent\"> <div id=\"toogleItem\" style=\"margin-left:25px\"> <div class=\"input-icon-right\"> <label> Target Penjualan RS dan SPK </label> <div class=\"switch\"> <input ng-model=\"BoardSalesGlobalParamPengaturanTampilan.TargetPenjualanSPKRS\" id=\"cmn-toggle-1\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"cmn-toggle-1\"></label> </div> </div> </div> <div id=\"toogleItem\" style=\"margin-left:25px\"> <div class=\"input-icon-right\"> <label> SPK Outstanding </label> <div class=\"switch\"> <input ng-model=\"BoardSalesGlobalParamPengaturanTampilan.SPKOutstanding\" id=\"cmn-toggle-2\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"cmn-toggle-2\"></label> </div> </div> </div> <div id=\"toogleItem\" style=\"margin-left:25px\"> <div class=\"input-icon-right\"> <label> Sales Target </label> <div class=\"switch\"> <input ng-model=\"BoardSalesGlobalParamPengaturanTampilan.SalesTarget\" id=\"cmn-toggle-3\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"cmn-toggle-3\"></label> </div> </div> </div> <div id=\"toogleItem\" style=\"margin-left:25px\"> <div class=\"input-icon-right\"> <label> Prospecting </label> <div class=\"switch\"> <input ng-model=\"BoardSalesGlobalParamPengaturanTampilan.Prospecting\" id=\"cmn-toggle-4\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"cmn-toggle-4\"></label> </div> </div> </div> <!--<div id=\"toogleItem\" style=\"margin-left:25px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<label>\tProspecting Model </label>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<div class=\"switch\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<input ng-model=\"BoardSalesGlobalParamPengaturanTampilan.ProspectingModel\" id=\"cmn-toggle-5\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<label for=\"cmn-toggle-5\"></label>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</div>--> <div id=\"toogleItem\" style=\"margin-left:25px\"> <div class=\"input-icon-right\"> <label> AR </label> <div class=\"switch\"> <input ng-model=\"BoardSalesGlobalParamPengaturanTampilan.AR\" id=\"cmn-toggle-6\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"cmn-toggle-6\"></label> </div> </div> </div> <div id=\"toogleItem\" style=\"margin-left:25px\"> <div class=\"input-icon-right\"> <label> Stock </label> <div class=\"switch\"> <input ng-model=\"BoardSalesGlobalParamPengaturanTampilan.Stock\" id=\"cmn-toggle-7\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"cmn-toggle-7\"></label> </div> </div> </div> </div> <div class=\"listData\" style=\"text-align: center\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_TargetPenjualan_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 95%; text-align:left; background-color:#D3D3D3; color:black\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Target Penjualan <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_TargetPenjualan\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_TargetPenjualan\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_TargetPenjualan\" class=\"formContent\"> <div style=\"margin-left:25px\"> <div><b>RS dan SPK</b></div> <div><b>Top 5 Models</b></div> <div style=\"margin-left:25px;margin-bottom:10px\"> <label style=\"width:50px\">Model 1</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_TargetPenjualanSelected(selected)\" placeholder=\"Select Model 1\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamTargetPenjualan[0].VehicleModelId\"> </bsselect> </div> <div style=\"margin-left:25px;margin-bottom:10px\"> <label style=\"width:50px\">Model 2</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_TargetPenjualanSelected(selected)\" placeholder=\"Select Model 2\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamTargetPenjualan[1].VehicleModelId\"> </bsselect> </div> <div style=\"margin-left:25px;margin-bottom:10px\"> <label style=\"width:50px\">Model 3</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_TargetPenjualanSelected(selected)\" placeholder=\"Select Model 3\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamTargetPenjualan[2].VehicleModelId\"> </bsselect> </div> <div style=\"margin-left:25px;margin-bottom:10px\"> <label style=\"width:50px\">Model 4</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_TargetPenjualanSelected(selected)\" placeholder=\"Select Model 4\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamTargetPenjualan[3].VehicleModelId\"> </bsselect> </div> <div style=\"margin-left:25px;margin-bottom:10px\"> <label style=\"width:50px\">Model 5</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_TargetPenjualanSelected(selected)\" placeholder=\"Select Model 5\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamTargetPenjualan[4].VehicleModelId\"> </bsselect> </div> <div style=\"color:red\" ng-if=\"BoardSalesGlobalParameter_TargetPenjualanSelected_UniqueCheck==false\"> <label ng-if=\"BoardSalesGlobalParamTargetPenjualan[0].VehicleModelId==undefined|| BoardSalesGlobalParamTargetPenjualan[0].VehicleModelId==null|| BoardSalesGlobalParamTargetPenjualan[1].VehicleModelId==undefined|| BoardSalesGlobalParamTargetPenjualan[1].VehicleModelId==null|| BoardSalesGlobalParamTargetPenjualan[2].VehicleModelId==undefined|| BoardSalesGlobalParamTargetPenjualan[2].VehicleModelId==null|| BoardSalesGlobalParamTargetPenjualan[3].VehicleModelId==undefined|| BoardSalesGlobalParamTargetPenjualan[3].VehicleModelId==null|| BoardSalesGlobalParamTargetPenjualan[4].VehicleModelId==undefined|| BoardSalesGlobalParamTargetPenjualan[4].VehicleModelId==null\">Pilihan Model Tidak Boleh Ada Yang Kosong</label> <label ng-if=\"BoardSalesGlobalParamTargetPenjualan[0].VehicleModelId!=undefined && BoardSalesGlobalParamTargetPenjualan[0].VehicleModelId!=null && BoardSalesGlobalParamTargetPenjualan[1].VehicleModelId!=undefined && BoardSalesGlobalParamTargetPenjualan[1].VehicleModelId!=null && BoardSalesGlobalParamTargetPenjualan[2].VehicleModelId!=undefined && BoardSalesGlobalParamTargetPenjualan[2].VehicleModelId!=null && BoardSalesGlobalParamTargetPenjualan[3].VehicleModelId!=undefined && BoardSalesGlobalParamTargetPenjualan[3].VehicleModelId!=null && BoardSalesGlobalParamTargetPenjualan[4].VehicleModelId!=undefined && BoardSalesGlobalParamTargetPenjualan[4].VehicleModelId!=null\">Pastikan Semua Model Tidak Duplikat</label> </div> </div> </div> <div class=\"listData\" style=\"text-align: center\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_SpkOutstanding_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 95%; text-align:left; background-color:#D3D3D3; color:black\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Spk Outstanding <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_SpkOutstanding\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_SpkOutstanding\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_SpkOutstanding\" class=\"formContent\"> <div style=\"margin-bottom:10px;margin-left:25px\"> <div><b>Aging</b></div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 1</label> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_SpkOutstanding_AgingChanged();BoardSalesGlobalParameter_SpkOutstanding_AgingChanged_P1();\" id=\"BoardSalesGlobalParameter_PreventNegative_1\" min=\"0\" ng-model=\"BoardSalesGlobalParamSpkOutstanding.AgingPeriod1\" style=\"margin-right:20px;width:48px\" type=\"number\"> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_SpkOutstanding_AgingChanged();BoardSalesGlobalParameter_SpkOutstanding_AgingChanged_P2();\" id=\"BoardSalesGlobalParameter_PreventNegative_2\" min=\"0\" ng-model=\"BoardSalesGlobalParamSpkOutstanding.AgingPeriod2\" style=\"width:48px\" type=\"number\"> </div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 2</label> <input ng-disabled=\"true\" value=\"{{BoardSalesGlobalParamSpkOutstanding.AgingPeriod2+1}}\" style=\"margin-right:20px;width:48px\" type=\"number\"> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_SpkOutstanding_AgingChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_3\" min=\"0\" ng-model=\"BoardSalesGlobalParamSpkOutstanding.AgingPeriod3\" type=\"number\" style=\"width:48px\"> </div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 3</label> <input ng-disabled=\"true\" value=\"{{BoardSalesGlobalParamSpkOutstanding.AgingPeriod3+1}}\" style=\"margin-right:20px;width:48px\" type=\"number\"> <!--<input ng-disabled=\"true\" value=\"null\" type=\"number\">--> </div> <div style=\"color:red\" ng-if=\"BoardSalesGlobalParameter_SpkOutstanding_AgingAngkaGaAman\"> Pastikan angka periode diisi dengan benar </div> </div> </div> <div class=\"listData\" style=\"text-align: center\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_SalesTarget_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 95%; text-align:left; background-color:#D3D3D3; color:black\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Sales Target <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_SalesTarget\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_SalesTarget\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_SalesTarget\" class=\"formContent\"> <div style=\"margin-left:25px;margin-bottom:10px\"> <div><b>Komposisi</b></div> <div style=\"margin-left:25px\"> <label style=\"width:152px\">Prospect To Contact</label> <input lang=\"en\" style=\"width:48px\" step=\"0.1\" ng-change=\"BoardSalesGlobalParameter_KomposisiPercentageChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_4\" min=\"1\" type=\"number\" ng-model=\"BoardSalesGlobalParamSalesTarget.ProspectToContactRatio\"> &nbsp; Kali </div> <div style=\"margin-left:25px\"> <label style=\"width:152px\">Hot Prospect To Prospect</label> <input lang=\"en\" style=\"width:48px\" step=\"0.1\" ng-change=\"BoardSalesGlobalParameter_KomposisiPercentageChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_5\" min=\"1\" type=\"number\" ng-model=\"BoardSalesGlobalParamSalesTarget.HotProspectToProspectRatio\"> &nbsp; Kali </div> <div style=\"margin-left:25px\"> <label style=\"width:152px\">SPK To Hot Prospect</label> <input lang=\"en\" style=\"width:48px\" step=\"0.1\" ng-change=\"BoardSalesGlobalParameter_KomposisiPercentageChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_6\" min=\"1\" type=\"number\" ng-model=\"BoardSalesGlobalParamSalesTarget.SPKToHotProspectRatio\"> &nbsp; Kali </div> <div style=\"margin-left:25px\"> <label style=\"width:152px\">DO To SPK</label> <input lang=\"en\" style=\"width:48px\" step=\"0.1\" ng-change=\"BoardSalesGlobalParameter_KomposisiPercentageChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_7\" min=\"1\" type=\"number\" ng-model=\"BoardSalesGlobalParamSalesTarget.DOToSPKRatio\"> &nbsp; Kali </div> <!--<div ng-if=\"BoardSalesGlobalParameter_LebihDari100_KomposisiPercentage\" style=\"margin-left:25px;color:red\">\r" +
    "\n" +
    "\t\t\t\t\tJumlah Komposisi Percentage Tidak Boleh Lebih dari 100%\r" +
    "\n" +
    "\t\t\t\t</div>--> </div> <div style=\"margin-left:25px;margin-bottom:10px\"> <div><b>Weekly Percentage</b></div> <div style=\"margin-left:25px\"> <label style=\"width:152px\">Week 1</label> <input ng-change=\"BoardSalesGlobalParameter_WeekChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_8\" min=\"0\" max=\"100\" type=\"number\" ng-model=\"BoardSalesGlobalParamSalesTarget.Week1Ratio\"> &nbsp; % </div> <div style=\"margin-left:25px\"> <label style=\"width:152px\">Week 2</label> <input ng-change=\"BoardSalesGlobalParameter_WeekChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_9\" min=\"0\" max=\"100\" type=\"number\" ng-model=\"BoardSalesGlobalParamSalesTarget.Week2Ratio\"> &nbsp; % </div> <div style=\"margin-left:25px\"> <label style=\"width:152px\">Week 3</label> <input ng-change=\"BoardSalesGlobalParameter_WeekChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_10\" min=\"0\" max=\"100\" type=\"number\" ng-model=\"BoardSalesGlobalParamSalesTarget.Week3Ratio\"> &nbsp; % </div> <div style=\"margin-left:25px\"> <label style=\"width:152px\">Week 4</label> <input ng-change=\"BoardSalesGlobalParameter_WeekChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_11\" min=\"0\" max=\"100\" type=\"number\" ng-model=\"BoardSalesGlobalParamSalesTarget.Week4Ratio\"> &nbsp; % </div> <div ng-if=\"BoardSalesGlobalParameter_LebihDari100\" style=\"margin-left:25px;color:red\"> Jumlah Weekly Ratio Tidak Boleh Lebih dari 100% </div> </div> <div style=\"margin-left:25px;margin-bottom:10px\"> <b><bsreqlabel style=\"width:50px\">Model</bsreqlabel></b> <select style=\"width:50%\" ng-model=\"BoardSalesGlobalParamSalesTarget.VehicleModelId\" class=\"form-control\" ng-options=\"item.VehicleModelId as item.VehicleModelName for item in optionsModelBoardSalesGlobalParameter\" placeholder=\"Select Model 1\"> <option label=\"-- Pilih --\" value=\"\" disabled></option> </select> <!--<bsselect style=\"width:50%;\"\r" +
    "\n" +
    "\t\t\t\t\t data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Select Model 1\"\r" +
    "\n" +
    "\t\t\t\t\t ng-model=\"BoardSalesGlobalParamSalesTarget.VehicleModelId\" icon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t\t</bsselect>--> </div> <div style=\"color:red; margin-left: 25px\" ng-if=\"BoardSalesGlobalParamSalesTarget.VehicleModelId==undefined||BoardSalesGlobalParamSalesTarget.VehicleModelId==null\"> Pastikan Model Terisi </div> </div> <div class=\"listData\" style=\"text-align: center\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_Prospecting_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 95%; text-align:left; background-color:#D3D3D3; color:black\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Prospecting <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_Prospecting\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_Prospecting\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_Prospecting\" class=\"formContent\"> <div style=\"margin-bottom:10px\"> <div style=\"margin-left:25px\"><b>Aging Hot Prospect</b></div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 1</label> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Prospecting_AgingChanged();BoardSalesGlobalParameter_Prospecting_AgingChanged_P1();\" id=\"BoardSalesGlobalParameter_PreventNegative_12\" min=\"0\" ng-model=\"BoardSalesGlobalParamProspecting.AgingPeriod1\" style=\"margin-right:20px;width:48px\" type=\"number\"> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Prospecting_AgingChanged();BoardSalesGlobalParameter_Prospecting_AgingChanged_P2();\" id=\"BoardSalesGlobalParameter_PreventNegative_13\" min=\"0\" ng-model=\"BoardSalesGlobalParamProspecting.AgingPeriod2\" type=\"number\" style=\"width:48px\"> </div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 2</label> <input ng-disabled=\"true\" value=\"{{BoardSalesGlobalParamProspecting.AgingPeriod2+1}}\" style=\"margin-right:20px;width:48px\" type=\"number\"> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Prospecting_AgingChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_14\" min=\"0\" ng-model=\"BoardSalesGlobalParamProspecting.AgingPeriod3\" type=\"number\" style=\"width:48px\"> </div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 3</label> <input ng-disabled=\"true\" value=\"{{BoardSalesGlobalParamProspecting.AgingPeriod3+1}}\" style=\"margin-right:20px;width:48px\" type=\"number\"> <!--<input ng-disabled=\"true\" value=\"null\" type=\"number\">--> </div> <div style=\"color:red;margin-left:25px\" ng-if=\"BoardSalesGlobalParameter_Prospecting_AgingAngkaGaAman\"> Pastikan angka periode diisi dengan benar </div> </div> <div> <div style=\"margin-left:25px\"><b>Fokus Model</b></div> <div style=\"margin-left:25px;margin-bottom:10px\"> <bsreqlabel style=\"width:50px\">Model</bsreqlabel> <select data-ng-options=\"dataModel.VehicleModelName for dataModel in optionsModelBoardSalesGlobalParameter\" ng-change=\"filtertipeBoardSalesGlobalParameter(BoardSalesGlobalParamselectedModelProspecting.VehicleModel)\" ng-model=\"BoardSalesGlobalParamselectedModelProspecting.VehicleModel\" class=\"form-control\" style=\"width: 50%;margin-bottom: 10px\"></select> </div> <div style=\"margin-left:25px\"> <label style=\"width:50px\">Tipe</label> <select data-ng-options=\"datatipe as (datatipe.Description + '-' + datatipe.KatashikiCode + '-' + datatipe.SuffixCode) for datatipe in vehicleTypefilterBoardSalesGlobalParameter\" ng-change=\"filterColorBoardSalesGlobalParameter(BoardSalesGlobalParamselectedTipeProspecting.VehicleType)\" ng-model=\"BoardSalesGlobalParamselectedTipeProspecting.VehicleType\" class=\"form-control\" style=\"width: 50%;margin-bottom: 10px\"> <option label=\"-- Pilih --\" value=\"\"></option> </select> </div> <div style=\"margin-left:25px\"> <label style=\"width:50px\">Warna</label> <select data-ng-options=\"datacolor.ColorCodeName for datacolor in vehicleTypecolorfilterBoardSalesGlobalParameter\" ng-change=\"selectedColorValueBoardSalesGlobalParameter(BoardSalesGlobalParamselectedwarnaProspecting.Warna)\" data-ng-model=\"BoardSalesGlobalParamselectedwarnaProspecting.Warna\" class=\"form-control\" style=\"width: 50%;margin-bottom: 10px\"> <option label=\"-- Pilih --\" value=\"\"></option> </select> </div> </div> </div> <div class=\"listData\" style=\"text-align: center\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_Ar_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 95%; text-align:left; background-color:#D3D3D3; color:black\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> AR <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_Ar\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_Ar\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_Ar\" class=\"formContent\"> <div style=\"margin-bottom:10px\"> <div style=\"margin-left:25px\"><b>Aging AR</b></div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 1</label> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Ar_AgingChanged();BoardSalesGlobalParameter_Ar_AgingChanged_P1();\" id=\"BoardSalesGlobalParameter_PreventNegative_15\" min=\"0\" ng-model=\"BoardSalesGlobalParamArOverdue.AgingPeriod1\" style=\"margin-right:20px;width:48px\" type=\"number\"> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Ar_AgingChanged();BoardSalesGlobalParameter_Ar_AgingChanged_P2();\" id=\"BoardSalesGlobalParameter_PreventNegative_16\" min=\"0\" ng-model=\"BoardSalesGlobalParamArOverdue.AgingPeriod2\" type=\"number\" style=\"width:48px\"> </div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 2</label> <input ng-disabled=\"true\" value=\"{{BoardSalesGlobalParamArOverdue.AgingPeriod2+1}}\" style=\"margin-right:20px;width:48px\" type=\"number\"> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Ar_AgingChanged()\" id=\"BoardSalesGlobalParameter_PreventNegative_17\" min=\"0\" ng-model=\"BoardSalesGlobalParamArOverdue.AgingPeriod3\" type=\"number\" style=\"width:48px\"> </div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 3</label> <input ng-disabled=\"true\" value=\"{{BoardSalesGlobalParamArOverdue.AgingPeriod3+1}}\" style=\"margin-right:20px;width:48px\" type=\"number\"> <!--<input ng-disabled=\"true\" value=\"null\" type=\"number\">--> </div> <div style=\"color:red;margin-left:25px\" ng-if=\"BoardSalesGlobalParameter_Ar_AgingAngkaGaAman\"> Pastikan angka periode diisi dengan benar </div> </div> </div> <div class=\"listData\" style=\"text-align: center\"> <button ng-click=\"Show_Hide_BoardSalesGlobalParameter_Stock_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 95%; text-align:left; background-color:#D3D3D3; color:black\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Stock <span></span> <span style=\"float:right; margin-right:3px\"><i ng-show=\"!Show_Hide_BoardSalesGlobalParameter_Stock\" class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i><i ng-show=\"Show_Hide_BoardSalesGlobalParameter_Stock\" class=\"fa fa-sort-asc\" aria-hidden=\"true\"></i></span> </button> </div> <div ng-show=\"Show_Hide_BoardSalesGlobalParameter_Stock\" class=\"formContent\"> <div style=\"margin-left:25px;margin-bottom:10px\"> <div><b>Top 5 Models</b></div> <div style=\"margin-left:18px;margin-bottom:10px\"> <label style=\"width:50px\">Model 1</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_StockModelSelected(selected)\" placeholder=\"Select Model 1\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamStockMonitoring[0].VehicleModelId\"> </bsselect> </div> <div style=\"margin-left:18px;margin-bottom:10px\"> <label style=\"width:50px\">Model 2</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_StockModelSelected(selected)\" placeholder=\"Select Model 2\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamStockMonitoring[1].VehicleModelId\"> </bsselect> </div> <div style=\"margin-left:18px;margin-bottom:10px\"> <label style=\"width:50px\">Model 3</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_StockModelSelected(selected)\" placeholder=\"Select Model 3\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamStockMonitoring[2].VehicleModelId\"> </bsselect> </div> <div style=\"margin-left:18px;margin-bottom:10px\"> <label style=\"width:50px\">Model 4</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_StockModelSelected(selected)\" placeholder=\"Select Model 4\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamStockMonitoring[3].VehicleModelId\"> </bsselect> </div> <div style=\"margin-left:18px;margin-bottom:10px\"> <label style=\"width:50px\">Model 5</label> <bsselect style=\"width:50%\" data=\"optionsModelBoardSalesGlobalParameter\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"BoardSalesGlobalParameter_StockModelSelected(selected)\" placeholder=\"Select Model 5\" icon=\"fa fa-search\" ng-model=\"BoardSalesGlobalParamStockMonitoring[4].VehicleModelId\"> </bsselect> </div> <div style=\"color:red\" ng-if=\"BoardSalesGlobalParameter_StockModelSelected_UniqueCheck==false\"> <label ng-if=\"BoardSalesGlobalParamStockMonitoring[0].VehicleModelId==undefined || BoardSalesGlobalParamStockMonitoring[0].VehicleModelId==null|| BoardSalesGlobalParamStockMonitoring[1].VehicleModelId==undefined|| BoardSalesGlobalParamStockMonitoring[1].VehicleModelId==null|| BoardSalesGlobalParamStockMonitoring[2].VehicleModelId==undefined|| BoardSalesGlobalParamStockMonitoring[2].VehicleModelId==null|| BoardSalesGlobalParamStockMonitoring[3].VehicleModelId==undefined|| BoardSalesGlobalParamStockMonitoring[3].VehicleModelId==null|| BoardSalesGlobalParamStockMonitoring[4].VehicleModelId==undefined|| BoardSalesGlobalParamStockMonitoring[4].VehicleModelId==null\">Pilihan Model Tidak Boleh Ada Yang Kosong</label> <label ng-if=\"BoardSalesGlobalParamStockMonitoring[0].VehicleModelId!=undefined && BoardSalesGlobalParamStockMonitoring[0].VehicleModelId!=null && BoardSalesGlobalParamStockMonitoring[1].VehicleModelId!=undefined && BoardSalesGlobalParamStockMonitoring[1].VehicleModelId!=null && BoardSalesGlobalParamStockMonitoring[2].VehicleModelId!=undefined && BoardSalesGlobalParamStockMonitoring[2].VehicleModelId!=null && BoardSalesGlobalParamStockMonitoring[3].VehicleModelId!=undefined && BoardSalesGlobalParamStockMonitoring[3].VehicleModelId!=null && BoardSalesGlobalParamStockMonitoring[4].VehicleModelId!=undefined && BoardSalesGlobalParamStockMonitoring[4].VehicleModelId!=null\">Pastikan Semua Model Tidak Duplikat</label> </div> </div> <div style=\"margin-left:25px\"> <div><b>Aging Stock</b></div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 1</label> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Stock_AgingChanged();BoardSalesGlobalParameter_Stock_AgingChanged_P1();\" id=\"BoardSalesGlobalParameter_PreventNegative_18\" min=\"0\" ng-model=\"BoardSalesGlobalParamStockMonitoring[0].AgingPeriod1\" style=\"margin-right:20px;width:48px\" type=\"number\"> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Stock_AgingChanged();BoardSalesGlobalParameter_Stock_AgingChanged_P2()\" id=\"BoardSalesGlobalParameter_PreventNegative_19\" min=\"0\" ng-model=\"BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2\" type=\"number\" style=\"width:48px\"> </div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 2</label> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" id=\"BoardSalesGlobalParameter_PreventNegative_20\" min=\"0\" ng-disabled=\"true\" value=\"{{BoardSalesGlobalParamStockMonitoring[0].AgingPeriod2+1}}\" style=\"margin-right:20px;width:48px\" type=\"number\"> <input oninput=\"BoardSales_GlobalParameter_Max100_CegahInput(this)\" ng-change=\"BoardSalesGlobalParameter_Stock_AgingChanged()\" ng-model=\"BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3\" type=\"number\" style=\"width:48px\"> </div> <div style=\"margin-left:25px\"> <label style=\"width:100px\">Periode 3</label> <input ng-disabled=\"true\" value=\"{{BoardSalesGlobalParamStockMonitoring[0].AgingPeriod3+1}}\" style=\"margin-right:20px;width:48px\" type=\"number\"> <!--<input value=\"null\" ng-disabled=\"true\" type=\"number\">--> </div> <div style=\"color:red\" ng-if=\"BoardSalesGlobalParameter_Stock_AgingAngkaGaAman\"> Pastikan angka periode diisi dengan benar </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales8/boardSalesMonitoringDriver/boardSalesMonitoringDriver.html',
    "<script type=\"text/javascript\">function StartTimeBoardSalesMonitoringDriver() {\r" +
    "\n" +
    "        var today = new Date();\r" +
    "\n" +
    "        var h = today.getHours();\r" +
    "\n" +
    "        var m = today.getMinutes();\r" +
    "\n" +
    "        var s = today.getSeconds();\r" +
    "\n" +
    "        m = CheckTimeBoardSalesMonitoringDriver(m);\r" +
    "\n" +
    "        s = CheckTimeBoardSalesMonitoringDriver(s);\r" +
    "\n" +
    "        document.getElementById('ClockBoardSalesMOnitoringDriver').innerHTML =\r" +
    "\n" +
    "            h + \":\" + m + \":\" + s;\r" +
    "\n" +
    "        var t = setTimeout(StartTimeBoardSalesMonitoringDriver, 500);\r" +
    "\n" +
    "\r" +
    "\n" +
    "        for (var Da_Waktu = 0; Da_Waktu < 9; Da_Waktu++) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W\" + Da_Waktu).style.background = \"none\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        if (h >= 6 && h < 8) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W0\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W0_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W0_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W0_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W0_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W0_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (h >= 8 && h < 10) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W1\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W1_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W1_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W1_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W1_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W1_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (h >= 10 && h < 12) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W2\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W2_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W2_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W2_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W2_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W2_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (h >= 12 && h < 14) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W3\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W3_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W3_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W3_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W3_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W3_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (h >= 14 && h < 16) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W4\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W4_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W4_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W4_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W4_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W4_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (h >= 16 && h < 18) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W5\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W5_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W5_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W5_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W5_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W5_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (h >= 18 && h < 20) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W6\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W6_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W6_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W6_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W6_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W6_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (h >= 20 && h < 22) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W7\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W7_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W7_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W7_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W7_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W7_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        if (h >= 22 && h < 24) {\r" +
    "\n" +
    "            document.getElementById(\"BoardSalesMonitoringDriver_W8\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W8_T0_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W8_T1_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W8_T2_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W8_T3_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "            document.getElementById(\"Board_MonitoringDriver_W8_T4_Cell\").style.background = \"#00FF00\";\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    function CheckTimeBoardSalesMonitoringDriver(i) {\r" +
    "\n" +
    "        if (i < 10) {\r" +
    "\n" +
    "            i = \"0\" + i\r" +
    "\n" +
    "        }; // add zero in front of numbers < 10\r" +
    "\n" +
    "        return i;\r" +
    "\n" +
    "    }</script> <style type=\"text/css\">.Monitoring_Stock_Bagi_16koma67_head {\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "        width: 16.66%;\r" +
    "\n" +
    "        height: 50px;\r" +
    "\n" +
    "        background-color: yellow;\r" +
    "\n" +
    "        border-bottom: 1px solid;\r" +
    "\n" +
    "        border-right: 1px solid;\r" +
    "\n" +
    "        border-top: 1px solid;\r" +
    "\n" +
    "        border-left: 1px solid;\r" +
    "\n" +
    "        font-size: 18px;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .Monitoring_Stock_Bagi_16koma67_head_Tanggal {\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        height: 20px;\r" +
    "\n" +
    "        font-size: 20px;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .Monitoring_Stock_Bagi_16koma67_head_Panah {\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        height: 30px;\r" +
    "\n" +
    "        font-size: 15px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .Monitoring_Stock_Bagi_16koma67 {\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "        width: 16.66%;\r" +
    "\n" +
    "        height: 55px;\r" +
    "\n" +
    "        border-bottom: 1px solid;\r" +
    "\n" +
    "        border-right: 1px solid;\r" +
    "\n" +
    "        border-left: 1px solid;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "    }</style> <!--height kalo 7-8 perlu 108px kalo 5-6 81px kalo 3-4 55px--> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uiboxstyle.css\"> <link rel=\"stylesheet\" href=\"css/uifullcalendarstyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/uimoment.js\"></script> <script type=\"text/javascript\" src=\"js/uifullcalendar.js\"></script> <script type=\"text/javascript\" src=\"js/uisparkline.js\"></script> <script type=\"text/javascript\" src=\"js/uiknob.js\"></script> <script type=\"text/javascript\" src=\"js/amcharts.js\"></script> <script type=\"text/javascript\" src=\"js/pie.js\"></script> <script type=\"text/javascript\" src=\"js/gauge.js\"></script> <script type=\"text/javascript\" src=\"js/funnel.js\"></script> <script type=\"text/javascript\" src=\"js/serial.js\"></script> <script type=\"text/javascript\" src=\"js/gantt.js\"></script> <script type=\"text/javascript\" src=\"js/light.js\"></script> <script type=\"text/javascript\" src=\"js/exportmin.js\"></script> <link rel=\"stylesheet\" href=\"css/export.css\" type=\"text/css\" media=\"all\"> <div class=\"row\"> <div> <div style=\"width:20%;float:left\"> <img style=\"height:85px; margin-left: 50px\" src=\"../images/app/board/logo_toyota_board.png\" alt=\"Calendar\" align=\"middle\"> </div> <div style=\"width:80%;float:left\"> <div> <div><b style=\"font-size:23px\">Monitoring Driver {{BoardSalesMonitoringDriverOutletName}}</b></div> </div> <div style=\"text-align:right\"> <div style=\"float:left;width:20%;height:48px;border:1px solid black;margin-right:8px;margin-left:20px\"> <div style=\"text-align:center;font-size:17px\"><b>Target On Time</b></div> <div style=\"text-align:center;font-size:21px\"><b>{{Calculation.TargetOnTime}} %</b></div> </div> <div style=\"float:left;width:20%;height:48px;border:1px solid black;margin-right:8px\"> <div style=\"text-align:center;font-size:17px\"><b>On Time Ratio</b></div> <div style=\"text-align:center;font-size:21px\"><b>{{Calculation.OnTimeRatio}} %</b></div> </div> <div style=\"float:left;width:20%;height:48px;border:1px solid black\"> <div style=\"text-align:center;font-size:15px\"><b>Delayed Task (Pelanggan)</b></div> <div style=\"text-align:center;font-size:21px\"><b>{{Calculation.DelayedTask}}</b></div> </div> </div> <div style=\"text-align:right\"> <b style=\"font-size:10px;margin-right:10px\">{{TodayDateBoardSalesMonitoringDriver_Raw| date:'d MMMM y'}} <label id=\"ClockBoardSalesMOnitoringDriver\"></label></b> </div> <div style=\"text-align:right\"> <b style=\"font-size:10px;margin-right:10px\">(Last Update {{BoardSales_DataMonitoringDriver_LastModifiedDate| date:'h:mm a'}})</b> </div> </div> </div> <div style=\"margin-bottom:8px\" class=\"col-xs-12\"> <div> <div class=\"Monitoring_Stock_Bagi_16koma67_head\"> Waktu </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head\"> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Tanggal\"> {{TodayDateMinus1| date:'d MMMM y'}} </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Panah\"> <button ng-click=\"BoardSales_MonitoringDriver_PreviousDate()\" ng-if=\"Step != 0\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left;height:28px\"> <span class=\"fa fa-angle-left\"></span> </button> </div> </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head\"> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Tanggal\"> {{TodayDate | date:'d MMMM y'}} </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Panah\"> </div> </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head\"> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Tanggal\"> {{TodayDatePlus1 | date:'d MMMM y'}} </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Panah\"> </div> </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head\"> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Tanggal\"> {{TodayDatePlus2 | date:'d MMMM y'}} </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Panah\"> </div> </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head\"> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Tanggal\"> {{TodayDatePlus3 | date:'d MMMM y'}} </div> <div class=\"Monitoring_Stock_Bagi_16koma67_head_Panah\"> <button ng-click=\"BoardSales_MonitoringDriver_NextDate()\" ng-if=\"Step != 2\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right;height:28px\"> <span class=\"fa fa-angle-right\"></span> </button> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W0\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 06:00-08:00 </div> <div id=\"Board_MonitoringDriver_W0_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W0_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W0_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W0_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W0_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W0_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W0_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W0_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W0_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W0_T4\"> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W1\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 08:00-10:00 </div> <div id=\"Board_MonitoringDriver_W1_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W1_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W1_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W1_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W1_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W1_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W1_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W1_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W1_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W1_T4\"> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W2\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 10:00-12:00 </div> <div id=\"Board_MonitoringDriver_W2_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W2_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W2_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W2_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W2_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W2_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W2_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W2_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W2_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W2_T4\"> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W3\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 12:00-14:00 </div> <div id=\"Board_MonitoringDriver_W3_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W3_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W3_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W3_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W3_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W3_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W3_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W3_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W3_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W3_T4\"> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W4\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 14:00-16:00 </div> <div id=\"Board_MonitoringDriver_W4_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W4_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W4_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W4_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W4_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W4_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W4_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W4_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W4_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W4_T4\"> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W5\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 16:00-18:00 </div> <div id=\"Board_MonitoringDriver_W5_T0_Cell\" id=\"Board_MonitoringDriver_W5_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W5_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W5_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W5_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W5_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W5_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W5_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W5_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W5_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W5_T4\"> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W6\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 18:00-20:00 </div> <div id=\"Board_MonitoringDriver_W6_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W6_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W6_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W6_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W6_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W6_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W6_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W6_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W6_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W6_T4\"> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W7\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 20:00-22:00 </div> <div id=\"Board_MonitoringDriver_W7_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W7_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W7_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W7_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W7_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W7_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W7_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W7_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W7_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W7_T4\"> </div> </div> </div> <div> <div id=\"BoardSalesMonitoringDriver_W8\" class=\"Monitoring_Stock_Bagi_16koma67\" style=\"font-size: 30px\"> 22:00-24:00 </div> <div id=\"Board_MonitoringDriver_W8_T0_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W8_T0\"> </div> </div> <div id=\"Board_MonitoringDriver_W8_T1_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W8_T1\"> </div> </div> <div id=\"Board_MonitoringDriver_W8_T2_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W8_T2\"> </div> </div> <div id=\"Board_MonitoringDriver_W8_T3_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W8_T3\"> </div> </div> <div id=\"Board_MonitoringDriver_W8_T4_Cell\" class=\"Monitoring_Stock_Bagi_16koma67\"> <div id=\"Board_MonitoringDriver_W8_T4\"> </div> </div> </div> </div> <div style=\"float:left;width:60%;margin-left:13px\"> <div style=\"float:left;width:12.66%;background-color:#D2E6C9;height:36px\"> <b>Pengiriman Ke Customer</b> </div> <div style=\"float:left;width:12.66%;background-color:#A0D5EC;height:36px\"> <b>Pengambilan Ke PDC</b> </div> <div style=\"float:left;width:12.66%;background-color:#FCCFBA;height:36px\"> <b>Pengiriman Ke Bengkel</b> </div> <div style=\"float:left;width:12.66%;background-color:#CA9FC4;height:36px\"> <b>Pengambilan Ke Bengkel</b> </div> <div style=\"float:left;width:12.66%;background-color:#CEA668;height:36px\"> <b>Pengiriman Ke Karoseri</b> </div> <div style=\"float:left;width:12.66%;background-color:#FFF57F;height:36px\"> <b>Pengambilan Ke Karoseri</b> </div> <div style=\"float:left;width:12.66%;background-color:grey;height:36px\"> <b>Others</b> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales8/boardSalesProductivitySales/boardSalesProductivitySales.html',
    "<script type=\"text/javascript\">function WarnainBoardSalesProductivitySalesPageAktif(da_page) {\r" +
    "\n" +
    "\tdocument.getElementById(\"BoardSalesProductivitySalesSelectedPage\"+da_page).style.color = \"#FF0000\";\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function WarnainBoardSalesProductivitySalesPageGaAktif(da_page) {\r" +
    "\n" +
    "\tdocument.getElementById(\"BoardSalesProductivitySalesSelectedPage\"+da_page).style.color = \"#3276b1\";\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function StartTimeBoardSalesProductivitySales() {\r" +
    "\n" +
    "    var today = new Date();\r" +
    "\n" +
    "    var h = today.getHours();\r" +
    "\n" +
    "    var m = today.getMinutes();\r" +
    "\n" +
    "    var s = today.getSeconds();\r" +
    "\n" +
    "    m = CheckTimeBoardSalesProductivitySales(m);\r" +
    "\n" +
    "    s = CheckTimeBoardSalesProductivitySales(s);\r" +
    "\n" +
    "    document.getElementById('ClockBoardSalesProductivitySales').innerHTML =\r" +
    "\n" +
    "    h + \":\" + m + \":\" + s;\r" +
    "\n" +
    "    var t = setTimeout(StartTimeBoardSalesProductivitySales, 500);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function CheckTimeBoardSalesProductivitySales(i) {\r" +
    "\n" +
    "    if (i < 10) {i = \"0\" + i};  // add zero in front of numbers < 10\r" +
    "\n" +
    "    return i;\r" +
    "\n" +
    "}</script> <style type=\"text/css\">.Productivity_Sales_Bagi_10\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:10%;\r" +
    "\n" +
    "        height:32px;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_7\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:7%;\r" +
    "\n" +
    "        height:32px;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_11_Productivity\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:11%;\r" +
    "\n" +
    "        height:32px;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    "\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_8_TotalDO\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:8%;\r" +
    "\n" +
    "        height:32px;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_8_Rank\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:8%;\r" +
    "\n" +
    "        height:32px;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:2px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_11Koma5_Supervisor\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:11.5%;\r" +
    "\n" +
    "        height:32px;\r" +
    "\n" +
    "\t\tfont-size: 12px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:2px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_11Koma5_Sales\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:11.5%;\r" +
    "\n" +
    "        height:32px;\r" +
    "\n" +
    "\t\tfont-size: 12px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_11Koma5_head_Supervisor\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:11.5%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:90px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:2px solid;\r" +
    "\n" +
    "\t\tborder-top:2px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_10_head\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:10%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:90px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-top:2px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_7_head\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:7%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:90px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-top:2px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_11Koma5_head_Salesman\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:11.5%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:90px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-top:2px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_20_head\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:20%;\r" +
    "\n" +
    "        height:90px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_16_head_Ytm\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:16%;\r" +
    "\n" +
    "        height:90px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_19Koma5_head_dalem_Prospect\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:19.5%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:60px;\r" +
    "\n" +
    "\t\tborder-bottom:2px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_33Koma5_head_dalem\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:33.5%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:60px;\r" +
    "\n" +
    "\t\tborder-bottom:2px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_33_head\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:33%;\r" +
    "\n" +
    "        height:90px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    "\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_Multiple_Tingkat_Atas\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:100%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:30px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_2_Tingkat_Bagi2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:50%;\r" +
    "\n" +
    "        height:60px;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "\t\tborder-bottom:2px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_3_Tingkat_Bagi2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:50%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:30px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:2px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    ".Productivity_Sales_Bagi_3_Tingkat_Bagi2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:50%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:30px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:2px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "\t\tbackground-color:#F9F5F4;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    "\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_5Koma5\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:5.5%;\r" +
    "\n" +
    "\t\tfont-size: 20px;\r" +
    "\n" +
    "        height:32px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "\t\tborder-right:1px solid;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_33Koma25\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:33.25%;\r" +
    "\n" +
    "        height:60px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    ".Productivity_Sales_Bagi_32Koma35\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:32.5%;\r" +
    "\n" +
    "        height:60px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    ".Productivity_Sales_Bagi_29\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tfloat:left;\r" +
    "\n" +
    "        width:29%;\r" +
    "\n" +
    "        height:60px;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\ttext-align: center;\r" +
    "\n" +
    "\t\tvertical-align:middle;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uiboxstyle.css\"> <link rel=\"stylesheet\" href=\"css/uifullcalendarstyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/uimoment.js\"></script> <script type=\"text/javascript\" src=\"js/uifullcalendar.js\"></script> <script type=\"text/javascript\" src=\"js/uisparkline.js\"></script> <script type=\"text/javascript\" src=\"js/uiknob.js\"></script> <script type=\"text/javascript\" src=\"js/amcharts.js\"></script> <script type=\"text/javascript\" src=\"js/pie.js\"></script> <script type=\"text/javascript\" src=\"js/gauge.js\"></script> <script type=\"text/javascript\" src=\"js/funnel.js\"></script> <script type=\"text/javascript\" src=\"js/serial.js\"></script> <script type=\"text/javascript\" src=\"js/gantt.js\"></script> <script type=\"text/javascript\" src=\"js/light.js\"></script> <script type=\"text/javascript\" src=\"js/exportmin.js\"></script> <link rel=\"stylesheet\" href=\"css/export.css\" type=\"text/css\" media=\"all\"> <div class=\"row\"> <div> <div style=\"width:20%;float:left\"> <img style=\"height:100px\" src=\"../images/app/board/logo_toyota_board.png\" alt=\"Calendar\" align=\"middle\"> </div> <div style=\"width:80%;float:left\"> <div style=\"padding-left:20%\"> <b style=\"font-size:35px\">Productivity Sales</b> </div> <div style=\"text-align:right\"> <b style=\"font-size:20px;margin-right:10px\">{{BoardSalesProductivitySalesDaMonth| date:'d MMMM y'}} <label id=\"ClockBoardSalesProductivitySales\"></label></b> </div> <div style=\"text-align:right\"> <b style=\"font-size:20px;margin-right:10px\">(Last Update {{BoardSales_ProductivitySales_LastModifiedDate| date:'h:mm a'}})</b> </div> </div> </div> <div class=\"col-xs-12\"> <div style=\"margin-left:1px;margin-right:1px\"> <div class=\"Productivity_Sales_Bagi_11Koma5_head_Supervisor\"> Supervisor </div> <div class=\"Productivity_Sales_Bagi_11Koma5_head_Salesman\"> Salesman </div> <div class=\"Productivity_Sales_Bagi_10_head\"> Gol </div> <div class=\"Productivity_Sales_Bagi_10_head\"> Pros </div> <div class=\"Productivity_Sales_Bagi_7_head\"> O/S </div> <div class=\"Productivity_Sales_Bagi_33_head\"> <div class=\"Productivity_Sales_Bagi_Multiple_Tingkat_Atas\" style=\"background-color:#E6EBF5\"> {{BoardSalesProductivitySalesDaMonth| date:'MMMM'}} </div> <div class=\"Productivity_Sales_Bagi_33Koma25\"> <div class=\"Productivity_Sales_Bagi_Multiple_Tingkat_Atas\" style=\"background-color:#F9F5F4\"> SPK </div> <div class=\"Productivity_Sales_Bagi_3_Tingkat_Bagi2\"> Target </div> <div class=\"Productivity_Sales_Bagi_3_Tingkat_Bagi2\"> Ach </div> </div> <div class=\"Productivity_Sales_Bagi_33Koma25\"> <div class=\"Productivity_Sales_Bagi_Multiple_Tingkat_Atas\" style=\"background-color:#F9F5F4\"> DO </div> <div class=\"Productivity_Sales_Bagi_3_Tingkat_Bagi2\"> Target </div> <div class=\"Productivity_Sales_Bagi_3_Tingkat_Bagi2\"> Ach </div> </div> <div class=\"Productivity_Sales_Bagi_33Koma5_head_dalem\"> Prod % </div> </div> <div class=\"Productivity_Sales_Bagi_16_head_Ytm\"> <div class=\"Productivity_Sales_Bagi_Multiple_Tingkat_Atas\" style=\"background-color:#E6EBF5\"> YTM </div> <div class=\"Productivity_Sales_Bagi_2_Tingkat_Bagi2\"> Total DO </div> <div class=\"Productivity_Sales_Bagi_2_Tingkat_Bagi2\"> Rank </div> </div> </div> <div style=\"min-height:352px;margin-bottom:63px;margin-left:1px;margin-right:1px\"> <div ng-repeat=\"BoardSales_ProductivitySales in BoardSales_ProductivitySales_Data \"> <div class=\"Productivity_Sales_Bagi_11Koma5_Supervisor\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.SupervisorName}} </div> <div class=\"Productivity_Sales_Bagi_11Koma5_Sales\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.SalesmanName}} </div> <div class=\"Productivity_Sales_Bagi_10\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.Golongan}} </div> <div class=\"Productivity_Sales_Bagi_10\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.ProspectValue}} </div> <div class=\"Productivity_Sales_Bagi_7\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.OSSPK}} </div> <div class=\"Productivity_Sales_Bagi_5Koma5\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.TargetSPK}} </div> <div class=\"Productivity_Sales_Bagi_5Koma5\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.AchSPK}} </div> <div class=\"Productivity_Sales_Bagi_5Koma5\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.TargetDO}} </div> <div class=\"Productivity_Sales_Bagi_5Koma5\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.AchDO}} </div> <div class=\"Productivity_Sales_Bagi_11_Productivity\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.ProductivityPercentage}}&#37; </div> <div class=\"Productivity_Sales_Bagi_8_TotalDO\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.TotalDO}} </div> <div class=\"Productivity_Sales_Bagi_8_Rank\" style=\"background-color:{{AssignWarna[$index]}}\"> {{BoardSales_ProductivitySales.Rank}} </div> </div> </div> <div style=\"margin-top:63px;font-size:18px;max-height:25px;max-width:90%;overflow-x:hidden;overflow-y: auto\"> <div style=\"float:left\">Pages:&nbsp;</div><div style=\"float:left\" id=\"BoardSales_ProductivitySalesPage\"></div> </div> </div> </div>"
  );

}]);
