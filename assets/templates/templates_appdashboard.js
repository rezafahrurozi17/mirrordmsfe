angular.module('templates_appdashboard', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app_dashboard/bp/appointmentPerformance.html',
    "<div ng-controller=\"DashboardAppointmentBP\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-calendar\"></i> </span> <h2><strong>Appointment Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"col-xs-2\"></div> <div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div class=\"percent-sign\" style=\"font-size: 2em\"> {{data1}} </div> <div> Appointment Rate </div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div class=\"col-xs-4 col-sm-4 col-md-4 col-lg-4\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #5e9739\"> <div class=\"percent-sign\" style=\"font-size: 2em\"> {{data2}} </div> <div> Show Rate </div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div class=\"col-xs-2\"></div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/averageLeadTime.html',
    "<div ng-controller=\"AverageLeadTimeBP\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-avgLeadTime\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-clock-o\"></i> </span> <h2><strong>Workshop Avg Lead Time</strong></h2> </header> <div class=\"widget-body\" style=\"text-align: center\"> <div ng-show=\"roleId.RoleId==1129\" class=\"row\" style=\"max-width: 800px; margin: auto\"> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Avg Lead Time TPS</label> <div class=\"time-panel color7\"> {{data1}} </div><br> <b>Target Avg Lead Time TPS</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data2}}</span> </div> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Avg Lead Time Light</label> <div class=\"time-panel color8\"> {{data3}} </div><br> <b>Target Avg Lead Time Light</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data4}}</span> </div> </div> <div ng-show=\"roleId.RoleId==1129\" class=\"row\" style=\"max-width: 800px; margin: auto; margin-top: 25px\"> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Avg Lead Time Medium</label> <div class=\"time-panel color7\"> {{data5}} </div><br> <b>Target Avg Lead Time Medium</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data6}}</span> </div> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Avg Lead Time Heavy</label> <div class=\"time-panel color8\"> {{data7}} </div><br> <b>Target Avg Lead Time Heavy</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data8}}</span> </div> </div> <div ng-show=\"roleId.RoleId==1029||1032\" ng-hide=\"roleId.RoleId==1129\" class=\"row\" style=\"max-width: 800px; margin: auto\"> <div class=\"col-md-6 col-lg-6\"> <div ng-show=\"roleId.RoleId==1029\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\"> Avg Lead Time Reception</label> <div class=\"time-panel color8\"> {{data1}} </div><br> <b>Target Average Lead Time</b><br> {{data2}} </div> <div ng-show=\"roleId.RoleId!=1029\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Avg Production Lead Time </label> <div class=\"time-panel color7\"> {{data1}} </div><br> <b>Target Average Lead Time<br> <span style=\"color:#5880b3; font-weight:600\">{{data2}}</span> </b></div> </div> <div class=\"col-md-6 col-lg-6\"> <div ng-show=\"roleId.RoleId==1029\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Avg Lead Time Waiting for Reception (All SA) </label> <div class=\"time-panel color2\"> {{data3}} </div><br> <b>Target Avg Lead Time Waiting for Reception</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data4}}</span> </div> <div ng-show=\"roleId.RoleId!=1029\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Avg Lead Time TPS </label> <div class=\"time-panel color8\"> {{data3}} </div><br> <b>Target Avg Lead Time TPS</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data4}}</span> </div> </div> </div> <div ng-show=\"roleId.RoleId==1129\" class=\"row\" style=\"max-width: 400px; margin: auto; margin-top: 25px\"> <div> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Avg Lead Time </label> <div class=\"time-panel color8\"> {{data9}} </div><br> <b>Target Avg Lead Time</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data10}}</span> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/averageLeadTimeSA.html',
    "<div ng-controller=\"AverageLeadTimeSABP\" class=\"row\"> <article class=\"col-md-12 col-lg-12\"> <div id=\"widget-avgLeadTime\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-clock-o\"></i> </span> <h2><strong>Today's Reception Average Lead Time</strong></h2> </header> <div class=\"widget-body\" style=\"text-align: center\"> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Your Average Lead Time Reception</label> <div class=\"time-panel color6\"> {{data1}} </div><br> <!-- <b>Target Average Lead Time<br>\r" +
    "\n" +
    "                        {{data2}}</b> --> </div> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Average Lead Time Waiting for Reception (ALL SA)</label> <div class=\"time-panel color8\"> {{data3}} </div><br> <b>Target Average Lead Time Waiting for Reception<br> <span style=\"color:#3168e0\">{{data4}}</span></b> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/groupPerformance.html',
    "<div ng-controller=\"DashboardGroupBP\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-group-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-users\"></i> </span> <h2 ng-if=\"user.RoleId == 1032\"><strong>All Group Performance</strong></h2> <h2 ng-if=\"user.RoleId !== 1032\"><strong>Group Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 1000px; margin: auto; display: flex;align-items: center\"> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div style=\"font-size: 200%\">{{data1}}%</div> <div>Stall Capacity</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color1\"> <div style=\"font-size: 200%\">{{data2}}</div> <div style=\"font-size: 90%\">Repair Unit Mechanic<br>(RUM)</div> </div> </div> <div class=\"row\" style=\"text-align: center; padding-top: 23px\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color1\"> <div style=\"font-size: 200%\">{{data3}}</div> <div style=\"font-size: 90%\">Repair Per Stall<br>(RPS)</div> </div> </div> <div class=\"row\" style=\"text-align: center; padding-top: 23px\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div style=\"font-size: 200%\">{{data4}}%</div> <div>Labour Utility (LU)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> <div class=\"row\" style=\"max-width: 800px; margin: auto\"> <div class=\"col-md-4 col-lg-4\"> <div class=\"row\" style=\"height: 170px; overflow: hidden\"> <div style=\"height: 220px\"> <amchart ng-model=\"gaugeOptions1\" width=\"100%\"></amchart> </div> </div> <div class=\"row\" style=\"text-align: center;font-weight: bolder\">Technician Efeciency (TE)</div> <div class=\"row\" style=\"text-align: center\">M to H-1</div> </div> <div class=\"col-md-4 col-lg-4\" style=\"height: 220px; padding-top: 30px\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data6\"> <span class=\"percent\" style=\"margin-top: -3em; color: #5e9739\"> <div style=\"font-size: 200%\">{{data6}}%</div> <div>Non FIR (Q1)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div class=\"col-md-4 col-lg-4\"> <div class=\"row\" style=\"height: 170px; overflow: hidden\"> <div style=\"height: 220px\"> <amchart ng-model=\"gaugeOptions2\" width=\"100%\"></amchart> </div> </div> <div class=\"row\" style=\"text-align: center; font-weight: bolder\">Overall Productivity (OP)</div> <div class=\"row\" style=\"text-align: center\">M to H-1</div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/groupPerformanceForeman.html',
    "<div ng-controller=\"DashboardGroupForemanBP\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-group-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-users\"></i> </span> <h2><strong>Group Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 1000px; margin: auto; display: flex;align-items: center\"> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div style=\"font-size: 200%\">{{data1}}%</div> <!-- <div>Return Job Rate<br>\r" +
    "\n" +
    "                                                (RTJ)\r" +
    "\n" +
    "                                            </div> --> <div>Stall Capacity</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color1\"> <div style=\"font-size: 200%\">{{data4}}</div> <!-- <div style=\"font-size: 90%\">Repair Per Stall<br>(RPS) TPS</div> --> <div style=\"font-size: 90%\">Repair Unit per Stall<br>(RPS) TPS</div> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color1\"> <div style=\"font-size: 200%\">{{data3}}</div> <!-- <div style=\"font-size: 90%\">Repair Unit Mechanic<br>(RUM)</div> --> <div style=\"font-size: 90%\">Repair Unit per Mechanic<br>(RUM)</div> </div> </div> <div class=\"row\" style=\"text-align: center;padding-top: 23px\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color1\"> <div style=\"font-size: 200%\">{{data8}}</div> <!-- <div style=\"font-size: 90%\">Repair Per Stall<br>(RPS) Reguler</div> --> <div style=\"font-size: 90%\">Repair Unit per Stall<br>(RPS) Reguler</div> </div> </div> <div class=\"row\" style=\"text-align: center;padding-top: 23px\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data7\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div style=\"font-size: 200%\">{{data7}}%</div> <div>Labour Utilization <br>(LU)</div> <!-- <div>Labour Utility (LU)</div> --> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> <div class=\"row\" style=\"max-width: 800px; margin: auto\"> <div class=\"col-md-4 col-lg-4\"> <div class=\"row\" style=\"height: 170px; overflow: hidden\"> <div style=\"height: 220px\"> <amchart ng-model=\"gaugeOptions1\" width=\"100%\"></amchart> </div> </div> <div class=\"row\" style=\"text-align: center;font-weight: bolder\">Technician Efficiency (TE)</div> <div class=\"row\" style=\"text-align: center\">M to H-1</div> </div> <div class=\"col-md-4 col-lg-4\" style=\"height: 220px; padding-top: 30px\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #5e9739\"> <div style=\"font-size: 200%\">{{data2}}%</div> <div>Non FIR (Q1)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div class=\"col-md-4 col-lg-4\"> <div class=\"row\" style=\"height: 170px; overflow: hidden\"> <div style=\"height: 220px\"> <amchart ng-model=\"gaugeOptions2\" width=\"100%\"></amchart> </div> </div> <div class=\"row\" style=\"text-align: center; font-weight: bolder\">Overall Productivity (OP)</div> <div class=\"row\" style=\"text-align: center\">M to H-1</div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/partsmanPerformance.html',
    "<div ng-controller=\"PartsmanBP\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-partsman-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-wrench\"></i> </span> <h2><strong>Partsman Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 800px; margin: auto\"> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #d36e2a\"> <div style=\"font-size: 200%\">{{data1}}%</div> <div>Immediate Fill Rate</div> </span> </div> </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #5b9435\"> <div style=\"font-size: 200%\">{{data2}}%</div> <div>Fill Rate</div> </span> </div> </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color2\"> <div style=\"font-size: 200%\">{{data3}}</div> <div style=\"font-size: 90%\">Stock Day</div> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #d36e2a\"> <div style=\"font-size: 200%\">{{data4}}%</div> <div>Service Rate</div> </span> </div> </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options4\" percent=\"data5\"> <span class=\"percent\" style=\"margin-top: -3em; color: #508abc\"> <div style=\"font-size: 200%\">{{data5}}%</div> <div>Back Order Performance</div> </span> </div> </div> </div> </div> <div class=\"row\" style=\"max-width: 600px; margin: auto\"> </div> </div> </div> </article> <article class=\"col-sm-12 col-md-6 col-lg-6\"> <div id=\"widget-partsman-fillrate-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Fill Rate</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"barOptionsFillRate\" width=\"100%\"></amchart> </div> </div> </article> <article class=\"col-sm-12 col-md-6 col-lg-6\"> <div id=\"widget-partsman-imfillrate-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Immediate Fill Rate</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"barOptionsImFillRate\" width=\"100%\"></amchart> </div> </div> </article> <article class=\"col-sm-12 col-md-6 col-lg-6\"> <div id=\"widget-partsman-service-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Service Rate</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"barOptionsSvcRate\" width=\"100%\"></amchart> </div> </div> </article> <article class=\"col-sm-12 col-md-6 col-lg-6\"> <div id=\"widget-partsman-stock-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Stock Day</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"barOptionsStockDay\" width=\"100%\"></amchart> </div> </div> </article> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-partsman-composition-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Komposisi Order</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"multilineOptions\" width=\"100%\"></amchart> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/progressToTarget.html',
    "<style>.amcharts-chart-div a {display:none !important;}</style> <div ng-controller=\"ProgressToTargetBP\" class=\"row\"> <article class=\"col-lg-12\"> <div id=\"widget-progress-to-target\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"><i class=\"fa fa-line-chart\"></i></span> <h2><strong>Workshop Progress To Target</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; overflow: hidden\"> <div class=\"col-md-4 col-lg-4\" style=\"height: 300px; padding: 0px\"> <div class=\"row\" style=\"text-align: center; font-weight: bolder; font-size: 1.1em\">Unit CPUS vs Target</div> <div class=\"row\" style=\"height: 200px; overflow: hidden\"> <amchart ng-model=\"gaugeOptions1\"></amchart> </div> <div class=\"row\" style=\"background-color: #4e4d4d; border-radius: 20px; color: white; margin-left: 10%; width: 80%;padding: 10px\"> <div class=\"col-md-6\" style=\"text-align: center;border-right:1px solid #fff\"> Unit Entry<br> {{data1+data2}} </div> <div class=\"col-md-6\" style=\"text-align: center\"> Unit Non CPUS<br> {{data2}} </div> </div> </div> <div class=\"col-md-4 col-lg-4\" style=\"padding-top: 50px\"> <div class=\"row\"> <div class=\"col-md-4\" style=\"text-align: left\"> <b>Unit CPUS</b> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-5\" style=\"text-align: right\"> <b>Revenue CPUS</b> </div> </div> <div class=\"row\" style=\"border-radius: 10px; border: solid 1px #555; font-size: 120%\"> <div class=\"col-md-3\" style=\"text-align: right\"> <b style=\"color: coral\">{{data5.toLocaleString()}}</b><br><b style=\"color: greenyellow\">{{data6.toLocaleString()}}</b> </div> <div class=\"col-md-4\" style=\"text-align: center\"> <b>Yesterday<br>Today</b> </div> <div class=\"col-md-5\" style=\"text-align: right\"> <b style=\"color: coral\">{{data7.toLocaleString()}}</b><br><b style=\"color: greenyellow\">{{data8.toLocaleString()}}</b> </div> </div> <div class=\"row\" style=\"text-align: center\"> <b style=\"font-size: 150%;color: coral\">{{data9}} Days</b><br> <b>Left till End of Month</b> </div> </div> <div class=\"col-md-4 col-lg-4\" style=\"height: 300px; padding: 0px\"> <div class=\"row\" style=\"text-align: center; font-weight: bolder; font-size: 1.1em\">Revenue CPUS vs Target</div> <div style=\"height: 200px; overflow: hidden\"> <!-- <div style=\"height: 320px\"> --> <amchart ng-model=\"gaugeOptions2\" width=\"100%\"></amchart> <!-- </div> --> </div> <div class=\"row\" style=\"background-color: #4e4d4d; border-radius: 20px; color: white; margin-left: 5%; width: 90%; padding:10px\"> <div class=\"col-md-6\" style=\"text-align: center;border-right:1px solid #fff\"> Revenue Amount<br> {{(data3+data4).toLocaleString()}} </div> <div class=\"col-md-6\" style=\"text-align: center\"> Amount Non CPUS<br> {{data4.toLocaleString()}} </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/progressToTargetSa.html',
    "<style>.chartWrapper{\r" +
    "\n" +
    "            /* width: 90%; */\r" +
    "\n" +
    "            margin: auto;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            /* padding-bottom: 50%; */\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        .chartnya{\r" +
    "\n" +
    "            height:200px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        .chartnya .subchart{\r" +
    "\n" +
    "            height:200px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .amcharts-chart-div a {display:none !important;}</style> <div ng-controller=\"ProgressToTargetSaBP\" class=\"row\"> <article class=\"col-lg-12\"> <div id=\"widget-progress-to-target\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header style=\"background-color:#6e6e6e;color:aliceblue\"> <span class=\"widget-icon\"><i class=\"fa fa-line-chart\"></i></span> <h2><strong>Your Progress To Target</strong></h2> </header> <div class=\"widget-body\" style=\"color:#000\"> <div class=\"row chartWrapper\"> <div class=\"col-md-12\" style=\"margin: auto; text-align: center\"> <div class=\"col-lg-4 chartnya\"> <div class=\"row subchart\"> <!-- <div style=\"height: 220px\"> --> <amchart ng-model=\"gaugeOptions1\" width=\"100%\"></amchart> <!-- </div> --> </div> <div class=\"row\" style=\"text-align: center;font-weight: bolder;font-size:1.1em;color:#808080\">Unit Entry vs Target</div> </div> <div class=\"col-lg-4\" style=\"padding-top: 50px;margin-top:3%\"> <div class=\"row\"> <div class=\"col-md-3\" style=\"text-align: right\"> <b>Unit(In)/Delivery(Out)</b> </div> <div class=\"col-md-4\"> </div> <div class=\"col-md-5\" style=\"text-align: right\"> <b>Revenue</b> </div> </div> <div class=\"row\" style=\"border-radius: 15px; border: solid 1px #555; font-size: 120%\"> <div class=\"col-md-3\" style=\"text-align: left\"> <b style=\"color: coral\">{{data4.toLocaleString()}}/{{data3.toLocaleString()}}</b><br><br><b style=\"color: lightgreen\">{{data2.toLocaleString()}}/{{data1.toLocaleString()}}</b> </div> <div class=\"col-md-5\" style=\"text-align: center\"> <b>Today<br><br>Yesterday</b> </div> <div class=\"col-md-4\" style=\"text-align: right\"> <b style=\"color: coral\">{{data8.toLocaleString()}}</b><br><br><b style=\"color: lightgreen\">{{data9.toLocaleString()}}</b> </div> </div> <div class=\"row\" style=\"text-align: center\"> <b style=\"font-size: 200%;color:coral\">{{data6.toLocaleString()}}</b> <b style=\"font-size: 200%;color:coral\">DAYS</b> <br><b style=\"font-size: 150%\"> Left till End of Month </b> <!-- <b style=\"font-size: 150%\">{{daysLeft}}</b><br>\r" +
    "\n" +
    "                                        <b>Left till End of Month</b> --> </div> </div> <div class=\"col-lg-4 chartnya\"> <div class=\"row subchart\"> <!-- <div style=\"height: 220px\"> --> <amchart ng-model=\"gaugeOptions2\" width=\"100%\"></amchart> <!-- </div> --> </div> <div class=\"row\" style=\"text-align: center;font-weight: bolder;font-size:1.1em;color:#808080\">Revenue vs Target</div> </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/psfuPerformance.html',
    "<div ng-controller=\"PSFUBP\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-psfu-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-phone\"></i> </span> <h2><strong>PSFU Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 800px; margin: auto\"> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -2em; color: #d36d27\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data1}}</div> <div>Follow Up Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -2em; color: #508abb\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data2}}</div> <div>Fix It Rate (FIR)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data3\"> <span class=\"percent\" style=\"margin-top: -2em; color: #508abb\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data3}}</div> <div>Q1</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options4\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -2em; color: #62993e\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data4}}</div> <div>Q2</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options5\" percent=\"data5\"> <span class=\"percent\" style=\"margin-top: -2em; color: #d36e2a\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data5}}</div> <div>Q3</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/saPerformance.html',
    "<div ng-controller=\"SABP\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-sa-bp\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-phone\"></i> </span> <h2><strong>SA Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 1000px; margin: auto\"> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color:  #d46d29\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data1}}</div> <div>On Time Delivery 1<br>(OTD 1)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data2}}</div> <div>One Time Delivery 2<br>(OTD 2)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data3\"> <span class=\"percent\" style=\"margin-top: -3em; color: #5e9739\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data3}}</div> <div>Return Job Rate (RTJ)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options4\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data4}}</div> <div>Delivery Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/workshopPerformance.html',
    "<div ng-controller=\"WorkshopBP\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-partsman-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-phone\"></i> </span> <h2><strong>Workshop Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"margin: auto\"> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #d46d29\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data1}}</div> <div>On Time Delivery 1<br>(OTD 1)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data2}}</div> <div>One Time Delivery 2<br>(OTD 2)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data3\"> <span class=\"percent\" style=\"margin-top: -3em; color: #5e9739\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data3}}</div> <div>Fill Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options4\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #5e9739\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data4}}</div> <div>Return Job Rate<br>(RTJ)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options5\" percent=\"data5\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data5}}</div> <div>Re-Do Paint</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options6\" percent=\"data6\"> <span class=\"percent\" style=\"margin-top: -3em; color: #568ab7\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data6}}</div> <div>Re-Do Fi</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> <div class=\"row\" style=\"font-size: 8px; margin-top: 30px\"> <div class=\"col-md-4 col-lg-4\" style=\"height: 400px\"> <amchart ng-model=\"donutOptions\" width=\"100%\"></amchart> </div> <div class=\"col-md-4 col-lg-4\" style=\"height: 400px\"> <amchart ng-model=\"donutOptions2\" width=\"100%\"></amchart> </div> <div class=\"col-md-4 col-lg-4\" style=\"height: 400px\"> <amchart ng-model=\"donutOptions3\" width=\"100%\"></amchart> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/bp/workshopUnitEntry.html',
    "<div ng-controller=\"UnitEntryBP\" class=\"row\"> <article class=\"col-lg-12\"> <div id=\"widget-workshop-unit-entry\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"><i class=\"fa fa-car\"></i></span> <h2><strong>Today's Workshop Unit Entry</strong></h2> </header> <div class=\"widget-body\"> <div class=\"col-md-3 col-lg-3\"> <div class=\"dsh-panel\" style=\"background-color: #5880b3\"> <label style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">Appointment</label> <br> <label style=\"font-size: 180%;font-weight:300\">{{data1}}</label> <div class=\"bot-left-panel\"> <label style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">Show</label> <br><label style=\"font-size: 120%;font-weight:300\">{{data2}}</label> </div> <div class=\"bot-right-panel\"> <label style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">No Show</label> <br><label style=\"font-size: 120%;font-weight:300\">{{data3}}</label> </div> <div class=\"hor-splitter\"></div> <div class=\"ver-splitter\"></div> </div> </div> <div class=\"col-md-2 col-lg-2\"> <div class=\"dsh-panel\" style=\"background-color: #00af50\"> <label style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">Walk In</label> <br><br> <label style=\"font-size: 250%;font-weight:300\">{{data4}}</label> </div> </div> <div class=\"col-md-2 col-lg-2\"> <div class=\"dsh-panel\" style=\"background-color: #595959\"> <label style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">Unit Entry</label> <br> <label style=\"font-size: 180%;font-weight:300\">{{data5}}</label> <div class=\"bot-left-panel\"> <label style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">Cash</label> <br><label style=\"font-size: 120%;font-weight:300\">{{data6}}</label> </div> <div class=\"bot-right-panel\"> <label style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">Insurance</label> <br><label style=\"font-size: 120%;font-weight:300\">{{data7}}</label> </div> <div class=\"hor-splitter\"></div> <div class=\"ver-splitter\"></div> </div> </div> <div class=\"col-md-2 col-lg-2\"> <div class=\"dsh-panel\" style=\"background-color: #7d649e\"> <label style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">App Tomorrow</label> <br><br> <label style=\"font-size: 250%;font-weight:300\">{{data8}}</label> </div> </div> <div class=\"col-md-3 col-lg-3\"> <div class=\"dsh-panel\" style=\"background-color: #ea700d\"> <label style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">WIP</label> <br> <label style=\"font-size: 180%;font-weight:300\">{{data9}}</label> <div class=\"bot-left-panel\"> <label style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">Being Serviced</label> <br><label style=\"font-size: 120%;font-weight:300\">{{data10}}</label> </div> <div class=\"bot-right-panel\"> <label style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">Billing</label> <br><label style=\"font-size: 120%;font-weight:300\">{{data11}}</label> </div> <div class=\"hor-splitter\"></div> <div class=\"ver-splitter\"></div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/dashboard.html',
    "<style>@import url('https://fonts.googleapis.com/css?family=Fredoka+One&display=swap');\r" +
    "\n" +
    "    @import url('https://fonts.googleapis.com/css?family=Dosis:700&display=swap');\r" +
    "\n" +
    "    @import url('https://fonts.googleapis.com/css?family=Montserrat&display=swap');\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.loader {\r" +
    "\n" +
    "\t  border: 16px solid #f3f3f3;\r" +
    "\n" +
    "\t  border-radius: 50%;\r" +
    "\n" +
    "\t  border-top: 16px solid #3498db;\r" +
    "\n" +
    "\t  width: 120px;\r" +
    "\n" +
    "\t  height: 120px;\r" +
    "\n" +
    "\t  -webkit-animation: spin 2s linear infinite; /* Safari */\r" +
    "\n" +
    "\t  animation: spin 2s linear infinite;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t/* Safari */\r" +
    "\n" +
    "\t@-webkit-keyframes spin {\r" +
    "\n" +
    "\t  0% { -webkit-transform: rotate(0deg); }\r" +
    "\n" +
    "\t  100% { -webkit-transform: rotate(360deg); }\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t@keyframes spin {\r" +
    "\n" +
    "\t  0% { transform: rotate(0deg); }\r" +
    "\n" +
    "\t  100% { transform: rotate(360deg); }\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .color1 {\r" +
    "\n" +
    "        background-color: #0ae;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .color2 {\r" +
    "\n" +
    "        background-color: #0aa;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .color3 {\r" +
    "\n" +
    "        background-color: #f80;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .color4 {\r" +
    "\n" +
    "        background-color: #e39;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .color5 {\r" +
    "\n" +
    "        background-color: #e33;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .color6 {\r" +
    "\n" +
    "        background-color: #888;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .color7{\r" +
    "\n" +
    "        background-color: #ff8262;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .color8{\r" +
    "\n" +
    "        background-color: #92d14f;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .dsh-panel {\r" +
    "\n" +
    "        border-radius: 5px;\r" +
    "\n" +
    "        height: 120px !important;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        padding: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ver-splitter {\r" +
    "\n" +
    "        top: 53%;\r" +
    "\n" +
    "        left: 50%;\r" +
    "\n" +
    "        width: 1px;\r" +
    "\n" +
    "        background-color: white;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        height: 47%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .hor-splitter {\r" +
    "\n" +
    "        top: 53%;\r" +
    "\n" +
    "        left: 0;\r" +
    "\n" +
    "        height: 1px;\r" +
    "\n" +
    "        background-color: white;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .bot-left-panel{\r" +
    "\n" +
    "        top: 55%;\r" +
    "\n" +
    "        left: 0;\r" +
    "\n" +
    "        width: 50%;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .bot-right-panel{\r" +
    "\n" +
    "        top: 55%;\r" +
    "\n" +
    "        left: 50%;\r" +
    "\n" +
    "        width: 50%;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .time-panel{\r" +
    "\n" +
    "        border-radius: 20px;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "        font-size: 200%;\r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .circle-panel{\r" +
    "\n" +
    "        height: 120px;\r" +
    "\n" +
    "        width: 120px;\r" +
    "\n" +
    "        border-radius: 60px;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        /* padding-top: 10%; */\r" +
    "\n" +
    "        padding-top:25px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t#chartdivProspectWithLabel1 {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 390px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t#chartdivProspectWithLabel1 {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 300px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t#chartdivProspectWithLabel2 {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 300px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t#chartdivProspectWithLabel1_2 {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 300px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t#chartdivProspectWithLabel2_2 {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 300px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t#chartstackedbarSalesTarget {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 500px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t#chartstackedbarSalesTargetModel {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 500px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t#RsDashboardGantiTabelKeGrafik {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 400px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t#SpkDashboardGantiTabelKeGrafik {\r" +
    "\n" +
    "\t  width: 100%;\r" +
    "\n" +
    "\t  height: 400px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t.PentolLegend {\r" +
    "\n" +
    "        height: 20px;\r" +
    "\n" +
    "        width: 20px;\r" +
    "\n" +
    "        border-radius: 50%;\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t.PentolBuatRsBaruLagiModelnya {\r" +
    "\n" +
    "  border-radius: 50%;\r" +
    "\n" +
    "  behavior: url(PIE.htc);\r" +
    "\n" +
    "  /* remove if you don't care about IE8 */\r" +
    "\n" +
    "  width: 36px;\r" +
    "\n" +
    "  height: 36px;\r" +
    "\n" +
    "  padding: 8px;\r" +
    "\n" +
    "  background: #fff;\r" +
    "\n" +
    "  border: 2px solid #666;\r" +
    "\n" +
    "  color: #666;\r" +
    "\n" +
    "  text-align: center;\r" +
    "\n" +
    "  font: 32px Arial, sans-serif;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.c1\r" +
    "\n" +
    "{\r" +
    "\n" +
    "float:left;\r" +
    "\n" +
    "\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".da_tinggi_tabel\r" +
    "\n" +
    "{\r" +
    "\n" +
    "\theight:390px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".headDiv\r" +
    "\n" +
    "{\r" +
    "\n" +
    "    width:100%;\r" +
    "\n" +
    "    border:1px soild blue;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .w50\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:50%;\r" +
    "\n" +
    "        height:200px;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.w50_2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:50%;\r" +
    "\n" +
    "        height:100px;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.w40\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:40%;\r" +
    "\n" +
    "        height:200px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.w20\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:20%;\r" +
    "\n" +
    "        height:200px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.w20_2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:20%;\r" +
    "\n" +
    "        height:100px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.w2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:100%;\r" +
    "\n" +
    "        height:100px;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .w1\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:100%;\r" +
    "\n" +
    "        height:200px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "        border-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakTotalAchievementAtas\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 36px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakTotalAchievementKiri\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 33px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakTotalAchievementKanan\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 32px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakAchievementAtas\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 23px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakAchievementKiriAtas\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 16px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakAchievementKiriBawah\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 26px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakAchievementKanan\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 16px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t.LebarTabelDashBaru\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\twidth:100%;\r" +
    "\n" +
    "        font-size:25px;\r" +
    "\n" +
    "        font-weight: 400;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "\r" +
    "\n" +
    "        /* font-family: 'Montserrat', sans-serif; */\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .iconDashboard{\r" +
    "\n" +
    "        font-size: 30px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .LebarTabelDashBaru tr{\r" +
    "\n" +
    "        border:2px solid white;\r" +
    "\n" +
    "        height:60px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .LebarTabelDashBaru tr td{\r" +
    "\n" +
    "        border:2px solid white;\r" +
    "\n" +
    "        width:40px; \r" +
    "\n" +
    "\t\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .DashboardTabel{\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "        width: 40%;\r" +
    "\n" +
    "        height:500px; \r" +
    "\n" +
    "        margin-bottom: -50%;\r" +
    "\n" +
    "        margin-top:-40px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .DashboardDiagram{\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "        width: 60%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .margintabelBawah{\r" +
    "\n" +
    "        margin-top: 7% !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .namaMobilStyle{\r" +
    "\n" +
    "        font-size: 20px;\r" +
    "\n" +
    "        font-weight: 100;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* tampilan mobile */\r" +
    "\n" +
    "  @media only screen \r" +
    "\n" +
    "  and (max-device-width: 890px)\r" +
    "\n" +
    "  and (-webkit-min-device-pixel-ratio: 2) {\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .namaMobilStyle{\r" +
    "\n" +
    "        font-size: 13px !important;\r" +
    "\n" +
    "        font-weight: 100;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .margintabelBawah{\r" +
    "\n" +
    "        margin-top: 232px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .DashboardDiagram{\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "        width: 40%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .DashboardTabel{\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "        width: 60%;\r" +
    "\n" +
    "        margin-bottom: -180% !important;\r" +
    "\n" +
    "        margin-top:0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .LebarTabelDashBaru\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\twidth:100%;\r" +
    "\n" +
    "        font-size:12px;\r" +
    "\n" +
    "        font-weight: 400;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "        margin-top: 20px !important; \r" +
    "\n" +
    "        /* font-family: 'Montserrat', sans-serif; */\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .LebarTabelDashBaru tr{\r" +
    "\n" +
    "        border:1px solid white;\r" +
    "\n" +
    "        height:25px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .LebarTabelDashBaru tr td{\r" +
    "\n" +
    "        border:1px solid white;\r" +
    "\n" +
    "        width:20px !important; \r" +
    "\n" +
    "\t\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .tabelAtas{\r" +
    "\n" +
    "        margin-top: 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .iconDashboard{\r" +
    "\n" +
    "        font-size: 15px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* css galang */\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .container_circle{\r" +
    "\n" +
    "        width:130px !important;\r" +
    "\n" +
    "        height:130px !important;\r" +
    "\n" +
    "        margin:15px !important;\r" +
    "\n" +
    "        margin:5px 15px 15px 15px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .circleDiagram{\r" +
    "\n" +
    "        width:100%;\r" +
    "\n" +
    "        height:100%;\r" +
    "\n" +
    "        background: white !important; \r" +
    "\n" +
    "        color:black;\r" +
    "\n" +
    "        border-radius: 50%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .warnaAsli{\r" +
    "\n" +
    "        width:100%;\r" +
    "\n" +
    "        height:100%;\r" +
    "\n" +
    "        border-radius: 50%;\r" +
    "\n" +
    "        padding:15%;\r" +
    "\n" +
    "        \r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .circleDiagram div{\r" +
    "\n" +
    "        padding-top:30% !important;\r" +
    "\n" +
    "        font-size:15px !important;\r" +
    "\n" +
    "        font-family: 'Dosis', sans-serif !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .circleDiagram div span{\r" +
    "\n" +
    "        font-size:25px !important;\r" +
    "\n" +
    "        font-family: 'Dosis', sans-serif !important;\r" +
    "\n" +
    "        line-height:1 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .kotak{\r" +
    "\n" +
    "        width:80px !important;\r" +
    "\n" +
    "        height:20px !important;\r" +
    "\n" +
    "        color:white !important;\r" +
    "\n" +
    "        font-size:16px !important;\r" +
    "\n" +
    "        font-family: 'Dosis', sans-serif !important;\r" +
    "\n" +
    "        margin-top:35px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "}\t\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media(max-width:600px){\r" +
    "\n" +
    ".c1\r" +
    "\n" +
    "{\r" +
    "\n" +
    "float:left;\r" +
    "\n" +
    "\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".da_tinggi_tabel\r" +
    "\n" +
    "{\r" +
    "\n" +
    "\theight:100px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".headDiv\r" +
    "\n" +
    "{\r" +
    "\n" +
    "    width:100%;\r" +
    "\n" +
    "    border:1px soild blue;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .w50\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:50%;\r" +
    "\n" +
    "        height:50px;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.w50_2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:50%;\r" +
    "\n" +
    "        height:25px;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.w40\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:40%;\r" +
    "\n" +
    "        height:50px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.w20\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:20%;\r" +
    "\n" +
    "        height:50px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.w20_2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:20%;\r" +
    "\n" +
    "        height:25px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.w2\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:100%;\r" +
    "\n" +
    "        height:25px;\r" +
    "\n" +
    "\t\tborder-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .w1\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:100%;\r" +
    "\n" +
    "        height:50px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "        border-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.w1\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        width:100%;\r" +
    "\n" +
    "        height:50px;\r" +
    "\n" +
    "\t\tborder-top:1px solid;\r" +
    "\n" +
    "        border-left:1px solid;\r" +
    "\n" +
    "\t\tborder-bottom:1px solid;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakTotalAchievementAtas\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 16px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakTotalAchievementKiri\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 13px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakTotalAchievementKanan\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 12px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakAchievementAtas\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 7px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakAchievementKiriAtas\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 6px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakAchievementKiriBawah\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 8px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.KotakAchievementKanan\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "\t\tfont-size: 6px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".warna_merah\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tbackground-color:red !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    ".warna_kuning\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tbackground-color:yellow !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    ".warna_hijau\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tbackground-color:#34EB14 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    ".warna_abu_abu\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tbackground-color:#e8eaed !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    ".warna_coklat\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tbackground-color:#FFD966 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t\r" +
    "\n" +
    ".warna_last_baru\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "\t\tbackground-color:#FFC000 !important;\r" +
    "\n" +
    "    }\t\r" +
    "\n" +
    "\r" +
    "\n" +
    ".warna_birulangit\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        background-color: #4fc3f7 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "#chartdivpie3d {\r" +
    "\n" +
    "  width: 100%;\r" +
    "\n" +
    "  height: 500px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "#chartdiv3 {\r" +
    "\n" +
    "  width: 100%;\r" +
    "\n" +
    "  height: 500px;\r" +
    "\n" +
    "  font-size: 11px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "#charts {\r" +
    "\n" +
    "  width: 100%;\r" +
    "\n" +
    "  height: 500px;\r" +
    "\n" +
    "  position: relative;\r" +
    "\n" +
    "  margin: 0 auto;\r" +
    "\n" +
    "  font-size: 8px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".chartdiv {\r" +
    "\n" +
    "    width       : 100%;\r" +
    "\n" +
    "    height      : 500px;\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  top: 0;\r" +
    "\n" +
    "  left: 0;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "#legenddiv {\r" +
    "\n" +
    "  width: 440px;\r" +
    "\n" +
    "\r" +
    "\n" +
    "  height: 200px;\r" +
    "\n" +
    "  font-size: 11px;\r" +
    "\n" +
    "  border: 2px dotted #ccc;\r" +
    "\n" +
    "\r" +
    "\n" +
    "  position: relative;\r" +
    "\n" +
    "  margin: 0 auto;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "#chartdivbatang {\r" +
    "\n" +
    "\twidth: 100%;\r" +
    "\n" +
    "\theight: 500px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* tooltip */\r" +
    "\n" +
    ".tooltipCustom {\r" +
    "\n" +
    "  position: relative;\r" +
    "\n" +
    "  padding: 12px 24px;\r" +
    "\n" +
    "  color: black;\r" +
    "\n" +
    "  transition: all 0.1s ease;\r" +
    "\n" +
    "  text-decoration: none;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".tooltipCustom:after {\r" +
    "\n" +
    "  text-transform: none;\r" +
    "\n" +
    "  content: attr(data-tooltip);\r" +
    "\n" +
    "  font-size: 14px;\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  color: #fff;\r" +
    "\n" +
    "  background: #282828;\r" +
    "\n" +
    "  padding: 0.1vw 2vw;\r" +
    "\n" +
    "  width: -webkit-max-content;\r" +
    "\n" +
    "  width: -moz-max-content;\r" +
    "\n" +
    "  width: max-content;\r" +
    "\n" +
    "  max-width: 200px;\r" +
    "\n" +
    "  opacity: 0;\r" +
    "\n" +
    "  pointer-events: none;\r" +
    "\n" +
    "  top:-18vw;\r" +
    "\n" +
    "  left:10vw !important;\r" +
    "\n" +
    "  border-radius: 4px;\r" +
    "\n" +
    "  transform: translate3d(-50%, 0%, 0);\r" +
    "\n" +
    "  transition: all 0.3s ease;\r" +
    "\n" +
    "  transition-delay: 0.1s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".tooltipCustom:hover:before, .tooltipCustom:hover:after {\r" +
    "\n" +
    "  opacity: 1;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".tooltipCustom:hover:before {\r" +
    "\n" +
    "  transform: translate3d(-50%, calc(-100% - 18px), 0);\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".tooltipCustom:hover:after {\r" +
    "\n" +
    "  transform: translate3d(-50%, calc(-100% - 16px), 0);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* end tooltip */\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* custom css by galang */\r" +
    "\n" +
    ".container_circle{\r" +
    "\n" +
    "\twidth:330px;\r" +
    "\n" +
    "\theight:330px;\r" +
    "\n" +
    "    margin:25px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".circleDiagram{\r" +
    "\n" +
    "\twidth:100%;\r" +
    "\n" +
    "\theight:100%;\r" +
    "\n" +
    "\tbackground: white;\r" +
    "\n" +
    "\tcolor:black;\r" +
    "\n" +
    "\tborder-radius: 50%;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".warnaAsli{\r" +
    "\n" +
    "\twidth:100%;\r" +
    "\n" +
    "\theight:100%;\r" +
    "\n" +
    "\tcolor:black;\r" +
    "\n" +
    "\tborder-radius: 50%;\r" +
    "\n" +
    "    padding:15%;\r" +
    "\n" +
    "    \r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".circleDiagram div{\r" +
    "\n" +
    "\tpadding-top:30%;\r" +
    "\n" +
    "\tfont-size:2.4em;\r" +
    "\n" +
    "\tfont-family: 'Dosis', sans-serif !important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    ".circleDiagram div span{\r" +
    "\n" +
    "\tfont-size:1.7em;\r" +
    "\n" +
    "\tfont-family: 'Dosis', sans-serif !important;\r" +
    "\n" +
    "    line-height:1;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    ".kotak{\r" +
    "\n" +
    "\twidth:50%;\r" +
    "\n" +
    "\theight:43px;\r" +
    "\n" +
    "\tfont-size:2em;\r" +
    "\n" +
    "\tfont-family: 'Dosis', sans-serif !important;\r" +
    "\n" +
    "\tmargin-top:26%;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".hvr-grow {\r" +
    "\n" +
    "  display: inline-block;\r" +
    "\n" +
    "  vertical-align: middle;\r" +
    "\n" +
    "  -webkit-transform: perspective(1px) translateZ(0);\r" +
    "\n" +
    "  transform: perspective(1px) translateZ(0);\r" +
    "\n" +
    "  box-shadow: 0 0 1px rgba(0, 0, 0, 0);\r" +
    "\n" +
    "  -webkit-transition-duration: 0.3s;\r" +
    "\n" +
    "  transition-duration: 0.3s;\r" +
    "\n" +
    "  -webkit-transition-property: transform;\r" +
    "\n" +
    "  transition-property: transform;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hvr-grow:hover, .hvr-grow:focus, .hvr-grow:active {\r" +
    "\n" +
    "  -webkit-transform: scale(1.1);\r" +
    "\n" +
    "  transform: scale(1.1);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* end custom css by galang */</style> <div id=\"content\" class=\"row\"> <section id=\"widget-grid\" data-widget-grid> <div ng-if=\"bitData[1].length>0\" ng-include=\"'app_dashboard/gr/workshopUnitEntry.html'\"></div> <div ng-if=\"bitData[2].length>0\" ng-include=\"'app_dashboard/gr/appointmentPerformance.html'\"></div> <div ng-if=\"bitData[4].length>0\" ng-include=\"'app_dashboard/gr/averageLeadTime.html'\"></div> <div ng-if=\"bitData[8].length>0\" ng-include=\"'app_dashboard/gr/workshopPerformance.html'\"></div> <div ng-if=\"bitData[16].length>0\" ng-include=\"'app_dashboard/gr/progressToTarget.html'\"></div> <div ng-if=\"bitData[32].length>0\" ng-include=\"'app_dashboard/gr/psfuPerformance.html'\"></div> <div ng-if=\"bitData[64].length>0\" ng-include=\"'app_dashboard/gr/groupPerformance.html'\"></div> <div ng-if=\"bitData[128].length>0\" ng-include=\"'app_dashboard/gr/mrsPerformance.html'\"></div> <div ng-if=\"bitData[256].length>0\" ng-include=\"'app_dashboard/gr/partsmanPerformance.html'\"></div> <div ng-if=\"bitData[512].length>0\" ng-include=\"'app_dashboard/gr/saPerformance.html'\"></div> <div ng-if=\"bitData[524288].length>0\" ng-include=\"'app_dashboard/sales_dashboard/prospectMonitoring.html'\"></div> <div ng-if=\"bitData[524288].length>0\" ng-include=\"'app_dashboard/sales_dashboard/prospectMonitoringModel.html'\"></div> <div ng-if=\"bitData[1048576].length>0\" ng-include=\"'app_dashboard/sales_dashboard/salesTarget.html'\"></div> <div ng-if=\"bitData[1048576].length>0\" ng-include=\"'app_dashboard/sales_dashboard/salesTargetModel.html'\"></div> <div ng-if=\"bitData[2097152].length>0\" ng-include=\"'app_dashboard/sales_dashboard/spkBoard.html'\"></div> <div ng-if=\"bitData[2097152].length>0\" ng-include=\"'app_dashboard/sales_dashboard/rsBoard.html'\"></div> <div ng-if=\"bitData[8388608].length>0\" ng-include=\"'app_dashboard/sales_dashboard/spkOutstanding.html'\"></div> <div ng-if=\"bitData[8388608].length>0\" ng-include=\"'app_dashboard/sales_dashboard/spkOutstandingDetail.html'\"></div> <div ng-if=\"bitData[16777216].length>0\" ng-include=\"'app_dashboard/sales_dashboard/arOverdue.html'\"></div> <div ng-if=\"bitData[33554432].length>0\" ng-include=\"'app_dashboard/gr/progressToTargetSa.html'\"></div> <div ng-if=\"bitData[4194304].length>0\" ng-include=\"'app_dashboard/sales_dashboard/stockDashboard.html'\"></div> <div ng-if=\"bitData[1024].length>0\" ng-include=\"'app_dashboard/bp/workshopUnitEntry.html'\"></div> <div ng-if=\"bitData[2048].length>0\" ng-include=\"'app_dashboard/bp/partsmanPerformance.html'\"></div> <div ng-if=\"bitData[4096].length>0\" ng-include=\"'app_dashboard/bp/averageLeadTime.html'\"></div> <div ng-if=\"bitData[8192].length>0\" ng-include=\"'app_dashboard/bp/groupPerformance.html'\"></div> <div ng-if=\"bitData[16384].length>0\" ng-include=\"'app_dashboard/bp/psfuPerformance.html'\"></div> <div ng-if=\"bitData[32768].length>0\" ng-include=\"'app_dashboard/bp/saPerformance.html'\"></div> <div ng-if=\"bitData[65536].length>0\" ng-include=\"'app_dashboard/bp/progressToTarget.html'\"></div> <div ng-if=\"bitData[131072].length>0\" ng-include=\"'app_dashboard/bp/appointmentPerformance.html'\"></div> <!-- Workshop Performance BP for KABENG --> <div ng-if=\"bitData[262144].length>0\" ng-include=\"'app_dashboard/bp/workshopPerformance.html'\"></div> <div ng-if=\"bitData[67108864].length>0\" ng-include=\"'app_dashboard/bp/progressToTargetSa.html'\"></div> <div ng-if=\"bitData[268435456].length>0\" ng-include=\"'app_dashboard/bp/groupPerformanceForeman.html'\"></div> <div ng-if=\"bitData[536870912].length>0\" ng-include=\"'app_dashboard/gr/averageLeadTimeSA.html'\"></div> <div ng-if=\"bitData[1073741824].length>0\" ng-include=\"'app_dashboard/bp/averageLeadTimeSA.html'\"></div> <!-- Group Performance GR --> <div ng-if=\"bitData[134217728].length>0\" ng-include=\"'app_dashboard/gr/groupPerformanceGR.html'\"></div> </section> </div>"
  );


  $templateCache.put('app_dashboard/dashboard2.html',
    "<!-- MAIN CONTENT --><!-- <div id=\"content\"> --><!-- ng-controller=\"DashboardCtrl\" --><!-- \t<div class=\"row\">\r" +
    "\n" +
    "\t\t<big-breadcrumbs items=\"['Dashboard', 'My Dashboard']\" class=\"col-xs-12 col-sm-7 col-md-7 col-lg-4\"></big-breadcrumbs>\r" +
    "\n" +
    "\t</div> --><!-- widget grid --><!-- \t<section id=\"widget-grid\" widget-grid>\r" +
    "\n" +
    "\t\t<div class=\"row\">\r" +
    "\n" +
    "\t\t\t<article class=\"col-sm-12\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div smart-include=\"app/themes/dashboard/live-feeds.tpl.html\" class=\"placeholder-live-feeds\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t</article>\r" +
    "\n" +
    "\t\t</div>\r" +
    "\n" +
    "\t</section> --><!-- end widget grid --><!-- </div> --> <div id=\"content\" style=\"padding:0\"> <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px\"> <h2 class=\"page-title txt-color-blueDark\" style=\"width:auto\"> <i class=\"fa fa-fw fa-tachometer\"></i>Dashboard </h2> </div> <div class=\"well clearfix\" style=\"padding:5px\" ng-cloak> DASHBOARD <button ng-click=\"incBadge()\">Inc Badge Count</button> <button ng-click=\"decBadge()\">Dec Badge Count</button> </div> </div> <!-- END MAIN CONTENT -->"
  );


  $templateCache.put('app_dashboard/gr/appointmentPerformance.html',
    "<div ng-controller=\"DashboardAppointmentGR\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header style=\"background-color:#6e6e6e;color:aliceblue\"> <span class=\"widget-icon\"> <i class=\"fa fa-calendar\"></i> </span> <h2><strong>Appointment Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 1000px; margin: auto\"> <div style=\"width: 60%; float: left; margin-left: 1%\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #1874cd\"> <div style=\"font-size: 200%\">{{data1}}%</div> <div>Appointment Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: -40%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #458b00\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data2}}</div> <div>Show Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/arOverdue.html',
    "<div ng-controller=\"DashboardArOverdue\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>AR Overdue</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"text-align: center\"> <div style=\"float:right;margin-right:20px\">TOTAL AR:{{TotalIsichartpie3d}}</div> <div id=\"chartdivpie3d\"></div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/averageLeadTime.html',
    "<div ng-controller=\"AverageLeadTimeGR\" class=\"row\"> <article class=\"col-md-12 col-lg-12\"> <div id=\"widget-avgLeadTime\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-clock-o\"></i> </span> <h2> <strong ng-show=\"user.RoleId == 1128\">Today's Workshop Average Lead Time</strong> <strong ng-show=\"user.RoleId !== 1128\">Today's Workshop Production Average Lead Time</strong> </h2> </header> <div ng-show=\"user.RoleId == 1128  \" class=\"widget-body\" style=\"text-align: center\"> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Average Lead Time</label> <div class=\"time-panel color7\"> {{data1}} </div><br> <b>Target Average Lead Time</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data2}}</span> </div> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Average Lead Time EM</label> <div class=\"time-panel color8\"> {{data3}} </div><br> <b>Target Average Lead Time EM</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data4}}</span> </div> </div> <div ng-show=\"user.RoleId !== 1128\" class=\"widget-body\" style=\"text-align: center\"> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Average Production Lead Time</label> <div class=\"time-panel color7\"> {{data1}} </div><br> <b>Target Average Lead Time</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data2}}</span> </div> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Average Production Lead Time EM</label> <div class=\"time-panel color8\"> {{data3}} </div><br> <b>Target Average Lead Time EM</b><br> <span style=\"color:#5880b3; font-weight:600\">{{data4}}</span> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/averageLeadTimeSA.html',
    "<div ng-controller=\"AverageLeadTimeSAGR\" class=\"row\"> <article class=\"col-md-12 col-lg-12\"> <div id=\"widget-avgLeadTime\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header style=\"background-color:#6e6e6e;color:aliceblue\"> <span class=\"widget-icon\"> <i class=\"fa fa-clock-o\"></i> </span> <h2><strong>Today's Reception Average Lead Time</strong></h2> </header> <div class=\"widget-body\" style=\"text-align: center\"> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Your Average Lead Time Reception</label> <div class=\"time-panel color1\" style=\"background-color:#6e6e6e\"> {{data1}} </div><br> <!-- <b>Target Average Lead Time<br>\r" +
    "\n" +
    "                        {{data2}}</b> --> </div> <div class=\"col-md-6 col-lg-6\"> <label style=\"color:#808080; font-weight: bolder;font-size:1.1em\">Average Lead Time Waiting for Reception (ALL SA)</label> <div class=\"time-panel color2\" style=\"background-color:#9bec5d\"> {{data3}} </div><br> <b>Target Average Lead Time Waiting for Reception</b><br> <div style=\"color:#3168e0\"> {{data4}} </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/groupPerformance.html',
    "<div ng-controller=\"DashboardGroupGR\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-group-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-users\"></i> </span> <h2 ng-if=\"user.RoleId == 1031\"><strong>All Group Performance</strong></h2> <h2 ng-if=\"user.RoleId !== 1031\"><strong>Group Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"margin: auto; display: flex;align-items: center\"> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #508abc\"> <div style=\"font-size: 200%\">{{data1}}%</div> <div>Stall Capacity</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color2\"> <div style=\"font-size: 200%\">{{data3}}</div> <div style=\"font-size: 90%\">Repair Unit Mechanic<br>(RUM)</div> </div> </div> <div class=\"row\" style=\"text-align: center;padding-top: 23px\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color3\"> <div style=\"font-size: 200%\">{{data4}}</div> <div style=\"font-size: 90%\">Repair Per Stall<br>(RPS)</div> </div> </div> <div class=\"row\" style=\"text-align: center;padding-top: 23px\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data7\"> <span class=\"percent\" style=\"margin-top: -3em; color: #508abc\"> <div style=\"font-size: 200%\">{{data7}}%</div> <div>Labour Utility<br>(LU)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> <div class=\"row\" style=\"max-width: 800px; margin: auto\"> <div class=\"col-md-4 col-lg-4\"> <div class=\"row\" style=\"height: 170px; overflow: hidden\"> <div style=\"height: 220px\"> <amchart ng-model=\"gaugeOptions1\" width=\"100%\"></amchart> </div> </div> <div class=\"row\" style=\"text-align: center;font-weight: bolder\">Technician Efeciency (TE)</div> <div class=\"row\" style=\"text-align: center\">M to H-1</div> </div> <div class=\"col-md-4 col-lg-4\" style=\"height: 220px; padding-top: 30px\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #92d14f\"> <div style=\"font-size: 200%\">{{data2}}%</div> <div>Non FIR<br>(Q1)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div class=\"col-md-4 col-lg-4\"> <div class=\"row\" style=\"height: 170px; overflow: hidden\"> <div style=\"height: 220px\"> <amchart ng-model=\"gaugeOptions2\" width=\"100%\"></amchart> </div> </div> <div class=\"row\" style=\"text-align: center; font-weight: bolder\">Overall Productivity (OP)</div> <div class=\"row\" style=\"text-align: center\">M to H-1</div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/groupPerformanceGR.html',
    "<div ng-controller=\"groupPerformanceGRController\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-group-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-users\"></i> </span> <h2><strong>Group Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 1000px; margin: auto; display: flex;align-items: center\"> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #508abc\"> <div style=\"font-size: 200%\">{{data1}}%</div> <div>Stall Capacity</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color1\"> <div style=\"font-size: 200%\">{{data3}}</div> <div style=\"font-size: 90%\">Repair Unit Mechanic<br>(RUM)</div> </div> </div> <div class=\"row\" style=\"text-align: center;padding-top: 23px\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color1\"> <div style=\"font-size: 200%\">{{data4}}</div> <div style=\"font-size: 90%\">Repair Per Stall<br>(RPS)</div> </div> </div> <div class=\"row\" style=\"text-align: center;padding-top: 23px\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 25%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #508abc\"> <div style=\"font-size: 200%\">{{data2}}%</div> <div>Non FIR<br>(Q1)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> <div class=\"row\" style=\"max-width: 800px; margin: auto\"> <div class=\"col-md-4 col-lg-4\"> <div class=\"row\" style=\"overflow: hidden\"> <div id=\"chart1\" style=\"min-height:220px; position: relative; height: 220px\"> <amchart ng-model=\"gaugeOptions1\" width=\"100%\"></amchart> </div> </div> <div class=\"row\" style=\"text-align: center;font-weight: bolder\">Technician Efeciency (TE)</div> <div class=\"row\" style=\"text-align: center\">M to H-1</div> </div> <div class=\"col-md-4 col-lg-4\" style=\"min-height:220px; position: relative; height: 220px; padding-top: 30px\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options\" percent=\"data6\"> <span class=\"percent\" style=\"margin-top: -3em; color: #508abc\"> <div style=\"font-size: 200%\">{{data7}}%</div> <div>Labour Utility<br>(LU)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div class=\"col-md-4 col-lg-4\"> <div id=\"chart2\" class=\"row\" style=\"overflow: hidden\"> <div style=\"min-height:220px; position: relative; height: 220px\"> <amchart ng-model=\"gaugeOptions2\" width=\"100%\"></amchart> </div> </div> <div class=\"row\" style=\"text-align: center; font-weight: bolder\">Overall Productivity (OP)</div> <div class=\"row\" style=\"text-align: center\">M to H-1</div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/mrsPerformance.html',
    "<div ng-controller=\"MRSPerformance\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-mrs-grMRSPerform\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header style=\"background-color:#6e6e6e;color:aliceblue\"> <span class=\"widget-icon\"> <i class=\"fa fa-phone\"></i> </span> <h2><strong>MRS Performance</strong></h2> </header> <div class=\"widget-body\"> <!-- <div class=\"row\" style=\"max-width: 800px; margin: auto;\"> --> <div style=\"width: 16%; float: left; margin-left: 1%\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #458b00\"> <div style=\"font-size: 200%\">{{data1}}%</div> <div>Call Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> H </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #458b00\"> <div style=\"font-size: 200%\">{{data2}}%</div> <div>Success Call Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> H </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data3\"> <span class=\"percent\" style=\"margin-top: -3em; color: #1874cd\"> <div style=\"font-size: 200%\">{{data3}}%</div> <div>Kontribusi MRS</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M-1 </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #d2691e\"> <div style=\"font-size: 200%\">{{data4}}%</div> <div>Kontribusi<br>MRS->SBE</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M-1 </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #1874cd\"> <div style=\"font-size: 200%\">{{data9}}%</div> <div>BOC</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M-1 </div> </div> <div style=\"width: 16%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #d2691e\"> <div style=\"font-size: 200%\">{{data10}}%</div> <div>BOC SHOW</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M-1 </div> </div> <!-- </div> --> </div> </div> </article> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-mrs-grMRSTarget\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header style=\"background-color:#6e6e6e;color:aliceblue\"> <span class=\"widget-icon\"> <i class=\"fa fa-phone\"></i> </span> <h2><strong>MRS Progress to Target (Reminder)</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"width: 100%; margin: auto\"> <div class=\"col-md-4\" style=\"padding-top: 80px\"> <div class=\"row\" style=\"border-radius: 10px; border: solid 1px #160b03; font-size: 120%; padding: 10px\"> <div class=\"col-md-9\"> <b><div style=\"font-size: 150%;font-weight:bold\">Today</div><br><div style=\"font-size:150%;font-weight: bold\">Yesterday</div></b> </div> <div class=\"col-md-3\"> <b><div style=\"color:#458b00;font-size: 150%;font-weight: bold\">{{data5}}</div><br><div style=\"color:#1874cd;font-size: 150%;font-weight: bold\">{{data6}}</div></b> </div> </div> </div> <div class=\"col-md-4\" style=\"height: 300px; padding: 10px\"> <div class=\"row\" style=\"height: 250px; overflow: hidden\"> <!-- <div style=\"height: 220px\"> --> <amchart ng-model=\"gaugeOptions\" width=\"100%\"></amchart> <!-- </div> --> </div> <div class=\"row\" style=\"text-align: center\">Actual vs Target</div> </div> <div class=\"col-md-4\" style=\"padding-top: 80px; text-align: center\"> <b style=\"font-size: 200%;color:coral\">{{data8}}</b> <b style=\"font-size: 200%;color:coral\">DAYS</b> <br><b style=\"font-size: 150%\"> Left till End of Month </b> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/partsmanPerformance.html',
    "<div ng-controller=\"PartsmanGR\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-partsman-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-wrench\"></i> </span> <h2><strong>Partsman Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 800px; margin: auto\"> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -3em; color: #5b9435\"> <div style=\"font-size: 200%\">{{data1}}%</div> <div>Fill Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> H </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #d36e2a\"> <div style=\"font-size: 200%\">{{data2}}%</div> <div>Immediate Fill Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> H </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"circle-panel color2\"> <div style=\"font-size: 200%\">{{data3}}</div> <div style=\"font-size: 90%\">Stock Day</div> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color: #d36e2a\"> <div style=\"font-size: 200%\">{{data4}}%</div> <div>Service Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> H </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options4\" percent=\"data5\"> <span class=\"percent\" style=\"margin-top: -3em; color: #508abc\"> <div style=\"font-size: 200%\">{{data5}}%</div> <div>Back Order Performance</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> H </div> </div> </div> <div class=\"row\" style=\"max-width: 600px; margin: auto\"> </div> </div> </div> </article> <article class=\"col-sm-12 col-md-6 col-lg-6\"> <div id=\"widget-partsman-fillrate-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Fill Rate</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"barOptionsFillRate\" width=\"100%\"></amchart> </div> </div> </article> <article class=\"col-sm-12 col-md-6 col-lg-6\"> <div id=\"widget-partsman-imfillrate-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Immediate Fill Rate</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"barOptionsImFillRate\" width=\"100%\"></amchart> </div> </div> </article> <article class=\"col-sm-12 col-md-6 col-lg-6\"> <div id=\"widget-partsman-service-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Service Rate</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"barOptionsSvcRate\" width=\"100%\"></amchart> </div> </div> </article> <article class=\"col-sm-12 col-md-6 col-lg-6\"> <div id=\"widget-partsman-stock-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Stock Day</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"barOptionsStockDay\" width=\"100%\"></amchart> </div> </div> </article> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-partsman-composition-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-bar-chart\"></i> </span> <h2><strong>Komposisi Order</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; padding: 0px\"> <amchart ng-model=\"multilineOptions\" width=\"100%\"></amchart> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/progressToTarget.html',
    "<style>.amcharts-chart-div a {display:none !important;}</style> <div ng-controller=\"ProgressToTargetGR\" class=\"row\"> <article class=\"col-lg-12\"> <div id=\"widget-progress-to-target\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"><i class=\"fa fa-line-chart\"></i></span> <h2><strong>Workshop Progress To Target</strong></h2> </header> <div class=\"widget-body\" style=\"height: 300px; overflow: hidden\"> <div class=\"col-md-4 col-lg-4\" style=\"height: 300px; padding: 0px\"> <div class=\"row\" style=\"text-align: center; font-weight: bolder; font-size: 1.1em\">Unit CPUS vs Target</div> <div class=\"row\" style=\"height: 200px; overflow: hidden\"> <amchart ng-model=\"gaugeOptions1\" width=\"100%\"></amchart> </div> <div class=\"row\" style=\"background-color: #4e4d4d; border-radius: 20px; color: white; margin-left: 10%; width: 80%; padding: 10px\"> <div class=\"col-md-6\" style=\"text-align: center;border-right:1px solid #fff\"> Unit Entry<br> {{data1+data2}} </div> <div class=\"col-md-6\" style=\"text-align: center\"> Unit Non CPUS<br> {{data2}} </div> </div> </div> <div class=\"col-md-4 col-lg-4\" style=\"padding-top: 50px\"> <div class=\"row\"> <div class=\"col-sm-4\" style=\"text-align: left\"> <b>Unit CPUS</b> </div> <div class=\"col-sm-3\"> </div> <div class=\"col-sm-5\" style=\"text-align: right\"> <b>Revenue CPUS</b> </div> </div> <div class=\"row\" style=\"border-radius: 10px; border: solid 1px #555; font-size: 120%\"> <div class=\"col-md-3\" style=\"text-align: right\"> <b style=\"color: coral\">{{data5.toLocaleString()}}</b><br><b style=\"color: greenyellow\">{{data6.toLocaleString()}}</b> </div> <div class=\"col-md-4\" style=\"text-align: center\"> <b>Yesterday<br>Today</b> </div> <div class=\"col-md-5\" style=\"text-align: right\"> <b style=\"color: coral\">{{data7.toLocaleString()}}</b><br><b style=\"color: greenyellow\">{{data8.toLocaleString()}}</b> </div> </div> <div class=\"row\" style=\"text-align: center\"> <b style=\"font-size: 150%;color: coral\">{{data9}} Days</b><br> <b>Left till End of Month</b> </div> </div> <div class=\"col-md-4 col-lg-4\" style=\"height: 300px; padding: 0px\"> <div class=\"row\" style=\"text-align: center; font-weight: bolder; font-size: 1.1em\">Revenue CPUS vs Target</div> <div style=\"height: 200px; overflow: hidden\"> <!-- <div style=\"height: 320px\"> --> <amchart ng-model=\"gaugeOptions2\" width=\"100%\"></amchart> <!-- </div> --> </div> <div class=\"row\" style=\"background-color: #4e4d4d; border-radius: 20px; color: white; margin-left: 5%; width: 90%; padding: 10px\"> <div class=\"col-md-6\" style=\"text-align: center; border-right:1px solid #fff\"> Revenue Amount<br> {{(data3+data4).toLocaleString()}} </div> <div class=\"col-md-6\" style=\"text-align: center\"> Amount Non CPUS<br> {{data4.toLocaleString()}} </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/progressToTargetSa.html',
    "<style>.amcharts-chart-div a {display:none !important;}\r" +
    "\n" +
    "    .chartWrapper{\r" +
    "\n" +
    "        /* width: 90%; */\r" +
    "\n" +
    "        margin: auto;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        /* padding-bottom: 50%; */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .chartnya{\r" +
    "\n" +
    "            height:200px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .chartnya .subchart{\r" +
    "\n" +
    "        height:200px;\r" +
    "\n" +
    "    }</style> <div ng-controller=\"ProgressToTargetSaGR\" class=\"row\"> <article class=\"col-lg-12\"> <div id=\"widget-progress-to-target\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header style=\"background-color:#6e6e6e;color:aliceblue\"> <span class=\"widget-icon\"><i class=\"fa fa-line-chart\"></i></span> <h2><strong>Your Progress To Target</strong></h2> </header> <div class=\"widget-body\" style=\"color:#000\"> <div class=\"row chartWrapper\"> <div class=\"col-md-12\" style=\"margin: auto; text-align: center\"> <div class=\"col-lg-4 chartnya\"> <div class=\"row subchart\"> <!-- <div style=\"height: 220px\"> --> <amchart ng-model=\"gaugeOptions1\" width=\"100%\"></amchart> <!-- </div> --> </div> <div class=\"row\" style=\"text-align: center;font-weight: bolder;font-size:1.1em;color:#808080\">Unit Entry vs Target</div> </div> <div class=\"col-lg-4\" style=\"padding-top: 50px;margin-top:3%\"> <div class=\"row\"> <div class=\"col-md-3\" style=\"text-align: right\"> <b>Unit(In)/Delivery(Out)</b> </div> <div class=\"col-md-4\"> </div> <div class=\"col-md-5\" style=\"text-align: right\"> <b>Revenue</b> </div> </div> <div class=\"row\" style=\"border-radius: 15px; border: solid 1px #555; font-size: 120%\"> <div class=\"col-md-3\" style=\"text-align: left\"> <b style=\"color: coral\">{{data4.toLocaleString()}}/{{data3.toLocaleString()}}</b><br><br><b style=\"color: lightgreen\">{{data2.toLocaleString()}}/{{data1.toLocaleString()}}</b> </div> <div class=\"col-md-5\" style=\"text-align: center\"> <b>Today<br><br>Yesterday</b> </div> <div class=\"col-md-4\" style=\"text-align: right\"> <b style=\"color: coral\">{{data8.toLocaleString()}}</b><br><br><b style=\"color: lightgreen\">{{data9.toLocaleString()}}</b> </div> </div> <div class=\"row\" style=\"text-align: center\"> <b style=\"font-size: 200%;color:coral\">{{data6.toLocaleString()}}</b> <b style=\"font-size: 200%;color:coral\">DAYS</b> <br><b style=\"font-size: 150%\"> Left till End of Month </b> <!-- <b style=\"font-size: 150%\">{{daysLeft}}</b><br>\r" +
    "\n" +
    "                                <b>Left till End of Month</b> --> </div> </div> <div class=\"col-lg-4 chartnya\"> <div class=\"row subchart\"> <!-- <div style=\"height: 220px\"> --> <amchart ng-model=\"gaugeOptions2\" width=\"100%\"></amchart> <!-- </div> --> </div> <div class=\"row\" style=\"text-align: center;font-weight: bolder;font-size:1.1em;color:#808080\">Revenue vs Target</div> </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/prospectMonitoring.html',
    "<div ng-controller=\"DashboardProspectMonitoring\"> <article class=\"col-sm-6 col-md-6 col-lg-5\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Prospect Monitoring (Prospect)</strong></h2> </header> <div class=\"widget-body\"> <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> <div class=\"row\" style=\"text-align: center\"> <div id=\"chartdivProspectWithLabel1\"></div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> </div> </div> <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> <div class=\"row\" style=\"text-align: center\"> <div id=\"chartdivProspectWithLabel2\"></div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/prospectMonitoringModel.html',
    "<div ng-controller=\"DashboardProspectMonitoringModel\"> <article class=\"col-sm-6 col-md-6 col-lg-5\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Prospect Monitoring {{ProspectMonitoringNamaModel}}</strong></h2> </header> <div class=\"widget-body\"> <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> <div class=\"row\" style=\"text-align: center\"> <div id=\"chartdivProspectWithLabel1_2\"></div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> </div> </div> <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> <div class=\"row\" style=\"text-align: center\"> <div id=\"chartdivProspectWithLabel2_2\"></div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/psfuPerformance.html',
    "<div ng-controller=\"PSFUGR\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-psfu-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-phone\"></i> </span> <h2><strong>PSFU Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 800px; margin: auto\"> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <span class=\"percent\" style=\"margin-top: -2em; color: #d36d27\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data1}}</div> <div>Follow Up Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -2em; color: #508abb\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data2}}</div> <div>Fix It Rate (FIR)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data3\"> <span class=\"percent\" style=\"margin-top: -2em; color: #508abb\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data3}}</div> <div>Q1</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options4\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -2em; color: #62993e\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data4}}</div> <div>Q2</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options5\" percent=\"data5\"> <span class=\"percent\" style=\"margin-top: -2em; color: #d36e2a\"> <div class=\"percent-sign\" style=\"font-size: 2em\">{{data5}}</div> <div>Q3</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/rsBoard.html',
    "<div ng-controller=\"DashboardRsBoard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>RS</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row da_tinggi_tabel\" style=\"text-align: center\"> <div class=\"headDiv\"> <div class=\"c1 w40\"> <div class=\"c1 w1\" ng-class=\"ach_total_rs\"> <div class=\"KotakTotalAchievementAtas\">Achievement</div> <div class=\"KotakTotalAchievementAtas\">{{BoardRsTotalAchievement}} ({{BoardRsTotalAchievementPercent}}&#37;)</div> </div> <div class=\"c1 w50\" ng-class=\"warna_target_rs\"> <div class=\"KotakTotalAchievementKiri\">Target RS</div> <div class=\"KotakTotalAchievementKiri\">{{BoardRsTargetRs}}</div> </div> <div class=\"c1 w50\" ng-class=\"ach_total_last_rs\"> <div class=\"KotakTotalAchievementKanan\">ACH</div> <div class=\"KotakTotalAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakTotalAchievementKanan\">{{BoardRsTargetRsLast}} ({{BoardRsTargetRsLastPercentage}}&#37;)</div> </div> </div> <div class=\"c1 w20\"> <div class=\"c1 w2\" ng-class=\"ach_total_t1_rs\"> <div class=\"KotakAchievementAtas\">ACH {{BoardRsCat1}}</div> <div class=\"KotakAchievementAtas\">{{BoardRsAch1}} ({{BoardRsAch1Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_rs\"> <div class=\"KotakAchievementKiriAtas\">Target RS</div> <div class=\"KotakAchievementKiriBawah\">{{BoardRsAch1Target}}</div> </div> <div class=\"c1 w50_2\" ng-class=\"ach_total_t1_last_rs\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardRsAch1Last}}</div> </div> <div class=\"c1 w2\" ng-class=\"ach_total_t2_rs\"> <div class=\"KotakAchievementAtas\">ACH {{BoardRsCat4}}</div> <div class=\"KotakAchievementAtas\">{{BoardRsAch4}} ({{BoardRsAch4Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_rs\"> <div class=\"KotakAchievementKiriAtas\">Target RS</div> <div class=\"KotakAchievementKiriBawah\">{{BoardRsAch4Target}}</div> </div> <div class=\"c1 w50_2\" ng-class=\"ach_total_t2_last_rs\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardRsAch4Last}}</div> </div> </div> <div class=\"c1 w20\"> <div class=\"c1 w2\" ng-class=\"ach_total_t3_rs\"> <div class=\"KotakAchievementAtas\">ACH {{BoardRsCat2}}</div> <div class=\"KotakAchievementAtas\">{{BoardRsAch2}} ({{BoardRsAch2Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_rs\"> <div class=\"KotakAchievementKiriAtas\">Target RS</div> <div class=\"KotakAchievementKiriBawah\">{{BoardRsAch2Target}}</div> </div> <div class=\"c1 w50_2\" ng-class=\"ach_total_t3_last_rs\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardRsAch2Last}}</div> </div> <div class=\"c1 w2\" ng-class=\"ach_total_t4_rs\"> <div class=\"KotakAchievementAtas\">ACH {{BoardRsCat5}}</div> <div class=\"KotakAchievementAtas\">{{BoardRsAch5}} ({{BoardRsAch5Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_rs\"> <div class=\"KotakAchievementKiriAtas\">Target RS</div> <div class=\"KotakAchievementKiriBawah\">{{BoardRsAch5Target}}</div> </div> <div class=\"c1 w50_2\" ng-class=\"ach_total_t4_last_rs\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardRsAch5Last}}</div> </div> </div> <div class=\"c1 w20\"> <div class=\"c1 w2\" style=\"border-right:1px solid\" ng-class=\"ach_total_t5_rs\"> <div class=\"KotakAchievementAtas\">ACH {{BoardRsCat3}}</div> <div class=\"KotakAchievementAtas\">{{BoardRsAch3}} ({{BoardRsAch3Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_rs\"> <div class=\"KotakAchievementKiriAtas\">Target RS</div> <div class=\"KotakAchievementKiriBawah\">{{BoardRsAch3Target}}</div> </div> <div class=\"c1 w50_2\" style=\"border-right:1px solid\" ng-class=\"ach_total_t5_last_rs\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardRsAch3Last}}</div> </div> <div class=\"c1 w2\" style=\"border-right:1px solid\" ng-class=\"ach_total_t6_rs\"> <div class=\"KotakAchievementAtas\">ACH {{BoardRsCat6}}</div> <div class=\"KotakAchievementAtas\">{{BoardRsAch6}} ({{BoardRsAch6Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_rs\"> <div class=\"KotakAchievementKiriAtas\">Target RS</div> <div class=\"KotakAchievementKiriBawah\">{{BoardRsAch6Target}}</div> </div> <div class=\"c1 w50_2\" style=\"border-right:1px solid\" ng-class=\"ach_total_t6_last_rs\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardRsAch6Last}}</div> </div> </div> </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/saPerformance.html',
    "<div ng-controller=\"SAGR\" class=\"row\"> <article class=\"col-sm-12 col-md-12 col-lg-12\"> <div id=\"widget-sa-gr\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header style=\"background-color:#6e6e6e;color:aliceblue\"> <span class=\"widget-icon\"> <i class=\"fa fa-phone\"></i> </span> <h2><strong>SA Performance</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"max-width: 1000px; margin: auto\"> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\" style=\"color:#d2691e\"> <span class=\"percent\" style=\"margin-top: -3em; color: #d2691e\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data1}}</div> <div>On Time Delivery<br>(OTD)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <span class=\"percent\" style=\"margin-top: -3em; color: #1874cd\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data2}}</div> <div>First Promised (OTD)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data3\"> <span class=\"percent\" style=\"margin-top: -3em; color: #458b00\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data3}}</div> <div>Return Job Rate (RTJ)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options4\" percent=\"data4\"> <span class=\"percent\" style=\"margin-top: -3em; color:#1874cd\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data4}}</div> <div>Fix It Rate (FIR)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 20%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options5\" percent=\"data5\"> <span class=\"percent\" style=\"margin-top: -3em; color: #458b00\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data5}}</div> <div>Delivery Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/salesTarget.html',
    "<div ng-controller=\"DashboardSalesTarget\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Sales Target</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"text-align: center\"> <div id=\"chartstackedbarSalesTarget\"></div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/salesTargetModel.html',
    "<div ng-controller=\"DashboardSalesTargetModel\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Sales Target Model</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"text-align: center\"> <div id=\"chartstackedbarSalesTargetModel\"></div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/spkBoard.html',
    "<div ng-controller=\"DashboardSpkBoard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>SPK</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row da_tinggi_tabel\" style=\"text-align: center\"> <div class=\"headDiv\"> <div class=\"c1 w40\"> <div class=\"c1 w1\" ng-class=\"ach_total_spk\"> <div class=\"KotakTotalAchievementAtas\">Achievement</div> <div class=\"KotakTotalAchievementAtas\">{{BoardSpkTotalAchievement}} ({{BoardSpkTotalAchievementPercent}}&#37;)</div> </div> <div class=\"c1 w50\" ng-class=\"warna_target_spk\"> <div class=\"KotakTotalAchievementKiri\">Target SPK</div> <div class=\"KotakTotalAchievementKiri\">{{BoardSpkTargetSpk}}</div> </div> <div class=\"c1 w50\" ng-class=\"ach_total_last_spk\"> <div class=\"KotakTotalAchievementKanan\">ACH</div> <div class=\"KotakTotalAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakTotalAchievementKanan\">{{BoardSpkTargetSpkLast}} ({{BoardSpkTargetSpkLastPercentage}}&#37;)</div> </div> </div> <div class=\"c1 w20\"> <div class=\"c1 w2\" ng-class=\"ach_total_t1_spk\"> <div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat1}}</div> <div class=\"KotakAchievementAtas\">{{BoardSpkAch1}} ({{BoardSpkAch1Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_spk\"> <div class=\"KotakAchievementKiriAtas\">Target SPK</div> <div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch1Target}}</div> </div> <div class=\"c1 w50_2\" ng-class=\"ach_total_t1_last_spk\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardSpkAch1Last}}</div> </div> <div class=\"c1 w2\" ng-class=\"ach_total_t2_spk\"> <div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat4}}</div> <div class=\"KotakAchievementAtas\">{{BoardSpkAch4}} ({{BoardSpkAch4Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_spk\"> <div class=\"KotakAchievementKiriAtas\">Target SPK</div> <div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch4Target}}</div> </div> <div class=\"c1 w50_2\" ng-class=\"ach_total_t2_last_spk\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardSpkAch4Last}}</div> </div> </div> <div class=\"c1 w20\"> <div class=\"c1 w2\" ng-class=\"ach_total_t3_spk\"> <div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat2}}</div> <div class=\"KotakAchievementAtas\">{{BoardSpkAch2}} ({{BoardSpkAch2Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_spk\"> <div class=\"KotakAchievementKiriAtas\">Target SPK</div> <div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch2Target}}</div> </div> <div class=\"c1 w50_2\" ng-class=\"ach_total_t3_last_spk\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardSpkAch2Last}}</div> </div> <div class=\"c1 w2\" ng-class=\"ach_total_t4_spk\"> <div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat5}}</div> <div class=\"KotakAchievementAtas\">{{BoardSpkAch5}} ({{BoardSpkAch5Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_spk\"> <div class=\"KotakAchievementKiriAtas\">Target SPK</div> <div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch5Target}}</div> </div> <div class=\"c1 w50_2\" ng-class=\"ach_total_t4_last_spk\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardSpkAch5Last}}</div> </div> </div> <div class=\"c1 w20\"> <div class=\"c1 w2\" style=\"border-right:1px solid\" ng-class=\"ach_total_t5_spk\"> <div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat3}}</div> <div class=\"KotakAchievementAtas\">{{BoardSpkAch3}} ({{BoardSpkAch3Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_spk\"> <div class=\"KotakAchievementKiriAtas\">Target SPK</div> <div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch3Target}}</div> </div> <div class=\"c1 w50_2\" style=\"border-right:1px solid\" ng-class=\"ach_total_t5_last_spk\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardSpkAch3Last}}</div> </div> <div class=\"c1 w2\" style=\"border-right:1px solid\" ng-class=\"ach_total_t6_spk\"> <div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat6}}</div> <div class=\"KotakAchievementAtas\">{{BoardSpkAch6}} ({{BoardSpkAch6Percentage}}&#37;)</div> </div> <div class=\"c1 w50_2\" ng-class=\"warna_target_spk\"> <div class=\"KotakAchievementKiriAtas\">Target SPK</div> <div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch6Target}}</div> </div> <div class=\"c1 w50_2\" style=\"border-right:1px solid\" ng-class=\"ach_total_t6_last_spk\"> <div class=\"KotakAchievementKanan\">ACH</div> <div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div> <div class=\"KotakAchievementKanan\">{{BoardSpkAch6Last}}</div> </div> </div> </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/spkOutstanding.html',
    "<div ng-controller=\"spkOutstandingBoard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>SPK Outstanding</strong></h2> </header> <div class=\"widget-body\"> <div id=\"chartdiv3\"></div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/spkOutstandingDetail.html',
    "<div ng-controller=\"spkOutstandingDetailBoard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>SPK Outstanding</strong></h2> </header> <div class=\"widget-body\"> <div id=\"charts\"> <!--<div id=\"chart1\" class=\"chartdiv\"></div>--> <div id=\"chart2\" class=\"chartdiv\"></div> <div id=\"chart3\" class=\"chartdiv\"></div> <div id=\"chart4\" class=\"chartdiv\"></div> </div> <div> <p align=\"center\"> <div id=\"legenddiv\"></div> </p> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/stockDashboard.html',
    "<div ng-controller=\"StockDashboard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Stock</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"text-align: center\"> <!--awal batang--> <div id=\"chartdivbatang\"></div> <!--akhir batang--> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/workshopPerformance.html',
    "<div ng-controller=\"WorkshopPerformanceGR\" class=\"row\"> <article class=\"col-lg-12\"> <div id=\"widget-workshop\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <span class=\"widget-icon\"> <i class=\"fa fa-dashboard\"></i> </span> <h2><strong>Workshop Performance</strong></h2> </header> <div class=\"widget-body\"> <div style=\"width: 14%; float: left; margin-left: 1%\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options1\" percent=\"data1\"> <!-- <span class=\"percent percent-sign\" style=\"font-size: 2em; margin-top: -0.8em; color: #508abc\">{{data1}}</span> --> <span class=\"percent\" style=\"margin-top: -3em; color: #508abc\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data1}}</div> <div>Stall Capacity</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> <!-- Stall Capacity --> M to H-1 </div> </div> <div style=\"width: 14%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options2\" percent=\"data2\"> <!-- <span class=\"percent percent-sign\" style=\"font-size: 2em; margin-top: -0.8em; color: #d36e2a\">{{data2}}</span> --> <span class=\"percent\" style=\"margin-top: -3em; color:  #d36e2a\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data2}}</div> <div>On Time Delivery<br>(OTD)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 14%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options3\" percent=\"data3\"> <!-- <span class=\"percent percent-sign\" style=\"font-size: 2em; margin-top: -0.8em; color: #508abc\">{{data3}}</span> --> <span class=\"percent\" style=\"margin-top: -3em; color:  #508abc\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data3}}</div> <div>First Promised <br>(OTD)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 14%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options4\" percent=\"data4\"> <!-- <span class=\"percent percent-sign\" style=\"font-size: 2em; margin-top: -0.8em; color: #62993e\">{{data4}}</span> --> <span class=\"percent\" style=\"margin-top: -3em; color:  #62993e\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data4}}</div> <div>Return Job Rate<br>(RTJ)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 14%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options5\" percent=\"data5\"> <!-- <span class=\"percent percent-sign\" style=\"font-size: 2em; margin-top: -0.8em; color: #508abc\">{{data5}}</span> --> <span class=\"percent\" style=\"margin-top: -3em; color:  #508abc\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data5}}</div> <div>Fix It Rate<br>(FIR)</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 14%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options6\" percent=\"data6\"> <!-- <span class=\"percent percent-sign\" style=\"font-size: 2em; margin-top: -0.8em; color: #d36e2a\">{{data6}}</span> --> <span class=\"percent\" style=\"margin-top: -3em; color:  #d36e2a\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data6}}</div> <div>Immediate Fill Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> <div style=\"width: 14%; float: left\"> <div class=\"row\" style=\"text-align: center\"> <div class=\"easy-pie-chart\" easypiechart options=\"options7\" percent=\"data7\"> <!-- <span class=\"percent percent-sign\" style=\"font-size: 2em; margin-top: -0.8em; color: #62993e\">{{data7}}</span> --> <span class=\"percent\" style=\"margin-top: -3em; color:  #62993e\"> <div class=\"percent-sign\" style=\"font-size: 200%\">{{data7}}</div> <div>Fill Rate</div> </span> </div> </div> <div class=\"row\" style=\"text-align: center\" class=\"color1\"> M to H-1 </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/gr/workshopUnitEntry.html',
    "<div ng-controller=\"UnitEntryGR\" class=\"row\"> <article class=\"col-lg-12\"> <div id=\"widget-workshop-unit-entry\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header style=\"background-color:#6e6e6e;color:aliceblue\"> <span class=\"widget-icon\"><i class=\"fa fa-car\"></i></span> <h2><strong>Today's Workshop Unit Entry</strong></h2> </header> <div class=\"widget-body\"> <div class=\"col-md-3 col-lg-3\"> <div class=\"dsh-panel\" style=\"background-color:#87d948\"> <b style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">Appointment</b> <br> <span style=\"font-size: 180%; font-weight:300\">{{data1}}</span> <div class=\"bot-left-panel\"> <b style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">Show</b> <br><span style=\"font-size: 120%; font-weight:300\">{{data2}}</span> </div> <div class=\"bot-right-panel\"> <b style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">No Show</b> <br><span style=\"font-size: 120%;font-weight:300\">{{data3}}</span> </div> <div class=\"hor-splitter\"></div> <div class=\"ver-splitter\"></div> </div> </div> <div class=\"col-md-2 col-lg-2\"> <div class=\"dsh-panel\" style=\"background-color: #ffb90f\"> <b style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">Walk In</b> <br><br> <span style=\"font-size: 250%;font-weight:300\">{{data4}}</span> </div> </div> <div class=\"col-md-2 col-lg-2\"> <div class=\"dsh-panel\" style=\"background-color:#696969\"> <b style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">Unit Entry</b> <br><br> <span style=\"font-size: 250%;font-weight:300\">{{data5}}</span> </div> </div> <div class=\"col-md-2 col-lg-2\"> <div class=\"dsh-panel\" style=\"background-color:#8968cd\"> <b style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">App Tomorrow</b> <br><br> <span style=\"font-size: 250%;font-weight:300\">{{data6}}</span> </div> </div> <div class=\"col-md-3 col-lg-3\"> <div class=\"dsh-panel\" style=\"background-color:\t#ff7f24\"> <b style=\"font-size: 1.3em; margin-bottom:0px; font-weight: bolder\">WIP</b> <br> <span style=\"font-size: 180%;font-weight:300\">{{data7}}</span> <div class=\"bot-left-panel\"> <b style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">Being Serviced</b> <br><span style=\"font-size: 120%;font-weight:300\">{{data8}}</span> </div> <div class=\"bot-right-panel\"> <b style=\"font-size: 1.2em; margin-bottom:0px; font-weight: bolder\">Billing</b> <br><span style=\"font-size: 120%;font-weight:300\">{{data9}}</span> </div> <div class=\"hor-splitter\"></div> <div class=\"ver-splitter\"></div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/arOverdue.html',
    "<div ng-controller=\"DashboardArOverdue\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>AR Overdue</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"text-align: center\"> <div ng-show=\"ShowLoadingDashboard_ArOverdue==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ArOverdue==false\" style=\"float:right;margin-right:20px\">TOTAL AR:{{TotalIsichartpie3d}}</div> <div ng-show=\"ShowLoadingDashboard_ArOverdue==false\" id=\"chartdivpie3d\"></div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/prospectMonitoring.html',
    "<div ng-controller=\"DashboardProspectMonitoring\"> <article class=\"col-sm-6 col-md-6 col-lg-6\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Prospect</strong></h2> </header> <div class=\"widget-body\"> <div style=\"text-align: center\"> <strong style=\"font-size:16px\"><label><b>Total Model</b></label></strong> </div> <div style=\"color: transparent;text-align: center\"> <strong style=\"font-size:16px\"><label><b>.</b></label></strong> </div> <div style=\"color: transparent;text-align: center\"> <strong style=\"font-size:16px\"><label><b>.</b></label></strong> </div> <br> <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoring1==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoring1==false\" class=\"row\" style=\"text-align: center\"> <p align=\"center\">Total Prospect</p> <div id=\"chartdivProspectWithLabel1\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoring1==false\" class=\"row\" style=\"text-align: center\" class=\"color1\"> </div> </div> <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoring2==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoring2==false\" class=\"row\" style=\"text-align: center\"> <p align=\"center\">Hot Prospect</p> <div id=\"chartdivProspectWithLabel2\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoring2==false\" class=\"row\" style=\"text-align: center\" class=\"color1\"> </div> </div> <div> <p align=\"center\">Drop Hot:{{ProspectMonitoringDropHot}}</p> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/prospectMonitoringModel.html',
    "<div ng-controller=\"DashboardProspectMonitoringModel\"> <article class=\"col-sm-6 col-md-6 col-lg-6\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Prospect</strong> </h2></header> <div class=\"widget-body\"> <div style=\"text-align: center\"> <strong style=\"font-size:16px\"><label><b>{{ProspectMonitoringNamaModel_1}}</b></label></strong> </div> <div style=\"text-align: center\"> <strong style=\"font-size:16px\"><label ng-if=\"ProspectMonitoringNamaModel_2==undefined\" style=\"color: transparent\"><b>.</b></label><label ng-if=\"ProspectMonitoringNamaModel_2!=undefined\"><b>{{ProspectMonitoringNamaModel_2}}</b></label></strong> </div> <div style=\"text-align: center\"> <strong style=\"font-size:16px\"><label ng-if=\"ProspectMonitoringNamaModel_3==undefined\" style=\"color: transparent\"><b>.</b></label><label ng-if=\"ProspectMonitoringNamaModel_3!=undefined\"><b>{{ProspectMonitoringNamaModel_3}}</b></label></strong> </div> <br> <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoringModel1==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoringModel1==false\" class=\"row\" style=\"text-align: center\"> <p align=\"center\">Total Prospect</p> <div id=\"chartdivProspectWithLabel1_2\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoringModel1==false\" class=\"row\" style=\"text-align: center\" class=\"color1\"> </div> </div> <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoringModel2==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoringModel2==false\" class=\"row\" style=\"text-align: center\"> <p align=\"center\">Hot Prospect</p> <div id=\"chartdivProspectWithLabel2_2\"></div> </div> <div ng-show=\"ShowLoadingDashboard_ProspectMonitoringModel2==false\" class=\"row\" style=\"text-align: center\" class=\"color1\"> </div> </div> <div> <p align=\"center\">Drop Hot:{{ProspectMonitoringModelDropHot}}</p> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/rsBoard.html',
    "<div ng-controller=\"DashboardRsBoard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>RS</strong></h2> </header> <div class=\"widget-body\"> <div ng-show=\"ShowLoadingDashboard_RsBoard==true\" align=\"center\"> <div class=\"loader\"></div> </div> <!--<div ng-show=\"ShowLoadingDashboard_RsBoard==false\" class=\"row da_tinggi_tabel\" style=\"text-align: center;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<div class=\"headDiv\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w40\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w1\" ng-class=\"ach_total_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementAtas\">Achievement</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementAtas\">{{BoardRsTotalAchievement}} ({{BoardRsTotalAchievementPercent}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKiri\">Target RS</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKiri\">{{BoardRsTargetRs}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50\" ng-class=\"ach_total_last_rs\"> \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKanan\">{{BoardRsTargetRsLast}}\t({{BoardRsTargetRsLastPercentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w20\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" ng-class=\"ach_total_t1_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardRsCat1}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardRsAch1}} ({{BoardRsAch1Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target RS</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardRsAch1Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"ach_total_t1_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardRsAch1Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" ng-class=\"ach_total_t2_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardRsCat4}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardRsAch4}} ({{BoardRsAch4Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target RS</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardRsAch4Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"ach_total_t2_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardRsAch4Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w20\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" ng-class=\"ach_total_t3_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardRsCat2}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardRsAch2}} ({{BoardRsAch2Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target RS</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardRsAch2Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"ach_total_t3_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardRsAch2Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" ng-class=\"ach_total_t4_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardRsCat5}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardRsAch5}} ({{BoardRsAch5Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target RS</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardRsAch5Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"ach_total_t4_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardRsAch5Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w20\" >\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" style=\"border-right:1px solid;\" ng-class=\"ach_total_t5_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardRsCat3}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardRsAch3}} ({{BoardRsAch3Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target RS</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardRsAch3Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" style=\"border-right:1px solid;\" ng-class=\"ach_total_t5_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardRsAch3Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" style=\"border-right:1px solid;\" ng-class=\"ach_total_t6_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardRsCat6}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardRsAch6}} ({{BoardRsAch6Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target RS</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardRsAch6Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" style=\"border-right:1px solid;\" ng-class=\"ach_total_t6_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardRsAch6Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t</div>--> <!--<div class=\"numberCircle\">30</div>--> <!-- <div ng-show=\"ShowLoadingDashboard_RsBoard==false\" class=\"row\" style=\"float:left;width:100%;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<div style=\"float:left;width:60%;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<div class=\"widget-body\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div id=\"RsDashboardGantiTabelKeGrafik\"></div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</div> --> <!-- mike punya diagram side batang --> <!-- <div style=\"float:left; width:40%;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<div style=\"height:500px; margin-bottom: -50%;\" align=\"right\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"tabelAtas\" style=\"height:15%;margin-top:50px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" style=\"height:43px;border-collapse: collapse\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr align=\"center\" style=\"border:2px solid white;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" style=\"border:2px solid white;width:40%; padding-right: 3px;\"> &nbsp;</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td><i style=\"border:2px solid white;width:20%\" class=\"fa fa-trophy iconDashboard\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\taria-hidden=\"true\"></i></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td><i style=\"border:2px solid white;width:20%\" class=\"fa fa-dot-circle-o iconDashboard\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\taria-hidden=\"true\"></i></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td><i style=\"border:2px solid white;width:20%\" class=\"fa fa-history iconDashboard\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\taria-hidden=\"true\"></i></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-34px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" style=\"height:43px;border-collapse: collapse\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr style=\"border:2px solid white;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" style=\"border:2px solid white;width:40%; padding-right: 3px;\">Other</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t6_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch6}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch6Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t6_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch6Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-34px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" style=\"height:43px;border-collapse: collapse\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr style=\"border:2px solid white;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" style=\"border:2px solid white;width:40%; padding-right: 3px;\">Avanza</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t1_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch1}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch1Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t1_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch1Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-34px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" style=\"height:43px;border-collapse: collapse\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr style=\"border:2px solid white;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" style=\"border:2px solid white;width:40%; padding-right: 3px;\">Corolla</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t3_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch2}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch2Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t3_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch2Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-34px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" style=\"height:43px;border-collapse: collapse\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr style=\"border:2px solid white;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" style=\"border:2px solid white;width:40%; padding-right: 3px;\">Transmover</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t5_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch3}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch3Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t5_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch3Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-34px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" style=\"height:43px;border-collapse: collapse\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr style=\"border:2px solid white;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" style=\"border:2px solid white;width:40%; padding-right: 3px;\">Calya</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t2_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch4}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch4Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t2_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch4Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-34px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" style=\"height:43px;border-collapse: collapse\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr style=\"border:2px solid white;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" style=\"border:2px solid white;width:40%; padding-right: 3px;\">Dyna</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t4_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch5}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch5Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_t4_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsAch5Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-34px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" style=\"height:43px;border-collapse: collapse\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr style=\"border:2px solid white;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" style=\"border:2px solid white;width:40%; padding-right: 3px;\">Total</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsTotalAchievement}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"warna_target_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsTargetRs}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"border:2px solid white;width:20%\" ng-class=\"ach_total_last_rs\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<b>{{BoardRsTargetRsLast}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</div> --> <!-- end mike punya diagram side batang --> <div ng-show=\"ShowLoadingDashboard_RsBoard==false\" class=\"row\" style=\"float:left;width:100%\"> <div class=\"DashboardDiagram\" align=\"center\"> <div class=\"widget-body\"> <div class=\"container_circle\" align=\"center\"> <a class=\"tooltipCustom\" data-tooltip=\"{{BoardRsTotalAchievement}}/{{BoardRsTargetRs}}\"> <div class=\"warnaAsli\" ng-class=\"ach_total_rs\"> <div class=\"circleDiagram hvr-grow\"> <div align=\"center\"> Achv/Target <br> <span>{{BoardRsTotalAchievement}}/{{BoardRsTargetRs}}</span> </div> </div> <div class=\"kotak hvr-grow\" ng-class=\"ach_total_last_rs\"> {{BoardRsTargetRsLast}} </div> </div> </a> </div> </div> </div> <div class=\"DashboardTabel\"> <div style=\"height:500px; margin-bottom: -50%\" align=\"right\"> <div class=\"tabelAtas\" style=\"height:15%;margin-top:50px\"> <table class=\"LebarTabelDashBaru\"> <tr> <td align=\"right\" class=\"namaMobilStyle\"> &nbsp;</td> <td align=\"center\"><i class=\"fa fa-trophy iconDashboard\" aria-hidden=\"true\"></i></td> <td align=\"center\"><i class=\"fa fa-dot-circle-o iconDashboard\" aria-hidden=\"true\"></i></td> <td align=\"center\"><i class=\"fa fa-history iconDashboard\" aria-hidden=\"true\"></i></td> </tr> <tr ng-repeat=\"RS in SelectRSCar\"> <td align=\"right\" class=\"namaMobilStyle\">{{SelectRSCar[$index].carName}} &nbsp;</td> <td align=\"center\" ng-class=\"SelectRSCar[$index].ach_total_rs\"><b>{{SelectRSCar[$index].BoardRsAch}}</b></td> <td align=\"center\" ng-class=\"SelectRSCar[$index].warna_target_rs\"><b>{{SelectRSCar[$index].BoardRsAchTarget}}</b></td> <td align=\"center\" ng-class=\"SelectRSCar[$index].ach_total_last_rs\"><b>{{SelectRSCar[$index].BoardRsAchLast}}</b></td> </tr> <!-- <tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" class=\"namaMobilStyle\">{{BoardRsCat5}} &nbsp;</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t5_rs\"><b>{{BoardRsAch5}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"warna_target_rs\"><b>{{BoardRsAch5Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t5_last_rs\"><b>{{BoardRsAch5Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" class=\"namaMobilStyle\">{{BoardRsCat1}} &nbsp;</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t1_rs\"><b>{{BoardRsAch1}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"warna_target_rs\"><b>{{BoardRsAch1Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t1_last_rs\"><b>{{BoardRsAch1Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" class=\"namaMobilStyle\">{{BoardRsCat2}} &nbsp;</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t2_rs\"><b>{{BoardRsAch2}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"warna_target_rs\"><b>{{BoardRsAch2Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t2_last_rs\"><b>{{BoardRsAch2Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" class=\"namaMobilStyle\">{{BoardRsCat3}} &nbsp;</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t3_rs\"><b>{{BoardRsAch3}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"warna_target_rs\"><b>{{BoardRsAch3Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t3_last_rs\"><b>{{BoardRsAch3Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\" class=\"namaMobilStyle\">{{BoardRsCat4}} &nbsp;</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t4_rs\"><b>{{BoardRsAch4}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"warna_target_rs\"><b>{{BoardRsAch4Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_t4_last_rs\"><b>{{BoardRsAch4Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr> --> <!-- <tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"right\">Total&nbsp;</td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_rs\"><b>{{BoardRsTotalAchievement}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"warna_target_rs\"><b>{{BoardRsTargetRs}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" ng-class=\"ach_total_last_rs\"><b>{{BoardRsTargetRsLast}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr> --> </table> </div> </div> </div> <div class=\"col-md-12 margintabelBawah\"> <table align=\"center\"> <tr> <td>Achievement &nbsp; </td> <td class=\"PentolLegend\" style=\"background-color:red\"></td> <td>&lt;50% &nbsp;</td> <td class=\"PentolLegend\" style=\"background-color:yellow\"></td> <td>50-99% &nbsp;</td> <td class=\"PentolLegend\" style=\"background-color:#34EB14\"></td> <td>100%</td> <td></td> </tr> </table> </div> </div> </div> <!--<div class=\"widget-body\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<div id=\"RsDashboardGantiTabelKeGrafik\"></div>\r" +
    "\n" +
    "\t\t\t\t\t\t</div>--> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/salesTarget.html',
    "<div ng-controller=\"DashboardSalesTarget\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Sales Target</strong></h2> </header> <div class=\"widget-body\"> <div ng-show=\"ShowLoadingDashboard_SalesTarget==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_SalesTarget==false\" style=\"text-align: center\"> <strong style=\"font-size:16px\">Total Model</strong> </div> <br> <div ng-show=\"ShowLoadingDashboard_SalesTarget==false\" class=\"row\" style=\"text-align: center\"> <div id=\"chartstackedbarSalesTarget\"></div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/salesTargetModel.html',
    "<div ng-controller=\"DashboardSalesTargetModel\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Sales Target</strong></h2> </header> <div class=\"widget-body\"> <div ng-show=\"ShowLoadingDashboard_SalesTargetModel==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_SalesTargetModel==false\" style=\"text-align: center\"> <strong style=\"font-size:16px\">{{SalesTargetModelJoedoel}}</strong> </div> <br> <div ng-show=\"ShowLoadingDashboard_SalesTargetModel==false\" class=\"row\" style=\"text-align: center\"> <div id=\"chartstackedbarSalesTargetModel\"></div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/spkBoard.html',
    "<div ng-controller=\"DashboardSpkBoard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>SPK</strong></h2> </header> <div class=\"widget-body\"> <div ng-show=\"ShowLoadingDashboard_SpkBoard==true\" align=\"center\"> <div class=\"loader\"></div> </div> <!--<div ng-show=\"ShowLoadingDashboard_SpkBoard==false\" class=\"row da_tinggi_tabel\" style=\"text-align: center;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<div class=\"headDiv\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w40\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w1\" ng-class=\"ach_total_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementAtas\">Achievement</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementAtas\">{{BoardSpkTotalAchievement}} ({{BoardSpkTotalAchievementPercent}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50\" ng-class=\"warna_target_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKiri\">Target SPK</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKiri\">{{BoardSpkTargetSpk}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50\" ng-class=\"ach_total_last_spk\"> \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakTotalAchievementKanan\">{{BoardSpkTargetSpkLast}}\t({{BoardSpkTargetSpkLastPercentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w20\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" ng-class=\"ach_total_t1_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat1}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardSpkAch1}} ({{BoardSpkAch1Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target SPK</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch1Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"ach_total_t1_last_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardSpkAch1Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" ng-class=\"ach_total_t2_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat4}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardSpkAch4}} ({{BoardSpkAch4Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target SPK</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch4Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"ach_total_t2_last_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardSpkAch4Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w20\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" ng-class=\"ach_total_t3_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat2}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardSpkAch2}} ({{BoardSpkAch2Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target SPK</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch2Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"ach_total_t3_last_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardSpkAch2Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" ng-class=\"ach_total_t4_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat5}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardSpkAch5}} ({{BoardSpkAch5Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target SPK</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch5Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"ach_total_t4_last_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardSpkAch5Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w20\" >\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" style=\"border-right:1px solid;\" ng-class=\"ach_total_t5_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat3}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardSpkAch3}} ({{BoardSpkAch3Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target SPK</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch3Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" style=\"border-right:1px solid;\" ng-class=\"ach_total_t5_last_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardSpkAch3Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w2\" style=\"border-right:1px solid;\" ng-class=\"ach_total_t6_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">ACH {{BoardSpkCat6}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementAtas\">{{BoardSpkAch6}} ({{BoardSpkAch6Percentage}}&#37;)</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" ng-class=\"warna_target_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriAtas\">Target SPK</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKiriBawah\">{{BoardSpkAch6Target}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div class=\"c1 w50_2\" style=\"border-right:1px solid;\" ng-class=\"ach_total_t6_last_spk\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">ACH</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{TodayMonthMinus1_Raw|date:'MMM yy'}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<div class=\"KotakAchievementKanan\">{{BoardSpkAch6Last}}</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t</div>--> <!-- mike punya --> <!-- <div ng-show=\"ShowLoadingDashboard_SpkBoard==false\" class=\"row\" style=\"float:left;width:100%;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<div style=\"float:left;width:90%;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<div class=\"widget-body\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div id=\"SpkDashboardGantiTabelKeGrafik\"></div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<div style=\"float:left;width:10%;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<div style=\"height:500px\" align=\"right\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:20px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" border=\"1\" style=\"height:43px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t6_spk\"><b>{{BoardSpkAch6}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"warna_target_spk\"><b>{{BoardSpkAch6Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t6_last_spk\"><b>{{BoardSpkAch6Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-25px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" border=\"1\" style=\"height:43px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t1_spk\"><b>{{BoardSpkAch1}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"warna_target_spk\"><b>{{BoardSpkAch1Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t1_last_spk\"><b>{{BoardSpkAch1Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-25px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" border=\"1\" style=\"height:43px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t3_spk\"><b>{{BoardSpkAch2}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"warna_target_spk\"><b>{{BoardSpkAch2Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t3_last_spk\"><b>{{BoardSpkAch2Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-25px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" border=\"1\" style=\"height:43px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t5_spk\"><b>{{BoardSpkAch3}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"warna_target_spk\"><b>{{BoardSpkAch3Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t5_last_spk\"><b>{{BoardSpkAch3Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-25px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" border=\"1\" style=\"height:43px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t2_spk\"><b>{{BoardSpkAch4}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"warna_target_spk\"><b>{{BoardSpkAch4Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t2_last_spk\"><b>{{BoardSpkAch4Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-25px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" border=\"1\" style=\"height:43px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t4_spk\"><b>{{BoardSpkAch5}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"warna_target_spk\"><b>{{BoardSpkAch5Target}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_t4_last_spk\"><b>{{BoardSpkAch5Last}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<div style=\"height:15%;margin-top:-25px\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t<table class=\"LebarTabelDashBaru\" border=\"1\" style=\"height:43px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_spk\"><b>{{BoardSpkTotalAchievement}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"warna_target_spk\"><b>{{BoardSpkTargetSpk}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t<td align=\"center\" style=\"width:33%\" ng-class=\"ach_total_last_spk\"><b>{{BoardSpkTargetSpkLast}}</b></td>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t</table>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</div> --> <!-- end mike punya --> <div ng-show=\"ShowLoadingDashboard_SpkBoard==false\" class=\"row\" style=\"float:left;width:100%\"> <div class=\"DashboardDiagram\" align=\"center\"> <div class=\"widget-body\"> <div class=\"container_circle\" align=\"center\"> <a class=\"tooltipCustom\" data-tooltip=\"{{BoardSpkTotalAchievement}}/{{BoardSpkTargetSpk}}\"> <div class=\"warnaAsli\" ng-class=\"ach_total_spk\"> <div class=\"circleDiagram hvr-grow\"> <div align=\"center\"> Achv/Target <br> <span>{{BoardSpkTotalAchievement}}/{{BoardSpkTargetSpk}}</span> </div> </div> <div class=\"kotak hvr-grow\" ng-class=\"ach_total_last_spk\"> {{BoardSpkTargetSpkLast}} </div> </div> </a> </div> </div> </div> <div class=\"DashboardTabel\"> <div style=\"height:500px; margin-bottom: -50%\" align=\"right\"> <div class=\"tabelAtas\" style=\"height:15%;margin-top:50px\"> <table class=\"LebarTabelDashBaru\"> <tr> <td align=\"right\" class=\"namaMobilStyle\"> &nbsp;</td> <td align=\"center\"><i class=\"fa fa-trophy iconDashboard\" aria-hidden=\"true\"></i></td> <td align=\"center\"><i class=\"fa fa-dot-circle-o iconDashboard\" aria-hidden=\"true\"></i></td> <td align=\"center\"><i class=\"fa fa-history iconDashboard\" aria-hidden=\"true\"></i></td> </tr> <tr ng-repeat=\"RS in SelectSPKCar\"> <td align=\"right\" class=\"namaMobilStyle\">{{SelectSPKCar[$index].carName}} &nbsp;</td> <td align=\"center\" ng-class=\"SelectSPKCar[$index].ach_total_spk\"> <b>{{SelectSPKCar[$index].BoardSpkAch}}</b></td> <td align=\"center\" ng-class=\"SelectSPKCar[$index].warna_target_spk\"> <b>{{SelectSPKCar[$index].BoardSpkAchTarget}}</b></td> <td align=\"center\" ng-class=\"SelectSPKCar[$index].ach_total_last_spk\"> <b>{{SelectSPKCar[$index].BoardSpkAchLast}}</b></td> </tr> </table> </div> </div> </div> <div class=\"col-md-12 margintabelBawah\"> <table align=\"center\"> <tr> <td>Achievement &nbsp; </td> <td class=\"PentolLegend\" style=\"background-color:red\"></td> <td>&lt;50% &nbsp;</td> <td class=\"PentolLegend\" style=\"background-color:yellow\"></td> <td>50-99% &nbsp;</td> <td class=\"PentolLegend\" style=\"background-color:#34EB14\"></td> <td>100%</td> <td></td> </tr> </table> </div> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/spkOutstanding.html',
    "<div ng-controller=\"spkOutstandingBoard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>SPK Outstanding</strong></h2> </header> <div class=\"widget-body\"> <div ng-show=\"ShowLoadingDashboard_SpkOutstanding==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_SpkOutstanding==false\" id=\"chartdiv3\"></div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/spkOutstandingDetail.html',
    "<div ng-controller=\"spkOutstandingDetailBoard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>SPK Outstanding by Aging</strong></h2> </header> <div class=\"widget-body\"> <div id=\"charts\"> <!--<div id=\"chart1\" class=\"chartdiv\"></div>--> <div ng-show=\"ShowLoadingDashboard_SpkOutstandingDetail==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_SpkOutstandingDetail==false\" id=\"chart2\" class=\"chartdiv\"></div> <div ng-show=\"ShowLoadingDashboard_SpkOutstandingDetail==false\" id=\"chart3\" class=\"chartdiv\"></div> <div ng-show=\"ShowLoadingDashboard_SpkOutstandingDetail==false\" id=\"chart4\" class=\"chartdiv\"></div> </div> <div> <p align=\"center\"> <div id=\"legenddiv\"></div> </p> </div> </div> </div> </div> </article> </div>"
  );


  $templateCache.put('app_dashboard/sales_dashboard/stockDashboard.html',
    "<div ng-controller=\"StockDashboard\"> <article class=\"col-lg-12\"> <div class=\"row\"> <div id=\"widget-appointment\" data-jarvis-widget data-widget-editbutton=\"false\" data-widget-deletebutton=\"false\"> <header> <h2><strong>Stock Aging</strong></h2> </header> <div class=\"widget-body\"> <div class=\"row\" style=\"text-align: center\"> <!--awal batang--> <div ng-show=\"ShowLoadingDashboard_StockDashboard==true\" align=\"center\"> <div class=\"loader\"></div> </div> <div ng-show=\"ShowLoadingDashboard_StockDashboard==false\" id=\"chartdivbatang\"></div> <!--akhir batang--> </div> </div> </div> </div> </article> </div>"
  );

}]);
