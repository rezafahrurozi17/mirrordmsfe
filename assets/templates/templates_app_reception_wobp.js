angular.module('templates_app_reception_wobp', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/services/reception/wo_bp/Technicalcompletion.html',
    "<div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <fieldset style=\"border:2px solid #e2e2e2\"> <legend style=\"margin-left:10px;width:inherit;border-bottom:0px\"> WO Information </legend> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-xs-12 col-sm-12 col-md-12\"> <div class=\"col-xs-6 col-sm-6 col-md-6\"> <div class=\"form-group\"> <label class=\"col-xs-4\">No. WO </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.WoNo}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-4\">No. Polisi </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.PoliceNumber}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-4\">Model </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.ModelType}}</label> </div> </div> <div class=\"col-xs-6 col-sm-6 col-md-6\"> <div class=\"form-group\"> <label class=\"col-xs-6\">Tgl. WO </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-4\">{{mData.WoCreatedDate | date: 'dd/MM/yyyy hh:mm:ss'}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-6\">Janji Penyerahan</label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-4\">{{mData.FixedDeliveryTime1 | date: 'dd/MM/yyyy hh:mm:ss'}}</label> </div> </div> </div> </div> </fieldset> </div> <div class=\"col-md-12\"> <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Job Request\" list-title=\"Job Request\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Permintaan\" name=\"reqdesc\" ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "            </textarea> <bserrmsg field=\"reqdesc\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> <div class=\"col-md-6\"> <!--     <pre>{{vm.appScope.ComplaintCategory}}</pre>\r" +
    "\n" +
    "    <pre>{{appScope.ComplaintCategory}}</pre>\r" +
    "\n" +
    "    <pre>{{ComplaintCategory}}</pre> --> <bslist data=\"JobComplaint\" m-id=\"JobComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Complaint\" list-title=\"Complaint\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <!--                     <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Complaint Category\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                    </bsselect> --> <input type=\"text\" skip-enable class=\"form-control\" ng-model=\"lmItem.ComplaintCatg\" name=\"\" ng-disabled=\"true\"> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Deskripsi\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div style=\"margin-bottom:90px\"> <div class=\"form-group\"> <bsreqlabel>Kategori</bsreqlabel> <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Kategori\" required> </bsselect> <bserrmsg field=\"ctype\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom: 90px\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\" required>\r" +
    "\n" +
    "                                </textarea> <bserrmsg field=\"ctext\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> <div class=\"col-md-6\"> <div style=\"text-align:center\">Status</div> <div style=\"height:181px;padding:5px 5px 5px 5px;text-align:center\"> <label> GI Part</label> <h1 style=\"color:red\">{{statusGI}}</h1> </div> <div ng-show=\"showOPLlabel == 1\" style=\"height:181px;padding:5px 5px 5px 5px; text-align:center\"> <label>OPL</label> <h1 style=\"color:red\">{{statusOPL}}</h1> </div> <div ng-show=\"showOPLBPlabel == 1\" style=\"height:181px;padding:5px 5px 5px 5px; text-align:center\"> <label>OPL BP</label> <h1 style=\"color:red\">{{statusOPLBP}}</h1> </div> </div> <div class=\"col-md-12\"> <!-- listView --> <bslist data=\"gridWork\" m-id=\"tmpTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApi\" confirm-save=\"false\" list-title=\"Order Pekerjaan\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <!-- <p>Tipe :{{lmItem.catName}}</p> --> </div> <div class=\"col-md-3\"> <!-- <p>Flat Rate: {{lmItem.FlatRate}}</p> --> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkSummary | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalWorkDiscountedSummary | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(totalWorkSummary - totalWorkDiscountedSummary) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialSummary | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialDiscountedSummary| currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(totalMaterialSummary - totalMaterialDiscountedSummary) | currency:\"\":0}}</td> </tr> <tr> <td>Total DP Parts Material/Bahan</td> <td style=\"text-align:right\"><span style=\"float: left\"></span>{{totalDpEstimate | currency:\"\":0}}</td> <td></td> <td style=\"text-align:right\"><span style=\"float: left\"></span>{{totalDpEstimate | currency:\"\":0}}</td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <!-- OPL BSLIST DISINI! --> <bslist data=\"tmpDataOPL\" m-id=\"Id\" list-model=\"lmModelOpl\" on-before-save=\"onBeforeSaveOpl\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <strong>{{lmItem.OPLName}}</strong> </div> <div class=\"form-group\"> {{lmItem.VendorName}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Estimasi : </label> {{lmItem.TargetFinishDate | date:'dd-MM-yyyy'}} </div> <div class=\"form-group\"> <label>Quantity : </label> {{lmItem.QtyPekerjaan}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}} </div> </div> </div> </bslist-template> <bslist-modal-form> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMasterOpl.html'\"> </div></bslist-modal-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <tbody> <tr> <td width=\"20%\"><b>Total</b></td> <!-- <td>{{sumWorkOpl | currency:\"Rp. \":0}}</td> --> <td><b>{{(totalWorkDiscountedSummary + totalMaterialDiscountedSummary) | currency:\"Rp. \":0}}</b></td> </tr> </tbody> </table> </div> <div class=\"col-md-12\" ng-show=\"checkIsWarranty == 1\"> <!-- <fieldset style=\"border:2px solid #e2e2e2;\">\r" +
    "\n" +
    "      <legend style=\"margin-left:10px;width:inherit;border-bottom:0px;\">\r" +
    "\n" +
    "        Warranty Info\r" +
    "\n" +
    "      </legend> --> <bslist data=\"getWarrantyInfo\" m-id=\"tmpTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModelWarr\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" confirm-save=\"false\" list-title=\"Warranty Info\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Claim :</label> <label>{{lmItem.ClaimNo}}</label> </div> <div class=\"form-group\"> <label>No. TWC :</label> <label>{{lmItem.TwcNo}}</label> </div> <div class=\"form-group\"> <label>VDS :</label> <label>{{lmItem.VDS}}</label> </div> <div class=\"form-group\"> <label>VIS :</label> <label>{{lmItem.VIS}}</label> </div> <div class=\"form-group\"> <label>Total Amount :</label> <label>{{lmItem.TotalAmount}}</label> </div> <div class=\"form-group\"> <label>Approved :</label> <label>{{lmItem.Approved}}</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Tipe Claim :</label> <label>{{lmItem.ClaimType}}</label> </div> <div class=\"form-group\"> <label>WMI :</label> <label>{{lmItem.WMI}}</label> </div> <div class=\"form-group\"> <label>CD :</label> <label>{{lmItem.CD}}</label> </div> <div class=\"form-group\"> <label>Status Claim :</label> <label>{{lmItem.StatusClaim}}</label> </div> <div class=\"form-group\"> <label>Error code :</label> <label>{{lmItem.ErrorCode}}</label> </div> </div> </div> </div> </bslist-template> </bslist> <!-- <div class=\"\">\r" +
    "\n" +
    "        <div class=\"col-md-12\">\r" +
    "\n" +
    "          <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "            <thead>\r" +
    "\n" +
    "              <tr>\r" +
    "\n" +
    "                <th>Nomor LT</th>\r" +
    "\n" +
    "                <th>Description</th>\r" +
    "\n" +
    "                <th>Claim Amount</th>\r" +
    "\n" +
    "                <th>Status</th>\r" +
    "\n" +
    "                <th>Warranty Number</th>\r" +
    "\n" +
    "                <th>Acceptable Amount</th>\r" +
    "\n" +
    "                <th>Error Code</th>\r" +
    "\n" +
    "              </tr>\r" +
    "\n" +
    "            </thead>\r" +
    "\n" +
    "            <tbody>\r" +
    "\n" +
    "              <tr>\r" +
    "\n" +
    "                <td>Nomor LT</td>\r" +
    "\n" +
    "                <td>Description</td>\r" +
    "\n" +
    "                <td>Claim Amount</td>\r" +
    "\n" +
    "                <td>Status</td>\r" +
    "\n" +
    "                <td>Warranty Number</td>\r" +
    "\n" +
    "                <td>Acceptable Amount</td>\r" +
    "\n" +
    "                <td>Error Code</td>\r" +
    "\n" +
    "              </tr>\r" +
    "\n" +
    "            </tbody>\r" +
    "\n" +
    "          </table>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "    <div class=\"col-md-6 form-group\">\r" +
    "\n" +
    "      <label>Status TECO : </label>\r" +
    "\n" +
    "      <input type=\"text\" class=\"form-control\" placeholder=\"Status TECO\" name=\"totalDp\">\r" +
    "\n" +
    "    </div> --> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Status TECO : </label> <input type=\"text\" class=\"form-control\" placeholder=\"Status TECO\" name=\"totalDp\"> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> </div> <div class=\"col-md-9\"> <!-- <div class=\"btn-group pull-right\">\r" +
    "\n" +
    "        <button type=\"button\" ng-disabled=\"btnComplete\" ng-if=\"mData.Status >= 12 && mData.Status <=13\" ng-click=\"completeTeco(mData)\" class=\"rbtn btn\">\r" +
    "\n" +
    "          Complete!\r" +
    "\n" +
    "        </button>\r" +
    "\n" +
    "        <button type=\"button\" ng-disabled=\"btnComplete\" ng-if=\"mData.Status == 14\" ng-click=\"UncompleteTeco(mData)\" class=\"rbtn btn\">\r" +
    "\n" +
    "          UnTeco\r" +
    "\n" +
    "        </button>\r" +
    "\n" +
    "        <button ng-show=\"showSubmitTeco\" ng-show=\"isWarranty\" type=\"button\" class=\"rbtn btn\" ng-click=\"SubmitApprovalTecho(chooseAprove)\">\r" +
    "\n" +
    "          Submit\r" +
    "\n" +
    "        </button>\r" +
    "\n" +
    "      </div> --> <div class=\"btn-group pull-right\"> <!-- {{btnComplete}} - {{checkIsWarranty}} - {{showCompleteTeco}} {{(btnComplete || checkIsWarranty == 0 && showCompleteTeco != 1) }} --> <button type=\"button\" ng-disabled=\"(btnComplete || checkIsWarranty == 1) || holdLoadingSubmit\" ng-show=\"showSubmitTeco > 0\" class=\"rbtn btn\" ng-click=\"SubmitApprovalTecho(mData,gridWork)\"> <!--  ng-show=\"showSubmitTeco > 0\" --> Submit </button> <button type=\"button\" ng-disabled=\"(((checkIsWarranty == 0 || checkIsWarranty == 2) && showSubmitTeco > 0 || btnComplete || (oplData.length > 0 && statusOPL !== 'Completed' && statusOPL !== 'Closed') || (oplBPData.length > 0 && statusOPLBP !== 'Completed' && statusOPLBP !== 'Closed') ))\" skip-enable ng-if=\"(mData.Status >= 12 && mData.Status <=13) && showCompleteTeco > 0\" ng-click=\"completeTeco(mData)\" class=\"rbtn btn\"> Complete </button> <!-- <button type=\"button\" ng-disabled=\"(btnComplete) || (checkIsWarranty == 0)\" skip-enable ng-if=\"(mData.Status >= 12 && mData.Status <=13) && showSubmitTeco >= 0\" ng-click=\"completeTeco(mData)\" class=\"rbtn btn\">\r" +
    "\n" +
    "        Complete 2!\r" +
    "\n" +
    "        </button> --> <button type=\"button\" ng-disabled=\"btnComplete\" ng-if=\"mData.Status == 14\" ng-click=\"UncompleteTeco(mData)\" class=\"rbtn btn\"> UnTeco </button> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/callCustomer.html',
    "<div> <div ng-hide=\"isService\" class=\"row\"> <style type=\"text/css\">.radioCustomer input[type=radio] {\r" +
    "\n" +
    "                display: none;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "            \r" +
    "\n" +
    "            .radioCustomer input[type=radio]+label {\r" +
    "\n" +
    "                background-color: grey;\r" +
    "\n" +
    "                color: #fff;\r" +
    "\n" +
    "                padding: 10px;\r" +
    "\n" +
    "                -webkit-border-radius: 2px;\r" +
    "\n" +
    "                display: inline-block;\r" +
    "\n" +
    "                font-weight: 400;\r" +
    "\n" +
    "                text-align: center;\r" +
    "\n" +
    "                vertical-align: middle;\r" +
    "\n" +
    "                cursor: pointer;\r" +
    "\n" +
    "                width: 100px;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "            \r" +
    "\n" +
    "            .radioCustomer input[type=radio]:checked+label {\r" +
    "\n" +
    "                background-color: #d53337;\r" +
    "\n" +
    "            }</style> <div class=\"col-md-12\"> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <legend style=\"margin-left:10px;width:inherit;border-bottom:0px\"> WO Information </legend> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nomor WO :</label> <label>{{mData.WoNo}}</label> </div> <div class=\"form-group\"> <label>Nomor Polisi :</label> <label>{{mData.PoliceNumber}}</label> </div> <div class=\"form-group\"> <label>Model :</label> <label>{{mData.ModelType}}</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Tanggal WO :</label> <label>{{mData.WoCreatedDate | date:'MM/dd/yyyy'}}</label> </div> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Category</bsreqlabel> <bsselect name=\"Category\" ng-model=\"mData.Category\" data=\"woCategory\" item-text=\"Name\" item-value=\"Category\" placeholder=\"Pilih Kategori\" required> </bsselect> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"TaskId\" d-id=\"PartsId\" list-model=\"lmModel\" list-detail-model=\"ldModel\" button-settings=\"listCustomer\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" list-api=\"listApi\" confirm-save=\"true\" list-title=\"Job List\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\" list-height=\"250\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"email\">{{ lmItem.FR }}</div> <div> </div> </bslist-template> <bslist-detail-template> <p class=\"address\"><strong>{{ ldItem.Parts.PartsCode }}</strong></p> <div class=\"city\">{{ ldItem.Parts.PartsName }}</div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/appointment/appointmentServiceGr/modalFormMaster.html'\"> --> <div ng-include=\"'app/services/lib/ModalAppointmentGR/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/appointment/appointmentServiceGr/modalFormDetail.html'\"> --> <div ng-include=\"'app/services/lib/ModalAppointmentGR/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> </div> <div class=\"col-md-12\"> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Nomor LT</th> <th>Description</th> <th>Claim Amount</th> <th>Status</th> <th>Warranty Number</th> <th>Acceptable Amount</th> <th>Error Code</th> </tr> </thead> <tbody> <tr> <td>Nomor LT</td> <td>Description</td> <td>Claim Amount</td> <td>Status</td> <td>Warranty Number</td> <td>Acceptable Amount</td> <td>Error Code</td> </tr> </tbody> </table> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Lama Pekerjaan : </label> <input type=\"text\" class=\"form-control\" placeholder=\"0\" name=\"callCust\"> </div> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Total DP Parts Material/Bahan:</label> <input type=\"text\" name=\"dp\" class=\"form-control\" placeholder=\"Total DP Parts Material/Bahan\"> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Part Bekas</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radio1\" ng-model=\"isParts\" name=\"radio1\" value=\"1\" checked> <label for=\"radio1\">Kembalikan</label> <input type=\"radio\" id=\"radio2\" ng-model=\"isParts\" name=\"radio1\" value=\"0\"> <label for=\"radio2\">Tinggal</label> </div> <!-- {{isParts}} --> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Waiting / Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"wait1\" ng-model=\"isWaiting\" name=\"radio2\" value=\"1\" checked> <label for=\"wait1\">Waiting</label> <input type=\"radio\" id=\"wait2\" ng-model=\"isWaiting\" name=\"radio2\" value=\"0\"> <label for=\"wait2\">Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Car Wash</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"washing1\" ng-model=\"isWashing\" name=\"radio3\" value=\"1\" checked> <label for=\"washing1\">Wash</label> <input type=\"radio\" id=\"washing2\" ng-model=\"isWashing\" name=\"radio3\" value=\"0\"> <label for=\"washing2\">No Wash</label> </div> <!-- {{isWashing}} --> </div> <div class=\"row\"> <div class=\"col-md-3\"> <label>Scheduled Service Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.firstDate\" ng-change=\"DateChange(dt)\" ng-disabled=\"modeQueue == 3\"> </bsdatepicker> </div> <div class=\"form-group text-center\" style=\"width:20px\"> <b>-</b> </div> <div class=\"form-group\"> <bsdatepicker name=\"lastDate\" date-options=\"DateOptions\" ng-model=\"mData.lastDate\" ng-change=\"DateChange(dt)\" ng-disabled=\"modeQueue == 3\"> </bsdatepicker> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <label>Estimation Delivery Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.Estimate\" ng-change=\"DateChange(dt)\"> </bsdatepicker> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <label>Adjusment Delivery Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.Adjusment\" ng-change=\"DateChange(dt)\"> </bsdatepicker> </div> <div class=\"form-group\" style=\"width:200px\"> <bscheckbox ng-model=\"mData.customerRequest\" label=\"Customer Request\" ng-click=\"\" true-value=\"2\" false-value=\"0\"> </bscheckbox> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"btn-group pull-right\"> <button class=\"rbtn btn\" ng-click=\"callingCustomer(mData)\">Call Customer</button> </div> </div> </fieldset> </div> <bsui-modal show=\"show_modal.show\" title=\"Customer Notification\" data=\"modal_model\" on-save=\"saveCustomer\" on-cancel=\"cancelCustomer\" button-settings=\"buttonSettingModal\" mode=\"modalMode\"> <div ng-form name=\"customerForm\"> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <div class=\"col-md-12\" style=\"text-align:center\"> <h2>Lakukan proses notifikasi sekarang?</h2> <h3>Silahkan tekan nomor telepon dibawah untuk menelepon</h3> <p> <h3>{{modal_model.ContactPerson}}</h3> </p> <p> <h3>{{modal_model.PhoneContactPerson1}}&nbsp;&nbsp;<a href=\"Tel:{{modal_model.PhoneContactPerson1}}\"><i style=\"color:green\" class=\"fa fa-lg fa-phone\"></i></a></h3> </p> <p> <h3>{{modal_model.PhoneContactPerson2}}&nbsp;&nbsp;<a href=\"Tel:{{modal_model.PhoneContactPerson2}}\"><i style=\"color:green\" class=\"fa fa-lg fa-phone\"></i></a></h3> </p> </div> </fieldset> </div> </bsui-modal> </div> <div ng-show=\"isService\"> <div smart-include=\"app/services/reception/wo_bp/serviceExplanation.html\"></div> <!-- added by sss on 2017-10-05 --> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsselect name=\"DeliverVehicle\" data=\"deliverCar\" ng-model=\"choosenDelv\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pengambilan Mobil\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <button ng-click=\"sendBackSat()\" class=\"rbtn btn\" ng-show=\"choosenDelv==2\">Send to Satellite</button> </div> </div> <!--  --> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/CPPKInfo.html',
    "<div class=\"panel panel-default\"> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-xs-6 col-xm-6 col-md-6\"> <div class=\"form-group\"> <label>List Kontak Person</label> <bsselect name=\"ContactPerson\" ng-model=\"mDataDM.CP\" data=\"cpList\" item-text=\"Name\" item-value=\"Name\" placeholder=\"Pilih Kontak Person\" on-select=\"onChangeCP(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.CP.Name\" placeholder=\"Nama\" maxlength=\"50\"> </div> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.CP.HP1\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.CP.HP2\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> <!-- ================= --> <div class=\"col-xs-6 col-xm-6 col-md-6\"> <div class=\"form-group\"> <label>List Pengambil Keputusan</label> <bsselect name=\"PengambilKeputusan\" ng-model=\"mDataDM.PK\" data=\"pkList\" item-text=\"Name\" item-value=\"Name\" placeholder=\"Pilih Penanggung Jawab\" on-select=\"onChangePK(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.PK.Name\" placeholder=\"Nama\" maxlength=\"50\"> </div> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.PK.HP1\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.PK.HP2\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/OPL.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <bslist ng-if=\"refreshBslist\" data=\"oplData\" form-name=\"orderOPLBp\" m-id=\"oplId\" list-model=\"lmModelOpl\" on-before-new=\"onBeforeNewOpl\" on-before-save=\"onBeforeSaveOpl\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"ButtonListOPL\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> <!-- button-settings=\"ButtonList\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> --> <bslist-template> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <strong>{{lmItem.OPLWorkName}}</strong> </div> <div class=\"form-group\"> {{lmItem.VendorName}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Dibutuhkan : </label> {{lmItem.RequiredDate | date:'dd-MM-yyyy'}} </div> <div class=\"form-group\"> <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}} </div> </div> <div class=\"col-md-4\"> </div> </div> </bslist-template> <bslist-modal-form> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMasterOpl.html'\"> </div></bslist-modal-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <tbody> <tr> <td width=\"20%\">Total</td> <td>{{sumWork | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/OPLNew.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Order Pekerjaan Luar</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-6 col-xs-6\"> <button class=\"ui icon inverted grey button\" style=\"font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px\" ng-click=\"deleteAllDataGridViewWork()\"><i class=\"fa fa-trash\"></i> </button> </div> <div class=\"col-md-6 col-xs-6\"> <button ng-click=\"plushOrderOpl()\" class=\"rbtn btn pull-right\" data-target=\"#ModalOP\" on-deny=\"denied\" data-toggle=\"modal\" data-backdrop=\"static\" data-keyboard=\"false\"><i class=\"fa fa-plus\"></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridWorkView\" class=\"ag-theme-balham\" style=\"height: 400px\"></div> </fieldset> <div class=\"ui modal ModalOPL\" role=\"dialog\" style=\"left:0% !important; width:75%; margin:-284px auto 0px; background-color: transparent !important\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Order Pekerjaan Luar</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <label>Kode Vendor</label> <input type=\"text\" class=\"form-control\" name=\"VendorCode\" ng-model=\"mOpl.VendorCode\" ng-disabled=\"true\" skip-enable> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <bsreqlabel>Vendor</bsreqlabel> <select name=\"vendor\" convert-to-number class=\"form-control\" ng-model=\"mOpl.vendor\" data=\"OplVendor\" placeholder=\"Pilih Vendor\" required ng-disabled=\"viewMode\" skip-enable ng-change=\"chooseVendor()\"> <option ng-repeat=\"sat in OplVendor\" value=\"{{sat.VendorId}}\">{{sat.Name}}</option> </select> <bserrmsg field=\"Vendor\"></bserrmsg> </div> </div> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">OPL</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-12 col-xs-12\"> <button ng-disabled=\"viewMode\" skip-enable ng-click=\"plushOPL()\" class=\"rbtn btn pull-right\"><i class=\"fa fa-plus\" n></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridDetailOPL\" class=\"ag-theme-balham\" style=\"height: 400px\" id=\"gridParts\"></div> </fieldset> </div> <!-- <label>{{OplVendor}}</label> --> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important;background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"wbtn btn ng-binding\" style=\"width:100px\" ng-click=\"closeModalOpl()\">Cancel</div> <div class=\"rbtn btn ng-binding ng-scope\" ng-disabled=\"viewMode || disableSimpan || masterOPL.length == 0\" skip-enable ng-click=\"saveFinalOpl()\" style=\"width:100px\">Simpan</div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpl.html\"><a>\r" +
    "\n" +
    "                    <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                    <span ng-bind-html=\"match.model.Name | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                    <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                    <span ng-bind-html=\"match.model.isExternalDesc | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                    <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "                </a></script> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/customerIndex.html',
    "<div class=\"col-md-12\"> <div class=\"row\"> <uib-accordion close-others=\"oneAtATime\"> <form> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(3)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Pelanggan & Kendaraan </div> </uib-accordion-heading> <div class=\"panel panel-default\"> <!-- <div class=\"panel-body\"> --> <div class=\"panel-body\"> <div class=\"row\" id=\"rowCustDisableCompleted\" style=\"margin-bottom:20px\"> <div class=\"col-md-4\" id=\"informasiCustomer\"> <!-- <select ng-show=\"mode == 'search' || mode == 'form'\" class=\"form-control\" ng-model=\"action\" ng-change=\"changeOwner(action)\">\r" +
    "\n" +
    "                                        <option value=\"1\">Ganti Pemilik dengan ToyotaID</option>\r" +
    "\n" +
    "                                        <option value=\"2\">Ganti Pemilik dengan Input Data</option>\r" +
    "\n" +
    "                                    </select> --> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <select ng-show=\"mode == 'search' || mode == 'form'\" class=\"form-control\" ng-model=\"action\" ng-change=\"changeOwner(selected)\">\r" +
    "\n" +
    "                                            <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                                            <option value=\"2\">Input Pemilik Data Baru</option>\r" +
    "\n" +
    "                                        </select>\r" +
    "\n" +
    "                                        <select ng-show=\"mode=='view'\" class=\"form-control\" ng-model=\"action\">\r" +
    "\n" +
    "                                            <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                                            <option value=\"2\">input Pemilik Data Baru</option>\r" +
    "\n" +
    "                                        </select> --> <!-- <bsselect ng-show=\"mode == 'search' || mode == 'form'\" on-select=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                                        </bsselect> --> <!-- <bsselect ng-show=\"mode=='view'\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                                        </bsselect> --> <select convert-to-number class=\"form-control\" ng-show=\"mode == 'search' || mode == 'form'\" on-select=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"actionCdata in actionCdata\" value=\"{{actionCdata.Id}}\">{{actionCdata.Value}}</option> </select> <select convert-to-number class=\"form-control\" ng-show=\"mode=='view'\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"actionCdata in actionCdata\" value=\"{{actionCdata.Id}}\">{{actionCdata.Value}}</option> </select> </div> </div> <div ng-show=\"mode == 'search'\" class=\"col-md-4\"> <!--<input type=\"text\" class=\"form-control\" name=\"search\" ng-hide=\"true\" placeholder=\"\" ng-model=\"text\" ng-maxlength=\"50\" style=\"display:inline;width:190px;\"> &nbsp;&nbsp;\r" +
    "\n" +
    "                                        <button class=\"rbtn btn\" style=\"display:inline;\" ng-hide=\"action == 2 || action == null\" ng-click=\"searchCustomer(action, toyotaID, policeNumber)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button>--> </div> <div class=\"col-md-4\"> <div class=\"btn-group pull-right\"> <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-disabled=\"(mFilterCust.action == null || mFilterCust.action == undefined)\" skip-enable ng-show=\"editenable && mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> <div class=\"row\" ng-show=\"mode == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null || mFilterCust.action == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-3\"> <label>Toyota ID</label> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" ng-model=\"filterSearch.TID\" placeholder=\"Input Toyota ID\" disallow-space> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelected}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterField\"> <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag != 6)\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" ng-disabled=\"filterSearch.choice\" disallow-space> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag == 6)\"> <bsdatepicker date-options=\"dateOptionsFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null\" ng-click=\"searchCustomer(mFilterCust.action, filterSearch)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div ng-show=\"mode == 'search'\" class=\"panel-body\"> <div class=\"row\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\" ng-show=\"mFilterCust.action == 2 || mFilterCust.action == 3 \"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Pemilik</bsreqlabel> <select class=\"form-control\" convert-to-number ng-model=\"mDataCrm.Customer.CustomerTypeId\" ng-change=\"changeType(mDataCrm.Customer.CustomerTypeId)\"> <!-- <option value=\"3\">Individu</option>\r" +
    "\n" +
    "                                            <option value=\"4\">Pemerintah</option>\r" +
    "\n" +
    "                                            <option value=\"5\">Yayasan</option>\r" +
    "\n" +
    "                                            <option value=\"6\">Perusahaan</option> --> <option ng-repeat=\"item in tipePemilik\" value=\"{{item.Id}}\">{{item.Name}}</option> </select> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldInst\" ng-form=\"listInst\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 && mDataCrm.Customer.CustomerTypeId != null\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" name=\"Name\" placeholder=\"Nama Institusi\" required> <em ng-messages=\"listInst.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em></div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" name=\"NamePic\" placeholder=\"Nama PIC\" required maxlength=\"50\"> <em ng-messages=\"listInst.NamePic.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 && mDataCrm.Customer.CustomerTypeId != null\">PIC Department</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">PIC Yayasan</bsreqlabel> <select name=\"PICDepartmentId\" convert-to-number class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\" required> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" name=\"PICDepartment\" placeholder=\"PIC Department\" required> --> <em ng-messages=\"listInst.PICDepartmentId.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\" name=\"PICHp\" maxlength=\"15\" required> <em ng-messages=\"listInst.PICHp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 && mDataCrm.Customer.CustomerTypeId != null\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" class=\"form-control\" maxlength=\"20\" required ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" style=\"text-transform: uppercase\"> <em ng-messages=\"listInst.SIUP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No NPWP</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" name=\"NpwpInst\" ng-model=\"mDataCrm.Customer.Npwp\" required=\"\" placeholder=\"No NPWP\" minlength=\"20\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase;\"> --> <input type=\"text\" class=\"form-control\" name=\"NpwpInst\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No NPWP\" minlength=\"20\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listInst.NpwpInst.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" form-name=\"formAddress\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" custom-button-settings=\"listCustomButtonSettingsAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT/RW : {{ lmItem.RT }}/{{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select name=\"contact\" class=\"form-control\" convert-to-number ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Propinsi...\" on-select=\"selectProvince(selected)\" icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Propinsi...\" ng-change=\"selectProvince(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"provinsiData in provinsiData\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"kabupatenData in kabupatenData\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.CityRegencyId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"kecamatanData in kecamatanData\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.DistrictId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kelurahan</bsreqlabel> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"kelurahanData in kelurahanData\" value=\"{{kelurahanData.VillageId}}\">{{kelurahanData.VillageName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.VillageId\" hidden required> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat ToyotaID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                    <option ng-repeat=\"flagToyotaID in flagToyotaID\" value=\"{{flagToyotaID.Id}}\">{{flagToyotaID.Desc}}</option>\r" +
    "\n" +
    "                                                </select> --> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"AFCOIdFlag\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"AFCOIdFlag\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"AFCOIdFlag\" class=\"form-control\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"AFCOIdFlag\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                    <option ng-repeat=\"AFCOIdFlag in AFCOIdFlag\" value=\"{{AFCOIdFlag.Id}}\">{{AFCOIdFlag.Desc}}</option>\r" +
    "\n" +
    "                                                </select> --> <em ng-messages=\"listInst.AFCOIdFlag.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listInst.$invalid || CustomerAddress.length == 0\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus()\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldPersonal\" ng-form=\"listPersonal\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" placeholder=\"Gelar Depan\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"Name\" ng-model=\"mDataCrm.Customer.Name\" required placeholder=\"Nama\" maxlength=\"50\"> <em ng-messages=\"listPersonal.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" placeholder=\"Gelar Belakang\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Lahir</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker max-date=\"maxDateBirth\" name=\"BirthDate\" ng-model=\"mDataCrm.Customer.BirthDate\" date-options=\"dateOptions\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(mDataCrm.Customer.BirthDate)\" required> </bsdatepicker> <em ng-messages=\"listPersonal.BirthDate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" name=\"HandPhone1\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" placeholder=\"Handphone 1\" maxlength=\"15\" numbers-only required> <em ng-messages=\"listPersonal.HandPhone1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" placeholder=\"Handphone 2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\" ng-required=\"false\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No KTP/KITAS</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" name=\"KTPKITAS\" ng-model=\"mDataCrm.Customer.KTPKITAS\" placeholder=\"No KTP/KITAS\" required=\"\" numbers-only maxlength=\"16\" style=\"text-transform: uppercase;\"> --> <input type=\"text\" class=\"form-control\" name=\"KTPKITAS\" ng-model=\"mDataCrm.Customer.KTPKITAS\" placeholder=\"No KTP/KITAS\" numbers-only maxlength=\"16\" style=\"text-transform: uppercase\"> <em ng-messages=\"listPersonal.KTPKITAS.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No NPWP</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" name=\"Npwp\" ng-model=\"mDataCrm.Customer.Npwp\" required=\"\" placeholder=\"No NPWP\" minlength=\"20\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase;\"> --> <input type=\"text\" class=\"form-control\" name=\"Npwp\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No NPWP\" minlength=\"20\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listPersonal.Npwp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" form-name=\"formAddress\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-show-detail=\"onShowDetailAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" custom-button-settings=\"listCustomButtonSettingsAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT/RW : {{ lmItem.RT }}/{{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"categoryContact in categoryContact\" ng-selected=\"lmModelAddress.AddressCategoryId == categoryContact.AddressCategoryId\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Propinsi...\" on-select=\"selectProvince(selected)\" icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"propinsi\" id=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Propinsi...\" ng-change=\"selectProvince(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"provinsiData in provinsiData\" ng-selected=\"lmModelAddress.ProvinceId == provinsiData.ProvinceId\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\"  ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"kabupaten\" id=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"kabupatenData in kabupatenData\" ng-selected=\"lmModelAddress.CityRegencyId == kabupatenData.CityRegencyId\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.CityRegencyId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"kecamatan\" id=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"kecamatanData in kecamatanData\" ng-selected=\"lmModelAddress.DistrictId == kecamatanData.DistrictId\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.DistrictId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kelurahan</bsreqlabel> <!-- <bsselect name=\"kelurahan\" id=\"kecamatan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"kelurahanData in kelurahanData\" ng-selected=\"lmModelAddress.VillageId == kelurahanData.VillageId\" value=\"{{kelurahanData.VillageId}}\">{{kelurahanData.VillageName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.VillageId\" hidden required> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat ToyotaID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                    <option ng-repeat=\"flagToyotaID in flagToyotaID\" value=\"{{flagToyotaID.Id}}\">{{flagToyotaID.Desc}}</option>\r" +
    "\n" +
    "                                                </select> --> <em ng-messages=\"listPersonal.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"AFCOIdFlag\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"AFCOIdFlag\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"AFCOIdFlag\" class=\"form-control\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"AFCOIdFlag\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                    <option ng-repeat=\"AFCOIdFlag in AFCOIdFlag\" value=\"{{AFCOIdFlag.Id}}\">{{AFCOIdFlag.Desc}}</option>\r" +
    "\n" +
    "                                                </select> --> <em ng-messages=\"listPersonal.AFCOIdFlag.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"(mode=='form' || mode=='search')\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listPersonal.$invalid || CustomerAddress.length == 0\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus()\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px;\">\r" +
    "\n" +
    "                                    <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress, mode)\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button>\r" +
    "\n" +
    "                                    <button type=\"button\" ng-click=\"batalEditcus()\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div> --> </div> <!-- untuk tampilan depan --> <div class=\"panel-body\" ng-hide=\"mode =='search'\"> <div class=\"row\" ng-show=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 && mDataCrm.Customer.CustomerTypeId != null\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>PIC Department</bsreqlabel> --> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 && mDataCrm.Customer.CustomerTypeId != null\">PIC Department</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">PIC Yayasan</bsreqlabel> <!-- <input type=\"text\" ng-readonly='true' class=\"form-control\" ng-readonly='true' ng-model=\"mDataCrm.Customer.PICDepartment\" placeholder=\"PIC Department\"> --> <select name=\"picDepartment\" convert-to-number ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\"> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"mDataCrm.Customer.PICHp\" maxlength=\"15\" placeholder=\"PIC HP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 && mDataCrm.Customer.CustomerTypeId != null\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" style=\"text-transform: uppercase\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\"> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address1\" form-name=\"formAddress\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\" ng-disabled=\"true\" skip-enabled> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT/RW : {{ lmItem.RT }}/{{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Propinsi...\" on-select=\"selectProvince(selected)\" icon=\"fa fa-home\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Propinsi...\" ng-change=\"selectProvince(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"provinsiData in provinsiData\" ng-selected=\"lmModelAddress.ProvinceId == provinsiData.ProvinceId\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-home\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <input type=\"text\" name=\"kabupaten\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.CityRegencyName\" required> <!-- <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                    <option ng-repeat=\"kabupatenData in kabupatenData\" ng-selected=\"lmModelAddress.CityRegencyId == kabupatenData.CityRegencyId\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option>\r" +
    "\n" +
    "                                                                </select> --> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-home\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <input type=\"text\" name=\"kecamatan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.DistrictName\" required> <!-- <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                    <option ng-repeat=\"kecamatanData in kecamatanData\" ng-selected=\"lmModelAddress.DistrictId == kecamatanData.DistrictId\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option>\r" +
    "\n" +
    "                                                                </select> --> </div> <div class=\"form-group\"> <bsreqlabel>Kelurahan</bsreqlabel> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-home\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <input type=\"text\" name=\"kelurahan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.VillageName\" required> <!-- <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                    <option ng-repeat=\"kelurahanData in kelurahanData\" ng-selected=\"lmModelAddress.VillageId == kelurahanData.VillageId\" value=\"{{kelurahanData.VillageId}}\">{{kelurahanData.VillageName}}</option>\r" +
    "\n" +
    "                                                                </select> --> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> </div> </div> <div class=\"row\" ng-hide=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" ng-readonly=\"true\" placeholder=\"Gelar Depan\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" ng-readonly=\"true\" placeholder=\"Nama\"> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" ng-readonly=\"true\" placeholder=\"Gelar Belakang\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" ng-readonly=\"true\" numbers-only> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" ng-readonly=\"true\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-required=\"false\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.KTPKITAS\" ng-readonly=\"true\" minlength=\"16\" maxlength=\"16\" placeholder=\"No. KTP/KITAS\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" ng-readonly=\"true\" placeholder=\"No. NPWP\" name=\"Npwp\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist form-name=\"formAddress\" name=\"address1\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\" ng-disabled=\"true\" skip-enabled> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT/RW : {{ lmItem.RT }}/{{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Propinsi...\" on-select=\"selectProvince(selected)\" icon=\"fa fa-home\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Propinsi...\" ng-change=\"selectProvince(lmModelAddress)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"provinsiData in provinsiData\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-home\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <input type=\"text\" name=\"kabupaten\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.CityRegencyName\" required> <!-- <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                    <option ng-repeat=\"kabupatenData in kabupatenData\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option>\r" +
    "\n" +
    "                                                                </select> --> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-home\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <input type=\"text\" name=\"kecamatan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.DistrictName\" required> <!-- <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                    <option ng-repeat=\"kecamatanData in kecamatanData\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option>\r" +
    "\n" +
    "                                                                </select> --> </div> <div class=\"form-group\"> <bsreqlabel>Kelurahan</bsreqlabel> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                                    icon=\"fa fa-home\" required=true>\r" +
    "\n" +
    "                                                                </bsselect> --> <input type=\"text\" name=\"kelurahan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.VillageName\" required> <!-- <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" icon=\"fa fa-taxi\" required=true>\r" +
    "\n" +
    "                                                                    <option ng-repeat=\"kelurahanData in kelurahanData\" value=\"{{kelurahanData.VillageId}}\">{{kelurahanData.VillageName}}</option>\r" +
    "\n" +
    "                                                                </select> --> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> </div> </div> </div> <hr width=\"95%\" style=\"height:3px;background-color:grey\"> <div class=\"panel-body\"> <div class=\"row\" id=\"rowVehicDisableCompleted\" style=\"margin-bottom:10px\"> <div id=\"informasiKendaraan\" class=\"col-md-4\"> <!--                                     <select ng-show=\"modeV == 'search' || modeV == 'form'\" class=\"form-control\" ng-model=\"actionV\" ng-change=\"changeVehic(actionV)\">\r" +
    "\n" +
    "                                        <option value=\"1\">Cari Vehicle</option>\r" +
    "\n" +
    "                                        <option value=\"2\">Input Data Vehicle Baru</option>\r" +
    "\n" +
    "                                    </select> --> <!-- <bsselect ng-show=\"modeV == 'search' || modeV == 'form'\" on-select=\"changeVehic(mFilterVehic.actionV)\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                                    </bsselect> --> <select convert-to-number class=\"form-control\" ng-show=\"modeV == 'search' || modeV == 'form'\" ng-change=\"changeVehic(mFilterVehic.actionV)\" data=\"actionVdata\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"actionVdata in actionVdata\" value=\"{{actionVdata.Id}}\">{{actionVdata.Value}}</option> </select> <!-- <bsselect ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                                    </bsselect> --> <select convert-to-number class=\"form-control\" ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" ng-change=\"changeKendaraan(mFilterVehic.actionV)\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"actionVdata in actionVdata\" value=\"{{actionVdata.Id}}\">{{actionVdata.Value}}</option> </select> </div> <div ng-show=\"modeV == 'search'\" class=\"col-md-4\"> <!--<input type=\"text\" class=\"form-control\" name=\"Km\" placeholder=\"\" ng-model=\"textV\" ng-hide=\"actionV == 3 || actionV == null\" ng-maxlength=\"50\" style=\"display:inline;width:190px;\"> &nbsp;&nbsp;\r" +
    "\n" +
    "                                        <button class=\"rbtn btn\" style=\"display:inline;\" ng-hide=\"actionV == 3 || actionV == null\" ng-click=\"searchVehicle(actionV, textV)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button>--> </div> <div class=\"col-md-4\"> <div class=\"btn-group pull-right\"> <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" ng-disabled=\"(mFilterVehic.actionV == null || mFilterVehic.actionV == undefined)\" skip-enable ng-show=\"editenable && modeV=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> </div> <div class=\"row\" ng-show=\"modeV == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null || mFilterVehic.actionV == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelectedV}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterFieldV\"> <a href=\"#\" ng-click=\"filterFieldChangeV(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearchV.filterValue\" ng-maxlength=\"17\" maxlength=\"17\" ng-disabled=\"filterSearchV.choice\" disallow-space> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null\" ng-click=\"searchVehicle(mFilterVehic.actionV, filterSearchV)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <!--rizk--> <div class=\"panel-body\" ng-form=\"listVehicle\" ng-hide=\"!hideVehic\" novalidate> <div class=\"col-md-12\" ng-show=\"!disVehic\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tipe Kendaraan</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changeTypeVehic1\" ng-change=\"checkTam(mFilterTAM.isTAM)\" ng-disabled=\"disableCheckTam\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"1\" checked> <label for=\"changeTypeVehic1\">TAM</label> <input ng-change=\"checkTam(mFilterTAM.isTAM)\" type=\"radio\" id=\"changeTypeVehic2\" ng-disabled=\"disableCheckTam\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"0\"> <label for=\"changeTypeVehic2\">Non TAM</label> </div> </div> </div> </div> <div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Polisi</bsreqlabel> <input name=\"noPolice\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-model=\"mDataCrm.Vehicle.LicensePlate\" ng-maxlength=\"10\" ng-keypress=\"allowPatternFilter($event,3,filter.noPolice)\" placeholder=\"No Polisi\" required> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Rangka</bsreqlabel> <input name=\"VIN\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null) || mData.Status >= 4\" skip-enable ng-model=\"mDataCrm.Vehicle.VIN\" placeholder=\"No Rangka\" required ng-maxlength=\"17\" ng-minlength=\"12\" maxlength=\"17\"> <em ng-messages=\"listVehicle.VIN.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                <bsreqlabel>Model Code</bsreqlabel>\r" +
    "\n" +
    "                                                <input type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ModelCode\" placeholder=\"Model Code\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div> --> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <!-- <bsselect data=\"VehicM\" name=\"vehicModel\" on-select=\"selectedModel(selected)\" item-value=\"VehicleModelId\" item-text=\"VehicleModelName\" ng-model=\"mDataCrm.Vehicle.Model\" placeholder=\"Vehicle Model\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select name=\"vehicModel\" ng-if=\"mFilterTAM.isTAM == 1 && mDataCrm.Vehicle.SourceDataId !== null\" ng-model=\"mDataCrm.Vehicle.Model\" data=\"VehicM\" on-select=\"selectedModel($item)\" ng-required=\"true\" search-enabled=\"false\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Model\"> {{$select.selected.VehicleModelName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicM\"> <span ng-bind-html=\"item.VehicleModelName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelName\" maxlength=\"18\" required name=\"vehicModel\" placeholder=\"Model Type\"> <input type=\"text\" ng-disabled=\"disVehic || mDataCrm.Vehicle.SourceDataId == null || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 1 && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.Model.VehicleModelName\" required name=\"vehicModel\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <!--<bsselect name=\"vehicType\" data=\"VehicT\" on-select=\"selectedType(selected)\" item-value=\"VehicleTypeId\" item-text=\"NewDescription\" ng-model=\"mDataCrm.Vehicle.Type\" placeholder=\"Vehicle Type\" ng-readonly=\"enableType\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <!-- <ui-select data=\"selectedType\" name=\"vehicType\" ng-if=\"mFilterTAM.isTAM == 1\" ng-model=\"mDataCrm.Vehicle.Type\" on-select=\"selectedType($item)\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\">\r" +
    "\n" +
    "                                                <ui-select-match allow-clear placeholder=\"Pilih Tipe Kendaraan\">\r" +
    "\n" +
    "                                                    {{$select.selected.Description}}\r" +
    "\n" +
    "                                                </ui-select-match>\r" +
    "\n" +
    "                                                <ui-select-choices repeat=\"item in VehicT\">\r" +
    "\n" +
    "                                                    <span ng-bind-html=\"item.Description\"></span>\r" +
    "\n" +
    "                                                </ui-select-choices>\r" +
    "\n" +
    "                                            </ui-select> --> <ui-select data=\"VehicT\" name=\"vehicType\" ng-if=\"mFilterTAM.isTAM == 1 && mDataCrm.Vehicle.SourceDataId !== null\" ng-model=\"mDataCrm.Vehicle.Type\" on-select=\"selectedType($item)\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.NewDescription}} </ui-select-match> <ui-select-choices repeat=\"item in VehicT\"> <span ng-bind-html=\"item.NewDescription\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelType\" required name=\"vehicType\" placeholder=\"Model Type\"> <input type=\"text\" ng-disabled=\"disVehic || mDataCrm.Vehicle.SourceDataId == null || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 1 && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.Type.NewDescription\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicType.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-readonly=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Warna\"> --> <!-- <bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"ColorName\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select name=\"colorMobil\" ng-model=\"mDataCrm.Vehicle.Color\" data=\"VehicColor\" on-select=\"selectedColor($item.MColor)\" ng-if=\"mFilterTAM.isTAM == 1\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Warna\"> {{$select.selected.ColorName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicColor\"> <span ng-bind-html=\"item.MColor.ColorName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ColorName\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\" ng-show=\"isCustomColor\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna Lain</bsreqlabel> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable class=\"form-control\" ng-model=\"mData.VehicleColor\" placeholder=\"Warna\"> <!--<bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"VehicleColor\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <!-- <div class=\"col-md-6\" ng-if=\"mFilterTAM.isTAM == 1\"> --> <div class=\"form-group\" show-errors> <bsreqlabel ng-if=\"mFilterTAM.isTAM == 1\">Kode Warna</bsreqlabel> <label ng-if=\"mFilterTAM.isTAM == 0\">Kode Warna</label> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCode\" placeholder=\"Kode Warna\" required> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCodeNonTAM\" placeholder=\"Kode Warna\"> <em ng-messages=\"listVehicle.colorCode.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bahan Bakar</bsreqlabel> <bsselect data=\"VehicGas\" on-select=\"selectedGas(selected)\" item-value=\"VehicleGasTypeId\" name=\"fuel\" item-text=\"VehicleGasTypeName\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required> </bsselect> <!-- <select class=\"form-control\" data=\"VehicGas\" on-select=\"selectedGas(selected)\" name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                                <option ng-repeat=\"VehicGas in VehicGas\" value=\"{{VehicGas.VehicleGasTypeId}}\">{{VehicGas.VehicleGasTypeName}}</option>\r" +
    "\n" +
    "                                            </select> --> <!-- <ui-select name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" on-select=\"selectedGas($item)\" item-text=\"VehicleGasTypeName\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\">\r" +
    "\n" +
    "                                                <ui-select-match allow-clear placeholder=\"Vehicle Gas Type\">\r" +
    "\n" +
    "                                                    {{$select.selected.VehicleGasTypeName}}\r" +
    "\n" +
    "                                                </ui-select-match>\r" +
    "\n" +
    "                                                <ui-select-choices repeat=\"item in VehicGas\">\r" +
    "\n" +
    "                                                    <span ng-bind-html=\"item.VehicleGasTypeName\"></span>\r" +
    "\n" +
    "                                                </ui-select-choices>\r" +
    "\n" +
    "                                            </ui-select> --> <em ng-messages=\"listVehicle.fuel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tahun Rakit</bsreqlabel> <input name=\"tahunRakit\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable numbers-only ng-model=\"mDataCrm.Vehicle.AssemblyYear\" placeholder=\"Tahun Rakit\" required> <em ng-messages=\"listVehicle.tahunRakit.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Full Model</bsreqlabel> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCode\" placeholder=\"Katashiki Code\" required> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" ng-disabled=\"disVehic\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCodeNonTAM\" placeholder=\"Katashiki Code\" required> <em ng-messages=\"listVehicle.fullModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Mesin</bsreqlabel> <input type=\"text\" name=\"NoMesin\" maxlength=\"20\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable ng-model=\"mDataCrm.Vehicle.EngineNo\" placeholder=\"No. Mesin\" required> <em ng-messages=\"listVehicle.NoMesin.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Asuransi</label> <input type=\"text\" name=\"Asuransi\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Insurance\" placeholder=\"Asuransi\"> <em ng-messages=\"listVehicle.Asuransi.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Polis Asuransi</label> <input type=\"text\" name=\"noSPK\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic|| (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable ng-model=\"mDataCrm.Vehicle.SPK\" placeholder=\"No. Polis Asuransi\"> <em ng-messages=\"listVehicle.noSPK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal DEC</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!--<input type=\"text\" class=\"form-control\"  ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal DEC\">--> <!-- {{disVehic}} --> <bsdatepicker name=\"DateDEC\" ng-model=\"mDataCrm.Vehicle.DECDate\" ng-if=\"disVehic == false\" date-options=\"dateDECOptions\" max-date=\"maxDateOption.maxDate\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" ng-disabled=\"disVehic\" skip-enable required> </bsdatepicker> <input name=\"DateDEC\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal Lahir\" required> <!-- \r" +
    "\n" +
    "                                            <bsdatepicker name=\"DateDEC\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" max-date='maxDateOptionDECLC.maxDate' date-options=\"dateDECOptions\" ng-change=\"dateChange(dt)\" required>\r" +
    "\n" +
    "                                            </bsdatepicker> --> <em ng-messages=\"listVehicle.DateDEC.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama STNK</bsreqlabel> <input type=\"text\" name=\"nameStnk\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKName\" placeholder=\"Nama STNK\" required> <em ng-messages=\"listVehicle.nameStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat STNK</bsreqlabel> <input name=\"addressStnk\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKAddress\" placeholder=\"Alamat STNK\" required> <em ng-messages=\"listVehicle.addressStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Masa Berlaku STNK</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"dateSTNK\" ng-disabled=\"disVehic\" ng-if=\"disVehic == false\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" date-options=\"dateSTNKOptions\" placeholder=\"Masa Berlaku STNK\" required> </bsdatepicker> <input name=\"dateSTNK\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" placeholder=\"Masa Berlaku STNK\" required> <em ng-messages=\"listVehicle.dateSTNK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"modeV=='form' || modeV=='search'\" ng-click=\"saveVehic(mFilterVehic.actionV, mDataCrm, mFilterTAM.isTAM, mDataCrm.Vehicle.DECDate);\" ng-disabled=\"listVehicle.$invalid\" skip-enable class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <button type=\"button\" ng-click=\"cancelSearchV()\" ng-show=\"modeV=='form' || modeV=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> <!-- </div> --> </div> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-click=\"cancelSearchV()\" ng-show=\"modeV=='search' && hideVehic == false\" class=\"wbtn btn\">Batal</button> </div> <!--end rizk--> </div> </div> <div uib-accordion-group class=\"panel-default\" ng-show=\"showInst\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Penanggung Jawab </div> </uib-accordion-heading> <div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPnj()\" ng-hide=\"!disPnj\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"savePnj()\" ng-show=\"!disPnj\" class=\"rbtn btn\">Save</button> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Lengkap</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" ng-readonly=\"disPnj\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" ng-readonly=\"disPnj\" numbers-only> </div> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(5)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Pengguna </div> </uib-accordion-heading> <div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPng(mData.US)\" ng-hide=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelPng(mData.US)\" ng-show=\"!disPng\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"savePng(mData.US, mDataCrm, CustomerVehicleId)\" ng-show=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" ng-show=\"!disPng\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-show=\"!hiddenUserList\"> <label>List Pengguna</label> <!-- <bsselect ng-model=\"mData.US\" data=\"pngList\" item-text=\"name\" item-value=\"vehicleUserId\" placeholder=\"Pilih Pengguna\" on-select=\"onChangePng(selected)\">\r" +
    "\n" +
    "                                </bsselect> --> <!-- <select class=\"form-control\" ng-model=\"mData.US\" data=\"pngList\" placeholder=\"Pilih Pengguna\" on-select=\"onChangePng(selected)\">\r" +
    "\n" +
    "                                    <option ng-repeat=\"pngList in pngList\" value=\"{{pngList.vehicleUserId}}\">{{pngList.name}}</option>\r" +
    "\n" +
    "                                </select> --> <select class=\"form-control\" ng-model=\"mData.US\" data=\"pngList\" placeholder=\"Pilih Pengguna\" ng-change=\"onChangePng(mData.US)\"> <option ng-repeat=\"item in pngList\" ng-selected=\"mData.US.VehicleUserId == item.VehicleUserId\" value=\"{{item.VehicleUserId}}\">{{item.name}}</option> </select> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Lengkap</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mData.US.name\" ng-readonly=\"disPng\"> <input type=\"text\" class=\"form-control\" ng-model=\"mData.US.vehicleUserId\" ng-hide=\"true\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" name=\"HP1\" numbers-only ng-maxlength=\"15\" maxlength=\"15\" class=\"form-control\" ng-model=\"mData.US.phoneNumber1\" ng-readonly=\"disPng\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" name=\"HP2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-maxlength=\"15\" maxlength=\"15\" class=\"form-control\" ng-model=\"mData.US.phoneNumber2\" ng-readonly=\"disPng\" ng-required=\"false\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Hubungan dengan Pemilik</bsreqlabel> <!-- <bsselect data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" item-value=\"RelationId\" item-text=\"RelationDesc\" ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable>\r" +
    "\n" +
    "                                    </bsselect> --> <!-- <select class=\"form-control\" data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable>\r" +
    "\n" +
    "                                        <option ng-repeat=\"dataRelationship in dataRelationship\" value=\"{{dataRelationship.RelationId}}\">{{dataRelationship.RelationDesc}}</option>\r" +
    "\n" +
    "                                    </select> --> <select convert-to-number class=\"form-control\" id=\"RelationshipID\" data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable> <option ng-repeat=\"dataRelationship in dataRelationship\" ng-selected=\"mData.US.Relationship == dataRelationship.RelationId\" value=\"{{dataRelationship.RelationId}}\">{{dataRelationship.RelationDesc}}</option> </select> </div> </div> </div> </div> <!-- <div class=\"row\" ng-show=\"!disPng\">\r" +
    "\n" +
    "                        <div class=\"btn-group pull-right\">\r" +
    "\n" +
    "                            <button type=\"button\" class=\"rbtn btn\" ng-click=\"addPengguna()\">\r" +
    "\n" +
    "                            <i class=\"fa fa-fw fa-plus\"></i>&nbsp;Pengguna\r" +
    "\n" +
    "                            </button>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div> --> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(6)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Kontak Person & Pengambil Keputusan </div> </uib-accordion-heading> <div class=\"col-md-12\"> <!--<div class=\"row\">\r" +
    "\n" +
    "                            <div class=\"col-md-12 text-right\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>--> <div class=\"row\"> <div smart-include=\"app/services/reception/wo_bp/includeForm/CPPKInfo.html\"> </div> </div> </div> </div> </form> </uib-accordion> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/estimation.html',
    "<div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-4\" style=\"padding-left:0px\"> <div class=\"form-group\"> <bsreqlabel>Nomor Estimasi</bsreqlabel> <input type=\"text\" min=\"0\" class=\"form-control\" name=\"NomorWo\" ng-model=\"mData.EstimationNo\" ng-disabled=\"true\" skip-enable disabled> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Estimation Date</bsreqlabel> <div class=\"form-group\" style=\"width:250px\"> <bsdatepicker name=\"estimationDate\" date-options=\"DateOptions\" ng-model=\"mData.EstimationDate\"> </bsdatepicker> <bserrmsg field=\"estimationDate\"></bserrmsg> </div> <!--<div class=\"col-md-3\">\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"col-md-9 form-inline\">\r" +
    "\n" +
    "                        </div>--> </div> </div> </div> </div> <!--             <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"btn-group pull-right\">\r" +
    "\n" +
    "                    <button type=\"button\" class=\"rbtn btn\">Buat Estimasi Baru</button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> <!-- <bstabwiz fullwidth=\"true\" on-finish=\"isFinished = true\" current-step=\"currentStep\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <bstabwiz-pane title=\"Kendaraan\" desc=\"Data Informasi Kendaraan\">\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                </bstabwiz-pane>\r" +
    "\n" +
    "                <bstabwiz-pane title=\"Mantap 1\" desc=\"Data Informasi Kendaraan 1\">\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                </bstabwiz-pane>\r" +
    "\n" +
    "                <bstabwiz-pane title=\"Mantap 2\" desc=\"Data Informasi Kendaraan 2\">\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                </bstabwiz-pane>\r" +
    "\n" +
    "                <bstabwiz-pane title=\"Mantap 3\" desc=\"Data Informasi Kendaraan3 \">\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                </bstabwiz-pane>\r" +
    "\n" +
    "            </bstabwiz> --> <uib-tabset active=\"activeJustified\" justified=\"true\" style=\"margin-top: 40px\"> <uib-tab class=\"headerEstimation\" ng-repeat=\"header in headerEst\" index=\"$index\" ng-click=\"getTimeEst($index+1)\" disable=\"isNextTab.priceEstimation && $index == 1\" heading=\"{{header.name}}\"> <div ng-if=\"$index == 0\"> <!-- <h1>Vehicle Condition Detail Here</h1> --> <!-- added by sss on 2017-10-11 --> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Type Kendaraan</bsreqlabel> <!-- <bsselect name=\"filterType\" ng-model=\"filter.vTypeVC\" data=\"VTypeData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Type Kendaraan\" on-select=\"chooseVTypeVC(selected)\" icon=\"fa fa-car\">\r" +
    "\n" +
    "                                        </bsselect> --> <!-- <i class=\"fa fa-car\"></i> --> <!-- <select name=\"filterType\" class=\"form-control\" ng-model=\"filter.vTypeVC\" convert-to-number item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Type Kendaraan\" ng-change=\"chooseVTypeVC(filter.vTypeVC)\" icon=\"fa fa-car\">\r" +
    "\n" +
    "                                            <option value=\"\"></option>\r" +
    "\n" +
    "                                            <option ng-repeat=\"VTypeData in VTypeData\" value=\"{{VTypeData.MasterId}}\">{{VTypeData.Name}}</option>\r" +
    "\n" +
    "                                        </select> --> <div class=\"icon-addon left-addon ui-select\"><i class=\"fa fa-car\"></i> <ui-select name=\"vehicType\" ng-model=\"filter.vTypeVC\" icon=\"fa fa-car\" on-select=\"chooseVTypeVC($item.MasterId)\" search-enabled=\"false\" skip-enable theme=\"select2\" id=\"EstimasiType\"> <ui-select-match style=\"padding-left:30px\" allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.Name}} </ui-select-match> <ui-select-choices repeat=\"item in VTypeData\"> <span ng-bind-html=\"item.Name\"></span> </ui-select-choices> </ui-select> </div> </div> </div> </div> <!-- <button ng-click=\"refreshWAC()\" class=\"rbtn btn\" >Refresh Gambar</button> --> <div class=\"row\" ng-if=\"refresh && imageSelVC != null\"> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArrayVC\" ng-areas-width=\"680\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSelVC}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClickVC\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> <!-- <pre>{{logDots}}</pre>  --> </div> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"!isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArrayVC\" ng-areas-width=\"800\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSelVC}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClickVC\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> <!-- <pre>{{logDots}}</pre>  --> </div> </div> <div class=\"row\" ng-show=\"!disableWACExtVC\"> <div class=\"col-md-12\"> <!-- <div ui-grid=\"gridVC\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellNav ui-grid-pinning ui-grid-auto-resize ui-grid-edit class=\"grid\" ng-style=\"getTableHeight()\">\r" +
    "\n" +
    "                                        <div class=\"ui-grid-nodata\" ng-if=\"!gridVC.data.length && !loading\" ng-cloak>No data available</div>\r" +
    "\n" +
    "                                        \r" +
    "\n" +
    "                                    </div> --> <table id=\"estGridVc\" width=\"100%\" class=\"table table-striped table-bordered table-hover table-responsive\"> <thead> <tr> <th ng-style=\"{'width':'{{col.width}}'}\" ng-hide=\"col.visible == false\" ng-repeat=\"col in gridVC.columnDefs\">{{col.name}}</th> </tr> </thead> <tbody> <tr ng-repeat=\"column in gridVC.data\"> <td ng-hide=\"true\" style=\"width:5%\">{{column.JobWACExtId}}</td> <td style=\"width:28%\">{{column.ItemName}}</td> <td style=\"width:47%\" style=\"padding:0px\"> <div class=\"col-md-12 col-xs-12\"> <div ng-repeat=\"item in StatusVehicle\" style=\"float:left;padding-right:15px;padding-left:15px\"> <label class=\"radio\"> <input class=\"radiobox style-0\" ng-model=\"column.StatusVehicleId\" type=\"radio\" ng-change=\"checkTaskByVehicleCondtion(gridVC.data,1,column.StatusVehicleId)\" ng-value=\"{{item.MasterId}}\"><span>{{item.Name}}</span> </label> </div> </div> </td> <td style=\"width:25%\" style=\"padding:0px\"> <div class=\"col-md-12 col-xs-12\"> <div ng-repeat=\"item in PointsVehicle\" ng-if=\"(column.StatusVehicleId !== undefined && column.StatusVehicleId !== null && column.typeName == 1)\" style=\"float:left;padding-right:10px;padding-left:10px\"> <label class=\"radio\"> <input class=\"radiobox style-0\" ng-model=\"column.PointId\" type=\"radio\" ng-change=\"checkTaskByVehicleCondtion(gridVC.data,0,column.PointId)\" ng-value=\"{{item.MasterId}}\"><span>{{item.Name}}</span> </label> </div> </div> </td> <td ng-hide=\"true\" style=\"width:5%\">{{column.typeName}}</td> </tr> <tr ng-if=\"!gridVC.data.length\"> <td colspan=\"{{gridVC.columnDefs.length}}\"> <div style=\"opacity: .25;font-size: 3em;width: 100%;text-align: center;z-index: 1000\">No Data Available</div> </td> </tr> </tbody> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bscheckbox ng-model=\"disableWACExtVC\" label=\"Clear\" true-value=\"true\" false-value=\"false\" ng-change=\"chgChkVC()\"> </bscheckbox> </div> </div> </div> </div> <!--  --> </div> <div ng-if=\"$index == 1\"> <uib-accordion close-others=\"oneAtATime\"> <form> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Permintaan </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <bslist form-name=\"jobEstimateRequestBuatBP\" data=\"JobRequest\" m-id=\"JobRequestId\" list-model=\"lmRequest\" on-select-rows=\"onListSelectRows\" selected-rows=\"listRequestSelectedRows\" confirm-save=\"false\" list-api=\"listApiEstimateRequest\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Permintaan\" list-title=\"Permintaan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Permintaan\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div ng-form=\"rowRequest\"> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea class=\"form-control\" ng-maxtlength=\"200\" maxlength=\"200\" placeholder=\"Permintaan\" name=\"reqdesc\" ng-model=\"lmRequest.RequestDesc\" required>\r" +
    "\n" +
    "                                                            </textarea> <em ng-messages=\"rowRequest.reqdesc.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> </bslist-modal-form> </bslist> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Keluhan </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <bslist data=\"JobComplaint\" form-name=\"jobEstimateComplaintBuatBP\" m-id=\"JobComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"fals\" list-api=\"listApiEstimateComplaint\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Keluhan\" list-title=\"Keluhan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <!-- <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Kategori:\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                            </bsselect> --> <select class=\"form-control\" data=\"vm.appScope.ComplaintCategory\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Kategori:\" ng-disabled=\"true\" skip-enable> <option ng-repeat=\"vaC in vm.appScope.ComplaintCategory\" value=\"{{vaC.ComplaintCatg}}\">{{vaC.ComplaintCatg}}</option> </select> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Deskripsi\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div ng-form=\"rowcomplaint\" style=\"margin-bottom:90px\"> <div class=\"form-group\"> <bsreqlabel>Kategori</bsreqlabel> <!-- <bsselect data=\"ComplaintCategory\" name=\"ctype\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Kategori\" required>\r" +
    "\n" +
    "                                                            </bsselect> --> <select class=\"form-control\" data=\"ComplaintCategory\" name=\"ctype\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Kategori\" required> <option ng-repeat=\"ComplaintCategory in ComplaintCategory\" value=\"{{ComplaintCategory.ComplaintCatg}}\">{{ComplaintCategory.ComplaintCatg}}</option> </select> <em ng-messages=\"rowcomplaint.ctype.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"ctype\"></bserrmsg> --> </div> <div class=\"form-group\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Deskripsi\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\" required>\r" +
    "\n" +
    "                                                            </textarea> <em ng-messages=\"rowcomplaint.ctext.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"ctext\"></bserrmsg> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Job List </uib-accordion-heading> <div class=\"row\" style=\"margin-bottom:25px\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <bsreqlabel>Kategori WO</bsreqlabel> <!-- <bsselect name=\"woCategory\" ng-model=\"mData.WoCategoryId\" data=\"woCategory\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Kategori WO\" on-select=\"selectCatWO(selected)\" icon=\"fa fa-list\">\r" +
    "\n" +
    "                                                </bsselect> --> <select class=\"form-control\" name=\"woCategory\" ng-model=\"mData.WoCategoryId\" data=\"woCategory\" placeholder=\"Pilih Kategori WO\" ng-change=\"changeCatWO(mData.WoCategoryId, woCategory)\" on-select=\"selectCatWO(selected)\" icon=\"fa fa-list\"> <option ng-repeat=\"woCategory in woCategory\" value=\"{{woCategory.MasterId}}\">{{woCategory.Name}}</option> </select> </div> <div class=\"col-md-6\"> <bsreqlabel>Kategori Perbaikan</bsreqlabel> <!-- <bsselect name=\"CatPerbaikan\" ng-model=\"mData.JobCatgForBP\" data=\"CatPerbaikan\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Kategori Perbaikan\" on-select=\"selectedCatPerbaikan(selected)\" icon=\"fa fa-list\">\r" +
    "\n" +
    "                                                </bsselect> --> <select class=\"form-control\" name=\"CatPerbaikan\" ng-model=\"mData.JobCatgForBPid\" data=\"CatPerbaikan\" placeholder=\"Pilih Kategori Perbaikan\" on-select=\"selectedCatPerbaikan(selected)\" icon=\"fa fa-list\"> <option ng-repeat=\"CatPerbaikan in CatPerbaikan\" value=\"{{CatPerbaikan.MasterId}}\">{{CatPerbaikan.Name}}</option> </select> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" id=\"jobestimasi\"> <bslist form-name=\"jobEstimateListBuatBP\" on-before-edit=\"onBeforeEditEstimate\" data=\"gridWorkEstimate\" on-before-save=\"onBeforeSaveEstimate\" on-before-new=\"onBeforeNewEstimate\" on-after-save=\"onAfterSaveEstimate\" on-after-delete=\"onAfterDeleteEstimate\" m-id=\"TaskBPId\" d-id=\"PartsId\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetailEstimate\" on-select-rows=\"onListSelectRows\" selected-rows=\"listEstimateSelectedRows\" list-api=\"listApiEstimate\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\" list-title=\"Job List\"> <bslist-template> <div class=\"col-md-1\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.NumberParent }}</strong></p> </div> <div class=\"col-md-6\" style=\"text-align:left\"> <!-- <div class=\"col-md-12\"> --> <p class=\"name\"><strong>{{ lmItem.EstimationNo }}</strong></p> <!-- </div> --> <!--<div class=\"col-xs-12 col-sm-12 col-md-12\">--> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <!--</div>\r" +
    "\n" +
    "                                                        <div class=\"col-xs-12 col-sm-12 col-md-12\">--> <p>Harga : {{lmItem.Fare | currency:\"Rp. \":0}}</p> <!--</div>--> </div> <div class=\"col-md-5\" style=\"text-align:left\"> <p>Satuan : {{lmItem.UnitBy}}</p> <p>Pembayaran : {{lmItem.PaidBy}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-1\"> <p><strong>{{ ldItem.NumberChild }}</strong></p> </div> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-2\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo_bp/includeForm/modalFormMasterEstimate.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo_bp/includeForm/modalFormDetailEstimate.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> </div> </div> </div> </form> </uib-accordion> </div> <div ng-if=\"$index == 2\" smart-include=\"app/services/reception/wo_bp/includeForm/estimationFormInclude/timeEstimation.html\"> </div> <div ng-if=\"$index == 3\"> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No Polisi</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.LicensePlate\" placeholder=\"No Polisi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No Wo Estimasi</bsreqlabel> <input type=\"text\" ng-disabled=\"true\" skip-enable class=\"form-control\" disabled ng-disabled=\"true\" placeholder=\"No WO Estimasi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Category</bsreqlabel> <!-- <bsselect name=\"Category\" ng-model=\"mData.WoCategoryId\" data=\"woCategory\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Kategori\" required>\r" +
    "\n" +
    "                                        </bsselect> --> <select class=\"form-control\" name=\"Category\" ng-model=\"mData.WoCategoryId\" data=\"woCategory\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Kategori\" required> <option ng-repeat=\"woCategory in woCategory\" value=\"{{woCategory.MasterId}}\">{{woCategory.Name}}</option> </select> </div> </div> </div> </div> <uib-accordion close-others=\"oneAtATime\"> <form> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Job Summary </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <bslist data=\"gridWorkEstimate\" m-id=\"TaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" list-api=\"listApiEstimateSummary\" confirm-save=\"true\" list-title=\"Job List\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\" list-height=\"120\" button-settings=\"listView\"> <bslist-template> <div class=\"col-md-1\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.NumberParent }}</strong></p> </div> <div class=\"col-md-6\" style=\"text-align:left\"> <div class=\"col-md-12\"> <p class=\"name\"><strong>{{ lmItem.EstimationNo }}</strong></p> </div> <div class=\"col-md-12\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> </div> <div class=\"col-md-12\"> <p>Harga : {{lmItem.Fare | currency:\"Rp. \":0}}</p> </div> </div> <div class=\"col-md-5\"> <p>Satuan : {{lmItem.UnitBy}}</p> <p>Pembayaran : {{lmItem.PaidBy}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-1\"> <p><strong>{{ ldItem.NumberChild }}</strong></p> </div> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-2\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/appointment/appointmentServiceBp/modalFormMaster.html'\"> --> <div ng-include=\"'app/services/lib/ModalAppointmentBP/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Price Summary </uib-accordion-heading> <div class=\"col-md-12\"> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkEstimate | currency:\"\":0}}</td> <!-- <td style=\"text-align: right\"><span style=\"float: left;\">Rp. </span>{{(totalWorkEstimate - totalWorkDiscountedEstimate) | currency:\"\":0}}</td> --> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkDiscountedEstimate | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ (totalWorkEstimate - totalWorkDiscountedEstimate ) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialEstimate | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialDiscountedEstimate | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ ( totalMaterialEstimate - totalMaterialDiscountedEstimate) | currency:\"\":0}}</td> </tr> <tr> <td>Total DP Parts Material/Bahan</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDpEstimate | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(0) | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDpEstimate | currency:\"\":0}}</td> </tr> </tbody> </table> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Time Summary </uib-accordion-heading> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Body</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrEstTime[0]\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Putty</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrEstTime[1]\"> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Surfacer</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrEstTime[2]\"> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Painting</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrEstTime[3]\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" style=\"margin-top:10px\"> <div class=\"col-md-6\"> <bsreqlabel>Polishing</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrEstTime[4]\"> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Re-Assembly</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrEstTime[5]\"> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Final Inspection</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrEstTime[6]\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-12\" style=\"margin-top:10px; background-color:lightgreen\"> <div class=\"form-group\"> <div class=\"col-xs-4\" style=\"margin-top:10px; width: 24.38%\"> <bsreqlabel style=\"color:white;font-weight: bold; font-size: 14px\">Total Pengerjaan</bsreqlabel> </div> <div class=\"col-xs-3\"> <input type=\"text\" ng-model=\"estHours\" size=\"15\" class=\"form-control\" ng-readonly=\"true\"> </div> <div style=\"margin-top:10px\" class=\"col-xs-2\"> <label>Jam</label> <img src=\"images/rightarrow.png\"> </div> <div class=\"col-xs-3\"> <input type=\"text\" ng-model=\"estDays\" size=\"15\" class=\"form-control\" ng-readonly=\"true\"> </div> <div style=\"margin-top:10px\" class=\"col-xs-1\"> <label>Hari</label> </div> <!-- <div class=\"col-xs-6 col-md-2\">\r" +
    "\n" +
    "                                                        <input type=\"text\" ng-model=\"estHours\" size='6' ng-readonly=\"true\">&nbsp;<label>Jam</label>&nbsp;\r" +
    "\n" +
    "                                                        <img src=\"images/rightarrow.png\">&nbsp;\r" +
    "\n" +
    "                                                        <input type=\"text\" ng-model=\"estDays\" size='6' ng-readonly=\"true\">&nbsp;<label><strong>Hari</strong></label>\r" +
    "\n" +
    "                                                    </div> --> </div> </div> </div> </div> </div> </form> </uib-accordion> <div class=\"row\" ng-show=\"isCenter\"> <div class=\"col-xs-12 col-sm-12 col-md-12\"> <div class=\"col-xs-6 col-sm-6 col-md-6 btn-group\"> <!-- <button type=\"button\" class=\"rbtn btn ng-binding\">\r" +
    "\n" +
    "                                        Pengajuan Claim\r" +
    "\n" +
    "                                    </button> --> <!-- added by sss on 2017-09-25 --> <!-- ini di minta jng di tampilin dl pada tanggal 30/01/2019 --> <button type=\"button\" ng-click=\"claimShow(mData)\" class=\"rbtn btn\" ng-show=\"false\"> Pengajuan Claim </button> <!--  --> <!-- <button type=\"button\" class=\"rbtn btn ng-binding\">\r" +
    "\n" +
    "                                        Print Preview\r" +
    "\n" +
    "                                    </button> --> <!-- added by sss on 2017-10-03 --> <!-- <button type=\"button\" ng-click=\"createBSTK(mData)\" class=\"rbtn btn\">\r" +
    "\n" +
    "                                        Print Preview\r" +
    "\n" +
    "                                    </button> --> <!--  --> </div> <div class=\"col-xs-6 col-sm-6 col-md-6\"> <button type=\"button\" ng-hide=\"isNextWO\" skip-enable ng-disabled=\"isNextWO || disableButtonNextWO || (mData.WoCategoryId == null || mData.WoCategoryId == undefined || mData.JobCatgForBPid == null || mData.JobCatgForBPid == undefined)\" ng-click=\"SaveEstimation(mData,isNextWO)\" class=\"rbtn btn ng-binding pull-right\"> Tidak Lanjut WO </button> <button skip-enable type=\"button\" ng-hide=\"isNextWO\" ng-disabled=\"disableButtonNextWO || (mData.WoCategoryId == null || mData.WoCategoryId == undefined || mData.JobCatgForBPid == null || mData.JobCatgForBPid == undefined)\" ng-click=\"NextWO()\" class=\"rbtn btn ng-binding pull-right\"> Lanjut </button> <button type=\"button\" ng-hide=\"isNextWO\" name=\"CloseWO\" ng-click=\"cancelWoBP(mData)\" class=\"rbtn btn ng-binding pull-right\"> Batal WO </button> </div> </div> </div> </div> </uib-tab> </uib-tabset> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/estimationFormInclude/timeEstimation.html',
    "<div class=\"col-md-12\"> <div class=\"row\"> <div ui-grid=\"gridTimeEst\" ui-grid-cellnav ui-grid-edit ui-grid-auto-resize ui-grid-selection class=\"grid\" ng-cloak> </div> </div> <div class=\"row\" style=\"margin-top: 20px\"> <div class=\"col-md-3 col-xs-3\"> <label for=\"\">TOTAL LEAD TIME</label> </div> <div class=\"col-md-3 col-xs-3\"> <input type=\"text\" ng-model=\"estHours\" size=\"5\" ng-readonly=\"true\">&nbsp;<label>Jam</label> &nbsp;&nbsp;&nbsp;<img src=\"images/rightarrow.png\">&nbsp; </div> <div class=\"col-md-3 col-xs-3\"> <input type=\"text\" ng-model=\"estDays\" size=\"5\" ng-readonly=\"true\">&nbsp;<label><strong>Hari</strong></label> </div> <div class=\"col-md-3 col-xs-3\"> <button ng-click=\"calculateforest()\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-clock-o\"></i>&nbsp;Calculate Time</button> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/jobList.html',
    "<div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\" style=\"margin-top: 40px\"> <uib-tab ng-repeat=\"header in headerJB\" index=\"$index\" heading=\"{{header.name}}\" class=\"headerJobList\"> <div ng-if=\"$index == 0\"> <uib-accordion close-others=\"false\"> <form> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Payment </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-6\"> <fieldset style=\"border:2px solid #e2e2e2;padding:20px\"> <legend style=\"width:inherit;margin:0px;border-bottom:0px\">Pembayaran</legend> <div class=\"col-md-6\"> <div class=\"radio\"> <label class=\"radio\" style=\"margin-top:5px;margin-bottom:6px\"> <input type=\"radio\" name=\"typePembayaran\" class=\"radiobox style-0\" ng-model=\"mData.isCash\" ng-click=\"cekPaymentlist(mData.isCash)\" ng-disabled=\"\" value=\"0\"> <span>Asuransi</span> </label> </div> </div> <div class=\"col-md-6\"> <div class=\"radio\"> <label class=\"radio\" style=\"margin-top:5px;margin-bottom:6px\"> <input type=\"radio\" name=\"typePembayaran\" class=\"radiobox style-0\" ng-model=\"mData.isCash\" ng-click=\"cekPaymentlist(mData.isCash)\" ng-disabled=\"\" value=\"1\"> <span>Cash</span> </label> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\" ng-show=\"mData.isCash == 0\"> <bsreqlabel>Nama Asuransi</bsreqlabel> <bsselect name=\"Asuransi\" ng-model=\"mData.InsuranceName\" data=\"Asuransi\" item-text=\"InsuranceName\" item-value=\"InsuranceName\" placeholder=\"Choose Asuransi\" on-select=\"selectInsurance(selected)\" icon=\"fa fa-building\"> </bsselect> <!-- <select class=\"form-control\" name=\"Asuransi\" ng-model=\"mData.InsuranceName\" data=\"Asuransi\" item-text=\"InsuranceName\" item-value=\"InsuranceName\" placeholder=\"Choose Asuransi\" ng-change=\"selectInsurance(selected)\" icon=\"fa fa-building\">\r" +
    "\n" +
    "                                                            <option ng-repeat=\"Asuransi in Asuransi\" value=\"{{Asuransi.InsuranceName}}\">{{Asuransi.InsuranceName}}</option>\r" +
    "\n" +
    "                                                        </select> --> </div> </div> </fieldset> </div> <div class=\"col-md-6\"> <fieldset ng-show=\"mData.isCash == 0\" style=\"border:2px solid #e2e2e2;padding:20px\"> <legend style=\"width:inherit;margin:0px;border-bottom:0px\">Ada SPK</legend> <div class=\"col-md-6\"> <div class=\"radio\"> <label class=\"radio\" style=\"margin-top:5px;margin-bottom:6px\"> <input type=\"radio\" name=\"typeSPK\" class=\"radiobox style-0\" ng-model=\"mData.isSpk\" ng-click=\"cekSPKlist(mData.isSpk)\" ng-disabled=\"\" value=\"1\"> <span>Ya</span> </label> </div> </div> <div class=\"col-md-6\"> <div class=\"radio\"> <label class=\"radio\" style=\"margin-top:5px;margin-bottom:6px\"> <input type=\"radio\" name=\"typeSPK\" class=\"radiobox style-0\" ng-model=\"mData.isSpk\" ng-click=\"cekSPKlist(mData.isSpk)\" ng-disabled=\"\" value=\"0\"> <span>Tidak</span> </label> </div> </div> <div ng-show=\"mData.isSpk == '1'\" class=\"col-md-12\"> <!-- <div class=\"col-md-12\"> --> <div class=\"col-md-8\"> <div class=\"form-group\"> <bsreqlabel>No. SPK</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"noSPK\" placeholder=\"No SPK\" ng-model=\"mData.SpkNo\" ng-maxlength=\"50\"> </div> </div> <div class=\"col-md-4\"> <div class=\"btn-group\" style=\"margin-top:25px\"> <!-- <button type=\"button\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\"> --> <!-- stlh pilih insurance, kl integrationBit ny 0 disable, kl slain 0 enable --> <!-- kl ya pk spk, input no spk, kl tidak, tdk input spk --> <button type=\"button\" id=\"btnCekSPK\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\"> <i class=\"fa fa-fw fa-save\"></i> Cek SPK </button> </div> </div> </div> <div ng-show=\"mData.isSpk == 1\" class=\"col-md-12\"> <bsreqlabel>Own Risk Amount</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"ORAmount\" ng-model=\"mData.ORAmount\" ng-keyup=\"givePatternHarga(mData.ORAmount,$event)\"> </div> </fieldset> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Permintaan </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <bslist form-name=\"jobRequestBuatBP\" data=\"JobRequest\" m-id=\"JobRequestId\" list-model=\"lmRequest\" on-select-rows=\"onListSelectRows\" selected-rows=\"listRequestSelectedRows\" confirm-save=\"false\" list-api=\"listApiJobRequest\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Permintaan\" list-title=\"Permintaan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Permintaan\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div ng-form=\"rowRequest\"> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea ng-maxtlength=\"200\" maxlength=\"200\" class=\"form-control\" placeholder=\"Permintaan\" name=\"reqdesc\" ng-model=\"lmRequest.RequestDesc\" required>\r" +
    "\n" +
    "                                                            </textarea> <em ng-messages=\"rowRequest.reqdesc.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> </bslist-modal-form> </bslist> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Keluhan </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <bslist form-name=\"jobComplaintBuatBP\" data=\"JobComplaint\" m-id=\"JobComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"fals\" list-api=\"listApiJobComplaint\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Keluhan\" list-title=\"Keluhan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <!-- <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Kategori\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                            </bsselect> --> <select class=\"form-control\" data=\"vm.appScope.ComplaintCategory\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Kategori\" ng-disabled=\"true\" skip-enable> <option ng-repeat=\"vac in vm.appScope.ComplaintCategory\" value=\"{{vac.ComplaintCatg}}\">{{vac.ComplaintCatg}}</option> </select> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Deskripsi\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div ng-form=\"rowcomplaint\" style=\"margin-bottom:90px\"> <div class=\"form-group\"> <bsreqlabel>Kategori</bsreqlabel> <!-- <bsselect data=\"ComplaintCategory\" name=\"ctype\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Kategori\" required>\r" +
    "\n" +
    "                                                            </bsselect> --> <select class=\"form-control\" data=\"ComplaintCategory\" name=\"ctype\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Kategori\" required> <option ng-repeat=\"ComplaintCategory in ComplaintCategory\" value=\"{{ComplaintCategory.ComplaintCatg}}\">{{ComplaintCategory.ComplaintCatg}}</option> </select> <em ng-messages=\"rowcomplaint.ctype.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"ctype\"></bserrmsg> --> </div> <div class=\"form-group\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Deskripsi\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\" required>\r" +
    "\n" +
    "                                                            </textarea> <em ng-messages=\"rowcomplaint.ctext.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"ctext\"></bserrmsg> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Job List </uib-accordion-heading> <div class=\"row\" style=\"margin-bottom:25px\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <bsreqlabel>Kategori WO</bsreqlabel> <!-- <bsselect name=\"CatWO\" ng-model=\"mData.WoCategoryId\" data=\"woCategory\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Kategori WO\" on-select=\"selectCatWO(selected)\" icon=\"fa fa-list\">\r" +
    "\n" +
    "                                                </bsselect> --> <select class=\"form-control\" name=\"CatWO\" ng-model=\"mData.WoCategoryId\" data=\"woCategory\" placeholder=\"Pilih Kategori WO\" on-select=\"selectCatWO(selected)\" icon=\"fa fa-list\"> <option ng-repeat=\"woCategory in woCategory\" value=\"{{woCategory.MasterId}}\">{{woCategory.Name}}</option> </select> </div> <div class=\"col-md-6\"> <bsreqlabel>Kategori Perbaikan</bsreqlabel> <!-- <bsselect name=\"CatPerbaikan\" ng-model=\"mData.JobCatgForBP\" data=\"CatPerbaikan\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Kategori Perbaikan\" on-select=\"selectedCatPerbaikan(selectInsuranceed)\" icon=\"fa fa-list\">\r" +
    "\n" +
    "                                                </bsselect> --> <select class=\"form-control\" name=\"CatPerbaikan\" ng-model=\"mData.JobCatgForBPid\" data=\"CatPerbaikan\" placeholder=\"Pilih Kategori Perbaikan\" on-select=\"selectedCatPerbaikan(selectInsuranceed)\" icon=\"fa fa-list\"> <option ng-repeat=\"CatPerbaikan in CatPerbaikan\" value=\"{{CatPerbaikan.MasterId}}\">{{CatPerbaikan.Name}}</option> </select> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist data=\"gridWork\" form-name=\"joblistBuatBP\" on-before-edit=\"onBeforeEdit\" on-before-new=\"onBeforeNew\" on-before-save=\"onBeforeSave\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" m-id=\"TaskBPId\" d-id=\"PartsId\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApi\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\" list-title=\"Job List\"> <bslist-template> <div class=\"col-md-1\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.NumberParent }}</strong></p> </div> <div class=\"col-md-6\" style=\"text-align:left\"> <div class=\"col-md-12\"> <p class=\"name\"><strong>{{ lmItem.EstimationNo }}</strong></p> </div> <div class=\"col-md-12\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> </div> <div class=\"col-md-12\"> <p>Harga : {{lmItem.Fare | currency:\"Rp. \":0}}</p> </div> </div> <div class=\"col-md-5\"> <p>Satuan : {{lmItem.UnitBy}}</p> <p>Pembayaran : {{lmItem.PaidBy}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-1\"> <p><strong>{{ ldItem.NumberChild }}</strong></p> </div> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-2\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo_bp/includeForm/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo_bp/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> </div> </div> </div> </form> </uib-accordion> </div> <div ng-if=\"$index == 1\"> <div class=\"col-md-12\"> <div class=\"row\"> <div ui-grid=\"gridTimeWork\" ui-grid-cellnav ui-grid-edit ui-grid-auto-resize ui-grid-selection class=\"grid\" ng-cloak> </div> </div> <div class=\"row\" style=\"margin-top: 20px\"> <div class=\"col-md-3 col-xs-3\"> <label for=\"\">TOTAL LEAD TIME</label> </div> <div class=\"col-md-3 col-xs-3\"> <input type=\"text\" ng-model=\"estHoursWork\" size=\"5\" ng-readonly=\"true\">&nbsp;<label>Jam</label> &nbsp;&nbsp;&nbsp;<img src=\"images/rightarrow.png\">&nbsp; </div> <div class=\"col-md-3 col-xs-3\"> <input type=\"text\" ng-model=\"estDaysWork\" size=\"5\" ng-readonly=\"true\">&nbsp;<label><strong>Hari</strong></label> </div> <div class=\"col-md-3 col-xs-3\"> <button ng-click=\"calculateforwork()\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-clock-o\"></i>&nbsp;Calculate Time</button> </div> </div> </div> </div> </uib-tab> </uib-tabset> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/modalFormDetail.html',
    "<div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Material</bsreqlabel> <bsselect name=\"tipe\" required ng-required=\"true\" ng-disabled=\"ldModel.PartsId !== undefined && ldModel.PartsId !== null\" ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeParts(selected)\"> </bsselect> <!-- <select class=\"form-control\" name=\"tipe\" ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" placeholder=\"Pilih Tipe\" ng-change=\"selectTypeParts(ldModel)\">\r" +
    "\n" +
    "                <option ng-repeat=\"materialCategory in materialCategory\" value=\"{{materialCategory.MasterId}}\">{{materialCategory.Name}}</option>\r" +
    "\n" +
    "            </select> --> <bserrmsg field=\"typeMaterial\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom:65px\" show-errors> <div class=\"form-inline\"> <label>No. Material</label> <bscheckbox class=\"pull-right\" ng-change=\"checkIsOPB(ldModel.IsOPB, ldModel)\" ng-disabled=\"!(ldModel.MaterialTypeId == 2) || (ldModel.PartsCode !== undefined && ldModel.PartsCode !== '' &&  ldModel.PartsCode !== null) || ldModel.JobPartsId !== undefined \" ng-model=\"ldModel.IsOPB\" label=\"OPB\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div class=\"col-md-7\" style=\"padding-left:0\"> <bstypeahead placeholder=\"No. Material\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" get-data=\"getParts\" item-text=\"PartsCode\" ng-disabled=\"(ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null) || ldModel.IsOPB == 1\" ng-maxlength=\"250\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectParts\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\" template-url=\"customTemplate.html\" icon=\"fa-id-card-o\"> </bstypeahead> <em ng-show=\"ldModel.PartsCode.length < 4\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p>Minimal 4 karakter untuk pencarian</p> </em> <em ng-show=\"ldModel.PartsCode.length >= 4\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p>&nbsp;</p> </em> </div> <div class=\"col-md-5\"> <button class=\"rbtn btn\" ng-disabled=\"!(ldModel.Qty !== null && ldModel.Qty !== undefined && partsAvailable)\" ng-click=\"checkAvailabilityParts(ldModel)\">Cek Ketersediaan Parts</button> </div> <bserrmsg field=\"noMaterial\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Material</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"namaMaterial\" ng-model=\"ldModel.PartsName\" ng-maxlength=\"64\" ng-disabled=\"ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null\"> <bserrmsg field=\"namaMaterial\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input step=\"1\" type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" required min=\"1\" class=\"form-control\" name=\"qty\" ng-model=\"ldModel.Qty\"> <!-- <bserrmsg field=\"qty\"></bserrmsg> --> <em ng-messages=\"row1.qty.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\"> Nilai Qty Harus Satuan</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-model=\"ldModel.SatuanId\" on-select=\"selectTypeUnitParts(selected)\" data=\"unitData\" item-text=\"Name\" required item-value=\"MasterId\" placeholder=\"Pilih Tipe\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" convert-to-number name=\"satuan\" ng-model=\"ldModel.SatuanId\" ng-change=\"selectTypeUnitParts(ldModel.SatuanId)\" data=\"unitData\" required placeholder=\"Pilih Tipe\"> <option ng-repeat=\"unitData in unitData\" value=\"{{unitData.MasterId}}\">{{unitData.Name}}</option> </select> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Tipe Order</label> <!-- <bsselect name=\"satuan\" ng-model=\"ldModel.Type\" data=\"typeMData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Tipe\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" convert-to-number ng-disabled=\"ldModel.Availbility == 'Tersedia'\" name=\"satuan\" ng-model=\"ldModel.OrderType\" data=\"typeMData\" placeholder=\"Pilih Tipe\"> <option value=\"\"></option> <option ng-repeat=\"typeMData in typeMData\" value=\"{{typeMData.Id}}\">{{typeMData.Name}}</option> </select> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\"> <label>ETA</label> <bsdatepicker name=\"DateAppoint\" ng-disabled=\"true\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\"> </bsdatepicker> <!-- <bsdatepicker name=\"DateAppoint\" ng-disabled=\"partsAvailable\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\">\r" +
    "\n" +
    "            </bsdatepicker> --> </div> <div class=\"form-group\" show-errors> <label>Ketersediaan</label> <input type=\"text\" min=\"0\" class=\"form-control\" ng-disabled=\"true\" name=\"ketersedian\" ng-model=\"ldModel.Availbility\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypePaidParts(selected)\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" convert-to-number name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" ng-show=\"ldModel.MaterialTypeId != 2\" ng-disabled=\"JobIsWarranty == 1\" ng-change=\"selectTypePaidParts(ldModel.PaidById,1)\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"paymentData in paymentData\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <select class=\"form-control\" convert-to-number name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentDataNoWarranty\" ng-show=\"ldModel.MaterialTypeId == 2\" ng-disabled=\"JobIsWarranty == 1\" ng-change=\"selectTypePaidParts(ldModel.PaidById,1)\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"paymentData in paymentDataNoWarranty\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"ldModel.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <input type=\"text\" ng-change=\"checkPrice(ldModel.RetailPriceMask)\" min=\"0\" class=\"form-control\" name=\"harga\" ng-if=\"ldModel.IsOPB != 1\" ng-required=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-disabled=\"(ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPriceMask\" ng-keyup=\"givePatternHargaPart(ldModel.RetailPriceMask,$event)\"> <input type=\"number\" ng-hide=\"true\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"0\" class=\"form-control\" name=\"harga\" ng-if=\"ldModel.IsOPB != 1\" ng-required=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-disabled=\"(ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\"> <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"1\" class=\"form-control\" name=\"hargaOPB\" ng-if=\"ldModel.IsOPB == 1\" ng-required=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-disabled=\"(ldModel.IsOPB == 1) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\"> <em ng-messages=\"row1.harga.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <em ng-messages=\"row1.hargaOPB.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Harga Tidak Boleh 0</p> </em> <!-- <em ng-messages=\"row1.harga.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "            <bserrmsg field=\"harga\"></bserrmsg> --> </div> <!--     <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "      <label>Diskon</label>\r" +
    "\n" +
    "      <input type=\"number\" min=0 max=\"100\" class=\"form-control\" name=\"harga\" ng-model=\"ldModel.Discount\">\r" +
    "\n" +
    "      <bserrmsg field=\"harga\"></bserrmsg>\r" +
    "\n" +
    "    </div> --> <div class=\"form-group\" show-errors> <label>Sub Total</label> <input type=\"text\" ng-disabled=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.xsubTotal\" currency-formating number-only> <input type=\"text\" ng-hide=\"true\" ng-disabled=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotalMask\"> <input type=\"number\" ng-hide=\"true\" ng-disabled=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotal\"> <bserrmsg field=\"subTotal\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>DP</label> <input type=\"text\" min=\"0\" ng-maxlength=\"10\" ng-disabled=\"ldModel.PaidById == null || ldModel.PaidById == undefined || flagIsInsurance || ldModel.Availbility == 'Tersedia' || disableDP\" class=\"form-control\" name=\"Dp\" ng-model=\"ldModel.DownPayment\" currency-formating number-only ng-change=\"checkDp(ldModel.DownPayment, ldModel)\"> <bserrmsg field=\"Dp\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypeDiskonParts(selected,0)\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" convert-to-number name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonDataParts\" ng-change=\"selectTypeDiskonParts(ldModel.DiscountTypeId,0)\" ng-disabled=\"ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1 || ldModel.CreatePartAt == 'WOBP'\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"diskonData in diskonDataParts\" value=\"{{diskonData.MasterId}}\">{{diskonData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <div ng-if=\"ldModel.DiscountTypeId !== undefined && ldModel.DiscountTypeId !== null && ldModel.DiscountTypeId > 0\"><bsreqlabel>List Diskon</bsreqlabel></div> <div ng-if=\"ldModel.DiscountTypeId == undefined || ldModel.DiscountTypeId == null || ldModel.DiscountTypeId <= 0\"><label>List Diskon</label></div> <!-- <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonPart(selected,ldModel.DiscountTypeId,0)\" placeholder=\"Pilih List Diskon\"\r" +
    "\n" +
    "                ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" convert-to-number name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1 || ldModel.DiscountTypeId == 10 || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1 || ldModel.CreatePartAt == 'WOBP'\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonPart(ldModel,ldModel.DiscountTypeId,0)\" placeholder=\"Pilih List Diskon\" ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\"> <option ng-repeat=\"diskonListData in diskonListData\" value=\"{{diskonListData.Id}}\">{{diskonListData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-hide=\"true\" ng-required=\"lmModel.DiscountTypeId !== undefined && lmModel.DiscountTypeId !== null && lmModel.DiscountTypeId != 0 && lmModel.DiscountTypeId != -1 && lmModel.DiscountTypeId != 10\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-required=\"isCustomTask\" ng-change=\"changeDiscount(ldModel.subTotal,ldModel.Discount)\" ng-disabled=\"(!isCustomPart) || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"ldModel.Discount\"> <!-- <input type=\"number\" ng-required=\"isCustomTask\" ng-change=\"onDiscountTypingJobTask(ldModel.Discount)\" ng-disabled=\"(!isCustomPart) || ldModel.PaidById == 2277\" min=0 max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"ldModel.Discount\"> --> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.PartsName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/modalFormDetailEstimate.html',
    "<div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Material</bsreqlabel> <bsselect name=\"tipe\" required ng-required=\"true\" ng-disabled=\"ldModel.PartsId !== undefined && ldModel.PartsId !== null\" convert-to-number ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeParts(selected)\"> </bsselect> <!-- <select class=\"form-control\" name=\"tipe\" ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" placeholder=\"Pilih Tipe\" ng-change=\"selectTypeParts(ldModel)\">\r" +
    "\n" +
    "                <option ng-repeat=\"materialCategory in materialCategory\" value=\"{{materialCategory.MasterId}}\">{{materialCategory.Name}}</option>\r" +
    "\n" +
    "            </select> --> <bserrmsg field=\"tipe\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom:65px\" show-errors> <div class=\"form-inline\"> <label>No. Material</label> <bscheckbox class=\"pull-right\" ng-change=\"checkIsOPB(ldModel.IsOPB, ldModel)\" ng-disabled=\"!(ldModel.MaterialTypeId == 2) || (ldModel.PartsCode !== undefined && ldModel.PartsCode !== '' &&  ldModel.PartsCode !== null) || ldModel.JobPartsId !== undefined\" ng-model=\"ldModel.IsOPB\" label=\"OPB\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div class=\"col-md-7\" style=\"padding-left:0\"> <bstypeahead ng-disabled=\"(ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null) || ldModel.IsOPB == 1\" placeholder=\"No. Material\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" get-data=\"getParts\" item-text=\"PartsCode\" ng-maxlength=\"250\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectParts\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\" template-url=\"customTemplate.html\" icon=\"fa-id-card-o\"> </bstypeahead> <em ng-show=\"ldModel.PartsCode.length < 4\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p>Minimal 4 karakter untuk pencarian</p> </em> <em ng-show=\"ldModel.PartsCode.length >= 4\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p>&nbsp;</p> </em> </div> <div class=\"col-md-5\"> <button ng-hide=\"!isCenter\" class=\"rbtn btn\" ng-disabled=\"!(ldModel.Qty !== null && ldModel.Qty !== undefined && partsAvailable)\" ng-click=\"checkAvailabilityParts(ldModel)\">Cek Ketersediaan Parts</button> </div> <bserrmsg field=\"noMaterial\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Material</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"namaMaterial\" ng-model=\"ldModel.PartsName\" ng-maxlength=\"64\" ng-disabled=\"ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null\"> <bserrmsg field=\"namaMaterial\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <!-- <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" required min=1 class=\"form-control\" name=\"qty\" ng-model=\"ldModel.Qty\">\r" +
    "\n" +
    "            <bserrmsg field=\"qty\"></bserrmsg> --> <input step=\"1\" type=\"number\" ng-disabled=\"ldModel.MaterialRequestStatusId == 80\" ng-change=\"checkPrice(ldModel.RetailPrice)\" required min=\"1\" class=\"form-control\" name=\"qty\" ng-model=\"ldModel.Qty\"> <em ng-messages=\"row1.qty.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\"> Nilai Qty Harus Satuan</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-model=\"ldModel.SatuanId\" on-select=\"selectTypeUnitParts(selected)\" data=\"unitData\" item-text=\"Name\" required item-value=\"MasterId\" placeholder=\"Pilih Tipe\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" name=\"satuan\" ng-model=\"ldModel.SatuanId\" ng-change=\"selectTypeUnitParts(ldModel.SatuanId)\" data=\"unitData\" required placeholder=\"Pilih Tipe\"> <option ng-repeat=\"unitData in unitData\" value=\"{{unitData.MasterId}}\">{{unitData.Name}}</option> </select> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Tipe Order</label> <!-- <bsselect name=\"satuan\" ng-model=\"ldModel.Type\" data=\"typeMData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Tipe\">\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number class=\"form-control\" ng-disabled=\"ldModel.Availbility == 'Tersedia'\" name=\"satuan\" ng-model=\"ldModel.OrderType\" data=\"typeMData\" placeholder=\"Pilih Tipe\"> <option value=\"\"></option> <option ng-repeat=\"typeMData in typeMData\" value=\"{{typeMData.Id}}\">{{typeMData.Name}}</option> </select> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\"> <label>ETA</label> <bsdatepicker name=\"DateAppoint\" ng-disabled=\"true\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\"> </bsdatepicker> <!-- <bsdatepicker name=\"DateAppoint\" ng-disabled=\"partsAvailable\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\">\r" +
    "\n" +
    "            </bsdatepicker> --> </div> <div class=\"form-group\" show-errors> <label>Ketersediaan</label> <input ng-hide=\"!isCenter\" type=\"text\" min=\"0\" class=\"form-control\" ng-disabled=\"true\" name=\"ketersedian\" ng-model=\"ldModel.Availbility\"> <input ng-hide=\"isCenter\" type=\"text\" min=\"0\" class=\"form-control\" ng-disabled=\"true\" name=\"ketersedian\" ng-model=\"ldModel.Availbility\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypePaidParts(selected)\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number class=\"form-control\" name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" ng-show=\"ldModel.MaterialTypeId != 2\" ng-disabled=\"JobIsWarranty == 1\" ng-change=\"selectTypePaidParts(ldModel.PaidById,0)\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"paymentData in paymentData\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <select convert-to-number class=\"form-control\" name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentDataNoWarranty\" ng-show=\"ldModel.MaterialTypeId == 2\" ng-disabled=\"JobIsWarranty == 1\" ng-change=\"selectTypePaidParts(ldModel.PaidById,0)\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"paymentData in paymentDataNoWarranty\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"ldModel.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <input type=\"text\" ng-change=\"checkPrice(ldModel.RetailPriceMask)\" min=\"0\" class=\"form-control\" name=\"harga\" ng-if=\"ldModel.IsOPB != 1\" ng-required=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-disabled=\"(ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPriceMask\" ng-keyup=\"givePatternHargaPart(ldModel.RetailPriceMask,$event)\"> <input type=\"number\" ng-hide=\"true\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"0\" class=\"form-control\" name=\"harga\" ng-if=\"ldModel.IsOPB != 1\" ng-required=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-disabled=\"(ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\"> <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"1\" class=\"form-control\" name=\"hargaOPB\" ng-if=\"ldModel.IsOPB == 1\" ng-required=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-disabled=\"(ldModel.IsOPB == 1) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\"> <!-- <bserrmsg field=\"harga\"></bserrmsg> --> <em ng-messages=\"row1.harga.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <em ng-messages=\"row1.hargaOPB.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Harga Tidak Boleh 0</p> </em> </div> <!--     <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "      <label>Diskon</label>\r" +
    "\n" +
    "      <input type=\"number\" min=0 max=\"100\" class=\"form-control\" name=\"harga\" ng-model=\"ldModel.Discount\">\r" +
    "\n" +
    "      <bserrmsg field=\"harga\"></bserrmsg>\r" +
    "\n" +
    "    </div> --> <div class=\"form-group\" show-errors> <label>Sub Total</label> <input type=\"text\" ng-disabled=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.xsubTotal\" currency-formating number-only> <input type=\"text\" ng-disabled=\"true\" ng-hide=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotalMask\"> <input type=\"number\" ng-disabled=\"true\" ng-hide=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotal\"> <bserrmsg field=\"subTotal\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Dp</label> <input type=\"text\" currency-formating number-only ng-change=\"checkDp(ldModel.DownPayment, ldModel)\" min=\"0\" ng-maxlength=\"10\" ng-disabled=\"ldModel.PaidById == null || ldModel.PaidById == undefined || flagIsInsurance || ldModel.Availbility == 'Tersedia' || disableDP\" class=\"form-control\" name=\"Dp\" ng-model=\"ldModel.DownPayment\"> <bserrmsg field=\"Dp\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypeDiskonParts(selected,1)\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonDataParts\" ng-change=\"selectTypeDiskonParts(ldModel.DiscountTypeId,1, ldModel)\" ng-disabled=\"ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1 || ldModel.CreatePartAt == 'WOBP'\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"diskonData in diskonDataParts\" value=\"{{diskonData.MasterId}}\">{{diskonData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <div ng-if=\"ldModel.DiscountTypeId !== undefined && ldModel.DiscountTypeId !== null && ldModel.DiscountTypeId > 0\"><bsreqlabel>List Diskon</bsreqlabel></div> <div ng-if=\"ldModel.DiscountTypeId == undefined || ldModel.DiscountTypeId == null || ldModel.DiscountTypeId <= 0\"><label>List Diskon</label></div> <!-- <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonPart(selected,ldModel.DiscountTypeId,1)\" placeholder=\"Pilih List Diskon\"\r" +
    "\n" +
    "                ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\">\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1 || ldModel.DiscountTypeId == 10 || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1 || ldModel.CreatePartAt == 'WOBP'\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonPart(ldModel,ldModel.DiscountTypeId,1)\" placeholder=\"Pilih List Diskon\" ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\"> <option ng-repeat=\"diskonListData in diskonListData\" value=\"{{diskonListData.Id}}\">{{diskonListData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-hide=\"true\" ng-required=\"ldModel.DiscountTypeId !== undefined && ldModel.DiscountTypeId !== null && ldModel.DiscountTypeId > 0 && ldModel.DiscountTypeId < 0\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-change=\"changeDiscount(ldModel.subTotal,ldModel.Discount)\" ng-required=\"isCustomTask\" ng-change=\"countDiscount(ldModel.Discount)\" ng-disabled=\"(!isCustomPart) || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"ldModel.Discount\"> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.PartsName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/modalFormMaster.html',
    "<div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pekerjaan</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"lmModel.TaskName\" get-data=\"getWork\" model-key=\"Name\" item-text=\"TaskName\" required loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWork\" on-noresult=\"onNoResultEstimate\" on-gotresult=\"onGotResult\" icon=\"fa-id-card-o\" ng-if=\"mFilterTAM.isTAM == 1 \"> </bstypeahead> <input type=\"text\" placeholder=\"Nama Pekerjaan\" class=\"form-control\" name=\"jobName\" ng-model=\"lmModel.TaskName\" ng-if=\"mFilterTAM.isTAM == 0\" required> <bserrmsg field=\"jobName\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label class=\"pull-left\">Additional Ope No</label> <bscheckbox ng-change=\"getDataOpeNo(lmModel.TaskName,lmModel.isOpe,lmModel)\" class=\"pull-right\" ng-model=\"lmModel.isOpe\" label=\"Ope No\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\" show-errors> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div style=\"padding-left:0;padding-right:0;padding-bottom:15px\"> <!-- <bstypeahead placeholder=\"Task Code\" name=\"noMaterial\" ng-model=\"lmModel.TaskCode\" get-data=\"getTaskOpe\"\r" +
    "\n" +
    "                    item-text=\"TaskCode\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected=\"selected\"\r" +
    "\n" +
    "                    on-select=\"onSelectOpeNo\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\"\r" +
    "\n" +
    "                    ng-disabled=\"lmModel.isOpe == 0\" template-url=\"customTemplateOpe.html\" icon=\"fa-id-card-o\">\r" +
    "\n" +
    "                    </bstypeahead> --> <select name=\"OpeNo\" ng-change=\"selectTypeOpeNo(lmModel.TaskCode,lmModel)\" class=\"form-control\" ng-disabled=\"lmModel.isOpe == 0 || lmModel.isOpe == undefined\" ng-model=\"lmModel.TaskCode\" data=\"DataOpeNo\" placeholder=\"Pilih Tipe\"> <option ng-repeat=\"sat in DataOpeNo\" value=\"{{sat.TaskListBPId}}\">{{sat.TaskName}}</option> </select> </div> <em ng-messages=\"row1.noMaterial.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"noMaterial\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <input type=\"text\" required ng-maxlength=\"8\" min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Fare\" ng-disabled=\"PriceAvailable\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan</bsreqlabel> <input type=\"text\" min=\"0\" class=\"form-control\" name=\"satuan\" ng-model=\"lmModel.UOM\" ng-disabled=\"mDataCrm.Vehicle.isNonTAM == 0\" ng-hide=\"mDataCrm.Vehicle.isNonTAM == 0\"> <!-- <bsselect name=\"satuan\"\r" +
    "\n" +
    "                    ng-model=\"lmModel.ProcessId\"\r" +
    "\n" +
    "                    data=\"unitData\"\r" +
    "\n" +
    "                    item-text=\"Name\"\r" +
    "\n" +
    "                    item-value=\"MasterId\"\r" +
    "\n" +
    "                    placeholder=\"Pilih Tipe\"\r" +
    "\n" +
    "                    required\r" +
    "\n" +
    "                    on-select=\"selectUnit(selected)\">\r" +
    "\n" +
    "                </bsselect> --> <select class=\"form-control\" convert-to-number name=\"satuan\" ng-model=\"lmModel.ProcessId\" data=\"unitData\" placeholder=\"Pilih Tipe\" required ng-change=\"selectUnit(lmModel.ProcessId)\" ng-if=\"mDataCrm.Vehicle.isNonTAM == 0\"> <option ng-repeat=\"unitData in unitData\" value=\"{{unitData.MasterId}}\">{{unitData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"lmModel.ProcessId\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\"\r" +
    "\n" +
    "                    ng-model=\"lmModel.PaidById\"\r" +
    "\n" +
    "                    data=\"paymentData\"\r" +
    "\n" +
    "                    required\r" +
    "\n" +
    "                    item-text=\"Name\"\r" +
    "\n" +
    "                    item-value=\"MasterId\"\r" +
    "\n" +
    "                    placeholder=\"Pilih Pembayaran\"\r" +
    "\n" +
    "                    on-select=\"selectTypeCust(selected)\">\r" +
    "\n" +
    "                </bsselect> --> <select ng-init=\"lmModel.PaidById=''\" class=\"form-control\" convert-to-number name=\"pembayaran\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" required placeholder=\"Pilih Pembayaran\" ng-change=\"selectTypeCust(lmModel.PaidById,1,lmModel)\" ng-disabled=\"disablePembayaran || mData.WoCategoryId == 2451\"> <option value=\"\"></option> <option ng-repeat=\"paymentData in paymentData\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModel.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>T1 <span ng-show=\"!disT1\" style=\"color:#b94a48\"> *</span> </label> <!-- <bsselect name=\"T1\"\r" +
    "\n" +
    "                        ng-model=\"lmModel.TFirst1\"\r" +
    "\n" +
    "                        data=\"TFirst\"\r" +
    "\n" +
    "                        item-text=\"Name\"\r" +
    "\n" +
    "                        item-value=\"Name\"\r" +
    "\n" +
    "                        ng-disabled=\"isRelease\"\r" +
    "\n" +
    "                        placeholder=\"T1\">\r" +
    "\n" +
    "                </bsselect> --> <select class=\"form-control\" ng-required=\"!disT1\" name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" ng-disabled=\"disT1\" placeholder=\"T1\"> <option ng-repeat=\"TFirst in TFirst\" value=\"{{TFirst.Name}}\">{{TFirst.Name}}</option> </select> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- <bsselect name=\"diskon\" ng-model=\"lmModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypeDiskonTask(selected,0,lmModel)\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "                </bsselect> --> <select class=\"form-control\" convert-to-number name=\"diskon\" ng-model=\"lmModel.DiscountTypeId\" data=\"diskonData\" ng-change=\"selectTypeDiskonTask(lmModel.DiscountTypeId,0,lmModel)\" ng-disabled=\"lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.CreateTaskAt == 'WOBP\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"diskonData in diskonData\" value=\"{{diskonData.MasterId}}\">{{diskonData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <div ng-if=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2 || lmModel.DiscountTypeId == null|| lmModel.DiscountTypeId == undefined\"><bsreqlabel>List Diskon</bsreqlabel></div> <div ng-if=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10\"><label>List Diskon</label></div> <!-- <bsselect name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonTask(selected,lmModel.DiscountTypeId,0)\" placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\">\r" +
    "\n" +
    "                </bsselect> --> <!-- <select class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonTask(lmModel.DiscountTypeListId,lmModel.DiscountTypeId,0)\" placeholder=\"Pilih List Diskon\" --> <select class=\"form-control\" convert-to-number name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10 || lmModel.PaidById == 2277 || lmModel.PaidById == 30\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonTask(lmModel.DiscountTypeListId,lmModel,0)\" placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\"> <option ng-repeat=\"diskonListData in diskonListData\" value=\"{{diskonListData.Id}}\">{{diskonListData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-hide=\"true\" ng-required=\"lmModel.DiscountTypeId !== undefined && lmModel.DiscountTypeId !== null && lmModel.DiscountTypeId != 0 && lmModel.DiscountTypeId != -1 && lmModel.DiscountTypeId != 10\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-required=\"isCustomTask\" ng-disabled=\"(!isCustomTask) || lmModel.PaidById == 2277 || lmModel.PaidById == 30\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"lmModel.Discount\"> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/modalFormMasterEstimate.html',
    "<div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pekerjaan</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"lmModel.TaskName\" get-data=\"getWork\" model-key=\"Name\" item-text=\"TaskName\" required loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkEstimate\" on-noresult=\"onNoResultEstimate\" on-gotresult=\"onGotResult\" icon=\"fa-id-card-o\" ng-if=\"mFilterTAM.isTAM == 1 \"> </bstypeahead> <input type=\"text\" placeholder=\"Nama Pekerjaan\" class=\"form-control\" name=\"jobName\" ng-model=\"lmModel.TaskName\" ng-if=\"mFilterTAM.isTAM == 0\" required> <bserrmsg field=\"jobName\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label class=\"pull-left\">Additional Ope No</label> <bscheckbox ng-change=\"getDataOpeNo(lmModel.TaskName,lmModel.isOpe,lmModel)\" class=\"pull-right\" ng-model=\"lmModel.isOpe\" label=\"Ope No\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\" show-errors> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div style=\"padding-left:0;padding-right:0;padding-bottom:15px\"> <!-- <bstypeahead placeholder=\"Task Code\" name=\"noMaterial\" ng-model=\"lmModel.TaskCode\" get-data=\"getTaskOpe\"\r" +
    "\n" +
    "                    item-text=\"TaskCode\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected=\"selected\"\r" +
    "\n" +
    "                    on-select=\"onSelectOpeNo\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\"\r" +
    "\n" +
    "                    ng-disabled=\"lmModel.isOpe == 0\" template-url=\"customTemplateOpe.html\" icon=\"fa-id-card-o\">\r" +
    "\n" +
    "                    </bstypeahead> --> <select name=\"OpeNo\" ng-change=\"selectTypeOpeNo(lmModel.TaskCode,lmModel)\" class=\"form-control\" ng-disabled=\"lmModel.isOpe == 0 || lmModel.isOpe == undefined\" ng-model=\"lmModel.TaskCode\" data=\"DataOpeNo\" placeholder=\"Pilih Tipe\"> <option ng-repeat=\"sat in DataOpeNo\" value=\"{{sat.TaskListBPId}}\">{{sat.TaskName}}</option> </select> </div> <em ng-messages=\"row1.noMaterial.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"noMaterial\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Harga</label> <input type=\"text\" required ng-maxlength=\"13\" min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Fare\" ng-disabled=\"PriceAvailable\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan</bsreqlabel> <input type=\"text\" min=\"0\" class=\"form-control\" name=\"satuan\" ng-model=\"lmModel.UOM\" ng-disabled=\"mDataCrm.Vehicle.isNonTAM == 0\" ng-hide=\"mDataCrm.Vehicle.isNonTAM == 0\"> <!-- <bsselect name=\"satuan\"\r" +
    "\n" +
    "                    ng-model=\"lmModel.ProcessId\"\r" +
    "\n" +
    "                    data=\"unitData\"\r" +
    "\n" +
    "                    item-text=\"Name\"\r" +
    "\n" +
    "                    item-value=\"MasterId\"\r" +
    "\n" +
    "                    placeholder=\"Pilih Tipe\"\r" +
    "\n" +
    "                    required\r" +
    "\n" +
    "                    on-select=\"selectUnit(selected)\">\r" +
    "\n" +
    "                </bsselect> --> <!-- <bsselect name=\"satuan\"\r" +
    "\n" +
    "                ng-model=\"lmModel.ProcessId\"\r" +
    "\n" +
    "                data=\"unitData\"\r" +
    "\n" +
    "                item-text=\"Name\"\r" +
    "\n" +
    "                item-value=\"MasterId\"\r" +
    "\n" +
    "                placeholder=\"Pilih Tipe\"\r" +
    "\n" +
    "                required\r" +
    "\n" +
    "                on-select=\"selectUnit(selected)\"\r" +
    "\n" +
    "                ng-if=\"mDataCrm.Vehicle.isNonTAM == 0\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"satuan\" ng-model=\"lmModel.ProcessId\" data=\"unitData\" placeholder=\"Pilih Tipe\" required ng-change=\"selectUnit(lmModel.ProcessId)\" ng-if=\"mDataCrm.Vehicle.isNonTAM == 0\"> <option ng-repeat=\"unitData in unitData\" value=\"{{unitData.MasterId}}\">{{unitData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"lmModel.ProcessId\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\"\r" +
    "\n" +
    "                    ng-model=\"lmModel.PaidById\"\r" +
    "\n" +
    "                    data=\"paymentData\"\r" +
    "\n" +
    "                    required\r" +
    "\n" +
    "                    item-text=\"Name\"\r" +
    "\n" +
    "                    item-value=\"MasterId\"\r" +
    "\n" +
    "                    placeholder=\"Pilih Pembayaran\"\r" +
    "\n" +
    "                    on-select=\"selectTypeCust(selected)\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number ng-init=\"lmModel.PaidById=''\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" required placeholder=\"Pilih Pembayaran\" ng-change=\"selectTypeCust(lmModel.PaidById,0,lmModel)\" ng-disabled=\"disablePembayaran || mData.WoCategoryId == 2451\"> <option value=\"\"></option> <option ng-repeat=\"paymentData in paymentData\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModel.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>T1</label> <!-- <bsselect name=\"T1\"\r" +
    "\n" +
    "                        ng-model=\"lmModel.TFirst1\"\r" +
    "\n" +
    "                        data=\"TFirst\"\r" +
    "\n" +
    "                        item-text=\"Name\"\r" +
    "\n" +
    "                        item-value=\"Name\"\r" +
    "\n" +
    "                        ng-disabled=\"isRelease\"\r" +
    "\n" +
    "                        placeholder=\"T1\">\r" +
    "\n" +
    "                </bsselect> --> <!-- <select class=\"form-control\"name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" ng-disabled=\"isRelease\" placeholder=\"T1\">\r" +
    "\n" +
    "                    <option ng-repeat=\"TFirst in TFirst\" value=\"{{TFirst.Name}}\">{{TFirst.Name}}</option>\r" +
    "\n" +
    "                </select> --> <select class=\"form-control\" name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" ng-disabled=\"disT1\" placeholder=\"T1\" ng-required=\"!disT1\"> <option ng-repeat=\"TFirst in TFirst\" value=\"{{TFirst.Name}}\">{{TFirst.Name}}</option> </select> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- <bsselect name=\"diskon\" ng-model=\"lmModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypeDiskonTask(selected,1,lmModel)\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"diskonTipe\" ng-model=\"lmModel.DiscountTypeId\" data=\"diskonData\" ng-change=\"selectTypeDiskonTask(lmModel.DiscountTypeId,1,lmModel)\" ng-disabled=\"lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.CreateTaskAt == 'WOBP'\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"diskonData in diskonData\" value=\"{{diskonData.MasterId}}\">{{diskonData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskonTipe\" ng-model=\"lmModel.DiscountTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"diskonTipe\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <div ng-if=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2 || lmModel.DiscountTypeId == null|| lmModel.DiscountTypeId == undefined\"><bsreqlabel>List Diskon</bsreqlabel></div> <div ng-if=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10\"><label>List Diskon</label></div> <!-- <bsselect name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonTask(selected,lmModel,1)\" placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10 || lmModel.PaidById == 2277 || lmModel.PaidById == 30\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonTask(lmModel.DiscountTypeListId,lmModel,1)\" placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\"> <option ng-repeat=\"diskonListData in diskonListData\" value=\"{{diskonListData.Id}}\">{{diskonListData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-hide=\"true\" ng-required=\"lmModel.DiscountTypeId !== undefined && lmModel.DiscountTypeId !== null && lmModel.DiscountTypeId != 0 && lmModel.DiscountTypeId != -1 && lmModel.DiscountTypeId != 10\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-required=\"isCustomTask\" ng-disabled=\"(!isCustomTask) || lmModel.PaidById == 2277 || lmModel.PaidById == 30\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"lmModel.Discount\"> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/wacFormInclude/exterior.html',
    "<div class=\"col-md-12\" style=\"margin-top: 25px\"> <div ui-grid=\"gridExt\" ui-grid-cellnav ui-grid-auto-resize ui-grid-selection class=\"grid\" ng-cloak> </div> <input type=\"submit\" value=\"Testing Grid\" ng-click=\"goGetGrid(mDataWAC)\"> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/wac_bp.html',
    "<div class=\"col-md-12\"> <style>/*.popover {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "            background: #fff;\r" +
    "\n" +
    "            border: 1px solid #999;\r" +
    "\n" +
    "            padding: 10px;\r" +
    "\n" +
    "            width: auto;\r" +
    "\n" +
    "            box-shadow: 0 0 10px rgba(0, 0, 0, .5);\r" +
    "\n" +
    "        }*/\r" +
    "\n" +
    "        /*.popover:after,\r" +
    "\n" +
    "        .popover:before {\r" +
    "\n" +
    "            right: 100%;\r" +
    "\n" +
    "            border: solid transparent;\r" +
    "\n" +
    "            content: \" \";\r" +
    "\n" +
    "            height: 0;\r" +
    "\n" +
    "            width: 0;\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            pointer-events: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .popover:after {\r" +
    "\n" +
    "            border-color: rgba(255, 255, 255, 0);\r" +
    "\n" +
    "            border-right-color: #ffffff;\r" +
    "\n" +
    "            border-width: 10px;\r" +
    "\n" +
    "            top: 50%;\r" +
    "\n" +
    "            margin-top: -10px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .popover:before {\r" +
    "\n" +
    "            border-color: rgba(201, 201, 201, 0);\r" +
    "\n" +
    "            border-right-color: #c9c9c9;\r" +
    "\n" +
    "            border-width: 11px;\r" +
    "\n" +
    "            top: 50%;\r" +
    "\n" +
    "            margin-top: -11px;\r" +
    "\n" +
    "        }*/\r" +
    "\n" +
    "        .checkbox+.checkbox, .radio+.radio {\r" +
    "\n" +
    "            margin-top: 10px;\r" +
    "\n" +
    "        }</style> <div class=\"row\"> <div class=\"col-md-2\"> <div class=\"form-group\"> <label>KM *</label> <!-- <input class=\"form-control input-lg\" type=\"text\" ng-model=\"mData.Km\"></input> --> <input type=\"text\" step=\"1\" min=\"0\" class=\"form-control input-lg\" name=\"Km\" placeholder=\"Km\" ng-model=\"mData.Km\" ng-maxlength=\"7\" maxlength=\"7\" ng-keyup=\"givePattern(mData.Km,$event)\"> <p ng-if=\"isKmHistory >= 1 && nilaiKM < KMTerakhir\" style=\"font-style:italic;margin-top:5px; color:  rgb(185, 74, 72)\">Tidak boleh kurang dari {{KMTerakhir | number}} KM</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Money Amout</label> <!-- <input class=\"form-control input-lg\" type=\"text\" ng-model=\"mData.MoneyAmount\"></input> --> <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Money Amount\" name=\"moneyAmount\" ng-model=\"mData.moneyAmount\" ng-keyup=\"givePatternMoney(mData.moneyAmount,$event)\"> </div> </div> <!-- <input ng-hide=\"true\" type=\"text\" class=\"form-control\" ng-model=\"slider.value\" > --> <div id=\"slide\" class=\"col-md-6\"> <label for=\"slider\">Fuel</label> <div class=\"scaleWarp\"> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> <garis>|</garis> <garis style=\"font-size: 20px;font-weight:bold\">|</garis> <garis>|</garis> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> </div> <rzslider skip-disable class=\"custom-slider\" rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\" style=\"margin-top: 40px\"> <uib-tab ng-repeat=\"header in headerWAC\" index=\"$index\" heading=\"{{header.GroupName}}\" ng-click=\"tabClick(header.Id,header.GroupName)\"> <div ng-if=\"$index == 0\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Type Kendaraan</bsreqlabel> <!-- <select class=\"form-control\" name=\"filterType\" convert-to-number ng-model=\"filter.vType\" data=\"VTypeData\" placeholder=\"Pilih Type Kendaraan\" ng-change=\"chooseVType(filter.vType)\" icon=\"fa fa-car\">\r" +
    "\n" +
    "                                            <option value=\"\"></option>\r" +
    "\n" +
    "                                            <option ng-repeat=\"VTypeData in VTypeData\" value=\"{{VTypeData.MasterId}}\">{{VTypeData.Name}}</option>\r" +
    "\n" +
    "                                        </select> --> <div class=\"icon-addon left-addon ui-select\"><i class=\"fa fa-car\"></i> <ui-select name=\"vehicType\" ng-model=\"filter.vType\" icon=\"fa fa-car\" on-select=\"chooseVType($item.MasterId)\" search-enabled=\"false\" skip-enable theme=\"select2\" id=\"EstimasiType\"> <ui-select-match style=\"padding-left:30px\" allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.Name}} </ui-select-match> <ui-select-choices repeat=\"item in VTypeData\"> <span ng-bind-html=\"item.Name\"></span> </ui-select-choices> </ui-select> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"680\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"!isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"750\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> </div> <div class=\"row\" ng-show=\"!disableWACExt\"> <div class=\"col-md-12\"> <!-- <div ui-grid=\"grid\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav ui-grid-pinning ui-grid-auto-resize ui-grid-edit class=\"grid\" ng-style=\"getTableHeight()\">\r" +
    "\n" +
    "                                        <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>No data available</div>\r" +
    "\n" +
    "                                    </div> --> <table id=\"wacGrid\" width=\"100%\" class=\"table table-bordered table-hover table-responsive\"> <thead> <tr> <th ng-style=\"{'width':'{{col.width}}'}\" ng-hide=\"col.visible == false\" ng-repeat=\"col in grid.columnDefs\">{{col.name}}</th> </tr> </thead> <tbody> <tr style=\"height:40px\" ng-repeat=\"column in grid.data\"> <td ng-hide=\"true\" style=\"width:5%\">{{column.JobWACExtId}}</td> <td style=\"width:55%\">{{column.ItemName}}</td> <td style=\"width:45%\" style=\"padding:0px\"> <select class=\"form-control\" name=\"status\" convert-to-number ng-change=\"onDropdownChange(column)\" ng-model=\"column.Status\" placeholder=\"Pilih Kerusakan\" style=\"height:30px\"> <option ng-repeat=\"item in DamageVehicle\" value=\"{{item.Id}}\">{{item.Name}}</option> </select> </td> <td ng-hide=\"true\" style=\"width:5%\">{{column.typeName}}</td> </tr> <tr ng-if=\"!grid.data.length\"> <td colspan=\"{{grid.columnDefs.length}}\"> <div style=\"opacity: .25;font-size: 3em;width: 100%;text-align: center;z-index: 1000\">No Data Available</div> </td> </tr> </tbody> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bscheckbox ng-model=\"disableWACExt\" label=\"Clear\" true-value=\"true\" false-value=\"false\" ng-change=\"chgChk()\"> </bscheckbox> </div> <div class=\"col-md-8\"> <div class=\"containeramk\" ng-style=\"{'max-width': boundingBox.width + 'px', 'max-height': boundingBox.height + 'px'}\"> <signature-pad ng-init=\"header.GroupName == gridName\" accept=\"accept\" clear=\"clear\" style=\"height:165px\" height=\"165\" width=\"500\" dataurl=\"signature.dataUrl\"></signature-pad> <div class=\"buttonsamk\"> <button class=\"btn wbtn\" ng-disabled=\"disableReset\" skip-enable ng-click=\"clear()\">Clear</button> <button class=\"btn rbtn\" ng-click=\"signature = accept(); open(signature)\">Sign</button> </div> </div> </div> <div class=\"col-md-2\"> <image-control max-upload=\"4\" is-image=\"1\" img-upload=\"uploadFiles\" multiple> <form-image-control> <input capture=\"camera\" type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"file\" ng-init=\"mData.UploadPhoto = uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> </div> </div> </div> <div class=\"row\" ng-show=\"!isCenter\"> <div class=\"btn-group pull-right\"> <button type=\"button\" name=\"printPreview\" class=\"rbtn btn\" ng-click=\"releaseBSTK()\"> BSTK </button> <button type=\"button\" name=\"CloseWO\" ng-click=\"cancelBSTK(mData)\" class=\"rbtn btn\"> Batal BSTK </button> </div> </div> </div> </div> <!-- Mantul  --> <div ng-if=\"$index !== 0\"> <div class=\"col-md-12\" ng-if=\"header.Quantity > 0\" style=\"margin-top: 25px\"> <!-- <div ng-if=\"$index !== 0 && header.GroupName == gridName\" ui-grid=\"data['gridData'+header.GroupName]\" ui-grid-cellNav ui-grid-auto-resize class=\"grid\" ng-cloak>\r" +
    "\n" +
    "                            </div> --> <table class=\"table table-striped table-bordered table-hover table-responsive\" ng-if=\"$index !== 0 && header.GroupName == gridName\"> <thead> <tr> <th ng-repeat=\"col in data['gridData'+header.GroupName].columnDefs\">{{col.name}}</th> </tr> </thead> <tbody> <tr ng-repeat=\"column in data['gridData'+header.GroupName].data\"> <td style=\"width:50%\">{{column.name}}</td> <td style=\"width:50%\" style=\"padding:0px\"> <!-- <div class=\"btn-group\">\r" +
    "\n" +
    "                                                <input class=\"radiobox style-0\" ng-model=\"column.ItemStatus\" type=\"radio\" ng-value=\"1\" style=\"width:20px\">&nbsp;Baik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r" +
    "\n" +
    "                                                <input class=\"radiobox style-0\" ng-model=\"column.ItemStatus\" type=\"radio\" ng-value=\"2\" style=\"width:20px\">&nbsp;Rusak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r" +
    "\n" +
    "                                                <input class=\"radiobox style-0\" ng-model=\"column.ItemStatus\" type=\"radio\" ng-value=\"3\" style=\"width:20px\">&nbsp;Tidak Ada\r" +
    "\n" +
    "                                            </div> --> <div class=\"col-md-12 col-xs-12\"> <div style=\"float:left;padding-right:20px;padding-left:20px\"> <label class=\"radio\"> <input class=\"radiobox style-0\" ng-model=\"column.ItemStatus\" type=\"radio\" ng-value=\"1\"><span>Baik</span> </label> </div> <div style=\"float:left;padding-right:20px;padding-left:20px\"> <label class=\"radio\"> <input class=\"radiobox style-0\" ng-model=\"column.ItemStatus\" type=\"radio\" ng-value=\"2\"><span>Rusak</span> </label> </div> <div style=\"float:left;padding-right:20px;padding-left:20px\"> <label class=\"radio\"> <input class=\"radiobox style-0\" ng-model=\"column.ItemStatus\" type=\"radio\" ng-value=\"3\"><span>Tidak Ada</span> </label> </div> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\" ng-if=\"header.Quantity == 0\"> <div style=\"margin-top : 25px\" oc-lazy-load=\"['summernote']\"> <input ng-hide=\"true\" type=\"text\" ng-model=\"gridOther[0].OtherDescription\" class=\"form-control\"> <summernote name=\"content\" config=\"summernoteOptions2\" ng-model=\"gridOther[0].OtherDescription\" required></summernote> <bserrmsg field=\"content\"></bserrmsg> </div> </div> </div> </uib-tab> </uib-tabset> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/includeForm/wobp/wo01.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Pemilik</label> <input ng-model=\"mDataCustomer.owner\" type=\"text\" skip-enable disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Pengguna</label> <input ng-model=\"mDataCustomer.user\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Alamat</label> <input ng-model=\"mDataCustomer.address\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>On Time/Late</label> <input ng-model=\"mDataCustomer.late\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Waiting Time</label> <input ng-model=\"mDataCustomer.waitTime\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Model</label> <input ng-model=\"mDataCustomer.model\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-12\"> <div class=\"well\"> <div class=\"form-group\"> <label>Job Suggest</label> <textarea class=\"form-control\" skip-enable ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\"></textarea> </div> </div> </div> <div class=\"col-xs-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Field Action\"> <div class=\"col-xs-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Operation No.</font> </td> <td> <font color=\"white\">Description</font> </td> </tr> <tr ng-repeat=\"a in GetDataTowass\"> <td>{{a.OpeNo}}</td> <td>{{a.Description}}</td> </tr> </table> </div> </uib-tab> <uib-tab index=\"1\" heading=\"PSFU\"> <div class=\"col-xs-12\" style=\"margin-top: 20px\"> <table id=\"QA\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>No.</th> <th>Pertanyaan</th> <th>Jawaban Customer</th> <!-- <th>Alasan Customer</th> --> </tr> </thead> <tbody> <tr ng-repeat=\"item in Question track by $index\"> <td width=\"5%\">{{$index + 1}}</td> <td width=\"40%\">{{item.Description}}</td> <td width=\"25%\"> <select class=\"form-control\" name=\"yesOrNo\" ng-disabled=\"true\" skip-enable> <option ng-disabled=\"true\" skip-enable ng-repeat=\"itemAnswer in item.Answer track by $index\" value=\"{{itemAnswer.AnswerId}}\">{{itemAnswer.Description}}</option> </select> </td> </tr> </tbody> </table> </div> </uib-tab> <uib-tab index=\"2\" heading=\"CS Survey\"> </uib-tab> <uib-tab index=\"3\" heading=\"PKS\"> <div class=\"col-md-12\" style=\"margin-top:10px\" ng-repeat=\"a in dataPKS\"> <label>Nama Perusahaan : {{a.CompanyName}}</label><br> <!-- <label>Balances : {{a.Balances | currency:\"Rp.\"}}</label><br> --> <label ng-show=\"a.CustomerPKSTop < 0 || a.CustomerPKSTop == null ||a.CustomerPKSTop = 0\">Saldo PKS : {{a.AvailableBalance | currency:\"Rp. \"}}</label><br> <label ng-show=\"a.CustomerPKSTop >= 1\">TOP : {{a.CustomerPKSTop}} Hari</label><br> <label>Periode Mulai : {{a.StartPeriod |date:dd/mm/yyyy}}</label><br> <label>Periode Selesai : {{a.EndPeriod |date:dd/mm/yyyy}}</label> </div> <div ng-if=\"dataPKS.length == 0\" style=\"text-align:center\"> <h3>Data PKS Tidak Ada</h3> </div> </uib-tab> </uib-tabset> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/mainmenu.html',
    "<style type=\"text/css\">.grey {\r" +
    "\n" +
    "        color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed {\r" +
    "\n" +
    "        width: 100%\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>thead,\r" +
    "\n" +
    "    .header-fixed>tbody,\r" +
    "\n" +
    "    .header-fixed>thead>tr,\r" +
    "\n" +
    "    .header-fixed>tbody>tr,\r" +
    "\n" +
    "    .header-fixed>thead>tr>th,\r" +
    "\n" +
    "    .header-fixed>tbody>tr>td {\r" +
    "\n" +
    "        display: block;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>tbody>tr>td {\r" +
    "\n" +
    "        border-top: 0;\r" +
    "\n" +
    "        border-bottom: 0;\r" +
    "\n" +
    "        border-left: 0;\r" +
    "\n" +
    "        border-right: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>tbody>tr:after,\r" +
    "\n" +
    "    .header-fixed>thead>tr:after {\r" +
    "\n" +
    "        content: ' ';\r" +
    "\n" +
    "        display: block;\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "        clear: both;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>tbody {\r" +
    "\n" +
    "        overflow-y: auto;\r" +
    "\n" +
    "        height: 300px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .header-fixed>tbody>tr>td,\r" +
    "\n" +
    "    .header-fixed>thead>tr>th {\r" +
    "\n" +
    "        width: 20%;\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* =================== */table-header {\r" +
    "\n" +
    "        width: 100%\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .table-header-fixed>thead,\r" +
    "\n" +
    "    .table-header-fixed>tbody,\r" +
    "\n" +
    "    .table-header-fixed>thead>tr,\r" +
    "\n" +
    "    .table-header-fixed>tbody>tr,\r" +
    "\n" +
    "    .table-header-fixed>thead>tr>th,\r" +
    "\n" +
    "    .table-header-fixed>tbody>tr>td {\r" +
    "\n" +
    "        display: block;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .table-header-fixed>tbody>tr:after,\r" +
    "\n" +
    "    .table-header-fixed>thead>tr:after {\r" +
    "\n" +
    "        content: ' ';\r" +
    "\n" +
    "        display: block;\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "        clear: both;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .table-header-fixed>tbody {\r" +
    "\n" +
    "        overflow-y: auto;\r" +
    "\n" +
    "        height: 300px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .table-header-fixed>tbody>tr>td,\r" +
    "\n" +
    "    .table-header-fixed>thead>tr>th {\r" +
    "\n" +
    "        width: 20%;\r" +
    "\n" +
    "        float: left;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /* =================== */\r" +
    "\n" +
    "    .call-red {\r" +
    "\n" +
    "        color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .customCell {\r" +
    "\n" +
    "        width: 20%;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        font-size: 12pt;\r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "        color: #fff;\r" +
    "\n" +
    "        height: 61px;\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /* .fa {\r" +
    "\n" +
    "        color: black !important;\r" +
    "\n" +
    "    } */\r" +
    "\n" +
    "    #example .fa ,#waiting .fa{\r" +
    "\n" +
    "        color: black !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (max-width: 1025px) {\r" +
    "\n" +
    "        .customCell {\r" +
    "\n" +
    "            text-align: center;\r" +
    "\n" +
    "            font-size: 10.2pt;\r" +
    "\n" +
    "            font-weight: bold;\r" +
    "\n" +
    "            height: 61px;\r" +
    "\n" +
    "            padding-left: 5px !important;\r" +
    "\n" +
    "            padding-right: 5px !important;\r" +
    "\n" +
    "            padding-top: 10px !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .panel-default>.panel-heading\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        color: #fff !important;\r" +
    "\n" +
    "        background-color: #d53337 !important;\r" +
    "\n" +
    "        border-color: #d53337 !important;\r" +
    "\n" +
    "        font-size: 18px !important;\r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #statusFuTime input[type=\"search\"], .ui-select-focusser {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #phoneType input[type=\"search\"], .ui-select-focusser {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    @media (max-width: 769px) {\r" +
    "\n" +
    "        #estGridVc label input[type=radio].radiobox+span{\r" +
    "\n" +
    "        font-size:11px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #wacGrid .form-control{\r" +
    "\n" +
    "        border: 0px solid #000;\r" +
    "\n" +
    "    }</style> <bsform-base ng-form=\"SAMainMenuFormBP\" loading=\"loading\" get-data=\"getData\" model=\"mData\" action-button-settings=\"actionButtonSettings\" action-button-settings-detail=\"actionButtonSettingsDetail\" form-api=\"formApi\" form-name=\"SAMainMenuFormBP\" form-title=\"SA Main Menu\" icon=\"fa fa-fw fa-child\"> <bsform-base-main> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom: 10px;padding-left: 0px\"> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <i class=\"fa fa-search\"></i> <input class=\"form-control\" type=\"text\" id=\"filterNopol\" name=\"mData.Filter\" ng-model=\"Data.FilterNoPolice\" placeholder=\"Filter No.Polisi\" style=\"text-transform: uppercase\"> </div> </div> </div> <div class=\"col-md-12\"> <!-- <div class=\"input-group\">\r" +
    "\n" +
    "                                    <input type=\"text\" ng-model=\"FilterNoPolice\" placeholder=\"filter\">       \r" +
    "\n" +
    "                                </div> --> <table id=\"example\" class=\"table table-striped table-bordered header-fixed\" width=\"100%\"> <thead> <tr> <!-- <th style=\"width:20%;\">SA Assignment</th>\r" +
    "\n" +
    "                                    <th style=\"width:20%;\">Waiting for Production</th>\r" +
    "\n" +
    "                                    <th style=\"width:20%;\">In Production</th>\r" +
    "\n" +
    "                                    <th style=\"width:20%;\">Waiting TECO</th>\r" +
    "\n" +
    "                                    <th style=\"width:20%;\">Delivery</th> --> <th style=\"width:20%;text-align: center;font-size: 12pt;font-weight: bold; height:61px; background-color: #d53337; color: #fff\">SA Assignment <br> ({{dataChips.SA.length}})</th> <th class=\"customCell\">Waiting for Production <br> ({{dataChips.wap.length}})</th> <th style=\"width:20%;text-align: center;font-size: 12pt;font-weight: bold; height:61px; background-color: #d53337; color: #fff\">In Production <br> ({{dataChips.ip.length}})</th> <th style=\"width:20%;text-align: center;font-size: 12pt;font-weight: bold; height:61px; background-color: #d53337; color: #fff\">Waiting TECO <br> ({{dataChips.wt.length}})</th> <th style=\"width:20%;text-align: center;font-size: 12pt;font-weight: bold; height:61px; background-color: #d53337; color: #fff\">Delivery <br> ({{dataChips.deliv.length}})</th> </tr> </thead> <tbody> <tr> <td> <!-- SA Assignment --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.SA |filter:Data.FilterNoPolice|orderBy:'item.PoliceNumber' track by $index\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.DateForChips\" time-finish=\"item.TimeForChips\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\"></chip-samainmenu> </td> <td> <!-- waiting --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.wap|filter:Data.FilterNoPolice|orderBy:'item.PoliceNumber'\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.DateForChips\" time-finish=\"item.TimeForChips\" place=\"item.TaskNameForChips\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\"></chip-samainmenu> </td> <td> <!-- Production --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.ip|filter:Data.FilterNoPolice|orderBy:'item.PoliceNumber'\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.DateForChips\" time-finish=\"item.TimeForChips\" place=\"item.TaskNameForChips\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\"></chip-samainmenu> </td> <td> <!-- Teco --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.wt|filter:Data.FilterNoPolice|orderBy:'item.PoliceNumber'\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.DateForChips\" time-finish=\"item.TimeForChips\" place=\"item.TaskNameForChips\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\"></chip-samainmenu> </td> <td> <!-- Delivery --> <chip-samainmenu ng-click=\"detailChip(item)\" ng-repeat=\"item in dataChips.deliv|filter:Data.FilterNoPolice|orderBy:'item.PoliceNumber'\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.ModelType\" time-start=\"item.DateForChips\" time-finish=\"item.TimeForChips\" place=\"item.TaskNameForChips\" employeesa=\"item.EmployeeName\" midcolor=\"item.color\"></chip-samainmenu> </td> </tr> </tbody> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-12\" style=\"background-color: #d53337\"> <center><label style=\"margin-top:5px; color: white;font-size: 23px; font-weight: bold\">Waiting</label></center> </div> <table id=\"waiting\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th style=\"width:33%;text-align: center;font-size: 12pt;font-weight: bold; height:61px\">Cust Approval<br>({{DataChipsWaiting.arr39.length}})</th> <th style=\"width:33%;text-align: center;font-size: 12pt;font-weight: bold; height:61px\">Parts<br>({{DataChipsWaiting.arr38.length}})</th> <th style=\"width:33%;text-align: center;font-size: 12pt;font-weight: bold; height:61px\">OPL<br>({{DataWaitingOPLs.length}})</th> <!-- <th style=\"width:33%\">Cust Approval</th>\r" +
    "\n" +
    "                                    <th style=\"width:33%\">Parts</th>\r" +
    "\n" +
    "                                    <th style=\"width:33%\">OPL</th> --> </tr> </thead> <tbody> <tr> <td> <chip-samainmenu ng-repeat=\"item in DataChipsWaiting.arr39|filter:Data.FilterNoPolice|orderBy:'item.PoliceNumber'\" status=\"item.PauseReasonId\" policenumber=\"item.PoliceNumber\" modelname=\"item.JobTaskBp.Job.ModelType\" time-start=\"item.JobTaskBp.Job.PlanStart\" time-finish=\"item.JobTaskBp.Job.PlanFinish\" employeesa=\"item.JobTaskBp.Job.UserSa.EmployeeName\" midcolor=\"item.Color\"></chip-samainmenu> </td> <td> <chip-samainmenu ng-repeat=\"item in DataChipsWaiting.arr38|filter:Data.FilterNoPolice|orderBy:'item.PoliceNumber'\" status=\"item.PauseReasonId\" policenumber=\"item.PoliceNumber\" modelname=\"item.JobTaskBp.Job.ModelType\" time-start=\"item.JobTaskBp.Job.PlanStart\" time-finish=\"item.JobTaskBp.Job.PlanFinish\" employeesa=\"item.JobTaskBp.Job.UserSa.EmployeeName\" midcolor=\"item.Color\"></chip-samainmenu> </td> <td> <chip-samainmenu ng-repeat=\"item in DataWaitingOPLs|filter:Data.FilterNoPolice|orderBy:'item.PoliceNumber'\" status=\"item.Status\" policenumber=\"item.PoliceNumber\" modelname=\"item.Job.ModelType\" time-start=\"item.Job.PlanStart\" time-finish=\"item.Job.PlanFinish\" employeesa=\"item.Job.UserSa.EmployeeName\" midcolor=\"item.Color\"></chip-samainmenu> </td> </tr> </tbody> </table> </div> </div> </div> </div> </bsform-base-main> <bsform-base-detail> <div ng-if=\"modeQueue == 2\"> <div ng-include=\"'app/services/reception/wo_bp/queueTaking.html'\"></div> </div> <div ng-if=\"modeQueue == 3\"> <div smart-include=\"app/services/reception/wo_bp/wobp.html\"></div> </div> <div ng-if=\"modeQueue == 4\"> <div smart-include=\"app/services/reception/wo_bp/wobp.html\"></div> </div> <div ng-if=\"modeQueue == 5\"> <!-- <div ng-include=\"'app/services/production/repairProcessGR/repairProcessGR.html'\"></div> --> <div ng-include=\"'app/services/reception/wo_bp/repairProcessGR.html'\"></div> </div> <!-- untuk Teco --> <div ng-if=\"modeQueue == 13\"> <div smart-include=\"app/services/reception/wo_bp/Technicalcompletion.html\"></div> </div> <!-- untuk CallCustomer --> <!--         <div ng-if=\"modeQueue == 16\">\r" +
    "\n" +
    "            <div smart-include=\"app/services/reception/wo_bp/callCustomer.html\"></div>\r" +
    "\n" +
    "        </div> --> <div ng-if=\"modeQueue == 16\"> <div smart-include=\"app/services/reception/wo_bp/serviceExplanation.html\"></div> </div> </bsform-base-detail> </bsform-base> <!-- added by sss on 2017-09-25 --> <bsui-modal show=\"fClaim.show\" title=\"Form Claim\" mode=\"modalMode\" data=\"mClaim\" button-settings=\"buttonSettingClaimModal\" on-save=\"spkTrigger\" on-cancel=\"spkTriggerCancel\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Asuransi</bsreqlabel> <bsselect name=\"Asuransi\" ng-model=\"mClaim.InsuranceId\" data=\"Asuransi\" item-text=\"InsuranceName\" item-value=\"InsuranceId\" placeholder=\"Choose Asuransi\" on-select=\"selectInsurance(selected)\" icon=\"fa fa-building\"> </bsselect> </div> <div class=\"form-group\"> <label>No Polis</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"No Polis\" ng-model=\"mClaim.PolicyNo\"> </div> </div> <div class=\"form-group\"> <label>Nama Tertanggung</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Tertanggung\" ng-model=\"mClaim.PolicyName\"> </div> </div> <div class=\"form-group\"> <label>Periode Polis</label> <bsdatepicker name=\"StartPeriode\" ng-model=\"mClaim.StartPeriod\" date-options=\"dateClaimOptions\" placeholder=\"Periode Awal\" skip-enable> </bsdatepicker> </div> <div class=\"form-group\"> <center><label>S/D</label></center> <bsdatepicker name=\"FinishPeriode\" ng-model=\"mClaim.EndPeriod\" date-options=\"dateClaimOptions\" placeholder=\"Periode Akhir\" skip-enable> </bsdatepicker> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No Polisi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"No Polisi\" ng-model=\"mClaim.LicensePlate\" style=\"text-transform: uppercase\" ng-minlength=\"3\" ng-maxlength=\"10\" readonly> </div> </div> <div class=\"form-group\"> <label>VIN Number</label> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text\" class=\"form-control\" style=\"text-transform: uppercase\" name=\"name\" placeholder=\"VIN Number\" ng-model=\"mClaim.VIN\" readonly> </div> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                <label>Model Kendaraan</label>\r" +
    "\n" +
    "                <bsselect name=\"Model\" ng-model=\"mClaim.VehicleModelId\" data=\"modelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"selectModelClaim(selected)\" placeholder=\"Pilih Model\" icon=\"fa fa-car\" required>\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\"> <label>Type</label> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-car\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" ng-model=\"mClaim.VehicleType\" placeholder=\"Model Type\" readonly>\r" +
    "\n" +
    "                </div> --> <bsselect name=\"Model\" ng-model=\"mClaim.VehicleTypeId\" data=\"VehicT\" item-text=\"NewDescription\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Model\" icon=\"fa fa-car\" required ng-disabled=\"true\"> </bsselect></div> <div class=\"form-group\"> <label>Warna Kendaraan</label> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-car\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" ng-model=\"mClaim.Color\" placeholder=\"Warna Kendaraan\" readonly>\r" +
    "\n" +
    "                </div> --> <bsselect name=\"Model\" ng-model=\"mClaim.ColorId\" data=\"VehicColor\" item-text=\"MColor.ColorName\" item-value=\"ColorId\" placeholder=\"Pilih Model\" icon=\"fa fa-car\" required ng-disabled=\"true\"> <ui-select-choices repeat=\"item in VehicColor\"> <span ng-bind-html=\"item.MColor.ColorName\"></span> </ui-select-choices> </bsselect></div> <div class=\"form-group\"> <label>Nomor Handphone</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"No Handphone\" ng-model=\"mClaim.Phone\" readonly> </div> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                <select class=\"form-control\" ng-model=\"mClaim.spk\" style=\"width:35%\">\r" +
    "\n" +
    "                    <option ng-repeat=\"actionSpkClaim in actionSpkClaim\" value=\"actionSpkClaim.Id\">{{actionSpkClaim.Value}}</option>\r" +
    "\n" +
    "                </select>\r" +
    "\n" +
    "            </div> --> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <label>Alasan Additional SPK</label> <textarea class=\"form-control\" placeholder=\"Alasan\" ng-model=\"mClaim.Reason\">\r" +
    "\n" +
    "                </textarea> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Lampirkan Foto Kendaraan</label> <image-control max-upload=\"4\" is-image=\"1\" img-upload=\"uploadFiles\" multiple> <form-image-control> <input capture=\"camera\" type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"LampiranFK\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"file\" ng-init=\"mClaim.LampiranFK = uploadFiles\" name=\"LampiranFK\" id=\"pickfiles-nav1\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Lampirkan LK</label> <image-control max-upload=\"4\" is-image=\"1\" img-upload=\"uploadFiles2\" multiple> <form-image-control> <input capture=\"camera\" type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles2\" name=\"LampiranLK\" id=\"pickfiles-nav2\" style=\"z-index: 1\"> <input type=\"file\" ng-init=\"mClaim.LampiranLK = uploadFiles2\" name=\"LampiranLK\" id=\"pickfiles-nav2\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> </div> </div> </div>  </bsui-modal> <bsui-modal show=\"showCallCustomer.show\" title=\"Customer Notification\" data=\"modal_modelCallCustomer\" on-save=\"saveCallCustomer\" on-cancel=\"cancelCallCustomer\" button-settings=\"buttonSettingModal3\" mode=\"modalModeCallCustomer\"> <div ng-form name=\"customerCallForm\"> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <div class=\"col-md-12\" style=\"text-align:center\"> <h2>Lakukan proses notifikasi sekarang?</h2> <h3>Silahkan tekan nomor telepon dibawah untuk menelepon</h3> <p> <h3>{{modal_modelCallCustomer.ContactPerson}}</h3> </p> <p ng-show=\"modal_modelCallCustomer.PhoneContactPerson1 !== null\"> <h3 ng-show=\"modal_modelCallCustomer.PhoneContactPerson1 !== null\">{{modal_modelCallCustomer.PhoneContactPerson1}}&nbsp;&nbsp;<a href=\"Tel:{{modal_modelCallCustomer.PhoneContactPerson1}}\"><i style=\"color:green\" class=\"fa fa-lg fa-phone\"></i></a></h3> </p> <p ng-show=\"modal_modelCallCustomer.PhoneContactPerson2 !== null\"> <h3 ng-show=\"modal_modelCallCustomer.PhoneContactPerson2 !== null\">{{modal_modelCallCustomer.PhoneContactPerson2}}&nbsp;&nbsp;<a href=\"Tel:{{modal_modelCallCustomer.PhoneContactPerson2}}\"><i style=\"color:green\" class=\"fa fa-lg fa-phone\"></i></a></h3> </p> </div> </fieldset> </div> </bsui-modal> <!--  -->"
  );


  $templateCache.put('app/services/reception/wo_bp/queueTaking.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">th.active {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* .stretch {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        top: 0;\r" +
    "\n" +
    "        left: 0;\r" +
    "\n" +
    "        right: 0;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "        overflow-x: inherit; */\r" +
    "\n" +
    "    .table_font thead tr th{\r" +
    "\n" +
    "        font-size: 24px !important;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .table_font tbody tr td{\r" +
    "\n" +
    "        font-size: 24px !important; \r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .refreshIcon{\r" +
    "\n" +
    "        color:#444;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .refreshIcon:hover{\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        color:#d53337;\r" +
    "\n" +
    "    }</style> <div ng-show=\"pageMode=='view'\" class=\"row\"> <!-- <div class=\"\" ng-show=\"isCenter\"> --> <div class=\"col-md-12\" style=\"display: flex;\r" +
    "\n" +
    "        justify-content: flex-end\"> <i ng-click=\"refreshData()\" style=\"width: 40px;height: 40px;font-size: 30px;margin-top: 20px\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" class=\"fa fa-refresh refreshIcon\"></i> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Penerimaan Booking</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div ui-grid=\"gridReceptBook\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav\r" +
    "\n" +
    "                ui-grid-pinning ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridReceptBookStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"emontime in gridReceptBook.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{emontime.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{emontime.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{'grey':emontime.WaitingTime > '0:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{'grey':((emontime.WaitingTimeHour == 0 && emontime.WaitingTimeMinute >= 10) || (emontime.WaitingTimeHour > 0)) }\"> {{emontime.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <!-- <a href=\"#\" ng-show=\"emontime.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(emontime)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\" ng-class=\"{'grey':emontime.WaitingTime > '0:10' }\"><i --> <a href=\"#\" ng-show=\"emontime.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(emontime)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\" ng-class=\"{'grey':((emontime.WaitingTimeHour == 0 && emontime.WaitingTimeMinute >= 10) || (emontime.WaitingTimeHour > 0)) }\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"editCall(emontime)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWoBP(emontime, 1)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Penerimaan Walk In</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div ui-grid=\"gridReceptWalkIn\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav\r" +
    "\n" +
    "                ui-grid-pinning ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridReceptWalkInStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"gridReceptWalkIn in gridReceptWalkIn.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{gridReceptWalkIn.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{gridReceptWalkIn.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':gridReceptWalkIn.WaitingTime > '0:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((gridReceptWalkIn.WaitingTimeHour == 0 && gridReceptWalkIn.WaitingTimeMinute >= 10) || (gridReceptWalkIn.WaitingTimeHour > 0)) }\"> {{gridReceptWalkIn.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"gridReceptWalkIn.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(gridReceptWalkIn)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((gridReceptWalkIn.WaitingTimeHour == 0 && gridReceptWalkIn.WaitingTimeMinute >= 10) || (gridReceptWalkIn.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(gridReceptWalkIn)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWoBP(gridReceptWalkIn, 2)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Penerimaan Satelite</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div ui-grid=\"gridReceptSatelite\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav\r" +
    "\n" +
    "                ui-grid-pinning ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridReceptSateliteStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"EM in gridReceptSatelite.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{EM.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{EM.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"  ng-class=\"{ 'grey':EM.WaitingTime > '0:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((EM.WaitingTimeHour == 0 && EM.WaitingTimeMinute >= 10) || (EM.WaitingTimeHour > 0)) }\"> {{EM.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"EM.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(EM)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((EM.WaitingTimeHour == 0 && EM.WaitingTimeMinute >= 10) || (EM.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(EM)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWoBP(EM, 2)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Pengambilan</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div ui-grid=\"gridSubmission\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav\r" +
    "\n" +
    "                ui-grid-pinning ui-grid-cellNav ui-grid-auto-resize class=\"grid\" ng-style=\"gridSubmissionStyle()\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"Booking in gridPengambilan.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{Booking.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{Booking.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"  ng-class=\"{ 'grey':Booking.WaitingTime > '0:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((Booking.WaitingTimeHour == 0 && Booking.WaitingTimeMinute >= 10) || (Booking.WaitingTimeHour > 0)) }\"> {{Booking.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"Booking.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(Booking)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((Booking.WaitingTimeHour == 0 && Booking.WaitingTimeMinute >= 10) || (Booking.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(Booking)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWoBP(Booking, 2)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <!-- </div> --> <div class=\"\" ng-show=\"!isCenter\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Penerimaan</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div ui-grid=\"gridCRecept\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav\r" +
    "\n" +
    "                ui-grid-pinning ui-grid-cellNav ui-grid-auto-resize ng-style=\"gridCReceptStyle()\" class=\"grid\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"gridCRecept in gridCRecept.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{gridCRecept.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{gridCRecept.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"  ng-class=\"{ 'grey':gridRecept.WaitingTime > '0:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((gridRecept.WaitingTimeHour == 0 && gridRecept.WaitingTimeMinute >= 10) || (gridRecept.WaitingTimeHour > 0)) }\"> {{gridCRecept.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"gridCRecept.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(gridCRecept)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((gridRecept.WaitingTimeHour == 0 && gridRecept.WaitingTimeMinute >= 10) || (gridRecept.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(gridCRecept)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWo(gridCRecept)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Penyerahan</th> </tr> </table> </div> <div class=\"col-md-12\"> <!-- <div ui-grid=\"gridCSubmission\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellNav\r" +
    "\n" +
    "                ui-grid-pinning ui-grid-cellNav ui-grid-auto-resize class=\"grid\" ng-style=\"gridCSubmissionStyle()\" ng-cloak></div> --> <table class=\"table table-striped table-bordered table-hover table-responsive grid table_font\"> <thead> <tr> <th class=\"col-xs-4 col-sm-4 col-md-4\">Customer</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Janji</th> <th class=\"col-xs-2 col-sm-2 col-md-2 text-center\">Waktu Tunggu</th> <th class=\"col-xs-4 col-sm-4 col-md-4 text-center\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"gridCSubmission in gridCSubmission.data\"> <td class=\"col-xs-4 col-sm-4 col-md-4\"> {{gridCSubmission.LicensePlate}} </td> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\"> {{gridCSubmission.Job.AppointmentTime}} </td> <!-- <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':gridCSubmission.WaitingTime > '0:10' }\"> --> <td class=\"col-xs-2 col-sm-2 col-md-2 text-center\" ng-class=\"{ 'grey':((gridCSubmission.WaitingTimeHour == 0 && gridCSubmission.WaitingTimeMinute >= 10) || (gridCSubmission.WaitingTimeHour > 0))}\"> {{gridCSubmission.WaitingTime}} </td> <td class=\"col-xs-4 col-sm-4 col-md-4 text-center\"> <div class=\"ui-grid-cell-contents\"> <a href=\"#\" ng-show=\"gridCSubmission.entity.TmpTime == \\'0:0\\' \" ng-click=\"manualCall(gridCSubmission)\" uib-tooltip=\"Call\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-phone\" style=\"padding:3px 8px 8px 0px;margin-left:8px\" ng-class=\"{ 'grey':((gridCSubmission.WaitingTimeHour == 0 && gridCSubmission.WaitingTimeMinute >= 10) || (gridCSubmission.WaitingTimeHour > 0)) }\"></i></a> <a href=\"#\" ng-click=\"editCall(gridCSubmission)\" uib-tooltip=\"Edit\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> <a href=\"#\" ng-click=\"cancelWoBP(gridCSubmission, 2)\" uib-tooltip=\"Cancel WO\" tooltip-placement=\"bottom\"><i class=\"fa fa-fw fa-lg fa-remove\" style=\"padding:3px 8px 8px 0px;margin-left:8px\"></i></a> </div> </td> </tr> </tbody> </table> </div> </div> <div class=\"col-md-12\"> <button ng-click=\"callCustomer()\" type=\"button\" class=\"btn btn-block btn-success\" class=\"active text-center header\" style=\"font-size: 26px; height:51px\">Next Call</button> </div> </div> <div ng-show=\"pageMode=='call'\" class=\"row\"> <div class=\"page\" ng-controller=\"AppCtrl\"> <div ng-swipe-up=\"swipeup($event)\" ng-swipe-down=\"swipedown($event)\" ng-swipe-left=\"swipeleft($event)\" ng-swipe-right=\"swiperight($event)\"> <div class=\"row\"> <div style=\"text-align: center; font-size: 48pt; font-weight: bold\" class=\"col-md-12\"> <!--No. Polisi :-->{{dataAuto.LicensePlate}} </div> <div class=\"col-md-12 text-center\"> <div class=\"col-md-4 text-left\"></div> <div class=\"col-md-4 text-center\">Panggil Ulang</div> <div class=\"col-md-4 text-right\"></div> </div> </div> <div> <div class=\"col-md-4 text-left\"></div> <!-- up --> <div class=\"col-md-4 text-center\"> <img src=\"images/swipeImage/swipe.gif\" height=\"30px\" style=\"transform:rotate(180deg)\"> </div> <div class=\"col-md-4 text-right\"></div> <!-- <div class=\"col-md-4\"><button style=\"background-color:#d53337;color:#fff;\" ng-click=\"skipCall()\" type=\"button\" class=\"btn btn-lg btn-block\">Berikutnya</button></div>\r" +
    "\n" +
    "                        <div class=\"col-md-4\"><button ng-click=\"callCustomerAgain()\" style=\"background-color:#f3e908;color:#fff;\" type=\"button\" class=\"btn btn-lg btn-block\">Panggil Ulang</button></div>\r" +
    "\n" +
    "                        <div class=\"col-md-4\"><button style=\"background-color:#0ef308; color:#fff;\" ng-click=\"pickEstimates()\" type=\"button\" class=\"btn btn-lg btn-block\">Start Reception</button></div> --> </div> <div class=\"col-md-12 text-center\"> <div class=\"row\"> <!-- left --> <div class=\"col-md-4 col-sm-4 col-xs-4 text-left\">Berikutnya &nbsp; <img src=\"images/swipeImage/swipe.gif\" height=\"30px\" style=\"transform:rotate(90deg)\"> </div> <div class=\"col-md-4 col-sm-4 col-xs-4 text-center\"> <h1>{{callTimer}}</h1> </div> <!-- right --> <div class=\"col-md-4 col-sm-4 col-xs-4 text-right customStyleRight\"> <img src=\"images/swipeImage/swipe.gif\" height=\"30px\" style=\"transform:rotate(270deg)\"> &nbsp; Start Reception </div> </div> </div> <div> <div class=\"col-md-4 text-center\"></div> <!-- down --> <div class=\"col-md-4 text-center\"> <img src=\"images/swipeImage/swipe.gif\" height=\"30px\"> </div> <!-- <div class=\"col-md-4 text-center\"><button ng-click=\"callCancel()\" type=\"button\" class=\"btn btn-block btn-default\">Kembali</button></div> --> <div class=\"col-md-4 text-center\"></div> </div> <div class=\"col-md-12 text-center\"> <div class=\"col-md-4 text-center\"></div> <div class=\"col-md-4 text-center\">Kembali</div> <div class=\"col-md-4 text-center\"></div> </div> <!-- <h1>Swipe me up!</h1> --> <!-- <div class=\"col-md-12\">Nomor Polisi : {{dataAuto.LicensePlate}}</div>\r" +
    "\n" +
    "                    <div class=\"col-md-4\"><button ng-click=\"skipCall()\" type=\"button\" class=\"btn btn-block btn-info\">Berikutnya</button></div>\r" +
    "\n" +
    "                    <div class=\"col-md-4\"><button ng-click=\"callCustomerAgain()\" type=\"button\" class=\"btn btn-block btn-warning\">Panggil Ulang</button></div>\r" +
    "\n" +
    "                    <div class=\"col-md-4\"><button ng-click=\"pickEstimates()\" type=\"button\" class=\"btn btn-block btn-success\">Start Reception</button></div>\r" +
    "\n" +
    "                    <div class=\"col-md-12 text-center\">\r" +
    "\n" +
    "                        <h1>{{callTimer}}</h1>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-4 text-center\"></div>\r" +
    "\n" +
    "                    <div class=\"col-md-4 text-center\"><button ng-click=\"callCancel()\" type=\"button\" class=\"btn btn-block btn-default\">Kembali</button></div>\r" +
    "\n" +
    "                    <div class=\"col-md-4 text-center\"></div> --> </div> </div> <div class=\"col-md-12\"> <div class=\"well\" style=\"margin-bottom:0px; margin-top:20px\"> <div class=\"row\"> <label class=\"col-sm-12\">Short Brief</label> </div> </div> <form class=\"form-horizontal well\"> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">General Info</label> </div> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">Pemilik</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.owner\" type=\"text\" disabled class=\"form-control\"> </div> <label class=\"col-sm-2 control-label\">On Time/Late</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.late\" type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">Pengguna</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.user\" type=\"text\" disabled class=\"form-control\"> </div> <label class=\"col-sm-2 control-label\">Waiting Time</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.waitTime\" type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">Alamat</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.address\" type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-2 control-label\">Model</label> <div class=\"col-sm-4\"> <input ng-model=\"mDataCustomer.model\" type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"form-group\" style=\"text-align: right\"> <button class=\"rbtn btn\" ng-click=\"gateOut(mData,mDataCustomer)\">Gate Out</button> </div> </form> </div> <div class=\"col-md-12\"> <div class=\"well\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"\"> <div class=\"form-group\"> <label>Job Suggest</label> <textarea class=\"form-control\" ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\"></textarea> </div> </div> <!-- <textarea ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\" style=\"height: 100px;width: 400px;\"></textarea> --> </div> </div> </div> </div> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Field Action\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">OpeNo</font> </td> <td> <font color=\"white\">Description</font> </td> </tr> <tr ng-repeat=\"a in GetDataTowass\"> <td>{{a.OpeNo}}</td> <td>{{a.Description}}</td> </tr> </table> </div> </uib-tab> <uib-tab index=\"1\" heading=\"PSFU\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table id=\"QA\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>No.</th> <th>Pertanyaan</th> <th>Jawaban Customer</th> <!-- <th>Alasan Customer</th> --> </tr> </thead> <tbody> <!-- track by $index --> <tr ng-repeat=\"item in Question track by $index\"> <td width=\"5%\">{{$index + 1}}</td> <td width=\"40%\">{{item.Description}}</td> <td width=\"25%\"> <select class=\"form-control\" name=\"yesOrNo\" ng-disabled=\"true\" skip-enable> <option ng-disabled=\"true\" skip-enable ng-repeat=\"itemAnswer in item.Answer track by $index\" value=\"{{itemAnswer.AnswerId}}\">{{itemAnswer.Description}}</option> </select> <!--<div ng-repeat=\"itemAnswer in item.Answer track by $index\">\r" +
    "\n" +
    "                                                    <bsselect data=\"item.Answer\" on-select=\"selectedAnswer(selected)\" item-value=\"itemAnswer.AnswerId\" item-text=\"Description\" ng-model=\"mFService.arrQuestion.AnswerId[$index]\" placeholder=\"Vehicle Model\">\r" +
    "\n" +
    "                                                    </bsselect>\r" +
    "\n" +
    "                                                </div>--> </td> <!-- <td width=\"30%\">\r" +
    "\n" +
    "                                        <input style=\"width : 100%;\" type=\"text\" class=\"form-control\" name=\"Reason\" placeholder=\"Reason\" ng-model=\"item.Reason\" ng-hide=\"item.ReasonStatus\" ng-disabled=\"true\" skip-enableng-maxlength=\"30\">\r" +
    "\n" +
    "                                    </td> --> </tr> </tbody> </table> </div> </uib-tab> <uib-tab index=\"2\" heading=\"CS Survey\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> </div> </uib-tab> <uib-tab index=\"3\" heading=\"MRS\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Tanggal Reminder</font> </td> <td> <font color=\"white\">Tipe Pekerjaan</font> </td> <td> <font color=\"white\">Status</font> </td> </tr> <tr ng-repeat=\"a in GetDataMRS\"> <td>{{a.ReminderDate | date : \"dd/MM/y\"}}</td> <td>{{a.Name}}</td> <td>{{a.StatusName}}</td> </tr> </table> </div> </uib-tab> <!--           <uib-tab index=\"5\" heading=\"MRS\">\r" +
    "\n" +
    "                  </uib-tab> --> </uib-tabset> <!-- Nav tabs --> <!--<ul class=\"nav nav-tabs\" role=\"tablist\" style=\"margin-top:0px;\">\r" +
    "\n" +
    "                    <li role=\"presentation\" class=\"active\"><a href=\"#home\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\">Field Action</a></li>\r" +
    "\n" +
    "                    <li role=\"presentation\"><a href=\"#profile\" aria-controls=\"profile\" role=\"tab\" data-toggle=\"tab\">TPSS</a></li>\r" +
    "\n" +
    "                    <li role=\"presentation\"><a href=\"#messages\" aria-controls=\"messages\" role=\"tab\" data-toggle=\"tab\">Follow Up</a></li>\r" +
    "\n" +
    "                    <li role=\"presentation\"><a href=\"#settings\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">PKS</a></li>\r" +
    "\n" +
    "                    <li role=\"presentation\"><a href=\"#settings\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">CS Survey</a></li>\r" +
    "\n" +
    "                    <li role=\"presentation\"><a href=\"#settings\" aria-controls=\"settings\" role=\"tab\" data-toggle=\"tab\">MRS</a></li>\r" +
    "\n" +
    "                </ul> --> <!-- Tab panes --> <!-- <div class=\"tab-content\">\r" +
    "\n" +
    "                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"home\">wew</div>\r" +
    "\n" +
    "                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"profile\">...</div>\r" +
    "\n" +
    "                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"messages\">...</div>\r" +
    "\n" +
    "                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"settings\">...</div>\r" +
    "\n" +
    "                </div> --> </div> </div> <bsui-modal show=\"listEstimation.show\" title=\"List Estimation\" data=\"lmEstimation\" on-save=\"createWo\" on-cancel=\"cancelWo\" button-settings=\"listEstimationButton\" mode=\"modalMode\"> <div ng-form name=\"contactForm\"> <div class=\"row\"> <div class=\"col-md-12\"> <p>List estimasi untuk kendaraan {{lmEstimation.policeNumber}}</p> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <table class=\"table table-hover\"> <tr> <th>No. Estimasi</th> <th>Tgl Release Estimasi</th> <th>SA</th> <th>Action</th> </tr> <tr ng-repeat=\"item in lmEstimation\"> <td>{{item.EstimationNo}}</td> <td>{{item.EstimationDate | date:'dd-MM-yyyy'}}</td> <td>{{item.EmployeeSa}}</td> <td> <button class=\"btn btn-default\" ng-click=\"pickCustomer()\" type=\"button\">Pilih</button> </td> </tr> </table> </div> </div> </div> </bsui-modal> <div ng-show=\"pageMode=='form'\" class=\"row\"> <div class=\"col-md-12\">gg</div> </div> <bsui-modal show=\"show_modal.show\" title=\"Edit\" data=\"modal_model\" on-save=\"editSave\" on-cancel=\"editCancel\" mode=\"modalMode\"> <div class=\"col-md-12\"> <label>No Polisi</label> </div> <div class=\"col-md-6\"> <input type=\"text\" name=\"LicensePlate\" ng-model=\"mData.LicensePlate\" class=\"form-control\" style=\"text-transform:uppercase\"> </div> </bsui-modal>"
  );


  $templateCache.put('app/services/reception/wo_bp/queueTakingPickEstimatesDialog.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <h4>List estimasi untuk kendaraan <b>{{mData.PoliceNumber}}</b></h4> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" ng-if=\"showGrid\"> <!--  <div ui-grid=\"listEstimasiYangBelumDigunakan\" ui-grid-selection class=\"grid ui-grid-bgst\" ui-grid-cellnav\r" +
    "\n" +
    "            ui-grid-edit ui-grid-row-edit ui-grid-pagination ui-grid-auto-resize name=\"gridBGST\"></div> --> <table class=\"table table-hover\"> <tr> <th></th> <th>No. Estimasi</th> <th>Tanggal Estimasi</th> <th>Pembayaran</th> <th>Pembuat</th> <th>Action</th> </tr> <tr ng-if=\"EstimateAppointment.length == 0\"> <td colspan=\"6\" align=\"center\">Data Tidak Ditemukan</td> </tr> <tr ng-if=\"EstimateAppointment.length > 0\" ng-repeat=\"item in EstimateAppointment\"> <td style=\"padding-bottom: 0px;padding-top:0px;padding-left:0px;padding-right:0px\"> <bscheckbox class=\"pull-right\" ng-disabled=\"item.isAppointment == 1\" ng-model=\"modelCheck[$index]\" skip-enable ng-change=\"triggeredPush(item)\"> </bscheckbox> </td> <!-- ini ada kondisi --> <td ng-if=\"item.isAppointment == 1\" style=\"padding-bottom: 0px;padding-top:0px;padding-left:0px;padding-right:0px\">{{item.EstimationNo}} <label style=\"color: #b94a48; font-size:25px\" ng-if=\"item.UsedBy == 1\"> *</label> <label style=\"color: #b94a48; font-size:25px\" ng-if=\"item.UsedBy == 2\"> **</label></td> <td ng-if=\"item.isAppointment != 1\" style=\"padding-bottom: 0px;padding-top:12px;padding-left:0px;padding-right:0px\">{{item.EstimationNo}}</td> <!-- ini ada kondisi --> <td style=\"padding-bottom: 0px;padding-top:12px;padding-left:0px;padding-right:0px\">{{item.EstimationDate | date:'dd-MM-yyyy'}}</td> <td style=\"padding-bottom: 0px;padding-top:12px;padding-left:0px;padding-right:0px\"> <p ng-if=\"item.isCash==1\">&nbsp; Cash</p><p ng-if=\"item.isCash==0\">&nbsp; Insurance</p> <!-- {{item.isCash}} --> </td> <td style=\"padding-bottom: 0px;padding-top:12px;padding-left:0px;padding-right:0px\">{{item.LastModifiedEmployeeName}}</td> <td style=\"padding-bottom: 0px;padding-top:10px;padding-left:0px;padding-right:0px\"> <i style=\"font-size:24px\" class=\"fa\" ng-click=\"lihatEstimasi(item)\">&#xf2bc;</i> </td> <!--  <button class=\"rbtn btn\" ng-click=\"confirm(EstimateAppointment[$index])\" type=\"button\">Pilih</button></td> --> <!-- <button class=\"rbtn btn\" ng-click=\"confirm(EstimateAppointment[$index]);ngDialog.closeAll();\" type=\"button\">Pilih</button></td> --> </tr> </table> </div> <div class=\"col-md-12\" ng-if=\"showForm\"> <div class=\"row\" style=\"margin:0 0 0 0\"> <div class=\"col-md-7\"> <h2><b>Service BP</b></h2> <h3 ng-if=\"dataViewEstimasi.isCash == 0\"><b>Asuransi</b></h3> </div> <div class=\"col-md-5\" ng-if=\"dataViewEstimasi.isCash == 0\"> <table id=\"example\" class=\"table cuklah\" width=\"100%\"> <tr> <td>Nama Asuransi</td> <td>:</td> <td>{{dataViewEstimasi.InsuranceName}}</td> </tr> <tr> <td>No. Polis Asuransi</td> <td>:</td> <td style=\"text-transform: uppercase\">{{dataViewEstimasi.NoPolis}}</td> </tr> <tr> <td>Periode Polis</td> <td>:</td> <td>{{dataViewEstimasi.PolisDateFrom}} &nbsp; s/d &nbsp; {{dataViewEstimasi.PolisDateTo}}</td> </tr> </table> </div> <div class=\"col-md-12\"> <table id=\"example\" class=\"table cuklah\" width=\"100%\"> <tr> <td>No. Estimasi</td> <th>:</th> <td><b>{{dataViewEstimasi.EstimationNo}}</b></td> <td>No. Polisi</td> <td>:</td> <td style=\"text-transform: uppercase\"><b>{{dataViewEstimasi.PoliceNumber}}</b></td> <td>Nama Pemilik</td> <td>:</td> <td>{{dataViewEstimasi.ContactPerson}}</td> </tr> <tr> <td style=\"width: 5%\">Tgl/Jam Estimasi</td> <th>:</th> <td><b>{{dataViewEstimasi.EstimationDate}}</b></td> <td>Model</td> <td>:</td> <td style=\"text-transform: uppercase\">{{dataViewEstimasi.VehicleModelName}} - {{dataViewEstimasi.VehicleTypeDesc}}</td> <td>Kontak Person</td> <td>:</td> <td>{{dataViewEstimasi.ContactPerson}}</td> </tr> <tr> <td>KM</td> <th>:</th> <td>{{dataViewEstimasi.Km}}</td> <td>Warna</td> <td>:</td> <td>{{dataViewEstimasi.ColorName}}</td> <td>No. Telepon</td> <td>:</td> <td>{{dataViewEstimasi.PhoneContactPerson1}}</td> </tr> <tr> <td style=\"width: 2%\">No. Rangka</td> <th style=\"width: 1%\">:</th> <td style=\"width: 5%; text-transform: uppercase\"> {{dataViewEstimasi.VIN}} </td> <td style=\"width: 2%\">No. Mesin</td> <td style=\"width: 1%\">:</td> <td style=\"width: 5%; text-transform: uppercase\"> {{dataViewEstimasi.EngineNo}} </td> <td style=\"width: 5%\">Alamat</td> <td style=\"width: 1%\">:</td> <td style=\"width: 11%\"> {{dataViewEstimasi.Alamat}} </td> </tr> </table> </div> <div class=\"col-md-12\" style=\"margin-top:10px\" ng-if=\"showForm\"> <div class=\"col-md-12\" ng-if=\"showForm\"> <bslist data=\"gridWorkViewEstimasi\" m-id=\"TaskBPId\" d-id=\"PartsId\" list-model=\"lmModelViewEstimasi\" list-detail-model=\"ldModelViewEstimasi\" grid-detail=\"gridPartsDetailViewEstimasi\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApiView\" modal-title=\"Job Data\" button-settings=\"listButtonSettingsAddress2\" modal-title-detail=\"Parts Data\" list-title=\"Order Pekerjaan\"> <bslist-template> <div class=\"col-md-1\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.NumberParent }}</strong></p> </div> <div class=\"col-md-5\"> <div class=\"col-md-12\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> </div> <div class=\"col-md-12\"> <p>Harga : {{lmItem.Fare | currency:\"Rp.\"}}</p> </div> </div> <div class=\"col-md-6\"> <p>Satuan : {{lmItem.UnitBy}}</p> <p>Pembayaran : {{lmItem.PaidBy}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-1\"> <p><strong>{{ ldItem.NumberChild }}</strong></p> </div> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp.\"}}</p> </div> <div class=\"col-md-2\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> </bslist> </div> </div> <div class=\"col-md-4\"></div> <div class=\"col-md-8\"> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Nominal Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalWorkViewEstimasi | currency:\"\":0}}</td> <td style=\"text-align: right\">{{ DiscountTaskViewEstimasi }}<span ng-if=\"DiscountTaskViewEstimasi != '' \">%</span></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalWorkDiscountedViewEstimasi | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ subTotalWorkSummaryViewEstimasi | currency:\"\":0}}</td> <!-- <td style=\"text-align: right;\"><span style=\"float: left;\">Rp. </span>{{(totalWorkViewEstimasi * DiscountTaskViewEstimasi/100) | currency:\"\":0}}</td>\r" +
    "\n" +
    "                            <td style=\"text-align: right;\"><span style=\"float: left;\">Rp. </span>{{totalWorkViewEstimasi - (totalWorkViewEstimasi * DiscountTaskViewEstimasi/100) | currency:\"\":0}}</td> --> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalMaterialViewEstimasi | currency:\"\":0}}</td> <td style=\"text-align: right\">{{ DiscountPartsViewEstimasi }}<span ng-if=\"DiscountPartsViewEstimasi != '' \">%</span></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalMaterialDiscountedViewEstimasi | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ subTotalMaterialSummaryViewEstimasi | currency:\"\":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalPPNViewEstimasi | currency:\"\":0 }}</td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalEstimasiViewEstimasi | currency:\"\":0}}</td> </tr> <tr> <td>Total DP Material (Parts / Bahan)</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDpViewEstimasi | currency:\"\":0}}</td> </tr> </tbody> </table> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 25px\" ng-repeat=\"a in dataPKS\"> <label>Nama Perusahaan : {{a.CompanyName}}</label><br> <!-- <label ng-show=\"a.CustomerPKSTop < 0 || a.CustomerPKSTop == null\">Balances : {{a.Balances | currency:\"Rp.\"}}</label> --> <label>Saldo : {{a.AvailableBalance | currency:\"Rp. \":0}}</label><br> <!-- <label ng-show=\"a.CustomerPKSTop > 0 || a.CustomerPKSTop !== null\">TOP : {{a.CustomerPKSTop}} Hari</label><br> --> <label ng-hide=\"{{a.CustomerPKSTop == 0}}\">TOP : {{a.CustomerPKSTop}} Hari</label><br ng-hide=\"{{a.CustomerPKSTop == 0}}\"> <label>Periode Mulai : {{a.StartPeriod | date:'dd-MM-yyyy'}}</label><br> <label>Periode Selesai : {{a.EndPeriod | date:'dd-MM-yyyy'}}</label> </div> </div> <div class=\"row\" style=\"margin-top: 15px\"> <div class=\"col-md-8\" ng-if=\"showGrid\"> <div class=\"col-md-12\" style=\"text-align:left;margin-left:0px;padding-left:0px\"> <p style=\"float:left; margin-top:8px\">( <span style=\"color:red\">*</span> ) Data No. Estimasi sudah di gunakan pada Appointment dan tidak dapat digunakan Kembali</p> </div> <div class=\"col-md-12\" style=\"text-align:left;margin-left:0px;padding-left:0px\"> <p style=\"float:left; margin-top:0px\">( <span style=\"color:red\">**</span> ) Data No. Estimasi sudah di gunakan pada WO dan tidak dapat digunakan Kembali</p> </div> </div> <div class=\"col-md-4 text-right\" ng-if=\"showGrid\"> <button ng-click=\"ngDialog.closeAll();\" class=\"btn btn-default\" type=\"button\">Batal</button> <button ng-click=\"confirm();\" class=\"btn btn-default\" type=\"button\">Buat WO</button> </div> <div class=\"col-md-12 text-right\" ng-if=\"showForm\"> <button ng-click=\"kembaliBTN();\" class=\"btn btn-default\" type=\"button\">Kembali</button> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/repairProcessGR.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.ChangeColor {\r" +
    "\n" +
    "        background-color: rgb(213, 51, 55);\r" +
    "\n" +
    "        border-color: rgb(213, 51, 55);\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .DefaultColor {\r" +
    "\n" +
    "        background-color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .StatusClocOn {\r" +
    "\n" +
    "        background-color: green;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .ngdialog.ngdialog-theme-plain.custom-width .ngdialog-content {\r" +
    "\n" +
    "        width: 70%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .bs-list > .panel-body > .ui-grid-nodata{\r" +
    "\n" +
    "        position: relative !important;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <!-- <bsform-grid\r" +
    "\n" +
    "        ng-form=\"NegotiationForm\"\r" +
    "\n" +
    "        factory-name=\"Negotiation\"\r" +
    "\n" +
    "        model=\"mRole\"\r" +
    "\n" +
    "        model-id=\"Id\"\r" +
    "\n" +
    "        loading=\"loading\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        get-data=\"getData\"\r" +
    "\n" +
    "        selected-rows=\"selectedRows\"\r" +
    "\n" +
    "        on-select-rows=\"onSelectRows\"\r" +
    "\n" +
    "\r" +
    "\n" +
    "        form-name=\"RoleForm\"\r" +
    "\n" +
    "        form-title=\"Role\"\r" +
    "\n" +
    "        modal-title=\"Role\"\r" +
    "\n" +
    "        modal-size=\"small\"\r" +
    "\n" +
    "        icon=\"fa fa-fw fa-child\"\r" +
    "\n" +
    "        >\r" +
    "\n" +
    " --> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- </bsform-grid> --> <!-- IF NOT USING FORMLY --> <!-- Form Filter View WO List --> <div class=\"row\" ng-hide=\"ShowRepair\" ng-init=\"getDataDefault()\"> <div class=\"col-md-12\"> <!-- Bagian FIlter --> <div class=\"col-md-3\" style=\"margin-top: 10px\" ng-init=\"dataTeknisi()\"> <label>Nama Teknisi/Foreman</label> <bsselect name=\"EmployeeId\" ng-model=\"mData.EmployeeId\" data=\"EmployeeId\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" placeholder=\"Teknisi\" ng-keypress=\"allowPatternFilter($event,2,mData)\"> </bsselect> </div> <div class=\"col-md-3\" style=\"margin-top: 32px\"> <bsselect name=\"ChoseFilter\" ng-model=\"mFilter.ChoseFilterId\" data=\"ChoseFilter\" item-text=\"ChoseFilter\" item-value=\"ChoseFilterId\" placeholder=\"Tipe Filter\" on-select=\"selectFilter(selected)\"> </bsselect> </div> <div class=\"col-md-3\" ng-show=\"mFilter.ChoseFilterId == 1\" style=\"margin-top: 15px\"> <bslabel>No Wo</bslabel> <input class=\"form-control\" type=\"text\" name=\"KodeWO\" ng-model=\"mData.NoWo\" placeholder=\"NO WO\" ng-keypress=\"allowPatternFilter($event,2,mData)\"> </div> <div class=\"col-md-3\" ng-show=\"mFilter.ChoseFilterId == 2\" style=\"margin-top: 15px\"> <bslabel>No Polisi</bslabel> <input class=\"form-control\" type=\"text\" name=\"NomorPolisi\" ng-model=\"mData.PoliceNumber\" placeholder=\"NO Polisi\" ng-keypress=\"allowPatternFilter($event,2,mData)\"> </div> <div class=\"col-md-3\" style=\"margin-top: 10px\"> <label>Tanggal</label> <bsdatepicker name=\"tglProcessGR\" date-options=\"DateOptions\" ng-model=\"mData.FilterDate\"> </bsdatepicker> <!-- <bserrmsg field=\"tglProcessGR\"></bserrmsg> --> </div> <div class=\"col-md-3\" style=\"margin-top: 32px\"> <!-- background-color:rgb(213,51,55); --> <button ng-click=\"getData(mData.PoliceNumber,mData.NoWo,mData.EmployeeId,mData.FilterDate)\" class=\"btn wbtn\"> <i class=\"large icons\"> <i class=\"database icon\"> </i> <i class=\"inverted corner filter icon\"> </i> </i> Search <!-- <span class=\"glyphicon glyphicon-search\"></span> --> </button> </div> <!--Akhir Bagian FIlter --> <!-- Bagian Table  --> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Menunggu Pengerjaan ({{CountProdFinal}})</font> </td> <td> <font color=\"white\">Dalam Pengerjaan ({{CountInProdFinal}})</font> </td> <td> <font color=\"white\">Menunggu FI ({{CountWaitFiFinal}})</font> </td> </tr> <tr> <td width=\"33%\"> <chip-technicianmainmenu ng-repeat=\"xDataWo1 in DataWoList  | filter:search0\" policenumber=\"xDataWo1.PoliceNumber\" date=\"xDataWo1.PlanStart\" modelname=\"xDataWo1.ModelType\" planfinish=\"xDataWo1.PlanDateFinish\" timeplanfinish=\"xDataWo1.PlanFinish\" employeesa=\"xDataWo1.EmployeeSa\" jobtype=\"xDataWo1.Process.Name\" ng-click=\"getDataDetail(xDataWo1.JobId, 1)\" midcolor=\"xDataWo1.color\" ng-hide=\"xDataWo1.StallId <= 0\" status=\"xDataWo1.Status\" iswaiting=\"xDataWo1.IsWaiting\" jmltask=\"xDataWo1.jmltask\" returnjob=\"xDataWo1.Revision\" jobtwc=\"xDataWo1.adaJobTWC\" jobpwc=\"xDataWo1.adaJobPWC\" waitingpart=\"xDataWo1.adaMenungguParts\"></chip-technicianmainmenu> </td> <td width=\"33%\"> <chip-technicianmainmenu ng-repeat=\"xDataWo1 in DataWoList  | filter:search\" policenumber=\"xDataWo1.PoliceNumber\" date=\"xDataWo1.PlanStart\" modelname=\"xDataWo1.ModelType\" planfinish=\"xDataWo1.PlanDateFinish\" timeplanfinish=\"xDataWo1.PlanFinish\" employeesa=\"xDataWo1.EmployeeSa\" jobtype=\"xDataWo1.Process.Name\" ng-click=\"getDataDetail(xDataWo1.JobId, 1)\" midcolor=\"xDataWo1.color\" ng-hide=\"xDataWo1.StallId <= 0\" status=\"xDataWo1.Status\" iswaiting=\"xDataWo1.IsWaiting\" jmltask=\"xDataWo1.jmltask\" returnjob=\"xDataWo1.Revision\" jobtwc=\"xDataWo1.adaJobTWC\" jobpwc=\"xDataWo1.adaJobPWC\" waitingpart=\"xDataWo1.adaMenungguParts\"></chip-technicianmainmenu> </td> <td width=\"33%\"> <chip-technicianmainmenu ng-repeat=\"xDataWo1 in DataWoList  | filter:search2\" policenumber=\"xDataWo1.PoliceNumber\" date=\"xDataWo1.PlanStart\" modelname=\"xDataWo1.ModelType\" planfinish=\"xDataWo1.PlanDateFinish\" timeplanfinish=\"xDataWo1.PlanFinish\" employeesa=\"xDataWo1.EmployeeSa\" jobtype=\"xDataWo1.Process.Name\" ng-click=\"getDataDetail(xDataWo1.JobId, 1)\" midcolor=\"xDataWo1.color\" ng-hide=\"xDataWo1.StallId <= 0\" status=\"xDataWo1.Status\" iswaiting=\"xDataWo1.IsWaiting\" jmltask=\"xDataWo1.jmltask\" returnjob=\"xDataWo1.Revision\" jobtwc=\"xDataWo1.adaJobTWC\" jobpwc=\"xDataWo1.adaJobPWC\" waitingpart=\"xDataWo1.adaMenungguParts\"></chip-technicianmainmenu> </td> </tr> </table> </div> <!-- Akhir Bagian Table  --> <!-- Bagian Keterangan Warna --> <div class=\"col-md-12\"> <div class=\"col-xs-12\"> <div class=\"col-md-3 col-xs-3\" style=\"margin-top: 5px\"> <div class=\"col-md-6 col-xs-6\" style=\"background-color: green;height: 20px;width: 40px\"></div> <div class=\"col-md-6 col-xs-6\"> Walk In </div> </div> <div class=\"col-md-3 col-xs-3\" style=\"margin-top: 5px\"> <div class=\"col-md-6 col-xs-6\" style=\"background-color: red;height: 20px;width: 40px\"></div> <div class=\"col-md-6 col-xs-6\"> Problem / Pause </div> </div> <div class=\"col-md-3 col-xs-3\" style=\"margin-top: 5px\"> <div class=\"col-md-6 col-xs-6\" style=\"background-color: yellow;height: 20px;width: 40px\"></div> <div class=\"col-md-6 col-xs-6\"> In Progress </div> </div> <div class=\"col-md-3 col-xs-3\" style=\"margin-top: 5px\"> <div class=\"col-md-6 col-xs-6\" style=\"background-color: blue;height: 20px;width: 40px\"></div> <div class=\"col-md-6 col-xs-6\"> Booking / Appoinment </div> </div> </div> </div> <!-- Akhir Bagian Keterangan Warna --> </div> </div> <!-- Akhir Form Filter View WO List --> <div class=\"row\"> <div class=\"col-xs-12\" style=\"text-align: right;margin-bottom: 20px\"> <button class=\"wbtn btn\" ng-click=\"Kembali()\" ng-show=\"ShowBtnKembali\">Kembali</button> </div> </div> <div class=\"row\" ng-show=\"ShowRepair\"> <div class=\"col-md-12 col-xs-12\"> <div class=\"col-md-12 col-xs-12\" style=\"padding-left: 0px;padding-right: 0px\"> <div class=\"col-md-7\"> <div class=\"col-md-12\" style=\"border:1px solid #ccc; background-color: #fff\"> <div class=\"col-md-12\"> <div class=\"col-md-12\" style=\"margin-top: 10px\"> <label>Nama Teknisi :</label><label ng-repeat=\"a in JobTaskTeknisi\">{{a.EmployeeName}} </label> </div> </div> <div class=\"col-md-12\"> <div ng-include=\"'app/services/production/repairProcessGR/BoardRepairProcessGR.html'\" style=\"margin-top: 35px\"> </div> </div> <!-- <h1>Info Terkait Data Stall</h1> --> </div> </div> <div class=\"col-md-5\"> <div class=\"col-xs-12\" style=\"border:1px solid #ccc;height: 290px\" ng-repeat=\"z in xDataWoList | filter : {JobId : JbId}\"> <div class=\"col-xs-12\" style=\"margin-top: 65px\"> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Full Model</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.KatashikiCode}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">W/O No.</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.WoNo}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">SA</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.UserSa.EmployeeName}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Nomor Polisi</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.PoliceNumber}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Model</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.ModelType}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Mulai</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.PlanDateStart | date : 'dd/MM/yyyy' }} {{z.PlanStart}}</div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Selesai</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.PlanDateFinish | date : 'dd/MM/yyyy' }} {{z.PlanFinish}}</div> </div> <div class=\"col-xs-12\" ng-show=\"z.FixedDeliveryTime2 == null\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Janji Penyerahan</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\"> {{z.FixedDeliveryTime1 | date : 'dd/MM/yyyy'}} {{JanjiPenyerahan}}</div> </div> <div class=\"col-xs-12\" ng-show=\"z.FixedDeliveryTime2 !== null\"> <div class=\"col-xs-4\" style=\"padding-right: 0px;padding-left: 0px;font-size:14px\">Janji Penyerahan</div> <div class=\"col-xs-1\" style=\"padding-left: 0px;font-size:14px\"> : </div> <div class=\"col-xs-7\" style=\"padding-left: 0px;font-size:14px\">{{z.FixedDeliveryTime2 | date : 'dd/MM/yyyy'}} {{JanjiPenyerahan}}</div> </div> </div> </div> </div> </div> <!-- Permintaan dan keluhan pelanggan --> <!--             <div class=\"col-md-12 col-xs-12\" style=\"padding-left: 0px;margin-top: 15px;\">\r" +
    "\n" +
    "              \r" +
    "\n" +
    "                 <uib-accordion close-others=\"false\">\r" +
    "\n" +
    "                   <div uib-accordion-group class=\"panel-default\" is-open=\"status.open\">\r" +
    "\n" +
    "                     <uib-accordion-heading>\r" +
    "\n" +
    "                       <label>Permintaan dan Keluhan Pelanggan</label>\r" +
    "\n" +
    "                       <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i>\r" +
    "\n" +
    "                     </uib-accordion-heading>\r" +
    "\n" +
    "                     \r" +
    "\n" +
    "                     <div class=\"col-md-12\">\r" +
    "\n" +
    "                      <div class=\"col-md-6\">\r" +
    "\n" +
    "                        Keluhan\r" +
    "\n" +
    "                        <p  ng-repeat = \"a in xDataWoList\">\r" +
    "\n" +
    "                          {{$index + 1 }}. {{a.JobComplaint[$index].ComplaintDesc}}\r" +
    "\n" +
    "                        </p>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                      <div class=\"col-md-6\">\r" +
    "\n" +
    "                        Permintaan\r" +
    "\n" +
    "                        <p  ng-repeat = \"a in xDataWoList\">\r" +
    "\n" +
    "                          {{$index + 1}}. {{a.JobRequest[$index].RequestDesc}}\r" +
    "\n" +
    "                        </p>\r" +
    "\n" +
    "                      </div>\r" +
    "\n" +
    "                     </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                     <div class=\"col-md-12\" style=\"margin-top: 10px;\">\r" +
    "\n" +
    "                       <div class=\"col-md-12\">\r" +
    "\n" +
    "                          <textarea style=\"width: 100%; height: 150px;\" placeholder=\"Action Yang Dilakukan Teknisi\" ng-model=\"mData.TechnicianNotes\"></textarea>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                          <button class=\"rbtn btn\" ng-click=\"SimpanNote();\">Simpan</button>\r" +
    "\n" +
    "                       </div>\r" +
    "\n" +
    "                     </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                   </div>\r" +
    "\n" +
    "                 </uib-accordion>\r" +
    "\n" +
    "            </div> --> <!-- Permintaan dan keluhan pelanggan --> <div class=\"col-md-12 col-xs-12\" ng-show=\"ShowRepair\" ng-repeat=\"z in xDataWoList | filter : {JobId : JbId}\"> <uib-tabset active=\"activeJustified\" justified=\"true\" style=\"margin-top: 40px\"> <!-- <bstabwiz fulwidth=\"true\"> --> <uib-tab index=\"0\" heading=\"Job Instruction\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036 || user.RoleId == 1033\"> <!-- <bstabwiz-pane title=\"Job Instruction\"> --> <div ng-show=\"user.RoleId == 1 || user.RoleId == 1036 || user.RoleId == 1033\" ng-include=\"'app/services/production/repairProcessGR/repairProcessGR_JobInstruction.html'\"> </div> <!-- </bstabwiz-pane> --> </uib-tab> <uib-tab index=\"1\" heading=\"Final Inspection\" ng-show=\"showFi\" ng-click=\"getFIById(z.JobId,z.JobTask)\"> <!-- <bstabwiz-pane title=\"Final Inspection\"> --> <div ng-include=\"'app/services/production/repairProcessGR/repairProcessGR_FinalInspection.html'\" ng-show=\"showFi2\"> </div> <!-- </bstabwiz-pane> --> </uib-tab> <!-- \r" +
    "\n" +
    "                 <uib-tab index=\"2\" heading=\"Job Suggest\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036 || user.RoleId == 1033\">\r" +
    "\n" +
    "                    <div ng-include=\"'app/services/production/repairProcessGR/repairProcessGR_JobSuggest.html'\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036 || user.RoleId == 1033\"></div>\r" +
    "\n" +
    "                 </uib-tab> --> <uib-tab index=\"3\" heading=\"Wo Detail\" ng-click=\"WoDetail(z.JobId)\"> <div class=\"col-md-12\" ng-repeat=\"fDwo in DataWOTask\" style=\"margin-top: 10px;padding-right: 0px;padding-left: 0px\" ng-disabled=\"true\"> <uib-accordion close-others=\"oneAtATime\" ng-disabled=\"true\"> <div uib-accordion-group is-open=\"true\" class=\"panel-default\"> <uib-accordion-heading> <label>Nama Pekerjaan : {{fDwo.TaskName}}</label> <br> <label>Tipe : {{fDwo.JobType.Name}} </label> <!-- <label>Tipe : {{fDwo.JobTypeId}} </label> --> <br> <label>Flat Rate : {{fDwo.FlatRate}}</label> <br> <label>Act Rate : {{fDwo.ActualRate}}</label> <!-- <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> --> </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <table class=\"table table-striped table-bordered\"> <tr> <td>Nomor Material</td> <td>Nama Material</td> <td>Status</td> <td>ETA</td> <td>Permintaan Qty</td> <td>Issue Qty</td> <td>Satuan</td> </tr> <tr ng-repeat=\"fDParts in fDwo.JobParts\"> <td>{{fDParts.Part.PartsCode}}</td> <td>{{fDParts.PartsName}}</td> <!-- <td>{{fDParts.Status}}</td> --> <!-- <td ng-if=\"fDParts.Qty == fDParts.QtyGI\">Tersedia</td>\r" +
    "\n" +
    "                                                <td ng-if=\"fDParts.Qty != fDParts.QtyGI\">Tidak Tersedia</td> --> <td>{{fDParts.Availability}}</td> <td>{{fDParts.ETA | date:\"dd-MM-yyyy\"}}</td> <td>{{fDParts.Qty}}</td> <td>{{fDParts.QtyGI}}</td> <td>{{fDParts.Satuan.Name}}</td> </tr> </table> <!-- <div ui-grid=\"gridWODetail\" ui-grid-cellNav ui-grid-auto-resize class=\"grid\" ng-cloak style=\"height: 200px;\"></div> --> </div> </div> </div> </uib-accordion> </div> <div class=\"col-md-12\"> <button class=\"btn btn-danger\" style=\"background-color: rgb(213,51,55)\" ng-click=\"getDataPrediagnose()\">Diagnoses Detail</button> <button class=\"btn btn-danger\" style=\"background-color: rgb(213,51,55)\" ng-disabled=\"showBtnTambah\" ng-click=\"addPekerjaan()\">Tambah Pekerjaan</button> <button class=\"btn btn-danger\" style=\"background-color: rgb(213,51,55)\" ng-disabled=\"showBtnTambah\" ng-click=\"addMaterial()\">Tambah Parts</button> </div> </uib-tab> <!-- \r" +
    "\n" +
    "                 ini tadinya riwayat service yg ternyata harus dipindah\r" +
    "\n" +
    "                  <uib-tab index=\"3\" heading=\"Riwayat Service\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036\">\r" +
    "\n" +
    "                    <div class=\"col-md-12\" style=\"margin-top: 10px;\" ng-repeat = \"b in VinData\" ng-show=\"user.RoleId == 1 || user.RoleId == 1036\">\r" +
    "\n" +
    "                      <vehicle-history vin=\"b.VIN\"></vehicle-history>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </uib-tab> --> <uib-tab index=\"4\" heading=\"WAC\" ng-click=\"chooseVType()\"> <!-- <bstabwiz-pane title=\"Riwayat Service\"> --> <div class=\"col-md-12\" id=\"dataWAC\" style=\"margin-top: 10px\"> <div ng-include=\"'app/services/production/repairProcessGR/repairProcessGR_WAC.html'\"></div> <!-- <vehicle-history vin=\"b.VIN\"></vehicle-history> --> </div> <!-- </bstabwiz-pane> --> </uib-tab> <!-- </bstabwiz> --> </uib-tabset> </div> </div> </div> <bsui-modal show=\"show_modal.show\" title=\"Tambah Material\" data=\"modal_model\" on-save=\"doAddMaterial\" on-cancel=\"rescheduleCancel\" mode=\"modalMode\"> <div class=\"col-md-12\"> <label>Nama Material</label> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <textarea style=\"width:100%\" ng-model=\"modal_model.Message\"></textarea> </div> </bsui-modal> <bsui-modal show=\"show_modalTask.show\" title=\"Tambah Pekerjaan\" data=\"modal_modelTask\" on-save=\"doAddPekerjaan\" on-cancel=\"rescheduleCancel\" mode=\"modalModeTask\"> <div class=\"col-md-12\"> <label>Nama Pekerjaan</label> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <textarea style=\"width:100%\" ng-model=\"modal_modelTask.Message\"></textarea> </div> </bsui-modal>"
  );


  $templateCache.put('app/services/reception/wo_bp/serviceExplanation.html',
    "<div ng-form=\"serviceExplanationWO\" class=\"row\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table> <tr style=\"padding: 10px\"> <td style=\"padding: 10px\">Nomor WO</td> <td style=\"padding: 10px\">:</td> <td style=\"padding: 10px\">{{mData.WoNo}}</td> </tr> <tr style=\"padding: 10px\"> <td style=\"padding: 10px\">Nomor Polisi</td> <td style=\"padding: 10px\">:</td> <td style=\"padding: 10px\">{{mData.PoliceNumber}}</td> </tr> </table> <!--\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "              <div class=\"form-group\">\r" +
    "\n" +
    "                <label>Nomor WO </label>\r" +
    "\n" +
    "                <label> : </label>\r" +
    "\n" +
    "                <label>{{mData.WoNo}}</label>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "              <div class=\"form-group\">\r" +
    "\n" +
    "                <label>Nomor Polisi </label>\r" +
    "\n" +
    "                <label> : </label>                \r" +
    "\n" +
    "                <label>{{mData.PoliceNumber}}</label>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>--> </div> </div> <div class=\"col-md-12\"> <uib-accordion close-others=\"oneAtATime\"> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Hasil Pekerjaan </div> </uib-accordion-heading> <div class=\"col-md-12\"> <bslist data=\"gridWorkEstimate\" m-id=\"tmpTaskId\" d-id=\"PartId\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" button-settings=\"listView\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApi\" confirm-save=\"false\" list-title=\"Job List\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-1\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.NumberParent }}</strong></p> </div> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <!-- <p>Tipe :{{lmItem.catName}}</p> --> </div> <div class=\"col-md-3\"> <!-- <p>Flat Rate: {{lmItem.FlatRate}}</p> --> </div> </div> <div class=\"col-md-5\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-1\"> <p><strong>{{ ldItem.NumberChild }}</strong></p> </div> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <!-- <p>Pembayaran : {{ ldItem.paidName}}</p> --> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-2\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/appointment/appointmentServiceGr/modalFormMaster.html'\"> --> <div ng-include=\"'app/services/lib/ModalAppointmentGR/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/appointment/appointmentServiceGr/modalFormDetail.html'\"> --> <div ng-include=\"'app/services/lib/ModalAppointmentGR/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Teknisi</label> <!-- <input type=\"text\" name=\"namaMekanik\" class=\"form-control\" placeholder=\"Nama Mekanik\" ng-model=\"mData.Technicians\" ng-disabled=\"true\" skip-enable> --> <textarea type=\"text\" name=\"namaMekanik\" class=\"form-control\" placeholder=\"Nama Mekanik\" ng-model=\"mData.Technicians\" ng-disabled=\"true\" skip-enable></textarea> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Jam Mulai (Plan)</label> <!-- <br> --> <!-- {{PlanStart}} --> <input type=\"text\" name=\"PlanStart\" class=\"form-control\" placeholder=\"PlanStart\" ng-model=\"PlanStart\" ng-disabled=\"true\" skip-enable> <!-- <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.PlanStart\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\">\r" +
    "\n" +
    "                        </bsdatepicker> --> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Jam Selesai (Plan)</label> <!-- <br>       --> <!-- {{PlanFinish}} --> <input type=\"text\" name=\"PlanFinish\" class=\"form-control\" placeholder=\"PlanFinish\" ng-model=\"PlanFinish\" ng-disabled=\"true\" skip-enable> <!-- <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.PlanFinish\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\">\r" +
    "\n" +
    "                      </bsdatepicker> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Jam Mulai (Aktual)</label> <input type=\"text\" name=\"ClockOn\" class=\"form-control\" placeholder=\"ClockOn\" ng-model=\"ClockOn\" ng-disabled=\"true\" skip-enable> <!-- {{PlanFinish}} --> <!-- <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.PlanStart\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                        </bsdatepicker> --> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Jam Selesai (Aktual)</label> <input type=\"text\" name=\"ClockOff\" class=\"form-control\" placeholder=\"ClockOff\" ng-model=\"ClockOff\" ng-disabled=\"true\" skip-enable> <!-- {{PlanFinish}} --> <!--  <bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.PlanFinish\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                      </bsdatepicker> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <label>Hasil Final Inspection</label> <div class=\"form-group\"> <div class=\"radio-inline\" style=\"padding-left:20px\"> <label class=\"radio\" style=\"margin-top:5px;margin-bottom:6px\"> <input type=\"radio\" name=\"typeAppointmentGR\" class=\"radiobox style-0\" ng-model=\"filter.Type\" ng-change=\"cekType(filter.Type)\" ng-disabled=\"true\" skip-enable value=\"1\"> <span>Ok</span> </label> </div> <div class=\"radio-inline\" style=\"padding-left:20px\"> <label class=\"radio\" style=\"margin-top:5px;margin-bottom:6px\"> <input type=\"radio\" name=\"typeAppointmentGR\" class=\"radiobox style-0\" ng-model=\"filter.Type\" ng-change=\"cekType(filter.Type)\" ng-disabled=\"true\" skip-enable value=\"0\"> <span>Not Ok</span> </label> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-3\"> <div class=\"form-group\"> <label>Nama Group</label> <input type=\"text\" name=\"namaMekanik\" class=\"form-control\" placeholder=\"Nama Group\" ng-model=\"mData.GroupName\" ng-disabled=\"true\" skip-enable> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Waktu Final Inspection</label> <!-- <bsdatepicker name=\"firstDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.waktuInspection\" ng-change=\"DateChange(dt)\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                      </bsdatepicker> --> <input type=\"text\" name=\"firstDate\" class=\"form-control\" placeholder=\"Waktu Final Inspection\" ng-model=\"mData.waktuInspection\" ng-disabled=\"true\" skip-enable> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <fieldset style=\"padding:10px;padding-top:0px;border:2px solid #e2e2e2\"> <legend style=\"margin-bottom:0px;margin-left:10px;width:inherit;border-bottom:0px\"> Job Suggest </legend> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <textarea ng-model=\"mData.Suggestion\" class=\"form-control\"></textarea> </div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-md-3\"> <label>Kategori Job Suggest</label> </div> <div class=\"col-md-6\"> <div class=\"col-xs-6\" ng-disabled=\"true\" skip-enable> <bscheckbox ng-model=\"mData.CategoryJobSuggestKeamanan\" label=\"Safety\" ng-click=\"CountShift()\" true-value=\"1\" false-value=\"0\" ng-disabled=\"true\" skip-enable> </bscheckbox> <bscheckbox ng-model=\"mData.CategoryJobSuggestPeraturan\" label=\"Regulation\" ng-click=\"CountShift()\" true-value=\"2\" false-value=\"0\" ng-disabled=\"true\" skip-enable> </bscheckbox> </div> <div class=\"col-xs-6\"> <bscheckbox ng-model=\"mData.CategoryJobSuggestKenyamanan\" label=\"Comfortable\" ng-click=\"CountShift()\" true-value=\"4\" false-value=\"0\" ng-disabled=\"true\" skip-enable> </bscheckbox> <bscheckbox ng-model=\"mData.CategoryJobSuggestEfisiensi\" label=\"Efficiency\" ng-click=\"CountShift()\" true-value=\"8\" false-value=\"0\" ng-disabled=\"true\" skip-enable> </bscheckbox> </div> </div> </div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-xs-6\"> <div class=\"form-group\"> <label>Tanggal Job Selanjutnya</label> <bsdatepicker name=\"DateAppoint\" date-options=\"DateOptions\" ng-model=\"mData.SuggestionDate\" ng-disabled=\"false\" skip-enable> </bsdatepicker> </div> </div> </div> <!-- <textarea class=\"form-control\"></textarea> --> </fieldset> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Pemeriksaan Akhir Sebelum Penyerahan </div> </uib-accordion-heading> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <!-- <textarea class=\"form-control\"></textarea> --> <div class=\"col-md-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.InsideCleaness\" label=\"Kebersihan kendaraan bagian dalam\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.OutsideCleaness\" label=\"Kebersihan kendaraan bagian luar\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.CompletenessVehicle\" label=\"Kelengkapan kendaraan (STNK, Tool Kits,Dll)\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.UsedPart\" label=\"Part Bekas\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-8\" ng-show=\"mData.UsedPart == 1\"> <input type=\"text\" class=\"form-control\" placeholder=\"Part Bekas\" name=\"\" ng-model=\"mData.OldPartKeepPlace\"> </div> <div class=\"col-md-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"mData.DriverCarpetPosition\" label=\"Safety Penempatan karpet pengemudi\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </fieldset> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Penjelasan Saat Penyerahan </div> </uib-accordion-heading> <fieldset style=\"padding:10px;border:2px solid #e2e2e2\"> <div class=\"col-md-12\"> <label>Penjelasan hasil pekerjaan</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsJobResultExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsJobResultExplainOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.IsJobResultExplainOk\"\r" +
    "\n" +
    "                          label=\"Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsJobResultExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsJobResultExplainOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Not Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-6\"> <input skip-enable ng-disabled=\"mData.IsJobResultExplainOk == 1 || mData.IsJobResultExplainOk == undefined\" type=\"text\" class=\"form-control\" placeholder=\"Alasan\" name=\"JobResultExplainReason\" ng-model=\"mData.JobResultExplainReason\"> </div> </div> </div> <div class=\"col-md-12\"> <label>Penjelasan Spare Part</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsSparePartExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsSparePartExplainOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsSparePartExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsSparePartExplainOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Not Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-6\"> <input skip-enable ng-disabled=\"mData.IsSparePartExplainOk == 1 || mData.IsSparePartExplainOk == undefined\" type=\"text\" class=\"form-control\" placeholder=\"Alasan\" name=\"SparePartExplainReason\" ng-model=\"mData.SparePartExplainReason\"> </div> </div> </div> <div class=\"col-md-12\"> <label>Penjelasan Harga</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsConfirmPriceOk\" class=\"radiobox style-0\" ng-model=\"mData.IsConfirmPriceOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsConfirmPriceOk\" class=\"radiobox style-0\" ng-model=\"mData.IsConfirmPriceOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Not Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-6\"> <input skip-enable ng-disabled=\"mData.IsConfirmPriceOk == 1 || mData.IsConfirmPriceOk == undefined\" type=\"text\" class=\"form-control\" placeholder=\"Alasan\" name=\"ConfirmPriceReason\" ng-model=\"mData.ConfirmPriceReason\"> </div> </div> </div> <div class=\"col-md-12\"> <label>Penjelasan Job Suggest</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsJobSuggestExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsJobSuggestExplainOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsJobSuggestExplainOk\" class=\"radiobox style-0\" ng-model=\"mData.IsJobSuggestExplainOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Not Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-6\"> <input skip-enable ng-disabled=\"mData.IsJobSuggestExplainOk == 1 || mData.IsJobSuggestExplainOk == undefined\" type=\"text\" class=\"form-control\" placeholder=\"Alasan\" name=\"JobSuggestExplainReason\" ng-model=\"mData.JobSuggestExplainReason\"> </div> </div> </div> <div class=\"col-md-12\"> <label>Konfirmasi cek pemeriksaan kendaraan</label> <div class=\"form-group\"> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsConfirmVehicleCheckOk\" class=\"radiobox style-0\" ng-model=\"mData.IsConfirmVehicleCheckOk\" value=\"1\"> <span>Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-2\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"IsConfirmVehicleCheckOk\" class=\"radiobox style-0\" ng-model=\"mData.IsConfirmVehicleCheckOk\" value=\"0\"> <span>Not Ok</span> </label> </div> <!-- <bscheckbox \r" +
    "\n" +
    "                          ng-model = \"mData.KlasifikasiMasalahDKota\"\r" +
    "\n" +
    "                          label=\"Not Ok\"\r" +
    "\n" +
    "                          true-value=1\r" +
    "\n" +
    "                          false-value=0>\r" +
    "\n" +
    "                    </bscheckbox> --> </div> <div class=\"col-md-6\"> <input skip-enable ng-disabled=\"mData.IsConfirmVehicleCheckOk == 1 || mData.IsConfirmVehicleCheckOk == undefined\" type=\"text\" class=\"form-control\" placeholder=\"Alasan\" name=\"ConfirmVehicleCheckReason\" ng-model=\"mData.ConfirmVehicleCheckReason\"> </div> </div> </div> </fieldset> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Konfirmasi Rencana Follow Up </div> </uib-accordion-heading> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Follow Up</bsreqlabel> <bsdatepicker name=\"FollowUpDate\" date-options=\"DateOptions\" ng-model=\"mData.FollowUpDate\" required> </bsdatepicker> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Telepon</bsreqlabel> <input type=\"text\" class=\"form-control\" placeholder=\"Nomor Telepon\" name=\"PhoneNumber\" ng-model=\"mData.PhoneNumber\" required> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Telepon</bsreqlabel> <!-- <bsselect name=\"phoneType\" ng-model=\"mData.phoneType\" data=\"phoneType\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Kategori\" required ng-required=\"true\">\r" +
    "\n" +
    "                </bsselect> --> <ui-select name=\"phoneType\" id=\"phoneType\" ng-model=\"mData.phoneType\" search-enabled=\"false\" skip-enable theme=\"select2\" required> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kategori\"> {{$select.selected.Name}} </ui-select-match> <ui-select-choices repeat=\"item.Id as item in phoneType\"> <span ng-bind-html=\"item.Name\"></span> </ui-select-choices> </ui-select> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Waktu Follow Up</bsreqlabel> <!-- <bsselect name=\"StatusFUTime\" ng-model=\"mData.StatusFUTime\" data=\"StatusFUTime\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Kategori\" required ng-required=\"true\">\r" +
    "\n" +
    "                </bsselect> --> <ui-select id=\"statusFuTime\" name=\"StatusFUTime\" ng-model=\"mData.StatusFUTime\" search-enabled=\"false\" skip-enable theme=\"select2\" required> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kategori\"> {{$select.selected.Name}} </ui-select-match> <ui-select-choices repeat=\"item.Id as item in StatusFUTime\"> <span ng-bind-html=\"item.Name\"></span> </ui-select-choices> </ui-select> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 18px; font-weight:bold\"> Cetak Toyota ID </div> </uib-accordion-heading> <div class=\"panel panel-default\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div id=\"printSectionId\"> <div style=\"position: relative;background-image: url('../images/logo_only.jpg');height: 250px;width: 500px;margin-left:50%;-webkit-print-color-adjust:exact\"> <div style=\"position: absolute;bottom:1px;text-align:left;margin-left:-30%\"> <a style=\"color: red;margin-left: 5% ;font-size: 30pt;margin-left:-40%\">ToyotaID<br></a> <a style=\"color: red;text-align:center;font-size: 50pt\">{{tmptoyotaid}}</a><br> <a style=\"color: black;margin-bottom: 5%; text-align:center;font-size: 35pt\">{{tmpCustomerName}}</a> </div> </div> </div> </div> <div class=\"col-md-12\"> <button type=\"button\" style=\"width: 300px; margin-right: 4px\" name=\"CetakToyota\" ng-click=\"CetakToyotaID(mData)\" class=\"rbtn btn; pull-right\"> Cetak Toyota ID </button> </div> </div> </div> </div> </div></uib-accordion> </div> <div class=\"row\"> <div class=\"btn-group pull-right\"> <button ng-disabled=\"serviceExplanationWO.$invalid\" type=\"button\" name=\"simpanExplanation\" class=\"rbtn btn\" ng-click=\"saveExplanation(mData)\"> Simpan </button> <button ng-disabled=\"serviceExplanationWO.$invalid\" type=\"button\" name=\"simpanExplanation\" class=\"rbtn btn\" ng-click=\"saveExplanationWithPrint(mData)\"> Simpan & Print </button> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo_bp/wobp.html',
    "<div ng-init=\"disableWO()\"> <script type=\"text/javascript\"></script> <style type=\"text/css\">@media (max-width: 880px) and (min-width: 768px) {\r" +
    "\n" +
    "            ul.nav-tabs {\r" +
    "\n" +
    "                overflow-x: scroll !important;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        th.active {\r" +
    "\n" +
    "            background-color: red;\r" +
    "\n" +
    "            color: white;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .uib-tab a {\r" +
    "\n" +
    "            border: 1px solid #000;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .uib-tab.active a {\r" +
    "\n" +
    "            border: 1px solid grey;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .radioCustomer input[type=radio] {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .radioCustomer input[type=radio]+label {\r" +
    "\n" +
    "            background-color: grey;\r" +
    "\n" +
    "            color: #fff;\r" +
    "\n" +
    "            padding: 10px;\r" +
    "\n" +
    "            -webkit-border-radius: 2px;\r" +
    "\n" +
    "            display: inline-block;\r" +
    "\n" +
    "            font-weight: 400;\r" +
    "\n" +
    "            text-align: center;\r" +
    "\n" +
    "            vertical-align: middle;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "            width: 100px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .radioCustomer input[type=radio]:checked+label {\r" +
    "\n" +
    "            background-color: #d53337;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .border-medium {\r" +
    "\n" +
    "            border-width: 2px;\r" +
    "\n" +
    "            /* border-style: solid; */\r" +
    "\n" +
    "            border-radius: 5px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .border-thin {\r" +
    "\n" +
    "            border-width: 1px;\r" +
    "\n" +
    "            border-style: solid;\r" +
    "\n" +
    "            border-radius: 3px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .select-area-field-label {\r" +
    "\n" +
    "            font-size: 11px;\r" +
    "\n" +
    "            background-color: floralwhite;\r" +
    "\n" +
    "            padding: 2px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            top: -20px;\r" +
    "\n" +
    "            left: -40px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .select-areas-overlay {\r" +
    "\n" +
    "            /*background-color: #ccc;*/\r" +
    "\n" +
    "            background-color: #fff;\r" +
    "\n" +
    "            overflow: hidden;\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        /*test*/\r" +
    "\n" +
    "        .blurred {\r" +
    "\n" +
    "            /*filter: url(\"/filters.svg#blur3px\");\r" +
    "\n" +
    "            -webkit-filter: blur(3px);\r" +
    "\n" +
    "            -moz-filter: blur(3px);\r" +
    "\n" +
    "            -o-filter: blur(3px);\r" +
    "\n" +
    "            filter: blur(3px);*/\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .select-areas-outline {\r" +
    "\n" +
    "            /*background: url('../images/outline.gif');*/\r" +
    "\n" +
    "            overflow: hidden;\r" +
    "\n" +
    "            padding: 2px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .choosen-area {\r" +
    "\n" +
    "            /*background-color: red;*/\r" +
    "\n" +
    "            border: 2px solid red;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .select-areas-resize-handler {\r" +
    "\n" +
    "            border: 2px #fff solid;\r" +
    "\n" +
    "            height: 10px;\r" +
    "\n" +
    "            width: 10px;\r" +
    "\n" +
    "            overflow: hidden;\r" +
    "\n" +
    "            background-color: #000;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .select-areas-delete-area {\r" +
    "\n" +
    "            background: url('../images/bt-delete.png');\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "            height: 20px;\r" +
    "\n" +
    "            width: 20px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .delete-area {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "            padding: 1px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        /* .containeramk {\r" +
    "\n" +
    "            border: 1px solid red;\r" +
    "\n" +
    "            padding: 10px 10px 40px 10px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            margin: 0 auto 0 auto;\r" +
    "\n" +
    "            height: 100%;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .signature {\r" +
    "\n" +
    "            border: 1px solid orange;\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .signature canvas {\r" +
    "\n" +
    "            border: 1px solid #999;\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .buttons {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            bottom: 10px;\r" +
    "\n" +
    "            left: 10px;\r" +
    "\n" +
    "            visibility: hidden;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .buttonsamk {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            bottom: 2px;\r" +
    "\n" +
    "            left: 10px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .containeramk .signature_container {\r" +
    "\n" +
    "            visibility: hidden;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .result {\r" +
    "\n" +
    "            border: 1px solid blue;\r" +
    "\n" +
    "            margin: 30px auto 0 auto;\r" +
    "\n" +
    "            height: 220px;\r" +
    "\n" +
    "            width: 568px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "         */\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .containeramk {\r" +
    "\n" +
    "            /* border: 1px solid red;\r" +
    "\n" +
    "            padding: 10px 10px 40px 10px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            margin: 0 auto 0 auto;\r" +
    "\n" +
    "            height: 100%;*/\r" +
    "\n" +
    "            border: 1px solid #8a8a8a;\r" +
    "\n" +
    "            /*padding: 20px 10px 10px 10px;*/\r" +
    "\n" +
    "            padding: 0px 0px 0px 0px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            margin: 0 auto 0 auto;\r" +
    "\n" +
    "            /*height: 100%;*/\r" +
    "\n" +
    "            background-color: #f5f5f5\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .containeramk .signature {\r" +
    "\n" +
    "            /*border: 1px solid orange;*/\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .containeramk .signature canvas {\r" +
    "\n" +
    "            /* border: 1px solid #999;\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;*/\r" +
    "\n" +
    "            /*border: 1px solid #999;*/\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "            background-color: #fff;\r" +
    "\n" +
    "            box-shadow: inset 0px 0px 12px 0px #c5c5c5;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .containeramk .buttons {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            bottom: 10px;\r" +
    "\n" +
    "            left: 10px;\r" +
    "\n" +
    "            visibility: hidden;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .containeramk .buttonsamk {\r" +
    "\n" +
    "            margin-top: 10px;\r" +
    "\n" +
    "            /*        position: absolute;\r" +
    "\n" +
    "            bottom: 2px;\r" +
    "\n" +
    "            left: 10px;*/\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .containeramk .signature_container {\r" +
    "\n" +
    "            visibility: hidden;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .result {\r" +
    "\n" +
    "            border: 1px solid blue;\r" +
    "\n" +
    "            margin: 30px auto 0 auto;\r" +
    "\n" +
    "            height: 220px;\r" +
    "\n" +
    "            width: 568px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        /*===========================*/\r" +
    "\n" +
    "        /*====================*/\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .custom-slider.rzslider .rz-bar {\r" +
    "\n" +
    "            background: #ffe4d1;\r" +
    "\n" +
    "            height: 2px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .custom-slider.rzslider .rz-selection {\r" +
    "\n" +
    "            background: orange;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .custom-slider.rzslider .rz-pointer {\r" +
    "\n" +
    "            width: 8px;\r" +
    "\n" +
    "            height: 16px;\r" +
    "\n" +
    "            top: auto;\r" +
    "\n" +
    "            /* to remove the default positioning */\r" +
    "\n" +
    "            bottom: 0;\r" +
    "\n" +
    "            /*background-color: #333;*/\r" +
    "\n" +
    "            background-color: red;\r" +
    "\n" +
    "            border-top-left-radius: 3px;\r" +
    "\n" +
    "            border-top-right-radius: 3px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .custom-slider.rzslider .rz-pointer:after {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .custom-slider.rzslider .rz-bubble {\r" +
    "\n" +
    "            bottom: 14px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .custom-slider.rzslider .rz-limit {\r" +
    "\n" +
    "            font-weight: bold;\r" +
    "\n" +
    "            color: orange;\r" +
    "\n" +
    "            font-size: 20px;\r" +
    "\n" +
    "            bottom: 0;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .custom-slider.rzslider .rz-tick {\r" +
    "\n" +
    "            width: 1px;\r" +
    "\n" +
    "            height: 10px;\r" +
    "\n" +
    "            margin-left: 4px;\r" +
    "\n" +
    "            border-radius: 0;\r" +
    "\n" +
    "            background: #ffe4d1;\r" +
    "\n" +
    "            top: -1px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .custom-slider.rzslider .rz-tick.rz-selected {\r" +
    "\n" +
    "            background: orange;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .scaleWarp {\r" +
    "\n" +
    "            margin-bottom: -40px;\r" +
    "\n" +
    "            /*padding:16px;*/\r" +
    "\n" +
    "            padding-bottom: 0px;\r" +
    "\n" +
    "            padding-left: 1px;\r" +
    "\n" +
    "            letter-spacing: -10px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            /*float:left;*/\r" +
    "\n" +
    "            /*width:100%;*/\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        #slide {\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        /*.scaleWarp span+span{*/\r" +
    "\n" +
    "        /*margin-left:23%;*/\r" +
    "\n" +
    "        /*vertical-align:bottom;*/\r" +
    "\n" +
    "        /*bottom:0;*/\r" +
    "\n" +
    "        /*}*/\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .scaleWarp garis {\r" +
    "\n" +
    "            margin-left: 24.75%;\r" +
    "\n" +
    "            vertical-align: bottom;\r" +
    "\n" +
    "            bottom: 0;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        /* added by sss on 2017-10-11*/\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .ui-grid-cell {\r" +
    "\n" +
    "            overflow: visible;\r" +
    "\n" +
    "            z-index: 99999;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        /**/\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .ui.modal {\r" +
    "\n" +
    "            z-index: 10000 !important;\r" +
    "\n" +
    "            position: relative !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .panel-default>.panel-heading {\r" +
    "\n" +
    "            color: #fff !important;\r" +
    "\n" +
    "            background-color: #d53337 !important;\r" +
    "\n" +
    "            border-color: #d53337 !important;\r" +
    "\n" +
    "            font-size: 18px !important;\r" +
    "\n" +
    "            font-weight: bold;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        @media (max-width: 769px) {\r" +
    "\n" +
    "            .nav-tabs.nav-justified {\r" +
    "\n" +
    "                width: 100% !important;\r" +
    "\n" +
    "                border-bottom: 0;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .headerJobList:hover a {\r" +
    "\n" +
    "            /* font-weight: bold;\r" +
    "\n" +
    "            font-size: 20px !important; */\r" +
    "\n" +
    "            background-color: #33AA8F !important;\r" +
    "\n" +
    "            color: rgb(221, 220, 220) !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .headerJobList.active a {\r" +
    "\n" +
    "            background-color: #33AA8F !important;\r" +
    "\n" +
    "            /* font-weight: bold !important;\r" +
    "\n" +
    "            font-size: 22px !important; */\r" +
    "\n" +
    "            color: #fff !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .headerJobList a {\r" +
    "\n" +
    "            background-color: rgb(43, 138, 116) !important;\r" +
    "\n" +
    "            font-weight: bold !important;\r" +
    "\n" +
    "            font-size: 23px !important;\r" +
    "\n" +
    "            color: rgb(216, 215, 215) !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .headerEstimation:hover a {\r" +
    "\n" +
    "            /* font-weight: bold;\r" +
    "\n" +
    "            font-size: 20px !important; */\r" +
    "\n" +
    "            background-color: #33AA8F !important;\r" +
    "\n" +
    "            color: rgb(221, 220, 220) !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .headerEstimation.active a {\r" +
    "\n" +
    "            background-color: #33AA8F !important;\r" +
    "\n" +
    "            /* font-weight: bold !important;\r" +
    "\n" +
    "            font-size: 22px !important; */\r" +
    "\n" +
    "            color: #fff !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        .headerEstimation a {\r" +
    "\n" +
    "            background-color: rgb(43, 138, 116) !important;\r" +
    "\n" +
    "            font-weight: bold !important;\r" +
    "\n" +
    "            font-size: 12px !important;\r" +
    "\n" +
    "            color: rgb(216, 215, 215) !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        #EstimasiType input[type=\"search\"],\r" +
    "\n" +
    "        .ui-select-focusser {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        /* @media (max-width: 1330px) {\r" +
    "\n" +
    "            .uib-time input{\r" +
    "\n" +
    "                width:auto !important;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        } */\r" +
    "\n" +
    "        .uib-time input{\r" +
    "\n" +
    "            width:100% !important;\r" +
    "\n" +
    "        }</style> <uib-accordion close-others=\"oneAtATime\"> <form> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(1)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Short Brief </div> </uib-accordion-heading> <div smart-include=\"app/services/reception/wo_bp/includeForm/wobp/wo01.html\" ng-if=\"includehtml1\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(2)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Informasi Umum </div> </uib-accordion-heading> <div smart-include=\"app/services/reception/wo_bp/includeForm/customerIndex.html\" ng-if=\"includehtml2\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(9, mDataCrm.Vehicle.VIN)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Riwayat Servis </div> </uib-accordion-heading> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-9\"> <div class=\"col-md-12\"> <label class=\"control-label\">Riwayat Servis</label> </div> <div class=\"col-md-3\" style=\"padding-left: 0px\"> <bsdatepicker name=\"Tanggal1\" date-options=\"DateOptionsHistory\" ng-model=\"MRS.startDate\"> </bsdatepicker> </div> <div class=\"col-md-1\" style=\"margin-top: 5px;padding-left: 0px\"> <label>S/D</label> </div> <div class=\"col-md-3\" style=\"padding-left: 0px\"> <bsdatepicker name=\"Tanggal2\" date-options=\"DateOptionsHistory\" ng-model=\"MRS.endDate\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <bsreqlabel class=\"control-label\">Kategori : </bsreqlabel> <!-- ng-change=\"change()\" --> <select ng-model=\"MRS.category\" class=\"form-control\" convert-to-number> <option value=\"2\">ALL</option> <option value=\"1\">GR</option> <option value=\"0\">BP</option> </select> </div> <div class=\"col-md-12\" style=\"margin-top: 10px;text-align: right\"> <button class=\"rbtn btn\" ng-click=\"change(MRS.startDate,MRS.endDate,MRS.category,mDataCrm.Vehicle.VIN)\">Cari</button> </div> </div> <uib-accordion> <div ng-repeat=\"item in mDataVehicleHistory\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <table style=\"width:100%\"> <tr> <td>{{item.VehicleJobService[0].JobDescription}}</td> <th class=\"text-right\">{{item.ServiceNumber}} | {{item.ServiceDate |date:'dd-MMM-yyyy'}}</th> <!-- <th colspan=\"2\">{{item.ServiceNumber}}</th> --> </tr> <tr> <td>Lokasi services : {{item.ServiceLocation}}</td> <td class=\"text-right\">SA : {{item.ServiceAdvisor}}</td> </tr> <tr> <td>KM : {{item.Km}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Teknisi : {{item.VehicleJobService[0].EmployeeName}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Group : {{item.namaGroup}}</td> <!-- <td class=\"text-right\">Tanggal Servis : {{item.ServiceDate |date:dd/mm/yyyy}}</td> --> </tr> <tr> <td>Job Suggest : {{item.JobSuggest}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Kategori : GR</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Kategori : BP</td> </tr> </table> </uib-accordion-heading> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"itemJob in item.VehicleJobService\" is-open=\"status.open\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> {{itemJob.JobDescription}} <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> </uib-accordion-heading> <table class=\"table table-striped\"> <tr> <th>No. Material</th> <th>Material</th> <!-- <th>No. Material</th> --> <th>Qty</th> <th>Satuan</th> </tr> <tr ng-repeat=\"tr in itemJob.VehicleServiceHistoryDetail\"> <td>{{tr.MaterialCode}}</td> <td>{{tr.Material}}</td> <!-- <td>{{tr.PartsCode}}</td> --> <td>{{tr.Quantity | number:2}}</td> <td>{{tr.Satuan}}</td> </tr> </table> </div> </uib-accordion> </div> </uib-accordion> <!-- <vehicle-history vin=\"mDataCrm.Vehicle.VIN\" value=\"dataRiwayatService\">\r" +
    "\n" +
    "                    </vehicle-history> --> <div class=\"row\"> <div class=\"col-md-12\"> <bscheckbox class=\"pull-right\" ng-model=\"isPrintRiwSer.value\" ng-change=\"triggerPrintRS()\" label=\"Print Riwayat Service\" ng-show=\"mDataVehicleHistory.length > 0\" ng-true-value=\"'true'\" ng-false-value=\"'false'\" true-value=\"'true'\" false-value=\"'false'\"> </bscheckbox> </div> </div> </div> <!--</div>--> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(4)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Estimasi </div> </uib-accordion-heading> <div smart-include=\"app/services/reception/wo_bp/includeForm/estimation.html\" ng-if=\"includehtml4\"></div> </div> <!--</div>--> <div uib-accordion-group class=\"panel-default\" ng-if=\"isNextWO\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(5);refreshSlider();getTypeVehicleFromEst(1, mDataCrm.Vehicle.VIN)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> WAC </div> </uib-accordion-heading> <div smart-include=\"app/services/reception/wo_bp/includeForm/wac_bp.html\" ng-if=\"includehtml5\"></div> </div> <!--</div>--> <div uib-accordion-group class=\"panel-default\" ng-if=\"isNextWO && isCenter\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(6)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Job List </div> </uib-accordion-heading> <div id=\"JobWo\"> <div smart-include=\"app/services/reception/wo_bp/includeForm/jobList.html\" ng-if=\"includehtml6\"></div> </div> </div> <!-- ng-if=\"isNextWO && isCenter\" --> <div uib-accordion-group class=\"panel-default\" ng-if=\"isNextWO && isCenter\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(7)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> OPL </div> </uib-accordion-heading> <div id=\"JobOPL\"> <!-- <div ng-if=\"includehtml7\" smart-include=\"app/services/reception/woHistoryBP/OPL.html\"></div> --> <div smart-include=\"app/services/reception/wo_bp/includeForm/OPLNew.html\" ng-if=\"includehtml7\"></div> </div> </div> <!--</div>--> <div uib-accordion-group class=\"panel-default\" ng-if=\"isNextWO && isCenter\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(7);\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Summary </div> </uib-accordion-heading> <!--content--> <uib-accordion close-others=\"oneAtATime\"> <div id=\"SummaryWO\" ng-if=\"isNextWO\"> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Info </uib-accordion-heading> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Polisi</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.LicensePlate\" ng-readonly=\"true\" placeholder=\"No Polisi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. WO</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataVehicle.No_Polisi\" ng-readonly=\"true\" placeholder=\"No WO\"> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Job Summary </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"TaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridSumPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" list-api=\"listApi\" confirm-save=\"true\" list-title=\"Job List\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\" list-height=\"120\" button-settings=\"listView\"> <bslist-template> <div class=\"col-md-1\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.NumberParent }}</strong></p> </div> <div class=\"col-md-6\" style=\"text-align:left\"> <div class=\"col-md-12\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> </div> <div class=\"col-md-12\"> <p>Harga : {{lmItem.Fare | currency:\"Rp. \":0}}</p> </div> </div> <div class=\"col-md-5\"> <p>Satuan : {{lmItem.UnitBy}}</p> <p>Pembayaran : {{lmItem.PaidBy}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-1\"> <p><strong>{{ ldItem.NumberChild }}</strong></p> </div> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-2\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo_bp/includeForm/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo_bp/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Price Summary </uib-accordion-heading> <div class=\"col-md-12\"> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkSummary | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkDiscountedSummary | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ (totalWorkSummary - totalWorkDiscountedSummary) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialSummary | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialDiscountedSummary | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ (totalMaterialSummary - totalMaterialDiscountedSummary) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi OPL</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOplDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl - sumWorkOplDiscounted | currency:\"\":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalPPN + (((sumWorkOpl - sumWorkOplDiscounted))*(10/100)) | currency:\"\":0}}</td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalEstimasi + (((sumWorkOpl - sumWorkOplDiscounted))*(10/100)) | currency:\"\":0}}</td> </tr> <tr> <td>Total DP Parts Material/Bahan</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDpSummary | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(0) | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDpSummary | currency:\"\":0}}</td> </tr> <tr ng-show=\"mData.isSpk == 1\"> <td>Own Risk</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{convertORAmount | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(0) | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{convertORAmount | currency:\"\":0}}</td> </tr> </tbody> </table> </div> </div> <div uib-accordion-group class=\"panel-default\" is-open=\"true\"> <uib-accordion-heading> Time Summary </uib-accordion-heading> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Body</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrWorkTime[0]\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Putty</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrWorkTime[1]\"> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Surfacer</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrWorkTime[2]\"> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Painting</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrWorkTime[3]\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" style=\"margin-top:10px\"> <div class=\"col-md-6\"> <bsreqlabel>Polishing</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrWorkTime[4]\"> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Re-Assembly</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrWorkTime[5]\"> </div> </div> </div> <div class=\"col-md-6\" style=\"margin-top:10px\"> <div class=\"form-group\"> <div class=\"col-md-6\"> <bsreqlabel>Final Inspection</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" ng-readonly=\"true\" ng-model=\"arrWorkTime[6]\"> </div> </div> </div> </div> <div class=\"row\"> <!-- <div class=\"col-md-12\" style=\"margin-top:10px; background-color:lightgreen;\">\r" +
    "\n" +
    "                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                            <div class=\"col-md-2\" style=\"margin-top:10px; width: 24.38%\">\r" +
    "\n" +
    "                                                <bsreqlabel style=\"color:white;\">Total Pengerjaan</bsreqlabel>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                            <div class=\"col-md-3\" style=\"width : 16.38%\">\r" +
    "\n" +
    "                                                <input type=\"text\" ng-model=\"estHoursWork\" class=\"form-control\" ng-readonly='true' />\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                            <div class=\"col-md-3\" style=\"margin-top:10px; width : 16.38%\">\r" +
    "\n" +
    "                                                <label>Jam</label>&nbsp;\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                            <div class=\"col-md-3\" style=\"width : 16.38%\">\r" +
    "\n" +
    "                                                <input type=\"text\" ng-model=\"estDaysWork\" class=\"form-control\" ng-readonly='true' />\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                            <div class=\"col-md-3\" style=\"margin-top:10px; width : 16.38%\">\r" +
    "\n" +
    "                                                <label>Hari</label>&nbsp;\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div> --> <div class=\"col-xs-12\" style=\"margin-top:10px; background-color:lightgreen\"> <div class=\"form-group\"> <div class=\"col-xs-4\" style=\"margin-top:10px; width: 24.38%\"> <bsreqlabel style=\"color:white; font-weight: bold; font-size: 14px\">Total Pengerjaan</bsreqlabel> </div> <div class=\"col-xs-3\"> <input type=\"text\" ng-model=\"estHoursWork\" size=\"15\" class=\"form-control\" ng-readonly=\"true\"> </div> <div style=\"margin-top:10px\" class=\"col-xs-1\"> <label>Jam</label> <img src=\"images/rightarrow.png\"> </div> <div class=\"col-xs-3\"> <input type=\"text\" ng-model=\"estDaysWork\" size=\"15\" class=\"form-control\" ng-readonly=\"true\"> </div> <div style=\"margin-top:10px\" class=\"col-xs-1\"> <label>Hari</label> </div> <!-- <div class=\"col-xs-6 col-md-2\">\r" +
    "\n" +
    "                                                <input type=\"text\" ng-model=\"estHours\" size='6' ng-readonly=\"true\">&nbsp;<label>Jam</label>&nbsp;\r" +
    "\n" +
    "                                                <img src=\"images/rightarrow.png\">&nbsp;\r" +
    "\n" +
    "                                                <input type=\"text\" ng-model=\"estDays\" size='6' ng-readonly=\"true\">&nbsp;<label><strong>Hari</strong></label>\r" +
    "\n" +
    "                                            </div> --> </div> </div> </div> </div> </div> </div> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Payment</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" ng-disabled=\"true\" disabled skip-enable id=\"wait1\" ng-click=\"cekPayment(isCash)\" ng-model=\"mData.isCash\" name=\"radio2\" value=\"1\" checked> <label ng-disabled=\"true\" disabled skip-enable for=\"wait1\">Cash</label> <input type=\"radio\" ng-disabled=\"true\" disabled skip-enable id=\"wait2\" ng-click=\"cekPayment(isCash)\" ng-model=\"mData.isCash\" name=\"radio2\" value=\"0\"> <label ng-disabled=\"true\" disabled skip-enable for=\"wait2\">Insurance</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Part Bekas</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radio1Create\" skip-disable ng-model=\"isParts\" name=\"radio1Create\" value=\"1\" checked> <label for=\"radio1Create\">Kembalikan</label> <input type=\"radio\" id=\"radio2Create\" skip-disable ng-model=\"isParts\" name=\"radio1Create\" value=\"0\"> <label for=\"radio2Create\">Tinggal</label> </div> <!-- {{isParts}} --> </div> <div class=\"col-md-6\"> <div class=\"form-group\" style=\"padding-top:20px\"> <label>Perbaikan Parts</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radioPerbaikan1Create\" skip-disable ng-model=\"isPerbaikan\" name=\"radioPerbaikan1Create\" value=\"1\" checked> <label for=\"radioPerbaikan1Create\">Dengan Kendaraan</label> <input type=\"radio\" id=\"radioPerbaikan2Create\" skip-disable ng-model=\"isPerbaikan\" name=\"radioPerbaikan1Create\" value=\"0\"> <label for=\"radioPerbaikan2Create\">Tidak Dengan Kendaraan</label> </div> <!-- {{isParts}} --> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Car Wash</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"washing1Create\" skip-disable ng-model=\"isWashing\" name=\"radio3Create\" value=\"1\" checked> <label for=\"washing1Create\">Wash</label> <input type=\"radio\" id=\"washing2Create\" skip-disable ng-model=\"isWashing\" name=\"radio3Create\" value=\"0\"> <label for=\"washing2Create\">No Wash</label> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <label>Estimation Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.EstimateDeliveryTime\" ng-disabled=\"true\" skip-enable> </bsdatepicker> <!--<bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.EstimateDeliveryTime\">\r" +
    "\n" +
    "                                                </bsdatepicker>--> </div> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px;margin-top:10px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <label>Adjusment Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <!--<bsdatepicker name=\"firstDate\" date-options=\"DateOptions\" ng-model=\"mData.EstimateDeliveryTime\">\r" +
    "\n" +
    "                                                </bsdatepicker>--> <bsdatepicker name=\"adjDate\" min-date=\"minDate\" date-options=\"DateOptions\" ng-model=\"mData.AdjusmentDate\" ng-disabled=\"adjustDate\" ng-change=\"adjustTime(mData.AdjusmentDate)\" skip-enable> </bsdatepicker> </div> <div class=\"form-group\" style=\"width:200px\"> <div uib-timepicker ng-model=\"mData.AdjusmentTime\" min=\"minTime\" ng-change=\"changedTime(mData.AdjusmentTime)\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" ng-disabled=\"adjustDate\"></div> </div> </div> <div class=\"col-md-3 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bscheckbox ng-model=\"mData.customerRequest\" label=\"Customer Request\" ng-click=\"adjRequest(mData.customerRequest)\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <!-- <div class=\"col-md-2 form-inline\">\r" +
    "\n" +
    "                                               \r" +
    "\n" +
    "                                            </div> --> </div> </div> <div class=\"row\" style=\"margin-bottom:10px;margin-top:10px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <label>Kategori Perbaikan</label> </div> <div class=\"col-md-6\"> <select class=\"form-control\" name=\"CatPerbaikan\" ng-model=\"mData.JobCatgForBPid\" data=\"CatPerbaikan\" placeholder=\"Pilih Kategori Perbaikan\" ng-disabled=\"true\" on-select=\"selectedCatPerbaikan(selectInsuranceed)\" icon=\"fa fa-list\"> <option ng-repeat=\"CatPerbaikan in CatPerbaikan\" value=\"{{CatPerbaikan.MasterId}}\">{{CatPerbaikan.Name}}</option> </select> <!--<bsselect name=\"CatPerbaikanView\" ng-model=\"mData.JobCatgForBP\" data=\"CatPerbaikan\" item-text=\"Name\" ng-disabled=\"true\" skip-enable=\"true\"\r" +
    "\n" +
    "                                            item-value=\"MasterId\" placeholder=\"Pilih Kategori Perbaikan\" on-select=\"selectedCatPerbaikan(selected)\"\r" +
    "\n" +
    "                                            icon=\"fa fa-list\">\r" +
    "\n" +
    "                                        </bsselect>--> </div> <!-- <div class=\"col-md-2 form-inline\">\r" +
    "\n" +
    "                                               \r" +
    "\n" +
    "                                            </div> --> </div> </div> <div class=\"row\" style=\"margin-bottom:10px;margin-top:10px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bsreqlabel>Kode Transaksi</bsreqlabel> </div> <div class=\"col-md-9 form-inline\"> <!-- <bsselect ng-disabled=\"false\" data=\"codeTransaction\" name=\"codeTrans\" item-value=\"Code\" item-text=\"Description\" ng-model=\"mData.CodeTransaction\" placeholder=\"Kode Transaksi\">\r" +
    "\n" +
    "                                        </bsselect> --> <select class=\"form-control\" ng-disabled=\"false\" data=\"codeTransaction\" ng-model=\"mData.CodeTransaction\"> <option ng-repeat=\"item in codeTransaction\" value=\"{{item.Code}}\">{{item.Description}}</option> </select> </div> <!-- <div class=\"col-md-2 form-inline\">\r" +
    "\n" +
    "                                               \r" +
    "\n" +
    "                                            </div> --> </div> </div> </div> </div> <div class=\"row\" style=\"margin-top:20px; position:relative; z-index:9999\"> <div class=\"btn-group pull-right\"> <div class=\"col-md-12\" ng-show=\"isBPSatelit\"> <div class=\"btn-group pull-right\"> <!--<button type=\"button\" name=\"saveWOBP\" class=\"rbtn btn\" ng-click=\"saveNewWOBP(isParts, isWashing)\">\r" +
    "\n" +
    "                                    Simpan\r" +
    "\n" +
    "                                </button>--> <!--<button type=\"button\" name=\"printWOBP\" class=\"rbtn btn\">\r" +
    "\n" +
    "                                        Print Preview\r" +
    "\n" +
    "                                    </button> --> <button type=\"button\" name=\"releaseWOBP\" ng-click=\"releaseWOBP(isParts, isWashing, isPerbaikan)\" class=\"rbtn btn\" ng-disabled=\"disableRelease || disableButtonReleaseWO\" skip-enable> Release WO BSTK </button> <button type=\"button\" name=\"CloseWO\" ng-click=\"cancelWoBPBSTK(mData)\" class=\"rbtn btn\"> Batal WO </button> </div> </div> <div class=\"col-md-12\" ng-show=\"isBP\"> <div class=\"btn-group pull-right\"> <button type=\"button\" name=\"saveWOBP\" class=\"rbtn btn no-animate\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" ng-click=\"saveNewWOBP(isParts, isWashing, isPerbaikan)\"> Simpan </button> <!--<button type=\"button\" name=\"printWOBP\" class=\"rbtn btn\">\r" +
    "\n" +
    "                                        Print Preview\r" +
    "\n" +
    "                                    </button> --> <!--  ng-disabled=\"disableRelease\" --> <button ng-disabled=\"disableButtonReleaseWO\" type=\"button\" name=\"releaseWOBP\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" ng-click=\"onValidateRelease(isParts, isWashing, isPerbaikan)\" class=\"rbtn btn no-animate\" skip-enable> Release WO </button> <button type=\"button\" name=\"CloseWO\" ng-click=\"cancelWoBP(mData)\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" class=\"rbtn btn no-animate\"> Batal WO </button> </div> </div> </div> </div></uib-accordion> </div> <!-- </div> --> </form> </uib-accordion> </div>"
  );


  $templateCache.put('app/services/reception/woHistoryGR/modalFormMasterOpl.html',
    "<div class=\"row\"> <div class=\"col-md-12\" ng-if=\"lmModelOpl.Status <= 1 || lmModelOpl.Status == undefined\"> <div class=\"form-group\"> <bsreqlabel>Nama OPL</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" ng-disabled=\"mData.Status > 3 && !isNew\" name=\"oplName\" ng-model=\"lmModelOpl.OPLName\" get-data=\"getOpl\" item-text=\"OPLWorkName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkOPL\" on-noresult=\"onNoResultOpl\" on-gotresult=\"onGotResult\" template-url=\"customTemplateOpl.html\" icon=\"fa-id-card-o\" required> </bstypeahead> <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModelOpl.OplName\"> --> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <!-- <bstypeahead placeholder=\"Nama Vendor\" name=\"vendorName\"\r" +
    "\n" +
    "                ng-model=\"lmModelOpl.VendorName\"\r" +
    "\n" +
    "                get-data=\"getOpl\"\r" +
    "\n" +
    "                model-key=\"Opl\"\r" +
    "\n" +
    "                ng-minlength=\"2\" ng-maxlength=\"10\" disallow-space\r" +
    "\n" +
    "                loading=\"loadingUsers\"\r" +
    "\n" +
    "                noresult=\"noResults\"\r" +
    "\n" +
    "                selected=\"selected\"\r" +
    "\n" +
    "                on-select=\"onSelectUser\"\r" +
    "\n" +
    "                on-noresult=\"onNoResult\"\r" +
    "\n" +
    "                on-gotresult=\"onGotResult\",\r" +
    "\n" +
    "                ta-minlength=\"1\"\r" +
    "\n" +
    "                icon=\"fa-id-card-o\"\r" +
    "\n" +
    "            >\r" +
    "\n" +
    "            </bstypeahead> --> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModelOpl.VendorName\" required> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <bsselect name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"OPLPaymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" required> </bsselect> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Estimasi Selesai</bsreqlabel> <bsdatepicker name=\"RequiredDate\" min-date=\"dateOptionsOPL.minDate\" date-options=\"dateOptionsOPL\" ng-model=\"lmModelOpl.TargetFinishDate\" ng-change=\"DateChange(dt)\" required> </bsdatepicker> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" required name=\"ReceiveBy\" ng-model=\"lmModelOpl.ReceiveBy\">\r" +
    "\n" +
    "            <bserrmsg field=\"ReceiveBy\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input min=\"0\" type=\"number\" ng-change=\"QtyOPL(lmModelOpl.QtyPekerjaan,lmModelOpl)\" class=\"form-control\" ng-disabled=\"false\" required name=\"qty\" ng-model=\"lmModelOpl.QtyPekerjaan\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Beli</bsreqlabel> <input min=\"0\" type=\"text\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.OPLPurchasePrice\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Jual</bsreqlabel> <input min=\"0\" type=\"text\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\"> <label>Notes</label> <textarea class=\"form-control\" name=\"notes\" ng-model=\"lmModelOpl.Notes\" style=\"height: 100px\"></textarea> </div> <div class=\"form-group\" show-errors> <label>Discount (%)</label> <input type=\"number\" min=\"0\" max=\"100\" class=\"form-control\" name=\"discount\" ng-model=\"lmModelOpl.Discount\" ng-disabled=\"DiscountAvailable\"> <bserrmsg field=\"discount\"></bserrmsg> </div> <div class=\"form-group\" ng-if=\"lmModel.OPLId !== undefined\"> <label>Status : {{lmModelOpl.xStatus}}</label> </div> </div> <div class=\"col-md-12\" ng-if=\"lmModelOpl.Status > 1\"> <div class=\"form-group\"> <bsreqlabel>Nama OPL</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" ng-disabled=\"true\" name=\"oplName\" ng-model=\"lmModelOpl.OPLName\" get-data=\"getOpl\" item-text=\"OPLWorkName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkOPL\" on-noresult=\"onNoResultOpl\" on-gotresult=\"onGotResult\" template-url=\"customTemplateOpl.html\" icon=\"fa-id-card-o\" required> </bstypeahead> <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModelOpl.OplName\"> --> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModelOpl.VendorName\"> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <bsselect name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"OPLPaymentData\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"true\" placeholder=\"Pilih Pembayaran\" required> </bsselect> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Estimasi Selesai</bsreqlabel> <bsdatepicker name=\"RequiredDate\" ng-disabled=\"true\" min-date=\"dateOptionsOPL.minDate\" date-options=\"dateOptionsOPL\" ng-model=\"lmModelOpl.TargetFinishDate\" ng-change=\"DateChange(dt)\"> </bsdatepicker> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" required name=\"ReceiveBy\" ng-model=\"lmModelOpl.ReceiveBy\">\r" +
    "\n" +
    "            <bserrmsg field=\"ReceiveBy\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input type=\"number\" ng-change=\"QtyOPL(lmModelOpl.QtyPekerjaan,lmModelOpl)\" class=\"form-control\" ng-disabled=\"true\" required name=\"qty\" ng-model=\"lmModelOpl.QtyPekerjaan\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\"> <label>Notes</label> <textarea ng-disabled=\"true\" class=\"form-control\" name=\"notes\" ng-model=\"lmModelOpl.Notes\" style=\"height: 100px\"></textarea> </div> <div class=\"form-group\" show-errors> <label>Discount (%)</label> <input type=\"number\" min=\"0\" max=\"100\" ng-disabled=\"true\" class=\"form-control\" name=\"discount\" ng-model=\"lmModelOpl.Discount\" ng-disabled=\"DiscountAvailable\"> <bserrmsg field=\"discount\"></bserrmsg> </div> <div class=\"form-group\"> <label>Status : {{lmModelOpl.xStatus}}</label> </div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpl.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.Vendor.Name | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.isExternalDesc | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );

}]);
