angular.module('templates_app_estimasiBP', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/services/estimasiBP/estimasiBP.html',
    "<link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    tr:nth-child(even) {\r" +
    "\n" +
    "        background-color: #dddddd;\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    fieldset.scheduler-border {\r" +
    "\n" +
    "        width: auto;\r" +
    "\n" +
    "        border: 1px groove #ffffff !important;\r" +
    "\n" +
    "        padding: 0 1.4em 1.4em 1.4em !important;\r" +
    "\n" +
    "        margin: 0 0 1.5em 0 !important;\r" +
    "\n" +
    "        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;\r" +
    "\n" +
    "        box-shadow: 0px 0px 0px 0px #ffffff;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    fieldset {\r" +
    "\n" +
    "        background-color: #ffffff;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    legend.scheduler-border {\r" +
    "\n" +
    "        width: auto;\r" +
    "\n" +
    "        padding: 0 10px;\r" +
    "\n" +
    "        border-bottom: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .border-medium {\r" +
    "\n" +
    "        border-width: 2px;\r" +
    "\n" +
    "        /* border-style: solid; */\r" +
    "\n" +
    "        border-radius: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /*.ag-tool-panel{\r" +
    "\n" +
    "        width: 0px !important;\r" +
    "\n" +
    "    }*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .border-thin {\r" +
    "\n" +
    "        border-width: 1px;\r" +
    "\n" +
    "        border-style: solid;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-area-field-label {\r" +
    "\n" +
    "        font-size: 11px;\r" +
    "\n" +
    "        background-color: floralwhite;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        top: -20px;\r" +
    "\n" +
    "        left: -40px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-areas-overlay {\r" +
    "\n" +
    "        /*background-color: #ccc;*/\r" +
    "\n" +
    "        background-color: #fff;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-areas-outline {\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .choosen-area {\r" +
    "\n" +
    "        /*background-color: red;*/\r" +
    "\n" +
    "        border: 2px solid red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-areas-resize-handler {\r" +
    "\n" +
    "        border: 2px #fff solid;\r" +
    "\n" +
    "        height: 10px;\r" +
    "\n" +
    "        width: 10px;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        background-color: #000;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-areas-delete-area {\r" +
    "\n" +
    "        background: url('../images/bt-delete.png');\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        height: 20px;\r" +
    "\n" +
    "        width: 20px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .delete-area {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        padding: 1px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .WACImageBorder {\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        border: 1px solid grey;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "    }</style> <!-- <div class=\"row\"> --> <div style=\"margin-right:20px\"> <button ng-click=\"doCustomSave()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px; width:12%\" ng-show=\"estimationMode == 'Create' || estimationMode == 'Edit'\" ng-disabled=\"master.length == 0 ||\r" +
    "\n" +
    "            mEstimationBP.CategoryId          == null || mEstimationBP.CategoryId          == undefined || mEstimationBP.CategoryId          == '' ||\r" +
    "\n" +
    "            mEstimationBP.Km                  == null || mEstimationBP.Km                  == undefined || mEstimationBP.Km                  == '' ||\r" +
    "\n" +
    "            mEstimationBP.isCash              == null || mEstimationBP.isCash              == undefined ||\r" +
    "\n" +
    "            mEstimationBP.ContactPerson       == null || mEstimationBP.ContactPerson       == undefined || mEstimationBP.ContactPerson       == '' ||\r" +
    "\n" +
    "            mEstimationBP.PhoneContactPerson1 == null || mEstimationBP.PhoneContactPerson1 == undefined || mEstimationBP.PhoneContactPerson1 == '' ||\r" +
    "\n" +
    "            mEstimationBP.PoliceNumber        == null || mEstimationBP.PoliceNumber        == undefined || mEstimationBP.PoliceNumber        == '' ||\r" +
    "\n" +
    "            mEstimationBP.ModelCode           == null || mEstimationBP.ModelCode           == undefined || mEstimationBP.ModelCode           == '' ||\r" +
    "\n" +
    "            mEstimationBP.Alamat              == null || mEstimationBP.Alamat              == undefined || mEstimationBP.Alamat              == '' ||\r" +
    "\n" +
    "            mEstimationBP.VIN                 == null || mEstimationBP.VIN                 == undefined || mEstimationBP.VIN                 == '' ||\r" +
    "\n" +
    "            mEstimationBP.ColorId             == null || mEstimationBP.ColorId             == undefined || mEstimationBP.ColorId             == '' ||\r" +
    "\n" +
    "            mEstimationBP.EngineNo            == null || mEstimationBP.EngineNo            == undefined || mEstimationBP.EngineNo            == '' ||\r" +
    "\n" +
    "            mEstimationBP.VehicleTypeId       == null || mEstimationBP.VehicleTypeId       == undefined || mEstimationBP.VehicleTypeId       == '' ||\r" +
    "\n" +
    "            mEstimationBP.KatashikiCode       == null || mEstimationBP.KatashikiCode       == undefined || mEstimationBP.KatashikiCode       == '' ||\r" +
    "\n" +
    "            estimateTime                       > 0    || countZeroTime                              > 0 ||  !isValidEmail || emailLength>50 ||\r" +
    "\n" +
    "            ( estDays                         == 0    && estHours                          == 0 )       ||\r" +
    "\n" +
    "            (mEstimationBP.isCash             == 0    && (mEstimationBP.InsuranceId        == undefined || mEstimationBP.InsuranceId         == '' || mEstimationBP.InsuranceId == null ))\r" +
    "\n" +
    "            \"> <!-- <span ng-if=\"estimationMode == 'Create'\">Simpan & Print</span> --> <span>Simpan & Print</span> <!-- <span ng-if=\"estimationMode == 'Edit'\">Simpan</span> --> </button> <button ng-click=\"doSaveDraft()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px; width:7%\" ng-show=\"(estimationMode == 'Create' || estimationMode == 'Edit') && mEstimationBP.EstimationNo == null \" ng-disabled=\"\r" +
    "\n" +
    "            mEstimationBP.CategoryId          == null || mEstimationBP.CategoryId          == undefined || mEstimationBP.CategoryId          == '' ||\r" +
    "\n" +
    "            mEstimationBP.Km                  == null || mEstimationBP.Km                  == undefined || mEstimationBP.Km                  == '' ||\r" +
    "\n" +
    "            mEstimationBP.isCash              == null || mEstimationBP.isCash              == undefined ||\r" +
    "\n" +
    "            mEstimationBP.ContactPerson       == null || mEstimationBP.ContactPerson       == undefined || mEstimationBP.ContactPerson       == '' ||\r" +
    "\n" +
    "            mEstimationBP.PhoneContactPerson1 == null || mEstimationBP.PhoneContactPerson1 == undefined || mEstimationBP.PhoneContactPerson1 == '' ||\r" +
    "\n" +
    "            mEstimationBP.PoliceNumber        == null || mEstimationBP.PoliceNumber        == undefined || mEstimationBP.PoliceNumber        == '' ||\r" +
    "\n" +
    "            mEstimationBP.Alamat              == null || mEstimationBP.Alamat              == undefined || mEstimationBP.Alamat              == '' ||\r" +
    "\n" +
    "            mEstimationBP.VIN                 == null || mEstimationBP.VIN                 == undefined || mEstimationBP.VIN                 == '' ||\r" +
    "\n" +
    "            mEstimationBP.ColorId             == null || mEstimationBP.ColorId             == undefined || mEstimationBP.ColorId             == '' ||\r" +
    "\n" +
    "            mEstimationBP.EngineNo            == null || mEstimationBP.EngineNo            == undefined || mEstimationBP.EngineNo            == '' ||\r" +
    "\n" +
    "            mEstimationBP.ModelCode           == null || mEstimationBP.ModelCode           == undefined || mEstimationBP.ModelCode           == '' ||\r" +
    "\n" +
    "            mEstimationBP.VehicleTypeId       == null || mEstimationBP.VehicleTypeId       == undefined || mEstimationBP.VehicleTypeId       == '' ||\r" +
    "\n" +
    "            mEstimationBP.KatashikiCode       == null || mEstimationBP.KatashikiCode       == undefined || mEstimationBP.KatashikiCode       == '' || !isValidEmail || emailLength>50 ||\r" +
    "\n" +
    "            (mEstimationBP.isCash             == 0    && (mEstimationBP.InsuranceId        == undefined || mEstimationBP.InsuranceId         == '' || mEstimationBP.InsuranceId == null ))\r" +
    "\n" +
    "            \">Draft</button> <button ng-click=\"doCustomCancel()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px; width:7%\" ng-show=\"estimationMode == 'Create' || estimationMode == 'Edit' || estimationMode == 'View'\">Kembali</button> </div> <bsform-grid ng-form=\"EstimasiBPform\" factory-name=\"estimasiBPFactory\" model=\"AEstimationBP\" model-id=\"EstimationBPId\" loading=\"loading\" get-data=\"getData\" on-validate-delete=\"onValidateDelete\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-before-edit=\"onBeforeEdit\" on-show-detail=\"onBeforeView\" on-before-new=\"onBeforeNew\" do-custom-cancel=\"doCustomCancel\" do-custom-save=\"doCustomSave\" hide-save-button=\"true\" hide-cancel-button=\"true\" grid-hide-action-column=\"true\" grid-custom-action-button-template=\"gridActionTemplate\" grid-custom-action-button-colwidth=\"170\" form-name=\"EstimasiBPform\" form-title=\"Diskon\" modal-title=\"Estimasi BP\" modal-size=\"small\" form-api=\"formApi\" icon=\"fa fa-fw fa-child\" show-advsearch=\"on\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>No. Polisi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" placeholder=\"No. Polisi\" name=\"NoPolisi\" onkeypress=\"return (event.charCode > 64 && event.charCode < 91)|| event.charCode >= 47 && event.charCode <= 57 || (event.charCode > 96 && event.charCode < 123)\" maxlength=\"9\" ng-model=\"estimationBPFilter.NoPolisi\" style=\"text-transform: uppercase\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Mulai</bsreqlabel> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"TanggalMulai\" date-options=\"dateOptions\" ng-model=\"estimationBPFilter.DateFrom\" required> </bsdatepicker> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Berakhir</bsreqlabel> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"TanggalBerakhir\" date-options=\"dateOptions\" ng-model=\"estimationBPFilter.DateTo\" required> </bsdatepicker> </div> </div> </div> </div> </bsform-advsearch> <div class=\"row\" style=\"padding:10px\"> <form name=\"formParent\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Estimasi</label> <input type=\"text\" name=\"NoEstimasi\" ng-model=\"mEstimationBP.EstimationNo\" skip-enable disabled class=\"form-control\"> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Kategori Pekerjaan</bsreqlabel> <bsselect style=\"margin-bottom: 10px\" name=\"CategoryId\" ng-change=\"onKategoriPekerjaanSelect(selected)\" ng-model=\"mEstimationBP.CategoryId\" data=\"woCategory\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Kategori Pekerjaan\" icon=\"fa fa-search\" required> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" ng-form=\"row1\"> <bsreqlabel>Km</bsreqlabel> <!-- <input ng-model=\"mEstimationBP.Km\" name=\"Km\"  placeholder=\"Km\" class=\"form-control\" required input-currencyprefix  maxlength=\"8\" number-only type=\"tel\"> --> <input type=\"text\" step=\"1\" min=\"0\" class=\"form-control\" name=\"Km\" placeholder=\"Km\" ng-model=\"mEstimationBP.Km\" ng-maxlength=\"7\" maxlength=\"7\" ng-keyup=\"givePattern(mEstimationBP.Km,$event)\" required> <em ng-messages=\"row1.Km.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jumlah karakter kurang</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> <p ng-if=\"isKmHistory >= 1 && nilaiKM <= KMTerakhir\" style=\"font-style:italic;margin-top:5px; color:  rgb(185, 74, 72)\">Tidak boleh kurang atau sama dengan {{KMTerakhir | number}} KM</p> </em> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\" style=\"padding-right:20px\"><!-- style=\"margin-left:15px;\" --> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Pembayaran</legend> <div class=\"col-md-6\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Pembayaran\" class=\"radiobox style-0\" ng-click=\"choosePembayaran(mEstimationBP.isCash)\" ng-model=\"mEstimationBP.isCash\" value=\"0\"> <span>Asuransi</span> </label> </div> </div> <div class=\"col-md-6\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Pembayaran\" class=\"radiobox style-0\" ng-click=\"choosePembayaran(mEstimationBP.isCash)\" ng-model=\"mEstimationBP.isCash\" value=\"1\"> <span>Cash</span> </label> </div> </div> <div class=\"col-md-12\" style=\"margin-top: 15px; margin-bottom:18px\" ng-show=\"mEstimationBP.isCash == 0\"> <div class=\"form-group\"> <bsreqlabel>Nama Asuransi</bsreqlabel> <bsselect name=\"InsuranceId\" ng-model=\"mEstimationBP.InsuranceId\" data=\"optionsLihatInformasi_NamaInsurance_Search\" item-text=\"InsuranceName\" item-value=\"InsuranceId\" on-select=\"onInsuranceSelected(selected)\" placeholder=\"Pilih Asuransi\" icon=\"fa fa-search\"> </bsselect> </div> </div> </fieldset></div> <div class=\"col-md-6\" ng-show=\"mEstimationBP.isCash == 0\" style=\"padding-right:10px; margin-left:-5px\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Asuransi</legend> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Polis Asuransi</label> <div class=\"input-icon-right\"> <input type=\"text\" name=\"NoPolis\" style=\"text-transform: uppercase\" ng-model=\"mEstimationBP.NoPolis\" placeholder=\"Nomor Polis Ansuransi\" class=\"form-control\" maxlength=\"50\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Periode Polis Asuransi Mulai</label> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"TanggalMulai\" date-options=\"dateOptions\" ng-model=\"mEstimationBP.PolisDateFrom\"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Periode Polis Asuransi Berakhir</label> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"TanggalBerakhir\" date-options=\"dateOptions\" ng-model=\"mEstimationBP.PolisDateTo\"> </bsdatepicker> </div> </div> </div> </div> </fieldset></div> </div> </div> </div> </div> </form> <div class=\"col-md-12\" style=\"margin-top:30px\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Informasi Umum\"> <form name=\"formInformasiUmum\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Informasi Pelanggan</legend> <div class=\"row\" ng-form=\"informasiPelanggan\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama</bsreqlabel> <!-- <div class=\"input-icon-right\"> --> <!-- <i class=\"fa fa-pencil\"></i> --> <input type=\"text\" class=\"form-control\" name=\"namaPelanggan\" style=\"text-transform: capitalize\" required ng-maxlength=\"50\" ng-minlength=\"3\" ng-model=\"mEstimationBP.ContactPerson\" placeholder=\"Nama Pelanggan\"> <em ng-messages=\"informasiPelanggan.namaPelanggan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> <!-- </div> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Handphone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input class=\"form-control\" name=\"Handphone\" number-only type=\"tel\" ng-model=\"mEstimationBP.PhoneContactPerson1\" placeholder=\"Nomor Handphone\" maxlength=\"15\" required> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Email</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Email\" placeholder=\"Email\" ng-change=\"onTypeEmail(mEstimationBP.Email)\" ng-model=\"mEstimationBP.Email\"> </div> <!-- <em ng-messages=\"informasiPelanggan.namaPelanggan.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\"> --> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <!-- <p ng-message=\"minlength\">Jml. Karakter kurang</p> --> <!-- <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> --> <!-- </em> --> <div ng-if=\"!isValidEmail\"><input type=\"hidden\" ng-model=\"fakeModel\" required><p style=\"color: #BB4A48; font-size: 12px; margin-top:7px\"><i>Format email tidak sesuai </i></p></div> <div ng-if=\"emailLength>50\"><input type=\"hidden\" ng-model=\"fakeModel\" required><p style=\"color: #BB4A48; font-size: 12px; margin-top:7px\"><i>Jumlah karakter terlalu banyak </i></p></div> </div> </div> <div class=\"col-md-12\"> <bsreqlabel>Alamat</bsreqlabel> <textarea ng-model=\"mEstimationBP.Alamat\" class=\"form-control\" name=\"Alamat\" rows=\"5\" ng-maxlength=\"150\" ng-minlength=\"3\"></textarea> <em ng-messages=\"informasiPelanggan.Alamat.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak ( Max. 150 Karakter) </p> </em> </div> </div> </fieldset> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Informasi Kendaraan</legend> <div class=\"row\" ng-form=\"listVehicle\" novalidate> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>No. Polisi</bsreqlabel> <div class=\"input-icon-right\" ng-if=\"estimationMode != 'Create'\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" skip-enable name=\"PoliceNumber\" ng-disabled=\"disablePoliceNo == true\" placeholder=\"No. Polisi\" maxlength=\"9\" alpha-numeric ng-model=\"mEstimationBP.PoliceNumber\" style=\"text-transform: uppercase\" required> </div> <div class=\"input-icon-right\" ng-if=\"estimationMode == 'Create'\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PoliceNumber\" placeholder=\"No. Polisi\" maxlength=\"9\" alpha-numeric ng-model=\"mEstimationBP.PoliceNumber\" style=\"text-transform: uppercase\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Model</bsreqlabel> <bsselect style=\"margin-bottom: 10px\" name=\"Model\" ng-model=\"mEstimationBP.ModelCode\" on-select=\"onSelectModelChanged(selected)\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Pilih Model\" icon=\"fa fa-search\" required> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tipe</bsreqlabel> <bsselect style=\"margin-bottom: 10px\" name=\"Tipe\" ng-model=\"mEstimationBP.VehicleTypeId\" data=\"optionsTipe\" item-text=\"Description,KatashikiCode\" item-value=\"VehicleTypeId\" on-select=\"onSelectTipeChanged(selected)\" placeholder=\"Pilih tipe\" icon=\"fa fa-search\" required> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Full Model / Katashiki</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" skip-enable disabled ng-model=\"mEstimationBP.KatashikiCode\" placeholder=\"Katashiki Code\" name=\"KatashikiCode\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Rangka</bsreqlabel> <input name=\"VIN\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-model=\"mEstimationBP.VIN\" placeholder=\"No Rangka\" required ng-maxlength=\"17\" ng-minlength=\"12\" maxlength=\"17\"> <em ng-messages=\"listVehicle.VIN.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna</bsreqlabel> <ui-select name=\"colorMobil\" ng-model=\"mEstimationBP.ColorName\" data=\"VehicColor\" on-select=\"selectedColor($item.MColor)\" ng-required=\"true\" search-enabled=\"false\" theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Warna\"> {{mEstimationBP.ColorName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicColor\"> <span ng-bind-html=\"item.MColor.ColorName\"></span> </ui-select-choices> </ui-select> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Mesin</bsreqlabel> <input type=\"text\" name=\"NoMesin\" maxlength=\"20\" style=\"text-transform: uppercase\" class=\"form-control\" ng-model=\"mEstimationBP.EngineNo\" placeholder=\"No. Mesin\" required> <em ng-messages=\"listVehicle.NoMesin.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </fieldset> </form> </uib-tab> <uib-tab index=\"1\" heading=\"Vehicle Condition\"> <div smart-include=\"app/services/estimasiBP/estimasi_WAC.html\"></div> </uib-tab> <uib-tab index=\"2\" heading=\"Order Pekerjaan\" ng-click=\"onOrderPekerjaan()\"> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top:10px\"> <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"lmRequest\" on-select-rows=\"onListSelectRows\" selected-rows=\"listRequestSelectedRows\" confirm-save=\"false\" list-api=\"listApiRequest\" button-settings=\"listButtonSettings\" on-before-save=\"onBeforeSaveJobRequest\" modal-size=\"small\" modal-title=\"Permintaan\" list-title=\"PERMINTAAN\" list-height=\"230\" title-style=\"font-weight:bold\"> <bslist-template style=\"font-size : 13px;font-weight:normal\"> <div> <div class=\"form-group\"> <textarea class=\"form-control\" name=\"RequestDesc\" placeholder=\"Permintaan\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" ng-form=\"rowpermintaan\"> <bsreqlabel>Permintaan</bsreqlabel> <textarea ng-maxtlength=\"200\" maxlength=\"200\" class=\"form-control\" placeholder=\"Permintaan\" name=\"reqdesc\" ng-model=\"lmRequest.RequestDesc\" required>\r" +
    "\n" +
    "                                                </textarea> <em ng-messages=\"rowpermintaan.reqdesc.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> </bslist-modal-form> </bslist> </div> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Order Pekerjaan</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-6 col-xs-6\"> <button class=\"ui icon inverted grey button\" style=\"font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px\" ng-click=\"deleteAllDataGridViewWork()\"><i class=\"fa fa-trash\"></i> </button> </div> <div class=\"col-md-6 col-xs-6\"> <button ng-click=\"plushOrder()\" class=\"rbtn btn pull-right\" data-target=\"#ModalOP\" on-deny=\"denied\" data-toggle=\"modal\" data-backdrop=\"static\" data-keyboard=\"false\"><i class=\"fa fa-plus\"></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridWorkView\" class=\"ag-theme-balham\" style=\"height: 400px\"></div> </fieldset> </div> <div class=\"col-md-12\" style=\"margin-top: 10px;margin-bottom: 10px\"> <div class=\"col-md-12\"> <div class=\"row\"> <div ui-grid=\"gridEstimation\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak id=\"gridEstimation\"> </div> </div> <div class=\"row\" style=\"margin-top: 20px\"> <div class=\"col-md-12 col-xs-12\"> <button ng-click=\"calculateforest()\" class=\"rbtn btn pull-right\"><i class=\"fa fa-fw fa-clock-o\"></i>&nbsp;Calculate Time</button> </div> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"col-md-6 col-xs-6\"> <input type=\"text\" name=\"Jam\" ng-model=\"estHours\" size=\"5\" ng-readonly=\"true\">&nbsp;<label>Jam</label> &nbsp;&nbsp;&nbsp;<img src=\"images/rightarrow.png\">&nbsp; </div> <div class=\"col-md-6 col-xs-6\"> <input type=\"text\" ng-model=\"estDays\" name=\"estDays\" size=\"5\" ng-readonly=\"true\">&nbsp;<label><strong>Hari</strong></label> </div> </div> <div class=\"col-md-8\"> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc.</th> <th>Nominal Diskon</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWork | currency:\"\":0}}</td> <td style=\"text-align: right\">{{DisplayDisTask}}%</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalFinalTask | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterial | currency:\"\":0}}</td> <td style=\"text-align: right\">{{DisplayDisParts}}%</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalFinalParts | currency:\"\":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{PPN| currency:\"\":0}}</td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ ( totalEstimasi ) | currency:\"\":0 }} </td> </tr> <tr> <td>Total DP Material (Parts / Bahan)</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDp | currency:\"\":0}}</td> </tr> </tbody> </table> </div> </div> </div> <!-- modal order pekerjaan --> <!--   --> <div class=\"ui modal ModalOP\" role=\"dialog\" tabindex=\"0\" ng-keydown=\"allowPatternFilter($event)\" settings=\"{'transition':'vertical flip','closable':false, 'keyboard': false }\" style=\"background-color: transparent !important\"> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Job Data</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-xs-6\"> <!-- <div class=\"row\"></div> --> <div class=\"form-group\" show-errors> <div class=\"col-xs-12\"> <bsreqlabel>Pekerjaan</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"mEstimationBP.TaskName\" get-data=\"getWork\" ng-disabled=\"disJob\" item-text=\"TaskName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWork\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" icon=\"fa-id-card-o\" required> </bstypeahead> <!--  <input ng-if=\"tmpIsTAM == 0\" placeholder=\"Nama Pekerjaan\" type=\"text\" required ng-disabled=\"disJob\" class=\"form-control\" name=\"jobName\" ng-model=\"mEstimationBP.TaskName\"> --> <em ng-messages=\"row1.jobName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- <bserrmsg field=\"jobName\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <!-- <div class=\"row\"> --> <!-- <div class=\"form-group\" show-errors> --> <div class=\"col-xs-12\"> <bsreqlabel>Satuan</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-disabled=\"disJob\" ng-model=\"mEstimationBP.ProcessId\" data=\"unitData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" required>\r" +
    "\n" +
    "                                                        </bsselect> --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-disabled=\"disJob\" ng-model=\"mEstimationBP.ProcessId\" ng-change=\"selectSatuan(mEstimationBP.ProcessId)\" data=\"unitData\" placeholder=\"Pilih Tipe\" required> <option ng-repeat=\"sat in unitData\" value=\"{{sat.MasterId}}\">{{sat.Name}}</option> </select> <!--  <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"mEstimationBP.ProcessId\" ng-hide=\"true\" required ng-required=\"true\"> --> <em ng-messages=\"row1.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- </div> --> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> <!-- </div> --> <!-- <bserrmsg field=\"Fr\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <!-- <div class=\"row\"> --> <!-- <div class=\"form-group\" show-errors> --> <div class=\"col-xs-12\"> <bsreqlabel>Tipe Diskon</bsreqlabel> <select name=\"TipeDisc\" convert-to-number class=\"form-control\" ng-model=\"mEstimationBP.TipeDisc\" data=\"diskonData\" placeholder=\"Pilih Tipe\" ng-disabled=\"showSPK\" ng-change=\"selectDiscount(mEstimationBP.TipeDisc)\" ng-required=\"mEstimationBP.isCash == 1\"> <option ng-repeat=\"disc in diskonData\" value=\"{{disc.MasterId}}\">{{disc.Name}}</option> </select> <em ng-messages=\"row1.TipeDisc.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- </div> --> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> <!-- </div> --> <!-- <bserrmsg field=\"Fr\"></bserrmsg> --> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <div class=\"col-md-12\" style=\"padding-right: 0px;padding-left: 0px\"> <bsreqlabel>Harga</bsreqlabel> <div> <!-- <input type=\"number\" required ng-maxlength=\"10\" min=0 class=\"form-control\" name=\"price\" ng-model=\"mEstimationBP.Summary\"\r" +
    "\n" +
    "                                                        ng-disabled=\"PriceAvailable\"> --> <input type=\"text\" required maxlength=\"11\" min=\"0\" class=\"form-control\" name=\"Fare\" ng-model=\"mEstimationBP.Fare\" ng-disabled=\"PriceAvailable\" input-currencyprefix number-only> </div> <em ng-messages=\"row1.Fare.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"maxlength\">justifiedmlah karakter terlalu banyak</p> </em> </div> <!-- <bserrmsg field=\"price\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-disabled=\"disJob\"  ng-model=\"mEstimationBP.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"Name\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCustFromPlus(selected)\" required>\r" +
    "\n" +
    "                                                </bsselect> --> <select name=\"pembayaran\" ng-disabled=\"disJob\" class=\"form-control\" ng-model=\"mEstimationBP.PaidById\" placeholder=\"Pilih Pembayaran\" ng-change=\"selectTypeCust(mEstimationBP.PaidById)\" ng-required=\"true\" required> <option ng-repeat=\"pay in paymentData\" value=\"{{pay.MasterId}}\">{{pay.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"PaidById\" ng-model=\"mEstimationBP.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div class=\"col-md-12\" style=\"padding-right: 0px;padding-left: 0px\"> <bsreqlabel>Diskon (%)</bsreqlabel> <div> <!-- <input type=\"number\" required ng-maxlength=\"10\" min=0 class=\"form-control\" name=\"price\" ng-model=\"mEstimationBP.Summary\"\r" +
    "\n" +
    "                                                        ng-disabled=\"PriceAvailable\"> --> <!-- <input type=\"text\" required ng-maxlength=\"2\" min=0 class=\"form-control\" name=\"DiskonPersen\" ng-model=\"mEstimationBP.DiskonPersen\" ng-disabled=\"disabledDisc || showSPK\"  number-only skip-enable> --> <input type=\"number\" onkeypress=\"return event.charCode >= 46 && event.charCode <= 57\" maxlength=\"10\" class=\"form-control\" name=\"DiskonPersen\" ng-model=\"mEstimationBP.DiskonPersen\" ng-change=\"onTypeDiskonPersen(mEstimationBP.DiskonPersen)\" ng-disabled=\"disabledDisc || showSPK\" skip-enable> </div> <div ng-if=\"mEstimationBP.DiskonPersen >=100.01\"><input type=\"hidden\" ng-model=\"fakeModel\" required><p style=\"color: #BB4A48; font-size: 12px; margin-top:7px\"><i>Diskon tidak boleh melebihi 100%</i></p></div> <!-- <em ng-messages=\"row1.DiskonPersen.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "                                                        <p ng-message=\"required\">Wajib diisi</p>\r" +
    "\n" +
    "                                                        <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "                                                    </em> --> </div> <!-- <bserrmsg field=\"price\"></bserrmsg> --> </div> </div> </div> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Parts Data</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-12 col-xs-12\"> <button ng-disabled=\"disJob || mEstimationBP.TaskName == undefined || mEstimationBP.TaskName == null || mEstimationBP.TaskName == ''\" skip-enable ng-click=\"plushParts()\" class=\"rbtn btn pull-right\"><i class=\"fa fa-plus\" n></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"GridParts\" class=\"ag-theme-balham\" style=\"height: 400px\" id=\"gridParts\"></div> </fieldset> </div> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <div class=\"col-md-3 pull-right\" style=\"padding-right: 0px;padding-left: 0px\"> <label>DP (%)</label> <div> <!-- <input type=\"number\" required ng-maxlength=\"10\" min=0 class=\"form-control\" name=\"price\" ng-model=\"mEstimationBP.Summary\"\r" +
    "\n" +
    "                                                    ng-disabled=\"PriceAvailable\"> --> <input type=\"number\" onkeypress=\"return event.charCode >= 46 && event.charCode <= 57\" maxlength=\"10\" class=\"form-control\" name=\"DP\" ng-model=\"mEstimationBP.DP\" ng-change=\"onTypeDP(mEstimationBP.DP)\" ng-disabled=\"disJob\"> </div> <div ng-if=\"mEstimationBP.DP >=100.01\"><input type=\"hidden\" ng-model=\"fakeModel\" required><p style=\"color: #BB4A48; font-size: 12px; margin-top:7px\"><i>DP tidak boleh melebihi 100%</i></p></div> <!-- <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "                                                    <p ng-message=\"required\">Wajib diisi</p>\r" +
    "\n" +
    "                                                    <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "                                                </em> --> </div> <!-- <bserrmsg field=\"price\"></bserrmsg> --> </div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"col-md-12\"> <div class=\"pull-right\" style=\"padding:1rem 1rem !important; background-color: #f9fafb\"> <div class=\"wbtn btn ng-binding\" style=\"width:100px\" ng-click=\"closeModal()\">Cancel</div> <div class=\"rbtn btn ng-binding ng-scope\" ng-if=\"mEstimationBP.isCash == 1 \" ng-disabled=\"disJob || \r" +
    "\n" +
    "                                            mEstimationBP.TaskName == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.DP >= 100.01 ||\r" +
    "\n" +
    "                                            mEstimationBP.TaskName == null || \r" +
    "\n" +
    "                                            mEstimationBP.TaskName == '' || \r" +
    "\n" +
    "                                            mEstimationBP.TipeDisc == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.TipeDisc == null || \r" +
    "\n" +
    "                                            mEstimationBP.TipeDisc == '' || \r" +
    "\n" +
    "                                            mEstimationBP.Fare == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.Fare == null || \r" +
    "\n" +
    "                                            mEstimationBP.ProcessId == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.ProcessId == null || \r" +
    "\n" +
    "                                            mEstimationBP.ProcessId == '' || \r" +
    "\n" +
    "                                            mEstimationBP.PaidById == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.PaidById == null || \r" +
    "\n" +
    "                                            mEstimationBP.PaidById == '' || \r" +
    "\n" +
    "                                            mEstimationBP.DiskonPersen >= 100.01 ||\r" +
    "\n" +
    "                                            mEstimationBP.DiskonPersen == null\" skip-enable ng-click=\"saveFin()\" style=\"width:100px\">Simpan</div> <div class=\"rbtn btn ng-binding ng-scope\" ng-if=\"mEstimationBP.isCash == 0 \" ng-disabled=\"disJob || \r" +
    "\n" +
    "                                            mEstimationBP.TaskName == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.DP >=100.01 ||\r" +
    "\n" +
    "                                            mEstimationBP.TaskName == null || \r" +
    "\n" +
    "                                            mEstimationBP.TaskName == '' || \r" +
    "\n" +
    "                                            mEstimationBP.Fare == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.Fare == null || \r" +
    "\n" +
    "                                            mEstimationBP.ProcessId == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.ProcessId == null || \r" +
    "\n" +
    "                                            mEstimationBP.ProcessId == '' || \r" +
    "\n" +
    "                                            mEstimationBP.PaidById == undefined || \r" +
    "\n" +
    "                                            mEstimationBP.PaidById == null || \r" +
    "\n" +
    "                                            mEstimationBP.PaidById == '' || \r" +
    "\n" +
    "                                            mEstimationBP.DiskonPersen >= 100.01 ||\r" +
    "\n" +
    "                                            mEstimationBP.DiskonPersen == null\" skip-enable ng-click=\"saveFin()\" style=\"width:100px\">Simpan</div> </div> </div> </div> </p> </div> </uib-tab> </uib-tabset> </div> </div> </bsform-grid> <!-- </div> --> <div class=\"ui modal ModalEstimasiYangBelumDigunakan\" role=\"dialog\" style=\"background-color: transparent !important\"> <p style=\"text-align: center\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Estimasi yang belum digunakan</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"row\"> <div class=\"col-md-5\" style=\"padding: 10px\"> <p>List Estimasi untuk kendaraan dengan No. Polisi</p> </div> <div class=\"col-md-3\"> <input type=\"text\" name=\"PoliceNum\" class=\"form-control\" skip-enable disabled ng-model=\"mEstimationBP.PoliceNumber\"> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <div ui-grid=\"listEstimasiYangBelumDigunakan\" ui-grid-selection class=\"grid\" ui-grid-cellnav ui-grid-edit ui-grid-row-edit ui-grid-pagination ui-grid-auto-resize></div> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important;background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"wbtn btn ng-binding\" style=\"width:100px\" ng-click=\"btnBatalCekEstimasi()\">Batal</div> <div class=\"rbtn btn ng-binding ng-scope\" style=\"width:100px\">Pilih </div> </div> </p> </div> <!-- \r" +
    "\n" +
    "<div class=\"ui modal ModalInputSPK\" role=\"dialog\" style=\"background-color: transparent !important\">\r" +
    "\n" +
    "    <p style=\"text-align: center\">\r" +
    "\n" +
    "        <div class=\"modal-header\"\r" +
    "\n" +
    "            style=\" padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15);\">\r" +
    "\n" +
    "            <h4 ng-if=\"isInputForm == false\" class=\"modal-title\">SPK List</h4>\r" +
    "\n" +
    "            <h4 ng-if=\"isInputForm == true\" class=\"modal-title\">Input SPK</h4>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\" ng-show=\"isInputForm == false\">\r" +
    "\n" +
    "            <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"col-md-12\">\r" +
    "\n" +
    "                        <fieldset style=\"margin-top: 10px; border:1px solid #bdbdbd; padding: 20px;\">\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Filter</label>\r" +
    "\n" +
    "                                        <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                            <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                            <input type=\"text\" class=\"form-control\" placeholder=\"Filter Data SPK\"\r" +
    "\n" +
    "                                                ng-model=\"FilterSpkNo\" id=\"filter-text-box\" ng-change=\"onFilterTextBoxChanged(FilterSpkNo)\">\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <div class=\"col-md-6\">\r" +
    "\n" +
    "                                    &nbsp;\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <div class=\"col-md-2\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>\r" +
    "\n" +
    "                                        <button class=\"btn wbtn pull-right\" onclick=\"this.blur()\"\r" +
    "\n" +
    "                                            ng-click=\"onSearchSPKFilter(FilterSpkNo)\" style=\"width:101px;\"\r" +
    "\n" +
    "                                            tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\"\r" +
    "\n" +
    "                                            tooltip-animation=\"false\" uib-tooltip=\"Search\">\r" +
    "\n" +
    "                                            <i class=\"large icons\">\r" +
    "\n" +
    "                                                <i class=\"database icon\"></i>\r" +
    "\n" +
    "                                                <i class=\"inverted corner filter icon\"></i>\r" +
    "\n" +
    "                                            </i>Search\r" +
    "\n" +
    "                                        </button>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        </fieldset>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\" style=\"margin: 10px 0 0 0\">\r" +
    "\n" +
    "                    <div ag-grid=\"SpkListGalang\" class=\"ag-theme-balham\" style=\"height: 400px;\"></div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\" ng-show=\"isInputForm == true\">\r" +
    "\n" +
    "            <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bsreqlabel>Kategori SPK</bsreqlabel>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"col-md-8\">\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"radio-inline\">\r" +
    "\n" +
    "                            <label class=\"radio\">\r" +
    "\n" +
    "                                <input type=\"radio\" class=\"radiobox style-0\" ng-model=\"mSPK.SpkCategory\"\r" +
    "\n" +
    "                                    ng-disabled=\"spkMode == 'view' || spkMode == 'edit'\" ng-change=\"onTipeSpkChange(mSPK.SpkCategory)\"\r" +
    "\n" +
    "                                    value=0>\r" +
    "\n" +
    "                                <span>SPK Utama</span>\r" +
    "\n" +
    "                            </label>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                \r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"radio-inline\">\r" +
    "\n" +
    "                            <label class=\"radio\">\r" +
    "\n" +
    "                                <input type=\"radio\" class=\"radiobox style-0\" ng-model=\"mSPK.SpkCategory\"\r" +
    "\n" +
    "                                    ng-disabled=\"spkMode == 'view' || spkMode == 'edit'\" value=1\r" +
    "\n" +
    "                                    ng-change=\"onTipeSpkChange(mSPK.SpkCategory)\">\r" +
    "\n" +
    "                                <span>SPK Tambahan</span>\r" +
    "\n" +
    "                            </label>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                \r" +
    "\n" +
    "                \r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>Nomor SPK</bsreqlabel>\r" +
    "\n" +
    "                            <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                <input type=\"text\" class=\"form-control\" ng-if=\"mSPK.SpkCategory == 0\" placeholder=\"Input Nomor SPK \"\r" +
    "\n" +
    "                                    ng-disabled=\"mSPK.SpkCategory != 0 || spkMode == 'view' \"\r" +
    "\n" +
    "                                    ng-model=\"mSPK.SpkNo\">\r" +
    "\n" +
    "                \r" +
    "\n" +
    "                                <input type=\"text\" class=\"form-control\" ng-if=\"mSPK.SpkCategory != 0\"\r" +
    "\n" +
    "                                    placeholder=\"Input Nomor SPK Takdipakai\" ng-disabled=\"mSPK.SpkCategory != 0 || spkMode == 'view' \"\r" +
    "\n" +
    "                                    ng-model=\"mSPK.SpkNoUtama\">\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>Nomor SPK Tambahan</bsreqlabel>\r" +
    "\n" +
    "                            <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                <input type=\"text\" class=\"form-control\" ng-if=\"mSPK.SpkCategory != 0\"\r" +
    "\n" +
    "                                    placeholder=\"Input Nomor SPK Tambahan\" ng-disabled=\"mSPK.SpkCategory == 0 || spkMode == 'view' \"\r" +
    "\n" +
    "                                    ng-model=\"mSPK.SpkNo\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <input type=\"text\" class=\"form-control\" ng-if=\"mSPK.SpkCategory == 0\"\r" +
    "\n" +
    "                                    placeholder=\"Input Nomor SPK Tambahan Takdipakai\"\r" +
    "\n" +
    "                                    ng-disabled=\"mSPK.SpkCategory == 0 || spkMode == 'view' \" ng-model=\"mSPK.SpkNoTambahan\">\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <label>Nomor SPK Utama</label>\r" +
    "\n" +
    "                            <bsselect style=\"margin-bottom: 10px;\" name=\"Model\" ng-model=\"mSPK.EstSPKListId\"\r" +
    "\n" +
    "                                on-select=\"onNomorSpkUtamaSelected(selected)\" data=\"optionsSpkList\" item-text=\"SpkNo\"\r" +
    "\n" +
    "                                item-value=\"EstSPKListId\" placeholder=\"Pilih Kategori SPK\" icon=\"fa fa-search\"\r" +
    "\n" +
    "                                ng-disabled=\"mSPK.SpkCategory == 0 || spkMode == 'view' || spkMode == 'edit'\"> \r" +
    "\n" +
    "                            </bsselect>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div style=\"width:300px; height:70px\">&nbsp; </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>Nilai OR (IDR)</bsreqlabel>\r" +
    "\n" +
    "                            <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                <input type=\"text\" class=\"form-control\" placeholder=\"Nilai OR\" ng-model=\"mSPK.OR\"\r" +
    "\n" +
    "                                    input-currencyprefix type=\"tel\"\r" +
    "\n" +
    "                                    onkeypress=\"return event.charCode >= 47 && event.charCode <= 57\"\r" +
    "\n" +
    "                                    ng-disabled=\"mSPK.SpkCategory != 0 || spkMode == 'view' \"\r" +
    "\n" +
    "                                    onpaste=\"return event.charCode >= 47 && event.charCode <= 57\">\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>Periode Asuransi</bsreqlabel>\r" +
    "\n" +
    "                            <div class=\"input-icon-right form-group \">\r" +
    "\n" +
    "                                <bsdatepicker name=\"TanggalMulai\" date-options=\"dateOptions\" placeholder=\"Periode Asuransi\"\r" +
    "\n" +
    "                                    ng-disabled=\"mSPK.SpkCategory != 0 || spkMode == 'view' \" ng-model=\"mSPK.PeriodePolis\">\r" +
    "\n" +
    "                                </bsdatepicker>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>No. Polis</bsreqlabel>\r" +
    "\n" +
    "                            <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                <input type=\"text\" class=\"form-control\" placeholder=\"Nomor Polis\"\r" +
    "\n" +
    "                                    ng-disabled=\"mSPK.SpkCategory != 0 || spkMode == 'view' \" ng-model=\"mSPK.NoPolis\">\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div style=\"width:300px; height:70px\">&nbsp; </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>No. LK</bsreqlabel>\r" +
    "\n" +
    "                            <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                <input type=\"text\" class=\"form-control\" ng-disabled=\"mSPK.SpkCategory != 0 || spkMode == 'view' \"\r" +
    "\n" +
    "                                    placeholder=\"Nomor Laporan Kecelakaan\" ng-model=\"mSPK.NoLK\">\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div style=\"width:300px; height:70px\">&nbsp; </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"col-md-4\">\r" +
    "\n" +
    "                    <div class=\"customheight\" style=\"height: 300px;\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"modal-footer\" ng-if=\"isInputForm == false\"\r" +
    "\n" +
    "            style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15);\">\r" +
    "\n" +
    "            <div class=\"col-md-12\">\r" +
    "\n" +
    "                <div class=\"pull-right\" style=\"padding:1rem 1rem !important; background-color: #f9fafb;\">\r" +
    "\n" +
    "                    <div class=\"wbtn btn ng-binding\" style=\"width:100px;\" ng-click=\"btnBatalSPK()\">Batal</div>\r" +
    "\n" +
    "                    <div class=\"rbtn btn ng-binding\" ng-scope style=\"width:100px;\" ng-click=\"switchTambahSPK();newSPK()\"><i class=\"fa fa-plus\"> </i> &nbsp; Tambah</div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"modal-footer\" ng-if=\"isInputForm == true\"\r" +
    "\n" +
    "            style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15);\">\r" +
    "\n" +
    "            <div class=\"col-md-12\">\r" +
    "\n" +
    "                <div class=\"pull-right\" style=\"padding:1rem 1rem !important; background-color: #f9fafb;\">\r" +
    "\n" +
    "                    <div class=\"wbtn btn ng-binding\" style=\"width:100px;\" ng-click=\"switchTambahSPK()\">Batal</div>\r" +
    "\n" +
    "                    <div class=\"rbtn btn ng-binding ng-scope\" style=\"width:100px;\" \r" +
    "\n" +
    "                    ng-disabled=\"mSPK.SpkNo == null || mSPK.SpkNo == '' || mSPK.SpkNo == undefined ||  \r" +
    "\n" +
    "                                mSPK.OR == null || mSPK.OR == '' || mSPK.OR == undefined ||  \r" +
    "\n" +
    "                                mSPK.PeriodePolis == null || mSPK.PeriodePolis == '' || mSPK.PeriodePolis == undefined ||  \r" +
    "\n" +
    "                                mSPK.NoLK == null || mSPK.NoLK == '' || mSPK.NoLK == undefined ||  \r" +
    "\n" +
    "                                mSPK.NoPolis == null || mSPK.NoPolis == '' || mSPK.NoPolis == undefined\" ng-click=\"onSpkSimpanClick()\">Simpan\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </p>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    " --> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "        <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "        <span ng-bind-html=\"match.model.PartsName | uibTypeaheadHighlight:query\"></span>        \r" +
    "\n" +
    "    </a></script> <!-- akhir modal order pekerjaan -->"
  );


  $templateCache.put('app/services/estimasiBP/estimasi_WAC.html',
    "<div class=\"col-md-12\" style=\"margin-top:20px\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Type Kendaraan</label> <div class=\"icon-addon left-addon ui-select\"><i class=\"fa fa-car\"></i> <ui-select name=\"vehicType\" ng-model=\"filter.vTypeVC\" icon=\"fa fa-car\" on-select=\"chooseVTypeVC($item.MasterId)\" search-enabled=\"false\" skip-enable theme=\"select2\" id=\"EstimasiType\"> <ui-select-match style=\"padding-left:30px\" allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.Name}} </ui-select-match> <ui-select-choices repeat=\"item in VTypeData\"> <span ng-bind-html=\"item.Name\"></span> </ui-select-choices> </ui-select> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"padding-right: 20%; padding-left:20%\" ng-if=\"imageSelVC !== undefined\" ng-style=\"customHeight\"><!-- ng-style=\"customHeight\" --> <img name=\"img\" ng-init=\"this.init\" ng-areas=\"areasArrayVC\" ng-areas-width=\"700\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSelVC}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClickVC\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> </div> <div class=\"row\" ng-show=\"!disableWACExt\"> <div class=\"col-md-12\"> <table id=\"estGridVc\" width=\"100%\" class=\"table table-striped table-bordered table-hover table-responsive\" style=\"margin-top: 10px\"> <thead> <tr> <th ng-style=\"{'width':'{{col.width}}'}\" ng-hide=\"col.visible == false\" ng-repeat=\"col in gridVC.columnDefs\">{{col.name}}</th> </tr> </thead> <tbody> <tr ng-repeat=\"column in gridVC.data\"> <td ng-hide=\"true\" style=\"width:5%\">{{column.JobWACExtId}}</td> <td style=\"width:28%\">{{column.ItemName}}</td> <td style=\"width:47%\" style=\"padding:0px\"> <div class=\"col-md-12 col-xs-12\"> <div ng-repeat=\"item in StatusVehicle\" style=\"float:left;padding-right:15px;padding-left:15px\"> <label class=\"radio\"> <input class=\"radiobox style-0\" ng-model=\"column.StatusVehicleId\" type=\"radio\" ng-disabled=\"estimationMode == 'View'\" ng-change=\"checkTaskByVehicleCondtion(gridVC.data,1,column.StatusVehicleId)\" ng-value=\"{{item.MasterId}}\"><span>{{item.Name}}</span> </label> </div> </div> </td> <td style=\"width:25%\" style=\"padding:0px\"> <div class=\"col-md-12 col-xs-12\"> <div ng-repeat=\"item in PointsVehicle\" ng-if=\"(column.StatusVehicleId !== undefined && column.StatusVehicleId !== null && column.typeName == 1 && column.showPoints == 1)\" style=\"float:left;padding-right:10px;padding-left:10px\"> <label class=\"radio\"> <input class=\"radiobox style-0\" ng-model=\"column.PointId\" type=\"radio\" ng-disabled=\"estimationMode == 'View'\" ng-change=\"checkTaskByVehicleCondtion(gridVC.data,0,column.PointId)\" ng-value=\"{{item.MasterId}}\"><span>{{item.Name}}</span> </label> </div> </div> </td> <td ng-hide=\"true\" style=\"width:5%\">{{column.typeName}}</td> </tr> <tr ng-if=\"!gridVC.data.length\"> <td colspan=\"{{gridVC.columnDefs.length}}\"> <div style=\"opacity: .25;font-size: 3em;width: 100%;text-align: center;z-index: 1000\">No Data Available </div> </td> </tr> </tbody> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bscheckbox ng-model=\"disableWACExtVC\" label=\"Clear\" true-value=\"true\" false-value=\"false\" ng-change=\"chgChkVC()\"> </bscheckbox> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/estimasiBP/modalMaster.html',
    "<div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-xs-6\"> <div class=\"row\"></div> <div class=\"form-group\" show-errors> <bsreqlabel>Pekerjaan</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"mEstimationBP.TaskName\" get-data=\"getWork\" ng-disabled=\"isRelease\" item-text=\"TaskName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWork\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" icon=\"fa-id-card-o\" required> </bstypeahead> <em ng-messages=\"row1.jobName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"jobName\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div class=\"row\"> <!-- <div class=\"form-group\" show-errors> --> <div class=\"col-xs-12\"> <bsreqlabel>Satuan</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-disabled=\"isRelease\" ng-model=\"mEstimationBP.ProcessId\" data=\"unitData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" required>\r" +
    "\n" +
    "                    </bsselect> --> <!-- ng-disabled=\"isRelease\"  --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-model=\"mEstimationBP.ProcessId\" data=\"unitData\" placeholder=\"Pilih Tipe\" required> <option ng-repeat=\"sat in unitData\" value=\"{{sat.MasterId}}\">{{sat.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"mEstimationBP.ProcessId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> <!-- </div> --> <!-- <bserrmsg field=\"Fr\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div class=\"row\"> <!-- <div class=\"form-group\" show-errors> --> <div class=\"col-xs-12\"> <bsreqlabel>No. SPK</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-disabled=\"isRelease\" ng-model=\"mEstimationBP.ProcessId\" data=\"unitData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" required>\r" +
    "\n" +
    "                    </bsselect> --> <!-- ng-disabled=\"isRelease\" --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-model=\"mEstimationBP.ProcessId\" data=\"unitData\" placeholder=\"Pilih Tipe\" required> <option ng-repeat=\"sat in unitData\" value=\"{{sat.MasterId}}\">{{sat.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"mEstimationBP.ProcessId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> <!-- </div> --> <!-- <bserrmsg field=\"Fr\"></bserrmsg> --> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <div class=\"col-md-12\" style=\"padding-right: 0px;padding-left: 0px\"> <bsreqlabel>Harga</bsreqlabel> <!-- <div ng-if=\"PriceAvailable == false\"> --> <!-- <input type=\"number\" ng-maxlength=\"10\" required min=0 class=\"form-control\" name=\"price\" ng-model=\"mEstimationBP.Fare\"\r" +
    "\n" +
    "                    ng-disabled=\"\"> --> <!--     <input type=\"text\" ng-maxlength=\"10\" required min=0 class=\"form-control\" name=\"price\" ng-model=\"mEstimationBP.Fare\" ng-disabled=\"\" currency-formating number-only>\r" +
    "\n" +
    "                </div> --> <div> <!-- <input type=\"number\" required ng-maxlength=\"10\" min=0 class=\"form-control\" name=\"price\" ng-model=\"mEstimationBP.Summary\"\r" +
    "\n" +
    "                    ng-disabled=\"PriceAvailable\"> --> <input type=\"text\" required ng-maxlength=\"10\" min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"mEstimationBP.Fare\" ng-disabled=\"PriceAvailable\" currency-formating number-only> </div> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> <!-- <bserrmsg field=\"price\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-disabled=\"isRelease\" ng-model=\"mEstimationBP.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected,mEstimationBP)\" required>\r" +
    "\n" +
    "            </bsselect> --> <select ng-init=\"mEstimationBP.PaidById=''\" name=\"pembayaran\" convert-to-number ng-disabled=\"disablePembayaran || JobIsWarranty == 1\" class=\"form-control\" ng-model=\"mEstimationBP.PaidById\" placeholder=\"Pilih Pembayaran\" ng-change=\"selectTypeCust(mEstimationBP,mEstimationBP.PaidById)\" ng-required=\"true\" required> <option value=\"\"></option> <option ng-repeat=\"pay in paymentData\" value=\"{{pay.MasterId}}\">{{pay.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"mEstimationBP.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpe.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.TaskName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );

}]);
