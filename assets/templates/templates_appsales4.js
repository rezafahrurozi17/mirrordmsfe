angular.module('templates_appsales4', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/sales/sales4/deliveryRequest/approvalUrgentMemo/approvalUrgentMemo.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "       -moz-border-radius: 4px;\r" +
    "\n" +
    "            border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <!--<script type=\"text/javascript\" src=\"js/uiscript.js\"></script>--> <bsform-grid ng-form=\"ApprovalUrgentMemoForm\" factory-name=\"ApprovalUrgentMemoFactory\" model=\"mApprovalUrgentMemo\" model-id=\"SpkId\" loading=\"loading\" hide-new-button=\"true\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ApprovalUrgentMemoForm\" form-title=\"Approval Urgent Memo\" modal-title=\"Approval Urgent Memo\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> </bsform-grid> <div class=\"ui modal ModalAlasanPenolakanApprovalUrgentMemo\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Alasan Penolakan</h4> </div> <br> <div class=\"modal-body\"> <textarea rows=\"10\" cols=\"80\" maxlength=\"512\" placeholder=\"input alasan...\" ng-model=\"mApprovalUrgentMemo.ApprovalNote\"></textarea> </div> <div class=\"modal-footer\"> <!--<button onclick=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"SubmitAlasanApprovalUrgentMemo()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-disabled=\"mApprovalUrgentMemo.ApprovalNote==undefined\">Simpan</button> <button ng-click=\"BatalAlasanApprovalUrgentMemo()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/checklistPDS/checklistPDS.html',
    "<script type=\"text/javascript\">$(\"#dropdownBtnOveride\").click(function() {\r" +
    "\n" +
    "        if ($('#dropdownContentOveride').css('display') == 'none') {\r" +
    "\n" +
    "            $('#dropdownContentOveride').show();\r" +
    "\n" +
    "        } else {\r" +
    "\n" +
    "            $('#dropdownContentOveride').hide();\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "\r" +
    "\n" +
    "    $(\"#button1\").click(function() {\r" +
    "\n" +
    "        if ($('#formData').css('display') == 'none') {\r" +
    "\n" +
    "            $('#formData').show();\r" +
    "\n" +
    "        } else {\r" +
    "\n" +
    "            $('#formData').hide();\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "\r" +
    "\n" +
    "    $('#myButton').click(function() {\r" +
    "\n" +
    "        $('#detail').modal();\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "     function washing() {\r" +
    "\n" +
    "        $('#myModal').modal('hide');\r" +
    "\n" +
    "        $('#signsales').modal();\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "     function problem() {\r" +
    "\n" +
    "         $('#problem1').show();\r" +
    "\n" +
    "        $('#problem').hide();\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    function myFunction() {\r" +
    "\n" +
    "        document.getElementById(\"demo\").style.color = \"white\";\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    function myButton(){\r" +
    "\n" +
    "        $('#myModal').modal();\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    function myFunction1() {\r" +
    "\n" +
    "        document.getElementById(\"demo1\").style.color = \"white\";\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    function OpenModal2() {\r" +
    "\n" +
    "\t\t\t $('#historydetail' ).show();\t//$('#ModalForm').modal('hide');\r" +
    "\n" +
    "            $('#myModal').modal('hide');\r" +
    "\n" +
    "           $('#formData2').hide();\r" +
    "\n" +
    "            //  $('#formData').(); \r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\tfunction Tidak_Clicked() {\r" +
    "\n" +
    "\t\t\t$('#ModalConfirmation').modal('hide'); \r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "    //function slide image ------------------------------------------------------------------------------------------------\r" +
    "\n" +
    "// var slideIndex = 1;\r" +
    "\n" +
    "// showDivs(slideIndex);\r" +
    "\n" +
    "\r" +
    "\n" +
    "// function plusDivs(n) {\r" +
    "\n" +
    "//   showDivs(slideIndex += n);\r" +
    "\n" +
    "// }\r" +
    "\n" +
    "\r" +
    "\n" +
    "// function currentDiv(n) {\r" +
    "\n" +
    "//   showDivs(slideIndex = n);\r" +
    "\n" +
    "// }\r" +
    "\n" +
    "\r" +
    "\n" +
    "// function showDivs(n) {\r" +
    "\n" +
    "//   var i;\r" +
    "\n" +
    "//   var x = document.getElementsByClassName(\"mySlides\");\r" +
    "\n" +
    "//   var dots = document.getElementsByClassName(\"demo\");\r" +
    "\n" +
    "//   if (n > x.length) {slideIndex = 1}    \r" +
    "\n" +
    "//   if (n < 1) {slideIndex = x.length}\r" +
    "\n" +
    "//   for (i = 0; i < x.length; i++) {\r" +
    "\n" +
    "//      x[i].style.display = \"none\";  \r" +
    "\n" +
    "//   }\r" +
    "\n" +
    "//   for (i = 0; i < dots.length; i++) {\r" +
    "\n" +
    "//      dots[i].className = dots[i].className.replace(\" w3-red\", \"\");\r" +
    "\n" +
    "//   }\r" +
    "\n" +
    "//   x[slideIndex-1].style.display = \"block\";  \r" +
    "\n" +
    "//   dots[slideIndex-1].className += \" w3-red\";\r" +
    "\n" +
    "// }</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer','tab1')\">Inspection Checklist</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer','tab2')\">History Inspeksi</a></li> </ul> <div id=\"tabContainer\" class=\"tab-content\"> <div id=\"tab1\" class=\"tab-pane fade in active\"> <div id=\"formData\"> <div class=\"row\" style=\"margin-left:0px;background-color:#e8eaed;padding:10px 5px 10px 10px\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride2\" onclick=\"navigateDropDown('dropdownContentOveride2')\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <span style=\"float:right\"><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></span> <span style=\"float:right;margin-right: 15px\"><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></span> </button> </div> <div class=\"w3-content\" style=\"max-width:800px\"> <img class=\"mySlides\" src=\"img_nature_wide.jpg\" style=\"width:100%\"> <img class=\"mySlides\" src=\"img_fjords_wide.jpg\" style=\"width:100%\"> <img class=\"mySlides\" src=\"img_mountains_wide.jpg\" style=\"width:100%\"> </div> <div class=\"row\" style=\"margin-left:0px;background-color:#e8eaed;padding:10px 5px 10px 10px\"> <!--<button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride2\" onclick=\"navigateDropDown('dropdownContentOveride2')\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white;\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r" +
    "\n" +
    "                <p style=\"font-size: 14px;\">Pemeriksaan Interior</p>\r" +
    "\n" +
    "                    <span style=\"float:right;\">In</span>\r" +
    "\n" +
    "                    <span style=\"float:right;margin-right: 15px;\">Out</span>\r" +
    "\n" +
    "\t\t\t\t  </button>--> <div class=\"row\"> <div class=\"col-xs-10\"> <p style=\"font-size: 14px\">Pemeriksaan Interior</p> </div> <div class=\"col-xs-1\"> <span style=\"float:right\">In</span> </div> <div class=\"col-xs-1\"> <span style=\"float:right;margin-right: 15px\">Out</span> </div>  </div> </div> </div> <div id=\"problem\"> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <th class=\"headerRow\">Pemeriksaan Interior</th> <th class=\"headerRow\">Ceklis</th> </tr> </thead> <tbody> <tr> <td>Kunci Kontak</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:green\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:black\"></i> </span> </td> </tr> <tr> <td>Central Door Lock</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:green\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:black\"></i> </span> </td> </tr> <tr> <td>Air Conditioner</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:black\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:red\"></i> </span> </td> </tr> <tr> <td>Plafon</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:green\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:black\"></i> </span> </td> </tr> <tr> <td>Lighter</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:green;size: 10px\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:black;size: 10px\"></i> </span> </td> </tr> </tbody> </table> <div style=\"margin-top:5px; text-align:center\"> <button id=\"saveBtn\" class=\"rbtn btn ng-binding ladda-button\"> Simpan</button> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\"> Kembali</button> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\"> Batalkan</button> </div> <hr> <footer class=\"footer navbar-bottom\" style=\"height:8%\"> <div class=\"row\"> <div class=\"col-xs-8\"> <p style=\"font-size: 17px;color: red\">Menemukan Problem?</p> </div> <div class=\"col-xs-2\" style=\"float: right\"> <button style=\"float: right\" onclick=\"problem()\"><i class=\"fa fa-chevron-circle-up\" aria-hidden=\"true\"></i></button> </div> </div> </footer> </div> <div id=\"problem1\" style=\"display: none\"> <div class=\"row\"> <div class=\"col-xs-8\"> <p style=\"font-size: 17px;color: red\">Menemukan Problem?</p> </div> <div class=\"col-xs-2\" style=\"float: right\"> <button style=\"float: right\"><i class=\"fa fa-chevron-circle-up\" aria-hidden=\"true\"></i></button> </div> </div><hr> <p style=\"font-size: 14px; font-weight: bold\"> Kategori Reparasi </p> <p> <ul class=\"list-inline\"> <li><input type=\"radio\" checked value=\"smallrepair\" name=\"repair&quot;\"> Small Repair</li> <li><input type=\"radio\" value=\"heavyrepair\" name=\"repair&quot;\"> Heavy Repair</li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Keterangan Kerusakan </p> <p> <ul class=\"list-inline\"> <li><textarea rows=\"4\" cols=\"50\">\r" +
    "\n" +
    "                            </textarea> </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Bukti Kserusakan </p> <div class=\"input-icon-right\"> <input type=\"file\" accept=\"image/*;capture=camera\"> </div> </div> </div> <div id=\"tab2\" class=\"tab-pane fade\" style=\"margin-top:10px; margin-left:2px\"> <div id=\"formData2\"> <p style=\"font-size: 14px; font-weight: bold\"> No Rangka </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">MHFC1JU41E5102209 </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Model </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Calya</li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Type </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">E M/T </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Warna </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Black </li> </ul> </p> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <th class=\"headerRow\">Tanggal Inspeksi</th> <th class=\"headerRow\">Staf PDS</th> </tr> </thead> <tbody> <tr> <td>10 Oktober 2016</td> <td>Dudi Surya<span><button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"\" type=\"button\" onclick=\"myButton()\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:right\"></span> </button></span></td> </tr> <tr> <td>11 Oktober 2016</td> <td>Ahmad Danie<span><button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:right\"></span> </button></span></td> </tr> <tr> <td>12 Oktober 2016</td> <td>Sundira<span><button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:right\"></span> </button></span></td> </tr> </tbody> </table> </div> <div id=\"historydetail\" style=\"display: none\"> <p style=\"font-size: 14px; font-weight: bold\"> Staff PDS </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Dedi Surya </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Tanggal Inspeksi </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">10 Oktober 2016</li> </ul> </p> <div class=\"row\" style=\"margin-left:0px;background-color:#e8eaed;padding:10px 5px 10px 10px\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride2\" onclick=\"navigateDropDown('dropdownContentOveride2')\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <span style=\"float:right\"><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></span> <span style=\"float:right;margin-right: 15px\"><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></span> </button> </div> <div class=\"w3-content\" style=\"max-width:800px\"> <img class=\"mySlides\" src=\"img_nature_wide.jpg\" style=\"width:100%\"> <img class=\"mySlides\" src=\"img_fjords_wide.jpg\" style=\"width:100%\"> <img class=\"mySlides\" src=\"img_mountains_wide.jpg\" style=\"width:100%\"> </div> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <th class=\"headerRow\">Pemeriksaan Interior</th> <th class=\"headerRow\">Ceklis</th> </tr> </thead> <tbody> <tr> <td>Kunci Kontak</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:green\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:black\"></i> </span> </td> </tr> <tr> <td>Central Door Lock</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:green\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:black\"></i> </span> </td> </tr> <tr> <td>Air Conditioner</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:black\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:red\"></i> </span> </td> </tr> <tr> <td>Plafon</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:green\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:black\"></i> </span> </td> </tr> <tr> <td>Lighter</td> <td> <i class=\"fa fa-check-circle-o\" style=\"color:green;size: 10px\"></i> <span style=\"margin-left: 10px\"> <i class=\"fa fa-times-circle-o\" style=\"color:black;size: 10px\"></i> </span> </td> </tr> </tbody> </table> <p style=\"font-size: 14px; font-weight: bold\"> Kategori Reparasi </p> <p> <ul class=\"list-inline\"> <li><input type=\"radio\" checked value=\"smallrepair\" name=\"repair&quot;\"> Small Repair</li> <li><input type=\"radio\" value=\"heavyrepair\" name=\"repair&quot;\"> Heavy Repair</li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Keterangan Kerusakan </p> <p> <ul class=\"list-inline\"> <li><textarea rows=\"4\" cols=\"50\">\r" +
    "\n" +
    "                            </textarea> </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Bukti Kserusakan </p> </div> </div> </div> <!--problem__________________________________________________________________________________________________________________________________--> <div class=\"modal fade\" id=\"myModal\" role=\"dialog\"> <div class=\"modal-content\" style=\"width:25%; margin-top:15%; margin-left:45%\"> <ul class=\"list-group\"> <a ng-click=\"LookSelected(da_data.SalesProgramName)\" onclick=\"OpenModal2()\"><li class=\"list-group-item\">Lihat Hasil Inspeksi</li></a> </ul> </div> </div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/checklistPDS/daftarUnitMasuk11.html',
    "<script type=\"text/javascript\">$(\"#dropdownBtnOveride\").click(function(){\r" +
    "\n" +
    "\tif($('#dropdownContentOveride').css('display') == 'none')\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride').show();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\telse\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride').hide();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "});\r" +
    "\n" +
    "\r" +
    "\n" +
    "$(\"#button1\").click(function(){\r" +
    "\n" +
    "\tif($('#formData').css('display') == 'none')\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#formData').show();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\telse\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#formData').hide();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "});\r" +
    "\n" +
    "\r" +
    "\n" +
    "$( '#myButton' ).click( function() {\r" +
    "\n" +
    "    $( '#myModal' ).modal();\r" +
    "\n" +
    "  });\r" +
    "\n" +
    "\r" +
    "\n" +
    "  function myFunction() {\r" +
    "\n" +
    "    document.getElementById(\"demo\").style.color = \"white\";\r" +
    "\n" +
    "}\r" +
    "\n" +
    "function myFunction1() {\r" +
    "\n" +
    "    document.getElementById(\"demo1\").style.color = \"white\";\r" +
    "\n" +
    "}</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"col-xs-12\"> <button id=\"button1\" type=\"button\" class=\"rbtn btn ng-binding ladda-button fa fa-search\" style=\"padding:5px;float:right;background-color:#d53337\"></button> </div> <br> <div id=\"formData\" style=\"display:none\"> <div class=\"row filtersearch\" style=\"margin-right:0px;padding-bottom:5px\"> <div class=\"col-xs-5\" style=\"padding-right:0px;width:auto\"> <div class=\"input-group\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" style=\"margin-left:10px;padding-bottom:10px\" class=\"form-control ng-pristine ng-valid ng-empty ng-touched\" placeholder=\"Filter\" ng-model=\"gridApi.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn dropdown\" class=\"col-xs-5\" uib-dropdown=\"\" style=\"width:auto\"> <!-- <div class=\"col-xs-5\"> --> <button id=\"filter\" type=\"button\" class=\"btn wbtn ng-binding dropdown-toggle\" uib-dropdown-toggle=\"\" data-toggle=\"dropdown\" tabindex=\"-1\" aria-haspopup=\"true\" aria-expanded=\"false\">Nama Pelanggan&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu=\"\" role=\"menu\" aria-labelledby=\"filter\"> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">Branch Tujuan<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[2].filters[0].term!='' &amp;&amp;gridApi.grid.columns[2].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">No SPK<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[3].filters[0].term!='' &amp;&amp;gridApi.grid.columns[3].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">No PO<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[3].filters[0].term!='' &amp;&amp;gridApi.grid.columns[3].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">No Rangka<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[3].filters[0].term!='' &amp;&amp;gridApi.grid.columns[3].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">Janji Pengiriman<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[3].filters[0].term!='' &amp;&amp;gridApi.grid.columns[3].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">Status<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[3].filters[0].term!='' &amp;&amp;gridApi.grid.columns[3].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> </ul> </div> <!--</div>--> <div class=\"col-xs-2\"> <button style=\"padding:5px;margin:left\">Cari</button> </div> </div> </div> </div> </div> <div class=\"listData\"> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('formData')\">Daftar Unit Masuk</a></li> <li class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('formData1')\">Pendataan GR</a></li> <!--<li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('menuProspect');\">Prospect</a></li>\r" +
    "\n" +
    "\t\t<li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('summaryFormTemplate');\">Summary Form Template</a></li>--> </ul> <div id=\"tabContainer\" class=\"tab-content\"> <div id=\"formData\" class=\"tab-pane fade in active\"> <div class=\"row\" style=\"margin-left:0px;background-color:#e8eaed;padding:10px 5px 10px 10px\"> <table class=\"col-xs-12\" style=\"table-layout: fixed;margin-right:5px;padding:10px 5px 10px 10px\"> <tr style=\"margin-bottom:5px\"> <th style=\"width:35%;word-wrap:break-word;padding-left:10px\"><p style=\"font-size: 12px\">Nama Pelanggan<br>Owner Branch<br>No PO<br>No SPK<br>No Rangka</p></th> <th style=\"width:21%;word-wrap:break-word\"><p style=\"font-size: 12px\">Schedule Penerimaan Dari PDC/ Branch Lain</p></th> <th style=\"width:22%;word-wrap:break-word\"><p style=\"font-size: 12px\">janji Pengiriman</p></th> <th style=\"width:17%;word-wrap:break-word\"><p style=\"font-size: 12px\">status</p></th> <th style=\"width:10%;word-wrap:break-word\"></th> </tr></table> </div> <div class=\"list-group-item\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">Peggy Carter </div> <div class=\"row\" style=\"margin-left:0px;background-color:#e8eaed\"> <table class=\"col-xs-12\" style=\"table-layout: fixed;margin-right:5px;padding:10px 5px 10px 10px\"> <th style=\"width:35%;word-wrap:break-word\"><p style=\"font-size: 12px\">Auto 2000 Gading<br>PO 12456<br>MHFC1JU41E510220!</p></th> <th style=\"width:21%;word-wrap:break-word\"><p style=\"font-size: 12px\">28 Sep 2016 09:00</p> </th> <th style=\"width:22%;word-wrap:break-word\"><p style=\"font-size: 12px\">02 Oktober 2016 09:00 </p></th> <th style=\"width:17%;word-wrap:break-word\"><p style=\"font-size: 12px\">Washing Drying </p></th> <th style=\"width:10%\"><button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:right\"></span> </button></th> </table> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">Maria Hill </div> <div class=\"row\" style=\"margin-left:0px;background-color:#e8eaed\"> <table class=\"col-xs-12\" style=\"table-layout: fixed;margin-right:5px;padding:10px 5px 10px 10px\"> <th style=\"width:35%;word-wrap:break-word\"><p style=\"font-size: 12px\">Auto 2000 Gading<br>PO 12456<br>MHFC1JU41E510220!</p></th> <th style=\"width:21%;word-wrap:break-word\"><p style=\"font-size: 12px\">28 Sep 2016 09:00 </p></th> <th style=\"width:22%;word-wrap:break-word\"><p style=\"font-size: 12px\">02 Oktober 2016 09:00 </p></th> <th style=\"width:17%;word-wrap:break-word\"><p style=\"font-size: 12px\">Washing Drying </p> </th> <th style=\"width:10%\"><button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:right\"></span> </button></th> </table> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">Sam Wilson  </div> <div class=\"row\" style=\"margin-left:0px;background-color:#e8eaed\"> <table class=\"col-xs-12\" style=\"table-layout: fixed;margin-right:5px;padding:10px 5px 10px 10px\"> <th style=\"width:35%;word-wrap:break-word\"><p style=\"font-size: 12px\">Auto 2000 Gading<br>PO 12456<br>MHFC1JU41E510220!</p></th> <th style=\"width:21%;word-wrap:break-word\"><p style=\"font-size: 12px\">28 Sep 2016 09:00 </p></th> <th style=\"width:22%;word-wrap:break-word\"><p style=\"font-size: 12px\">02 Oktober 2016 09:00 </p></th> <th style=\"width:17%;word-wrap:break-word\"><p style=\"font-size: 12px\">Washing Drying </p></th> <th style=\"width:10%\"><button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:right\"></span> </button></th> </table> </div> </div> </div> <div class=\"modal fade\" id=\"myModal\" role=\"dialog\"> <!-- Modal content--> <div class=\"modal-content\" style=\"width:25%; margin-top:15%; margin-left:45%\"> <ul class=\"list-group\"> <a ng-click=\"LookSelected(da_data.SalesProgramName)\"><li class=\"list-group-item\">Lihat Detail</li></a> <a ng-click=\"ChangeSelected(da_data.SalesProgramId,da_data.SalesProgramName)\"><li class=\"list-group-item\">Ubah Detail</li></a> <a ng-click=\"DeleteSelected(da_data.SalesProgramId)\"><li class=\"list-group-item\">Hapus Detail</li></a> </ul> </div> </div> <div id=\"formData1\" class=\"tab-pane fade\" style=\"margin-top:10px; margin-left:2px\"> <div id=\"headerPenerimaan\"> <p style=\"font-size: 14px; font-weight: bold\"> Nama Pelanggan </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Adrian Ball </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Owner Branch </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Auto 2000 Cempaka Putih</li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> No. PO </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">PO 123123123 </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> No. SPK </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">SPK 123123123 </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> No Rangka </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">MHFC1JU41E5102209</li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Tipe </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">E M/T </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Warna </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Black </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Schedule Penerimaan dari PDC/Branch Lain </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">15 Agt 2016 09.00 </li> </ul> </p> <p style=\"font-size: 14px; font-weight: bold\"> Sudah Terima SPPDC? </p> <p> <ul class=\"list-inline\"> <input type=\"checkbox\" name=\"vehicle\" value=\"Bike\">Yes<br> </ul> </p> <div class=\"container\" style=\"margin-top:5px; text-align:center;float:right\"> <button id=\"saveBtn\" class=\"rbtn btn ng-binding ladda-button\"> Batalkan</button> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\">Simpan</button> </div> </div> </div></div></div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/daftarUnitMasuk/daftarUnitMasuk.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "        $('body .modals').remove();\r" +
    "\n" +
    "    }</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .box-show-setup,\r" +
    "\n" +
    "    .box-hide-setup {\r" +
    "\n" +
    "        -webkit-transition: all linear 0.3s;\r" +
    "\n" +
    "        -moz-transition: all linear 0.3s;\r" +
    "\n" +
    "        -ms-transition: all linear 0.3s;\r" +
    "\n" +
    "        -o-transition: all linear 0.3s;\r" +
    "\n" +
    "        transition: all linear 0.3s;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "        -webkit-border-radius: 4px;\r" +
    "\n" +
    "        -moz-border-radius: 4px;\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "        border: 0px;\r" +
    "\n" +
    "        background-color: transparent;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .adv-srch {\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        margin-bottom: 0px;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        background-color: rgba(213, 51, 55, 0.02);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .adv-srch-on {\r" +
    "\n" +
    "        padding-top: 4px !important;\r" +
    "\n" +
    "        padding: 6px;\r" +
    "\n" +
    "        margin: 0 3px 0 0;\r" +
    "\n" +
    "        margin-bottom: -5px !important;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        border-bottom-style: none !important;\r" +
    "\n" +
    "        background-color: #FEFBFB;\r" +
    "\n" +
    "        border-top-left-radius: 3px;\r" +
    "\n" +
    "        border-top-right-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .colorsrch {\r" +
    "\n" +
    "        color: red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .modalpadding {\r" +
    "\n" +
    "        padding-top: 0px !important;\r" +
    "\n" +
    "        padding-right: 0px !important;\r" +
    "\n" +
    "        padding-bottom: 0px !important;\r" +
    "\n" +
    "        padding-left: 0px !important;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /*@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin-top: 10px !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .noppadingFormGroup {\r" +
    "\n" +
    "        margin-bottom: 0px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .ScrollStyle {\r" +
    "\n" +
    "        max-height: 600px;\r" +
    "\n" +
    "        overflow-y: scroll;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .item-modal {\r" +
    "\n" +
    "        border: none;\r" +
    "\n" +
    "        padding-bottom: 15px;\r" +
    "\n" +
    "        border-bottom: 1px solid #ddd !important;\r" +
    "\n" +
    "        margin: 10px 30px 10px 30px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .mobileAddBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1!important;\r" +
    "\n" +
    "        font-size: 15px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .mobileFilterBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1!important;\r" +
    "\n" +
    "        font-size: 13px;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        float: right;\r" +
    "\n" +
    "        /*background-color: silver;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .filterMarg {\r" +
    "\n" +
    "        margin-bottom: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media only screen and (max-width: 430px) {\r" +
    "\n" +
    "        .iconFilter {\r" +
    "\n" +
    "            margin-left: -4px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -28px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        .iconFilter {\r" +
    "\n" +
    "            margin-left: -4px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #desktopderectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: block !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media (min-width: 501px) and (max-width: 600px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -36px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #desktopderectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: block !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media (min-width: 601px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -33px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .mobilesearchBtn {\r" +
    "\n" +
    "        text-decoration: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media (max-width: 350px) {\r" +
    "\n" +
    "        .bsbreadcrumb .bsbreadcrumb-path,\r" +
    "\n" +
    "        .bsbreadcrumb i {\r" +
    "\n" +
    "            display: block !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /*Modal*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .vertical-alignment-helper {\r" +
    "\n" +
    "        display: table;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "        /* This makes sure that we can still click outside of the modal to close it */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .vertical-align-center {\r" +
    "\n" +
    "        /* To center vertically */\r" +
    "\n" +
    "        display: table-cell;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .modal-content {\r" +
    "\n" +
    "        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */\r" +
    "\n" +
    "        width: inherit;\r" +
    "\n" +
    "        height: inherit;\r" +
    "\n" +
    "        /* To center horizontally */\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        pointer-events: all;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @-webkit-keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            -webkit-transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-hide=\"daftarUnitMasuk\"> <bsform-mobile factory-name=\"DaftarUnitMasukNewFactory\" bind-data=\"bind_data\" info-totaldata=\"true\" enable-listtoggle=\"false\" filter-data=\"filterData\" enable-addbutton=\"disable\" custom-modallihat=\"true\" custom-modalubah=\"true\" custom-modalhapus=\"true\" enable-listtoggle=\"false\" info-totaldata=\"false\" service-enable=\"true\" updated-data=\"updated_data\" selected-data=\"selected_data\" form-name=\"DaftarUnitMasukForm\" list-title=\"Daftar Unit Masuk\" row-template-url=\"app/sales/sales4/deliveryRequest/daftarUnitMasuk/rowTemplate.html\"> <bsform-mobilemodal> <div ng-if=\"selected_data.StatusDRName=='Washing & Drying'\"> <a ng-click=\"selesaiWashingDryingFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span> Selesai Washing & Drying</li> </a> <a ng-click=\"historyInspeksiFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>History Inspeksi</li> </a> <a ng-click=\"DetailFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat Detail</li> </a> </div> <div ng-if=\"selected_data.StatusDRName=='Repair Selesai'\"> <a ng-click=\"mulaiInspeksiFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-pencil\" style=\"margin-right: 15px\"></i> </span>Mulai Inspeksi</li> </a> <a ng-click=\"historyInspeksiFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>History Inspeksi</li> </a> <a ng-click=\"DetailFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat Detail</li> </a> </div> <div ng-if=\"selected_data.StatusDRName=='Siap Pesta'\"> <a ng-click=\"historyInspeksiFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>History Inspeksi</li> </a> <a ng-click=\"DetailFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat Detail</li> </a> </div> <div ng-if=\"selected_data.StatusDRName=='Gate In'\"> <a ng-click=\"goodReceivedFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Good Received</li> </a> </div> <div ng-if=\"selected_data.StatusDRName=='Good Received'\"> <a ng-click=\"mulaiInspeksiFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-pencil\" style=\"margin-right: 15px\"></i> </span>Mulai Inspeksi</li> </a> <a ng-click=\"DetailFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat Detail</li> </a> </div> <div ng-if=\"selected_data.StatusDRName=='Washing & Drying Selesai'\"> <a ng-click=\"historyInspeksiFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>History Inspeksi</li> </a> <a ng-click=\"DetailFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat Detail</li> </a> </div> <div ng-if=\"selected_data.StatusDRName=='Mulai Inspeksi'\"> <a ng-click=\"mulaiInspeksiFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-pencil\" style=\"margin-right: 15px\"></i> </span>Mulai Inspeksi</li> </a> <a ng-click=\"historyInspeksiFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>History Inspeksi</li> </a> <a ng-click=\"DetailFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat Detail</li> </a> </div> <div ng-if=\"selected_data.StatusDRName=='Repair'\"> <a ng-click=\"selesaiRepairFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-wrench\" style=\"margin-right: 15px\"></i> </span> Repair Selesai</li> </a> <a ng-click=\"historyInspeksiFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>History Inspeksi</li> </a> <a ng-click=\"DetailFunction(selected_data)\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat Detail</li> </a> </div> </bsform-mobilemodal> </bsform-mobile> <div class=\"ui modal ModalSelesaiWashingDrying\" style=\"background-color: transparent\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\"> <h4 class=\"modal-title\">Washing & Drying</h4> </div> <div class=\"modal-body\"> <p> <b> Unit Sudah Selesai Washing & Drying ? </b> </p> </div> <div class=\"modal-footer\"> <button ng-click=\"Ya_Clicked(selected_data)\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"Tidak_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div> </div> </div> <div ng-show=\"goodReceivedForm\"> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"SimpanGoodReceived(selected_data)\">Simpan</button> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"batalkanGoodReceived()\"> Kembali</button> <p> <br> </p> <p> <br> </p> <p> <b>No. Rangka <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.FrameNo}} </p> <p> <b>Model <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.VehicleModelName}} </p> <p> <b>Tipe <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.Description}} </p> <p> <b>Warna <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.ColorName}} </p> <p> <b>No. SPK <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.FormSPKNo}} </p> <p> <b>Nama Pelanggan <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.NameCustomer}} </p> <p ng-if=\"selected_data.POCode != null\"> <b>No. PO <br> </b> </p> <p style=\"color: #888b91\" ng-if=\"selected_data.POCode != null\">{{selected_data.POCode}} </p> <p ng-if=\"selected_data.OwnerBranch != null\"> <b>Cabang Pemilik <br> </b> </p> <p ng-if=\"selected_data.OwnerBranch != null\" style=\"color: #888b91\">{{selected_data.OwnerBranch}} </p> <p ng-if=\"selected_data.POStatus != 'Swapping'\"> <b>Schedule Penerimaan dari PDC <br> </b> </p> <p style=\"color: #888b91\" ng-if=\"selected_data.POStatus != 'Swapping'\">{{selected_data.EstimatePersiapanPDC| date:\"dd MMMM yyyy\"}} {{selected_data.EstimatePersiapanPDC | date:\"HH:mm\"}} </p> <p ng-if=\"selected_data.POStatus == 'Swapping'\"> <b>Schedule Penerimaan dari Branch Lain <br> </b> </p> <p style=\"color: #888b91\" ng-if=\"selected_data.POStatus == 'Swapping'\">{{selected_data.EstimateBranchReceivedDate| date:\"dd MMMM yyyy\"}} {{selected_data.EstimateBranchReceivedDate | date:\"HH:mm\"}} </p> </div> <div ng-show=\"mulaiInspeksiForm\"> <div ng-show=\"inputNonProblem\"> <div id=\"groupAtas\"> <button ng-if=\"step == 10\" id=\"saveBtn\" class=\"rbtn btn\" style=\"float: right\" ng-click=\"SimpanDetailChecklist(selected_data)\"> Simpan</button> <button ng-if=\"step != 10\" id=\"sendEmailBtn\" ng-click=\"nextStep()\" class=\"rbtn btn\" style=\"float: right\">Lanjutkan</button> <button ng-if=\"step == 1\" id=\"saveBtn\" class=\"wbtn btn\" style=\"float: right\" ng-click=\"batalkanmulaiInspeksiFunction()\"> Batalkan</button> <button ng-if=\"step != 1\" id=\"saveBtn\" class=\"wbtn btn\" style=\"float: right\" ng-click=\"kembaliStep()\"> Kembali</button> </div> <p> <br> </p> <p> <br> </p> <div id=\"groupTengah\"> <div ng-hide=\"step01\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 1 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step02\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 2 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/samping.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist2.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist2.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step03\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 3 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist3.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist3.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step04\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 4 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist4.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist4.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step05\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 5 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist5.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist5.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step06\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 6 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist6.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist6.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step07\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 7 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist7.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist7.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step08\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 8 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist8.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist8.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step09\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 9 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist9.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist9.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step10\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 10 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist10.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist10.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> </div> <div id=\"groupBawah\"> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" ng-disabled=\"true\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> {{DetailCeklist.GroupChecklistInspectionName}} </button> <div class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-repeat=\"item in DetailCeklist.ListItemInspection\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleChecked(item);\"> <div class=\"row\"> <div class=\"row\"> <div class=\"col-xs-11\"> <h7 style=\"float: left\">{{item.ChecklistItemInspectionName}}</h7> </div> <div class=\"col-xs-1\"> <span style=\"float:right\"> <i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.checked == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i> </span> </div> </div> </div> </div> </div> </div> <p style=\"text-align: right\"> <a ng-click=\"historyInspeksiFunctionDariInspectionChecklist()\">History Inspeksi</a> </p> <p> <br> </p> <p> <br> </p> <p> <hr> </p> <p> <b style=\"color: red\">Menemukan Problem ?</b> <span style=\"float:right\"> <i ng-click=\"modalInputProblem()\" class=\"fa fa-chevron-circle-up fa-2x fa-lg\" aria-hidden=\"true\" style=\"border: none;background-color: transparent;display: inline-block\"></i> </span> </p> </div> </div> <div ng-show=\"inputProblem\"> <p> <b style=\"color: red\">Menemukan Problem ?</b> <span style=\"float:right\"> <i ng-click=\"batalInputProblem(InputDataProblem)\" class=\"fa fa-chevron-circle-down fa-2x fa-lg\" aria-hidden=\"true\" style=\"border: none;background-color: transparent;display: inline-block\"></i> </span> </p> <p> <hr> </p> <p> <b> Kategori Reparasi </b> </p> <p> <input type=\"radio\" ng-model=\"InputDataProblem.CategoryReparasiId\" name=\"RadioKategoriReparasi\" ng-value=\"1\">Perbaikan Kecil <input type=\"radio\" ng-model=\"InputDataProblem.CategoryReparasiId\" name=\"RadioKategoriReparasi\" ng-value=\"2\">Perbaikan Besar </p> <p> <b> Keterangan Kerusakan </b> </p> <p> <textarea ng-model=\"InputDataProblem.KeteranganKerusakan\" name=\"keteranganKerusakan\" rows=\"10\" cols=\"50\" placeholder=\"Keterangan Kerusakan...\" maxlength=\"250\"></textarea> </p> <p> <input type=\"checkbox\" ng-model=\"InputDataProblem.NeedInsuranceBit\"> <b>Perlu Asuransi</b> </p> <p> <b> Tanggal Keluar </b> </p> <bsdatepicker ng-model=\"InputDataProblem.OutDate\" ng-model-options=\"{timezone: 'utc'}\" name=\"tanggalKeluar\" date-options=\"dateOptions\"></bsdatepicker> <p> <b> Estimasi Tanggal Kembali </b> </p> <bsdatepicker ng-model=\"InputDataProblem.BackDateEstimated\" ng-model-options=\"{timezone: 'utc'}\" name=\"estimasiTanggalKembali\" date-options=\"dateOptions\"></bsdatepicker> <p style=\"margin-top:10px\"> <b> Bukti Kerusakan </b> </p> <image-control max-upload=\"10\" is-image=\"1\" img-upload=\"uploadFiles\"> <form-image-control> <input type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> </form-image-control> </image-control> <p> <br> </p> </div> </div> <div ng-show=\"LihatInspeksiForm\"> <div id=\"groupAtas\"> <button ng-if=\"step2 != 10\" id=\"sendEmailBtn\" ng-click=\"nextStep2()\" class=\"rbtn btn\" style=\"float: right\">Lanjutkan</button> <button ng-if=\"step2 == 1\" id=\"saveBtn\" class=\"wbtn btn\" style=\"float: right\" ng-click=\"batalkanlihatInspeksiFunction()\"> Batalkan</button> <button ng-if=\"step2 == 10\" id=\"saveBtn\" class=\"wbtn btn\" style=\"float: right\" ng-click=\"batalkanlihatInspeksiFunction()\"> Keluar</button> <button ng-if=\"step2 != 1\" id=\"saveBtn\" class=\"wbtn btn\" style=\"float: right\" ng-click=\"kembaliStep2()\"> Kembali</button> </div> <p> <br> </p> <p> <br> </p> <div> <p> <b>STAFF PDS</b> </p> <p> {{History.EmployeeName}} </p> <p> <b>Tanggal Inspeksi</b> </p> <p> {{History.InspectionDate | date:\"dd/MM/yyyy\"}} </p> </div> <div id=\"groupTengah\"> <div ng-hide=\"step201\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 1 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step202\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 2 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/samping.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist2.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist2.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step203\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 3 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist3.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist3.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step204\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 4 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist4.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist4.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step205\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 5 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist5.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist5.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step206\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 6 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist6.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist6.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step207\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 7 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist7.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist7.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step208\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 8 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist8.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist8.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step209\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 9 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist9.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist9.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> <div ng-show=\"step210\"> <div style=\"font-size:14px; background-color: gray; color: black\"> <p style=\"margin-left: 10px\"> <b>Step 10 dari 10</b> </p> </div> <p style=\"text-align:center\"> <!-- <img src=\"../images/app/dr/depan.jpg\" alt=\"Tampak Depan\" align=\"middle\"> --> <img ng-src=\"{{imageChecklist10.viewdocument}}\" ng-style=\"gambar\"> <div class=\"row\" ng-if=\"imageChecklist10.viewdocument==null\"> <p style=\"text-align: center;font-size: 16px\"> No Image. </p> </div> </p> </div> </div> <div id=\"groupBawah\"> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" ng-disabled=\"true\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> {{DetailCeklist2.GroupChecklistInspectionName}} </button> <div class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-repeat=\"item in DetailCeklist2.ListItemInspection\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item.ChecklistItemInspectionName}}</h7> <span style=\"float:right\"> <i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.checked == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i> </span> </div> </div> </div> </div> </div> </div> <div> <p> <hr> </p> <p> <b> Kategori Reparasi </b> </p> <p> <input type=\"radio\" ng-disabled=\"true\" ng-model=\"DataLihatDetailDaftarUnitMasuk.CategoryReparasiId\" name=\"RadioKategoriReparasiLihat\" ng-value=\"1\">Perbaikan Kecil <input type=\"radio\" ng-disabled=\"true\" ng-model=\"DataLihatDetailDaftarUnitMasuk.CategoryReparasiId\" name=\"RadioKategoriReparasiLihat\" ng-value=\"2\">Perbaikan Besar </p> <p> <b> Keterangan Kerusakan </b> </p> <p> <textarea ng-disabled=\"true\" ng-model=\"DataLihatDetailDaftarUnitMasuk.KeteranganKerusakan\" name=\"keteranganKerusakan\" rows=\"10\" cols=\"50\" maxlength=\"250\" placeholder=\"Keterangan Kerusakan...\"></textarea> </p> <p> <input ng-disabled=\"true\" type=\"checkbox\" ng-model=\"DataLihatDetailDaftarUnitMasuk.NeedInsuranceBit\"> <b>Perlu Asuransi</b> </p> <p> <b> Tanggal Keluar </b> </p> <bsdatepicker ng-disabled=\"true\" ng-model=\"DataLihatDetailDaftarUnitMasuk.OutDate\" name=\"tanggalKeluar\" date-options=\"dateOptions\"></bsdatepicker> <p> <b> Estimasi Tanggal Kembali </b> </p> <bsdatepicker ng-disabled=\"true\" ng-model=\"DataLihatDetailDaftarUnitMasuk.BackDateEstimated\" name=\"estimasiTanggalKembali\" date-options=\"dateOptions\"></bsdatepicker> <p style=\"margin-top:10px\"> <b> Bukti Kerusakan </b> </p> <!-- <image-control max-upload=10 is-image=1 img-upload=\"uploadFiles\">\r" +
    "\n" +
    "            <form-image-control>\r" +
    "\n" +
    "                <input type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1;\">\r" +
    "\n" +
    "            </form-image-control>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        </image-control> --> <div class=\"row\" style=\"margin: 10px 15px 0 0\"> <table class=\"table table-responsive\"> <!-- <thead>\r" +
    "\n" +
    "                    <tr>\r" +
    "\n" +
    "                        <th class=\"headerRow\">No.</th>\r" +
    "\n" +
    "                        <th class=\"headerRow\">Bukti</th>\r" +
    "\n" +
    "                        <th class=\"headerRow\"></th>\r" +
    "\n" +
    "                    </tr>\r" +
    "\n" +
    "                </thead> --> <tbody> <tr ng-repeat=\"list in BuktiKerusakan\"> <td>{{$index+1}}.&nbsp;&nbsp; <a ng-click=\"viewImage(list)\">{{list.PhotoName}}</a> </td> <!-- <td><a ng-click=\"viewImage(list)\">View</a></td> --> </tr> </tbody> </table> </div> <p> <br> </p> </div> </div> <div ng-show=\"historyInspeksi\"> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"batalkanHistoryInspeksi()\"> Kembali</button> <p> <br> </p> <p> <b>No. Rangka <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.FrameNo}} </p> <p> <b>Model <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.VehicleModelName}} </p> <p> <b>Tipe <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.Description}} </p> <p> <b>Warna <br> </b> </p> <p style=\"color: #888b91\">{{selected_data.ColorName}} </p> <p> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridHistoryInspeksi\" ui-grid-auto-resize></div> </p> </div> <div class=\"ui modal viewImageDR\"> <div> <div class=\"modal-body\"> <img ng-src=\"{{image.viewdocument}}\" ng-style=\"gambar\"> <p align=\"center\" style=\"margin: 15px 0px 0px 0px !important\">{{image.viewdocumentName}}</p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <button ng-click=\"keluarDokumen()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Kembali</button> </div> </div> </div> </div> <div class=\"ui modal ModalSelesaiRepair\" style=\"background-color: transparent\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\"> <h4 class=\"modal-title\">Repair Selesai</h4> </div> <div class=\"modal-body\"> <p> <b> Unit sudah selesai repair ? </b> </p> </div> <div class=\"modal-footer\"> <button ng-click=\"Ya_ClickedRepair(selected_data)\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"Tidak_ClickedRepair()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div> </div> <div class=\"ui modal loadingImgDR\" style=\"height:200px; background-color:transparent; box-shadow: none !important\"> <div align=\"center\" class=\"list-group-item\" style=\"border: none !important;padding:10px 0px 10px 0px; background-color:transparent\"> <font color=\"white\"> <i class=\"fa fa-spinner fa-spin fa-3x fa-lg\"></i>  <b> Loading.... </b> </font> </div> </div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/daftarUnitMasuk/rowTemplate.html',
    "<!--Harus item--> <div style=\"font-size: 12px\"> <b ng-if=\"item.POStatus != 'Swapping'\">Nama Pelanggan : {{item.NameCustomer}}</b> <b ng-if=\"item.POStatus == 'Swapping'\">Owner Branch : {{item.OwnerBranch}}</b> <button ng-click=\"openModal(item)\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:transparent; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:10px\"> <li ng-if=\"item.FormSPKNo != null\"><b>No. SPK : </b>{{item.FormSPKNo}}</li> <li ng-if=\"item.POStatus != 'Swapping'\"><b>Jadwal Penerimaan dari PDC : </b>{{item.EstimatePersiapanPDC| date:\"dd/MM/yyyy\"}} {{item.EstimatePersiapanPDC | date:\"HH:mm\"}}</li> <li ng-if=\"item.POStatus == 'Swapping'\"><b>Jadwal Penerimaan dari Branch Lain : </b>{{item.EstimateBranchReceivedDate| date:\"dd/MM/yyyy\"}} {{item.EstimateBranchReceivedDate | date:\"HH:mm\"}}</li> <li><b>Janji Pengiriman : </b>{{item.SentUnitDate| date:\"dd/MM/yyyy\"}} {{item.SentUnitTime | date:\"HH:mm\"}}</li> <li><b>Status : </b>{{item.StatusDRName}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:10px\"> <li ng-if=\"item.POCode != null\"><b>No. PO : </b>{{item.POCode}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:10px\"> <li ng-if=\"item.TOCode != null\"><b>No. TO : </b>{{item.TransferStockNo}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:10px\"> <li><b>No. Rangka : </b>{{item.FrameNo}}</li> </ul> </p>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/daftarUnitRepair/daftarUnitRepair.html',
    "<script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "        -webkit-border-radius: 4px;\r" +
    "\n" +
    "        -moz-border-radius: 4px;\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "        border: 0px;\r" +
    "\n" +
    "        background-color: transparent;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .adv-srch {\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        margin-bottom: 0px;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        background-color: rgba(213, 51, 55, 0.02);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .adv-srch-on {\r" +
    "\n" +
    "        padding-top: 4px !important;\r" +
    "\n" +
    "        padding: 6px;\r" +
    "\n" +
    "        margin: 0 3px 0 0;\r" +
    "\n" +
    "        margin-bottom: -5px !important;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        border-bottom-style: none !important;\r" +
    "\n" +
    "        background-color: #FEFBFB;\r" +
    "\n" +
    "        border-top-left-radius: 3px;\r" +
    "\n" +
    "        border-top-right-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .colorsrch {\r" +
    "\n" +
    "        color: red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .modalpadding {\r" +
    "\n" +
    "        padding-top: 0px !important;\r" +
    "\n" +
    "        padding-right: 0px !important;\r" +
    "\n" +
    "        padding-bottom: 0px !important;\r" +
    "\n" +
    "        padding-left: 0px !important;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin-top: 10px !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .noppadingFormGroup {\r" +
    "\n" +
    "        margin-bottom: 0px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .ScrollStyle {\r" +
    "\n" +
    "        max-height: 400px;\r" +
    "\n" +
    "        overflow-y: scroll;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .item-modal {\r" +
    "\n" +
    "        border: none;\r" +
    "\n" +
    "        padding-bottom: 15px;\r" +
    "\n" +
    "        border-bottom: 1px solid #ddd !important;\r" +
    "\n" +
    "        margin: 10px 30px 10px 30px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobileAddBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1!important;\r" +
    "\n" +
    "        font-size: 15px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobileFilterBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1!important;\r" +
    "\n" +
    "        font-size: 13px;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        float: right;\r" +
    "\n" +
    "        /*background-color: silver;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .filterMarg {\r" +
    "\n" +
    "        margin-bottom: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media only screen and (max-width: 430px) {\r" +
    "\n" +
    "        .iconFilter {\r" +
    "\n" +
    "            margin-left: -4px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -28px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (min-width: 501px) and (max-width: 600px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -36px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (min-width: 601px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -33px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (max-width: 600px) {\r" +
    "\n" +
    "        .inputForm {\r" +
    "\n" +
    "            margin: 45px 0 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobilesearchBtn {\r" +
    "\n" +
    "        text-decoration: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (max-width: 350px) {\r" +
    "\n" +
    "        .bsbreadcrumb .bsbreadcrumb-path,\r" +
    "\n" +
    "        .bsbreadcrumb i {\r" +
    "\n" +
    "            display: block !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*Modal*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .vertical-alignment-helper {\r" +
    "\n" +
    "        display: table;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "        /* This makes sure that we can still click outside of the modal to close it */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .vertical-align-center {\r" +
    "\n" +
    "        /* To center vertically */\r" +
    "\n" +
    "        display: table-cell;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .modal-content {\r" +
    "\n" +
    "        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */\r" +
    "\n" +
    "        width: inherit;\r" +
    "\n" +
    "        height: inherit;\r" +
    "\n" +
    "        /* To center horizontally */\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        pointer-events: all;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @-webkit-keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            -webkit-transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"show.grid\"> <bsform-grid ng-form=\"DaftarUnitRepairForm\" factory-name=\"DaftarUnitRepairFactory\" model=\"mDaftarUnitRepair\" model-id=\"RepairId\" show-advsearch=\"on\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" hide-new-button=\"true\" form-name=\"DaftarUnitRepairForm\" form-title=\"Daftar Unit Repair\" modal-title=\"Daftar Unit Repair\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status Unit</label> <bsselect name=\"model\" ng-model=\"filter.StatusUnit\" data=\"StatusUnitRes\" ng-change=\"filterStatus(filter.StatusUnit)\" item-text=\"ReparationName\" item-value=\"ReparationId\" placeholder=\"Pilih Status Unit\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tanggal Pengiriman</label> <bsdatepicker name=\"tanggal\" ng-model=\"filter.TanggalPengiriman\" date-options=\"dateOptions\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div class=\"row\" style=\"margin: 0 0 0 0\" ng-show=\"show.inputProblem\"> <button style=\"margin: -30px 0 0 5px; float: right\" ng-click=\"printUpdate()\" class=\"rbtn btn\">Print Pengajuan</button> <button style=\"margin: -30px 0 0 0; float: right\" ng-click=\"kembali()\" class=\"wbtn btn\">Kembali</button> <div class=\"row\" style=\"margin: 30px 0 0 0\"> <div class=\"col-md-4\"> <label><b>Nama Pelanggan / Cabang Pemilik : </b> </label> <p>{{mFollowup.NamaPelanggan}}</p> </div> </div> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-md-4\"> <label><b>No. SPK / No. PO</b></label> <p>{{mFollowup.FormSPKNo}}</p> </div> <div class=\"col-md-4\"> <label><b>No. Rangka</b></label> <p>{{mFollowup.FrameNo}}</p> </div> </div> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-md-4\"> <label><b>Model : </b></label> <p>{{mFollowup.VehicleModelName}}</p> </div> <div class=\"col-md-4\"> <label><b>Tipe : </b></label> <p>{{mFollowup.Description}}</p> </div> <div class=\"col-md-4\"> <label><b>Warna : </b></label> <p>{{mFollowup.ColorName}}</p> </div> </div> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-md-4\"> <label><b>Jadwal Keluar Kendaraan</b></label> <bsdatepicker ng-model=\"mFollowup.OutDate\" ng-disabled=\"true\" name=\"tanggalKeluar\" date-options=\"dateOptions\"></bsdatepicker> </div> <div class=\"col-md-4\"> <label><b>Driver </b></label> <div class=\"input-icon-right input\"> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"\" ng-model=\"mFollowup.DriverPDSName\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"row\" style=\"margin: 30px 0 0 0\"> <div class=\"col-md-4\"> <label><b>Kategori Reparasi</b></label> <p> <input type=\"radio\" ng-disabled=\"true\" ng-model=\"mFollowup.CategoryReparasiId\" name=\"RadioKategoriReparasi\" ng-value=\"1\">Perbaikan Kecil &nbsp&nbsp <input type=\"radio\" ng-disabled=\"true\" ng-model=\"mFollowup.CategoryReparasiId\" name=\"RadioKategoriReparasi\" ng-value=\"2\">Perbaikan Besar </p> </div> <div class=\"col-md-4\"> <p><input type=\"checkbox\" ng-disabled=\"true\" ng-model=\"mFollowup.NeedInsuranceBit\"><b>Dibutuhkan Asuransi</b></p> </div> </div> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-md-4\"> <label><b>Keterangan Kerusakan</b></label> <div class=\"input-icon-right input\"> <textarea ng-disabled=\"true\" ng-model=\"mFollowup.KeteranganKerusakan\" name=\"keteranganKerusakan\" rows=\"6\" cols=\"50\" placeholder=\"Keterangan Kerusakan...\"></textarea> </div> </div> </div> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-md-4\"> <label><b>Estimasi Tanggal Kembali</b></label> <bsdatepicker ng-model=\"mFollowup.BackDateEstimated\" name=\"estimasiTanggalKembali\" date-options=\"dateOptions\"></bsdatepicker> </div> </div> <div class=\"row\" style=\"margin: 20px 0 0 15px\"> <label><b>Bukti Kerusakan :</b></label> <div class=\"row\" style=\"margin: 10px 15px 0 0\"> <table class=\"table table-bordered table-responsive\"> <thead> <tr> <th class=\"headerRow\">No.</th> <th class=\"headerRow\">Bukti</th> <th class=\"headerRow\">Action</th> </tr> </thead> <tbody> <tr ng-repeat=\"list in mFollowup.BuktiKerusakan\"> <td>{{$index+1}}</td> <td>{{list.PhotoName}}</td> <td><a ng-click=\"viewImage(list)\">View</a></td> </tr> </tbody> </table> </div> </div> </div> <div class=\"ui modal viewImage\"> <div> <div class=\"modal-header\"> <p style=\"text-align: center;font-size: 14px\">Bukti Kerusakan</p> </div> <div class=\"modal-body\"> <img ng-src=\"{{image.viewdocument}}\" style=\"max-width: 860px; max-height: 526px; text-align: center\"> <p align=\"center\" style=\"margin: 15px 0px 0px 0px !important\">{{image.viewdocumentName}}</p> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <button ng-click=\"keluarDokumen()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Keluar</button> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/daftarUrgentMemo/daftarUrgentMemo.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "        $('body .modals').remove();\r" +
    "\n" +
    "    }</script> <style type=\"text/css\">/* @media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    } */\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .box-show-setup,\r" +
    "\n" +
    "    .box-hide-setup {\r" +
    "\n" +
    "        -webkit-transition: all linear 0.3s;\r" +
    "\n" +
    "        -moz-transition: all linear 0.3s;\r" +
    "\n" +
    "        -ms-transition: all linear 0.3s;\r" +
    "\n" +
    "        -o-transition: all linear 0.3s;\r" +
    "\n" +
    "        transition: all linear 0.3s;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "        -webkit-border-radius: 4px;\r" +
    "\n" +
    "        -moz-border-radius: 4px;\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "        border: 0px;\r" +
    "\n" +
    "        background-color: transparent;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .adv-srch {\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        margin-bottom: 0px;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        background-color: rgba(213, 51, 55, 0.02);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .adv-srch-on {\r" +
    "\n" +
    "        padding-top: 4px !important;\r" +
    "\n" +
    "        padding: 6px;\r" +
    "\n" +
    "        margin: 0 3px 0 0;\r" +
    "\n" +
    "        margin-bottom: -5px !important;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        border-bottom-style: none !important;\r" +
    "\n" +
    "        background-color: #FEFBFB;\r" +
    "\n" +
    "        border-top-left-radius: 3px;\r" +
    "\n" +
    "        border-top-right-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .colorsrch {\r" +
    "\n" +
    "        color: red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .modalpadding {\r" +
    "\n" +
    "        padding-top: 0px !important;\r" +
    "\n" +
    "        padding-right: 0px !important;\r" +
    "\n" +
    "        padding-bottom: 0px !important;\r" +
    "\n" +
    "        padding-left: 0px !important;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin-top: 10px !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .noppadingFormGroup {\r" +
    "\n" +
    "        margin-bottom: 0px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .ScrollStyle {\r" +
    "\n" +
    "        max-height: 600px;\r" +
    "\n" +
    "        overflow-y: scroll;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .item-modal {\r" +
    "\n" +
    "        border: none;\r" +
    "\n" +
    "        padding-bottom: 15px;\r" +
    "\n" +
    "        border-bottom: 1px solid #ddd !important;\r" +
    "\n" +
    "        margin: 10px 30px 10px 30px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobileAddBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1!important;\r" +
    "\n" +
    "        font-size: 15px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobileFilterBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1!important;\r" +
    "\n" +
    "        font-size: 13px;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        float: right;\r" +
    "\n" +
    "        /*background-color: silver;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .filterMarg {\r" +
    "\n" +
    "        margin-bottom: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media only screen and (max-width: 430px) {\r" +
    "\n" +
    "        .iconFilter {\r" +
    "\n" +
    "            margin-left: -4px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -28px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        .iconFilter {\r" +
    "\n" +
    "            margin-left: -4px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #desktopderectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: block !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (min-width: 501px) and (max-width: 600px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -36px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #desktopderectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: block !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (min-width: 601px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -33px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobilesearchBtn {\r" +
    "\n" +
    "        text-decoration: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (max-width: 350px) {\r" +
    "\n" +
    "        .bsbreadcrumb .bsbreadcrumb-path,\r" +
    "\n" +
    "        .bsbreadcrumb i {\r" +
    "\n" +
    "            display: block !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*Modal*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .vertical-alignment-helper {\r" +
    "\n" +
    "        display: table;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "        /* This makes sure that we can still click outside of the modal to close it */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .vertical-align-center {\r" +
    "\n" +
    "        /* To center vertically */\r" +
    "\n" +
    "        display: table-cell;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .modal-content {\r" +
    "\n" +
    "        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */\r" +
    "\n" +
    "        width: inherit;\r" +
    "\n" +
    "        height: inherit;\r" +
    "\n" +
    "        /* To center horizontally */\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        pointer-events: all;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @-webkit-keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            -webkit-transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "        -webkit-border-radius: 4px;\r" +
    "\n" +
    "        -moz-border-radius: 4px;\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "        border: 0px;\r" +
    "\n" +
    "        background-color: transparent;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    input[type=date]::-webkit-inner-spin-button {\r" +
    "\n" +
    "        -webkit-appearance: none;\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <button ng-show=\"ShowAddUrgentMemoBtn\" tooltip-placement=\"bottom\" tooltip-trigger=\"focus\" tooltip-animation=\"true\" uib-tooltip=\"Tambah\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"addNewUrgentMemo()\"> <i class=\"fa fa-plus\"></i> </button> <bsform-mobile ng-show=\"DaftarUrgentMemoForm\" factory-name=\"DaftarUrgentMemoFactory\" bind-data=\"bind_data\" info-totaldata=\"true\" enable-listtoggle=\"true\" filter-data=\"filterData\" custom-modallihat=\"true\" custom-modalubah=\"true\" custom-modalhapus=\"true\" enable-listtoggle=\"false\" enable-addbutton=\"disabled\" info-totaldata=\"false\" service-enable=\"true\" updated-data=\"updated_data\" selected-data=\"selected_data\" form-name=\"DaftarUrgentMemoForm\" list-title=\"Daftar Urgent Memo\" row-template-url=\"app/sales/sales4/deliveryRequest/daftarUrgentMemo/rowTemplate.html\"> <bsform-mobileformdata> </bsform-mobileformdata> <bsform-mobilemodal> <!-- <div ng-if=\"selected_data.StatusUrgentMemoName=='Submitted'\"> --> <div> <a ng-click=\"lihatUrgentMemoFunction()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Lihat Detail</li> </a> </div> </bsform-mobilemodal> </bsform-mobile> <div ng-show=\"TambahUrgentMemoForm\"> <div class=\"row\"> <button ng-disabled=\"(mModeTambahUrgentMemo == 2 && (selected_data.BackDateEstimated==null||selected_data.BackDateEstimated==undefined))\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"SaveTambah()\"> Simpan </button> <button class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"BatalTambah()\"> Kembali </button> </div> <p> <br> </p> <p> <!-- <bsselect name=\"filterModel\" ng-model=\"mModeTambahUrgentMemo\" data=\"ModeTambahUrgentMemo\" item-text=\"modeName\" item-value=\"modeId\" on-select=\"modeTambahMemo(selected)\" placeholder=\"Pilih Mode Tambah\">\r" +
    "\n" +
    "        </bsselect> --> <select ng-change=\"modeTambahMemo(mModeTambahUrgentMemo)\" data-ng-options=\"data.modeId as data.modeName for data in ModeTambahUrgentMemo\" class=\"form-control mobile\" name=\"filterModel1\" ng-model=\"mModeTambahUrgentMemo\" value=\"modeId\" required> <option label=\"Pilih Mode Tambah\" value=\"\" disabled style=\"color:gray\"></option> </select> </p> <p> <br> </p> <div ng-show=\"denganSPK\"> <p><b>Sisa Kuota Urgent Memo : {{Quota.CurrentQuota}}<br></b></p> <div style=\"margin: 0 0 0 0\" class=\"row\"> <div class=\"form-group\"> <bsreqlabel>No SPK</bsreqlabel> <div class=\"input-group\"> <input type=\"text\" name=\"FormSPKNo\" class=\"form-control\" placeholder=\"FormSPKNo\" readonly ng-model=\"selected_data.FormSPKNo\" ng-maxlength=\"50\" required> <span class=\"input-group-addon\"> <i ng-click=\"lookupSPK()\" class=\"fa fa-search\"></i> </span> </div> </div> </div> <p><b>Nama Pelanggan<br></b> </p> <p style=\"color: #888b91\">{{selected_data.CustomerName}} </p> <p><b>No. Rangka<br></b> </p> <p style=\"color: #888b91\">{{selected_data.FrameNo}} </p> <p><b>Model<br></b> </p> <p style=\"color: #888b91\">{{selected_data.VehicleModelName}} </p> <p><b>Tipe<br></b> </p> <p style=\"color: #888b91\">{{selected_data.Description}} </p> <p><b>Warna<br></b> </p> <p style=\"color: #888b91\">{{selected_data.ColorName}} </p> <p><b>Janji Pengiriman<br></b> </p> <p style=\"color: #888b91\">{{selected_data.SentUnitTime | date:\"dd/MM/yyyy\"}} </p> <p ng-show=\"false\"> <input type=\"text\" ng-model=\"selected_data.SOId\"> <input type=\"text\" ng-model=\"selected_data.SpkId\"> <input type=\"text\" ng-model=\"selected_data.PDDId\"> </p> <div class=\"col-md-12\" style=\"margin-bottom:15px;margin-left:-25px\"> <div class=\"col-md-3\"> <b><bsreqlabel>Tgl. Janji Pengiriman untuk Urgent Memo</bsreqlabel></b> <div class=\"input-group date\" id=\"datetimepicker3\"> <input onkeydown=\"return false\" min=\"{{TodayDate}}\" type=\"date\" ng-model=\"selected_data.PromiseDelDate\"> </div> </div> <div class=\"col-md-3\" ng-show=\"tanpaSPK == true\"> <b><bsreqlabel>Waktu Pengiriman untuk Urgent Memo</bsreqlabel></b> <div class=\"input-group date\" id=\"datetimepicker3\"> <input onkeydown=\"return false\" type=\"time\" ng-model=\"selected_data.PromiseDelTime\"> </div> </div> </div> <div> <p><bsreqlabel><b>Alasan Pengajuan</b></bsreqlabel></p> <p><textarea rows=\"10\" cols=\"50\" maxlength=\"250\" placeholder=\"Alasan Pengajuan...\" ng-model=\"selected_data.ReasonSubmission\"></textarea></p> </div> </div> <div ng-show=\"tanpaSPK\"> <p><b>Sisa Kuota Urgent Memo : {{Quota.CurrentQuota}}<br></b></p> <div style=\"margin: 0 0 0 0\" class=\"row\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-group\"> <input type=\"text\" name=\"FrameNo\" class=\"form-control\" placeholder=\"No. Rangka\" readonly ng-model=\"selected_data.FrameNo\" ng-maxlength=\"50\" required> <span class=\"input-group-addon\"> <i ng-click=\"lookupNoRangka()\" class=\"fa fa-search\"></i> </span> </div> </div> </div> <p><b>Model<br></b> </p> <p style=\"color: #888b91\">{{selected_data.VehicleModelName}} </p> <p><b>Tipe<br></b> </p> <p style=\"color: #888b91\">{{selected_data.Description}} </p> <p><b>Warna<br></b> </p> <p style=\"color: #888b91\">{{selected_data.ColorName}} </p> <div> <p><b>Alasan Pengajuan</b></p> <p> <bsselect name=\"filterModel\" ng-model=\"selected_data.ReasonId\" data=\"OptionsAlasanPengajuan\" item-text=\"ReasonName\" item-value=\"ReasonName\" on-select=\"AlasanPengajuanFunction(selected)\" placeholder=\"Alasan Pengajuan\"> </bsselect> </p> </div> <div class=\"col-md-12\" style=\"margin-bottom:15px;margin-left:-25px\"> <div class=\"col-md-3\"> <b><bsreqlabel>Tgl. Pengambilan</bsreqlabel></b> <div class=\"input-group date\" id=\"datetimepicker3\"> <input onkeydown=\"return false\" min=\"{{TodayDate}}\" type=\"date\" ng-model=\"selected_data.PromiseDelDate\"> </div> </div> <div class=\"col-md-3\" ng-show=\"tanpaSPK == true\"> <b><bsreqlabel>Waktu Pengembalian</bsreqlabel></b> <div class=\"input-group date\" id=\"datetimepicker3\"> <input onkeydown=\"return false\" type=\"time\" ng-model=\"selected_data.PromiseDelTime\"> </div> </div> </div> <!-- <div class=\"row\" style=\"margin-top: -75px;margin-left: 150px\">\r" +
    "\n" +
    "            <div uib-timepicker ng-model=\"selected_data.mytime\" ng-change=\"changed()\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\"\r" +
    "\n" +
    "                style=\"float: left;\"></div>\r" +
    "\n" +
    "        </div> --> <div> <p><bsreqlabel><b>Lokasi Pengiriman</b></bsreqlabel></p> <p> <bsselect name=\"filterModel\" ng-model=\"selected_data.LocationCode\" data=\"OptionsLocation\" item-text=\"LocationName\" item-value=\"LocationName\" on-select=\"LokasiValue(selected)\" placeholder=\"Lokasi Pengiriman\"> </bsselect> </p> </div> <div> <fieldset ng-disabled=\"DisabledLocation\"> <p><bsreqlabel><b>Nama Tempat Pengiriman<br></b></bsreqlabel> </p> <input type=\"text\" class=\"form-control\" name=\"NLocationName\" placeholder=\"Nama Tempat Pengiriman\" ng-model=\"selected_data.LocationName\" maxlength=\"50\" style=\"margin-bottom: 10px\"> <p><bsreqlabel><b>Alamat Pengiriman</b></bsreqlabel></p> <p><textarea rows=\"10\" cols=\"50\" maxlength=\"100\" placeholder=\"Alamat Pengiriman...\" ng-model=\"selected_data.SentAddress\"></textarea></p> </fieldset> </div> <div> <p><bsreqlabel><b>Perkiraan Tanggal Kembali</b></bsreqlabel></p> <p><input onkeydown=\"return false\" min=\"{{TodayDate}}\" type=\"date\" ng-model=\"selected_data.BackDateEstimated\"></p> </div> <div> <input ng-model=\"selected_data.BackToPDCBit\" type=\"checkbox\"><b>Kembali ke PDC</b> </div> </div> </div> <div ng-show=\"detildenganSPK\"> <button class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"BatalLihatDetailUrgentMemo()\">Kembali</button> <p></p> <p></p> <div style=\"margin: 0 0 0 0\" class=\"row\"> <div class=\"form-group\" style=\"margin-top: 40px\"> <bsreqlabel>No SPK</bsreqlabel> <div class=\"input-group\"> <input type=\"text\" name=\"FormSPKNo\" class=\"form-control\" placeholder=\"FormSPKNo\" readonly ng-model=\"selected_data.FormSPKNo\" ng-maxlength=\"50\" required> <span class=\"input-group-addon\"> <i ng-disabled=\"true\" class=\"fa fa-search\"></i> </span> </div> </div> </div> <p><b>Nama Pelanggan<br></b> </p> <p style=\"color: #888b91\">{{selected_data.CustomerName}} </p> <p><b>No. Rangka<br></b> </p> <p style=\"color: #888b91\">{{selected_data.FrameNo}} </p> <p><b>Model<br></b> </p> <p style=\"color: #888b91\">{{selected_data.VehicleModelName}} </p> <p><b>Tipe<br></b> </p> <p style=\"color: #888b91\">{{selected_data.Description}} </p> <p><b>Warna<br></b> </p> <p style=\"color: #888b91\">{{selected_data.ColorName}} </p> <p><b>Janji Pengiriman<br></b> </p> <p style=\"color: #888b91\">{{selected_data.SentUnitTime | date:\"dd/MM/yyyy\"}} </p> <bsreqlabel>Tgl. Janji Pengiriman untuk Urgent Memo</bsreqlabel> <div class=\"input-group date\" id=\"datetimepicker3\"> <input onkeydown=\"return false\" readonly min=\"{{TodayDate}}\" type=\"date\" ng-model=\"selected_data.PromiseDelDate\" datepicker-options=\"dateOptions\"> </div> <p> <bsreqlabel>Alasan Pengajuan</bsreqlabel> </p> <p> <textarea readonly rows=\"10\" cols=\"50\" maxlength=\"250\" placeholder=\"Alasan Pengajuan...\" ng-model=\"selected_data.ReasonSubmission\"></textarea> </p> </div> <div ng-show=\"detiltanpaSPK\"> <button class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"BatalLihatDetailUrgentMemo()\">Kembali</button> <p></p> <p></p> <div style=\"margin: 0 0 0 0\" class=\"row\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-group\"> <input type=\"text\" name=\"FrameNo\" class=\"form-control\" placeholder=\"FrameNo\" readonly ng-model=\"selected_data.FrameNo\" ng-maxlength=\"50\" required> <span class=\"input-group-addon\"> <i ng-disabled=\"true\" class=\"fa fa-search\"></i> </span> </div> </div> </div> <p><b>Model<br></b> </p> <p style=\"color: #888b91\">{{selected_data.VehicleModelName}} </p> <p><b>Tipe<br></b> </p> <p style=\"color: #888b91\">{{selected_data.Description}} </p> <p><b>Warna<br></b> </p> <p style=\"color: #888b91\">{{selected_data.ColorName}} </p> <div> <p><b>Alasan Pengajuan</b></p> <p> <bsselect ng-disabled=\"true\" name=\"filterModel\" ng-model=\"selected_data.ReasonId\" data=\"OptionsAlasanPengajuan\" item-text=\"ReasonName\" item-value=\"TujuanPengirimanId\" on-select=\"AlasanPengajuan(selected)\" placeholder=\"Alasan Pengajuan\"> </bsselect> </p> </div> <bsreqlabel>Tgl. Janji Pengiriman</bsreqlabel> <div class=\"input-group date\" id=\"datetimepicker3\"> <input onkeydown=\"return false\" readonly type=\"date\" min=\"{{TodayDate}}\" ng-model=\"selected_data.PromiseDelDate\"> </div> <p> <bsreqlabel>Alamat Pengiriman</bsreqlabel> </p> <p> <textarea readonly rows=\"10\" cols=\"50\" maxlength=\"250\" placeholder=\"Alasan Pengajuan...\" ng-model=\"selected_data.SentAddress\"></textarea> </p> <bsreqlabel>Perkiraan Tanggal Kembali</bsreqlabel> <div class=\"input-group date\" id=\"datetimepicker3\"> <input onkeydown=\"return false\" readonly type=\"date\" min=\"{{TodayDate}}\" ng-model=\"selected_data.BackDateEstimated\"> </div> <div class=\"form-group noppadingFormGroup\"> <input readonly ng-model=\"selected_data.BackToPDCBit\" type=\"checkbox\"><b>Back to PDC</b> </div> </div> <div class=\"ui modal ModalFormNoSPK\"> <div class=\"modal-header\"> <h4 class=\"modal-title\">No. SPK</h4> </div> <br> <div> <div class=\"col-xs-4\"> <input type=\"text\" class=\"form-control\" ng-model=\"ModalFormNoSPKSearchValue\"> </div> <div class=\"col-xs-4\"> <select ng-model=\"SPKNoSearchCriteria\" class=\"form-control\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionSPKNoSearchCriteria\"> </select> </div> <div class=\"col-xs-4\"> <button class=\"rbtn btn ng-binding ladda-button\" ng-click=\"ResetModalFormNoSPK()\">Reset</button> <button class=\"rbtn btn ng-binding ladda-button\" ng-click=\"SearchModalFormNoSPK()\">Cari</button> </div> </div> <p> <br> </p> <div class=\"modal-body\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridNoSPK\" ui-grid-auto-resize></div> </div> <div class=\"modal-footer\"> <button ng-click=\"BatalLookupSPK_Clicked()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </div> </div> <div class=\"ui modal ModalFormNoRangka\"> <div class=\"modal-header\"> <h4 class=\"modal-title\">No. Rangka</h4> </div> <br> <div> <div class=\"col-xs-4\"> <input type=\"text\" class=\"form-control\" ng-model=\"ModalFormNoRangkaSearchValue\"> </div> <div class=\"col-xs-4\"> <select ng-model=\"FrameNoSearchCriteria\" class=\"form-control\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionFrameNoSearchCriteria\"> </select> </div> <div class=\"col-xs-4\"> <button class=\"rbtn btn ng-binding ladda-button\" ng-click=\"ResetModalFormNoRangka()\">Reset</button> <button class=\"rbtn btn ng-binding ladda-button\" ng-click=\"SearchModalFormNoRangka()\">Cari</button> </div> </div> <p> <br> </p> <div class=\"modal-body\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridNoRangka\" ui-grid-auto-resize></div> </div> <div class=\"modal-footer\"> <!--<button ng-click=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"BatalLookupNoRangka_Clicked()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/daftarUrgentMemo/lookupNoRangka.html',
    "<!--Harus item--> <div style=\"font-size: 12px\"> <b>No. Rangka : {{item.FrameNo}} </b> <button ng-click=\"openModal(item)\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:10px\"> <li><b>Model : </b>{{item.VehicleModelName}}</li> <li><b>Tipe : </b>{{item.KatashikiSuffixCode}}</li> <li><b>Warna : </b>{{item.ColorName}}</li> </ul> </p>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/daftarUrgentMemo/lookupSPK.html',
    "<!--Harus item--> <div style=\"font-size: 12px\"> <b>No. SPK :{{item.FormSPKNo}} </b> <button ng-click=\"openModal(item)\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:10px\"> <li><b>No. Rangka : </b>{{item.FrameNo}}</li> <li><b>Nama Customer : </b>{{item.CustomerName}}</li> </ul> </p>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/daftarUrgentMemo/rowTemplate.html',
    "<!--Harus item--> <div style=\"font-size: 12px\"> <b>No. SPK :{{item.FormSPKNo}} Nama Customer : {{item.CustomerName}}</b> <button ng-click=\"openModal(item)\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:transparent box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:10px\"> <li><b>No. Rangka : </b>{{item.FrameNo}}</li> <li><b>Janji Pengiriman : </b><label>{{item.SentUnitTime| date:\"dd/MM/yyyy\"}}</label> <!--<label>{{item.PromiseDelDate| date:\"dd MMMM yyyy\"}}</label>--></li> <li><b>Status : </b>{{item.StatusUrgentMemoName}}</li> </ul> </p>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/deliveryNote/deliveryNote.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "\t\t$('body .modals').remove();\r" +
    "\n" +
    "\t}</script> <style type=\"text/css\">input[type=date]::-webkit-inner-spin-button {\r" +
    "\n" +
    "\t\t-webkit-appearance: none;\r" +
    "\n" +
    "\t\tdisplay: none;\r" +
    "\n" +
    "\t}</style> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <bsbreadcrumb-custom action-label=\"breadcrums.title\" tab=\"tab\"></bsbreadcrumb-custom> <div> <button ng-show=\"ShowAddDeliveryNoteBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top: -31px\" ng-click=\"addNewDeliveryNote()\"> Tambah </button> <bsform-grid ng-show=\"ShowDeliveryNoteMain\" ng-form=\"DeliveryNoteForm\" factory-name=\"DeliveryNoteFactory\" model=\"mDeliveryNote\" model-id=\"DeliveryNoteId\" loading=\"loading\" hide-new-button=\"true\" show-advsearch=\"on\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"DeliveryNoteForm\" form-title=\"Delivery Note\" modal-title=\"Delivery Note\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model</label> <bsselect name=\"model\" ng-model=\"mDeliveryNoteSearch.VehicleModelId\" data=\"VehicleModel\" ng-change=\"filtertipe(mDeliveryNoteSearch.VehicleModelId)\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"PIlih Model\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tipe</label> <bsselect name=\"type\" ng-model=\"mDeliveryNoteSearch.VehicleTypeId\" data=\"vehicleTypefilter\" ng-change=\"filterColor(mDeliveryNoteSearch.VehicleTypeId)\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Tipe\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Warna</label> <bsselect name=\"color\" ng-model=\"mDeliveryNoteSearch.ColorId\" data=\"vehicleTypecolorfilter\" item-text=\"ColorName, ColorCode\" item-value=\"ColorId\" placeholder=\"Pilih Warna\" icon=\"fa fa glyph-left\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tujuan Pengiriman</label> <bsselect id=\"comboBox\" ng-model=\"mDeliveryNoteSearch.TujuanPengirimanId\" data=\"optionsTujuanPengiriman\" item-text=\"TujuanPengirimanName\" item-value=\"TujuanPengirimanId\" on-select=\"SOTypeId\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tanggal Keluar</label> <bsdatepicker ng-model=\"mDeliveryNoteSearch.OutDate\" name=\"tanggal\" date-options=\"dateOptions\"> </bsdatepicker> </div> </div> <div class=\"col-md-4\"> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div class=\"ui modal ModalFrame\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"BatalLookupFrame()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">No. Rangka</h4> </div> <div style=\"margin-bottom: 10px\"> <div style=\"margin-top: 10px;margin-bottom: 20px;margin-left: 20px;margin-right: 20px;font-color:red\"> *isi salah satu No. Rangka atau Model </div> <div class=\"row\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\" style=\"margin-left:10px\"> <bsreqlabel>No. Rangka:</bsreqlabel> <input maxlength=\"20\" type=\"text\" class=\"form-control\" placeholder=\"No. Rangka\" ng-model=\"FrameNoSearch\"> </div> <div class=\"col-md-6\" style=\"margin-left:-20px\"> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"model\" ng-model=\"FrameModelSearch\" data=\"VehicleModel\" ng-change=\"filtertipe(FrameModelSearch)\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"PIlih Model\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"row\" style=\"margin-bottom: 10px\"> <div class=\"col-md-6\" style=\"margin-left:10px\"> <label>Tipe</label> <bsselect name=\"type\" ng-model=\"FrameTipeSearch\" data=\"vehicleTypefilter\" ng-change=\"filterColor(FrameTipeSearch)\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Tipe\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> <div class=\"col-md-6\" style=\"margin-left:-20px\"> <label>Warna</label> <bsselect name=\"color\" ng-model=\"FrameWarnaSearch\" data=\"vehicleTypecolorfilter\" item-text=\"ColorName, ColorCode\" item-value=\"ColorId\" placeholder=\"Pilih Warna\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> </div> <div style=\"margin-right: 15px\"> <button ng-click=\"SearchDaFrame()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"><i class=\"fa fa-fw fa-search\"></i>Cari</button> <button ng-click=\"ResetSearch()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"><i class=\"fa fa-fw fa-refresh\"></i>Refresh</button> </div> </div> <br> <br> <div class=\"modal-body\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridFrameNo\" ui-grid-auto-resize></div> </div> <div class=\"modal-footer\"> <!--<button onclick=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"BatalLookupFrame()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </div> </div> <div ng-show=\"false\"> <div class=\"row\" style=\"margin-bottom:10px;margin-right:5px\"> <button style=\"float: right\" ng-click=\"DeliveryNoteCetakPanda()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Cetak </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliKeMainList()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\" style=\"border-style:solid;border-width: thin;margin-left:5px;margin-right:5px\"> <div class=\"row\" style=\"margin-left:10px;margin-right:10px;margin-top:15px\"> <div class=\"col-md-3\"> {{buat_cetak.CompanyName}} <br> {{buat_cetak.OutletName}} <br> {{buat_cetak.OutletAddress}} <br> {{buat_cetak.CityRegencyName}}-{{buat_cetak.PostalCode}} <br> Telp:{{buat_cetak.OutletPhoneNumber}} Fax:{{buat_cetak.OutletFaxNumber}} </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> Surat Jalan Keluar <br> Nomor Dokumen:{{buat_cetak.DeliveryNoteNo}} <br> Tanggal:{{TodayDate}} </div> </div> <br> <br> <div class=\"row\" style=\"margin-left:10px;margin-right:10px\"> <div class=\"col-md-12\"> Dengan Surat ini menyatakan unit dengan data berikut: </div> </div> <div class=\"row\" style=\"margin-left:10px;margin-right:10px\"> <div class=\"col-md-12\"> <table> <tr> <th> No Rangka </th> <td> : </td> <td> {{buat_cetak.FrameNo}} </td> </tr> <tr> <th> Model </th> <td> : </td> <td> {{buat_cetak.VehicleModelName}} </td> </tr> <tr> <th> Tipe </th> <td> : </td> <td> {{buat_cetak.Description}} </td> </tr> <tr> <th> Warna </th> <td> : </td> <td> {{buat_cetak.ColorName}} </td> </tr> </table> </div> </div> <br> <div class=\"row\" style=\"margin-left:10px;margin-right:10px\"> <div class=\"col-md-12\"> Akan Dibawa keluar dari cabang untuk keperluan {{buat_cetak.TujuanPengirimanName}} pada tanggal {{buat_cetak.OutDate| date:\"dd MMMM yyyy\"}} Oleh:{{buat_cetak.DriverPDSName}} </div> </div> <div class=\"row\" style=\"margin-left:10px;margin-right:10px\"> <div class=\"col-md-12\"> <p> Tempat Tujuan: </p> <p> {{buat_cetak.SentAddress}} </p> </div> </div> <br> <br> <br> <br> <br> <div class=\"row\" style=\"margin-left:10px;margin-right:10px;margin-bottom:15px\"> <div class=\"col-md-3\"> Disiapkan Oleh <br> PDS, <br> <br> <br> <br> _____________________ </div> <div class=\"col-md-3\"> Disetujui Oleh ADH or <br> Kacab, <br> <br> <br> <br> _____________________ </div> <div class=\"col-md-3\"> Diterima Oleh, <br> Tanggal: / / <br> <br> <br> <br> _____________________ </div> <div class=\"col-md-3\"> </div> </div> </div> </div> <div ng-show=\"ShowDeliveryNoteDetail\" style=\"margin-top:-50px\"> <button ng-disabled=\"mDeliveryNote.JenisTugasId==undefined||mDeliveryNote.JenisTugasId==null||mDeliveryNote.TujuanPengirimanId==null || mDeliveryNote.TujuanPengirimanId==undefined || mDeliveryNote.SentAddress==''|| mDeliveryNote.ReceiverName==''|| mDeliveryNote.ReceiverName==null|| mDeliveryNote.ReceiverName==undefined\" style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"SaveTheDeliveryNote()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Simpan </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"BataladdNewDeliveryNote()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> <p> <br> </p> <div class=\"form-group row\" style=\"padding: 0%\"> <div class=\"col-md-6\"> <bsreqlabel>No. Rangka</bsreqlabel> <div class=\"col-xs-11\" style=\"padding: 0%\"> <input style=\"background-color:white\" readonly type=\"text\" class=\"form-control\" name=\"ProspectName\" placeholder=\"Search\" ng-model=\"mDeliveryNote.FrameNo\" ng-maxlength=\"50\" ng-disabled=\"true\" required> </div> <div class=\"col-xs-1\" style=\"padding: 0%\"> <button ng-click=\"lookupFrameNo()\" style=\"background-color: #e7e7e7; border: none; padding: 0%; height:32px; width: 100%; display: inline-block\"> <span> <i class=\"fa fa-search\"></i> </span> </button> </div> </div> </div> <div class=\"form-group row\" style=\"padding: 0%\"> <div class=\"col-md-3\"> <bsreqlabel>Model</bsreqlabel> <input readonly type=\"text\" class=\"form-control\" ng-model=\"mDeliveryNote.VehicleModelName\"> </div> <div class=\"col-md-3\"> <bsreqlabel>Tipe</bsreqlabel> <input readonly type=\"text\" class=\"form-control\" ng-model=\"mDeliveryNote.Description\"> </div> <div class=\"col-md-6\"> <bsreqlabel>Warna</bsreqlabel> <input readonly type=\"text\" class=\"form-control\" ng-model=\"mDeliveryNote.ColorName\"> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <bsreqlabel>Tujuan Pengiriman</bsreqlabel> <bsselect ng-disabled=\"mDeliveryNote.FrameNo==null || mDeliveryNote.FrameNo==undefined ||mDeliveryNote.FrameNo==''\" ng-model=\"mDeliveryNote.TujuanPengirimanId\" ng-change=\"TujuanPengirimanSelectedChanged()\" data=\"optionsTujuanPengiriman\" item-text=\"TujuanPengirimanName\" item-value=\"TujuanPengirimanId\" on-select=\"TujuanPengirimanValue(selected)\" placeholder=\" \"> </bsselect> </div> <div class=\"col-md-6\"> <div> <bsreqlabel>Jenis Tugas</bsreqlabel> <bsselect ng-disabled=\"mDeliveryNote.FrameNo==null || mDeliveryNote.FrameNo==undefined ||mDeliveryNote.FrameNo==''\" ng-model=\"mDeliveryNote.JenisTugasId\" data=\"optionsJenisTugas\" item-text=\"JenisTugasName\" item-value=\"JenisTugasId\" on-select=\"SalesmanId(selected)\" placeholder=\" \"> </bsselect> </div> <div style=\"margin-top:5px\"> <bsreqlabel>Nama Penerima</bsreqlabel> <input alpha-numeric mask=\"Huruf\" maxlength=\"50\" ng-disabled=\"mDeliveryNote.FrameNo==null || mDeliveryNote.FrameNo==undefined ||mDeliveryNote.FrameNo==''\" type=\"text\" class=\"form-control\" ng-model=\"mDeliveryNote.ReceiverName\"> </div> </div> </div> <div class=\"row\"> <p> </p> <p> </p> <div class=\"col-md-6\"> <p> <bsreqlabel ng-if=\"SelectTujuanPengiriman.TujuanPengirimanName == 'Karoseri'\">Cabang Karoseri</bsreqlabel> <bsreqlabel ng-if=\"SelectTujuanPengiriman.TujuanPengirimanName == 'Repair'\">Cabang Repair</bsreqlabel> </p> <p> <bsselect ng-disabled=\"mDeliveryNote.FrameNo==null || mDeliveryNote.FrameNo==undefined ||mDeliveryNote.FrameNo==''\" ng-if=\"SelectTujuanPengiriman.TujuanPengirimanName == 'Karoseri' || SelectTujuanPengiriman.TujuanPengirimanName == 'Repair'\" style=\"margin-bottom: 10px\" ng-model=\"mDeliveryNote.DeliveryCaroseriesVendorId\" data=\"optionsCabang\" item-text=\"Name\" item-value=\"VendorId\" icon=\"fa fa-search\" ng-change=\"CabangKaroseriSelectionChanged()\"> </bsselect> </p> <p> <bsreqlabel>Tanggal Keluar</bsreqlabel> </p> <p> <bsdatepicker ng-disabled=\"mDeliveryNote.FrameNo==null || mDeliveryNote.FrameNo==undefined ||mDeliveryNote.FrameNo==''\" ng-model=\"mDeliveryNote.OutDate\" name=\"tanggal\" date-options=\"dateOptions02\"> </bsdatepicker> <!-- <input ng-disabled=\"mDeliveryNote.FrameNo==null || mDeliveryNote.FrameNo==undefined ||mDeliveryNote.FrameNo==''\" min=\"{{TodayDate}}\" type=\"date\" class=\"form-control\" ng-model=\"mDeliveryNote.OutDate\"> --> </p> </div> <div class=\"col-md-6\"> <p> <bsreqlabel ng-if=\"SelectTujuanPengiriman.TujuanPengirimanName != 'Karoseri'\">Alamat Pengiriman</bsreqlabel> <bsreqlabel ng-if=\"SelectTujuanPengiriman.TujuanPengirimanName == 'Karoseri'\">Alamat Karoseri</bsreqlabel> </p> <p> <textarea maxlength=\"100\" ng-disabled=\"DeliveryNoteNotAllowChangeAddress\" rows=\"6\" class=\"form-control\" placeholder=\"Alamat\" ng-model=\"mDeliveryNote.SentAddress\"></textarea> </p> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <p> <bslabel>Kategori Driver</bslabel> </p> <p> <input readonly type=\"text\" class=\"form-control\" ng-model=\"mDeliveryNote.DriverCategoryName\"> </p> </div> <div class=\"col-md-6\"> <p> <bslabel>Driver</bslabel> </p> <p> <input readonly type=\"text\" class=\"form-control\" ng-model=\"mDeliveryNote.DriverPDSName\"> </p> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <p> <bslabel>Lokasi Unit</bslabel> </p> <p> <input readonly type=\"text\" class=\"form-control\" ng-model=\"mDeliveryNote.LastLocation\"> </p> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "\t\t\t<p>\r" +
    "\n" +
    "\t\t\t\t<bslabel>Alamat</bslabel>\r" +
    "\n" +
    "\t\t\t</p>\r" +
    "\n" +
    "\t\t\t<p>\r" +
    "\n" +
    "\t\t\t\t<input readonly type=\"text\" class=\"form-control\" ng-model=\"mDeliveryNote.Address\">\r" +
    "\n" +
    "\t\t\t</p>\r" +
    "\n" +
    "\t\t</div> --> <div class=\"col-md-6\"> <p> <bslabel>Alamat Unit</bslabel> </p> <p> <textarea maxlength=\"100\" rows=\"6\" class=\"form-control\" readonly placeholder=\"Alamat\" ng-model=\"mDeliveryNote.LastAddressLocation\"></textarea> </p> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <!--<p ng-show=\"DeliveryNoteShowEstimasiTanggalKembali\">\r" +
    "\n" +
    "\t\t\t\t<bsreqlabel>Estimasi Tanggal Kembali</bsreqlabel>\r" +
    "\n" +
    "\t\t\t</p>\r" +
    "\n" +
    "\t\t\t<p ng-show=\"DeliveryNoteShowEstimasiTanggalKembali\">\r" +
    "\n" +
    "\t\t\t\t<input min=\"{{TodayDate}}\" class=\"form-control\" type=\"date\" ng-model=\"mDeliveryNote.DateBackEstimated\">\r" +
    "\n" +
    "\t\t\t</p>--> <p ng-show=\"DeliveryNoteShowEstimasiTanggalKembali\"> <bsreqlabel>Estimasi Tanggal Kembali</bsreqlabel> </p> <p ng-show=\"DeliveryNoteShowEstimasiTanggalKembali\"> <bsdatepicker ng-model=\"mDeliveryNote.DateBackEstimated\" name=\"tanggal2\" date-options=\"dateOptions02\"> </bsdatepicker> </p> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> </div> <div class=\"row\" ng-if=\"SelectTujuanPengiriman.TujuanPengirimanName == 'Karoseri'\"> <div class=\"col-md-3\"> <p> <bsreqlabel>Pengiriman Unit</bsreqlabel> </p> <p ng-repeat=\"da_radio_RadioPengirimanUnit in RadioPengirimanUnit\"> <input type=\"radio\" name=\"PengirimanRadio\" ng-model=\"mDeliveryNote.DeliveryCaroseriesDestinationId\" ng-value=\"da_radio_RadioPengirimanUnit.CaroseriesDestinationId\">{{da_radio_RadioPengirimanUnit.CaroseriesDestinationName}} </p> </div> <div class=\"col-md-3\"> <p> <bsreqlabel>Driver</bsreqlabel> </p> <p ng-repeat=\"da_radio_RadioCategoryDriver in RadioCategoryDriver\"> <input ng-disabled=\"true\" type=\"radio\" ng-change=\"SelectedDriverCategoryRadio()\" name=\"DriverRadio\" ng-model=\"mDeliveryNote.DeliveryCaroseriesDriverCategoryId\" ng-value=\"da_radio_RadioCategoryDriver.DriverCategoryId\">{{da_radio_RadioCategoryDriver.DriverCategoryName}} </p> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/listTaskDriver/listTaskDriver.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "        -webkit-border-radius: 4px;\r" +
    "\n" +
    "        -moz-border-radius: 4px;\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "        border: 0px;\r" +
    "\n" +
    "        background-color: transparent;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .adv-srch {\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        margin-bottom: 0px;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        background-color: rgba(213, 51, 55, 0.02);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .adv-srch-on {\r" +
    "\n" +
    "        padding-top: 4px !important;\r" +
    "\n" +
    "        padding: 6px;\r" +
    "\n" +
    "        margin: 0 3px 0 0;\r" +
    "\n" +
    "        margin-bottom: -5px !important;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        border-bottom-style: none !important;\r" +
    "\n" +
    "        background-color: #FEFBFB;\r" +
    "\n" +
    "        border-top-left-radius: 3px;\r" +
    "\n" +
    "        border-top-right-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .colorsrch {\r" +
    "\n" +
    "        color: red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .modalpadding {\r" +
    "\n" +
    "        padding-top: 0px !important;\r" +
    "\n" +
    "        padding-right: 0px !important;\r" +
    "\n" +
    "        padding-bottom: 0px !important;\r" +
    "\n" +
    "        padding-left: 0px !important;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    /*@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin-top: 10px !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .noppadingFormGroup {\r" +
    "\n" +
    "        margin-bottom: 0px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .ScrollStyle {\r" +
    "\n" +
    "        max-height: 400px;\r" +
    "\n" +
    "        overflow-y: scroll;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .item-modal {\r" +
    "\n" +
    "        border: none;\r" +
    "\n" +
    "        padding-bottom: 15px;\r" +
    "\n" +
    "        border-bottom: 1px solid #ddd !important;\r" +
    "\n" +
    "        margin: 10px 30px 10px 30px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobileAddBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1!important;\r" +
    "\n" +
    "        font-size: 15px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobileFilterBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1!important;\r" +
    "\n" +
    "        font-size: 13px;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        float: right;\r" +
    "\n" +
    "        /*background-color: silver;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .filterMarg{\r" +
    "\n" +
    "        margin-bottom: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media only screen and (max-width: 430px) {\r" +
    "\n" +
    "        .iconFilter{\r" +
    "\n" +
    "            margin-left: -4px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "     @media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp{\r" +
    "\n" +
    "            margin: -28px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "     @media (min-width: 501px) and (max-width: 600px) {\r" +
    "\n" +
    "        .btnUp{\r" +
    "\n" +
    "            margin: -36px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media (min-width: 601px) {\r" +
    "\n" +
    "        .btnUp{\r" +
    "\n" +
    "            margin: -33px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media (max-width: 600px) {\r" +
    "\n" +
    "        .inputForm{\r" +
    "\n" +
    "            margin: 45px 0 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .mobilesearchBtn {\r" +
    "\n" +
    "        text-decoration: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @media (max-width: 350px) {\r" +
    "\n" +
    "        .bsbreadcrumb .bsbreadcrumb-path,\r" +
    "\n" +
    "        .bsbreadcrumb i {\r" +
    "\n" +
    "            display: block !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*Modal*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .vertical-alignment-helper {\r" +
    "\n" +
    "        display: table;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "        /* This makes sure that we can still click outside of the modal to close it */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .vertical-align-center {\r" +
    "\n" +
    "        /* To center vertically */\r" +
    "\n" +
    "        display: table-cell;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .modal-content {\r" +
    "\n" +
    "        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */\r" +
    "\n" +
    "        width: inherit;\r" +
    "\n" +
    "        height: inherit;\r" +
    "\n" +
    "        /* To center horizontally */\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        pointer-events: all;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @-webkit-keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            -webkit-transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    @keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .thumb {\r" +
    "\n" +
    "        height: 75px;\r" +
    "\n" +
    "        border: 1px solid #000;\r" +
    "\n" +
    "        margin: 10px 5px 0 0;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"ListTaskDriverMain\"> <bsform-mobile factory-name=\"ListTaskDriverFactory\" bind-data=\"bind_data\" info-totaldata=\"true\" enable-listtoggle=\"true\" filter-data=\"filterData\" enable-addbutton=\"disable\" custom-modallihat=\"true\" custom-modalubah=\"true\" custom-modalhapus=\"true\" enable-listtoggle=\"false\" info-totaldata=\"false\" service-enable=\"true\" updated-data=\"updated_data\" selected-data=\"selected_data\" form-name=\"ListTaskDriverForm\" list-title=\"List Task Driver\" row-template-url=\"app/sales/sales4/deliveryRequest/listTaskDriver/rowTemplate.html\"> <bsform-mobilemodal> <div> <a ng-click=\"lihatDetailListTaskDriver()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Lihat Detail</li> </a> <a ng-click=\"AssignListTaskDriver()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-pencil\" style=\"margin-right: 15px\"></i></span>Assign</li> </a> </div> </bsform-mobilemodal> <bsform-mobileformdata> </bsform-mobileformdata> </bsform-mobile> </div> <div ng-show=\"ListTaskDriverDetail\"> <div class=\"form-group\" style=\"margin-top:20px\"> <div> <button ng-show=\"!DisableTheDetailEditing\" ng-disabled=\"selected_data.DriverCategoryId == null || selected_data.DriverId == null\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"ListTaskDriverSaveAssign()\"> Simpan </button> <button class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"ListTaskDriverBackToMain()\"> Kembali </button> </div> <div> <br> <br> <p><b>No. Rangka<br></b> </p> <p style=\"color: #888b91\">{{selected_data.FrameNo}} </p> </div> <div class=\"form-group\" style=\"margin-left:-25px\"> <div class=\"row col-xs-12\"> <div class=\"col-xs-4\"> <p><b>Model<br></b> </p> <p style=\"color: #888b91\">{{selected_data.VehicleModelName}} </p> </div> <div class=\"col-xs-4\"> <p><b>Tipe<br></b> </p> <p style=\"color: #888b91\">{{selected_data.Description}} </p> </div> <div class=\"col-xs-4\"> <p><b>Warna<br></b> </p> <p style=\"color: #888b91\">{{selected_data.ColorName}} </p> </div> </div> </div> <div class=\"form-group\"> <p><b>Nama Pelanggan<br></b> </p> <p style=\"color: #888b91\">{{selected_data.CustomerName}} </p> </div> <div class=\"form-group\"> <p><b>No. SPK<br></b> </p> <p style=\"color: #888b91\">{{selected_data.FormSPKNo}} </p> </div> <div class=\"form-group noppadingFormGroup\" ng-class=\"{ 'has-error' : formPanel.tipe.$invalid && formPanel.tipe.$touched }\"> <label><b>Tujuan Pengiriman</b></label> <bsselect ng-disabled=\"true\" id=\"comboBox\" ng-model=\"selected_data.TujuanPengirimanId\" data=\"optionsTujuanPengiriman\" item-text=\"TujuanPengirimanName\" item-value=\"TujuanPengirimanId\" on-select=\"SOTypeId\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> <p> <br> </p> <div class=\"form-group\" style=\"margin-left:-25px\"> <div class=\"row col-xs-12\"> <div class=\"col-xs-6\"> <p><b>Tanggal Pengiriman<br></b> </p> <p style=\"color: #888b91\">{{selected_data.SentDate | date:\"dd MMMM yyyy\"}} </p> </div> <div class=\"col-xs-6\"> <p><b>Waktu Pengiriman<br></b> </p> <p style=\"color: #888b91\">{{selected_data.SentTime | date:\"HH:mm\"}} </p> </div> </div> <div class=\"row col-xs-12\" ng-if=\"selected_data.NamaJenisTugas == 'Pengiriman'\"> <div class=\"col-xs-6\"> <p><b>Tanggal Keluar<br></b> </p> <p style=\"color: #888b91\">{{selected_data.OutDate | date:\"dd MMMM yyyy\"}} </p> </div> <div class=\"col-xs-6\"> <p><b><bsreqlabel>Waktu Keluar</bsreqlabel><br></b> </p> <div ng-disabled=\"DisableTheDetailEditing\" uib-timepicker ng-model=\"mytime\" ng-change=\"ListTaskDriverChangedOutTime(mytime)\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" style=\"float: left;margin-top:-25px\"></div> </div> </div> <div class=\"row\" ng-if=\"selected_data.NamaJenisTugas != 'Pengiriman'\"> <div class=\"col-xs-6\"> <p><b>Tanggal Kembali<br></b> </p> <p style=\"color: #888b91\">{{selected_data.DateBackEstimated | date:\"dd MMMM yyyy\"}} </p> </div> <div class=\"col-xs-6\"> <p><b>Waktu Kembali<br></b> </p> <div uib-timepicker ng-model=\"selected_data.DateBackEstimatedTime\" ng-change=\"changed()\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" style=\"float: left\"></div> </div> </div> </div> <p> <label><b>Direct Delivery dari PDC</b></label> </p> <p> <input ng-disabled=\"true\" ng-model=\"selected_data.DirectDeliveryBit\" type=\"checkbox\"> </p> <div class=\"form-group\"> <p><b>Lokasi Pengiriman<br></b> </p> <p style=\"color: #888b91\">{{selected_data.SentAddress}} </p> </div> <div class=\"form-group noppadingFormGroup\" ng-class=\"{ 'has-error' : formPanel.tipe.$invalid && formPanel.tipe.$touched }\"> <bsreqlabel><b>Kategori Driver</b></bsreqlabel> <bsselect ng-disabled=\"DisableTheDetailEditing\" id=\"comboBox\" choice-filter=\"propsFilter: {DriverCategoryName: $select.search}\" ng-model=\"selected_data.DriverCategoryId\" data=\"optionsCategoryDriver\" item-text=\"DriverCategoryName\" item-value=\"DriverCategoryId\" on-select=\"SelectDriver(selected)\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> <p> <br> </p> <div class=\"form-group noppadingFormGroup\" ng-class=\"{ 'has-error' : formPanel.tipe.$invalid && formPanel.tipe.$touched }\"> <bsreqlabel><b>Driver</b></bsreqlabel> <bsselect ng-disabled=\"DisableTheDetailEditing\" id=\"comboBox\" choice-filter=\"propsFilter: {DriverPDSName: $select.search}\" ng-model=\"selected_data.DriverId\" data=\"optionsDriverThingy\" item-text=\"DriverPDSName\" item-value=\"DriverPDSId\" on-select=\"SOTypeId\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> <p ng-show=\"formPanel.DriverName.$invalid && formPanel.DriverName.$touched\" class=\"help-block\">Your Driver Name is required.</p> <p> <br> </p> </div></div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/listTaskDriver/rowTemplate.html',
    "<!--Harus item--> <div style=\"font-size: 12px\"> <b>{{item.CustomerName}}</b> <button ng-click=\"openModal(item)\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:transparent; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:10px\"> <li><b>No. Rangka : </b>{{item.FrameNo}}</li> <li><b>Tanggal Pengiriman : </b>{{item.SentDate| date:\"dd MMMM yyyy\"}}</li> <li><b>Tipe Pengiriman : </b><label ng-show=\"item.DirectDeliveryBit==false\">Normal Delivery</label><label ng-show=\"item.DirectDeliveryBit==true\">Direct Delivery</label></li> <li><b>Tujuan Pengiriman : </b>{{item.TujuanPengirimanName}}</li> <li><b>Jenis Tugas : </b>{{item.NamaJenisTugas}}</li> <li><b>Driver : </b>{{item.DriverPDSName}}</li> </ul> </p>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/monitoringDriver/monitoringDriver.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "\t$('body .modals').remove();\r" +
    "\n" +
    "}</script> <style type=\"text/css\">input[type=date]::-webkit-inner-spin-button {\r" +
    "\n" +
    "\t\t-webkit-appearance: none;\r" +
    "\n" +
    "\t\tdisplay: none;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "fifthDate button span {\r" +
    "\n" +
    "        background-color: crimson;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .fourthDate button span {\r" +
    "\n" +
    "        background-color: darkseagreen;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .thirdDate button span {\r" +
    "\n" +
    "        background-color: lightsteelblue;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .secondDate button span {\r" +
    "\n" +
    "        background-color: sandybrown;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .firstDate button span {\r" +
    "\n" +
    "        background-color: mediumpurple;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "        width: 300px;\r" +
    "\n" +
    "        height: 300px;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <div ng-show=\"MonitoringDriverCalendar\"> <div ng-show=\"MonitoringDriverCalendarTop\"> <p align=\"center\"> <div style=\"display:inline-block; min-height:300px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-change=\"SearchWithCalendarChanged(dt1)\" uib-datepicker ng-model=\"dt1\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </p> <p style=\"text-align: center\"> <button type=\"button\" class=\"btn btn-sm btn-info\" ng-click=\"today()\">Today</button> <!--<button  type=\"button\" class=\"btn btn-sm btn-info\" ng-click=\"clear()\">clear</button>--> </p> </div> <div ng-show=\"MonitoringDriverCalendarDetail\"> <div class=\"row\"> <!--<button style=\"float: left;\" ng-click=\"HideCalendarDetailShowCalendar()\">--> <label style=\"float: left;margin-left:15px\" ng-click=\"HideCalendarDetailShowCalendar()\"> <b>&lt;{{MonthBack| date:\"MMMM\"}}</b> </label> <!--</button>--> <button class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-right:10px\" ng-click=\"MonitoringDriverToListTask()\"> <i class=\"fa fa-plus\"></i> </button> </div> <div class=\"row\"> <p align=\"center\"> <a style=\"margin-left:10px;margin-right:10px\" ng-click=\"CalendarDetailChoicePlus1Clicked(CalendarDetailChoice)\">{{CalendarDetailChoice| date:\"EEEE,d\"}}</a> <a style=\"margin-left:10px;margin-right:10px\" ng-click=\"CalendarDetailChoicePlus1Clicked(CalendarDetailChoicePlus1)\">{{CalendarDetailChoicePlus1| date:\"EEEE,d\"}}</a> <a style=\"margin-left:10px;margin-right:10px\" ng-click=\"CalendarDetailChoicePlus1Clicked(CalendarDetailChoicePlus2)\">{{CalendarDetailChoicePlus2| date:\"EEEE,d\"}}</a> <a style=\"margin-left:10px;margin-right:10px\" ng-click=\"CalendarDetailChoicePlus1Clicked(CalendarDetailChoicePlus3)\">{{CalendarDetailChoicePlus3| date:\"EEEE,d\"}}</a> <a style=\"margin-left:10px;margin-right:10px\" ng-click=\"CalendarDetailChoicePlus1Clicked(CalendarDetailChoicePlus4)\">{{CalendarDetailChoicePlus4| date:\"EEEE,d\"}}</a> </p> <p align=\"center\"> {{CalendarDetailChoice| date:\"EEEE, MMMM d, y\"}} </p> </div> <br> <br> <div style=\"display: block\" class=\"grid\" ui-grid=\"TabelListJadwal\" ui-grid-auto-resize></div> </div> </div> <div ng-show=\"MonitoringDriverListTask\"> <div class=\"row\"> <button class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-right:10px\" ng-click=\"MonitoringDriverKembaliKeCalendar()\"> Kembali </button> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row col-md-5\"> <p>Tanggal Pengiriman</p> <p><input ng-model=\"MonitoringDriverListTask_SearchSentDate\" type=\"date\"></p> </div> <div class=\"row col-md-2\"> </div> <div class=\"row col-md-5\"> <p>No Rangka</p> <p><input ng-model=\"MonitoringDriverListTask_SearchFrameNo\" type=\"text\"></p> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-12\"> <div class=\"row col-md-5\"> <label>Tipe Pengiriman</label> <bsselect ng-model=\"MonitoringDriverListTask_SearchDirectDeliveryBit\" id=\"comboBox\" data=\"optionsDirectDeliverySearch\" item-text=\"DirectDeliveryName\" item-value=\"DirectDeliveryBit\" on-select=\"SOTypeId\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"row col-md-2\"> </div> <div class=\"row col-md-5\"> <label>Tujuan Pengiriman</label> <bsselect ng-model=\"MonitoringDriverListTask_SearchTujuanPengirimanId\" id=\"comboBox\" data=\"optionsTujuanPengirimanSearch\" item-text=\"TujuanPengirimanName\" item-value=\"TujuanPengirimanId\" on-select=\"SOTypeId\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-12\"> <div class=\"row col-md-5\"> </div> <div class=\"row col-md-2\"> </div> <div class=\"row col-md-5\"> <button class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"MonitoringDriverListTask_Search_Btn_Clicked()\"> Search </button> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <!--<div style=\"display: block\" class=\"grid\" ui-grid=\"TabelListTaskDriver\" ui-grid-auto-resize></div>--> <div aria-labelledby=\"dropdownMenu1\" ng-repeat=\"ContentTabelListTaskDriver in TabelListTaskDriver\" style=\"background-color:#e8eaed\"> <div style=\"border-bottom:1px solid\"> <div style=\"font-size: 12px\"> <b>{{ContentTabelListTaskDriver.CustomerName}}</b> <button ng-click=\"MonitoringDriverListTaskBukaModal(ContentTabelListTaskDriver)\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:10px\"> <li><b>No. Rangka : </b>{{ContentTabelListTaskDriver.FrameNo}}</li> <li><b>Tanggal Pengiriman : </b>{{ContentTabelListTaskDriver.SentDate| date:\"dd MMMM yyyy\"}}</li> <li><b>Tipe Pengiriman : </b><label ng-show=\"ContentTabelListTaskDriver.DirectDeliveryBit==false\">Normal Delivery</label><label ng-show=\"ContentTabelListTaskDriver.DirectDeliveryBit==true\">Direct Delivery</label></li> <li><b>Tujuan Pengiriman : </b>{{ContentTabelListTaskDriver.TujuanPengirimanName}}</li> <li><b>Driver : </b>{{ContentTabelListTaskDriver.DriverPDSName}}</li> </ul> </p> </div> </div> </div> </div> </div> <div ng-show=\"MonitoringDriverDetailPage\"> <div class=\"row\"> <button ng-disabled=\"TheDriverDetail.TujuanPengirimanId==undefined || TheDriverDetail.DriverCategoryId==undefined || TheDriverDetail.DriverId==undefined\" ng-show=\"!DisableTheDetailEditing\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-left:5px;margin-right:10px\" ng-click=\"MonitoringDriverSaveAssign()\"> Simpan </button> <button class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-left:5px;margin-right:5px\" ng-click=\"MonitoringDriverKembaliKeListTask()\"> Kembali </button> </div> <div style=\"margin-bottom:5px;margin-left:5px;margin-right:5px\" class=\"row\"> <p><b>No Rangka:</b></p> <p>{{TheDriverDetail.FrameNo}}</p> </div> <div style=\"margin-bottom:5px;margin-right:5px\" class=\"row\"> <div class=\"col-xs-12\"> <div class=\"col-xs-4\"> <p><b>Model</b></p> <p>{{TheDriverDetail.VehicleModelName}}</p> </div> <div class=\"col-xs-4\"> <p><b>Tipe</b></p> <p>{{TheDriverDetail.Description}}</p> </div> <div class=\"col-xs-4\"> <p><b>Warna</b></p> <p>{{TheDriverDetail.ColorName}}</p> </div> </div> </div> <div style=\"margin-bottom:5px;margin-left:5px;margin-right:5px\" class=\"row\"> <p><b>Nama Pelanggan</b></p> <p>{{TheDriverDetail.CustomerName}}</p> <p></p> <p><b>No SPK</b></p> <p>{{TheDriverDetail.FormSPKNo}}</p> </div> <div style=\"margin-bottom:5px;margin-left:5px;margin-right:5px\" class=\"row\"> <label>Tujuan Pengiriman</label> <bsselect ng-disabled=\"DisableTheDetailEditing\" id=\"comboBox\" ng-model=\"TheDriverDetail.TujuanPengirimanId\" data=\"optionsTujuanPengiriman\" item-text=\"TujuanPengirimanName\" item-value=\"TujuanPengirimanId\" on-select=\"SOTypeId\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> <div style=\"margin-bottom:5px;margin-right:5px\" class=\"row\"> <div class=\"row col-xs-12\"> <div class=\"col-xs-6\"> <p><b>Tanggal Pengiriman</b></p> <p>{{TheDriverDetail.SentDate| date:\"dd MMMM yyyy\"}}</p> </div> <div class=\"col-xs-6\"> <p><b>Waktu Pengiriman</b></p> <p>{{TheDriverDetail.SentTime}}</p> </div> </div> </div> <div style=\"margin-bottom:5px;margin-left:5px;margin-right:5px\" class=\"row\"> <p><input ng-disabled=\"true\" ng-model=\"TheDriverDetail.DirectDeliveryBit\" type=\"checkbox\"> Direct Delivery From PDC</p> <p></p> <p><b>Lokasi Pengiriman:</b></p> <p>{{TheDriverDetail.SentAddress}}</p> </div> <div style=\"margin-bottom:5px;margin-left:5px;margin-right:5px\" class=\"row\"> <label>Category Driver</label> <bsselect ng-disabled=\"DisableTheDetailEditing\" id=\"comboBox\" ng-model=\"TheDriverDetail.DriverCategoryId\" data=\"optionsCategoryDriver\" item-text=\"DriverCategoryName\" item-value=\"DriverCategoryId\" on-select=\"SOTypeId\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> <div style=\"margin-bottom:15px;margin-left:5px;margin-right:5px\" class=\"row\"> <label>Driver</label> <bsselect ng-disabled=\"DisableTheDetailEditing\" id=\"comboBox\" ng-model=\"TheDriverDetail.DriverId\" data=\"optionsDriverThingy\" item-text=\"DriverPDSName\" item-value=\"DriverPDSId\" on-select=\"SOTypeId\" placeholder=\"\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"ui modal ModalMonitoringDriverListTask\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"MonitoringDriverListTaskTutupModal()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Apa Yang Ingin Anda Lakukan?</h4> </div> <br> <div class=\"modal-body\"> <a ng-click=\"MonitoringDriverAssign(Da_Selek_MonitoringDriver)\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Assign</li> </a> <a ng-click=\"MonitoringDriverDetail(Da_Selek_MonitoringDriver)\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Detail</li> </a> </div> <div class=\"modal-footer\"> <button ng-click=\"MonitoringDriverListTaskTutupModal()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales4/deliveryRequest/monitoringJadwalDriver/monitoringJadwalDriver.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "\t$('body .modals').remove();\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function navigateTab(containerId,divId){\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t$(\"#\"+containerId).children().each(function(n, i) {\r" +
    "\n" +
    "\t\tvar id = this.id;\r" +
    "\n" +
    "\t\tconsole.log(id+' - '+divId);\r" +
    "\n" +
    "\t\tif(id == divId)\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\t$('#'+divId).attr('class', 'tab-pane fade in active');\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t\telse\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\t$('#'+id).attr('class', 'tab-pane fade');\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t});\r" +
    "\n" +
    "}</script> <style type=\"text/css\">input[type=date]::-webkit-inner-spin-button {\r" +
    "\n" +
    "\t\t-webkit-appearance: none;\r" +
    "\n" +
    "\t\tdisplay: none;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "#chartdivPengirimanUnit {\r" +
    "\n" +
    "  width: 100%;\r" +
    "\n" +
    "  height: 500px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "#chartdivPengirimanKeBengkel {\r" +
    "\n" +
    "  width: 100%;\r" +
    "\n" +
    "  height: 500px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "#chartdivPengambilanUnitSelesaiRepair {\r" +
    "\n" +
    "  width: 100%;\r" +
    "\n" +
    "  height: 500px;\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/amcharts.js\"></script> <script type=\"text/javascript\" src=\"js/pie.js\"></script> <script type=\"text/javascript\" src=\"js/gauge.js\"></script> <script type=\"text/javascript\" src=\"js/funnel.js\"></script> <script type=\"text/javascript\" src=\"js/serial.js\"></script> <script type=\"text/javascript\" src=\"js/gantt.js\"></script> <script type=\"text/javascript\" src=\"js/light.js\"></script> <script type=\"text/javascript\" src=\"js/exportmin.js\"></script> <link rel=\"stylesheet\" href=\"css/export.css\" type=\"text/css\" media=\"all\"> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <div> <ul class=\"nav nav-tabs\" style=\"margin-top:10px\"> <li class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer_MonitoringJadwalDriver','Container_MonitoringJadwalDriver_PengirimanUnit')\">Pengiriman Unit</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer_MonitoringJadwalDriver','Container_MonitoringJadwalDriver_PengirimanKeBengkel')\">Pengiriman Ke Bengkel</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer_MonitoringJadwalDriver','Container_MonitoringJadwalDriver_PengambilanUnitSelesaiRepair')\">Pengambilan Unit Selesai Repair</a></li> </ul> <div id=\"tabContainer_MonitoringJadwalDriver\" class=\"tab-content\"> <div id=\"Container_MonitoringJadwalDriver_PengirimanUnit\" class=\"tab-pane fade in active\"> <div class=\"row\" style=\"margin-top:10px;margin-bottom:10px\"> <button class=\"rbtn btn ng-binding ladda-button\"> Jadwal Hari Ini </button> <button class=\"rbtn btn ng-binding ladda-button\"> Jadwal H+1 </button> <button class=\"rbtn btn ng-binding ladda-button\"> Jadwal H+2 </button> </div> <div class=\"row\" style=\"margin-top:10px;margin-bottom:10px\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"TabelMonitoringJadwalDriver_PengirimanUnit\" ui-grid-auto-resize></div> </div> <div class=\"row\" style=\"margin-top:10px\"> <p align=\"center\"> <i class=\"fa fa-2x fa fa-chevron-left\" ng-click=\"MonitoringJadwalDriverPengirimanUnitGeserKurang1()\"></i> {{GeserPengirimanUnit| date:\"dd MM yyyy\"}} <i class=\"fa fa-2x fa fa-chevron-right\" ng-click=\"MonitoringJadwalDriverPengirimanUnitGeserTambah1()\"></i> </p> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div id=\"chartdivPengirimanUnit\"></div> </div> </div> <div id=\"Container_MonitoringJadwalDriver_PengirimanKeBengkel\" class=\"tab-pane fade\"> <div class=\"row\" style=\"margin-top:10px;margin-bottom:10px\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"TabelMonitoringJadwalDriver_PengirimanKeBengkel\" ui-grid-auto-resize></div> </div> <div class=\"row\" style=\"margin-top:10px\"> <p align=\"center\"> <i class=\"fa fa-2x fa fa-chevron-left\" ng-click=\"MonitoringJadwalDriverPengirimanKeBengkelGeserKurang1()\"></i> {{GeserPengirimanKeBengkel| date:\"dd MM yyyy\"}} <i class=\"fa fa-2x fa fa-chevron-right\" ng-click=\"MonitoringJadwalDriverPengirimanKeBengkelGeserTambah1()\"></i> </p> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div id=\"chartdivPengirimanKeBengkel\"></div> </div> </div> <div id=\"Container_MonitoringJadwalDriver_PengambilanUnitSelesaiRepair\" class=\"tab-pane fade\"> <div class=\"row\" style=\"margin-top:10px;margin-bottom:10px\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"TabelMonitoringJadwalDriver_PengambilanUnitSelesaiRepair\" ui-grid-auto-resize></div> </div> <div class=\"row\" style=\"margin-top:10px\"> <p align=\"center\"> <i class=\"fa fa-2x fa fa-chevron-left\" ng-click=\"MonitoringJadwalDriverPengambilanUnitSelesaiRepairGeserKurang1()\"></i> {{GeserPengambilanUnitSelesaiRepair| date:\"dd MM yyyy\"}} <i class=\"fa fa-2x fa fa-chevron-right\" ng-click=\"MonitoringJadwalDriverPengambilanUnitSelesaiRepairGeserTambah1()\"></i> </p> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div id=\"chartdivPengambilanUnitSelesaiRepair\"></div> </div> </div> </div> </div>"
  );

}]);
