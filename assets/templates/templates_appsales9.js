angular.module('templates_appsales9', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/sales/sales9/master/ListProspect/ListProspect.html',
    "<script type=\"text/javascript\">$(\"#dropdownBtnOveride\").click(function(){\r" +
    "\n" +
    "\tif($('#dropdownContentOveride').css('display') == 'none')\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride').show();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\telse\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride').hide();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "});\r" +
    "\n" +
    "\r" +
    "\n" +
    "$( '#myButton' ).click( function() {\r" +
    "\n" +
    "    $( '#myModal' ).modal();\r" +
    "\n" +
    "  });</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"row filtersearch\" style=\"margin-right:0px;padding-bottom:5px;width: 100%\"> <div class=\"col-md-12\" style=\"padding-right:0px\"> <div class=\"input-group\" style=\"\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control ng-pristine ng-valid ng-empty ng-touched\" placeholder=\"Filter\" ng-model=\"gridApi.grid.columns[filterColIdx].filters[0].term\" style=\"\"> <div class=\"input-group-btn dropdown\" uib-dropdown=\"\" style=\"\"> <button id=\"filter\" type=\"button\" class=\"btn wbtn ng-binding dropdown-toggle\" uib-dropdown-toggle=\"\" data-toggle=\"dropdown\" tabindex=\"-1\" aria-haspopup=\"true\" aria-expanded=\"false\">Nama&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu=\"\" role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">Nama<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[1].filters[0].term!='' &amp;&amp;gridApi.grid.columns[1].filters[0].term!=undefined\"></i></span> </a> </li> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">No Handphone<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[2].filters[0].term!='' &amp;&amp;gridApi.grid.columns[2].filters[0].term!=undefined\"></i></span> </a> </li> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">No SPK Terakhir<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[3].filters[0].term!='' &amp;&amp;gridApi.grid.columns[3].filters[0].term!=undefined\"></i></span> </a> </li> </ul> </div> </div> <div class=\"formHeader\"> <div id=\"addMode\"> <button id=\"addBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; bottom: 10px; padding-bottom: 6px\">Tambah </button> </div> <div id=\"editMode\"> <button id=\"saveBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; bottom: 10px\">Simpan </button> <button id=\"cancelBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; bottom: 10px\">Batal </button> </div> </div> </div> </div> <br> <uib-tabset active=\"active\" id=\"templateDetailTab\"> <br> <uib-tab index=\"0\" heading=\"Suspect\"> </uib-tab> <uib-tab index=\"1\" heading=\"Prospect\"> <div id=\"formPanel\"> <div class=\"formContent\"> <bsreqlabel>Sales Program Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Sales Program Name\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Sales Address</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Sales Address\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Tanggal Activity Working Calendar</bsreqlabel> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\"> </bsdatepicker> </div> <div class=\"formGroupPanel\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"formGroupPanelBtn\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Informasi Kendaraan <span id=\"formGroupPanelIcon\" style=\"float:right; margin-right:3px\"> + </span> </button> <div class=\"formContent\"> <div class=\"list-group\" id=\"formGroupPanelContent\" aria-labelledby=\"dropdownMenu2\" style=\"display:none\"> <bsreqlabel>Nama Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Tipe Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Tipe Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Warna Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Warna Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </div> <div id=\"formData\"> <div class=\"listData\"> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\" style=\"\"> <div ng-repeat=\"datas in list\" class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 12px\">{{datas.Nama}}</p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">{{datas.Rate}}</li> <li><i class=\"fa fa-phone-square\" aria-hidden=\"true\" style=\"\"></i> </li> <li>{{datas.NoTlp}}</li> </ul> </p> <p style=\"font-size: 10px\">{{datas.Type}} </p> </div> <div class=\"col-sm-1\" style=\"padding-right:0px\"> <button class=\"btn btn-default\" style=\"float:right; border:0; padding-left:5px; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> </div> </div> <div class=\"modal fade\" id=\"myModal\" role=\"dialog\"> <div class=\"modal-content\" style=\"width:25%; margin-top:15%; margin-left:45%\"> <ul class=\"list-group\"> <a ng-click=\"LookSelected(da_data.SalesProgramName)\"><li class=\"list-group-item\">Lihat Detail</li></a> <a ng-click=\"ChangeSelected(da_data.SalesProgramId,da_data.SalesProgramName)\"><li class=\"list-group-item\">Ubah Detail</li></a> <a ng-click=\"DeleteSelected(da_data.SalesProgramId)\"><li class=\"list-group-item\">Hapus Detail</li></a> </ul> </div> </div> </div> </div> </div> </uib-tab> <uib-tab index=\"2\" heading=\"Pelanggan\"> </uib-tab> </uib-tabset>"
  );


  $templateCache.put('app/sales/sales9/master/accountAccessibleGL/accountAccessibleGL.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountAccessibleGLForm\" factory-name=\"AccountAccessibleGLFactory\" model=\"mAccountAccessibleGL\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountAccessibleGLForm\" form-title=\"Account Accessible GL\" modal-title=\"Account Accessible GL\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Name\" placeholder=\"Name\" ng-model=\"mAccountAccessibleGL.Name\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Value\" placeholder=\"Value\" ng-model=\"mAccountAccessibleGL.Value\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Value\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountBankList/accountBankList.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountBankListForm\" factory-name=\"AccountBankListFactory\" model=\"mAccountBankList\" model-id=\"BankId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountBankListForm\" form-title=\"Account Bank List\" modal-title=\"Account Bank List\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bank Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"BankName\" placeholder=\"BankName\" ng-model=\"mAccountBankList.BankName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BankName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountGL/accountGL.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountGLForm\" factory-name=\"AccountGLFactory\" model=\"mAccountGL\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountGLForm\" form-title=\"Account GL\" modal-title=\"Account GL\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>GL Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"GLName\" placeholder=\"GLName\" ng-model=\"mAccountGL.GLName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GLName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Value\" placeholder=\"Value\" ng-model=\"mAccountGL.Value\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Value\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bslabel>gl group id</bslabel> <bsselect name=\"gl_group_id\" ng-model=\"mAccountGL.gl_group_id\" data=\"optionsgl_group_id\" item-text=\"gl_group_name\" item-value=\"gl_group_id\" on-select=\"value(selected)\" placeholder=\"Select gl group\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"gl_group_id\"></bserrmsg> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountGroup/accountGroup.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountGroupForm\" factory-name=\"AccountGroupFactory\" model=\"mAccountGroup\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountGroupForm\" form-title=\"Account Group\" modal-title=\"Account Group\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Account Group Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"account_group_code\" placeholder=\"Account Group Code\" ng-model=\"mAccountGroup.account_group_code\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"account_group_code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Account Group Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"account_group_name\" placeholder=\"Account Group Name\" ng-model=\"mAccountGroup.account_group_name\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"account_group_name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bslabel>Account Type</bslabel> <bsselect name=\"account_type_id\" ng-model=\"mAccountGroup.account_type_id\" data=\"optionsAccountType\" item-text=\"account_type_name\" item-value=\"account_type_id\" on-select=\"value(selected)\" placeholder=\"Select Account Type\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"account_type_id\"></bserrmsg> <div class=\"form-group\" show-errors> <bsreqlabel>Account Format</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"account_format\" placeholder=\"Account Format\" ng-model=\"mAccountGroup.account_format\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"account_format\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountGroupCOARelations/accountGroupCOARelations.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountGroupCOARelationsForm\" factory-name=\"AccountGroupCOARelationsFactory\" model=\"mAccountGroupCOARelations\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountGroupCOARelationsForm\" form-title=\"Account Group COA Relations\" modal-title=\"Account Group COA Relations\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Name\" placeholder=\"Name\" ng-model=\"mAccountGroupCOARelations.Name\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Value\" placeholder=\"Value\" ng-model=\"mAccountGroupCOARelations.Value\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Value\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountGroupGL/accountGroupGL.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "       -moz-border-radius: 4px;\r" +
    "\n" +
    "            border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;\r" +
    "\n" +
    "}</style> <bsform-grid ng-form=\"AccountGLGroupForm\" factory-name=\"AccountGLGroupFactory\" model=\"mAccountGLGroup\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountGLGroupForm\" form-title=\"Account Group GL\" modal-title=\"Account Group GL\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>GL Group Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"gl_group_code\" placeholder=\"GL Group Code\" ng-model=\"mAccountGLGroup.gl_group_code\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"gl_group_code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>GL Group Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"gl_group_name\" placeholder=\"GL Group Name\" ng-model=\"mAccountGLGroup.gl_group_name\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"gl_group_name\"></bserrmsg> </div> <div class=\"form-group col-md-6 col-xs-5\" show-errors style=\"padding-left:0 !important\"> <bslabel>AccountingCoa</bslabel> <bsselect name=\"AccountingCoa\" ng-model=\"DropdownAccountingCoa\" data=\"optionsAccountingCoa\" item-text=\"GLAccountName\" item-value=\"GLAccountId\" on-select=\"SetSelectedGLAccount(selected)\" placeholder=\"Select Accounting Coa\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"AccountingCoa\"></bserrmsg> <div class=\"col-md-6 col-xs-1\" style=\"padding-left:0 !important\"> <button ng-click=\"AddAccountingCoa()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>GL Account Id</th> <th>GL Account Code</th> <th>GL Account Name</th> </tr> <tr ng-repeat=\"ListAccountingCoa in mAccountGLGroup.AccountingCoa track by $index\"> <td>{{ListAccountingCoa.GLAccountId}}</td> <td>{{ListAccountingCoa.GLAccountCode}}</td> <td>{{ListAccountingCoa.GLAccountName}}</td> <td><a ng-click=\"RemoveAccountingCoa($index)\">Remove</a></td> </tr> </tbody> </table> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountList/accountList.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountListForm\" factory-name=\"AccountListFactory\" model=\"mAccountList\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountListForm\" form-title=\"Account Group GL\" modal-title=\"Account Group GL\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Account Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"account_code\" placeholder=\"Account Code\" ng-model=\"mAccountList.account_code\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"account_code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Account Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"account_name\" placeholder=\"Account Name\" ng-model=\"mAccountList.account_name\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"account_name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Balance</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Balance\" placeholder=\"Balance\" ng-model=\"mAccountList.Balance\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Balance\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Blocked Balance</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"blocked_balance\" placeholder=\"Blocked Balance\" ng-model=\"mAccountList.blocked_balance\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"blocked_balance\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Balance Type</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"balance_type\" placeholder=\"Balance Type\" ng-model=\"mAccountList.balance_type\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"balance_type\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Routing Format</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"routing_format\" placeholder=\"Routing Format\" ng-model=\"mAccountList.routing_format\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"routing_format\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Routing Reset</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"routing_reset\" placeholder=\"Routing Reset\" ng-model=\"mAccountList.routing_reset\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"routing_reset\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Account Owner</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"account_owner\" placeholder=\"Account Owner\" ng-model=\"mAccountList.account_owner\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"account_owner\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bslabel>account_group_id</bslabel> <bsselect name=\"account_group_id\" ng-model=\"mAccountList.account_group_id\" data=\"optionsaccount_group_id\" item-text=\"account_group_name\" item-value=\"account_group_id\" on-select=\"value(selected)\" placeholder=\"Select account group\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"account_group_id\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>bank_id</bslabel> <bsselect name=\"bank_id\" ng-model=\"mAccountList.bank_id\" data=\"optionsbank_id\" item-text=\"bank_name\" item-value=\"bank_id\" on-select=\"value(selected)\" placeholder=\"Select bank\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"bank_id\"></bserrmsg> <div class=\"form-group\" show-errors> <bsreqlabel>Bank Checking Account Plafond</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"bank_checking_account_plafond\" placeholder=\"Bank Checking Account Plafond\" ng-model=\"mAccountList.bank_checking_account_plafond\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"bank_checking_account_plafond\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Bank Address</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"bank_address\" placeholder=\"Bank Address\" ng-model=\"mAccountList.bank_address\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"bank_address\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Bank Contact Person Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"bank_contact_person_name\" placeholder=\"Bank Contact Person Name\" ng-model=\"mAccountList.bank_contact_person_name\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"bank_contact_person_name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Bank Contact Person Phone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"bank_contact_person_phone\" placeholder=\"Bank Contact Person Phone\" ng-model=\"mAccountList.bank_contact_person_phone\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"bank_contact_person_phone\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Bank Contact Person Fax</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"bank_contact_person_fax\" placeholder=\"Bank Contact Person Fax\" ng-model=\"mAccountList.bank_contact_person_fax\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"bank_contact_person_fax\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Bank Contact Person HP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"bank_contact_person_hp\" placeholder=\"Bank Contact Person HP\" ng-model=\"mAccountList.bank_contact_person_hp\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"bank_contact_person_hp\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Bank Contact Person Email</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"bank_contact_person_email\" placeholder=\"Bank Contact Person Email\" ng-model=\"mAccountList.bank_contact_person_email\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"bank_contact_person_email\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>description</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"description\" placeholder=\"description\" ng-model=\"mAccountList.description\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"description\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountListSequence/accountListSequence.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountListSequenceForm\" factory-name=\"AccountListSequenceFactory\" model=\"mAccountListSequence\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountListSequenceForm\" form-title=\"Account Group GL\" modal-title=\"Account Group GL\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Routing Distinct</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"routing_distinct\" placeholder=\"Routing Distinct\" ng-model=\"mAccountListSequence.routing_distinct\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"routing_distinct\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Routing Sequence</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"routing_sequence\" placeholder=\"Routing Sequence\" ng-model=\"mAccountListSequence.routing_sequence\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"routing_sequence\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountingCOA/accountingCOA.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountingCOAForm\" factory-name=\"AccountingCOAFactory\" model=\"mAccountingCOA\" model-id=\"GLAccountId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountingCOAForm\" form-title=\"AccountingCOA\" modal-title=\"Accounting COA\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>GL Account Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"GLAccountCode\" placeholder=\"GL Account Code\" ng-model=\"mAccountingCOA.GLAccountCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GLAccountCode\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>GL Account Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"GLAccountName\" placeholder=\"GL Account Name\" ng-model=\"mAccountingCOA.GLAccountName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GLAccountName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountingGlType/accountingGlType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountingGlTypeForm\" factory-name=\"AccountingGlTypeFactory\" model=\"mAccountingGlType\" model-id=\"AccountingGlTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountingGlTypeForm\" form-title=\"AccountingGlType\" modal-title=\"Accounting Gl Type\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>GL Type Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"GLTypeName\" placeholder=\"GL Type Name\" ng-model=\"mAccountingGlType.GLTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GLTypeName\"></bserrmsg> </div> <div class=\"form-group col-md-6 col-xs-5\" show-errors style=\"padding-left:0 !important\"> <bslabel>AccountingCoa</bslabel> <bsselect name=\"AccountingCoa\" ng-model=\"DropdownAccountingCoa\" data=\"optionsAccountingCoa\" item-text=\"GLAccountName\" item-value=\"GLAccountId\" on-select=\"SetSelectedGLAccount(selected)\" placeholder=\"Select Accounting Coa\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"AccountingCoa\"></bserrmsg> <div class=\"col-md-6 col-xs-1\" style=\"padding-left:0 !important\"> <button ng-click=\"AddAccountingCoa()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>GL Account Id</th> <th>GL Account Code</th> <th>GL Account Name</th> </tr> <tr ng-repeat=\"ListAccountingCoa in mAccountingGlType.AccountingCoa track by $index\"> <td>{{ListAccountingCoa.GLAccountId}}</td> <td>{{ListAccountingCoa.GLAccountCode}}</td> <td>{{ListAccountingCoa.GLAccountName}}</td> <td><a ng-click=\"RemoveAccountingCoa($index)\">Remove</a></td> </tr> </tbody> </table> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountingInventoryGroup/accountingInventoryGroup.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountingInventoryGroupForm\" factory-name=\"AccountingInventoryGroupFactory\" model=\"mAccountingInventoryGroup\" model-id=\"InventoryGroupId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountingInventoryGroupForm\" form-title=\"AccountingInventoryGroup\" modal-title=\"Accounting Inventory Group\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bslabel>Inventory Type Name</bslabel> <bsselect name=\"InventoryTypeId\" ng-model=\"mAccountingInventoryGroup.InventoryTypeId\" data=\"optionsInventoryTypeId\" item-text=\"InventoryTypeName\" item-value=\"InventoryTypeId\" on-select=\"value(selected)\" placeholder=\"Select Inventory Type\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"InventoryTypeId\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>Inventory GL Account Id</bslabel> <bsselect name=\"InventoryGLAccountId\" ng-model=\"mAccountingInventoryGroup.InventoryGLAccountId\" data=\"optionsInventoryGLAccountId\" item-text=\"GLAccountName\" item-value=\"GLAccountId\" on-select=\"value(selected)\" placeholder=\"Select Inventory GL Account\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"InventoryGLAccountId\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>Purchase GL Account Name</bslabel> <bsselect name=\"PurchaseGLAccountId\" ng-model=\"mAccountingInventoryGroup.PurchaseGLAccountId\" data=\"optionsInventoryGLAccountId\" item-text=\"GLAccountName\" item-value=\"GLAccountId\" on-select=\"value(selected)\" placeholder=\"Select Purchase GL Account\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"PurchaseGLAccountId\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>Selling GL Account Name</bslabel> <bsselect name=\"SellingGLAccountId\" ng-model=\"mAccountingInventoryGroup.SellingGLAccountId\" data=\"optionsInventoryGLAccountId\" item-text=\"GLAccountName\" item-value=\"GLAccountId\" on-select=\"value(selected)\" placeholder=\"Select selling GL Account\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"SellingGLAccountId\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>Adjustment Defect GL Account Name</bslabel> <bsselect name=\"AdjustmentDefectGLAccountId\" ng-model=\"mAccountingInventoryGroup.AdjustmentDefectGLAccountId\" data=\"optionsInventoryGLAccountId\" item-text=\"GLAccountName\" item-value=\"GLAccountId\" on-select=\"value(selected)\" placeholder=\"Select adjustment defect\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"AdjustmentDefectGLAccountId\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>Adjustment Lost GL Account Name</bslabel> <bsselect name=\"AdjustmentLostGLAccountId\" ng-model=\"mAccountingInventoryGroup.AdjustmentLostGLAccountId\" data=\"optionsInventoryGLAccountId\" item-text=\"GLAccountName\" item-value=\"GLAccountId\" on-select=\"value(selected)\" placeholder=\"Select adjustment lost\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"AdjustmentLostGLAccountId\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>Adjustment Found GL Account Name</bslabel> <bsselect name=\"AdjustmentFountGLAccountId\" ng-model=\"mAccountingInventoryGroup.AdjustmentFountGLAccountId\" data=\"optionsInventoryGLAccountId\" item-text=\"GLAccountName\" item-value=\"GLAccountId\" on-select=\"value(selected)\" placeholder=\"Select adjustment found\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"AdjustmentFountGLAccountId\"></bserrmsg> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountingInventoryType/accountingInventoryType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountingInventoryTypeForm\" factory-name=\"AccountingInventoryTypeFactory\" model=\"mAccountingInventoryType\" model-id=\"InventoryTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountingInventoryTypeForm\" form-title=\"AccountingInventoryType\" modal-title=\"Accounting Inventory Type\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Inventory Type Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"InventoryTypeName\" placeholder=\"Inventory Type Name\" ng-model=\"mAccountingInventoryType.InventoryTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"InventoryTypeName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/accountingTransactionCode/accountingTransactionCode.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AccountingTransactionCodeForm\" factory-name=\"AccountingTransactionCodeFactory\" model=\"mAccountingTransactionCode\" model-id=\"AccountingTransactionCodeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccountingTransactionCodeForm\" form-title=\"AccountingTransactionCode\" modal-title=\"Accounting Transaction Code\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Transaction Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"transactionCode\" placeholder=\"Transaction Code\" ng-model=\"mAccountingTransactionCode.TransactionCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"transactionCode\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bslabel>GLTypeDebetId</bslabel> <bsselect name=\"GLTypeDebetId\" ng-model=\"mAccountingTransactionCode.GLTypeDebetId\" data=\"optionsGLTypeDebetId\" item-text=\"GLTypeName\" item-value=\"GLTypeId\" on-select=\"value(selected)\" placeholder=\"Select Accounting Coa\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"GLTypeDebetId\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>GLTypeCreditId</bslabel> <bsselect name=\"GLTypeCreditId\" ng-model=\"mAccountingTransactionCode.GLTypeCreditId\" data=\"optionsGLTypeCreditId\" item-text=\"GLTypeName\" item-value=\"GLTypeId\" on-select=\"value(selected)\" placeholder=\"Select Accounting Coa\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"GLTypeCreditId\"></bserrmsg> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityAlasanBatalPembelianKendaraan/activityAlasanBatalPembelianKendaraan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityAlasanBatalPembelianKendaraanForm\" factory-name=\"ActivityAlasanBatalPembelianKendaraanFactory\" model=\"mActivityAlasanBatalPembelianKendaraan\" model-id=\"ReasonCancelBuyId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityAlasanBatalPembelianKendaraanForm\" form-title=\"Alasan Batal Beli Kendaraan\" modal-title=\"Activity Alasan Batal PembelianKendaraan\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alasan Batal Pembelian Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"mReasonCancelBuyName\" placeholder=\"Alasan Batal Pembelian Kendaraan\" ng-model=\"mActivityAlasanBatalPembelianKendaraan.ReasonCancelBuyName\" cols=\"50\" rows=\"5\" required>\r" +
    "\n" +
    "                    </textarea></div> <bserrmsg field=\"mReasonCancelBuyName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityAlasanDropProspek/activityAlasanDropProspek.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityAlasanDropProspekForm\" factory-name=\"ActivityAlasanDropProspekFactory\" model=\"mActivityAlasanDropProspek\" model-id=\"ReasonDropProspectId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityAlasanDropProspekForm\" form-title=\"Activity Alasan Drop Prospek\" modal-title=\"Activity Alasan Drop Prospek\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alasan Drop Prospect</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Alasan Drop Prospect\" ng-model=\"mActivityAlasanDropProspek.ReasonDropProspectName\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "                    </textarea></div> <bserrmsg field=\"NamaAlasanDropProspek\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityAlasanPengecualianPengiriman/activityAlasanPengecualianPengiriman.html',
    "<link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityAlasanPengecualianPengirimanForm\" factory-name=\"ActivityAlasanPengecualianPengiriman\" model=\"mActivityAlasanPengecualianPengiriman\" model-id=\"DeliveryExcReasonId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityAlasanPengecualianPengirimanForm\" form-title=\"Activity Alasan Pengecualian Pengiriman\" modal-title=\"Activity Alasan Pengecualian Pengiriman\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alasan Pengecualian Pengiriman</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"NamaAlasanPengecualianPengiriman\" placeholder=\"Alasan Pengecualian Pengiriman\" ng-model=\"mActivityAlasanPengecualianPengiriman.DeliveryExcReasonName\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "                </textarea></div> <bserrmsg field=\"DeliveryExcReasonName\"></bserrmsg> </div> <div class=\"form-group\"> <div class=\"input-icon-right\"> <label>Admin</label> <div class=\"switch\"> <input id=\"cmn-toggle-5\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\" ng-model=\"mActivityAlasanPengecualianPengiriman.InputByAdminBit\"> <label for=\"cmn-toggle-5\"></label> </div> </div> </div> <div class=\"form-group\"> <div class=\"input-icon-right\"> <label>Salesman</label> <div class=\"switch\"> <input id=\"cmn-toggle-6\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\" ng-model=\"mActivityAlasanPengecualianPengiriman.InputBySalesmanBit\"> <label for=\"cmn-toggle-6\"></label> </div> </div> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityChecklistItemAdministrasiPembayaran/activityChecklistItemAdministrasiPembayaran.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityChecklistItemAdministrasiPembayaranForm\" factory-name=\"ActivityChecklistItemAdministrasiPembayaran\" model=\"mActivityChecklistItemAdministrasiPembayaran\" model-id=\"ChecklistAdminPaymentId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityChecklistItemAdministrasiPembayaranForm\" form-title=\"Activity Checklist Item Administrasi\" modal-title=\"Checklist Item Administrasi\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Checklist Item Administrasi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Checklist Item Administrasi\" ng-model=\"mActivityChecklistItemAdministrasiPembayaran.ChecklistAdminPaymentName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityChecklistItemDEC/activityChecklistItemDec.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityChecklistItemDecForm\" factory-name=\"ActivityChecklistItemDecFactory\" model=\"mactivityChecklistItemDec\" model-id=\"ChecklistItemDECId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityChecklisItemtDecForm\" form-title=\"Checklis Item Dec\" modal-title=\"ActivityChecklisItemtDec\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- <bsform-advsearch>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-4\">\r" +
    "\n" +
    "                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                        <label>Model</label>\r" +
    "\n" +
    "                        <bsselect name=\"model\" data=\"optionsModel\" ng-change=\"filtertipe(filterItemDEC.VehicleModelId)\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"filterItemDEC.VehicleModelId\" placeholder=\"PIlih Model\" icon=\"fa fa-car\"\r" +
    "\n" +
    "                            style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                        <bserrmsg field=\"model\"></bserrmsg>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-4\">\r" +
    "\n" +
    "                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                        <label>Tipe</label>\r" +
    "\n" +
    "                        <bsselect name=\"type\" data=\"optionTipe\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"filterItemDEC.VehicleTypeId\" placeholder=\"Pilih Tipe\"\r" +
    "\n" +
    "                            icon=\"fa fa-car\" style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                        <bserrmsg field=\"type\"></bserrmsg>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </bsform-advsearch> --> <div class=\"row\"> <div class=\"col-md-4\"> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Model</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"model\" data=\"optionsModel\" ng-change=\"filtertipe(mactivityChecklistItemDec.VehicleModelId)\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"mactivityChecklistItemDec.VehicleModelId\" placeholder=\"PIlih Model\" icon=\"fa fa-car\"\r" +
    "\n" +
    "                        style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                    <bserrmsg field=\"model\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Tipe</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"type\" data=\"optionTipe\" ng-change=\"getDEC()\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"mactivityChecklistItemDec.VehicleTypeId\" placeholder=\"Pilih Tipe\"\r" +
    "\n" +
    "                        icon=\"fa fa-car\" style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                    <bserrmsg field=\"type\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Group Checklist DEC</bsreqlabel> <bsselect name=\"parentRole\" data=\"GroupDECData\" item-text=\"GroupChecklistDECName\" item-value=\"GroupChecklistDECId\" ng-model=\"mactivityChecklistItemDec.GroupChecklistDECId\" on-selected=\"Bebas(Selected)\" placeholder=\"Nama Group Checklist DEC\" icon=\"fa fa-search glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"parentRole\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Checklist DEC</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ChecklistItemDECName\" placeholder=\"Nama Checklist DEC\" ng-model=\"mactivityChecklistItemDec.ChecklistItemDECName\" ng-maxlength=\"100\" required> </div> <bserrmsg field=\"ChecklistItemDECName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityChecklistItemInspeksi/activityChecklistItemInspeksi.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityChecklistItemInspeksiForm\" factory-name=\"ActivityChecklistItemInspeksi\" model=\"mActivityChecklistItemInspeksi\" model-id=\"ChecklistItemInspectionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityChecklistItemInspeksiForm\" form-title=\"Activity Checklist Item Inspeksi\" modal-title=\"Activity ChecklistItemInspeksi\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Group Checklist Item Inspeksi</bsreqlabel> <bsselect name=\"parentRole\" data=\"GroupInspeksi\" item-text=\"GroupChecklistInspectionName\" item-value=\"GroupChecklistInspectionId\" icon=\"fa fa-search glyph-left\" ng-model=\"mActivityChecklistItemInspeksi.GroupChecklistInspectionId\" placeholder=\"Nama Group Checklist Item Inspeksi\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"parentRole\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Checklist Item Inspeksi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Checklist Item Inspeksi\" ng-model=\"mActivityChecklistItemInspeksi.ChecklistItemInspectionName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityGroupChecklistDec/activityGroupChecklistDec.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityGroupChecklistDECForm\" factory-name=\"NewActivityGroupChecklistDECFactory\" model=\"mNewActivityGroupChecklistDEC\" model-id=\"GroupChecklistDECId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityGroupChecklistDECForm\" form-title=\" \" modal-title=\"Activity Group Checklist DEC\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input alpha-numeric mask=\"Huruf\" type=\"text\" class=\"form-control\" name=\"groupChecklistDECName\" placeholder=\"Nama\" ng-model=\"mNewActivityGroupChecklistDEC.GroupChecklistDECName\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"groupChecklistDECName\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Model Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"ModelKendaraan\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"mNewActivityGroupChecklistDEC.VehicleModelId\" ng-change=\"filterModel()\" placeholder=\"Model Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                    <bserrmsg field=\"ModelKendaraan\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                 <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Type Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"TypeKendaraan\" data=\"getTipe\" item-text=\"Description\" item-value=\"VehicleTypeId\" ng-model=\"mNewActivityGroupChecklistDEC.VehicleTypeId\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                    <bserrmsg field=\"TypeKendaraan\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityGroupChecklistInspeksi/activityGroupChecklistInspeksi.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityGroupChecklistInspeksiForm\" factory-name=\"ActivityGroupChecklistInspeksi\" model=\"mActivityGroupChecklistInspeksi\" model-id=\"GroupChecklistInspectionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityGroupChecklistInspeksiForm\" form-title=\"Activity Group Checklist Inspeksi\" modal-title=\"Activity Group Checklist Inspeksi\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Group Checklist Inspeksi Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Group Checklist Inspeksi Name\" ng-model=\"mActivityGroupChecklistInspeksi.GroupChecklistInspectionName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Model Kendaraan</bsreqlabel> <bsselect name=\"ModelKendaraan\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"mActivityGroupChecklistInspeksi.VehicleModelId\" ng-change=\"filterModel()\" placeholder=\"Model Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"ModelKendaraan\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Type Kendaraan</bsreqlabel> <bsselect name=\"TypeKendaraan\" data=\"getTipe\" item-text=\"Description\" item-value=\"VehicleTypeId\" ng-model=\"mActivityGroupChecklistInspeksi.VehicleTypeId\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"TypeKendaraan\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Description</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"Description\" placeholder=\"Description\" ng-model=\"mActivityGroupChecklistInspeksi.GroupChecklistInspectionDescription\" cols=\"50\" rows=\"5\" required>\r" +
    "\n" +
    "                    </textarea></div> <bserrmsg field=\"Description\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityMappingKendaraanTestDrive/activityMappingKendaraanTestDrive.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityMappingKendaraanTestDriveForm\" factory-name=\"ActivityMappingKendaraanTestDrive\" model=\"mActivityMappingKendaraanTestDrive\" model-id=\"MappingKendaraanTestDriveId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityMappingKendaraanTestDriveForm\" form-title=\"Activity Mapping Kendaraan Test Drive\" modal-title=\"Activity Mapping Kendaraan Test Drive\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Test Drive Unit</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TestDriveUnitId\" placeholder=\"Test Drive Unit Id\" ng-model=\"mActivityMappingKendaraanTestDrive.TestDriveUnitId\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"TestDriveUnitId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Cabang Pemilik</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"BranchOwnerId\" placeholder=\"Branch Owner Id\" ng-model=\"mActivityMappingKendaraanTestDrive.BranchOwnerId\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BranchOwnerId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Cabang yang meminta</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"BranchRequestorId\" placeholder=\"Branch Requestor Id\" ng-model=\"mActivityMappingKendaraanTestDrive.BranchRequestorId\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BranchRequestorId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Batas Waktu</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"LimitTime\" placeholder=\"Limit Time\" ng-model=\"mActivityMappingKendaraanTestDrive.LimitTime\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"LimitTime\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Batas Booking Sehari</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"LimitBookingInADay\" placeholder=\"Limit Booking In A Day\" ng-model=\"mActivityMappingKendaraanTestDrive.LimitBookingInADay\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"LimitBookingInADay\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/activityPreDefineRespond/activityPreDefineRespond.html',
    "<link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\">$(\"#ActivityPredefineRespondPreventPercent\").keypress(function(e) {\r" +
    "\n" +
    "  if (e.which === 37) { // 'w'\r" +
    "\n" +
    "    e.preventDefault();\r" +
    "\n" +
    "  }\r" +
    "\n" +
    "});</script> <style type=\"text/css\">.ui-grid-viewport {\r" +
    "\n" +
    "        overflow-anchor: none;\r" +
    "\n" +
    "    }</style> <div ng-show=\"ActivityPreDefineRespond\" class=\"row\" style=\"height:100vh;margin-left:0px;margin-right:0px\"> <button style=\"float: right\" ng-click=\"TambahActivityPredefineRespond()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <i class=\"fa fa-plus\" style=\"margin-right:5px\"></i> Level 1 </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"DeleteActivityPredefineRespond()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Hapus Data </span><span class=\"ladda-spinner\"></span> </button> <br> <br> <div style=\"display: block\" class=\"grid\" ui-grid=\"grid\" ui-grid-cellnav ui-grid-selection ui-grid-auto-resize ui-grid-pagination></div> <br> </div> <div ng-show=\"ShowActivityPreDefineRespondDetail\" class=\"row\" style=\"height:100vh;margin-left:0px;margin-right:0px\"> <!--{{ActivityPredefineRespondCurrentLevel}}--> <button style=\"float: right\" ng-disabled=\"(ActivityPredefineRespondCurrentLevel=='Level 3' && OperationActivityPredefineRespond=='Update' && (mActivityPreDefineRespond.EndRespond==undefined ||mActivityPreDefineRespond.EndRespond==null))||(OperationActivityPredefineRespond=='Insert' && ActivityPredefineRespondEndRespondNoNull==true && (mActivityPreDefineRespond.EndRespond==null||mActivityPreDefineRespond.EndRespond==undefined))||formValidBuat.$invalid\" ng-show=\"ShowSimpanActivityPredefineRespond\" ng-click=\"SimpanActivityPredefineRespond()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliDariActivityPredefineRespond()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> <form name=\"formValidBuat\" novalidate> <bsreqlabel style=\"margin-top:30px\">Tipe Aktivitas</bsreqlabel> <bsselect ng-disabled=\"!ActivityPredefineRespondEnableTheDetails_TipeAktivitas\" name=\"TaskAktivitas\" data=\"optionsActivityType\" item-text=\"ActivityTypeName\" item-value=\"ActivityTypeId\" ng-model=\"mActivityPreDefineRespond.ActivityTypeId\" placeholder=\"Task Aktivitas\" style=\"min-width: 150px\" on-select=\"ActivityPredefineDaFilterThing(selected)\" required> </bsselect> <bsreqlabel style=\"margin-top:10px\" ng-show=\"ActivityPredefineRespondDaTipeTugasIlang\">Tipe Tugas</bsreqlabel> <bsselect ng-disabled=\"!ActivityPredefineRespondEnableTheDetails_TipeTugas || mActivityPreDefineRespond.ActivityTypeId==null || mActivityPreDefineRespond.ActivityTypeId==undefined \" ng-show=\"ActivityPredefineRespondDaTipeTugasIlang\" name=\"TypeAktivitas\" data=\"optionsActivityTaskType\" item-text=\"TaskTypeName\" item-value=\"TaskTypeId\" ng-model=\"mActivityPreDefineRespond.TaskTypeId\" placeholder=\"Type Aktivitas\" style=\"min-width: 150px\" on-select=\"ActivityPredefineDaFilterThing(selected)\" required> </bsselect> <label style=\"margin-top:10px\">Parent Predefine Respond</label> <bsselect ng-disabled=\"!ActivityPredefineRespondEnableTheDetails_ParentPredefineRespond || optionsParentRespond.length<=0\" name=\"ParentId\" data=\"optionsParentRespond\" item-text=\"ActivityPreDefineRespondName\" item-value=\"ActivityPreDefineRespondId\" ng-model=\"mActivityPreDefineRespond.ParentId\" placeholder=\"Parent predefine respond\" style=\"min-width: 150px\"> </bsselect> <bsreqlabel style=\"margin-top:10px\">Predefine Respond Name</bsreqlabel> <input id=\"ActivityPredefineRespondPreventPercent\" style=\"margin-bottom:10px\" ng-disabled=\"!ActivityPredefineRespondEnableTheDetails_PredefineRespondName\" type=\"text\" class=\"form-control\" name=\"NamaPreDefine\" placeholder=\"PreDefine Respond Name\" ng-model=\"mActivityPreDefineRespond.ActivityPreDefineRespondName\" maxlength=\"50\" required> <bslabel style=\"margin-top:10px\">End Respond</bslabel> <bsselect ng-disabled=\"!ActivityPredefineRespondEnableTheDetails_EndRespond\" data=\"optionsActivityPredefineRespondEndRespond\" item-text=\"SearchOptionName\" item-value=\"SearchOptionId\" ng-model=\"mActivityPreDefineRespond.EndRespond\" placeholder=\"End respond\" style=\"min-width: 150px\"> </bsselect> </form> </div> <div class=\"ui modal ModalActivityPreDefineRespondHapus\" role=\"dialog\"> <ul class=\"swal2-progresssteps\" style=\"display: none\"></ul> <div class=\"swal2-icon swal2-error\" style=\"display: none\"> <span class=\"x-mark\"> <span class=\"line left\"> </span> <span class=\"line right\"> </span> </span> </div> <div class=\"swal2-icon swal2-question\" style=\"display: none\">?</div> <div class=\"swal2-icon swal2-warning\" style=\"display: block\">!</div> <div class=\"swal2-icon swal2-info\" style=\"display: none\">i</div> <div class=\"swal2-icon swal2-success\" style=\"display: none\"> <span class=\"line tip\"> </span> <span class=\"line long\"> </span> <div class=\"placeholder\"> </div> <div class=\"fix\"> </div> </div> <img class=\"swal2-image\" style=\"display: none\"> <h2 class=\"swal2-title\" id=\"swal2-title\"> <p align=\"center\" style=\"font-size:20px\">Delete <span style=\"color:red\">{{ActivityPreDefineRespondSelctedRow.length}} item(s) </span>from <span style=\"color:blue\">Activity Predefine Respond.</span></p> <p align=\"center\">Anda Yakin?</p> </h2> <div id=\"swal2-content\" class=\"swal2-content\" style=\"display: block\" align=\"center\">Tidak dapat kembali ke kondisi sebelumnya!</div> <div align=\"center\" class=\"swal2-buttonswrapper\" style=\"display: block;margin-top:10px;margin-bottom:10px\"> <button ng-click=\"HapusActivityPreDefineRespondOk()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-confirm swal2-styled\" style=\"background-color: rgb(140, 212, 245); border-left-color: rgb(140, 212, 245); border-right-color: rgb(140, 212, 245);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">OK</button> <button ng-click=\"HapusActivityPreDefineRespondCancel()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-cancel swal2-styled\" style=\"display: inline-block; background-color: rgb(170, 170, 170);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">Cancel</button></div> </div> <div class=\"ui modal ModalActivityPredefineRespondPeringatanAnak\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"ModalActivityPredefineRespondPeringatanAnakBatal()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Peringatan</h4> </div> <br> <br> <div class=\"modal-body\"> Data yang akan dihapus memiliki level dibawahnya, jika dihapus level dibawahnya akan ikut terhapus. Apakah anda yakin akan menghapus data ini? </div> <div class=\"modal-footer\"> <button ng-click=\"ModalActivityPredefineRespondPeringatanAnakLanjut()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"ModalActivityPredefineRespondPeringatanAnakBatal()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/activityWorkingCalender/activityWorkingCalender.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ActivityWorkingCalenderForm\" factory-name=\"ActivityWorkingCalender\" model=\"mActivityWorkingCalender\" model-id=\"WorkingCalenderId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ActivityWorkingCalenderForm\" form-title=\"Activity Working Calender\" modal-title=\"Activity Working Calender\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Activity Working Calender</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Activity Working Calender\" ng-model=\"mActivityWorkingCalender.WorkingCalenderName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Activity Working Calender</bsreqlabel> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\" ng-model=\"mActivityWorkingCalender.WorkingCalenderDate\"> </bsdatepicker> <bserrmsg field=\"tanggal\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Keterangan Activity Working Calendar</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"keterangan\" placeholder=\"Keterangan Activity Working Calender\" ng-model=\"mActivityWorkingCalender.WorkingCalenderDesc\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"keterangan\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/agama/agama.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AgamaForm\" factory-name=\"AgamaFactoryMaster\" model=\"mAgama\" model-id=\"CustomerReligionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AgamaForm\" form-title=\"Agama\" modal-title=\"Master Agama\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Agama</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CustomerReligionName\" placeholder=\"Input Agama\" ng-model=\"mAgama.CustomerReligionName\" maxlength=\"15\" alpha-numeric mask=\"Huruf\" required> </div> <bserrmsg field=\"CustomerReligionName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/alasan/alasan.html',
    "<script type=\"text/javascript\">// var MasterAlasanHuruf = document.getElementById('AlasanDaAlasan');\r" +
    "\n" +
    "\t// MasterAlasanHuruf.onkeydown = function (e) {\r" +
    "\n" +
    "\t// \tif ((e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode == 8) || (e.keyCode == 32) || (e.keyCode >= 37 && e.keyCode <= 40)) {\r" +
    "\n" +
    "\t// \t\t//titik tu 190 buat decimal\r" +
    "\n" +
    "\t// \t\treturn true;\r" +
    "\n" +
    "\t// \t}\r" +
    "\n" +
    "\t// \telse {\r" +
    "\n" +
    "\t// \t\treturn false;\r" +
    "\n" +
    "\t// \t}\r" +
    "\n" +
    "\t// }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t// (function($){var w=($.browser.msie?'paste':'input')+\".mask\";var x=(window.orientation!=undefined);$.mask={definitions:{'9':\"[0-9]\",'a':\"[A-Za-z]\",'*':\"[A-Za-z0-9]\"}};$.fn.extend({caret:function(b,c){if(this.length==0)return;if(typeof b=='number'){c=(typeof c=='number')?c:b;return this.each(function(){if(this.setSelectionRange){this.focus();this.setSelectionRange(b,c)}else if(this.createTextRange){var a=this.createTextRange();a.collapse(true);a.moveEnd('character',c);a.moveStart('character',b);a.select()}})}else{if(this[0].setSelectionRange){b=this[0].selectionStart;c=this[0].selectionEnd}else if(document.selection&&document.selection.createRange){var d=document.selection.createRange();b=0-d.duplicate().moveStart('character',-100000);c=b+d.text.length}return{begin:b,end:c}}},unmask:function(){return this.trigger(\"unmask\")},mask:function(m,n){if(!m&&this.length>0){var o=$(this[0]);var q=o.data(\"tests\");return $.map(o.data(\"buffer\"),function(c,i){return q[i]?c:null}).join('')}n=$.extend({placeholder:\"_\",completed:null},n);var r=$.mask.definitions;var q=[];var s=m.length;var u=null;var v=m.length;$.each(m.split(\"\"),function(i,c){if(c=='?'){v--;s=i}else if(r[c]){q.push(new RegExp(r[c]));if(u==null)u=q.length-1}else{q.push(null)}});return this.each(function(){var f=$(this);var g=$.map(m.split(\"\"),function(c,i){if(c!='?'){return r[c]?(n.placeholder.length>1?n.placeholder.charAt(i):n.placeholder):c}});var h=false;var l=f.val();f.data(\"buffer\",g).data(\"tests\",q);function seekNext(a){while(++a<=v&&!q[a]);return a};function shiftL(a){while(!q[a]&&--a>=0);for(var i=a;i<v;i++){if(q[i]){g[i]=n.placeholder.length>1?n.placeholder.charAt(i):n.placeholder;var j=seekNext(i);if(j<v&&q[i].test(g[j])){g[i]=g[j]}else break}}writeBuffer();f.caret(Math.max(u,a))};function shiftR(a){for(var i=a;i<v;i++){var c=n.placeholder.length>1?n.placeholder.charAt(i):n.placeholder;if(q[i]){var j=seekNext(i);var t=g[i];g[i]=c;if(j<v&&q[j].test(t))c=t;else break}}};function keydownEvent(e){var a=$(this).caret();var k=e.keyCode;h=(k<16||(k>16&&k<32)||(k>32&&k<41));if((a.begin-a.end)!=0&&(!h||k==8||k==46))clearBuffer(a.begin,a.end);if(k==8||k==46||(x&&k==127)){shiftL(a.begin+(k==46?0:-1));return false}else if(k==27){f.val(l);f.caret(0,checkVal());return false}};function keypressEvent(e){if(h){h=false;return(e.keyCode==8)?false:null}e=e||window.event;var k=e.charCode||e.keyCode||e.which;var a=$(this).caret();if(e.ctrlKey||e.altKey||e.metaKey){return true}else if((k>=32&&k<=125)||k>186){var p=seekNext(a.begin-1);if(p<v){var c=String.fromCharCode(k);if(q[p].test(c)){shiftR(p);g[p]=c;writeBuffer();var b=seekNext(p);$(this).caret(b);if(n.completed&&b==v)n.completed.call(f)}}}return false};function clearBuffer(a,b){for(var i=a;i<b&&i<v;i++){if(q[i])g[i]=n.placeholder.length>1?n.placeholder.charAt(i):n.placeholder}};function writeBuffer(){return f.val(g.join('')).val()};function checkVal(a){var b=f.val();var d=-1;for(var i=0,pos=0;i<v;i++){if(q[i]){g[i]=n.placeholder.length>1?n.placeholder.charAt(i):n.placeholder;while(pos++<b.length){var c=b.charAt(pos-1);if(q[i].test(c)){g[i]=c;d=i;break}}if(pos>b.length)break}else if(g[i]==b[pos]&&i!=s){pos++;d=i}}if(!a&&d+1<s){f.val(\"\");clearBuffer(0,v)}else if(a||d+1>=s){writeBuffer();if(!a)f.val(f.val().substring(0,d+1))}return(s?i:u)};if(!f.attr(\"readonly\"))f.one(\"unmask\",function(){f.unbind(\".mask\").removeData(\"buffer\").removeData(\"tests\")}).bind(\"focus.mask\",function(){l=f.val();var a=checkVal();writeBuffer();setTimeout(function(){if(a==m.length)f.caret(0,a);else f.caret(a)},0)}).bind(\"blur.mask\",function(){checkVal();if(f.val()!=l)f.change()}).bind(\"keydown.mask\",keydownEvent).bind(\"keypress.mask\",keypressEvent).bind(w,function(){setTimeout(function(){f.caret(checkVal(true))},0)});checkVal()})}})})(jQuery);\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t// jQuery(function($){\r" +
    "\n" +
    "\t// $(\"#testdate\").mask(\"99/99/9999\", {placeholder: 'dd/mm/yyyy'});});</script> <style type=\"text/css\"></style> <div ng-show=\"mainmenu\"> <button style=\"float: right; margin-top: -31px; margin-left: 5px\" ng-click=\"TambahAlasan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <i class=\"fa fa-plus\"></i> <span>&nbsp&nbspTambah</span> </button> <bsform-grid ng-form=\"masterAlasanForm\" factory-name=\"AlasanFactory\" model=\"mAlasan\" model-id=\"ReasonId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AlasanForm\" form-title=\"Alasan\" modal-title=\"Alasan\" loading=\"loading\" modal-size=\"small\" on-bulk-delete=\"hapusAlasan\" form-api=\"formApi\" grid-hide-action-column=\"true\" hide-new-button=\"true\" show-advsearch=\"on\" icon=\"fa fa-fw fa-child\" mode=\"mode\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <label>Kategori Alasan</label> <bsselect name=\"Category\" ng-model=\"filter.AlasanFilterByReasonCode\" data=\"optionsAlasanCategory\" item-text=\"ReasonCategory\" item-value=\"ReasonCode\" on-select=\"value(selected)\" placeholder=\"Kategori Alasan\" icon=\"fa fa-search\"> </bsselect> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-4\">\r" +
    "\n" +
    "\t\t\t\t\t<label>Date</label>\r" +
    "\n" +
    "\t\t\t\t\t<Input id=\"testdate\" type=\"date\" ng-model=\"datetest\"></Input>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div> --> </bsform-advsearch> </bsform-grid> </div> <!-- <div ng-show=\"hide\">\r" +
    "\n" +
    "\t<div class=\"row\" style=\"margin-bottom:10px;margin-top: -31px;\">\t\r" +
    "\n" +
    "\t\t<button style=\"float: right;margin-right:15px;\" ng-click=\"TambahAlasan()\"  type=\"button\" class=\"rbtn btn ng-binding ladda-button\" >\r" +
    "\n" +
    "\t\t\t<i class=\"fa fa-plus\"></i><span>&nbsp&nbspTambah</span>\r" +
    "\n" +
    "\t\t</button>\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\t<button class=\"btn ubtn mobilesearchbtn\" style=\"float: right;margin-left:2px;margin-right:2px;\" ng-click=\"RefreshAlasan()\">\r" +
    "\n" +
    "\t\t\t<span class=\"fa fa-fw fa-refresh iconFilter\" style=\"font-size:1.4em\"></span>\r" +
    "\n" +
    "\t\t</button>\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t\t<button class=\"btn ubtn mobilesearchbtn\" style=\"float: right;margin-left:2px;margin-right:2px;\" ng-click=\"ToggleAlasanSearch()\">\r" +
    "\n" +
    "\t\t\t<span class=\"fa fa-fw fa-search iconFilter\" style=\"font-size:1.4em\"></span>\r" +
    "\n" +
    "\t\t</button>\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t</div>\r" +
    "\n" +
    "\t<div ng-show=\"ShowAlasanSearch\" style=\"margin-left:10px;margin-bottom:10px;background-color:rgba(213,51,55,0.02);\" class=\"row\">\r" +
    "\n" +
    "\t\t<div class=\"row\">\r" +
    "\n" +
    "\t\t<div class=\"col-md-6\">\r" +
    "\n" +
    "\t\t<bslabel>Kategori Alasan</bslabel>\r" +
    "\n" +
    "\t\t\t\t\t<bsselect name=\"Category\" \r" +
    "\n" +
    "\t\t\t\t\t\tng-model=\"AlasanFilterByReasonCode\" \r" +
    "\n" +
    "\t\t\t\t\t\tdata=\"optionsAlasanCategory\" \r" +
    "\n" +
    "\t\t\t\t\t\titem-text=\"ReasonCategory\" \r" +
    "\n" +
    "\t\t\t\t\t\titem-value=\"ReasonCode\" \r" +
    "\n" +
    "\t\t\t\t\t\ton-select=\"value(selected)\" \r" +
    "\n" +
    "\t\t\t\t\t\tplaceholder=\"Kategori Alasan\" \r" +
    "\n" +
    "\t\t\t\t\t\ticon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t</bsselect>\r" +
    "\n" +
    "\t\t</div>\r" +
    "\n" +
    "\t\t</div>\r" +
    "\n" +
    "\t\t<div class=\"row\" style=\"margin-right:10px\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t<button class=\"btn wbtn\" \r" +
    "\n" +
    "\t\t\t\t\t\tstyle=\"padding:0px 5px 0px 0px;float:right;margin-right:20px;\" \r" +
    "\n" +
    "\t\t\t\t\t\tng-click=\"CariAlasan()\" onclick=\"this.blur()\"\r" +
    "\n" +
    "\t\t\t\t\t\ttooltip-placement=\"bottom\"\r" +
    "\n" +
    "\t\t\t\t\t\ttooltip-trigger=\"mouseenter focus\"\r" +
    "\n" +
    "\t\t\t\t\t\ttooltip-animation=\"false\"\r" +
    "\n" +
    "\t\t\t\t\t\tuib-tooltip=\"Search\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"fa-stack fa-lg\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-database fa-stack-1x\" style=\"color:#aeaeae;font-size:20px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t</i><i class=\"fa fa-search fa-stack-1x\" style=\"font-size:17px;top:5px;left:8px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t</i>\r" +
    "\n" +
    "\t\t\t\t\t\t</span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t</div>\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t</div>\t\r" +
    "\n" +
    "\t\t\r" +
    "\n" +
    "\t<div style=\"display: block;height:380px;\" class=\"grid\" ui-grid=\"grid3\" ui-grid-selection ui-grid-auto-resize ui-grid-pagination></div>\r" +
    "\n" +
    "\t<br/>\r" +
    "\n" +
    "</div> --> <div ng-show=\"ShowAlasanDetail\" style=\"margin-top: -31px;min-height: 100vh\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <button ng-disabled=\"mAlasan_Category==null||mAlasan_Category==undefined||mAlasan_ReasonName==null||mAlasan_ReasonName==''||mAlasan_ReasonName==undefined\" style=\"float: right\" ng-show=\"ShowSimpanAlasan\" ng-click=\"SimpanAlasan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span> <span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliDariAlasan()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> </div> <br> <div class=\"row\"> <div class=\"col-md-6\"> <bsreqlabel>Kategori Alasan</bsreqlabel> <bsselect name=\"Category\" ng-disabled=\"!mAlasan_Category_EnableDisable\" ng-model=\"mAlasan_Category\" data=\"optionsAlasanCategory\" item-text=\"ReasonCategory\" item-value=\"ReasonCode\" on-select=\"KategoriAlasanChanged(selected)\" placeholder=\"Kategori Alasan\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-top:10px\">Alasan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea id=\"AlasanDaAlasan\" alpha-numeric mask=\"\" ng-change=\"MasterAlasanPaksaanReasonName()\" maxlength=\"30\" type=\"text\" class=\"form-control\" name=\"ReasonName\" placeholder=\"Input Alasan\" ng-disabled=\"!mAlasan_ReasonName_EnableDisable\" ng-model=\"mAlasan_ReasonName\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "\t\t\t\t\t</textarea> </div> </div> </div> </div> <div class=\"ui small modal ModalAlasanHapus\" role=\"dialog\"> <ul class=\"swal2-progresssteps\" style=\"display: none\"></ul> <div class=\"swal2-icon swal2-error\" style=\"display: none\"> <span class=\"x-mark\"> <span class=\"line left\"> </span> <span class=\"line right\"> </span> </span> </div> <div class=\"swal2-icon swal2-question\" style=\"display: none\">?</div> <div class=\"swal2-icon swal2-warning\" style=\"display: block\">!</div> <div class=\"swal2-icon swal2-info\" style=\"display: none\">i</div> <div class=\"swal2-icon swal2-success\" style=\"display: none\"> <span class=\"line tip\"> </span> <span class=\"line long\"> </span> <div class=\"placeholder\"> </div> <div class=\"fix\"> </div> </div> <img class=\"swal2-image\" style=\"display: none\"> <h2 class=\"swal2-title\" id=\"swal2-title\"> <p align=\"center\" style=\"font-size:20px\">Delete <span style=\"color:red\">1 item(s) </span>from <span style=\"color:blue\">Master Alasan.</span> </p> <p align=\"center\">Anda Yakin?</p> </h2> <div id=\"swal2-content\" class=\"swal2-content\" style=\"display: block\" align=\"center\">Tidak dapat kembali ke kondisi sebelumnya!</div> <div align=\"center\" class=\"swal2-buttonswrapper\" style=\"display: block;margin-top:10px;margin-bottom:10px\"> <button ng-click=\"HapusAlasanOk()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-confirm swal2-styled\" style=\"background-color: rgb(140, 212, 245); border-left-color: rgb(140, 212, 245); border-right-color: rgb(140, 212, 245);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">OK</button> <button ng-click=\"HapusAlasanCancel()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-cancel swal2-styled\" style=\"display: inline-block; background-color: rgb(170, 170, 170);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">Cancel</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/alasan/alasan_bckp.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"Alasan\"> <div class=\"row\" style=\"margin-bottom:10px;margin-top: -31px\"> <button style=\"float: right;margin-right:15px\" ng-click=\"TambahAlasan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <i class=\"fa fa-plus\"></i><span>&nbsp&nbspTambah</span> </button> <button class=\"btn ubtn mobilesearchbtn\" style=\"float: right;margin-left:2px;margin-right:2px\" ng-click=\"RefreshAlasan()\"> <span class=\"fa fa-fw fa-refresh iconFilter\" style=\"font-size:1.4em\"></span> </button> <button class=\"btn ubtn mobilesearchbtn\" style=\"float: right;margin-left:2px;margin-right:2px\" ng-click=\"ToggleAlasanSearch()\"> <span class=\"fa fa-fw fa-search iconFilter\" style=\"font-size:1.4em\"></span> </button> <!--<button style=\"float: right;\" ng-click=\"DeleteAlasan()\"  type=\"button\" class=\"rbtn btn ng-binding ladda-button\" >\r" +
    "\n" +
    "\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\tHapus Data\r" +
    "\n" +
    "\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t</button>--> </div> <div ng-show=\"ShowAlasanSearch\" style=\"margin-left:10px;margin-bottom:10px;background-color:rgba(213,51,55,0.02)\" class=\"row\"> <div class=\"row\"> <div class=\"col-md-6\"> <bslabel>Kategori Alasan</bslabel> <bsselect name=\"Category\" ng-model=\"AlasanFilterByReasonCode\" data=\"optionsAlasanCategory\" item-text=\"ReasonCategory\" item-value=\"ReasonCode\" on-select=\"value(selected)\" placeholder=\"Kategori Alasan\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"row\" style=\"margin-right:10px\"> <button class=\"btn wbtn\" style=\"padding:0px 5px 0px 0px;float:right;margin-right:20px\" ng-click=\"CariAlasan()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter focus\" tooltip-animation=\"false\" uib-tooltip=\"Search\"> <span class=\"fa-stack fa-lg\"> <i class=\"fa fa-database fa-stack-1x\" style=\"color:#aeaeae;font-size:20px\"> </i><i class=\"fa fa-search fa-stack-1x\" style=\"font-size:17px;top:5px;left:8px\"> </i> </span> </button> </div> </div> <div style=\"display: block;height:380px\" class=\"grid\" ui-grid=\"grid3\" ui-grid-selection ui-grid-auto-resize ui-grid-pagination></div> <br> </div> <div ng-show=\"ShowAlasanDetail\" style=\"margin-bottom:10px;margin-top: -31px\"> <button ng-disabled=\"mAlasan_Category==null||mAlasan_Category==undefined||mAlasan_ReasonName==null||mAlasan_ReasonName==''||mAlasan_ReasonName==undefined\" style=\"float: right\" ng-show=\"ShowSimpanAlasan\" ng-click=\"SimpanAlasan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliDariAlasan()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> <br> <br> <bslabel>Kategori Alasan</bslabel> <bsselect name=\"Category\" ng-disabled=\"!mAlasan_Category_EnableDisable\" ng-model=\"mAlasan_Category\" data=\"optionsAlasanCategory\" item-text=\"ReasonCategory\" item-value=\"ReasonCode\" on-select=\"KategoriAlasanChanged(selected)\" placeholder=\"Kategori Alasan\" icon=\"fa fa-search\"> </bsselect> <bsreqlabel style=\"margin-top:10px\">Alasan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea ng-change=\"MasterAlasanPaksaanReasonName()\" id=\"AlasanDaAlasan\" maxlength=\"30\" type=\"text\" class=\"form-control\" name=\"ReasonName\" placeholder=\"Alasan\" ng-disabled=\"!mAlasan_ReasonName_EnableDisable\" ng-model=\"mAlasan_ReasonName\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "\t\t\t\t</textarea></div> </div> <div class=\"ui modal ModalAlasanHapus\" role=\"dialog\"> <ul class=\"swal2-progresssteps\" style=\"display: none\"></ul> <div class=\"swal2-icon swal2-error\" style=\"display: none\"> <span class=\"x-mark\"> <span class=\"line left\"> </span> <span class=\"line right\"> </span> </span> </div> <div class=\"swal2-icon swal2-question\" style=\"display: none\">?</div> <div class=\"swal2-icon swal2-warning\" style=\"display: block\">!</div> <div class=\"swal2-icon swal2-info\" style=\"display: none\">i</div> <div class=\"swal2-icon swal2-success\" style=\"display: none\"> <span class=\"line tip\"> </span> <span class=\"line long\"> </span> <div class=\"placeholder\"> </div> <div class=\"fix\"> </div> </div> <img class=\"swal2-image\" style=\"display: none\"> <h2 class=\"swal2-title\" id=\"swal2-title\"> <p align=\"center\" style=\"font-size:20px\">Delete <span style=\"color:red\">1 item(s) </span>from <span style=\"color:blue\">Master Alasan.</span></p> <p align=\"center\">Anda Yakin?</p> </h2> <div id=\"swal2-content\" class=\"swal2-content\" style=\"display: block\" align=\"center\">Tidak dapat kembali ke kondisi sebelumnya!</div> <div align=\"center\" class=\"swal2-buttonswrapper\" style=\"display: block;margin-top:10px;margin-bottom:10px\"> <button ng-click=\"HapusAlasanOk()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-confirm swal2-styled\" style=\"background-color: rgb(140, 212, 245); border-left-color: rgb(140, 212, 245); border-right-color: rgb(140, 212, 245);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">OK</button> <button ng-click=\"HapusAlasanCancel()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-cancel swal2-styled\" style=\"display: inline-block; background-color: rgb(170, 170, 170);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">Cancel</button></div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/alasanPembelianKendaraan/alasanPembelianKendaraan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AlasanPembelianKendaraanForm\" factory-name=\"AlasanPembelianKendaraanFactory\" model=\"mAlasanpembelian\" model-id=\"ReasonBuyingId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AlasanPembelianKendaraanForm\" form-title=\"Alasan Pembelian\" modal-title=\"sitePds\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alasan Pembelian Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"NamaAlasanPembelianKendaraan\" placeholder=\"Alasan Pembelian Kendaraan\" ng-model=\"mAlasanpembelian.ReasonBuyingName\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "                    </textarea></div> <bserrmsg field=\"NamaAlasanPembelianKendaraan\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/alasanRevisiPdd/alasanRevisiPdd.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AlasanRevisiPddForm\" factory-name=\"AlasanRevisiPddFactoryMaster\" model=\"mAlasanRevisiPdd\" model-id=\"ReasonRevisionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AlasanRevisiPddForm\" form-title=\"AlasanRevisiPdd\" modal-title=\"AlasanRevisiPdd\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alasan Revisi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input alpha-numeric mask=\"Huruf\" type=\"text\" class=\"form-control\" name=\"ReasonRevisionName\" placeholder=\"AlasanRevisiPdd\" ng-model=\"mAlasanRevisiPdd.ReasonRevisionName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ReasonRevisionName\"></bserrmsg> </div> <label>Alasan Ini Terhitung Ke Dalam Batas Revisi</label> <form> <input type=\"checkbox\" ng-model=\"mAlasanRevisiPdd.LimitRevision\"> </form> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/areaDealer/areaDealer.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">table {\r" +
    "\n" +
    "    font-family: arial, sans-serif;\r" +
    "\n" +
    "    border-collapse: collapse;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "td, th {\r" +
    "\n" +
    "    border: 1px solid #dddddd;\r" +
    "\n" +
    "    text-align: left;\r" +
    "\n" +
    "    padding: 8px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "tr:nth-child(even) {\r" +
    "\n" +
    "    background-color: #dddddd;\r" +
    "\n" +
    "}</style> <bsform-grid ng-form=\"AreaDealerForm\" factory-name=\"AreaDealerFactoryMaster\" model=\"mAreaDealer\" model-id=\"AreaId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AreaDealerForm\" form-title=\"Area TAM\" modal-title=\"Area TAM\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Area TAM</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"AreaName\" placeholder=\"Nama Area TAM\" ng-model=\"mAreaDealer.AreaName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AreaName\"></bserrmsg> </div> </div> <table style=\"margin-left: 20px\"> <tr> <th>Kode Branch</th> <th>Nama Branch</th> </tr> <tr ng-repeat=\"mAreaDealerChild in mAreaDealer.Outlets\"> <td>{{mAreaDealerChild.OutletCode}}</td> <td>{{mAreaDealerChild.OutletName}}</td> </tr> </table> <!-- <div class=\"col-md-12\" style=\"margin-top:10px\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\tKode Branch\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-5\">\r" +
    "\n" +
    "\t\t\t\t\tNama Branch\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\" ng-repeat=\"mAreaDealerChild in mAreaDealer.Outlets\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\t{{mAreaDealerChild.OutletCode}}\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-5\">\r" +
    "\n" +
    "\t\t\t\t\t{{mAreaDealerChild.OutletName}}\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div> --> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/assetBrochureType/assetBrochureType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AssetBrochureTypeForm\" factory-name=\"AssetBrochureTypeFactory\" model=\"mAssetBrochureType\" model-id=\"BrochureTypeId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" form-name=\"AssetBrochureTypeForm\" form-title=\"Tipe Brosur\" modal-title=\"Master Asset Tipe Brosur\" modal-size=\"small\" on-bulk-delete=\"onBulkDeleteTipeBrosur\" icon=\"fa fa-fw fa-child\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Tipe Brosur</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"brochureTypeName\" placeholder=\"Input Nama Tipe Brosur\" ng-model=\"mAssetBrochureType.BrochureTypeName\" maxlength=\"20\" required> </div> <bserrmsg field=\"brochureTypeName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/assetBrosur/assetBrosur.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"AssetBrosurForm\" factory-name=\"AssetBrosur\" model=\"mAssetBrosur\" model-id=\"BrochureId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AssetBrosurForm\" form-title=\"AssetBrosur\" modal-title=\"AssetBrosur\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-5\"> <!--<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Vehicle Model</bsreqlabel>\r" +
    "\n" +
    "                <bsselect name=\"vehicleModel\" data=\"VehicleModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"mAssetBrosur.BranchId\" placeholder=\"Select Vehicle Model\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bserrmsg field=\"vehicleModel\"></bserrmsg>\r" +
    "\n" +
    "            </div>--> <div class=\"form-group\" show-errors> <bsreqlabel>Brochure Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"namaBrosur\" placeholder=\"Nama Brosur\" ng-model=\"mAssetBrosur.BrochureName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"namaBrosur\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Brochure Type</bsreqlabel> <bsselect name=\"brochuretype\" data=\"BrochureType\" item-text=\"BrochureTypeName\" item-value=\"BrochureTypeId\" ng-model=\"mAssetBrosur.BrochureTypeId\" placeholder=\"Select Brochure Type\" icon=\"fa fa-file-o glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"brochuretype\"></bserrmsg> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-9\" style=\"padding-left:0px\"> <div class=\"form-group\" show-errors> <bsreqlabel>File Modul</bsreqlabel> <image-control max-upload=\"1\" is-image=\"1\" img-upload=\"uploadFiles\"> <form-image-control> <input type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"text\" ng-model=\"mAssetBrosur.UploadDataBrochure = uploadFiles[0]\" name=\"\" style=\"display:none\"> </form-image-control> </image-control> <!--<div class=\"input-icon-right\">\r" +
    "\n" +
    "                            <i class=\"fa\"></i>\r" +
    "\n" +
    "                            <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"file\" placeholder=\"File Modul\" ng-model=\"mAssetBrosur.FileModul\" required>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <bserrmsg field=\"FileModul\"></bserrmsg>--> </div> </div> <div class=\"col-md-3\" style=\"margin:23px 0 0 0; padding-right:0px\"> <label class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> Browse<input type=\"file\" name=\"file\" data-my-directive style=\"display: none\" class=\"form-control\"> </label> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"padding-left: 0 !important\"> <div class=\"form-group\" show-errors> <bsreqlabel>Valid On</bsreqlabel> <bsdatepicker name=\"ValidOn\" ng-change=\"tanggalmin(dt)\" date-options=\"dateOptionsFrom\" ng-model=\"mAssetBrosur.ValidOn\" ng-change=\"\"> </bsdatepicker> <bserrmsg field=\"ValidOn\"></bserrmsg> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"padding-right: 0 !important\"> <div class=\"form-group\" show-errors> <bsreqlabel>Valid Until</bsreqlabel> <bsdatepicker name=\"ValidUntil\" ng-disabled=\"mAssetBrosur.ValidOn == null\" date-options=\"dateOptionLastDate\" ng-model=\"mAssetBrosur.ValidUntil\" ng-change=\"\"> </bsdatepicker> <bserrmsg field=\"ValidUntil\"></bserrmsg> </div> </div> <input type=\"checkbox\" name=\"enabled\" ng-checked=\"mAssetBrosur.ShowBit\" ng-model=\"mAssetBrosur.ShowBit\"> Enabled<br> <!--<button ng-click=\"cekObject()\">Cek</button>--> </div> </div> </bsform-grid> <div class=\"ui modal mAssetBrosururMaster\"> <div> <div class=\"modal-header\"> <h6>{{BrocureNames}}</h6> </div> <div class=\"modal-body\"> <img ng-src=\"{{dataStream}}\"> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/assetTestDriveUnit/assetTestDriveUnit.html',
    "<script type=\"text/javascript\">function AssetTestDriveUnit_Cuma_Huruf_Angka_1(e) {\r" +
    "\n" +
    "    var k;\r" +
    "\n" +
    "    document.all ? k = e.keyCode : k = e.which;\r" +
    "\n" +
    "    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function AssetTestDriveUnit_Cuma_Huruf_Angka_2(e) {\r" +
    "\n" +
    "    var k;\r" +
    "\n" +
    "    document.all ? k = e.keyCode : k = e.which;\r" +
    "\n" +
    "    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));\r" +
    "\n" +
    "}</script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AssetTestDriveUnitForm\" factory-name=\"AssetTestDriveUnit\" model=\"mAssetTestDriveUnit\" model-id=\"TestDriveUnitId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AssetTestDriveUnitForm\" form-title=\"Unit\" modal-title=\"AssetTestDriveUnit\" modal-size=\"small\" form-api=\"formApi\" loading=\"loading\" do-custom-save=\"doCustomSave\" mode=\"AssetTestDriveDaMode\" icon=\"fa fa-fw fa-child\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nomor Polisi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <!-- <input onkeypress=\"return AssetTestDriveUnit_Cuma_Huruf_Angka_1(event)\" type=\"text\" class=\"form-control\" name=\"vehicleNo\" placeholder=\"Vehicle Number\" ng-model=\"mAssetTestDriveUnit.PoliceNumber\" maxlength=\"10\" required> --> <input alpha-numeric type=\"text\" class=\"form-control\" name=\"vehicleNo\" placeholder=\"Vehicle Number\" ng-model=\"mAssetTestDriveUnit.PoliceNumber\" maxlength=\"10\" required> </div> <bserrmsg field=\"vehicleNo\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"model\" data=\"VehicleModel\" ng-change=\"filtertipe(mAssetTestDriveUnit.VehicleModelId)\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"mAssetTestDriveUnit.VehicleModelId\" placeholder=\"PIlih Model\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"model\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <bsselect name=\"type\" data=\"vehicleTypefilter\" ng-change=\"filterColor(mAssetTestDriveUnit.VehicleTypeId)\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"mAssetTestDriveUnit.VehicleTypeId\" placeholder=\"Pilih Tipe\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"type\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Warna</bsreqlabel> <bsselect name=\"color\" data=\"vehicleTypecolorfilter\" item-text=\"ColorName, ColorCode\" item-value=\"ColorId\" ng-model=\"mAssetTestDriveUnit.ColorId\" placeholder=\"Pilih Warna\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> <div class=\"form-group\" show-errors> <bsreqlabel>No Rangka</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input alpha-numeric type=\"text\" class=\"form-control\" name=\"rangka\" placeholder=\"Nomor Rangka\" ng-model=\"mAssetTestDriveUnit.FrameNo\" maxlength=\"20\" required> </div> <bserrmsg field=\"rangka\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Waktu Maksimal (Jam)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"LimitTime\" min=\"0\" placeholder=\"Limit Time\" ng-model=\"mAssetTestDriveUnit.TimeMaxTestDrive\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"LimitTime\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Batas Booking Perhari</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" min=\"0\" name=\"LimitBookingInADay\" placeholder=\"Batas perhari\" ng-model=\"mAssetTestDriveUnit.MaxOrderPerdayPerSales\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"LimitBookingInADay\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Jarak Terakhir (KM)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" min=\"0\" name=\"jarakterakhir\" placeholder=\"Jarak Terakhir\" ng-model=\"mAssetTestDriveUnit.LastMetter\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"jarakterakhir\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Mulai</bsreqlabel> <bsdatepicker onkeydown=\"return false\" name=\"LastStartDate\" ng-change=\"tanggalmin(mAssetTestDriveUnit.LastStartDate)\" ng-model=\"mAssetTestDriveUnit.LastStartDate\" date-options=\"dateOptionsFrom\" ng-change=\"\"> </bsdatepicker> <bserrmsg field=\"LastStartDate\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Berakhir</bsreqlabel> <bsdatepicker onkeydown=\"return false\" name=\"LastEndDate\" ng-model=\"mAssetTestDriveUnit.LastEndDate\" ng-disabled=\"mAssetTestDriveUnit.LastStartDate == null\" date-options=\"dateOptionLastDate\" ng-change=\"\"> </bsdatepicker> <bserrmsg field=\"LastEndDate\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"row\" ng-hide=\"viewMode == true\" style=\"margin: 0\"> <div class=\"col-md-8\" style=\"padding-left: 0 !important\"> <div class=\"form-group\"> <label>Outlet Requester</label> <div class=\"form-group\"> <bsselect on-select=\"OutletReq(selected)\" data=\"Outlet\" item-text=\"Name\" item-value=\"OutletId\" ng-model=\"EquipmentStandardId\" placeholder=\"Pilih Outlet\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> </div> </div> <div style=\"float:right\"> <button style=\"margin: 23px 0 0 0\" class=\"btn rbtn\" ng-click=\"tambah()\" ng-disabled=\"(munitChecklistPerlengkapanStandar.NamaPerlengkapanStandar == 'null' || munitChecklistPerlengkapanStandar.NamaPerlengkapanStandar == 'undefined')?true:false;\">Tambah</button> </div> </div> <label ng-show=\"viewMode==true\">Daftar Cabang</label> <table class=\"table table-bordered\"> <thead> <tr> <th>Nama Cabang</th> </tr> </thead> <tr ng-repeat=\"data in mAssetTestDriveUnit.listDetail track by $index\"> <td style=\"display: none\">{{data.OutletRequestorId}}</td> <td style=\"display: none\">{{data.VehicleMappingId}}</td> <td>{{data.OutletRequestorName}}</td> <td ng-hide=\"AssetTestDriveDaMode=='view'||viewMode==true || data.OutletRequestorId==user.OutletId\"><u style=\"color: blue\" ng-disabled=\"viewMode == true\" ng-click=\"remove($index)\" i class=\"fa fa-times\"></u></td> </tr> </table> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/assetTestDriveUnitHistory/assetTestDriveUnitHistory.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AssetTestDriveUnitHistoryForm\" factory-name=\"AssetTestDriveUnitHistory\" model=\"mAssetTestDriveUnitHistory\" model-id=\"TestDriveUnitHistoryId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AssetTestDriveUnitHistoryForm\" form-title=\"AssetTestDriveUnitHistoryX\" modal-title=\"AssetTestDriveUnitHistory\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Nama Tes Drive Unit</label> <bsselect name=\"TestDriveUnitId\" ng-model=\"mAssetTestDriveUnitHistory.TestDriveUnitId\" data=\"grid.data\" item-text=\"NamaTestDriveUnit\" item-value=\"TestDriveUnitId\" placeholder=\"Pilih Test Drive Unit\" required> </bsselect> <bserrmsg field=\"TestDriveUnitId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Test Drive Unit History</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NamaTestDriveUnitHistory\" placeholder=\"Nama Test Drive Unit History\" ng-model=\"mAssetTestDriveUnitHistory.NamaTestDriveUnitHistory\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"NamaTestDriveUnitHistory\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Test Drive Unit History</bsreqlabel> <bsdatepicker name=\"TanggalTestDriveUnitHistory\" date-options=\"dateOptions\" ng-model=\"mAssetTestDriveUnitHistory.TanggalTestDriveUnitHistory\"> </bsdatepicker> <bserrmsg field=\"TanggalTestDriveUnitHistory\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/baru/accessoryPackage/accessoryPackage.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.unstyled::-webkit-inner-spin-button {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "        -webkit-appearance: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .text-right {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t#layoutContainer_AccessoryPackageForm {\r" +
    "\n" +
    "        min-height:100vh !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.PaketAksesorisWarnainYgGanjil {\r" +
    "\n" +
    "\t\tbackground-color: #EEEEEE;\r" +
    "\n" +
    "\t}</style> <bsform-grid ng-form=\"AccessoryPackageForm\" factory-name=\"AccessoryPackageFactory\" model=\"mAccessoryPackage\" model-id=\"AccessoriesPackageCode\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AccessoryPackageForm\" show-advsearch=\"on\" mode=\"AccessoryPackageDaMode\" form-title=\"Paket Aksesoris\" modal-title=\"Paket Aksesoris\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" do-custom-save=\"doCustomSave\" form-api=\"formApi\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors style=\"padding-left:0 !important\"> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"Model\" ng-model=\"filterAccessoryPackage.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"SelectedModelSearchAccessoryPackage(selected)\" placeholder=\"Pilih Model\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors style=\"padding-left:0 !important\"> <bsreqlabel>Tipe</bsreqlabel> <bsselect name=\"Model\" ng-model=\"filterAccessoryPackage.VehicleTypeId\" data=\"optionsTypeSearchBuatAccessoryPackage\" item-text=\"Description,KatashikiCode,SuffixCode\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Tipe\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> </bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"Model\" ng-model=\"mAccessoryPackage.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-change=\"SelectedModel(selected)\" on-select=\"SelectedModel(selected)\" placeholder=\"Pilih Model\" icon=\"fa fa-search\" ng-disabled=\"AccessoryPackageDaMode=='edit'\"> </bsselect> </div> <bserrmsg field=\"Model\"></bserrmsg> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <bsselect name=\"Tipe\" ng-model=\"mAccessoryPackage.VehicleTypeId\" data=\"optionsTipeFiltered\" item-text=\"Description,KatashikiCode,SuffixCode\" item-value=\"VehicleTypeId\" on-select=\"SelectedTipe(selected)\" placeholder=\"Pilih Tipe\" icon=\"fa fa-search\" ng-disabled=\"AccessoryPackageDaMode=='edit'\"> </bsselect> </div> <bserrmsg field=\"Tipe\"></bserrmsg> <!--<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Kode Paket Aksesoris</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"PackageCode\" placeholder=\"PackageCode\" ng-model=\"mAccessoryPackage.AccessoriesPackageCode\"\r" +
    "\n" +
    "                        ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"PackageCode\"></bserrmsg>\r" +
    "\n" +
    "            </div>--> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Aksesoris</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AccessoriesPackageCode\" placeholder=\"Input Kode Aksesoris\" ng-model=\"mAccessoryPackage.AccessoriesPackageCode\" maxlength=\"50\" required> </div> <bserrmsg field=\"AccessoriesPackageCode\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Paket Aksesoris</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PackageName\" placeholder=\"Input Paket Aksesoris\" ng-model=\"mAccessoryPackage.AccessoriesPackageName\" maxlength=\"50\" required> </div> <bserrmsg field=\"PackageName\"></bserrmsg> </div> <!--<div class=\"form-group\" show-errors style=\"padding-left:0 !important\">\r" +
    "\n" +
    "                <bslabel>Accessories</bslabel>\r" +
    "\n" +
    "                <bsselect ng-disabled=\"DisabledAcc\" multiple name=\"Accessories\" ng-model=\"SelectVehicle.PartsId\" data=\"optionsAccessories\" item-text=\"PartsName\" item-value=\"PartsId\"\r" +
    "\n" +
    "                  ng-change=\"SelectMultiAcc(selected)\"  placeholder=\"Pilih Aksesoris\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <bserrmsg field=\"Accessories\"></bserrmsg>--> <button ng-if=\"mAccessoryPackage.VehicleModelId!=null && mAccessoryPackage.VehicleTypeId!=null && mAccessoryPackage.VehicleModelId!=undefined && mAccessoryPackage.VehicleTypeId!=undefined\" ng-click=\"AddAccessories()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px; margin-bottom:10px\" ng-disabled=\"AccessoryPackageDaMode=='edit'\"> <span class=\"ladda-label\"> Tambah Aksesoris </span> <span class=\"ladda-spinner\"></span> </button> <!-- <button type=\"button\" ng-click=\"debug()\" class=\"btn btn-danger\">debug</button> --> <table style=\"margin-top:10px\" class=\"table table-bordered table-responsive\"> <tbody> <tr> <th style=\"background-color: #CCCCCC\">Kode Part</th> <th style=\"background-color: #CCCCCC\">Nama Part</th> <th style=\"background-color: #CCCCCC\">Harga Retail</th> <th style=\"background-color: #CCCCCC\">Action</th> </tr> <tr ng-repeat=\"ListAccessories in mAccessoryPackage.ListMUnitAccessoriesPackageHODetail track by $index\"> <td data-ng-class=\"PaketAksesorisWarnainTabel($index)\">{{ListAccessories.AccessoriesCode}}</td> <td data-ng-class=\"PaketAksesorisWarnainTabel($index)\">{{ListAccessories.AccessoriesName}}</td> <td data-ng-class=\"PaketAksesorisWarnainTabel($index)\" align=\"right\">Rp. {{ListAccessories.RetailPrice | number:0}}</td> <td data-ng-class=\"PaketAksesorisWarnainTabel($index)\" ng-hide=\"AccessoryPackageDaMode=='view' || AccessoryPackageDaMode=='edit'\"> <a ng-click=\"RemoveAccessories($index)\">Hapus</a> </td> </tr> </tbody> </table> <div class=\"form-group\" style=\"margin-top:10px\"> <bslabel>Aktif</bslabel> <div class=\"input-icon-right\"> <label><input class=\"form-control\" type=\"checkbox\" ng-model=\"mAccessoryPackage.ActiveBit\" style=\"float:left;width: 20px;height: 20px\"></label> </div> </div> <div class=\"form-group\"> <bsreqlabel>Harga</bsreqlabel> <div class=\"row\"> <label class=\"col-md-1\" style=\"margin-top:8px\">Rp.</label> <div class=\"col-md-11\"> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"true\" readonly input-currency number-only type=\"text\" class=\"form-control\" name=\"Price\" placeholder=\"Price\" ng-model=\"mAccessoryPackage.Price\"> </div> </div> </div> <!--<bserrmsg field=\"Price\"></bserrmsg>--> </div> </div> </div>  <div class=\"ui modal ModalAccessoryPackageDaAksesoris\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"BatalLookupDaAksesoris()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Pilih Aksesoris</h4> </div> <br> <br> <div class=\"modal-body\"> <div class=\"form-group\" show-errors style=\"padding-left:0 !important\"> <bsreqlabel>Toyota Genuine Accessories</bsreqlabel> <bsselect ng-model=\"SelectedDataAccesories\" data=\"optionsTGA\" item-text=\"TGAName\" item-value=\"TGAId\" on-select=\"SelectedTGA(selected)\" placeholder=\"Pilih TGA\" icon=\"fa fa-search\"> </bsselect> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridAccessoryPackageDaAksesoris\" ui-grid-selection ui-grid-auto-resize></div> </div> <div class=\"modal-footer\"> <button ng-click=\"AddDaAccessoriesToTable()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\"> Pilih </span> <span class=\"ladda-spinner\"></span> </button> </div> </div></bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/baru/areaRestriction/areaRestriction.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AreaRestrictionForm\" factory-name=\"AreaRestrictionFactory\" model=\"mAreaRestriction\" model-id=\"AreaRuleRestrictionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AreaRestrictionForm\" form-title=\"AreaRestriction\" modal-title=\"Area Restriction\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bslabel>Area</bslabel> <bsselect name=\"Area\" ng-model=\"mAreaRestriction.AreaId\" data=\"optionsArea\" item-text=\"AreaName\" item-value=\"AreaId\" on-select=\"value(selected)\" placeholder=\"Select Area\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"Area\"></bserrmsg> <div class=\"form-group\" show-errors> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"Provinsi\" ng-model=\"mAreaRestriction.ProvinceId\" data=\"province\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" ng-change=\"filterKabupaten(mAreaRestriction.ProvinceId)\" placeholder=\"Select\" icon=\"fa fa-search\"> </bsselect> <bserrmsg field=\"Provinsi\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"KabupatenKota\" ng-model=\"mAreaRestriction.CityRegencyId\" data=\"kabupaten\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" ng-change=\"filterKecamatan(mAreaRestriction.CityRegencyId)\" placeholder=\"Select\" icon=\"fa fa-search\"> </bsselect> <bserrmsg field=\"KabupatenKota\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"Kecamatan\" ng-model=\"mAreaRestriction.DistrictId\" data=\"kecamatan\" item-text=\"DistrictName\" item-value=\"DistrictId\" ng-change=\"filterKelurahan(mAreaRestriction.DistrictId)\" placeholder=\"Select\" icon=\"fa fa-search\"> </bsselect> <bserrmsg field=\"Kecamatan\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bslabel>SPKRestriction</bslabel> <bsselect name=\"SPKRestrictionId\" ng-model=\"mAreaRestriction.SPKRestrictionId\" data=\"optionsRegionLevelRestriction\" item-text=\"AreaRestrictionName\" item-value=\"AreaRestrictionId\" placeholder=\"Select SPK Restriction\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"SPKRestrictionId\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>SwappingRestrictionId</bslabel> <bsselect name=\"SwappingRestrictionId\" ng-model=\"mAreaRestriction.SwappingRestrictionId\" data=\"optionsRegionLevelRestriction\" item-text=\"AreaRestrictionName\" item-value=\"AreaRestrictionId\" placeholder=\"Select Swapping Restriction\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"SwappingRestrictionId\"></bserrmsg> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/baru/categoryProspectVariable/categoryProspectVariable.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"CategoryProspectVariable\"> <button style=\"float: right\" ng-click=\"TambahCategoryProspectVariable()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"DeleteCategoryProspectVariable()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Hapus Data </span><span class=\"ladda-spinner\"></span> </button> <br> <br> <div style=\"display: block\" class=\"grid\" ui-grid=\"grid3\" ui-grid-selection ui-grid-auto-resize></div> <br> </div> <div ng-show=\"ShowTingkatKebutuhan\"> <br> <br> <div style=\"display: block\" class=\"grid\" ui-grid=\"grid2\" ui-grid-auto-resize></div> <br> </div> <div ng-show=\"ShowTingkatKebutuhanDetail\"> <button style=\"float: right\" ng-click=\"KembaliDariTingkatKebutuhan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-show=\"ShowSimpanTingkatKebutuhan\" ng-click=\"SimpanTingkatKebutuhan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <bslabel>Tingkat Kebutuhan</bslabel> <bsselect ng-disabled=\"!mCategoryProspectVariable_TingkatKebutuhan_EnableDisable\" name=\"TingkatKebutuhan\" ng-model=\"mCategoryProspectVariable_TingkatKebutuhan\" data=\"optionsTingkatKebutuhan\" item-text=\"PurchaseTimeName\" item-value=\"PurchaseTimeId\" on-select=\"value(selected)\" placeholder=\"Select Tingkat Kebutuhan\" icon=\"fa fa-search\"> </bsselect> <bslabel>Category</bslabel> <bsselect name=\"Category\" ng-disabled=\"!mCategoryProspectVariable_TingkatKebutuhan_Category_EnableDisable\" ng-model=\"mCategoryProspectVariable_TingkatKebutuhan_Category\" data=\"optionsCategory\" item-text=\"CategoryProspectName\" item-value=\"CategoryProspectId\" on-select=\"value(selected)\" placeholder=\"Select Category\" icon=\"fa fa-search\"> </bsselect> </div> <div ng-show=\"ShowCategoryProspectVariableDetail\"> <button style=\"float: right\" ng-click=\"KembaliDariCategoryProspectVariable()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-show=\"ShowSimpanCategoryProspectVariable\" ng-click=\"SimpanCategoryProspectVariable()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <bsreqlabel>Field</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Field\" placeholder=\"Field\" ng-disabled=\"!mCategoryProspectVariable_Field_EnableDisable\" ng-model=\"mCategoryProspectVariable_Field\" ng-maxlength=\"50\" required> </div> <bslabel>Category</bslabel> <bsselect name=\"Category\" ng-disabled=\"!mCategoryProspectVariable_Category_EnableDisable\" ng-model=\"mCategoryProspectVariable_Category\" data=\"optionsCategory\" item-text=\"CategoryProspectName\" item-value=\"CategoryProspectId\" on-select=\"value(selected)\" placeholder=\"Select Category\" icon=\"fa fa-search\"> </bsselect> </div>"
  );


  $templateCache.put('app/sales/sales9/master/baru/leadTimeBookingUnitKendaraan/leadTimeBookingUnitKendaraan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"LeadTimeBookingUnitKendaraanForm\" factory-name=\"LeadTimeBookingUnitKendaraanFactory\" model=\"mLeadTimeBookingUnitKendaraan\" model-id=\"PDDBufferId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"LeadTimeBookingUnitKendaraanForm\" form-title=\"LeadTimeBookingUnitKendaraan\" modal-title=\"Lead Time Booking Unit Kendaraan\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bslabel>Model</bslabel> <bsselect name=\"VehicleModelId\" ng-model=\"mLeadTimeBookingUnitKendaraan.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Select Model\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"VehicleModelId\"></bserrmsg> <div class=\"form-group\" show-errors> <bsreqlabel>Length Time Of Booking</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"LengthTimeOfBooking\" placeholder=\"Length Time Of Booking\" ng-model=\"mLeadTimeBookingUnitKendaraan.LengthTimeOfBooking\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"LengthTimeOfBooking\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Length Time Booking Fee</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"LengthTimeBookingFee\" placeholder=\"Length Time Booking Fee\" ng-model=\"mLeadTimeBookingUnitKendaraan.LengthTimeBookingFee\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"LengthTimeBookingFee\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Lead Time Stock</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"LeadTimeStock\" placeholder=\"Lead Time\" ng-model=\"mLeadTimeBookingUnitKendaraan.LeadTimeStock\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"LeadTimeStock\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/baru/leadTimeProspectFu/leadTimeProspectFu.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"LeadTimeProspectFuTable\"> <button ng-click=\"TambahLeadTimeProspectFu()\" style=\"float: right;margin:-31px 0 0 8px\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <i class=\"fa fa-plus\"></i> Tambah </button> <bsform-grid ng-form=\"LeadTimeProspectFuForm\" factory-name=\"LeadTimeProspectFuFactory\" model=\"mLeadTimeProspectFu\" model-id=\"FollowUpProspectLeadTimeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"LeadTimeProspectFuForm\" form-title=\"Lead Time Prospect FU\" modal-title=\"Lead Time Prospect FU\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" action-button-caption=\"actButtonCaption\" grid-hide-action-column=\"true\" hide-new-button=\"true\" icon=\"fa fa-fw fa-child\"> </bsform-grid> </div> <div class=\"row\" ng-show=\"ShowLeadTimeProspectFuDetail\" style=\"margin: -31px 0 0 0;min-height:100vh\"> <button ng-disabled=\"mLeadTimeProspectFu.OutletId==null||mLeadTimeProspectFu.OutletId==undefined||mLeadTimeProspectFu.CategoryProspectId==null||mLeadTimeProspectFu.CategoryProspectId==undefined||mLeadTimeProspectFu.LeadTime==null||mLeadTimeProspectFu.LeadTime==undefined||mLeadTimeProspectFu.LeadTime==''\" style=\"float: right;margin-right:5px\" ng-show=\"ShowSimpanLeadTimeProspectFu\" ng-click=\"SimpanLeadTimeProspectFu()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right;margin-right:5px\" ng-click=\"KembaliDariLeadTimeProspectFu()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> <br> <br> <div class=\"col-md-4\" style=\"margin-top: 10px\"> <div class=\"form-group\"> <label>Cabang</label> <bsselect ng-disabled=\"!mLeadTimeProspectFu_Enable\" name=\"Outlet\" ng-model=\"mLeadTimeProspectFu.OutletId\" data=\"optionsOutlet\" item-text=\"Name\" item-value=\"OutletId\" placeholder=\"Pilih Cabang\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"form-group\"> <label>Kategori Prospek</label> <bsselect ng-disabled=\"!mLeadTimeProspectFu_Enable\" name=\"CategoryProspectId\" ng-model=\"mLeadTimeProspectFu.CategoryProspectId\" data=\"optionsCategoryProspect\" item-text=\"CategoryProspectName\" item-value=\"CategoryProspectId\" on-select=\"value(selected)\" placeholder=\"Kategori Prospek\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Waktu Standart Follow Up</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input number-only ng-disabled=\"!mLeadTimeProspectFu_Enable\" type=\"text\" class=\"form-control\" name=\"LeadTime\" placeholder=\"Waktu Standart Follow Up\" ng-model=\"mLeadTimeProspectFu.LeadTime\" maxlength=\"3\" required> </div> </div> <!--<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Ratio Spk (%)</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"RatioSpk\" placeholder=\"Ratio Spk (%)\" ng-model=\"mLeadTimeProspectFu.RatioSPK\"\r" +
    "\n" +
    "                            ng-maxlength=\"50\" required\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"RatioSpk\"></bserrmsg>\r" +
    "\n" +
    "            </div>--> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/baru/pddBuffer/pddBuffer.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"PddBufferForm\" factory-name=\"PddBufferFactory\" model=\"mPddBuffer\" model-id=\"PDDBufferId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"PddBufferForm\" form-title=\"PddBuffer\" modal-title=\"Pdd Buffer\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bslabel>Assembly Type</bslabel> <bsselect name=\"AssemblyType\" ng-model=\"mPddBuffer.AssemblyTypeId\" data=\"optionsAssemblyType\" item-text=\"AssemblyTypeName\" item-value=\"AssemblyTypeId\" on-select=\"value(selected)\" placeholder=\"Select Assembly Type\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"AssemblyType\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>Model</bslabel> <bsselect name=\"Model\" ng-model=\"mPddBuffer.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Select Model\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"Model\"></bserrmsg> <div class=\"form-group\" show-errors> <bslabel>Stock Position</bslabel> <bsselect name=\"StockPosition\" ng-model=\"mPddBuffer.StatusUnitId\" data=\"optionsStockPosition\" item-text=\"StatusUnitName\" item-value=\"StatusUnitId\" on-select=\"value(selected)\" placeholder=\"Select Stock Position\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"StockPosition\"></bserrmsg> <div class=\"form-group\" show-errors> <bsreqlabel>Buffer Time</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"BufferTime\" placeholder=\"Buffer Time\" ng-model=\"mPddBuffer.BufferTime\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BufferTime\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/categoryContact/pCategoryContact.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ContactCategoryForm\" factory-name=\"ContactCategoryFactory\" model=\"mpCategoryContact\" model-id=\"ContactCategoryId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ContactCategoryForm\" form-title=\"Contact Category\" modal-title=\"Contact Category\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Contact Category</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ContactCategory\" placeholder=\"Contact Category\" ng-model=\"mpCategoryContact.ContactCategoryName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ContactCategory\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/categoryDeliveryCategory/deliveryCategory.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"DeliveryCategoryForm\" factory-name=\"DeliveryCategoryFactory\" model=\"mDeliveryCategory\" model-id=\"DeliveryCategoryId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"DeliveryCategoryForm\" form-title=\"Delivery Category\" modal-title=\"Delivery Category\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Delivery Category</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"DeliveryCategory\" placeholder=\"Delivery Category\" ng-model=\"mDeliveryCategory.DeliveryCategoryName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"DeliveryCategory\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/categoryDocument/categoryDocument.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CategoryDocumentForm\" factory-name=\"CategoryDocumentFactory\" model=\"mCategoryDocument\" model-id=\"DocumentId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CategoryDocumentForm\" form-title=\"Kategori Dokumen\" modal-title=\"Kategori Dokumen\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" on-bulk-delete=\"DeleteManual\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kategori Dokumen</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" alpha-numeric mask=\"Huruf\" class=\"form-control\" name=\"categoryDocumentType\" placeholder=\"Kategori Dokumen\" ng-model=\"mCategoryDocument.DocumentType\" maxlength=\"10\" required> </div> <bserrmsg field=\"categoryDocumentType\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Dokumen</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" alpha-numeric mask=\"Huruf\" class=\"form-control\" name=\"categoryDocumentName\" placeholder=\"Document Name\" ng-model=\"mCategoryDocument.DocumentName\" maxlength=\"50\" required> </div> <bserrmsg field=\"categoryDocumentName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Status</label> <div> <!-- <input type=\"checkbox\" name=\"enabled\" ng-checked=\"mAssetBrosur.ShowBit\" ng-model=\"mAssetBrosur.ShowBit\"> Aktif<br> --> <input type=\"checkbox\" name=\"enabled\" ng-checked=\"mCategoryDocument.StatusActive\" ng-model=\"mCategoryDocument.StatusActive\"> Aktif<br> </div> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/categoryFeedback/categoryFeedback.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CategoryFeedbackForm\" factory-name=\"CategoryFeedbackFactory\" model=\"mCategoryFeedback\" model-id=\"FeedbackCategoryId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CategoryFeedbackForm\" form-title=\"Category Feedback\" modal-title=\"Category Feedback\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Feedback</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"feedback\" placeholder=\"Feedback\" ng-model=\"mCategoryFeedback.Description\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"feedback\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/categoryModelKendaraanDiminati/categoryModelKendaraanDiminati.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CategoryModelKendaraanDiminatiForm\" factory-name=\"CategoryModelKendaraanDiminatiFactory\" model=\"mCategoryModelKendaraanDiminati\" show-advsearch=\"on\" model-id=\"CategoryModelKendaraanDiminatiId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CategoryModelKendaraanDiminatiForm\" form-title=\"Category Model Kendaraan Diminati\" modal-title=\"Category Model Kendaraan Diminati\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model unit id\" placeholder=\"Model Kendaraan\" ng-model=\"mCategoryModelKendaraanDiminati.ModelKendaraanDiminatiName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"model\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model unit id\" placeholder=\"Value\" ng-model=\"mCategoryModelKendaraanDiminati.ModelKendaraanDiminatiValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"model\"></bserrmsg> </div> </div> </div></bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Description</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model unit id\" placeholder=\"Description\" ng-model=\"mCategoryModelKendaraanDiminati.ModelKendaraanDiminatiName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"model\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"type unit id\" placeholder=\"Value\" ng-model=\"mCategoryModelKendaraanDiminati.ModelKendaraanDiminatiValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"type\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/categoryPointsource/categoryPointsource.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CategoryPointsourceForm\" factory-name=\"CategoryPointsourceFactory\" model=\"mCategoryPointsource\" model-id=\"PointSourceId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CategoryPointsourceForm\" form-title=\"Category Pointsource\" modal-title=\"Category Pointsource\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Category Point Source</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"categoryPointsource\" placeholder=\"Category Point Source\" ng-model=\"mCategoryPointsource.PointSourceName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"categoryPointsource\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/categoryPrePrintedTTUServices/categoryPrePrinterdTTUServices.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CategoryPrePrinterdTTUServicesForm\" factory-name=\"CategoryPrePrinterdTTUServicesFactory\" model=\"mCategoryPrePrinterdTTUServices\" show-advsearch=\"on\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CategoryPrePrinterdTTUServicesForm\" form-title=\"Category PrePrinterd TTUServices\" modal-title=\"Category PrePrinterd TTUServices\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bussiness Type Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model unit id\" placeholder=\"Nama Category\" ng-model=\"mCategoryPointsource.CategoryPointsourceName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"model\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>ShowBit</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model unit id\" placeholder=\"Number\" ng-model=\"mCategoryPointsource.CategoryPointsourceValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"model\"></bserrmsg> </div> </div> </div></bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bussiness Type Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"model unit id\" placeholder=\"Nama Category\" ng-model=\"mCategoryPointsource.CategoryPointsourceName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"model\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>ShowBit</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"type unit id\" placeholder=\"Number\" ng-model=\"mCategoryPointsource.CategoryPointsourceValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"type\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/categoryReasonBookingUnitRevision/categoryReasonBookingUnitRevision.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CategoryReasonBookingUnitRevisionForm\" factory-name=\"CategoryReasonBookingUnitRevisionFactory\" model=\"mCategoryReasonBookingUnitRevision\" model-id=\"ReasonRevisionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CategoryReasonBookingUnitRevisionForm\" form-title=\"Category Reason Revision\" modal-title=\"Category Reason Revision\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Reason Booking Unit Revision</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ReasonRevisionName\" placeholder=\"Reason Booking Unit Revision\" ng-model=\"mCategoryReasonBookingUnitRevision.ReasonRevisionName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ReasonRevisionName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Fund Source</bsreqlabel> <bsselect data=\"listDataFund\" item-text=\"FundSourceName\" item-value=\"FundSourceId\" name=\"FundSourceName\" ng-model=\"mCategoryReasonBookingUnitRevision.FundSourceId\" placeholder=\"Select Fund Source\" icon=\"fa fa-credit-card glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"FundSourceName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/checklistInspeksi/checklistInspeksi.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"ChecklistInspeksiMain\"> <!--<div style=\"display: block\" class=\"grid\" ui-grid=\"grid3\" ui-grid-selection ui-grid-auto-resize ui-grid-pagination></div>--> <bsform-grid ng-form=\"ChecklistInspeksiForm\" factory-name=\"ChecklistInspeksiFactoryMaster\" model=\"mChecklistInspeksi\" model-id=\"GroupChecklistInspectionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ChecklistInspeksiForm\" form-title=\"Checklist Inspeksi\" modal-title=\"Checklist Inspeksi\" grid-hide-action-column=\"true\" hide-new-button=\"true\" form-api=\"formApi\" action-button-caption=\"actButtonCaption\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" show-advsearch=\"on\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 5px 0\"> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                            <label>Model</label>\r" +
    "\n" +
    "                            <bsselect ng-model=\"filter.VehicleModelId\" data=\"optionsModelChecklistInspeksi\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Select Model\">\r" +
    "\n" +
    "                            </bsselect>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        </div> --> <div class=\"col-md-6\"> <label>Nama grup checklist inspeksi</label> <input type=\"text\" class=\"form-control\" ng-model=\"filter.GroupChecklistInspectionName\"> </div> </div> <br> <br> </bsform-advsearch> </bsform-grid> <br> </div> <div ng-show=\"ChecklistInspeksiDetail\"> <div class=\"row\" style=\"margin-bottom:10px;margin-right:10px\"> <button ng-show=\"ShowBtnSimpan\" style=\"float: right\" ng-click=\"SimpanChecklistInspeksi()\" type=\"button\" class=\"rbtn btn\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"ShowBtnSimpan\" type=\"button\" class=\"wbtn btn ng-binding ng-scope\" ng-click=\"KembaliChecklistInspeksi()\" style=\"margin-right:3px;float: right\"> Kembali </button> <button ng-show=\"ShowBtnKembali\" style=\"float: right\" ng-click=\"KembaliChecklistInspeksi()\" type=\"button\" class=\"wbtn btn\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-top:10px\">Nama Grup Checklist Inspeksi</bsreqlabel> <bsselect readonly ng-disabled=\"true\" data=\"optionsGrupChecklistInspeksi\" item-text=\"GroupChecklistInspectionName\" item-value=\"GroupChecklistInspectionId\" ng-model=\"mChecklistInspeksi.GroupChecklistInspectionId\" placeholder=\"Nama Grup\" style=\"min-width: 150px\"> </bsselect> <hr> <bsreqlabel>Detail</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"DisabledEdit\" type=\"text\" class=\"form-control\" name=\"Detail\" placeholder=\"Input item checklist inspection\" ng-model=\"mChecklistInspeksiDaName\" ng-maxlength=\"50\"> </div> <button ng-disabled=\"mChecklistInspeksi_Field_EnableDisable || mChecklistInspeksiDaName==null || mChecklistInspeksiDaName=='' ||mChecklistInspeksiDaName==undefined\" ng-click=\"AddChecklistInspeksiChild(mChecklistInspeksi.GroupChecklistInspectionId,mChecklistInspeksiDaName,mChecklistInspeksi.OutletId)\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px;margin-bottom:10px\"> <span class=\"ladda-label\">Tambah </span><span class=\"ladda-spinner\"> </span> </button> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Deskripsi</th> <th>Detail</th> <th ng-if=\"DisabledEdit == false\">Action</th> </tr> <tr ng-repeat=\"mChecklistInspeksiChildren in mChecklistInspeksi.ListItemInspection track by $index\"> <td>{{mChecklistInspeksiChildren.GroupChecklistInspectionName}}</td> <td>{{mChecklistInspeksiChildren.ChecklistItemInspectionName}}</td> <td ng-if=\"DisabledEdit == false\"><a ng-click=\"RemoveChecklistInspeksi($index)\">Hapus</a></td> </tr> </tbody> </table> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/checklistItemAdministrasi/checklistItemAdministrasi.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ChecklistItemAdministrasiForm\" factory-name=\"ChecklistItemAdministrasiFactoryMaster\" model=\"mChecklistItemAdministrasi\" model-id=\"ChecklistAdminPaymentId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ChecklistItemAdministrasiForm\" form-title=\"Checklist Item Administrasi\" modal-title=\"Checklist Item Administrasi\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" form-api=\"formApi\" do-custom-save=\"doCustomSave\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Checklist Item Administrasi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ChecklistAdminPaymentName\" placeholder=\"Checklist Item Administrasi\" ng-model=\"mChecklistItemAdministrasi.ChecklistAdminPaymentName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ChecklistAdminPaymentName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerList/customerList.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerListForm\" factory-name=\"CustomerListFactory\" model=\"mCustomerList\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerListForm\" form-title=\"Customer List\" modal-title=\"Customer List\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Code\" placeholder=\"Customer Code\" ng-model=\"mCustomerList.Code\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Name\" placeholder=\"Customer Name\" ng-model=\"mCustomerList.Name\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Nick Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NickName\" placeholder=\"Customer Nick Name\" ng-model=\"mCustomerList.NickName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"NickName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Origin Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"OriginCode\" placeholder=\"Origin Code\" ng-model=\"mCustomerList.OriginCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"OriginCode\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>KTP/KITAS</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"KTPKITAS\" placeholder=\"KTP/KITAS\" ng-model=\"mCustomerList.KTPKITAS\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"KTPKITAS\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>SIUP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"SIUP\" placeholder=\"SIUP\" ng-model=\"mCustomerList.SIUP\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"SIUP\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>TDP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TDP\" placeholder=\"TDP\" ng-model=\"mCustomerList.TDP\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"TDP\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>NPWP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NPWP\" placeholder=\"NPWP\" ng-model=\"mCustomerList.NPWP\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"NPWP\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Birth Place</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"BirthPlace\" placeholder=\"SIUP\" ng-model=\"mCustomerList.BirthPlace\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BirthPlace\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Birth Date</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"BirthDate\" placeholder=\"Birth Date\" ng-model=\"mCustomerList.BirthDate\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BirthDate\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Handphone1\" placeholder=\"Handphone 1\" ng-model=\"mCustomerList.Handphone1\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BirthPlace\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Handphone 2</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Handphone2\" placeholder=\"Handphone 2\" ng-model=\"mCustomerList.Handphone2\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BirthPlace\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Email</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Email\" placeholder=\"Email\" ng-model=\"mCustomerList.Email\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Email\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Job</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Job\" placeholder=\"Job\" ng-model=\"mCustomerList.Job\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Job\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Company</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Company\" placeholder=\"Company\" ng-model=\"mCustomerList.Company\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Company\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>PIC Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PICName\" placeholder=\"PIC Name\" ng-model=\"mCustomerList.PICName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PICName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>PIC Hp</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PICHp\" placeholder=\"PIC HP\" ng-model=\"mCustomerList.PICHp\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PICHp\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Department</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Department\" placeholder=\"Department\" ng-model=\"mCustomerList.Department\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Department\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Marital Date</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"MaritalDate\" placeholder=\"Marital Date\" ng-model=\"mCustomerList.MaritalDate\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"MaritalDate\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Total Point</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TotalPoint\" placeholder=\"Total Point\" ng-model=\"mCustomerList.TotalPoint\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"TotalPoint\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Total Point</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TotalPoint\" placeholder=\"Total Point\" ng-model=\"mCustomerList.TotalPoint\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"TotalPoint\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Total Point</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TotalPoint\" placeholder=\"Total Point\" ng-model=\"mCustomerList.TotalPoint\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"TotalPoint\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerListContact/customerListContact.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerListContactForm\" factory-name=\"CustomerListContactFactory\" model=\"mCustomerListContact\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerListContactForm\" form-title=\"Customer List Contact\" modal-title=\"Customer List ontact\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Phone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"Phone\" ng-model=\"mCustomerListContact.Phone\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Phone\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Address</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Address\" placeholder=\"Address\" ng-model=\"mCustomerListContact.Address\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>RT/RW</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"RTRW\" placeholder=\"RTRW\" ng-model=\"mCustomerListContact.RTRW\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"RTRW\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerListPoint/customerListPoint.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerListPointForm\" factory-name=\"CustomerListPointFactory\" model=\"mCustomerListPoint\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerListPointForm\" form-title=\"Customer List Point\" modal-title=\"Customer List Point\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>TotalPoint</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TotalPoint\" placeholder=\"TotalPoint\" ng-model=\"mCustomerListPoint.TotalPoint\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"TotalPoint\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerListPreference/customerListPreference.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerListPreferenceForm\" factory-name=\"CustomerListPreferenceFactory\" model=\"mCustomerListPreference\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerListPreferenceForm\" form-title=\"Customer List Preference\" modal-title=\"Customer List Preference\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Description</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Description\" placeholder=\"Description\" ng-model=\"mCustomerListPreference.Description\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Value\" placeholder=\"Value\" ng-model=\"mCustomerListPreference.Value\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerListPromo/customerListPromo.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <bsform-grid ng-form=\"CustomerListPromoForm\" factory-name=\"CustomerListPromoFactory\" model=\"mCustomerListPromo\" model-id=\"PromoId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerListPromoForm\" form-title=\"Customer List Promo\" modal-title=\"Customer List Promo\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Name Promo</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PromoName\" placeholder=\"Promo Name\" ng-model=\"mCustomerListPromo.PromoName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PromoName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Promo Start</bsreqlabel> <bsdatepicker name=\"StartValidDate\" date-options=\"dateOptions\" ng-model=\"mCustomerListPromo.StartValidDate\"> </bsdatepicker> <bserrmsg field=\"StartValidDate\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Promo End</bsreqlabel> <bsdatepicker name=\"EndValidDate\" date-options=\"dateOptions\" ng-model=\"mCustomerListPromo.EndValidDate\"> </bsdatepicker> <bserrmsg field=\"EndValidDate\"></bserrmsg> </div> <div class=\"form-group\"> <div class=\"input-icon-right\"> <label>Active/InActive</label> <div class=\"switch\"> <input id=\"cmn-toggle-6\" class=\"cmn-toggle cmn-toggle-round-flat\" ng-model=\"mCustomerListPromo.EnableBit\" type=\"checkbox\"> <label for=\"cmn-toggle-6\"></label> </div> </div> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerListSosialMedia/customerListSosialMedia.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerListSosialMediaForm\" factory-name=\"CustomerListSosialMediaFactory\" model=\"mCustomerListSosialMedia\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerListSosialMediaForm\" form-title=\"Customer List Preference\" modal-title=\"Customer List Preference\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Social Media Address</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"SocialMediaAddress\" placeholder=\"SocialMediaAddress\" ng-model=\"mCustomerListSosialMedia.SocialMediaAddress\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"SocialMediaAddress\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Frequently Used</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"FrequentlyUsed\" placeholder=\"Frequently Used\" ng-model=\"mCustomerListSosialMedia.FrequentlyUsed\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"FrequentlyUsed\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerListUnsatified/customerListUnsatified.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerListUnsatifiedForm\" factory-name=\"CustomerListUnsatifiedFactory\" model=\"mCustomerListUnsatified\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerListUnsatifiedForm\" form-title=\"Task List\" modal-title=\"Task List\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Date</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Date\" placeholder=\"Date\" ng-model=\"mCustomerListUnsatified.Date\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Date\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>CSL</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CSL\" placeholder=\"CSL\" ng-model=\"mCustomerListUnsatified.CSL\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"CSL\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>IDI</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"IDI\" placeholder=\"IDI\" ng-model=\"mCustomerListUnsatified.IDI\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"IDI\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Complaint</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CustomerComplaint\" placeholder=\"Customer Complaint\" ng-model=\"mCustomerListUnsatified.CustomerComplaint\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"CustomerComplaint\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Survey</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CustomerSurvey\" placeholder=\"Customer Survey\" ng-model=\"mCustomerListUnsatified.CustomerSurvey\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"CustomerSurvey\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerListVehicle/customerListVehicle.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerListVehicleForm\" factory-name=\"CustomerListVehicleFactory\" model=\"mCustomerListVehicle\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerListVehicleForm\" form-title=\"Customer List Vehicle\" modal-title=\"Customer List Vehicle\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Police Number</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PoliceNumber\" placeholder=\"Police Number\" ng-model=\"mCustomerListVehicle.PoliceNumber\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PoliceNumber\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Year Buy</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"YearBuy\" placeholder=\"Year Buy\" ng-model=\"mCustomerListVehicle.YearBuy\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"YearBuy\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Buy Price</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"BuyPrice\" placeholder=\"Buy Price\" ng-model=\"mCustomerListVehicle.BuyPrice\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BuyPrice\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerPreferenceTP/customerPreferenceTP.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerPreferenceTPForm\" factory-name=\"CustomerPreferenceTPFactory\" model=\"mCustomerPreferenceTP\" model-id=\"PreferenceId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerPreferenceTPForm\" form-title=\"Customer Preference\" modal-title=\"Customer Preference\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Preferensi Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Preference\" placeholder=\"Preferensi Name\" ng-model=\"mCustomerPreferenceTP.PreferenceName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PreferenceName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/customerSosialMediaTP/customerSosialMediaTP.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"CustomerSosialMediaTPForm\" factory-name=\"CustomerSosialMediaTPFactory\" model=\"mCustomerSosialMediaTP\" model-id=\"SocialMediaId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"CustomerSosialMediaTPForm\" form-title=\"Customer Sosial Media\" modal-title=\"Customer Sosial Media\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Sosial Media</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"SocialMediaName\" placeholder=\"Nama Social Media\" ng-model=\"mCustomerSosialMediaTP.SocialMediaName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"SocialMediaName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/diskon/diskon.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "\t\t.nav.nav-tabs {\r" +
    "\n" +
    "\t\t\tdisplay: none;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\ttr:nth-child(even) {\r" +
    "\n" +
    "\t\tbackground-color: #dddddd;\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\tfieldset.scheduler-border {\r" +
    "\n" +
    "\t\twidth: auto;\r" +
    "\n" +
    "\t\tborder: 1px groove #ddd !important;\r" +
    "\n" +
    "\t\tpadding: 0 1.4em 1.4em 1.4em !important;\r" +
    "\n" +
    "\t\tmargin: 0 0 1.5em 0 !important;\r" +
    "\n" +
    "\t\t-webkit-box-shadow: 0px 0px 0px 0px #000;\r" +
    "\n" +
    "\t\tbox-shadow: 0px 0px 0px 0px #000;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\tlegend.scheduler-border {\r" +
    "\n" +
    "\t\twidth: auto;\r" +
    "\n" +
    "\t\t/* Or auto */\r" +
    "\n" +
    "\t\tpadding: 0 10px;\r" +
    "\n" +
    "\t\t/* To give a bit of padding on the left and right */\r" +
    "\n" +
    "\t\tborder-bottom: none;\r" +
    "\n" +
    "\t}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"row\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <!-- <fieldset class=\"scheduler-border\" style=\"margin-top: 10px;\" ng-show=\"ShowFieldGenerate_Diskon\"> --> <legend style=\"background:white\" class=\"scheduler-border\">Data Filter</legend> <div class=\"col-md-12\"> <!-- model --> <div class=\"col-md-3\"> <bslabel>Model</bslabel> <bsselect style=\"margin-bottom: 10px\" ng-model=\"filterModel.VehicleModelId\" on-select=\"onSelectModelChanged(selected)\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelName\" placeholder=\"Pilih Model\" icon=\"fa fa-search\"> </bsselect> </div> <!-- tipe --> <div class=\"col-md-3\"> <bslabel>Tipe</bslabel> <bsselect style=\"margin-bottom: 10px\" ng-model=\"filterModel.VehicleTypeId\" data=\"optionsTipe\" item-text=\"Description,KatashikiCode,SuffixCode\" item-value=\"VehicleTypeId\" on-select=\"onSelectTipeChanged(selected)\" placeholder=\"Pilih tipe\" icon=\"fa fa-search\"> </bsselect> </div> <!-- tahun kendaraan --> <div class=\"col-md-3\"> <bslabel>Tahun Produksi</bslabel> <!-- <select name=\"TahunProduksi\" data-ng-options=\"datatahun.Year for datatahun in ListTahunKendaraan\"\r" +
    "\n" +
    "\t\t\t\t\tdata-ng-model=\"filterModel.TahunProduksi\" class=\"form-control\"\r" +
    "\n" +
    "\t\t\t\t\tstyle=\"width: 100%;margin-bottom: 10px;\">\r" +
    "\n" +
    "\t\t\t\t</select> --> <bsselect style=\"margin-bottom: 10px\" ng-model=\"filterModel.TahunProduksi\" data=\"ListTahunKendaraan\" item-text=\"YearName\" item-value=\"YearName\" on-select=\"onSelectTahunChanged(selected)\" placeholder=\"Pilih Tahun\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-3\"> <bslabel>Rentang Tanggal Efektif</bslabel> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"TanggalFilterStart\" date-options=\"dateOptions\" ng-model=\"filterModel.TanggalFilterStart\"> </bsdatepicker> </div> </div> <div class=\"col-md-1\" style=\"text-align: center\"> <bslabel>s.d</bslabel> </div> <div class=\"col-md-3\"> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"TanggalFilterEnd\" date-options=\"dateOptions\" ng-model=\"filterModel.TanggalFilterEnd\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> &nbsp; <!-- cuma buat ngatur posisi grid --> </div> <div class=\"col-md-2\"> <button class=\"btn wbtn pull-right\" ng-click=\"GetDiskon()\" onclick=\"this.blur()\" style=\"width:101px;margin-right:-9px\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search\"> <i class=\"large icons\"> <i class=\"database icon\"></i> <i class=\"inverted corner filter icon\"></i> </i>Search</button> </div> </div> </fieldset> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Upload File</legend> <div class=\"col-md-6\"> <input type=\"file\" style=\"width: 90%\" accept=\".xlsx\" id=\"uploadSellingFile_Diskon\" onchange=\"angular.element(this).scope().loadXLS(this)\"> </div> <div class=\"col-md-6\"> <button id=\"MasterSelling_Simpan_Button\" ng-click=\"MasterSelling_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button pull-right\" style=\"width:101px\"> Simpan </button> <button id=\"MasterSelling_Batal_Button\" ng-click=\"MasterSelling_Batal_Clicked()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button pull-right\" style=\"width:101px\"> Batal </button> </div> </fieldset> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsselect ng-model=\"ComboBulkAction_Diskon\" data=\"optionsComboBulkAction_Diskon\" ng-change=\"ComboBulkAction_Diskon_Changed()\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" icon=\"fa fa-search\" placeholder=\"Bulk Action\"> </bsselect> </div> <div class=\"col-md-2\"> <div class=\"form-group filterMarg\"> <input type=\"text\" required ng-model=\"textFilterMasterDiskon\" class=\"form-control\" placeholder=\"Search\" maxlength=\"100\" ng-change=\"FilterGridMasterDiskon()\"> </div> </div> <div class=\"col-md-2\"> <div class=\"form-group filterMarg\"> <select required ng-model=\"selectedFilterMasterDiskon\" class=\"form-control\" ng-options=\"item.field as item.name for item in MasterSellingPriceFU_Diskon_grid.columnDefs \" placeholder=\"Filter \"> <option label=\"Filter \" value=\" \" disabled>Filter</option> </select> </div> </div> <div class=\"col-md-2\"> <!-- <button id=\"MasterSelling_Diskon_Cari_Button\" ng-click=\"MasterSelling_Diskon_Cari_Clicked()\" type=\"button\"\r" +
    "\n" +
    "\t\t\t\tclass=\"rbtn btn ng-binding ladda-button\" style=\"float: left;\">\r" +
    "\n" +
    "\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\tCari\r" +
    "\n" +
    "\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t</button> --> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t<input type=\"file\" style=\"width: 90%\" accept=\".xlsx\" id=\"uploadSellingFile_Diskon\"\r" +
    "\n" +
    "\t\t\t\tonchange=\"angular.element(this).scope().loadXLS(this)\">\r" +
    "\n" +
    "\t\t</div> --> <div class=\"col-md-2\"> &nbsp; </div> <div class=\"col-md-2\"> <button id=\"MasterSelling_Diskon_Download_Button\" ng-click=\"MasterSelling_Diskon_Download_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button pull-right\" style=\"width:101px; margin-right:9px\"> <span class=\"ladda-label\"> Download </span><span class=\"ladda-spinner\"></span> </button> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t<button ng-if=\"btnUpload == show\" id=\"MasterSelling_Diskon_Upload_Button\"\r" +
    "\n" +
    "\t\t\t\tng-click=\"MasterSelling_Diskon_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"\r" +
    "\n" +
    "\t\t\t\tstyle=\"float: left; \">\r" +
    "\n" +
    "\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\tUpload\r" +
    "\n" +
    "\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "\t\t</div> --> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"MasterSellingPriceFU_Diskon_grid\" ui-grid-edit ui-grid-selection ui-grid-resize-columns ui-grid-auto-resize ui-grid-pagination class=\"Mygrid\" style=\"width:100%;height: 250px; margin-top:10px; margin-bottom:20px\"> </div> </div> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t<button id=\"MasterSelling_Simpan_Button\" ng-click=\"MasterSelling_Simpan_Clicked()\" type=\"button\"\r" +
    "\n" +
    "\t\t\tclass=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:24px;margin-left:1px\">\r" +
    "\n" +
    "\t\t\tSimpan\r" +
    "\n" +
    "\t\t</button>\r" +
    "\n" +
    "\t\t<button id=\"MasterSelling_Batal_Button\" ng-click=\"MasterSelling_Batal_Clicked()\" type=\"button\"\r" +
    "\n" +
    "\t\t\tclass=\"wbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:24px;margin-left:24px\">\r" +
    "\n" +
    "\t\t\tBatal\r" +
    "\n" +
    "\t\t</button>\r" +
    "\n" +
    "\t</div> --> </div> <div class=\"swal2-container swal2-fade swal2-shown\" style=\"overflow-y: auto\" id=\"DiskonPriceModal\"> <div role=\"dialog\" aria-labelledby=\"swal2-title\" aria-describedby=\"swal2-content\" class=\"swal2-modal swal2-show\" tabindex=\"-1\" style=\"width: 500px; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: 355px\"> <ul class=\"swal2-progresssteps\" style=\"display: none\"></ul> <div class=\"swal2-icon swal2-error\" style=\"display: none\"> <span class=\"x-mark\"> <span class=\"line left\"></span> <span class=\"line right\"></span> </span> </div> <div class=\"swal2-icon swal2-question\" style=\"display: none\">?</div> <div class=\"swal2-icon swal2-warning\" style=\"display: block\">!</div> <div class=\"swal2-icon swal2-info\" style=\"display: none\">i</div> <div class=\"swal2-icon swal2-success\" style=\"display: none\"> <span class=\"line tip\"></span> <span class=\"line long\"></span> <div class=\"placeholder\"></div> <div class=\"fix\"></div> </div> <img class=\"swal2-image\" style=\"display: none\"> <h2 class=\"swal2-title\" id=\"swal2-title\"> <p style=\"font-size:20px\"> Delete <span style=\"color:red\">{{TotalSelectedData}} item(s) </span> from <span style=\"color:blue\">Diskon Price</span> </p> Are you sure? </h2> <div id=\"swal2-content\" class=\"swal2-content\" style=\"display: block\">You won't be able to revert this!</div> <input class=\"swal2-input\" style=\"display: none\"> <input type=\"file\" class=\"swal2-file\" style=\"display: none\"> <div class=\"swal2-range\" style=\"display: none\"> <output></output><input type=\"range\"> </div> <select class=\"swal2-select\" style=\"display: none\"></select> <div class=\"swal2-radio\" style=\"display: none\"></div> <label for=\"swal2-checkbox\" class=\"swal2-checkbox\" style=\"display: none\"> <input type=\"checkbox\"></label> <textarea class=\"swal2-textarea\" style=\"display: none\"></textarea> <div class=\"swal2-validationerror\" style=\"display: none\"></div> <div class=\"swal2-buttonswrapper\" style=\"display: block\"> <button type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-confirm swal2-styled\" ng-click=\"OK_Button_Clicked()\" style=\"background-color: rgb(140, 212, 245); border-left-color: rgb(140, 212, 245); border-right-color: rgb(140, 212, 245)\">OK</button> <button type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-cancel swal2-styled\" style=\"display: inline-block; background-color: rgb(170, 170, 170)\" ng-click=\"DeleteCancel_Button_Clicked()\">Cancel</button> </div> <button type=\"button\" tabindex=\"0\" class=\"swal2-close\" aria-label=\"Close this dialog\" style=\"display: none\">×</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/dpDanBookingFee/dpDanBookingFee.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!--<script src=\"http://alasql.org/console/alasql.min.js\"></script> \r" +
    "\n" +
    "<script src=\"http://alasql.org/console/xlsx.core.min.js\"></script>--> <div ng-show=\"DpDanBookingFeeTable\"> <button ng-click=\"TambahDpDanBookingFee()\" style=\"float: right;margin:-31px 0 0 8px\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <i class=\"fa fa-plus\"></i> Tambah </button> <bsform-grid ng-form=\"DpDanBookingFeeForm\" factory-name=\"DpDanBookingFeeFactoryMaster\" model=\"mDpDanBookingFee\" model-id=\"MasterVehicleBookingFeeDPId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"DpDanBookingFeeForm\" form-title=\"DP & Booking Fee\" modal-title=\"DP & Booking Fee\" modal-size=\"small\" show-advsearch=\"on\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" grid-hide-action-column=\"true\" hide-new-button=\"true\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <bslabel>Model</bslabel> <bsselect style=\"margin-bottom: 10px\" ng-model=\"filter.VehicleModelId\" data=\"optionsModelDpDanBookingFee\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Select Model\" icon=\"fa fa-search\"> </bsselect> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"DpDanBookingFeeTable\"> <p> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div> Upload File </div> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"uploadDpDanBookingFee\" onchange=\"angular.element(this).scope().loadXLSDpDanBookingFee(this)\"> </div> </div> <div class=\"col-md-6\"> <button id=\"MainMasterVirtualAccount_Download_Button\" ng-click=\"DpDanBookingFee_DownloadExcelTemplate()\" type=\"button\" class=\"btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Download </span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"DpDanBookingFee_UploadExcelTemplate()\" type=\"button\" class=\"btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> </p> </div> <div ng-show=\"DpDanBookingFeeDetail\" class=\"row\" style=\"min-height: 100vh\"> <button ng-disabled=\"mDpDanBookingFee.VehicleModelId==null||mDpDanBookingFee.VehicleModelId==undefined||mDpDanBookingFee.VehicleTypeId==null||mDpDanBookingFee.VehicleTypeId==undefined||mDpDanBookingFee.NominalBookingFee==null||mDpDanBookingFee.NominalBookingFee==undefined||mDpDanBookingFee.NominalDP==null||mDpDanBookingFee.NominalDP==undefined||mDpDanBookingFee.ValidOn==null||mDpDanBookingFee.ValidOn==undefined||mDpDanBookingFee.ValidUntil==null||mDpDanBookingFee.ValidUntil==undefined||mDpDanBookingFee.NominalBookingFee==''||mDpDanBookingFee.NominalDP==''\" ng-click=\"SimpanDpDanBookingFee()\" type=\"button\" style=\"float: right;margin-right:10px\" ng-show=\"ShowSimpanDpDanBookingFee\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float: right;margin-right:10px\" ng-click=\"KembaliDariDetailDpDanBookingFee()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> <br> <br> <div class=\"col-md-6\"> <bsreqlabel>Model</bsreqlabel> <bsselect ng-disabled=\"mDpDanBookingFee.MasterVehicleBookingFeeDPId==0\" style=\"margin-bottom: 10px\" ng-model=\"mDpDanBookingFee.VehicleModelId\" ng-change=\"ModelKendaraan_DpDanBookingFeeChanged()\" data=\"optionsModelDpDanBookingFee\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Select Model\" icon=\"fa fa-search\" required> </bsselect> <bsreqlabel>Tipe</bsreqlabel> <bsselect ng-disabled=\"mDpDanBookingFee.MasterVehicleBookingFeeDPId==0\" style=\"margin-bottom: 10px\" ng-model=\"mDpDanBookingFee.VehicleTypeId\" ng-change=\"ModelColor_KreditChanged(mDpDanBookingFee.VehicleTypeId)\" data=\"optionsTipe_DpDanBookingFeeChanged\" item-text=\"Description,KatashikiCode,SuffixCode\" item-value=\"VehicleTypeId\" on-select=\"value(selected)\" placeholder=\"Select Tipe\" icon=\"fa fa-search\" required> </bsselect> <bslabel style=\"margin-top:10px\">Leasing</bslabel> <bsselect ng-disabled=\"mDpDanBookingFee.MasterVehicleBookingFeeDPId==0\" style=\"margin-bottom:10px\" data=\"optionsLeasing\" item-text=\"LeasingName\" item-value=\"LeasingId\" ng-change=\"onSelectedLeasingChange(selected)\" ng-model=\"mDpDanBookingFee.LeasingId\" placeholder=\"Leasing\" style=\"min-width: 150px\"> </bsselect> <!--<bslabel>Tenor</bslabel>\r" +
    "\n" +
    "\t\t\t\t\t<div style=\"float:left;width:120%;\">\r" +
    "\n" +
    "\t\t\t\t\t\t<div style=\"float:left;width:83.5%\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<div class=\"input-icon-right block\" style=\"margin-bottom:10px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" placeholder=\"Tenor (Bulan)\" ng-model=\"mDpDanBookingFee.Tenor\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tng-maxlength=\"50\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t<div style=\"float:left;width:10%\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<label style=\"margin-top:6px;margin-left:3px\">Bulan</label>\r" +
    "\n" +
    "\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t</div>--> <bsreqlabel>Nominal Booking Fee</bsreqlabel> <div class=\"input-icon-right\" style=\"margin-bottom:10px\"> <i class=\"fa fa-pencil\"></i> <input style=\"text-align: right\" ng-disabled=\"!mDpDanBookingFee_Enable\" ng-change=\"returnVal(mDpDanBookingFee.NominalBookingFee)\" input-currency number-only min=\"1\" type=\"text\" class=\"form-control\" name=\"NominalBookingFee\" placeholder=\"Nominal Booking Fee\" ng-model=\"mDpDanBookingFee.NominalBookingFee\" maxlength=\"18\" required> </div> <bsreqlabel>Nominal DP</bsreqlabel> <div class=\"input-icon-right\" style=\"margin-bottom:10px\"> <i class=\"fa fa-pencil\"></i> <input style=\"text-align: right\" ng-disabled=\"!mDpDanBookingFee_Enable\" ng-change=\"returnVal1(mDpDanBookingFee.NominalDP)\" input-currency number-only min=\"1\" type=\"text\" class=\"form-control\" name=\"Nominal DP\" placeholder=\"NominalDP\" ng-model=\"mDpDanBookingFee.NominalDP\" maxlength=\"18\" required> </div> <bsreqlabel>Tanggal Berlaku Dari</bsreqlabel> <div class=\"input-group date\" id=\"datetimepicker3\" style=\"margin-bottom:10px\"> <bsdatepicker ng-disabled=\"mDpDanBookingFee.MasterVehicleBookingFeeDPId==0\" required ng-model=\"mDpDanBookingFee.ValidOn\" date-options=\"dateOptionsDpBookingFeeValidOn\" ng-change=\"DaValidOnDpBookingFeeChanged()\"> </bsdatepicker> </div> <bsreqlabel>Tanggal Berlaku Sampai</bsreqlabel> <div class=\"input-group date\" id=\"datetimepicker3\"> <bsdatepicker ng-disabled=\"mDpDanBookingFee.MasterVehicleBookingFeeDPId==0\" required ng-model=\"mDpDanBookingFee.ValidUntil\" date-options=\"dateOptionsDpBookingFeeValidUntil\"> </bsdatepicker> </div> <!--<input ng-model=\"mDpDanBookingFee.ActiveBit\" type=\"checkbox\"> Active--> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/dropReason/dropReason.html',
    "<!DOCTYPE html> <script type=\"text/javascript\">$(\"#dropdownBtnOveride\").click(function(){\r" +
    "\n" +
    "\tif($('#dropdownContentOveride').css('display') == 'none')\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride').show();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\telse\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride').hide();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "});\r" +
    "\n" +
    "\r" +
    "\n" +
    "$( '#myButton' ).click( function() {\r" +
    "\n" +
    "    $( '#myModal' ).modal();\r" +
    "\n" +
    "  });</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"formHeader\"> </div> <div id=\"formPanel\"> <div class=\"formContent\"> <bsreqlabel>Sales Program Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Sales Program Name\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Sales Address</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Sales Address\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Tanggal Activity Working Calendar</bsreqlabel> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\"> </bsdatepicker> </div> <div class=\"formGroupPanel\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"formGroupPanelBtn\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Informasi Kendaraan <span id=\"formGroupPanelIcon\" style=\"float:right; margin-right:3px\"> + </span> </button> <div class=\"formContent\"> <div class=\"list-group\" id=\"formGroupPanelContent\" aria-labelledby=\"dropdownMenu2\" style=\"display:none\"> <bsreqlabel>Nama Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Tipe Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Tipe Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Warna Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Warna Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </div> <div id=\"formData\"> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtn\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Siap Test Drive <span>(3)</span> <span class=\"caret\" style=\"float:right;margin-top: 10px; margin-right:3px\"></span> </button> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\" style=\"display:none\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 12px\">Peggy Carter</p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">12 December 2016 </li> <li><i class=\"fa fa-circle\" aria-hidden=\"true\" style=\"color:red\"></i> </li> <li>Veloz </li> </ul> </p> </div> <div class=\"col-sm-1\" style=\"padding-right:0px\"> <button class=\"btn btn-default\" style=\"float:right; border:0; padding-left:5px; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 12px\">Maria Hill</p> <p> <ul class=\"list-inline\" style=\"font-size: 10px\"> <li style=\"width:100px\">11 June 2016 </li> <li><i class=\"fa fa-circle\" aria-hidden=\"true\" style=\"color:white\"></i> </li> <li>Veloz </li> </ul> </p> </div> <div class=\"col-sm-1\" style=\"padding-right:0px\"> <button class=\"btn btn-default\" style=\"float:right; border:0; padding-left:5px; background-color:#e8eaed; box-shadow:none\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 12px\">Sam Wilson</p> <p> <ul class=\"list-inline\" style=\"font-size: 10px\"> <li style=\"width:100px\">8 May 2016 </li> <li><i class=\"fa fa-circle\" aria-hidden=\"true\" style=\"color:silver\"></i></li> <li>Veloz</li> </ul> </p> </div> <div class=\"col-sm-1\" style=\"padding-right:0px\"> <button class=\"btn btn-default\" style=\"float:right; border:0; padding-left:5px; background-color:#e8eaed; box-shadow:none\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> </div> </div> <div class=\"modal fade\" id=\"myModal\" role=\"dialog\"> <!-- Modal content--> <div class=\"modal-content\" style=\"width:25%; margin-top:15%; margin-left:45%\"> <ul class=\"list-group\"> <a ng-click=\"LookSelected(da_data.SalesProgramName)\"><li class=\"list-group-item\">Lihat Detail</li></a> <a ng-click=\"ChangeSelected(da_data.SalesProgramId,da_data.SalesProgramName)\"><li class=\"list-group-item\">Ubah Detail</li></a> <a ng-click=\"DeleteSelected(da_data.SalesProgramId)\"><li class=\"list-group-item\">Hapus Detail</li></a> </ul> </div> </div> </div> </div> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Produk Baru <span>(5)</span> <span class=\"caret\" style=\"float:right;margin-top: 10px; margin-right:3px\"></span> </button> <div class=\"list-group\" id=\"dropdownContentOveride\" aria-labelledby=\"dropdownMenu1\" style=\"display:none\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Avanza Veloz</p> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Harrier</p> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Alphard</p> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Fortuner</p> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Innova</p> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/employee/employee.html',
    "<script type=\"text/javascript\">function navigateTab(containerId,divId){\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t$(\"#\"+containerId).children().each(function(n, i) {\r" +
    "\n" +
    "\t\tvar id = this.id;\r" +
    "\n" +
    "\t\tconsole.log(id+' - '+divId);\r" +
    "\n" +
    "\t\tif(id == divId)\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\t$('#'+divId).attr('class', 'tab-pane fade in active');\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t\telse\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\t$('#'+id).attr('class', 'tab-pane fade');\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t});\r" +
    "\n" +
    "}</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "\t\t.nav.nav-tabs {\r" +
    "\n" +
    "\t\t\tdisplay: block !important;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}</style> <bsform-grid ng-form=\"EmployeeForm\" factory-name=\"EmployeeFactory\" model=\"mEmployee\" model-id=\"EmployeeNameId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"EmployeeForm\" form-title=\"Employee\" modal-title=\"Employee\" show-advsearch=\"on\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\" style=\"margin: 0 0 40px 0\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Dealer</label> <bsselect ng-disabled=\"true\" ng-change=\"MasterEmployeeSearchDealerBrubah()\" name=\"Outlet\" ng-model=\"filter.GroupDealerId\" data=\"optionsEmployee_Dealer\" item-text=\"GroupDealerName\" item-value=\"GroupDealerId\" placeholder=\"Select\"> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Cabang</label> <bsselect name=\"Outlet\" ng-model=\"filter.OutletId\" data=\"outlet\" item-text=\"OutletName\" item-value=\"OutletId\" placeholder=\"Select\"> </bsselect> </div> </div> </div> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Nama Perusahaan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"NamaPerusahaan\" placeholder=\"Input Nama Perusahaan\" ng-model=\"mEmployee.CompanyName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"NamaPerusahaan\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Nama Cabang</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"NamaCabang\" placeholder=\"Input Nama Cabang\" ng-model=\"mEmployee.OutletName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"NamaCabang\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Id Employee</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"EmployeeId\" placeholder=\"Id Employee\" ng-model=\"mEmployee.EmployeeId\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"EmployeeId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Nama</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"EmployeeName\" placeholder=\"Input Nama\" ng-model=\"mEmployee.EmployeeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"EmployeeName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Nama Atasan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" placeholder=\"Input Nama Atasan\" ng-model=\"mEmployee.SPVName\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\" show-errors> <label>Jenis Kelamin</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" placeholder=\"Jenis Kelamin\" ng-model=\"mEmployee.Sex\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\" show-errors> <label>No. Telepon</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"Input No. Telepon\" ng-model=\"mEmployee.Phone\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Phone\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Alamat Sales</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea readonly type=\"text\" class=\"form-control\" name=\"Address\" placeholder=\"Address\" ng-model=\"mEmployee.Address\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "                    </textarea></div> <bserrmsg field=\"Address\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Role</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"Role\" placeholder=\"Role\" ng-model=\"mEmployee.LastManPowerPositionTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Role\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Kategori</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"Category\" placeholder=\"Input Kategori\" ng-model=\"mEmployee.Category\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Category\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>No. Handphone</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" placeholder=\"Input No. Handphone\" ng-model=\"mEmployee.Phone\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\" show-errors> <label>Email</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" placeholder=\"Input Email\" ng-model=\"mEmployee.Email\" ng-maxlength=\"50\" required> </div> </div> <label>Tanggal Masuk/Aktif</label> <div class=\"input-group date\" id=\"datetimepicker3\"> <input style=\"padding-left: 10px; margin-bottom: 14px;line-height: 27px !important\" readonly type=\"date\" ng-model=\"mEmployee.EmployeDat\"> </div> <label>Tanggal Tidak Aktif</label> <div class=\"input-group date\" id=\"datetimepicker3\"> <input style=\"padding-left: 10px;line-height: 27px !important\" readonly type=\"date\" ng-model=\"mEmployee.ResigneDat\"> </div> </div> </div> <div class=\"row\"> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "\t\t\t\t\t<label>Role</label>\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input readonly type=\"text\" class=\"form-control\" name=\"Role\" placeholder=\"Role\" ng-model=\"mEmployee.LastManPowerPositionTypeName\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tng-maxlength=\"50\" required\r" +
    "\n" +
    "\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t<bserrmsg field=\"Role\"></bserrmsg>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "\t\t\t\t\t<label>Category</label>\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input readonly type=\"text\" class=\"form-control\" name=\"Category\" placeholder=\"Category\" ng-model=\"mEmployee.Category\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tng-maxlength=\"50\" required\r" +
    "\n" +
    "\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t<bserrmsg field=\"Category\"></bserrmsg>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "\t\t\t\t\t<label>No HP</label>\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input readonly type=\"text\" class=\"form-control\" placeholder=\"No HP\" ng-model=\"mEmployee.Phone\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tng-maxlength=\"50\" required\r" +
    "\n" +
    "\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "\t\t\t\t\t<label>Email</label>\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input readonly type=\"text\" class=\"form-control\" placeholder=\"Email\" ng-model=\"mEmployee.Email\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tng-maxlength=\"50\" required\r" +
    "\n" +
    "\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<label>Tanggal Masuk/Aktif</label>\r" +
    "\n" +
    "\t\t\t\t<div class='input-group date' id='datetimepicker3'>\r" +
    "\n" +
    "\t\t\t\t<input style=\"margin-top:5px;margin-bottom: 10px;\" readonly type=\"date\" ng-model=\"mEmployee.EmployeDat\">\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<label>Tanggal Tidak Aktif</label>\r" +
    "\n" +
    "\t\t\t\t<div class='input-group date' id='datetimepicker3'>\r" +
    "\n" +
    "\t\t\t\t<input readonly type=\"date\" ng-model=\"mEmployee.ResigneDat\">\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "                \r" +
    "\n" +
    "\t\t\t</div> --> </div> <ul class=\"nav nav-tabs\" style=\"margin-top:10px;margin-left:10px\"> <li id=\"TabTraining\" class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer_MasterEmployee','menuEmployeeTraining')\" ng-click=\"TabTrainingPressed()\">Pelatihan</a></li> <li id=\"TabCertification\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer_MasterEmployee','menuEmployeeCertification')\" ng-click=\"TabCertificationPressed()\">Certification</a></li> </ul> <div id=\"tabContainer_MasterEmployee\" class=\"tab-content\" style=\"margin-left:10px\"> <div id=\"menuEmployeeTraining\" class=\"tab-pane fade in active\"> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\"> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Nama Pelatihan</th> <th>Tanggal Pelatihan</th> <th>Status Pelatihan</th> <th>Nilai Pelatihan</th> </tr> <tr ng-repeat=\"DaEmployeeTraining in mEmployee.Trainings\"> <td>{{DaEmployeeTraining.TrainingName}}</td> <td>{{DaEmployeeTraining.TraningDate| date:\"dd MMMM yyyy\"}}</td> <td>{{DaEmployeeTraining.ResultStatus}}</td> <td>{{DaEmployeeTraining.Value}}</td> </tr> </tbody> </table> </div> </div> <div id=\"menuEmployeeCertification\" class=\"tab-pane fade\"> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\"> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Nama Sertifikasi</th> <th>Tanggal Sertifikasi</th> <th>Status Sertifikasi</th> <th>Nilai Sertifikasi</th> </tr> <tr ng-repeat=\"DaEmployeeCertification in mEmployee.Certifications\"> <td>{{DaEmployeeCertification.CertificationName}}</td> <td>{{DaEmployeeCertification.CertificationDate| date:\"dd MMMM yyyy\"}}</td> <td>{{DaEmployeeCertification.ResultStatus}}</td> <td>{{DaEmployeeCertification.Value}}</td> </tr> </tbody> </table> </div> </div> </div>  </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/financeDaftarCash/financeDaftarCash.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"FinanceDaftarCashForm\" factory-name=\"FinanceDaftarCashFactory\" model=\"mFinanceDaftarCash\" model-id=\"DaftarCashId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"FinanceDaftarCashForm\" form-title=\"Finance Daftar Cash\" modal-title=\"Finance Daftar Cash\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Daftar Cash</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"FinanceDaftarCash\" placeholder=\"Daftar Cash\" ng-model=\"mFinanceDaftarCash.NamaDaftarCash\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"FinanceDaftarCash\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Value Daftar Cash</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"ValueDaftarCash\" placeholder=\"Value Daftar Cash\" ng-model=\"mFinanceDaftarCash.ValueDaftarCash\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"ValueDaftarCash\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/financeDaftarRekeningBank/financeDaftarRekeningBank.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"FinanceDaftarRekeningBankForm\" factory-name=\"FinanceDaftarRekeningBankFactory\" model=\"mfinancedaftarrekeningbank\" model-id=\"AccountBankId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"FinanceDaftarRekeningBankForm\" form-title=\"Nama Rekening\" modal-title=\"FinanceDaftarRekeningBank\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bslabel>Bank</bslabel> <bsselect name=\"bank_id\" ng-model=\"mfinancedaftarrekeningbank.BankId\" data=\"optionsBank\" item-text=\"BankName\" item-value=\"BankId\" placeholder=\"Select Bank\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"bank_id\"></bserrmsg> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Rekening</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AccountBankName\" placeholder=\"nama rekening\" ng-model=\"mfinancedaftarrekeningbank.AccountBankName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AccountBankName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>No Daftar RekeningBank</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AccountBankNo\" placeholder=\"no rekening\" ng-model=\"mfinancedaftarrekeningbank.AccountBankNo\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AccountBankNo\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/financeFundSource/financeFundSource.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"FinanceFundSourceForm\" factory-name=\"FinanceFundSourceFactory\" model=\"mFinanceFundSource\" model-id=\"FinanceFundSourceId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"FinanceFundSourceForm\" form-title=\"FinanceFundSource\" modal-title=\"Finance Fund Source\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Fund Source Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"fundSourceName\" placeholder=\"Fund Source Name\" ng-model=\"mFinanceFundSource.FundSourceName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"fundSourceName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/globalParameter/globalParameter.html',
    "<script type=\"text/javascript\">for (i = 0; i < 2; i++) \r" +
    "\n" +
    "{\r" +
    "\n" +
    "    var number = document.getElementById('MasterGlobalParameter_PreventNegative_'+i);\r" +
    "\n" +
    "\t// Listen for input event on numInput.\r" +
    "\n" +
    "\tnumber.onkeydown = function(e) {\r" +
    "\n" +
    "\t\tif(!((e.keyCode >= 48 && e.keyCode <= 57)\r" +
    "\n" +
    "\t\t  || e.keyCode == 8|| e.keyCode == 46)) {\r" +
    "\n" +
    "\t\t\t//titik tu 190 buat decimal\r" +
    "\n" +
    "\t\t\treturn false;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "}</script> <style type=\"text/css\"></style> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <div class=\"row\" style=\"min-height: 100vh\" ng-if=\"user.RoleName == 'ADH'\"> <button class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:-10px;margin-right:20px\" ng-click=\"SimpanGlobalParameter()\"> Simpan </button> <!--<div class=\"col-md-12\" style=\" margin-top: 20px;\">\r" +
    "\n" +
    "\t  <b style=\" margin-top:-20px; margin-left:14px;\">Process Billing dan AFI</b>\r" +
    "\n" +
    "\t  <hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t  <table style=\"margin-left:15px;margin-bottom:15px\" border=\"1\">\r" +
    "\n" +
    "\t\t<tr style=\"background-color:#CCCCCC;\">\r" +
    "\n" +
    "\t\t\t<th><div style=\"margin-left:5px;margin-right:5px;\"><p align=\"center\">Keterangan</p></div></th>\r" +
    "\n" +
    "\t\t\t<th><div style=\"margin-left:5px;margin-right:5px;\"><p align=\"center\">Urutan</p></div></th>\r" +
    "\n" +
    "\t\t\t<th><div style=\"margin-left:5px;margin-right:5px;\"><p align=\"center\">Automatic Process</p></div></th>\r" +
    "\n" +
    "\t\t</tr>\r" +
    "\n" +
    "\t\t<tr style=\"background-color:#EEEEEE;\">\r" +
    "\n" +
    "\t\t\t<td align=\"center\">\r" +
    "\n" +
    "\t\t\t\tAFI\r" +
    "\n" +
    "\t\t\t</td>\r" +
    "\n" +
    "\t\t\t<td align=\"center\">\r" +
    "\n" +
    "\t\t\t\t<select \r" +
    "\n" +
    "\t\t\t\t\t\tclass=\"form-control\"\r" +
    "\n" +
    "\t\t\t\t\t\tstyle=\"margin-left:5px;margin-right:5px\"\r" +
    "\n" +
    "\t\t\t\t\t\tng-model=\"mGlobalParameterAdh.UrutanAfi\"\r" +
    "\n" +
    "\t\t\t\t\t\tng-change=\"GlobalParameterJagaSwitch()\"\r" +
    "\n" +
    "\t\t\t\t\t\tng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionGlobalParameter1Sama2\"\r" +
    "\n" +
    "\t\t\t\t\t\tplaceholder=\"Pilih\">\r" +
    "\n" +
    "\t\t\t\t\t</select>\r" +
    "\n" +
    "\t\t\t</td>\r" +
    "\n" +
    "\t\t\t<td align=\"center\">\r" +
    "\n" +
    "\t\t\t\t<div id=\"toogleItem\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<div class=\"switch\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<input ng-change=\"GlobalParameterJagaSwitch()\" ng-model=\"mGlobalParameterAdh.AfiAuto\" id=\"GlobalParamSwitch\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<label for=\"GlobalParamSwitch\"></label>\r" +
    "\n" +
    "\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</td>\r" +
    "\n" +
    "\t\t</tr>\r" +
    "\n" +
    "\t\t<tr>\r" +
    "\n" +
    "\t\t\t<td align=\"center\">\r" +
    "\n" +
    "\t\t\t\tBilling\r" +
    "\n" +
    "\t\t\t</td>\r" +
    "\n" +
    "\t\t\t<td align=\"center\">\r" +
    "\n" +
    "\t\t\t\t\t<select \r" +
    "\n" +
    "\t\t\t\t\t\tclass=\"form-control\"\r" +
    "\n" +
    "\t\t\t\t\t\tstyle=\"margin-left:5px;margin-right:5px\"\r" +
    "\n" +
    "\t\t\t\t\t\tng-model=\"mGlobalParameterAdh.UrutanBilling\"\r" +
    "\n" +
    "\t\t\t\t\t\tng-change=\"GlobalParameterJagaSwitch2()\"\r" +
    "\n" +
    "\t\t\t\t\t\tng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionGlobalParameter1Sama2\"\r" +
    "\n" +
    "\t\t\t\t\t\tplaceholder=\"Pilih\">\r" +
    "\n" +
    "\t\t\t\t\t</select>\r" +
    "\n" +
    "\t\t\t</td>\r" +
    "\n" +
    "\t\t\t<td align=\"center\">\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t</td>\r" +
    "\n" +
    "\t\t</tr>\r" +
    "\n" +
    "\t  </table>\r" +
    "\n" +
    "\t</div>--> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 10px\"> <p style=\"width:150px; margin-top:-20px; margin-left:14px; background:white\"> <b>Process Billing dan AFI</b> </p> <table style=\"margin-left:15px;margin-bottom:15px\" border=\"1\"> <tr style=\"background-color:#CCCCCC\"> <th><div style=\"margin-left:5px;margin-right:5px\"><br><p align=\"center\">Keterangan</p></div></th> <th><div style=\"margin-left:5px;margin-right:5px\"><br><p align=\"center\">Urutan</p></div></th> <th><div style=\"margin-left:5px;margin-right:5px\"><br><p align=\"center\">Automatic Process</p></div></th> </tr> <tr style=\"background-color:#EEEEEE\"> <td align=\"center\"> AFI </td> <td align=\"center\"> <select class=\"form-control\" ng-model=\"mGlobalParameterAdh.UrutanAfi\" ng-change=\"GlobalParameterJagaSwitch()\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionGlobalParameter1Sama2\" placeholder=\"Pilih\"> </select> </td> <td align=\"center\"> <div id=\"toogleItem\"> <div class=\"input-icon-right\"> <div style=\"margin-top:5px\" class=\"switch\"> <input ng-change=\"GlobalParameterJagaSwitch()\" ng-model=\"mGlobalParameterAdh.AfiAuto\" id=\"GlobalParamSwitch\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"GlobalParamSwitch\"></label> </div> </div> </div> </td> </tr> <tr> <td align=\"center\"> Billing </td> <td align=\"center\"> <select class=\"form-control\" ng-model=\"mGlobalParameterAdh.UrutanBilling\" ng-change=\"GlobalParameterJagaSwitch2()\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionGlobalParameter1Sama2\" placeholder=\"Pilih\"> </select> </td> <td align=\"center\"> </td> </tr> </table> </div> <!--<div class=\"col-md-12\" style=\" margin-top: 20px\">\r" +
    "\n" +
    "\t  <b style=\"margin-top:-20px; margin-left:14px; background:white;\">Cetakan FKB</b>\r" +
    "\n" +
    "\t\t<hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t\t<p><input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdh.ShowBbn\"><label style=\"margin-left:5px;\">Show BBN</label></p>\r" +
    "\n" +
    "\t\t<p><input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdh.ShowDiskon\"><label style=\"margin-left:5px;\">Show Diskon</label></p>\r" +
    "\n" +
    "\t\t<p><input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdh.ShowPph22\"><label style=\"margin-left:5px;\">Show PPnBM</label></p>\r" +
    "\n" +
    "\t</div>--> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 20px\"> <p style=\"width:90px; margin-top:-20px; margin-left:14px; background:white\"> <b>Cetakan FKB</b> </p> <table style=\"margin-left:5px\"> <tr> <td> <p><input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdh.ShowBbn\"><label style=\"margin-left:5px\">Show BBN</label></p> </td> <td> <p><input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdh.ShowDiskon\"><label style=\"margin-left:5px\">Show Diskon</label></p> </td> </tr> <tr> <td> <p><input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdh.ShowPph22\"><label style=\"margin-left:5px\">Show PPnBM</label></p> </td> <td> </td> </tr> </table> </div> <div class=\"col-md-11\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 20px;margin-left: 20px\"> <p style=\"width:150px; margin-top:-20px; margin-left:14px; background:white\"><b>Generate Number SPK</b></p> <input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdh.GenerateNumberSpkBySystem\"><label style=\"margin-left:5px\">Generate Oleh System</label> </div> </div> <div class=\"row\" style=\"min-height: 100vh\" ng-if=\"user.RoleName == 'Admin TAM'\"> <button class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:-10px;margin-right:20px\" ng-click=\"SimpanGlobalParameter()\"> Simpan </button> <!--<div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 20px;margin-left: 20px\">\r" +
    "\n" +
    "\t  <b style=\"margin-top:-20px; margin-left:14px; background:white;\">Mengaktifasi Tanda Tangan SPK</b>\r" +
    "\n" +
    "\t  <hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t  <input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminTam.TandaTanganSpk\"><label style=\"margin-left:5px;\">Aktifasi Tanda Tangan</label>\r" +
    "\n" +
    "\t</div>--> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 10px\"> <p style=\"width:190px; margin-top:-20px; margin-left:14px; background:white\"> <b>Mengaktifasi Tanda Tangan SPK</b> </p> <input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminTam.TandaTanganSpk\"><label style=\"margin-left:5px\">Aktifasi Tanda Tangan</label> </div> <!--<div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 20px;margin-left: 20px\">\r" +
    "\n" +
    "\t  <b style=\"margin-top:-20px; margin-left:14px; background:white;\">Mengaktifkan Reserved Stock (User TAM)</b>\r" +
    "\n" +
    "\t  <hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t  <input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminTam.ReversedStock\"><label style=\"margin-left:5px;\">Aktifasi Reserved Stock (User TAM)</label>\r" +
    "\n" +
    "\t</div>--> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 20px\"> <p style=\"width:250px; margin-top:-20px; margin-left:14px; background:white\"> <b>Mengaktifkan Reserved Stock (User TAM)</b> </p> <input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminTam.ReversedStock\"><label style=\"margin-left:5px\">Aktifasi Reserved Stock (User TAM)</label> </div> <!--<div ng-hide=\"true\" class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 20px;margin-left: 20px\">\r" +
    "\n" +
    "\t  <b style=\"margin-top:-20px; margin-left:14px; background:white;\">Mengaktifkan Area Violation (User TAM)</b>\r" +
    "\n" +
    "\t  <hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t  <input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminTam.AreaViolation\"><label style=\"margin-left:5px;\">Aktifasi Area Violation (User TAM)</label>\r" +
    "\n" +
    "\t</div>--> <div ng-hide=\"true\" class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 20px\"> <p style=\"width:250px; margin-top:-20px; margin-left:14px; background:white\"> <b>Mengaktifkan Area Violation (User TAM)</b> </p> <input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminTam.AreaViolation\"><label style=\"margin-left:5px\">Aktifasi Area Violation (User TAM)</label> </div> <!--<div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 20px;margin-left: 20px\">\r" +
    "\n" +
    "\t  <b style=\"margin-top:-20px; margin-left:14px; background:white;\">Mengaktifkan Paket DIO/Aksesoris (User TAM) </b>\r" +
    "\n" +
    "\t  <hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t  <input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminTam.PaketDioAksesoris\"><label style=\"margin-left:5px;\">Aktifasi Paket DIO/Aksesoris (User TAM)</label>\r" +
    "\n" +
    "\t</div>--> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 20px\"> <p style=\"width:280px; margin-top:-20px; margin-left:14px; background:white\"> <b>Mengaktifkan Paket DIO/Aksesoris (User TAM)</b> </p> <input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminTam.PaketDioAksesoris\"><label style=\"margin-left:5px\">Aktifasi Paket DIO/Aksesoris (User TAM)</label> </div> </div> <div class=\"row\" style=\"min-height: 100vh\" ng-if=\"user.RoleName == 'Admin HO'\"> <button class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:-10px;margin-right:20px\" ng-click=\"SimpanGlobalParameter()\"> Simpan </button> <!--<div class=\"col-md-12\" style=\"margin-top: 20px;\">\r" +
    "\n" +
    "\t  <b style=\"margin-top:-20px; margin-left:14px; background:white;\">Kuota Urgent Memo</b> \r" +
    "\n" +
    "\t  <hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t  <p><input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminHo.KuotaUrgentMemoAktif\"><label style=\"margin-left:5px;\">Aktif/Non Aktif</label></p>\r" +
    "\n" +
    "\t  <p><label style=\"margin-left:10px;\">Batas Pengajuan Urgent Memo</label> &nbsp;<input id=\"MasterGlobalParameter_PreventNegative_0\" min=\"0\" type=\"number\" ng-model=\"mGlobalParameterAdminHo.KuotaUrgentMemoKali\"> &nbsp; kali</p>\r" +
    "\n" +
    "\t</div>--> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 10px\"> <p style=\"width:150px; margin-top:-20px; margin-left:14px; background:white\"> <b>Kuota Urgent Memo</b> </p> <p><input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminHo.KuotaUrgentMemoAktif\"><label style=\"margin-left:5px\">Aktif/Non Aktif</label></p> <p><label style=\"margin-left:10px\">Batas Pengajuan Urgent Memo</label> &nbsp;<input id=\"MasterGlobalParameter_PreventNegative_0\" min=\"0\" type=\"number\" ng-model=\"mGlobalParameterAdminHo.KuotaUrgentMemoKali\" ng-disabled=\"!mGlobalParameterAdminHo.KuotaUrgentMemoAktif\"> &nbsp; kali</p> </div> <!--<div class=\"col-md-12\" style=\" margin-top: 20px;\">\r" +
    "\n" +
    "\t  <b style=\"margin-top:-20px; margin-left:14px; background:white;\">Kuota Revisi PDD</b>\r" +
    "\n" +
    "\t  <hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t  <p><input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminHo.KuotaRevisiPddAktif\"><label style=\"margin-left:5px;\">Aktif/Non Aktif</label></p>\r" +
    "\n" +
    "\t  <p><label style=\"margin-left:10px;\">Batas Revisi PDD</label> &nbsp;<input id=\"MasterGlobalParameter_PreventNegative_1\" min=\"0\" type=\"number\" ng-model=\"mGlobalParameterAdminHo.KuotaRevisiPddKali\"> &nbsp; kali</p>\r" +
    "\n" +
    "\t</div>--> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 20px\"> <p style=\"width:130px; margin-top:-20px; margin-left:14px; background:white\"> <b>Kuota Revisi PDD</b> </p> <p><input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminHo.KuotaRevisiPddAktif\"><label style=\"margin-left:5px\">Aktif/Non Aktif</label></p> <p><label style=\"margin-left:10px\">Batas Revisi PDD</label> &nbsp;<input id=\"MasterGlobalParameter_PreventNegative_1\" min=\"0\" type=\"number\" ng-model=\"mGlobalParameterAdminHo.KuotaRevisiPddKali\" ng-disabled=\"!mGlobalParameterAdminHo.KuotaRevisiPddAktif\"> &nbsp; kali</p> </div> <!--<div class=\"col-md-12\" style=\"margin-top: 20px;\">\r" +
    "\n" +
    "\t  <b style=\"margin-top:-20px; margin-left:14px; background:white;\">Calculate include PKB</b>\r" +
    "\n" +
    "\t  <hr style=\" margin-top:0px;\"/>\r" +
    "\n" +
    "\t  <p><input style=\"margin-left:10px;\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminHo.IncludePkb\"><label style=\"margin-left:5px;\">Yes/No</label></p>\r" +
    "\n" +
    "\t</div>--> <div class=\"col-md-11\" style=\"margin-left:20px;border:1px solid lightgrey; padding: 10px 0 15px 0; margin-top: 20px\"> <p style=\"width:160px; margin-top:-20px; margin-left:14px; background:white\"> <b>Calculate include PKB</b> </p> <p><input style=\"margin-left:10px\" type=\"checkbox\" ng-model=\"mGlobalParameterAdminHo.IncludePkb\"><label style=\"margin-left:5px\">Yes/No</label></p> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/groupChecklistFinalInspection/groupChecklistFinalInspection.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div class=\"row\" style=\"margin:0 0 0 0\" ng-show=\"Main\"> <button class=\"rbtn btn\" style=\"margin-left:10px;margin-top:-30px; float:right\" ng-click=\"tambah()\"><i class=\"fa fa-plus\"></i>&nbsp &nbsp Tambah</button> <bsform-grid ng-form=\"GroupChecklistFinalInspectionForm\" grid-hide-action-column=\"true\" factory-name=\"GroupChecklistFinalInspectionFactoryMaster\" model=\"mGroupChecklistFinalInspection\" model-id=\"GroupChecklistInspectionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"GroupChecklistFinalInspectionForm\" form-title=\"Group Checklist Inspection\" modal-title=\"Group Checklist Inspection\" modal-size=\"small\" hide-new-button=\"true\" icon=\"fa fa-fw fa-child\"> </bsform-grid> </div> <div class=\"row\" style=\"margin:0 0 0 0\" ng-show=\"!Main\"> <button ng-if=\"status != 'lihat'\" class=\"rbtn btn\" style=\"margin: -30px 0 0 5px; float:right\" ng-click=\"Simpan()\">Simpan</button> <button class=\"wbtn btn\" style=\"margin: -30px 0 0 0; float:right\" ng-click=\"Kembali()\">Kembali</button> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Group Checklist Inspection</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" ng-disabled=\"status == 'lihat'\" class=\"form-control\" name=\"GroupChecklistInspectionName\" placeholder=\"Group Checklist Inspection\" ng-model=\"mGroupChecklistFinalInspection.GroupChecklistInspectionName\" maxlength=\"50\" required> </div> </div> <!-- <bslabel>Model</bslabel>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<bsselect style=\"margin-bottom: 10px;\" ng-disabled=\"status == 'lihat'\" ng-model=\"mGroupChecklistFinalInspection.VehicleModelId\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\tdata=\"optionsModelGroupChecklistFinalInspection\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Select Model\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\ticon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</bsselect> --> <bsreqlabel>Gambar</bsreqlabel> <div class=\"row\" style=\"margin: 0 0 0 0; min-height: 400px;padding-top: 5px\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"row\" style=\"margin: 0 0 10px 0;padding-left:0px !important\"> <div class=\"col-md-8\" style=\"padding-left: 0px !important\"> <div class=\"input-icon-right\"> <p style=\"text-align:center\"> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"mGroupChecklistFinalInspection.GroupCheckListImage.FileName\" name=\"fieldNoSpkEnd\" placeholder=\"\" ng-maxlength=\"50\" required> <abbr ng-if=\"status == 'buat' || status == 'ubah'\" ng-click=\"hapusGambar()\" class=\"select2-search-choice-close\" style=\"color:#d90f0f !important;margin-top: 5px\"></abbr> </p> </div> </div> <div class=\"col-md-4\" style=\"padding-right: 0px;padding-left: 0px !important\" ng-if=\"status == 'buat'\"> <label class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;width: 100% !important\"> <i class=\"fa fa-folder-open\"></i> Browse <input type=\"file\" loading=\"GroupChecklistFinalInspectionFormModal\" formattype=\"['jpg','png','img']\" bs-uploadsingle=\"onFileSelect($files)\" img-upload=\"mGroupChecklistFinalInspection.GroupCheckListImage\" style=\"display:none\"> </label> </div> <div class=\"col-md-4\" style=\"padding-right: 0px !important;padding-left: 0px !important\" ng-disabled=\"mGroupChecklistFinalInspection.GroupCheckListImage == null\" ng-if=\"status == 'lihat'\"> <button class=\"rbtn btn\" style=\"width: 100%\" ng-click=\"lihatImage(mGroupChecklistFinalInspection)\">Lihat</button> </div> <div class=\"col-md-2\" style=\"padding-right: 0px;padding-left: 0px !important\" ng-if=\"status == 'ubah'\"> <label class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;width: 100% !important\"> <i class=\"fa fa-folder-open\"></i> Browse <input type=\"file\" loading=\"GroupChecklistFinalInspectionFormModal\" formattype=\"['jpg','png','img']\" bs-uploadsingle=\"onFileSelect($files)\" img-upload=\"mGroupChecklistFinalInspection.GroupCheckListImage\" style=\"display:none\"> </label> </div> <div class=\"col-md-2\" style=\"padding-right: 0px !important;padding-left: 0px !important\" ng-disabled=\"mGroupChecklistFinalInspection.GroupCheckListImage == null\" ng-if=\"status == 'ubah'\"> <button class=\"rbtn btn\" style=\"width: 100%\" ng-click=\"lihatImage(mGroupChecklistFinalInspection)\">Lihat</button> </div> </div> </div> </div> </div> </div> <div class=\"ui modal grupChecklist\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4><b>Gambar</b></h4> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> </div> <div class=\"modal-body\"> <!--<img ng-src=\"{{viewdocument}}\" image-set-aspect ng-class=\"{'wide': viewdocument.width/viewdocument.height > 1, 'tall': viewdocument.width/viewdocument.height <= 1}\">--> <img ng-src=\"{{mGroupChecklistFinalInspection.GroupCheckListImage.UpDocObj}}\" style=\"text-align: center; max-width: 860px\"> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <!-- <button ng-click=\"btnkehilanganSpk()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width:15%;\">Simpan</button> --> <button ng-click=\"keluarImage()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Keluar</button> </div> </div> </div> </div> <div class=\"ui modal GroupChecklistFinalInspectionFormModal\" style=\"height:200px; background-color:transparent; box-shadow: none !important\"> <div align=\"center\" class=\"list-group-item\" style=\"border: none !important;padding:10px 0px 10px 0px; background-color:transparent\"> <font color=\"white\"><i class=\"fa fa-spinner fa-spin fa-3x fa-lg\"></i> <b> Upload Image.... </b> </font> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/jabatan/jabatan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"JabatanForm\" factory-name=\"MasterJabatanFactory\" model=\"mJabatan\" model-id=\"CustomerPositionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"JabatanForm\" form-title=\"Jabatan\" modal-title=\"Jabatan\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Jabatan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CustomerPositionName\" placeholder=\"Jabatan\" ng-model=\"mJabatan.CustomerPositionName\" maxlength=\"25\" required> </div> <bserrmsg field=\"CustomerPositionName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/jasa/jasa.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.unstyled::-webkit-inner-spin-button {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "        -webkit-appearance: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .text-right {\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t#layoutContainer_JasaForm {\r" +
    "\n" +
    "        min-height:100vh !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t.PaketAksesorisWarnainYgGanjil {\r" +
    "\n" +
    "\t\tbackground-color: #EEEEEE;\r" +
    "\n" +
    "\t}</style> <div ng-show=\"ShowMasterJasaMain\"> <button ng-click=\"AddJasaMaster()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:-30px;margin-right:5px;margin-left:15px\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> <bsform-grid ng-form=\"JasaForm\" factory-name=\"JasaFactory\" model=\"mJasa\" model-id=\"JasaId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"JasaForm\" on-bulk-delete=\"HapusJasa\" hide-new-button=\"true\" form-title=\"Jasa\" modal-title=\"Jasa\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" do-custom-save=\"doCustomSave\" grid-hide-action-column=\"true\" form-api=\"formApi\"> </bsform-grid> </div> <div class=\"row\" ng-show=\"ShowMasterJasaDetail\"> <div style=\"float:right;margin-right:10px\"> <button ng-click=\"KembaliKeJasaMasterMain()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> <button ng-hide=\"MasterJasaOperation=='Lihat'\" ng-disabled=\"MasterJasaOperation=='Lihat'||mJasa.JasaName==null||mJasa.JasaName==''||mJasa.JasaName==undefined||mJasa.JasaCode==null||mJasa.JasaCode==''||mJasa.JasaCode==undefined||gridJasaDaAksesoris.data==null||gridJasaDaAksesoris.data.length<1||gridJasaDaAksesoris.data==undefined || mJasa.MaterialTypeId==null || mJasa.MaterialTypeId==undefined\" ng-click=\"MasterJasaSimpan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span> <span class=\"ladda-spinner\"></span> </button> </div> <div style=\"margin-left:0px;margin-right:0px;margin-top:50px\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Kode Jasa</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"MasterJasaOperation=='Lihat'\" type=\"text\" class=\"form-control\" name=\"KodeJasa\" placeholder=\"Kode Jasa\" ng-model=\"mJasa.JasaCode\" maxlength=\"50\" required> </div> </div> <div class=\"form-group\"> <bsreqlabel>Nama Jasa</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"MasterJasaOperation=='Lihat'\" type=\"text\" class=\"form-control\" name=\"NamaJasa\" placeholder=\"Nama Jasa\" ng-model=\"mJasa.JasaName\" maxlength=\"50\" required> </div> </div> <div class=\"form-group\"> <bsreqlabel>Tipe Jasa</bsreqlabel> <bsselect ng-model=\"mJasa.MaterialTypeId\" data=\"optionsMasterJasaTipeJasa\" item-text=\"MaterialTypeName\" item-value=\"MaterialTypeId\" on-select=\"value(selected)\" placeholder=\"Pilih Tipe Jasa\" icon=\"fa fa-search\" ng-disabled=\"MasterJasaOperation=='Lihat' || MasterJasaOperation=='Update'\"> </bsselect> </div> <div style=\"margin-top:20px; margin-bottom:65px\"> <button ng-disabled=\"MasterJasaOperation=='Lihat'\" ng-click=\"AddJasaVehicleTypeChild()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridJasaDaAksesoris\" ui-grid-selection ui-grid-auto-resize></div> </div> </div> </div> <div class=\"ui modal ModalJasaVehicleTypeChild\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"BatalLookupModalJasaVehicleTypeChild()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Pilih Kendaraan</h4> </div> <br> <br> <div class=\"modal-body\"> <div class=\"form-group\" style=\"padding-left:0 !important\"> <div class=\"form-group\"> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"Model\" ng-model=\"mJasa.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"SelectedModel(selected)\" placeholder=\"Pilih Model\" icon=\"fa fa-search\" ng-disabled=\"JasaDaMode=='edit'\"> </bsselect> </div> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridModalJasaDaAksesoris\" ui-grid-selection ui-grid-auto-resize></div> </div> <div class=\"modal-footer\"> <button ng-click=\"SelectJasaVehicleTypeChild()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\"> Pilih </span> <span class=\"ladda-spinner\"></span> </button> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/karoseri/karoseri.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"MasterKaroseriMain\"> <button ng-hide=\"MasterKaroseriOperation=='Lihat'||MasterKaroseriOperation=='Ubah'\" ng-click=\"AddMasterKaroseri()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:-30px;margin-right:5px;margin-left:15px\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> <bsform-grid ng-form=\"KaroseriForm\" factory-name=\"KaroseriFactory\" model=\"mKaroseri\" model-id=\"KaroseriCode\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"karoseriForm\" form-title=\"Karoseri\" modal-title=\"Karoseri\" modal-size=\"small\" show-advsearch=\"on\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" grid-hide-action-column=\"true\" icon=\"fa fa-fw fa-child\" hide-new-button=\"true\"> <bsform-advsearch> <div class=\"row\" style=\"margin-bottom:40px\"> <div class=\"col-md-6\"> <bslabel>Model</bslabel> <bsselect style=\"margin-bottom: 10px\" ng-model=\"filter.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Model\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-6\"> <bslabel>Karoseri</bslabel> <input type=\"text\" class=\"form-control\" placeholder=\"Nama Karoseri\" ng-model=\"filter.KaroseriName\"> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"MasterKaroseriDetail\"> <div style=\"float:right;margin-right:10px\"> <button ng-click=\"KembaliKeKaroseriMasterMain()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> <button ng-hide=\"MasterKaroseriOperation=='Lihat'\" ng-disabled=\"mKaroseri.KaroseriName==null||mKaroseri.KaroseriName==''||mKaroseri.KaroseriName==undefined||mKaroseri.KaroseriCode==null||mKaroseri.KaroseriCode==''||mKaroseri.KaroseriCode==undefined||gridModelKaroseri.data==null||gridModelKaroseri.data.length<1||gridModelKaroseri.data==undefined\" ng-click=\"MasterKaroseriSimpan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span> <span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"Model\" ng-model=\"mKaroseri.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"SelectedModelMasterKaroseri(selected)\" placeholder=\"Pilih Model\" icon=\"fa fa-search\" ng-disabled=\"MasterKaroseriOperation=='Lihat'\"> </bsselect> <bsreqlabel>Kode Karoseri</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"KaroseriCode\" placeholder=\"karoseri\" ng-model=\"mKaroseri.KaroseriCode\" ng-disabled=\"MasterKaroseriOperation=='Lihat'\" maxlength=\"15\" required> </div> <bsreqlabel>Karoseri</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"KaroseriName\" placeholder=\"karoseri\" ng-model=\"mKaroseri.KaroseriName\" ng-disabled=\"MasterKaroseriOperation=='Lihat'\" maxlength=\"15\" alpha-numeric mask=\"Huruf\" required> </div> <div style=\"margin-top:20px; margin-bottom:65px\"> <button ng-disabled=\"MasterKaroseriOperation=='Lihat'\" ng-click=\"AddKaroseriVehicleTypeChild()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridModelKaroseri\" ui-grid-selection ui-grid-auto-resize></div> </div> <!-- <div class=\"modal-footer\">\r" +
    "\n" +
    "\t\t\t\t\t\t<button ng-click=\"SelectJasaVehicleTypeChild()\" \r" +
    "\n" +
    "\t\t\t\t\t\ttype=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tPilih\r" +
    "\n" +
    "\t\t\t\t\t\t\t</span>\r" +
    "\n" +
    "\t\t\t\t\t\t\t<span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t\t</div> --> </div> </div> </div> <div class=\"ui modal gridModalKaroseriVehicle\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"BatalLookupModalKaroseriVehicleTypeChild()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Pilih Aksesoris</h4> </div> <br> <br> <div class=\"modal-body\"> <div class=\"form-group\" style=\"padding-left:0 !important\"> <div class=\"form-group\"> </div> </div> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridModalKaroseri\" ui-grid-selection ui-grid-auto-resize></div> </div> <div class=\"modal-footer\"> <button ng-click=\"SelectKaroseriVehicleTypeChild()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\"> Pilih </span> <span class=\"ladda-spinner\"></span> </button> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/kategoriPelanggan/kategoriPelanggan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"KategoriPelangganForm\" factory-name=\"MasterKategoriPelangganFactory\" model=\"mKategoriPelanggan\" model-id=\"CustomerTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"KategoriPelangganForm\" form-title=\"Kategori Pelanggan\" modal-title=\"Kategori Pelanggan\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kategori Parent</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly value=\"Instansi\" type=\"text\" class=\"form-control\" name=\"ParentId\" placeholder=\"Fund Source Name\" maxlength=\"50\" required> </div> <bserrmsg field=\"ParentId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kategori Pelanggan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CustomerTypeDesc\" placeholder=\"Kategori Pelanggan\" ng-model=\"mKategoriPelanggan.CustomerTypeDesc\" maxlength=\"50\" required> </div> <bserrmsg field=\"CustomerTypeDesc\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/klasifikasiPembelian/klasifikasiPembelian.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"KlasifikasiPembelianForm\" factory-name=\"KlasifikasiPembelianFactory\" model=\"mKlasifikasiPembelian\" model-id=\"PurchasingClassificationId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"KlasifikasiPembelianForm\" form-title=\"Klasifikasi Pembelian\" modal-title=\"Master Klasifikasi Pembelian\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Klasifikasi Pembelian</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PurchasingClassificationName\" placeholder=\"Input Klasifikasi Pembelian\" ng-model=\"mKlasifikasiPembelian.PurchasingClassificationName\" maxlength=\"15\" alpha-numeric mask=\"Huruf\" required> </div> <bserrmsg field=\"PurchasingClassificationName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/konfigurasiRegistration/konfigurasiRegistration.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <style type=\"text/css\">/* The switch - the box around the slider */\r" +
    "\n" +
    ".switch {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  display: inline-block;\r" +
    "\n" +
    "  width: 60px;\r" +
    "\n" +
    "  height: 55px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* Hide default HTML checkbox */\r" +
    "\n" +
    ".switch input {\r" +
    "\n" +
    "  opacity: 0;\r" +
    "\n" +
    "  width: 0;\r" +
    "\n" +
    "  height: 0;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* The slider */\r" +
    "\n" +
    ".slider {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  cursor: pointer;\r" +
    "\n" +
    "  top: 0;\r" +
    "\n" +
    "  left: 0;\r" +
    "\n" +
    "  right: 0;\r" +
    "\n" +
    "  bottom: 0;\r" +
    "\n" +
    "  background-color: #ccc;\r" +
    "\n" +
    "  -webkit-transition: .4s;\r" +
    "\n" +
    "  transition: .4s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider:before {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  content: \"\";\r" +
    "\n" +
    "  height: 26px;\r" +
    "\n" +
    "  width: 26px;\r" +
    "\n" +
    "  left: 4px;\r" +
    "\n" +
    "  bottom: 0px;\r" +
    "\n" +
    "  background-color: white;\r" +
    "\n" +
    "  -webkit-transition: .4s;\r" +
    "\n" +
    "  transition: .4s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:checked + .slider {\r" +
    "\n" +
    "  background-color: #00FF00;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:focus + .slider {\r" +
    "\n" +
    "  box-shadow: 0 0 1px #00FF00;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:checked + .slider:before {\r" +
    "\n" +
    "  -webkit-transform: translateX(26px);\r" +
    "\n" +
    "  -ms-transform: translateX(26px);\r" +
    "\n" +
    "  transform: translateX(26px);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* Rounded sliders */\r" +
    "\n" +
    ".slider.round {\r" +
    "\n" +
    "  border-radius: 34px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider.round:before {\r" +
    "\n" +
    "  border-radius: 50%;\r" +
    "\n" +
    "}</style> <div style=\"margin-right:5px;margin-left:5px\" ng-show=\"ShowKonfigurasiRegistrationMain\" class=\"row\"> <div class=\"pull-right\" style=\"margin-bottom:5px;margin-right:10px\"> <button class=\"ubtn\" style=\"margin-bottom:-9px;width:40px\" ng-click=\"KonfigurasiRegistrationSearchToggle()\" onclick=\"this.blur()\" ng-class=\"{'adv-search-active':showAdvsearch}\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search Panel\" tabindex=\"0\"> <i class=\"fa fa-fw fa-search\" style=\"font-size:1.4em\"></i> </button> <button class=\"ubtn\" ng-click=\"KonfigurasiRegistrationSearchData()\" onclick=\"this.blur()\" style=\"padding-bottom:6px;width:40px\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" tabindex=\"0\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> </div> <div ng-show=\"KonfigurasiRegistrationShowAdvSearch\" class=\"col-md-12 advsearch\" style=\"padding:10px; margin-bottom:0px; border:1px solid #e2e2e2;background-color:rgba(213,51,55,0.02)\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Dealer</bsreqlabel> <bsselect ng-model=\"filter.GroupDealerId\" data=\"optionsTambahMasterCAO_Dealer\" item-text=\"name\" item-value=\"value\" ng-disabled=\"true\" placeholder=\"Group Dealer\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bslabel>Cabang</bslabel> <bsselect ng-model=\"filter.OutletId\" data=\"OptionsKonfigurasiRegistrationOutlet\" item-text=\"OutletName\" item-value=\"OutletId\" placeholder=\"Cabang\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"col-md-12\"> <br> <div class=\"row\" style=\"position:absolute;right:20px;margin-top:-10px\"> <div class=\"pull-right\"> <div class=\"btn-group\"> <button class=\"btn wbtn\" style=\"padding:4px\" ng-click=\"KonfigurasiRegistrationSearchData()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search\"> <i class=\"large icons\"> <i class=\"database icon\"></i> <i class=\"inverted corner filter icon\"></i> </i>Search</button> </div> </div> </div> </div> </div> <br> </div> <div style=\"margin-top:180px\"> <div class=\"grid\" ui-grid=\"gridMainKonfigurasiRegistration\" ui-grid-selection ui-grid-pinning ui-grid-pagination ui-grid-auto-resize></div> </div> </div> <div ng-show=\"ShowKonfigurasiRegistrationDetail\" class=\"row\"> <div> <button ng-click=\"KonfigurasiRegistrationSimpan()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> Simpan</button> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"KembaliKeKonfigurasiRegistrationMasterMain()\">Kembali</button> </div> <div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Dealer</bsreqlabel> <bsselect ng-model=\"mKonfigurasiRegistration.GroupDealerId\" data=\"optionsTambahMasterCAO_Dealer\" item-text=\"name\" item-value=\"value\" ng-disabled=\"KonfigurasiRegistrationOperation=='Lihat'\" on-select=\"ulala(selected)\" placeholder=\"Group Dealer\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Cabang</bsreqlabel> <bsselect ng-model=\"mKonfigurasiRegistration.OutletId\" data=\"OptionsKonfigurasiRegistrationOutlet\" item-text=\"OutletName\" ng-disabled=\"KonfigurasiRegistrationOperation=='Lihat'\" item-value=\"OutletId\" placeholder=\"Cabang\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Pembaruan Terakhir</bsreqlabel> <input type=\"text\" class=\"form-control\" placeholder=\"Pembaruan Terakhir\" ng-disabled=\"true\" ng-model=\"mKonfigurasiRegistration.LastModifiedDate\"> </div> </div> </div> </div> <div style=\"margin-top:180px\"> <div class=\"grid\" ui-grid=\"gridMainKonfigurasiRegistrationChild\" ui-grid-selection ui-grid-pinning ui-grid-pagination ui-grid-auto-resize></div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/konfigurasiStokBasket/konfigurasiStokBasket.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <div class=\"no-add\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"RightTopBox\" style=\"float: right\"> <button ng-show=\"!MasterStokBasket_Edit_Show\" id=\"MasterStokBasket_Batal_Button\" ng-click=\"MasterStokBasket_Batal_Clicked()\" type=\"button\" class=\"wbtn btn\" style=\"margin:-30px 0 0 0\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"!MasterStokBasket_Edit_Show\" id=\"MasterStokBasket_Simpan_Button\" ng-click=\"MasterStokBasket_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn\" style=\"margin:-30px 0 0 0\"> <span class=\"ladda-label\"> <i class=\"fa fa-floppy\"></i> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MasterStokBasket_Edit_Button\" ng-click=\"MasterStokBasket_Edit_Clicked()\" ng-show=\"MasterStokBasket_Edit_Show\" type=\"button\" class=\"rbtn btn\" style=\"margin:-30px 0 0 0\"> <span class=\"ladda-label\"><i class=\"fa fa-pencil\"></i> Edit </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"row\" style=\"margin: 30px 0px 0px\"> <div class=\"col-md-4\" style=\"text-align: left; float:right\"> <div id=\"toogleItem\" class=\"pull-right\"> <div class=\"input-icon-right\"> <div class=\"switch\"> <input ng-change=\"StockBasketValue()\" ng-disabled=\"MasterStokBasket_Edit_Show\" ng-model=\"StockBasket.StockBasketBit\" id=\"cmn-toggle-5\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"cmn-toggle-5\"></label> </div> </div> </div> <label class=\"pull-right\" style=\"margin: 5px 10px 0 0\"><b>Stock Basket : </b></label> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <div ng-if=\"!MasterStokBasket_Edit_Show\" style=\"font-size:10px;margin-top:10px;margin-bottom:-5px;color: red\"> <p align=\"right\"> * Klik dua kali pada field Lead Time untuk merubah. </p> </div> <div ng-if=\"MasterStokBasket_Edit_Show\" style=\"height: 25px;font-size:10px;margin-top:10px;margin-bottom:-5px;color: red\"> <p align=\"right\"> </p> </div> <div id=\"myGrid\" ui-grid=\"StockBasket_UIGrid\" ui-grid-edit ui-grid-auto-resize ui-grid-pagination class=\"Mygrid\" style=\"height: 370px\"></div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/leadTimeBookingFeeDanPenyerahanBookingFeeKeKasir/leadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.html',
    "<script type=\"text/javascript\">//for (i = 0; i < 2; i++) \r" +
    "\n" +
    "//{\r" +
    "\n" +
    "    //var number = document.getElementById('LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir_PreventNegative_'+i);\r" +
    "\n" +
    "\t// Listen for input event on numInput.\r" +
    "\n" +
    "\t//number.onkeydown = function(e) {\r" +
    "\n" +
    "\t//\tif(!((e.keyCode >= 48 && e.keyCode <= 57)\r" +
    "\n" +
    "\t//\t  || e.keyCode == 190|| e.keyCode == 8|| e.keyCode == 46)) {\r" +
    "\n" +
    "\t//\t\t//titik tu 190 buat decimal\r" +
    "\n" +
    "\t//\t\treturn false;\r" +
    "\n" +
    "\t//\t}\r" +
    "\n" +
    "\t//}\r" +
    "\n" +
    "//}</script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirForm\" factory-name=\"LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirFactory\" model=\"mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir\" model-id=\"LeadtimeBookingFeeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"LeadTimeBookingUnitForm\" form-title=\"Lead Time\" modal-title=\"Master Lead Time Booking Unit\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\" show-advsearch=\"on\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <bslabel>Model</bslabel> <bsselect style=\"margin-bottom: 10px\" ng-model=\"filter.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Select Model\" icon=\"fa fa-search\"> </bsselect> </div> </div> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-top:10px\">Model</bsreqlabel> <bsselect required data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.VehicleModelId\" placeholder=\"Model\" style=\"min-width: 150px\"> </bsselect> <div class=\"form-group\" show-errors> <bsreqlabel style=\"margin-top:10px\">Batas Waktu Booking Unit (jam)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-change=\"noFirstZero1(mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBooking)\" id=\"LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir_PreventNegative_0\" type=\"text\" class=\"form-control\" name=\"LeadTimeBooking\" placeholder=\"Batas Waktu Booking Unit (jam)\" ng-model=\"mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBooking\" maxlength=\"3\" required> </div> <bserrmsg field=\"LeadTimeBooking\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Batas Waktu Penerimaan Booking Fee (jam)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-change=\"noFirstZero2(mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBookingReceive)\" id=\"LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir_PreventNegative_1\" type=\"text\" class=\"form-control\" name=\"LeadTimeBookingReceive\" placeholder=\"Batas Waktu Penerimaan Booking Fee (jam)\" ng-model=\"mLeadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.LeadTimeBookingReceive\" maxlength=\"3\" required> </div> <bserrmsg field=\"LeadTimeBookingReceive\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/leadtimeAdministrasiPdd/leadtimeAdministrasiPdd.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div style=\"margin-bottom:20px\"> <button ng-show=\"LeadtimeAdministrasiPdd_DisableAtas\" style=\"float: right;margin-bottom:10px\" ng-click=\"LeadtimeAdministrasiPdd_Ubah()\" type=\"button\" class=\"rbtn btn\"> <span class=\"ladda-label\"> Ubah </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"!LeadtimeAdministrasiPdd_DisableAtas\" style=\"float: right;margin-bottom:10px\" ng-click=\"LeadtimeAdministrasiPdd_Simpan()\" type=\"button\" class=\"rbtn btn\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"!LeadtimeAdministrasiPdd_DisableAtas\" style=\"float: right;margin-bottom:10px\" ng-click=\"LeadtimeAdministrasiPdd_BatalUbah()\" type=\"button\" class=\"wbtn btn\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> <div ng-if=\"user.RoleName == 'ADH'\"> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Parameter</th> <th>Lead Time (Hari)</th> </tr> <tr> <td style=\"width: 50%\">Lead Time Approval Leasing</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableAtas\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.ApprovalLeasingLeadTimeYes\"></td> </tr> <tr> <td style=\"width: 50%\">STNK Jadi</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableAtas\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.STNKDoneLeadTimeYes\"></td> </tr> <tr> <td style=\"width: 50%\">Pemilihan Plat Nomor</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableAtas\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.ChoosePoliceNoLeadTimeYes\"></td> </tr> <tr> <td style=\"width: 50%\"> Pemasangan Aksesoris</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableAtas\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.AccessoriesInstallationLeadTimeYes\"></td> </tr> <tr> <td style=\"width: 50%\">Lead Time Karoseri</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableAtas\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.KaroseriLeadTimeYes\"></td> </tr> <tr> <td style=\"width: 50%\">Persiapan DEC</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableAtas\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.DECPreparationLeadTimeYes\"></td> </tr> <tr> <td style=\"width: 50%\">Direct Delivery</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableAtas\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.DirectDeliveryToCustomerLeadTimeYes\"></td> </tr> </tbody> </table> </div> <div ng-if=\"user.RoleName == 'Admin TAM'\"> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Parameter</th> <th>Lead Time (Hari)</th> </tr> <tr> <td style=\"width: 50%\">Form A</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableBawah || user.RoleName!='Admin TAM'\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.FormALeadTimeYes\"></td> </tr> <tr> <td style=\"width: 50%\">Faktur Jadi</td> <td><input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableBawah || user.RoleName!='Admin TAM'\" type=\"number\" ng-model=\"mLeadtimeAdministrasiPdd.InvoiceDoneLeadTimeYes\"></td> </tr> </tbody> </table> <table style=\"margin-top:20px\" class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Model</th> <th>Lead Time (Hari)</th> </tr> <tr ng-repeat=\"LeadtimeAdministrasiPddBawahModel in mLeadtimeAdministrasiPdd.LeadTimes track by $index\"> <td style=\"width: 50%\">{{LeadtimeAdministrasiPddBawahModel.VehicleModelName}}</td> <td> <input min=\"0\" ng-disabled=\"LeadtimeAdministrasiPdd_DisableBawah\" type=\"number\" ng-model=\"LeadtimeAdministrasiPddBawahModel.LeadTime\"> </td> </tr> </tbody> </table> </div>"
  );


  $templateCache.put('app/sales/sales9/master/masterAreaDealer/masterAreaDealer.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"OutletForm\" factory-name=\"MasterAreaDealerFactoryMaster\" model=\"mMasterAreaDealer\" model-id=\"CustomerReligionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"OutletForm\" form-title=\"Outlet\" modal-title=\"Master Outlet\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" show-advsearch=\"on\"> <bsform-advsearch> <div class=\"row\" style=\"margin-bottom:40px\"> <div class=\"col-md-6\"> <bslabel>Dealer</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"GroupDealerName\" placeholder=\"Dealer\" ng-model=\"DaMasterAreaDealerDealerName\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-md-6\"> <bslabel>Outlet</bslabel> <select style=\"margin-top:5px\" ng-model=\"filter.OutletId\" class=\"form-control\" ng-options=\"item.OutletId as item.OutletName for item in optionsOutlet_Outlet\" placeholder=\"Outlet\"> <option label=\"-- Pilih --\" value=\"\" disabled></option> </select> </div> </div> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Dealer</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"GroupDealerName\" placeholder=\"Dealer\" ng-model=\"DaMasterAreaDealerDealerName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GroupDealerName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" style=\"float:left;width:100%\" show-errors> <div style=\"float:left;width:85%\"> <bsreqlabel>Nama Cabang</bsreqlabel> <div class=\"input-icon-right\"> <bsselect ng-model=\"mMasterAreaDealer_OutletId\" data=\"optionsMasterAreaDealerCAbang\" item-text=\"OutletName\" item-value=\"OutletId\" on-select=\"MasterAreaDealerAddDaCabang(selected)\" placeholder=\"Select Model\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"ProvinceName\"></bserrmsg> </div> <div style=\"float:left;width:15%\"> <button style=\"margin-top:23px\" class=\"rbtn btn\" ng-click=\"MasterAreaDealerAddNamaCabang()\"> <i class=\"fa fa-plus\"></i> </button> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Area Dealer</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AreaDealerName\" placeholder=\"Area Dealer\" ng-model=\"mMasterAreaDealer.AreaDealerName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AreaDealerName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Nama Cabang</th> <th>Action</th> </tr> <tr ng-repeat=\"DaListDetailOutlet in mMasterAreaDealer.ListDetailOutlet track by $index\"> <td>{{DaListDetailOutlet.OutletName}}</td> <td><a ng-click=\"RemoveListDetailOutlet($index)\">Remove</a></td> </tr> </tbody> </table> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/masterCAO/masterCAO.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\" style=\"min-height: 100vh\"> <div ng-show=\"MainMasterCAO_Show\"> <div class=\"pull-right ng-scope\"> <div class=\"btn-group\" style=\"margin-top:-55px;margin-right:-5px\"> <!-- ngIf: showAdvsearchButton --> <button class=\"ubtn ng-scope adv-search-active\" style=\"padding-right:0px\" ng-click=\"showAdvsearch = !showAdvsearch\" onclick=\"this.blur()\" ng-class=\"{'adv-search-active':showAdvsearch}\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search Panel\" tabindex=\"0\"> <i class=\"fa fa-fw fa-search\" style=\"font-size:1.4em\"></i> </button> <!-- end ngIf: showAdvsearchButton --> <button class=\"ubtn\" ng-click=\"MainMasterCAO_Refresh_Clicked()\" onclick=\"this.blur()\" style=\"padding-bottom:6px\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" tabindex=\"0\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> <!-- ngRepeat: customAct in customActFunc | orderBy:'-' --> <!-- ngIf: !hideNewButton && allowNew --> <button class=\"rbtn btn ng-binding ng-scope\" ng-if=\"user.RoleName =='Admin TAM'\" style=\"margin-right:5px!important\" ng-click=\"MainMasterCAO_Tambah_Clicked()\" tabindex=\"0\"> <i class=\"fa fa-fw fa-plus\"> </i>&nbsp;Tambah </button> <!-- end ngIf: !hideNewButton && allowNew --> </div> </div> <div class=\"row\" ng-if=\"showAdvsearch\" style=\"margin: 10px 0 0 0; padding: 15px 15px 0px 15px; border:1px solid #e2e2e2; background-color:rgba(213,51,55,0.02)\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Dealer</label> <bsselect ng-model=\"mMaintainCAOfilter.GroupDealerId\" data=\"optionsTambahMasterCAO_Dealer\" ng-change=\"TambahMasterCAO_Dealer_ChangedAdv(mMaintainCAOfilter.GroupDealerId)\" item-text=\"name\" item-value=\"value\" placeholder=\"Grop Dealer\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>CAO/HO</label> <bsselect ng-model=\"mMaintainCAOfilter.OutletId\" data=\"listCabangadv\" ng-disabled=\"mMaintainCAOfilter.GroupDealerId == null\" item-text=\"OutletName\" item-value=\"OutletId\" placeholder=\"CAO/HO\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 5px 0\"> <div class=\"pull-right\"> <div class=\"btn-group\" style=\"\"> <button class=\"btn wbtn\" style=\"padding:4px\" ng-click=\"MainMasterCAO_ADV_Clicked()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search\" tabindex=\"0\"> <i class=\"large icons\"> <i class=\"database icon\"></i> <i class=\"inverted corner filter icon\"></i> </i>Search</button> </div> </div> </div> </div> <div id=\"filterData\" class=\"row\" style=\"margin:0; margin-top:10px\"> <div id=\"inputFilter\" style=\"float:left; width:20%\"> <div class=\"form-group filterMarg\"> <input type=\"text\" ng-change=\"filterSearch()\" required ng-model=\"textFilter\" class=\"form-control\" name=\"ModelKendaraan\" placeholder=\"Search\" maxlength=\"100\"> </div> </div> <div id=\"comboBoxFilter\" style=\"float:left; width:15%; font-size:12px\"> <div class=\"form-group filterMarg\"> <select required ng-model=\"selectedFilter\" ng-change=\"filterSearch()\" class=\"form-control\" ng-options=\"item.value as item.name for item in filterData.defaultFilter\" placeholder=\"Filter\"> <option label=\"Filter\" value=\"\" disabled>Filter</option> </select> </div> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div id=\"myGrid\" ui-grid=\"CAO_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:100%;min-height: 300px\"></div> </div> </div> <div ng-show=\"TambahMasterCAO_Show\"> <div class=\"row\" style=\"margin : -30px 0 0 0\"> <button ng-disabled=\"CAOOutletList_UIGrid.data.length==0||mMaintainCAO.GroupDealerId==undefined || mMaintainCAO.GroupDealerId==null || mMaintainCAO.OutletId==null ||mMaintainCAO.OutletId==undefined\" id=\"TambahMasterCAO_Simpan_Button\" ng-click=\"TambahMasterCAO_Simpan_Clicked()\" ng-show=\"TambahmasterCAO_Simpan_Show\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Simpan </span> <span class=\"ladda-spinner\"></span> </button> <button id=\"TambahMasterCAO_Kembali_Button\" ng-if=\"status != 'lihat'\" ng-click=\"TambahMasterCAO_Kembali_Clicked()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> <button id=\"TambahMasterCAO_Kembali_Button\" ng-if=\"status == 'lihat'\" ng-click=\"TambahMasterCAO_Kembali_Clicked_lihat()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-4\"> <bsreqlabel>Group Dealer</bsreqlabel> <bsselect ng-model=\"mMaintainCAO.GroupDealerId\" ng-disabled=\"status == 'lihat' || status == 'Update'\" data=\"optionsTambahMasterCAO_Dealer\" ng-change=\"TambahMasterCAO_Dealer_Changed(mMaintainCAO.GroupDealerId)\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Group Dealer\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"row\" style=\"margin-top:10px;margin-left:0px\"> <div ng-if=\"status == 'Insert'\" class=\"col-md-4\"> <bsreqlabel>CAO / HO</bsreqlabel> <bsselect ng-model=\"mMaintainCAO.OutletId\" data=\"listCabang\" ng-disabled=\"status == 'lihat'||status == 'Update'\" item-text=\"OutletName\" ng-change=\"pilihSebagaiCAO(mMaintainCAO.OutletIdCAO)\" item-value=\"OutletId\" placeholder=\"CAO/HO\" icon=\"fa fa-search\"> </bsselect> </div> <div ng-if=\"status != 'Insert'\" class=\"col-md-4\"> <bsreqlabel>CAO / HO</bsreqlabel> <input class=\"form-control\" ng-model=\"mMaintainCAO.OutletNameCAO\" data=\"listCabang\" ng-disabled=\"status == 'lihat'||status == 'Update'\" placeholder=\"CAO/HO\"> </input> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <button ng-hide=\"status == 'lihat'\" ng-disabled=\"mMaintainCAO.OutletId==null||mMaintainCAO.OutletId==undefined\" ng-click=\"tambahOutlet()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\" style=\"margin-top:15px;margin-left:13px;margin-right:13px\"> <div id=\"myGrid\" ui-grid=\"CAOOutletList_UIGrid\" ui-grid-edit ui-grid-auto-resize ui-grid-pagination class=\"Mygrid\" style=\"width:100%;height: 350px\"></div> </div> </div> <div class=\"ui modal OutletCabang\" style=\"background-color: transparent ;box-shadow :none !important\"> <div style=\"margin: auto; width: 600px\"> <div class=\"row\" style=\"margin: 0 0 0 0; background-color: white; border-radius:3px; box-shadow :1px 3px 3px 0 rgba(0,0,0,.2), 1px 3px 15px 2px rgba(0,0,0,.2) !important\"> <button class=\"btn btn-default\" type=\"button\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; ; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Daftar Outlet </button> <div class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom: 10px !important; overflow-y:auto; height:400px\"> <div class=\"list-group-item\" ng-repeat=\"item in outletCabang\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleChecked(item);\"> <div class=\"row\"> <div class=\"col-md-12\"> <h7 style=\"float: left\">{{item.OutletCode}}</h7> <span style=\"float:right\"> <i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.checked == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i> </span> </div> <div class=\"col-md-12\" style=\"font-size:10px\">{{item.OutletName}}</div> </div> </div> </div> <div class=\"listData\" id=\"addMode\" style=\"float:right; margin-bottom: 0px !important;padding-top: 0px!important;padding-bottom: 8px!important;padding-right: 8px!important\"> <button id=\"addBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"PilihKategori()\">Pilih </button> <button id=\"addBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"BatalKategori()\">Batal </button> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/masterFollowUpDec/masterFollowUpDec.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"MasterFollowUpDecForm\" factory-name=\"MasterFollowUpDecFactory\" model=\"mMasterFollowUpDec\" model-id=\"CustomerReligionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"MasterFollowUpDecForm\" form-title=\"Follow Up Dec\" modal-title=\"Follow Up Dec\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Follow Up DEC</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CustomerReligionName\" placeholder=\"Follow Up DEC\" ng-model=\"mMasterFollowUpDec.CustomerReligionName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"CustomerReligionName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/masterTaskList/masterTaskList.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"MasterTaskListForm\" factory-name=\"MasterTaskListFactory\" model=\"mMasterTaskList\" model-id=\"TaskId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"MasterTaskListForm\" form-title=\"Task List\" modal-title=\"Task List\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Task Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Code\" placeholder=\"Task Code\" ng-model=\"mMasterTaskList.TaskCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Task Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Name\" placeholder=\"Task Name\" ng-model=\"mMasterTaskList.TaskName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Flat Rate</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"flatRAte\" placeholder=\"Flat Rate\" ng-model=\"mMasterTaskList.FR\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"flatRate\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/masterTop/masterTop.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"MasterTopForm\" factory-name=\"MasterTopFactoryMaster\" model=\"mMasterTop\" model-id=\"TopId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"MasterTopForm\" form-title=\"MasterTop\" modal-title=\"Master Top\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kategori Pelanggan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <bsselect ng-disabled=\"true\" name=\"CustomerTypeId\" ng-model=\"mMasterTop.CustomerTypeId\" data=\"MasterTopGetCustomerCategory\" item-text=\"CustomerTypeDesc\" item-value=\"CustomerTypeId\" on-select=\"CustomerTypeId\" placeholder=\"\" icon=\"fa fa-search\" required> </bsselect> </div> <bserrmsg field=\"CustomerTypeId\"></bserrmsg> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>TOP CKD Cash (Hari)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TopDayCKD\" placeholder=\"Input MasterTop\" ng-model=\"mMasterTop.TopDayCKD\" maxlength=\"15\" alpha-numeric required> </div> <bserrmsg field=\"TopDayCKD\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>TOP CBU Cash (Hari)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TopDayCBU\" placeholder=\"Input MasterTop\" ng-model=\"mMasterTop.TopDayCBU\" maxlength=\"15\" alpha-numeric required> </div> <bserrmsg field=\"TopDayCBU\"></bserrmsg> </div> </div> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/metodePembayaran/metodePembayaran.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"MetodePembayaranForm\" factory-name=\"MasterMetodePembayaranFactory\" model=\"mMetodePembayaran\" model-id=\"PaymentTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"MetodePembayaranForm\" form-title=\"Sumber Dana\" modal-title=\"Sumber Dana\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Sumber Dana</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PaymentTypeName\" placeholder=\"Metode Pembayaran\" ng-model=\"mMetodePembayaran.PaymentTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PaymentTypeName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/outlet/outlet.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"OutletForm\" factory-name=\"OutletFactoryMaster\" model=\"mOutlet\" model-id=\"CustomerReligionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"OutletForm\" form-title=\"Outlet\" modal-title=\"Master Outlet\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" show-advsearch=\"on\"> <bsform-advsearch> <div class=\"row\" style=\"margin-bottom:40px\"> <div class=\"col-md-6\"> <bslabel>Dealer</bslabel> <bsselect placeholder=\"Pilih Dealer\" style=\"margin-top:5px\" ng-disabled=\"MasterOutletDisableFilterDealer\" ng-model=\"filter.GroupDealerId\" data=\"optionsOutlet_Dealer\" item-text=\"GroupDealerName\" item-value=\"GroupDealerId\" ng-change=\"MasterOutletSearchDealerBrubah()\"> </bsselect> </div> <div class=\"col-md-6\"> <bslabel>Cabang</bslabel> <bsselect placeholder=\"Pilih Cabang\" style=\"margin-top:5px\" ng-disabled=\"MasterOutletDisableFilterOutlet\" ng-model=\"filter.OutletId\" data=\"optionsOutlet_Outlet\" item-text=\"OutletName\" item-value=\"OutletId\"> </bsselect> </div> </div> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Dealer</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"DealerName\" placeholder=\"Dealer\" ng-model=\"mOutlet.DealerName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"DealerName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Provinsi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ProvinceName\" placeholder=\"Provinsi\" ng-model=\"mOutlet.ProvinceName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ProvinceName\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Perusahaan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CompanyName\" placeholder=\"Perusahaan\" ng-model=\"mOutlet.CompanyName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"CompanyName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Kabupaten/Kota</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CityRegencyName\" placeholder=\"Kabupaten/Kota\" ng-model=\"mOutlet.CityRegencyName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"CityRegencyName\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Kode Outlet</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" placeholder=\"Kode Outlet\" ng-model=\"mOutlet.OutletCode\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Kecamatan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"DistrictName\" placeholder=\"Kecamatan\" ng-model=\"mOutlet.DistrictName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"DistrictName\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Cabang</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"OutletName\" placeholder=\"Pilih Cabang\" ng-model=\"mOutlet.OutletName\" required> </div> <bserrmsg field=\"OutletName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Kelurahan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"VillageName\" placeholder=\"Kelurahan\" ng-model=\"mOutlet.VillageName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"VillageName\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Nomor Telepon</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"Phone\" ng-model=\"mOutlet.PhoneNumber\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Phone\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Kode Pos</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PostalCode\" placeholder=\"Kode Pos\" ng-model=\"mOutlet.PostalCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PostalCode\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Nomor Fax</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Fax\" placeholder=\"Fax\" ng-model=\"mOutlet.FaxNumber\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Fax\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Alamat</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"textarea\" class=\"form-control\" name=\"Address\" placeholder=\"Alamat\" ng-model=\"mOutlet.OutletAddress\"> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Fungsi Cabang</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"OutletFunctionName\" placeholder=\"Input Fungsi Cabang\" ng-model=\"mOutlet.OutletFunctionName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"OutletFunctionName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Area TAM</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AreaTAMName\" placeholder=\"Area TAM\" ng-model=\"mOutlet.AreaTAMName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AreaTAMName\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Area Dealer</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AreaDealerName\" placeholder=\"Area Dealer\" ng-model=\"mOutlet.AreaDealerName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AreaDealerName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/pelanggaranWilayah/pelanggaranWilayah.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div class=\"row\"> <!--<button ng-click=\"PelanggaranWilayahSimpan()\" id=\"addBtn\" class=\"rbtn btn\" style=\"float: right;\" > \r" +
    "\n" +
    "        Simpan\r" +
    "\n" +
    "    </button>--> <div style=\"margin-top:20px\" class=\"col-md-12\"> <div class=\"col-md-6\"> <div style=\"font-size:20px\"> <b>Area</b> </div> <button id=\"addBtn\" class=\"rbtn btn\" style=\"float: right\" ng-click=\"PelanggaranWilayah_WilayahModal()\"> <i class=\"fa fa-plus\"></i> <span class=\"ladda-label\"> Tambah </span> </button> <br> <br> <div style=\"display: block;height:410px\" class=\"grid\" ui-grid=\"GridPelanggaranWilayah_Wilayah\" ui-grid-auto-resize ui-grid-pagination></div> </div> <div class=\"col-md-6\"> <div style=\"font-size:20px\"> <b>Provinsi</b> </div> <button ng-click=\"PelanggaranWilayah_ProvinsiModal()\" id=\"addBtn\" class=\"rbtn btn\" style=\"float: right\"> <i class=\"fa fa-plus\"></i> <span class=\"ladda-label\"> Tambah </span> </button> <br> <br> <div style=\"display: block;height:410px\" class=\"grid\" ui-grid=\"GridPelanggaranWilayah_Provinsi\" ui-grid-auto-resize ui-grid-pagination></div> </div> </div> <div style=\"margin-top:20px\" class=\"col-md-12\"> <div class=\"col-md-6\"> <div style=\"font-size:20px\"> <b>Kota</b> </div> <button ng-click=\"PelanggaranWilayah_KotaModal()\" id=\"addBtn\" class=\"rbtn btn\" style=\"float: right\"> <i class=\"fa fa-plus\"></i> <span class=\"ladda-label\"> Tambah </span> </button> <br> <br> <div style=\"display: block;height:410px\" class=\"grid\" ui-grid=\"GridPelanggaranWilayah_Kota\" ui-grid-auto-resize ui-grid-pagination></div> </div> <div class=\"col-md-6\"> <div style=\"font-size:20px\"> <b>Kecamatan</b> </div> <button ng-click=\"PelanggaranWilayah_KecamatanModal()\" id=\"addBtn\" class=\"rbtn btn\" style=\"float: right\"> <i class=\"fa fa-plus\"></i> <span class=\"ladda-label\"> Tambah </span> </button> <br> <br> <div style=\"display: block;height:410px\" class=\"grid\" ui-grid=\"GridPelanggaranWilayah_Kecamatan\" ui-grid-auto-resize ui-grid-pagination></div> </div> </div> </div> <div class=\"ui modal Modal_PelanggaranWilayah_Wilayah\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"Modal_PelanggaranWilayah_Wilayah_Batal()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Area</h4> </div> <div class=\"modal-body\"> <div class=\"row\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"Modal_PelanggaranWilayah_Wilayah_Grid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection></div> </div> </div> <div class=\"modal-footer\"> <button ng-click=\"Modal_PelanggaranWilayah_Wilayah_Simpan()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-10px 0 0 10px; width:11%\">Simpan</button> </div> </div> <div class=\"ui modal Modal_PelanggaranWilayah_Provinsi\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"Modal_PelanggaranWilayah_Provinsi_Batal()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Provinsi</h4> </div> <div class=\"modal-body\"> <div class=\"row\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"Modal_PelanggaranWilayah_Provinsi_Grid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection></div> </div> </div> <div class=\"modal-footer\"> <button ng-click=\"Modal_PelanggaranWilayah_Provinsi_Simpan()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-10px 0 0 10px; width:11%\">Simpan</button> </div> </div> <div class=\"ui modal Modal_PelanggaranWilayah_Kota\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"Modal_PelanggaranWilayah_Kota_Batal()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Kota</h4> </div> <div class=\"modal-body\"> <div id=\"filterData\" class=\"row\" style=\"margin-bottom:20px\"> <div id=\"comboBoxFilter\" style=\"float:left; width:45%\"> <div> Provinsi </div> <div class=\"form-group filterMarg\"> <select class=\"form-control\" style=\"width:100%\" ng-model=\"Modal_PelanggaranWilayah_Kota_SelectProvinsi\" ng-options=\"item.ProvinceId as item.ProvinceName for item in Modal_PelanggaranWilayah_Kota_OptionSelectProvinsi\"> </select> </div> </div> <div id=\"btnFilter\" style=\"float:left; width:10%;margin-top:13px\"> <button style=\"margin-right: 0px!important\" class=\"rbtn btn mobileFilterBtn\" ng-click=\"Modal_PelanggaranWilayah_Kota_SelectProvinsiClicked()\"> <i class=\"fa fa-filter iconFilter\"></i> </button> </div> </div> <div class=\"row\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"Modal_PelanggaranWilayah_Kota_Grid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection></div> </div> </div> <div class=\"modal-footer\"> <button ng-click=\"Modal_PelanggaranWilayah_Kota_Simpan()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-10px 0 0 10px; width:11%\">Simpan</button> </div> </div> <div class=\"ui modal Modal_PelanggaranWilayah_Kecamatan\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"Modal_PelanggaranWilayah_Kecamatan_Batal()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Kecamatan</h4> </div> <div class=\"modal-body\"> <div id=\"filterData\" class=\"row\" style=\"margin-bottom:20px\"> <div id=\"comboBoxFilter\" style=\"float:left; width:40%\"> <div> Provinsi </div> <div class=\"form-group filterMarg\"> <select ng-change=\"Modal_PelanggaranWilayah_Kecamatan_SelectProvinsiChanged()\" class=\"form-control\" style=\"width:100%\" ng-model=\"Modal_PelanggaranWilayah_Kecamatan_SelectProvinsi\" ng-options=\"item.ProvinceId as item.ProvinceName for item in Modal_PelanggaranWilayah_Kecamatan_OptionSelectProvinsi\"> </select> </div> </div> <div id=\"comboBoxFilter\" style=\"float:left; width:40%\"> <div> Kota </div> <div class=\"form-group filterMarg\"> <select class=\"form-control\" style=\"width:100%\" ng-model=\"Modal_PelanggaranWilayah_Kecamatan_SelectKota\" ng-options=\"item.CityRegencyId as item.CityRegencyName for item in Modal_PelanggaranWilayah_Kecamatan_OptionSelectKota\"> </select> </div> </div> <div id=\"btnFilter\" style=\"float:left; width:10%\"> <button style=\"margin-top:13px; margin-right: 0px!important\" class=\"rbtn btn mobileFilterBtn\" ng-click=\"Modal_PelanggaranWilayah_Kecamatan_SelectKotaClicked()\"> <i class=\"fa fa-filter iconFilter\"></i> </button> </div> </div> <div class=\"row\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"Modal_PelanggaranWilayah_Kecamatan_Grid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection></div> </div> </div> <div class=\"modal-footer\"> <button ng-click=\"Modal_PelanggaranWilayah_Kecamatan_Simpan()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-10px 0 0 10px; width:11%\">Simpan</button> </div> </div> <div class=\"ui modal Modal_ApusPelanggaranWilayah_Wilayah\" role=\"dialog\"> <ul class=\"swal2-progresssteps\" style=\"display: none\"></ul> <div class=\"swal2-icon swal2-error\" style=\"display: none\"> <span class=\"x-mark\"> <span class=\"line left\"> </span> <span class=\"line right\"> </span> </span> </div> <div class=\"swal2-icon swal2-question\" style=\"display: none\">?</div> <div class=\"swal2-icon swal2-warning\" style=\"display: block\">!</div> <div class=\"swal2-icon swal2-info\" style=\"display: none\">i</div> <div class=\"swal2-icon swal2-success\" style=\"display: none\"> <span class=\"line tip\"> </span> <span class=\"line long\"> </span> <div class=\"placeholder\"> </div> <div class=\"fix\"> </div> </div> <img class=\"swal2-image\" style=\"display: none\"> <h2 class=\"swal2-title\" id=\"swal2-title\"> <p align=\"center\" style=\"font-size:20px\">Delete <span style=\"color:red\">1 item(s) </span>from <span style=\"color:blue\">Data Area ini.</span></p> <p align=\"center\">Anda Yakin?</p> </h2> <div id=\"swal2-content\" class=\"swal2-content\" style=\"display: block\" align=\"center\">Tidak dapat kembali ke kondisi sebelumnya!</div> <div align=\"center\" class=\"swal2-buttonswrapper\" style=\"display: block;margin-top:10px;margin-bottom:10px\"> <button ng-click=\"Modal_ApusPelanggaranWilayah_Wilayah_Simpan()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-confirm swal2-styled\" style=\"background-color: rgb(140, 212, 245); border-left-color: rgb(140, 212, 245); border-right-color: rgb(140, 212, 245);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">OK</button> <button ng-click=\"Modal_ApusPelanggaranWilayah_Wilayah_Batal()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-cancel swal2-styled\" style=\"display: inline-block; background-color: rgb(170, 170, 170);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">Cancel</button></div> </div> <div class=\"ui modal Modal_ApusPelanggaranWilayah_Provinsi\" role=\"dialog\"> <ul class=\"swal2-progresssteps\" style=\"display: none\"></ul> <div class=\"swal2-icon swal2-error\" style=\"display: none\"> <span class=\"x-mark\"> <span class=\"line left\"> </span> <span class=\"line right\"> </span> </span> </div> <div class=\"swal2-icon swal2-question\" style=\"display: none\">?</div> <div class=\"swal2-icon swal2-warning\" style=\"display: block\">!</div> <div class=\"swal2-icon swal2-info\" style=\"display: none\">i</div> <div class=\"swal2-icon swal2-success\" style=\"display: none\"> <span class=\"line tip\"> </span> <span class=\"line long\"> </span> <div class=\"placeholder\"> </div> <div class=\"fix\"> </div> </div> <img class=\"swal2-image\" style=\"display: none\"> <h2 class=\"swal2-title\" id=\"swal2-title\"> <p align=\"center\" style=\"font-size:20px\">Delete <span style=\"color:red\">1 item(s) </span>from <span style=\"color:blue\">Data Provinsi ini.</span></p> <p align=\"center\">Anda Yakin?</p> </h2> <div id=\"swal2-content\" class=\"swal2-content\" style=\"display: block\" align=\"center\">Tidak dapat kembali ke kondisi sebelumnya!</div> <div align=\"center\" class=\"swal2-buttonswrapper\" style=\"display: block;margin-top:10px;margin-bottom:10px\"> <button ng-click=\"Modal_ApusPelanggaranWilayah_Provinsi_Simpan()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-confirm swal2-styled\" style=\"background-color: rgb(140, 212, 245); border-left-color: rgb(140, 212, 245); border-right-color: rgb(140, 212, 245);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">OK</button> <button ng-click=\"Modal_ApusPelanggaranWilayah_Provinsi_Batal()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-cancel swal2-styled\" style=\"display: inline-block; background-color: rgb(170, 170, 170);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">Cancel</button></div> </div> <div class=\"ui modal Modal_ApusPelanggaranWilayah_Kota\" role=\"dialog\"> <ul class=\"swal2-progresssteps\" style=\"display: none\"></ul> <div class=\"swal2-icon swal2-error\" style=\"display: none\"> <span class=\"x-mark\"> <span class=\"line left\"> </span> <span class=\"line right\"> </span> </span> </div> <div class=\"swal2-icon swal2-question\" style=\"display: none\">?</div> <div class=\"swal2-icon swal2-warning\" style=\"display: block\">!</div> <div class=\"swal2-icon swal2-info\" style=\"display: none\">i</div> <div class=\"swal2-icon swal2-success\" style=\"display: none\"> <span class=\"line tip\"> </span> <span class=\"line long\"> </span> <div class=\"placeholder\"> </div> <div class=\"fix\"> </div> </div> <img class=\"swal2-image\" style=\"display: none\"> <h2 class=\"swal2-title\" id=\"swal2-title\"> <p align=\"center\" style=\"font-size:20px\">Delete <span style=\"color:red\">1 item(s) </span>from <span style=\"color:blue\">Data Kota ini.</span></p> <p align=\"center\">Anda Yakin?</p> </h2> <div id=\"swal2-content\" class=\"swal2-content\" style=\"display: block\" align=\"center\">Tidak dapat kembali ke kondisi sebelumnya!</div> <div align=\"center\" class=\"swal2-buttonswrapper\" style=\"display: block;margin-top:10px;margin-bottom:10px\"> <button ng-click=\"Modal_ApusPelanggaranWilayah_Kota_Simpan()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-confirm swal2-styled\" style=\"background-color: rgb(140, 212, 245); border-left-color: rgb(140, 212, 245); border-right-color: rgb(140, 212, 245);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">OK</button> <button ng-click=\"Modal_ApusPelanggaranWilayah_Kota_Batal()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-cancel swal2-styled\" style=\"display: inline-block; background-color: rgb(170, 170, 170);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">Cancel</button></div> </div> <div class=\"ui modal Modal_ApusPelanggaranWilayah_Kecamatan\" role=\"dialog\"> <ul class=\"swal2-progresssteps\" style=\"display: none\"></ul> <div class=\"swal2-icon swal2-error\" style=\"display: none\"> <span class=\"x-mark\"> <span class=\"line left\"> </span> <span class=\"line right\"> </span> </span> </div> <div class=\"swal2-icon swal2-question\" style=\"display: none\">?</div> <div class=\"swal2-icon swal2-warning\" style=\"display: block\">!</div> <div class=\"swal2-icon swal2-info\" style=\"display: none\">i</div> <div class=\"swal2-icon swal2-success\" style=\"display: none\"> <span class=\"line tip\"> </span> <span class=\"line long\"> </span> <div class=\"placeholder\"> </div> <div class=\"fix\"> </div> </div> <img class=\"swal2-image\" style=\"display: none\"> <h2 class=\"swal2-title\" id=\"swal2-title\"> <p align=\"center\" style=\"font-size:20px\">Delete <span style=\"color:red\">1 item(s) </span>from <span style=\"color:blue\">Data Kecamatan ini.</span></p> <p align=\"center\">Anda Yakin?</p> </h2> <div id=\"swal2-content\" class=\"swal2-content\" style=\"display: block\" align=\"center\">Tidak dapat kembali ke kondisi sebelumnya!</div> <div align=\"center\" class=\"swal2-buttonswrapper\" style=\"display: block;margin-top:10px;margin-bottom:10px\"> <button ng-click=\"Modal_ApusPelanggaranWilayah_Kecamatan_Simpan()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-confirm swal2-styled\" style=\"background-color: rgb(140, 212, 245); border-left-color: rgb(140, 212, 245); border-right-color: rgb(140, 212, 245);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">OK</button> <button ng-click=\"Modal_ApusPelanggaranWilayah_Kecamatan_Batal()\" type=\"button\" role=\"button\" tabindex=\"0\" class=\"swal2-cancel swal2-styled\" style=\"display: inline-block; background-color: rgb(170, 170, 170);box-sizing: border-box;border: 4px solid transparent;border-color: transparent;width: 100px;height: 40px;padding: 0;border-radius: 12px\">Cancel</button></div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/penggunaanKendaraan/penggunaanKendaraan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"PenggunaanKendaraanForm\" factory-name=\"PenggunaanKendaraanFactory\" model=\"mPenggunaanKendaraan\" model-id=\"PurchasePurposeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"PenggunaanKendaraanForm\" form-title=\"Penggunaan Kendaraan\" modal-title=\"Penggunaan Kendaraan\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <!--<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "\t\t\t\t\t<bsreqlabel>Kode Penggunaan Kendaraan</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"PurchasePurposeCode\" placeholder=\"Kode Penggunaan Kendaraan\" ng-model=\"mPenggunaanKendaraan.PurchasePurposeCode\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tng-maxlength=\"50\" required\r" +
    "\n" +
    "\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t<bserrmsg field=\"PurchasePurposeCode\"></bserrmsg>\r" +
    "\n" +
    "\t\t\t\t</div>--> <div class=\"form-group\" show-errors> <bsreqlabel>Penggunaan Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input alpha-numeric mask=\"Huruf\" type=\"text\" class=\"form-control\" name=\"PurchasePurposeName\" placeholder=\"Penggunaan Kendaraan\" ng-model=\"mPenggunaanKendaraan.PurchasePurposeName\" maxlength=\"20\" required> </div> <bserrmsg field=\"PurchasePurposeName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/poType/poType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"PoTypeForm\" factory-name=\"PoType\" model=\"mPoType\" model-id=\"POTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"PoTypeForm\" form-title=\"PoType\" modal-title=\"PoType\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>PO Type</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"POTypeName\" placeholder=\"PO Type\" ng-model=\"mPoType.POTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"POTypeName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/powerBI/powerBI.html',
    "<script type=\"text/javascript\">$(function(){\r" +
    "\n" +
    "            $(\"a.hidelink\").each(function (index, element){\r" +
    "\n" +
    "                var href = $(this).attr(\"href\");\r" +
    "\n" +
    "                $(this).attr(\"hiddenhref\", href);\r" +
    "\n" +
    "                $(this).removeAttr(\"href\");\r" +
    "\n" +
    "            });\r" +
    "\n" +
    "            $(\"a.hidelink\").click(function(){\r" +
    "\n" +
    "                url = $(this).attr(\"hiddenhref\");\r" +
    "\n" +
    "                window.open(url, '_blank');\r" +
    "\n" +
    "            })\r" +
    "\n" +
    "        });</script> <style type=\"text/css\">a.hidelink {\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "            text-decoration: underline;\r" +
    "\n" +
    "        }</style> <!-- <bsform-grid\r" +
    "\n" +
    "        ng-form=\"AgamaForm\" factory-name=\"AgamaFactoryMaster\" model=\"mAgama\" model-id=\"CustomerReligionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AgamaForm\" form-title=\"Agama\" modal-title=\"Master Agama\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\" show-errors>\r" +
    "\n" +
    "\t\t\t\t\t<bsreqlabel>Agama</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"CustomerReligionName\" placeholder=\"Agama\" ng-model=\"mAgama.CustomerReligionName\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tng-maxlength=\"50\" required\r" +
    "\n" +
    "\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t<bserrmsg field=\"CustomerReligionName\"></bserrmsg>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        \r" +
    "\n" +
    "</bsform-grid-tree> --> <div class=\"row\" style=\"margin-left: 10px\"> <!-- <embed src=\"{{currentProjectUrl}\" width=\"100%\" height=\"100%\"> --> <!-- test url ===> {{currentProjectUrl}} --> <iframe marginheight=\"0\" marginwidth=\"0\" scrolling=\"auto\" frameborder=\"0\" allowfullscreen style=\"width: 98%;height: 650px\" src=\"{{currentProjectUrl}}\"></iframe> <div style=\"background-color: white;width: 5%;height: 610px;position: absolute;margin-left: 92%;margin-top: -655px\"> <!-- <a class=\"hidelink\" href=\"{{currentProjectUrl}}\" onMouseover=\"JavaScript:window.status='Status Bar Message goes here'; return true\" onMouseout=\"JavaScript:window.status=''; return true\">Link Dashboard</a> --> </div> </div> <div class=\"ui modal loadingPowerBI\" style=\"height:200px; background-color:transparent; box-shadow: none !important\"> <div align=\"center\" class=\"list-group-item\" style=\"border: none !important;padding:10px 0px 10px 0px; background-color:transparent\"> <font color=\"white\"><i class=\"fa fa-spinner fa-spin fa-3x fa-lg\"></i> <b> Please Wait.... </b> </font> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/profileAsuransiPerluasanJaminan/profileAsuransiPerluasanJaminan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileAsuransiPerluasanJaminanForm\" factory-name=\"ProfileAsuransiPerluasanJaminan\" model=\"mProfileAsuransiPerluasanJaminan\" model-id=\"InsuranceExtensionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileAsuransiPerluasanJaminanForm\" form-title=\"Perluasan Jaminan\" modal-title=\"Perluasan Jaminan\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Perluasan Jaminan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Perluasan Jaminan\" ng-model=\"mProfileAsuransiPerluasanJaminan.InsuranceExtensionName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileBank/profileBank.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileBankForm\" factory-name=\"ProfileBank\" model=\"mProfileBank\" model-id=\"ProfileBankId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileBankForm\" form-title=\"ProfileBank\" modal-title=\"ProfileBank\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Account Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AccountCode\" placeholder=\"Account Code\" ng-model=\"mProfileBank.AccountCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AccountCode\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Account Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AccountName\" placeholder=\"Account Name\" ng-model=\"mProfileBank.AccountName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AccountName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Account User</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"AccountUser\" placeholder=\"Account User\" ng-model=\"mProfileBank.AccountUser\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"AccountUser\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bslabel>Account Type</bslabel> <bsselect name=\"AccountType\" ng-model=\"mProfileBank.AccountType\" data=\"optionsAccountType\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Select Account Type\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"AccountType\"></bserrmsg> <div class=\"form-group\" show-errors> <bsreqlabel>GL Account Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"GLAccountCode\" placeholder=\"GL Account Code\" ng-model=\"mProfileBank.GLAccountCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GLAccountCode\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileBiroJasa/profileBiroJasa.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileBiroJasaForm\" factory-name=\"ProfileBiroJasaFactory\" model=\"mProfileBiroJasa\" model-id=\"ServiceBureauId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileBiroJasaForm\" form-title=\"ProfileBiroJasa\" modal-title=\"Profile Biro Jasa\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Service Bureau Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ServiceBureauCode\" placeholder=\"Service Bureau Code\" ng-model=\"mProfileBiroJasa.ServiceBureauCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ServiceBureauCode\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Service Bureau Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ServiceBureauName\" placeholder=\"Service Bureau Name\" ng-model=\"mProfileBiroJasa.ServiceBureauName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ServiceBureauName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Service Bureau Address</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ServiceBureauAddress\" placeholder=\"Service Bureau Address\" ng-model=\"mProfileBiroJasa.ServiceBureauAddress\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ServiceBureauAddress\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Service Bureau Phone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ServiceBureauPhone\" placeholder=\"Service Bureau Phone\" ng-model=\"mProfileBiroJasa.ServiceBureauPhone\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ServiceBureauPhone\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Service Bureau Pic Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ServiceBureauPicName\" placeholder=\"ServiceBureauPicName\" ng-model=\"mProfileBiroJasa.ServiceBureauPicName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ServiceBureauPicName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Service Bureau Desc</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ServiceBureauDesc\" placeholder=\"Service Bureau Desc\" ng-model=\"mProfileBiroJasa.ServiceBureauDesc\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ServiceBureauDesc\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Service Bureau Price</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"ServiceBureauPrice\" placeholder=\"Service Bureau Price\" ng-model=\"mProfileBiroJasa.ServiceBureauPrice\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ServiceBureauPrice\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bslabel>Service Bureau City</bslabel> <bsselect name=\"ServiceBureauCity\" ng-model=\"mProfileBiroJasa.ServiceBureauCityId\" data=\"optionsKabupatenKota\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" on-select=\"value(selected)\" placeholder=\"Select Kabupaten Kota\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"ServiceBureauCity\"></bserrmsg> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileCustomerFleet/profileCustomerFleet.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileCustomerFleetForm\" factory-name=\"ProfileCustomerFleet\" model=\"mProfileCustomerFleet\" model-id=\"CustomerFleetId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileCustomerFleetForm\" form-title=\"ProfileCustomerFleet\" modal-title=\"ProfileCustomerFleet\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"code\" placeholder=\"Customer Code\" ng-model=\"mProfileCustomerFleet.CustomerCode\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Customer Name\" ng-model=\"mProfileCustomerFleet.CustomerName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileDriverPDS/profileDriverPDS.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileDriverPDSForm\" factory-name=\"ProfileDriverPDSFactory\" model=\"mProfileDriverPDS\" model-id=\"DriverPDSId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileDriverPDSForm\" form-title=\"Profile Driver PDS\" modal-title=\"Profile Driver PDS\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\" mode=\"ProfileDriverPDSMode\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Driver PDS</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Driver PDS\" ng-model=\"mProfileDriverPDS.DriverPDSName\" maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>No Telp Driver PDS</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input number-only type=\"tel\" class=\"form-control\" name=\"nama\" placeholder=\"No Telp Driver PDS\" ng-model=\"mProfileDriverPDS.DriverPDSPhoneNo\" maxlength=\"15\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <table> <tr> <td><bslabel>Driver Outsource</bslabel></td> <td> <div style=\"margin-left:15px;margin-top:6px\" class=\"input-icon-right\"> <label><input name=\"nama\" class=\"form-control\" type=\"checkbox\" ng-model=\"mProfileDriverPDS.DriverOutsource\" style=\"float:left;width: 20px;height: 20px\"></label> </div> <bserrmsg field=\"nama\"></bserrmsg> </td> </tr> </table> </div> <div class=\"form-group col-md-6 col-xs-5\" style=\"padding-left:0 !important\"> <bslabel>Site PDC</bslabel> <bsselect ng-model=\"DropdownSitePds\" data=\"optionsSitePdc\" item-text=\"PDCName\" item-value=\"PDCId\" on-select=\"SetSelectedSitePds(selected)\" placeholder=\"Select PDC\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-6 col-xs-1\" style=\"padding-left:0 !important\"> <button ng-click=\"AddSitePds()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\">Tambah </span><span class=\"ladda-spinner\"> </span> </button> </div> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Site PDC ID</th> <th>Name Site PDC</th> <th>Action</th> </tr> <tr ng-repeat=\"ListSitePds in mProfileDriverPDS.MSitesPDCDriverPDS track by $index\"> <td>{{ListSitePds.PDCId}}</td> <td>{{ListSitePds.PDCName}}</td> <td ng-if=\"ProfileDriverPDSMode!='view'\"><a ng-click=\"RemoveSitePds($index)\">Remove</a></td> </tr> </tbody> </table> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileInsurance/profileInsurance.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.ProfileInsuranceWarnainYgGanjil {\r" +
    "\n" +
    "    background-color: #EEEEEE;\r" +
    "\n" +
    "}</style> <div ng-show=\"ProfileInsuranceMain\"> <button style=\"float: right;margin-top:-30px;margin-left:10px\" ng-click=\"TambahProfileInsurance()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <i class=\"fa fa-plus\"></i><span>&nbsp&nbspTambah</span> </button> <bsform-grid ng-form=\"ProfileInsuranceForm\" factory-name=\"ProfileInsuranceFactory\" model=\"mProfileInsurance\" model-id=\"InsuranceId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" form-name=\"ProfileInsuranceForm\" form-title=\"Asuransi\" modal-title=\"Asuransi\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" grid-hide-action-column=\"true\" hide-new-button=\"true\" on-bulk-delete=\"deleteManualInsurance\"> </bsform-grid> </div> <div ng-show=\"ProfileInsuranceDetail\"> <div class=\"row\" style=\"margin-bottom:10px;margin-right:10px\"> <button ng-disabled=\"mProfileInsurance.InsuranceName==null||mProfileInsurance.InsuranceName==undefined||!mProfileInsurance.ListMProfileInsuranceExtensionView.length>=1\" ng-show=\"ShowBtnSimpanProfileInsurance\" style=\"float: right\" ng-click=\"SimpanProfileInsurance()\" type=\"button\" class=\"rbtn btn\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"ShowBtnSimpanProfileInsurance\" type=\"button\" class=\"wbtn btn ng-binding ng-scope\" ng-click=\"KembaliProfileInsurance()\" style=\"margin-right:3px;float: right\"> Batal </button> <button ng-show=\"ShowBtnKembaliProfileInsurance\" style=\"float: right\" ng-click=\"KembaliProfileInsurance()\" type=\"button\" class=\"rbtn btn\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <bsreqlabel>Nama Asuransi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"mProfileInsurance_Field_EnableDisable\" type=\"text\" class=\"form-control\" name=\"insuranceName\" placeholder=\"Nama Asuransi\" ng-model=\"mProfileInsurance.InsuranceName\" ng-maxlength=\"30\" maxlength=\"30\"> </div> <!--<bserrmsg field=\"insuranceName\"></bserrmsg>--> </div> </div> <div class=\"col-md-1\"> <bslabel>Status</bslabel> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <input type=\"checkbox\" ng-disabled=\"mProfileInsurance_Field_EnableDisable\" ng-model=\"mProfileInsurance.StatusActive\">&nbsp;&nbsp;<label style=\"margin-top:7px\">Aktif</label> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group col-md-6 col-xs-5\" style=\"padding-left:0 !important\"> <bslabel>Nama Perluasan Jaminan</bslabel> <bsselect ng-disabled=\"mProfileInsurance_Field_EnableDisable\" name=\"ProfileAsuransiPerluasanJaminan\" ng-model=\"DropdownProfileAsuransiPerluasanJaminan\" data=\"optionsProfileAsuransiPerluasanJaminan\" item-text=\"InsuranceExtensionName\" item-value=\"InsuranceExtensionId\" on-select=\"SetSelectedPerluasan(selected)\" placeholder=\"Nama Perluasan Jaminan\" icon=\"fa fa-search\"> </bsselect> </div> <!--<bserrmsg field=\"ProfileAsuransiPerluasanJaminan\"></bserrmsg>--> <div class=\"col-md-6 col-xs-1\" style=\"padding-left:0 !important\"> <button ng-disabled=\"mProfileInsurance_Field_EnableDisable\" ng-click=\"AddProfileAsuransiPerluasanJaminan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th style=\"background-color: #CCCCCC\">Id</th> <th style=\"background-color: #CCCCCC\">Nama Perluasan Jaminan</th> <th style=\"background-color: #CCCCCC\">Action</th> </tr> <tr ng-repeat=\"ListProfileAsuransiPerluasanJaminan in mProfileInsurance.ListMProfileInsuranceExtensionView track by $index\"> <td data-ng-class=\"ProfileInsuranceWarnainTabel($index)\">{{ListProfileAsuransiPerluasanJaminan.InsuranceExtensionId}}</td> <td data-ng-class=\"ProfileInsuranceWarnainTabel($index)\">{{ListProfileAsuransiPerluasanJaminan.InsuranceExtensionName}}</td> <td data-ng-class=\"ProfileInsuranceWarnainTabel($index)\" ng-show=\"!mProfileInsurance_Field_EnableDisable\"><a ng-show=\"!mProfileInsurance_Field_EnableDisable\" ng-click=\"RemoveProfileAsuransiPerluasanJaminan($index)\">Hapus</a></td> </tr> </tbody> </table> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/profileInsuranceType/profileInsuranceType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileInsuranceTypeForm\" factory-name=\"ProfileInsuranceTypeFactory\" model=\"mProfileInsuranceType\" model-id=\"ProfileInsuranceTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileInsuranceTypeForm\" form-title=\"ProfileInsuranceType\" modal-title=\"Profile Insurance Type\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Insurance Type Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"insuranceTypeName\" placeholder=\"Insurance Type Name\" ng-model=\"mProfileInsuranceType.InsuranceTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"insuranceTypeName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Insurance Type Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"insuranceTypeValue\" placeholder=\"Insurance Type Value\" ng-model=\"mProfileInsuranceType.InsuranceTypeValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"insuranceTypeValue\"></bserrmsg> </div> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <label>Parent Role</label>\r" +
    "\n" +
    "                <bsselect\r" +
    "\n" +
    "                          name=\"parentRole\"\r" +
    "\n" +
    "                          data=\"grid.data\"\r" +
    "\n" +
    "                          item-text=\"title\"\r" +
    "\n" +
    "                          item-value=\"id\"\r" +
    "\n" +
    "                          ng-model=\"mRole.pid\"\r" +
    "\n" +
    "                          placeholder=\"Select Parent Role\"\r" +
    "\n" +
    "                          icon=\"fa fa-child glyph-left\"\r" +
    "\n" +
    "                          style=\"min-width: 150px;\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bserrmsg field=\"parentRole\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileInsuranceUserType/profileInsuranceUserType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileInsuranceUserTypeForm\" factory-name=\"ProfileInsuranceUserTypeFactory\" model=\"mProfileInsuranceUserType\" model-id=\"ProfileInsuranceUserTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileInsuranceUserTypeForm\" form-title=\"ProfileInsuranceUserType\" modal-title=\"Profile Insurance User Type\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Insurance User Type Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"insuranceUserTypeName\" placeholder=\"Insurance User Type Name\" ng-model=\"mProfileInsuranceUserType.InsuranceUserTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"insuranceUserTypeName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Insurance User Type Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"insuranceUserTypeValue\" placeholder=\"Insurance User Type Value\" ng-model=\"mProfileInsuranceUserType.InsuranceUserTypeValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"insuranceUserTypeValue\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileLeasing/profileLeasing.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\">function navigateTabProfileLeasing(containerId, divId) {\r" +
    "\n" +
    "\t\t$(\"#\" + containerId).children().each(function (n, i) {\r" +
    "\n" +
    "\t\t\tvar id = this.id;\r" +
    "\n" +
    "\t\t\tconsole.log(id + ' - ' + divId);\r" +
    "\n" +
    "\t\t\tif (id == divId) {\r" +
    "\n" +
    "\t\t\t\t$('#' + divId).attr('class', 'tab-pane fade in active');\r" +
    "\n" +
    "\t\t\t} else {\r" +
    "\n" +
    "\t\t\t\t$('#' + id).attr('class', 'tab-pane fade');\r" +
    "\n" +
    "\t\t\t}\r" +
    "\n" +
    "\t\t});\r" +
    "\n" +
    "\t}</script> <style type=\"text/css\">.ProfileLeasingWarnainYgGanjil {\r" +
    "\n" +
    "\t\tbackground-color: #EEEEEE;\r" +
    "\n" +
    "\t}</style> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <link rel=\"styleSheet\" href=\"http://localhost:9000/release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <!-- ------------------------------------------------------------------------------------- --> <div ng-show=\"ShowProfileLeasingMain\"> <button style=\"float: right; margin-top: -31px; margin-left: 5px\" ng-click=\"TambahProfileLeasing()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <i class=\"fa fa-plus\"></i> <span>&nbsp&nbspTambah</span> </button> <bsform-grid ng-form=\"ProfileLeasingForm\" factory-name=\"ProfileLeasingFactory\" model=\"mProfileLeasing\" model-id=\"LeasingId\" loading=\"loading\" grid-hide-action-column=\"true\" hide-new-button=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" mode=\"uimode\" form-name=\"ProfileLeasingForm\" form-title=\"Profile Leasing\" modal-title=\"Profile Leasing\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-left:-12px\"> <div class=\"col-md-5\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Leasing</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"LeasingName\" placeholder=\"Nama Leasing\" ng-model=\"mProfileLeasing.LeasingName\" maxlength=\"50\" required> </div> <bserrmsg field=\"LeasingName\"></bserrmsg> </div> </div> <div class=\"col-md-1\"> <bslabel> Aktif / Non-Aktif </bslabel> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <input type=\"checkbox\" ng-model=\"mProfileLeasing.StatusActive\">&nbsp;&nbsp;<label style=\"margin-top:7px\">Aktif</label> </div> </div> <div class=\"col-md-5\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat</bsreqlabel> <div class=\"input-icon-right\"> <!--<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"LeasingAlamat\" placeholder=\"Alamat\" ng-model=\"mProfileLeasing.Address\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tmaxlength=\"50\" required\r" +
    "\n" +
    "\t\t\t\t\t\t>--> <textarea maxlength=\"255\" type=\"text\" class=\"form-control\" name=\"LeasingAlamat\" placeholder=\"Alamat\" ng-model=\"mProfileLeasing.Address\" rows=\"5\" cols=\"70\">\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> <!--<bserrmsg field=\"LeasingAlamat\"></bserrmsg>--> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group col-md-6 col-xs-5\" show-errors style=\"padding-left:0 !important\"> <bslabel>Waktu Tenor</bslabel> <bsselect ng-model=\"DropdownProfileLeasingTenor\" data=\"optionsProfileLeasingTenor\" item-text=\"LeasingTenorTime\" item-value=\"LeasingTenorId\" on-select=\"SetSelectedLeasing(selected)\" placeholder=\"Waktu Tenor\"> </bsselect> <!-- choice-filter=\"propsFilter: {LeasingTenorTime: $select.search}\" --> </div> <div class=\"col-md-6 col-xs-1\" style=\"padding-left:0 !important\"> <button ng-hide=\"(modenow == 'view' || uimode == 'view')\" ng-disabled=\"DropdownProfileLeasingTenor==null || DropdownProfileLeasingTenor==undefined || The_Leasing==undefined\" ng-click=\"AddProfileLeasingTenor()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <table class=\"table table-bordered table-responsive\"> <tbody> <tr style=\"background: lightgray\"> <th>Id Tenor</th> <th>Waktu Tenor</th> <th ng-if=\"(modenow == 'new' || modenow == 'edit') || (uimode == 'new' || uimode == 'edit')\">Action</th> </tr> <tr ng-repeat=\"ListProfileLeasingTenor in mProfileLeasing.ListLeasingTenor track by $index\"> <td data-ng-class=\"ProfileLeasingWarnainTabel($index)\">{{ListProfileLeasingTenor.LeasingTenorId}}</td> <td data-ng-class=\"ProfileLeasingWarnainTabel($index)\">{{ListProfileLeasingTenor.LeasingTenorTime}}</td> <td data-ng-class=\"ProfileLeasingWarnainTabel($index)\" ng-if=\"(modenow == 'new' || modenow == 'edit') || (uimode == 'new' || uimode == 'edit')\"><a ng-click=\"RemoveProfileLeasingTenor($index)\"><u>Hapus</u></a></td> </tr> </tbody> </table> </div> </div> </bsform-grid> </div> <div ng-show=\"ShowProfileLeasingDetail\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <button style=\"float: right\" ng-show=\"ProfileLeasingOPeration=='Insert' || ProfileLeasingOPeration=='Update'\" ng-disabled=\" gridProfileLeasing_Tenor.data==undefined || gridProfileLeasing_InformasiBank.data.length<=0 || gridProfileLeasing_InformasiBank.data==null || gridProfileLeasing_InformasiBank.data==undefined || gridProfileLeasing_Outlet.data.length<=0 || gridProfileLeasing_Outlet.data==undefined || gridProfileLeasing_Outlet.data==null\" ng-click=\"SimpanProfileLeasing()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span> <span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliDariProfileLeasingDetail()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Kode Leasing</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-show=\"ProfileLeasingOPeration=='Lihat'\" type=\"text\" class=\"form-control\" name=\"LeasingCode\" placeholder=\"Kode Leasing\" ng-model=\"mProfileLeasing.LeasingCode\" readonly> <input ng-show=\"ProfileLeasingOPeration=='Insert' || ProfileLeasingOPeration=='Update'\" type=\"text\" class=\"form-control\" name=\"LeasingCode\" placeholder=\"Kode Leasing\" ng-model=\"mProfileLeasing.LeasingCode\" required> </div> </div> </div> <div class=\"col-md-4\"> </div> <div class=\"col-md-4\"> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Nama Leasing</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"LeasingName\" ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" placeholder=\"Nama Leasing\" ng-model=\"mProfileLeasing.LeasingName\" maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-4\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" style=\"margin-top:5px\" ng-model=\"mProfileLeasing.ProvinceId\" data=\"ProvinsiOption\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" on-select=\"ProfileLeasingProvinsiSelected(selected)\" placeholder=\"Pilih Provinsi\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-4\"> <bsreqlabel>Bisnis Unit</bsreqlabel> <!-- <bsselect ng-disabled=\"ProfileLeasingOPeration=='Lihat'\"  style=\"margin-top:5px\"\r" +
    "\n" +
    "\t\t\t\t\tng-model=\"mProfileLeasing.BisnisUnit\" data=\"OptionsProfileLeasingBusinessUnit\"\r" +
    "\n" +
    "\t\t\t\t\titem-text=\"BusinessUnitName\" item-value=\"BusinessUnitId\"\r" +
    "\n" +
    "\t\t\t\t\ton-select=\"value(selected)\" placeholder=\"Pilih Bisnis Unit\"\r" +
    "\n" +
    "\t\t\t\t\ticon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t</bsselect> --> <input type=\"text\" class=\"form-control\" name=\"BisnisUnit\" ng-model=\"mProfileLeasing.BisnisUnit\" readonly> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Alamat Kantor</bsreqlabel> <div class=\"input-icon-right\"> <textarea ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" maxlength=\"255\" type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"mProfileLeasing.OfficeAddress\" rows=\"4\" cols=\"75\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <div class=\"col-md-4\"> <bsreqlabel>Kota</bsreqlabel> <bsselect ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" style=\"margin-bottom: 10px\" ng-model=\"mProfileLeasing.CityRegencyId\" data=\"ProfileLeasingKota\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" on-select=\"value(selected)\" placeholder=\"Pilih Kota\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-4\"> <bsreqlabel>Tipe Vendor</bsreqlabel> <bsselect ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" style=\"margin-bottom: 10px\" ng-model=\"mProfileLeasing.VendorTypeId\" data=\"TipeVendorOption\" item-text=\"Name\" item-value=\"VendorTypeId\" on-select=\"value(selected)\" placeholder=\"Pilih Tipe Vendor\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel>Nama NPWP</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" type=\"text\" class=\"form-control\" name=\"VendorName\" placeholder=\"Nama NPWP\" ng-model=\"mProfileLeasing.NPWPName\" required> </div> </div> </div> <div class=\"col-md-4\"> <bslabel>NPWP</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" ng-keypress=\"allowPattern($event,1)\" type=\"text\" class=\"form-control\" name=\"NPWPNo\" maskme=\"99.999.999.9-999.999\" placeholder=\"NPWP\" ng-model=\"mProfileLeasing.NPWPNo\" required> </div> </div> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "\t\t\t\t<bslabel>Jatuh Tempo Penerimaan (Hari)</bslabel>\r" +
    "\n" +
    "\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t<input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" number-only type=\"text\" class=\"form-control\"\r" +
    "\n" +
    "\t\t\t\t\t\tname=\"VendorName\" placeholder=\"Jatuh Tempo Penerimaan\" ng-model=\"mProfileLeasing.TermOfPayment\" required>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div> --> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel>Alamat NPWP</bslabel> <div class=\"input-icon-right\"> <textarea ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" maxlength=\"255\" type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Alamat NPWP\" ng-model=\"mProfileLeasing.NPWPAddress\" rows=\"4\" cols=\"75\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <div class=\"col-md-4\"> <bslabel>Kode Pos</bslabel> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Kode Pos\" ng-model=\"mProfileLeasing.PostCode\" required> </div> </div> <div class=\"col-md-4\"> <!--Performance Evaluation\r" +
    "\n" +
    "\t\t\t\t<div class=\"switch\">\r" +
    "\n" +
    "\t\t\t\t\t<input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" ng-model=\"mProfileLeasing.PerformanceEvaluation\"\r" +
    "\n" +
    "\t\t\t\t\t\tid=\"ProfileLeasingDaPerformanceEvaluation\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\">\r" +
    "\n" +
    "\t\t\t\t\t<label for=\"ProfileLeasingDaPerformanceEvaluation\"></label>\r" +
    "\n" +
    "\t\t\t\t</div>Ya--> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Email</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" type=\"email\" class=\"form-control\" placeholder=\"Email\" ng-model=\"mProfileLeasing.Email\" required> </div> </div> </div> <div class=\"col-md-4\"> <bslabel>Telepon</bslabel> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Telepon\" ng-model=\"mProfileLeasing.OfficePhone\" required> </div> </div> <div class=\"col-md-4\"> <!--Aktif / Non-Aktif\r" +
    "\n" +
    "\t\t\t\t<div class=\"switch\">\r" +
    "\n" +
    "\t\t\t\t\t<input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" ng-model=\"mProfileLeasing.StatusActive\"\r" +
    "\n" +
    "\t\t\t\t\t\tid=\"ProfileLeasingStatusActive\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\">\r" +
    "\n" +
    "\t\t\t\t\t<label for=\"ProfileLeasingStatusActive\"></label>\r" +
    "\n" +
    "\t\t\t\t</div>--> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>No HP Kontak Yang Dihubungi</bsreqlabel> <div class=\"input-icon-right\" style=\"margin-top:-5px\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" number-only type=\"text\" class=\"form-control\" placeholder=\"No HP Kontak Yang Dihubungi\" ng-model=\"mProfileLeasing.HP\" required> </div> </div> </div> <div class=\"col-md-4\"> <bslabel>Fax</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" number-only type=\"text\" class=\"form-control\" placeholder=\"Fax\" ng-model=\"mProfileLeasing.Fax\" required> </div> </div> <div class=\"col-md-4\"> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Kontak Yang Dihubungi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Kontak Yang Dihubungi\" ng-model=\"mProfileLeasing.ContactPerson\" required> </div> </div> </div> <div class=\"col-md-4\"> </div> <div class=\"col-md-4\"> </div> </div> <div class=\"col-md-12\"> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li id=\"ProfileLeasing_InformasiBank\" class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabProfileLeasing('tabContainerMaintainProfileLeasing','menu_ProfileLeasing_InformasiBank')\"><i class=\"fa fa-user\"></i>&nbsp Informasi Bank</a></li> <li id=\"ProfileLeasing_HargaBeli\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabProfileLeasing('tabContainerMaintainProfileLeasing','menu_ProfileLeasing_HargaBeli')\"><i class=\"fa fa-folder-open-o\"></i>&nbsp Tenor</a></li> <li id=\"ProfileLeasing_Outlet\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabProfileLeasing('tabContainerMaintainProfileLeasing','menu_ProfileLeasing_Outlet')\"><i class=\"fa fa-folder-open-o\"></i>&nbsp Outlet</a></li> <li id=\"ProfileLeasing_Outlet\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabProfileLeasing('tabContainerMaintainProfileLeasing','menu_ProfileLeasing_TOP')\"><i class=\"fa fa-folder-open-o\"></i>&nbsp TOP</a></li> </ul> <div id=\"tabContainerMaintainProfileLeasing\" class=\"tab-content\" style=\"padding-top: 15px; border: none !important\"> <div id=\"menu_ProfileLeasing_InformasiBank\" class=\"tab-pane fade in active\"> <div style=\"margin-top:20px;margin-left:-25px\"> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <bsreqlabel>Nama Bank</bsreqlabel> <!--<div style=\"margin-bottom:10px\" class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Nama Bank\" ng-model=\"mProfileLeasing.BankName\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t required\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>--> <bsselect style=\"margin-bottom: 10px\" ng-model=\"mProfileLeasing.BankId\" ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" data=\"OptionsProfileLeasingBank\" item-text=\"BankName\" item-value=\"BankId\" on-select=\"value(selected)\" placeholder=\"Bank\" icon=\"fa fa-search\"> </bsselect> <bsreqlabel>No Rekening</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" class=\"form-control\" placeholder=\"No Rekening\" ng-model=\"mProfileLeasing.AccountNumber\" required> </div> </div> <div class=\"col-md-4\"> <bsreqlabel>Rekening Atas Nama</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Atas Nama Bank\" ng-model=\"mProfileLeasing.AccountName\" required> </div> </div> </div> <button ng-disabled=\"ProfileLeasingOPeration=='Lihat' || mProfileLeasing.BankId==null || mProfileLeasing.BankId==undefined || mProfileLeasing.AccountNumber==null || mProfileLeasing.AccountNumber==undefined || mProfileLeasing.AccountNumber=='' || mProfileLeasing.AccountName==null || mProfileLeasing.AccountName=='' || mProfileLeasing.AccountName==undefined\" ng-click=\"TambahProfileLeasing_InformationBank()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-bottom:10px\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridProfileLeasing_InformasiBank\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> </div> <div id=\"menu_ProfileLeasing_HargaBeli\" class=\"tab-pane fade\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <div class=\"form-group col-md-6 col-xs-5\" style=\"padding-left:0 !important\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<bsreqlabel>Waktu Tenor</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<bsselect ng-model=\"DropdownProfileLeasingTenor\" data=\"OptionsProfileLeasingTenor\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\titem-text=\"LeasingTenorTime\" item-value=\"LeasingTenorId\" on-select=\"SetSelectedLeasing(selected)\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\tplaceholder=\"Waktu Tenor\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</div> --> <div class=\"form-group col-md-6 col-xs-5\" style=\"padding-left:0 !important\"> <bsreqlabel>Waktu Tenor Grid</bsreqlabel> <!-- <bsselect ng-model=\"DropdownProfileLeasingTenor\" data=\"OptionsProfileLeasingTenor\" item-text=\"LeasingTenorTime\" on-select=\"SetSelectedLeasing(selected)\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\titem-value=\"LeasingTenorId\" placeholder=\"Waktu Tenor\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</bsselect> --> <input type=\"number\" ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" class=\"form-control\" placeholder=\"Waktu Tenor\" ng-model=\"The_Leasing\" required> </div> <div class=\"col-md-6 col-xs-1\" style=\"padding-left:0 !important\"> <button ng-hide=\"(modenow == 'view' || uimode == 'view')\" ng-disabled=\"The_Leasing==undefined || The_Leasing==''||The_Leasing== null \" ng-click=\"AddProfileLeasingTenor()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:20px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridProfileLeasing_Tenor\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> </div> </div> </div> <div id=\"menu_ProfileLeasing_Outlet\" class=\"tab-pane fade\"> <!-- <div style=\"margin-top:20px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t<button ng-disabled=\"ProfileLeasingOPeration=='Lihat'\" ng-click=\"TambahProfileLeasing_Outlet()\" type=\"button\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tclass=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tTambah\r" +
    "\n" +
    "\t\t\t\t\t\t\t</span>\r" +
    "\n" +
    "\t\t\t\t\t\t\t<span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t\t</div> --> <div class=\"col-md-12\" style=\"margin-top:20px\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridProfileLeasing_Outlet\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> </div> <div id=\"menu_ProfileLeasing_TOP\" class=\"tab-pane fade\"> <div class=\"col-md-12\" style=\"margin-top:20px\"> <div style=\"display: block\" class=\"Mygrid\" ui-grid=\"gridProfileLeasing_TOP\" ui-grid-pagination ui-grid-edit ui-grid-selection ui-grid-auto-resize></div> </div> </div> </div> </div> </div> </div> <div class=\"ui modal ModalProfileLeasing_Outlet\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"ModalProfileLeasing_Outlet_Hilang()\" class=\"close\" data-dismiss=\"modal\">&times;</button> Outlet </div> <div class=\"modal-body\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"Modal_gridProfileLeasing_Outlet\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> <div class=\"modal-footer\"> <button ng-click=\"ProfileLeasingOutletPilih()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Pilih</button> <button ng-click=\"ModalProfileLeasing_Outlet_Hilang()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/profileLeasingSimulation/profileLeasingSimulation.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileLeasingSimulationForm\" factory-name=\"ProfileLeasingSimulationFactory\" model=\"mProfileLeasingSimulation\" model-id=\"ProfileLeasingSimulationId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileLeasingSimulationForm\" form-title=\"ProfileLeasingSimulation\" modal-title=\"Profile Leasing Simulation\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Leasing Simulation Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"leasingSimulationName\" placeholder=\"Leasing Simulation Name\" ng-model=\"mProfileLeasingSimulation.LeasingSimulationName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"leasingSimulationName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileLeasingTenor/profileLeasingTenor.html',
    "<script type=\"text/javascript\">var number = document.getElementById('ProfileLeasingTenorPreventNegative');\r" +
    "\n" +
    "\t// Listen for input event on numInput.\r" +
    "\n" +
    "\tnumber.onkeydown = function(e) {\r" +
    "\n" +
    "\t\tif(!((e.keyCode >= 48 && e.keyCode <= 57)\r" +
    "\n" +
    "\t\t  || e.keyCode == 8|| e.keyCode == 46)) {\r" +
    "\n" +
    "\t\t\t//titik tu 190 buat decimal\r" +
    "\n" +
    "\t\t\treturn false;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}</script> <style type=\"text/css\">.input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "       -moz-border-radius: 4px;\r" +
    "\n" +
    "            border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;</style> <bsform-grid ng-form=\"ProfileLeasingTenorForm\" factory-name=\"ProfileLeasingTenorFactory\" model=\"mProfileLeasingTenor\" model-id=\"LeasingTenorId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProfileLeasingTenorForm\" form-title=\"Leasing Tenor\" modal-title=\"Leasing Tenor\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" on-bulk-delete=\"DeleteManual\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Waktu Tenor</bsreqlabel> <ul class=\"list-inline\"> <li><div class=\"input-icon-right block\"> <i class=\"fa fa-pencil\"></i> <input maxlength=\"3\" number-only id=\"ProfileLeasingTenorPreventNegative\" type=\"text\" class=\"form-control\" name=\"leasingTenorTime\" placeholder=\"Waktu Tenor\" ng-model=\"mProfileLeasingTenor.LeasingTenorTime\" required> </div> </li> <li><label>(Bulan)</label></li> </ul> <bserrmsg field=\"leasingTenorTime\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/profileSalesProgram/profileSalesProgram.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProfileSalesProgramForm\" factory-name=\"ProfileSalesProgram\" model=\"mProfileSalesProgram\" model-id=\"SalesProgramId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-bulk-delete=\"hapusProfilSales\" form-name=\"ProfileSalesProgramForm\" form-title=\"Profile Sales Program\" modal-title=\"Profile Sales Program\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-left:-12px\"> <div class=\"col-md-5\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Sales Program</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Sales Program\" ng-model=\"mProfileSalesProgram.SalesProgramName\" maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> </div> <div class=\"col-md-1\"> <bslabel>Status</bslabel> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <input type=\"checkbox\" ng-model=\"mProfileSalesProgram.StatusActive\">&nbsp;&nbsp;<label style=\"margin-top:7px\">Aktif</label> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"col-md-5\" style=\"padding-left:0 !important\"> <bsreqlabel>Tanggal Mulai</bsreqlabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"StartValidDate\" date-options=\"dateOptionsStart\" ng-change=\"tanggalmin(mProfileSalesProgram.StartValidDate)\" ng-model=\"mProfileSalesProgram.StartValidDate\" required></bsdatepicker> </div> <bserrmsg field=\"StartValidDate\"></bserrmsg> </div> <div class=\"col-md-5\" style=\"padding-left:0 !important\"> <bsreqlabel>Tanggal Berakhir</bsreqlabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"EndValidDate\" date-options=\"dateOptionsEnd\" ng-change=\"tanggalmin(mProfileSalesProgram.StartValidDate)\" ng-model=\"mProfileSalesProgram.EndValidDate\" required> </bsdatepicker> </div> <bserrmsg field=\"EndValidDate\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/provinsiKabKecKelKodePos/provinsiKabKecKelKodePos.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ProvinsiKabKecKelKodePosForm\" factory-name=\"ProvinsiKabKecKelKodePosFactory\" model=\"mProvinsiKabKecKelKodePos\" model-id=\"LocationId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ProvinsiKabKecKelKodePosForm\" form-title=\"ProvinsiKabKecKelKodePos\" modal-title=\"ProvinsiKabKecKelKodePos\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" show-advsearch=\"on\"> <bsform-advsearch> <div class=\"row\" style=\"margin-left:10px\"> <div class=\"col-md-3\"> <div class=\"form-group filterMarg\"> <bsreqlabel>Pulau</bsreqlabel> <!-- <select \r" +
    "\n" +
    "\t\t\t\t\t\t\tstyle=\"margin-top:-5px\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-model=\"filter.IslandId\" \r" +
    "\n" +
    "\t\t\t\t\t\t\tclass=\"form-control\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-change=\"ProvinsiKabKecKelKodePosSelectedChanged_Island(filter.IslandId)\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-options=\"item.IslandId as item.IslandName for item in ProvinsiKabKecKelKodePos_DaIslandOptions\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tplaceholder=\"Pulau\">\r" +
    "\n" +
    "\t\t\t\t\t\t</select> --> <bsselect id=\"comboBox\" name=\"filterModel\" ng-change=\"ProvinsiKabKecKelKodePosSelectedChanged_Island(filter.IslandId)\" ng-model=\"filter.IslandId\" data=\"ProvinsiKabKecKelKodePos_DaIslandOptions\" item-text=\"IslandName\" item-value=\"IslandId\" on-select=\"IslandId\" placeholder=\"Pulau\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group filterMarg\"> <label>Kabupaten</label> <!-- <select \r" +
    "\n" +
    "\t\t\t\t\t\t\tng-model=\"filter.CityRegencyId\" \r" +
    "\n" +
    "\t\t\t\t\t\t\tclass=\"form-control\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-change=\"ProvinsiKabKecKelKodePosSelectedChanged_Kabupaten(filter.CityRegencyId)\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-options=\"item.CityRegencyId as item.CityRegencyName for item in ProvinsiKabKecKelKodePos_DaKabupatenOptions\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tplaceholder=\"Kabupaten\">\r" +
    "\n" +
    "\t\t\t\t\t\t</select> --> <bsselect id=\"comboBox\" name=\"filterModel\" ng-change=\"ProvinsiKabKecKelKodePosSelectedChanged_Kabupaten(filter.CityRegencyId)\" ng-model=\"filter.CityRegencyId\" data=\"ProvinsiKabKecKelKodePos_DaKabupatenOptions\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" on-select=\"CityRegencyId\" placeholder=\"Kabupaten\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group filterMarg\"> <label>Kelurahan</label> <!-- <select \r" +
    "\n" +
    "\t\t\t\t\t\t\tng-model=\"filter.VillageId\" \r" +
    "\n" +
    "\t\t\t\t\t\t\tclass=\"form-control\" \r" +
    "\n" +
    "\t\t\t\t\t\t\tng-options=\"item.VillageId as item.VillageName for item in ProvinsiKabKecKelKodePos_DaKelurahanOptions\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tplaceholder=\"Kelurahan\">\r" +
    "\n" +
    "\t\t\t\t\t\t</select> --> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.VillageId\" data=\"ProvinsiKabKecKelKodePos_DaKelurahanOptions\" item-text=\"VillageName\" item-value=\"VillageId\" on-select=\"VillageId\" placeholder=\"Kelurahan\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"row\" style=\"margin-left:10px\"> <div class=\"col-md-3\"> <div class=\"form-group filterMarg\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <select \r" +
    "\n" +
    "\t\t\t\t\t\t\tstyle=\"margin-top:-5px\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-model=\"filter.ProvinceId\" \r" +
    "\n" +
    "\t\t\t\t\t\t\tclass=\"form-control\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-change=\"ProvinsiKabKecKelKodePosSelectedChanged_Provinsi(filter.ProvinceId)\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-options=\"item.ProvinceId as item.ProvinceName for item in ProvinsiKabKecKelKodePos_DaProvinceOptions\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tplaceholder=\"Provinsi\">\r" +
    "\n" +
    "\t\t\t\t\t\t</select> --> <bsselect id=\"comboBox\" name=\"filterModel\" ng-change=\"ProvinsiKabKecKelKodePosSelectedChanged_Provinsi(filter.ProvinceId)\" ng-model=\"filter.ProvinceId\" data=\"ProvinsiKabKecKelKodePos_DaProvinceOptions\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" on-select=\"ProvinceId\" placeholder=\"Provinsi\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group filterMarg\"> <label>Kecamatan</label> <!-- <select \r" +
    "\n" +
    "\t\t\t\t\t\t\tng-model=\"filter.DistrictId\" \r" +
    "\n" +
    "\t\t\t\t\t\t\tclass=\"form-control\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-change=\"ProvinsiKabKecKelKodePosSelectedChanged_Kecamatan(filter.DistrictId)\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tng-options=\"item.DistrictId as item.DistrictName for item in ProvinsiKabKecKelKodePos_DaKecamatanOptions\"\r" +
    "\n" +
    "\t\t\t\t\t\t\tplaceholder=\"Kecamatan\">\r" +
    "\n" +
    "\t\t\t\t\t\t</select> --> <bsselect id=\"comboBox\" name=\"filterModel\" ng-change=\"ProvinsiKabKecKelKodePosSelectedChanged_Kecamatan(filter.DistrictId)\" ng-model=\"filter.DistrictId\" data=\"ProvinsiKabKecKelKodePos_DaKecamatanOptions\" item-text=\"DistrictName\" item-value=\"DistrictId\" on-select=\"DistrictId\" placeholder=\"Kecamatan\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Pulau</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"IslandName\" placeholder=\"Provinsi\" ng-model=\"mProvinsiKabKecKelKodePos.IslandName\" ng-maxlength=\"50\"> </div> </div> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"ProvinceName\" placeholder=\"Provinsi\" ng-model=\"mProvinsiKabKecKelKodePos.ProvinceName\" ng-maxlength=\"50\"> </div> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten Kota</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"CityRegencyName\" placeholder=\"Kabupaten Kota\" ng-model=\"mProvinsiKabKecKelKodePos.CityRegencyName\" ng-maxlength=\"50\"> </div> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"DistrictName\" placeholder=\"Kecamatan\" ng-model=\"mProvinsiKabKecKelKodePos.DistrictName\" ng-maxlength=\"50\"> </div> </div> <div class=\"form-group\"> <bsreqlabel>Kelurahan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"VillageName\" placeholder=\"Kelurahan\" ng-model=\"mProvinsiKabKecKelKodePos.VillageName\" ng-maxlength=\"50\"> </div> </div> <div class=\"form-group\"> <bsreqlabel>Kode Pos</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"true\" type=\"text\" class=\"form-control\" name=\"PostalCode\" placeholder=\"Postal Code\" ng-model=\"mProvinsiKabKecKelKodePos.PostalCode\" ng-maxlength=\"50\"> </div> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/ratioSales/ratioSales.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"MasterRatioSales_Simpan_Button\" ng-click=\"MasterRatioSales_Simpan_Clicked()\" ng-show=\"!MasterRatioSales_Edit_Show\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MasterRatioSales_Batal_Button\" ng-click=\"MasterRatioSales_Batal_Clicked()\" ng-show=\"!MasterRatioSales_Edit_Show\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <button id=\"MasterRatioSales_Edit_Button\" ng-click=\"MasterRatioSales_Edit_Clicked()\" ng-show=\"MasterRatioSales_Edit_Show\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:15px;margin-bottom:10px\"> <span class=\"ladda-label\"> Ubah </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div id=\"myGrid\" ui-grid=\"RatioSales_UIGrid\" ui-grid-edit ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:98%;height: 250px\"></div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/reason/reason.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ReasonForm\" factory-name=\"ReasonFactory\" model=\"mReason\" model-id=\"ReasonId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ReasonForm\" form-title=\"Reason\" modal-title=\"Reason\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Reason Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"reasonName\" placeholder=\"Reason Name\" ng-model=\"mReason.ReasonName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"reasonName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Reason Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"reasonValue\" placeholder=\"Reason Value\" ng-model=\"mReason.ReasonValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"reasonValue\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/reasonCancelBilling/reasonCancelBilling.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"ReasonCancelBillingForm\" factory-name=\"ReasonCancelBilling\" model=\"mReasonCancelBilling\" model-id=\"ReasonCancelId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ReasonCancelBillingForm\" form-title=\"Reason Cancel Billing\" modal-title=\"Reason Cancel Billing\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Reason Cancel</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ReasonCancelName\" placeholder=\"Reason Cancel\" ng-model=\"mReasonCancelBilling.ReasonCancelName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ReasonCancelName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/sampleUITemplate/sampleUITemplate.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uiboxstyle.css\"> <link rel=\"stylesheet\" href=\"css/uifullcalendarstyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/uimoment.js\"></script> <script type=\"text/javascript\" src=\"js/uifullcalendar.js\"></script> <script type=\"text/javascript\" src=\"js/uisparkline.js\"></script> <script type=\"text/javascript\" src=\"js/uiknob.js\"></script> <script type=\"text/javascript\">$(\"#dropdownBtnOveride\").click(function(){\r" +
    "\n" +
    "\tif($('#dropdownContentOveride').css('display') == 'none')\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride').show();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\telse\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride').hide();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "});\r" +
    "\n" +
    "\r" +
    "\n" +
    "$(\"#dropdownBtnOveride2\").click(function(){\r" +
    "\n" +
    "\tif($('#dropdownContentOveride2').css('display') == 'none')\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride2').show();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\telse\r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\t$('#dropdownContentOveride2').hide();\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "});\r" +
    "\n" +
    "\r" +
    "\n" +
    "$( '#myButton' ).click( function() {\r" +
    "\n" +
    "    $( '#myModal' ).modal();\r" +
    "\n" +
    "  });\r" +
    "\n" +
    " \r" +
    "\n" +
    "//function draw knob diagram\r" +
    "\n" +
    " $(function () {\r" +
    "\n" +
    "    /* jQueryKnob */\r" +
    "\n" +
    "\r" +
    "\n" +
    "    $(\".knob\").knob({\r" +
    "\n" +
    "      /*change : function (value) {\r" +
    "\n" +
    "       //console.log(\"change : \" + value);\r" +
    "\n" +
    "       },\r" +
    "\n" +
    "       release : function (value) {\r" +
    "\n" +
    "       console.log(\"release : \" + value);\r" +
    "\n" +
    "       },\r" +
    "\n" +
    "       cancel : function () {\r" +
    "\n" +
    "       console.log(\"cancel : \" + this.value);\r" +
    "\n" +
    "       },*/\r" +
    "\n" +
    "      draw: function () {\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // \"tron\" case\r" +
    "\n" +
    "        if (this.$.data('skin') == 'tron') {\r" +
    "\n" +
    "\r" +
    "\n" +
    "          var a = this.angle(this.cv)  // Angle\r" +
    "\n" +
    "              , sa = this.startAngle          // Previous start angle\r" +
    "\n" +
    "              , sat = this.startAngle         // Start angle\r" +
    "\n" +
    "              , ea                            // Previous end angle\r" +
    "\n" +
    "              , eat = sat + a                 // End angle\r" +
    "\n" +
    "              , r = true;\r" +
    "\n" +
    "\r" +
    "\n" +
    "          this.g.lineWidth = this.lineWidth;\r" +
    "\n" +
    "\r" +
    "\n" +
    "          this.o.cursor\r" +
    "\n" +
    "          && (sat = eat - 0.3)\r" +
    "\n" +
    "          && (eat = eat + 0.3);\r" +
    "\n" +
    "\r" +
    "\n" +
    "          if (this.o.displayPrevious) {\r" +
    "\n" +
    "            ea = this.startAngle + this.angle(this.value);\r" +
    "\n" +
    "            this.o.cursor\r" +
    "\n" +
    "            && (sa = ea - 0.3)\r" +
    "\n" +
    "            && (ea = ea + 0.3);\r" +
    "\n" +
    "            this.g.beginPath();\r" +
    "\n" +
    "            this.g.strokeStyle = this.previousColor;\r" +
    "\n" +
    "            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);\r" +
    "\n" +
    "            this.g.stroke();\r" +
    "\n" +
    "          }\r" +
    "\n" +
    "\r" +
    "\n" +
    "          this.g.beginPath();\r" +
    "\n" +
    "          this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;\r" +
    "\n" +
    "          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);\r" +
    "\n" +
    "          this.g.stroke();\r" +
    "\n" +
    "\r" +
    "\n" +
    "          this.g.lineWidth = 2;\r" +
    "\n" +
    "          this.g.beginPath();\r" +
    "\n" +
    "          this.g.strokeStyle = this.o.fgColor;\r" +
    "\n" +
    "          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);\r" +
    "\n" +
    "          this.g.stroke();\r" +
    "\n" +
    "\r" +
    "\n" +
    "          return false;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "      }\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "    /* END JQUERY KNOB */\r" +
    "\n" +
    "\r" +
    "\n" +
    "    //INITIALIZE SPARKLINE CHARTS\r" +
    "\n" +
    "    $(\".sparkline\").each(function () {\r" +
    "\n" +
    "      var $this = $(this);\r" +
    "\n" +
    "      $this.sparkline('html', $this.data());\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* SPARKLINE DOCUMENTATION EXAMPLES http://omnipotent.net/jquery.sparkline/#s-about */\r" +
    "\n" +
    "    drawDocSparklines();\r" +
    "\n" +
    "    drawMouseSpeedDemo();\r" +
    "\n" +
    "\r" +
    "\n" +
    "  });\r" +
    "\n" +
    "  function drawDocSparklines() {\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Bar + line composite charts\r" +
    "\n" +
    "    $('#compositebar').sparkline('html', {type: 'bar', barColor: '#aaf'});\r" +
    "\n" +
    "    $('#compositebar').sparkline([4, 1, 5, 7, 9, 9, 8, 7, 6, 6, 4, 7, 8, 4, 3, 2, 2, 5, 6, 7],\r" +
    "\n" +
    "        {composite: true, fillColor: false, lineColor: 'red'});\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Line charts taking their values from the tag\r" +
    "\n" +
    "    $('.sparkline-1').sparkline();\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Larger line charts for the docs\r" +
    "\n" +
    "    $('.largeline').sparkline('html',\r" +
    "\n" +
    "        {type: 'line', height: '2.5em', width: '4em'});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Customized line chart\r" +
    "\n" +
    "    $('#linecustom').sparkline('html',\r" +
    "\n" +
    "        {\r" +
    "\n" +
    "          height: '1.5em', width: '8em', lineColor: '#f00', fillColor: '#ffa',\r" +
    "\n" +
    "          minSpotColor: false, maxSpotColor: false, spotColor: '#77f', spotRadius: 3\r" +
    "\n" +
    "        });\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Bar charts using inline values\r" +
    "\n" +
    "    $('.sparkbar').sparkline('html', {type: 'bar'});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    $('.barformat').sparkline([1, 3, 5, 3, 8], {\r" +
    "\n" +
    "      type: 'bar',\r" +
    "\n" +
    "      tooltipFormat: '{{value:levels}} - {{value}}',\r" +
    "\n" +
    "      tooltipValueLookups: {\r" +
    "\n" +
    "        levels: $.range_map({':2': 'Low', '3:6': 'Medium', '7:': 'High'})\r" +
    "\n" +
    "      }\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Tri-state charts using inline values\r" +
    "\n" +
    "    $('.sparktristate').sparkline('html', {type: 'tristate'});\r" +
    "\n" +
    "    $('.sparktristatecols').sparkline('html',\r" +
    "\n" +
    "        {type: 'tristate', colorMap: {'-2': '#fa7', '2': '#44f'}});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Composite line charts, the second using values supplied via javascript\r" +
    "\n" +
    "    $('#compositeline').sparkline('html', {fillColor: false, changeRangeMin: 0, chartRangeMax: 10});\r" +
    "\n" +
    "    $('#compositeline').sparkline([4, 1, 5, 7, 9, 9, 8, 7, 6, 6, 4, 7, 8, 4, 3, 2, 2, 5, 6, 7],\r" +
    "\n" +
    "        {composite: true, fillColor: false, lineColor: 'red', changeRangeMin: 0, chartRangeMax: 10});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Line charts with normal range marker\r" +
    "\n" +
    "    $('#normalline').sparkline('html',\r" +
    "\n" +
    "        {fillColor: false, normalRangeMin: -1, normalRangeMax: 8});\r" +
    "\n" +
    "    $('#normalExample').sparkline('html',\r" +
    "\n" +
    "        {fillColor: false, normalRangeMin: 80, normalRangeMax: 95, normalRangeColor: '#4f4'});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Discrete charts\r" +
    "\n" +
    "    $('.discrete1').sparkline('html',\r" +
    "\n" +
    "        {type: 'discrete', lineColor: 'blue', xwidth: 18});\r" +
    "\n" +
    "    $('#discrete2').sparkline('html',\r" +
    "\n" +
    "        {type: 'discrete', lineColor: 'blue', thresholdColor: 'red', thresholdValue: 4});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Bullet charts\r" +
    "\n" +
    "    $('.sparkbullet').sparkline('html', {type: 'bullet'});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Pie charts\r" +
    "\n" +
    "    $('.sparkpie').sparkline('html', {type: 'pie', height: '1.0em'});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Box plots\r" +
    "\n" +
    "    $('.sparkboxplot').sparkline('html', {type: 'box'});\r" +
    "\n" +
    "    $('.sparkboxplotraw').sparkline([1, 3, 5, 8, 10, 15, 18],\r" +
    "\n" +
    "        {type: 'box', raw: true, showOutliers: true, target: 6});\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // Box plot with specific field order\r" +
    "\n" +
    "    $('.boxfieldorder').sparkline('html', {\r" +
    "\n" +
    "      type: 'box',\r" +
    "\n" +
    "      tooltipFormatFieldlist: ['med', 'lq', 'uq'],\r" +
    "\n" +
    "      tooltipFormatFieldlistKey: 'field'\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // click event demo sparkline\r" +
    "\n" +
    "    $('.clickdemo').sparkline();\r" +
    "\n" +
    "    $('.clickdemo').bind('sparklineClick', function (ev) {\r" +
    "\n" +
    "      var sparkline = ev.sparklines[0],\r" +
    "\n" +
    "          region = sparkline.getCurrentRegionFields();\r" +
    "\n" +
    "      value = region.y;\r" +
    "\n" +
    "      alert(\"Clicked on x=\" + region.x + \" y=\" + region.y);\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "\r" +
    "\n" +
    "    // mouseover event demo sparkline\r" +
    "\n" +
    "    $('.mouseoverdemo').sparkline();\r" +
    "\n" +
    "    $('.mouseoverdemo').bind('sparklineRegionChange', function (ev) {\r" +
    "\n" +
    "      var sparkline = ev.sparklines[0],\r" +
    "\n" +
    "          region = sparkline.getCurrentRegionFields();\r" +
    "\n" +
    "      value = region.y;\r" +
    "\n" +
    "      $('.mouseoverregion').text(\"x=\" + region.x + \" y=\" + region.y);\r" +
    "\n" +
    "    }).bind('mouseleave', function () {\r" +
    "\n" +
    "      $('.mouseoverregion').text('');\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "  }\r" +
    "\n" +
    "\r" +
    "\n" +
    "  /**\r" +
    "\n" +
    "   ** Draw the little mouse speed animated graph\r" +
    "\n" +
    "   ** This just attaches a handler to the mousemove event to see\r" +
    "\n" +
    "   ** (roughly) how far the mouse has moved\r" +
    "\n" +
    "   ** and then updates the display a couple of times a second via\r" +
    "\n" +
    "   ** setTimeout()\r" +
    "\n" +
    "   **/\r" +
    "\n" +
    "  function drawMouseSpeedDemo() {\r" +
    "\n" +
    "    var mrefreshinterval = 500; // update display every 500ms\r" +
    "\n" +
    "    var lastmousex = -1;\r" +
    "\n" +
    "    var lastmousey = -1;\r" +
    "\n" +
    "    var lastmousetime;\r" +
    "\n" +
    "    var mousetravel = 0;\r" +
    "\n" +
    "    var mpoints = [];\r" +
    "\n" +
    "    var mpoints_max = 30;\r" +
    "\n" +
    "    $('html').mousemove(function (e) {\r" +
    "\n" +
    "      var mousex = e.pageX;\r" +
    "\n" +
    "      var mousey = e.pageY;\r" +
    "\n" +
    "      if (lastmousex > -1) {\r" +
    "\n" +
    "        mousetravel += Math.max(Math.abs(mousex - lastmousex), Math.abs(mousey - lastmousey));\r" +
    "\n" +
    "      }\r" +
    "\n" +
    "      lastmousex = mousex;\r" +
    "\n" +
    "      lastmousey = mousey;\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "    var mdraw = function () {\r" +
    "\n" +
    "      var md = new Date();\r" +
    "\n" +
    "      var timenow = md.getTime();\r" +
    "\n" +
    "      if (lastmousetime && lastmousetime != timenow) {\r" +
    "\n" +
    "        var pps = Math.round(mousetravel / (timenow - lastmousetime) * 1000);\r" +
    "\n" +
    "        mpoints.push(pps);\r" +
    "\n" +
    "        if (mpoints.length > mpoints_max)\r" +
    "\n" +
    "          mpoints.splice(0, 1);\r" +
    "\n" +
    "        mousetravel = 0;\r" +
    "\n" +
    "        $('#mousespeed').sparkline(mpoints, {width: mpoints.length * 2, tooltipSuffix: ' pixels per second'});\r" +
    "\n" +
    "      }\r" +
    "\n" +
    "      lastmousetime = timenow;\r" +
    "\n" +
    "      setTimeout(mdraw, mrefreshinterval);\r" +
    "\n" +
    "    };\r" +
    "\n" +
    "    // We could use setInterval instead, but I prefer to do it this way\r" +
    "\n" +
    "    setTimeout(mdraw, mrefreshinterval);\r" +
    "\n" +
    "  }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "//function full calendar for Scheduling\r" +
    "\n" +
    "$(function () {\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* initialize the external events\r" +
    "\n" +
    "     -----------------------------------------------------------------*/\r" +
    "\n" +
    "    function ini_events(ele) {\r" +
    "\n" +
    "      ele.each(function () {\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)\r" +
    "\n" +
    "        // it doesn't need to have a start or end\r" +
    "\n" +
    "        var eventObject = {\r" +
    "\n" +
    "          title: $.trim($(this).text()) // use the element's text as the event title\r" +
    "\n" +
    "        };\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // store the Event Object in the DOM element so we can get to it later\r" +
    "\n" +
    "        $(this).data('eventObject', eventObject);\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // make the event draggable using jQuery UI\r" +
    "\n" +
    "        $(this).draggable({\r" +
    "\n" +
    "          zIndex: 1070,\r" +
    "\n" +
    "          revert: true, // will cause the event to go back to its\r" +
    "\n" +
    "          revertDuration: 0  //  original position after the drag\r" +
    "\n" +
    "        });\r" +
    "\n" +
    "\r" +
    "\n" +
    "      });\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    ini_events($('#external-events div.external-event'));\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* initialize the calendar\r" +
    "\n" +
    "     -----------------------------------------------------------------*/\r" +
    "\n" +
    "    //Date for the calendar events (dummy data)\r" +
    "\n" +
    "    var date = new Date();\r" +
    "\n" +
    "    var d = date.getDate(),\r" +
    "\n" +
    "        m = date.getMonth(),\r" +
    "\n" +
    "        y = date.getFullYear();\r" +
    "\n" +
    "    $('#calendar').fullCalendar({\r" +
    "\n" +
    "      header: {\r" +
    "\n" +
    "        left: 'prev,next today',\r" +
    "\n" +
    "        center: 'title',\r" +
    "\n" +
    "        right: 'month,agendaWeek,agendaDay'\r" +
    "\n" +
    "      },\r" +
    "\n" +
    "      buttonText: {\r" +
    "\n" +
    "        today: 'today',\r" +
    "\n" +
    "        month: 'month',\r" +
    "\n" +
    "        week: 'week',\r" +
    "\n" +
    "        day: 'day'\r" +
    "\n" +
    "      },\r" +
    "\n" +
    "      //Random default events\r" +
    "\n" +
    "      events: [\r" +
    "\n" +
    "        {\r" +
    "\n" +
    "          title: 'All Day Event',\r" +
    "\n" +
    "          start: new Date(y, m, 1),\r" +
    "\n" +
    "          backgroundColor: \"#f56954\", //red\r" +
    "\n" +
    "          borderColor: \"#f56954\" //red\r" +
    "\n" +
    "        },\r" +
    "\n" +
    "        {\r" +
    "\n" +
    "          title: 'Long Event',\r" +
    "\n" +
    "          start: new Date(y, m, d - 5),\r" +
    "\n" +
    "          end: new Date(y, m, d - 2),\r" +
    "\n" +
    "          backgroundColor: \"#f39c12\", //yellow\r" +
    "\n" +
    "          borderColor: \"#f39c12\" //yellow\r" +
    "\n" +
    "        },\r" +
    "\n" +
    "        {\r" +
    "\n" +
    "          title: 'Meeting',\r" +
    "\n" +
    "          start: new Date(y, m, d, 10, 30),\r" +
    "\n" +
    "          allDay: false,\r" +
    "\n" +
    "          backgroundColor: \"#0073b7\", //Blue\r" +
    "\n" +
    "          borderColor: \"#0073b7\" //Blue\r" +
    "\n" +
    "        },\r" +
    "\n" +
    "        {\r" +
    "\n" +
    "          title: 'Lunch',\r" +
    "\n" +
    "          start: new Date(y, m, d, 12, 0),\r" +
    "\n" +
    "          end: new Date(y, m, d, 14, 0),\r" +
    "\n" +
    "          allDay: false,\r" +
    "\n" +
    "          backgroundColor: \"#00c0ef\", //Info (aqua)\r" +
    "\n" +
    "          borderColor: \"#00c0ef\" //Info (aqua)\r" +
    "\n" +
    "        },\r" +
    "\n" +
    "        {\r" +
    "\n" +
    "          title: 'Birthday Party',\r" +
    "\n" +
    "          start: new Date(y, m, d + 1, 19, 0),\r" +
    "\n" +
    "          end: new Date(y, m, d + 1, 22, 30),\r" +
    "\n" +
    "          allDay: false,\r" +
    "\n" +
    "          backgroundColor: \"#00a65a\", //Success (green)\r" +
    "\n" +
    "          borderColor: \"#00a65a\" //Success (green)\r" +
    "\n" +
    "        },\r" +
    "\n" +
    "        {\r" +
    "\n" +
    "          title: 'Click for Google',\r" +
    "\n" +
    "          start: new Date(y, m, 28),\r" +
    "\n" +
    "          end: new Date(y, m, 29),\r" +
    "\n" +
    "          url: 'http://google.com/',\r" +
    "\n" +
    "          backgroundColor: \"#3c8dbc\", //Primary (light-blue)\r" +
    "\n" +
    "          borderColor: \"#3c8dbc\" //Primary (light-blue)\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "      ],\r" +
    "\n" +
    "      editable: true,\r" +
    "\n" +
    "      droppable: true, // this allows things to be dropped onto the calendar !!!\r" +
    "\n" +
    "      drop: function (date, allDay) { // this function is called when something is dropped\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // retrieve the dropped element's stored Event Object\r" +
    "\n" +
    "        var originalEventObject = $(this).data('eventObject');\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // we need to copy it, so that multiple events don't have a reference to the same object\r" +
    "\n" +
    "        var copiedEventObject = $.extend({}, originalEventObject);\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // assign it the date that was reported\r" +
    "\n" +
    "        copiedEventObject.start = date;\r" +
    "\n" +
    "        copiedEventObject.allDay = allDay;\r" +
    "\n" +
    "        copiedEventObject.backgroundColor = $(this).css(\"background-color\");\r" +
    "\n" +
    "        copiedEventObject.borderColor = $(this).css(\"border-color\");\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // render the event on the calendar\r" +
    "\n" +
    "        // the last `true` argument determines if the event \"sticks\" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)\r" +
    "\n" +
    "        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // is the \"remove after drop\" checkbox checked?\r" +
    "\n" +
    "        if ($('#drop-remove').is(':checked')) {\r" +
    "\n" +
    "          // if so, remove the element from the \"Draggable Events\" list\r" +
    "\n" +
    "          $(this).remove();\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "      }\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /* ADDING EVENTS */\r" +
    "\n" +
    "    var currColor = \"#3c8dbc\"; //Red by default\r" +
    "\n" +
    "    //Color chooser button\r" +
    "\n" +
    "    var colorChooser = $(\"#color-chooser-btn\");\r" +
    "\n" +
    "    $(\"#color-chooser > li > a\").click(function (e) {\r" +
    "\n" +
    "      e.preventDefault();\r" +
    "\n" +
    "      //Save color\r" +
    "\n" +
    "      currColor = $(this).css(\"color\");\r" +
    "\n" +
    "      //Add color effect to button\r" +
    "\n" +
    "      $('#add-new-event').css({\"background-color\": currColor, \"border-color\": currColor});\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "    $(\"#add-new-event\").click(function (e) {\r" +
    "\n" +
    "      e.preventDefault();\r" +
    "\n" +
    "      //Get value and make sure it is not null\r" +
    "\n" +
    "      var val = $(\"#new-event\").val();\r" +
    "\n" +
    "      if (val.length == 0) {\r" +
    "\n" +
    "        return;\r" +
    "\n" +
    "      }\r" +
    "\n" +
    "\r" +
    "\n" +
    "      //Create events\r" +
    "\n" +
    "      var event = $(\"<div />\");\r" +
    "\n" +
    "      event.css({\"background-color\": currColor, \"border-color\": currColor, \"color\": \"#fff\"}).addClass(\"external-event\");\r" +
    "\n" +
    "      event.html(val);\r" +
    "\n" +
    "      $('#external-events').prepend(event);\r" +
    "\n" +
    "\r" +
    "\n" +
    "      //Add draggable funtionality\r" +
    "\n" +
    "      ini_events(event);\r" +
    "\n" +
    "\r" +
    "\n" +
    "      //Remove event from text input\r" +
    "\n" +
    "      $(\"#new-event\").val(\"\");\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "  });</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}</style> <div class=\"row formHeader\"> <table> <tr> <td> <canvas width=\"35\" height=\"35\"></canvas> <input type=\"text\" class=\"knob\" value=\"80\" data-width=\"50\" data-height=\"50\" data-fgcolor=\"#3c8dbc\" data-readonly=\"true\" readonly style=\"width: 49px; height: 30px; position: absolute; vertical-align: middle; margin-top: 30px; \r" +
    "\n" +
    "\t\t\t\t\tmargin-left: -69px; border: 0px; background: none; font-style: normal; font-variant: normal; font-weight: bold; \r" +
    "\n" +
    "\t\t\t\t\tfont-stretch: normal; font-size: 18px; line-height: normal; font-family: Arial; text-align: center; \r" +
    "\n" +
    "\t\t\t\t\tcolor: rgb(60, 141, 188); padding: 0px; -webkit-appearance: none\"> </td> <td> <div class=\"knob-label\" style=\"max-width: 150px; word-wrap: break-word\">Data Prospect/Pelanggan (80%)</div> </td> </tr> </table> <div id=\"addMode\" style=\"=&quot;float:right;&quot\"> <button id=\"addBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; bottom: 10px\">Tambah </button> </div> <div id=\"editMode\" style=\"=&quot;float:right;&quot\"> <button id=\"saveBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Simpan </button> <button id=\"cancelBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Batal </button> </div> </div> <div id=\"formPanel\"> <!-- Edit Form Template --> <div class=\"formContent\"> <bsreqlabel>Sales Program Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Sales Program Name\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Sales Address</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Sales Address\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Tanggal Activity Working Calendar</bsreqlabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\"> </bsdatepicker> </div> <bsreqlabel>Upload</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"file\" accept=\"image/*;capture=camera\"> </div> <bsreqlabel>Combo Selection</bsreqlabel> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.Id\" data=\"selectionItem\" item-text=\"Name\" item-value=\"Id\" on-select=\"Id\" placeholder=\"Name\" icon=\"fa fa-search\"> </bsselect> </div> <bsreqlabel>Check Box</bsreqlabel> <div class=\"input-icon-right\"> <div class=\"checkbox\"> <label><input type=\"checkbox\" value=\"\">Option 1</label> </div> <div class=\"checkbox\"> <label><input type=\"checkbox\" value=\"\">Option 2</label> </div> <div class=\"checkbox\"> <label><input type=\"checkbox\" value=\"\">Option 3</label> </div> <div id=\"inlineCheckbox\"> <label class=\"checkbox-inline\"><input type=\"checkbox\" value=\"\">Option 1</label> <label class=\"checkbox-inline\"><input type=\"checkbox\" value=\"\">Option 2</label> <label class=\"checkbox-inline\"><input type=\"checkbox\" value=\"\">Option 3</label> </div> <!-- checkbox --> <div class=\"form-group\"> <div class=\"checkbox\"> <label> <input type=\"checkbox\"> Checkbox 1 </label> </div> <div class=\"checkbox\"> <label> <input type=\"checkbox\"> Checkbox 2 </label> </div> <div class=\"checkbox\"> <label> <input type=\"checkbox\" disabled> Checkbox disabled </label> </div> </div> </div> <bsreqlabel>Text Area</bsreqlabel> <div class=\"input-icon-right\"> <textarea class=\"form-control\" rows=\"5\" id=\"comment\"></textarea> </div> </div> <div class=\"formGroupPanel\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"formGroupPanelBtn\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Informasi Kendaraan <span id=\"formGroupPanelIcon\" style=\"float:right; margin-right:3px\"> + </span> </button> <div class=\"formContent\"> <div class=\"list-group\" id=\"formGroupPanelContent\" aria-labelledby=\"dropdownMenu2\" style=\"display:none\"> <bsreqlabel>Nama Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Nama Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Tipe Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Tipe Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> <bsreqlabel>Warna Kendaraan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Warna Kendaraan\" ng-model=\"mProfileSalesProgram_SalesProgramName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </div> <div id=\"formData\"> <div class=\"row filtersearch\" style=\"margin-right:0px;padding-bottom:5px\"> <div class=\"col-md-12\" style=\"padding-right:0px\"> <div class=\"input-group\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control ng-pristine ng-valid ng-empty ng-touched\" placeholder=\"Filter\" ng-model=\"gridApi.grid.columns[filterColIdx].filters[0].term\" style=\"\"> <div class=\"input-group-btn dropdown\" uib-dropdown=\"\" style=\"\"> <button id=\"filter\" type=\"button\" class=\"btn wbtn ng-binding dropdown-toggle\" uib-dropdown-toggle=\"\" data-toggle=\"dropdown\" tabindex=\"-1\" aria-haspopup=\"true\" aria-expanded=\"false\">MODEL CODE&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu=\"\" role=\"menu\" aria-labelledby=\"filter\"> <!-- ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">MODEL CODE<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[1].filters[0].term!='' &amp;&amp;gridApi.grid.columns[1].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">MODEL NAME<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[2].filters[0].term!='' &amp;&amp;gridApi.grid.columns[2].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">BRANDNAME<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[3].filters[0].term!='' &amp;&amp;gridApi.grid.columns[3].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> </ul> </div> </div> </div> </div> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer','menuTimeline')\">Timeline</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer','menuListData')\">List Data</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer','summaryFormTemplate')\">Summary Form Template</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer','menuScheduling')\">Scheduling</a></li> </ul> <!--Nama container harus unique berdasarkan page html--> <div id=\"tabContainer\" class=\"tab-content\"> <div id=\"menuTimeline\" class=\"tab-pane fade in active\"> <div class=\"row\" style=\"margin-top:10px\"> <div class=\"col-md-12\"> <!-- The time line --> <ul class=\"timeline\"> <!-- timeline time label --> <li class=\"time-label\"> <span class=\"bg-red\"> 10 Feb. 2014 </span> </li> <!-- /.timeline-label --> <!-- timeline item --> <li> <i class=\"fa fa-car\"></i> <div class=\"timeline-item\"> <span class=\"time\"><i class=\"fa fa-clock-o\"></i> 12:05</span> <h3 class=\"timeline-header\"><a href=\"#\">Support Team</a> sent you an email</h3> <div class=\"timeline-body\"> Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo... </div> <div class=\"timeline-footer\"> <a class=\"btn btn-primary btn-xs\">Read more</a> <a class=\"btn btn-danger btn-xs\">Delete</a> </div> </div> </li> <!-- END timeline item --> <!-- timeline item --> <li> <i class=\"fa fa-car\"></i> <div class=\"timeline-item\"> <span class=\"time\"><i class=\"fa fa-clock-o\"></i> 5 mins ago</span> <h3 class=\"timeline-header no-border\"><a href=\"#\">Sarah Young</a> accepted your friend request</h3> </div> </li> <!-- END timeline item --> <!-- timeline item --> <li> <i class=\"fa fa-car\"></i> <div class=\"timeline-item\"> <span class=\"time\"><i class=\"fa fa-clock-o\"></i> 27 mins ago</span> <h3 class=\"timeline-header\"><a href=\"#\">Jay White</a> commented on your post</h3> <div class=\"timeline-body\"> Take me to your leader! Switzerland is small and neutral! We are more like Germany, ambitious and misunderstood! </div> <div class=\"timeline-footer\"> <a class=\"btn btn-warning btn-flat btn-xs\">View comment</a> </div> </div> </li> <!-- END timeline item --> <!-- timeline time label --> <li class=\"time-label\"> <span class=\"bg-green\"> 3 Jan. 2014 </span> </li> <!-- /.timeline-label --> <!-- timeline item --> <li> <i class=\"fa fa-car\"></i> <div class=\"timeline-item\"> <span class=\"time\"><i class=\"fa fa-clock-o\"></i> 2 days ago</span> <h3 class=\"timeline-header\"><a href=\"#\">Mina Lee</a> uploaded new photos</h3> <div class=\"timeline-body\"> <img src=\"http://placehold.it/150x100\" alt=\"...\" class=\"margin\"> <img src=\"http://placehold.it/150x100\" alt=\"...\" class=\"margin\"> <img src=\"http://placehold.it/150x100\" alt=\"...\" class=\"margin\"> <img src=\"http://placehold.it/150x100\" alt=\"...\" class=\"margin\"> </div> </div> </li> <!-- END timeline item --> <!-- timeline item --> <li> <i class=\"fa fa-car\"></i> <div class=\"timeline-item\"> <span class=\"time\"><i class=\"fa fa-clock-o\"></i> 5 days ago</span> <h3 class=\"timeline-header\"><a href=\"#\">Mr. Doe</a> shared a video</h3> <div class=\"timeline-body\"> <div class=\"embed-responsive embed-responsive-16by9\"> <iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/gLGZwqpmnlY\" frameborder=\"0\" allowfullscreen></iframe> </div> </div> <div class=\"timeline-footer\"> <a href=\"#\" class=\"btn btn-xs bg-maroon\">See comments</a> </div> </div> </li> <!-- END timeline item --> <li> <i class=\"fa fa-clock-o bg-gray\"></i> </li> </ul> </div> <!-- /.col --> </div> </div> <div id=\"menuListData\" class=\"tab-pane fade\"> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtn\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Siap Test Drive <span>(3)</span> <span class=\"caret\" style=\"float:right;margin-top: 10px; margin-right:3px\"></span> </button> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\" style=\"display:none\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">Peggy Carter <button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">12 December 2016 </li> <li><i class=\"fa fa-circle\" style=\"color:red\"></i> </li> <li>Avanza Veloz 1.5 AT</li> </ul> </p> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">Maria Hill <button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">11 January 2016 </li> <li><i class=\"fa fa-circle\" style=\"color:white\"></i> </li> <li>Avanza Veloz 1.5 AT </li> </ul> </p> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">Sam Wilson <button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">21 June 2015 </li> <li><i class=\"fa fa-circle\" style=\"color:black\"></i> </li> <li>Avanza Veloz 1.5 MT </li> </ul> </p> </div> <div class=\"modal fade\" id=\"myModal\" role=\"dialog\"> <!-- Modal content--> <div class=\"modal-content\" style=\"width:25%; margin-top:15%; margin-left:45%\"> <ul class=\"list-group\"> <a ng-click=\"LookSelected(da_data.SalesProgramName)\"><li class=\"list-group-item\">Lihat Detail</li></a> <a ng-click=\"ChangeSelected(da_data.SalesProgramId,da_data.SalesProgramName)\"><li class=\"list-group-item\">Ubah Detail</li></a> <a ng-click=\"DeleteSelected(da_data.SalesProgramId)\"><li class=\"list-group-item\">Hapus Detail</li></a> </ul> </div> </div> </div> </div> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Produk Baru <span>(5)</span> <span class=\"caret\" style=\"float:right;margin-top: 10px; margin-right:3px\"></span> </button> <div class=\"list-group\" id=\"dropdownContentOveride\" aria-labelledby=\"dropdownMenu1\" style=\"display:none\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Avanza Veloz</p> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Harrier</p> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Alphard</p> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Fortuner</p> </div> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-11\"> <p style=\"font-size: 14px\">New Innova</p> </div> </div> </div> </div> </div> </div> <div id=\"summaryFormTemplate\" class=\"tab-pane fade\" style=\"margin-top:10px; margin-left:2px\"> <div id=\"headerSummary\"> <p style=\"font-size: 14px; font-weight: bold\"> All New Sienta ( G 1.5 M/T) </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Jakarta </li> <li><i class=\"fa fa-circle\" style=\"color:gray\"></i> </li> <li>Auto2000 Sunter</li> </ul> </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Salesman </li> <li>Justin Timberlake</li> </ul> </p> <p> <ul class=\"list-inline\"> <li style=\"width:100px\">Prospect </li> <li>Gigi Hadid</li> </ul> </p> </div> <div id=\"contentSummary\"> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th scope=\"row\" class=\"headerRow\">Nilai Kendaraan</th> <td>230.500.000</td> </tr> <tr> <th scope=\"row\" class=\"headerRow\">Total Down Payment</th> <td>30.000.000 atau 13.01%</td> </tr> <tr> <th scope=\"row\" class=\"headerRow\">Tenor</th> <td>48 Bulan</td> </tr> <tr> <th scope=\"row\" class=\"headerRow\">Leasing</th> <td>Toyota Astra Finance</td> </tr> </tbody> </table> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride2\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Tabel Simulasi <span class=\"caret\" style=\"float:right;margin-top: 10px; margin-right:3px\"></span> </button> <div id=\"dropdownContentOveride2\" aria-labelledby=\"dropdownMenu1\" style=\"display:none\"> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <th class=\"headerRow\">Bulan</th> <th class=\"headerRow\">Cicilan</th> <th class=\"headerRow\">Sisa Cicilan</th> </tr> </thead> <tbody> <tr> <th scope=\"row\">1</th> <td>x.xxx.xxx</td> <td>xx.xxx.xxx</td> </tr> <tr> <th scope=\"row\">2</th> <td>x.xxx.xxx</td> <td>xx.xxx.xxx</td> </tr> <tr> <th scope=\"row\">3</th> <td>x.xxx.xxx</td> <td>xx.xxx.xxx</td> </tr> <tr> <th scope=\"row\">4</th> <td>x.xxx.xxx</td> <td>xx.xxx.xxx</td> </tr> <tr> <th scope=\"row\">5</th> <td>x.xxx.xxx</td> <td>xx.xxx.xxx</td> </tr> </tbody> </table> </div> <div id=\"contentParagraph\"> <h6>Syarat & Kondisi</h6> <ul> <li>Semua harga yang tertera dalam mata uang Rupiah (Rp)</li> <li>OTR DKI Jakarta</li> <li>Rincian kredit di atas tidak mengikat dan dapat berubah sewaktu-waktu</li> <li>Harga di atas sudah termasuk asuransi all risk & ACP (ACC Credit Protection)</li> </ul> </div> </div> <footer class=\"footer navbar-fixed-bottom\" style=\"background-color: #f5f5f5; height:8%\"> <div class=\"container\" style=\"margin-top:5px; text-align:center\"> <button id=\"saveBtn\" class=\"rbtn btn ng-binding ladda-button\"> Simpan</button> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\"> Kirim Via Email</button> </div> </footer> </div> </div> <div id=\"menuScheduling\" class=\"tab-pane fade\"> <div class=\"wrapper\" style=\"height: auto; margin-top:10px\"> <div class=\"content-wrapper\"> <section class=\"content\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"box box-solid\"> <div class=\"box-header with-border\"> <h4 class=\"box-title\">Draggable Events</h4> </div> <div class=\"box-body\"> <!-- the events --> <div id=\"external-events\"> <div class=\"external-event bg-green ui-draggable ui-draggable-handle\" style=\"position: relative; z-index: auto; width: 209px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px\">Lunch</div> <div class=\"external-event bg-yellow ui-draggable ui-draggable-handle\" style=\"position: relative\">Go home</div> <div class=\"external-event bg-aqua ui-draggable ui-draggable-handle\" style=\"position: relative\">Do homework</div> <div class=\"external-event bg-light-blue ui-draggable ui-draggable-handle\" style=\"position: relative\">Work on UI design</div> <div class=\"external-event bg-red ui-draggable ui-draggable-handle\" style=\"position: relative\">Sleep tight</div> <div class=\"checkbox\"> <label for=\"drop-remove\"> <input type=\"checkbox\" id=\"drop-remove\"> remove after drop </label> </div> </div> </div> <!-- /.box-body --> </div> <!-- /. box --> <div class=\"box box-solid\"> <div class=\"box-header with-border\"> <h3 class=\"box-title\">Create Event</h3> </div> <div class=\"box-body\"> <div class=\"btn-group\" style=\"width: 100%; margin-bottom: 10px\"> <!--<button type=\"button\" id=\"color-chooser-btn\" class=\"btn btn-info btn-block dropdown-toggle\" data-toggle=\"dropdown\">Color <span class=\"caret\"></span></button>--> <ul class=\"fc-color-picker\" id=\"color-chooser\"> <li><a class=\"text-aqua\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-blue\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-light-blue\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-teal\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-yellow\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-orange\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-green\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-lime\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-red\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-purple\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-fuchsia\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-muted\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> <li><a class=\"text-navy\" href=\"#\"><i class=\"fa fa-square\"></i></a></li> </ul> </div> <!-- /btn-group --> <div class=\"input-group\"> <input id=\"new-event\" type=\"text\" class=\"form-control\" placeholder=\"Event Title\"> <div class=\"input-group-btn\"> <button id=\"add-new-event\" type=\"button\" class=\"btn btn-primary btn-flat\">Add</button> </div> <!-- /btn-group --> </div> <!-- /input-group --> </div> </div> </div> <!-- /.col --> <div class=\"col-md-9\"> <div class=\"box box-primary\"> <div class=\"box-body no-padding\"> <!-- THE CALENDAR --> <div id=\"calendar\" class=\"fc fc-ltr fc-unthemed\"><div class=\"fc-toolbar\"><div class=\"fc-left\"><div class=\"fc-button-group\"><button type=\"button\" class=\"fc-prev-button fc-button fc-state-default fc-corner-left\"><span class=\"fc-icon fc-icon-left-single-arrow\"></span></button><button type=\"button\" class=\"fc-next-button fc-button fc-state-default fc-corner-right\"><span class=\"fc-icon fc-icon-right-single-arrow\"></span></button></div><button type=\"button\" class=\"fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right fc-state-disabled\" disabled>today</button></div><div class=\"fc-right\"><div class=\"fc-button-group\"><button type=\"button\" class=\"fc-month-button fc-button fc-state-default fc-corner-left fc-state-active\">month</button><button type=\"button\" class=\"fc-agendaWeek-button fc-button fc-state-default\">week</button><button type=\"button\" class=\"fc-agendaDay-button fc-button fc-state-default fc-corner-right\">day</button></div></div><div class=\"fc-center\"><h2>January 2017</h2></div><div class=\"fc-clear\"></div></div><div class=\"fc-view-container\" style=\"\"><div class=\"fc-view fc-month-view fc-basic-view\"><table><thead><tr><td class=\"fc-widget-header\"><div class=\"fc-row fc-widget-header\"><table><thead><tr><th class=\"fc-day-header fc-widget-header fc-sun\">Sun</th><th class=\"fc-day-header fc-widget-header fc-mon\">Mon</th><th class=\"fc-day-header fc-widget-header fc-tue\">Tue</th><th class=\"fc-day-header fc-widget-header fc-wed\">Wed</th><th class=\"fc-day-header fc-widget-header fc-thu\">Thu</th><th class=\"fc-day-header fc-widget-header fc-fri\">Fri</th><th class=\"fc-day-header fc-widget-header fc-sat\">Sat</th></tr></thead></table></div></td></tr></thead><tbody><tr><td class=\"fc-widget-content\"><div class=\"fc-day-grid-container\"><div class=\"fc-day-grid\"><div class=\"fc-row fc-week fc-widget-content\" style=\"height: 106px\"><div class=\"fc-bg\"><table><tbody><tr><td class=\"fc-day fc-widget-content fc-sun fc-past\" data-date=\"2017-01-01\"></td><td class=\"fc-day fc-widget-content fc-mon fc-past\" data-date=\"2017-01-02\"></td><td class=\"fc-day fc-widget-content fc-tue fc-past\" data-date=\"2017-01-03\"></td><td class=\"fc-day fc-widget-content fc-wed fc-past\" data-date=\"2017-01-04\"></td><td class=\"fc-day fc-widget-content fc-thu fc-past\" data-date=\"2017-01-05\"></td><td class=\"fc-day fc-widget-content fc-fri fc-past\" data-date=\"2017-01-06\"></td><td class=\"fc-day fc-widget-content fc-sat fc-past\" data-date=\"2017-01-07\"></td></tr></tbody></table></div><div class=\"fc-content-skeleton\"><table><thead><tr><td class=\"fc-day-number fc-sun fc-past\" data-date=\"2017-01-01\">1</td><td class=\"fc-day-number fc-mon fc-past\" data-date=\"2017-01-02\">2</td><td class=\"fc-day-number fc-tue fc-past\" data-date=\"2017-01-03\">3</td><td class=\"fc-day-number fc-wed fc-past\" data-date=\"2017-01-04\">4</td><td class=\"fc-day-number fc-thu fc-past\" data-date=\"2017-01-05\">5</td><td class=\"fc-day-number fc-fri fc-past\" data-date=\"2017-01-06\">6</td><td class=\"fc-day-number fc-sat fc-past\" data-date=\"2017-01-07\">7</td></tr></thead><tbody><tr><td class=\"fc-event-container\"><a class=\"fc-day-grid-event fc-event fc-start fc-end fc-draggable\" style=\"background-color:#f56954;border-color:#f56954\"><div class=\"fc-content\"><span class=\"fc-time\">12a</span> <span class=\"fc-title\">All Day Event</span></div></a></td><td class=\"fc-event-container\" colspan=\"2\"><a class=\"fc-day-grid-event fc-event fc-start fc-end fc-draggable fc-resizable\" style=\"background-color:rgb(0, 166, 90);border-color:rgb(255, 255, 255)\"><div class=\"fc-content\"> <span class=\"fc-title\">Lunch</span></div><div class=\"fc-resizer\"></div></a></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class=\"fc-row fc-week fc-widget-content\" style=\"height: 106px\"><div class=\"fc-bg\"><table><tbody><tr><td class=\"fc-day fc-widget-content fc-sun fc-past\" data-date=\"2017-01-08\"></td><td class=\"fc-day fc-widget-content fc-mon fc-past\" data-date=\"2017-01-09\"></td><td class=\"fc-day fc-widget-content fc-tue fc-past\" data-date=\"2017-01-10\"></td><td class=\"fc-day fc-widget-content fc-wed fc-past\" data-date=\"2017-01-11\"></td><td class=\"fc-day fc-widget-content fc-thu fc-past\" data-date=\"2017-01-12\"></td><td class=\"fc-day fc-widget-content fc-fri fc-past\" data-date=\"2017-01-13\"></td><td class=\"fc-day fc-widget-content fc-sat fc-past\" data-date=\"2017-01-14\"></td></tr></tbody></table></div><div class=\"fc-content-skeleton\"><table><thead><tr><td class=\"fc-day-number fc-sun fc-past\" data-date=\"2017-01-08\">8</td><td class=\"fc-day-number fc-mon fc-past\" data-date=\"2017-01-09\">9</td><td class=\"fc-day-number fc-tue fc-past\" data-date=\"2017-01-10\">10</td><td class=\"fc-day-number fc-wed fc-past\" data-date=\"2017-01-11\">11</td><td class=\"fc-day-number fc-thu fc-past\" data-date=\"2017-01-12\">12</td><td class=\"fc-day-number fc-fri fc-past\" data-date=\"2017-01-13\">13</td><td class=\"fc-day-number fc-sat fc-past\" data-date=\"2017-01-14\">14</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td class=\"fc-event-container\"><a class=\"fc-day-grid-event fc-event fc-start fc-not-end fc-draggable\" style=\"background-color:#f39c12;border-color:#f39c12\"><div class=\"fc-content\"><span class=\"fc-time\">12a</span> <span class=\"fc-title\">Long Event</span></div></a></td></tr></tbody></table></div></div><div class=\"fc-row fc-week fc-widget-content\" style=\"height: 106px\"><div class=\"fc-bg\"><table><tbody><tr><td class=\"fc-day fc-widget-content fc-sun fc-past\" data-date=\"2017-01-15\"></td><td class=\"fc-day fc-widget-content fc-mon fc-past\" data-date=\"2017-01-16\"></td><td class=\"fc-day fc-widget-content fc-tue fc-past\" data-date=\"2017-01-17\"></td><td class=\"fc-day fc-widget-content fc-wed fc-past\" data-date=\"2017-01-18\"></td><td class=\"fc-day fc-widget-content fc-thu fc-today fc-state-highlight\" data-date=\"2017-01-19\"></td><td class=\"fc-day fc-widget-content fc-fri fc-future\" data-date=\"2017-01-20\"></td><td class=\"fc-day fc-widget-content fc-sat fc-future\" data-date=\"2017-01-21\"></td></tr></tbody></table></div><div class=\"fc-content-skeleton\"><table><thead><tr><td class=\"fc-day-number fc-sun fc-past\" data-date=\"2017-01-15\">15</td><td class=\"fc-day-number fc-mon fc-past\" data-date=\"2017-01-16\">16</td><td class=\"fc-day-number fc-tue fc-past\" data-date=\"2017-01-17\">17</td><td class=\"fc-day-number fc-wed fc-past\" data-date=\"2017-01-18\">18</td><td class=\"fc-day-number fc-thu fc-today fc-state-highlight\" data-date=\"2017-01-19\">19</td><td class=\"fc-day-number fc-fri fc-future\" data-date=\"2017-01-20\">20</td><td class=\"fc-day-number fc-sat fc-future\" data-date=\"2017-01-21\">21</td></tr></thead><tbody><tr><td class=\"fc-event-container\" colspan=\"2\"><a class=\"fc-day-grid-event fc-event fc-not-start fc-end fc-draggable\" style=\"background-color:#f39c12;border-color:#f39c12\"><div class=\"fc-content\"> <span class=\"fc-title\">Long Event</span></div></a></td><td rowspan=\"2\"></td><td rowspan=\"2\"></td><td class=\"fc-event-container\"><a class=\"fc-day-grid-event fc-event fc-start fc-end fc-draggable\" style=\"background-color:#0073b7;border-color:#0073b7\"><div class=\"fc-content\"><span class=\"fc-time\">10:30a</span> <span class=\"fc-title\">Meeting</span></div></a></td><td class=\"fc-event-container\" rowspan=\"2\"><a class=\"fc-day-grid-event fc-event fc-start fc-end fc-draggable\" style=\"background-color:#00a65a;border-color:#00a65a\"><div class=\"fc-content\"><span class=\"fc-time\">7p</span> <span class=\"fc-title\">Birthday Party</span></div></a></td><td rowspan=\"2\"></td></tr><tr><td></td><td></td><td class=\"fc-event-container\"><a class=\"fc-day-grid-event fc-event fc-start fc-end fc-draggable\" style=\"background-color:#00c0ef;border-color:#00c0ef\"><div class=\"fc-content\"><span class=\"fc-time\">12p</span> <span class=\"fc-title\">Lunch</span></div></a></td></tr></tbody></table></div></div><div class=\"fc-row fc-week fc-widget-content\" style=\"height: 106px\"><div class=\"fc-bg\"><table><tbody><tr><td class=\"fc-day fc-widget-content fc-sun fc-future\" data-date=\"2017-01-22\"></td><td class=\"fc-day fc-widget-content fc-mon fc-future\" data-date=\"2017-01-23\"></td><td class=\"fc-day fc-widget-content fc-tue fc-future\" data-date=\"2017-01-24\"></td><td class=\"fc-day fc-widget-content fc-wed fc-future\" data-date=\"2017-01-25\"></td><td class=\"fc-day fc-widget-content fc-thu fc-future\" data-date=\"2017-01-26\"></td><td class=\"fc-day fc-widget-content fc-fri fc-future\" data-date=\"2017-01-27\"></td><td class=\"fc-day fc-widget-content fc-sat fc-future\" data-date=\"2017-01-28\"></td></tr></tbody></table></div><div class=\"fc-content-skeleton\"><table><thead><tr><td class=\"fc-day-number fc-sun fc-future\" data-date=\"2017-01-22\">22</td><td class=\"fc-day-number fc-mon fc-future\" data-date=\"2017-01-23\">23</td><td class=\"fc-day-number fc-tue fc-future\" data-date=\"2017-01-24\">24</td><td class=\"fc-day-number fc-wed fc-future\" data-date=\"2017-01-25\">25</td><td class=\"fc-day-number fc-thu fc-future\" data-date=\"2017-01-26\">26</td><td class=\"fc-day-number fc-fri fc-future\" data-date=\"2017-01-27\">27</td><td class=\"fc-day-number fc-sat fc-future\" data-date=\"2017-01-28\">28</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td class=\"fc-event-container\"><a class=\"fc-day-grid-event fc-event fc-start fc-end fc-draggable\" href=\"http://google.com/\" style=\"background-color:#3c8dbc;border-color:#3c8dbc\"><div class=\"fc-content\"><span class=\"fc-time\">12a</span> <span class=\"fc-title\">Click for Google</span></div></a></td></tr></tbody></table></div></div><div class=\"fc-row fc-week fc-widget-content\" style=\"height: 106px\"><div class=\"fc-bg\"><table><tbody><tr><td class=\"fc-day fc-widget-content fc-sun fc-future\" data-date=\"2017-01-29\"></td><td class=\"fc-day fc-widget-content fc-mon fc-future\" data-date=\"2017-01-30\"></td><td class=\"fc-day fc-widget-content fc-tue fc-future\" data-date=\"2017-01-31\"></td><td class=\"fc-day fc-widget-content fc-wed fc-other-month fc-future\" data-date=\"2017-02-01\"></td><td class=\"fc-day fc-widget-content fc-thu fc-other-month fc-future\" data-date=\"2017-02-02\"></td><td class=\"fc-day fc-widget-content fc-fri fc-other-month fc-future\" data-date=\"2017-02-03\"></td><td class=\"fc-day fc-widget-content fc-sat fc-other-month fc-future\" data-date=\"2017-02-04\"></td></tr></tbody></table></div><div class=\"fc-content-skeleton\"><table><thead><tr><td class=\"fc-day-number fc-sun fc-future\" data-date=\"2017-01-29\">29</td><td class=\"fc-day-number fc-mon fc-future\" data-date=\"2017-01-30\">30</td><td class=\"fc-day-number fc-tue fc-future\" data-date=\"2017-01-31\">31</td><td class=\"fc-day-number fc-wed fc-other-month fc-future\" data-date=\"2017-02-01\">1</td><td class=\"fc-day-number fc-thu fc-other-month fc-future\" data-date=\"2017-02-02\">2</td><td class=\"fc-day-number fc-fri fc-other-month fc-future\" data-date=\"2017-02-03\">3</td><td class=\"fc-day-number fc-sat fc-other-month fc-future\" data-date=\"2017-02-04\">4</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class=\"fc-row fc-week fc-widget-content\" style=\"height: 108px\"><div class=\"fc-bg\"><table><tbody><tr><td class=\"fc-day fc-widget-content fc-sun fc-other-month fc-future\" data-date=\"2017-02-05\"></td><td class=\"fc-day fc-widget-content fc-mon fc-other-month fc-future\" data-date=\"2017-02-06\"></td><td class=\"fc-day fc-widget-content fc-tue fc-other-month fc-future\" data-date=\"2017-02-07\"></td><td class=\"fc-day fc-widget-content fc-wed fc-other-month fc-future\" data-date=\"2017-02-08\"></td><td class=\"fc-day fc-widget-content fc-thu fc-other-month fc-future\" data-date=\"2017-02-09\"></td><td class=\"fc-day fc-widget-content fc-fri fc-other-month fc-future\" data-date=\"2017-02-10\"></td><td class=\"fc-day fc-widget-content fc-sat fc-other-month fc-future\" data-date=\"2017-02-11\"></td></tr></tbody></table></div><div class=\"fc-content-skeleton\"><table><thead><tr><td class=\"fc-day-number fc-sun fc-other-month fc-future\" data-date=\"2017-02-05\">5</td><td class=\"fc-day-number fc-mon fc-other-month fc-future\" data-date=\"2017-02-06\">6</td><td class=\"fc-day-number fc-tue fc-other-month fc-future\" data-date=\"2017-02-07\">7</td><td class=\"fc-day-number fc-wed fc-other-month fc-future\" data-date=\"2017-02-08\">8</td><td class=\"fc-day-number fc-thu fc-other-month fc-future\" data-date=\"2017-02-09\">9</td><td class=\"fc-day-number fc-fri fc-other-month fc-future\" data-date=\"2017-02-10\">10</td><td class=\"fc-day-number fc-sat fc-other-month fc-future\" data-date=\"2017-02-11\">11</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div></div></div></td></tr></tbody></table></div></div></div> </div> <!-- /.box-body --> </div> <!-- /. box --> </div> <!-- /.col --> </div> <!-- /.row --> </section> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/sektorBisnis/sektorBisnis.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SektorBisnisForm\" factory-name=\"SektorBisnisFactory\" model=\"mSektorBisnis\" model-id=\"SectorBusinessId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SektorBisnisForm\" form-title=\"Sektor Bisnis\" modal-title=\"Sektor Bisnis\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Sektor Bisnis</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"SectorBusinessName\" placeholder=\"Sektor Bisnis\" ng-model=\"mSektorBisnis.SectorBusinessName\" maxlength=\"50\" required> </div> <bserrmsg field=\"SectorBusinessName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/setStatusFleet/setStatusFleet.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"mainForm\"> <bsform-grid ng-form=\"SetStatusFleetForm\" factory-name=\"SetStatusFleetFactory\" model=\"mSetStatusFleet\" model-id=\"CustomerId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" grid-hide-action-column=\"true\" show-advsearch=\"on\" hide-new-button=\"true\" on-select-rows=\"onSelectRows\" form-name=\"setstatusfleetForm\" form-title=\"Set Status Fleet\" modal-title=\"Set Status Fleet\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pembelian dalam kurun waktu (tahun)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-th-list\"></i> <input type=\"number\" step=\"1\" pattern=\"[0-9]\" value=\"0\" min=\"0\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"filter.timeFrameYear\" maxlength=\"10\"> <!-- <input number-only type=\"number\"  step=\"1\" pattern=\"[0-9]\" value=\"0\" min=\"0\" class=\"form-control\" name=\"name\" placeholder=\"\" ng-model=\"filter.timeFrameYear\" maxlength=\"30\" required> --> <!-- <bsdatepicker name=\"StartValidDate\" date-options=\"dateOptions\" ng-model=\"filter.timeFrameYearStart\">\r" +
    "\n" +
    "                                </bsdatepicker> --> </div> <bserrmsg field=\"name\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "                        <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                        <bsreqlabel>Pembelian Dalam kurun Waktu (End)</bsreqlabel>\r" +
    "\n" +
    "                        <div class=\"input-icon-right\">\r" +
    "\n" +
    "                            <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"\" ng-model=\"filter.timeFrameYear\" maxlength=\"30\" required>\r" +
    "\n" +
    "                            <bsdatepicker ng-disabled=\"filter.timeFrameYearStart == null\" name=\"StartValidDate\" date-options=\"dateOptions\" ng-model=\"filter.timeFrameYearEnd\">\r" +
    "\n" +
    "                            </bsdatepicker>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"name\"></bserrmsg>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                </div> --> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Jumlah Minimal Pembelian</label> <div class=\"input-icon-right\"> <i class=\"fa fa-money\"></i> <!-- <input type=\"text\" class=\"form-control\" name=\"Sejumlah\" placeholder=\"\" ng-model=\"filter.unitCount\" maxlength=\"30\" required> --> <input type=\"number\" step=\"1\" pattern=\"[0-9]\" value=\"0\" min=\"0\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"filter.unitCount\" maxlength=\"10\"> </div> <bserrmsg field=\"Sejumlah\"></bserrmsg> </div> </div> </div> <div class=\"row\" style=\"margin-bottom: 20px\"> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"secondForm\"> <button ng-click=\"btnubahTerm()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:11%\">Simpan</button> <button ng-click=\"btnKembaliToMainForm()\" class=\"wbtn btn no-animate ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:11%\">Kembali</button> <div class=\"row\" style=\"padding-top: 15px\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Nama Prospect/Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"ubahTOPFleet.CustomerName\" maxlength=\"100\" ng-disabled=\"true\"> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Kategori Pelanggan</label> <bsselect id=\"comboBox\" name=\"Kategori\" ng-model=\"ubahTOPFleet.CustomerTypeId\" data=\"dataCusCategory\" item-text=\"CustomerTypeDesc\" item-value=\"CustomerTypeId\" on-select=\"CustomerTypeId\" placeholder=\"\" icon=\"fa fa-search\" ng-disabled=\"true\"></bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <!--<bscheckbox \r" +
    "\n" +
    "                ng-model=\"ubahTOPFleet.Afco\" \r" +
    "\n" +
    "                label=\"AFCO\" \r" +
    "\n" +
    "                true-value=\"true\" \r" +
    "\n" +
    "                false-value=\"false\">\r" +
    "\n" +
    "            </bscheckbox>--> <bsreqlabel>AFCO</bsreqlabel> <select ng-model=\"ubahTOPFleet.Afco\" class=\"form-control\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionsAfcoBuatSetStatusFleet\" placeholder=\"\"> <option label=\"-- Pilih --\" value=\"\" disabled></option> </select> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Status Fleet</label> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"Fleet\" ng-model=\"ubahTOPFleet.FleetId\" data=\"selectionItem\" item-text=\"CategoryFleetName\" item-value=\"FleetId\" on-select=\"FleetId\" placeholder=\"\" icon=\"fa fa-search\"></bsselect> </div> </div> </div> </div> <div align=\"right\"> <button ng-click=\"btnTambah()\" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-bottom:12px; width:11%\"> <i class=\"fa fa-plus\">&nbspTambah</i> </button> </div> <div class=\"ui modal tambahTOP\" role=\"dialog\"> <i class=\"close icon\"></i> <div class=\"header\">Term Of Payment</div> <div class=\"content\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>TOP ke</label> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" type=\"number\" step=\"1\" value=\"0\" min=\"1\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"ubahTOPFleet.TOPKe\"> </div> </div> <div class=\"form-group\"> <label>Persentase</label> <div class=\"input-icon-right\"> <i class=\"fa fa-percent\"></i> <input id=\"DaStatusFleetPersen\" ng-change=\"SetStatusFleetJagaPersen()\" type=\"number\" step=\"1\" value=\"0\" min=\"0\" max=\"100\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-change=\"SetStatusFleetCekPersen(ubahTOPFleet.Persentase)\" ng-model=\"ubahTOPFleet.Persentase\"> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Due Date (Hari)</label> <div class=\"input-icon-right\"> <!-- <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"ubahTOPFleet.Durasi\" maxlength=\"10\"> --> <!-- <bsdatepicker name=\"StartValidDate\" date-options=\"dateOptions\" ng-model=\"ubahTOPFleet.Durasi\">\r" +
    "\n" +
    "                                </bsdatepicker> --> <input type=\"number\" step=\"1\" value=\"0\" min=\"0\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"ubahTOPFleet.Durasi\"> </div> </div> </div> </div> </div> <div class=\"actions\"> <div class=\"wbtn btn ng-binding\" style=\"width:100px\" ng-click=\"btnBatal()\">Batal</div> <div class=\"rbtn btn ng-binding ng-scope\" style=\"width:100px\" ng-click=\"btnSimpan()\">Simpan</div> </div> </div> <div ui-grid=\"gridEditTOP\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-auto-resize></div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/settinganCetakan/settinganCetakan.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SettinganCetakanForm\" factory-name=\"SettinganCetakanFactoryMaster\" model=\"mSettinganCetakan\" model-id=\"PrintSettingId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SettinganCetakanForm\" form-title=\"SettinganCetakan\" modal-title=\"SettinganCetakan\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" hide-new-button=\"true\" mode=\"SettinganCetakanDaMode\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Cetakan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"PrintSettingName\" placeholder=\"Cetakan\" ng-model=\"mSettinganCetakan.PrintSettingName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PrintSettingName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Transaksi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input readonly type=\"text\" class=\"form-control\" name=\"PrintTransactionTypeName\" placeholder=\"Tipe Transaksi\" ng-model=\"mSettinganCetakan.PrintTransactionTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PrintTransactionTypeName\"></bserrmsg> </div> </div> <div class=\"col-md-12\"> Keterangan <hr> <div class=\"col-md-6\"> <bsreqlabel style=\"margin-top:10px\">Distribusi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea onkeypress=\"if(event.keyCode==13){return false;}\" id=\"SetinganCetakanDistribusiId\" maxlength=\"100\" type=\"text\" class=\"form-control\" name=\"Distribution\" placeholder=\"Distribution\" ng-model=\"mSettinganCetakan.Distribution\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> <div class=\"col-md-6\"> <button style=\"float: right;margin-top:10px;margin-bottom:10px\" ng-click=\"TambahSettinganCetakanTandaTanganChild()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Tambah </span><span class=\"ladda-spinner\"></span> </button> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Nama</th> <th>Jabatan</th> </tr> <tr ng-repeat=\"SettinganCetakanTandaTanganChild in mSettinganCetakan.ListPrintSignature track by $index\"> <td>{{SettinganCetakanTandaTanganChild.EmployeeName}}</td> <td>{{SettinganCetakanTandaTanganChild.LastManPowerPositionTypeName}}</td> <td ng-hide=\"SettinganCetakanDaMode=='view'\"><a ng-click=\"RemoveSettinganCetakanTandaTanganChild($index)\">Hapus</a></td> </tr> </tbody> </table> </div> </div> <div class=\"col-md-12\"> <bsreqlabel style=\"margin-top:10px\">Catatan</bsreqlabel> <div class=\"input-icon-right\"> <!-- <i class=\"fa fa-pencil\"></i> --> <textarea style=\"width:738.33px; height:180px; font-family: Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace; font-size: 10pt\" type=\"text\" class=\"form-control\" name=\"Notation\" maxlength=\"800\" placeholder=\"Notation\" ng-model=\"mSettinganCetakan.Notation\" rows=\"5\" required>\r" +
    "\n" +
    "\t\t\t</textarea></div> </div> </div>  <div style=\"height:400px\" class=\"ui modal ModalEmployeeBuatSettinganCetakan\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"BatalSettinganCetakanTandaTanganChild()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Nama Karyawan</h4> </div> <div class=\"modal-body\"> <!--<div style=\"display: block\" class=\"grid\" ui-grid=\"gridSettinganCetakanEmployee\" ui-grid-auto-resize></div>--> <div> <p><bsreqlabel>Nama Role</bsreqlabel></p> <p><bsselect ng-model=\"Da_ModalSettinganCetakanRoleId\" data=\"optionSettinganCetakanRole\" id=\"SettinganCetakanRoleIdDropDown\" item-text=\"LastManPowerPositionTypeName\" item-value=\"LastManPowerPositionTypeId\" on-select=\"Da_ModalSettinganCetakanEmployeeRole_Selected(selected)\" placeholder=\" \"> </bsselect> </p> </div> <div> <p><bsreqlabel>Nama Karyawan</bsreqlabel></p> <p><input alpha-numeric mask=\"Huruf\" maxlength=\"50\" id=\"SettinganCetakanEmployeeNameTxtbx\" class=\"form-control\" type=\"text\" ng-model=\"Da_ModalSettinganCetakanEmployeeName\"></p> </div> <div> </div> </div> <div class=\"modal-footer\"> <!--<button onclick=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"pilihSettignanCetakanEmployee()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Tambahkan</button> <button ng-click=\"BatalSettinganCetakanTandaTanganChild()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div></bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/siteAreaPelanggaranWilayah/siteAreaPelanggaranWilayah.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SiteAreaPelanggaranWilayahForm\" factory-name=\"SiteAreaPelanggaranWilayahFactory\" model=\"msitePelanggaranWilayah\" model-id=\"AreaPelanggaranWilayahId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SiteAreaPelanggaranWilayahForm\" form-title=\"Area Pelanggaran\" modal-title=\"siteAreaPelanggaranWilayah\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Area Pelanggaran</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama area pelanggaran\" placeholder=\"nama\" ng-model=\"msitePelanggaranWilayah.NamaAreaPelanggaranWilayah\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/siteAreaStock/siteAreaStock.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SiteAreaStockForm\" factory-name=\"SiteAreaStockFactory\" model=\"mSiteAreaStock\" model-id=\"AreaStockId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SiteAreaStockForm\" form-title=\"Area Stock\" modal-title=\"SiteAreastock\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Area Stock</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Area Stock\" ng-model=\"mSiteAreaStock.AreaStockName\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"SiteAreaStock\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/sitePDC/sitePDC.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SitePDCForm\" factory-name=\"SitePDCFactory\" model=\"msitePdc\" model-id=\"SitePDCId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SitePDCForm\" form-title=\"Site PDC\" modal-title=\"sitePds\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama PDC</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"name site pdc\" placeholder=\"nama\" ng-model=\"msitePdc.NameSitePDC\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"nama\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/sitePDS/sitePds.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SitePdsForm\" factory-name=\"SitePdsFactory\" model=\"msitePds\" model-id=\"PDSId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"sitePdsForm\" form-title=\"PDS\" modal-title=\"sitePds\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama PDS</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"name site pds\" placeholder=\"Nama PDS\" ng-model=\"msitePds.PDSName\" ng-maxlength=\"50\" alpha-numeric mask=\"Huruf\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"address\" placeholder=\"Alamat\" ng-model=\"msitePds.PDSAddress\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"address\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kota / Kabupaten</bsreqlabel> <bsselect name=\"cityRegency\" data=\"cityRegency\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" ng-model=\"msitePds.CityRegencyId\" placeholder=\"Kota / Kabupaten\" icon=\"fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"cityRegency\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Pos</bsreqlabel> <bsselect name=\"kodePos\" data=\"kelurahan\" item-text=\"PostalCode\" item-value=\"VillageId\" ng-model=\"msitePds.VillageId\" placeholder=\"Kode Pos\" icon=\"fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"kodePos\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/sitePlantAddress/sitePlantAddress.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SitePlantAddressForm\" factory-name=\"SitePlantAddressFactory\" model=\"mSitePlantAddress\" model-id=\"PlantId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SitePlantAddressForm\" form-title=\"Alamat Plant\" modal-title=\"SitePlantAddress\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Plant</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Plant\" ng-model=\"mSitePlantAddress.PlantName\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"SitePlantAddress\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Address</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Address\" placeholder=\"Address\" ng-model=\"mSitePlantAddress.PlantAddress\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kota / Kabupaten</bsreqlabel> <bsselect name=\"cityRegency\" data=\"cityRegency\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" ng-model=\"mSitePlantAddress.CityRegencyId\" placeholder=\"Kota / Kabupaten\" icon=\"fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"cityRegency\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Pos</bsreqlabel> <bsselect name=\"kodePos\" data=\"kelurahan\" item-text=\"PostalCode\" item-value=\"VillageId\" ng-model=\"mSitePlantAddress.VillageId\" placeholder=\"Kode Pos\" icon=\"fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"kodePos\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/soCancelReason/soCancelReason.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SoCancelReasonForm\" factory-name=\"SoCancelReason\" model=\"mSoCancelReason\" model-id=\"SOCancelReasonId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SoCancelReasonForm\" form-title=\"SoCancelReason\" modal-title=\"SoCancelReason\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>SO Cancel Reason</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"SOCancelReasonName\" placeholder=\"SO Cancel Reason\" ng-model=\"mSoCancelReason.SOCancelReasonName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"SOCancelReasonName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/soType/soType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SoTypeForm\" factory-name=\"SoType\" model=\"mSoType\" model-id=\"SOTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SoTypeForm\" form-title=\"So Type\" modal-title=\"So Type\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>SO Type</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"SOTypeName\" placeholder=\"SO Type\" ng-model=\"mSoType.SOTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"SOTypeName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/spkCancel/spkCancel.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SpkCancelForm\" factory-name=\"SpkCancel\" model=\"mSpkCancel\" model-id=\"SpkCancelId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SpkCancelForm\" form-title=\"SPK Cancel\" modal-title=\"SPK Cancel\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>SPK Cancel</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"SpkCancelName\" placeholder=\"SPK Cancel\" ng-model=\"mSpkCancel.SpkCancelName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"SpkCancelName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/spkLost/spkLost.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SpkLostForm\" factory-name=\"SpkLost\" model=\"mSpkLost\" model-id=\"SpkLostId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SpkLostForm\" form-title=\"SPK Lost\" modal-title=\"SPK Lost\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>SPK Lost</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"SpkLostName\" placeholder=\"SPK Lost\" ng-model=\"mSpkLost.SpkLostName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"SpkLostName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/stampCost/stampCost.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"StampCostForm\" factory-name=\"StampCost\" model=\"mStampCost\" model-id=\"StampCostId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"StampCostForm\" form-title=\"StampCost\" modal-title=\"StampCost\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Stamp Value</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"StampValue\" placeholder=\"Stamp Value\" ng-model=\"mStampCost.StampValue\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"StampValue\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Minimum Transaction</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"MinimumTransaction\" placeholder=\"Minimum Transaction\" ng-model=\"mStampCost.MinimumTransaction\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"MinimumTransaction\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Maximum Transaction</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"MaximumTransaction\" placeholder=\"Maximum Transaction\" ng-model=\"mStampCost.MaximumTransaction\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"MaximumTransaction\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Valid Date</bsreqlabel> <bsdatepicker name=\"ValidDate\" date-options=\"dateOptions\" ng-model=\"mStampCost.ValidDate\"> </bsdatepicker> <bserrmsg field=\"ValidDate\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/suku/suku.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SukuForm\" factory-name=\"MasterSukuFactory\" model=\"mSuku\" model-id=\"CustomerEthnicId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SukuForm\" form-title=\"Suku\" modal-title=\"Suku\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Suku</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"CustomerEthnicName\" placeholder=\"Input Suku\" ng-model=\"mSuku.CustomerEthnicName\" maxlength=\"20\" required> </div> <bserrmsg field=\"CustomerEthnicName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/sumberProspect/sumberProspect.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SumberProspectForm\" factory-name=\"SumberProspectFactory\" model=\"mSumberProspect\" model-id=\"ProspectSourceId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SumberProspectForm\" form-title=\"Sumber Prospect\" modal-title=\"Sumber Prospect\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Sumber Prospek</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ProspectSourceName\" placeholder=\"Sumber Prospect\" ng-model=\"mSumberProspect.ProspectSourceName\" maxlength=\"20\" required> </div> <bserrmsg field=\"ProspectSourceName\"></bserrmsg> </div> </div> </div> </bsform-grid> <!--<div class=\"row\" ng-show=\"ShowSumberProspectDetail\" style=\"min-height:100vh\">\r" +
    "\n" +
    "<button ng-disabled=\"mSumberProspect.ProspectSourceName==null||mSumberProspect.ProspectSourceName==undefined||mSumberProspect.ProspectSourceName==''\" style=\"float: right;margin-right:10px\" ng-show=\"ShowSimpanSumberProspect\" ng-click=\"SimpanSumberProspect()\"  type=\"button\" class=\"rbtn btn ng-binding ladda-button\" >\r" +
    "\n" +
    "\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\tSimpan\r" +
    "\n" +
    "\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t</button>\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\t<button style=\"float: right;margin-right:10px\" ng-click=\"KembaliDariSumberProspect()\"  type=\"button\" class=\"rbtn btn ng-binding ladda-button\" >\r" +
    "\n" +
    "\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\tKembali\r" +
    "\n" +
    "\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t</button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</div>-->"
  );


  $templateCache.put('app/sales/sales9/master/testDriveUnit/testDriveUnit.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"TestDriveUnitForm\" factory-name=\"TestDriveUnit\" model=\"mTestDriveUnit\" model-id=\"CustomerFleetId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"TestDriveUnitForm\" form-title=\"TestDriveUnitX\" modal-title=\"TestDriveUnit\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Police Number</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PoliceNumber\" placeholder=\"Police Number\" ng-model=\"mTestDriveUnit.PoliceNumber\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"PoliceNumber\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Model Type Id</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ModelTypeId\" placeholder=\"Model Type Id\" ng-model=\"mTestDriveUnit.ModelTypeId\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"ModelTypeId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Last Meter</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"LastMeter\" placeholder=\"Last Meter\" ng-model=\"mTestDriveUnit.LastMeter\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"LastMeter\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Last Start Date</bsreqlabel> <bsdatepicker name=\"LastStartDate\" date-options=\"dateOptions\" ng-model=\"mTestDriveUnit.LastStartDate\"> </bsdatepicker> <bserrmsg field=\"LastStartDate\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Last End Date</bsreqlabel> <bsdatepicker name=\"LastEndDate\" date-options=\"dateOptions\" ng-model=\"mTestDriveUnit.LastEndDate\"> </bsdatepicker> <bserrmsg field=\"LastEndDate\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Input User Id</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"InputUserId\" placeholder=\"Input User Id\" ng-model=\"mTestDriveUnit.InputUserId\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"InputUserId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Input Date</bsreqlabel> <bsdatepicker name=\"InputDate\" date-options=\"dateOptions\" ng-model=\"mTestDriveUnit.InputDate\"> </bsdatepicker> <bserrmsg field=\"InputDate\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/tipeRole/tipeRole.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"TipeRole\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"grid3\" ui-grid-selection ui-grid-auto-resize ui-grid-pagination></div> <br> </div> <div ng-show=\"ShowTipeRoleDetail\"> <button style=\"float: right\" ng-show=\"ShowSimpanTipeRole\" ng-click=\"SimpanTipeRole()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span> <span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliDariTipeRole()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span> <span class=\"ladda-spinner\"></span> </button> <bslabel style=\"margin-top:10px\">Permohonan Persetujuan</bslabel> <br> {{mTipeRole_ApprovalCategoryName}} <br> <br> <bsreqlabel style=\"margin-top:10px\">Role</bsreqlabel> <bsselect ng-disabled=\"!mTipeRole_Field_EnableDisable\" data=\"OptionsRoleJabatan\" item-text=\"Name\" item-value=\"Id\" ng-model=\"mTipeRole_Child_RoleId\" placeholder=\"Role\" style=\"min-width: 150px\"> </bsselect> <br> <br> <button ng-disabled=\"!mTipeRole_Field_EnableDisable\" style=\"float: right;margin-bottom:10px\" ng-click=\"TambahTipeRoleChild()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> <table class=\"table table-bordered table-responsive\"> <tbody> <tr> <th>Role</th> </tr> <tr ng-repeat=\"TipeRoleChild in TipeRoleChildData track by $index\"> <td>{{TipeRoleChild.RoleName}}</td> <td ng-if=\"OperationTipeRole!='Look'\"> <a ng-click=\"DeleteTipeRoleChild($index)\">Remove</a> </td> </tr> </tbody> </table> </div>"
  );


  $templateCache.put('app/sales/sales9/master/tipeTugas/tipeTugas.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"isEdit == false\" class=\"row\" style=\"margin: 0 0 0 0\"> <button type=\"button\" class=\"rbtn btn\" ng-click=\"Tambah()\" style=\"margin: -31px 0 0 8px; float: right\" tabindex=\"0\"> <i class=\"fa fa-fw fa-plus\"></i>&nbspTambah </button> <bsform-grid ng-form=\"TipeTugasForm\" factory-name=\"TipeTugasFactory\" model=\"mTipeTugas\" model-id=\"TaskTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"TipeTugasForm\" form-title=\"Tipe Tugas\" modal-title=\"Tipe Tugas\" grid-hide-action-column=\"true\" hide-new-button=\"true\" form-api=\"formApi\" action-button-caption=\"actButtonCaption\" modal-size=\"small\" on-bulk-delete=\"onBulkDeleteTipeTugas\" icon=\"fa fa-fw fa-child\"> </bsform-grid> </div> <div ng-show=\"isEdit == true\" class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"pull-right ng-scope\"> <div class=\"btn-group\" style=\"margin-top:-50px;margin-right:-5px\"> <button type=\"button\" class=\"wbtn btn\" ng-click=\"back()\" style=\"margin-right:3px\" tabindex=\"0\">Kembali</button> <button type=\"button\" ng-hide=\"mode == 'view'\" class=\"rbtn btn\" onclick=\"this.blur()\" ng-click=\"SimpanMasterTipeTugas()\" ng-disabled=\"TipeTugasForm.$invalid || loading == true\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span> <span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"row\" style=\"margin: 10px 0 0 0\"> <form name=\"TipeTugasForm\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Aktivitas</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input value=\"Customer/Prospect\" readonly type=\"text\" class=\"form-control\" placeholder=\"Tipe Aktivitas\" ng-maxlength=\"50\"> </div> </div> <div class=\"form-group\"> <bsreqlabel>Tipe Tugas</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"mode == 'view'\" type=\"text\" class=\"form-control\" name=\"TaskTypeName\" placeholder=\"Tipe Tugas\" ng-model=\"mTipeTugas.TaskTypeName\" maxlength=\"25\" required> </div> </div> </div> </form> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/tujuanPengiriman/tujuanPengiriman.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"TujuanPengirimanForm\" factory-name=\"TujuanPengirimanFactoryMaster\" model=\"mTujuanPengiriman\" model-id=\"TujuanPengirimanId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"TujuanPengirimanForm\" form-title=\"Tujuan Pengiriman\" modal-title=\"Tujuan Pengiriman\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tujuan Pengiriman</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TujuanPengirimanName\" placeholder=\"Tujuan Pengiriman\" ng-model=\"mTujuanPengiriman.TujuanPengirimanName\" maxlength=\"50\" required> </div> <bserrmsg field=\"TujuanPengirimanName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Keterangan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" maxlength=\"200\" title=\"Please fill out this field.\" class=\"form-control\" name=\"KeteranganTujuanPengiriman\" placeholder=\"Keterangan\" ng-model=\"mTujuanPengiriman.KeteranganTujuanPengiriman\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "\t\t\t\t\t</textarea></div> <bserrmsg field=\"KeteranganTujuanPengiriman\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/typeIncomingPayment/typeIncomingPayment.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"TypeIncomingPaymentForm\" factory-name=\"TypeIncomingPayment\" model=\"mTypeIncomingPayment\" model-id=\"IncomingPaymentTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"IncomingPaymentTypeForm\" form-title=\"Incoming Payment Type\" modal-title=\"Type Incoming Payment\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Incoming Payment</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"IncomingPaymentName\" placeholder=\"Incoming Payment\" ng-model=\"mTypeIncomingPayment.IncomingPaymentTypeName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"IncomingPaymentName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/typeOutgoingPayment/typeOutgoingPayment.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"TypeOutgoingPaymentForm\" factory-name=\"TypeOutgoingPayment\" model=\"mTypeOutgoingPayment\" model-id=\"OutgoingPaymentId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"TypeOutgoingPaymentForm\" form-title=\"TypeOutgoingPayment\" modal-title=\"Type Outgoing Payment\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Outgoing Payment</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"OutgoingPaymentName\" placeholder=\"Outgoing Payment\" ng-model=\"mTypeOutgoingPayment.OutgoingPaymentName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"OutgoingPaymentName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/typeRefund/typeRefund.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"TypeRefundForm\" factory-name=\"TypeRefund\" model=\"mTypeRefund\" model-id=\"TypeRefundId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"TypeRefundForm\" form-title=\"TypeRefund\" modal-title=\"Type Refund\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Type Refund</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TypeRefundName\" placeholder=\"Type Refund\" ng-model=\"mTypeRefund.TypeRefundName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"TypeRefundName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/typeTransactionPoint/typeTransactionPoint.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"TypeTransactionPointForm\" factory-name=\"TypeTransactionPoint\" model=\"mTypeTransactionPoint\" model-id=\"TypeTransactionPointId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"TypeTransactionPointForm\" form-title=\"TypeTransactionPoint\" modal-title=\"TypeTransactionPoint\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Type Transaction Point</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"TypeTransactionPointName\" placeholder=\"Type Transaction Point\" ng-model=\"mTypeTransactionPoint.TypeTransactionPointName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"TypeTransactionPointName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/unitAksesoris/UnitAksesoris.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"UnitAksesorisForm\" factory-name=\"UnitAksesorisFactory\" model=\"mUnitAksesoris\" model-id=\"AlasanDropProspekId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"UnitAksesorisForm\" form-title=\"Unit Aksesoris\" modal-title=\"Unit Aksesoris\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Aksesoris</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Aksesoris\" ng-model=\"mUnitAksesoris.NamaAlasanDropProspek\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"UnitAksesoris\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Stock</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Aksesoris\" ng-model=\"mUnitAksesoris.NamaAlasanDropProspek\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"UnitAksesoris\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Price</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Aksesoris\" ng-model=\"mUnitAksesoris.NamaAlasanDropProspek\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"UnitAksesoris\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>UOM Name</label> <bsselect name=\"uomname\" data=\"grid.data\" item-text=\"title\" item-value=\"id\" ng-model=\"mRole.pid\" placeholder=\"Select UOM\" icon=\"fa fa glyph-left\"> </bsselect> <bserrmsg field=\"uomname\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/unitBrand/unitBrand.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"UnitBrandForm\" factory-name=\"UnitBrandFactory\" model=\"mUnitBrand\" model-id=\"BrandId\" get-data=\"getData\" loading=\"loading\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"UnitBrandForm\" form-title=\"Brand\" modal-title=\"UnitBrand\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Merek</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"brandName\" placeholder=\"Input Merek\" ng-model=\"mUnitBrand.BrandName\" maxlength=\"25\" required> </div> <bserrmsg field=\"brandName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/unitChecklistPerlengkapanStandar/unitChecklistPerlengkapanStandar.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <!--<button class=\"ui icon inverted grey button ng-scope\" style=\"font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px\" onclick=\"this.blur()\" ng-if=\"!grid.appScope.gridHideActionColumn &amp;&amp; !row.groupHeader\" ng-click=\"grid.appScope.gridClickViewDetailHandler(row.entity)\" tabindex=\"0\"> <i class=\"fa fa-fw fa-lg fa-list-alt\"></i> </button>--> <!-- end ngIf: !grid.appScope.gridHideActionColumn && !row.groupHeader --> <!-- ngIf: grid.appScope.allowEdit && !grid.appScope.gridHideActionColumn && !grid.appScope.hideEditButton\r" +
    "\n" +
    "\t\t       && !row.groupHeader && !grid.appScope.defHideEditButtonFunc(row) --> <!--<button class=\"ui icon inverted grey button ng-scope\" style=\"font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px\" onclick=\"this.blur()\" ng-if=\"grid.appScope.allowEdit &amp;&amp; !grid.appScope.gridHideActionColumn &amp;&amp; !grid.appScope.hideEditButton\r" +
    "\n" +
    "\t\t       &amp;&amp; !row.groupHeader &amp;&amp; !grid.appScope.defHideEditButtonFunc(row)\" ng-click=\"grid.appScope.gridClickEditHandler(row.entity)\" tabindex=\"0\"> <i class=\"fa fa-fw fa-lg fa-pencil\"></i> </button> --> <!-- end ngIf: grid.appScope.allowEdit && !grid.appScope.gridHideActionColumn && !grid.appScope.hideEditButton && !row.groupHeader && !grid.appScope.defHideEditButtonFunc(row) --> <div ng-show=\"gridData\"> <button ng-hide=\"true\" type=\"button\" style=\"margin: -30px 0 0 10px; float:right\" class=\"rbtn btn ng-binding ladda-button\" onclick=\"this.blur()\" ng-click=\"tambahChecklist()\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-plus\"></i>&nbsp;Tambah</span><span class=\"ladda-spinner\"></span> </button> <bsform-grid ng-form=\"UnitChecklistPerlengkapanStandarForm\" factory-name=\"UnitChecklistPerlengkapanStandarFactory\" model=\"munitChecklistPerlengkapanStandar\" model-id=\"PrintOutTypeId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-bulk-delete=\"customDelete\" form-name=\"UnitChecklistPerlengkapanSatandarForm\" form-title=\"Checklist Perlengkapan\" hide-new-button=\"true\" grid-hide-action-column=\"true\" modal-title=\"UnitChecklistPerlengkapanStandar\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> </bsform-grid> </div> <div class=\"row\" style=\"margin: 0 0 0 0; min-height:100vh\" ng-show=\"formContent\"> <div class=\"btn-group\" style=\"margin-top:-30px;margin-right:0;float:right\"> <button type=\"button\" class=\"wbtn btn ng-binding\" ng-click=\"batal()\" style=\"margin-right:3px\" tabindex=\"0\">Kembali</button> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" onclick=\"this.blur()\" ng-click=\"updateSave()\" ng-disabled=\"UnitChecklistPerlengkapanSatandarForm.$invalid || munitChecklistPerlengkapanStandar.listDetail.length == 0\" ng-if=\"btnUpdate\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-check\"></i>&nbsp;Ubah</span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" class=\"rbtn btn ng-binding ladda-button\" onclick=\"this.blur()\" ng-click=\"Save()\" ng-disabled=\"UnitChecklistPerlengkapanSatandarForm.$invalid || munitChecklistPerlengkapanStandar.listDetail.length == 0\" ng-if=\"btnSave\" ladda=\"savingState\" data-style=\"expand-left\" tabindex=\"0\"> <span class=\"ladda-label\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Simpan</span><span class=\"ladda-spinner\"></span> </button> </div> <fieldset ng-disabled=\"viewMode\"> <div class=\"col-md-6\" style=\"margin: 10px 0 0 0\"> <form name=\"UnitChecklistPerlengkapanSatandarForm\"> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                    <bsreqlabel>Vehicle Model</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"model\" ng-disabled=\"ubah\" ng-change=\"filtertipe(munitChecklistPerlengkapanStandar.VehicleModelId)\" data=\"VehicleModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"munitChecklistPerlengkapanStandar.VehicleModelId\" placeholder=\"Select Model\" icon=\"fa fa glyph-left\" style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <bsreqlabel>Vehicle Type</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"type\" ng-disabled=\"ubah\" data=\"vehicleTypefilter\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"munitChecklistPerlengkapanStandar.VehicleTypeId\" placeholder=\"Select Type\" icon=\"fa fa glyph-left\" style=\"min-width: 150px;\" required>\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                </div> --> <div class=\"form-group\"> <bsreqlabel>Tipe Cetakan</bsreqlabel> <bsselect name=\"type\" ng-disabled=\"ubah\" data=\"PrintOutType\" item-text=\"PrintOutTypeName\" item-value=\"PrintOutTypeId\" ng-model=\"munitChecklistPerlengkapanStandar.PrintOutTypeId\" placeholder=\"Select Type\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </form> <div class=\"row\" ng-hide=\"viewMode == true\" style=\"margin: 0\"> <div class=\"col-md-8\" style=\"padding-left: 0 !important\"> <div class=\"form-group\"> <label>Nama Perlengkapan Standar</label> <div class=\"form-group\"> <bsselect name=\"sequence\" on-select=\"perlengkapan(selected)\" data=\"perlenkapan\" item-text=\"EquipmentStandardName\" item-value=\"EquipmentStandardId\" ng-model=\"EquipmentStandardId\" placeholder=\"Select Perlengkapan\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> </div> </div> <div style=\"float:right\"> <button style=\"margin: 23px 0 0 0\" class=\"btn rbtn\" ng-click=\"TambahChildUnitChecklistPerlengkapanStandar()\" ng-disabled=\"(EquipmentStandardId == null || EquipmentStandardId == undefined)\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;Tambah</button> </div> </div> <label ng-show=\"viewMode==true\">Daftar Perlengkapan</label> <table class=\"table table-bordered\"> <thead> <tr> <th>No *</th> <th>Nama Perlengkapan Standar *</th> <th>Action</th> </tr> </thead> <tr ng-repeat=\"data in munitChecklistPerlengkapanStandar.listDetail track by $index\"> <td style=\"display: none\">{{data.EquipmentStandardId}}</td> <td>{{$index+1}}</td><!--<td>{{data.SequenceNumber}}</td>--> <td>{{data.EquipmentStandardName}}</td> <td ng-hide=\"viewMode==true\"><u style=\"color: blue\" ng-disabled=\"viewMode == true\" ng-click=\"RemoveChildUnitChecklistPerlengkapanStandar(data)\">Hapus</u></td> </tr> </table> </div> </fieldset> </div>"
  );


  $templateCache.put('app/sales/sales9/master/unitPerlengkapanStandar/unitPerlengkapanStandar.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"UnitPerlengkapanStandarForm\" factory-name=\"UnitPerlengkapanStandarFactory\" model=\"munitPerlengkapanStandar\" model-id=\"EquipmentStandardId\" get-data=\"getData\" loading=\"loading\" form-api=\"formApi\" do-custom-save=\"doCustomSaveUnitPerlengkapanStandar\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"UnitPerlengkapanStandarForm\" form-title=\"Unit Perlengkapan\" modal-title=\"UnitPerlengkapanStandar\" on-bulk-delete=\"HapusUnitPerlengkapanStandar\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Perlengkapan Standar</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"unitPerlengkapan\" placeholder=\"Nama Perlengkapan\" ng-model=\"munitPerlengkapanStandar.EquipmentStandardName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"unitPerlengkapan\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Serial Number</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"SerialNumber\" placeholder=\"Serial Number\" ng-model=\"munitPerlengkapanStandar.SerialNumber\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"SerialNumber\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/unitPrice/unitPrice.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"UnitPriceForm\" factory-name=\"UnitPriceFactory\" model=\"mUnitPrice\" model-id=\"UnitPriceId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"UnitPriceForm\" form-title=\"Price\" modal-title=\"UnitPrice\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"model\" data=\"VehicleModel\" ng-change=\"filterType(mUnitPrice.VehicleModelId)\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"mUnitPrice.VehicleModelId\" placeholder=\"Select Model\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"model\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <bsselect name=\"type\" data=\"vehicleTypefilter\" ng-change=\"filterColor(mUnitPrice.VehicleTypeId)\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" ng-model=\"mUnitPrice.VehicleTypeId\" placeholder=\"Select Tipe\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"type\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Warna</bsreqlabel> <bsselect name=\"color\" data=\"vehicleTypecolorfilter\" item-text=\"ColorCode, ColorName\" item-value=\"VehicleTypeColorId\" ng-model=\"mUnitPrice.VehicleTypeColorId\" placeholder=\"Select Warna\" icon=\"fa fa glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"color\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Year</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"Year\" placeholder=\"Year\" ng-model=\"mUnitPrice.Year\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Year\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Price</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"price\" placeholder=\"Price\" ng-model=\"mUnitPrice.Price\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"price\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/unitUOM/unitUOM.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"UnitUomForm\" factory-name=\"UnitUOMFactory\" model=\"munitUom\" model-id=\"UomId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"UnitUomForm\" form-title=\"Satuan Unit\" modal-title=\"UnitUOM\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Satuan\" placeholder=\"Satuan\" ng-model=\"munitUom.UomName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Satuan\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/unitYear/unitYear.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"UnitYearForm\" factory-name=\"UnitYearFactory\" model=\"mUnitYear\" model-id=\"YearId\" get-data=\"getData\" loading=\"loading\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"UnitYearForm\" form-title=\"Tahun\" modal-title=\"Unit Year\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tahun</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> <input type=\"number\" class=\"form-control\" name=\"yearName\" placeholder=\"Tahun\" ng-model=\"mUnitYear.YearName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"yearName\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/vehicleModel/vehicleModel.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"VehicleModelForm\" factory-name=\"VehicleModel\" model=\"mVehicleModel\" model-id=\"VehicleModelId\" loading=\"loading\" show-advsearch=\"on\" get-data=\"getData\" selected-rows=\"xVehicleModel.selected\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\"> <bsform-advsearch> <div class=\"row\" style=\"margin-bottom:40px\"> <div class=\"col-md-6\"> <label>Dealer</label> <bsselect ng-model=\"filter.GroupDealerId\" tabindex=\"1\" data=\"optionsOutlet_Dealer\" item-text=\"GroupDealerName\" item-value=\"GroupDealerId\" placeholder=\"Pilih Dealer\" ng-change=\"MasterOutletSearchDealerBrubah()\" ng-disabled=\"MasterOutletDisableFilterDealer\" required></bsselect> </div> <div class=\"col-md-6\"> <label>Cabang</label> <bsselect ng-model=\"filter.OutletId\" tabindex=\"2\" data=\"optionsOutlet_Outlet\" item-text=\"OutletName\" item-value=\"OutletId\" placeholder=\"Pilih Cabang\" ng-disabled=\"MasterOutletDisableFilterOutlet\"></bsselect> </div> </div> </bsform-advsearch> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"code\" placeholder=\"Fleet Code\" ng-model=\"mVehicleModel.VehicleModelCode\" disabled> </div> <bserrmsg field=\"code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Model Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Fleet Name\" ng-model=\"mVehicleModel.VehicleModelName\" disabled> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Brand Name</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mVehicleModel.BrandName\" disabled>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"desc\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <bscheckbox ng-model=\"mVehicleModel.SpotOrder\" label=\"Spot Order\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/vehicleTypeColor/vehicleTypeColor.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"VehicleTypeColorForm\" factory-name=\"VehicleTypeColor\" model=\"mVehicleTypeColor\" model-id=\"VehicleTransactionId\" loading=\"loading\" on-show-detail=\"onShowDetail\" show-advsearch=\"on\" hide-new-button=\"true\" on-before-edit=\"onBeforeEdit\" get-data=\"getData\" selected-rows=\"xVehicleTypeColor.selected\" on-select-rows=\"onSelectRows\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model</label> <bsselect name=\"filterModel\" ng-model=\"filter.vehicleModelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"filter.productModel=selected\" placeholder=\"Pilih Model\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Tipe</label> <bsselect name=\"filterType\" ng-model=\"filter.vehicleTypeId\" data=\"filter.productModel.listVehicleType\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" on-select=\"filproductType(selected)\" placeholder=\"Pilih Tipe\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Warna</label> <bsselect name=\"filterColor\" ng-model=\"filter.colorId\" data=\"filter.productType.listColor\" item-text=\"ColorName, ColorCode\" item-value=\"ColorId\" placeholder=\"Pilih Warna\" icon=\"fa fa-car\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Start Order</label> <bsdatepicker name=\"StartPeriod\" date-options=\"startDateOptions\" ng-model=\"filter.startValidDate\" ng-change=\"startDateChange(dt)\"> </bsdatepicker> <!-- <bserrmsg field=\"StartPeriod\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>End Order</label> <bsdatepicker name=\"EndPeriod\" date-options=\"endDateOptions\" ng-model=\"filter.endValidDate\" min-date=\"filter.startValidDate\" ng-change=\"endDateChange(dt)\"> </bsdatepicker> <!-- <bserrmsg field=\"EndPeriod\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>Tipe Suffix(Cabang)</label> <bsselect name=\"suffixTypeId\" ng-model=\"filter.suffixTypeId\" data=\"suffixTypeData\" item-text=\"SuffixTypeName, SuffixTypeCode, SuffixTypeDescription\" item-value=\"SuffixTypeId\" placeholder=\"Pilih Tipe Suffix\" icon=\"fa fa-car\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Start SPK</label> <bsdatepicker name=\"StartPeriod\" date-options=\"startDateOptions\" ng-model=\"filter.startSPKDate\" ng-change=\"startDateChange(dt)\"> </bsdatepicker> <!-- <bserrmsg field=\"StartPeriod\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>End SPK</label> <bsdatepicker name=\"EndPeriod\" date-options=\"endDateOptions\" ng-model=\"filter.endSPKDate\" min-date=\"filter.startSPKDate\" ng-change=\"endDateChange(dt)\"> </bsdatepicker> <!-- <bserrmsg field=\"EndPeriod\"></bserrmsg> --> </div> </div> </div> <!-- <pre>\r" +
    "\n" +
    "                {{mRSSPFleet}}\r" +
    "\n" +
    "            </pre> --> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model</label> <bsselect name=\"filterModel\" ng-model=\"mVehicleTypeColor.VehicleModelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Pilih Model\" ng-disabled=\"true\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Tipe</label> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mEditView.Description\" disabled></input> --> <bsselect name=\"filterType\" ng-model=\"mVehicleTypeColor.VehicleTypeId\" data=\"typeData\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Tipe\" ng-disabled=\"true\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Warna</label> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mEditView.ColorName\" disabled></input> --> <bsselect name=\"filterColor\" ng-model=\"mVehicleTypeColor.ColorId\" data=\"colorData\" item-text=\"ColorName, ColorCode\" item-value=\"ColorId\" placeholder=\"Pilih Warna\" ng-disabled=\"true\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Katashiki</label> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mEditView.KatashikiCode\" disabled></input> --> <bsselect name=\"katashiki\" ng-model=\"mVehicleTypeColor.KatashikiCode\" data=\"katashikiData\" item-text=\"KatashikiCode\" item-value=\"KatashikiCode\" placeholder=\"Pilih Katashiki\" ng-disabled=\"true\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Tipe Suffix(Cabang)</label> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mEditView.SuffixCode\" disabled></input> --> <bsselect name=\"suffix\" ng-model=\"mVehicleTypeColor.SuffixCode\" data=\"suffixData\" item-text=\"SuffixCode\" item-value=\"SuffixCode\" placeholder=\"Pilih Tipe Suffix\" ng-disabled=\"true\" icon=\"fa fa-car\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Assembly Type</label> <bsselect name=\"assemblyType\" ng-model=\"mVehicleTypeColor.AssemblyType\" data=\"assemblyTypeData\" item-text=\"AssemblyType\" item-value=\"AssemblyType\" placeholder=\"Pilih Assembly Type\" ng-disabled=\"true\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Formula Hitung</label> <bsselect name=\"formula\" ng-model=\"mVehicleTypeColor.FormulaHitung\" data=\"formulaData\" item-text=\"FormulaHitung\" item-value=\"FormulaHitung\" placeholder=\"Pilih Formula\" ng-disabled=\"true\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Tipe Suffix</bsreqlabel> <bsselect name=\"suffixTypeId\" ng-model=\"mVehicleTypeColor.SuffixTypeId\" data=\"suffixTypeDataEdit\" item-text=\"SuffixTypeName, SuffixTypeCode, SuffixTypeDescription\" item-value=\"SuffixTypeId\" placeholder=\"Pilih Tipe Suffix\" icon=\"fa fa-car\" ng-disabled=\"disEl\" skip-disable=\"enbElement\" required> </bsselect> </div> <bscheckbox ng-model=\"mVehicleTypeColor.SpotOrder\" label=\"Slow Moving\" true-value=\"true\" false-value=\"false\" ng-disabled=\"true\"> </bscheckbox> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Start Order</bsreqlabel> <bsdatepicker name=\"startOrder\" date-options=\"startDateOptions\" ng-change=\"startOrderDateChange(mVehicleTypeColor.StartValidDate)\" ng-model=\"mVehicleTypeColor.StartValidDate\" ng-disabled=\"disEl\" skip-disable=\"enbElement\"> </bsdatepicker> <bserrmsg field=\"startOrder\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>End Order</bsreqlabel> <bsdatepicker name=\"endOrder\" date-options=\"endOrderDateOptions\" ng-model=\"mVehicleTypeColor.EndValidDate\" min-date=\"mVehicleTypeColor.StartValidDate\" ng-disabled=\"disEl\" skip-disable=\"enbElement\"> </bsdatepicker> <bserrmsg field=\"endOrder\"></bserrmsg> </div> <div class=\"form-group\"> <label>Start SPK</label> <bsdatepicker name=\"startSPK\" date-options=\"startSpkDateOptions\" ng-change=\"startSpkDateChange(mVehicleTypeColor.StartSPKDate)\" ng-model=\"mVehicleTypeColor.StartSPKDate\" ng-disabled=\"disEl\" skip-disable=\"enbElement\"> </bsdatepicker> <!-- <bserrmsg field=\"startSPK\"></bserrmsg> --> </div> <div class=\"form-group\"> <label>End SPK</label> <bsdatepicker name=\"endSPK\" date-options=\"endSpkDateOptions\" ng-model=\"mVehicleTypeColor.EndSPKDate\" min-date=\"mVehicleTypeColor.StartSPKDate\" ng-disabled=\"disEl\" skip-disable=\"enbElement\"> </bsdatepicker> <!-- <bserrmsg field=\"endSPK\"></bserrmsg> --> </div> </div> </div> <!-- <pre>\r" +
    "\n" +
    "        {{filter}}\r" +
    "\n" +
    "    </pre> --> <!-- <pre>\r" +
    "\n" +
    "        {{gridDetail.data}}\r" +
    "\n" +
    "    </pre> --> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/vendor/vendor.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"VendorForm\" factory-name=\"VendorFactory\" model=\"mVendor\" model-id=\"VendorId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"VendorForm\" form-title=\"Vendor\" modal-title=\"Vendor\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Name\" placeholder=\"Name\" ng-model=\"mVendor.Name\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>SPLD</bsreqlabel> <div class=\"input-icon-right\"> <label><input name=\"SPLD\" class=\"form-control\" type=\"checkbox\" ng-model=\"mVendor.SPLDBit\" style=\"float:left;width: 20px;height: 20px\"></label> </div> <bserrmsg field=\"SPLD\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bslabel>Tipe Vendor</bslabel> <div class=\"input-icon-right\"> <bsselect name=\"VendorType\" ng-model=\"mVendor.VendorTypeId\" data=\"optionsVendorType\" item-text=\"VendorTypeName\" item-value=\"VendorTypeId\" on-select=\"value(selected)\" placeholder=\"Select Vendor Type\" icon=\"fa fa-search\"> </bsselect> </div> <bserrmsg field=\"VendorType\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales9/master/vendorJasa/vendorJasa.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <script type=\"text/javascript\">function navigateTabVendorJasa(containerId, divId) {\r" +
    "\n" +
    "        $(\"#\" + containerId).children().each(function (n, i) {\r" +
    "\n" +
    "            var id = this.id;\r" +
    "\n" +
    "            console.log(id + ' - ' + divId);\r" +
    "\n" +
    "            if (id == divId) {\r" +
    "\n" +
    "                $('#' + divId).attr('class', 'tab-pane fade in active');\r" +
    "\n" +
    "            } else {\r" +
    "\n" +
    "                $('#' + id).attr('class', 'tab-pane fade');\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        });\r" +
    "\n" +
    "    }</script> <style type=\"text/css\"></style> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <div ng-show=\"ShowVendorJasaMain\"> <button style=\"float: right; margin-top: -31px; margin-left: 5px\" ng-click=\"TambahVendorJasa()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <i class=\"fa fa-plus\"></i> <span>&nbsp&nbspTambah</span> </button> <bsform-grid ng-form=\"VendorJasaForm\" factory-name=\"VendorJasaFactory\" model=\"mVendorJasa\" model-id=\"CustomerReligionId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"VendorJasaForm\" form-title=\"VendorJasa\" show-advsearch=\"on\" modal-title=\"Master VendorJasa\" modal-size=\"small\" form-api=\"formApi\" grid-hide-action-column=\"true\" hide-new-button=\"true\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <bslabel>Kode Vendor</bslabel> <input type=\"text\" class=\"form-control\" placeholder=\"Kode Vendor\" ng-model=\"filter.VendorJasaCode \"> </div> <div class=\"col-md-3\"> <bslabel>Nama Vendor</bslabel> <input type=\"text\" class=\"form-control\" placeholder=\"Nama Vendor\" ng-model=\"filter.VendorJasaName\"> </div> <div class=\"col-md-3\"> <bslabel>Bisnis Unit</bslabel> <bsselect style=\"margin-bottom: 10px\" ng-model=\"filter.BusinessUnitId\" data=\"OptionsVendorJasaBusinessUnit\" item-text=\"BusinessUnitName\" item-value=\"BusinessUnitId\" on-select=\"value(selected)\" placeholder=\"Bisnis Unit\" icon=\"fa fa-search\"> </bsselect> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"ShowVendorJasaDetail\"> <div class=\"row\" style=\"margin: 0 0 0 0\"> <button style=\"float: right\" ng-show=\"VendorJasaOPeration=='Insert' || VendorJasaOPeration=='Update'\" ng-disabled=\"VendorJasaHargaTidakValid>0 || gridVendorJasa_HargaBeli.data.length <=0 || gridVendorJasa_HargaBeli.data==null || gridVendorJasa_HargaBeli.data==undefined || gridVendorJasa_InformasiBank.data.length<=0 || gridVendorJasa_InformasiBank.data==null || gridVendorJasa_InformasiBank.data==undefined || gridVendorJasa_Outlet.data.length<=0 || gridVendorJasa_Outlet.data==undefined || gridVendorJasa_Outlet.data==null\" ng-click=\"SimpanVendorJasa()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span> <span class=\"ladda-spinner\"></span> </button> <button style=\"float: right\" ng-click=\"KembaliDariVendorJasaDetail()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span> <span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Kode Vendor</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration!='Insert'\" type=\"text\" class=\"form-control\" name=\"VendorCode\" placeholder=\"Kode Vendor\" ng-model=\"mVendorJasa.VendorCode\" required> </div> </div> </div> <div class=\"col-md-4\"> </div> <div class=\"col-md-4\"> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Nama Vendor</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" type=\"text\" class=\"form-control\" name=\"VendorName\" placeholder=\"Nama Vendor\" ng-model=\"mVendorJasa.Name\" required> </div> </div> </div> <div class=\"col-md-4\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect ng-disabled=\"VendorJasaOPeration=='Lihat'\" style=\"margin-top:5px\" ng-model=\"mVendorJasa.ProvinceId\" data=\"ProvinsiOption\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" on-select=\"VendorJasaProvinsiSelected(selected)\" placeholder=\"Pilih Provinsi\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-4\"> <bsreqlabel>Bisnis Unit</bsreqlabel> <bsselect ng-disabled=\"VendorJasaOPeration=='Lihat'\" style=\"margin-top:5px\" ng-model=\"mVendorJasa.BusinessUnitId\" data=\"OptionsVendorJasaBusinessUnit\" item-text=\"BusinessUnitName\" item-value=\"BusinessUnitId\" on-select=\"VendorJasaVendorJasaSelected(selected)\" placeholder=\"Pilih Bisnis Unit\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Alamat Kantor</bsreqlabel> <div class=\"input-icon-right\"> <textarea ng-disabled=\"VendorJasaOPeration=='Lihat'\" maxlength=\"255\" type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"mVendorJasa.Address\" rows=\"4\" cols=\"75\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <div class=\"col-md-4\"> <bsreqlabel>Kota</bsreqlabel> <bsselect ng-disabled=\"VendorJasaOPeration=='Lihat'\" style=\"margin-bottom: 10px\" ng-model=\"mVendorJasa.CityRegencyId\" data=\"VendorJasaKota\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" on-select=\"value(selected)\" placeholder=\"Pilih Kota\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-4\"> <bsreqlabel>Tipe Vendor</bsreqlabel> <bsselect ng-disabled=\"VendorJasaOPeration=='Lihat'\" style=\"margin-bottom: 10px\" ng-model=\"mVendorJasa.VendorTypeId\" data=\"TipeVendorOption\" item-text=\"Name\" item-value=\"VendorTypeId\" on-select=\"value(selected)\" placeholder=\"Pilih Tipe Vendor\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel>Nama NPWP</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" type=\"text\" class=\"form-control\" name=\"VendorName\" placeholder=\"Nama NPWP\" ng-model=\"mVendorJasa.NPWPName\" required> </div> </div> </div> <div class=\"col-md-4\"> <bslabel>NPWP</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" type=\"text\" class=\"form-control\" name=\"VendorName\" placeholder=\"NPWP\" ng-model=\"mVendorJasa.NPWP\" required> </div> </div> <div class=\"col-md-4\"> <bslabel>TOP</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" number-only type=\"text\" class=\"form-control\" name=\"VendorName\" placeholder=\"TOP\" ng-model=\"mVendorJasa.TermOfPayment\" required> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bslabel>Alamat NPWP</bslabel> <div class=\"input-icon-right\"> <textarea ng-disabled=\"VendorJasaOPeration=='Lihat'\" maxlength=\"255\" type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Alamat NPWP\" ng-model=\"mVendorJasa.NPWPAddress\" rows=\"4\" cols=\"75\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <div class=\"col-md-4\"> <bslabel>Kode Pos</bslabel> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Kode Pos\" ng-model=\"mVendorJasa.PostalCode\" required> </div> </div> <div class=\"col-md-4\"> Performance Evaluation <div class=\"switch\"> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" ng-model=\"mVendorJasa.PerformanceEvaluationBit\" id=\"VendorJasaDaPerformanceEvaluation\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"VendorJasaDaPerformanceEvaluation\"></label> </div>Ya </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Email</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Email\" ng-model=\"mVendorJasa.Email\" required> </div> </div> </div> <div class=\"col-md-4\"> <bslabel>Telepon</bslabel> <div style=\"margin-top:5px\" class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Telepon\" ng-model=\"mVendorJasa.Phone\" required> </div> </div> <div class=\"col-md-4\"> Booking Status <div class=\"switch\"> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" ng-model=\"mVendorJasa.BlockingStatusBit\" id=\"VendorJasaDaBookingStatus\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\"> <label for=\"VendorJasaDaBookingStatus\"></label> </div>Aktif </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>No HP Kontak Yang Dihubungi</bsreqlabel> <div class=\"input-icon-right\" style=\"margin-top:-5px\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" number-only type=\"text\" class=\"form-control\" placeholder=\"No HP Kontak Yang Dihubungi\" ng-model=\"mVendorJasa.CPTelephone\" required> </div> </div> </div> <div class=\"col-md-4\"> <bslabel>Fax</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" number-only type=\"text\" class=\"form-control\" placeholder=\"Fax\" ng-model=\"mVendorJasa.Fax\" required> </div> </div> <div class=\"col-md-4\"> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Kontak Yang Dihubungi</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Kontak Yang Dihubungi\" ng-model=\"mVendorJasa.ContactPerson\" required> </div> </div> </div> <div class=\"col-md-4\"> </div> <div class=\"col-md-4\"> </div> </div> <div class=\"col-md-12\"> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li id=\"VendorJasa_InformasiBank\" class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabVendorJasa('tabContainerMaintainVendorJasa','menu_VendorJasa_InformasiBank')\"><i class=\"fa fa-user\"></i>&nbsp Informasi Bank</a></li> <li id=\"VendorJasa_HargaBeli\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabVendorJasa('tabContainerMaintainVendorJasa','menu_VendorJasa_HargaBeli')\"><i class=\"fa fa-folder-open-o\"></i>&nbsp Harga Beli</a></li> <li id=\"VendorJasa_Outlet\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabVendorJasa('tabContainerMaintainVendorJasa','menu_VendorJasa_Outlet')\"><i class=\"fa fa-folder-open-o\"></i>&nbsp Outlet</a></li> </ul> <div id=\"tabContainerMaintainVendorJasa\" class=\"tab-content\" style=\"padding-top: 15px; border: none !important\"> <div id=\"menu_VendorJasa_InformasiBank\" class=\"tab-pane fade in active\"> <div style=\"margin-top:20px;margin-left:-25px\"> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <bsreqlabel>Nama Bank</bsreqlabel> <!--<div style=\"margin-bottom:10px\" class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Nama Bank\" ng-model=\"mVendorJasa.BankName\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t required\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</div>--> <bsselect style=\"margin-bottom: 10px\" ng-model=\"mVendorJasa.BankId\" ng-disabled=\"VendorJasaOPeration=='Lihat'\" data=\"OptionsVendorJasaBank\" item-text=\"BankName\" item-value=\"BankId\" on-select=\"value(selected)\" placeholder=\"Bank\" icon=\"fa fa-search\"> </bsselect> <bsreqlabel>No Rekening</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" ng-disabled=\"VendorJasaOPeration=='Lihat'\" class=\"form-control\" placeholder=\"No Rekening\" ng-model=\"mVendorJasa.AccountNumber\" required> </div> </div> <div class=\"col-md-4\"> <bsreqlabel>Rekening Atas Nama</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input ng-disabled=\"VendorJasaOPeration=='Lihat'\" type=\"text\" class=\"form-control\" placeholder=\"Atas Nama Bank\" ng-model=\"mVendorJasa.AccountName\" required> </div> </div> </div> <button ng-disabled=\"VendorJasaOPeration=='Lihat' || mVendorJasa.BankId==null || mVendorJasa.BankId==undefined || mVendorJasa.AccountNumber==null || mVendorJasa.AccountNumber==undefined || mVendorJasa.AccountNumber=='' || mVendorJasa.AccountName==null || mVendorJasa.AccountName=='' || mVendorJasa.AccountName==undefined\" ng-click=\"TambahVendorJasa_InformationBank()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-bottom:10px\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridVendorJasa_InformasiBank\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> </div> <div id=\"menu_VendorJasa_HargaBeli\" class=\"tab-pane fade\"> <button ng-hide=\"true\" ng-click=\"VendorJasa_GenerateHargaBeli()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right;margin-bottom:5px\"> <span class=\"ladda-label\"> Generate </span> <span class=\"ladda-spinner\"></span> </button> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <bslabel>Pricing Area Cluster</bslabel> <bsselect ng-model=\"mVendorJasa.PriceArea\" data=\"OptionsVendorJasaPricingArea\" item-text=\"PriceArea\" item-value=\"PriceArea\" on-select=\"value(selected)\" placeholder=\"Pricing Area Cluster\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-4\"> <bslabel>Model</bslabel> <bsselect ng-model=\"mVendorJasa.VehicleModelId\" data=\"OptionVendorJasaVehicleModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Pilih Model\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-4\"> <bslabel>Kode Jasa</bslabel> <bsselect ng-model=\"mVendorJasa.JasaId\" data=\"OptionsVendorJasa_Jasa\" item-text=\"JasaCode,JasaName\" item-value=\"JasaId\" placeholder=\"Kode Jasa\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div style=\"margin-top:10px\" class=\"col-md-12\" ng-show=\"VendorJasaOPeration=='Update'\"> <div class=\"col-md-4\"> <bsreqlabel>Tanggal Berlaku</bsreqlabel> <bsdatepicker date-options=\"dateOptionsStart\" ng-model=\"mVendorJasa.TanggalBerlaku\"> </bsdatepicker> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-11\"> </div> <div style=\"margin-top:10px;margin-bottom:10px\" class=\"col-md-1\"> <button ng-show=\"false\" ng-click=\"VendorJasa_SearchHargaBeli()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Search </span> <span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div class=\"col-md-2\"> <button ng-disabled=\"mVendorJasa.BusinessUnitId==null || mVendorJasa.BusinessUnitId==undefined || mVendorJasa.VendorCode==null || mVendorJasa.VendorCode==undefined || mVendorJasa.Name==null || mVendorJasa.Name==undefined || mVendorJasa.Address==null || mVendorJasa.Address==undefined || mVendorJasa.CityRegencyId==null || mVendorJasa.CityRegencyId==undefined || mVendorJasa.VendorTypeId==null || mVendorJasa.VendorTypeId==undefined || mVendorJasa.Email==null || mVendorJasa.Email==undefined || mVendorJasa.CPTelephone==null || mVendorJasa.CPTelephone==undefined || mVendorJasa.ContactPerson==null || mVendorJasa.ContactPerson==undefined || mVendorJasa.ProvinceId==null || mVendorJasa.ProvinceId==undefined \" ng-show=\"VendorJasaOPeration=='Insert'\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"VendorJasa_Git_Da_Template()\">Download Template</button> <button ng-show=\"VendorJasaOPeration=='Update' || VendorJasaOPeration=='Lihat'\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"VendorJasa_Git_Dari_Grid_HargaBeli()\">Download</button> </div> <div class=\"col-md-6\"> <div class=\"col-xs-12\"> <div class=\"col-xs-7\"> <div class=\"input-icon-right\" ng-hide=\"VendorJasaOPeration=='Lihat'\"> <!--<input type=\"file\" accept=\".xlsx\" id=\"UploadvendorJasa\" onchange=\"angular.element(this).scope().VendorJasaloadXLS(this)\">--> <label> <input type=\"text\" style=\"width:200px\" ng-disabled=\"true\" class=\"form-control\" ng-model=\"fileNameVendorJasa\" name=\"fieldVendorJasaHarga\" placeholder=\"\" ng-maxlength=\"50\"> </label> <label style=\"margin-right: 0px\" class=\"rbtn btn\" ng-disabled=\"VendorJasa_ExcelData.length > 0\"> <i class=\"fa fa-folder-open\"></i> <input type=\"file\" style=\"display:none\" accept=\".xlsx\" id=\"UploadvendorJasa\" onselect=\"test()\" ng-click=\"openDaVendorJasa()\" onchange=\"angular.element(this).scope().VendorJasaloadXLS(this)\"> </label> </div> </div> <div class=\"col-xs-3\"> <button ng-hide=\"VendorJasaOPeration=='Lihat'\" id=\"MainMasterVirtualAccount_Upload_Button\" ng-click=\"VendorJasa_Upload_Clicked()\" ng-disabled=\"MasterVendorJasaDisableUpload\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-right:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-xs-2\"> <button ng-hide=\"VendorJasaOPeration=='Lihat'\" class=\"\" ng-click=\"VendorJasaGridHargaRefresh()\" style=\"width:40px\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> </div> </div> </div> <div class=\"col-md-4\"> <div style=\"width:100%\"> <div style=\"float:left; width:33%;height: 100%;display :flex;align-items : center\"> <i style=\"color:green\" class=\"fa fa-2x fa-check-circle\" aria-hidden=\"true\"></i> <label>Valid &nbsp; : &nbsp; {{VendorJasaHargaValid}}</label> </div> <div style=\"float:left; width:33%;height: 100%;display :flex;align-items : center\"> <i style=\"color:red\" class=\"fa fa-2x fa-times-circle\" aria-hidden=\"true\"></i><label>Invalid &nbsp; : &nbsp; {{VendorJasaHargaTidakValid}}</label> </div> <div style=\"float:left; width:33%;height: 100%;display :flex;align-items : center\"> <label>Total:{{VendorJasaHargaDaTotal}}</label> </div> </div> </div> </div> <div class=\"col-md-12\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridVendorJasa_HargaBeli\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> </div> <div id=\"menu_VendorJasa_Outlet\" class=\"tab-pane fade\"> <div style=\"margin-top:20px\"> <button ng-disabled=\"VendorJasaOPeration=='Lihat'\" ng-click=\"TambahVendorJasa_Outlet()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Tambah </span> <span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" style=\"margin-top:20px\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"gridVendorJasa_Outlet\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> </div> </div> </div> </div> </div> <div class=\"ui modal ModalVendorJasa_Outlet\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"ModalVendorJasa_Outlet_Hilang()\" class=\"close\" data-dismiss=\"modal\">&times;</button> Outlet </div> <div class=\"modal-body\"> <div style=\"display: block\" class=\"grid\" ui-grid=\"Modal_gridVendorJasa_Outlet\" ui-grid-pagination ui-grid-pinning ui-grid-selection ui-grid-auto-resize></div> </div> <div class=\"modal-footer\"> <button ng-click=\"VendorJasaOutletPilih()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Pilih</button> <button ng-click=\"ModalVendorJasa_Outlet_Hilang()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div> <div class=\"ui modal VendorJasaFormModalUpload\" role=\"dialog\" style=\"height:200px; background-color:transparent; box-shadow: none !important\"> <div align=\"center\" class=\"list-group-item\" style=\"border: none !important;padding:10px 0px 10px 0px; background-color:transparent\"> <font color=\"white\"><i class=\"fa fa-spinner fa-spin fa-3x fa-lg\"></i> <b> Upload File.... </b> </font> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/virtualAccount/virtualAccount.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style></style> <div class=\"no-add\"> <div> <div class=\"row\"> <div class=\"col-md-12\" style=\"width:100%;margin-bottom:10px;margin-top:20px\"> <!--<div id=\"myGrid\" ui-grid=\"VirtualAccount_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:98%;height: 250px\"></div>--> <bsform-grid ng-form=\"VirtualAccountForm\" factory-name=\"VirtualAccountFactory\" model=\"mVirtualAccount\" model-id=\"VirtualAccountId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"VirtualAccountForm\" form-title=\"VirtualAccount\" modal-title=\"Master Virtual Account\" modal-size=\"small\" form-api=\"formApi\" mode=\"VirtualAccountDaMode\" do-custom-save=\"doCustomSave\" on-bulk-delete=\"DeleteManual\" icon=\"fa fa-fw fa-child\"> <bsform-above-grid> <div class=\"col-md-12\" style=\"width:100%;margin-bottom:30px\"> <div class=\"col-md-1\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-5\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"uploadVirtualAccount\" onchange=\"angular.element(this).scope().loadXLS(this)\"> </div> </div> <div class=\"col-md-1\"> <button ng-disabled=\"MasterVirtualAccountDisableUpload\" id=\"MainMasterVirtualAccount_Upload_Button\" ng-click=\"MainMasterVirtualAccount_Upload_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-5\"> <button id=\"MainMasterVirtualAccount_Download_Button\" ng-click=\"MainMasterVirtualAccount_Download_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Download </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </bsform-above-grid> <div class=\"row\"> <!--<div style=\"margin-top:-30px\" class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<button id=\"EditMasterVirtualAccount_Simpan_Button\"\r" +
    "\n" +
    "\t\t\t\tng-disabled=\"EditMasterVirtualAccount_No==''||EditMasterVirtualAccount_No==null||EditMasterVirtualAccount_No==undefined\"\r" +
    "\n" +
    "\t\t\t\tng-click=\"EditMasterVirtualAccount_Simpan_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\"\r" +
    "\n" +
    "\t\t\t\t ng-show=\"EditMasterVirtualAccount_Simpan_Show\">\r" +
    "\n" +
    "\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\tSimpan\r" +
    "\n" +
    "\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<button id=\"EditMasterVirtualAccount_Kembali_Button\" \r" +
    "\n" +
    "\t\t\t\tng-click=\"EditMasterVirtualAccount_Kembali_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right;margin-top:5px;margin-right:5px\">\r" +
    "\n" +
    "\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\tKembali\r" +
    "\n" +
    "\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t</div>--> <div class=\"col-md-12\" style=\"margin-top:20px\"> <div class=\"col-md-4\"> <bslabel>Virtual Account</bslabel> <div style=\"margin-top:10px\" class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"EditMasterVirtualAccountNo\" placeholder=\"\" ng-model=\"mVirtualAccount.VirtualAccountNo\" maxlength=\"20\"> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-4\"> <bslabel>Status Terpakai</bslabel> <div style=\"margin-top:10px\" class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"EditMasterVirtualAccountStatus\" placeholder=\"\" ng-model=\"mVirtualAccount.VAStatus\" ng-maxlength=\"50\" ng-disabled=\"VirtualAccountDaMode=='new'\"> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-4\"> <bslabel>No. SPK</bslabel> <div style=\"margin-top:10px\" class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"EditMasterVirtualAccountNoSPK\" placeholder=\"\" ng-model=\"mVirtualAccount.SPKNo\" ng-maxlength=\"50\" ng-disabled=\"VirtualAccountDaMode=='new'\"> </div> </div> </div> </div> </bsform-grid> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales9/master/warna/warna.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"WarnaForm\" factory-name=\"WarnaFactory\" model=\"mWarna\" model-id=\"VehicleTypeColorId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"WarnaForm\" form-title=\"Warna\" modal-title=\"Warna\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alasan Drop Prospect</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Alasan Drop Prospect\" ng-model=\"mWarna.ReasonDropProspectName\" rows=\"5\" cols=\"50\" required>\r" +
    "\n" +
    "                    </textarea></div> <bserrmsg field=\"NamaAlasanDropProspek\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );

}]);
