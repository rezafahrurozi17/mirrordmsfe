angular.module('templates_sysapp', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('_sys/role/role.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"RoleForm\" factory-name=\"Role\" model=\"mRole\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"RoleForm\" form-title=\"Role\" modal-title=\"Role\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Role Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Role Name\" ng-model=\"mRole.Name\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"title\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Description</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mRole.Description\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <label>Parent Role</label>\r" +
    "\n" +
    "                <bsselect\r" +
    "\n" +
    "                          name=\"parentRole\"\r" +
    "\n" +
    "                          data=\"grid.data\"\r" +
    "\n" +
    "                          item-text=\"title\"\r" +
    "\n" +
    "                          item-value=\"id\"\r" +
    "\n" +
    "                          ng-model=\"mRole.pid\"\r" +
    "\n" +
    "                          placeholder=\"Select Parent Role\"\r" +
    "\n" +
    "                          icon=\"fa fa-child glyph-left\"\r" +
    "\n" +
    "                          style=\"min-width: 150px;\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bserrmsg field=\"parentRole\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> </div> </div> </bsform-grid>"
  );


  $templateCache.put('_sys/role/role_manual.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div id=\"content\" style=\"padding:0\"> <div class=\"bs-navbar no-content-padding\"> <h2 class=\"page-title txt-color-blueDark\" style=\"width:auto\"> <i class=\"fa fa-fw fa-child\"></i> Role </h2> <div class=\"pull-right\"> <div class=\"btn-group\" style=\"margin-top:0px\"> <button class=\"zbtn btn btn-default\" tooltip=\"Add\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actNew()\"> <i class=\"fa fa-plus\"> </i> </button> <button class=\"zbtn btn btn-default\" tooltip=\"Edit\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actEdit()\" ng-disabled=\"selRole===null\"> <i class=\"fa fa-edit\"> </i> </button> <button class=\"zbtn btn btn-default\" tooltip=\"Delete\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actDel()\" ng-disabled=\"selRole===null\"> <i class=\"fa fa-remove\"></i> </button> </div> </div> </div> <div class=\"well clearfix\" style=\"padding:5px\"> <bsgrid grid-name=\"grid\" grid-api=\"gridApi\" selected-row=\"selRole\" loading=\"loading\"></bsgrid> </div> </div> <!-- modal --> <form name=\"RoleForm\" novalidate> <bsmodal id=\"modalRole\" title=\"Role\" mode=\"sm_mode\" visible model=\"mRole\" action=\"sm_act(mRole)\" action-cancel=\"doCancel(mRole)\" action-disabled=\"RoleForm.$invalid\" novalidate> <formly-form fields=\"vm.fields\" model=\"mRole\" form=\"vm.form\" novalidate> </formly-form> <input type=\"hidden\" name=\"id\" ng-model=\"mRole.id\"> </bsmodal> </form> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <label>Role Name</label>\r" +
    "\n" +
    "            <div class=\"input-icon-right\">\r" +
    "\n" +
    "                <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Title\" ng-model=\"mRole.title\" required>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <bserrmsg field=\"title\"></bserrmsg>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <label>Description</label>\r" +
    "\n" +
    "            <div class=\"input-icon-right\">\r" +
    "\n" +
    "                <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mRole.desc\" required>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <bserrmsg field=\"desc\"></bserrmsg>\r" +
    "\n" +
    "        </div> -->"
  );


  $templateCache.put('_sys/user/user.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.user-avatar {\r" +
    "\n" +
    "    width: 100px;\r" +
    "\n" +
    "    height: 100px;\r" +
    "\n" +
    "    max-width: 100px;\r" +
    "\n" +
    "    max-height: 100px;\r" +
    "\n" +
    "    -webkit-border-radius: 50%;\r" +
    "\n" +
    "    -moz-border-radius: 50%;\r" +
    "\n" +
    "    border-radius: 50%;\r" +
    "\n" +
    "    border: 5px solid rgba(255,255,255,0.5);\r" +
    "\n" +
    "}</style> <bsform-grid ng-form=\"UserForm\" factory-name=\"User\" model=\"mUser\" model-id=\"Id\" loading=\"loading\" selected-rows=\"selectedRows\" on-before-new-mode=\"onBeforeNewMode\" on-before-edit-mode=\"onBeforeEditMode\" on-before-delete-mode=\"onBeforeDeleteMode\" form-title=\"User\" modal-size=\"small\" icon=\"fa fa-fw fa-users\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>User ID</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-id-card-o\"></i> <input type=\"text\" class=\"form-control\" name=\"username\" placeholder=\"User ID\" ng-model=\"mUser.UserName\" ng-minlength=\"4\" ng-maxlength=\"15\" required disallow-space> </div> <bserrmsg field=\"username\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-id-badge\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Name\" ng-model=\"mUser.FullName\" required ng-minlength=\"4\" ng-maxlength=\"50\"> </div> <bserrmsg field=\"name\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>E-mail</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-at\"></i> <input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"Email Address\" required ng-model=\"mUser.Email\" ng-minlength=\"4\"> </div> <bserrmsg field=\"email\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Password </bsreqlabel> <!-- <span><a href=\"\" ng-click=\"doGeneratePass()\" tabindex=\"-1\">&nbsp;&nbsp;&nbsp;Generate</a></span> --> <div class=\"input-icon-right\"> <i class=\"fa fa-lock\"></i> <input type=\"text\" class=\"form-control\" id=\"txtPassword\" name=\"password\" placeholder=\"Password\" required ng-model=\"mUser.Password\" ng-minlength=\"7\" ng-maxlength=\"30\"> </div> <bserrmsg field=\"password\"></bserrmsg> </div> </div> </div> <!-- ng-required=\"formmode.mode !== 'del'\" --> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Phone</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" class=\"form-control\" name=\"phone1\" placeholder=\"Phone 1\" ng-model=\"mUser.Phone1\" ng-pattern=\"/^[+]*[0-9]+(\\-[0-9]+)*$/\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Phone</label> <div class=\"input-icon-right\"> <i class=\"fa fa-mobile\"></i> <input type=\"text\" class=\"form-control\" name=\"phone2\" placeholder=\"Phone 2\" ng-model=\"mUser.Phone2\" ng-pattern=\"/^[+]*[0-9]+(\\-[0-9]+)*$/\"> </div> </div> </div> </div> <div class=\"col-md-6\"> <label>User Profile</label> <div> <img class=\"user-avatar\" ngf-src=\"mUser.profile\" src=\"images/default_profile-icon.png\"> <input type=\"file\" ng-model=\"mUser.profile\" style=\"display:inline\" ngf-select=\"\" ngf-max-size=\"'200KB'\" ngf-accept=\"'image/*'\" ngf-capture=\"'camera'\"> </div> </div> </bsform-grid>"
  );


  $templateCache.put('_sys/user/userLoginLog.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"UserLoginLogForm\" factory-name=\"User\" model=\"mUser\" model-id=\"Id\" loading=\"loading\" selected-rows=\"selectedRows\" form-title=\"User\" modal-size=\"small\" icon=\"fa fa-fw fa-users\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>User ID</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"username\" placeholder=\"User ID\" ng-model=\"mUser.userID\"> </div> </div> <div class=\"form-group\"> <label>User Name</label> <div class=\"input-icon-right\"> <i class=\"glyphicon glyphicon-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"User Name\" ng-model=\"mUser.userName\"> </div> </div> <div class=\"form-group\"> <label>Dealer Code</label> <div class=\"input-icon-right\"> <i class=\"fa fa-users\"></i> <input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"Dealer Code\" ng-model=\"mUser.orgCode\"> </div> </div> <div class=\"form-group\"> <label>Dealer Name</label> <div class=\"input-icon-right\"> <i class=\"fa fa-users\"></i> <input type=\"text\" class=\"form-control\" id=\"txtPassword\" name=\"password\" placeholder=\"Dealer Name\" ng-model=\"mUser.orgName\"> </div> </div> <div class=\"form-group\"> <label>Role</label> <div class=\"input-icon-right\"> <i class=\"fa fa-child\"></i> <input type=\"email\" class=\"form-control\" name=\"address\" placeholder=\"Role\" ng-model=\"mUser.roleName\"> </div> </div> <div class=\"form-group\"> <label>Last Login</label> <div class=\"input-icon-right\"> <i class=\"fa fa-clock-o\"></i> <input type=\"text\" class=\"form-control\" name=\"phone\" placeholder=\"Last Login\" ng-model=\"mUser.updatedAt\"> </div> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('_sys/user/userOrgRole.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"UserOrgRoleForm\" factory-name=\"UserOrgRole\" model=\"mUser\" model-id=\"Id\" loading=\"loading\" selected-rows=\"selectedRows\" get-supporting-data=\"getSupportingData\" show-advsearch=\"on\" on-before-new-mode=\"onBeforeNewMode\" on-before-edit-mode=\"onBeforeEditMode\" on-before-delete-mode=\"onBeforeDeleteMode\" form-title=\"User\" modal-size=\"small\" icon=\"fa fa-fw fa-users\" mode=\"formMode\" form-api=\"formApi\" linksref=\"app.role\" linkview=\"role@app\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Organization</label> <bsselect-tree data=\"orgData\" ng-model=\"selOrg.selected\" tree-id=\"Id\" tree-text=\"Code\" tree-text2=\"Name\" tree-child=\"Child\" expand-level=\"3\" tree-control=\"ctlOrg\" on-select=\"selectOrg(selected)\" placeholder=\"All Organization...\" icon=\"fa fa-sitemap\"> </bsselect-tree> </div> <!-- <pre>{{selOrg.selected}}</pre> --> </div> </div> </bsform-advsearch> <fieldset style=\"border:1px solid #cecece; padding:10px\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Organization</bsreqlabel> <bsselect-tree name=\"org\" data=\"orgData2\" ng-model=\"mUser.OrgId\" tree-id=\"Id\" tree-text=\"Code\" tree-text2=\"Name\" tree-child=\"Child\" expand-level=\"3\" tree-control=\"ctlOrg\" on-select=\"selectOrg2(selected)\" placeholder=\"Organization...\" icon=\"fa fa-sitemap\" required> </bsselect-tree> <bserrmsg field=\"org\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Role</bsreqlabel> <bsselect name=\"roleselect\" ng-model=\"mUser.RoleId\" data=\"roleData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Select Role...\" on-select=\"selectRole(selected)\" required icon=\"fa fa-user\"> </bsselect> <bserrmsg field=\"roleselect\"></bserrmsg> </div> </div> </div> </fieldset> <fieldset style=\"border:1px solid #cecece; padding:10px;margin-top:5px\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"pull-right\"> <i ng-show=\"loadingUsers\" class=\"fa fa-fw fa-refresh fa-spin\"></i> <span ng-show=\"!loadingUsers && (noResults || !mUser.UserId)\" style=\"color:rgb(213,51,55)\"> <i class=\"fa fa-fw fa-user-plus\"></i>&nbsp;<label> New User ID</label></span> <span ng-show=\"!loadingUsers && !noResults && mUser.UserId\" style=\"color:#66afe9\"> <i class=\"fa fa-fw fa-user\"></i>&nbsp;<label> Existing User ID</label></span> </div> <div class=\"form-group\" show-errors> <bsreqlabel>User ID</bsreqlabel> <!--                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-id-card-o\"></i> --> <!--\r" +
    "\n" +
    "                              <input type=\"text\" class=\"form-control\" name=\"username\" placeholder=\"User ID\" ng-model=\"mUser.userName\"\r" +
    "\n" +
    "                               ng-minlength=\"4\" ng-maxlength=\"10\" required disallow-space\r" +
    "\n" +
    "                               uib-typeahead=\"user.userName for user in getUserByUserName($viewValue)\"\r" +
    "\n" +
    "                               uib-typeahead=\"state for state in states | filter:$viewValue\">\r" +
    "\n" +
    "                        --> <!--                         <input type=\"text\" class=\"form-control\" name=\"xusername\" placeholder=\"User ID\" ng-model=\"mUser.UserName\"\r" +
    "\n" +
    "                               ng-minlength=\"3\" ng-maxlength=\"10\" required disallow-space\r" +
    "\n" +
    "                               uib-typeahead=\"xuser.UserName for xuser in getUserByUserName($viewValue)\"\r" +
    "\n" +
    "                               typeahead-on-select=\"onSelectUser($item, $model, $label)\"\r" +
    "\n" +
    "                               ng-model-options=\"modelOptions\"\r" +
    "\n" +
    "                               typeahead-loading=\"loadingUsers\"\r" +
    "\n" +
    "                               typeahead-no-results=\"noResults\"\r" +
    "\n" +
    "                               typeahead-min-length=\"0\"\r" +
    "\n" +
    "                               typeahead-template-url=\"customTemplate.html\"\r" +
    "\n" +
    "                        > --> <!-- </div> --> <bstypeahead placeholder=\"User ID\" name=\"xusername\" ng-model=\"mUser.userName\" get-data=\"getUserByUserName\" model-key=\"UserName\" ng-minlength=\"4\" ng-maxlength=\"10\" required disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectUser\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" template-url=\"customTemplate.html\" ta-minlength=\"1\" icon=\"fa-id-card-o\"> </bstypeahead> <!-- <pre>{{mUser.userName}} {{loadingUsers}} {{noResults}}</pre> --> <bserrmsg field=\"xusername\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-id-badge\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Name\" ng-model=\"mUser.FullName\" ng-minlength=\"4\" ng-maxlength=\"50\" required ng-readonly=\"!newUserMode && !editMode\"> </div> <bserrmsg field=\"name\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>E-mail</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-at\"></i> <input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"Email Address\" ng-model=\"mUser.Email\" ng-minlength=\"4\" required ng-readonly=\"!newUserMode && !editMode\"> </div> <bserrmsg field=\"email\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Password</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-lock\"></i> <input type=\"password\" class=\"form-control\" id=\"txtPassword\" name=\"password\" placeholder=\"Password\" ng-required=\"newUserMode\" ng-readonly=\"!newUserMode && !editMode\" ng-model=\"mUser.Password\" ng-minlength=\"7\" ng-maxlength=\"30\"> </div> <bserrmsg field=\"password\"></bserrmsg> </div> </div> </div> <!-- ng-required=\"formmode.mode !== 'del'\" --> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Phone</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" class=\"form-control\" name=\"phone1\" placeholder=\"Phone 1\" ng-model=\"mUser.Phone1\" ng-readonly=\"!newUserMode && !editMode\" ng-pattern=\"/^[+]*[0-9]+(\\-[0-9]+)*$/\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Phone</label> <div class=\"input-icon-right\"> <i class=\"fa fa-mobile\"></i> <input type=\"text\" class=\"form-control\" name=\"phone2\" placeholder=\"Phone 2\" ng-model=\"mUser.Phone2\" ng-readonly=\"!newUserMode && !editMode\" ng-pattern=\"/^[+]*[0-9]+(\\-[0-9]+)*$/\"> </div> </div> </div> </div> </fieldset> <!--         <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Phone</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-phone\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"phone\" placeholder=\"Phone No\"\r" +
    "\n" +
    "                               ng-model=\"mUser.phone\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> </bsform-grid> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "      <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "      <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "  </a></script>"
  );


  $templateCache.put('_sys/user/user_bsform.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform ng-form=\"UserForm\" form-title=\"User List\" grid-name=\"grid\" field-def=\"fields\" factory-name=\"User\" get-data=\"getData()\" modal-title=\"User\" loading=\"loading\"> </bsform>"
  );


  $templateCache.put('_sys/user/user_manual.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div id=\"content\" style=\"padding:0\"> <div class=\"bs-navbar no-content-padding\"> <h2 class=\"page-title txt-color-blueDark\" style=\"width:auto\"> <i class=\"fa fa-fw fa-users\"></i> User List </h2> <div class=\"pull-right\"> <div class=\"btn-group\" style=\"margin-top:0px\"> <button class=\"zbtn btn btn-default\" tooltip=\"Add\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actNew()\"> <i class=\"fa fa-plus\"> </i> </button> <button class=\"zbtn btn btn-default\" tooltip=\"Edit\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actEdit()\" ng-disabled=\"selUser===null\"> <i class=\"fa fa-edit\"> </i> </button> <button class=\"zbtn btn btn-default\" tooltip=\"Delete\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actDel()\" ng-disabled=\"selUser===null\"> <i class=\"fa fa-remove\"></i> </button> </div> </div> </div> <div class=\"well clearfix\" style=\"padding:5px\"> <bsgrid grid-name=\"grid\" grid-api=\"gridApi\" selected-row=\"selUser\" loading=\"loading\"></bsgrid> </div> </div> <!-- modal --> <form name=\"UserForm\"> <bsmodal id=\"modalUser\" title=\"User\" mode=\"sm_mode\" visible model=\"mUser\" action=\"sm_act(mUser)\" action-cancel=\"doCancel(mUser)\" action-disabled=\"UserForm.$invalid\" size=\"large\" novalidate> <formly-form fields=\"vm.fields\" model=\"mUser\" form=\"vm.form\" novalidate> </formly-form> <input type=\"hidden\" name=\"id\" ng-model=\"mUser.id\"> </bsmodal> </form> <!--         <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <label>Name</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Name\" ng-model=\"mUser.name\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"name\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <label>Username</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-at\"></i>\r" +
    "\n" +
    "                        <input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"User Name/Email\"\r" +
    "\n" +
    "                               ng-model=\"mUser.email\"\r" +
    "\n" +
    "                               ng-minlength=\"4\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"email\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <label>Password (<a href=\"\" ng-click=\"doGeneratePass()\">Generate</a>)</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-lock\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" id=\"txtPassword\" name=\"password\" placeholder=\"Password\"\r" +
    "\n" +
    "                               ng-model=\"mUser.password\"\r" +
    "\n" +
    "                               ng-minlength=\"4\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"password\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Phone</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-phone\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"phone\" placeholder=\"Phone No\" ng-model=\"mUser.phone\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Address</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-building\"></i>\r" +
    "\n" +
    "                        <input type=\"email\" class=\"form-control\" name=\"address\" placeholder=\"Address\"\r" +
    "\n" +
    "                               ng-model=\"mUser.address\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> -->"
  );


  $templateCache.put('_sys/org/org.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid-tree ng-form=\"OrgForm\" factory-name=\"OrgChart\" model=\"mOrg\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"OrgForm\" form-title=\"Organization\" modal-title=\"Organization\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Organization Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-id-badge\"></i> <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Organization Code\" ng-model=\"mOrg.Code\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"title\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Organization Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-id-card-o\"></i> <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Organization Name\" ng-model=\"mOrg.Name\" ng-maxlength=\"100\" required> </div> <bserrmsg field=\"title\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Organization Type</bsreqlabel> <bsselect name=\"orgTypeSelect\" ng-model=\"mOrg.OrgTypeId\" data=\"dataOrgType\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Select Organization Type...\" required icon=\"fa fa-tree\"> </bsselect> <bserrmsg field=\"desc\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Parent Organization</label> <bsselect name=\"parentOrg\" data=\"grid.data\" item-text=\"Name\" item-value=\"Id\" ng-model=\"mOrg.ParentId\" placeholder=\"Select Parent Organization\" icon=\"fa fa-sitemap glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"parentOrg\"></bserrmsg> </div> </div> </div> </bsform-grid-tree>"
  );


  $templateCache.put('_sys/org/orgType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"OrgTypeForm\" factory-name=\"OrgType\" model=\"mOrgType\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"OrgTypeForm\" form-title=\"Organization Type\" modal-title=\"Organization Type\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Organization Code</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Organization Code\" ng-model=\"mOrgType.code\"\r" +
    "\n" +
    "                            ng-maxlength=\"30\" required\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"title\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Organization Type</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Organization Type\" ng-model=\"mOrgType.Name\" ng-maxlength=\"30\" required> </div> <bserrmsg field=\"title\"></bserrmsg> </div> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Description</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mOrg.description\"\r" +
    "\n" +
    "                            ng-maxlength=\"50\" required\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"desc\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <label>Parent Role</label>\r" +
    "\n" +
    "                <bsselect\r" +
    "\n" +
    "                          name=\"parentRole\"\r" +
    "\n" +
    "                          data=\"grid.data\"\r" +
    "\n" +
    "                          item-text=\"title\"\r" +
    "\n" +
    "                          item-value=\"id\"\r" +
    "\n" +
    "                          ng-model=\"mRole.pid\"\r" +
    "\n" +
    "                          placeholder=\"Select Parent Role\"\r" +
    "\n" +
    "                          icon=\"fa fa-child glyph-left\"\r" +
    "\n" +
    "                          style=\"min-width: 150px;\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bserrmsg field=\"parentRole\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> </div> </div> </bsform-grid>"
  );


  $templateCache.put('_sys/org/org_old.html',
    "<script type=\"text/javascript\">$('.ui.dropdown')\r" +
    "\n" +
    "    .dropdown();</script> <style type=\"text/css\">.ui-splitbar {\r" +
    "\n" +
    "    background-color: #e3e3e3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "tr:hover {\r" +
    "\n" +
    "    background-color: #818486;\r" +
    "\n" +
    "    color: white;\r" +
    "\n" +
    "    cursor: pointer;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".selectedRecord {\r" +
    "\n" +
    "    background-color: #00aeef;\r" +
    "\n" +
    "    color: white;\r" +
    "\n" +
    "    /*font-weight: bold;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-control button {\r" +
    "\n" +
    "    height: 10px;\r" +
    "\n" +
    "    margin-top: 10px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-control input {\r" +
    "\n" +
    "    height: 14px;\r" +
    "\n" +
    "    margin-top: 9px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-row-count-picker {\r" +
    "\n" +
    "    height: 14px;\r" +
    "\n" +
    "    margin-top: 7px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-count-container .ui-grid-pager-count {\r" +
    "\n" +
    "    margin-top: 8px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-control .ui-grid-pager-max-pages-number {\r" +
    "\n" +
    "    font-size: 12px;\r" +
    "\n" +
    "    vertical-align: sub;\r" +
    "\n" +
    "    padding: 5px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-nodata {\r" +
    "\n" +
    "    position: absolute;\r" +
    "\n" +
    "    top: 4rem;\r" +
    "\n" +
    "    opacity: 0.25;\r" +
    "\n" +
    "    font-size: 3em;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "    text-align: center;\r" +
    "\n" +
    "    z-index: 1000;\r" +
    "\n" +
    "}</style> <!-- <div class=\"ui grid\"> --> <ui-layout options=\"{flow:'column', dividerSize:2}\" style=\"top:85px\"> <div ui-layout-container min-size=\"0%\" max-size=\"50%\" size=\"30%\"> <div style=\"padding: 0.5em\"> <div class=\"ui fluid card\"> <div class=\"content\" style=\"background-color: whitesmoke\"> <div class=\"right floated meta\"> <div class=\"ui small blue basic icon buttons right floated\"> </div> </div> <div class=\"header\">Organization Chart</div> <div class=\"meta\">Add/Edit/Delete Organization Chart</div> </div> <div class=\"content\"> <!-- <div ng-if=\"orgChartData==null\"> --> <button class=\"ui basic button\" ng-click=\"addEditOrgModal()\"> <i class=\"add circle icon\"></i>Add New Organization </button> <!-- </div> --> <div data-angular-treeview=\"true\" data-tree-id=\"orgTree\" data-tree-model=\"orgChartData\" data-node-id=\"id\" data-node-label=\"title\" data-node-children=\"child\"> </div> </div> </div> </div> </div> <div ui-layout-container min-size=\"10%\" max-size=\"100%\" size=\"70%\"> <div class=\"eight wide column\"> <div style=\"padding: 0.5em\"> <div class=\"ui fluid card\" ng-show=\"selectedOrg\"> <div class=\"content\" style=\"background-color: whitesmoke\"> <div class=\"right floated meta\"> <div class=\"ui labeled selection dropdown left floated\" style=\"padding:5px 5px 5px\"> <div class=\"ui label\">Role</div> <input type=\"hidden\" name=\"role\"> <i class=\"dropdown icon\" style=\"padding-top:15px\"></i> <div class=\"default text\">Select Role</div> <div class=\"menu\"> <div class=\"item\" ng-repeat=\"role in roleList\" data-value=\"role.title\" ng-click=\"selectRole(role)\"> <i class=\"child icon\"></i> {{role.title}} </div> </div> </div> <div class=\"ui small blue basic icon buttons right floated\"> <button class=\"ui button\" tooltips tooltip-content=\"New \" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" ng-click=\"listUserModal()\" ng-disabled=\"!selectedRole\"> <i class=\"add square icon\"></i> </button> <button class=\"ui button\" tooltips tooltip-content=\"Edit\" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" ng-click=\"doEditUser()\" ng-disabled=\"gridApi.selection.getSelectedRows().length != 1\"> <i class=\"edit icon\"></i> </button> <button class=\"ui button\" tooltips tooltip-content=\"Delete\" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" ng-click=\"showModal_deleteUser=true\" ng-disabled=\"gridApi.selection.getSelectedRows().length < 1\"> <i class=\"remove icon\"></i> </button> </div> </div> <div class=\"header\">Users in Organization: {{selectedOrg.title}}</div> <div class=\"meta\">Add/Edit/Delete User in Organization</div> </div> <div class=\"ui raised segment\" ng-show=\"selectedRole\"> <!-- OrgUser list table --> <div id=\"grid1\" ui-grid=\"gridOptions\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight()\"> <div class=\"ui-grid-nodata\" ng-show=\"!gridOptions.data.length\">No data available</div> </div> </div> </div> </div> </div> </div>  </ui-layout> <!-- </div> --> <!-- modal --> <sm-modal id=\"addEditOrg\" class=\"modalAddEditOrg small\" visible settings=\"{closable: false,transition:'horizontal flip'}\"> <div class=\"header\" style=\"background-color: whitesmoke\"> <label>{{mOrgData.id==null?'New Organization Name':'Edit Organization Name'}}</label> </div> <div class=\"content\"> <form class=\"ui form\" name=\"myOrgForm\"> <div class=\"field required\"> <label>Title</label> <input type=\"text\" name=\"title\" placeholder=\"Organization Chart Title\" ng-model=\"mOrgData.title\" required> <div ng-messages=\"myOrgForm.title.$error\" ng-show=\"myOrgForm.title.$touched && myOrgForm.title.$invalid\" class=\"ui pointing label basic blue\"> <div ng-messages-include=\"common/error_msg.html\"></div> </div> </div> <div class=\"field\"> <label>Desc</label> <input type=\"text\" name=\"desc\" placeholder=\"Organization Chart Desc\" ng-model=\"mOrgData.desc\"> </div> <input type=\"hidden\" name=\"id\" ng-model=\"mOrgData.id\"> <input type=\"hidden\" name=\"parent\" ng-model=\"mOrgData.parent\"> </form> </div> <div class=\"actions\"> <div class=\"ui ok right labeled icon blue basic button\" ng-show=\"myOrgForm.title.$valid\" ng-click=\"updateOrg(mOrgData)\">Save<i class=\"checkmark icon\"></i> </div> <div class=\"ui cancel blue basic button\" ng-click=\"myOrgForm.$rollbackViewValue(); myOrgForm.$setPristine(); myOrgForm.$setUntouched();\">Cancel</div> </div> </sm-modal> <sm-modal id=\"deleteOrg\" class=\"modalDeleteOrg small\" visible settings=\"{closable: false,transition:'horizontal flip'}\"> <div class=\"header\" style=\"background-color: whitesmoke\">Delete Org Chart</div> <div class=\"content\"> <p>Delete Organization Name ( {{mOrgData.title}} ). Are you sure?</p> </div> <div class=\"actions\"> <div class=\"ui ok right labeled icon blue basic button\" ng-click=\"doDeleteOrg()\">Yes <i class=\"checkmark icon\"></i> </div> <div class=\"ui cancel blue basic button\">No</div> </div> </sm-modal> <sm-modal id=\"listAllUser\" class=\"modalListAllUser small\" visible settings=\"{closable: false,transition:'horizontal flip',blurring: true}\"> <div class=\"header\" style=\"background-color: whitesmoke\">Assign User or <a href=\"\" ng-click=\"doAddUser()\">Add new User</a></div> <div class=\"content\"> <div id=\"grid2\" ui-grid=\"gridOptionsUserList\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeightUserList()\"> </div> </div> <div class=\"actions\"> <div class=\"ui ok right labeled icon blue basic button\" ng-click=\"assignUser()\">Save <i class=\"checkmark icon\"></i> </div> <div class=\"ui cancel blue basic button\">Cancel</div> </div> </sm-modal> <sm-modal id=\"deleteUser\" class=\"modalDeleteUser small\" visible settings=\"{closable: false,transition:'horizontal flip',blurring: true}\"> <div class=\"header\" style=\"background-color: whitesmoke\">Delete User From Organization</div> <div class=\"content\"> <p> <span style=\"color:#FF0000\">{{gridApi.selection.getSelectedRows().length}} </span> user will be deleted from <span style=\"color:#FF0000\">{{selectedOrg.title}}</span> with role <span style=\"color:#FF0000\">{{selectedRole.title}}</span> </p> <div class=\"ui checkbox\"> <input type=\"checkbox\" ng-model=\"chkDelete\" ng-click=\"setChkDelete(chkDelete)\"> <label style=\"color:#1e70bf\">Delete From User Table</label> </div> <p><br>This action cannot be rollbacked. Are you sure?</p> </div> <div class=\"actions\"> <div class=\"ui ok right labeled icon blue basic button\" ng-click=\"removeUser()\">Yes <i class=\"checkmark icon\"></i> </div> <div class=\"ui cancel blue basic button\">No</div> </div> </sm-modal> <sm-modal id=\"addEditUser\" class=\"modalAddEditUser small\" visible settings=\"{closable: false,transition:'horizontal flip'}\"> <div class=\"header\" style=\"background-color: whitesmoke\"> <label>{{newUser.id==null?'New User':'Update User'}}</label> </div> <div class=\"content\"> <form class=\"ui form\" name=\"addEditUserForm\"> <div class=\"ui grid\"> <div class=\"eight wide column\"> <div ng-class=\"{ 'error': addEditUserForm.name.$touched && addEditUserForm.name.$invalid }\" class=\"field required\"> <label>Name</label> <div class=\"ui icon input\"> <input type=\"text\" name=\"name\" placeholder=\"Name\" ng-model=\"newUser.name\" required> <i class=\"user icon\"></i> </div> <div ng-messages=\"addEditUserForm.name.$error\" ng-show=\"addEditUserForm.name.$touched && addEditUserForm.name.$invalid\" class=\"ui pointing label basic blue\"> <div ng-messages-include=\"common/error_msg.html\"></div> </div> </div> </div> <div class=\"eight wide column\"> <div ng-class=\"{ 'error': addEditUserForm.email.$touched && addEditUserForm.email.$invalid }\" class=\"field required\"> <label>Username</label> <div class=\"ui icon input\"> <input type=\"email\" name=\"email\" placeholder=\"User Name/Email\" ng-model=\"newUser.email\" ng-minlength=\"4\" required> <i class=\"at icon\"></i> </div> <div ng-messages=\"addEditUserForm.email.$error\" ng-show=\"addEditUserForm.email.$touched && addEditUserForm.email.$invalid\" class=\"ui pointing label basic blue\"> <div ng-messages-include=\"common/error_msg.html\"></div> </div> </div> </div> <div class=\"eight wide column\"> <div ng-class=\"{ 'error': addEditUserForm.password.$touched && addEditUserForm.password.$invalid && newUser.id==null}\" class=\"field required\"> <label>Password (<a href=\"\" ng-click=\"doGeneratePass()\">Generate</a>)</label> <div class=\"ui icon input\"> <input type=\"text\" id=\"txtPassword\" name=\"password\" placeholder=\"Password\" ng-model=\"newUser.password\" ng-minlength=\"4\" required> <i class=\"lock icon\"></i> </div> <div ng-messages=\"addEditUserForm.password.$error\" ng-show=\"addEditUserForm.password.$touched && addEditUserForm.password.$invalid && newUser.id==null\" class=\"ui pointing label basic blue\"> <div ng-messages-include=\"common/error_msg.html\"></div> </div> </div> </div> <div class=\"eight wide column\"> <div class=\"field\"> <label>Phone</label> <div class=\"ui icon input\"> <input type=\"text\" name=\"phone\" placeholder=\"Phone\" ng-model=\"newUser.phone\"> <i class=\"phone icon\"></i> </div> </div> </div> <div class=\"sixteen wide column\"> <div class=\"field\"> <label>Address</label> <div class=\"ui icon input\"> <textarea rows=\"2\" name=\"address\" placeholder=\"Address\" ng-model=\"newUser.address\">\r" +
    "\n" +
    "                            <i class=\"building icon\"></i>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <input type=\"hidden\" name=\"id\" ng-model=\"newUser.id\">\r" +
    "\n" +
    "        </form>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"actions\">\r" +
    "\n" +
    "        <div ng-if=\"newUser.id==null\" class=\"ui ok right labeled icon blue basic button\" ng-show=\"addEditUserForm.name.$valid && addEditUserForm.email.$valid && addEditUserForm.password.$valid\" ng-click=\"doSaveUser(newUser)\">Save<i class=\"checkmark icon\"></i>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div ng-if=\"newUser.id!=null\" class=\"ui ok right labeled icon blue basic button\" ng-show=\"addEditUserForm.name.$valid && addEditUserForm.email.$valid\" ng-click=\"doSaveUser(newUser)\">Update<i class=\"checkmark icon\"></i>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"ui blue basic cancel button\" ng-click=\"addEditUserForm.$rollbackViewValue(); addEditUserForm.$setPristine(); addEditUserForm.$setUntouched(); doCancelUser()\">Cancel</div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</sm-modal>"
  );


  $templateCache.put('_sys/org/org_oldx.html',
    "<style type=\"text/css\"></style> <div id=\"content\" style=\"padding:0\"> <div id=\"layoutContainer_org\" style=\"position:relative;margin:0px;padding:0px;height:100%\"> <ui-layout options=\"{flow:'column', dividerSize:3, disableToggle:true}\"> <div ui-layout-container size=\"25%\"> <div class=\"inbox-side-bar\" style=\"width:100%\"> <div><label>ORGANIZATION CHART</label></div> <hr style=\"margin-top:10px;margin-bottom:5px\"> <div style=\"margin-top:0px;margin-bottom:5px\"> <div class=\"btn-group\"> <button class=\"btn wbtn\" tooltip=\"Add\" tooltip-placement=\"bottom\" ng-click=\"actNewOrg()\" ng-disabled=\"!selOrg || !selRole\"> <i class=\"fa fa-plus\"> </i> </button> <button class=\"btn wbtn\" tooltip=\"Edit\" tooltip-placement=\"bottom\" ng-click=\"actEditOrg()\" ng-disabled=\"!selOrg || !selRole\"> <i class=\"fa fa-edit\"> </i> </button> <button class=\"btn wbtn\" tooltip=\"Delete\" tooltip-placement=\"bottom\" ng-click=\"actDelOrg()\" ng-disabled=\"!selOrg || !selRole\"> <i class=\"fa fa-remove\"></i> </button> </div> </div> <abn-tree tree-data=\"orgData\" tree-control=\"ctlOrg\" icon-leaf=\"fa fa-fw fa-dot-circle-o\" icon-expand=\"glyphicon glyphicon-plus-sign\" icon-collapse=\"glyphicon glyphicon-minus-sign\" on-select=\"showSelOrg(branch)\"> </abn-tree> <!--                     <bsselect-tree title=\"Organization\" placeholder=\"Organization...\" icon=\"fa fa-sitemap\"\r" +
    "\n" +
    "                                   data=\"orgData\" on-select=\"showSelOrg(selected)\" tree-control=\"ctlOrg\" >\r" +
    "\n" +
    "                    </bsselect-tree> --> </div> </div> <div ui-layout-container> <div class=\"inbox-side-bar\" style=\"width:100%\"> <h6 style=\"padding:0px;margin:0px;font-size:1.4rem\">USER OF ORGANIZATION CHART <div class=\"pull-right\" style=\"margin-top:-0.2rem\" ng-hide=\"!selOrg\"> <label style=\"display:inline-block;vertical-align: middle;margin-top:-17px;margin-right:10px\">Role:</label> <bsselect data=\"roleData\" item-value=\"id\" item-text=\"title\" placeholder=\"Select Role...\" on-select=\"selectRole(selected)\" style=\"display:inline-block;min-width: 150px\"> </bsselect> <!--\r" +
    "\n" +
    "                                <ui-select ng-model=\"role.selected\" on-select=\"selectRole(role.selected)\" theme=\"select2\"\r" +
    "\n" +
    "                                                style=\"width:150px;display:inline-block;vertical-align:middle;margin-left:5px;\">\r" +
    "\n" +
    "                                        <ui-select-match placeholder=\"Select Role...\">{{$select.selected.title}}\r" +
    "\n" +
    "                                        </ui-select-match>\r" +
    "\n" +
    "                                        <ui-select-choices repeat=\"role in roleData | filter: $select.search\">\r" +
    "\n" +
    "                                            <span ng-bind-html=\"role.title | highlight: $select.search\"></span>\r" +
    "\n" +
    "                                        </ui-select-choices>\r" +
    "\n" +
    "                                </ui-select>\r" +
    "\n" +
    "                                 --> </div> </h6> <hr> <div class=\"ui icon info message no-animate\" ng-show=\"!selOrg && !selRole\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\"> Please select Organization to manage </div> </div> </div> <div class=\"ui icon info message no-animate\" ng-show=\"selOrg && !selRole\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\"> Please select Role to manage </div> </div> </div> <div class=\"ui fluid card\" ng-if=\"selOrg && selRole\"> <div class=\"content\" style=\"background-color: whitesmoke\"> <div class=\"right floated meta time\"> <div class=\"btn-group\"> <button class=\"zbtnsc btn btn-default\" tooltip=\"Add\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actAssignUser()\" ng-disabled=\"!selOrg || !selRole\"> <i class=\"fa fa-plus\"> </i> </button> <!--                                     <button class=\"zbtnsc btn btn-default\" tooltip=\"Edit\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actEditUser()\" ng-disabled=\"gridApiOrgUser.selection.getSelectedRows().length != 1\">\r" +
    "\n" +
    "                                        <i class=\"fa fa-edit\"> </i>\r" +
    "\n" +
    "                                    </button> --> <button class=\"zbtnsc btn btn-default\" tooltip=\"Delete\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actRemoveUser()\" ng-disabled=\"gridApiOrgUser.selection.getSelectedRows().length < 1\"> <i class=\"fa fa-remove\"></i> </button> </div> </div> <div class=\"header\" style=\"font-weight:300\">USER LIST</div> <div class=\"meta\">Organization: {{selOrg.title}} / Role: {{selRole.title}}</div> </div> <div id=\"grid1\" ui-grid=\"gridOrgUser\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight()\" ng-hide=\"!selRole\"> <div class=\"ui-grid-nodata\" ng-show=\"!gridOrgUser.data.length\">No data available</div> </div> </div> </div> </div> </ui-layout> </div> </div> <!-- modal --> <form name=\"OrgForm\" novalidate> <bsmodal id=\"modalOrg\" title=\"Organization Chart\" mode=\"sm_modeOrg\" visible model=\"mOrg\" action=\"sm_actOrg(mOrg)\" action-cancel=\"doCancelOrg()\" action-disabled=\"OrgForm.$invalid\" novalidate> <div class=\"form-group\" show-errors> <bsreqlabel>Title</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Organization Chart Title\" ng-model=\"mOrg.title\" ng-minlength=\"2\" required> <bserrmsg field=\"title\"></bserrmsg> </div> <div class=\"form-group\"> <label>Desc</label> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Organization Chart Desc\" ng-model=\"mOrg.desc\"> </div> <input type=\"hidden\" name=\"id\" ng-model=\"mOrg.id\"> <input type=\"hidden\" name=\"parent\" ng-model=\"mOrg.parent\"> </bsmodal> </form> <form name=\"UserListForm\"> <bsmodal id=\"modalUserList\" title=\"Assign/Create User In Organization\" visible action=\"doAssignUser()\" size=\"large\" action-disabled=\"gridApiUser.selection.getSelectedRows().length==0\" action-caption=\"Assign\" action-icon=\"user-plus\"> <div class=\"content\"> <!-- <p>Assign User or <a class=\"btn btn-default\" ng-click=\"actAddUser()\">Add new User</a></p> --> <p>Assign User</p> <div id=\"grid2\" ui-grid=\"gridUser\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeightUser()\"> </div> </div> </bsmodal> </form> <form name=\"RemoveUserForm\"> <bsmodal id=\"modalConfirmRemoveUser\" title=\"Remove User From Organization\" visible action=\"doRemoveUser()\" action-caption=\"Remove\" action-icon=\"user-times\"> <div class=\"content\"> <p> <span style=\"color:#FF0000\">{{gridApiOrgUser.selection.getSelectedRows().length}} </span> user(s) will be removed from <span style=\"color:#FF0000\">{{selOrg.title}}</span> with role <span style=\"color:#FF0000\">{{selRole.title}}</span> </p> <!--         <div class=\"checkbox\">\r" +
    "\n" +
    "            <label style=\"color:#1e70bf;\"><input type=\"checkbox\" ng-model=\"chkDelete\" ng-click=\"setChkDelete(chkDelete)\">\r" +
    "\n" +
    "            Delete From User Table</label>\r" +
    "\n" +
    "        </div> --> <p><br>This action cannot be rollbacked. Are you sure?</p> </div> </bsmodal> </form> <div ng-form name=\"UserForm\"> <bsmodal id=\"modalUser\" title=\"User\" mode=\"sm_modeUser\" visible model=\"mUser\" size=\"large\" action=\"sm_actUser(mUser)\" action-disabled=\"UserForm.$invalid\" action-cancel=\"doCancelUser()\" novalidate> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Name</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Name\" ng-model=\"mUser.name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Username</label> <div class=\"input-icon-right\"> <i class=\"fa fa-at\"></i> <input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"User Name/Email\" ng-model=\"mUser.email\" ng-minlength=\"4\" required> </div> <bserrmsg field=\"email\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Password (<a href=\"\" ng-click=\"doGeneratePass()\">Generate</a>)</label> <div class=\"input-icon-right\"> <i class=\"fa fa-lock\"></i> <input type=\"text\" class=\"form-control\" id=\"txtPassword\" name=\"password\" placeholder=\"Password\" ng-model=\"mUser.password\" ng-minlength=\"6\" required> </div> <bserrmsg field=\"password\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Phone</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" class=\"form-control\" name=\"phone\" placeholder=\"Phone No\" ng-model=\"mUser.phone\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Address</label> <div class=\"input-icon-right\"> <i class=\"fa fa-building\"></i> <input type=\"text\" class=\"form-control\" name=\"address\" placeholder=\"Address\" ng-model=\"mUser.address\"> </div> </div> </div> </div> <input type=\"hidden\" name=\"id\" ng-model=\"mUser.id\"> </bsmodal> </div>"
  );


  $templateCache.put('_sys/appl/appl.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform ng-form=\"ApplForm\" factory-name=\"Appl\" model=\"vm.model\" loading=\"loading\" selected-rows=\"selectedRow\" form-title=\"Application List\" modal-title=\"Application\" modal-size=\"small\" icon=\"fa fa-fw fa-gear\"> <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate> </formly-form> </bsform>"
  );


  $templateCache.put('_sys/service/appResourceList.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"AppResourceListForm\" factory-name=\"AppResource\" model=\"mService\" model-id=\"Id\" loading=\"loading\" selected-rows=\"selectedRows\" form-title=\"AppResource List\" modal-size=\"small\" icon=\"fa fa-fw fa-users\"> <!-- <bsform-advsearch>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "              <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                              <label>Organization</label>\r" +
    "\n" +
    "                              <bsselect-tree\r" +
    "\n" +
    "                                        data=\"orgData\"\r" +
    "\n" +
    "                                        ng-model=\"selOrg.selected\"\r" +
    "\n" +
    "                                        tree-id=\"id\"\r" +
    "\n" +
    "                                        tree-text=\"code\"\r" +
    "\n" +
    "                                        tree-text2=\"name\"\r" +
    "\n" +
    "                                        tree-child=\"child\"\r" +
    "\n" +
    "                                        expand-level=3\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                        tree-control=\"ctlOrg\"\r" +
    "\n" +
    "                                        on-select=\"selectOrg(selected)\"\r" +
    "\n" +
    "                                        placeholder=\"All Organization...\"\r" +
    "\n" +
    "                                        icon=\"fa fa-sitemap\"\r" +
    "\n" +
    "                              >\r" +
    "\n" +
    "                              </bsselect-tree>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </bsform-advsearch> --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>App Resource Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"App Resource Name\" ng-model=\"mService.Name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>HTTP Method</bsreqlabel>\r" +
    "\n" +
    "                <bsselect name=\"method\"\r" +
    "\n" +
    "                          ng-model=\"mService.Method\"\r" +
    "\n" +
    "                          data=\"[{code:'GET',name:'GET'},{code:'POST',name:'POST'},{code:'PUT',name:'PUT'}]\"\r" +
    "\n" +
    "                          item-text=\"name\"\r" +
    "\n" +
    "                          item-value=\"code\"\r" +
    "\n" +
    "                          placeholder=\"HTTP Method\"\r" +
    "\n" +
    "                          required=\"true\"\r" +
    "\n" +
    "                          icon=\"fa fa-exchange\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bserrmsg field=\"method\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Description</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mService.Description\"> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Action Name</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"actname\" placeholder=\"Action Name\" ng-model=\"mService.ActionName\" required\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"actname\"></bserrmsg>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <bscheckbox\r" +
    "\n" +
    "                        ng-model=\"mService.StatusCode\"\r" +
    "\n" +
    "                        name=\"chk1\"\r" +
    "\n" +
    "                        label=\"Enabled\"\r" +
    "\n" +
    "                        true-value = \"true\"\r" +
    "\n" +
    "                        false-value = \"false\" style=\"float:left;\">\r" +
    "\n" +
    "            </bscheckbox> --> </div> </div> </bsform-grid>"
  );


  $templateCache.put('_sys/service/serviceAccessMatrix.html',
    "<style type=\"text/css\"></style> <div id=\"content\" style=\"padding:0\"> <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px 15px 10px 7px\"> <!--         <h2 class=\"page-title txt-color-blueDark\" style=\"width:auto;\">\r" +
    "\n" +
    "            <i class=\"fa fa-fw fa-sitemap\"></i> Service Access Matrix\r" +
    "\n" +
    "        </h2> --> <!-- <bsbreadcrumb icon=\"fa fa-fw fa-sitemap\"></bsbreadcrumb> --> </div> <div id=\"layoutContainer_sam\" style=\"position:relative;margin:0px;padding:0px;height:100%\"> <ui-layout options=\"{flow:'column', dividerSize:2,disableToggle:true}\"> <div ui-layout-container size=\"25%\"> <div class=\"inbox-side-bar\" style=\"width:100%\"> <h6 style=\"padding:0px;margin:0px;font-size:1.4rem\">ORGANIZATION CHART </h6> <hr> <abn-tree tree-data=\"orgData\" tree-control=\"ctlOrg\" on-select=\"showSelOrg(branch)\"> </abn-tree> </div> </div> <div ui-layout-container size=\"75%\"> <div class=\"inbox-side-bar\" style=\"width:100%\"> <h6 style=\"padding:0px;margin:0px;font-size:1.4rem\">SERVICE ACCESS MATRIX <div class=\"pull-right\" style=\"margin-top:-0.2rem\" ng-hide=\"!selOrg\"> <label style=\"display:table-cell;vertical-align: middle\">Role:</label> <bsselect title=\"Role\" data=\"roleData\" item-key=\"title\" placeholder=\"Select Role...\" on-select=\"selectRole(selected)\"></bsselect> <!--                             <label>Role: </label>\r" +
    "\n" +
    "                            <ui-select ng-model=\"role.selected\" on-select=\"selectRole(role.selected)\" style=\"width:150px;display:inline-block; vertical-align:middle\">\r" +
    "\n" +
    "                                            <ui-select-match placeholder=\"Select Role...\">{{$select.selected.title}}</ui-select-match>\r" +
    "\n" +
    "                                            <ui-select-choices repeat=\"role in roleData | filter: $select.search\">\r" +
    "\n" +
    "                                                <i class=\"fa fa-child\"></i> <span ng-bind-html=\"role.title | highlight: $select.search\"></span>\r" +
    "\n" +
    "                                            </ui-select-choices>\r" +
    "\n" +
    "                            </ui-select> --> </div> </h6> <hr> <div class=\"ui icon info message no-animate\" ng-show=\"!selOrg && !selRole\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\"> Please select Organization to manage </div> </div> </div> <div class=\"ui icon info message no-animate\" ng-show=\"selOrg && !selRole\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\"> Please select Role to manage </div> </div> </div> <div class=\"ui fluid card\" ng-if=\"selOrg && selRole\"> <div class=\"content\" style=\"background-color: whitesmoke\"> <div class=\"right floated meta time\"> <div class=\"btn-group\"> <button class=\"zbtnsc btn btn-default\" tooltip=\"Add\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actAssign()\" ng-disabled=\"!selOrg || !selRole\"> <i class=\"fa fa-plus\"> </i> </button> <button class=\"zbtnsc btn btn-default\" tooltip=\"Edit\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actEdit()\" ng-disabled=\"gridApiServiceRights.selection.getSelectedRows().length != 1\"> <i class=\"fa fa-edit\"> </i> </button> <button class=\"zbtnsc btn btn-default\" tooltip=\"Delete\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actRemove()\" ng-disabled=\"gridApiServiceRights.selection.getSelectedRows().length < 1\"> <i class=\"fa fa-remove\"></i> </button> </div> </div> <div class=\"header\" style=\"font-weight:300\">SERVICE LIST</div> <div class=\"meta\">Organization: {{selOrg.title}} / Role: {{selRole.title}}</div> </div> <div id=\"grid1\" ui-grid=\"gridServiceRights\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight()\" ng-hide=\"!selRole\"> <div class=\"ui-grid-nodata\" ng-show=\"!gridServiceRights.data.length\">No data available</div> </div> </div> </div> </div> </ui-layout> </div> </div> <!-- modal --> <form name=\"ServiceListForm\"> <bsmodal id=\"modalServiceList\" title=\"Add Service In Organization\" visible action=\"doAssign()\" size=\"large\" action-disabled=\"gridApiService.selection.getSelectedRows().length==0\" action-caption=\"Assign\" action-icon=\"user-plus\"> <div class=\"content\"> <div id=\"grid2\" ui-grid=\"gridService\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeightService()\"> </div> </div> </bsmodal> </form> <form name=\"RemoveServiceForm\"> <bsmodal id=\"modalConfirmRemoveService\" title=\"Remove Service From Organization\" visible action=\"doRemove()\" action-caption=\"Remove\" action-icon=\"user-times\"> <div class=\"content\"> <p> <span style=\"color:#FF0000\">{{gridApiServiceRights.selection.getSelectedRows().length}} </span> service(s) will be deleted from <span style=\"color:#FF0000\">{{selOrg.title}}</span> with role <span style=\"color:#FF0000\">{{selRole.title}}</span> </p> </div> </bsmodal> </form>"
  );


  $templateCache.put('_sys/service/serviceList.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"SvcListForm\" factory-name=\"ServiceList\" model=\"mService\" model-id=\"Id\" loading=\"loading\" selected-rows=\"selectedRows\" form-title=\"Service List\" modal-size=\"small\" icon=\"fa fa-fw fa-users\"> <!-- <bsform-advsearch>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "              <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                              <label>Organization</label>\r" +
    "\n" +
    "                              <bsselect-tree\r" +
    "\n" +
    "                                        data=\"orgData\"\r" +
    "\n" +
    "                                        ng-model=\"selOrg.selected\"\r" +
    "\n" +
    "                                        tree-id=\"id\"\r" +
    "\n" +
    "                                        tree-text=\"code\"\r" +
    "\n" +
    "                                        tree-text2=\"name\"\r" +
    "\n" +
    "                                        tree-child=\"child\"\r" +
    "\n" +
    "                                        expand-level=3\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                        tree-control=\"ctlOrg\"\r" +
    "\n" +
    "                                        on-select=\"selectOrg(selected)\"\r" +
    "\n" +
    "                                        placeholder=\"All Organization...\"\r" +
    "\n" +
    "                                        icon=\"fa fa-sitemap\"\r" +
    "\n" +
    "                              >\r" +
    "\n" +
    "                              </bsselect-tree>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </bsform-advsearch> --> <div class=\"row\"> <div class=\"col-md-6\"> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Method</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"method\" placeholder=\"Method\" ng-model=\"mService.Method\"\r" +
    "\n" +
    "                           capitalize disallow-space required\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"method\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>HTTP Method</bsreqlabel> <bsselect name=\"method\" ng-model=\"mService.Method\" data=\"[{code:'GET',name:'GET'},{code:'POST',name:'POST'},{code:'PUT',name:'PUT'}]\" item-text=\"name\" item-value=\"code\" placeholder=\"HTTP Method\" required icon=\"fa fa-exchange\"> </bsselect> <bserrmsg field=\"method\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Controller Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"ctrlname\" placeholder=\"Controller Name\" ng-model=\"mService.ControllerName\" required> </div> <bserrmsg field=\"ctrlname\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Action Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"actname\" placeholder=\"Action Name\" ng-model=\"mService.ActionName\" required> </div> <bserrmsg field=\"actname\"></bserrmsg> </div> <bscheckbox ng-model=\"mService.StatusCode\" name=\"chk1\" label=\"Enabled\" true-value=\"true\" false-value=\"false\" style=\"float:left\"> </bscheckbox> </div> </div> </bsform-grid>"
  );


  $templateCache.put('_sys/files/download.html',
    "<style>.thumb {\r" +
    "\n" +
    "    width: 24px;\r" +
    "\n" +
    "    height: 24px;\r" +
    "\n" +
    "    float: none;\r" +
    "\n" +
    "    position: relative;\r" +
    "\n" +
    "    top: 7px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "form .progress {\r" +
    "\n" +
    "    line-height: 15px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".progress {\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    width: 100px;\r" +
    "\n" +
    "    border: 3px groove #CCC;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".progress div {\r" +
    "\n" +
    "    font-size: smaller;\r" +
    "\n" +
    "    background: orange;\r" +
    "\n" +
    "    width: 0;\r" +
    "\n" +
    "}</style> <div class=\"ui grid\"> <div class=\"row\"> <div class=\"column padding-reset\"> <div ng-controller=\"DownloadController\"> <div class=\"ui segment\" style=\"background-color: #f0f0f0;margin-bottom: 0\"> <button class=\"ui inverted blue button\">New</button> <button class=\"ui inverted blue button\">Edit</button> <button class=\"ui inverted blue button\">Delete</button> </div> <button type=\"file\" ng-model=\"picFile\" ngf-select=\"upload($file)\" ngf-max-size=\"500MB\">Upload on file select</button> File: <div style=\"font:smaller\">{{f.name}} size:{{f.size}} {{f.$error}} {{f.$errorParam}} location: {{f.result.file[0].fd}} <span class=\"progress\" ng-show=\"f.progress >= 0\"> <div style=\"width:{{f.progress}}%\" ng-bind=\"f.progress + '%'\"></div> </span> </div> {{errorMsg}} Image thumbnail: <img ngf-src=\"picFile\"> Audio preview: <audio controls ngf-src=\"picFile\"></audio> Video preview: <video controls ngf-src=\"picFile\"></video> </div> </div> </div> </div>"
  );


  $templateCache.put('_sys/files/upload.html',
    "<style>.thumb {\r" +
    "\n" +
    "    width: 24px;\r" +
    "\n" +
    "    height: 24px;\r" +
    "\n" +
    "    float: none;\r" +
    "\n" +
    "    position: relative;\r" +
    "\n" +
    "    top: 7px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "form .progress {\r" +
    "\n" +
    "    line-height: 15px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".progress {\r" +
    "\n" +
    "    display: inline-block;\r" +
    "\n" +
    "    width: 100px;\r" +
    "\n" +
    "    border: 3px groove #CCC;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".progress div {\r" +
    "\n" +
    "    font-size: smaller;\r" +
    "\n" +
    "    background: orange;\r" +
    "\n" +
    "    width: 0;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    ".embed-container \r" +
    "\n" +
    "{ position: relative; padding-bottom: 50%; height: 0; overflow: hidden; max-width: 100%; }\r" +
    "\n" +
    ".embed-container video, .embed-container object, .embed-container embed \r" +
    "\n" +
    "{ position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style> <div class=\"ui grid\"> <div class=\"row\"> <div class=\"column padding-reset\"> <div class=\"ui segment\" style=\"background-color: #f0f0f0;margin-bottom: 0\"> <button class=\"ui inverted blue button\">New</button> <button class=\"ui inverted blue button\">Edit</button> <button class=\"ui inverted blue button\">Delete</button> </div> <div class=\"ui three column grid\" style=\"margin:5px\"> <div class=\"column\" ng-repeat=\"fn in cFiles\"> <div class=\"ui fluid card\"> <div class=\"content\"> <div class=\"meta\">{{fn.f.name}} <span ng-show=\"fn.f.size>0\">({{fn.f.sizeKB}} KB)</span></div> <!-- {{fn.f.type}} --> <div class=\"header\"> </div> <div class=\"metadata\"> <form name=\"frm\" novalidate ng-class=\"{ 'error': frm.title.$touched}\"> <md-input-container style=\"padding-bottom: 17px\"> <label>Title</label> <input name=\"title\" required ng-model=\"fn.f.title\"> <div ng-if=\"frm.title.$touched\" ng-messages=\"frm.title.$error\"> <div ng-message=\"required\">This is required.</div> </div> </md-input-container> <md-input-container style=\"padding-bottom: 17px\"> <label>Desc</label> <textarea name=\"desc\" required ng-model=\"fn.f.desc\">\r" +
    "\n" +
    "                                        <div ng-if=\"frm.desc.$touched\" ng-messages=\"frm.desc.$error\">\r" +
    "\n" +
    "                                            <div ng-message=\"required\">This is required.</div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </md-input-container>\r" +
    "\n" +
    "                                </form>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"ui raised segment\" ng-show=\"fn.f.errorMsg!='' && fn.f.errorMsg!=null\">\r" +
    "\n" +
    "                                <div class=\"ui red ribbon label\">Error</div>\r" +
    "\n" +
    "                                <span>{{fn.f.errorMsg}}\r" +
    "\n" +
    "                                </span>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"image\">\r" +
    "\n" +
    "                            <img ngf-src=\"fn.f\">\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"embed-container\" ng-show=\"fn.f.type == 'video/mp4'\">\r" +
    "\n" +
    "                            <!-- <video controls ngf-src=\"picFile\"></video> -->\r" +
    "\n" +
    "                            <!-- <iframe src='http://www.youtube.com/embed/QILiHiTD3uc' frameborder='0' allowfullscreen></iframe> -->\r" +
    "\n" +
    "                            <!-- <iframe ngf-src=\"picFile\" frameborder='0' allowfullscreen></iframe> -->\r" +
    "\n" +
    "                            <video controls ngf-src=\"fn.f\"></video>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <audio controls ngf-src=\"fn.f\"></audio>\r" +
    "\n" +
    "                        <pdf>\r" +
    "\n" +
    "                            <iframe ngf-src=\"fn.f\" style=\"width:100%\"></iframe>\r" +
    "\n" +
    "                        </pdf>\r" +
    "\n" +
    "                        <embed ngf-src=\"fn.f\" style=\"width:100%\">\r" +
    "\n" +
    "                        <div class=\"ui bottom attached button\" type=\"file\" ng-model=\"fn.f\" ngf-select=\"upload($file)\" ngf-max-size=\"500MB\">\r" +
    "\n" +
    "                            <i class=\"add icon\"></i> Upload\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"ui bottom attached progress\" ng-show=\"fn.f.progress >= 0\" data-percent=\"{{fn.f.progress}}\">\r" +
    "\n" +
    "                            <div class=\"bar\" style=\"transition-duration: 300ms; width: {{fn.f.progress}}%\"></div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "<!--         <div class=\"column\">                     \r" +
    "\n" +
    "            <div class=\"ui fluid card\">\r" +
    "\n" +
    "              <div class=\"content\">\r" +
    "\n" +
    "                <div class=\"header\">Attachment</div>\r" +
    "\n" +
    "                <div class=\"meta\">{{f.name}} <span ng-show=\"f.size>0\">({{f.sizeKB}} KB) {{f.type}}</span></div>\r" +
    "\n" +
    "                <div class=\"ui raised segment\" ng-show=\"errorMsg!=''\">\r" +
    "\n" +
    "                  <div class=\"ui red ribbon label\">Error</div>\r" +
    "\n" +
    "                  <span>{{errorMsg}}</br></span>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "              <div class=\"image\">\r" +
    "\n" +
    "                <img ngf-src=\"picFile\">\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "              <div class=\"embed-container\" ng-show=\"f.type == 'video/mp4'\">                \r" +
    "\n" +
    "                <iframe src='http://www.youtube.com/embed/QILiHiTD3uc' frameborder='0' allowfullscreen></iframe>\r" +
    "\n" +
    "                <video controls ngf-src=\"picFile\"></video>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "              <audio controls ngf-src=\"picFile\"></audio>\r" +
    "\n" +
    "              <div class=\"ui bottom attached button\" type=\"file\" ng-model=\"picFile\" ngf-select=\"upload($file)\" ngf-max-size=\"500MB\">\r" +
    "\n" +
    "                <i class=\"add icon\"></i>\r" +
    "\n" +
    "                Upload\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "              <div class=\"ui bottom attached progress\" ng-show=\"f.progress >= 0\" data-percent=\"{{f.progress}}\">\r" +
    "\n" +
    "                <div class=\"bar\" style=\"transition-duration: 300ms; width: {{f.progress}}%;\"></div>\r" +
    "\n" +
    "              </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>  -->"
  );


  $templateCache.put('_sys/job/job.html',
    "<script type=\"text/javascript\">function deleteTagModal() {\r" +
    "\n" +
    "    $('#deleteTag').modal({\r" +
    "\n" +
    "        blurring: true\r" +
    "\n" +
    "            /*,\r" +
    "\n" +
    "                      onDeny    : function(){\r" +
    "\n" +
    "                        window.alert('Wait not yet!');\r" +
    "\n" +
    "                        //return false;\r" +
    "\n" +
    "                      },\r" +
    "\n" +
    "                      onApprove : function() {\r" +
    "\n" +
    "                        window.alert('Approved!');\r" +
    "\n" +
    "                      }*/\r" +
    "\n" +
    "    }).modal('show');\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "function addEditTag() {\r" +
    "\n" +
    "    // $('#addEditTag').modal({\r" +
    "\n" +
    "    //     blurring: true,\r" +
    "\n" +
    "    //   }).modal('show'); \r" +
    "\n" +
    "    //showFrontPanel()\r" +
    "\n" +
    "}</script> <style type=\"text/css\">.ui-splitbar {\r" +
    "\n" +
    "    background-color: #e3e3e3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "tr:hover {\r" +
    "\n" +
    "    background-color: #00aeef;\r" +
    "\n" +
    "    color: white;\r" +
    "\n" +
    "    cursor: pointer;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".selectedRecord {\r" +
    "\n" +
    "    background-color: #00aeef;\r" +
    "\n" +
    "    color: white;\r" +
    "\n" +
    "    /*font-weight: bold;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-control button {\r" +
    "\n" +
    "    height: 10px;\r" +
    "\n" +
    "    margin-top: 10px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-control input {\r" +
    "\n" +
    "    height: 14px;\r" +
    "\n" +
    "    margin-top: 9px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-row-count-picker {\r" +
    "\n" +
    "    height: 14px;\r" +
    "\n" +
    "    margin-top: 7px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-count-container .ui-grid-pager-count {\r" +
    "\n" +
    "    margin-top: 8px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui-grid-pager-control .ui-grid-pager-max-pages-number {\r" +
    "\n" +
    "    font-size: 12px;\r" +
    "\n" +
    "}</style> <div> <div class=\"ui grid\"> <div class=\"sixteen wide column\"> <div style=\"padding: 0.5em\"> <div class=\"ui fluid card\"> <div class=\"content\" style=\"background-color: whitesmoke\"> <div class=\"right floated\"> <div class=\"ui small blue basic icon buttons right floated\" style=\"border:none\"> <button class=\"ui button\" tooltips tooltip-content=\"New \" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" ng-click=\"doAddJob()\"> <i class=\"add square icon\"></i> </button> <button class=\"ui button\" tooltips tooltip-content=\"Edit\" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" ng-click=\"doEditJob()\" ng-disabled=\"gridApi.selection.getSelectedRows().length < 1\"> <i class=\"edit icon\"></i> </button> <button class=\"ui button\" tooltips tooltip-content=\"Del\" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" ng-click=\"showModal_deleteJob=true\" ng-disabled=\"gridApi.selection.getSelectedRows().length < 1\"> <i class=\"remove icon\"></i> </button> </div> </div> <div class=\"header\"> Role </div> <div class=\"meta\"> Add/Edit/Delete Role </div> </div> <div class=\"content\"> <div id=\"grid1\" ui-grid=\"gridOptions\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight()\"> </div> </div> </div> </div> <div class=\"extra content\"> </div> </div> </div> </div>  <!-- modal --> <sm-modal id=\"deleteJob\" class=\"modalDeleteJob small\" visible settings=\"{closable: false,transition:'horizontal flip',blurring: true}\"> <div class=\"header\" style=\"background-color: whitesmoke\">Delete Job Function</div> <div class=\"content\"> <p> <span style=\"color:#FF0000\">{{mJob.title}} </span> <br>This action cannot be rollbacked. Are you sure? </p> </div> <div class=\"actions\"> <div class=\"ui ok right labeled icon blue basic button\" ng-click=\"doDelete()\">Yes <i class=\"checkmark icon\"></i> </div> <div class=\"ui cancel blue basic button\">No</div> </div> </sm-modal> <sm-modal id=\"addEditJob\" class=\"modalAddEditJob small\" visible settings=\"{closable: false,transition:'horizontal flip',blurring: true}\"> <div class=\"header\" style=\"background-color: whitesmoke\" ng-if=\"newJob.id==null\">New Job</div> <div class=\"header\" style=\"background-color: whitesmoke\" ng-if=\"newJob.id!=null\">Update Job</div> <div class=\"content\"> <form class=\"ui form\" name=\"myForm\"> <div class=\"ui grid\"> <div class=\"eight wide column\"> <div ng-class=\"{ 'error': myForm.title.$touched &amp;&amp; myForm.title.$invalid }\" class=\"field\"> <label>Title</label> <div class=\"ui icon input\"> <input type=\"text\" name=\"title\" placeholder=\"Title\" ng-model=\"newJob.title\" required><i class=\"user icon\"></i> </div> <div ng-messages=\"myForm.title.$error\" ng-show=\"myForm.title.$touched &amp;&amp;\r" +
    "\n" +
    "myForm.title.$invalid\" class=\"ui pointing label pink\"> <div ng-messages-include=\"common/error_msg.html\"></div> </div> </div> </div> <div class=\"eight wide column\"> <div ng-class=\"{ 'error': myForm.desc.$touched &amp;&amp; myForm.desc.$invalid }\" class=\"field\"> <label>Description</label> <div class=\"ui icon input\"> <input type=\"text\" name=\"desc\" placeholder=\"Description\" ng-model=\"newJob.description\" required><i class=\"pencil icon\"></i> </div> <div ng-messages=\"myForm.desc.$error\" ng-show=\"myForm.desc.$touched &amp;&amp; myForm.desc.$invalid\" class=\"ui pointing label pink\"> <div ng-messages-include=\"common/error_msg.html\"></div> </div> </div> </div> </div> <input type=\"hidden\" name=\"id\" ng-model=\"newJob.id\"> </form> </div> <div class=\"actions\"> <div ng-if=\"newJob.id==null\" class=\"ui ok right labeled icon blue basic button\" ng-show=\"myForm.title.$valid && myForm.desc.$valid\" ng-click=\"doSaveJob(newJob)\">Save <i class=\"checkmark icon\"></i> </div> <div ng-if=\"newJob.id!=null\" class=\"ui ok right labeled icon blue basic button\" ng-show=\"myForm.title.$valid && myForm.desc.$valid\" ng-click=\"doSaveJob(newJob)\">Update <i class=\"checkmark icon\"></i> </div> <div class=\"ui cancel blue basic button\">Cancel</div> </div> </sm-modal>"
  );


  $templateCache.put('_sys/job/job_ori.html',
    "<script type=\"text/javascript\">function deleteTagModal(){ \r" +
    "\n" +
    "      $('#deleteTag').modal({\r" +
    "\n" +
    "          blurring: true/*,\r" +
    "\n" +
    "          onDeny    : function(){\r" +
    "\n" +
    "            window.alert('Wait not yet!');\r" +
    "\n" +
    "            //return false;\r" +
    "\n" +
    "          },\r" +
    "\n" +
    "          onApprove : function() {\r" +
    "\n" +
    "            window.alert('Approved!');\r" +
    "\n" +
    "          }*/\r" +
    "\n" +
    "        }).modal('show');\r" +
    "\n" +
    "    }</script> <style type=\"text/css\">.ui-splitbar{\r" +
    "\n" +
    "  background-color: #e3e3e3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    " tr:hover{\r" +
    "\n" +
    "    background-color: #00aeef;\r" +
    "\n" +
    "    color: white;\r" +
    "\n" +
    "    cursor: pointer;\r" +
    "\n" +
    "  }\r" +
    "\n" +
    "\r" +
    "\n" +
    "  .selectedRecord{\r" +
    "\n" +
    "    background-color: #00aeef;\r" +
    "\n" +
    "    color: white;\r" +
    "\n" +
    "    /*font-weight: bold;*/\r" +
    "\n" +
    "  }</style> <div class=\"ui grid\"> <ui-layout options=\"{flow:'column', dividerSize:2}\" style=\"top:85px\"> <div ui-layout-container min-size=\"0%\" max-size=\"50%\" size=\"10%\"> </div> <div ui-layout-container min-size=\"10%\" max-size=\"100%\" size=\"90%\"> <div class=\"eight wide column\"> <div style=\"padding: 0.5em\"> <div class=\"ui fluid card\"> <div class=\"content\" style=\"background-color: whitesmoke\"> <div class=\"right floated meta\"> <div class=\"ui small blue basic icon buttons right floated\"> <button class=\"ui button\" tooltips tooltip-content=\"New \" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" onclick=\"addEditTag()\" ng-click=\"doAddNew()\"> <i class=\"add square icon\"></i> </button> <button class=\"ui button\" tooltips tooltip-content=\"Edit\" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" ng-click=\"doEdit()\"> <i class=\"edit icon\"></i> </button> <button class=\"ui button\" tooltips tooltip-content=\"Delete\" tooltip-side=\"bottom\" tooltip-lazy=\"false\" tooltip-speed=\"fast\" tooltip-size=\"small\" tooltip-hide-trigger=\"click mouseleave\" onclick=\"deleteTagModal()\"> <i class=\"remove icon\"></i> </button> </div> </div> <div class=\"header\"> <div class=\"ui transparent left input\"> Job Function </div> </div> <div class=\"meta\"> <div class=\"ui transparent left input\" style=\"min-width: 320px\"> You can add or delete Job Function in organization </div> </div> <div class=\"ui raised segment\"> <!-- stat table tag  --> <table class=\"ui celled table\"> <thead> <tr> <th>Job Name</th> <th>Description</th> </tr> </thead> <tbody> <tr ng-repeat=\"tag in allTag\" ng-class=\"{'selectedRecord':tag == selectedRow}\" ng-click=\"selectRow(tag)\"> <td>{{tag.title}}</td> <td>{{tag.description}}</td> </tr> </tbody> <tfoot> <tr> <th colspan=\"2\"> <div class=\"ui right floated pagination menu\"> <a class=\"icon item\"> <i class=\"left chevron icon\"></i> </a> <a class=\"item\">1</a> <a class=\"item\">2</a> <a class=\"item\">3</a> <a class=\"item\">4</a> <a class=\"icon item\"> <i class=\"right chevron icon\"></i> </a> </div> </th> </tr></tfoot> </table> <!-- end table tag --> <!-- modal --> <div id=\"deleteTag\" class=\"ui small modal transition\"> <div class=\"header\">Delete Job Function</div> <div class=\"content\"> <p>Delete Job Function ( {{selectedRow.title}} ). Are you sure?</p> </div> <div class=\"actions\"> <div class=\"ui positive right labeled icon button\" ng-click=\"doDelete()\">Yes <i class=\"checkmark icon\"></i> </div> <div class=\"ui negative button\">No</div> </div> </div> <div id=\"addEditTag\" class=\"ui small modal transition\"> <div class=\"header\">Job Function</div> <div class=\"content\"> <form class=\"ui form\" name=\"myForm\"> <div class=\"field\"> <input type=\"text\" name=\"title\" placeholder=\"New Job Function\" ng-model=\"newJob.title\" required> <span style=\"color:red\" ng-show=\"myForm.title.$dirty && myForm.title.$invalid\"> <span ng-show=\"myForm.title.$error.required\">Title is required.</span> </span> </div> <div class=\"field\"> <input type=\"text\" name=\"desc\" placeholder=\"Description\" ng-model=\"newJob.description\" required> <span style=\"color:red\" ng-show=\"myForm.desc.$dirty && myForm.desc.$invalid\"> <span ng-show=\"myForm.desc.$error.required\">Description is required.</span> </span> </div> </form> </div> <div class=\"actions\"> <div class=\"ui positive right labeled icon basic button\" ng-show=\"myForm.title.$valid\" ng-click=\"doSave()\">{{btnText}} <i class=\"checkmark icon\"></i> </div> <div class=\"ui negative button\">Cancel</div> </div> </div> </div> </div> </div> </div> </div> </div> </ui-layout> </div>"
  );


  $templateCache.put('_sys/menu/accessMatrix.html',
    "<style>.ng-hide.ng-hide-animate{\r" +
    "\n" +
    "     display: none !important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".c-iExpanded {\r" +
    "\n" +
    " font-family: \"Open Sans\";\r" +
    "\n" +
    "    content: \"+\";\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".popover-content {\r" +
    "\n" +
    "    padding: 2px 2px 10px 2px;\r" +
    "\n" +
    "}</style> <div id=\"content\" style=\"padding:0\"> <!--         <div class=\"row\">\r" +
    "\n" +
    "        <big-breadcrumbs items=\"['Initialization', 'Organization']\" icon=\"sitemap\" class=\"col-xs-12 col-sm-7 col-md-7 col-lg-4\"></big-breadcrumbs>\r" +
    "\n" +
    "    </div> --> <!-- <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px 15px 10px 7px;\"> --> <!--         <h2 class=\"page-title txt-color-blueDark\" style=\"width:auto;\">\r" +
    "\n" +
    "            <i class=\"fa fa-fw fa-key\"></i> Access Matrix\r" +
    "\n" +
    "        </h2> --> <!-- <bsbreadcrumb icon=\"fa fa-fw fa-key\"></bsbreadcrumb> --> <!--         <div class=\"pull-right\">\r" +
    "\n" +
    "            <div class=\"btn-group\" style=\"margin-top:-5px;\">\r" +
    "\n" +
    "                <button class=\"btn btn-default\" onClick=\"this.blur();\" ng-click=\"refreshRoleRights()\" ng-disabled=\"selOrg==null || roleid==null\">\r" +
    "\n" +
    "                    <i class=\"fa fa-fw fa-lg fa-refresh\"> </i>Refresh\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "                <button class=\"btn btn-default\" onClick=\"this.blur();\" ng-click=\"saveRoleRights()\" ng-disabled=\"selOrg==null || roleid==null\">\r" +
    "\n" +
    "                    <i class=\"fa fa-fw fa-lg fa-save\"> </i>Save\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <!-- </div> --> <div class=\"well clearfix\" id=\"topnav_am\" style=\"border:none;margin:0px;padding:10px 10px 0px 10px;box-shadow:0 0 0 #fff\"> <!-- <form class=\"form-inline\"> --> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label>Organization: </label> <bsselect-tree title=\"Organization\" data=\"orgData\" on-select=\"showSelOrg(selected)\" tree-id=\"Id\" tree-text=\"Code\" tree-text2=\"Name\" tree-child=\"Child\" expand-level=\"3\" placeholder=\"Organization...\" tree-control=\"ctlOrg\" icon=\"fa fa-sitemap\"> </bsselect-tree> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" style=\"margin-left:10px\"> <label>Role: </label> <bsselect data=\"roleData\" item-text=\"Name\" ng-model=\"role.selected\" on-select=\"selectRole(selected)\" placeholder=\"Select Role...\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> </div> <div class=\"pull-right\" style=\"padding-top:15px\"> <div class=\"btn-group\" style=\"margin-top:-5px\"> <button class=\"btn btn-default\" onclick=\"this.blur()\" ng-click=\"refreshRoleRights()\" ng-disabled=\"selOrg==null || roleid==null\"> <i class=\"fa fa-fw fa-lg fa-refresh\"> </i>Refresh </button> <button class=\"btn btn-default\" onclick=\"this.blur()\" ng-click=\"saveRoleRights()\" ng-disabled=\"selOrg==null || roleid==null\"> <i class=\"fa fa-fw fa-lg fa-save\"> </i>Save </button> </div> </div> </div> <!-- </form> --> </div> <div id=\"layoutContainer_am\" style=\"position:relative;margin:0px;padding:0px;height:100%\"> <ui-layout options=\"{flow:'column', dividerSize:2}\"> <!-- <div ui-layout-container size=\"100%\"> --> <div class=\"inbox-side-bar\" style=\"width:100%\"> <div class=\"well clearfix\" style=\"border:none;background-color:#fff;margin:0px;padding:0 0 20px 0;box-shadow:0 0 0 #fff\"> <div class=\"ui icon info message no-animate\" ng-show=\"selOrg==null\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\"> Please select Organization to manage </div> </div> </div> <div class=\"ui icon info message no-animate\" ng-show=\"selOrg!=null && roleid==null\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\"> Please select Role to manage </div> </div> </div> <div ng-show=\"selOrg!=null && roleid!=null\" style=\"height:600px\" ng-cloak> <div id=\"agGrid_am\" ag-grid=\"grid\" class=\"ag-fresh\"></div> </div> </div> </div> <!-- </div> --> </ui-layout> </div> </div>"
  );


  $templateCache.put('_sys/menu/accessMatrix_backup.html',
    "<style></style> <ui-layout options=\"{flow:'column', dividerSize:2}\" style=\"top:85px\"> <div ui-layout-container size=\"30%\"> </div> <div ui-layout-container size=\"70%\"> <div style=\"padding: 0.5em\"> <div class=\"ui card fluid\"> <div class=\"content\" style=\"background-color: #f0f0f0\"> <div class=\"floated right\"> <button class=\"ui basic blue button right floated\" ng-click=\"saveRoleRights()\" ng-disabled=\"roleid==null\">Save</button> <div class=\"ui selection dropdown right floated\"> <input type=\"hidden\" name=\"role\"> <i class=\"dropdown icon\"></i> <div class=\"default text\">Select Role</div> <div class=\"menu\"> <div class=\"item\" ng-repeat=\"role in roledata\" data-value=\"role.desc\" ng-click=\"selectRole(role)\"> <i class=\"child icon\"></i> {{role.desc}} </div> </div> </div> </div> <div class=\"header\">Access Matrix</div> <div class=\"meta\">Change Access Matrix</div> </div> <div class=\"content\"> <div class=\"ui cards fluid\"> <div id=\"level-{{node.level}}-{{$index}}\" ng-repeat=\"node in data\" class=\"ui card fluid\" style=\"background-color: aliceblue\"> <div class=\"content\"> <div class=\"header\">.:. {{node.title}} <div class=\"ui checkbox floated left\" style=\"margin-left:20px\"> <input type=\"checkbox\" tabindex=\"0\" ng-model=\"node.bitArr[0]\" ng-change=\"chgRbyte(node)\"> <label> Access All</label> </div> </div> </div> <div ng-include=\"'ng-am'\" class=\"ui cards\" style=\"margin-bottom: 20px\"></div> </div> </div> </div> </div> </div> </div> </ui-layout> <script type=\"text/ng-template\" id=\"ng-am\"><div id=\"level-{{node.level}}-{{$index}}\" ng-repeat=\"node in node.child\" ng-class=\"{ 'ui card fluid': node.level==2, 'card':node.level==3 }\" ng-style=\"{\r" +
    "\n" +
    "                      'background-color':(node.level==2?'whitesmoke':''),\r" +
    "\n" +
    "                      'margin':(node.level==2?'20px 20px 0 20px':''),\r" +
    "\n" +
    "                      'width':(node.level==3?'295px':'')\r" +
    "\n" +
    "                    }\">\r" +
    "\n" +
    "        <div ng-if=\"node.level==2\" class=\"content\">\r" +
    "\n" +
    "            <div class=\"header\" style=\"margin-bottom: 10px;\">:: {{node.title}}\r" +
    "\n" +
    "                <div class=\"ui checkbox floated left\" style=\"margin-left:20px;\">\r" +
    "\n" +
    "                    <input type=\"checkbox\" tabindex=\"0\" ng-model=\"node.bitArr[0]\" ng-change=\"chgRbyte(node)\">\r" +
    "\n" +
    "                    <label> Access All</label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-include=\"'ng-am'\" class=\"ui cards\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"extra content\" ng-if=\"node.level==3\" style=\"background-color: gainsboro;\">\r" +
    "\n" +
    "            <div class=\"header\">{{node.title}}</div>\r" +
    "\n" +
    "            <div class=\"meta\"> {{node.description}}</div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"extra content\" ng-if=\"node.level==3\">\r" +
    "\n" +
    "            <div class=\"description\">\r" +
    "\n" +
    "                <div class=\"ui checkbox\" ng-repeat=\"rb in rbdata\" ng-if=\"checkRB(node.byte_available,rb.rights_byte)\">\r" +
    "\n" +
    "                    <input type=\"checkbox\" tabindex=\"0\" ng-model=\"node.bitArr[$index]\" ng-change=\"chgRbyte(node)\">\r" +
    "\n" +
    "                    <label style=\"padding-left: 22px;padding-right:10px;\"> {{rb.rights_desc}}</label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    </div></script> <script>$('.ui.dropdown')\r" +
    "\n" +
    "  .dropdown()\r" +
    "\n" +
    ";</script>"
  );


  $templateCache.put('_sys/menu/accessMatrix_v1.html',
    "<style>.ng-hide.ng-hide-animate{\r" +
    "\n" +
    "     display: none !important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".c-iExpanded {\r" +
    "\n" +
    " font-family: \"Open Sans\";\r" +
    "\n" +
    "    content: \"+\";\r" +
    "\n" +
    "}</style> <div id=\"content\" style=\"padding:0\"> <!--     <div class=\"row\">\r" +
    "\n" +
    "        <big-breadcrumbs items=\"['Initialization', 'Organization']\" icon=\"sitemap\" class=\"col-xs-12 col-sm-7 col-md-7 col-lg-4\"></big-breadcrumbs>\r" +
    "\n" +
    "    </div> --> <div class=\"bs-navbar no-content-padding\" style=\"height:60px;margin:0px;padding:14px 20px 20px 14px\"> <h2 class=\"page-title txt-color-blueDark\" style=\"width:auto\"> <i class=\"fa fa-fw fa-key\"></i> Access Matrix </h2> </div> <div id=\"layoutContainer_am\" style=\"position:relative;margin:0px;padding:0px;height:100%\"> <ui-layout options=\"{flow:'column', dividerSize:2}\"> <div ui-layout-container size=\"25%\"> <div class=\"inbox-side-bar\" style=\"width:100%\"> <h6 style=\"padding:0px;margin:0px;font-size:1.4rem\">ORGANIZATION CHART </h6> <hr> <abn-tree tree-data=\"orgData\" tree-control=\"ctlOrg\" icon-leaf=\"fa fa-file\" icon-expand=\"fa fa-plus\" icon-collapse=\"fa fa-minus\" on-select=\"showSelOrg(branch)\"> </abn-tree> </div> </div> <div ui-layout-container size=\"75%\"> <div class=\"inbox-side-bar\" style=\"width:100%\"> <h6 style=\"padding:0px;margin:0px;font-size:1.4rem\">ACCESS MATRIX<br> <div class=\"pull-right\" style=\"margin-top:-2rem\" ng-if=\"selOrg!=null\"> <div class=\"form-group\" style=\"margin-bottom:0px\"> <label class=\"control-label\">Role: </label> <ui-select ng-model=\"role.selected\" on-select=\"selectRole(role.selected)\" theme=\"select2\" class=\"select2\" style=\"min-width:150px;display:inline-block;vertical-align:middle;margin-left:5px;margin-right:5px\"> <ui-select-match placeholder=\"Select Role...\">{{$select.selected.title}}</ui-select-match> <ui-select-choices repeat=\"role in roleData | filter: $select.search\"> <i class=\"fa fa-child\"></i> <span ng-bind-html=\"role.title | highlight: $select.search\"></span> </ui-select-choices> </ui-select> <button class=\"btn btn-default\" ng-click=\"saveRoleRights()\" ng-disabled=\"roleid==null\"> <i class=\"fa fa-fw fa-lg fa-save\"> </i>Save </button> </div> </div> </h6> <hr> <div class=\"well clearfix\" ng-if=\"selOrg!=null\" style=\"border:none;background-color:#fff;margin:0px;padding:0 0 20px 0;box-shadow:0 0 0 #fff\"> <div class=\"pull-left\"> <div class=\"dropdown\" uib-dropdown> <button class=\"btn btn-default dropdown-toggle\" type=\"button\" uib-dropdown-toggle>Select Top Menu <span class=\"caret\"></span></button> <ul class=\"dropdown-menu\" uib-dropdown-menu style=\"min-width: 200px\"> <div class=\"input-icon-right\"> <i class=\"fa fa-search\"></i> <input type=\"text\" name=\"search\" placeholder=\"Search...\" style=\"margin-left:5px;margin-right:5px\"> </div> <abn-tree tree-data=\"l2data\" tree-control=\"ctlMenu\" icon-leaf=\"fa fa-file\" icon-expand=\"fa fa-plus\" icon-collapse=\"fa fa-minus\" on-select=\"l2Selected(branch)\"> </abn-tree> </ul> </div> </div> </div> <div class=\"ui icon info message no-animate\" ng-show=\"selOrg==null\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\"> Please select Organization to manage </div> </div> </div> <div class=\"ui cards fluid\"> <div id=\"level-{{node.level}}-{{$index}}\" ng-repeat=\"node in data\" class=\"ui card fluid\" style=\"background-color: aliceblue\" ng-show=\"node.visible\"> <div class=\"content\" style=\"padding-bottom:0px\"> <div class=\"header\">.:. {{node.title}}&nbsp&nbsp <div class=\"ui checkbox floated left\" style=\"margin-left:20px\"> <input type=\"checkbox\" tabindex=\"0\" ng-model=\"node.bitArr[0]\" ng-change=\"chgRbyte(node)\"> <label>Access All</label> </div> </div> </div> <div ng-include=\"'ng-am'\" class=\"ui cards\" style=\"margin:0 5px 5px 5px\"></div> </div> </div> </div> </div> </ui-layout> </div> </div> <!-- ng-style=\"{\r" +
    "\n" +
    "                      'background-color':(node.level==2?'whitesmoke':''),\r" +
    "\n" +
    "                      'margin':(node.level==2?'20px 20px 0 20px':''),\r" +
    "\n" +
    "                      'width':(node.level==3?'295px':'')\r" +
    "\n" +
    "                     }\"> --> <!-- ng-class=\"{ 'panel panel-default': node.level==2, 'panel panel-default':node.level==3 }\" --> <script type=\"text/ng-template\" id=\"ng-am\"><div id=\"level-{{node.level}}-{{$index}}\" ng-repeat=\"node in node.child\" \r" +
    "\n" +
    "         ng-show=\"node.visible\" \r" +
    "\n" +
    "         ng-class=\"{ 'ui card fluid': node.child.length>0 && node.parent!=null, 'ui card':node.child.length==0 }\"\r" +
    "\n" +
    "         ng-style=\"{\r" +
    "\n" +
    "                      'background-color':(node.child.length>0 && node.parent!=null?'whitesmoke':''),\r" +
    "\n" +
    "                      'width':(node.child.length==0?'295px':'')\r" +
    "\n" +
    "                    }\">\r" +
    "\n" +
    "                    <!-- 'margin':(node.child.length>0 && node.parent!=null?'':''), -->\r" +
    "\n" +
    "        <div ng-if=\"node.child.length>0 && node.parent!=null\" class=\"content\">\r" +
    "\n" +
    "            <div class=\"header\" style=\"margin-bottom: 10px;\">:: {{node.title}}&nbsp&nbsp\r" +
    "\n" +
    "                <div class=\"ui checkbox floated left\" style=\"margin-left:20px;\">\r" +
    "\n" +
    "                    <input type=\"checkbox\" tabindex=\"0\" ng-model=\"node.bitArr[0]\" ng-change=\"chgRbyte(node)\">\r" +
    "\n" +
    "                    <label> Access All</label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-include=\"'ng-am'\" class=\"ui cards\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"content\" ng-if=\"node.child.length==0\" style=\"margin-bottom: 5px;\">\r" +
    "\n" +
    "            <div class=\"header\">{{node.title}}</div>\r" +
    "\n" +
    "            <div class=\"meta\"> {{node.description}}</div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"content\" ng-if=\"node.child.length==0\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"ui form\" style=\"font-size:1.2rem\">\r" +
    "\n" +
    "            <div class=\"inline fields\">\r" +
    "\n" +
    "                <div class=\"field\" ng-repeat=\"rb in rbData\" ng-if=\"checkRB(node.byte_available,rb.rights_byte)\">\r" +
    "\n" +
    "                <div class=\"ui checkbox\" >\r" +
    "\n" +
    "                    <input type=\"checkbox\" tabindex=\"0\" ng-model=\"node.bitArr[$index]\" ng-change=\"chgRbyte(node)\">\r" +
    "\n" +
    "                    <label>{{rb.rights_desc}}</label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div></script>"
  );


  $templateCache.put('_sys/menu/menuManager.html',
    "<style type=\"text/css\"></style> <bstreeform ng-form=\"MenuForm\" form-name=\"Menu\" form-title=\"Application Menu Manager\" tree-title=\"Application Menu\" detail-title=\"Application Menu Detail\" factory-name=\"Menu\" collection=\"cData\" model=\"mData\" get-data=\"getData()\" icon=\"fa fa-fw fa-sitemap\"> <formly-form fields=\"vm.fields\" model=\"mData\" form=\"vm.form\" options=\"vm.options\" novalidate> <label><i class=\"fa fa-fw fa-key\"></i> Byte Available</label> <div class=\"col-md-12\" style=\"border-style:solid;border-width:1px;border-color:#cecece\"> <div class=\"form-group\"> <label class=\"col-md-3\" ng-repeat=\"rb in rbData\"> <input type=\"checkbox\" class=\"checkbox style-0\" id=\"$index\" ng-model=\"mData.bitArr[$index]\" ng-change=\"chgRbyte(mData)\"> <span>{{rb.rights_desc}}</span> </label> </div> </div> </formly-form> </bstreeform>"
  );


  $templateCache.put('_sys/menu/menuManager_manual.html',
    "<style type=\"text/css\"></style> <div id=\"content\" style=\"padding:0\"> <div class=\"bs-navbar no-content-padding\" style=\"height:60px;margin:0px;padding:14px 20px 20px 14px\"> <h2 class=\"page-title txt-color-blueDark\" style=\"width:auto\"> <i class=\"fa fa-fw fa-sitemap\"></i> Application Menu Manager </h2> </div> <div id=\"layoutContainer\" style=\"position:relative;margin:0px;padding:0px;height:100%\"> <ui-layout options=\"{flow:'column', dividerSize:2}\"> <div ui-layout-container min-size=\"0%\" max-size=\"50%\" size=\"25%\" style=\"background-color:#fefefe\"> <div class=\"inbox-side-bar\" style=\"width:100%;height:100%;padding:1rem\"> <h6 style=\"padding:0px;margin:0px;font-size:1.4rem\">APPLICATION MENU </h6> <hr style=\"margin-bottom:10px\"> <form class=\"smart-form\" style=\"background-color:#ffffff\"> <label class=\"input\"> <i class=\"icon-prepend fa fa-search\"></i> <a href=\"#\" ng-click=\"clearFilter()\" class=\"icon-append fa fa-times-circle\" ng-if=\"filterText\" style=\"border-left-style:none\"></a> <input class=\"form-control\" placeholder=\"filter...\" ng-model=\"filterText\" style=\"border-radius:23px !important\"> </label> </form> <!--                     <abn-tree tree-data=\"menuData\"\r" +
    "\n" +
    "                              tree-control=\"ctlMenu\"\r" +
    "\n" +
    "                              icon-leaf=\"fa fa-file\"\r" +
    "\n" +
    "                              icon-expand=\"fa fa-plus\"\r" +
    "\n" +
    "                              icon-collapse=\"fa fa-minus\"\r" +
    "\n" +
    "                              on-select=\"showselData(branch)\">\r" +
    "\n" +
    "                    </abn-tree> --> <treecontrol tree-model=\"cData\" options=\"treeOptions\" on-selection=\"showSelData(node)\" expanded-nodes=\"expandedNodes\" filter-expression=\"filterText\"> {{node.title}} </treecontrol> </div> </div> <div ui-layout-container min-size=\"10%\" max-size=\"100%\" size=\"75%\" style=\"background-color:#fefefe\"> <div class=\"inbox-side-bar\" style=\"width:100%;padding:1rem\"> <h6 style=\"padding:0px;margin:0px;font-size:1.4rem\">MENU DETAIL <div class=\"pull-right\" style=\"margin-top:-2px\"> <div class=\"btn-group\"> <button class=\"zbtnsc btn btn-default\" tooltip=\"Add\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actNew()\" ng-disabled=\"!selData || sm_mode!='idle'\"> <i class=\"fa fa-plus\"> </i> </button> <button class=\"zbtnsc btn btn-default\" tooltip=\"Edit\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actEdit()\" ng-disabled=\"!selData || sm_mode!='idle'\"> <i class=\"fa fa-edit\"> </i> </button> <button class=\"zbtnsc btn btn-default\" tooltip=\"Delete\" tooltip-placement=\"bottom\" class=\"txt-color-darken\" ng-click=\"actDel()\" ng-disabled=\"!selData || sm_mode!='idle'\"> <i class=\"fa fa-remove\"></i> </button> </div> </div> </h6> <hr> <div class=\"ui icon info message no-animate\" ng-if=\"!selData\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\"> Please select Application Menu to manage </div> </div> </div> <div class=\"ui fluid card\" ng-show=\"selData\"> <div class=\"content\" style=\"background-color: whitesmoke\"> <div class=\"right floated meta time\"> <div class=\"ui buttons\"> <button class=\"ui button\" style=\"font-size:1.15rem\" ng-show=\"sm_mode!='idle'\" ng-click=\"doCancel()\">Cancel </button> <div class=\"or\" ng-show=\"(sm_mode!='idle' && MenuForm.$invalid==false && MenuForm.$pristine==false) || sm_mode=='del'\"></div> <button class=\"ui blue button\" style=\"font-size:1.15rem\" ng-show=\"(sm_mode!='idle' && MenuForm.$invalid==false && MenuForm.$pristine==false) || sm_mode=='del'\" ng-click=\"sm_act()\">{{sm_mode=='del'?'Delete':'Save'}} </button> </div> </div> <div class=\"header\" style=\"font-weight:300\">MENU ITEM</div> <div class=\"meta\">Menu Path: {{selData.title}}</div> </div> <div class=\"content\"> <form name=\"MenuForm\"> <fieldset ng-disabled=\"sm_mode=='del' || sm_mode=='idle'\"> <formly-form fields=\"vm.fields\" model=\"mData\" form=\"vm.form\" novalidate> <label><i class=\"fa fa-fw fa-key\"></i> Byte Available</label> <div class=\"col-md-12\" style=\"border-style:solid;border-width:1px;border-color:#cecece\"> <div class=\"form-group\"> <label class=\"checkbox-inline col-md-2\" ng-repeat=\"rb in rbData\" style=\"margin-left:0px;margin-right:10px\"> <input type=\"checkbox\" id=\"$index\" ng-model=\"mData.bitArr[$index]\" ng-change=\"chgRbyte(mData)\"> {{rb.rights_desc}} </label> </div> </div> </formly-form> </fieldset> </form> </div> </div> </div> </div> </ui-layout> </div> </div> <!--                                     <div class=\"row\">\r" +
    "\n" +
    "                                        <div class=\"col-md-6\">\r" +
    "\n" +
    "                                            <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                <label>Menu Title</label>\r" +
    "\n" +
    "                                                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                                    <i class=\"fa fa-bars\"></i>\r" +
    "\n" +
    "                                                    <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Title\" ng-model=\"mData.title\" required>\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                                <bserrmsg field=\"title\"></bserrmsg>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                        <div class=\"col-md-6\">\r" +
    "\n" +
    "                                            <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                <label>Description</label>\r" +
    "\n" +
    "                                                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                                                    <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\"\r" +
    "\n" +
    "                                                           ng-model=\"mData.description\">\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                                <bserrmsg field=\"desc\"></bserrmsg>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"row\">\r" +
    "\n" +
    "                                        <div class=\"col-md-4\">\r" +
    "\n" +
    "                                            <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                <label>Menu URL (s-ref)</label>\r" +
    "\n" +
    "                                                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                                    <i class=\"fa fa-link\"></i>\r" +
    "\n" +
    "                                                    <input type=\"text\" class=\"form-control\" name=\"sref\" placeholder=\"URL\" ng-model=\"mData.sref\">\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                                <bserrmsg field=\"sref\"></bserrmsg>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                        <div class=\"col-md-4\">\r" +
    "\n" +
    "                                            <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                                <label>Icon</label>\r" +
    "\n" +
    "                                                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                                                    <i class=\"fa fa-fonticons\"></i>\r" +
    "\n" +
    "                                                    <input type=\"text\" class=\"form-control\" name=\"icon\" placeholder=\"Icon\"\r" +
    "\n" +
    "                                                           ng-model=\"mData.icon\">\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                                <bserrmsg field=\"icon\"></bserrmsg>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <label><i class=\"fa fa-fw fa-key\"></i> Byte Available</label>\r" +
    "\n" +
    "                                    <div class=\"col-md-12\" style=\"border-style:solid;border-width:1px;border-color:#cecece;\">\r" +
    "\n" +
    "                                        <div class=\"form-group\">\r" +
    "\n" +
    "                                            <label class=\"checkbox-inline col-md-2\" ng-repeat=\"rb in rbData\" style=\"margin-left:0px;margin-right:10px;\">\r" +
    "\n" +
    "                                                <input type=\"checkbox\" id=\"$index\" ng-model=\"mData.bitArr[$index]\" ng-change=\"chgRbyte(mData)\">\r" +
    "\n" +
    "                                                {{rb.rights_desc}}\r" +
    "\n" +
    "                                            </label>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div> -->"
  );


  $templateCache.put('_sys/menu/menulist.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid-tree ng-form=\"MenuListForm\" factory-name=\"Menu\" model=\"mMenu\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-title=\"Menu\" modal-title=\"Menu\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Title</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-bars\"></i> <input type=\"text\" class=\"form-control\" name=\"title\" placeholder=\"Memu Title\" ng-model=\"mMenu.Name\" ng-maxlength=\"40\" required> </div> <bserrmsg field=\"title\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Menu URL (sref)</label> <div class=\"input-icon-right\"> <i class=\"fa fa-link\"></i> <input type=\"text\" class=\"form-control\" name=\"sref\" placeholder=\"Menu URL (sref)\" ng-model=\"mMenu.Sref\" ng-maxlength=\"30\"> </div> <bserrmsg field=\"sref\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Sequence</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-sort-numeric-asc\"></i> <input type=\"number\" class=\"form-control\" name=\"seq\" placeholder=\"Sequence\" ng-model=\"mMenu.Sequence\" min=\"0\" required> </div> <bserrmsg field=\"seq\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Byte Available</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-lock\"></i> <input type=\"number\" class=\"form-control\" name=\"ba\" placeholder=\"Byte Available\" ng-model=\"mMenu.ByteAvailable\" min=\"0\" required> </div> <bserrmsg field=\"ba\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Icon</label> <div class=\"input-icon-right\"> <i class=\"fa fa-fonticons\"></i> <input type=\"text\" class=\"form-control\" name=\"icon\" placeholder=\"Icon\" ng-model=\"mMenu.Icon\" ng-maxlength=\"30\"> </div> <bserrmsg field=\"icon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Parent Menu</label> <bsselect name=\"parentMenu\" data=\"grid.data\" item-text=\"Name\" item-value=\"Id\" ng-model=\"mMenu.ParentId\" placeholder=\"Select Parent Menu\" icon=\"fa fa-sitemap glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"parentMenu\"></bserrmsg> </div> </div> </div> </bsform-grid-tree>"
  );


  $templateCache.put('_sys/_demo/formlyDemo.html',
    "<style>/*--------------------\r" +
    "\n" +
    "        Field\r" +
    "\n" +
    "---------------------*/\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui.form .formly-field {\r" +
    "\n" +
    "  clear: both;\r" +
    "\n" +
    "  margin: 0em 0em 1em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui.form .formly-field .field {\r" +
    "\n" +
    "  clear: both;\r" +
    "\n" +
    "  margin: 0em 0em 1em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui.form .formly-field .field:last-child,\r" +
    "\n" +
    ".ui.form .formly-field .fields:last-child .field {\r" +
    "\n" +
    "  margin-bottom: 0em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui.form .formly-field .fields .field {\r" +
    "\n" +
    "  clear: both;\r" +
    "\n" +
    "  margin: 0em 0em 1em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/*--------------------\r" +
    "\n" +
    "        Labels\r" +
    "\n" +
    "---------------------*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "/*.ui.form .formly-field .field > label {\r" +
    "\n" +
    "  display: block;\r" +
    "\n" +
    "  margin: 0em 0em 0.28571429rem 0em;\r" +
    "\n" +
    "  color: rgba(0, 0, 0, 0.87);\r" +
    "\n" +
    "  font-size: 0.92857143em;\r" +
    "\n" +
    "  font-weight: bold;\r" +
    "\n" +
    "  text-transform: none;\r" +
    "\n" +
    "}*/</style> <div class=\"row\" style=\"background-color:#fff\"> <div class=\"sixteen column\" style=\"margin:0 20px 0 20px\"> <div ng-controller=\"FormlyDemoController as vm\"> <div> <h1>{{::vm.formTitle}}</h1> <h4>Angular: {{::vm.env.angularVersion}} Formly: {{vm.env.formlyVersion}}</h4> <div> <div class=\"row\"> <div class=\"col-md-8\"> <div class=\"ui segment\"> <formly-form model=\"vm.model\" fields=\"vm.fields\"> <button type=\"submit\" class=\"ui button\" ng-click=\"vm.submit(vm.model)\">Submit</button> </formly-form> </div> </div> <div class=\"col-md-4\"> <div class=\"ui segment\"> <h2>vm.model</h2> <pre>{{vm.model | json}}</pre> <h2>vm.fields <small>(note, functions are not shown)</small></h2> <pre>{{vm.fields | json}}</pre> </div> </div> </div> </div> </div> </div> </div> </div> <script type=\"text/javascript\"></script>"
  );


  $templateCache.put('_sys/_demo/formlyDemo_Semantic.html',
    "<style>/*--------------------\r" +
    "\n" +
    "        Field\r" +
    "\n" +
    "---------------------*/\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui.form .formly-field {\r" +
    "\n" +
    "  clear: both;\r" +
    "\n" +
    "  margin: 0em 0em 1em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui.form .formly-field .field {\r" +
    "\n" +
    "  clear: both;\r" +
    "\n" +
    "  margin: 0em 0em 1em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui.form .formly-field .field:last-child,\r" +
    "\n" +
    ".ui.form .formly-field .fields:last-child .field {\r" +
    "\n" +
    "  margin-bottom: 0em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ui.form .formly-field .fields .field {\r" +
    "\n" +
    "  clear: both;\r" +
    "\n" +
    "  margin: 0em 0em 1em;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/*--------------------\r" +
    "\n" +
    "        Labels\r" +
    "\n" +
    "---------------------*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "/*.ui.form .formly-field .field > label {\r" +
    "\n" +
    "  display: block;\r" +
    "\n" +
    "  margin: 0em 0em 0.28571429rem 0em;\r" +
    "\n" +
    "  color: rgba(0, 0, 0, 0.87);\r" +
    "\n" +
    "  font-size: 0.92857143em;\r" +
    "\n" +
    "  font-weight: bold;\r" +
    "\n" +
    "  text-transform: none;\r" +
    "\n" +
    "}*/</style> <div class=\"ui grid\"> <div class=\"row\"> <div class=\"ui top attached tabular menu\"> <a class=\"active item\" data-tab=\"first\">First</a> <a class=\"item\" data-tab=\"second\">Second</a> <a class=\"item\" data-tab=\"third\">Third</a> </div> <div class=\"ui bottom attached active tab segment\" data-tab=\"first\"> <div class=\"ui input\"> <input type=\"text\"> </div> <div class=\"ui input\"> <input type=\"text\"> </div> <div class=\"ui input\"> <input type=\"text\"> </div> </div> <div class=\"ui bottom attached tab segment\" data-tab=\"second\"> Second </div> <div class=\"ui bottom attached tab segment\" data-tab=\"third\"> Third </div> </div> <div class=\"row\"> <div class=\"sixteen column\" style=\"margin:0 20px 0 20px\"> <div ng-controller=\"FormlyDemoController as vm\"> <div> <h1>{{::vm.formTitle}}</h1> <h4>Angular: {{::vm.env.angularVersion}} Formly: {{vm.env.formlyVersion}}</h4> <div> <div class=\"ui grid\"> <div class=\"ui form\"> </div> <div class=\"ui row\"> <div class=\"ui ten wide column\"> <div class=\"ui segment\"> <formly-form model=\"vm.model\" fields=\"vm.fields\" class=\"ui form\"> <button type=\"submit\" class=\"ui button\" ng-click=\"vm.submit(vm.model)\">Submit</button> </formly-form> </div> </div> <div class=\"ui six wide column\"> <div class=\"ui segment\"> <h2>vm.model</h2> <pre>{{vm.model | json}}</pre> <h2>vm.fields <small>(note, functions are not shown)</small></h2> <pre>{{vm.fields | json}}</pre> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <script type=\"text/javascript\">$(function(){\r" +
    "\n" +
    "    $('.ui.dropdown').dropdown();\r" +
    "\n" +
    "    $('.ui.search.dropdown').dropdown();\r" +
    "\n" +
    "\r" +
    "\n" +
    "    $('.menu .item').tab();\r" +
    "\n" +
    "\r" +
    "\n" +
    "    $('input[name=\"birthdate\"]').daterangepicker({\r" +
    "\n" +
    "        //singleDatePicker: true,\r" +
    "\n" +
    "        showDropdowns: true,\r" +
    "\n" +
    "        timePicker: true,\r" +
    "\n" +
    "        locale: {\r" +
    "\n" +
    "            format: 'DD/MM/YYYY h:mm A'\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    });\r" +
    "\n" +
    "});</script>"
  );

}]);
