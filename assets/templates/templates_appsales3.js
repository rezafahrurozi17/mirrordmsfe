angular.module('templates_appsales3', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/sales/sales3/pdd/maintainPDD/maintainPDD.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "        $('body .modals').remove();\r" +
    "\n" +
    "    }</script> <style type=\"text/css\">.card {\r" +
    "\n" +
    "        background: #fff;\r" +
    "\n" +
    "        border-radius: 2px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .card-2 {\r" +
    "\n" +
    "        box-shadow: 0 3px 3px rgba(0, 0, 0, 0.1), 0 2px 8px rgba(0, 0, 0, 0.1)\r" +
    "\n" +
    "        /* box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23); */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .card-1 {\r" +
    "\n" +
    "        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);\r" +
    "\n" +
    "        transition: all 0.3s cubic-bezier(.25, .8, .25, 1);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .card-1:hover {\r" +
    "\n" +
    "        box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .row {\r" +
    "\n" +
    "        margin: 0 0 0 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .box-show-setup,\r" +
    "\n" +
    "    .box-hide-setup {\r" +
    "\n" +
    "        -webkit-transition: all linear 0.3s;\r" +
    "\n" +
    "        -moz-transition: all linear 0.3s;\r" +
    "\n" +
    "        -ms-transition: all linear 0.3s;\r" +
    "\n" +
    "        -o-transition: all linear 0.3s;\r" +
    "\n" +
    "        transition: all linear 0.3s;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "        -webkit-border-radius: 4px;\r" +
    "\n" +
    "        -moz-border-radius: 4px;\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "        border-radius: 4px;\r" +
    "\n" +
    "        border: 0px;\r" +
    "\n" +
    "        background-color: transparent;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .adv-srch {\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        margin-bottom: 0px;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        background-color: rgba(213, 51, 55, 0.02);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .adv-srch-on {\r" +
    "\n" +
    "        padding-top: 4px !important;\r" +
    "\n" +
    "        padding: 6px;\r" +
    "\n" +
    "        margin: 0 3px 0 0;\r" +
    "\n" +
    "        margin-bottom: -5px !important;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        border-bottom-style: none !important;\r" +
    "\n" +
    "        background-color: #FEFBFB;\r" +
    "\n" +
    "        border-top-left-radius: 3px;\r" +
    "\n" +
    "        border-top-right-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .colorsrch {\r" +
    "\n" +
    "        color: red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .modalpadding {\r" +
    "\n" +
    "        padding-top: 0px !important;\r" +
    "\n" +
    "        padding-right: 0px !important;\r" +
    "\n" +
    "        padding-bottom: 0px !important;\r" +
    "\n" +
    "        padding-left: 0px !important;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /*@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin-top: 10px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .noppadingFormGroup {\r" +
    "\n" +
    "        margin-bottom: 0px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .ScrollStyle {\r" +
    "\n" +
    "        max-height: 600px;\r" +
    "\n" +
    "        overflow-y: scroll;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .item-modal {\r" +
    "\n" +
    "        border: none;\r" +
    "\n" +
    "        padding-bottom: 15px;\r" +
    "\n" +
    "        border-bottom: 1px solid #ddd !important;\r" +
    "\n" +
    "        margin: 10px 30px 10px 30px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .mobileAddBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1 !important;\r" +
    "\n" +
    "        font-size: 15px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .mobileFilterBtn {\r" +
    "\n" +
    "        padding: 0 0 0 1 !important;\r" +
    "\n" +
    "        font-size: 13px;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        float: right;\r" +
    "\n" +
    "        /*background-color: silver;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .filterMarg {\r" +
    "\n" +
    "        margin-bottom: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media only screen and (max-width: 430px) {\r" +
    "\n" +
    "        .iconFilter {\r" +
    "\n" +
    "            margin-left: -4px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media only screen and (max-width: 500px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -28px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        .iconFilter {\r" +
    "\n" +
    "            margin-left: -4px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #desktopderectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: block !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media (min-width: 501px) and (max-width: 600px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -36px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #desktopderectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: block !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media (min-width: 601px) {\r" +
    "\n" +
    "        .btnUp {\r" +
    "\n" +
    "            margin: -33px 0px 0 0 !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        #mobilederectivemobile {\r" +
    "\n" +
    "            display: none !important\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .mobilesearchBtn {\r" +
    "\n" +
    "        text-decoration: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @media (max-width: 350px) {\r" +
    "\n" +
    "        .bsbreadcrumb .bsbreadcrumb-path,\r" +
    "\n" +
    "        .bsbreadcrumb i {\r" +
    "\n" +
    "            display: block !important;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /*Modal*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .vertical-alignment-helper {\r" +
    "\n" +
    "        display: table;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "        /* This makes sure that we can still click outside of the modal to close it */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .vertical-align-center {\r" +
    "\n" +
    "        /* To center vertically */\r" +
    "\n" +
    "        display: table-cell;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        pointer-events: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .modal-content {\r" +
    "\n" +
    "        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */\r" +
    "\n" +
    "        width: inherit;\r" +
    "\n" +
    "        height: inherit;\r" +
    "\n" +
    "        /* To center horizontally */\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        pointer-events: all;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @-webkit-keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            -webkit-transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    @keyframes spin {\r" +
    "\n" +
    "        0% {\r" +
    "\n" +
    "            transform: rotate(0deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        100% {\r" +
    "\n" +
    "            transform: rotate(360deg);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .sixDate button span {\r" +
    "\n" +
    "        background-color: lightskyblue;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .fifthDate button span {\r" +
    "\n" +
    "        background-color: crimson;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .fourthDate button span {\r" +
    "\n" +
    "        background-color: darkseagreen;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .thirdDate button span {\r" +
    "\n" +
    "        background-color: lightsteelblue;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .secondDate button span {\r" +
    "\n" +
    "        background-color: sandybrown;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .dafirstDate button span {\r" +
    "\n" +
    "        background-color: mediumpurple;\r" +
    "\n" +
    "        border-radius: 32px;\r" +
    "\n" +
    "        color: black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .firstDate button span {\r" +
    "\n" +
    "        background-color: mediumpurple !important;\r" +
    "\n" +
    "        border-radius: 32px !important;\r" +
    "\n" +
    "        color: black !important;\r" +
    "\n" +
    "        width: 300px !important;\r" +
    "\n" +
    "        height: 300px !important;\r" +
    "\n" +
    "        \r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-hide=\"daftarPDD\" class=\"tab-pane fade in active\"> <bsform-mobile factory-name=\"MaintainPDDFactory\" bind-data=\"bind_data\" info-totaldata=\"true\" filter-data=\"filterData\" enable-addbutton=\"disable\" custom-modallihat=\"true\" custom-modalubah=\"true\" custom-modalhapus=\"true\" enable-listtoggle=\"false\" info-totaldata=\"false\" service-enable=\"true\" updated-data=\"updated_data\" selected-data=\"selected_data\" form-name=\"MaintainPDDForm\" list-title=\"Daftar PDD\" row-template-url=\"app/sales/sales3/pdd/maintainPDD/rowTemplate.html\"> <bsform-mobilemodal> <div ng-if=\"selected_data.StatusPDDName=='Sudah Dibuat'\"> <a ng-click=\"lihatPDDFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat PDD</li> </a> <a ng-click=\"revisiPDDFunction(selected_data)\" ng-if=\"user.RoleName == 'SALES'\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-pencil\" style=\"margin-right: 15px\"></i> </span>Revisi PDD</li> </a> </div> <div ng-if=\"selected_data.StatusPDDName=='Butuh Konfirmasi'\"> <a ng-click=\"lihatPDDFunction()\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i> </span>Lihat PDD</li> </a> <a ng-click=\"konfirmasiPDDFunction(selected_data)\" ng-if=\"user.RoleName == 'SALES'\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-pencil\" style=\"margin-right: 15px\"></i> </span>Konfirmasi PDD</li> </a> </div> <div ng-if=\"selected_data.StatusPDDName == 'Belum Dibuat'\"> <a ng-click=\"buatPDDFunction(selected_data)\" ng-if=\"user.RoleName == 'SALES'\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-pencil\" style=\"margin-right: 15px\"></i> </span>Buat PDD</li> </a> <a ng-click=\"\" ng-if=\"user.RoleName != 'SALES'\"> <li class=\"list-group-item item-modal\"> <span> <i class=\"fa fa-fw fa-lg fa-window-close-o\" style=\"margin-right: 15px\"></i> </span>User ini tidak diberikan hak akses.</li> </a> </div> </bsform-mobilemodal> </bsform-mobile> </div> <div ng-show=\"tambahPDD\"> <p> <button id=\"openPDDBtn\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"kalkulasiPDD();DateStart()\" style=\"float: right\">Berikutnya</button> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" ng-click=\"kembaliListPDD()\" style=\"float: right\">Kembali</button> </p> <p> <br> </p> <fieldset ng-disabled=\"!disabledLihatPDD\"> <p> <br> </p> <p> <b>No. SPK <br> </b> </p> <p style=\"color: #888b91;margin-top: -10px\">{{selected_data.FormSPKNo}} </p> <p> <b>Tanggal SPK <br> </b> </p> <p style=\"color: #888b91;margin-top: -10px\">{{selected_data.SPKDate | date:\"dd/MM/yyyy\"}} </p> <p> <b>Nama Pelanggan <br> </b> </p> <p style=\"color: #888b91;margin-top: -10px\">{{selected_data.CustomerName}} </p> <p> <b>Model <br> </b> </p> <p style=\"color: #888b91;margin-top: -10px\">{{selected_data.VehicleModelName}} </p> <p> <b>Tipe <br> <b> </b></b></p> <p style=\"color: #888b91;margin-top: -10px\">{{selected_data.KatashikiSuffixCode}} </p> <p> <b>Warna <br> <b> </b></b></p> <p style=\"color: #888b91;margin-top: -10px\">{{selected_data.ColorName}} </p> <p> <b>Status Unit <br> </b> </p> <p style=\"color: #888b91;margin-top: -10px\">{{selected_data.StatusUnitName}} </p> <p> <b>No. RRN <br> </b> </p> <p style=\"color: #888b91;margin-top: -10px\">{{selected_data.RRNNo}} </p> <p> <b>No. Rangka <br> </b> </p> <p style=\"color: #888b91;margin-top: -10px\"> <label id=\"labelNoRangka\">{{selected_data.FrameNo}}</label> </p> <p> <b>Sumber Dana <br> </b> </p> <p style=\"color: #888b91;margin-top: -10px\"> <label id=\"labelPembayaran\">{{selected_data.FundSourceName}}</label> </p> <p> <label> <b>Direct Delivery dari PDC</b> </label> </p> <p style=\"margin-top: -20px\"> <input type=\"checkbox\" name=\"optionYa\" ng-model=\"selected_data.DirectDeliveryPDC\"> </p> <p> <label> <b>Pilih Nomor Polisi ?</b> </label> </p> <p style=\"margin-top: -20px\"> <input ng-disabled=\"disabledNoPolisi\" type=\"checkbox\" name=\"optionYa\" ng-model=\"selected_data.ChoosePoliceNo\"> </p> <p> <label> <b>Off the road ?</b> </label> </p> <p style=\"margin-top: -20px\"> <input ng-disabled=\"!disabledOffTheRoad\" ng-checked=\"selected_data.OnOffTheRoadId==2\" type=\"checkbox\" name=\"optionYa\"> </p> <p> <label> <b>Pengiriman dengan STNK ?</b> </label> </p> <p style=\"margin-top: -20px\"> <input ng-disabled=\"disabledSTNK\" ng-checked=\"checkSentWithSTNK\" type=\"checkbox\" name=\"optionYa\" ng-model=\"selected_data.SentWithSTNK\"> </p> </fieldset> </div> <div ng-show=\"MainPDD\"> <p> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"kalkulasiPDDNorangka();\"> Kalkulasi PDD</button> <!--<button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\"> Batal</button>--> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cancelBuatPDD()\">Kembali</button> </p> <p> <br> </p> <div id=\"viewcalendar\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar03.png\" alt=\"Calendar\" align=\"middle\"></p>--> <p> <br> </p> <div> <!--<pre>Selected date is: <em>{{dt | date:'fullDate' }}</em></pre>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <h4>Inline</h4>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dt1\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> </div> <p> <br> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: mediumpurple\"></i> <i> <b> Tanggal Dokumen Komplit diterima</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"firstDateChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDate.date\" is-open=\"popup1.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open1()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: sandybrown\"></i> <i> <b> Tanggal DP</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"secondDateChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDate2.date\" is-open=\"popup2.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open2()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: lightsteelblue\"></i> <i> <b> Tanggal Full Payment</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"thirdDateChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDate3.date\" is-open=\"popup3.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open3()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> <p> <hr> </p> </div> <div ng-show=\"DetilKalkulasiPDDCash\"> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"openHistoryPDD()\"> History</button> <!--<button id=\"sendEmailBtn\" style=\"float: right;\" onclick=\"OpenModal1()\"> RevisiOK</button>--> <button id=\"sendEmailBtn\" ng-if=\"user.RoleName == 'SALES' && selected_data.StatusPDDName != 'Butuh Konfirmasi'\" style=\"float: right\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"openModalRevision()\"> Revisi</button> <button id=\"sendEmailBtn\" style=\"float: right\" class=\"wbtn btn ng-binding ladda-button\" ng-click=\"kembaliListPDD()\"> Kembali</button> <p> <br> </p> <p> <br> </p> <div class=\"tab-content\"> <div id=\"viewcalendar\" class=\"tab-pane fade in active\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar04.png\" alt=\"Calendar\" align=\"middle\"></p>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dtdetilpdd\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> <p> <br> </p> <div ng-if=\"selected_data.StatusPDDName != 'Butuh Konfirmasi'\"> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: mediumpurple\"></i> <i> <b> Tanggal Dokumen Komplit diterima</b> </i> </div> <div class=\"col-xs-1\"> <b>:</b> </div> <div class=\"col-xs-6\"> <i>{{selected_data.DocCompRecDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <div class=\"row\" ng-if=\"selected_data.FundSourceName == 'Credit'\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: lightskyblue\"></i> <i> <b> Tanggal Approval Leasing</b> </i> </div> <div class=\"col-xs-1\"> <b>:</b> </div> <div class=\"col-xs-6\"> <i>{{selected_data.ApprovalLeasingDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\" ng-if=\"selected_data.FundSourceName == 'Cash'\"> <i class=\"fa fa-square\" style=\"color: sandybrown\"></i> <i> <b> Tanggal DP</b> </i> </div> <div class=\"col-xs-5\" ng-if=\"selected_data.FundSourceName == 'Credit'\"> <i class=\"fa fa-square\" style=\"color: sandybrown\"></i> <i> <b> Tanggal TDP</b> </i> </div> <div class=\"col-xs-1\"> <b>:</b> </div> <div class=\"col-xs-6\" ng-if=\"selected_data.FundSourceName == 'Cash'\"> <i>{{selected_data.DPDate | date:\"dd/MM/yyyy\"}}</i> </div> <div class=\"col-xs-6\" ng-if=\"selected_data.FundSourceName == 'Credit'\"> <i>{{selected_data.TDPDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <div class=\"row\" ng-if=\"selected_data.FundSourceName == 'Cash'\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: lightsteelblue\"></i> <i> <b> Tanggal Full Payment</b> </i> </div> <div class=\"col-xs-1\"> <b>:</b> </div> <div class=\"col-xs-6\"> <i>{{selected_data.FullPaymentDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: darkseagreen\"></i> <i> <b> Tanggal STNK Selesai</b> </i> </div> <div class=\"col-xs-1\"> <b>:</b> </div> <div class=\"col-xs-6\"> <i>{{selected_data.STNKDoneDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: crimson\"></i> <i> <b> Pengiriman Unit</b> </i> </div> <div class=\"col-xs-1\"> <b>:</b> </div> <div class=\"col-xs-6\"> <i>{{selected_data.SentUnitDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <hr> </p> <p> <div class=\"row\"> <div class=\"col-xs-4\"> <b>ETA Pelanggan</b> </div> <div class=\"col-xs-6\"> <p style=\"float: left; text-align: left\"> <b>: {{selected_data.SentUnitTime | date:\"HH:mm\"}} - {{EtaCustomerPlus | date:\"HH:mm\"}}</b> </p> </div> </div> </p> </div> <div ng-if=\"selected_data.StatusPDDName == 'Butuh Konfirmasi'\"> <p> <b>Estimasi Rentang Waktu</b> </p> <p> {{selected_data.EstimateDate}} </p> </div> <hr> <div ng-if=\"selected_data.ReasonRevisionId != null\"> <p> <b>Alasan Revisi</b> </p> <p> {{selected_data.ReasonRevisionName}} </p> <p> <br> </p> <p> <br> </p> </div> <div class=\"ui modal ModalFormRevision\" style=\"background-color: transparent\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\"> <h4 class=\"modal-title\">Alasan Revisi</h4> </div> <div class=\"row\" ng-repeat=\"Revision in listRevision\"> <div class=\"col-xs-1\"> <input type=\"radio\" ng-model=\"$parent.PilihanRevisi\" ng-value=\"Revision\" style=\"height:25px; width:25px; vertical-align: middle\"> </div> <div class=\"col-xs-11\" style=\"margin-top: 10px\"> {{Revision.ReasonRevisionName}} </div> </div> <div class=\"modal-footer\"> <button ng-click=\"Lanjut_Clicked(selected_data)\" ng-disabled=\"PilihanRevisi == null\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Lanjutkan</button> <button ng-click=\"Batal_Clicked()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </div> </div> </div> <!-- Modal 3 Template: Alert Pop Up--> <div class=\"ui modal ModalRevisionAlert\" style=\"background-color: transparent\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\"> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4 class=\"modal-title\">Peringatan</h4> </div> <div ng-if=\"selected_data.StatusPDC=='Belum Dikirim PDC' && selected_data.CountRevisi>=3\" class=\"modal-body\"> <p> Anda tidak bisa melakukan revisi PDD dikarenakan sudah melakukan revisi PDD sebanyak 3 kali. </p> </div> <div ng-if=\"selected_data.StatusPDC=='Sudah Dikirim PDC'\" class=\"modal-body\"> <p> Anda tidak bisa melakukan revisi PDD dikarenakan unit sudah dikirim dari PDC. Silahkan lakukan perubahan rencana Delivery pada menu konfirmasi DEC. </p> </div> <div class=\"modal-footer\"> <p align=\"center\"> <button ng-click=\"OK_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">OK</button> </p> </div> </div> </div> </div> </div> <div ng-show=\"KalkulasiPDDCash\"> <p> <br> </p> <!--<button id=\"saveBtn\" class=\"rbtn btn ng-binding ladda-button\" onclick=\"detailKalkulasiPDDCash()\"> Detail</button>--> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cekSimpanCashNorangka()\"> Simpan</button> <!--<button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\"> Batal</button>--> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cancelPDDCashNorangka();cancelDateChange()\"> Kembali</button> <div id=\"viewcalendar\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar04.png\" alt=\"Calendar\" align=\"middle\"></p>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dt2\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> <p> <br> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: mediumpurple\"></i> <i> <b> Tanggal Dokumen Komplit Diterima</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <i>{{bind_data.data[0].DocCompRecDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: sandybrown\"></i> <i> <b> Tanggal DP</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <i>{{bind_data.data[0].DPDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: lightsteelblue\"></i> <i> <b> Tanggal Full Payment</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <i>{{bind_data.data[0].FullPaymentDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: darkseagreen\"></i> <i> <b> Tanggal STNK Selesai</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <i>{{bind_data.data[0].STNKDoneDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: crimson\"></i> <i> <b> Pengiriman Unit</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"fifthDateChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDate5.date\" is-open=\"popup5.opened\" datepicker-options=\"dateOptions5\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open5()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> <p> <hr> </p> <p> <div> <b>ETA Pelanggan :</b> </div> <div> <!--<i><input type=\"time\" id=\"exampleInput\" name=\"input\" ng-model=\"example.value\"\r" +
    "\n" +
    "                            placeholder=\"HH:mm:ss\" min=\"00:00:00\" max=\"23:59:59\" required /></i>--> <div uib-timepicker ng-model=\"mytime\" ng-change=\"changed()\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" style=\"float: left\"></div> </div> </p> <p> <br> </p> <p> <br> </p> </div> <div ng-show=\"MainRRNCash\"> <p> <button id=\"kalkulasiRRNCashBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"kalkulasiRRNCash();DateStart();kalkulasiDateRRNCash()\"> Kalkulasi PDD</button> <!--<button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\"> Batal</button>--> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cancelBuatPDD()\"> Kembali</button> </p> <p> <br> </p> <div id=\"viewcalendar\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar05.png\" alt=\"Calendar\" align=\"middle\"></p>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dt3\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> <p> <br> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: mediumpurple\"></i> <i> <b> Tanggal Dokumen Komplit Diterima</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"firstDateRRNChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDateRRN.date\" is-open=\"popupRRN1.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"openRRN1()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: sandybrown\"></i> <i> <b> Tanggal DP</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"secondDateRRNChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDateRRN2.date\" is-open=\"popupRRN2.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"openRRN2()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: lightsteelblue\"></i> <i> <b> Tanggal Full Payment</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"thirdDateRRNChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDateRRN3.date\" is-open=\"popupRRN3.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"openRRN3()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> <p> <hr> </p> <p> <br> </p> </div> <div ng-show=\"KalkulasiRRNCash\"> <p> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cekSimpanCashRRN()\"> Simpan</button> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cancelkalkulasiRRNCash();DateStart();\"> Kembali</button> </p> <p> <br> </p> <div id=\"viewcalendar\" class=\"tab-pane fade in active\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar06.png\" alt=\"Calendar\" align=\"middle\"></p>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dt4\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> <p> <br> </p> <p> <br> </p> <p> <b>Estimasi Rentang Waktu</b> </p> <p> {{bind_data.data[0].EstimateDate}} </p> <p> <br> </p> <p> <br> </p> <p> <hr> </p> <p> </p> <p> <br> </p> <p> <br> </p> </div> <div ng-show=\"MainPDDKredit\"> <p> <button id=\"kalkulasiPDDKreditBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"kalkulasiPDDNorangkaCredit()\"> Kalkulasi PDD</button> <!--<button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\"> Batal</button>--> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cancelBuatPDD()\"> Kembali</button> </p> <p> <br> </p> <div id=\"viewcalendar\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar07.png\" alt=\"Calendar\" align=\"middle\"></p>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dt1\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: mediumpurple\"></i> <i> <b> Tanggal Dokumen Komplit Diterima</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -15px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"firstDateChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDate.date\" is-open=\"popup1.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open1()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: sandybrown\"></i> <i> <b> Tanggal TDP</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"secondDateChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDate2.date\" is-open=\"popup2.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open2()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> <p> <hr> </p> <p> <br> </p> </div> <div ng-show=\"KalkulasiPDDKredit\"> <p> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"simpanPDDCredit()\"> Simpan</button> <!--<button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\" onclick=\"cancelKalkulasiKredit()\"> Batal</button>--> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cancelPDDNorangkaCredit()\"> Kembali</button> </p> <p> <br> </p> <div id=\"viewcalendar\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar08.png\" alt=\"Calendar\" align=\"middle\"></p>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dt4\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: mediumpurple\"></i> <i> <b> Tanggal Dokumen Komplit Diterima</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <i>{{bind_data.data[0].DocCompRecDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: lightskyblue\"></i> <i> <b> Tanggal Approval Leasing</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <i>{{bind_data.data[0].ApprovalLeasingDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: sandybrown\"></i> <i> <b> Tanggal TDP</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <i>{{bind_data.data[0].TDPDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: darkseagreen\"></i> <i> <b> Tanggal STNK Jadi</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <i>{{bind_data.data[0].STNKDoneDate | date:\"dd/MM/yyyy\"}}</i> </div> </div> </p> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: crimson\"></i> <i> <b> Pengiriman Unit</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"fifthDateChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDate5.date\" is-open=\"popup5.opened\" datepicker-options=\"dateOptions5\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open5()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> <p> <br> </p> <p> <hr> </p> <p> <div> <b>ETA Pelanggan :</b> </div> <div> <!--<i><input type=\"time\" id=\"exampleInput\" name=\"input\" ng-model=\"example.value\"\r" +
    "\n" +
    "                            placeholder=\"HH:mm:ss\" min=\"00:00:00\" max=\"23:59:59\" required /></i>--> <div uib-timepicker ng-model=\"mytime2\" ng-change=\"changed()\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" style=\"float: left\"></div> </div> </p> <p> <br> </p> <p> <br> </p> </div> <div ng-show=\"MainRRNKredit\"> <p> <button id=\"RRNKreditBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"kalkulasiRRNCredit()\"> Kalkulasi PDD</button> <!--<button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\"> Batal</button>--> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cancelBuatPDD()\"> Kembali</button> </p> <p> <br> </p> <div id=\"viewcalendar\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar09.png\" alt=\"Calendar\" align=\"middle\"></p>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dt4\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> <p> <br> </p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: mediumpurple\"></i> <i> <b> Tanggal Dokumen Komplit Diterima</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"firstDateRRNChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDateRRN.date\" is-open=\"popup1.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" close-text=\"Close\" popup-placement=\"auto\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open1()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> <p> <br> </p> <p> <div class=\"row\"> <div class=\"col-xs-5\"> <i class=\"fa fa-square\" style=\"color: sandybrown\"></i> <i> <b> Tanggal TDP</b> </i> </div> <div class=\"col-xs-1\" style=\"margin-left: -15px\"> <b>:</b> </div> <div class=\"col-xs-6\" style=\"margin-left: -20px\"> <p class=\"input-group\"> <input date-disabled=\"disabledWeekend(date, mode)\" type=\"text\" class=\"form-control\" ng-change=\"secondDateRRNChange()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"testDateRRN2.date\" is-open=\"popup2.opened\" datepicker-options=\"dateOptions\" ng-required=\"true\" popup-placement=\"auto\" close-text=\"Close\" alt-input-formats=\"altInputFormats\"> <span class=\"input-group-btn\"> <button type=\"button\" class=\"btn btn-default\" ng-click=\"open2()\"> <i class=\"glyphicon glyphicon-calendar\"></i> </button> </span> </p> </div> </div> </p> <p> <br> </p> </div> <div ng-show=\"KalkulasiRRNKredit\"> <p> <button id=\"sendEmailBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"simpanRRNCredit()\"> Simpan</button> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\" ng-click=\"cancelkalkulasiRRNCredit()\"> Kembali</button> </p> <p> <br> </p> <div id=\"viewcalendar\"> <!--<p style=\"text-align:center;\"><img src=\"../images/app/pdd/Gambar10.png\" alt=\"Calendar\" align=\"middle\"></p>--> <div style=\"display:inline-block; min-height:290px;font-size: 20px\"> <div ng-if=\"refreshCalendar\"> <div ng-disabled=\"true\" uib-datepicker ng-model=\"dt4\" class=\"well well-sm\" datepicker-options=\"options\"></div> </div> </div> </div> <p> <br> </p> <p> <br> </p> <p> <b>Estimasi Rentang Waktu</b> </p> <p> {{bind_data.data[0].EstimateDate}} </p> <p> <br> </p> <p> <br> </p> <p> <hr> </p> <p> </p> <p> <br> </p> <p> <br> </p> </div> <div ng-show=\"HistoryPDD\"> <p> <button id=\"sendEmailBtn\" class=\"wbtn btn ng-binding ladda-button\" ng-click=\"cancelHistoryPDD()\" style=\"float: right\"> Kembali</button> </p> <p> <br> </p> <p> <br> </p> <div class=\"listData\" ng-repeat=\"listHistory in HistoryPDD\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtn\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Tanggal Pembuatan : {{listHistory.PDDDate | date:\"dd/MM/yyyy\"}} </button> <div ng-if=\"listHistory.FrameNo != null\"> <div class=\"list-group\" id=\"dropdownContentOverideProspect\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\"> <b> <ul class=\"list-inline\"> <li style=\"width:100px\">{{listHistory.DocCompRecDate | date:\"dd/MM/yyyy\"}} </li> <li>:</li> <li>Tanggal Dokumen Komplit Diterima</li> </ul> </b> </div> </div> <div ng-if=\"selected_data.FundSourceName == 'Credit'\" class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\"> <b> <ul class=\"list-inline\"> <li style=\"width:100px\" ng-if=\"selected_data.FundSourceName == 'Credit'\">{{listHistory.ApprovalLeasingDate | date:\"dd/MM/yyyy\"}} </li> <li>:</li> <li ng-if=\"selected_data.FundSourceName == 'Credit'\">Tanggal Approval Leasing</li> </ul> </b> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\"> <b> <ul class=\"list-inline\"> <li style=\"width:100px\" ng-if=\"selected_data.FundSourceName == 'Cash'\">{{listHistory.DPDate | date:\"dd/MM/yyyy\"}} </li> <li style=\"width:100px\" ng-if=\"selected_data.FundSourceName == 'Credit'\">{{listHistory.TDPDate | date:\"dd/MM/yyyy\"}} </li> <li>:</li> <li ng-if=\"selected_data.FundSourceName == 'Cash'\">Tanggal DP</li> <li ng-if=\"selected_data.FundSourceName == 'Credit'\">Tanggal TDP</li> </ul> </b> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\" ng-if=\"selected_data.FundSourceName == 'Cash'\"> <div style=\"font-size: 12px\"> <b> <ul class=\"list-inline\"> <li style=\"width:100px\">{{listHistory.FullPaymentDate | date:\"dd/MM/yyyy\"}} </li> <li>:</li> <li>Tanggal Full Payment</li> </ul> </b> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\"> <b> <ul class=\"list-inline\"> <li style=\"width:100px\">{{listHistory.STNKDoneDate | date:\"dd/MM/yyyy\"}} </li> <li>:</li> <li>Tanggal STNK Selesai</li> </ul> </b> </div> </div> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\"> <b> <ul class=\"list-inline\"> <li style=\"width:100px\">{{listHistory.SentUnitDate | date:\"dd/MM/yyyy\"}} </li> <li>:</li> <li>Pengiriman Unit</li> </ul> </b> </div> </div> </div> </div> <div ng-if=\"listHistory.FrameNo == null\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\"> <b> <ul class=\"list-inline\"> <li style=\"width:100px\">Est. Rentang Waktu</li> <li>:</li> <li>{{listHistory.EstimateDate}}</li> </ul> </b> </div> </div> </div> <p> <br> </p> <div ng-if=\"listHistory.ReasonRevisionName != null\"> <p> <b>Alasan Revisi</b> </p> <p> {{listHistory.ReasonRevisionName}} </p> </div> <hr style=\"height:1px;border:none;color:#333;background-color:#333\"> </div> <hr> </div> <div class=\"ui modal SavePDDNoRangka\"> <div class=\"modal-header\"> <h4 class=\"modal-title\">Peringatan</h4> </div> <div class=\"modal-body\"> <ul class=\"list-group\"> <p> <b> Tanggal pengiriman unit lebih kecil dari full payment date, yakin akan disimpan ? </b> </p> </ul> </div> <div class=\"modal-footer\"> <button ng-click=\"YaSaveNorangka()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"TidakSaveNorangka()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div> <div class=\"ui modal SavePDDRRN\"> <div class=\"modal-header\"> <h4 class=\"modal-title\">Peringatan</h4> </div> <div class=\"modal-body\"> <ul class=\"list-group\"> <p> <b> Tanggal pengiriman unit lebih kecil dari full payment date, yakin akan disimpan ? </b> </p> </ul> </div> <div class=\"modal-footer\"> <button ng-click=\"YaSaveRRN()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"TidakSaveRRN()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales3/pdd/maintainPDD/rowTemplate.html',
    "<!--Harus item--> <div style=\"font-size: 12px\"> <b>{{item.CustomerName}}</b> <button ng-click=\"openModal(item)\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:transparent; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:12px\"> <li><b>No. SPK : </b>{{item.FormSPKNo}}</li> <li><b>No. Rangka / RRN : </b>{{item.FrameNo}} / {{item.RRNNo}}</li> <li><b>Status : </b>{{item.StatusPDDName}}</li> </ul> </p>"
  );

}]);
