angular.module('templates_appbranchorder', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/branchorder/getsudoPeriod/getsudoPeriod.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">fieldset.detail-border {\r" +
    "\n" +
    "    border: 1px solid #e2e2e2 !important;\r" +
    "\n" +
    "    padding: 0 1.4em 1.4em 1.4em !important;\r" +
    "\n" +
    "    margin: 0 0 1.5em 0 !important;\r" +
    "\n" +
    "    -webkit-box-shadow:  0px 0px 0px 0px #000;\r" +
    "\n" +
    "            box-shadow:  0px 0px 0px 0px #000;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "legend.detail-border {\r" +
    "\n" +
    "    font-size: 1.2em !important;\r" +
    "\n" +
    "    font-weight: 400 !important;\r" +
    "\n" +
    "    text-align: left !important;\r" +
    "\n" +
    "    width:auto;\r" +
    "\n" +
    "    padding:0 10px;\r" +
    "\n" +
    "    border-bottom:none;\r" +
    "\n" +
    "}</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"GetsudoPeriodForm\" factory-name=\"GetsudoPeriod\" model=\"mPeriod\" model-id=\"GetsudoPeriodId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xGetsudoPeriod.selected\" on-select-rows=\"onSelectRows\" rights-byte=\"myRights\" grid-hide-action-column=\"actionHide\" on-show-detail=\"onShowDetail\" on-before-cancel=\"onBeforeCancel\" form-name=\"GetsudoPeriodForm\" form-title=\"Getsudo Period\" modal-title=\"Getsudo Period\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-detail> <div ng-show=\"showDetailForm\"> <fieldset class=\"detail-border\"> <legend class=\"detail-border\">Search <span> <i ng-class=\"{ 'fa fa-fw fa-eye-slash':(searchStat==true),\r" +
    "\n" +
    "                                           'fa fa-fw fa-eye':(searchStat==false),\r" +
    "\n" +
    "                                         }\" ng-click=\"toggleSearch()\"></i> </span> </legend> <div ng-show=\"searchStat\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Getsudo Period : ( {{Criteria.prdCode}} ) {{Criteria.prdName}}</label> </div> <div class=\"form-group\"> <label>Area</label> <bsselect name=\"getsudoArea\" ng-model=\"Criteria.areaid\" data=\"areaData\" item-text=\"Description\" item-value=\"AreaId\" placeholder=\"Pilih Area...\" on-select=\"selectPeriod(selected)\" icon=\"fa fa-building\"> </bsselect> </div> <div class=\"form-group\"> <label>Province</label> <bsselect name=\"getsudoPrd\" ng-model=\"Criteria.provinceid\" data=\"provinceData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Province...\" on-select=\"selectPeriod(selected)\" icon=\"fa fa-globe\"> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Year : {{Criteria.xYear}}</label> </div> <div class=\"form-group\"> <label>Dealer</label> <bsselect name=\"getsudoDealer\" ng-model=\"Criteria.dealerid\" data=\"dealerData\" item-text=\"GroupDealerCode,GroupDealerName\" item-value=\"GroupDealerId\" placeholder=\"Pilih Dealer...\" on-select=\"selectDealer(selected)\" icon=\"fa fa-automobile\"> </bsselect> </div> <div class=\"form-group\"> <label>Outlet</label> <bsselect name=\"getsudoPrd\" ng-model=\"Criteria.outletid\" data=\"outletData\" item-text=\"OutletCode,Name\" item-value=\"OutletId\" placeholder=\"Pilih Outlet...\" on-select=\"selectPeriod(selected)\" icon=\"fa fa-taxi\"> </bsselect> </div> </div> </div> <div class=\"row\" style=\"margin-right:0px\"> <div class=\"pull-right\"> <div class=\"btn-group\" style=\"\"> <button class=\"btn wbtn\" style=\"\" ng-click=\"getDataDetail()\" onclick=\"this.blur()\"> <i class=\"fa fa-fw fa-search\" style=\"font-size:1.4em\"></i> </button> </div> </div> </div> </div> <!--                    {{Criteria}}--> </fieldset> <div class=\"ui icon info message no-animate\" ng-show=\"!showDetailPanel\" ng-cloak> <i class=\"fa fa-info-circle fa-2x fa-fw\"></i> <div class=\"content\"> <div class=\"header\">Please Select Criteria of Data to Show.</div> </div> </div> <form class=\"form-inline\" role=\"form\" ng-show=\"showDetailPanel\"> <div class=\"row\" style=\"margin-right:0px;padding-bottom:5px\"> <div class=\"col-md-12\" style=\"padding-right:0px\"> <div class=\"input-group\"> <div class=\"input-group-btn\" uib-dropdown>'+ <button style=\"margin-right:5px\" ng-disabled=\"!selectedDetailRows.length>0\" type=\"button\" class=\"btn wbtn\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> Bulk Action&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\"><a href=\"#\" ng-click=\"actOpenAll(selectedDetailRows)\">Open Periods</a></li> <li role=\"menuitem\"><a href=\"#\" ng-click=\"actCloseAll(selectedDetailRows)\">Close Periods</a></li> </ul> </div> <input type=\"text\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"gridApiDetail.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridDetailCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\" ng-show=\"\r" +
    "\n" +
    "                                                          gridApiDetail.grid.columns[{{$index+1}}].filters[0].term!='' &&\r" +
    "\n" +
    "                                                          gridApiDetail.grid.columns[{{$index+1}}].filters[0].term!=undefined\r" +
    "\n" +
    "                                                \"></i> </span> </a> </li> </ul> </div> </div> </div> </div> </form> <div id=\"grid1\" ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight()\" ng-show=\"showDetailPanel\"> <div class=\"ui-grid-nodata\" ng-if=\"!gridDetail.data.length && !loadingDetail\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loadingDetail\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </div> </bsform-detail> <div ng-show=\"!showDetailForm\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <!-- <bslabel>Period Code</bslabel> --> <label>Getsudo Period : ( {{Criteria.prdCode}} ) {{Criteria.prdName}}</label> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-calendar-o\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"GetsudoPeriodCode\" placeholder=\"Period Code\" ng-model=\"mPeriod.GetsudoPeriodCode\" ng-maxlength=\"50\" required capitalize>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"GetsudoPeriodCode\"></bserrmsg> --> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Period Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-calendar\"></i> <input type=\"text\" class=\"form-control\" name=\"GetsudoPeriodName\" placeholder=\"Period Name\" ng-model=\"mPeriod.GetsudoPeriodName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GetsudoPeriodName\"></bserrmsg> </div> </div> </div> <!-- ng-change=\"startDateChanged(dt)\" --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Start Date</bsreqlabel> <bsdatepicker name=\"StartPeriod\" date-options=\"startDateOptions\" ng-model=\"mPeriod.StartPeriod\" ng-change=\"startDateChange(dt)\"> </bsdatepicker> <bserrmsg field=\"StartPeriod\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>End Date</bsreqlabel> <bsdatepicker name=\"EndPeriod\" date-options=\"endDateOptions\" ng-model=\"mPeriod.EndPeriod\" min-date=\"mPeriod.StartPeriod\" ng-change=\"endDateChange(dt)\"> </bsdatepicker> <bserrmsg field=\"EndPeriod\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Start Edit Date</bsreqlabel> <bsdatepicker name=\"startEditDate\" date-options=\"startEditDateOptions\" ng-model=\"mPeriod.EditStartPeriod\" min-date=\"mPeriod.StartPeriod\" max-date=\"mPeriod.EndPeriod\" ng-change=\"startEditDateChange(dt)\"> </bsdatepicker> <bserrmsg field=\"EditStartPeriod\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>End Edit Date</bsreqlabel> <bsdatepicker name=\"EditEndPeriod\" date-options=\"endEditDateOptions\" ng-model=\"mPeriod.EditEndPeriod\" min-date=\"mPeriod.EditStartPeriod\" max-date=\"mPeriod.EndPeriod\" ng-change=\"endEditDateChange(dt)\"> </bsdatepicker> <bserrmsg field=\"EditEndPeriod\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Start HO Review Date</bsreqlabel> <bsdatepicker name=\"StartDateHOReview\" date-options=\"startHOReviewDateOptions\" ng-model=\"mPeriod.StartDateHOReview\" min-date=\"mPeriod.EditStartPeriod\" max-date=\"mPeriod.EditEndPeriod\" ng-change=\"startHOReviewDateChange(dt)\"> </bsdatepicker> <bserrmsg field=\"StartDateHOReview\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>End Date</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Period Name\" ng-model=\"mPeriod.name\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"name\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> </div> <!-- <pre>{{mPeriod}}</pre> --> </div> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/maintenanceStatus/maintenanceStatus.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"MaintenanceForm\" factory-name=\"MaintenanceStatus\" model=\"mMaintenanceStatus\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xMaintenanceStatus.selected\" on-select-rows=\"onSelectRows\" on-show-detail=\"onShowDetail\" grid-hide-action-column=\"actionHide\" form-name=\"MaintenanceForm\" form-title=\"Maintenance Status\" modal-title=\"Maintenance Status\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" custom-bulk-settings=\"customBulkSettings\"> <!-- ng-show=\"showDetailForm\" --> <!-- ng-show=\"showDetailPanel\" --> <!-- <bsform-detail> --> <!-- </bsform-detail> --> </bsform-grid> <bsform-grid-detail> <div class=\"row\" ng-show=\"showDetailForm\"> <form class=\"form-inline\" role=\"form\" ng-show=\"showDetailPanel\"> <div class=\"row\" style=\"margin-right:0px;padding-bottom:5px;margin-left:5px\"> <div class=\"col-md-12\" style=\"padding-right:0px\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"gridApiFormDetail.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridFormDetailCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\" ng-show=\"\r" +
    "\n" +
    "                                                          gridApiFormDetail.grid.columns[{{$index+1}}].filters[0].term!='' &&\r" +
    "\n" +
    "                                                          gridApiFormDetail.grid.columns[{{$index+1}}].filters[0].term!=undefined\r" +
    "\n" +
    "                                                \"></i> </span> </a> </li> </ul> </div> </div> </div> </div> </form> <div id=\"grid1\" ui-grid=\"gridFormDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight();\" ng-show=\"showDetailPanel\"> <div class=\"ui-grid-nodata\" ng-if=\"!gridForfDetail.data.length\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loadingDetail\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </div> </bsform-grid-detail> <bsform-detail> <div class=\"row\"> <form class=\"form-inline\" role=\"form\"> <div class=\"row\" style=\"margin-right:0px;padding-bottom:5px;margin-left:5px\"> <div class=\"col-md-12\" style=\"padding-right:0px\"> <div class=\"input-group\"> <div class=\"input-group-btn\" uib-dropdown>'+ <button style=\"margin-right:5px\" ng-disabled=\"!selectedDetailRows.length>0\" type=\"button\" class=\"btn wbtn\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> Bulk Action&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\"><a href=\"#\" ng-click=\"onStartJob(selectedDetailRows)\">Start</a></li> <li role=\"menuitem\"><a href=\"#\" ng-click=\"onLogJob(selectedDetailRows)\">Log</a></li> </ul> </div> <input type=\"text\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"gridApiDetail.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridDetailCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\" ng-show=\"\r" +
    "\n" +
    "                                                          gridApiDetail.grid.columns[{{$index+1}}].filters[0].term!='' &&\r" +
    "\n" +
    "                                                          gridApiDetail.grid.columns[{{$index+1}}].filters[0].term!=undefined\r" +
    "\n" +
    "                                                \"></i> </span> </a> </li> </ul> </div> </div> </div> </div> </form> <div id=\"grid1\" ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight();\" style=\"margin-left:18px;margin-bottom:40px\"> <div class=\"ui-grid-nodata\" ng-if=\"!gridDetail.data.length\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loadingDetail\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </div> </bsform-detail>"
  );


  $templateCache.put('app/branchorder/marketadj/marketadj.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"MarketAdjForm\" factory-name=\"MarketAdj\" model=\"mMarketAdj\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xMarketAdj.selected\" custom-action-settings=\"customActionSettings\" form-api=\"formApi\" on-after-register-grid-api=\"afterRegisterGridApi\" hide-new-button=\"true\" show-advsearch=\"on\" grid-inline-edit=\"true\" on-show-detail=\"onShowDetail\" on-select-rows=\"onSelectRows\" on-before-save=\"onBeforeSave\" modal-title=\"Market Adjustment\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Tahun</bsreqlabel> <bsselect name=\"year\" ng-model=\"filter.year\" data=\"yearData\" item-text=\"name\" item-value=\"name\" placeholder=\"Pilih Tahun\" required icon=\"fa fa-calendar\"> </bsselect> </div> <div class=\"form-group\"> <label>Model</label> <bsselect name=\"productModelId\" ng-model=\"filter.VehicleModelId\" data=\"productModelData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Model\" icon=\"fa fa-car\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Area</label> <bsselect name=\"areaId\" ng-model=\"filter.areaId\" data=\"areaData\" item-text=\"Description\" item-value=\"AreaId\" placeholder=\"Pilih Area\" ng-change=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-sitemap\"> </bsselect> </div> <div class=\"form-group\"> <label>Province</label> <bsselect name=\"regionId\" ng-model=\"filter.provinceId\" data=\"provinceData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Region\" ng-change=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-sitemap\"> </bsselect> </div> </div> <input type=\"file\" accept=\".xlsx\" id=\"uploadMarketAdj\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </div> </bsform-advsearch>  </bsform-grid>"
  );


  $templateCache.put('app/branchorder/oap/oap.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"OAPForm\" factory-name=\"OAP\" model=\"mOAP\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xOAP.selected\" custom-action-settings=\"customActionSettings\" form-grid-api=\"gridApi\" on-after-register-grid-api=\"afterRegisterGridApi\" form-api=\"formApi\" hide-new-button=\"true\" show-advsearch=\"on\" grid-inline-edit=\"true\" rights-byte=\"myRights\" on-show-detail=\"onShowDetail\" on-select-rows=\"onSelectRows\" on-before-save=\"onBeforeSave\" modal-title=\"OAP\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Tipe</bsreqlabel> <bsradiobox ng-model=\"filter.typeId\" options=\"typeData\" layout=\"V\"> </bsradiobox> </div> <div class=\"form-group\"> <bsreqlabel>Tahun</bsreqlabel> <bsselect name=\"year\" ng-model=\"filter.year\" data=\"yearData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Tahun\" on-select=\"selectYear(selected)\" required icon=\"fa fa-calendar\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" ng-disabled=\"orgData[0].child.length==1\"> <bsreqlabel>Dealer Outlet</bsreqlabel> <bsselect-tree name=\"outletId\" ng-model=\"filter.outletId\" data=\"orgData\" placeholder=\"Pilih Outlet Dealer\" on-select=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-sitemap\"> </bsselect-tree> </div> <div class=\"form-group\" ng-if=\"filter.typeId<3\"> <bsreqlabel>Field</bsreqlabel> <bsselect name=\"field\" ng-model=\"filter.fieldId\" data=\"fieldData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Field\" on-select=\"selectField(selected)\" required> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Area</label> <bsselect name=\"areaId\" ng-model=\"filter.areaId\" data=\"areaData\" item-text=\"Description\" item-value=\"AreaId\" placeholder=\"Pilih Area\" ng-change=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-sitemap\"> </bsselect> </div> <div class=\"form-group\"> <label>Province</label> <bsselect name=\"regionId\" ng-model=\"filter.provinceId\" data=\"provinceData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Region\" ng-change=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-sitemap\"> </bsselect> </div> </div> <input type=\"file\" accept=\".xlsx\" id=\"uploadOAP\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </div> </bsform-advsearch>  </bsform-grid>"
  );


  $templateCache.put('app/branchorder/oapPeriod/oapPeriod.html',
    "<script type=\"text/javascript\"></script> <!--         '<div class=\"angucomplete-holder\" ng-class=\"{\\'angucomplete-dropdown-visible\\': showDropdown}\">' +\r" +
    "\n" +
    "        '  <input id=\"{{id}}_value\" name=\"{{inputName}}\" tabindex=\"{{fieldTabindex}}\"\r" +
    "\n" +
    "                    ng-class=\"{\\'angucomplete-input-not-empty\\': notEmpty}\"\r" +
    "\n" +
    "                    ng-model=\"searchStr\"\r" +
    "\n" +
    "                    ng-disabled=\"disableInput\"\r" +
    "\n" +
    "                    type=\"{{inputType}}\"\r" +
    "\n" +
    "                    placeholder=\"{{placeholder}}\"\r" +
    "\n" +
    "                    maxlength=\"{{maxlength}}\"\r" +
    "\n" +
    "                    ng-focus=\"onFocusHandler()\"\r" +
    "\n" +
    "                    class=\"{{inputClass}}\"\r" +
    "\n" +
    "                    ng-focus=\"resetHideResults()\"\r" +
    "\n" +
    "                    ng-blur=\"hideResults($event)\"\r" +
    "\n" +
    "                    autocapitalize=\"off\"\r" +
    "\n" +
    "                    autocorrect=\"off\"\r" +
    "\n" +
    "                    autocomplete=\"off\"\r" +
    "\n" +
    "                    ng-change=\"inputChangeHandler(searchStr)\"/>' +\r" +
    "\n" +
    "        '  <div id=\"{{id}}_dropdown\" class=\"angucomplete-dropdown\" ng-show=\"showDropdown\">' +\r" +
    "\n" +
    "        '    <div class=\"angucomplete-searching\" ng-show=\"searching\" ng-bind=\"textSearching\"></div>' +\r" +
    "\n" +
    "        '    <div class=\"angucomplete-searching\" ng-show=\"!searching && (!results || results.length == 0)\" ng-bind=\"textNoResults\"></div>' +\r" +
    "\n" +
    "        '    <div class=\"angucomplete-row\" ng-repeat=\"result in results\" ng-click=\"selectResult(result)\" ng-mouseenter=\"hoverRow($index)\" ng-class=\"{\\'angucomplete-selected-row\\': $index == currentIndex}\">' +\r" +
    "\n" +
    "        '      <div ng-if=\"imageField\" class=\"angucomplete-image-holder\">' +\r" +
    "\n" +
    "        '        <img ng-if=\"result.image && result.image != \\'\\'\" ng-src=\"{{result.image}}\" class=\"angucomplete-image\"/>' +\r" +
    "\n" +
    "        '        <div ng-if=\"!result.image && result.image != \\'\\'\" class=\"angucomplete-image-default\"></div>' +\r" +
    "\n" +
    "        '      </div>' +\r" +
    "\n" +
    "        '      <div class=\"angucomplete-title\" ng-if=\"matchClass\" ng-bind-html=\"result.title\"></div>' +\r" +
    "\n" +
    "        '      <div class=\"angucomplete-title\" ng-if=\"!matchClass\">{{ result.title }}</div>' +\r" +
    "\n" +
    "        '      <div ng-if=\"matchClass && result.description && result.description != \\'\\'\" class=\"angucomplete-description\" ng-bind-html=\"result.description\"></div>' +\r" +
    "\n" +
    "        '      <div ng-if=\"!matchClass && result.description && result.description != \\'\\'\" class=\"angucomplete-description\">{{result.description}}</div>' +\r" +
    "\n" +
    "        '    </div>' +\r" +
    "\n" +
    "        '  </div>' +\r" +
    "\n" +
    "        '</div>' --> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"OAPPeriodForm\" factory-name=\"OAPPeriod\" model=\"mPeriod\" loading=\"loading\" model-id=\"MasterPeriodOAPRAPId\" get-data=\"getData\" selected-rows=\"xPeriod.selected\" on-select-rows=\"onSelectRows\" form-name=\"OAPPeriod\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Period Type</bsreqlabel> <!-- on-select=\"selectPeriodType(selected)\" --> <bsselect name=\"periodTypeSelect\" ng-model=\"mPeriod.MasterPeriodOAPRAPType\" data=\"periodTypeData\" item-text=\"type\" item-value=\"MasterPeriodOAPRAPType\" placeholder=\"Select Period Type...\" required icon=\"fa fa-calendar\"> </bsselect> <bserrmsg field=\"periodTypeSelect\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Year</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-calendar-o\"></i> <input type=\"number\" class=\"form-control\" name=\"year\" placeholder=\"Year\" ng-model=\"mPeriod.Year\"> </div> <bserrmsg field=\"year\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Period Name</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-calendar\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Period Name\" ng-model=\"mPeriod.name\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"name\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> </div> <!-- ng-change=\"startDateChanged(dt)\" --> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Start Date</bsreqlabel> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"mPeriod.StartDate\"> </bsdatepicker> <bserrmsg field=\"startDate\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>End Date</bsreqlabel> <bsdatepicker name=\"endDate\" date-options=\"dateOptions\" ng-model=\"mPeriod.EndDate\" min-date=\"mPeriod.StartDate\"> </bsdatepicker> <bserrmsg field=\"endDate\"></bserrmsg> </div> </div> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Description</bsreqlabel>\r" +
    "\n" +
    "            <div class=\"input-icon-right\">\r" +
    "\n" +
    "                <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mPeriod.desc\" required>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <bserrmsg field=\"desc\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/outlet/internaloutlet.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"InternalOutletForm\" model-id=\"OutletTAMId\" factory-name=\"InternalOutlet\" model=\"mIntOutlet\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xIntOutlet.selected\" on-before-cancel=\"onBeforeCancel\" on-before-new-mode=\"onBeforeNewMode\" form-name=\"InternalOutletForm\" form-title=\"Internal Outlet\" modal-title=\"Internal Outlet\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Outlet</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"code\" placeholder=\"Kode Outlet\" ng-model=\"mIntOutlet.OutletCode\" required> </div> <bserrmsg field=\"code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Outlet</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Outlet\" ng-model=\"mIntOutlet.Name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Area</bsreqlabel> <bsselect name=\"areaId\" ng-model=\"mIntOutlet.AreaId\" data=\"areaData\" placeholder=\"Pilih Area\" item-value=\"AreaId\" item-text=\"Description\" required ng-change=\"changeArea(selected)\" icon=\"fa fa-sitemap\"> </bsselect> <!--  --> <bserrmsg field=\"area\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Kota</bsreqlabel> <!-- <bsselect-tree name=\"CityRegencyId\"\r" +
    "\n" +
    "                            ng-model=\"mIntOutlet.CityRegencyId\"\r" +
    "\n" +
    "                            data=\"provinceData\"                            \r" +
    "\n" +
    "                            placeholder=\"Pilih Kota\"\r" +
    "\n" +
    "                            required=\"true\"\r" +
    "\n" +
    "                            icon=\"fa fa-sitemap\"\r" +
    "\n" +
    "                            on-select=\"changeProvince(selected)\" \r" +
    "\n" +
    "                            >\r" +
    "\n" +
    "                    </bsselect-tree>  --><!-- data=\"regionData\" --> <bsselect-tree-ex name=\"regionId\" ng-model=\"mIntOutlet.CityRegencyId\" data=\"provinceData\" on-select=\"changeProvince(selected)\" tree-id-level1=\"IslandId\" tree-id-level2=\"ProvinceId\" tree-id-level3=\"CityRegencyId\" tree-text-level1=\"IslandName\" tree-text-level2=\"ProvinceName\" tree-text-level3=\"CityRegencyName\" tree-child-level1=\"listProvinceCity\" tree-child-level2=\"listCityRegency\" tree-mode=\"1\" target-level=\"3\" expand-level=\"3\" placeholder=\"Pilih Kota\" required icon=\"fa fa-sitemap\"> <!-- tree-text2-level1=\"IslandId\" \r" +
    "\n" +
    "                                 tree-text2-level2=\"ProvinceId\"\r" +
    "\n" +
    "                                 tree-text2-level3=\"CityRegencyId\"\r" +
    "\n" +
    "                                  --> </bsselect-tree-ex> <bserrmsg field=\"CityRegencyId\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Telepon</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"phone\" placeholder=\"Telepon\" ng-model=\"mIntOutlet.PhoneNumber\"> <!-- [0-9.^abc] pattern=\"(?=.*\\d)(?=.*\\s)(?=.*\\W)\"--> </div> <bserrmsg field=\"phone\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"address\" placeholder=\"Alamat\" ng-model=\"mIntOutlet.Address\" rows=\"5\" required></textarea> </div> <bserrmsg field=\"address\"></bserrmsg> </div> </div> </div> <!-- <pre> {{mIntOutlet}} </pre> --> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/parameter/parameter.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"ParameterForm\" factory-name=\"Parameter\" model=\"mParameter\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selParameter.selected\" on-select-rows=\"onSelectRows\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Parameter Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"code\" placeholder=\"Parameter Code\" ng-model=\"mParameter.code\" required capitalize> </div> <bserrmsg field=\"code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Parameter Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Parameter Name\" ng-model=\"mParameter.name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Description</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mParameter.desc\" required> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/report/report.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"BORReportForm\" factory-name=\"BORReport\" model=\"mBORReport\" loading=\"loading\" custom-action-settings=\"customActionSettings\" hide-new-button=\"true\" show-advsearch=\"on\" modal-title=\"Report\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Report</bsreqlabel> <bsselect name=\"report\" ng-model=\"report.Type\" data=\"reportData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Report\" required> </bsselect> </div> </div> </div> </bsform-advsearch> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/report/reportFilter.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"reportFilterForm\" factory-name=\"ReportFilter\" model=\"mreportFilter\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xreportFilter.selected\" rights-byte=\"myRights\" show-advsearch=\"on\" on-show-detail=\"onShowDetail\" modal-title=\"reportFilter\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Report</label> <bsselect name=\"getReport\" ng-model=\"mreportFilter.reportId\" data=\"report\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Report\" on-select=\"selectReport(selected)\" icon=\"fa fa-globe\"> </bsselect> </div> </div> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==1 || mreportFilter.reportId==2 || mreportFilter.reportId==5 || mreportFilter.reportId==6\"> <div class=\"form-group\"> <bsreqlabel>GetSudo Period</bsreqlabel> <bsselect name=\"getsudoPrd\" ng-model=\"mreportFilter.GetsudoPeriodId\" data=\"getSudoPrdData\" item-text=\"GetsudoPeriodName\" item-value=\"GetsudoPeriodId\" placeholder=\"Pilih Getsudo Period\" on-select=\"selectPeriod(selected)\" icon=\"fa fa-globe\"> </bsselect> <bserrmsg field=\"GetsudoPeriodId\"></bserrmsg> </div> </div> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==3 || mreportFilter.reportId==4\"> <div class=\"form-group\"> <bsreqlabel>Tahun</bsreqlabel> <bsselect name=\"getYear\" ng-model=\"mreportFilter.xyear\" data=\"olYear\" item-text=\"year\" item-value=\"id\" placeholder=\"Pilih Tahun\" on-select=\"selectPeriod(selected)\" icon=\"fa fa-globe\"> </bsselect> <bserrmsg field=\"xyear\"></bserrmsg> </div> </div> </div> <!-- custom --> <div class=\"row\"> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==6\"> <!-- item-text=\"GroupDealerName\"\r" +
    "\n" +
    "                                    item-value=\"GroupDealerId\" mreportFilter.dealerId selectOutlet(selected)--> <div class=\"form-group\"> <label>Dealer - Branch</label> <bsselect-tree name=\"outlet2\" ng-model=\"mreportFilter.outletIdInternal\" data=\"orgDataInternal\" placeholder=\"Pilih Dealer ....\" on-select=\"selectOutletInternal(selected)\" icon=\"fa fa-sitemap\"> </bsselect-tree> </div> </div> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==1 || mreportFilter.reportId==2 || mreportFilter.reportId==3 || mreportFilter.reportId==4 || mreportFilter.reportId==5  || mreportFilter.reportId==7\"> <div class=\"form-group\"> <!-- selectPeriod(selected) changeArea(selected)  ng-if=\"mreportFilter.reportId!=6\"--> <bsreqlabel ng-if=\"mreportFilter.reportId!=6 && mreportFilter.reportId!=2 && mreportFilter.reportId!=7\">Area</bsreqlabel> <bslabel ng-if=\"mreportFilter.reportId==6 || mreportFilter.reportId==2 || mreportFilter.reportId==7\">Area</bslabel> <bsselect name=\"area\" ng-model=\"mreportFilter.areaId\" data=\"areaData\" item-text=\"Description\" item-value=\"AreaId\" placeholder=\"Pilih Area\" ng-change=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-building\"> </bsselect> <bserrmsg field=\"areaId\"></bserrmsg> </div> </div> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==2\"> <div class=\"form-group\"> <bsreqlabel>Month</bsreqlabel> <bsselect name=\"getMonth\" ng-model=\"mreportFilter.monthId\" data=\"listMonth\" item-text=\"tx\" item-value=\"id\" placeholder=\"Pilih Month\" on-select=\"selectMonth(selected)\" icon=\"fa fa-globe\"> </bsselect> </div> </div> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==1 || mreportFilter.reportId==5 || mreportFilter.reportId==6 || mreportFilter.reportId==7\"> <div class=\"form-group\"> <!-- <bsreqlabel ng-if=\"mreportFilter.reportId==2\">Model</bsreqlabel> --> <label ng-if=\"(mreportFilter.reportId!=2)\">Model</label> <bsselect name=\"getModel\" ng-model=\"mreportFilter.modelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Pilih Model\" on-select=\"selectProduct(selected)\" icon=\"fa fa-car\"> </bsselect> </div> </div> <!-- <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==9\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <bslabel>Allowance</bslabel>\r" +
    "\n" +
    "                        <div class=\"input-icon-right\">\r" +
    "\n" +
    "                            <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                            <input type=\"number\" class=\"form-control\" name=\"allowance\" placeholder=\"number....\" ng-model=\"mreportFilter.allowance\">\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <bserrmsg field=\"allowance\"></bserrmsg>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div> --> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-6\">\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-6\">\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> <div class=\"row\"> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==6\"></div> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==1 || mreportFilter.reportId==2 || mreportFilter.reportId==3 || mreportFilter.reportId==4 || mreportFilter.reportId==5  || mreportFilter.reportId==7\"> <div class=\"form-group\"> <!-- <label>Island - Province - Cities</label> listRegion selectRegion(selected) changeProvince(selected)--> <label>Province</label> <bsselect name=\"province\" ng-model=\"mreportFilter.provinceId\" data=\"provinceData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Region\" ng-change=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-globe\"> </bsselect> </div> </div> <!-- <div class=\"col-md-4\" ng-if=\"mreportFilter.dealerId > 0 || mreportFilter.dealerId != null\">\r" +
    "\n" +
    "                    <div ng-if=\"mreportFilter.reportId==1 || mreportFilter.reportId==2 || mreportFilter.reportId==3 || mreportFilter.reportId==4 || mreportFilter.reportId==5 || mreportFilter.reportId==6 || mreportFilter.reportId==7\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                        <label>Outlet</label>\r" +
    "\n" +
    "                                    <bsselect name=\"outlet\"\r" +
    "\n" +
    "                                        ng-model=\"mreportFilter.outletId\"\r" +
    "\n" +
    "                                        data=\"outletData\"\r" +
    "\n" +
    "                                        item-text=\"Name\"\r" +
    "\n" +
    "                                        item-value=\"OutletId\"\r" +
    "\n" +
    "                                        placeholder=\"Pilih Outlet ....\"\r" +
    "\n" +
    "                                        on-select=\"selectOutlet(selected)\"\r" +
    "\n" +
    "                                        required=\"true\"\r" +
    "\n" +
    "                                        icon=\"fa fa-sitemap\">\r" +
    "\n" +
    "                                    </bsselect>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div> --> <div class=\"col-md-4\" ng-if=\"(mreportFilter.reportId==1 || mreportFilter.reportId==5 || mreportFilter.reportId==6)\"> <div class=\"form-group\"> <label>Type</label> <bsselect name=\"getType\" ng-model=\"mreportFilter.typeId\" data=\"productTypeData\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Type\" on-select=\"selectType(selected)\" icon=\"fa fa-car\"> </bsselect> </div> </div> <!-- model for profiling --> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==2\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mreportFilter.reportId==2\">Model</bsreqlabel> <!-- <label ng-if=\"(mreportFilter.reportId!=2)\">Model</label> --> <bsselect name=\"getModel2\" ng-model=\"mreportFilter.modelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Pilih Model\" on-select=\"selectProduct(selected)\" icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <div class=\"row\"> <!-- <div class=\"col-md-4\" ng-if=\"(mreportFilter.reportId==3 || mreportFilter.reportId==4) && mreportFilter.provinceId != ''\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                     \r" +
    "\n" +
    "                    <label>City</label>\r" +
    "\n" +
    "                            <bsselect name=\"getReport\"\r" +
    "\n" +
    "                                    ng-model=\"mreportFilter.cityId\"\r" +
    "\n" +
    "                                    data=\"lcity\"\r" +
    "\n" +
    "                                    item-text=\"CityRegencyName\"\r" +
    "\n" +
    "                                    item-value=\"CityRegencyId\"\r" +
    "\n" +
    "                                    placeholder=\"Select City...\"\r" +
    "\n" +
    "                                    on-select=\"selectCity(selected)\"\r" +
    "\n" +
    "                                    icon=\"fa fa-globe\">\r" +
    "\n" +
    "                            </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div> --> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==6\"></div> <div class=\"col-md-4\" ng-if=\"mreportFilter.reportId==1 || mreportFilter.reportId==2 || mreportFilter.reportId==3 || mreportFilter.reportId==4 || mreportFilter.reportId==5 || mreportFilter.reportId==7\"> <!-- item-text=\"GroupDealerName\"\r" +
    "\n" +
    "                                    item-value=\"GroupDealerId\" mreportFilter.dealerId selectOutlet(selected)--> <div class=\"form-group\"> <label>Dealer - Branch</label> <bsselect-tree name=\"outlet\" ng-model=\"mreportFilter.outletId\" data=\"orgData\" placeholder=\"Pilih Dealer ....\" on-select=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-sitemap\"> </bsselect-tree> </div> </div> <!-- <div class=\"col-md-8\" ng-if=\"mreportFilter.reportId!=3 && mreportFilter.reportId!=4\"></div> --> <div class=\"col-md-4\" ng-if=\"(mreportFilter.reportId==1 || mreportFilter.reportId==5 || mreportFilter.reportId==6)\"> <div class=\"form-group\"> <label>Colour</label> <bsselect name=\"getModelColour\" ng-model=\"mreportFilter.colourId\" data=\"productColorModel\" item-text=\"ColorName, ColorCode\" item-value=\"ColorId\" placeholder=\"Pilih Colour\" on-select=\"selectColor(selected)\" icon=\"fa fa-car\"> </bsselect> </div> </div> <div class=\"col-md-8\" ng-if=\"mreportFilter.reportId==7\"> <div class=\"col-md-4\"> <bsreqlabel>Month Start</bsreqlabel> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"mreportFilter.date1\" ng-change=\"startDate(dt)\"> </bsdatepicker> <!-- <datepicker ng-model=\"mreportFilter.date1\" min-mode=\"month\" datepicker-mode=\"'month'\"></datepicker> --> <!-- <bserrmsg field=\"date1\"></bserrmsg> --> </div> <div class=\"col-md-4\"> <bsreqlabel>Month End</bsreqlabel> <bsdatepicker name=\"EditDate\" date-options=\"endDateOptions\" ng-model=\"mreportFilter.date2\" min-date=\"mreportFilter.date1\" ng-change=\"endDate(dt)\"> </bsdatepicker> <!-- <bserrmsg field=\"date2\"></bserrmsg> --> </div> </div> </div> <!-- <div class=\"row\" ng-if=\"mreportFilter.reportId==7 \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                \r" +
    "\n" +
    "                \r" +
    "\n" +
    "            </div> --> <div class=\"row\" ng-if=\"mreportFilter.reportId==1 || mreportFilter.reportId==3\"> <div class=\"col-md-6\">Filter Field (Pilih Minimal 1)</div> <div class=\"col-md-6\"></div> </div> <div class=\"row\"> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.marketPolreg\" ng-change=\"showHideData()\" label=\"Market Polreg By Prov\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.rsSugestion\" ng-change=\"showHideData()\" label=\"RS Sugestion\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.os\" ng-change=\"showHideData()\" label=\"O/S\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.gapVsTarget\" ng-change=\"showHideData()\" label=\"GAP VS RS Target OAP/RAP\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- end row 1 --> </div> <div class=\"row\"> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.outletShare\" ng-change=\"showHideData()\" label=\"Outlet Share\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.rsAdjusment\" ng-change=\"showHideData()\" label=\"RS Adjusment\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.cl\" ng-change=\"showHideData()\" label=\"C/L\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.gapVsTargetMarket\" ng-change=\"showHideData()\" label=\"GAP VS RS Target Market\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- end row 2 --> </div> <div class=\"row\"> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.rsTargetMarket\" ng-change=\"showHideData()\" label=\"RS Target Market\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.wsSugestion\" ng-change=\"showHideData()\" label=\"WS Sugestion\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.spk\" ng-change=\"showHideData()\" label=\"SPK\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.gapVsTargetOutletShare\" ng-change=\"showHideData()\" label=\"GAP VS Target Outlet Share\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- end row 3 --> </div> <div class=\"row\"> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.rsOapRap\" ng-change=\"showHideData()\" label=\"RS OAP RAP\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.wsAdjusment\" ng-change=\"showHideData()\" label=\"WS Adjusment\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.minMax\" ng-change=\"showHideData()\" label=\"Min Max\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.gapVsTargetOutletSalesContribution\" ng-change=\"showHideData()\" label=\"GAP VS Target Outlet Sales Contribution\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- end row 4 --> </div> <div class=\"row\"> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.stockBase\" ng-change=\"showHideData()\" label=\"Stock Base On Adjusment\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.stockRatio\" ng-change=\"showHideData()\" label=\"Stock Ratio\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==1\"> <bscheckbox ng-model=\"mreportFilter.visibility.outerSalesContribution\" ng-change=\"showHideData()\" label=\"Outlet Sales Contribution\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- end row 5 end RSSP--> </div> <!-- end RSSP--> <div class=\"row\"> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==3 || mreportFilter.reportId==4\"> <bscheckbox ng-model=\"mreportFilter.visibility.market\" ng-change=\"showHideData()\" label=\"Market\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==3 || mreportFilter.reportId==4\"> <bscheckbox ng-model=\"mreportFilter.visibility.retailSales\" ng-change=\"showHideData()\" label=\"Retail Sales\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\" ng-if=\"mreportFilter.reportId==3 || mreportFilter.reportId==4\"> <bscheckbox ng-model=\"mreportFilter.visibility.outletShare\" ng-change=\"showHideData()\" label=\"Outlet Share\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- end row OAP/RAP --> </div> <!-- <pre>{{mreportFilter}}</pre> --> <!-- end custom --> <input type=\"file\" accept=\".xlsx\" id=\"uploadRSSPFleet\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </bsform-advsearch> <!-- </div> ' size=\"{{modalSize}}\"' ' novalidate>'++--> <!--  <div ng-show=\"showPreview\">\r" +
    "\n" +
    "                <table class=\"table table-bordered\" ng-repeat=\"(sheetName, sheetData) in sheets\" ng-show=\"sheetName == selectedSheetName\">\r" +
    "\n" +
    "                    <thead>\r" +
    "\n" +
    "                        <tr>\r" +
    "\n" +
    "                            <th ng-bind=\"sheetName\"></th>\r" +
    "\n" +
    "                        </tr>\r" +
    "\n" +
    "                    </thead>\r" +
    "\n" +
    "                    <tr ng-repeat=\"row in sheetData.data\">\r" +
    "\n" +
    "                        <td ng-repeat=\"col in row\" ng-bind=\"col\"></td>\r" +
    "\n" +
    "                    </tr>\r" +
    "\n" +
    "                    action=\"sm_act(mData)\" model=\"mData\" action-caption=\"Delete\"\r" +
    "\n" +
    "                </table>\r" +
    "\n" +
    "            </div> --> </bsform-grid> <bsmodal id=\"modal_report\" title=\"Pembuatan Report\" mode=\"\" visible size=\"small\" hide-footer=\"true\"> <p style=\"font-size:15px\">Mohon menunggu pembuatan report....</p> </bsmodal>"
  );


  $templateCache.put('app/branchorder/rs/rs.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"RSForm\" factory-name=\"RSSuggestion\" model=\"mRS\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xRS.selected\" custom-action-settings=\"customActionSettings\" form-api=\"formApi\" on-after-register-grid-api=\"afterRegisterGridApi\" hide-new-button=\"true\" show-advsearch=\"on\" grid-inline-edit=\"true\" on-show-detail=\"onShowDetail\" on-select-rows=\"onSelectRows\" on-before-save=\"onBeforeSave\" modal-title=\"RS Suggestion\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Tahun</bsreqlabel> <bsselect name=\"year\" ng-model=\"filter.year\" data=\"yearData\" item-text=\"name\" item-value=\"id\" placeholder=\"Pilih Tahun\" on-select=\"selectYear(selected)\" required icon=\"fa fa-calendar\"> </bsselect> </div> <div class=\"form-group\"> <label>Dealer Outlet</label> <bsselect-tree name=\"OutletId\" ng-model=\"filter.OutletId\" data=\"orgData\" placeholder=\"Pilih Outlet Dealer\" on-select=\"changeFilterCombo(selected,sender)\" required icon=\"fa fa-sitemap\"> </bsselect-tree> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <!-- <bsreqlabel>Area</bsreqlabel> --> <label>Area</label> <bsselect name=\"areaId\" ng-model=\"filter.areaId\" data=\"areaData\" item-text=\"Description\" item-value=\"AreaId\" placeholder=\"Pilih Area\" ng-change=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-sitemap\"> </bsselect> </div> <div class=\"form-group\"> <!-- <bsreqlabel>Province</bsreqlabel> --> <label>Province</label> <bsselect name=\"regionId\" ng-model=\"filter.provinceId\" data=\"provinceData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Region\" ng-change=\"changeFilterCombo(selected,sender)\" icon=\"fa fa-sitemap\"> </bsselect> </div> </div> <input type=\"file\" accept=\".xlsx\" id=\"uploadRS\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </div> </bsform-advsearch>  </bsform-grid>"
  );


  $templateCache.put('app/branchorder/rssp/fleet/rsspfleet.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"RSSPFleetForm\" factory-name=\"RSSPFleet\" model=\"tempRSSP\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xRSSPFleet.selected\" on-before-cancel=\"onBeforeCancel\" on-before-save=\"onBeforeSave\" do-custom-save=\"doCustomSave\" on-before-new-mode=\"beforeNew\" on-bulk-delete=\"customDelete\" on-bulk-approve=\"customApprove\" on-bulk-reject=\"customReject\" on-after-register-grid-api=\"afterRegisterGridApi\" form-grid-api=\"gridApi\" form-api=\"formApi\" custom-action-settings=\"customActionSettings\" show-advsearch=\"on\" grid-inline-edit=\"true\" hide-new-button=\"hideNewButton\" form-name=\"RSSPFleetForm\" modal-title=\"RSSP Fleet\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Getsudo Period</bsreqlabel> <bsselect name=\"filterPeriod\" ng-model=\"filter.period\" data=\"periodData\" on-select=\"filsudoPeriodId(selected)\" item-text=\"GetsudoPeriodName, GetsudoPeriodCode\" item-value=\"period\" placeholder=\"Pilih Getsudo Period\" required icon=\"fa fa-calendar\"> </bsselect> </div> <div class=\"form-group\"> <label>Customer Fleet</label> <!-- <bsselect name=\"filterFleet\"\r" +
    "\n" +
    "                                ng-model=\"filter.CustomerCode\"\r" +
    "\n" +
    "                                data=\"fleetData\"\r" +
    "\n" +
    "                                on-select=\"filcustomerId(selected)\"\r" +
    "\n" +
    "                                item-text=\"CustomerName, CustomerCode\"\r" +
    "\n" +
    "                                item-value=\"CustomerName\"\r" +
    "\n" +
    "                                placeholder=\"Pilih Customer Fleet\"\r" +
    "\n" +
    "                                icon=\"fa fa-sitemap\">\r" +
    "\n" +
    "                        </bsselect> --> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Customer Fleet\" ng-model=\"filter.CustomerName\"> </div> </div> <div class=\"form-group\"> <label>Dealer Outlet</label> <bsselect-tree name=\"filterDealer\" ng-model=\"filter.OutletId\" data=\"orgData\" on-select=\"filOutlet(selected)\" placeholder=\"Pilih Outlet Dealer\" icon=\"fa fa-sitemap\"> </bsselect-tree> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model</label> <bsselect name=\"filterModel\" ng-model=\"filter.VehicleModelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"filter.productModel=selected\" placeholder=\"Pilih Model\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Type</label> <bsselect name=\"filterType\" ng-model=\"filter.productType\" data=\"filter.productModel.listVehicleType\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" on-select=\"filproductType(selected)\" placeholder=\"Pilih Type\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Name Color</label> <bsselect name=\"filterColor\" ng-model=\"filter.productColor\" data=\"filter.productType.listColor\" item-text=\"ColorName, ColorCode\" item-value=\"ColorId\" placeholder=\"Pilih Name Color\" icon=\"fa fa-car\"> </bsselect> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsreqlabel>Status HO</bsreqlabel>\r" +
    "\n" +
    "                        <bsreqlabel>\r" +
    "\n" +
    "                            {{}}\r" +
    "\n" +
    "                        </bsreqlabel>\r" +
    "\n" +
    "                    </div> --> </div> <input type=\"file\" accept=\".xlsx\" id=\"uploadRSSPFleet\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </div> <!-- <pre>\r" +
    "\n" +
    "                {{mRSSPFleet}}\r" +
    "\n" +
    "            </pre> --> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Getsudo Period</bsreqlabel> <bsselect name=\"getsudoPeriodId\" ng-model=\"selPer.selPer\" data=\"periodData\" on-select=\"selectsudoPeriodId(selected)\" item-text=\"GetsudoPeriodName, GetsudoPeriodCode\" item-value=\"period\" placeholder=\"Pilih Getsudo Period\" required ng-disabled=\"true\" icon=\"fa fa-calendar\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Customer Fleet</bsreqlabel> <!-- <bsselect name=\"fleetId\"\r" +
    "\n" +
    "                            ng-model=\"tempRSSP.selCustomerFleetId\"\r" +
    "\n" +
    "                            data=\"fleetData\"\r" +
    "\n" +
    "                            item-text=\"CustomerName, CustomerCode\"\r" +
    "\n" +
    "                            item-value=\"Id\"\r" +
    "\n" +
    "                            on-select=\"selectCustomer(selected)\"\r" +
    "\n" +
    "                            required=\"true\"\r" +
    "\n" +
    "                            placeholder=\"Pilih Customer Fleet\"\r" +
    "\n" +
    "                            icon=\"fa fa-sitemap\">\r" +
    "\n" +
    "                    </bsselect> --> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Customer Fleet\" ng-model=\"saveCusName.cusName\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Dealer Outlet</bsreqlabel> <bsselect-tree name=\"outletId\" ng-model=\"tempRSSP.selOutletId\" data=\"orgData2\" on-select=\"selectOutlet(selected)\" placeholder=\"Pilih Outlet Dealer\" icon=\"fa fa-sitemap\"> </bsselect-tree> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"productModelId\" ng-model=\"tempRSSP.selVehicleModelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"selectModel(selected)\" placeholder=\"Pilih Model\" required icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Type</bsreqlabel> <bsselect name=\"productTypeId\" ng-model=\"tempRSSP.selVehicleTypeId\" data=\"selectedModel.listVehicleType\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" on-select=\"selectType(selected)\" placeholder=\"Pilih Type\" required icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Name Color</bsreqlabel> <bsselect multiple name=\"productColorId\" ng-model=\"tempRSSP.selColorId\" data=\"selectedType.listColor\" item-text=\"ColorName, ColorCode\" item-value=\"TheId\" ng-change=\"selectMultiColor(selected)\" placeholder=\"Pilih Type Color\" icon=\"fa fa-car\"> </bsselect> </div> </div> <input type=\"file\" accept=\".xlsx\" id=\"uploadRSSPFleet\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div>  <!-- <pre>\r" +
    "\n" +
    "        {{mRSSPFleet}}\r" +
    "\n" +
    "    </pre> --> <!-- <pre>\r" +
    "\n" +
    "        {{gridDetail.data}}\r" +
    "\n" +
    "    </pre> --> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/rssp/internal/rsspinternal.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"RSSPInternalForm\" factory-name=\"RSSPInternal\" model=\"tempRSSP\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xRSSPInternal.selected\" on-before-cancel=\"onBeforeCancel\" on-before-save=\"onBeforeSave\" do-custom-save=\"doCustomSave\" on-before-new-mode=\"beforeNew\" on-bulk-delete=\"customDelete\" on-after-register-grid-api=\"afterRegisterGridApi\" form-grid-api=\"gridApi\" custom-action-settings=\"customActionSettings\" show-advsearch=\"on\" grid-inline-edit=\"true\" hide-new-button=\"hideNewButton\" form-name=\"RSSPInternalForm\" modal-title=\"RSSP Internal\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Getsudo Period</bsreqlabel> <bsselect name=\"filterPeriod\" ng-model=\"filter.GetsudoPeriodId\" data=\"periodData\" on-select=\"filsudoPeriodId(selected)\" item-text=\"GetsudoPeriodName, GetsudoPeriodCode\" item-value=\"GetsudoPeriodId\" placeholder=\"Pilih Getsudo Period\" required icon=\"fa fa-calendar\"> </bsselect> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                        <label>Customer Fleet</label>\r" +
    "\n" +
    "                        <bsselect name=\"filterInternal\"\r" +
    "\n" +
    "                                ng-model=\"filter.CustomerCode\"\r" +
    "\n" +
    "                                data=\"InternalData\"\r" +
    "\n" +
    "                                item-text=\"CustomerName, CustomerCode\"\r" +
    "\n" +
    "                                item-value=\"CustomerName\"\r" +
    "\n" +
    "                                placeholder=\"Pilih Customer\"\r" +
    "\n" +
    "                                icon=\"fa fa-sitemap\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div> --> <div class=\"form-group\"> <label>Dealer Outlet</label> <!-- <bsselect-tree name=\"filterDealer\"\r" +
    "\n" +
    "                                ng-model=\"filter.org\"\r" +
    "\n" +
    "                                data=\"orgData\"\r" +
    "\n" +
    "                                on-select=\"filOutlet(selected)\"\r" +
    "\n" +
    "                                placeholder=\"Pilih Outlet Dealer\"\r" +
    "\n" +
    "                                icon=\"fa fa-sitemap\">\r" +
    "\n" +
    "                        </bsselect-tree> --> <bsselect name=\"filterDealer\" ng-model=\"filter.OutletId\" data=\"orgData\" item-text=\"Name, OutletCode\" item-value=\"OutletTAMId\" placeholder=\"Pilih Outlet Dealer\" icon=\"fa fa-sitemap\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model</label> <bsselect name=\"filterModel\" ng-model=\"filter.VehicleModelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"filter.productModel=selected\" placeholder=\"Pilih Model\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Type</label> <bsselect name=\"filterType\" ng-model=\"filter.productType\" data=\"filter.productModel.listVehicleType\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" on-select=\"filproductType(selected)\" placeholder=\"Pilih Type\" icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <label>Name Color</label> <bsselect name=\"filterColor\" ng-model=\"filter.ColorId\" data=\"filter.productType.listColor\" item-text=\"ColorName, ColorCode\" item-value=\"ColorId\" placeholder=\"Pilih Name Color\" icon=\"fa fa-car\"> </bsselect> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsreqlabel>Status HO</bsreqlabel>\r" +
    "\n" +
    "                        <bsreqlabel>\r" +
    "\n" +
    "                            {{}}\r" +
    "\n" +
    "                        </bsreqlabel>\r" +
    "\n" +
    "                    </div> --> </div> <input type=\"file\" accept=\".xlsx\" id=\"uploadRSSPInternal\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </div> <!-- <pre>\r" +
    "\n" +
    "                {{mRSSPInternal}}\r" +
    "\n" +
    "            </pre> --> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Getsudo Period</bsreqlabel> <bsselect name=\"getsudoPeriodId\" ng-model=\"selPer.selPer\" data=\"periodData\" on-select=\"selectsudoPeriodId(selected)\" item-text=\"GetsudoPeriodName, GetsudoPeriodCode\" item-value=\"period\" placeholder=\"Pilih Getsudo Period\" required ng-disabled=\"true\" icon=\"fa fa-calendar\"> </bsselect> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                    <bsreqlabel>Customer Fleet</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"InternalId\"\r" +
    "\n" +
    "                            ng-model=\"mRSSPFleet.CustomerFleetId\"\r" +
    "\n" +
    "                            data=\"InternalData\"\r" +
    "\n" +
    "                            item-text=\"CustomerName, CustomerCode\"\r" +
    "\n" +
    "                            item-value=\"Id\"\r" +
    "\n" +
    "                            on-select=\"selectCustomer(selected)\"\r" +
    "\n" +
    "                            required=\"true\"\r" +
    "\n" +
    "                            placeholder=\"Pilih Customer\"\r" +
    "\n" +
    "                            icon=\"fa fa-sitemap\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                </div> --> <div class=\"form-group\"> <bsreqlabel>Dealer Outlet</bsreqlabel> <bsselect name=\"outletId\" ng-model=\"tempRSSP.selOutletId\" data=\"orgData2\" item-text=\"Name, OutletCode\" item-value=\"OutletTAMId\" on-select=\"selectOutlet(selected)\" placeholder=\"Pilih Outlet Dealer\" icon=\"fa fa-sitemap\"> </bsselect> <!-- <bsselect-tree name=\"outletId\"\r" +
    "\n" +
    "                            ng-model=\"mRSSPInternal.OutletId\"\r" +
    "\n" +
    "                            data=\"orgData2\"\r" +
    "\n" +
    "                            on-select=\"selectOutlet(selected)\"\r" +
    "\n" +
    "                            placeholder=\"Pilih Outlet Dealer\"\r" +
    "\n" +
    "                            icon=\"fa fa-sitemap\">\r" +
    "\n" +
    "                    </bsselect-tree> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"productModelId\" ng-model=\"tempRSSP.selVehicleModelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"selectModel(selected)\" placeholder=\"Pilih Model\" required icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Type</bsreqlabel> <bsselect name=\"productTypeId\" ng-model=\"tempRSSP.selVehicleTypeId\" data=\"selectedModel.listVehicleType\" item-text=\"Description, KatashikiCode, SuffixCode\" item-value=\"VehicleTypeId\" on-select=\"selectType(selected)\" placeholder=\"Pilih Type\" required icon=\"fa fa-car\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Name Color</bsreqlabel> <bsselect multiple name=\"productColorId\" ng-model=\"tempRSSP.selColorId\" data=\"selectedType.listColor\" item-text=\"ColorName, ColorCode\" item-value=\"TheId\" ng-change=\"selectMultiColor(selected)\" placeholder=\"Pilih Type Color\" icon=\"fa fa-car\"> </bsselect> </div> </div> <input type=\"file\" accept=\".xlsx\" id=\"uploadRSSPInternal\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div>  <!-- <pre>\r" +
    "\n" +
    "        {{mRSSPInternal}}\r" +
    "\n" +
    "    </pre>\r" +
    "\n" +
    "    <pre>\r" +
    "\n" +
    "        {{gridDetail.data}}\r" +
    "\n" +
    "    </pre> --> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/rssp/konstanta/konstanta.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"KonstantaForm\" factory-name=\"Konstanta\" model=\"mKonstanta\" loading=\"loading\" model-id=\"AdjustmentRatioRelationId\" on-before-new-mode=\"onBeforeNewMode\" on-show-detail=\"onShowDetail\" get-data=\"getData\" selected-rows=\"xKonstanta.selected\" on-select-rows=\"onSelectRows\" modal-title=\"Konstanta\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <!-- <pre>{{mKonstanta}}</pre> --> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Period</bsreqlabel> <bsselect name=\"periodId\" ng-model=\"mKonstanta.GetsudoPeriodId\" data=\"periodData\" item-text=\"GetsudoPeriodName,GetsudoPeriodCode\" item-value=\"GetsudoPeriodId\" placeholder=\"Pilih Periode Getsudo\" style=\"min-width: 150px\" on-select=\"selectPeriod(selected)\" required> </bsselect> <bserrmsg field=\"periodId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Konstanta</bsreqlabel> <bsselect name=\"rsspVarId\" ng-model=\"mKonstanta.AdjustmentRatioTypeId\" data=\"konstantData\" item-text=\"AdjustmentName\" item-value=\"AdjustmentRatioTypeId\" placeholder=\"Pilih Konstanta\" required style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"rsspVarId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nilai Konstanta</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" min=\"0\" class=\"form-control\" name=\"value\" placeholder=\"Nilai Konstanta\" ng-model=\"mKonstanta.AdjustmentRatioValue\" required> </div> <bserrmsg field=\"value\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Prioritas</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" min=\"0\" class=\"form-control\" name=\"prior\" placeholder=\"Prioritas\" ng-model=\"mKonstanta.RatioPriority\" required> </div> <bserrmsg field=\"prior\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Area</label> <bsselect name=\"areaId\" ng-model=\"mKonstanta.AreaId\" placeholder=\"Pilih Area\" data=\"areaData\" item-text=\"Description\" item-value=\"AreaId\" ng-change=\"changeArea(selected)\" style=\"min-width: 150px\"> <!-- ng-model=\"filter.areaId\" --> </bsselect> <bserrmsg field=\"areaId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Provinsi</label> <bsselect name=\"provinceId\" ng-model=\"mKonstanta.ProvinceId\" placeholder=\"Pilih Provinsi\" data=\"provinceData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" ng-change=\"changeProvince(selected)\" style=\"min-width: 150px\"> </bsselect> <!-- ng-model=\"filter.provinceId\" --> <bserrmsg field=\"provinceId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Dealer</label> <bsselect name=\"dealerId\" ng-model=\"mKonstanta.GroupDealerId\" placeholder=\"Pilih Dealer\" data=\"orgData\" item-text=\"GroupDealerName,GroupDealerCode\" item-value=\"GroupDealerId\" on-select=\"selectDealer(selected)\" style=\"min-width: 150px\"> </bsselect> <!-- ng-model=\"filter.outletId\" --> <bserrmsg field=\"dealerId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Outlet</label> <bsselect name=\"outletId\" ng-model=\"mKonstanta.OutletId\" placeholder=\"Pilih Outlet\" data=\"outletData.child\" item-text=\"Name,OutletCode\" item-value=\"OutletId\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"outletId\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Model</label> <bsselect name=\"modelId\" ng-model=\"mKonstanta.VehicleModelId\" placeholder=\"Pilih Model\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"selectModel(selected)\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"modelId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Type</label> <bsselect name=\"typeId\" ng-model=\"mKonstanta.VehicleTypeId\" data=\"selectedModel.listVehicleType\" item-text=\"Description\" item-value=\"VehicleTypeId\" placeholder=\"Pilih Tipe\" on-select=\"selectType(selected)\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"typeId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Type Color</label> <bsselect name=\"typeColorId\" ng-model=\"mKonstanta.VehicleTypeColorId\" placeholder=\"Pilih Tipe Warna\" data=\"selectedType.listColor\" item-text=\"ColorName\" item-value=\"VehicleTypeColorId\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"typeColorId\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/rssp/modeltemp/modeltemp.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"ModelTempForm\" factory-name=\"ModelTemplate\" model=\"mModelTemp\" loading=\"loading\" model-id=\"VehicleModelId\" hide-new-button=\"true\" get-data=\"getData\" selected-rows=\"xModelTemp.selected\" on-select-rows=\"onSelectRows\" modal-title=\"Model Template\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input disabled type=\"text\" class=\"form-control\" name=\"modelName\" placeholder=\"Model Name\" ng-model=\"mModelTemp.VehicleModelName\"> </div> <bserrmsg field=\"modelName\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Model Template</label> <bsselect data=\"template\" item-text=\"Name\" item-value=\"Id\" ng-model=\"mModelTemp.HeaderFormulaId\" placeholder=\"Pilih Model Template\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"rsspTempHdrId\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Type Color Template</label> <bsselect data=\"template\" item-text=\"Name\" item-value=\"Id\" ng-model=\"mModelTemp.HeaderFormulaDetailId\" placeholder=\"Pilih Type Color Template\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"rsspTempHdrId2\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/rssp/rssp/rssp.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.gridSum{\r" +
    "\n" +
    "        height: 300px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .gridSumGap{        \r" +
    "\n" +
    "        height: 100px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .emptyRow {\r" +
    "\n" +
    "        border-left: none !important;\r" +
    "\n" +
    "        border-right: none !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .editingCell {\r" +
    "\n" +
    "        background-color: #ff7 !important; \r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .headerCell {\r" +
    "\n" +
    "        background-color: #f7f7f7 !important; \r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #rsspcomposition label, #rsspcomposition td, #rsspcomposition a{\r" +
    "\n" +
    "        font-size: 13px !important;\r" +
    "\n" +
    "        line-height: 25px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .comment-box{\r" +
    "\n" +
    "        border-right: solid 1px #ddd;\r" +
    "\n" +
    "        border-left: solid 1px #ddd;\r" +
    "\n" +
    "        border-bottom: solid 1px #ddd;\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .comments{\r" +
    "\n" +
    "        overflow: scroll;    \r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .comment{\r" +
    "\n" +
    "        border: solid 1px #ddd;\r" +
    "\n" +
    "        background: #fafafa;\r" +
    "\n" +
    "        border-radius: 5px;\r" +
    "\n" +
    "        margin-bottom: 5px;\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "    }</style> <bsform-grid ng-form=\"fRSSPForm\" factory-name=\"RSSP\" model=\"mRSSP\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xRSSP.selected\" on-select-rows=\"onSelectRows\" on-bulk-review=\"bulkReview\" do-custom-save=\"doCustomSave\" rights-byte=\"myRights\" hide-new-button=\"true\" hide-edit-button=\"hideEditButton\" show-advsearch=\"on\" on-show-detail=\"onShowDetail\" modal-title=\"RSSP\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Getsudo Period</bsreqlabel> <bsselect name=\"periodId\" ng-model=\"filter.GetsudoPeriodId\" data=\"periodData\" item-text=\"GetsudoPeriodName,GetsudoPeriodCode\" item-value=\"GetsudoPeriodId\" placeholder=\"Pilih Getsudo Period\" on-select=\"selectPeriod(selected)\" required icon=\"fa fa-calendar\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Outlet Dealer</bsreqlabel> <bsselect-tree name=\"outletId\" ng-model=\"filter.orgId\" data=\"orgData\" placeholder=\"Pilih Outlet Dealer\" on-select=\"selectOutlet(selected)\" required icon=\"fa fa-sitemap\"> </bsselect-tree> </div> </div> </div> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Getsudo Period</bsreqlabel> <bsselect name=\"periodId2\" ng-model=\"mRSSP.GetsudoPeriodId\" data=\"periodData\" item-text=\"GetsudoPeriodName,GetsudoPeriodCode\" item-value=\"GetsudoPeriodId\" placeholder=\"Pilih Getsudo Period\" on-select=\"selectPeriod(selected)\" required ng-disabled=\"true\" icon=\"fa fa-calendar\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"productModelId\" ng-model=\"mRSSP.VehicleModelId\" data=\"productModelData\" item-text=\"VehicleModelName,VehicleModelCode\" item-value=\"VehicleModelId\" placeholder=\"Pilih Model\" on-select=\"onShowDetail()\" required icon=\"fa fa-car\"> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" ng-disabled=\"orgData[0].child.length==1\"> <bsreqlabel>Outlet Dealer</bsreqlabel> <bsselect-tree name=\"outletId2\" ng-model=\"mRSSP.OutletId\" data=\"orgData\" placeholder=\"Pilih Outlet Dealer\" on-select=\"selectOutlet(selected)\" required icon=\"fa fa-sitemap\"> </bsselect-tree> </div> <div class=\"form-group\"> <label>Status</label> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"HOReviewState\" placeholder=\"Status\" ng-model=\"mRSSP.HOReviewState\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\" ng-if=\"checkRights(2)\"> <input type=\"file\" accept=\".xlsx\" id=\"rsspWS\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> <div class=\"btn-group\"> <button type=\"button\" class=\"rbtn btn ng-binding\" ng-click=\"download()\"> <i class=\"fa fa-fw fa-download\"></i> </button> <!--button type=\"button\" class=\"wbtn btn ng-binding\" ng-click=\"upload()\">\r" +
    "\n" +
    "                                <i class=\"fa fa-fw fa-upload\"></i>\r" +
    "\n" +
    "                            </button--> <button type=\"button\" class=\"rbtn btn ng-binding\" ng-click=\"redistribute()\"> <i class=\"fa fa-fw fa-calculator\"></i> </button> </div> </div> <div class=\"col-md-3\"> <bscheckbox ng-model=\"Visibility.market\" ng-change=\"showHideData()\" label=\"Market\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2\"> <bscheckbox ng-model=\"Visibility.spk\" ng-change=\"showHideData()\" label=\"SPK\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-3\"> <bscheckbox ng-model=\"Visibility.reference\" ng-change=\"showHideData()\" label=\"Reference\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> </div> <div class=\"col-md-6\"> <!-- <div class=\"ui raised segment\" id=\"rsspcomposition\">\r" +
    "\n" +
    "                    <label>Komposisi dalam bulan</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r" +
    "\n" +
    "                    <a href=\"#\" ng-click=\"changeComposition('lg')\" style=\"font-weight: bold\">Ubah komposisi</a>\r" +
    "\n" +
    "                    <table style=\"width: 100%\">\r" +
    "\n" +
    "                        <tbody>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>A.&nbsp;&nbsp;SPK</td>\r" +
    "\n" +
    "                                <td>Total <span style=\"font-weight: bold\">{{totalSPK}}</span> atau rata-rata <span style=\"font-weight: bold\">{{avgSPK}}</span> per 3 bulan</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>B.&nbsp;&nbsp;Outstanding</td>\r" +
    "\n" +
    "                                <td>Total <span style=\"font-weight: bold\">{{totalOS}}</span> atau rata-rata <span style=\"font-weight: bold\">{{avgOS}}</span> per 3 bulan</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>C.&nbsp;&nbsp;Retail Sales</td>\r" +
    "\n" +
    "                                <td>Total <span style=\"font-weight: bold\">{{totalRS}}</span> atau rata-rata <span style=\"font-weight: bold\">{{avgRS}}</span> per 3 bulan</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>D.&nbsp;&nbsp;Cancelation</td>\r" +
    "\n" +
    "                                <td>Total <span style=\"font-weight: bold\">{{totalCL}}</span> atau rata-rata <span style=\"font-weight: bold\">{{avgCL}}</span> per 3 bulan</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </tbody>\r" +
    "\n" +
    "                    </table>\r" +
    "\n" +
    "                </div> --> </div> </div> <div class=\"row\"> <div class=\"col-md-8\" id=\"rsspModelContainer\"> <hot-table hot-id=\"hotRSSP\" settings=\"hotRSSPSettings\" datarows=\"hotData\" merge-cells formulas manual-column-resize manual-row-resize col-headers> </hot-table> </div> <div class=\"col-md-4\"> <bsradiobox ng-model=\"summary.type\" options=\"summary.data\" ng-change=\"changeTypeSum()\" layout=\"V\"> </bsradiobox> <div ui-grid=\"gridSum\" class=\"gridSum\" ui-grid-resize-columns ui-grid-pinning ui-grid-selection ui-grid-auto-resize ui-grid-tree-view ng-cloak> </div> <div ui-grid=\"gridSumGap\" class=\"gridSumGap\" ui-grid-resize-columns ui-grid-pinning ui-grid-auto-resize ui-grid-tree-view ng-cloak> </div> <div class=\"comment-box\" style=\"height: 280px\"> <div class=\"comments\" style=\"height: 200px\"> <div ng-repeat=\"comment in comments\" class=\"comment\"> <label style=\"font-weight: bold\">{{comment.UserName}}</label> <p>{{comment.Comment}}</p> <label style=\"font-size: 85%; color: #bbb; margin-top:-10px; float: right\">{{comment.time}}</label> </div> </div> <textarea ng-model=\"comment.message\" rows=\"3\" col=\"0\" style=\"top: 200; width: 100%\" ng-keydown=\"sendComment($event)\" skip-disable=\"true\">\r" +
    "\n" +
    "                    </textarea> </div> </div> </div> <bsmodal id=\"composition\" title=\"Ubah Komposisi\" mode=\"1\" visible action-caption=\"Ok\" size=\"lg\" novalidate> <div class=\"col-md-3\"> <label style=\"font-weight: bold\">Total SPK</label> <div ui-grid=\"gridSPK\" ui-grid-selection ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> <div class=\"col-md-3\"> <label style=\"font-weight: bold\">Outstanding</label> <div ui-grid=\"gridOS\" ui-grid-selection ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> <div class=\"col-md-3\"> <label style=\"font-weight: bold\">Cancelation</label> <div ui-grid=\"gridCL\" ui-grid-selection ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> <div class=\"col-md-3\"> <label style=\"font-weight: bold\">Ratio Polreg</label> <div ui-grid=\"gridPolreg\" ui-grid-selection ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </bsmodal> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/rssp/rssp/rssp_model.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.hotCell{\r" +
    "\n" +
    "    /*background-color: #d3d3d3!important;*/\r" +
    "\n" +
    "    /*color: #0!important;*/\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    /*font-weight: 700!important;*/\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "    border-left: 1px solid #000!important;\r" +
    "\n" +
    "    border-top: 1px solid #000!important;\r" +
    "\n" +
    "    border-right: 1px solid #000!important;\r" +
    "\n" +
    "    border-bottom: 1px solid #000!important;\r" +
    "\n" +
    "/*    border-width: 1px!important;\r" +
    "\n" +
    "    border:1px solid #000!important;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader{\r" +
    "\n" +
    "    background-color: #d3d3d3!important;\r" +
    "\n" +
    "    color: #0!important;\r" +
    "\n" +
    "    text-align: center!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "    border-left: 1px solid #000!important;\r" +
    "\n" +
    "    border-top: 1px solid #000!important;\r" +
    "\n" +
    "    border-right: 1px solid #000!important;\r" +
    "\n" +
    "    border-bottom: 1px solid #000!important;\r" +
    "\n" +
    "/*    border-width: 1px!important;\r" +
    "\n" +
    "    border:1px solid #000!important;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader2{\r" +
    "\n" +
    "    background-color: #a3a3a3!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader3{\r" +
    "\n" +
    "    background-color: #d3d3d3!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "    border-left: 1px solid #000!important;\r" +
    "\n" +
    "    border-top: 1px solid #000!important;\r" +
    "\n" +
    "    border-right: 1px solid #000!important;\r" +
    "\n" +
    "    border-bottom: 1px solid #000!important;\r" +
    "\n" +
    "/*    border-width: 1px!important;\r" +
    "\n" +
    "    border:1px solid #000!important;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader4{\r" +
    "\n" +
    "    background-color: #b3b3b3!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader5{\r" +
    "\n" +
    "    background-color: #FDE4D8!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader6{\r" +
    "\n" +
    "    background-color: #F7AF8A!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeaderReverse{\r" +
    "\n" +
    "    background-color: #0f1a69!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    text-align: center!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotEditable{\r" +
    "\n" +
    "    background-color: yellow!important;\r" +
    "\n" +
    "    color: #0!important;\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "    border:1px solid #000!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotFromData{\r" +
    "\n" +
    "    background-color: #fff!important;\r" +
    "\n" +
    "    color: #0!important;\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotBold{\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHide{\r" +
    "\n" +
    "    display:none;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".handsontable th, .handsontable td{\r" +
    "\n" +
    "    font-size:12px;\r" +
    "\n" +
    "}</style> <div id=\"content\" style=\"padding:0\"> <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px 15px 10px 7px\"> <bsbreadcrumb icon=\"fa fa-fw fa-user\" form-title=\"bsForm Demo Manual\" action-type=\"actionType\"></bsbreadcrumb> <div class=\"pull-right\"> <div class=\"btn-group\" style=\"margin-top:-57px\"> <button class=\"zbtn btn btn-default\" ng-click=\"actOpt()\" onclick=\"this.blur()\"> <i class=\"fa fa-fw fa-filter\"></i>&nbsp;Options </button> <!--                 <button class=\"zbtn btn btn-default\" ng-click=\"actDel()\" ng-if=\"selectedRows.length>0\">\r" +
    "\n" +
    "                    <i class=\"fa fa-fw fa-remove\"></i>&nbsp;Delete\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "                <button class=\"zbtn btn btn-default\" ng-click=\"actRefresh()\" onclick=\"this.blur();\">\r" +
    "\n" +
    "                    <i class=\"fa fa-fw fa-refresh\"></i>&nbsp;Refresh\r" +
    "\n" +
    "                </button> --> </div> </div> </div> <div class=\"layout_wrapper\"> <div id=\"layoutContainer_bsFormDemoManual\" style=\"position:relative;margin:0px 0px 0px 0px;padding:0px;height:700px\"> <ui-layout options=\"{flow:'column', dividerSize:3, disableToggle:true}\"> <div class=\"inbox-side-bar\" style=\"width:100%;padding:0px\"> <div class=\"well clearfix\" style=\"padding:0px 15px 0px 15px;border:none;background:#fff;box-shadow:none\"> <br> <div class=\"well clearfix\" style=\"padding:0px 15px 0px 15px;border:1px solid #ccc;background:#fff;box-shadow:none\" ng-show=\"showOpt\"> <div> <div class=\"form-group\"> <bsreqlabel>Getsudo Period</bsreqlabel> <bsselect name=\"periodSelect\" ng-model=\"period.selected\" data=\"periodData\" item-text=\"code\" item-value=\"id\" placeholder=\"Select Period...\" on-select=\"selectPeriod(selected)\" required icon=\"fa fa-calendar\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Outlet</bsreqlabel> <bsselect name=\"periodSelect\" ng-model=\"period.selected\" data=\"periodData\" item-text=\"code\" item-value=\"id\" placeholder=\"Select Outlet...\" on-select=\"selectPeriod(selected)\" required icon=\"fa fa-calendar\"> </bsselect> </div> </div> </div> <hot-table hot-id=\"hot_rsspmodel\" settings=\"hot_settings\" datarows=\"data\"> </hot-table> </div> </div> </ui-layout> </div> </div> </div> <!-- <pre>{{db.items[3]}}</pre>\r" +
    "\n" +
    "<pre>{{db.items[4]}}</pre>\r" +
    "\n" +
    "<pre>{{db.items[5]}}</pre> -->"
  );


  $templateCache.put('app/branchorder/rssp/rssptemp/rssptemp.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"RSSPTempForm\" factory-name=\"RSSPTemplate\" model=\"mRSSPTemp\" loading=\"loading\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"xRSSPTemp.selected\" on-select-rows=\"onSelectRows\" on-show-detail=\"onShowDetail\" on-validate-save=\"onValidateSave\" modal-title=\"RSSP Template\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-12\"> <label style=\"font-weight:bold; color: red\"> PERHATIAN! TEMPLATE INI HANYA BEKERJA DI BROWSER, SEDANGKAN PERHITUNGAN SUGGESTION DI SERVER MENGGUNAKAN KALKULASI INDEPENDEN. </label> <br> <label style=\"font-weight:bold; color: red\"> OLEH KARENA ITU, PERUBAHAN FORMULA PADA TEMPLATE TIDAK AKAN MEMPENGARUHI PERHITUNGAN DI SERVER. GUNAKAN DENGAN HATI-HATI! </label> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Name Template</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Nama Template\" ng-model=\"mRSSPTemp.Name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Jumlah Kolom Fix</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"fixCtr\" placeholder=\"Jumlah Kolom Fix\" ng-model=\"mRSSPTemp.FixCtr\" required> </div> <bserrmsg field=\"fixCtr\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Keterangan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Keterangan\" ng-model=\"mRSSPTemp.Description\"> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> </div> </div> <uib-tabset active=\"active\" id=\"templateDetailTab\"> <uib-tab index=\"0\" heading=\"Pemilihan Variabel\"> <br> <dualmultiselect options=\"selectionOpt\" style=\"height: 700 !important\"> </dualmultiselect> <br> <bsbutton style=\"float: right; margin-right: 10\" ng-click=\"confirmVarSelect()\">Masukkan variabel ke template </bsbutton> </uib-tab> <uib-tab index=\"1\" heading=\"Konfigurasi Formula\"> <div class=\"col-md-12\" id=\"rsspDetailContainer\"> <br> <hot-table hot-id=\"hotRSSPTemp\" settings=\"hotSettings\" datarows=\"detailData\" merge-cells formulas manual-column-resize manual-row-resize row-headers col-headers context-menu> </hot-table> </div> </uib-tab> </uib-tabset> </bsform-grid>"
  );


  $templateCache.put('app/branchorder/rssp/rsspvar/rsspvar.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid-tree ng-form=\"RSSPVarForm\" factory-name=\"RSSPVar\" model=\"mRSSPVar\" loading=\"loading\" model-id=\"AdjustmentRatioTypeId\" get-data=\"getData\" selected-rows=\"xRSSPVar.selected\" on-select-rows=\"onSelectRows\" modal-title=\"RSSP Variable\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Variable Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Variable Name\" ng-model=\"mRSSPVar.AdjustmentName\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Parent Variable</label> <bsselect data=\"parentData\" item-text=\"AdjustmentName\" item-value=\"AdjustmentRatioTypeId\" ng-model=\"mRSSPVar.ParentId\" placeholder=\"Pilih Parent Variable\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"pid\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Table Field Name</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"beFldName\" placeholder=\"Table Field Name\" ng-model=\"mRSSPVar.BEFieldName\"> </div> <bserrmsg field=\"beFldName\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Type</label> <bsselect name=\"varTypeId\" ng-model=\"mRSSPVar.VariableInputTypeId\" data=\"varTypeData\" item-text=\"VariableType\" item-value=\"VariableInputTypeId\" placeholder=\"Pilih Tipe Variabel\" required> </bsselect> <bserrmsg field=\"varTypeId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Format</label> <bsselect name=\"varFormatId\" ng-model=\"mRSSPVar.FormatTypeId\" data=\"varFormatData\" item-text=\"TypeFormat\" item-value=\"FormatTypeId\" placeholder=\"Pilih Format Variabel\" required> </bsselect> <bserrmsg field=\"varFormatId\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Description</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea rows=\"5\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mRSSPVar.Description\">\r" +
    "\n" +
    "                    </textarea> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> </div> </div> </bsform-grid-tree>"
  );


  $templateCache.put('app/branchorder/rssp_model/rssp_model.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.hotCell{\r" +
    "\n" +
    "    /*background-color: #d3d3d3!important;*/\r" +
    "\n" +
    "    /*color: #0!important;*/\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    /*font-weight: 700!important;*/\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "    border-left: 1px solid #000!important;\r" +
    "\n" +
    "    border-top: 1px solid #000!important;\r" +
    "\n" +
    "    border-right: 1px solid #000!important;\r" +
    "\n" +
    "    border-bottom: 1px solid #000!important;\r" +
    "\n" +
    "/*    border-width: 1px!important;\r" +
    "\n" +
    "    border:1px solid #000!important;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader{\r" +
    "\n" +
    "    background-color: #d3d3d3!important;\r" +
    "\n" +
    "    color: #0!important;\r" +
    "\n" +
    "    text-align: center!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "    border-left: 1px solid #000!important;\r" +
    "\n" +
    "    border-top: 1px solid #000!important;\r" +
    "\n" +
    "    border-right: 1px solid #000!important;\r" +
    "\n" +
    "    border-bottom: 1px solid #000!important;\r" +
    "\n" +
    "/*    border-width: 1px!important;\r" +
    "\n" +
    "    border:1px solid #000!important;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader2{\r" +
    "\n" +
    "    background-color: #a3a3a3!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader3{\r" +
    "\n" +
    "    background-color: #d3d3d3!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "    border-left: 1px solid #000!important;\r" +
    "\n" +
    "    border-top: 1px solid #000!important;\r" +
    "\n" +
    "    border-right: 1px solid #000!important;\r" +
    "\n" +
    "    border-bottom: 1px solid #000!important;\r" +
    "\n" +
    "/*    border-width: 1px!important;\r" +
    "\n" +
    "    border:1px solid #000!important;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader4{\r" +
    "\n" +
    "    background-color: #b3b3b3!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader5{\r" +
    "\n" +
    "    background-color: #FDE4D8!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader6{\r" +
    "\n" +
    "    background-color: #F7AF8A!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeaderReverse{\r" +
    "\n" +
    "    background-color: #0f1a69!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    text-align: center!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotEditable{\r" +
    "\n" +
    "    background-color: yellow!important;\r" +
    "\n" +
    "    color: #0!important;\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "    border:1px solid #000!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotFromData{\r" +
    "\n" +
    "    background-color: #fff!important;\r" +
    "\n" +
    "    color: #0!important;\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotBold{\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHide{\r" +
    "\n" +
    "    display:none;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".handsontable th, .handsontable td{\r" +
    "\n" +
    "    font-size:12px;\r" +
    "\n" +
    "}</style> <div id=\"content\" style=\"padding:0\"> <div class=\"bs-navbar no-content-padding\" style=\"height:40px;margin:0px;padding:7px 15px 10px 7px\"> <div class=\"pull-right\"> <div class=\"btn-group\" style=\"margin-top:-57px\"> <button class=\"zbtn btn btn-default\" ng-click=\"actOpt()\" onclick=\"this.blur()\"> <i class=\"fa fa-fw fa-filter\"></i>&nbsp;Options </button> <button class=\"zbtn btn btn-default\" ng-click=\"actMerge()\" onclick=\"this.blur()\"> <i class=\"fa fa-fw fa-filter\"></i>&nbsp;Options </button> <!--                 <button class=\"zbtn btn btn-default\" ng-click=\"actDel()\" ng-if=\"selectedRows.length>0\">\r" +
    "\n" +
    "                    <i class=\"fa fa-fw fa-remove\"></i>&nbsp;Delete\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "                <button class=\"zbtn btn btn-default\" ng-click=\"actRefresh()\" onclick=\"this.blur();\">\r" +
    "\n" +
    "                    <i class=\"fa fa-fw fa-refresh\"></i>&nbsp;Refresh\r" +
    "\n" +
    "                </button> --> </div> </div> </div> <div class=\"layout_wrapper\"> <div id=\"layoutContainer_bsFormDemoManual\" style=\"position:relative;margin:0px 0px 0px 0px;padding:0px;height:700px\"> <ui-layout options=\"{flow:'column', dividerSize:3, disableToggle:true}\"> <div class=\"inbox-side-bar\" style=\"width:100%;padding:0px\"> <div class=\"well clearfix\" style=\"padding:0px 15px 0px 15px;border:none;background:#fff;box-shadow:none\"> <br> <div class=\"well clearfix\" style=\"padding:0px 15px 0px 15px;border:1px solid #ccc;background:#fff;box-shadow:none\" ng-show=\"showOpt\"> <div> <div class=\"form-group\"> <bsreqlabel>Getsudo Period</bsreqlabel> <bsselect name=\"periodSelect\" ng-model=\"period.selected\" data=\"periodData\" item-text=\"code\" item-value=\"id\" placeholder=\"Select Period...\" on-select=\"selectPeriod(selected)\" required icon=\"fa fa-calendar\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Outlet</bsreqlabel> <bsselect name=\"periodSelect\" ng-model=\"period.selected\" data=\"periodData\" item-text=\"code\" item-value=\"id\" placeholder=\"Select Outlet...\" on-select=\"selectPeriod(selected)\" required icon=\"fa fa-calendar\"> </bsselect> </div> </div> </div> <hot-table hot-id=\"{{'hot_rsspmodel'}}\" settings=\"hot_settings\" datarows=\"data\"> </hot-table> </div> </div> </ui-layout> </div> </div> </div> <!-- <pre>{{db.items[3]}}</pre>\r" +
    "\n" +
    "<pre>{{db.items[4]}}</pre>\r" +
    "\n" +
    "<pre>{{db.items[5]}}</pre> -->"
  );


  $templateCache.put('app/branchorder/rssp_model_clean_1/rssp_model_clean_1.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.hotHeader{\r" +
    "\n" +
    "    background-color: #a3a3a3!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    text-align: center!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader2{\r" +
    "\n" +
    "    background-color: #a3a3a3!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader3{\r" +
    "\n" +
    "    background-color: #d3d3d3!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader4{\r" +
    "\n" +
    "    background-color: #b3b3b3!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader5{\r" +
    "\n" +
    "    background-color: #FDE4D8!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader6{\r" +
    "\n" +
    "    background-color: #F7AF8A!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeaderReverse{\r" +
    "\n" +
    "    background-color: #000!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    text-align: center!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotEditable{\r" +
    "\n" +
    "    background-color: yellow!important;\r" +
    "\n" +
    "    color: #0!important;\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotFromData{\r" +
    "\n" +
    "    background-color: green!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotBold{\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHide{\r" +
    "\n" +
    "    display:none;\r" +
    "\n" +
    "}</style> <hot-table hot-id=\"hot_rsspmodel\" settings=\"hot_settings\" datarows=\"data\"> <!--     <hot-column data=\"title\" title=\"'APRIL GETSUDO'\" type=\"grayedOut\" read-only></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m1\" title=\"'N-2'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m2\" title=\"'N-1'\" width=\"100\" type=\"grayedOut\" read-only></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m3\" title=\"'N'\" width=\"100\" type=\"grayedOut\" read-only></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m4\" title=\"'N+1'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m5\" title=\"'N+2'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m6\" title=\"'N+3'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m7\" title=\"'N+4'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m8\" title=\"'N+5'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m9\" title=\"'N+6'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m10\" title=\"'N+7'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m11\" title=\"'N+8'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m12\" title=\"'N+9'\" width=\"100\"></hot-column> --> </hot-table> <!-- <pre>{{db.items[3]}}</pre>\r" +
    "\n" +
    "<pre>{{db.items[4]}}</pre>\r" +
    "\n" +
    "<pre>{{db.items[5]}}</pre> -->"
  );


  $templateCache.put('app/branchorder/rssp_model_excel_1/rssp_model_excel.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.hotHeader{\r" +
    "\n" +
    "    background-color: #a3a3a3!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    text-align: center!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader2{\r" +
    "\n" +
    "    background-color: #a3a3a3!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader3{\r" +
    "\n" +
    "    background-color: #d3d3d3!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader4{\r" +
    "\n" +
    "    background-color: #b3b3b3!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader5{\r" +
    "\n" +
    "    background-color: #FDE4D8!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeader6{\r" +
    "\n" +
    "    background-color: #F7AF8A!important;\r" +
    "\n" +
    "    color: #000!important;\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotHeaderReverse{\r" +
    "\n" +
    "    background-color: #000!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    text-align: center!important;\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotEditable{\r" +
    "\n" +
    "    background-color: yellow!important;\r" +
    "\n" +
    "    color: #0!important;\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotFromData{\r" +
    "\n" +
    "    background-color: green!important;\r" +
    "\n" +
    "    color: #fff!important;\r" +
    "\n" +
    "    /*text-align: center!important;*/\r" +
    "\n" +
    "    font-weight: normal!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".hotBold{\r" +
    "\n" +
    "    font-weight: 700!important;\r" +
    "\n" +
    "    white-space: nowrap!important;\r" +
    "\n" +
    "}</style> <hot-table hot-id=\"hot_rsspmodel\" settings=\"hot_settings\" datarows=\"data\"> <!--     <hot-column data=\"title\" title=\"'APRIL GETSUDO'\" type=\"grayedOut\" read-only></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m1\" title=\"'N-2'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m2\" title=\"'N-1'\" width=\"100\" type=\"grayedOut\" read-only></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m3\" title=\"'N'\" width=\"100\" type=\"grayedOut\" read-only></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m4\" title=\"'N+1'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m5\" title=\"'N+2'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m6\" title=\"'N+3'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m7\" title=\"'N+4'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m8\" title=\"'N+5'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m9\" title=\"'N+6'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m10\" title=\"'N+7'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m11\" title=\"'N+8'\" width=\"100\"></hot-column>\r" +
    "\n" +
    "    <hot-column data=\"m12\" title=\"'N+9'\" width=\"100\"></hot-column> --> </hot-table> <!-- <pre>{{db.items[3]}}</pre>\r" +
    "\n" +
    "<pre>{{db.items[4]}}</pre>\r" +
    "\n" +
    "<pre>{{db.items[5]}}</pre> -->"
  );

}]);
