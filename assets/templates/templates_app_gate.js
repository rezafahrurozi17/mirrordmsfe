angular.module('templates_app_gate', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/services/gate/gate.html',
    "<script type=\"text/javascript\">// (function(){\r" +
    "\n" +
    "    //   var $document = window.angular.element(document).injector().get('$document');\r" +
    "\n" +
    "    //\r" +
    "\n" +
    "    //   var triggerKeypressEvent = function(char) {\r" +
    "\n" +
    "    //     var event = new Event('keypress');\r" +
    "\n" +
    "    //     event.which = char.charCodeAt(0);\r" +
    "\n" +
    "    //     $document.triggerHandler(event);\r" +
    "\n" +
    "    //   };\r" +
    "\n" +
    "    //\r" +
    "\n" +
    "    //   var barcode = prompt('Text to scan');\r" +
    "\n" +
    "    //   for (var i = 0; i < barcode.length; i++) {\r" +
    "\n" +
    "    //     triggerKeypressEvent(barcode[i]);\r" +
    "\n" +
    "    //   }\r" +
    "\n" +
    "    // })();</script> <script type=\"module\" src=\"/module/barcode/build/browser/index.esm.min.js\"></script> <script src=\"module/barcode/build/browser/index.min.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">#Gate table,\r" +
    "\n" +
    "    #Gate th,\r" +
    "\n" +
    "    #Gate td {\r" +
    "\n" +
    "        border: 1px solid black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #Gate th {\r" +
    "\n" +
    "        height: 50px;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        background-color: grey;\r" +
    "\n" +
    "        font-size: 24px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #Gate th.active {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #Gate .row {\r" +
    "\n" +
    "        margin: 10px 0px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #Gate .btn-gate {\r" +
    "\n" +
    "        margin: 10px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #interactive {\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        height: 500px;\r" +
    "\n" +
    "        /* left: 15%; */\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #interactive video {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #interactive canvas {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .grid .ui-grid-header-cell {\r" +
    "\n" +
    "      /*height: 60px;*/\r" +
    "\n" +
    "      text-align: center;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ui-grid-cell-contents{\r" +
    "\n" +
    "        text-align:center;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ui-grid-header-cell-label{\r" +
    "\n" +
    "        padding-left:20px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .colorRed{\r" +
    "\n" +
    "        color: red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*  .btn{\r" +
    "\n" +
    "    margin: 10px 0px;\r" +
    "\n" +
    "  }*/</style> <bsform-base ng-form=\"GateForm\" loading=\"loading\" get-data=\"getData\" action-button-settings=\"actionButtonSettings\" action-button-settings-detail=\"actionButtonSettingsDetail\" form-api=\"formApi\" form-name=\"GateForm\" icon=\"fa fa-fw fa-child\"> <bsform-base-main> <div id=\"Gate\"> <div ng-if=\"mode=='in' || mode=='out'\" class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th ng-click=\"gateIn();\" style=\"width: 27%\" ng-class=\"{'active' : mode=='in'}\">Gate In</th> <th ng-click=\"gateOut();\" style=\"width: 30%\" ng-class=\"{'active' : mode=='out'}\">Gate Out</th> </tr> </table> </div> </div> <div ng-show=\"mode=='scanIn' || mode=='scanOut'\" class=\"row\"> <div class=\"col-md-12\"> <h4>*Scanner for barcode only - {{debuginfo}}</h4> </div> <div id=\"scandit-barcode-picker\" class=\"col-md-12\" style=\"max-height:600px\"> <!-- <div id=\"interactive\" class=\"viewport\" class=\"col-md-8\">\r" +
    "\n" +
    "                    </div> --> </div> <div class=\"col-md-12\" style=\"text-align: center\"> <input type=\"text\" name=\"CodeInput\" placeholder=\"Manual Scan\" ng-model=\"mData.CodeInput\" class=\"form-control\" style=\"text-transform: uppercase\"> </div> </div> <div ng-if=\"mode=='scanInResult' || mode=='scanOutResult'\" class=\"row\"> <div class=\"col-md-12 text-center\"> <h1>Nomor barcode : {{barcode}}</h1> </div> <div class=\"col-md-12 text-center\"> <h2>tanggal : {{scanDate}}</h2> </div> </div> <div ng-if=\"mode=='scanInResult'\" class=\"row\"> <div class=\"col-md-6\"> <input class=\"rbtn btn btn-lg btn-block\" ng-click=\"GoodRecived()\" type=\"button\" value=\"Good Received\"> </div> <div class=\"col-md-6\"> <input class=\"rbtn btn btn-lg btn-block\" ng-click=\"RepairIn()\" type=\"button\" value=\"Repair In\"> </div> <div class=\"col-md-6\"> <input class=\"rbtn btn btn-lg btn-block\" ng-click=\"ServiceGrBtn()\" type=\"button\" value=\"Service GR (PDS)\"> </div> <div class=\"col-md-6\"> <input class=\"rbtn btn btn-lg btn-block\" ng-click=\"ServiceBpBtn()\" type=\"button\" value=\"Service BP (PDS)\"> </div> </div> <div ng-if=\"mode=='scanOutResult'\" class=\"row\"> <div class=\"col-md-6\"> <input class=\"rbtn btn btn-lg btn-block\" type=\"button\" value=\"Delivery Out\" ng-click=\"DeliveryOut()\"> </div> <div class=\"col-md-6\"> <input class=\"rbtn btn btn-lg btn-block\" type=\"button\" value=\"Repair Out\" ng-click=\"RepairOut()\"> </div> </div> <div ng-if=\"mode=='scanIn' || mode=='scanOut' || mode=='scanInResult' || mode=='scanOutResult'\" class=\"row\"> <div class=\"col-md-12 text-center\"> <input class=\"rbtn btn btn-lg\" ng-show=\"mode=='scanIn' || mode=='scanOut'\" type=\"button\" value=\"Simpan\" ng-click=\"manualScan()\"> <input ng-click=\"gateScanBack();\" class=\"rbtn btn btn-lg\" type=\"button\" value=\"Kembali\"> </div> </div> <div ng-if=\"mode=='in' || mode=='out'\" class=\"row\"> <div class=\"col-md-12\"> <div class=\"input-icon-left\"> <i class=\"fa fa-search\"></i> <input type=\"text\" style=\"text-align:center; text-transform: uppercase\" class=\"form-control input-lg\" name=\"nomorPolisi\" ng-change=\"filterGrid(filter)\" maxlength=\"10\" ng-model=\"filter\" disallow-space placeholder=\"Masukkan Nomor Polisi\"> </div> </div> </div> <div ng-if=\"mode=='in'\" class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th ng-click=\"gateIn();\" class=\"active\">Gate In</th> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridGateIn\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-cellnav ng-style=\"getTableHeightgridGateIn()\" ui-grid-auto-resize class=\"grid\" ng-cloak></div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th ng-click=\"gateIn();\" class=\"active\">Ke Production</th> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridGateInProduction\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-cellnav ui-grid-auto-resize ng-style=\"getTableHeightgridGateInProduction()\" class=\"grid\" ng-cloak></div> </div> </div> </div> </div> <div ng-if=\"mode=='in'\" class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <table style=\"width:100%; margin-bottom:10px\"> <tr> <th ng-click=\"gateIn();\" class=\"active\">Walk In</th> </tr> </table> <div class=\"col-md-12\"> <div class=\"input-icon-left\"> <i class=\"fa fa-search\"></i> <input type=\"text\" style=\"text-align:center;text-transform:uppercase\" maxlength=\"10\" class=\"form-control input-lg\" name=\"nomorPolisi\" ng-keypress=\"allowPattern($event,3,filter.noPolice)\" ng-model=\"mData.LicensePlate\" disallow-space placeholder=\"Masukkan Nomor Polisi\"> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"buyCar(mData)\" ng-disabled=\"disabledSales\"> <img src=\"images/gate/buy-car.png\"> <span style=\"word-wrap: break-word;display: block\">Beli Mobil</span> </button> </div> </div> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"serviceGr(mData)\" ng-disabled=\"disabledGR\"> <img src=\"images/gate/wrench.jpg\"> <span style=\"word-wrap: break-word;display: block\">Service GR</span> </button> </div> </div> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"serviceBp(mData)\" ng-disabled=\"disabledBP\"> <img src=\"images/gate/bp22.jpg\"> <span style=\"word-wrap: break-word;display: block\"> Service BP</span> </button> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"complaint(mData)\" ng-disabled=\"disabledComplaint\"> <img src=\"images/gate/5.jpg\"> <span style=\"word-wrap: break-word;display: block\">Complaint</span> </button> </div> </div> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"othersGr(mData)\" icon=\"fa fa-car\" ng-disabled=\"disabledOthers\"> <img src=\"images/gate/othersgr.jpg\"> <span style=\"word-wrap: break-word;display: block\">Others GR</span> </button> </div> </div> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"takeBp(mData)\" ng-disabled=\"disabledPengambilanBp\"> <img src=\"images/gate/7.jpg\"> <span style=\"word-wrap: break-word;display: block\">Pengambilan BP</span> </button> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"buyParts(mData)\" ng-disabled=\"disabledParts\"> <img src=\"images/gate/4.jpg\"> <span style=\"word-wrap: break-word;display: block\">Beli Parts</span> </button> </div> </div> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"walkInOthers(mData)\" ng-disabled=\"disabledLainnya\"> <img src=\"images/gate/others.png\"> <span style=\"word-wrap: break-word;display: block\"> Lainnya</span> </button> </div> </div> <div class=\"col-md-4 col-xs-4\"> <div class=\"btn-gate\"> <button class=\"wbtn btn btn-lg btn-block\" ng-click=\"sateliteBp(mData)\" ng-disabled=\"disabledSatelite\"> <img src=\"images/gate/9.jpg\"> <span style=\"word-wrap: break-word;display: block\">Penerimaan BP Satelite</span> </button> </div> </div> </div> </div> </div> <div ng-if=\"mode=='in'\" class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active\">Scan In PDS</th> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12 text-center\"> <input ng-click=\"gateScanIn();\" class=\"rbtn btn btn-lg\" type=\"button\" value=\"Scan\"> </div> </div> </div> </div> <div ng-if=\"mode=='out'\" class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active\">Selesai Service</th> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <div id=\"gridGateOutFinishService\" ui-grid=\"{ data: mDataGateOutFinishService, columnDefs: columnDefs }\" class=\"grid\"></div> --> <div ui-grid=\"gridGateOutFinishService\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-cellnav ui-grid-auto-resize ng-style=\"getTableHeightgridGateOutFinishService()\" class=\"grid\" ng-cloak></div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active\">Dari Production</th> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridGateOutProduction\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-cellnav ui-grid-auto-resize ng-style=\"getTableHeightgridGateOutProduction()\" class=\"grid\" ng-cloak></div> </div> </div> </div> </div> </div> </div> <div ng-if=\"mode=='out'\" class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active\">Rawat Jalan</th> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridOutPatient\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-cellnav ui-grid-auto-resize ng-style=\"getTableHeightgridOutPatient()\" class=\"grid\" ng-cloak></div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active\">Visitor</th> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridGateOutPassingBy\" ui-grid-move-columns ui-grid-resize-columns ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-cellnav ui-grid-auto-resize ng-style=\"getTableHeightgridGateOutPassingBy()\" class=\"grid\" ng-cloak></div> </div> </div> </div> </div> </div> </div> <div ng-if=\"mode=='out'\" class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <table style=\"width:100%\"> <tr> <th class=\"active\">Scan Out PDS</th> </tr> </table> </div> </div> <div class=\"row\"> <div class=\"col-md-12 text-center\"> <input ng-click=\"gateScanOut();\" class=\"rbtn btn btn-lg\" type=\"button\" value=\"Scan\"> </div> </div> </div> </div> </div> <bsui-modal show=\"show_modal.show\" title=\"Alasan Kedatangan\" data=\"modal_model\" on-save=\"reasonSave\" on-cancel=\"reasonCancel\" mode=\"modalMode\"> <!-- <div ng-form name=\"contactForm\"> --> <div class=\"col-md-12\"> <div class=\"form-group\"> <label>Alasan Kedatangan</label> <textarea class=\"form-control\" name=\"Alasan\" ng-model=\"modal_model.Reason\" maxlength=\"30\" style=\"height: 120px\"></textarea> </div> </div> <!-- </div> --> </bsui-modal> </bsform-base-main> <bsform-base-detail> </bsform-base-detail> </bsform-base>"
  );

}]);
