angular.module('templates_app_reception_wolistgr', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/services/reception/woHistoryGR/OPL.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Order Pekerjaan Luar</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-12\"> <div class=\"rbtn btn ng-binding ng-scope\" ng-click=\"saveOPLEnd()\" style=\"width:100px\">test</div> </div> <div class=\"col-md-6 col-xs-6\"> <button class=\"ui icon inverted grey button\" style=\"font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px\" ng-click=\"deleteAllDataGridViewWork()\"><i class=\"fa fa-trash\"></i> </button> </div> <div class=\"col-md-6 col-xs-6\"> <button ng-click=\"plushOrderOpl()\" class=\"rbtn btn pull-right\" data-target=\"#ModalOP\" on-deny=\"denied\" data-toggle=\"modal\" data-backdrop=\"static\" data-keyboard=\"false\"><i class=\"fa fa-plus\"></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridWorkView\" class=\"ag-theme-balham\" style=\"height: 400px\"></div> </fieldset> <div class=\"ui modal ModalOPL\" role=\"dialog\" tabindex=\"0\" ng-keydown=\"allowPatternFilter($event)\" settings=\"{'transition':'vertical flip','closable':false, 'keyboard': false }\" style=\"background-color: transparent !important\"> <!--  <div class=\"ui modal ModalOPL\" role=\"dialog\" tabindex=\"0\" ng-keydown=\"allowPatternFilter($event)\" settings=\"{'transition':'vertical flip','closable':false, 'keyboard': false }\"  style=\"left:0% !important; width:75%; margin:-284px auto 0px; background-color: transparent !important;\"> --> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Order Pekerjaan Luar</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <label>Kode Vendor</label> <input type=\"text\" class=\"form-control\" name=\"VendorCode\" ng-model=\"mOpl.VendorCode\" ng-disabled=\"true\" skip-enable> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <bsreqlabel>Vendor</bsreqlabel> <select name=\"vendor\" convert-to-number class=\"form-control\" ng-model=\"mOpl.vendor\" data=\"OplVendor\" placeholder=\"Pilih Vendor\" required ng-disabled=\"viewMode || editmode\" skip-enable ng-change=\"chooseVendor()\"> <option ng-repeat=\"sat in OplVendor\" value=\"{{sat.VendorId}}\">{{sat.Name}}</option> </select> <bserrmsg field=\"Vendor\"></bserrmsg> </div> </div> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">OPL</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-12 col-xs-12\"> <button ng-disabled=\"viewMode\" skip-enable ng-click=\"plushOPL()\" class=\"rbtn btn pull-right\"><i class=\"fa fa-plus\" n></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridDetailOPL\" class=\"ag-theme-balham\" style=\"height: 400px\" id=\"gridParts\"></div> </fieldset> </div> <!-- <label>{{OplVendor}}</label> --> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important;background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"wbtn btn ng-binding\" style=\"width:100px\" ng-click=\"closeModalOpl()\">Cancel</div> <div class=\"rbtn btn ng-binding ng-scope\" ng-disabled=\"viewMode || disableSimpan || masterOPL.length == 0\" skip-enable ng-click=\"saveFinalOpl()\" style=\"width:100px\">Simpan</div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpl.html\"><a>\r" +
    "\n" +
    "                    <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                    <span ng-bind-html=\"match.model.Name | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                    <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                    <span ng-bind-html=\"match.model.isExternalDesc | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                    <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "                </a></script> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/woHistoryGR/Teco.html',
    "<div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <fieldset style=\"border:2px solid #e2e2e2\"> <legend style=\"margin-left:10px;width:inherit;border-bottom:0px\"> WO Information </legend> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-xs-12 col-sm-12 col-md-12\"> <div class=\"col-xs-6 col-sm-6 col-md-6\"> <div class=\"form-group\"> <label class=\"col-xs-4\">Nomor WO </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.WoNo}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-4\">Nomor Polisi </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.PoliceNumber}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-4\">Model </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.ModelType}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-4\">NPWP </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-6\">{{mData.Npwp.Npwp}}</label> </div> </div> <div class=\"col-xs-6 col-sm-6 col-md-6\"> <div class=\"form-group\"> <label class=\"col-xs-6\">Tanggal WO </label> <label class=\"col-xs-1\">:</label> <!-- <label class=\"col-xs-4\">{{mData.WoCreatedDate | date: 'dd-MM-yyyy h:mma'}}</label> --> <label class=\"col-xs-4\">{{mData.WoCreatedDate | date: 'dd-MM-yyyy HH:mm'}}</label> </div> <div class=\"form-group\"> <label class=\"col-xs-6\">Janji Penyerahan</label> <label class=\"col-xs-1\">:</label> <!-- <label class=\"col-xs-4\">{{mData.FixedDeliveryTime1 | date: 'dd-MM-yyyy h:mma'}}</label> --> <label class=\"col-xs-4\">{{mData.FixedDeliveryTime2 | date: 'dd-MM-yyyy HH:mm'}}</label> <!-- <label class=\"col-xs-4\">{{mData.FixedDeliveryTime1 == mData.FixedDeliveryTime2 ? mData.FixedDeliveryTime1 : mData.FixedDeliveryTime2 }}</label> --> </div> <div class=\"form-group\"> <label class=\"col-xs-6\">Status Kendaraan </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-4\">{{mData.MStatusJob.Description}} &nbsp;</label> </div> <div class=\"form-group\"> <label class=\"col-xs-6\">Status Washing </label> <label class=\"col-xs-1\">:</label> <label class=\"col-xs-4\">{{mData.StatusWashing}} &nbsp;</label> </div> </div> </div> </div> </fieldset> </div> <div class=\"col-md-12\"> <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Job Request\" list-title=\"Job Request\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Permintaan\" name=\"reqdesc\" ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "            </textarea> <bserrmsg field=\"reqdesc\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> <div class=\"col-md-6\"> <bslist data=\"JobComplaint\" m-id=\"JobComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Complaint\" list-title=\"Complaint\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <input type=\"text\" skip-enable class=\"form-control\" ng-model=\"lmItem.ComplaintCatg\" name=\"\" ng-disabled=\"true\"> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Deskripsi\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div style=\"margin-bottom:90px\"> <div class=\"form-group\"> <bsreqlabel>Kategori</bsreqlabel> <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Kategori\" required> </bsselect> <bserrmsg field=\"ctype\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom: 90px\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\" required>\r" +
    "\n" +
    "                                </textarea> <bserrmsg field=\"ctext\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> <div class=\"col-md-6\"> <div style=\"text-align:center\">Status</div> <div style=\"height:181px;padding:5px 5px 5px 5px;text-align:center\"> <label> GI Part</label> <h1 style=\"color:red\">{{statusGI}}</h1> </div> <div ng-show=\"showOPLlabel == 1\" style=\"height:181px;padding:5px 5px 5px 5px; text-align:center\"> <label>OPL</label> <h1 style=\"color:red\">{{statusOPL}}</h1> </div> <div ng-show=\"showOPLBPlabel == 1\" style=\"height:181px;padding:5px 5px 5px 5px; text-align:center\"> <label>OPL BP</label> <h1 style=\"color:red\">{{statusOPLBP}}</h1> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"JobTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApi\" confirm-save=\"false\" list-title=\"Order Pekerjaan\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\">{{totalWorkSummary | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{(totalWorkSummary - totalWorkDiscountedSummary) | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalWorkDiscountedSummary | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\">{{totalMaterialSummary | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{(totalMaterialSummary - totalMaterialDiscountedSummary) | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalMaterialDiscountedSummary | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align:right\">{{totalDpEstimate | currency:\"Rp. \":0}}</td> <td></td> <td style=\"text-align:right\">{{totalDpEstimate | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> <div class=\"col-md-12\"> <bslist data=\"tmpDataOPL\" m-id=\"Id\" list-model=\"lmModelOpl\" on-before-save=\"onBeforeSaveOpl\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <strong>{{lmItem.OPLName}}</strong> </div> <div class=\"form-group\"> {{lmItem.VendorName}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Estimasi : </label> {{lmItem.TargetFinishDate | date:'dd-MM-yyyy'}} </div> <div class=\"form-group\"> <label>Quantity : </label> {{lmItem.QtyPekerjaan}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}} </div> </div> </div> </bslist-template> <bslist-modal-form> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMasterOpl.html'\"> </div></bslist-modal-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <tbody> <tr> <td width=\"20%\"><b>Total</b></td> <!-- <td>{{sumWorkOpl | currency:\"Rp. \":0}}</td> --> <td><b>{{(totalWorkDiscountedSummary + totalMaterialDiscountedSummary) | currency:\"Rp. \":0}}</b></td> </tr> </tbody> </table> </div> <div class=\"col-md-12\" ng-show=\"checkIsWarranty == 1\"> <bslist data=\"getWarrantyInfo\" m-id=\"JobTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModelWarr\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" confirm-save=\"false\" list-title=\"Warranty Info\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Claim :</label> <label>{{lmItem.ClaimNo}}</label> </div> <div class=\"form-group\"> <label>No. TWC :</label> <label>{{lmItem.TwcNo}}</label> </div> <div class=\"form-group\"> <label>VDS :</label> <label>{{lmItem.VDS}}</label> </div> <div class=\"form-group\"> <label>VIS :</label> <label>{{lmItem.VIS}}</label> </div> <div class=\"form-group\"> <label>Total Amount :</label> <label>{{lmItem.TotalAmount}}</label> </div> <div class=\"form-group\"> <label>Approved :</label> <label>{{lmItem.Approved}}</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Tipe Claim :</label> <label>{{lmItem.ClaimType}}</label> </div> <div class=\"form-group\"> <label>WMI :</label> <label>{{lmItem.WMI}}</label> </div> <div class=\"form-group\"> <label>CD :</label> <label>{{lmItem.CD}}</label> </div> <div class=\"form-group\"> <label>Status Claim :</label> <label>{{lmItem.StatusClaim}}</label> </div> <div class=\"form-group\"> <label>Error code :</label> <label>{{lmItem.ErrorCode}}</label> </div> </div> </div> </div> </bslist-template> </bslist> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Status TECO : </label> <input type=\"text\" class=\"form-control\" placeholder=\"Status TECO\" name=\"totalDp\"> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> </div> <div class=\"col-md-9\"> <div class=\"btn-group pull-right\"> <!-- katanya cuma buat unteco aja sih ini form nya jd submit sama complete di comment --> <!-- <button type=\"button\" ng-disabled=\"(btnComplete || checkIsWarranty == 1)\" ng-show=\"showSubmitTeco > 0\" class=\"rbtn btn\" ng-click=\"SubmitApprovalTecho(mData,gridWork)\">Submit</button> --> <!-- <button type=\"button\" ng-disabled=\"((checkIsWarranty == 0 && showSubmitTeco > 0 || btnComplete || (oplData.length > 0 && statusOPL !== 'Completed') || (oplBPData.length > 0 && statusOPLBP !== 'Completed') ))\" skip-enable ng-if=\"(mData.Status >= 12 && mData.Status <=13) && showCompleteTeco > 0\" ng-click=\"completeTeco(mData)\" class=\"rbtn btn\">Complete</button> --> <!-- <button type=\"button\" ng-disabled=\"btnComplete\" ng-if=\"mData.Status == 14\" ng-click=\"UncompleteTeco(mData)\" class=\"rbtn btn\">UnTeco</button> --> <button type=\"button\" ng-disabled=\"btnComplete\" ng-click=\"UncompleteTeco(mData)\" class=\"rbtn btn\">UnTeco</button> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/woHistoryGR/customerIndex.html',
    "<div class=\"col-md-12\"> <div class=\"row\"> <uib-accordion close-others=\"oneAtATime\"> <form> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Pelanggan & Kendaraan </div> </uib-accordion-heading> <div class=\"panel panel-default\"> <!-- <div class=\"panel-body\"> --> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Pelanggan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <select ng-show=\"mode == 'search' || mode == 'form'\" class=\"form-control\" ng-model=\"action\" ng-change=\"changeOwner(selected)\">\r" +
    "\n" +
    "                                            <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                                            <option value=\"2\">Input Pemilik Data Baru</option>\r" +
    "\n" +
    "                                        </select>\r" +
    "\n" +
    "                                        <select ng-show=\"mode=='view'\" class=\"form-control\" ng-model=\"action\">\r" +
    "\n" +
    "                                            <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                                            <option value=\"2\">input Pemilik Data Baru</option>\r" +
    "\n" +
    "                                        </select> --> <bsselect ng-show=\"mode == 'search' || mode == 'form'\" on-select=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> </bsselect> <bsselect ng-show=\"mode=='view'\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> </bsselect> </div> </div> <div ng-show=\"mode == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-show=\"editenable && mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-disabled=\"mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> <div class=\"row\" ng-show=\"mode == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null || mFilterCust.action == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-3\"> <label>Toyota ID</label> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" ng-model=\"filterSearch.TID\" placeholder=\"Input Toyota ID\" disallow-space> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelected}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterField\"> <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag != 6)\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" ng-disabled=\"filterSearch.choice\" disallow-space> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag == 6)\"> <bsdatepicker date-options=\"XFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null\" ng-click=\"searchCustomer(mFilterCust.action, filterSearch)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div ng-show=\"mode == 'search'\" class=\"panel-body\"> <div class=\"row\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\" ng-show=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Pelanggan</bsreqlabel> <select class=\"form-control\" ng-model=\"mDataCrm.Customer.CustomerTypeId\" ng-change=\"changeType(mDataCrm.Customer.CustomerTypeId)\"> <option value=\"3\">Individu</option> <option value=\"4\">Pemerintah</option> <option value=\"5\">Yayasan</option> <option value=\"6\">Perusahaan</option> </select> </div> </div> </div> <!--EDIT INSTANSI--> <div class=\"row\" ng-show=\"showFieldInst\" ng-form=\"listInst\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"Name\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\" ng-required=\"requiredFieldInst\"> <em ng-messages=\"listInst.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"NamePic\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\" ng-required=\"requiredFieldInst\"> <em ng-messages=\"listInst.NamePic.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" name=\"PICDepartment\" placeholder=\"PIC Department\" ng-required=\"requiredFieldInst\"> --> <select name=\"PICDepartmentId\" convert-to-number class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\" ng-required=\"requiredFieldInst\"> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> <em ng-messages=\"listInst.PICDepartmentId.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\" name=\"PICHp\" ng-required=\"requiredFieldInst\"> </div> <em ng-messages=\"listInst.PICHp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" maxlength=\"20\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" required placeholder=\"SIUP\" name=\"SIUP\" ng-required=\"requiredFieldInst\"> <em ng-messages=\"listInst.SIUP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\" name=\"PICEmail\" ng-required=\"requiredFieldInst\"> <em ng-messages=\"listInst.PICEmail.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" on-before-cancel=\"onListCancel\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone1\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"form-group\"> <label>No Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone2\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <bsreqlabel>Alamat Utama</bsreqlabel>\r" +
    "\n" +
    "                                                                <bsselect name=\"contact\" ng-model=\"lmModelAddress.MainAddress\" data=\"dataMainAddress\" item-text=\"Description\" item-value=\"Id\" placeholder=\"Pilih Informasi\" skip-disable=\"true\" on-select=\"selectMainAddress(selected)\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                </bsselect>\r" +
    "\n" +
    "                                                            </div> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listInst.$invalid\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> <!--END EDIT INSTANSI--> <!--EDIT PERSONAL--> <div class=\"row\" ng-show=\"showFieldInst\" ng-form=\"listInst\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"Name\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\" ng-required=\"requiredFieldInst\"> <em ng-messages=\"listInst.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"NamePic\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\" ng-required=\"requiredFieldInst\"> <em ng-messages=\"listInst.NamePic.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" name=\"PICDepartment\" placeholder=\"PIC Department\" ng-required=\"requiredFieldInst\"> --> <select name=\"PICDepartmentId\" convert-to-number class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\" ng-required=\"requiredFieldInst\"> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> <em ng-messages=\"listInst.PICDepartment.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\" name=\"PICHp\" ng-required=\"requiredFieldInst\"> </div> <em ng-messages=\"listInst.PICHp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" name=\"SIUP\" ng-required=\"requiredFieldInst\"> <em ng-messages=\"listInst.SIUP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\" name=\"PICEmail\" ng-required=\"requiredFieldInst\"> <em ng-messages=\"listInst.PICEmail.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" on-before-cancel=\"onListCancel\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"form-group\"> <label>No Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <bsreqlabel>Alamat Utama</bsreqlabel>\r" +
    "\n" +
    "                                                                <bsselect name=\"contact\" ng-model=\"lmModelAddress.MainAddress\" data=\"dataMainAddress\" item-text=\"Description\" item-value=\"Id\" placeholder=\"Pilih Informasi\" skip-disable=\"true\" on-select=\"selectMainAddress(selected)\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                </bsselect>\r" +
    "\n" +
    "                                                            </div> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listInst.$invalid\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldPersonal\" ng-form=\"listPersonal\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" placeholder=\"Gelar Depan\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <!-- style=\"text-transform: uppercase;\"  --> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama\" required name=\"Name\"> <!-- style=\"text-transform: uppercase;\" --> <em ng-messages=\"listPersonal.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" placeholder=\"Gelar Belakang\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Lahir</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"BirthDate\" ng-model=\"mDataCrm.Customer.BirthDate\" date-options=\"dateOptionsBirth\" max-date=\"maxDateOption.maxDate\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" required> </bsdatepicker> <!-- <bserrmsg field=\"BirthDate\"></bserrmsg> --> <em ng-messages=\"listPersonal.BirthDate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" placeholder=\"Handphone 1\" ng-keypress=\"allowPattern($event,1)\" required name=\"HandPhone1\"> <em ng-messages=\"listPersonal.HandPhone1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" placeholder=\"Handphone 2\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" required ng-model=\"mDataCrm.Customer.KTPKITAS\" placeholder=\"No. KTP/KITAS\" ng-keypress=\"allowPattern($event,2)\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> <!-- required --> <em ng-messages=\"listPersonal.KTPKITAS.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                                <bsreqlabel>No. NPWP</bsreqlabel>\r" +
    "\n" +
    "                                                <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No. NPWP\"ng-keypress=\"allowPattern($event,1)\" required name=\"Npwp\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                                <em ng-messages=\"listPersonal.Npwp.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "                                                <p ng-message=\"required\">Wajib diisi</p>\r" +
    "\n" +
    "                                                <p ng-message=\"minlength\">Jml. Karakter kurang</p>\r" +
    "\n" +
    "                                                <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p>\r" +
    "\n" +
    "                                                </em>\r" +
    "\n" +
    "                                            </div> --> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No. NPWP\" name=\"Npwp\" ng-keypress=\"allowPattern($event,1)\" minlength=\"20\" maxlength=\"20\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listPersonal.Npwp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <!-- Pending Dulu Kata Pak Ian --> <!-- <div class=\"row\">\r" +
    "\n" +
    "                                        <div class=\"col-md-12\">\r" +
    "\n" +
    "                                            KETERANGAN NPWP DISINI\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div> --> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-show-detail=\"onShowDetailAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" custom-button-settings-detail=\"listCustomButtonSettingsDetailAdress\" on-after-save=\"onAfterSaveAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" ng-keypress=\"allowPattern($event,1)\" maxlength=\"15\"> <!-- ng-keypress=\"allowPattern($event,1)\" --> </div> <div class=\"form-group\"> <label>No. Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" ng-keypress=\"allowPattern($event,1)\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <bsreqlabel>Alamat Utama</bsreqlabel>\r" +
    "\n" +
    "                                                                <bsselect name=\"contact\" ng-model=\"lmModelAddress.MainAddress\" data=\"dataMainAddress\" item-text=\"Description\" item-value=\"Id\" placeholder=\"Pilih Informasi\" skip-disable=\"true\" on-select=\"selectMainAddress(selected)\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                </bsselect>\r" +
    "\n" +
    "                                                            </div> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listPersonal.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listPersonal.$invalid\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> </div> <!--END EDIT PERSONAL--> </div> <!-- untuk tampilan depan --> <div class=\"panel-body\" ng-hide=\"mode =='search'\"> <div class=\"row\" ng-show=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <!-- <input type=\"text\" ng-readonly='true' class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" placeholder=\"PIC Department\"> --> <select name=\"picDepartment\" convert-to-number ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\"> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\"> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc}}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT/RW : {{ lmItem.RT }}/{{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"> </bsselect> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"form-group\"> <label>No. Telepon 2 </label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\"> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Propinsi...\" on-select=\"selectProvince(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId==undefined\" on-select=\"selectRegency(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId==undefined\" on-select=\"selectDistrict(selected)\"> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan... \" ng-disabled=\"lmModelAddress.DistrictId==undefined \" on-select=\"selectVillage(selected)\"> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> </div> </div> <div class=\"row\" ng-hide=\"showInst \"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text \" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle \" ng-readonly=\"true\" placeholder=\"Gelar Depan \"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text \" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name \" ng-readonly=\"true\" placeholder=\"Nama \"> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text \" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle \" ng-readonly=\"true\" placeholder=\"Gelar Belakang \"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14 \" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1 \" ng-readonly=\"true\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 2</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14 \" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2 \" ng-readonly=\"true\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.KTPKITAS\" ng-readonly=\"true\" minlength=\"16\" maxlength=\"16\" placeholder=\"No. KTP/KITAS\" ng-keypress=\"allowPattern($event,1)\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" ng-readonly=\"true\" placeholder=\"No. NPWP\" name=\"Npwp\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address1 \" data=\"CustomerAddress \" m-id=\"CustomerAddressId \" on-select-rows=\"onListSelectRows \" selected-rows=\"listSelectedRows \" confirm-save=\"true \" list-model=\"lmModelAddress\r" +
    "\n" +
    "                                                                    \" list-title=\"Alamat Pelanggan \" on-before-save=\"onBeforeSaveAddress \" list-api=\"listApiAddress \" on-before-edit=\"onBeforeEditAddress \" on-before-delete=\"onBeforeDeleteAddress \" modal-title=\"Alamat Pelanggan \" button-settings=\"listView \" custom-button-settings=\"listView\r" +
    "\n" +
    "                                                                    \" list-height=\"200 \" ng-disabled=\"true \" skip-enabled> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc}}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress \" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress==1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT/RW : {{ lmItem.RT }}/{{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact \" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory\r" +
    "\n" +
    "                                                                    Kontak... \" skip-disable=\"disPng \" on-select=\"selectPendapatan(selected)\"> </bsselect> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone \" ng-model=\"lmModelAddress.Phone1\" ng-keypress=\"allowPattern($event,1)\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone \" ng-model=\"lmModelAddress.Phone2\" ng-keypress=\"allowPattern($event,1)\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text \" name=\"add \" class=\"form-control\" ng-model=\"lmModelAddress.Address\"> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text \" name=\"rt \" class=\"form-control\" skip-disable=\"disPng \" ng-model=\"lmModelAddress.RT \" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text \" name=\"rw \" class=\"form-control\" skip-disable=\"disPng \" ng-model=\"lmModelAddress.RW \" maxlength=\"3\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi \" ng-model=\"lmModelAddress.ProvinceId \" data=\"provinsiData \" item-text=\"ProvinceName \" item-value=\"ProvinceId \" placeholder=\"Pilih Propinsi... \" on-select=\"selectProvince(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> </div> </div> </div> <hr width=\"95%\" style=\"height:3px;background-color:grey\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Kendaraan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <select ng-show=\"modeV == 'search' || modeV == 'form'\" class=\"form-control\" ng-model=\"actionV\" ng-change=\"changeVehic(actionV)\"> --> <!-- <select ng-show=\"modeV == 'search' || modeV == 'form'\" class=\"form-control\" ng-model=\"actionV\" ng-change=\"changeVehic(actionV)\" ng-options=\"value.Id as value.Value for value in actionVdata\">\r" +
    "\n" +
    "                                            <option value=\"\"> </option>\r" +
    "\n" +
    "                                            <option value=\"1\">Cari Vehicle</option>\r" +
    "\n" +
    "                                            <option value=\"2\">Input Data Vehicle Baru</option>\r" +
    "\n" +
    "                                        </select> --> <bsselect ng-show=\"modeV == 'search' || modeV == 'form'\" on-select=\"changeVehic(actionV)\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> </bsselect> <bsselect ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> </bsselect> <!-- <select ng-show=\"modeV == 'view'\" ng-model=\"actionV\" class=\"form-control\">\r" +
    "\n" +
    "                                            <option value=\"\"> </option>\r" +
    "\n" +
    "                                            <option value=\"1\">Cari Vehicle</option>\r" +
    "\n" +
    "                                            <option value=\"2\">Input Data Vehicle Baru</option>\r" +
    "\n" +
    "                                        </select> --> </div> </div> <div ng-show=\"modeV == 'search'\" class=\"col-md-4\"> <!--<input type=\"text\" class=\"form-control\" name=\"Km\" placeholder=\"\" ng-model=\"textV\" ng-hide=\"actionV == 3 || actionV == null\" ng-maxlength=\"50\" style=\"display:inline;width:190px;\"> &nbsp;&nbsp;\r" +
    "\n" +
    "                                        <button class=\"rbtn btn\" style=\"display:inline;\" ng-hide=\"actionV == 3 || actionV == null\" ng-click=\"searchVehicle(actionV, textV)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button>--> </div> <div class=\"col-md-4\"> <div class=\"btn-group pull-right\"> <button type=\"button\" ng-click=\"cancelVehic()\" ng-show=\"!disVehic\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"saveVehic(actionV, mDataCrm, isTAM)\" ng-show=\"!disVehic\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <button type=\"button\" ng-click=\"editVehic(actionV)\" ng-hide=\"!disVehic && modeV=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> </div> <div class=\"row\" ng-show=\"modeV == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"actionV == 2 || actionV == null\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelectedV}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterFieldV\"> <a href=\"#\" ng-click=\"filterFieldChangeV(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearchV.filterValue\" ng-maxlength=\"15\" ng-keypress=\"allowPatternFilterSrc($event,actionV,filterSearchV)\" ng-disabled=\"filterSearchV.choice\" disallow-space> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"actionV == 2 || actionV == null\" ng-click=\"searchVehicle(actionV, filterSearchV)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div class=\"panel-body\" ng-form=\"listVehicle\" ng-hide=\"!hideVehic\" novalidate> <div class=\"col-md-12\" ng-show=\"!disVehic\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tipe Kendaraan</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changeTypeVehic1\" ng-disabled=\"disableCheckTam\" ng-model=\"isTAM\" name=\"radio4\" value=\"1\" checked> <label for=\"changeTypeVehic1\">TAM</label> <input type=\"radio\" id=\"changeTypeVehic2\" ng-disabled=\"disableCheckTam\" ng-model=\"isTAM\" name=\"radio4\" value=\"0\"> <label for=\"changeTypeVehic2\">Non TAM</label> </div> </div> </div> </div> <div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Polisi</bsreqlabel> <input name=\"noPolice\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.LicensePlate\" ng-maxlength=\"10\" ng-keypress=\"allowPatternFilter($event,3,filter.noPolice)\" placeholder=\"No Polisi\" required> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No Rangka</bsreqlabel> <input name=\"VIN\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null))\" skip-enable ng-model=\"mDataCrm.Vehicle.VIN\" placeholder=\"No Rangka\" required ng-maxlength=\"20\" ng-minlength=\"8\" maxlength=\"20\"> <em ng-messages=\"listVehicle.VIN.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                <bsreqlabel>Model Code</bsreqlabel>\r" +
    "\n" +
    "                                                <input type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ModelCode\" placeholder=\"Model Code\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div> --> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <!-- <bsselect data=\"VehicM\" name=\"vehicModel\" on-select=\"selectedModel(selected)\" item-value=\"VehicleModelId\" item-text=\"VehicleModelName\" ng-model=\"mDataCrm.Vehicle.Model\" placeholder=\"Vehicle Model\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select name=\"vehicModel\" ng-if=\"mFilterTAM.isTAM == 1 && mDataCrm.Vehicle.SourceDataId !== null \" ng-model=\"mDataCrm.Vehicle.Model\" data=\"VehicM\" on-select=\"selectedModel($item)\" ng-required=\"true\" search-enabled=\"false\" ng-disabled=\"disVehic\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Model\"> {{$select.selected.VehicleModelName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicM\"> <span ng-bind-html=\"item.VehicleModelName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelName\" required name=\"vehicModel\" placeholder=\"Model Type\"> <input type=\"text\" ng-disabled=\"disVehic || mDataCrm.Vehicle.SourceDataId == null\" skip-enable ng-if=\"mFilterTAM.isTAM == 1 && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.Model.VehicleModelName\" required name=\"vehicModel\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <!--<bsselect name=\"vehicType\" data=\"VehicT\" on-select=\"selectedType(selected)\" item-value=\"VehicleTypeId\" item-text=\"NewDescription\" ng-model=\"mDataCrm.Vehicle.Type\" placeholder=\"Vehicle Type\" ng-readonly=\"enableType\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select data=\"VehicT\" name=\"vehicType\" ng-if=\"mFilterTAM.isTAM == 1 && mDataCrm.Vehicle.SourceDataId !== null\" ng-model=\"mDataCrm.Vehicle.Type\" on-select=\"selectedType($item)\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.NewDescription}} </ui-select-match> <ui-select-choices repeat=\"item in VehicT\"> <span ng-bind-html=\"item.NewDescription\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelType\" required name=\"vehicType\" placeholder=\"Model Type\"> <input type=\"text\" ng-disabled=\"disVehic || mDataCrm.Vehicle.SourceDataId == null\" skip-enable ng-if=\"mFilterTAM.isTAM == 1 && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.Type.NewDescription\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicType.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-readonly=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Warna\"> --> <!-- <bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"ColorName\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select name=\"colorMobil\" ng-model=\"mDataCrm.Vehicle.Color\" data=\"Color\" on-select=\"selectedColor($item.MColor)\" ng-if=\"isTAM == 1\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Warna\"> {{$select.selected.ColorName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicColor\"> <span ng-bind-html=\"item.MColor.ColorName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ColorName\" required name=\"vehicTypeColor\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\" ng-show=\"isCustomColor\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna Lain</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mData.VehicleColor\" placeholder=\"Warna\"> <!--<bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"VehicleColor\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\" ng-if=\"isTAM == 1\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Warna</bsreqlabel> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCode\" placeholder=\"Kode Warna\" required> <em ng-messages=\"listVehicle.colorCode.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bahan Bakar</bsreqlabel> <bsselect data=\"VehicGas\" on-select=\"selectedGas(selected)\" item-value=\"VehicleGasTypeId\" name=\"fuel\" item-text=\"VehicleGasTypeName\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required> </bsselect> <!-- <ui-select name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" on-select=\"selectedGas($item)\" item-text=\"VehicleGasTypeName\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\">\r" +
    "\n" +
    "                                                <ui-select-match allow-clear placeholder=\"Vehicle Gas Type\">\r" +
    "\n" +
    "                                                    {{$select.selected.VehicleGasTypeName}}\r" +
    "\n" +
    "                                                </ui-select-match>\r" +
    "\n" +
    "                                                <ui-select-choices repeat=\"item in VehicGas\">\r" +
    "\n" +
    "                                                    <span ng-bind-html=\"item.VehicleGasTypeName\"></span>\r" +
    "\n" +
    "                                                </ui-select-choices>\r" +
    "\n" +
    "                                            </ui-select> --> <em ng-messages=\"listVehicle.fuel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tahun Rakit</bsreqlabel> <!-- <input name=\"tahunRakit\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enableng-keypress=\"allowPattern($event,1)\" ng-model=\"mDataCrm.Vehicle.AssemblyYear\" placeholder=\"Tahun Rakit\" required> --> <input name=\"tahunRakit\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enableng-keypress=\"allowPattern($event,1)\" ng-model=\"mDataCrm.Vehicle.AssemblyYear\" placeholder=\"Tahun Rakit\" required> <em ng-messages=\"listVehicle.tahunRakit.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\" ng-if=\"isTAM == 1\"> <div class=\"form-group\" show-errors> <bsreqlabel>Full Model</bsreqlabel> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCode\" placeholder=\"Katashiki Code\" required> <em ng-messages=\"listVehicle.fullModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Mesin</bsreqlabel> <!-- <input type=\"text\" name=\"NoMesin\" style=\"text-transform: uppercase;\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.EngineNo\" placeholder=\"No. Mesin\" required> --> <input type=\"text\" name=\"NoMesin\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable ng-model=\"mDataCrm.Vehicle.EngineNo\" placeholder=\"No. Mesin\" required> <em ng-messages=\"listVehicle.NoMesin.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Asuransi</label> <input type=\"text\" name=\"Asuransi\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Insurance\" placeholder=\"Asuransi\"> <em ng-messages=\"listVehicle.Asuransi.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Polis Asuransi</label> <!-- <input type=\"text\" name=\"noSPK\" style=\"text-transform: uppercase;\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.SPK\" placeholder=\"No. Polis Asuransi\"> --> <input type=\"text\" name=\"noSPK\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable ng-model=\"mDataCrm.Vehicle.SPK\" placeholder=\"No. Polis Asuransi\"> <em ng-messages=\"listVehicle.noSPK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal DEC</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!-- <bsdatepicker name=\"DateDEC\" ng-model=\"mDataCrm.Vehicle.DECDate\" date-options=\"dateOptionsBirth\" max-date='maxDateOption.maxDate' placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsdatepicker> --> <bsdatepicker name=\"DateDEC\" ng-model=\"mDataCrm.Vehicle.DECDate\" ng-if=\"disVehic == false\" date-options=\"dateOptionsBirth\" max-date=\"maxDateS\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" ng-disabled=\"disVehic\" skip-enable required> </bsdatepicker> <input name=\"DateDEC\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal Lahir\" required> <em ng-messages=\"listVehicle.DateDEC.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama STNK</bsreqlabel> <input type=\"text\" name=\"nameStnk\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKName\" placeholder=\"Nama STNK\" required> <em ng-messages=\"listVehicle.nameStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat STNK</bsreqlabel> <input name=\"addressStnk\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKAddress\" placeholder=\"Alamat STNK\" required> <em ng-messages=\"listVehicle.addressStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Masa Berlaku STNK</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"dateSTNK\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" date-options=\"dateDECOptions\" placeholder=\"Masa Berlaku STNK\" required> </bsdatepicker> <em ng-messages=\"listVehicle.dateSTNK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"modeV=='form' || modeV=='search'\" ng-click=\"saveVehic(actionV, mDataCrm, isTAM);\" ng-disabled=\"listVehicle.$invalid\" skip-enable class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <button type=\"button\" ng-click=\"cancelSearchV()\" ng-show=\"modeV=='form' || modeV=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> <!-- </div> --> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" ng-show=\"showInst\"> <uib-accordion-heading> Penanggung Jawab </uib-accordion-heading> <div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPnj()\" ng-hide=\"!disPnj\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"savePnj()\" ng-show=\"!disPnj\" class=\"rbtn btn\">Save</button> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Lengkap</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" ng-readonly=\"disPnj\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" ng-readonly=\"disPnj\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Pengguna </div> </uib-accordion-heading> <div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPng()\" ng-hide=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelPng(mData.US)\" ng-show=\"!disPng\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"savePng(mData.US, CustomerVehicleId, mDataCrm)\" ng-show=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" ng-show=\"!disPng\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-show=\"!hiddenUserList\"> <label>List Pengguna</label> <bsselect ng-model=\"mData.US\" data=\"pngList\" item-text=\"name\" item-value=\"vehicleUserId\" placeholder=\"Pilih Pengguna\" on-select=\"onChangePng(selected)\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Lengkap</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mData.US.name\" ng-readonly=\"disPng\"> <input type=\"text\" class=\"form-control\" ng-model=\"mData.US.vehicleUserId\" ng-hide=\"true\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mData.US.phoneNumber1\" ng-readonly=\"disPng\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mData.US.phoneNumber2\" ng-readonly=\"disPng\" ng-keypress=\"allowPattern($event,1)\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Hubungan dengan Pemilik</bsreqlabel> <bsselect data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" item-value=\"RelationId\" item-text=\"RelationDesc\" ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable> </bsselect> </div> </div> </div> </div> <!-- <div class=\"row\" ng-show=\"!disPng\">\r" +
    "\n" +
    "                        <div class=\"btn-group pull-right\">\r" +
    "\n" +
    "                            <button type=\"button\" class=\"rbtn btn\" ng-click=\"addPengguna()\">\r" +
    "\n" +
    "                            <i class=\"fa fa-fw fa-plus\"></i>&nbsp;Pengguna\r" +
    "\n" +
    "                            </button>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div> --> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Kontak Person & Pengambil Keputusan </div> </uib-accordion-heading> <div class=\"col-md-12\"> <!--<div class=\"row\">\r" +
    "\n" +
    "                            <div class=\"col-md-12 text-right\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>--> <div class=\"row\"> <div smart-include=\"app/services/reception/wo/includeForm/CPPKInfo.html\"> </div> </div> </div> </div> </form> </uib-accordion> </div> </div>"
  );


  $templateCache.put('app/services/reception/woHistoryGR/dialog_ReqServiceHistory.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"><h3 class=\"panel-title\">Permohonan Approval</h3> </div> <!-- {{DataSelect}} --> <div class=\"panel-body\"> <form method=\"post\"> <div class=\"row\"> <div class=\"col-md-10\"> <p><i>Cetakan ini membutuhkan Approval.</i></p> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label class=\"control-label\">Nama Kegiatan </label> </div> </div> <div class=\"col-md-7\"> <div class=\"form-group\"> <label class=\"control-label\">Cetak Riwayat Servis </label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label class=\"control-label\">Nomor Request </label> </div> </div> <div class=\"col-md-7\"> <div class=\"form-group\"> <label class=\"control-label\"> </label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label class=\"control-label\">Tanggal dan Jam </label> </div> </div> <div class=\"col-md-7\"> <div class=\"form-group\"> <label class=\"control-label\">{{DataSelect.dateTimee}} </label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-5\"> <div class=\"form-group\"> <label class=\"control-label\">Pesan </label> </div> </div> <div class=\"col-md-7\"> <textarea class=\"form-control\" rows=\"4\" ng-model=\"DataSelect.RequestReason\">\r" +
    "\n" +
    "\t\t\t\t</textarea> </div> </div> <br> <!-- <div class=\"row\">\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-5\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"form-group\">\r" +
    "\n" +
    "\t\t\t\t\t<label class=\"control-label\">\r" +
    "\n" +
    "\t\t\t\t\t</label>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-7\">\r" +
    "\n" +
    "\t\t\t\t<textarea class=\"form-control\" ng-model=\"ApproveData.CancelReasonDesc\" rows=\"4\">\r" +
    "\n" +
    "\t\t\t\t\tMohon untuk approve list dari item Sales Order berikut. Terima kasih.\r" +
    "\n" +
    "\t\t\t\t</textarea>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</div>  --> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div> <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0)\">Batal</button> <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"kirimApproval(DataSelect)\" style=\"color:white; background-color:#D53337\">Kirim Permohonan</button> </div> </div> </div> </div> </form> </div> </div> <!-- <pre>{{ApproveData}}</pre> -->"
  );


  $templateCache.put('app/services/reception/woHistoryGR/modalFormDetail.html',
    "<div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Material</bsreqlabel> <bsselect required ng-required=\"true\" ng-disabled=\"ldModel.PartsId !== undefined && ldModel.PartsId !== null\" name=\"tipe\" ng-disabled=\"isBilling\" ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" item-text=\"Name\" item-value=\"MaterialTypeId\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeParts(selected)\"> </bsselect> <em ng-messages=\"row1.tipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"typeMaterial\"></bserrmsg> --> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label>No. Material</label> <bscheckbox ng-change=\"checkIsOPB(ldModel.IsOPB, ldModel)\" ng-disabled=\"!(ldModel.MaterialTypeId == 2) || (ldModel.PartsCode !== undefined && ldModel.PartsCode !== '' &&  ldModel.PartsCode !== null) || ldModel.JobPartsId !== undefined \" class=\"pull-right\" ng-model=\"ldModel.IsOPB\" label=\"OPB\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div class=\"col-xs-8\" style=\"padding-left:0\"> <bstypeahead placeholder=\"No. Material\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" ng-change=\"ubahPartsCode(ldModel)\" get-data=\"getParts\" item-text=\"PartsCode\" ng-disabled=\"(ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null) || ldModel.IsOPB == 1\" ng-maxlength=\"250\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectParts\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResultP\" ta-minlength=\"4\" template-url=\"customTemplate.html\" icon=\"fa-id-card-o\"> </bstypeahead> </div> <div class=\"col-xs-4\"> <button class=\"rbtn btn\" ng-disabled=\"!(ldModel.Qty !== null && ldModel.Qty !== undefined && partsAvailable)\" ng-click=\"checkAvailabilityParts(ldModel)\">Cek Ketersediaan</button> </div> <bserrmsg field=\"noMaterial\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Material</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"namaMaterial\" ng-model=\"ldModel.PartsName\" ng-maxlength=\"64\" ng-disabled=\"ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null\" required> <bserrmsg field=\"namaMaterial\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input step=\"1\" type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" ng-disabled=\"isBilling || ldModel.MaterialRequestStatusId == 80\" required min=\"1\" class=\"form-control\" name=\"qty\" ng-model=\"ldModel.Qty\"> <em ng-messages=\"row1.qty.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\"> Nilai Qty Harus Satuan</p> </em> <!-- <bserrmsg field=\"qty\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan</bsreqlabel> <bsselect name=\"satuan\" ng-disabled=\"isBilling || isLock == true\" ng-model=\"ldModel.SatuanId\" on-select=\"selectTypeUnitParts(selected)\" data=\"unitData\" item-text=\"Name\" required item-value=\"MasterId\" placeholder=\"Pilih Tipe\"> </bsselect> <em ng-messages=\"row1.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Tipe Order</label> <bsselect name=\"satuan\" ng-model=\"ldModel.Type\" data=\"typeMData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Tipe\" ng-disabled=\"DisTipeOrder || isLock == true\" skip-enable> </bsselect> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\"> <label>ETA</label><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!-- <bsdatepicker name=\"DateAppoint\" ng-disabled=\"partsAvailable\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\">\r" +
    "\n" +
    "            </bsdatepicker> --> <bsdatepicker name=\"DateAppoint\" ng-disabled=\"true\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\"> </bsdatepicker> </div> <div class=\"form-group\" show-errors> <label>Ketersedian</label> <input type=\"text\" min=\"0\" class=\"form-control\" ng-disabled=\"true\" name=\"ketersedian\" ng-model=\"ldModel.Availbility\"> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!--  ng-disabled=\"!isBilling\" --> <bsselect name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypePaidParts(selected)\" placeholder=\"Pilih Pembayaran\" ng-disabled=\"JobIsWarranty == 1\" required> </bsselect> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"0\" class=\"form-control\" name=\"harga\" ng-if=\"ldModel.IsOPB != 1\" ng-maxlength=\"10\" ng-required=\"ldModel.PartsCode != null && ldModel.PartsCode != undefined\" ng-disabled=\"(ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\"> <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"1\" class=\"form-control\" name=\"hargaOPB\" ng-if=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-required=\"ldModel.IsOPB == 1\" ng-disabled=\"(ldModel.IsOPB == 1) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\"> <em ng-messages=\"row1.harga.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <em ng-messages=\"row1.hargaOPB.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Harga Tidak Boleh 0</p> </em> <!-- <bserrmsg field=\"harga\"></bserrmsg> --> </div> <!--     <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "      <label>Diskon</label>\r" +
    "\n" +
    "      <input type=\"number\" min=0 max=\"100\" class=\"form-control\" name=\"harga\" ng-model=\"ldModel.Discount\">\r" +
    "\n" +
    "      <bserrmsg field=\"harga\"></bserrmsg>\r" +
    "\n" +
    "    </div> --> <div class=\"form-group\" show-errors> <label>Sub Total</label> <input type=\"number\" ng-hide=\"true\" ng-disabled=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotal\"> <input type=\"text\" ng-disabled=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.xsubTotal\" currency-formating number-only> <bserrmsg field=\"subTotal\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Dp</label> <!-- <input type=\"text\" currency-formating number-only ng-change=\"checkDp(ldModel.DownPayment, ldModel)\" min=0 ng-maxlength=\"10\" ng-disabled=\"isBilling ||disableDP || (ldModel.PaidById == 2277 || ldModel.PaidById == 30 || ldModel.PaidById == 31 || ldModel.PaidById == 2458) || ldModel.Availbility == 'Tersedia'\" skip-enable class=\"form-control\" name=\"Dp\" ng-model=\"ldModel.DownPayment\"> --> <input type=\"text\" currency-formating number-only ng-change=\"checkDp(ldModel.DownPayment, ldModel)\" min=\"0\" ng-maxlength=\"10\" ng-disabled=\"isBilling || ((disableDP || disableDP == null || disableDP == undefined) && ldModel.PaidById != 28) || ldModel.Availbility == 'Tersedia'\" skip-enable class=\"form-control\" name=\"Dp\" ng-model=\"ldModel.DownPayment\"> <bserrmsg field=\"Dp\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypeDiskonParts(selected)\" ng-disabled=\"ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1 || (ldModel.IsOPB != 1 && (ldModel.PartsCode == '' || ldModel.PartsCode == null || ldModel.PartsCode == undefined))\" placeholder=\"Pilih Tipe Diskon\" required> </bsselect> <em ng-messages=\"row1.diskon.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>List Diskon</bsreqlabel> <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1 || ldModel.DiscountTypeId == 10 || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonPart(selected,ldModel.DiscountTypeId)\" placeholder=\"Pilih List Diskon\" ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\"> </bsselect> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-required=\"isCustomTask\" ng-change=\"changeDiscount(ldModel.subTotal,ldModel.Discount)\" ng-disabled=\"ldModel.DiscountTypeId != 0 || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"ldModel.Discount\"> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.PartsName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> <div style=\"margin-top:50px; float: left\"> &nbsp; </div> </div>"
  );


  $templateCache.put('app/services/reception/woHistoryGR/modalFormMaster.html',
    "<div class=\"col-md-12\" ng-form=\"row2\"> <div ng-if=\"!isLock\"> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <bsselect name=\"tipe\" ng-model=\"lmModel.JobTypeId\" data=\"taskCategory\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"isRelease\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeWork(selected)\"> </bsselect> <em ng-messages=\"row2.tipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pekerjaan</bsreqlabel> <bstypeahead ng-if=\"mFilterTAM.isTAM == 1\" placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"lmModel.TaskName\" get-data=\"getWork\" ng-disabled=\"isRelease\" item-text=\"TaskName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWork\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" icon=\"fa-id-card-o\" required> </bstypeahead> <input ng-if=\"mFilterTAM.isTAM != 1\" placeholder=\"Nama Pekerjaan\" type=\"text\" required ng-disabled=\"isRelease\" class=\"form-control\" name=\"jobName\" ng-model=\"lmModel.TaskName\"> <!--       <input placeholder=\"Pilih Pekerjaan\"\r" +
    "\n" +
    "                ng-minlength=\"3\"\r" +
    "\n" +
    "                ng-maxlength=50 \r" +
    "\n" +
    "                \r" +
    "\n" +
    "                disallow-space\r" +
    "\n" +
    "                uib-typeahead=\"xuser.Name for xuser in getWork($viewValue)\"\r" +
    "\n" +
    "                typeahead-on-select=\"onSelectWork($item, $model, $label)\" \r" +
    "\n" +
    "                ng-model-options=\"modelOptions\"\r" +
    "\n" +
    "                typeahead-loading=\"loadingUsers\" \r" +
    "\n" +
    "                typeahead-no-results=\"noResults\" \r" +
    "\n" +
    "                typeahead-min-length=\"2\"\r" +
    "\n" +
    "                type=\"text\"\r" +
    "\n" +
    "                ng-disabled=\"lmModel.Type = null\"\r" +
    "\n" +
    "                ng-model=\"lmModel.TaskName\"\r" +
    "\n" +
    "                class=\"form-control\"\r" +
    "\n" +
    "                name=\"jobName\"\r" +
    "\n" +
    "                \r" +
    "\n" +
    "        > --> <em ng-messages=\"row2.jobName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"jobName\"></bserrmsg> --> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label class=\"pull-left\">Additional OpeNo</label> <bscheckbox ng-change=\"getDataOpeNo(lmModel.TaskName,lmModel.isOpe,lmModel)\" class=\"pull-right\" ng-model=\"lmModel.isOpe\" label=\"Ope No\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div class=\"col-md-12\" style=\"padding-left:0;padding-right:0;padding-bottom:15px\"> <!-- <bstypeahead \r" +
    "\n" +
    "                            placeholder=\"Task Code\" \r" +
    "\n" +
    "                            name=\"noMaterial\" \r" +
    "\n" +
    "                            ng-model=\"lmModel.TaskCode\" \r" +
    "\n" +
    "                            get-data=\"getTaskOpe\" \r" +
    "\n" +
    "                            item-text=\"TaskCode\" \r" +
    "\n" +
    "                            ng-maxlength=\"250\" \r" +
    "\n" +
    "                            disallow-space \r" +
    "\n" +
    "                            loading=\"loadingUsers\" \r" +
    "\n" +
    "                            noresult=\"noResults\" \r" +
    "\n" +
    "                            selected=\"selected\" \r" +
    "\n" +
    "                            on-select=\"onSelectOpeNo\"\r" +
    "\n" +
    "                            on-noresult=\"onNoOpeResult\" \r" +
    "\n" +
    "                            on-gotresult=\"onGotResult\" \r" +
    "\n" +
    "                            ta-minlength=\"4\"\r" +
    "\n" +
    "                            ng-disabled=\"lmModel.isOpe == 0\" \r" +
    "\n" +
    "                            template-url=\"customTemplateOpe.html\" \r" +
    "\n" +
    "                            icon=\"fa-id-card-o\">\r" +
    "\n" +
    "                    </bstypeahead> --> <select name=\"OpeNo\" ng-change=\"selectTypeOpeNo(lmModel.TaskCode,lmModel)\" class=\"form-control\" ng-disabled=\"lmModel.isOpe == 0\" ng-model=\"lmModel.TaskCode\" data=\"DataOpeNo\" placeholder=\"Pilih Tipe\"> <option ng-repeat=\"sat in DataOpeNo\" value=\"{{sat.TaskCode}}\">{{sat.TaskName}}</option> </select> </div> <bserrmsg field=\"noMaterial\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>FR</bsreqlabel>\r" +
    "\n" +
    "                <input type=\"number\" ng-if=\"isRelease == false\" required min=0 step=0.1 class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"FlatRateAvailable\">\r" +
    "\n" +
    "                <input type=\"number\" ng-if=\"isRelease == true\" required min=\"{{lmModel.defaultFR}}\" step=0.1 class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"FlatRateAvailable\">\r" +
    "\n" +
    "                <bserrmsg field=\"Fr\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\" show-errors> <div class=\"col-xs-6\" style=\"padding-left: 0px\"> <bsreqlabel>FR</bsreqlabel> <input type=\"number\" ng-if=\"isRelease == false\" required min=\"0\" step=\"0.0000000001\" class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"FlatRateAvailable\" ng-change=\"checkServiceRate(lmModel.FlatRate,lmModel.JobTypeId)\"> <input type=\"number\" ng-if=\"isRelease == true\" required min=\"{{lmModel.defaultFR}}\" step=\"0.0000000001\" class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"FlatRateAvailable\" ng-change=\"checkServiceRate(lmModel.FlatRate,lmModel.JobTypeId)\"> <em ng-messages=\"row2.Fr.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Angka harus lebih besar atau sama dengan 0 </p> </em> </div> <!-- <div class=\"form-group\" show-errors> --> <div class=\"col-xs-6\" style=\"padding-right: 0px\"> <bsreqlabel>Satuan</bsreqlabel> <bsselect name=\"satuan\" ng-disabled=\"isRelease\" ng-model=\"lmModel.ProcessId\" data=\"unitData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" required> </bsselect> <em ng-messages=\"row2.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> <!-- </div> --> <!-- <bserrmsg field=\"Fr\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <div ng-if=\"PriceAvailable == false && (isRelease == false || isRelease == true)\"> <!-- <div ng-if=\"PriceAvailable == false && (isRelease == false || isRelease == true) && (lmModel.priceAvailable == false || lmModel.priceAvailable == null)\"> --> <input type=\"number\" ng-maxlength=\"10\" required min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Fare\" ng-change=\"changeHarga(lmModel, mFilterTAM.isTAM)\" ng-disabled=\"PriceIsTam\" placeholder=\"\"> </div> <div ng-if=\"PriceAvailable == true && (isRelease == false || isRelease == true)\"> <!-- <div ng-if=\"PriceAvailable == true && (isRelease == false || isRelease == true) && (lmModel.priceAvailable == true)\"> --> <input type=\"number\" required ng-maxlength=\"10\" min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Summary\" ng-disabled=\"PriceAvailable\" placeholder=\"\"> </div> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row2.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Job Plan / SAR</bsreqlabel> <input type=\"number\" ng-disabled=\"isRelease\" step=\"0.25\" min=\"0.25\" class=\"form-control\" name=\"jobplan\" ng-model=\"lmModel.ActualRate\" required> <em ng-messages=\"row2.jobplan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Jobplan / SAR harus kelipatan 0.25 </p> </em> <!-- <bserrmsg field=\"jobplan\"></bserrmsg> --> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Pembayaran</bsreqlabel>\r" +
    "\n" +
    "                <bsselect name=\"pembayaran\" ng-disabled=\"!isBilling\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected)\" required>\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bsselect name=\"pembayaran\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected)\" required>\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bserrmsg field=\"pembayaran\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <bsselect name=\"pembayaran\" ng-disabled=\"JobIsWarranty == 1 || isDisablePaid\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected,lmModel.PaidById)\" required> </bsselect> <em ng-messages=\"row2.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>T1 <span ng-show=\"!disT1\" style=\"color:#b94a48\"> *</span> </label> <bsselect name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" item-text=\"xName\" item-value=\"xName\" ng-disabled=\"disT1\" ng-required=\"!disT1\" placeholder=\"T1\"> </bsselect> <!-- ng-required=\"!disT1\" --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <bsselect name=\"diskonTipe\" ng-model=\"lmModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypeDiskonTask(selected,lmModel)\" ng-disabled=\"lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" placeholder=\"Pilih Tipe Diskon\" required> </bsselect> <em ng-messages=\"row1.diskonTipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>List Diskon</bsreqlabel> <bsselect name=\"diskonList\" ng-model=\"lmModel.DiscountTypeListId\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10 || lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonTask(selected,lmModel.DiscountTypeId,lmModel)\" placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\"></bsselect> <em ng-messages=\"row1.diskonList.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-required=\"isCustomTask\" ng-disabled=\"(!isCustomTask) || lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"lmModel.Discount\"> <em ng-messages=\"row2.Discount.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- <input type=\"text\" ng-required=\"lmModel.isDeleted == 1\" class=\"form-control\" ng-hide=\"true\" name=\"Discount\" ng-model=\"lmModel.Tes\">         --> </div> <script type=\"text/ng-template\" id=\"customTemplateOpe.html\"><a>\r" +
    "\n" +
    "                <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                <span ng-bind-html=\"match.model.TaskName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "            </a></script> </div> <!-- ============= AFTER FI ============ --> <!-- {{isLock}} --> <div ng-if=\"isLock\"> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <bsselect name=\"tipe\" ng-model=\"lmModel.JobTypeId\" data=\"taskCategory\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"isLock\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeWork(selected)\"> </bsselect> <em ng-messages=\"row2.tipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pekerjaan</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"lmModel.TaskName\" get-data=\"getWork\" ng-disabled=\"isLock\" item-text=\"TaskName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWork\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" icon=\"fa-id-card-o\" required> </bstypeahead> <em ng-messages=\"row2.jobName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"jobName\"></bserrmsg> --> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label class=\"pull-left\">Additional OpeNo</label> <bscheckbox class=\"pull-right\" ng-disabled=\"isLock\" ng-model=\"lmModel.isOpe\" label=\"Ope No\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div class=\"col-md-12\" style=\"padding-left:0;padding-right:0;padding-bottom:15px\"> <bstypeahead placeholder=\"Task Code\" name=\"noMaterial\" ng-model=\"lmModel.TaskCode\" get-data=\"getTaskOpe\" item-text=\"TaskCode\" ng-maxlength=\"250\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectOpeNo\" on-noresult=\"onNoOpeResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\" ng-disabled=\"isLock\" template-url=\"customTemplateOpe.html\" icon=\"fa-id-card-o\"> </bstypeahead> </div> <bserrmsg field=\"noMaterial\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>FR</bsreqlabel>\r" +
    "\n" +
    "                <input type=\"number\" ng-if=\"isRelease == false\" required min=0 step=0.1 class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"FlatRateAvailable\">\r" +
    "\n" +
    "                <input type=\"number\" ng-if=\"isRelease == true\" required min=\"{{lmModel.defaultFR}}\" step=0.1 class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"FlatRateAvailable\">\r" +
    "\n" +
    "                <bserrmsg field=\"Fr\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\" show-errors> <div class=\"col-xs-6\" style=\"padding-left: 0px\"> <bsreqlabel>FR</bsreqlabel> <input type=\"number\" ng-if=\"isRelease == false\" required min=\"0\" step=\"0.0000000001\" class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"isLock\" ng-change=\"checkServiceRate(lmModel.FlatRate,lmModel.JobTypeId)\"> <input type=\"number\" ng-if=\"isRelease == true\" required min=\"{{lmModel.defaultFR}}\" step=\"0.0000000001\" class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"isLock\" ng-change=\"checkServiceRate(lmModel.FlatRate,lmModel.JobTypeId)\"> <em ng-messages=\"row2.Fr.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Angka harus lebih besar atau sama dengan 0 </p> </em> </div> <!-- <div class=\"form-group\" show-errors> --> <div class=\"col-xs-6\" style=\"padding-right: 0px\"> <bsreqlabel>Satuan</bsreqlabel> <bsselect name=\"satuan\" ng-disabled=\"isLock\" ng-model=\"lmModel.ProcessId\" data=\"unitData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" required> </bsselect> <em ng-messages=\"row2.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> <!-- </div> --> <!-- <bserrmsg field=\"Fr\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <div ng-if=\"PriceAvailable == false && (isRelease == false || isRelease == true)\"> <!-- <div ng-if=\"PriceAvailable == false && (isRelease == false || isRelease == true) && (lmModel.priceAvailable == false || lmModel.priceAvailable == null)\"> --> <input type=\"number\" ng-maxlength=\"10\" required min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Fare\" ng-disabled=\"isLock\" placeholder=\"\"> </div> <div ng-if=\"PriceAvailable == true && (isRelease == false || isRelease == true)\"> <!-- <div ng-if=\"PriceAvailable == true && (isRelease == false || isRelease == true) && (lmModel.priceAvailable == true)\"> --> <input type=\"number\" required ng-maxlength=\"10\" min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Summary\" ng-disabled=\"isLock\" placeholder=\"\"> </div> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row2.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Job Plan / SAR</bsreqlabel> <input type=\"number\" ng-disabled=\"isLock\" step=\"0.25\" min=\"0.25\" class=\"form-control\" name=\"jobplan\" ng-model=\"lmModel.ActualRate\" required> <em ng-messages=\"row2.jobplan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Jobplan / SAR harus kelipatan 0.25 </p> </em> <!-- <bserrmsg field=\"jobplan\"></bserrmsg> --> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Pembayaran</bsreqlabel>\r" +
    "\n" +
    "                <bsselect name=\"pembayaran\" ng-disabled=\"!isBilling\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected)\" required>\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bsselect name=\"pembayaran\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected)\" required>\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bserrmsg field=\"pembayaran\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- ng-disabled=\"isLock\" --> <bsselect name=\"pembayaran\" ng-disabled=\"JobIsWarranty == 1 || isDisablePaid\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected,lmModel.PaidById)\" required> </bsselect> <em ng-messages=\"row2.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>T1 <span ng-show=\"!disT1\" style=\"color:#b94a48\"> *</span> </label> <bsselect name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" item-text=\"xName\" item-value=\"xName\" ng-disabled=\"isLock\" ng-required=\"!disT1\" placeholder=\"T1\"> </bsselect> <!-- ng-required=\"!disT1\" --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- ng-disabled=\"isLock\" --> <bsselect name=\"diskonTipe\" ng-model=\"lmModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypeDiskonTask(selected,lmModel)\" ng-disabled=\"lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" placeholder=\"Pilih Tipe Diskon\" required> </bsselect> <em ng-messages=\"row1.diskonTipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>List Diskon</bsreqlabel> <!-- ng-disabled=\"isLock\" --> <bsselect name=\"diskonList\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" ng-model=\"lmModel.DiscountTypeListId\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonTask(selected,lmModel.DiscountTypeId,lmModel)\" placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\"> </bsselect> <em ng-messages=\"row1.diskonList.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-disabled=\"(!isCustomTask) || lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" ng-required=\"isCustomTask\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"lmModel.Discount\"> <em ng-messages=\"row2.Discount.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- <input type=\"text\" ng-required=\"lmModel.isDeleted == 1\" class=\"form-control\" ng-hide=\"true\" name=\"Discount\" ng-model=\"lmModel.Tes\">         --> </div> <script type=\"text/ng-template\" id=\"customTemplateOpe.html\"><a>\r" +
    "\n" +
    "                <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                <span ng-bind-html=\"match.model.TaskName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "            </a></script> </div> </div>"
  );


  $templateCache.put('app/services/reception/woHistoryGR/modalFormMasterOpl.html',
    "<div class=\"row\"> <div class=\"col-md-12\" ng-if=\"lmModelOpl.Status <= 1 || lmModelOpl.Status == undefined\"> <div class=\"form-group\"> <bsreqlabel>Nama OPL</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" ng-disabled=\"mData.Status > 3 && !isNew\" name=\"oplName\" ng-model=\"lmModelOpl.OPLName\" get-data=\"getOpl\" item-text=\"OPLWorkName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkOPL\" on-noresult=\"onNoResultOpl\" on-gotresult=\"onGotResult\" template-url=\"customTemplateOpl.html\" icon=\"fa-id-card-o\" required> </bstypeahead> <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModelOpl.OplName\"> --> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <!-- <bstypeahead placeholder=\"Nama Vendor\" name=\"vendorName\"\r" +
    "\n" +
    "                ng-model=\"lmModelOpl.VendorName\"\r" +
    "\n" +
    "                get-data=\"getOpl\"\r" +
    "\n" +
    "                model-key=\"Opl\"\r" +
    "\n" +
    "                ng-minlength=\"2\" ng-maxlength=\"10\" disallow-space\r" +
    "\n" +
    "                loading=\"loadingUsers\"\r" +
    "\n" +
    "                noresult=\"noResults\"\r" +
    "\n" +
    "                selected=\"selected\"\r" +
    "\n" +
    "                on-select=\"onSelectUser\"\r" +
    "\n" +
    "                on-noresult=\"onNoResult\"\r" +
    "\n" +
    "                on-gotresult=\"onGotResult\",\r" +
    "\n" +
    "                ta-minlength=\"1\"\r" +
    "\n" +
    "                icon=\"fa-id-card-o\"\r" +
    "\n" +
    "            >\r" +
    "\n" +
    "            </bstypeahead> --> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModelOpl.VendorName\" required> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <bsselect name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"OPLPaymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" required> </bsselect> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Estimasi Selesai</bsreqlabel> <bsdatepicker name=\"RequiredDate\" min-date=\"dateOptionsOPL.minDate\" date-options=\"dateOptionsOPL\" ng-model=\"lmModelOpl.TargetFinishDate\" ng-change=\"DateChange(dt)\" required> </bsdatepicker> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" required name=\"ReceiveBy\" ng-model=\"lmModelOpl.ReceiveBy\">\r" +
    "\n" +
    "            <bserrmsg field=\"ReceiveBy\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input min=\"0\" type=\"number\" ng-change=\"QtyOPL(lmModelOpl.QtyPekerjaan,lmModelOpl)\" class=\"form-control\" ng-disabled=\"false\" required name=\"qty\" ng-model=\"lmModelOpl.QtyPekerjaan\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Beli</bsreqlabel> <input min=\"0\" type=\"text\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.OPLPurchasePrice\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Jual</bsreqlabel> <input min=\"0\" type=\"text\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\"> <label>Notes</label> <textarea class=\"form-control\" name=\"notes\" ng-model=\"lmModelOpl.Notes\" style=\"height: 100px\"></textarea> </div> <div class=\"form-group\" show-errors> <label>Discount (%)</label> <input type=\"number\" min=\"0\" max=\"100\" class=\"form-control\" name=\"discount\" ng-model=\"lmModelOpl.Discount\" ng-disabled=\"DiscountAvailable\"> <bserrmsg field=\"discount\"></bserrmsg> </div> <div class=\"form-group\" ng-if=\"lmModel.OPLId !== undefined\"> <label>Status : {{lmModelOpl.xStatus}}</label> </div> </div> <div class=\"col-md-12\" ng-if=\"lmModelOpl.Status > 1\"> <div class=\"form-group\"> <bsreqlabel>Nama OPL</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" ng-disabled=\"true\" name=\"oplName\" ng-model=\"lmModelOpl.OPLName\" get-data=\"getOpl\" item-text=\"OPLWorkName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkOPL\" on-noresult=\"onNoResultOpl\" on-gotresult=\"onGotResult\" template-url=\"customTemplateOpl.html\" icon=\"fa-id-card-o\" required> </bstypeahead> <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModelOpl.OplName\"> --> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModelOpl.VendorName\"> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <bsselect name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"OPLPaymentData\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"true\" placeholder=\"Pilih Pembayaran\" required> </bsselect> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Estimasi Selesai</bsreqlabel> <bsdatepicker name=\"RequiredDate\" ng-disabled=\"true\" min-date=\"dateOptionsOPL.minDate\" date-options=\"dateOptionsOPL\" ng-model=\"lmModelOpl.TargetFinishDate\" ng-change=\"DateChange(dt)\"> </bsdatepicker> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" required name=\"ReceiveBy\" ng-model=\"lmModelOpl.ReceiveBy\">\r" +
    "\n" +
    "            <bserrmsg field=\"ReceiveBy\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input type=\"number\" ng-change=\"QtyOPL(lmModelOpl.QtyPekerjaan,lmModelOpl)\" class=\"form-control\" ng-disabled=\"true\" required name=\"qty\" ng-model=\"lmModelOpl.QtyPekerjaan\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <input type=\"text\" ng-disabled=\"true\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> <bserrmsg field=\"price\"></bserrmsg> </div> <div class=\"form-group\"> <label>Notes</label> <textarea ng-disabled=\"true\" class=\"form-control\" name=\"notes\" ng-model=\"lmModelOpl.Notes\" style=\"height: 100px\"></textarea> </div> <div class=\"form-group\" show-errors> <label>Discount (%)</label> <input type=\"number\" min=\"0\" max=\"100\" ng-disabled=\"true\" class=\"form-control\" name=\"discount\" ng-model=\"lmModelOpl.Discount\" ng-disabled=\"DiscountAvailable\"> <bserrmsg field=\"discount\"></bserrmsg> </div> <div class=\"form-group\"> <label>Status : {{lmModelOpl.xStatus}}</label> </div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpl.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.Vendor.Name | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.isExternalDesc | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );


  $templateCache.put('app/services/reception/woHistoryGR/woHistoryGR%20-%20Copy.html',
    "<style type=\"text/css\">th.active {\r" +
    "\n" +
    "        background-color: red;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .border-medium {\r" +
    "\n" +
    "        border-width: 2px;\r" +
    "\n" +
    "        border-style: solid;\r" +
    "\n" +
    "        border-radius: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .border-thin {\r" +
    "\n" +
    "        border-width: 1px;\r" +
    "\n" +
    "        border-style: solid;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-area-field-label {\r" +
    "\n" +
    "        font-size: 11px;\r" +
    "\n" +
    "        background-color: floralwhite;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        top: -20px;\r" +
    "\n" +
    "        left: -40px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-overlay {\r" +
    "\n" +
    "        /*background-color: #ccc;*/\r" +
    "\n" +
    "        background-color: #fff;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .blurred {\r" +
    "\n" +
    "        /*filter: url(\"/filters.svg#blur3px\");\r" +
    "\n" +
    "        -webkit-filter: blur(3px);\r" +
    "\n" +
    "        -moz-filter: blur(3px);\r" +
    "\n" +
    "        -o-filter: blur(3px);\r" +
    "\n" +
    "        filter: blur(3px);*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-outline {\r" +
    "\n" +
    "        /*background: url('../images/outline.gif');*/\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .choosen-area {\r" +
    "\n" +
    "        /*background-color: red;*/\r" +
    "\n" +
    "        border: 2px solid red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .unchoosen-area {\r" +
    "\n" +
    "        /*background-color: red;*/\r" +
    "\n" +
    "        border: 2px solid black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-resize-handler {\r" +
    "\n" +
    "        border: 2px #fff solid;\r" +
    "\n" +
    "        height: 10px;\r" +
    "\n" +
    "        width: 10px;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        background-color: #000;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-delete-area {\r" +
    "\n" +
    "        background: url('../images/bt-delete.png');\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        height: 20px;\r" +
    "\n" +
    "        width: 20px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .delete-area {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        padding: 1px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk {\r" +
    "\n" +
    "        border: 1px solid red;\r" +
    "\n" +
    "        padding: 10px 10px 40px 10px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        margin: 0 auto 0 auto;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature {\r" +
    "\n" +
    "        border: 1px solid orange;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature canvas {\r" +
    "\n" +
    "        border: 1px solid #999;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .buttons {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        bottom: 10px;\r" +
    "\n" +
    "        left: 10px;\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .buttonsamk {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        bottom: 2px;\r" +
    "\n" +
    "        left: 10px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature_container {\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .result {\r" +
    "\n" +
    "        border: 1px solid blue;\r" +
    "\n" +
    "        margin: 30px auto 0 auto;\r" +
    "\n" +
    "        height: 220px;\r" +
    "\n" +
    "        width: 568px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio] {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio]+label {\r" +
    "\n" +
    "        background-color: grey;\r" +
    "\n" +
    "        color: #fff;\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        -webkit-border-radius: 2px;\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "        font-weight: 400;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        width: 100px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio]:checked+label {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*====================*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-bar {\r" +
    "\n" +
    "        background: #ffe4d1;\r" +
    "\n" +
    "        height: 2px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-selection {\r" +
    "\n" +
    "        background: orange;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-pointer {\r" +
    "\n" +
    "        width: 8px;\r" +
    "\n" +
    "        height: 16px;\r" +
    "\n" +
    "        top: auto;\r" +
    "\n" +
    "        /* to remove the default positioning */\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "        /*background-color: #333;*/\r" +
    "\n" +
    "        background-color: red;\r" +
    "\n" +
    "        border-top-left-radius: 3px;\r" +
    "\n" +
    "        border-top-right-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-pointer:after {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-bubble {\r" +
    "\n" +
    "        bottom: 14px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-limit {\r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "        color: orange;\r" +
    "\n" +
    "        font-size: 20px;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-tick {\r" +
    "\n" +
    "        width: 1px;\r" +
    "\n" +
    "        height: 10px;\r" +
    "\n" +
    "        margin-left: 4px;\r" +
    "\n" +
    "        border-radius: 0;\r" +
    "\n" +
    "        background: #ffe4d1;\r" +
    "\n" +
    "        top: -1px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-tick.rz-selected {\r" +
    "\n" +
    "        background: orange;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .scaleWarp {\r" +
    "\n" +
    "        margin-bottom: -40px;\r" +
    "\n" +
    "        /*padding:16px;*/\r" +
    "\n" +
    "        padding-bottom: 0px;\r" +
    "\n" +
    "        padding-left: 1px;\r" +
    "\n" +
    "        letter-spacing: -10px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        /*float:left;*/\r" +
    "\n" +
    "        /*width:100%;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #slide {\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*.scaleWarp span+span{*/\r" +
    "\n" +
    "    /*margin-left:23%;*/\r" +
    "\n" +
    "    /*vertical-align:bottom;*/\r" +
    "\n" +
    "    /*bottom:0;*/\r" +
    "\n" +
    "    /*}*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .scaleWarp garis {\r" +
    "\n" +
    "        margin-left: 24.75%;\r" +
    "\n" +
    "        vertical-align: bottom;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "    }</style> <bsform-grid loading=\"loading\" ng-form=\"WoHistoryGRForm\" factory-name=\"WoHistoryGR\" model=\"mData\" model-id=\"JobId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-validate-save=\"onValidateSave\" on-before-cancel=\"onBeforeCancel\" show-advsearch=\"on\" form-name=\"WoHistoryGRForm\" form-title=\"WoHistoryGR\" form-api=\"formApi\" modal-title=\"WoHistoryGR\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" action-button-caption=\"actButtonCaption\" hide-new-button=\"true\" on-bulk-approve=\"bulkApprove\" on-bulk-reject=\"bulkReject\" on-bulk-print=\"bulkPrint\" on-show-detail=\"onShowDetail\" grid-custom-action-button-template=\"gridActionTemplate\" grid-hide-action-column=\"true\" grid-custom-action-button-colwidth=\"170\" form-api=\"formApi\" form-grid-api=\"formGridApi\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:50px\"> <div class=\"row\" ng-if=\"isKabeng\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>WO yang Ditampilkan</bsreqlabel> <!-- <select class=\"form-control\" ng-change=\"selectType()\"  placeholder=\"Select Car Name\" name=\"carName\" ng-model = \"mQSetting.CarName\" >\r" +
    "\n" +
    "                                <option value=\"Camry\">Camry</option>\r" +
    "\n" +
    "                                <option value=\"Avanza\">Avanza</option>\r" +
    "\n" +
    "                                <option value=\"Yaris\">Yaris</option>\r" +
    "\n" +
    "                            </select> --> <bsselect name=\"showDataKabeng\" data=\"option\" item-text=\"Name\" item-value=\"Id\" ng-model=\"filter.ShowDataKabeng\" placeholder=\"Select Filter Show Data\" ng-change=\"select(filter.ShowDataKabeng,1)\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"ShowDataKabeng\"></bserrmsg> </div> </div> </div> <div class=\"row\" ng-if=\"!isKabeng\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>WO yang Ditampilkan</bsreqlabel> <!-- <select class=\"form-control\" ng-change=\"selectType()\"  placeholder=\"Select Car Name\" name=\"carName\" ng-model = \"mQSetting.CarName\" >\r" +
    "\n" +
    "                                <option value=\"Camry\">Camry</option>\r" +
    "\n" +
    "                                <option value=\"Avanza\">Avanza</option>\r" +
    "\n" +
    "                                <option value=\"Yaris\">Yaris</option>\r" +
    "\n" +
    "                            </select> --> <bsselect name=\"ShowData\" data=\"option2\" item-text=\"Name\" item-value=\"Id\" ng-model=\"filter.ShowData\" placeholder=\"Select Filter Show Data\" ng-change=\"select(filter.ShowData,2)\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"ShowData\"></bserrmsg> </div> </div> </div> <div class=\"row\" ng-if=\"isKabeng && filter.ShowDataKabeng == 1 \"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsselect name=\"ParamList\" ng-model=\"Param\" data=\"dataParam\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectFilter(selected)\" placeholder=\"Pilih Metode Cari\" icon=\"fa fa-list\"> </bsselect> </div> </div> <div ng-if=\"Param == 3\" class=\"col-md-9\"> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" name=\"NoPolisi\" ng-model=\"filter.NoPolisi\" placeholder=\"No Polisi\" ng-keypress=\"allowPattern($event,2,filter.NoPolisi)\"> </div> </div> <div ng-if=\"Param == 2\" class=\"col-md-9\"> <div class=\"col-md-4\"> <!-- <input type=\"text\" class=\"form-control\" name=\"KodeWO\" ng-model=\"filter.KodeWO\" placeholder=\"No WO\" ng-keypress=\"allowPattern($event,2,filter.KodeWO)\"> --> <input type=\"text\" class=\"form-control\" name=\"KodeWO\" ng-model=\"filter.KodeWO\" placeholder=\"No WO\"> </div> </div> <div ng-if=\"Param == 1\" class=\"col-md-9\"> <div class=\"col-md-4\"> <bsdatepicker name=\"Date\" date-options=\"DateOptions\" ng-model=\"filter.DateFrom\"> </bsdatepicker> </div> <div class=\"col-md-1\"> <label for=\"strip\">-</label> </div> <div class=\"col-md-4\"> <bsdatepicker name=\"Date\" date-options=\"DateOptions\" ng-model=\"filter.DateTo\"> </bsdatepicker> </div> </div> </div> <div class=\"row\" ng-if=\"isKabeng && filter.ShowDataKabeng == 3 \"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Tipe Approval Diskon</bsreqlabel> <!-- <select class=\"form-control\" ng-change=\"selectType()\"  placeholder=\"Select Car Name\" name=\"carName\" ng-model = \"mQSetting.CarName\" >\r" +
    "\n" +
    "                                <option value=\"Camry\">Camry</option>\r" +
    "\n" +
    "                                <option value=\"Avanza\">Avanza</option>\r" +
    "\n" +
    "                                <option value=\"Yaris\">Yaris</option>\r" +
    "\n" +
    "                            </select> --> <bsselect name=\"tipeApprovalDiskon\" data=\"dataDiskon\" item-text=\"Name\" item-value=\"Id\" ng-model=\"filter.tipeApprovalDiskon\" placeholder=\"Select Filter Show Data\" on-select=\"selectDiskon(filter.tipeApprovalDiskon)\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"ShowData\"></bserrmsg> </div> </div> </div> <div class=\"row\" ng-if=\"(!isKabeng && filter.ShowData == 1) || (!isKabeng && filter.ShowData == 2)\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsselect name=\"ParamList\" ng-model=\"Param\" data=\"dataParam\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectFilter(selected)\" placeholder=\"Pilih Metode Cari\" icon=\"fa fa-list\"> </bsselect> </div> </div> <div ng-if=\"Param == 3\" class=\"col-md-9\"> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" name=\"NoPolisi\" ng-model=\"filter.NoPolisi\" placeholder=\"No Polisi\" ng-keypress=\"allowPattern($event,2,filter.NoPolisi)\"> </div> </div> <div ng-if=\"Param == 2\" class=\"col-md-9\"> <div class=\"col-md-4\"> <!-- <input type=\"text\" class=\"form-control\" name=\"KodeWO\" ng-model=\"filter.KodeWO\" placeholder=\"No WO\" ng-keypress=\"allowPattern($event,2,filter.KodeWO)\"> --> <input type=\"text\" class=\"form-control\" name=\"KodeWO\" ng-model=\"filter.KodeWO\" placeholder=\"No WO\"> </div> </div> <div ng-if=\"Param == 1\" class=\"col-md-9\"> <div class=\"col-md-4\"> <bsdatepicker name=\"Date\" date-options=\"DateOptions\" ng-model=\"filter.DateFrom\"> </bsdatepicker> </div> <div class=\"col-md-1\"> <label for=\"strip\">-</label> </div> <div class=\"col-md-4\"> <bsdatepicker name=\"Date\" date-options=\"DateOptions\" ng-model=\"filter.DateTo\"> </bsdatepicker> </div> </div> </div> </div> </div> </bsform-advsearch> <bsform-below-grid> <div class=\"row\" style=\"margin-top:30px\"> <div class=\"col-md-12\"> <div id=\"grid1\" ui-grid=\"gridFrontParts\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\"></div> </div> </div> </bsform-below-grid> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-xs-4\"> <bsreqlabel>Kategori WO</bsreqlabel> <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" ng-disabled=\"true\" skip-enable placeholder=\"WO Category\"> <!--  ng-change=\"PDS(mData.WoCategoryId)\" --> </bsselect> <bserrmsg field=\"categoryWO\"></bserrmsg> </div> <div class=\"col-xs-4\"> <button ng-disabled=\"statusWO\" ng-if=\"statusWO == true\" skip-enable class=\"rbtn btn\" style=\"margin-top:22px\" ng-click=\"closeWO(mData)\">Close WO</button> <button ng-if=\"statusWO == false\" class=\"rbtn btn\" style=\"margin-top:22px\" ng-click=\"closeWO(mData)\">Close WO</button> </div> </div> <!--     <div class=\"\">\r" +
    "\n" +
    "        <button class=\"rbtn btn pull-right\" style=\"margin-top:5px;\" ng-click=\"closeWO(mData)\">Close WO</button>\r" +
    "\n" +
    "    </div> --> <uib-accordion close-others=\"oneAtATime\"> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Short Brief </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Pemilik</label> <input ng-model=\"mDataCustomer.owner\" type=\"text\" skip-enable disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Pengguna</label> <input ng-model=\"mDataCustomer.user\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Alamat</label> <input ng-model=\"mDataCustomer.address\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>On Time/Late</label> <input ng-model=\"mDataCustomer.late\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <!--<div class=\"form-group\">\r" +
    "\n" +
    "                                <label>Waiting Time</label>\r" +
    "\n" +
    "                                <input ng-model=\"mDataCustomer.waitTime\" skip-enable type=\"text\" disabled class=\"form-control\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div>--> <div class=\"form-group\"> <label>Model</label> <input ng-model=\"mDataCustomer.model\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-12\"> <div class=\"well\"> <div class=\"form-group\"> <label>Job Suggest</label> <textarea class=\"form-control\" skip-enable ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\"></textarea> </div> </div> </div> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Field Action\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Operation No.</font> </td> <td> <font color=\"white\">Description</font> </td> </tr> <tr ng-repeat=\"a in GetDataTowass\"> <td>{{a.OpeNo}}</td> <td>{{a.Description}}</td> </tr> </table> </div> </uib-tab> <uib-tab index=\"1\" heading=\"PSFU\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table id=\"QA\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>No.</th> <th>Pertanyaan</th> <th>Jawaban Customer</th> <!-- <th>Alasan Customer</th> --> </tr> </thead> <tbody> <tr ng-repeat=\"item in Question track by $index\"> <td width=\"5%\">{{$index + 1}}</td> <td width=\"40%\">{{item.Description}}</td> <td width=\"25%\"> <select class=\"form-control\" name=\"yesOrNo\" ng-disabled=\"true\" skip-enable> <option ng-disabled=\"true\" skip-enable ng-repeat=\"itemAnswer in item.Answer track by $index\" value=\"{{itemAnswer.AnswerId}}\">{{itemAnswer.Description}}</option> </select> </td> </tr> </tbody> </table> </div> </uib-tab> <uib-tab index=\"2\" heading=\"CS Survey\"> </uib-tab> </uib-tabset> </div> </div> </div> </div> </div> <div uib-accordion-group is-open=\"true\" class=\"panel-default\"> <uib-accordion-heading> Plotting Stall </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-9\"> </div> <div class=\"col-md-3\"> <div class=\"form-group form-inline\" ng-hide=\"true\"> <bsreqlabel>Estimasi Durasi</bsreqlabel> <input type=\"number\" min=\"0.1\" max=\"9\" step=\"0.1\" class=\"form-control\" style=\"width:100px\" name=\"EstimateHour\" ng-disabled=\"true\" skip-enable ng-change=\"estimationDuration(mData.EstimateHour)\" ng-model=\"mData.EstimateHour\"> <label>Jam</label> </div> <div class=\"form-group\"> <bsdatepicker name=\"startDate\" disabled skip-enable ng-disabled=\"true\" date-options=\"dateOptions\" ng-model=\"asb.startDate\" ng-change=\"reloadBoard(asb.startDate)\" min-date=\"minDateASB\" skip-enable></bsdatepicker> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"plottingStall\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> </div> </div> </div> </div> </div> <!--  --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Informasi Umum </uib-accordion-heading> <div id=\"CustIndex\" class=\"row\"> <div smart-include=\"app/services/reception/woHistoryGR/customerIndex.html\"> </div> </div> </div> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                Kontak Person & Pengambil Keputusan\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "            <div class=\"col-md-12\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                <div id=\"CPPKInfo\" class=\"row\">\r" +
    "\n" +
    "                    <div smart-include=\"app/services/reception/includeForm/CPPKInfo.html\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Riwayat Servis </uib-accordion-heading> <vehicle-history ng-disabled=\"true\" skip-enable vin=\"mDataCrm.Vehicle.VIN\"> </vehicle-history> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Order Pekerjaan </uib-accordion-heading> <div class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\"> <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Permintaan\" list-title=\"Permintaan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Job Request\" name=\"reqdesc\" ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "                                            </textarea> <bserrmsg field=\"reqdesc\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\" style=\"text-align:right\"> <div class=\"btn-group\" style=\"margin-top:25px\"> <button type=\"button\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\"> Pre Diagnose </button> </div> </div> <div class=\"col-md-12\"> <bslist data=\"JobComplaint\" m-id=\"ComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Keluhan\" list-title=\"Keluhan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Complaint Category\" ng-disabled=\"true\" skip-enable> </bsselect> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Complaint Text\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                        </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\"> <bsreqlabel>Tipe Keluhan</bsreqlabel> <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Complaint Category\"> </bsselect> <bserrmsg field=\"ctype\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\">\r" +
    "\n" +
    "                                                        </textarea> <bserrmsg field=\"ctext\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nomor WO Estimasi</label> <input type=\"text\" min=\"0\" class=\"form-control\" name=\"NomorWo\" ng-model=\"mData.NomorWoEstimasi\"> <bserrmsg field=\"NomorWo\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Kategori WO</bsreqlabel> <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\"> <!--  ng-change=\"PDS(mData.WoCategoryId)\" --> </bsselect> <bserrmsg field=\"categoryWO\"></bserrmsg> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"JobTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" on-before-edit=\"onBeforeEdit\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApiJob\" confirm-save=\"false\" list-title=\"Job List\" button-settings=\"ButtonList\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\">{{totalWork | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalWork - totalWorkDiscounted | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalWorkDiscounted | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\">{{totalMaterial | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalMaterialDiscounted | currency:\"Rp. \":0}}</td> </tr> <!-- <tr>\r" +
    "\n" +
    "                                <td>Estimasi OPL</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                <td></td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr> --> <tr> <td>PPN</td> <td style=\"text-align: right\"> <!-- {{((totalWork + totalMaterial) * 10)/100 | currency:\"Rp. \":0}} --> </td> <td></td> <td style=\"text-align: right\"> {{(totalMaterial + totalWork) + (((totalWork + totalMaterial) * 10)/100) | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"> <!-- {{totalWork + totalMaterial | currency:\"Rp. \":0}}</td> --> </td><td></td> <!-- <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                            {{(totalMaterial - totalMaterialDiscounted)+(totalWork - totalWorkDiscounted) | currency:\"Rp. \":0}}\r" +
    "\n" +
    "                                        </td> --> <td style=\"text-align: right\">{{(totalMaterial + totalWork) + (((totalWork + totalMaterial) * 10)/100) | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\">{{totalDp | currency:\"Rp. \":0}}</td> <td></td> <td></td> </tr> </tbody> </table> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> OPL </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <bslist data=\"oplData\" m-id=\"OPLId\" list-model=\"lmModelOpl\" on-before-new=\"onBeforeNewOPL\" on-before-save=\"onBeforeSaveOpl\" on-before-edit=\"onBeforeEditOPL\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <strong>{{lmItem.OPLName}}</strong> </div> <div class=\"form-group\"> {{lmItem.VendorName}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Estimasi : </label> {{lmItem.TargetFinishDate | date:'dd-MM-yyyy'}} </div> <div class=\"form-group\"> <label>Quantity : </label> {{lmItem.QtyPekerjaan}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>NO. OPL : </label> {{lmItem.OplNo}} </div> <div class=\"form-group\"> <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}} </div> <div class=\"form-group\"> <label>Status : {{lmItem.xStatus}}</label> </div> </div> </div> </bslist-template> <bslist-modal-form> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMasterOpl.html'\"></div> </bslist-modal-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <tbody> <tr> <td width=\"20%\">Total</td> <td>{{sumWorkOpl | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" ng-click=\"dataForWAC()\" is-open=\"accordionWac.isOpen\"> <uib-accordion-heading> WAC <!-- <span ng-click=\"dataForWAC()\"></span> --> </uib-accordion-heading> <!--<div ng-include=\"'app/services/reception/wo/includeForm/wac.html'\">\r" +
    "\n" +
    "                 </div> --> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">INTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Km</bsreqlabel> <input type=\"text\" step=\"1\" min=\"0\" class=\"form-control\" name=\"Km\" placeholder=\"Km\" ng-model=\"mData.Km\" ng-maxlength=\"7\" maxlength=\"7\" ng-keyup=\"givePattern(mData.Km,$event)\"> <bserrmsg field=\"km\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <label for=\"slider\">Fuel</label>\r" +
    "\n" +
    "                        <rzslider rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider>\r" +
    "\n" +
    "                    </div> --> <div id=\"slide\" class=\"col-md-6\"> <label for=\"slider\">Fuel</label> <div class=\"scaleWarp\"> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> <garis>|</garis> <garis style=\"font-size: 20px;font-weight:bold\">|</garis> <garis>|</garis> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> </div> <rzslider class=\"custom-slider\" rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.BanSerep\" label=\"Ban Serep\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.CDorKaset\" label=\"CD/Kaset\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Payung\" label=\"Payung\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Dongkrak\" label=\"Dongkrak\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.STNK\" label=\"STNK\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.P3k\" label=\"P3K\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.KunciSteer\" label=\"Kunci Steer\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ToolSet\" label=\"Tool Set\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ClipKarpet\" label=\"Clip Karpet\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.BukuService\" label=\"Buku Service\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Alarm Condition</bsreqlabel> <bsselect name=\"alarmCondition\" ng-model=\"mData.AlarmCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition Alarm\"> </bsselect> <bserrmsg field=\"alarmCondition\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>AC Condition</bsreqlabel> <bsselect name=\"AcCondition\" ng-model=\"mData.AcCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition AC\"> </bsselect> <bserrmsg field=\"AcCondition\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Power Window</bsreqlabel> <bsselect name=\"PowerWindow\" ng-model=\"mData.PowerWindow\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition Power Window\"> </bsselect> <bserrmsg field=\"PowerWindow\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>Jenis Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                            <bsselect name=\"CarType\" ng-model=\"mData.CarType\" data=\"carTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Jenis Kendaraan\">\r" +
    "\n" +
    "                            </bsselect>\r" +
    "\n" +
    "                            <bserrmsg field=\"CarType\"></bserrmsg>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div> --> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Other</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Other\" name=\"Other\" ng-model=\"mData.OtherDescription\">\r" +
    "\n" +
    "                            </textarea> <bserrmsg field=\"Other\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <bsreqlabel>Money Amount</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Money Amount\" name=\"moneyAmount\" ng-model=\"mData.moneyAmount\" ng-keyup=\"givePatternMoney(mData.moneyAmount,$event)\">\r" +
    "\n" +
    "                        </textarea> <bserrmsg field=\"moneyAmount\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">EXTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Type Kendaraan</label> <bsselect name=\"filterType\" ng-model=\"filter.vType\" data=\"VTypeData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Type Kendaraan\" on-select=\"chooseVType(selected)\" icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <!-- <div class=\"row\" ng-repeat=\"imageWAC in imageWACArr\">\r" +
    "\n" +
    "                                    <img ng-if=\"imageSel==imageWAC.imgId\" ng-areas=\"areasArray\" \r" +
    "\n" +
    "                                        ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\"\r" +
    "\n" +
    "                                        ng-src=\"{{imageWAC.img}}\" style=\"width:700px;height:400px\"> --> <!-- <img ng-if=\"imageSel==imageWAC.imgId\" ng-src=\"{{imageWAC.img}}\" style=\"width:700px;height:400px\"> --> <!-- </div> --> <!-- <button ng-click=\"tes()\">tes</button> --> <div class=\"row\" ng-if=\"showPicWac\"> <div class=\"col-md-12\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"750\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> <!-- <pre>{{logDots}}</pre>  --> </div> </div> <div class=\"row\" ng-show=\"!disableWACExt\"> <div class=\"col-md-12\"> <div ui-grid=\"gridWac\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize ui-grid-edit class=\"grid\" ng-style=\"getTableHeight()\"> <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </div> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsreqlabel>Jenis Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                        <bsselect name=\"CarType\" ng-model=\"mData.CarType\" data=\"carTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Jenis Kendaraan\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                        <bserrmsg field=\"CarType\"></bserrmsg>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-12\" style=\"text-align:center;\">\r" +
    "\n" +
    "                    <pitch-canvas data='drawData' status=\"paramClosePopover\"></pitch-canvas>\r" +
    "\n" +
    "                    <div id='myPopover' ng-show='showPopup' ng-style=\"popoverStyle\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <button ng-click=\"closePopover(1)\">Baret</button>\r" +
    "\n" +
    "                            <button ng-click=\"closePopover(2)\">Penyok</button>\r" +
    "\n" +
    "                            <button ng-click=\"closePopover(3)\">Rusak</button>\r" +
    "\n" +
    "                            <button ng-click=\"closePopover(0)\">Delete</button>\r" +
    "\n" +
    "                            <p><span>Y : {{coorY}} - X : {{coorX}}</span></p>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div> --> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <!-- <button class=\"btn rbtn\" ng-click=\"disableExt\">Clear</button> --> <!-- <button type=\"button\" class=\"btn rbtn\" ng-model=\"disableWACExt\" uib-btn-checkbox btn-checkbox-true=\"1\" btn-checkbox-false=\"0\">Clear</button> --> <!-- <button class=\"btn rbtn\" ng-disabled=\"disableBPCenter\" skip-enable ng-click=\"\">BP Center</button> --> <!-- added by sss on 2017-09-07 --> <bscheckbox ng-model=\"disableWACExt\" label=\"Clear\" true-value=\"true\" false-value=\"false\" ng-change=\"chgChk()\"> </bscheckbox> <!-- <div class=\"row\">\r" +
    "\n" +
    "                            <bsdatepicker \r" +
    "\n" +
    "                                    name=\"bpCenterDate\" \r" +
    "\n" +
    "                                    date-options=\"DateOptions\" \r" +
    "\n" +
    "                                    ng-model=\"bpCenterDate\" \r" +
    "\n" +
    "                                    ng-change=\"setCenterDate()\">\r" +
    "\n" +
    "                            </bsdatepicker>\r" +
    "\n" +
    "                        </div> --> <div class=\"row\" style=\"margin-top:10px\"> <button class=\"btn rbtn\" ng-disabled=\"disableBPCenter\" skip-enable ng-click=\"triggerBPC()\"> BP Center </button> </div> <!--  --> </div> <div class=\"col-md-8\"> <div class=\"containeramk\" ng-style=\"{'max-width': boundingBox.width + 'px', 'max-height': boundingBox.height + 'px'}\"> <signature-pad accept=\"accept\" clear=\"clear\" height=\"220\" width=\"568\" dataurl=\"dataurl\"></signature-pad> <div class=\"buttonsamk\"> <button class=\"btn wbtn\" ng-disabled=\"disableReset\" skip-enable ng-click=\"clear()\">Clear</button> <!-- <button class=\"btn rbtn\" ng-click=\"dataurl = signature.dataUrl\" ng-disabled=\"!signature\">Reset</button> --> <button class=\"btn rbtn\" ng-click=\"signature = accept(); open(signature)\">Sign</button> </div> </div> </div> <div class=\"col-md-2\"> <!-- <input type=\"button\" class=\"btn rbtn\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" isImage=\"1\" img-upload=\"uploadFiles\"><i class=\"fa fa-fw fa-lg fa-car\" style=\"font-size:30px; padding:8px 8px 8px 0px;margin-left:8px;margin-right:8px;\"></i>\r" +
    "\n" +
    "                        <input type=\"file\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" isImage=\"1\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1;\"> --> <!-- <label class=\"input-group-btn\">\r" +
    "\n" +
    "                            <span class=\"btn rbtn\" style=\"padding:12px 10px 12px 10px;\">\r" +
    "\n" +
    "                                <i class=\"fa fa-fw fa-lg fa-car\" style=\"font-size:55px;\"></i>\r" +
    "\n" +
    "                                <input type=\"file\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" img-upload=\"fileKK\" style=\"display: none;\" uploadFiles=\"uploadWACImage\" multiple>\r" +
    "\n" +
    "                            </span>\r" +
    "\n" +
    "                        </label> --> <image-control max-upload=\"4\" is-image=\"1\" img-upload=\"uploadFiles\" multiple> <form-image-control> <input capture=\"camera\" type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"file\" ng-init=\"mData.UploadPhoto = uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> <!-- <button ng-click=\"TestingImage(mData)\">Testing Image</button> --> <!-- <div class=\"custom-file-upload\">\r" +
    "\n" +
    "                            <input type=\"file\" id=\"file\" name=\"myfiles[]\" bs-upload=\"onFileSelect($files)\" img-upload=\"fileKK\" file-model=\"mData.Attachment\" upload />\r" +
    "\n" +
    "                        </div>  --> </div> </div> </div> </div> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                Permintaan\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "            <div class=\"row\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Job Request\" list-title=\"Job Request\"\r" +
    "\n" +
    "                        list-height=\"200\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    </textarea>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div>\r" +
    "\n" +
    "                                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                    <bsreqlabel>Job Request</bsreqlabel>\r" +
    "\n" +
    "                                    <textarea class=\"form-control\" placeholder=\"Job Request\" name='reqdesc' ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "                            </textarea>\r" +
    "\n" +
    "                                    <bserrmsg field=\"reqdesc\"></bserrmsg>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                Keluhan\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"row\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                <div class=\"col-md-12\" style=\"text-align:right;\">\r" +
    "\n" +
    "                    <div class=\"btn-group\" style=\"margin-top:25px;\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\">\r" +
    "\n" +
    "                      Pre Diagnose\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist data=\"JobComplaint\" m-id=\"ComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Complaint\"\r" +
    "\n" +
    "                        list-title=\"Complaint\" list-height=\"200\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Complaint Type: </bsreqlabel>\r" +
    "\n" +
    "                                    <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Complaint Category\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    </bsselect>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <textarea class=\"form-control\" placeholder=\"Complaint Text\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                </textarea>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Complaint Type</bsreqlabel>\r" +
    "\n" +
    "                                    <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Complaint Category\">\r" +
    "\n" +
    "                                    </bsselect>\r" +
    "\n" +
    "                                    <bserrmsg field=\"ctype\"></bserrmsg>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Job Complaint</bsreqlabel>\r" +
    "\n" +
    "                                    <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\">\r" +
    "\n" +
    "                                                </textarea>\r" +
    "\n" +
    "                                    <bserrmsg field=\"ctext\"></bserrmsg>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Job Suggest </uib-accordion-heading> <div class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\" style=\"margin-top: 10px\"> <div class=\"row\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editSuggest()\" ng-hide=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"SimpanJobSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" style=\"margin-top: 25px\"> <div class=\"col-md-12\"> <textarea ng-readonly=\"disSgs\" ng-model=\"mData.Suggestion\" maxlength=\"200\" style=\"width: 100%;height: 100px\"></textarea> </div> </div> </div> </div> </div> <!--         <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                Job List\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-6\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <label>Nomor WO Estimasi</label>\r" +
    "\n" +
    "                        <input type=\"text\" min=0 class=\"form-control\" name=\"NomorWo\" ng-model=\"mData.NomorWoEstimasi\">\r" +
    "\n" +
    "                        <bserrmsg field=\"NomorWo\"></bserrmsg>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-6\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsreqlabel>Kategori WO</bsreqlabel>\r" +
    "\n" +
    "                        <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                        <bserrmsg field=\"categoryWO\"></bserrmsg>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist data=\"gridWork\" m-id=\"JobTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" on-before-edit=\"onBeforeEdit\" list-model=\"lmModel\"\r" +
    "\n" +
    "                        list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api='listApiJob' confirm-save=\"false\" list-title=\"Job List\" button-settings=\"ButtonList\" modal-title=\"Job Data\"\r" +
    "\n" +
    "                        modal-title-detail=\"Parts Data\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div class=\"col-md-6\" style=\"text-align:left;\">\r" +
    "\n" +
    "                                <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <p>Tipe :{{lmItem.catName}}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <p>Flat Rate: {{lmItem.FlatRate}}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-6\" style=\"text-align:right;\">\r" +
    "\n" +
    "                                <p>Pembayaran: {{lmItem.PaidBy}}</p>\r" +
    "\n" +
    "                                <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-detail-template>\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <p><strong>{{ ldItem.PartsCode }}</strong></p>\r" +
    "\n" +
    "                                <p>{{ ldItem.PartsName }}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <p>{{ ldItem.Availbility }}</p>\r" +
    "\n" +
    "                                <p>Pembayaran : {{ ldItem.paidName}}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p>\r" +
    "\n" +
    "                                <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp.\"}}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p>\r" +
    "\n" +
    "                                <p>Reserve : {{ ldItem.Qty }}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-detail-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\">\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                        <bslist-modal-detail-form>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\">\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-detail-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                    <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "                        <thead>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <th>Item</th>\r" +
    "\n" +
    "                                <th>Harga</th>\r" +
    "\n" +
    "                                <th>Disc</th>\r" +
    "\n" +
    "                                <th>Sub Total</th>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </thead>\r" +
    "\n" +
    "                        <tbody>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Estimasi Service / Jasa</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalWork | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalWork - totalWorkDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalWorkDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Estimasi Material (Parts / Bahan)</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalMaterial | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalMaterialDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Estimasi OPL</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOpl - sumWorkOplDiscounted | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOplDiscounted | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>PPN</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                </td>\r" +
    "\n" +
    "                                <td></td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                    {{(((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Total Estimasi</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                </td>\r" +
    "\n" +
    "                                <td></td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{(totalMaterialDiscounted + totalWorkDiscounted) + (((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Total DP</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\"></td>\r" +
    "\n" +
    "                                <td></td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalDp | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </tbody>\r" +
    "\n" +
    "                    </table>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <!--         <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                OPL\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist data=\"oplData\" m-id=\"oplId\" list-model=\"lmModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApi\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\"\r" +
    "\n" +
    "                        list-title=\"Order Pekerjaan Luar\" list-height=\"200\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        {{lmItem.OplName}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        {{lmItem.VendorName}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Dibutuhkan : </label> {{lmItem.RequiredDate | date:'dd-MM-yyyy'}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Harga : </label> {{lmItem.Price | currency:\"Rp.\"}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"col-md-12\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <bsreqlabel>Nama OPL</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModel.OplName\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"oplName\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <bsreqlabel>Vendor</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"text\" class=\"form-control\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModel.VendorName\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"oplName\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                        <bsreqlabel>Pembayaran</bsreqlabel>\r" +
    "\n" +
    "                                        <bsselect name=\"pembayaran\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected)\">\r" +
    "\n" +
    "                                        </bsselect>\r" +
    "\n" +
    "                                        <bserrmsg field=\"pembayaran\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <bsreqlabel>Dibutuhkan Tanggal</bsreqlabel>\r" +
    "\n" +
    "                                        <bsdatepicker name=\"RequiredDate\" date-options=\"DateOptions\" ng-model=\"lmModel.RequiredDate\" ng-change=\"DateChange(dt)\">\r" +
    "\n" +
    "                                        </bsdatepicker>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                        <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"text\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.ReceiveBy\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"price\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                        <bsreqlabel>Harga</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Price\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"price\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Notes</label>\r" +
    "\n" +
    "                                        <textarea class=\"form-control\" name=\"Address\" ng-model=\"lmModel.Address\" style=\"height: 100px;\"></textarea>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                        <label>Discount</label>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"discount\" ng-model=\"lmModel.Discount\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"discount\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                    <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "                        <tbody>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td width=\"20%\">Total</td>\r" +
    "\n" +
    "                                <td>{{sumWork | currency:\"Rp.\"}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </tbody>\r" +
    "\n" +
    "                    </table>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <!--  --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Summary </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label>No. Polisi</label> </div> <div class=\"form-group\"> <label>Nomor WO</label> </div> <div class=\"form-group\"> <label>Kategori WO</label> </div> </div> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label>{{mDataCrm.Vehicle.LicensePlate}}</label> </div> <div class=\"form-group\"> <label ng-if=\"mData.WoNo != Null\">{{mData.WoNo}}</label> <label ng-if=\"mData.WoNo == Null\">-</label> </div> <div class=\"form-group\"> <bsselect ng-disabled=\"true\" data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\"> </bsselect> </div> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"TaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-after-cancel=\"onAfterCancel\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridSumPartsDetail\" list-api=\"listApi\" confirm-save=\"false\" list-title=\"Job List\" modal-title=\"Job Data\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.Part.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.paidName}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp.\"}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\">{{totalWork | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalWork - totalWorkDiscounted | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalWorkDiscounted | currency:\"Rp.\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\">{{totalMaterial | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalMaterialDiscounted | currency:\"Rp.\":0}}</td> </tr> <tr> <td>Estimasi OPL</td> <td style=\"text-align: right\">{{sumWorkOpl | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{sumWorkOplDiscounted | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{sumWorkOpl - sumWorkOplDiscounted | currency:\"Rp. \":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"> <!-- {{((totalWork + totalMaterial) * 10)/100 | currency:\"Rp. \":0}} --> </td> <td></td> <td style=\"text-align: right\"> {{(((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"> <!-- {{totalWork + totalMaterial | currency:\"Rp. \":0}} --> </td> <td></td> <!-- <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                    {{(totalMaterial - totalMaterialDiscounted)+(totalWork - totalWorkDiscounted) | currency:\"Rp.\":0}}\r" +
    "\n" +
    "                                </td> --> <td style=\"text-align: right\">{{(totalMaterialDiscounted + totalWorkDiscounted) + (((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\"></td> <td></td> <td style=\"text-align: right\">{{totalDp | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Lama Pekerjaan : </label> <input type=\"text\" ng-model=\"mData.ActualRate\" class=\"form-control\" placeholder=\"0\" name=\"\" ng-disabled=\"true\" skip-enable> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Part Bekas</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radio1\" ng-model=\"mData.UsedPart\" name=\"radio1\" value=\"1\" checked> <label for=\"radio1\">Kembalikan</label> <input type=\"radio\" id=\"radio2\" ng-model=\"mData.UsedPart\" name=\"radio1\" value=\"0\"> <label for=\"radio2\">Tinggal</label> <!-- {{mData.UsedPart}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Waiting / Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"wait1\" ng-model=\"mData.IsWaiting\" name=\"radio2\" value=\"1\" checked> <label for=\"wait1\">Waiting</label> <input type=\"radio\" id=\"wait2\" ng-model=\"mData.IsWaiting\" name=\"radio2\" value=\"0\"> <label for=\"wait2\">Drop Off</label> <!-- {{mData.IsWaiting}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Car Wash</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"washing1\" ng-model=\"mData.IsWash\" name=\"radio3\" value=\"1\" checked> <label for=\"washing1\">Wash</label> <input type=\"radio\" id=\"washing2\" ng-model=\"mData.IsWash\" name=\"radio3\" value=\"0\"> <label for=\"washing2\">No Wash</label> <!-- {{mData.IsWash}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Penggantian Parts</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changePart1\" ng-model=\"mData.PermissionPartChange\" name=\"radio4\" value=\"1\" checked> <label for=\"changePart1\">Langsung</label> <input type=\"radio\" id=\"changePart2\" ng-model=\"mData.PermissionPartChange\" name=\"radio4\" value=\"0\"> <label for=\"changePart2\">Izin</label> <!-- {{mData.PermissionPartChange}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Metode Pembayaran</label> </div> </div> <div class=\"col-md-6\"> <bsselect name=\"paymentType\" ng-model=\"mData.PaymentType\" data=\"paymentTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Type Pembayaran\"> </bsselect> <bserrmsg field=\"paymentType\"></bserrmsg> </div> </div> <div class=\"row\" style=\"margin-top:25px; margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Scheduled Service Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.firstDate\" ng-disabled=\"true\"> </bsdatepicker> </div> <div class=\"form-group text-center\" style=\"width:20px\"> <b>-</b> </div> <div class=\"form-group\"> <bsdatepicker name=\"lastDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.lastDate\" ng-disabled=\"true\"> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Estimation Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"estDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.EstimateDate\" ng-disabled=\"true\" skip-enable> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Adjusment Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"adjDate\" min-date=\"minDate\" date-options=\"DateOptions\" ng-model=\"mData.AdjusmentDate\" ng-disabled=\"adjustDate\" ng-change=\"adjustTime(mData.AdjusmentDate)\" skip-enable> </bsdatepicker> </div> <div class=\"form-group\" style=\"width:200px\"> <div uib-timepicker ng-model=\"mData.AdjusmentDate\" ng-change=\"changed()\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" ng-disabled=\"adjustDate\"></div> </div> </div> <div class=\"col-md-3 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bscheckbox ng-model=\"mData.customerRequest\" label=\"Customer Request\" ng-click=\"adjRequest()\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <!-- <div class=\"col-md-2 form-inline\">\r" +
    "\n" +
    "                   \r" +
    "\n" +
    "                </div> --> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Kode Transaksi</label> </div> <div class=\"col-md-9 form-inline\"> <bsselect ng-disabled=\"false\" data=\"codeTransaction\" name=\"codeTrans\" item-value=\"Id\" item-text=\"Description\" ng-model=\"mData.CodeTransaction\" placeholder=\"Kode Transaksi\"> </bsselect> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" name=\"saveWO\" class=\"rbtn btn\" ng-click=\"saveNewWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                            Simpan\r" +
    "\n" +
    "                        </button>\r" +
    "\n" +
    "                        <button type=\"button\" name=\"releaseWO\" class=\"rbtn btn\" ng-click=\"releaseWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                            Release WO\r" +
    "\n" +
    "                        </button> --> <!-- ng-show=\"mData.JobId >= 0\" --> <button type=\"button\" name=\"CloseWO\" ng-click=\"cancelWo(mData)\" class=\"rbtn btn\"> Batal WO </button> </div> </div> </div> <!--             <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"btn-group pull-right\">\r" +
    "\n" +
    "                    <button type=\"button\" name=\"saveWO\" class=\"rbtn btn\" ng-click=\"saveNewWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                        Simpan\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                    <button type=\"button\" name=\"printWO\" class=\"rbtn btn\">\r" +
    "\n" +
    "                        Print Preview\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                    <button type=\"button\" name=\"releaseWO\" class=\"rbtn btn\" ng-click=\"releaseWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                        Release WO\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> </div> </uib-accordion> </bsform-grid> <bsui-modal show=\"show_modal.show\" title=\"PDS\" mode=\"modalMode\" button-settings=\"buttonEmail\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 25px\" ng-repeat=\"a in dataPKSFinal\"> <label>Balances : {{a.Balances | currency:\"Rp.\"}}</label><br> <label>Start Periode : {{a.StartPeriod |date:dd/mm/yyyy}}</label><br> <label>End Periode : {{a.EndPeriod |date:dd/mm/yyyy}}</label> </div> </div> </bsui-modal> <!-- approval --> <bsui-modal show=\"show_modal.showApprovalPrint\" title=\"Alasan Di Tolak\" data=\"mPrintServiceHistory\" on-save=\"onReject\" button-settings=\"buttonSettingModalApprovalReject\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"contactForm\"> <div class=\"row\"> <textarea name=\"reason\" ng-model=\"mPrintServiceHistory.RejectReason\" class=\"form-control\"></textarea> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalPrintApprove\" title=\"Alasan Di Terima\" data=\"mPrintServiceHistory\" on-save=\"onApprove\" button-settings=\"buttonSettingModalApprovalApprove\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"contactForm\"> <div class=\"row\"> <textarea name=\"reason\" ng-model=\"mPrintServiceHistory.RejectReason\" class=\"form-control\"></textarea> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalDp\" title=\"Alasan Approval DP\" data=\"modal_model\" on-save=\"doSaveApproval\" on-cancel=\"doBack\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"ApprovalForm\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <h4>{{modal_model.RequestReason}} Dari {{modal_model.DPDefault | currency:\"Rp.\"}} ~ {{modal_model.DPRequested | currency:\"Rp.\"}}</h4> <textarea style=\"height:104px\" ng-required=\"isReject\" class=\"form-control\" placeholder=\"Reason\" ng-model=\"modal_model.Reason\"></textarea> </div> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalJobReduction\" title=\"Alasan Approve/Reject Pengurangan Pekerjaan\" data=\"modal_model\" on-save=\"doSaveApprovalJobReduction\" on-cancel=\"doBack\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"ApprovalForm\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <!-- <h4>{{modal_model.RequestReason}} Dari {{modal_model.DPDefault | currency:\"Rp.\"}} ~ {{modal_model.DPRequested | currency:\"Rp.\"}}</h4> --> <textarea style=\"height:104px\" ng-required=\"isReject\" class=\"form-control\" placeholder=\"Reason\" ng-model=\"modal_model.Reason\"></textarea> </div> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalDiskon\" title=\"Alasan Approval Diskon\" data=\"modal_model\" on-save=\"doSaveApprovalDiskon\" on-cancel=\"doBack\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"ApprovalForm\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <h4>{{modal_model.RequestReason}} <br> Harga Normal : {{modal_model.NormalPrice | currency:\"Rp.\"}} dan Diskon : {{modal_model.Discount}}%</h4> <textarea style=\"height:104px\" ng-required=\"isReject\" class=\"form-control\" placeholder=\"Reason\" ng-model=\"modal_model.Reason\"></textarea> </div> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalDiskonReject\" title=\"Alasan Reject Diskon\" data=\"modal_model\" on-save=\"doSaveApprovalDiskon\" on-cancel=\"doBack\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"ApprovalForm\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <h4>{{modal_model.RequestReason}} <br> Harga Normal : {{modal_model.NormalPrice | currency:\"Rp.\"}} dan Diskon : {{modal_model.Discount}}%</h4> <textarea style=\"height:104px\" ng-required=\"isReject\" class=\"form-control\" placeholder=\"Reason\" ng-model=\"modal_model.Reason\"></textarea> </div> </div> </div> </bsui-modal> <bsui-modal show=\"showPrediagnose\" title=\"PreDiagnose Form\" data=\"dataPrediagnose\" on-save=\"savePreDiagnose\" on-cancel=\"cancelPreDiagnose\" button-settings=\"listEstimationButton\" mode=\"modalModepreDiagnose\"> <div ng-form name=\"preDiagnoseForm\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <div ng-include=\"'app/services/preDiagnose/preDiagnose_CustomerComplaint.html'\"></div> --> <div class=\"form-group\"> <div class=\"col-md-12\" ng-hide=\"true\"> <div class=\"col-xs-12\"> <label>Job Id</label> </div> <div class=\"col-xs-4\"> <input type=\"text\" name=\"JobId\" ng-model=\"dataPrediagnose.JobId\" class=\"form-control\"> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-12\" ng-hide=\"true\"> <div class=\"col-xs-12\"> <label>Job Id</label> </div> <div class=\"col-xs-4\"> <input type=\"text\" name=\"PrediagnoseId\" ng-model=\"dataPrediagnose.PrediagnoseId\" class=\"form-control\"> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-12\"> <div class=\"col-xs-12\" style=\"padding-left: 0px\"> <label>Gejala Terdeteksi Pertama Kali</label> </div> </div> <div class=\"col-xs-4\"> <bsdatepicker name=\"FirstIndication\" date-options=\"DateOptions\" ng-model=\"dataPrediagnose.FirstIndication\"> </bsdatepicker> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Frekuensi Kejadian</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"1\"> <span>Sekali</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"2\"> <span>Kadang - Kadang</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"3\"> <span>Selalu</span> </label> </div> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>MIL saat terjadi</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"1\"> <span>On</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"2\"> <span>Berkedip</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"3\"> <span>Off</span> </label> </div> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Kondisi Kendaraan Saat Gejala Terjadi</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"1\"> <span>Idling</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"2\"> <span>Starting</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"3\"> <span>Kecepatan Konstan</span> </label> </div> </div> <div class=\"col-xs-6\" ng-show=\"dataPrediagnose.ConditionIndication == 3\" style=\"padding-left: 0px\"> <!-- <input type=\"number\" name=\"Kecepatan\" style=\"width: 100%\" placeholder=\"Kecepatan\" ng-model=\"mData.Speed\"> --> <!--  ng-keypress=\"allowPattern($event,1)\" --> <textarea name=\"Kecepatan\" ng-model=\"dataPrediagnose.notes\" maxlength=\"100\" style=\"width: 100%;height: 50px\"></textarea> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Kondisi Mesin</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"1\"> <span>Panas</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"2\"> <span>Dingin</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"3\"> <span>Lainnya</span> </label> </div> </div> <div class=\"col-xs-6\" ng-show=\"dataPrediagnose.MachineCondition == 3\" style=\"padding-left: 0px\"> <textarea name=\"lainnya\" ng-model=\"dataPrediagnose.MachineConditionTxt\" style=\"width: 100%;height: 70px\"></textarea> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Posisi Shift Lever</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftP\" label=\"P\" ng-click=\"CountShiftPrediagnose()\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftN\" label=\"N\" ng-click=\"CountShiftPrediagnose()\" true-value=\"2\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftD\" label=\"D\" ng-click=\"CountShiftPrediagnose()\" true-value=\"4\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftS\" label=\"S\" ng-click=\"CountShiftPrediagnose()\" true-value=\"8\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftR\" label=\"R\" ng-click=\"CountShiftPrediagnose()\" true-value=\"16\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftOne\" label=\"1\" ng-click=\"CountShiftPrediagnose()\" true-value=\"32\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftTwo\" label=\"2\" ng-click=\"CountShiftPrediagnose()\" true-value=\"64\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftThree\" label=\"3\" ng-click=\"CountShiftPrediagnose()\" true-value=\"128\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftFour\" label=\"4\" ng-click=\"CountShiftPrediagnose()\" true-value=\"256\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftLainnya\" ng-click=\"CountShiftPrediagnose()\" label=\"Lainnya\" true-value=\"512\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-12\" ng-hide=\"true\"> <input type=\"text\" name=\"PositionShiftLever\" ng-model=\"dataPrediagnose.PositionShiftLever\"> </div> </div> <div class=\"col-xs-12\" ng-show=\"dataPrediagnose.PosisiShiftLainnya == 512\"> <textarea name=\"PositionShiftLeverTxt\" ng-model=\"dataPrediagnose.PositionShiftLeverTxt\" style=\"width: 100%;height: 70px\"></textarea> </div> </div> <div class=\"col-xs-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"dataPrediagnose.IsNeedTest\" ng-change=\"testDrive(mData)\" label=\"Perlu Test Drive\" true-value=\"1\" false-value=\"0\" ng-hide=\"true\"> </bscheckbox> </div> </div> </div> </div> </bsui-modal>"
  );


  $templateCache.put('app/services/reception/woHistoryGR/woHistoryGR.html',
    "<style type=\"text/css\">body {\r" +
    "\n" +
    "        overscroll-behavior-y: contain !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .uib-time input {\r" +
    "\n" +
    "        width: 100% !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .ui-grid-viewport {\r" +
    "\n" +
    "        overflow-anchor: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    th.active {\r" +
    "\n" +
    "        background-color: red;\r" +
    "\n" +
    "        color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /* .border-medium {\r" +
    "\n" +
    "        border-width: 2px;\r" +
    "\n" +
    "        border-style: solid;\r" +
    "\n" +
    "        border-radius: 5px;\r" +
    "\n" +
    "    } */\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .whiteList {\r" +
    "\n" +
    "        background-color: white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .redList {\r" +
    "\n" +
    "        background-color: red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .border-medium {\r" +
    "\n" +
    "        border-width: 2px;\r" +
    "\n" +
    "        /*border-style: solid;*/\r" +
    "\n" +
    "        border-radius: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .border-thin {\r" +
    "\n" +
    "        border-width: 1px;\r" +
    "\n" +
    "        border-style: solid;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-area-field-label {\r" +
    "\n" +
    "        font-size: 11px;\r" +
    "\n" +
    "        background-color: floralwhite;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        top: -20px;\r" +
    "\n" +
    "        left: -40px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-overlay {\r" +
    "\n" +
    "        /*background-color: #ccc;*/\r" +
    "\n" +
    "        background-color: #fff;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .blurred {\r" +
    "\n" +
    "        /*filter: url(\"/filters.svg#blur3px\");\r" +
    "\n" +
    "        -webkit-filter: blur(3px);\r" +
    "\n" +
    "        -moz-filter: blur(3px);\r" +
    "\n" +
    "        -o-filter: blur(3px);\r" +
    "\n" +
    "        filter: blur(3px);*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-outline {\r" +
    "\n" +
    "        /*background: url('../images/outline.gif');*/\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .choosen-area {\r" +
    "\n" +
    "        /*background-color: red;*/\r" +
    "\n" +
    "        border: 2px solid red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /* .unchoosen-area {\r" +
    "\n" +
    "        background-color: red;\r" +
    "\n" +
    "        border: 2px solid black;\r" +
    "\n" +
    "    } */\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-resize-handler {\r" +
    "\n" +
    "        border: 2px #fff solid;\r" +
    "\n" +
    "        height: 10px;\r" +
    "\n" +
    "        width: 10px;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        background-color: #000;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .select-areas-delete-area {\r" +
    "\n" +
    "        background: url('../images/bt-delete.png');\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        height: 20px;\r" +
    "\n" +
    "        width: 20px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .delete-area {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        padding: 1px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk {\r" +
    "\n" +
    "        /* border: 1px solid red;\r" +
    "\n" +
    "            padding: 10px 10px 40px 10px;\r" +
    "\n" +
    "            position: relative;\r" +
    "\n" +
    "            margin: 0 auto 0 auto;\r" +
    "\n" +
    "            height: 100%;*/\r" +
    "\n" +
    "        border: 1px solid #8a8a8a;\r" +
    "\n" +
    "        /*padding: 20px 10px 10px 10px;*/\r" +
    "\n" +
    "        padding: 0px 0px 0px 0px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        margin: 0 auto 0 auto;\r" +
    "\n" +
    "        /*height: 100%;*/\r" +
    "\n" +
    "        background-color: #f5f5f5\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature {\r" +
    "\n" +
    "        /*border: 1px solid orange;*/\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature canvas {\r" +
    "\n" +
    "        /* border: 1px solid #999;\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;*/\r" +
    "\n" +
    "        /*border: 1px solid #999;*/\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        background-color: #fff;\r" +
    "\n" +
    "        box-shadow: inset 0px 0px 12px 0px #c5c5c5;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .buttons {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        bottom: 10px;\r" +
    "\n" +
    "        left: 10px;\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .buttonsamk {\r" +
    "\n" +
    "        margin-top: 10px;\r" +
    "\n" +
    "        /*        position: absolute;\r" +
    "\n" +
    "            bottom: 2px;\r" +
    "\n" +
    "            left: 10px;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containeramk .signature_container {\r" +
    "\n" +
    "        visibility: hidden;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .result {\r" +
    "\n" +
    "        border: 1px solid blue;\r" +
    "\n" +
    "        margin: 30px auto 0 auto;\r" +
    "\n" +
    "        height: 220px;\r" +
    "\n" +
    "        width: 568px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio] {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio]+label {\r" +
    "\n" +
    "        background-color: grey;\r" +
    "\n" +
    "        color: #fff;\r" +
    "\n" +
    "        padding: 10px;\r" +
    "\n" +
    "        -webkit-border-radius: 2px;\r" +
    "\n" +
    "        display: inline-block;\r" +
    "\n" +
    "        font-weight: 400;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "        vertical-align: middle;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        width: 100px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .radioCustomer input[type=radio]:checked+label {\r" +
    "\n" +
    "        background-color: #d53337;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*====================*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-bar {\r" +
    "\n" +
    "        background: #ffe4d1;\r" +
    "\n" +
    "        height: 2px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-selection {\r" +
    "\n" +
    "        background: orange;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-pointer {\r" +
    "\n" +
    "        width: 8px;\r" +
    "\n" +
    "        height: 16px;\r" +
    "\n" +
    "        top: auto;\r" +
    "\n" +
    "        /* to remove the default positioning */\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "        /*background-color: #333;*/\r" +
    "\n" +
    "        background-color: red;\r" +
    "\n" +
    "        border-top-left-radius: 3px;\r" +
    "\n" +
    "        border-top-right-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-pointer:after {\r" +
    "\n" +
    "        display: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-bubble {\r" +
    "\n" +
    "        bottom: 14px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-limit {\r" +
    "\n" +
    "        font-weight: bold;\r" +
    "\n" +
    "        color: orange;\r" +
    "\n" +
    "        font-size: 20px;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-tick {\r" +
    "\n" +
    "        width: 1px;\r" +
    "\n" +
    "        height: 10px;\r" +
    "\n" +
    "        margin-left: 4px;\r" +
    "\n" +
    "        border-radius: 0;\r" +
    "\n" +
    "        background: #ffe4d1;\r" +
    "\n" +
    "        top: -1px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .custom-slider.rzslider .rz-tick.rz-selected {\r" +
    "\n" +
    "        background: orange;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .scaleWarp {\r" +
    "\n" +
    "        margin-bottom: -40px;\r" +
    "\n" +
    "        /*padding:16px;*/\r" +
    "\n" +
    "        padding-bottom: 0px;\r" +
    "\n" +
    "        padding-left: 1px;\r" +
    "\n" +
    "        letter-spacing: -10px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        /*float:left;*/\r" +
    "\n" +
    "        /*width:100%;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    #slide {\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    /*.scaleWarp span+span{*/\r" +
    "\n" +
    "    /*margin-left:23%;*/\r" +
    "\n" +
    "    /*vertical-align:bottom;*/\r" +
    "\n" +
    "    /*bottom:0;*/\r" +
    "\n" +
    "    /*}*/\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .scaleWarp garis {\r" +
    "\n" +
    "        margin-left: 24.75%;\r" +
    "\n" +
    "        vertical-align: bottom;\r" +
    "\n" +
    "        bottom: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .panel-default>.panel-heading {\r" +
    "\n" +
    "        color: #fff !important;\r" +
    "\n" +
    "        background-color: #d53337 !important;\r" +
    "\n" +
    "        border-color: #d80e0e !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .panel-default>.panel-heading>.panel-title {\r" +
    "\n" +
    "        /* font-size: 26px !important; */\r" +
    "\n" +
    "        /* font-weight: bold; */\r" +
    "\n" +
    "    }</style> <bsform-grid loading=\"loading\" ng-form=\"WoHistoryGRForm\" factory-name=\"WoHistoryGR\" model=\"mData\" model-id=\"JobId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-validate-save=\"onValidateSave\" on-before-cancel=\"onBeforeCancel\" show-advsearch=\"on\" form-name=\"WoHistoryGRForm\" form-title=\"WoHistoryGR\" form-api=\"formApi\" modal-title=\"WoHistoryGR\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" action-button-caption=\"actButtonCaption\" hide-new-button=\"true\" on-bulk-approve=\"bulkApprove\" on-bulk-reject=\"bulkReject\" on-bulk-print=\"bulkPrint\" on-show-detail=\"onShowDetail\" grid-custom-action-button-template=\"gridActionTemplate\" grid-hide-action-column=\"true\" grid-custom-action-button-colwidth=\"170\" form-api=\"formApi\" form-grid-api=\"formGridApi\" rights-byte=\"rightEnableByte\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:50px\"> <div class=\"row\" ng-if=\"isKabeng\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>WO yang Ditampilkan</bsreqlabel> <!-- <select class=\"form-control\" ng-change=\"selectType()\"  placeholder=\"Select Car Name\" name=\"carName\" ng-model = \"mQSetting.CarName\" >\r" +
    "\n" +
    "                                <option value=\"Camry\">Camry</option>\r" +
    "\n" +
    "                                <option value=\"Avanza\">Avanza</option>\r" +
    "\n" +
    "                                <option value=\"Yaris\">Yaris</option>\r" +
    "\n" +
    "                            </select> --> <bsselect name=\"showDataKabeng\" data=\"option\" item-text=\"Name\" item-value=\"Id\" ng-model=\"filter.ShowDataKabeng\" placeholder=\"Select Filter Show Data\" ng-change=\"select(filter.ShowDataKabeng,1)\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"ShowDataKabeng\"></bserrmsg> </div> </div> </div> <div class=\"row\" ng-if=\"!isKabeng\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>WO yang Ditampilkan</bsreqlabel> <!-- <select class=\"form-control\" ng-change=\"selectType()\"  placeholder=\"Select Car Name\" name=\"carName\" ng-model = \"mQSetting.CarName\" >\r" +
    "\n" +
    "                                <option value=\"Camry\">Camry</option>\r" +
    "\n" +
    "                                <option value=\"Avanza\">Avanza</option>\r" +
    "\n" +
    "                                <option value=\"Yaris\">Yaris</option>\r" +
    "\n" +
    "                            </select> --> <!-- bsselect dibawah ini di comment karena pengen performa di tablet cepet, pdhl tablet nya aja yg jlk --> <!-- <bsselect name=\"ShowData\" data=\"option2\" item-text=\"Name\" item-value=\"Id\" ng-model=\"filter.ShowData\" placeholder=\"Select Filter Show Data\" ng-change=select(filter.ShowData,2) icon=\"fa fa-child glyph-left\" style=\"min-width: 150px;\">\r" +
    "\n" +
    "                            </bsselect>\r" +
    "\n" +
    "                            <bserrmsg field=\"ShowData\"></bserrmsg> --> <select class=\"form-control\" convert-to-number ng-model=\"filter.ShowData\" ng-change=\"select(filter.ShowData,2)\"> <option value=\"1\" selected>Data Default SA</option> <option value=\"2\">Data Semua SA</option> </select> </div> </div> </div> <div class=\"row\" ng-if=\"isKabeng && filter.ShowDataKabeng == 1 \"> <div class=\"col-md-3\"> <div class=\"form-group\"> <!-- bsselect dibawah ini di comment karena pengen performa di tablet cepet, pdhl tablet nya aja yg jlk --> <!-- <bsselect name=\"ParamList\" ng-model=\"Param\" data=\"dataParam\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectFilter(selected)\" placeholder=\"Pilih Metode Cari\" icon=\"fa fa-list\">\r" +
    "\n" +
    "                            </bsselect> --> <select class=\"form-control\" convert-to-number ng-model=\"Param\" ng-change=\"selectFilter(Param)\"> <option value=\"3\">No. Polisi</option> <option value=\"2\">No. WO</option> <option value=\"1\" selected>Tanggal</option> </select> </div> </div> <div ng-if=\"Param == 3\" class=\"col-md-9\"> <div class=\"col-md-4\"> <!-- <input type=\"text\" class=\"form-control\" name=\"NoPolisi\" ng-model=\"filter.NoPolisi\" placeholder=\"No Polisi\" ng-keypress=\"allowPattern($event,2,filter.NoPolisi)\"> --> <input type=\"text\" class=\"form-control\" name=\"NoPolisi\" id=\"filterNoPolisi\" placeholder=\"No Polisi\" ng-keypress=\"allowPattern($event,2,filterNoPolisi)\" style=\"text-transform: uppercase\"> </div> </div> <div ng-if=\"Param == 2\" class=\"col-md-9\"> <div class=\"col-md-4\"> <!-- <input type=\"text\" class=\"form-control\" name=\"KodeWO\" ng-model=\"filter.KodeWO\" placeholder=\"No WO\" ng-keypress=\"allowPattern($event,2,filter.KodeWO)\"> --> <!-- <input type=\"text\" class=\"form-control\" name=\"KodeWO\" ng-model=\"filter.KodeWO\" placeholder=\"No WO\"> --> <input type=\"text\" class=\"form-control\" style=\"text-transform: uppercase\" name=\"KodeWO\" id=\"filterKodeWO\" placeholder=\"No WO\"> </div> </div> <div ng-if=\"Param == 1\" class=\"col-md-9\"> <div class=\"col-md-4\"> <bsdatepicker name=\"Date\" date-options=\"DateOptions\" ng-model=\"filter.DateFrom\"> </bsdatepicker> </div> <div class=\"col-md-1\"> <label for=\"strip\">-</label> </div> <div class=\"col-md-4\"> <bsdatepicker name=\"Date\" date-options=\"DateOptions\" ng-model=\"filter.DateTo\"> </bsdatepicker> </div> </div> </div> <div class=\"row\" ng-if=\"isKabeng && filter.ShowDataKabeng == 3 \"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Tipe Approval Diskon</bsreqlabel> <!-- <select class=\"form-control\" ng-change=\"selectType()\"  placeholder=\"Select Car Name\" name=\"carName\" ng-model = \"mQSetting.CarName\" >\r" +
    "\n" +
    "                                <option value=\"Camry\">Camry</option>\r" +
    "\n" +
    "                                <option value=\"Avanza\">Avanza</option>\r" +
    "\n" +
    "                                <option value=\"Yaris\">Yaris</option>\r" +
    "\n" +
    "                            </select> --> <bsselect name=\"tipeApprovalDiskon\" data=\"dataDiskon\" item-text=\"Name\" item-value=\"Id\" ng-model=\"filter.tipeApprovalDiskon\" placeholder=\"Select Filter Show Data\" on-select=\"selectDiskon(filter.tipeApprovalDiskon)\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"ShowData\"></bserrmsg> </div> </div> </div> <div class=\"row\" ng-if=\"(!isKabeng && filter.ShowData == 1) || (!isKabeng && filter.ShowData == 2)\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <!-- bsselect dibawah ini di comment karena pengen performa di tablet cepet, pdhl tablet nya aja yg jlk --> <!-- <bsselect name=\"ParamList\" ng-model=\"Param\" data=\"dataParam\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectFilter(selected)\" placeholder=\"Pilih Metode Cari\" icon=\"fa fa-list\">\r" +
    "\n" +
    "                            </bsselect> --> <select class=\"form-control\" convert-to-number ng-model=\"Param\" ng-change=\"selectFilter(Param)\"> <option value=\"3\">No. Polisi</option> <option value=\"2\">No. WO</option> <option value=\"1\" selected>Tanggal</option> </select> </div> </div> <div ng-if=\"Param == 3\" class=\"col-md-9\"> <div class=\"col-md-4\"> <!-- <input type=\"text\" class=\"form-control\" name=\"NoPolisi\" ng-model=\"filter.NoPolisi\" placeholder=\"No Polisi\" ng-keypress=\"allowPattern($event,2,filter.NoPolisi)\"> --> <input type=\"text\" class=\"form-control\" name=\"NoPolisi\" id=\"filterNoPolisi\" placeholder=\"No Polisi\" ng-keypress=\"allowPattern($event,2,filterNoPolisi)\" style=\"text-transform: uppercase\"> </div> </div> <div ng-if=\"Param == 2\" class=\"col-md-9\"> <div class=\"col-md-4\"> <!-- <input type=\"text\" class=\"form-control\" name=\"KodeWO\" ng-model=\"filter.KodeWO\" placeholder=\"No WO\" ng-keypress=\"allowPattern($event,2,filter.KodeWO)\"> --> <!-- <input type=\"text\" class=\"form-control\" name=\"KodeWO\" ng-model=\"filter.KodeWO\" placeholder=\"No WO\"> --> <input type=\"text\" class=\"form-control\" style=\"text-transform: uppercase\" name=\"KodeWO\" id=\"filterKodeWO\" placeholder=\"No WO\"> </div> </div> <div ng-if=\"Param == 1\" class=\"col-md-9\"> <div class=\"col-md-4\"> <bsdatepicker name=\"Date\" date-options=\"DateOptions\" ng-model=\"filter.DateFrom\"> </bsdatepicker> </div> <div class=\"col-md-1\"> <label for=\"strip\">-</label> </div> <div class=\"col-md-4\"> <bsdatepicker name=\"Date\" date-options=\"DateOptions\" ng-model=\"filter.DateTo\"> </bsdatepicker> </div> </div> </div> </div> </div> </bsform-advsearch> <bsform-below-grid> <div class=\"row\" style=\"margin-top:30px\"> <div class=\"col-md-12\"> <div id=\"grid1\" ui-grid=\"gridFrontParts\" ng-if=\"filter.ShowDataKabeng != 3\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\"></div> </div> </div> </bsform-below-grid> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-xs-4\"> <bsreqlabel>Kategori WO</bsreqlabel> <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" ng-disabled=\"true\" skip-enable placeholder=\"WO Category\"> <!--  ng-change=\"PDS(mData.WoCategoryId)\" --> </bsselect> <bserrmsg field=\"categoryWO\"></bserrmsg> </div> <div class=\"col-xs-4\"> <button ng-disabled=\"statusWO\" ng-if=\"statusWO == true\" skip-enable class=\"rbtn btn\" style=\"margin-top:22px\" ng-click=\"closeWO(mData)\">Close WO</button> <button ng-if=\"statusWO == false\" class=\"rbtn btn\" style=\"margin-top:22px\" ng-click=\"closeWO(mData)\">Close WO</button> </div> </div> <!--     <div class=\"\">\r" +
    "\n" +
    "        <button class=\"rbtn btn pull-right\" style=\"margin-top:5px;\" ng-click=\"closeWO(mData)\">Close WO</button>\r" +
    "\n" +
    "    </div> --> <uib-accordion close-others=\"oneAtATime\"> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Short Brief </div> </uib-accordion-heading> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Pemilik</label> <input ng-model=\"mDataCustomer.owner\" type=\"text\" skip-enable disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Pengguna</label> <input ng-model=\"mDataCustomer.user\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Alamat</label> <input ng-model=\"mDataCustomer.address\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>On Time/Late</label> <input ng-model=\"mDataCustomer.late\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <!--<div class=\"form-group\">\r" +
    "\n" +
    "                                <label>Waiting Time</label>\r" +
    "\n" +
    "                                <input ng-model=\"mDataCustomer.waitTime\" skip-enable type=\"text\" disabled class=\"form-control\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div>--> <div class=\"form-group\"> <label>Model</label> <input ng-model=\"mDataCustomer.model\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-12\"> <div class=\"well\"> <div class=\"form-group\"> <label>Job Suggest</label> <textarea class=\"form-control\" skip-enable ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\"></textarea> </div> </div> </div> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Field Action\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Operation No.</font> </td> <td> <font color=\"white\">Description</font> </td> </tr> <tr ng-repeat=\"a in GetDataTowass\"> <td>{{a.OpeNo}}</td> <td>{{a.Description}}</td> </tr> </table> </div> </uib-tab> <uib-tab index=\"1\" heading=\"PSFU\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table id=\"QA\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>No.</th> <th>Pertanyaan</th> <th>Jawaban Customer</th> <!-- <th>Alasan Customer</th> --> </tr> </thead> <tbody> <tr ng-repeat=\"item in Question track by $index\"> <td width=\"5%\">{{$index + 1}}</td> <td width=\"40%\">{{item.Description}}</td> <td width=\"25%\"> <select class=\"form-control\" name=\"yesOrNo\" ng-disabled=\"true\" skip-enable> <option ng-disabled=\"true\" skip-enable ng-repeat=\"itemAnswer in item.Answer track by $index\" value=\"{{itemAnswer.AnswerId}}\">{{itemAnswer.Description}}</option> </select> </td> </tr> </tbody> </table> </div> </uib-tab> <uib-tab index=\"2\" heading=\"CS Survey\"> </uib-tab> </uib-tabset> </div> </div> </div> </div> </div> <div uib-accordion-group is-open=\"true\" class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\" ng-click=\"loadPlotting()\"> Plotting Stall </div> </uib-accordion-heading> <div class=\"row\" id=\"accPlottingStall\"> <div class=\"col-md-12\"> <div class=\"col-md-9\"> </div> <div class=\"col-md-3\"> <div class=\"form-group form-inline\" ng-hide=\"true\"> <bsreqlabel>Estimasi Durasi</bsreqlabel> <input type=\"number\" min=\"0.1\" max=\"9\" step=\"0.1\" class=\"form-control\" style=\"width:100px\" name=\"EstimateHour\" ng-disabled=\"true\" skip-enable ng-change=\"estimationDuration(mData.EstimateHour)\" ng-model=\"mData.EstimateHour\"> <label>Jam</label> </div> <div class=\"form-group\"> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"asb.startDate\" ng-change=\"reloadBoard(asb.startDate)\" min-date=\"minDateASB\"></bsdatepicker> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"plottingStall\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> </div> </div> </div> </div> </div> <!--  --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Informasi Umum </div> </uib-accordion-heading> <div id=\"CustIndex\"> <!-- <div smart-include=\"app/services/reception/woHistoryGR/customerIndex.html\">\r" +
    "\n" +
    "                </div> --> <div class=\"col-md-12\"> <div class=\"row\"> <uib-accordion close-others=\"oneAtATime\"> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Pelanggan & Kendaraan </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/customer-01.html'\"></div> </div> <!-- di komen dl penanggung jawab nya.. kata pa ari double sama yg info pelanggan di atas --> <!-- <div uib-accordion-group class=\"panel-default\" ng-show=\"showInst\">\r" +
    "\n" +
    "                                <uib-accordion-heading>\r" +
    "\n" +
    "                                    <div ng-click=\"includetheHtml(4)\" style=\"font-size: 26px; height:30px;font-weight: bold;\">\r" +
    "\n" +
    "                                        Penanggung Jawab\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </uib-accordion-heading>\r" +
    "\n" +
    "                                <div ng-include=\"'app/services/reception/wo/includeForm/customer-02.html'\" ng-if=\"includehtml4\"></div>\r" +
    "\n" +
    "                            </div> --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Pengguna </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/customer-03.html'\"></div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Kontak Person & Pengambil Keputusan </div> </uib-accordion-heading> <div ng-include=\"'app/services/reception/wo/includeForm/CPPKInfo.html'\"></div> </div> </uib-accordion> </div> </div> </div> </div> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                Kontak Person & Pengambil Keputusan\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "            <div class=\"col-md-12\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                <div id=\"CPPKInfo\" class=\"row\">\r" +
    "\n" +
    "                    <div smart-include=\"app/services/reception/includeForm/CPPKInfo.html\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"includetheHtml(9, mDataCrm.Vehicle.VIN)\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Riwayat Servis </div> </uib-accordion-heading> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-9\"> <div class=\"col-md-12\"> <label class=\"control-label\">Riwayat Servis</label> </div> <div class=\"col-md-3\" style=\"padding-left: 0px\"> <bsdatepicker name=\"Tanggal1\" date-options=\"DateOptionsHistory\" ng-model=\"MRS.startDate\"> </bsdatepicker> </div> <div class=\"col-md-1\" style=\"margin-top: 5px;padding-left: 0px\"> <label>S/D</label> </div> <div class=\"col-md-3\" style=\"padding-left: 0px\"> <bsdatepicker name=\"Tanggal2\" date-options=\"DateOptionsHistory\" ng-model=\"MRS.endDate\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <bsreqlabel class=\"control-label\">Kategori : </bsreqlabel> <!-- ng-change=\"change()\" --> <select ng-model=\"MRS.category\" class=\"form-control\" convert-to-number> <option value=\"2\">ALL</option> <option value=\"1\">GR</option> <option value=\"0\">BP</option> </select> </div> <div class=\"col-md-12\" style=\"margin-top: 10px;text-align: right\"> <button class=\"rbtn btn\" ng-click=\"change(MRS.startDate,MRS.endDate,MRS.category,mDataCrm.Vehicle.VIN)\">Cari</button> </div> </div> <uib-accordion> <div ng-repeat=\"item in mDataVehicleHistory\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <table style=\"width:100%\"> <tr> <td>{{item.VehicleJobService[0].JobDescription}}</td> <th class=\"text-right\">{{item.ServiceNumber}} | {{item.ServiceDate |date:'dd-MMM-yyyy'}}</th> <!-- <th colspan=\"2\">{{item.ServiceNumber}}</th> --> </tr> <tr> <td>Lokasi services : {{item.ServiceLocation}}</td> <td class=\"text-right\">SA : {{item.ServiceAdvisor}}</td> </tr> <tr> <td>KM : {{item.Km}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Teknisi : {{item.VehicleJobService[0].EmployeeName}} </td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Group : {{item.namaGroup}}</td> <!-- <td class=\"text-right\">Tanggal Servis : {{item.ServiceDate |date:dd/mm/yyyy}}</td> --> </tr> <tr> <td>Job Suggest : {{item.JobSuggest}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Kategori : GR</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Kategori : BP</td> </tr> </table> </uib-accordion-heading> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"itemJob in item.VehicleJobService\" is-open=\"status.open\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> {{itemJob.JobDescription}} <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> </uib-accordion-heading> <table class=\"table table-striped\"> <tr> <th>No. Material</th> <th>Material</th> <!-- <th>No. Material</th> --> <th>Qty</th> <th>Satuan</th> </tr> <tr ng-repeat=\"tr in itemJob.VehicleServiceHistoryDetail\"> <td>{{tr.MaterialCode}}</td> <td>{{tr.Material}}</td> <!-- <td>{{tr.PartsCode}}</td> --> <td>{{tr.Quantity | number:2}}</td> <td>{{tr.Satuan}}</td> </tr> </table> </div> </uib-accordion> </div> </uib-accordion> <div class=\"row\"> <div class=\"col-md-12\"> <bscheckbox class=\"pull-right\" ng-model=\"mRiwayatService.isNeedPrint\" label=\"Print Riwayat Service\" ng-change=\"NeedPrint(mRiwayatService.isNeedPrint)\" ng-show=\"mDataVehicleHistory.length > 0 && cekReqPrint == 1\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </div> </div> <!-- <vehicle-history ng-disabled=\"true\" skip-enable vin=\"mDataCrm.Vehicle.VIN\">\r" +
    "\n" +
    "            </vehicle-history> --> </div> <div is-open=\"status.open\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"opened()\" style=\"font-size: 26px; height:30px;font-weight: bold\"> Order Pekerjaan </div> </uib-accordion-heading> <div class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\"> <bslist data=\"JobRequest\" m-id=\"JobRequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"ButtonList\" modal-size=\"small\" modal-title=\"Permintaan\" list-title=\"Permintaan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea ng-maxtlength=\"200\" maxlength=\"200\" class=\"form-control\" placeholder=\"Job Request\" name=\"reqdesc\" ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "                                            </textarea> <bserrmsg field=\"reqdesc\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\" style=\"text-align:right\"> <div class=\"btn-group\" style=\"margin-top:25px\"> <button type=\"button\" id=\"btnPrediagnose\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\"> Pre Diagnose </button> </div> </div> <div class=\"col-md-12\"> <bslist data=\"JobComplaint\" m-id=\"JobComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"ButtonList\" modal-size=\"small\" modal-title=\"Keluhan\" list-title=\"Keluhan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Complaint Category\" ng-disabled=\"true\" skip-enable> </bsselect> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Complaint Text\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                        </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\"> <bsreqlabel>Tipe Keluhan</bsreqlabel> <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Complaint Category\"> </bsselect> <bserrmsg field=\"ctype\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom: 100px\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\">\r" +
    "\n" +
    "                                                        </textarea> <bserrmsg field=\"ctext\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\"> <div id=\"headerOrderTask\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nomor WO Estimasi</label> <input type=\"text\" min=\"0\" class=\"form-control\" name=\"NomorWo\" ng-model=\"mData.NomorWoEstimasi\"> <bserrmsg field=\"NomorWo\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Kategori WO</bsreqlabel> <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\"> <!--  ng-change=\"PDS(mData.WoCategoryId)\" --> </bsselect> <bserrmsg field=\"categoryWO\"></bserrmsg> </div> </div> </div> <div class=\"col-md-12\"> <!-- ng-if=\"refreshBslist\" --> <!-- <button ng-click=\"testBtn()\">TES AJAH DAH</button> --> <bslist form-name=\"OrderPekerjaanWOLIST\" data=\"gridWork\" id=\"gridOrder\" m-id=\"JobTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" on-before-edit=\"onBeforeEdit\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApiJob\" confirm-save=\"false\" list-title=\"Job List\" button-settings=\"ButtonList\" modal-title=\"Job Data\" grid-hide-action-column=\"flagColumn\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p> </div> </bslist-template> <bslist-detail-template> <div> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\">{{totalWork | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalWork - totalWorkDiscounted | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{totalWorkDiscounted | currency:\"Rp. \":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\">{{totalMaterial | currency:\"Rp. \":0}}</td> <!-- <td style=\"text-align: right;\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp. \":0}}</td> --> <td style=\"text-align: right\">{{nominalDiscountMaterial | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalMaterialDiscounted | currency:\"Rp. \":0}}</td> </tr> <!-- <tr>\r" +
    "\n" +
    "                                <td>Estimasi OPL</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                <td></td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr> --> <tr> <td>PPN</td> <td style=\"text-align: right\"> <!-- {{((totalWork + totalMaterial) * 10)/100 | currency:\"Rp. \":0}} --> </td> <td></td> <td style=\"text-align: right\"> {{totalPPN | currency:\"Rp.\":0}} <!-- {{(((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}} --> </td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"> <!-- {{totalWork + totalMaterial | currency:\"Rp. \":0}} --> </td> <td></td> <!-- <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                        {{(totalMaterial - totalMaterialDiscounted)+(totalWork - totalWorkDiscounted) | currency:\"Rp.\":0}}\r" +
    "\n" +
    "                                    </td> --> <td style=\"text-align: right\"> {{ totalEstimasi | currency:\"Rp. \":0}} <!-- {{(totalMaterialDiscounted + totalWorkDiscounted) + (((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}} --> </td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\">{{totalDp | currency:\"Rp. \":0}}</td> <td></td> <td></td> </tr> </tbody> </table> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div ng-click=\"opened()\" style=\"font-size: 26px; height:30px;font-weight: bold\"> OPL </div> </uib-accordion-heading> <!-- ============================================OPL BS LIST =================================================== --> <!-- <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist ng-if='refreshBslist' data=\"oplData\" m-id=\"OPLId\" form-name=\"orderOPL\" list-model=\"lmModelOpl\" on-before-new=\"onBeforeNewOPL\" on-before-save=\"onBeforeSaveOpl\" on-after-delete=\"onAfterDeleteOPL\" on-before-edit=\"onBeforeEditOPL\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\"\r" +
    "\n" +
    "                        confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"ButtonList\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <strong>{{lmItem.OPLName}}</strong>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        {{lmItem.VendorName}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Estimasi : </label> {{lmItem.TargetFinishDate | date:'dd-MM-yyyy'}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Quantity : </label> {{lmItem.QtyPekerjaan}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>NO. OPL : </label> {{lmItem.OplNo}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Status : {{lmItem.xStatus}}</label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMasterOpl.html'\"></div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                    <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "                        <tbody>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td width=\"20%\">Total</td>\r" +
    "\n" +
    "                                <td>{{sumWorkOpl * (1.1) | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </tbody>\r" +
    "\n" +
    "                    </table>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> <!-- ============================================OPL BS LIST =================================================== --> <!-- ============================================OPL AG Grid =================================================== --> <div id=\"accJobOPL\"> <div smart-include=\"app/services/reception/woHistoryGR/OPL.html\"></div> </div> <!-- ============================================OPL AG Grid =================================================== --> </div> <div uib-accordion-group class=\"panel-default\" ng-click=\"dataForWAC()\" is-open=\"accordionWac.isOpen\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> WAC </div> <!-- <span ng-click=\"dataForWAC()\"></span> --> </uib-accordion-heading> <!--<div ng-include=\"'app/services/reception/wo/includeForm/wac.html'\">\r" +
    "\n" +
    "                 </div> --> <div id=\"accWAC\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">INTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Km</bsreqlabel> <input type=\"text\" step=\"1\" min=\"0\" class=\"form-control\" name=\"Km\" placeholder=\"Km\" ng-model=\"mData.Km\" ng-maxlength=\"7\" maxlength=\"7\" ng-keyup=\"givePattern(mData.Km,$event)\"> <bserrmsg field=\"km\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                            <label for=\"slider\">Fuel</label>\r" +
    "\n" +
    "                            <rzslider rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider>\r" +
    "\n" +
    "                        </div> --> <div id=\"slide\" class=\"col-md-6\"> <label for=\"slider\">Fuel</label> <div class=\"scaleWarp\"> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> <garis>|</garis> <garis style=\"font-size: 20px;font-weight:bold\">|</garis> <garis>|</garis> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> </div> <rzslider class=\"custom-slider\" rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.BanSerep\" label=\"Ban Serep\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.CDorKaset\" label=\"CD/Kaset\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Payung\" label=\"Payung\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Dongkrak\" label=\"Dongkrak\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.STNK\" label=\"STNK\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.P3k\" label=\"P3K\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.KunciSteer\" label=\"Kunci Steer\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ToolSet\" label=\"Tool Set\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ClipKarpet\" label=\"Clip Karpet\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.BukuService\" label=\"Buku Service\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Alarm Condition</bsreqlabel> <bsselect name=\"alarmCondition\" ng-model=\"mData.AlarmCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition Alarm\"> </bsselect> <bserrmsg field=\"alarmCondition\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>AC Condition</bsreqlabel> <bsselect name=\"AcCondition\" ng-model=\"mData.AcCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition AC\"> </bsselect> <bserrmsg field=\"AcCondition\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Power Window</bsreqlabel> <bsselect name=\"PowerWindow\" ng-model=\"mData.PowerWindow\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Condition Power Window\"> </bsselect> <bserrmsg field=\"PowerWindow\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <bsreqlabel>Jenis Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                                <bsselect name=\"CarType\" ng-model=\"mData.CarType\" data=\"carTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Jenis Kendaraan\">\r" +
    "\n" +
    "                                </bsselect>\r" +
    "\n" +
    "                                <bserrmsg field=\"CarType\"></bserrmsg>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div> --> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Other</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Other\" name=\"Other\" ng-model=\"mData.OtherDescription\">\r" +
    "\n" +
    "                                </textarea> <bserrmsg field=\"Other\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <bsreqlabel>Money Amount</bsreqlabel> <textarea currency-formating number-only class=\"form-control\" placeholder=\"Money Amount\" name=\"moneyAmount\" ng-model=\"mData.MoneyAmount\">\r" +
    "\n" +
    "                            </textarea> <bserrmsg field=\"moneyAmount\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">EXTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Type Kendaraan</label> <bsselect name=\"filterType\" ng-model=\"filter.vType\" data=\"VTypeData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Type Kendaraan\" on-select=\"chooseVType(selected)\" icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <!-- <div class=\"row\" ng-repeat=\"imageWAC in imageWACArr\">\r" +
    "\n" +
    "                                        <img ng-if=\"imageSel==imageWAC.imgId\" ng-areas=\"areasArray\"\r" +
    "\n" +
    "                                            ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\"\r" +
    "\n" +
    "                                            ng-src=\"{{imageWAC.img}}\" style=\"width:700px;height:400px\"> --> <!-- <img ng-if=\"imageSel==imageWAC.imgId\" ng-src=\"{{imageWAC.img}}\" style=\"width:700px;height:400px\"> --> <!-- </div> --> <!-- <button ng-click=\"tes()\">tes</button> --> <div class=\"row\" ng-if=\"showPicWac\"> <div class=\"row\"> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"680\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"!isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"750\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> </div> </div> <div class=\"row\" ng-show=\"!disableWACExt\"> <div class=\"col-md-12\"> <!-- <div ui-grid=\"gridWac\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellNav ui-grid-pinning ui-grid-auto-resize ui-grid-edit class=\"grid\" ng-style=\"getTableHeight()\">\r" +
    "\n" +
    "                                    <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>No data available</div>\r" +
    "\n" +
    "                                    <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div>\r" +
    "\n" +
    "                                </div> --> <table id=\"wacGrid\" width=\"100%\" class=\"table table-bordered table-hover table-responsive\"> <thead> <tr> <th ng-style=\"{'width':'{{col.width}}'}\" ng-hide=\"col.visible == false\" ng-repeat=\"col in gridWac.columnDefs\">{{col.name}}</th> </tr> </thead> <tbody> <tr style=\"height:40px\" ng-repeat=\"column in gridWac.data\"> <td ng-hide=\"true\" style=\"width:5%\">{{column.JobWACExtId}}</td> <td style=\"width:55%\">{{column.ItemName}}</td> <td style=\"width:45%\" style=\"padding:0px\"> <select class=\"form-control\" name=\"status\" convert-to-number ng-change=\"onDropdownChange(column)\" ng-model=\"column.Status\" placeholder=\"Pilih Kerusakan\" style=\"height:30px\"> <option ng-repeat=\"item in DamageVehicle\" value=\"{{item.Id}}\">{{item.Name}}</option> </select> </td> <td ng-hide=\"true\" style=\"width:5%\">{{column.typeName}}</td> </tr> <tr ng-if=\"!gridWac.data.length\"> <td colspan=\"{{gridWac.columnDefs.length}}\"> <div style=\"opacity: .25;font-size: 3em;width: 100%;text-align: center;z-index: 1000\">No Data Available</div> </td> </tr> </tbody> </table> </div> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>Jenis Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                            <bsselect name=\"CarType\" ng-model=\"mData.CarType\" data=\"carTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Jenis Kendaraan\">\r" +
    "\n" +
    "                            </bsselect>\r" +
    "\n" +
    "                            <bserrmsg field=\"CarType\"></bserrmsg>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-md-12\" style=\"text-align:center;\">\r" +
    "\n" +
    "                        <pitch-canvas data='drawData' status=\"paramClosePopover\"></pitch-canvas>\r" +
    "\n" +
    "                        <div id='myPopover' ng-show='showPopup' ng-style=\"popoverStyle\">\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <button ng-click=\"closePopover(1)\">Baret</button>\r" +
    "\n" +
    "                                <button ng-click=\"closePopover(2)\">Penyok</button>\r" +
    "\n" +
    "                                <button ng-click=\"closePopover(3)\">Rusak</button>\r" +
    "\n" +
    "                                <button ng-click=\"closePopover(0)\">Delete</button>\r" +
    "\n" +
    "                                <p><span>Y : {{coorY}} - X : {{coorX}}</span></p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div> --> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <!-- <button class=\"btn rbtn\" ng-click=\"disableExt\">Clear</button> --> <!-- <button type=\"button\" class=\"btn rbtn\" ng-model=\"disableWACExt\" uib-btn-checkbox btn-checkbox-true=\"1\" btn-checkbox-false=\"0\">Clear</button> --> <!-- <button class=\"btn rbtn\" ng-disabled=\"disableBPCenter\" skip-enable ng-click=\"\">BP Center</button> --> <!-- added by sss on 2017-09-07 --> <bscheckbox ng-model=\"disableWACExt\" label=\"Clear\" true-value=\"true\" false-value=\"false\" ng-change=\"chgChk()\"> </bscheckbox> <!-- <div class=\"row\">\r" +
    "\n" +
    "                                <bsdatepicker\r" +
    "\n" +
    "                                        name=\"bpCenterDate\"\r" +
    "\n" +
    "                                        date-options=\"DateOptions\"\r" +
    "\n" +
    "                                        ng-model=\"bpCenterDate\"\r" +
    "\n" +
    "                                        ng-change=\"setCenterDate()\">\r" +
    "\n" +
    "                                </bsdatepicker>\r" +
    "\n" +
    "                            </div> --> <!-- <div class=\"row\" style=\"margin-top:10px\">\r" +
    "\n" +
    "                                <button class=\"btn rbtn\" ng-disabled=\"disableBPCenter\" skip-enable ng-click=\"triggerBPC()\">\r" +
    "\n" +
    "                                        BP Center\r" +
    "\n" +
    "                                </button>\r" +
    "\n" +
    "                            </div> --> <!--  --> </div> <div class=\"col-md-8\" ng-if=\"showPicWac\"> <div class=\"containeramk\" ng-style=\"{'max-width': boundingBox.width + 'px', 'max-height': boundingBox.height + 'px'}\"> <signature-pad accept=\"accept\" disabled clear=\"clear\" height=\"220\" width=\"568\" dataurl=\"dataurl\"></signature-pad> <div class=\"buttonsamk\"> <button class=\"btn wbtn\" ng-disabled=\"disableReset\" skip-enable ng-click=\"clear()\">Clear</button> <!-- <button class=\"btn rbtn\" ng-click=\"dataurl = signature.dataUrl\" ng-disabled=\"!signature\">Reset</button> --> <button class=\"btn rbtn\" ng-click=\"signature = accept(); open(signature)\">Sign</button> </div> </div> </div> <div class=\"col-md-2\"> <!-- <input type=\"button\" class=\"btn rbtn\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" isImage=\"1\" img-upload=\"uploadFiles\"><i class=\"fa fa-fw fa-lg fa-car\" style=\"font-size:30px; padding:8px 8px 8px 0px;margin-left:8px;margin-right:8px;\"></i>\r" +
    "\n" +
    "                            <input type=\"file\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" isImage=\"1\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1;\"> --> <!-- <label class=\"input-group-btn\">\r" +
    "\n" +
    "                                <span class=\"btn rbtn\" style=\"padding:12px 10px 12px 10px;\">\r" +
    "\n" +
    "                                    <i class=\"fa fa-fw fa-lg fa-car\" style=\"font-size:55px;\"></i>\r" +
    "\n" +
    "                                    <input type=\"file\" bs-upload=\"onFileSelect($files)\" maxupload=\"4\" img-upload=\"fileKK\" style=\"display: none;\" uploadFiles=\"uploadWACImage\" multiple>\r" +
    "\n" +
    "                                </span>\r" +
    "\n" +
    "                            </label> --> <image-control max-upload=\"4\" is-image=\"1\" img-upload=\"uploadFiles\" multiple> <form-image-control> <input capture=\"camera\" type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"file\" ng-init=\"mData.UploadPhoto = uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> <!-- <button ng-click=\"TestingImage(mData)\">Testing Image</button> --> <!-- <div class=\"custom-file-upload\">\r" +
    "\n" +
    "                                <input type=\"file\" id=\"file\" name=\"myfiles[]\" bs-upload=\"onFileSelect($files)\" img-upload=\"fileKK\" file-model=\"mData.Attachment\" upload />\r" +
    "\n" +
    "                            </div>  --> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"col-md-12\"> <div class=\"col-xs-2\"> <div class=\"\" style=\"margin-top:50px\"> <bscheckbox ng-model=\"mData.isBPToCenter\" ng-disabled=\"disableBPCenter\" ng-change=\"triggerBPC()\" label=\"BP Center\" ng-true-value=\"'true'\" ng-false-value=\"'false'\" true-value=\"'true'\" false-value=\"'false'\"> </bscheckbox> <!-- <input type=\"checkbox\" ng-model=\"mData.isBPToCenter\" ng-disabled=\"disableBPCenter\" ng-change=\"triggerBPC()\" ng-true-value=\"'true'\" ng-false-value=\"'false'\"> --> </div> </div> <!-- <pre>{{mData.isBPToCenter}}</pre> --> <div class=\"col-xs-10\"> <div ng-show=\"mData.isBPToCenter == 'true'\" class=\"\" style=\"margin-top:50px\"> <div class=\"form-group\"> <label>Request Appointment Date</label> <bsdatepicker name=\"bpCenterDate\" date-options=\"DateOptions\" ng-model=\"bpCenterDate.date\"> </bsdatepicker> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-xs-6\"> <select class=\"form-control\" convert-to-number ng-show=\"mData.isBPToCenter == 'true'\" ng-change=\"changeBPCenter(mData.BPCenter)\" data=\"BPCenterdata\" ng-model=\"mData.BPCenter\" placeholder=\"\"> <option ng-repeat=\"bp in BPCenterdata\" ng-selected=\"mData.BPCenter == bp.OutletIdBPCenter\" value=\"{{bp.OutletIdBPCenter}}\">{{bp.Name}}</option> </select> </div> </div> </div> </div> </div> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                Permintaan\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "            <div class=\"row\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist data=\"JobRequest\" m-id=\"RequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Job Request\" list-title=\"Job Request\"\r" +
    "\n" +
    "                        list-height=\"200\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <textarea class=\"form-control\" placeholder=\"Job Request\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    </textarea>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div>\r" +
    "\n" +
    "                                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                    <bsreqlabel>Job Request</bsreqlabel>\r" +
    "\n" +
    "                                    <textarea class=\"form-control\" placeholder=\"Job Request\" name='reqdesc' ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "                            </textarea>\r" +
    "\n" +
    "                                    <bserrmsg field=\"reqdesc\"></bserrmsg>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <!--  --> <!-- <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                Keluhan\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"row\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                <div class=\"col-md-12\" style=\"text-align:right;\">\r" +
    "\n" +
    "                    <div class=\"btn-group\" style=\"margin-top:25px;\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\">\r" +
    "\n" +
    "                      Pre Diagnose\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist data=\"JobComplaint\" m-id=\"ComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listView\" modal-size=\"small\" modal-title=\"Complaint\"\r" +
    "\n" +
    "                        list-title=\"Complaint\" list-height=\"200\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Complaint Type: </bsreqlabel>\r" +
    "\n" +
    "                                    <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Complaint Category\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                    </bsselect>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <textarea class=\"form-control\" placeholder=\"Complaint Text\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                                </textarea>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Complaint Type</bsreqlabel>\r" +
    "\n" +
    "                                    <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Complaint Category\">\r" +
    "\n" +
    "                                    </bsselect>\r" +
    "\n" +
    "                                    <bserrmsg field=\"ctype\"></bserrmsg>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Job Complaint</bsreqlabel>\r" +
    "\n" +
    "                                    <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\">\r" +
    "\n" +
    "                                                </textarea>\r" +
    "\n" +
    "                                    <bserrmsg field=\"ctext\"></bserrmsg>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Job Suggest </div> </uib-accordion-heading> <div id=\"accJobSuggest\" class=\"row\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-12\" style=\"margin-top: 10px\"> <div class=\"row\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editSuggest()\" ng-hide=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"SimpanJobSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" style=\"margin-top: 25px\"> <div class=\"col-md-12\"> <textarea ng-readonly=\"disSgs\" ng-model=\"mData.Suggestion\" maxlength=\"300\" style=\"width: 100%;height: 100px\"></textarea> </div> </div> </div> </div> </div> <!--         <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                Job List\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-6\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <label>Nomor WO Estimasi</label>\r" +
    "\n" +
    "                        <input type=\"text\" min=0 class=\"form-control\" name=\"NomorWo\" ng-model=\"mData.NomorWoEstimasi\">\r" +
    "\n" +
    "                        <bserrmsg field=\"NomorWo\"></bserrmsg>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-6\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsreqlabel>Kategori WO</bsreqlabel>\r" +
    "\n" +
    "                        <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                        <bserrmsg field=\"categoryWO\"></bserrmsg>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist data=\"gridWork\" m-id=\"JobTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-save=\"onBeforeSave\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" on-before-edit=\"onBeforeEdit\" list-model=\"lmModel\"\r" +
    "\n" +
    "                        list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api='listApiJob' confirm-save=\"false\" list-title=\"Job List\" button-settings=\"ButtonList\" modal-title=\"Job Data\"\r" +
    "\n" +
    "                        modal-title-detail=\"Parts Data\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div class=\"col-md-6\" style=\"text-align:left;\">\r" +
    "\n" +
    "                                <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <p>Tipe :{{lmItem.catName}}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-3\">\r" +
    "\n" +
    "                                    <p>Flat Rate: {{lmItem.FlatRate}}</p>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-6\" style=\"text-align:right;\">\r" +
    "\n" +
    "                                <p>Pembayaran: {{lmItem.PaidBy}}</p>\r" +
    "\n" +
    "                                <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-detail-template>\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <p><strong>{{ ldItem.PartsCode }}</strong></p>\r" +
    "\n" +
    "                                <p>{{ ldItem.PartsName }}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <p>{{ ldItem.Availbility }}</p>\r" +
    "\n" +
    "                                <p>Pembayaran : {{ ldItem.paidName}}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p>\r" +
    "\n" +
    "                                <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp.\"}}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"col-md-3\">\r" +
    "\n" +
    "                                <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p>\r" +
    "\n" +
    "                                <p>Reserve : {{ ldItem.Qty }}</p>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-detail-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\">\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                        <bslist-modal-detail-form>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\">\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-detail-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                    <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "                        <thead>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <th>Item</th>\r" +
    "\n" +
    "                                <th>Harga</th>\r" +
    "\n" +
    "                                <th>Disc</th>\r" +
    "\n" +
    "                                <th>Sub Total</th>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </thead>\r" +
    "\n" +
    "                        <tbody>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Estimasi Service / Jasa</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalWork | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalWork - totalWorkDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalWorkDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Estimasi Material (Parts / Bahan)</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalMaterial | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalMaterialDiscounted | currency:\"Rp.\":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Estimasi OPL</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOpl | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOpl - sumWorkOplDiscounted | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{sumWorkOplDiscounted | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>PPN</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                </td>\r" +
    "\n" +
    "                                <td></td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                    {{(((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Total Estimasi</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                </td>\r" +
    "\n" +
    "                                <td></td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{(totalMaterialDiscounted + totalWorkDiscounted) + (((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td>Total DP</td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\"></td>\r" +
    "\n" +
    "                                <td></td>\r" +
    "\n" +
    "                                <td style=\"text-align: right;\">{{totalDp | currency:\"Rp. \":0}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </tbody>\r" +
    "\n" +
    "                    </table>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <!--         <div uib-accordion-group class=\"panel-default\">\r" +
    "\n" +
    "            <uib-accordion-heading>\r" +
    "\n" +
    "                OPL\r" +
    "\n" +
    "            </uib-accordion-heading>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <bslist data=\"oplData\" m-id=\"oplId\" list-model=\"lmModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApi\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\"\r" +
    "\n" +
    "                        list-title=\"Order Pekerjaan Luar\" list-height=\"200\">\r" +
    "\n" +
    "                        <bslist-template>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        {{lmItem.OplName}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        {{lmItem.VendorName}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Dibutuhkan : </label> {{lmItem.RequiredDate | date:'dd-MM-yyyy'}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Harga : </label> {{lmItem.Price | currency:\"Rp.\"}}\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-md-4\">\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-template>\r" +
    "\n" +
    "                        <bslist-modal-form>\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"col-md-12\">\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <bsreqlabel>Nama OPL</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModel.OplName\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"oplName\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <bsreqlabel>Vendor</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"text\" class=\"form-control\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModel.VendorName\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"oplName\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                        <bsreqlabel>Pembayaran</bsreqlabel>\r" +
    "\n" +
    "                                        <bsselect name=\"pembayaran\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected)\">\r" +
    "\n" +
    "                                        </bsselect>\r" +
    "\n" +
    "                                        <bserrmsg field=\"pembayaran\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <bsreqlabel>Dibutuhkan Tanggal</bsreqlabel>\r" +
    "\n" +
    "                                        <bsdatepicker name=\"RequiredDate\" date-options=\"DateOptions\" ng-model=\"lmModel.RequiredDate\" ng-change=\"DateChange(dt)\">\r" +
    "\n" +
    "                                        </bsdatepicker>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                        <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"text\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.ReceiveBy\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"price\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                        <bsreqlabel>Harga</bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Price\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"price\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\">\r" +
    "\n" +
    "                                        <label>Notes</label>\r" +
    "\n" +
    "                                        <textarea class=\"form-control\" name=\"Address\" ng-model=\"lmModel.Address\" style=\"height: 100px;\"></textarea>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                                        <label>Discount</label>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"discount\" ng-model=\"lmModel.Discount\">\r" +
    "\n" +
    "                                        <bserrmsg field=\"discount\"></bserrmsg>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </bslist-modal-form>\r" +
    "\n" +
    "                    </bslist>\r" +
    "\n" +
    "                    <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\">\r" +
    "\n" +
    "                        <tbody>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td width=\"20%\">Total</td>\r" +
    "\n" +
    "                                <td>{{sumWork | currency:\"Rp.\"}}</td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </tbody>\r" +
    "\n" +
    "                    </table>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <!--  --> <div uib-accordion-group class=\"panel-default\" ng-click=\"PressSummary()\" is-open=\"accordionSummary.isOpen\"> <uib-accordion-heading> <div style=\"font-size: 26px; height:30px;font-weight: bold\"> Summary </div> </uib-accordion-heading> <div id=\"accSummary\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label>No. Polisi</label> </div> <div class=\"form-group\"> <label>Nomor WO</label> </div> <div class=\"form-group\"> <label>Kategori WO</label> </div> </div> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label>{{mDataCrm.Vehicle.LicensePlate}}</label> </div> <div class=\"form-group\"> <label ng-if=\"mData.WoNo != Null\">{{mData.WoNo}}</label> <label ng-if=\"mData.WoNo == Null\">-</label> </div> <div class=\"form-group\"> <bsselect ng-disabled=\"true\" data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\"> </bsselect> </div> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"TaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-after-cancel=\"onAfterCancel\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridSumPartsDetail\" list-api=\"listApi\" confirm-save=\"false\" list-title=\"Job List\" modal-title=\"Job Data\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp.\"}}</p> </div> </bslist-template> <bslist-detail-template> <div> <div class=\"col-md-3\"> <p><strong>{{ ldItem.Part.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.paidName}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp.\"}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/woHistoryGR/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\">{{totalWork | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalWork - totalWorkDiscounted | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalWorkDiscounted | currency:\"Rp.\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\">{{totalMaterial | currency:\"Rp. \":0}}</td> <!-- <td style=\"text-align: right;\">{{totalMaterial - totalMaterialDiscounted | currency:\"Rp.\":0}}</td> --> <td style=\"text-align: right\">{{nominalDiscountMaterial | currency:\"Rp.\":0}}</td> <td style=\"text-align: right\">{{totalMaterialDiscounted | currency:\"Rp.\":0}}</td> </tr> <tr> <td>Estimasi OPL</td> <td style=\"text-align: right\">{{sumWorkOpl | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{sumWorkOpl - sumWorkOplDiscounted | currency:\"Rp. \":0}}</td> <td style=\"text-align: right\">{{sumWorkOplDiscounted | currency:\"Rp. \":0}}</td> <!-- <td style=\"text-align: right;\">{{sumWorkOpl - sumWorkOplDiscounted | currency:\"Rp. \":0}}</td> --> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"> <!-- {{((totalWork + totalMaterial) * 10)/100 | currency:\"Rp. \":0}} --> </td> <td></td> <td style=\"text-align: right\"> {{ totalPPN + (sumWorkOplDiscounted*(10/100)) | currency:\"Rp. \":0}} <!-- {{(((totalWorkDiscounted + totalMaterialDiscounted + sumWorkOplDiscounted) * 10)/100) | currency:\"Rp. \":0}} --> </td> </tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"> <!-- {{totalWork + totalMaterial | currency:\"Rp. \":0}} --> </td> <td></td> <!-- <td style=\"text-align: right;\">\r" +
    "\n" +
    "                                        {{(totalMaterial - totalMaterialDiscounted)+(totalWork - totalWorkDiscounted) | currency:\"Rp.\":0}}\r" +
    "\n" +
    "                                    </td> --> <td style=\"text-align: right\"> {{ totalEstimasi + sumWorkOplDiscounted + (sumWorkOplDiscounted*(10/100)) | currency:\"Rp. \":0}} <!-- {{(totalMaterialDiscounted + totalWorkDiscounted) + (((totalWorkDiscounted + totalMaterialDiscounted) * 10)/100) + ((sumWorkOplDiscounted) + ((sumWorkOplDiscounted * 10) / 100 )) | currency:\"Rp. \":0}} --> </td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\"></td> <td></td> <td style=\"text-align: right\">{{totalDp | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\" ng-disabled=\"true\" skip-enable> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Lama Pekerjaan : </label> <input type=\"text\" ng-model=\"mData.ActualRate\" class=\"form-control\" placeholder=\"0\" name=\"\" ng-disabled=\"true\" skip-enable> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Part Bekas</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radio1\" ng-model=\"mData.UsedPart\" name=\"radio1\" value=\"1\" checked> <label for=\"radio1\">Kembalikan</label> <input type=\"radio\" id=\"radio2\" ng-model=\"mData.UsedPart\" name=\"radio1\" value=\"0\"> <label for=\"radio2\">Tinggal</label> <!-- {{mData.UsedPart}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Waiting / Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"wait1\" ng-model=\"mData.IsWaiting\" name=\"radio2\" value=\"1\" checked> <label for=\"wait1\">Waiting</label> <input type=\"radio\" id=\"wait2\" ng-model=\"mData.IsWaiting\" name=\"radio2\" value=\"0\"> <label for=\"wait2\">Drop Off</label> <!-- {{mData.IsWaiting}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Car Wash</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"washing1\" ng-model=\"mData.IsWash\" name=\"radio3\" value=\"1\" checked> <label for=\"washing1\">Wash</label> <input type=\"radio\" id=\"washing2\" ng-model=\"mData.IsWash\" name=\"radio3\" value=\"0\"> <label for=\"washing2\">No Wash</label> <!-- {{mData.IsWash}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Penggantian Parts</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changepart1\" ng-model=\"mData.PermissionPartChange\" name=\"radio5\" value=\"1\" checked> <label for=\"changepart1\">Langsung</label> <input type=\"radio\" id=\"changepart2\" ng-model=\"mData.PermissionPartChange\" name=\"radio5\" value=\"0\"> <label for=\"changepart2\">Izin</label> <!-- <input type=\"radio\" ng-model=\"mData.PermissionPartChange\" name=\"radio6\" value=\"0\">J\r" +
    "\n" +
    "                                <input type=\"radio\" ng-model=\"mData.PermissionPartChange\" name=\"radio6\" value=\"1\">K --> <!-- {{mData.PermissionPartChange}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Metode Pembayaran</label> </div> </div> <div class=\"col-md-6\"> <bsselect name=\"paymentType\" ng-model=\"mData.PaymentType\" data=\"paymentTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Type Pembayaran\"> </bsselect> <bserrmsg field=\"paymentType\"></bserrmsg> </div> </div> <div class=\"row\" style=\"margin-top:25px; margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Scheduled Service Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"firstDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.firstDate\" ng-disabled=\"true\"> </bsdatepicker> </div> <div class=\"form-group text-center\" style=\"width:20px\"> <b>-</b> </div> <div class=\"form-group\"> <bsdatepicker name=\"lastDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.lastDate\" ng-disabled=\"true\"> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Estimation Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"estDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.EstimateDate\" ng-disabled=\"true\" skip-enable> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Adjusment Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"adjDate\" min-date=\"minDate\" date-options=\"DateOptions\" ng-model=\"mData.AdjusmentDate\" ng-disabled=\"adjustDate\" ng-change=\"adjustTime(mData.AdjusmentDate)\" skip-enable> </bsdatepicker> </div> <div class=\"form-group\" style=\"width:200px\"> <div uib-timepicker ng-model=\"mData.AdjusmentDate\" ng-change=\"changed()\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" ng-disabled=\"adjustDate\" skip-enable></div> </div> </div> <div class=\"col-md-3 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bscheckbox ng-model=\"mData.customerRequest\" label=\"Customer Request\" ng-click=\"adjRequest(mData.customerRequest)\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <!-- <div class=\"col-md-2 form-inline\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    </div> --> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Kode Transaksi</label> </div> <div class=\"col-md-9 form-inline\"> <bsselect ng-disabled=\"false\" data=\"codeTransaction\" name=\"codeTrans\" item-value=\"Id\" item-text=\"Description\" ng-model=\"mData.CodeTransaction\" placeholder=\"Kode Transaksi\"> </bsselect> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"btn-group pull-right\">\r" +
    "\n" +
    "                            <button type=\"button\" ng-disabled=\"counterGi > 0\" name=\"CloseWO\" ng-click=\"cancelWo(mData)\" class=\"rbtn btn\">\r" +
    "\n" +
    "                                Batal WO\r" +
    "\n" +
    "                            </button>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div> --> </div> <!--             <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"btn-group pull-right\">\r" +
    "\n" +
    "                    <button type=\"button\" name=\"saveWO\" class=\"rbtn btn\" ng-click=\"saveNewWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                        Simpan\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                    <button type=\"button\" name=\"printWO\" class=\"rbtn btn\">\r" +
    "\n" +
    "                        Print Preview\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                    <button type=\"button\" name=\"releaseWO\" class=\"rbtn btn\" ng-click=\"releaseWO(isParts, isWashing, isWaiting, isChangePart)\">\r" +
    "\n" +
    "                        Release WO\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> </div> <div class=\"row\"> <div class=\"btn-group pull-right\"> <button type=\"button\" ng-disabled=\"counterGi > 0 || cekBatal == 1\" skip-enable name=\"CloseWO\" ng-click=\"cancelWo(mData)\" class=\"rbtn btn\"> Batal WO </button> </div> </div> </div> </uib-accordion> </bsform-grid> <bsui-modal show=\"showTeco\" title=\"TECO\" data=\"mData\"> <!-- untuk Teco --> <div ng-if=\"showTeco == true\"> <div smart-include=\"app/services/reception/woHistoryGR/Teco.html\"></div> </div> </bsui-modal> <bsui-modal show=\"show_modal.show\" title=\"PKS\" mode=\"modalMode\" button-settings=\"buttonEmail\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 25px\" ng-repeat=\"a in dataPKSFinal\"> <label>Balances : {{a.Balances | currency:\"Rp.\"}}</label><br> <label>Start Periode : {{a.StartPeriod |date:dd/mm/yyyy}}</label><br> <label>End Periode : {{a.EndPeriod |date:dd/mm/yyyy}}</label> </div> </div> </bsui-modal> <!-- approval --> <bsui-modal show=\"show_modal.showApprovalPrint\" title=\"Alasan Di Tolak\" data=\"mPrintServiceHistory\" on-save=\"onReject\" button-settings=\"buttonSettingModalApprovalReject\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"contactForm\"> <div class=\"row\"> <textarea name=\"reason\" ng-model=\"mPrintServiceHistory.RejectReason\" class=\"form-control\"></textarea> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalPrintApprove\" title=\"Alasan Di Terima\" data=\"mPrintServiceHistory\" on-save=\"onApprove\" button-settings=\"buttonSettingModalApprovalApprove\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"contactForm\"> <div class=\"row\"> <textarea name=\"reason\" ng-model=\"mPrintServiceHistory.RejectReason\" class=\"form-control\"></textarea> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalDp\" title=\"Alasan Approval DP\" data=\"modal_model\" on-save=\"doSaveApprovalDp\" on-cancel=\"doBack\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"ApprovalForm\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <h4>{{modal_model.RequestReason}} dari {{modal_model.DPDefault | currency:\"Rp \"}} Menjadi {{modal_model.DPRequested | currency:\"Rp \"}}</h4> <textarea style=\"height:104px\" ng-required=\"isReject\" class=\"form-control\" placeholder=\"Reason\" ng-model=\"modal_model.Reason\"></textarea> </div> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalJobReduction\" title=\"Alasan Approve/Reject Pengurangan Pekerjaan\" data=\"modal_model\" on-save=\"doSaveApprovalJobReduction\" on-cancel=\"doBack\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"ApprovalForm\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <!-- <h4>{{modal_model.RequestReason}} Dari {{modal_model.DPDefault | currency:\"Rp.\"}} ~ {{modal_model.DPRequested | currency:\"Rp.\"}}</h4> --> <textarea style=\"height:104px\" ng-required=\"isReject\" class=\"form-control\" placeholder=\"Reason\" ng-model=\"modal_model.Reason\"></textarea> </div> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalDiskon\" title=\"Alasan Approval Diskon\" data=\"modal_model\" on-save=\"doSaveApprovalDiskon\" on-cancel=\"doBack\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"ApprovalForm\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <h4>{{modal_model.RequestReason}} <br> Harga Normal {{modal_model.HargaNormal | currency:\"Rp \"}} dengan nilai diskon sebesar {{modal_model.Discount}}%</h4> <textarea style=\"height:104px\" ng-required=\"isReject\" class=\"form-control\" placeholder=\"Reason\" ng-model=\"modal_model.Reason\"></textarea> </div> </div> </div> </bsui-modal> <bsui-modal show=\"show_modal.showApprovalDiskonReject\" title=\"Alasan Reject Diskon\" data=\"modal_model\" on-save=\"doSaveApprovalDiskon\" on-cancel=\"doBack\" mode=\"modalModeAproval\"> <!-- on-cancel=\"CheckCancel\" --> <div ng-form name=\"ApprovalForm\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <h4>{{modal_model.RequestReason}} <br> Harga Normal {{modal_model.HargaNormal | currency:\"Rp \"}} dengan nilai diskon sebesar {{modal_model.Discount}}%</h4> <textarea style=\"height:104px\" ng-required=\"isReject\" class=\"form-control\" placeholder=\"Reason\" ng-model=\"modal_model.Reason\"></textarea> </div> </div> </div> </bsui-modal> <bsui-modal show=\"showPrediagnose\" title=\"PreDiagnose Form\" data=\"dataPrediagnose\" on-save=\"savePreDiagnose\" on-cancel=\"cancelPreDiagnose\" button-settings=\"listEstimationButton\" mode=\"modalModepreDiagnose\"> <div ng-form name=\"preDiagnoseForm\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <div ng-include=\"'app/services/preDiagnose/preDiagnose_CustomerComplaint.html'\"></div> --> <div class=\"form-group\"> <div class=\"col-md-12\" ng-hide=\"true\"> <div class=\"col-xs-12\"> <label>Job Id</label> </div> <div class=\"col-xs-4\"> <input type=\"text\" name=\"JobId\" ng-model=\"dataPrediagnose.JobId\" class=\"form-control\"> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-12\" ng-hide=\"true\"> <div class=\"col-xs-12\"> <label>Job Id</label> </div> <div class=\"col-xs-4\"> <input type=\"text\" name=\"PrediagnoseId\" ng-model=\"dataPrediagnose.PrediagnoseId\" class=\"form-control\"> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-md-12\"> <div class=\"col-xs-12\" style=\"padding-left: 0px\"> <label>Gejala Terdeteksi Pertama Kali</label> </div> </div> <div class=\"col-xs-4\"> <bsdatepicker name=\"FirstIndication\" date-options=\"DateOptions\" ng-model=\"dataPrediagnose.FirstIndication\"> </bsdatepicker> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Frekuensi Kejadian</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"1\"> <span>Sekali</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"2\"> <span>Kadang - Kadang</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"FrequencyIncident\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.FrequencyIncident\" value=\"3\"> <span>Selalu</span> </label> </div> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>MIL saat terjadi</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"1\"> <span>On</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"2\"> <span>Berkedip</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"Mil\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.Mil\" value=\"3\"> <span>Off</span> </label> </div> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Kondisi Kendaraan Saat Gejala Terjadi</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"1\"> <span>Idling</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"2\"> <span>Starting</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"ConditionIndication\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.ConditionIndication\" value=\"3\"> <span>Kecepatan Konstan</span> </label> </div> </div> <div class=\"col-xs-6\" ng-show=\"dataPrediagnose.ConditionIndication == 3\" style=\"padding-left: 0px\"> <!-- <input type=\"number\" name=\"Kecepatan\" style=\"width: 100%\" placeholder=\"Kecepatan\" ng-model=\"mData.Speed\"> --> <!--  ng-keypress=\"allowPattern($event,1)\" --> <textarea name=\"Kecepatan\" ng-model=\"dataPrediagnose.notes\" maxlength=\"100\" style=\"width: 100%;height: 50px\"></textarea> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Kondisi Mesin</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"1\"> <span>Panas</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"2\"> <span>Dingin</span> </label> </div> </div> <div class=\"col-xs-2\" style=\"padding-left: 0px\"> <div class=\"radio-inline\"> <label class=\"radio\"> <input type=\"radio\" name=\"MachineCondition\" class=\"radiobox style-0\" ng-model=\"dataPrediagnose.MachineCondition\" value=\"3\"> <span>Lainnya</span> </label> </div> </div> <div class=\"col-xs-6\" ng-show=\"dataPrediagnose.MachineCondition == 3\" style=\"padding-left: 0px\"> <textarea name=\"lainnya\" ng-model=\"dataPrediagnose.MachineConditionTxt\" style=\"width: 100%;height: 70px\"></textarea> </div> </div> </div> <div class=\"form-group\"> <div class=\"col-xs-12\" style=\"margin-top: 10px\"> <label>Posisi Shift Lever</label> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftP\" label=\"P\" ng-click=\"CountShiftPrediagnose()\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftN\" label=\"N\" ng-click=\"CountShiftPrediagnose()\" true-value=\"2\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftD\" label=\"D\" ng-click=\"CountShiftPrediagnose()\" true-value=\"4\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftS\" label=\"S\" ng-click=\"CountShiftPrediagnose()\" true-value=\"8\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftR\" label=\"R\" ng-click=\"CountShiftPrediagnose()\" true-value=\"16\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftOne\" label=\"1\" ng-click=\"CountShiftPrediagnose()\" true-value=\"32\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftTwo\" label=\"2\" ng-click=\"CountShiftPrediagnose()\" true-value=\"64\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftThree\" label=\"3\" ng-click=\"CountShiftPrediagnose()\" true-value=\"128\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-1\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftFour\" label=\"4\" ng-click=\"CountShiftPrediagnose()\" true-value=\"256\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-3\" style=\"padding-left: 0px\"> <bscheckbox ng-model=\"dataPrediagnose.PosisiShiftLainnya\" ng-click=\"CountShiftPrediagnose()\" label=\"Lainnya\" true-value=\"512\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-xs-12\" ng-hide=\"true\"> <input type=\"text\" name=\"PositionShiftLever\" ng-model=\"dataPrediagnose.PositionShiftLever\"> </div> </div> <div class=\"col-xs-12\" ng-show=\"dataPrediagnose.PosisiShiftLainnya == 512\"> <textarea name=\"PositionShiftLeverTxt\" ng-model=\"dataPrediagnose.PositionShiftLeverTxt\" style=\"width: 100%;height: 70px\"></textarea> </div> </div> <div class=\"col-xs-12\"> <!-- ng-click=\"CountShift()\" --> <bscheckbox ng-model=\"dataPrediagnose.IsNeedTest\" ng-change=\"testDrive(mData)\" label=\"Perlu Test Drive\" true-value=\"1\" false-value=\"0\" ng-hide=\"true\"> </bscheckbox> </div> </div> </div> </div> </bsui-modal>"
  );


  $templateCache.put('app/boards/asb/asbmaintest.html',
    "<div class=\"panel panel-default\" ng-show=\"asb.showMode=='main'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label for=\"usr\">No Polisi:</label> <input ng-model=\"asb.nopol\" type=\"text\" class=\"form-control\" id=\"usr\"> </div> <div class=\"form-group\"> <label for=\"usr\">Tipe:</label> <select ng-model=\"asb.tipe\" name=\"type\" class=\"form-control\"> <option value=\"Avanza\">Avanza</option> <option value=\"Yaris\">Yaris</option> <option value=\"Innova\">Innova</option> <option value=\"Innova Diesel\">Innova Diesel</option> <option value=\"Vios\">Vios</option> </select> </div> <div class=\"form-group\"> <label for=\"usr\">Durasi:</label> <input ng-model=\"mdata.durasi\" type=\"text\" class=\"form-control\" id=\"usr\"> </div> </div> </div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbEstimasi()\">ASB Estimasi</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbProduksi()\">ASB Produksi</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbGr()\">ASB GR</button> </div> </div> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='estimasi'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB Estimasi </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 70%; position: relative; right: 20px; top: 20px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"asb.currentDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div id=\"asb_estimasi_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.FullScreen()\" ng-show=\"false\">Full Screen</button> </div> </div> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='produksi'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB Produksi </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 15px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"asb.startDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 48px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"endDat\" date-options=\"dateOptions\" ng-model=\"asb.endDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div style=\"margin-left: 80%; position: relative; right: 20px; top: 81px; z-index: 1; margin-bottom: -30px\"> <label> <input type=\"radio\" ng-model=\"asb.BpCategory\" ng-value=\"asb.TPS\" ng-change=\"asb.reloadBoard()\">TPS&nbsp;</label> <input type=\"radio\" ng-model=\"asb.BpCategory\" ng-value=\"asb.Light\" ng-change=\"asb.reloadBoard()\">Light </div> <div id=\"asb_produksi_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> </div> </div> <!-- {{asb}} --> </div> <div class=\"panel panel-default\" ng-show=\"asb.showMode=='asbgr'\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> ASB GR </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div style=\"margin-left: 70%; position: relative; right: 20px; top: 20px; z-index: 100; margin-bottom: -30px\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"asb.currentDate\" ng-change=\"asb.reloadBoard()\"></bsdatepicker> </div> <div id=\"test_asb_asbgr_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.showAsbMain()\">Kembali</button> <button type=\"button\" class=\"btn btn-default btn-lg\" ng-click=\"asb.SaveData()\">Save Data</button> </div> </div> </div>"
  );


  $templateCache.put('app/boards/factories/showMessage.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">{{showMsg.title}}</h3></div> <div class=\"panel-body\"> <p> {{showMsg.message}}</p> <button type=\"button \" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0) \">{{showMsg.closeText}}</button> </div> </div>"
  );


  $templateCache.put('app/boards/jpcbgrview/jpcbgrview.html',
    "<div style=\"margin-left: 10px; margin-right: 10px\"> <div class=\"row\"> <button type=\"button\" class=\"btn btn-default panel-title pull-right\" ng-click=\"showOptions(true)\" style=\"margin-top: -4px; margin-left: 4px; padding-bottom: 4px;margin-bottom:10px\"> <i class=\"fa fa-fw fa-gear\"></i> </button> </div> <div class=\"row\"> <div ng-class=\"wcolumn\"> <div class=\"panel panel-default\" id=\"jpcb_gr_view_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> JPCB GR </h3> <h3 class=\"panel-title pull-right\" ng-show=\"jpcbgrview.isCurrentDate\"> &nbsp;&nbsp;{{ clock }} </h3> <div style=\"width: 200px; margin-top: -4px\" class=\"pull-right\"> <bsdatepicker name=\"currentDate\" date-options=\"dateOptions\" ng-model=\"jpcbgrview.currentDate\" ng-change=\"jpcbgrview.reloadBoard()\"></bsdatepicker> </div> <h3 class=\"panel-title text-center\" style=\"font-size: 18px; font-weight:bolder;margin:10px 0px 10px 0px\"><br><br> <!-- <i class=\"fa fa-group\" style=\"font-size:18px\"></i>&nbsp; --> <!-- <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Working Technician : {{countTechnician}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">In Progress : {{countInProgress}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Production Capacity : {{productionCapacity}} %</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">No Show : {{countNoShow}}</span> &nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Carry Over : {{countCarryOver}}</span>&nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">Booking : {{countBookingA}}/{{countBookingB}}</span> &nbsp;\r" +
    "\n" +
    "                        <span style=\"border-style:solid; border-color: #000000;padding: 1px 4px 1px 4px; margin-left:-5px\">WIP : {{countWIP}}</span> &nbsp; --> <span>Working Technician : {{countTechnician}}</span>&nbsp; <span>In Progress : {{countInProgress}}</span>&nbsp; <span>Production Capacity : {{countProductionCapacity}} %</span>&nbsp; <span>No Show : {{countNoShow}}</span> &nbsp; <span>Carry Over : {{countCarryOver}}</span>&nbsp; <span>Booking : {{countBookingA}}/{{countBookingB}}</span> &nbsp; <span>WIP : {{countWIP}}</span> &nbsp; </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"jpcb_gr_view\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> </div> </div> <div ng-class=\"ncolumn\" style=\"padding-left: 0\" ng-show=\"ncolvisible\"> <div class=\"panel panel-default\" id=\"jpcb_gr_edit_panel\"> <div class=\"panel-heading\"> <h3 class=\"panel-title pull-left\"> <!-- Select Stall --> Options </h3> <h3 class=\"panel-title pull-right\"> <a href=\"#\"><i class=\"fa fa-fw fa-remove\" ng-click=\"showOptions(false)\">&nbsp;</i></a> </h3> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\" style=\"padding: 8px\"> <div class=\"row\"> <div class=\"col-md-12\"> <h4 style=\"border-bottom: 2px solid #a5a5a5\">Hide Header / Footer</h4> <div ng-repeat=\"item in filter\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.name}}</label> </div> <hr> <h4 style=\"border-bottom: 2px solid #a5a5a5\">Select Stall</h4> <div ng-repeat=\"item in jpcbgrview.tempStallVisible\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.title}}</label> </div> </div> </div> </div> <div class=\"panel-footer\"> <div class=\"btn-group\"> <button type=\"button\" class=\"btn btn-danger btn-sm\" ng-click=\"saveOptions(filter)\">Update Board</button> </div> </div> </div> </div> </div> </div> <br>"
  );


  $templateCache.put('app/boards/jpcbgrview/options.html',
    "<div class=\"panel panel-danger\" style=\"border-color:#D53337; margin: -10px\"> <div class=\"panel-heading\" style=\"color:white; background-color:#D53337\"> <h3 class=\"panel-title\">Select Stall</h3> </div> <div class=\"panel-body\"> {{jpcbgrview}} <form method=\"post\"> <div class=\"row\"> <div class=\"col-md-12\"> <div ng-repeat=\"item in jpcbgrview.tempStallVisible\"> <input type=\"checkbox\" ng-model=\"item.visible\" ng-change=\"\"> <label>{{item.Name}}</label> </div> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <div> <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog(0)\" style=\"color:white; background-color:#D53337\">OK</button> <button type=\"button\" class=\"ngdialog-button ngdialog-button-secondary\" ng-click=\"closeThisDialog(0)\">Cancel</button> </div> </div> </div> </div> </form> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/CPPKInfo.html',
    "<div class=\"panel panel-default\"> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-xs-6 col-xm-6 col-md-6\"> <div class=\"form-group\"> <label>List Kontak Person</label> <bsselect name=\"ContactPerson\" ng-disabled=\"disableCPPK\" skip-enable ng-model=\"mDataDM.CP\" data=\"cpList\" item-text=\"Name\" item-value=\"Name\" placeholder=\"Pilih Kontak Person\" on-select=\"onChangeCP(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.CP.Name\" placeholder=\"Nama\" maxlength=\"50\"> </div> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataDM.CP.HP1\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataDM.CP.HP2\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> </div> <!-- ================= --> <div class=\"col-xs-6 col-xm-6 col-md-6\"> <div class=\"form-group\"> <label>List Pengambil Keputusan</label> <bsselect name=\"PengambilKeputusan\" ng-disabled=\"disableCPPK\" skip-enable ng-model=\"mDataDM.PK\" data=\"pkList\" item-text=\"Name\" item-value=\"Name\" placeholder=\"Pilih Penanggung Jawab\" on-select=\"onChangePK(selected)\"> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataDM.PK.Name\" placeholder=\"Nama\" maxlength=\"50\"> </div> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataDM.PK.HP1\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"numbers-only\" class=\"form-control\" ng-model=\"mDataDM.PK.HP2\" placeholder=\"No Handphone\" maxlength=\"15\" numbers-only> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/OPLNew.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Order Pekerjaan Luar</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-6 col-xs-6\"> <button class=\"ui icon inverted grey button\" style=\"font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px\" ng-click=\"deleteAllDataGridViewWork()\"><i class=\"fa fa-trash\"></i> </button> </div> <div class=\"col-md-6 col-xs-6\"> <button ng-click=\"plushOrderOpl()\" class=\"rbtn btn pull-right\" data-target=\"#ModalOP\" on-deny=\"denied\" data-toggle=\"modal\" data-backdrop=\"static\" data-keyboard=\"false\"><i class=\"fa fa-plus\"></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridWorkView\" class=\"ag-theme-balham\" style=\"height: 400px\"></div> </fieldset> <div class=\"ui modal ModalOPL\" role=\"dialog\" style=\"left:0% !important; width:75%; margin:-284px auto 0px; background-color: transparent !important\"> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h4 class=\"modal-title\">Order Pekerjaan Luar</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <label>Kode Vendor</label> <input type=\"text\" class=\"form-control\" name=\"VendorCode\" ng-model=\"mOpl.VendorCode\" ng-disabled=\"true\" skip-enable> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <bsreqlabel>Vendor</bsreqlabel> <select name=\"vendor\" convert-to-number class=\"form-control\" ng-model=\"mOpl.vendor\" data=\"OplVendor\" placeholder=\"Pilih Vendor\" required ng-disabled=\"viewMode\" skip-enable ng-change=\"chooseVendor()\"> <option ng-repeat=\"sat in OplVendor\" value=\"{{sat.VendorId}}\">{{sat.Name}}</option> </select> <bserrmsg field=\"Vendor\"></bserrmsg> </div> </div> <div class=\"col-md-12\"> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">OPL</legend> <div class=\"row\" style=\"margin-top: 20px;margin-bottom: 10px\"> <div class=\"col-md-12 col-xs-12\"> <button ng-disabled=\"viewMode\" skip-enable ng-click=\"plushOPL()\" class=\"rbtn btn pull-right\"><i class=\"fa fa-plus\" n></i>&nbsp;Tambah</button> </div> </div> <div ag-grid=\"gridDetailOPL\" class=\"ag-theme-balham\" style=\"height: 400px\" id=\"gridParts\"></div> </fieldset> </div> <!-- <label>{{OplVendor}}</label> --> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important;background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"wbtn btn ng-binding\" style=\"width:100px\" ng-click=\"closeModalOpl()\">Cancel</div> <div class=\"rbtn btn ng-binding ng-scope\" ng-disabled=\"viewMode || disableSimpan || masterOPL.length == 0\" skip-enable ng-click=\"saveFinalOpl()\" style=\"width:100px\">Simpan</div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpl.html\"><a>\r" +
    "\n" +
    "                    <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                    <span ng-bind-html=\"match.model.Name | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                    <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "                    <span ng-bind-html=\"match.model.isExternalDesc | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "                    <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "                </a></script> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/customer-01.html',
    "<div class=\"panel panel-default\"> <!-- <div class=\"panel-body\"> --> <div class=\"panel-body\"> <div id=\"rowCustDisableCompleted\" class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Pelanggan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <select ng-show=\"mode == 'search' || mode == 'form'\" class=\"form-control\" ng-model=\"action\" ng-change=\"changeOwner(selected)\">\r" +
    "\n" +
    "                        <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                        <option value=\"2\">Input Pemilik Data Baru</option>\r" +
    "\n" +
    "                    </select>\r" +
    "\n" +
    "                    <select ng-show=\"mode=='view'\" class=\"form-control\" ng-model=\"action\">\r" +
    "\n" +
    "                        <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                        <option value=\"2\">input Pemilik Data Baru</option>\r" +
    "\n" +
    "                    </select> --> <!-- <bsselect ng-show=\"mode == 'search' || mode == 'form'\" on-select=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                    </bsselect> --> <select class=\"form-control\" convert-to-number ng-show=\"mode == 'search' || mode == 'form'\" ng-change=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"act1 in actionCdata\" value=\"{{act1.Id}}\">{{act1.Value}}</option> </select> <!-- <bsselect ng-show=\"mode=='view'\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                    </bsselect> --> <select class=\"form-control\" convert-to-number ng-show=\"mode=='view'\" data=\"actionCdata\" ng-change=\"changeInfo(mFilterCust.action)\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"act2 in actionCdata\" value=\"{{act2.Id}}\">{{act2.Value}}</option> </select> </div> </div> <div ng-show=\"mode == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-disabled=\"disabledEdit || (mFilterCust.action == null || mFilterCust.action == undefined)\" skip-enable ng-show=\"editenable && mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <!-- <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-disabled=\"mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> </div> </div> </div> <div class=\"row\" ng-show=\"mode == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null || mFilterCust.action == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-3\"> <label>Toyota ID</label> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" ng-model=\"filterSearch.TID\" placeholder=\"Input Toyota ID\" disallow-space> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelected}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterField\"> <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag != 6)\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" ng-disabled=\"filterSearch.choice\" disallow-space> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag == 6)\"> <bsdatepicker date-options=\"XFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null\" ng-click=\"searchCustomer(mFilterCust.action, filterSearch)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div ng-show=\"mode == 'search'\" class=\"panel-body\"> <div class=\"row\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\" ng-show=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Pelanggan</bsreqlabel> <!-- <select class=\"form-control\" ng-model=\"mDataCrm.Customer.CustomerTypeId\" ng-change=\"changeType(mDataCrm.Customer.CustomerTypeId)\">\r" +
    "\n" +
    "                        <option value=\"3\">Individu</option>\r" +
    "\n" +
    "                        <option value=\"4\">Pemerintah</option>\r" +
    "\n" +
    "                        <option value=\"5\">Yayasan</option>\r" +
    "\n" +
    "                        <option value=\"6\">Perusahaan</option>\r" +
    "\n" +
    "                    </select> --> <select class=\"form-control\" convert-to-number ng-model=\"mDataCrm.Customer.CustomerTypeId\" ng-change=\"changeType(mDataCrm.Customer.CustomerTypeId)\"> <!-- <option value=\"3\">Individu</option>\r" +
    "\n" +
    "                        <option value=\"4\">Pemerintah</option>\r" +
    "\n" +
    "                        <option value=\"5\">Yayasan</option>\r" +
    "\n" +
    "                        <option value=\"6\">Perusahaan</option> --> <option ng-repeat=\"item in tipePemilik\" value=\"{{item.Id}}\">{{item.Name}}</option> </select> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldInst\" ng-form=\"listInst\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"Name\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\" required> <em ng-messages=\"listInst.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>Nama PIC</bsreqlabel> --> <bsreqlabel>Nama Penanggung Jawab</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"NamePic\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\" required maxlength=\"50\"> <em ng-messages=\"listInst.NamePic.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>PIC Department</bsreqlabel> --> <bsreqlabel>Department Penanggung Jawab</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" name=\"PICDepartment\" placeholder=\"PIC Department\" required> --> <select name=\"PICDepartmentId\" convert-to-number class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\" required> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> <em ng-messages=\"listInst.PICDepartmentId.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>PIC HP</bsreqlabel> --> <bsreqlabel>HP Penanggung Jawab</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\" name=\"PICHp\" maxlength=\"15\" required> </div> <em ng-messages=\"listInst.PICHp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" maxlength=\"20\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" name=\"SIUP\" style=\"text-transform: uppercase\" required> <em ng-messages=\"listInst.SIUP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <bsreqlabel>PIC Email</bsreqlabel> --> <bsreqlabel>Email Penanggung Jawab</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\" required name=\"PICEmail\"> <em ng-messages=\"listInst.PICEmail.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No. NPWP\" name=\"NpwpInst\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listInst.NpwpInst.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" on-before-cancel=\"onListCancel\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> <option ng-repeat=\"cat1 in categoryContact\" value=\"{{cat1.AddressCategoryId}}\">{{cat1.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Provinsi...\" ng-change=\"selectProvince(lmModelAddress)\" required> <option ng-repeat=\"prov in provinsiData\" value=\"{{prov.ProvinceId}}\">{{prov.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" required> <option ng-repeat=\"kab in kabupatenData\" value=\"{{kab.CityRegencyId}}\">{{kab.CityRegencyName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.CityRegencyId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" required> <option ng-repeat=\"kec in kecamatanData\" value=\"{{kec.DistrictId}}\">{{kec.DistrictName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.DistrictId\" hidden required> </div> <div class=\"form-group\"> <label>Kelurahan</label> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" required> <option ng-repeat=\"kel in kelurahanData\" value=\"{{kel.VillageId}}\">{{kel.VillageName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.VillageId\" hidden required> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                            <bsreqlabel>Alamat Utama</bsreqlabel>\r" +
    "\n" +
    "                                            <bsselect name=\"contact\" ng-model=\"lmModelAddress.MainAddress\" data=\"dataMainAddress\" item-text=\"Description\" item-value=\"Id\" placeholder=\"Pilih Informasi\" skip-disable=\"true\" on-select=\"selectMainAddress(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect>\r" +
    "\n" +
    "                                        </div> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagToyotaID(mDataCrm)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"flToyota in flagToyotaID\" ng-selected=\"mDataCrm.Customer.ToyotaIDFlag == flToyota.Id\" value=\"{{flToyota.Id}}\">{{flToyota.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <!-- <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                            </bsselect> --> <select convert-to-number name=\"FlagAfco\" class=\"form-control\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagAfco(mDataCrm)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"FlagAfco in FlagAfco\" ng-selected=\"mDataCrm.Customer.AFCOIdFlag == FlagAfco.Id\" value=\"{{FlagAfco.Id}}\">{{FlagAfco.Desc}}</option> </select> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"(mode=='form' || mode=='search')\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listInst.$invalid || CustomerAddress.length == 0\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldPersonal\" ng-form=\"listPersonal\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" placeholder=\"Gelar Depan\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <!-- style=\"text-transform: uppercase;\"  --> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama\" required name=\"Name\" maxlength=\"50\"> <!-- style=\"text-transform: uppercase;\" --> <em ng-messages=\"listPersonal.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" placeholder=\"Gelar Belakang\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Lahir</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"BirthDate\" ng-model=\"mDataCrm.Customer.BirthDate\" date-options=\"dateOptionsBirth\" max-date=\"maxDateOption.maxDate\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" required> </bsdatepicker> <!-- <bserrmsg field=\"BirthDate\"></bserrmsg> --> <em ng-messages=\"listPersonal.BirthDate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" ng-maxlength=\"14\" maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" placeholder=\"Handphone 1\" numbers-only required name=\"HandPhone1\"> <em ng-messages=\"listPersonal.HandPhone1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" ng-maxlength=\"14\" maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" placeholder=\"Handphone 2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.KTPKITAS\" minlength=\"16\" maxlength=\"16\" placeholder=\"No. KTP/KITAS\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> <!-- required --> <em ng-messages=\"listPersonal.KTPKITAS.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No. NPWP\" name=\"Npwp\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listPersonal.Npwp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-show-detail=\"onShowDetailAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" custom-button-settings-detail=\"listCustomButtonSettingsDetailAdress\" on-after-save=\"onAfterSaveAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" ng-change=\"selectPendapatan(lmModelAddress)\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> <!--  numbers-only --> </div> <div class=\"form-group\"> <label>No. Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Provinsi...\" ng-change=\"selectProvince(lmModelAddress)\" required> <option ng-repeat=\"provinsiData in provinsiData\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" required> <option ng-repeat=\"kabupatenData in kabupatenData\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.CityRegencyId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" required> <option ng-repeat=\"kecamatanData in kecamatanData\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.DistrictId\" hidden required> </div> <div class=\"form-group\"> <label>Kelurahan</label> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" required> <option ng-repeat=\"kelurahanData in kelurahanData\" value=\"{{kelurahanData.VillageId}}\" postalcode=\"{{kelurahanData.PostalCode}}\">{{kelurahanData.VillageName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.VillageId\" hidden required> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagToyotaID(mDataCrm)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"flagToyotaID in flagToyotaID\" ng-selected=\"mDataCrm.Customer.ToyotaIDFlag == flagToyotaID.Id\" value=\"{{flagToyotaID.Id}}\">{{flagToyotaID.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listPersonal.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"FlagAfco\" class=\"form-control\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagAfco(mDataCrm)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"FlagAfco in FlagAfco\" ng-selected=\"mDataCrm.Customer.AFCOIdFlag == FlagAfco.Id\" value=\"{{FlagAfco.Id}}\">{{FlagAfco.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listPersonal.$invalid || CustomerAddress.length == 0\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> </div> </div> <!-- untuk tampilan depan --> <div class=\"panel-body\" ng-hide=\"mode =='search'\"> <div class=\"row\" ng-show=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <!-- <input type=\"text\" ng-readonly='true' class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" placeholder=\"PIC Department\"> --> <select name=\"picDepartment\" convert-to-number ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartmentId\" placeholder=\"Pilih PIC Department\" icon=\"fa fa-user\"> <option ng-repeat=\"item in ListPicDepartment\" value=\"{{item.CustomerPositionId}}\">{{item.CustomerPositionName}}</option> </select> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" maxlength=\"15\" placeholder=\"PIC HP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" style=\"text-transform: uppercase\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\"> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" ng-change=\"selectPendapatan(lmModelAddress)\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> </div> <div class=\"form-group\"> <label>No. Telepon 2 </label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Provinsi...\" ng-change=\"selectProvince(lmModelAddress)\" required> <option ng-repeat=\"provinsiData in provinsiData\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceData}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kabupaten\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.CityRegencyName\" required> <!-- <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kabupatenData in kabupatenData\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kecamatan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.DistrictName\" required> <!-- <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kecamatanData in kecamatanData\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <label>Kelurahan</label> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kelurahan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.VillageName\" required> <!-- <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kelurahanData in kelurahanData\" value=\"{{kelurahanData.VillageId}}\">{{kelurahanData.VillageName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagToyotaID(mDataCrm)\" icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"flagToyotaID in flagToyotaID\" value=\"{{flagToyotaID.Id}}\">{{flagToyotaID.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <!-- <bsselect name=\"FlagAfco\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\"\r" +
    "\n" +
    "                                required=\"true\">\r" +
    "\n" +
    "                            </bsselect> --> <select convert-to-number name=\"FlagAfco\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagAfco(mDataCrm)\" icon=\"fa fa-taxi\" required> <option ng-repeat=\"FlagAfco in FlagAfco\" value=\"{{FlagAfco.Id}}\">{{FlagAfco.Desc}}</option> </select> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> </div> <div class=\"row\" ng-hide=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" ng-readonly=\"true\" placeholder=\"Gelar Depan\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" ng-readonly=\"true\" placeholder=\"Nama\"> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" ng-readonly=\"true\" placeholder=\"Gelar Belakang\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" ng-readonly=\"true\" numbers-only> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" ng-readonly=\"true\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.KTPKITAS\" ng-readonly=\"true\" minlength=\"16\" maxlength=\"16\" placeholder=\"No. KTP/KITAS\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" ng-readonly=\"true\" placeholder=\"No. NPWP\" name=\"Npwp\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address1\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\" ng-disabled=\"true\" skip-enabled> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <!-- <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"contact\" class=\"form-control\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" ng-change=\"selectPendapatan(lmModelAddress)\" required> <option ng-repeat=\"categoryContact in categoryContact\" value=\"{{categoryContact.AddressCategoryId}}\">{{categoryContact.AddressCategoryDesc}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.AddressCategoryId\" hidden required> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RT</bsreqlabel> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> <div class=\"col-md-6 col-xs-6\"> <bsreqlabel>RW</bsreqlabel> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" required> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <!-- <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <select convert-to-number name=\"propinsi\" class=\"form-control\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" placeholder=\"Pilih Provinsi...\" ng-change=\"selectProvince(lmModelAddress)\" required> <option ng-repeat=\"provinsiData in provinsiData\" value=\"{{provinsiData.ProvinceId}}\">{{provinsiData.ProvinceName}}</option> </select> <input type=\"text\" ng-model=\"lmModelAddress.ProvinceId\" hidden required> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <!-- <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kabupaten\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.CityRegencyName\" required> <!-- <select convert-to-number name=\"kabupaten\" class=\"form-control\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" ng-change=\"selectRegency(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kabupatenData in kabupatenData\" value=\"{{kabupatenData.CityRegencyId}}\">{{kabupatenData.CityRegencyName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <!-- <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kecamatan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.DistrictName\" required> <!-- <select convert-to-number name=\"kecamatan\" class=\"form-control\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" ng-change=\"selectDistrict(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kecamatanData in kecamatanData\" value=\"{{kecamatanData.DistrictId}}\">{{kecamatanData.DistrictName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <label>Kelurahan</label> <!-- <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\"\r" +
    "\n" +
    "                                                required=true>\r" +
    "\n" +
    "                                            </bsselect> --> <input type=\"text\" name=\"kelurahan\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.VillageName\" required> <!-- <select convert-to-number name=\"kelurahan\" class=\"form-control\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" ng-change=\"selectVillage(lmModelAddress)\" required=true>\r" +
    "\n" +
    "                                                <option ng-repeat=\"kelurahanData in kelurahanData\" value=\"{{kelurahanData.VillageId}}\">{{kelurahanData.VillageName}}</option>\r" +
    "\n" +
    "                                            </select> --> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"flagToyotaID\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagToyotaID(mDataCrm)\"\r" +
    "\n" +
    "                                icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"flagToyotaID in flagToyotaID\" value=\"{{flagToyotaID.Id}}\">{{flagToyotaID.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect convert-to-number name=\"FlagAfco\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <!-- <select name=\"FlagAfco\" class=\"form-control\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" ng-change=\"selectFlagAfco(mDataCrm)\" icon=\"fa fa-taxi\" required=\"true\">\r" +
    "\n" +
    "                                <option ng-repeat=\"FlagAfco in FlagAfco\" value=\"{{FlagAfco.Id}}\">{{FlagAfco.Desc}}</option>\r" +
    "\n" +
    "                            </select> --> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> </div> </div> <hr width=\"95%\" style=\"height:3px;background-color:grey\"> <div class=\"panel-body\"> <div id=\"rowVehicDisableCompleted\" class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Kendaraan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <bsselect ng-show=\"modeV == 'search' || modeV == 'form'\" on-select=\"changeVehic(mFilterVehic.actionV)\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                    </bsselect> --> <select class=\"form-control\" convert-to-number ng-show=\"modeV == 'search' || modeV == 'form'\" ng-change=\"changeVehic(mFilterVehic.actionV)\" data=\"actionVdata\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"actionVdata in actionVdata\" value=\"{{actionVdata.Id}}\">{{actionVdata.Value}}</option> </select> <!-- <bsselect ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\">\r" +
    "\n" +
    "                    </bsselect>                                         --> <select class=\"form-control\" convert-to-number ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-change=\"changeKendaraan(mFilterVehic.actionV)\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> <option ng-repeat=\"actionVdata in actionVdata\" value=\"{{actionVdata.Id}}\">{{actionVdata.Value}}</option> </select> </div> </div> <div ng-show=\"modeV == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" ng-show=\"editenable && modeV=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" skip-enable ng-disabled=\"disabledEditV || (mFilterVehic.actionV == null || mFilterVehic.actionV == undefined) \" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> </div> <div class=\"row\" ng-show=\"modeV == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null || mFilterVehic.actionV == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelectedV}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterFieldV\"> <a href=\"#\" ng-click=\"filterFieldChangeV(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearchV.filterValue\" ng-keypress=\"allowPatternFilterSrc($event,mFilterVehic.actionV,filterSearchV)\" ng-disabled=\"filterSearchV.choice\" disallow-space> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null\" ng-click=\"searchVehicle(mFilterVehic.actionV, filterSearchV)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div class=\"panel-body\" ng-form=\"listVehicle\" ng-hide=\"!hideVehic\" novalidate> <div class=\"col-md-12\" ng-show=\"!disVehic\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tipe Kendaraan</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changeTypeVehic1\" ng-change=\"checkTam(mFilterTAM.isTAM)\" ng-disabled=\"disableCheckTam\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"1\" checked> <label for=\"changeTypeVehic1\">TAM</label> <input ng-change=\"checkTam(mFilterTAM.isTAM)\" type=\"radio\" id=\"changeTypeVehic2\" ng-disabled=\"disableCheckTam\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"0\"> <label for=\"changeTypeVehic2\">Non TAM</label> </div> </div> </div> </div> <div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Polisi</bsreqlabel> <input name=\"noPolice\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-model=\"mDataCrm.Vehicle.LicensePlate\" ng-maxlength=\"10\" ng-keypress=\"allowPatternFilter($event,3,filter.noPolice)\" placeholder=\"No Polisi\" required> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No Rangka</bsreqlabel> <input name=\"VIN\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)) || mData.Status >= 4\" skip-enable ng-model=\"mDataCrm.Vehicle.VIN\" placeholder=\"No Rangka\" required ng-maxlength=\"17\" ng-minlength=\"12\" maxlength=\"17\"> <em ng-messages=\"listVehicle.VIN.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                        <div class=\"form-group\">\r" +
    "\n" +
    "                            <bsreqlabel>Model Code</bsreqlabel>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ModelCode\" placeholder=\"Model Code\">\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div> --> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <!-- <bsselect data=\"VehicM\" name=\"vehicModel\" on-select=\"selectedModel(selected)\" item-value=\"VehicleModelId\" item-text=\"VehicleModelName\" ng-model=\"mDataCrm.Vehicle.Model\" placeholder=\"Vehicle Model\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </bsselect> --> <ui-select name=\"vehicModel\" ng-if=\"mFilterTAM.isTAM == 1 && mDataCrm.Vehicle.SourceDataId !== null \" ng-model=\"mDataCrm.Vehicle.Model\" data=\"VehicM\" on-select=\"selectedModel($item)\" ng-required=\"true\" search-enabled=\"false\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Model\"> {{$select.selected.VehicleModelName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicM\"> <span ng-bind-html=\"item.VehicleModelName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelName\" maxlength=\"18\" required name=\"vehicModel\" placeholder=\"Model Type\"> <input type=\"text\" ng-disabled=\"disVehic || mDataCrm.Vehicle.SourceDataId == null || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 1 && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.Model.VehicleModelName\" required name=\"vehicModel\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <!--<bsselect name=\"vehicType\" data=\"VehicT\" on-select=\"selectedType(selected)\" item-value=\"VehicleTypeId\" item-text=\"NewDescription\" ng-model=\"mDataCrm.Vehicle.Type\" placeholder=\"Vehicle Type\" ng-readonly=\"enableType\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </bsselect> --> <ui-select data=\"VehicT\" name=\"vehicType\" ng-if=\"mFilterTAM.isTAM == 1 && mDataCrm.Vehicle.SourceDataId !== null\" ng-model=\"mDataCrm.Vehicle.Type\" on-select=\"selectedType($item)\" ng-required=\"true\" ng-disabled=\"disVehic || mData.Status >= 4\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.NewDescription}} </ui-select-match> <ui-select-choices repeat=\"item in VehicT\"> <span ng-bind-html=\"item.NewDescription\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelType\" required name=\"vehicType\" placeholder=\"Model Type\"> <input type=\"text\" ng-disabled=\"disVehic || mDataCrm.Vehicle.SourceDataId == null || mData.Status >= 4\" skip-enable ng-if=\"mFilterTAM.isTAM == 1 && (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.Type.NewDescription\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicType.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-readonly=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Warna\"> --> <!-- <bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"ColorName\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </bsselect> --> <ui-select name=\"colorMobil\" ng-model=\"mDataCrm.Vehicle.Color\" data=\"VehicColor\" on-select=\"selectedColor($item.MColor)\" ng-if=\"mFilterTAM.isTAM == 1\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Warna\"> {{$select.selected.ColorName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicColor\"> <span ng-bind-html=\"item.MColor.ColorName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ColorName\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\" ng-show=\"isCustomColor\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna Lain</bsreqlabel> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable class=\"form-control\" ng-model=\"mData.VehicleColor\" placeholder=\"Warna\"> <!--<bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"VehicleColor\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </bsselect> --> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <!-- <div class=\"col-md-6\" ng-if=\"mFilterTAM.isTAM == 1\"> --> <div class=\"form-group\" show-errors> <bsreqlabel ng-if=\"mFilterTAM.isTAM == 1\">Kode Warna</bsreqlabel> <label ng-if=\"mFilterTAM.isTAM == 0\">Kode Warna</label> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCode\" placeholder=\"Kode Warna\" required> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCodeNonTAM\" placeholder=\"Kode Warna\"> <em ng-messages=\"listVehicle.colorCode.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bahan Bakar</bsreqlabel> <bsselect data=\"VehicGas\" on-select=\"selectedGas(selected)\" item-value=\"VehicleGasTypeId\" name=\"fuel\" item-text=\"VehicleGasTypeName\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required> </bsselect> <!-- <select class=\"form-control\" data=\"VehicGas\" on-select=\"selectedGas(selected)\" item-value=\"VehicleGasTypeId\" name=\"fuel\" item-text=\"VehicleGasTypeName\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                        </select>     --> <!-- <ui-select name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" on-select=\"selectedGas($item)\" item-text=\"VehicleGasTypeName\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\">\r" +
    "\n" +
    "                            <ui-select-match allow-clear placeholder=\"Vehicle Gas Type\">\r" +
    "\n" +
    "                                {{$select.selected.VehicleGasTypeName}}\r" +
    "\n" +
    "                            </ui-select-match>\r" +
    "\n" +
    "                            <ui-select-choices repeat=\"item in VehicGas\">\r" +
    "\n" +
    "                                <span ng-bind-html=\"item.VehicleGasTypeName\"></span>\r" +
    "\n" +
    "                            </ui-select-choices>\r" +
    "\n" +
    "                        </ui-select> --> <!-- <select class=\"form-control\" name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" on-select=\"selectedGas(selected)\" item-text=\"VehicleGasTypeName\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable>\r" +
    "\n" +
    "                            <option ng-repeat=\"item in VehicGas\" value=\"{{item.VehicleGasTypeId}}\">{{item.VehicleGasTypeName}}</option>\r" +
    "\n" +
    "                        </select> --> <em ng-messages=\"listVehicle.fuel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tahun Rakit</bsreqlabel> <input name=\"tahunRakit\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable numbers-only ng-model=\"mDataCrm.Vehicle.AssemblyYear\" placeholder=\"Tahun Rakit\" required> <em ng-messages=\"listVehicle.tahunRakit.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Full Model</bsreqlabel> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCode\" placeholder=\"Katashiki Code\" required> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" ng-disabled=\"disVehic\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCodeNonTAM\" placeholder=\"Katashiki Code\" required> <em ng-messages=\"listVehicle.fullModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Mesin</bsreqlabel> <input type=\"text\" maxlength=\"20\" name=\"NoMesin\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable ng-model=\"mDataCrm.Vehicle.EngineNo\" placeholder=\"No. Mesin\" required> <em ng-messages=\"listVehicle.NoMesin.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Asuransi</label> <input type=\"text\" name=\"Asuransi\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Insurance\" placeholder=\"Asuransi\"> <em ng-messages=\"listVehicle.Asuransi.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Polis Asuransi</label> <input type=\"text\" name=\"noSPK\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic || (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null)\" skip-enable ng-model=\"mDataCrm.Vehicle.SPK\" placeholder=\"No. Polis Asuransi\"> <em ng-messages=\"listVehicle.noSPK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal DEC</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!--<input type=\"text\" class=\"form-control\"  ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal DEC\">--> <!-- {{disVehic}} (mDataCrm.Vehicle.SourceDataId !== undefined && mDataCrm.Vehicle.SourceDataId == null) --> <bsdatepicker name=\"DateDEC\" ng-model=\"mDataCrm.Vehicle.DECDate\" ng-if=\"disVehic == false\" date-options=\"dateOptionsBirth\" max-date=\"maxDateS\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" ng-disabled=\"disVehic\" skip-enable required> </bsdatepicker> <input name=\"DateDEC\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal Lahir\" required> <!--\r" +
    "\n" +
    "                        <bsdatepicker name=\"DateDEC\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" max-date='maxDateOptionDECLC.maxDate' date-options=\"dateDECOptions\" ng-change=\"dateChange(dt)\" required>\r" +
    "\n" +
    "                        </bsdatepicker> --> <em ng-messages=\"listVehicle.DateDEC.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama STNK</bsreqlabel> <input type=\"text\" name=\"nameStnk\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKName\" placeholder=\"Nama STNK\" required> <em ng-messages=\"listVehicle.nameStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat STNK</bsreqlabel> <input name=\"addressStnk\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKAddress\" placeholder=\"Alamat STNK\" required> <em ng-messages=\"listVehicle.addressStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Masa Berlaku STNK</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!-- Enable for SALES --> <bsdatepicker name=\"dateSTNK\" ng-disabled=\"disVehic\" ng-if=\"disVehic == false\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" date-options=\"dateDECOptions\" placeholder=\"Masa Berlaku STNK\" required> </bsdatepicker> <input name=\"dateSTNK\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" placeholder=\"Masa Berlaku STNK\" required> <em ng-messages=\"listVehicle.dateSTNK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"modeV=='form' || modeV=='search'\" ng-click=\"saveVehic(mFilterVehic.actionV, mDataCrm, mFilterTAM.isTAM);\" ng-disabled=\"listVehicle.$invalid\" skip-enable class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <button type=\"button\" ng-click=\"cancelSearchV()\" ng-show=\"modeV=='form' || modeV=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> <!-- </div> --> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/customer-02.html',
    "<div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPnj()\" ng-hide=\"!disPnj\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"savePnj()\" ng-show=\"!disPnj\" class=\"rbtn btn\">Save</button> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Lengkap</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" ng-readonly=\"disPnj\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" ng-readonly=\"disPnj\" numbers-only> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/customer-03.html',
    "<div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPng(mData.US)\" ng-hide=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelPng(mData.US)\" ng-show=\"!disPng\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"savePng(mData.US, mDataCrm, CustomerVehicleId)\" ng-show=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" ng-show=\"!disPng\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-show=\"!hiddenUserList\"> <label>List Pengguna</label> <!-- <bsselect ng-model=\"mData.US\" data=\"pngList\" item-text=\"name\" item-value=\"VehicleUserId\" placeholder=\"Pilih Pengguna\" on-select=\"onChangePng(selected)\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" ng-model=\"mData.US\" data=\"pngList\" placeholder=\"Pilih Pengguna\" ng-change=\"onChangePng(mData.US)\"> <option ng-repeat=\"item in pngList\" ng-selected=\"mData.US.VehicleUserId == item.VehicleUserId\" value=\"{{item.VehicleUserId}}\">{{item.name}}</option> </select> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Lengkap</label> <input type=\"text\" name=\"nameLengkap\" class=\"form-control\" ng-model=\"mData.US.name\" ng-readonly=\"disPng\"> <input type=\"text\" class=\"form-control\" ng-model=\"mData.US.VehicleUserId\" ng-hide=\"true\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 1</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" name=\"HP1\" numbers-only ng-maxlength=\"15\" maxlength=\"15\" class=\"form-control\" ng-model=\"mData.US.phoneNumber1\" ng-readonly=\"disPng\"> <!--  numbers-only --> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" name=\"HP2\" numbers-only ng-maxlength=\"15\" maxlength=\"15\" class=\"form-control\" ng-model=\"mData.US.phoneNumber2\" ng-readonly=\"disPng\"> <!-- numbers-only --> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Hubungan dengan Pemilik</label> <!-- <bsselect data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" item-value=\"RelationId\" item-text=\"RelationDesc\"\r" +
    "\n" +
    "                    ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable>\r" +
    "\n" +
    "                </bsselect> --> <select class=\"form-control\" id=\"RelationshipID\" data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable> <option ng-repeat=\"dataRelationship in dataRelationship\" ng-selected=\"mData.US.Relationship == dataRelationship.RelationId\" value=\"{{dataRelationship.RelationId}}\">{{dataRelationship.RelationDesc}}</option> </select> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/customerIndex.html',
    "<div class=\"col-md-12\"> <div class=\"row\"> <uib-accordion close-others=\"oneAtATime\"> <form> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Pelanggan & Kendaraan </uib-accordion-heading> <div class=\"panel panel-default\"> <!-- <div class=\"panel-body\"> --> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Pelanggan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <!-- <select ng-show=\"mode == 'search' || mode == 'form'\" class=\"form-control\" ng-model=\"action\" ng-change=\"changeOwner(selected)\">\r" +
    "\n" +
    "                                            <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                                            <option value=\"2\">Input Pemilik Data Baru</option>\r" +
    "\n" +
    "                                        </select>\r" +
    "\n" +
    "                                        <select ng-show=\"mode=='view'\" class=\"form-control\" ng-model=\"action\">\r" +
    "\n" +
    "                                            <option value=\"1\">Input Pemilik Toyota ID</option>\r" +
    "\n" +
    "                                            <option value=\"2\">input Pemilik Data Baru</option>\r" +
    "\n" +
    "                                        </select> --> <bsselect ng-show=\"mode == 'search' || mode == 'form'\" on-select=\"changeOwner(mFilterCust.action)\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> </bsselect> <bsselect ng-show=\"mode=='view'\" data=\"actionCdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterCust.action\" placeholder=\"Tipe Informasi\"> </bsselect> </div> </div> <div ng-show=\"mode == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-show=\"editenable && mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> <button type=\"button\" ng-click=\"editcus(mFilterCust.action,mDataCrm.Customer.CustomerTypeId)\" ng-disabled=\"mode=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> <div class=\"row\" ng-show=\"mode == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null || mFilterCust.action == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-3\"> <label>Toyota ID</label> </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" ng-model=\"filterSearch.TID\" placeholder=\"Input Toyota ID\" disallow-space> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelected}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterField\"> <a href=\"#\" ng-click=\"filterFieldChange(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag != 6)\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearch.filterValue\" ng-maxlength=\"15\" ng-disabled=\"filterSearch.choice\" disallow-space> </div> <div class=\"col-md-6\" ng-if=\"(filterSearch.flag == 6)\"> <bsdatepicker date-options=\"XFilter\" ng-model=\"filterSearch.filterValue\"></bsdatepicker> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == null\" ng-click=\"searchCustomer(mFilterCust.action, filterSearch)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div ng-show=\"mode == 'search'\" class=\"panel-body\"> <div class=\"row\" ng-hide=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\" ng-show=\"mFilterCust.action == 2 || mFilterCust.action == 3\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Pelanggan</bsreqlabel> <select class=\"form-control\" ng-model=\"mDataCrm.Customer.CustomerTypeId\" ng-change=\"changeType(mDataCrm.Customer.CustomerTypeId)\"> <option value=\"3\">Individu</option> <option value=\"4\">Pemerintah</option> <option value=\"5\">Yayasan</option> <option value=\"6\">Perusahaan</option> </select> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldInst\" ng-form=\"listInst\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"Name\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\" required> <em ng-messages=\"listInst.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"NamePic\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\" required> <em ng-messages=\"listInst.NamePic.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" name=\"PICDepartment\" placeholder=\"PIC Department\" required> <em ng-messages=\"listInst.PICDepartment.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\" name=\"PICHp\" required> </div> <em ng-messages=\"listInst.PICHp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\" name=\"SIUP\"> <em ng-messages=\"listInst.SIUP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\" required name=\"PICEmail\"> <em ng-messages=\"listInst.PICEmail.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" on-before-cancel=\"onListCancel\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone1\" numbers-only> </div> <div class=\"form-group\"> <label>No Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone2\" numbers-only> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" numbers-only> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" numbers-only> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                                                                <bsreqlabel>Alamat Utama</bsreqlabel>\r" +
    "\n" +
    "                                                                <bsselect name=\"contact\" ng-model=\"lmModelAddress.MainAddress\" data=\"dataMainAddress\" item-text=\"Description\" item-value=\"Id\" placeholder=\"Pilih Informasi\" skip-disable=\"true\" on-select=\"selectMainAddress(selected)\"\r" +
    "\n" +
    "                                                                    required=true>\r" +
    "\n" +
    "                                                                </bsselect>\r" +
    "\n" +
    "                                                            </div> --> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listInst.$invalid\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> <div class=\"row\" ng-show=\"showFieldPersonal\" ng-form=\"listPersonal\" novalidate> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" placeholder=\"Gelar Depan\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <!-- style=\"text-transform: uppercase;\"  --> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama\" required name=\"Name\"> <!-- style=\"text-transform: uppercase;\" --> <em ng-messages=\"listPersonal.Name.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" placeholder=\"Gelar Belakang\"> <!--  style=\"text-transform: uppercase;\" --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Lahir</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"BirthDate\" ng-model=\"mDataCrm.Customer.BirthDate\" date-options=\"dateOptionsBirth\" max-date=\"maxDateOption.maxDate\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" required> </bsdatepicker> <!-- <bserrmsg field=\"BirthDate\"></bserrmsg> --> <em ng-messages=\"listPersonal.BirthDate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" placeholder=\"Handphone 1\" numbers-only required name=\"HandPhone1\"> <em ng-messages=\"listPersonal.HandPhone1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" placeholder=\"Handphone 2\" numbers-only> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. KTP/KITAS</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.KTPKITAS\" minlength=\"16\" maxlength=\"16\" placeholder=\"No. KTP/KITAS\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" name=\"KTPKITAS\" ng-change=\"checkKTP(mDataCrm.Customer.KTPKITAS)\" style=\"text-transform: uppercase\"> <!-- required --> <em ng-messages=\"listPersonal.KTPKITAS.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. NPWP</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Npwp\" placeholder=\"No. NPWP\" name=\"Npwp\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\"> <em ng-messages=\"listPersonal.Npwp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <!-- <p ng-message=\"required\">Wajib diisi</p> --> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-show-detail=\"onShowDetailAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listButtonSettingsAddress\" custom-button-settings-detail=\"listCustomButtonSettingsDetailAdress\" on-after-save=\"onAfterSaveAddress\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone1\" numbers-only> <!--  numbers-only --> </div> <div class=\"form-group\"> <label>No. Telepon 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" maxlength=\"15\" ng-model=\"lmModelAddress.Phone2\" numbers-only> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" numbers-only> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" numbers-only> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listPersonal.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\" ng-hide=\"hideToyAfco\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(mFilterCust.action, mDataCrm, CustomerAddress, mode)\" ng-disabled=\"listPersonal.$invalid\" skip-enable><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <!--  <button type=\"button\" ng-show=\"mode=='form' || mode=='search'\" class=\"rbtn btn\" ng-click=\"saveCustomerCrm(action, mDataCrm, CustomerAddress); mode='view'\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> --> <button type=\"button\" ng-click=\"batalEditcus();\" ng-show=\"mode=='form' || mode=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> </div> </div> </div> <!-- untuk tampilan depan --> <div class=\"panel-body\" ng-hide=\"mode =='search'\"> <div class=\"row\" ng-show=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">Nama Institusi</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">Nama Yayasan</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" placeholder=\"Nama Institusi\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama PIC</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" placeholder=\"Nama PIC\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Department</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICDepartment\" placeholder=\"PIC Department\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC HP</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" placeholder=\"PIC HP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId != 5 || mDataCrm.Customer.CustomerTypeId != null\">SIUP</bsreqlabel> <bsreqlabel ng-if=\"mDataCrm.Customer.CustomerTypeId == 5\">No. Izin Pendiriran</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.SIUP\" placeholder=\"SIUP\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>PIC Email</bsreqlabel> <input type=\"text\" ng-readonly=\"true\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICEmail\" placeholder=\"PIC Email\"> </div> </div> </div> <!--bslist alamat--> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\"> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No. Telepon 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" numbers-only> </div> <div class=\"form-group\"> <label>No. Telepon 2 </label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" numbers-only> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" numbers-only> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" numbers-only> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> </div> <div class=\"row\" ng-hide=\"showInst\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Toyota ID</bsreqlabel> <h1 style=\"color: red\">{{mDataCrm.Customer.ToyotaId}}</h1> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Depan</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.FrontTitle\" ng-readonly=\"true\" placeholder=\"Gelar Depan\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.Name\" ng-readonly=\"true\" placeholder=\"Nama\"> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Gelar Belakang</label> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.EndTitle\" ng-readonly=\"true\" placeholder=\"Gelar Belakang\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone1\" ng-readonly=\"true\" numbers-only> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mDataCrm.Customer.HandPhone2\" ng-readonly=\"true\" numbers-only> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <bslist name=\"address1\" data=\"CustomerAddress\" m-id=\"CustomerAddressId\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" list-model=\"lmModelAddress\" list-title=\"Alamat Pelanggan\" on-before-save=\"onBeforeSaveAddress\" list-api=\"listApiAddress\" on-before-edit=\"onBeforeEditAddress\" on-before-delete=\"onBeforeDeleteAddress\" modal-title=\"Alamat Pelanggan\" button-settings=\"listView\" custom-button-settings=\"listView\" list-height=\"200\" ng-disabled=\"true\" skip-enabled> <bslist-template> <p class=\"name\"><strong>{{ lmItem.AddressCategory.AddressCategoryDesc }}</strong> <i class=\"glyphicon glyphicon-home\" ng-show=\"lmItem.MainAddress\" style=\"color:red\"></i> <label ng-show=\"lmItem.MainAddress == 1\" style=\"color:red\">(UTAMA)</label></p> <p>{{ lmItem.Address }} ,RT : {{ lmItem.RT }}/RW : {{ lmItem.RW }}</p> <p>Kelurahan : {{ lmItem.VillageData.VillageName }} ,Kecamatan : {{ lmItem.DistrictData.DistrictName }} ,Kota/Kabupaten : {{ lmItem.CityRegencyData.CityRegencyName }}</p> <p>Provinsi : {{ lmItem.ProvinceData.ProvinceName }} , <i class=\"glyphicon glyphicon-envelope\"></i> {{ lmItem.VillageData.PostalCode }}, <i class=\"glyphicon glyphicon-phone-alt\"></i>({{lmItem.Phone1}}) -- <i class=\"glyphicon glyphicon-phone-alt\"></i> ({{ lmItem.Phone2 }})</p> </bslist-template> <bslist-modal-form> <div class=\"row\"> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Kategori Kontak</bsreqlabel> <bsselect name=\"contact\" ng-model=\"lmModelAddress.AddressCategoryId\" data=\"categoryContact\" item-text=\"AddressCategoryDesc\" item-value=\"AddressCategoryId\" placeholder=\"Pilih Kategory Kontak...\" skip-disable=\"true\" on-select=\"selectPendapatan(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>No Telephone 1</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone1\" numbers-only maxlength=\"15\"> </div> <div class=\"form-group\"> <label>No Telephone 2</label> <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"lmModelAddress.Phone2\" numbers-only maxlength=\"15\"> </div> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <input type=\"text\" name=\"add\" class=\"form-control\" ng-model=\"lmModelAddress.Address\" required> </div> <div class=\"form-group\"> <div class=\"col-md-6 col-xs-6\"> <label>RT</label> <input type=\"text\" name=\"rt\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RT\" maxlength=\"3\" numbers-only> </div> <div class=\"col-md-6 col-xs-6\"> <label>RW</label> <input type=\"text\" name=\"rw\" class=\"form-control\" skip-disable=\"true\" ng-model=\"lmModelAddress.RW\" maxlength=\"3\" numbers-only> </div> </div> </div> <div class=\"col-md-6 col-xs-6\" style=\"margin-bottom:95px\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect name=\"propinsi\" ng-model=\"lmModelAddress.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kabupaten/Kota</bsreqlabel> <bsselect name=\"kabupaten\" ng-model=\"lmModelAddress.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"lmModelAddress.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Kecamatan</bsreqlabel> <bsselect name=\"kecamatan\" ng-model=\"lmModelAddress.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"lmModelAddress.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect name=\"kelurahan\" ng-model=\"lmModelAddress.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"lmModelAddress.DistrictId == undefined\" on-select=\"selectVillage(selected)\" required> </bsselect> </div> <div class=\"form-group\"> <label style=\"margin-top: 5px\">Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"lmModelAddress.PostalCode\" disabled> </div> </div> </div> </bslist-modal-form> </bslist> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>Buat Toyota ID ?</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"flagToyotaID\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.ToyotaIDFlag\" data=\"flagToyotaID\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagToyotaID(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.flagToyotaID.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"form-group\"> <div class=\"col-md-2\" style=\"height:30px; line-height:30px\"> <bsreqlabel>AFCO</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect name=\"FlagAfco\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Customer.AFCOIdFlag\" data=\"FlagAfco\" item-text=\"Desc\" item-value=\"Id\" placeholder=\"Pilih Ya / Tidak\" skip-disable=\"true\" on-select=\"selectFlagAfco(selected)\" icon=\"fa fa-taxi\" required> </bsselect> <em ng-messages=\"listInst.FlagAfco.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> </div> </div> <hr width=\"95%\" style=\"height:3px;background-color:grey\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-4\"> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <label>Informasi Kendaraan</label> </div> <div class=\"col-md-12\" style=\"padding-left: 0px\"> <bsselect ng-show=\"modeV == 'search' || modeV == 'form'\" on-select=\"changeVehic(mFilterVehic.actionV)\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> </bsselect> <bsselect ng-show=\"modeV == 'view'\" data=\"actionVdata\" item-value=\"Id\" item-text=\"Value\" ng-model=\"mFilterVehic.actionV\" placeholder=\"Tipe Informasi\"> </bsselect> </div> </div> <div ng-show=\"modeV == 'search'\" class=\"col-md-4\"> </div> <div class=\"col-md-8\"> <div class=\"btn-group pull-right\"> <!-- <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" ng-show=\"editenable && modeV=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> --> <button type=\"button\" ng-click=\"editVehic(mFilterVehic.actionV)\" ng-disabled=\"modeV=='view'\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> </div> </div> </div> </div> <div class=\"row\" ng-show=\"modeV == 'search'\"> <div class=\"col-md-12\"> <div class=\"panel panel-default\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null || mFilterVehic.actionV == 3\"> <div class=\"panel-body\"> <div class=\"row\" style=\"margin-bottom:20px\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <div class=\"bslabel-horz\" uib-dropdown style=\"position:relative!important\"> <a href=\"#\" style=\"color:#444\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\">{{filterFieldSelectedV}}&nbsp; <span class=\"caret\"></span> <span style=\"color:#b94a48\">&nbsp;*</span> </a> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\" ng-repeat=\"item in filterFieldV\"> <a href=\"#\" ng-click=\"filterFieldChangeV(item)\">{{item.desc}}</a> </li> </ul> </div> </div> <div class=\"col-md-6\"> <input style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" placeholder=\"\" ng-model=\"filterSearchV.filterValue\" ng-keypress=\"allowPatternFilterSrc($event,mFilterVehic.actionV,filterSearchV)\" ng-disabled=\"filterSearchV.choice\" ng-maxlength=\"17\" disallow-space> </div> <div class=\"col-md-3\"> <button class=\"rbtn btn\" style=\"display:inline\" ng-hide=\"mFilterVehic.actionV == 2 || mFilterVehic.actionV == null\" ng-click=\"searchVehicle(mFilterVehic.actionV, filterSearchV)\"><i class=\"fa fa-fw fa-search\"></i>&nbsp;Search</button> </div> </div> </div> </div> </div> </div> </div> <div class=\"panel-body\" ng-form=\"listVehicle\" ng-hide=\"!hideVehic\" novalidate> <div class=\"col-md-12\" ng-show=\"!disVehic\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tipe Kendaraan</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changeTypeVehic1\" ng-change=\"checkTam(mFilterTAM.isTAM)\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"1\" checked> <label for=\"changeTypeVehic1\">TAM</label> <input ng-change=\"checkTam(mFilterTAM.isTAM)\" type=\"radio\" id=\"changeTypeVehic2\" ng-model=\"mFilterTAM.isTAM\" name=\"radio4\" value=\"0\"> <label for=\"changeTypeVehic2\">Non TAM</label> </div> </div> </div> </div> <div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Polisi</bsreqlabel> <input name=\"noPolice\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.LicensePlate\" ng-maxlength=\"10\" ng-keypress=\"allowPatternFilter($event,3,filter.noPolice)\" placeholder=\"No Polisi\" required> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No Rangka</bsreqlabel> <input name=\"VIN\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.VIN\" placeholder=\"No Rangka\" required ng-maxlength=\"17\" ng-minlength=\"8\" maxlength=\"20\"> <em ng-messages=\"listVehicle.VIN.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                                            <div class=\"form-group\">\r" +
    "\n" +
    "                                                <bsreqlabel>Model Code</bsreqlabel>\r" +
    "\n" +
    "                                                <input type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.ModelCode\" placeholder=\"Model Code\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div> --> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <!-- <bsselect data=\"VehicM\" name=\"vehicModel\" on-select=\"selectedModel(selected)\" item-value=\"VehicleModelId\" item-text=\"VehicleModelName\" ng-model=\"mDataCrm.Vehicle.Model\" placeholder=\"Vehicle Model\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select name=\"vehicModel\" ng-if=\"mFilterTAM.isTAM == 1\" ng-model=\"mDataCrm.Vehicle.Model\" data=\"VehicM\" on-select=\"selectedModel($item)\" ng-required=\"true\" search-enabled=\"false\" ng-disabled=\"disVehic\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Model\"> {{$select.selected.VehicleModelName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicM\"> <span ng-bind-html=\"item.VehicleModelName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelName\" required name=\"vehicModel\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <!--<bsselect name=\"vehicType\" data=\"VehicT\" on-select=\"selectedType(selected)\" item-value=\"VehicleTypeId\" item-text=\"NewDescription\" ng-model=\"mDataCrm.Vehicle.Type\" placeholder=\"Vehicle Type\" ng-readonly=\"enableType\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select data=\"VehicT\" name=\"vehicType\" ng-if=\"mFilterTAM.isTAM == 1\" ng-model=\"mDataCrm.Vehicle.Type\" on-select=\"selectedType($item)\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Pilih Tipe Kendaraan\"> {{$select.selected.NewDescription}} </ui-select-match> <ui-select-choices repeat=\"item in VehicT\"> <span ng-bind-html=\"item.NewDescription\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ModelType\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.vehicType.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna</bsreqlabel> <!-- <input type=\"text\" class=\"form-control\" ng-readonly=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Warna\"> --> <!-- <bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"ColorName\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <ui-select name=\"colorMobil\" ng-model=\"mDataCrm.Vehicle.Color\" data=\"VehicColor\" on-select=\"selectedColor($item.MColor)\" ng-if=\"mFilterTAM.isTAM == 1\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\"> <ui-select-match allow-clear placeholder=\"Warna\"> {{$select.selected.ColorName}} </ui-select-match> <ui-select-choices repeat=\"item in VehicColor\"> <span ng-bind-html=\"item.MColor.ColorName\"></span> </ui-select-choices> </ui-select> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable ng-if=\"mFilterTAM.isTAM == 0\" class=\"form-control\" ng-model=\"mDataCrm.Vehicle.ColorName\" required name=\"vehicType\" placeholder=\"Model Type\"> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\" ng-show=\"isCustomColor\"> <div class=\"form-group\" show-errors> <bsreqlabel>Warna Lain</bsreqlabel> <input type=\"text\" ng-disabled=\"disVehic\" skip-enable class=\"form-control\" ng-model=\"mData.VehicleColor\" placeholder=\"Warna\"> <!--<bsselect name=\"colorMobil\" data=\"VehicColor\" on-select=\"selectedColor(selected)\" item-value=\"ColorId\" item-text=\"VehicleColor\" ng-model=\"mDataCrm.Vehicle.Color\" placeholder=\"Color Type\" ng-readonly=\"enableColor\" ng-disabled=\"disVehic\" skip-enable required>\r" +
    "\n" +
    "                                            </bsselect> --> <em ng-messages=\"listVehicle.colorMobil.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <!-- <div class=\"col-md-6\" ng-if=\"mFilterTAM.isTAM == 1\"> --> <div class=\"form-group\" show-errors> <bsreqlabel ng-if=\"mFilterTAM.isTAM == 1\">Kode Warna</bsreqlabel> <label ng-if=\"mFilterTAM.isTAM == 0\">Kode Warna</label> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCode\" placeholder=\"Kode Warna\" required> <input name=\"colorCode\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" skip-enable ng-model=\"mDataCrm.Vehicle.ColorCodeNonTAM\" placeholder=\"Kode Warna\"> <em ng-messages=\"listVehicle.colorCode.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Bahan Bakar</bsreqlabel> <bsselect data=\"VehicGas\" on-select=\"selectedGas(selected)\" item-value=\"VehicleGasTypeId\" name=\"fuel\" item-text=\"VehicleGasTypeName\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" placeholder=\"Vehicle Gas Type\" ng-disabled=\"disVehic\" skip-enable required> </bsselect> <!-- <ui-select name=\"fuel\" ng-model=\"mDataCrm.Vehicle.GasTypeId\" on-select=\"selectedGas($item)\" item-text=\"VehicleGasTypeName\" ng-required=\"true\" ng-disabled=\"disVehic\" search-enabled=\"false\" skip-enable theme=\"select2\">\r" +
    "\n" +
    "                                                <ui-select-match allow-clear placeholder=\"Vehicle Gas Type\">\r" +
    "\n" +
    "                                                    {{$select.selected.VehicleGasTypeName}}\r" +
    "\n" +
    "                                                </ui-select-match>\r" +
    "\n" +
    "                                                <ui-select-choices repeat=\"item in VehicGas\">\r" +
    "\n" +
    "                                                    <span ng-bind-html=\"item.VehicleGasTypeName\"></span>\r" +
    "\n" +
    "                                                </ui-select-choices>\r" +
    "\n" +
    "                                            </ui-select> --> <em ng-messages=\"listVehicle.fuel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tahun Rakit</bsreqlabel> <input name=\"tahunRakit\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable numbers-only ng-model=\"mDataCrm.Vehicle.AssemblyYear\" placeholder=\"Tahun Rakit\" required> <em ng-messages=\"listVehicle.tahunRakit.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Full Model</bsreqlabel> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 1\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCode\" placeholder=\"Katashiki Code\" required> <input name=\"fullModel\" style=\"text-transform: uppercase\" type=\"text\" class=\"form-control\" ng-if=\"mFilterTAM.isTAM == 0\" skip-enable ng-model=\"mDataCrm.Vehicle.KatashikiCodeNonTAM\" placeholder=\"Katashiki Code\" required> <em ng-messages=\"listVehicle.fullModel.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Mesin</bsreqlabel> <input type=\"text\" name=\"NoMesin\" maxlength=\"20\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.EngineNo\" placeholder=\"No. Mesin\" required> <em ng-messages=\"listVehicle.NoMesin.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Asuransi</label> <input type=\"text\" name=\"Asuransi\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.Insurance\" placeholder=\"Asuransi\"> <em ng-messages=\"listVehicle.Asuransi.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. SPK</label> <input type=\"text\" name=\"noSPK\" style=\"text-transform: uppercase\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.SPK\" placeholder=\"No. SPK\"> <em ng-messages=\"listVehicle.noSPK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal DEC</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <!--<input type=\"text\" class=\"form-control\"  ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal DEC\">--> <!-- {{disVehic}} --> <bsdatepicker name=\"DateDEC\" ng-model=\"mDataCrm.Vehicle.DECDate\" ng-if=\"disVehic == false\" date-options=\"dateOptionsBirth\" max-date=\"maxDateS\" placeholder=\"Tanggal Lahir\" ng-change=\"dateChange(dt)\" ng-disabled=\"disVehic\" skip-enable required> </bsdatepicker> <input name=\"DateDEC\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" placeholder=\"Tanggal Lahir\" required> <!--\r" +
    "\n" +
    "                                            <bsdatepicker name=\"DateDEC\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.DECDate\" max-date='maxDateOptionDECLC.maxDate' date-options=\"dateDECOptions\" ng-change=\"dateChange(dt)\" required>\r" +
    "\n" +
    "                                            </bsdatepicker> --> <em ng-messages=\"listVehicle.DateDEC.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama STNK</bsreqlabel> <input type=\"text\" name=\"nameStnk\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKName\" placeholder=\"Nama STNK\" required> <em ng-messages=\"listVehicle.nameStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat STNK</bsreqlabel> <input name=\"addressStnk\" type=\"text\" class=\"form-control\" ng-disabled=\"disVehic\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKAddress\" placeholder=\"Alamat STNK\" required> <em ng-messages=\"listVehicle.addressStnk.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Masa Berlaku STNK</bsreqlabel><span style=\"font-size: 10px\">(dd/mm/yyyy)</span> <bsdatepicker name=\"dateSTNK\" ng-disabled=\"disVehic\" ng-if=\"disVehic == false\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" date-options=\"dateDECOptions\" placeholder=\"Masa Berlaku STNK\" required> </bsdatepicker> <input name=\"dateSTNK\" type=\"date\" class=\"form-control\" ng-if=\"disVehic == true\" ng-disabled=\"true\" skip-enable ng-model=\"mDataCrm.Vehicle.STNKDate\" placeholder=\"Masa Berlaku STNK\" required> <em ng-messages=\"listVehicle.dateSTNK.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\" style=\"margin-top: 20px; margin-right: 10px\"> <button type=\"button\" ng-show=\"modeV=='form' || modeV=='search'\" ng-click=\"saveVehic(mFilterVehic.actionV, mDataCrm, mFilterTAM.isTAM);\" ng-disabled=\"listVehicle.$invalid\" skip-enable class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> <button type=\"button\" ng-click=\"cancelSearchV()\" ng-show=\"modeV=='form' || modeV=='search'\" class=\"wbtn btn\">Batal</button> </div> </div> <!-- </div> --> </div> </div> </div> <div uib-accordion-group class=\"panel-default\" ng-show=\"showInst\"> <uib-accordion-heading> Penanggung Jawab </uib-accordion-heading> <div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPnj()\" ng-hide=\"!disPnj\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"savePnj()\" ng-show=\"!disPnj\" class=\"rbtn btn\">Save</button> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Lengkap</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICName\" ng-readonly=\"disPnj\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>No. Handphone</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"mDataCrm.Customer.PICHp\" ng-readonly=\"disPnj\" numbers-only> </div> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Pengguna </uib-accordion-heading> <div class=\"row\" ng-hide=\"wobpMode == 'new'\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editPng(mData.US)\" ng-hide=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelPng(mData.US)\" ng-show=\"!disPng\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"savePng(mData.US, mDataCrm, CustomerVehicleId)\" ng-show=\"!disPng\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" ng-show=\"!disPng\"> <div class=\"col-md-12\"> <div class=\"form-group\" ng-show=\"!hiddenUserList\"> <label>List Pengguna</label> <bsselect ng-model=\"mData.US\" data=\"pngList\" item-text=\"name\" item-value=\"VehicleUserId\" placeholder=\"Pilih Pengguna\" on-select=\"onChangePng(selected)\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Lengkap</label> <input type=\"text\" name=\"nameLengkap\" class=\"form-control\" ng-model=\"mData.US.name\" ng-readonly=\"disPng\"> <input type=\"text\" class=\"form-control\" ng-model=\"mData.US.VehicleUserId\" ng-hide=\"true\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 1</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" name=\"HP1\" numbers-only ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mData.US.phoneNumber1\" ng-readonly=\"disPng\"> <!--  numbers-only --> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>No. Handphone 2</label> <div class=\"input-icon-right\"> <i class=\"fa fa-phone\"></i> <input type=\"text\" name=\"HP2\" numbers-only ng-maxlength=\"14\" class=\"form-control\" ng-model=\"mData.US.phoneNumber2\" ng-readonly=\"disPng\"> <!-- numbers-only --> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Hubungan dengan Pemilik</label> <bsselect data=\"dataRelationship\" on-select=\"selectedRelationship(selected)\" item-value=\"RelationId\" item-text=\"RelationDesc\" ng-model=\"mData.US.Relationship\" placeholder=\"Pilih Hubungan dengan Pemilik\" ng-disabled=\"disPng\" skip-enable> </bsselect> </div> </div> </div> </div> </div> <div uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> Kontak Person & Pengambil Keputusan </uib-accordion-heading> <div class=\"col-md-12\"> <div class=\"row\"> <div smart-include=\"app/services/reception/wo/includeForm/CPPKInfo.html\"> </div> </div> </div> </div> </form> </uib-accordion> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/decisionMaker.html',
    "<div class=\"form-group\" show-errors> <bsreqlabel>Pengambil Keputusan</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"decisionMaker\" placeholder=\"\" ng-model=\"mData.decisionMaker.name\" required> <bserrmsg field=\"contactPerson\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>No. Handphone 1</bsreqlabel> <input type=\"numbers-only\" class=\"form-control\" name=\"decisionMakerPhoneNumber1\" placeholder=\"\" ng-model=\"mData.decisionMaker.phoneNumber1\" required> <bserrmsg field=\"decisionMakerPhoneNumber1\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>No. Handphone 2</bsreqlabel> <input type=\"numbers-only\" class=\"form-control\" name=\"decisionMakerPhoneNumber2\" placeholder=\"\" ng-model=\"mData.decisionMaker.phoneNumber2\"> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/modalFormDetail.html',
    "<div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Material</bsreqlabel> <!-- <bsselect name=\"tipe\" ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" item-text=\"Name\" item-value=\"MaterialTypeId\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeParts(selected)\">\r" +
    "\n" +
    "            </bsselect> --> <select ng-disabled=\"ldModel.PartsId !== undefined && ldModel.PartsId !== null\" required ng-required=\"true\" name=\"tipe\" class=\"form-control\" convert-to-number ng-model=\"ldModel.MaterialTypeId\" data=\"materialCategory\" placeholder=\"Pilih Tipe\" ng-change=\"selectTypeParts(ldModel)\"> <option ng-repeat=\"mtr in materialCategory\" value=\"{{mtr.MaterialTypeId}}\">{{mtr.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"ldModel.MaterialTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.tipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"typeMaterial\"></bserrmsg> --> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label>No. Material</label> <bscheckbox ng-change=\"checkIsOPB(ldModel.IsOPB, ldModel)\" ng-disabled=\"!(ldModel.MaterialTypeId == 2) || (ldModel.PartsCode !== undefined && ldModel.PartsCode !== '' &&  ldModel.PartsCode !== null) \" class=\"pull-right\" ng-model=\"ldModel.IsOPB\" label=\"OPB\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div class=\"col-xs-8\" style=\"padding-left:0\"> <bstypeahead placeholder=\"No. Material\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" ng-change=\"ubahPartsCode(ldModel)\" get-data=\"getParts\" item-text=\"PartsCode\" ng-disabled=\"(ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null) || ldModel.IsOPB == 1\" ng-maxlength=\"250\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectParts\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\" template-url=\"customTemplate.html\" icon=\"fa-id-card-o\"> </bstypeahead> <em ng-show=\"ldModel.PartsCode.length < 4\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p>Minimal 4 karakter untuk pencarian</p> </em> <em ng-show=\"ldModel.PartsCode.length >= 4\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p>&nbsp;</p> </em> </div> <!-- <div class=\"col-md-1 col-sm-12\">\r" +
    "\n" +
    "                    <div class=\"form-group\" > --> <div class=\"col-xs-4\"> <button class=\"rbtn btn\" ng-disabled=\"!(ldModel.Qty !== null && ldModel.Qty !== undefined && partsAvailable)\" ng-click=\"checkAvailabilityParts(ldModel)\">Cek</button> <!-- </div> --> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Material</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"namaMaterial\" ng-model=\"ldModel.PartsName\" ng-maxlength=\"64\" ng-disabled=\"ldModel.JobPartsId !== undefined && ldModel.JobPartsId !== null\" ng-change=\"checkNameParts(ldModel.PartsName)\" required> <bserrmsg field=\"namaMaterial\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input step=\"1\" type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" required min=\"1\" class=\"form-control\" name=\"qty\" ng-model=\"ldModel.Qty\"> <em ng-messages=\"row1.qty.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\"> Nilai Qty Harus Satuan</p> </em> <!-- <bserrmsg field=\"qty\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-model=\"ldModel.SatuanId\" on-select=\"selectTypeUnitParts(selected)\" data=\"unitData\" item-text=\"Name\" required item-value=\"MasterId\" placeholder=\"Pilih Tipe\">\r" +
    "\n" +
    "            </bsselect> --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-model=\"ldModel.SatuanId\" ng-change=\"selectTypeUnitParts(ldModel)\" data=\"unitData\" placeholder=\"Pilih Tipe\"> <option ng-repeat=\"unt in unitData\" value=\"{{unt.MasterId}}\">{{unt.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"ldModel.SatuanId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Tipe Order</label> <!-- <bsselect name=\"satuan\" ng-model=\"ldModel.Type\" data=\"typeMData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Tipe\" ng-disabled = \"DisTipeOrder\" skip-enable>\r" +
    "\n" +
    "            </bsselect> --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-model=\"ldModel.OrderType\" data=\"typeMData\" placeholder=\"Pilih Tipe\" ng-disabled=\"DisTipeOrder\" skip-enable> <option ng-repeat=\"tdata in typeMData\" value=\"{{tdata.Id}}\">{{tdata.Name}}</option> </select> <bserrmsg field=\"satuan\"></bserrmsg> </div> <div class=\"form-group\"> <label>ETA</label><span style=\"font-size: 10px\"> (dd/mm/yyyy)</span> <!-- <bsdatepicker name=\"DateAppoint\" ng-disabled=\"partsAvailable\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\">\r" +
    "\n" +
    "            </bsdatepicker> --> <bsdatepicker name=\"DateAppoint\" ng-disabled=\"true\" date-options=\"DateOptions\" ng-model=\"ldModel.ETA\" ng-change=\"DateChange(dt)\"> </bsdatepicker> </div> <div class=\"form-group\" show-errors> <label>Ketersediaan</label> <input type=\"text\" min=\"0\" class=\"form-control\" ng-disabled=\"true\" name=\"ketersedian\" ng-model=\"ldModel.Availbility\"> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectTypePaidParts(selected)\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select name=\"pembayaran\" ng-init=\"ldModel.PaidById=''\" convert-to-number class=\"form-control\" ng-model=\"ldModel.PaidById\" data=\"paymentData\" ng-change=\"selectTypePaidParts(ldModel.PaidById)\" ng-disabled=\"JobIsWarranty == 1\" placeholder=\"Pilih Pembayaran\" required> <option value=\"\"></option> <option ng-repeat=\"pyt in paymentData\" value=\"{{pyt.MasterId}}\">{{pyt.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"ldModel.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Harga</label> <!-- <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=0 class=\"form-control\" name=\"harga\" ng-maxlength=\"10\" ng-disabled=\"ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0\" ng-model=\"ldModel.RetailPrice\"> --> <input type=\"text\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"0\" class=\"form-control\" name=\"harga\" ng-if=\"ldModel.IsOPB != 1\" ng-maxlength=\"10\" ng-required=\"ldModel.IsOPB == 1\" ng-disabled=\"(ldModel.IsOPB == null || ldModel.IsOPB == undefined || ldModel.IsOPB == 0) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\" currency-formating number-only> <input type=\"number\" ng-change=\"checkPrice(ldModel.RetailPrice)\" min=\"1\" class=\"form-control\" name=\"hargaOPB\" ng-if=\"ldModel.IsOPB == 1\" ng-maxlength=\"10\" ng-required=\"ldModel.IsOPB == 1\" ng-disabled=\"(ldModel.IsOPB == 1) && (ldModel.PartsId != null && ldModel.PartsId != undefined)\" ng-model=\"ldModel.RetailPrice\"> <em ng-messages=\"row1.harga.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <em ng-messages=\"row1.hargaOPB.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Harga Tidak Boleh 0</p> </em> <!-- <bserrmsg field=\"harga\"></bserrmsg> --> </div> <!--     <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "      <label>Diskon</label>\r" +
    "\n" +
    "      <input type=\"number\" min=0 max=\"100\" class=\"form-control\" name=\"harga\" ng-model=\"ldModel.Discount\">\r" +
    "\n" +
    "      <bserrmsg field=\"harga\"></bserrmsg>\r" +
    "\n" +
    "    </div> --> <div class=\"form-group\" show-errors> <label>Sub Total</label> <!-- <input type=\"number\" ng-disabled=\"true\" min=0 class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotal\"> --> <input type=\"text\" ng-disabled=\"true\" min=\"0\" class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.subTotal\" currency-formating number-only> <!-- <input type=\"text\" ng-disabled=\"true\" min=0 class=\"form-control\" name=\"subTotal\" ng-model=\"ldModel.xsubTotal\" currency-formating number-only> --> <bserrmsg field=\"subTotal\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Dp</label> <!-- <input type=\"text\" currency-formating number-only min=0 ng-maxlength=\"10\" ng-disabled=\"disableDP || (ldModel.PaidById == 2277 || ldModel.PaidById == 30 || ldModel.PaidById == 31 || ldModel.PaidById == 2458) || ldModel.Availbility == 'Tersedia'\" skip-enable class=\"form-control\" name=\"Dp\" ng-change=\"checkDp(ldModel.DownPayment, ldModel)\" ng-model=\"ldModel.DownPayment\"> --> <input type=\"text\" currency-formating number-only min=\"0\" ng-maxlength=\"10\" ng-disabled=\" (disableDP && ldModel.PaidById != 28) || ldModel.Availbility == 'Tersedia'\" skip-enable class=\"form-control\" name=\"Dp\" ng-change=\"checkDp(ldModel.DownPayment, ldModel)\" ng-model=\"ldModel.DownPayment\"> <bserrmsg field=\"Dp\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectDiscountTypeIdParts(selected)\" placeholder=\"Pilih Tipe Diskon\" required>\r" +
    "\n" +
    "            </bsselect> --> <select name=\"diskon\" convert-to-number class=\"form-control\" ng-model=\"ldModel.DiscountTypeId\" data=\"diskonDataParts\" ng-change=\"selectTypeDiskonParts(ldModel.DiscountTypeId)\" ng-disabled=\"ldModel.PaidById == 2277 || ldModel.PaidById == 30 || ldModel.PaidById == 31 || ldModel.PaidById == 32 || JobIsWarranty == 1 || (ldModel.IsOPB != 1 && (ldModel.PartsCode == '' || ldModel.PartsCode == null || ldModel.PartsCode == undefined) )\" placeholder=\"Pilih Tipe Diskon\" required> <option value=\"\"></option> <option ng-repeat=\"disc1 in diskonDataParts\" value=\"{{disc1.MasterId}}\">{{disc1.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.diskon.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>List Diskon</bsreqlabel> <!-- <bsselect name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonPart(selected,ldModel.DiscountTypeId)\" placeholder=\"Pilih List Diskon\"\r" +
    "\n" +
    "                ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\">\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number name=\"diskon\" class=\"form-control\" ng-model=\"ldModel.DiscountTypeListId\" ng-disabled=\"ldModel.DiscountTypeId == 0 || ldModel.DiscountTypeId == -1 || ldModel.DiscountTypeId == 10 || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonPart(ldModel,ldModel.DiscountTypeId)\" placeholder=\"Pilih List Diskon\" ng-required=\"ldModel.DiscountTypeId == 1 || ldModel.DiscountTypeId == 2\"> <option ng-repeat=\"disclist in diskonListDataParts\" value=\"{{disclist.Id}}\">{{disclist.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"ldModel.DiscountTypeListId\" ng-hide=\"true\" ng-required=\"ldModel.typeDiskon !== undefined && ldModel.typeDiskon !== null && ldModel.typeDiskon > 0\"> <bserrmsg field=\"diskon\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-change=\"changeDiscount(ldModel.subTotal,ldModel.Discount)\" ng-required=\"isCustomTask\" ng-disabled=\"(!isCustomPart) || ldModel.PaidById == 2277 || ldModel.PaidById == 30 || JobIsWarranty == 1 || ldModel.DiscountTypeId == -1\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"ldModel.Discount\"> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.PartsName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> <div style=\"margin-top:50px; float: left\"> &nbsp; </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/modalFormMaster.html',
    "<div class=\"col-md-12\" ng-form=\"row1\"> <div class=\"col-xs-6\"> <div class=\"row\"></div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe</bsreqlabel> <bsselect name=\"tipe\" ng-model=\"lmModel.JobTypeId\" data=\"taskCategory\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"isRelease\" placeholder=\"Pilih Tipe\" on-select=\"selectTypeWork(selected,lmModel.JobTypeId)\"> </bsselect> <!-- <select name=\"tipe\" ng-model=\"lmModel.JobTypeId\" data=\"taskCategory\" ng-disabled=\"isRelease\" placeholder=\"Pilih Tipe\"\r" +
    "\n" +
    "                    ng-change=\"selectTypeWork(lmModel,lmModel)\" class=\"form-control\">\r" +
    "\n" +
    "                    <option ng-repeat=\"task in taskCategory\" value=\"{{task.MasterId}}\">{{task.Name}}</option>\r" +
    "\n" +
    "                </select> --> <em ng-messages=\"row1.tipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pekerjaan</bsreqlabel> <bstypeahead ng-if=\"tmpIsTAM != 0 && mFilterTAM.isTAM == 1\" placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"lmModel.TaskName\" get-data=\"getWork\" ng-disabled=\"isRelease\" item-text=\"TaskName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWork\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" icon=\"fa-id-card-o\" required> </bstypeahead> <input ng-if=\"tmpIsTAM == 0 || mFilterTAM.isTAM != 1\" placeholder=\"Nama Pekerjaan\" type=\"text\" required ng-disabled=\"isRelease\" class=\"form-control\" name=\"jobName\" ng-model=\"lmModel.TaskName\"> <em ng-messages=\"row1.jobName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"jobName\"></bserrmsg> --> </div> <div class=\"form-group\" style=\"margin-bottom:45px\" show-errors> <div class=\"form-inline\"> <label class=\"pull-left\">Additional OpeNo</label> <bscheckbox ng-change=\"getDataOpeNo(lmModel.TaskName,lmModel.isOpe,lmModel)\" class=\"pull-right\" ng-model=\"lmModel.isOpe\" label=\"Ope No\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\" show-errors> <!-- <input type=\"text\" class=\"form-control\" name=\"noMaterial\" ng-model=\"ldModel.PartsCode\" > --> <div style=\"padding-left:0;padding-right:0;padding-bottom:15px\"> <!-- <bstypeahead placeholder=\"Task Code\" name=\"noMaterial\" ng-model=\"lmModel.TaskCode\" get-data=\"getTaskOpe\"\r" +
    "\n" +
    "                    item-text=\"TaskCode\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected=\"selected\"\r" +
    "\n" +
    "                    on-select=\"onSelectOpeNo\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\"\r" +
    "\n" +
    "                    ng-disabled=\"lmModel.isOpe == 0\" template-url=\"customTemplateOpe.html\" icon=\"fa-id-card-o\">\r" +
    "\n" +
    "                    </bstypeahead> --> <select name=\"OpeNo\" ng-change=\"selectTypeOpeNo(lmModel.TaskCode,lmModel)\" class=\"form-control\" ng-disabled=\"lmModel.isOpe == 0\" ng-model=\"lmModel.TaskCode\" data=\"DataOpeNo\" placeholder=\"Pilih Tipe\"> <option ng-repeat=\"sat in DataOpeNo\" value=\"{{sat.TaskCode}}\">{{sat.TaskName}}</option> </select> </div> <em ng-messages=\"row1.noMaterial.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"noMaterial\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div class=\"row\"> <div class=\"col-xs-6\"> <bsreqlabel>FR</bsreqlabel> <input type=\"number\" required step=\"0.0000000001\" min=\"0\" class=\"form-control\" name=\"Fr\" ng-model=\"lmModel.FlatRate\" ng-disabled=\"FlatRateAvailable\" ng-change=\"checkServiceRate(lmModel.FlatRate,lmModel.JobTypeId)\"> <em ng-messages=\"row1.Fr.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"min\">Angka harus lebih besar atau sama dengan 0 </p> </em> </div> <!-- <div class=\"form-group\" show-errors> --> <div class=\"col-xs-6\"> <bsreqlabel>Satuan</bsreqlabel> <!-- <bsselect name=\"satuan\" ng-disabled=\"isRelease\" ng-model=\"lmModel.ProcessId\" data=\"unitData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe\" required>\r" +
    "\n" +
    "                    </bsselect> --> <select name=\"satuan\" convert-to-number class=\"form-control\" ng-disabled=\"isRelease\" ng-model=\"lmModel.ProcessId\" data=\"unitData\" placeholder=\"Pilih Tipe\" required> <option ng-repeat=\"sat in unitData\" value=\"{{sat.MasterId}}\">{{sat.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"satuan\" ng-model=\"lmModel.ProcessId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.satuan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <!-- <bserrmsg field=\"satuan\"></bserrmsg> --> <!-- </div> --> <!-- <bserrmsg field=\"Fr\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div class=\"col-md-12\" style=\"padding-right: 0px;padding-left: 0px\"> <bsreqlabel>Harga</bsreqlabel> <div ng-if=\"PriceAvailable == false && (isRelease == false || isRelease == true)\"> <!-- <input type=\"number\" ng-maxlength=\"10\" required min=0 class=\"form-control\" name=\"price\" ng-model=\"lmModel.Fare\"\r" +
    "\n" +
    "                    ng-disabled=\"\"> --> <input type=\"text\" ng-maxlength=\"10\" required min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Fare\" ng-disabled=\"\" currency-formating number-only> </div> <div ng-if=\"PriceAvailable == true && (isRelease == false || isRelease == true)\"> <!-- <input type=\"number\" required ng-maxlength=\"10\" min=0 class=\"form-control\" name=\"price\" ng-model=\"lmModel.Summary\"\r" +
    "\n" +
    "                    ng-disabled=\"PriceAvailable\"> --> <input type=\"text\" required ng-maxlength=\"10\" min=\"0\" class=\"form-control\" name=\"price\" ng-model=\"lmModel.Summary\" ng-change=\"changeHarga(lmModel, mFilterTAM.isTAM)\" ng-disabled=\"PriceAvailable && mFilterTAM.isTAM == 1\" currency-formating number-only> </div> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> <!-- <bserrmsg field=\"price\"></bserrmsg> --> </div> </div> <div class=\"col-xs-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Job Plan / SAR</bsreqlabel> <input type=\"number\" ng-disabled=\"isRelease\" step=\"0.25\" min=\"0.25\" class=\"form-control\" name=\"jobplan\" ng-model=\"lmModel.ActualRate\" required ng-change=\"onSARtype(lmModel.ActualRate)\"> <em ng-messages=\"row1.jobplan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Jobplan / SAR harus kelipatan 0.25 </p> <p ng-message=\"min\">Jobplan / SAR harus lebih dari sama dengan 0.25 </p> </em> <!-- <bserrmsg field=\"jobplan\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-disabled=\"isRelease\" ng-model=\"lmModel.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" on-select=\"selectTypeCust(selected,lmModel)\" required>\r" +
    "\n" +
    "            </bsselect> --> <select ng-init=\"lmModel.PaidById=''\" name=\"pembayaran\" convert-to-number ng-disabled=\"disablePembayaran || JobIsWarranty == 1\" class=\"form-control\" ng-model=\"lmModel.PaidById\" placeholder=\"Pilih Pembayaran\" ng-change=\"selectTypeCust(lmModel,lmModel.PaidById)\" ng-required=\"true\" required> <option value=\"\"></option> <option ng-repeat=\"pay in paymentData\" value=\"{{pay.MasterId}}\">{{pay.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModel.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors ng-hide=\"hideT1customer\"> <label>T1 <span ng-show=\"!disT1\" style=\"color:#b94a48\"> *</span> </label> <!-- <bsselect name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" item-text=\"Name\" item-value=\"Name\" ng-disabled=\"disT1\" placeholder=\"T1\">\r" +
    "\n" +
    "            </bsselect> --> <!-- ng-required=\"!disT1\"  ng-disabled=\"disT1\" --> <select name=\"T1\" ng-model=\"lmModel.TFirst1\" data=\"TFirst\" placeholder=\"T1\" class=\"form-control\" ng-disabled=\"disT1\" ng-required=\"!disT1\"> <option value=\"\"></option> <option ng-repeat=\"tf1 in TFirst\" value=\"{{tf1.T1Code}} - {{tf1.Name}}\">{{tf1.T1Code}} - {{tf1.Name}}</option> </select> <em ng-messages=\"row1.T1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\" ng-if=\"disablePembayaran\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Diskon</bsreqlabel> <!-- <bsselect name=\"diskonTipe\" ng-model=\"lmModel.DiscountTypeId\" data=\"diskonData\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"selectDiscountTypeIdTask(selected,lmModel)\" placeholder=\"Pilih Tipe Diskon\" required>\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number name=\"diskonTipe\" ng-model=\"lmModel.DiscountTypeId\" class=\"form-control\" data=\"diskonData\" ng-change=\"selectTypeDiskonTask(lmModel.DiscountTypeId,lmModel)\" ng-disabled=\"lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.PaidById == 31 || lmModel.PaidById == 32 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" placeholder=\"Pilih Tipe Diskon\" required> <option value=\"\"></option> <option ng-repeat=\"disc in diskonData\" value=\"{{disc.MasterId}}\">{{disc.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeId\" ng-hide=\"true\" required ng-required=\"true\"> <em ng-messages=\"row1.diskonTipe.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <div ng-if=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2|| lmModel.DiscountTypeId == null|| lmModel.DiscountTypeId == undefined\"><bsreqlabel>List Diskon</bsreqlabel></div> <div ng-if=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10\"><label>List Diskon</label></div> <!-- <bsselect name=\"diskonList\" ng-model=\"lmModel.DiscountTypeListId\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1\" data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonTask(selected,lmModel.DiscountTypeId,lmModel)\"\r" +
    "\n" +
    "                    data=\"diskonListData\" item-text=\"Name\" item-value=\"Id\" on-select=\"selectTypeListDiskonTask(selected,lmModel.DiscountTypeId,lmModel)\"\r" +
    "\n" +
    "                    placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\">\r" +
    "\n" +
    "            </bsselect> --> <select convert-to-number name=\"diskonList\" ng-model=\"lmModel.DiscountTypeListId\" class=\"form-control\" ng-disabled=\"lmModel.DiscountTypeId == 0 || lmModel.DiscountTypeId == -1 || lmModel.DiscountTypeId == 10 || lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60\" data=\"diskonListData\" ng-change=\"selectTypeListDiskonTask(lmModel.DiscountTypeListId,lmModel.DiscountTypeId,lmModel)\" placeholder=\"Pilih List Diskon\" ng-required=\"lmModel.DiscountTypeId == 1 || lmModel.DiscountTypeId == 2\"> <option ng-repeat=\"discl in diskonListData\" value=\"{{discl.Id}}\">{{discl.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"diskon\" ng-model=\"lmModel.DiscountTypeListId\" ng-hide=\"true\" ng-required=\"lmModel.DiscountTypeId !== undefined && lmModel.DiscountTypeId !== null && lmModel.DiscountTypeId != 0 && lmModel.DiscountTypeId != -1 && lmModel.DiscountTypeId != 10\"> <em ng-messages=\"row1.diskonList.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <bserrmsg field=\"diskon\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <label>Diskon</label> <input type=\"number\" ng-required=\"isCustomTask\" ng-disabled=\"(!isCustomTask) || lmModel.PaidById == 2277 || lmModel.PaidById == 30 || lmModel.JobTypeId == 59 || lmModel.JobTypeId == 60 || lmModel.DiscountTypeId == -1\" min=\"0\" max=\"100\" class=\"form-control\" name=\"Discount\" ng-model=\"lmModel.Discount\"> <em ng-messages=\"row1.Discount.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpe.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.TaskName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/modalFormMasterOpl.html',
    "<div class=\"row\" ng-form=\"row1\"> <div class=\"col-md-12\" ng-if=\"(mData.Status <= 3 ||  mData.Status == undefined) && (lmModelOpl.Status <= 1 || lmModelOpl.Status == undefined)\"> <div class=\"form-group\"> <bsreqlabel>Nama OPL</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" ng-disabled=\"mData.Status > 3 && !isNew\" name=\"oplName\" ng-model=\"lmModelOpl.OPLName\" get-data=\"getOpl\" item-text=\"OPLWorkName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkOPL\" on-noresult=\"onNoResultOpl\" on-gotresult=\"onGotResult\" template-url=\"customTemplateOpl.html\" icon=\"fa-id-card-o\" required> </bstypeahead> <em ng-messages=\"row1.oplName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModelOpl.OplName\"> --> <!-- <bserrmsg field=\"oplName\"></bserrmsg> --> </div> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <!-- <bstypeahead placeholder=\"Nama Vendor\" name=\"vendorName\"\r" +
    "\n" +
    "                ng-model=\"lmModelOpl.VendorName\"\r" +
    "\n" +
    "                get-data=\"getOpl\"\r" +
    "\n" +
    "                model-key=\"Opl\"\r" +
    "\n" +
    "                ng-minlength=\"2\" ng-maxlength=\"10\" disallow-space\r" +
    "\n" +
    "                loading=\"loadingUsers\"\r" +
    "\n" +
    "                noresult=\"noResults\"\r" +
    "\n" +
    "                selected=\"selected\"\r" +
    "\n" +
    "                on-select=\"onSelectUser\"\r" +
    "\n" +
    "                on-noresult=\"onNoResult\"\r" +
    "\n" +
    "                on-gotresult=\"onGotResult\",\r" +
    "\n" +
    "                ta-minlength=\"1\"\r" +
    "\n" +
    "                icon=\"fa-id-card-o\"\r" +
    "\n" +
    "            >\r" +
    "\n" +
    "            </bstypeahead> --> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModelOpl.VendorName\" required> <!-- <bserrmsg field=\"oplName\"></bserrmsg> --> <em ng-messages=\"row1.vendorName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"OPLPaymentData\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"paymentData in OPLPaymentData\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <!-- <bserrmsg field=\"pembayaran\"></bserrmsg> --> <em ng-messages=\"row1.pembayaran.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <bsreqlabel>Estimasi Selesai</bsreqlabel> <bsdatepicker name=\"RequiredDate\" min-date=\"dateOptionsOPL.minDate\" date-options=\"dateOptionsOPL\" ng-model=\"lmModelOpl.TargetFinishDate\" ng-change=\"DateChange(dt)\" ng-required=\"true\"> </bsdatepicker> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" required name=\"ReceiveBy\" ng-model=\"lmModelOpl.ReceiveBy\">\r" +
    "\n" +
    "            <bserrmsg field=\"ReceiveBy\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input type=\"number\" min=\"0\" ng-change=\"QtyOPL(lmModelOpl.QtyPekerjaan,lmModelOpl)\" class=\"form-control\" ng-disabled=\"false\" required name=\"qty\" ng-model=\"lmModelOpl.QtyPekerjaan\"> <!-- <bserrmsg field=\"qty\"></bserrmsg> --> <em ng-messages=\"row1.qty.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Harga</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> --> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <!-- <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "                <p ng-message=\"required\">Wajib diisi</p>\r" +
    "\n" +
    "            </em>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Beli</bsreqlabel> <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"PurchasePrice\" ng-model=\"lmModelOpl.OPLPurchasePrice\"> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Jual</bsreqlabel> <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <label>Notes</label> <textarea class=\"form-control\" name=\"notes\" ng-model=\"lmModelOpl.Notes\" style=\"height: 100px\"></textarea> </div> <div class=\"form-group\" show-errors> <label>Discount (%)</label> <input type=\"number\" min=\"0\" max=\"100\" class=\"form-control\" name=\"discount\" ng-model=\"lmModelOpl.Discount\" ng-disabled=\"DiscountAvailable\"> <bserrmsg field=\"discount\"></bserrmsg> </div> </div> <div class=\"col-md-12\" ng-if=\"mData.Status > 3 && lmModelOpl.Status > 1\"> <div class=\"form-group\"> <bsreqlabel>Nama OPL</bsreqlabel> <bstypeahead placeholder=\"Nama Pekerjaan\" ng-disabled=\"true\" name=\"oplName\" ng-model=\"lmModelOpl.OPLName\" get-data=\"getOpl\" item-text=\"OPLWorkName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWorkOPL\" on-noresult=\"onNoResultOpl\" on-gotresult=\"onGotResult\" template-url=\"customTemplateOpl.html\" icon=\"fa-id-card-o\" required> </bstypeahead> <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Nama OPL\" name=\"oplName\" ng-model=\"lmModelOpl.OplName\"> --> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Vendor</bsreqlabel> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" placeholder=\"Nama Vendor\" name=\"vendorName\" ng-model=\"lmModelOpl.VendorName\"> <bserrmsg field=\"oplName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Pembayaran</bsreqlabel> <!-- <bsselect name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"paymentData\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"true\" placeholder=\"Pilih Pembayaran\" required>\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" data=\"OPLPaymentData\" ng-disabled=\"true\" placeholder=\"Pilih Pembayaran\" required> <option ng-repeat=\"paymentData in OPLPaymentData\" value=\"{{paymentData.MasterId}}\">{{paymentData.Name}}</option> </select> <input type=\"text\" class=\"form-control\" name=\"pembayaran\" ng-model=\"lmModelOpl.PaidById\" ng-hide=\"true\" required ng-required=\"true\"> <bserrmsg field=\"pembayaran\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Estimasi Selesai</bsreqlabel> <bsdatepicker name=\"RequiredDate\" ng-disabled=\"true\" min-date=\"dateOptionsOPL.minDate\" date-options=\"dateOptionsOPL\" ng-model=\"lmModelOpl.TargetFinishDate\" ng-change=\"DateChange(dt)\" ng-required=\"true\"> </bsdatepicker> </div> <!--         <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Penerima</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"text\" class=\"form-control\" required name=\"ReceiveBy\" ng-model=\"lmModelOpl.ReceiveBy\">\r" +
    "\n" +
    "            <bserrmsg field=\"ReceiveBy\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Quantity</bsreqlabel> <input type=\"number\" ng-change=\"QtyOPL(lmModelOpl.QtyPekerjaan,lmModelOpl)\" class=\"form-control\" ng-disabled=\"true\" required name=\"qty\" ng-model=\"lmModelOpl.QtyPekerjaan\"> <bserrmsg field=\"price\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "            <bsreqlabel>Harga</bsreqlabel>\r" +
    "\n" +
    "            <input type=\"number\" ng-disabled=\"true\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\">\r" +
    "\n" +
    "            <bserrmsg field=\"price\"></bserrmsg>\r" +
    "\n" +
    "        </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Beli</bsreqlabel> <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"PurchasePrice\" ng-model=\"lmModelOpl.OPLPurchasePrice\"> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Jual</bsreqlabel> <input type=\"number\" min=\"0\" class=\"form-control\" required name=\"price\" ng-model=\"lmModelOpl.Price\"> <!-- <bserrmsg field=\"price\"></bserrmsg> --> <em ng-messages=\"row1.price.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <label>Notes</label> <textarea ng-disabled=\"true\" class=\"form-control\" name=\"notes\" ng-model=\"lmModelOpl.Notes\" style=\"height: 100px\"></textarea> </div> <div class=\"form-group\" show-errors> <label>Discount (%)</label> <input type=\"number\" min=\"0\" max=\"100\" ng-disabled=\"true\" class=\"form-control\" name=\"discount\" ng-model=\"lmModelOpl.Discount\" ng-disabled=\"DiscountAvailable\"> <bserrmsg field=\"discount\"></bserrmsg> </div> </div> <script type=\"text/ng-template\" id=\"customTemplateOpl.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.Vendor.Name | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.isExternalDesc | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wac.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-01.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Pemilik</label> <input ng-model=\"mDataCustomer.owner\" type=\"text\" skip-enable disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Pengguna</label> <input ng-model=\"mDataCustomer.user\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Alamat</label> <input ng-model=\"mDataCustomer.address\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>On Time/Late</label> <input ng-model=\"mDataCustomer.late\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Waiting Time</label> <input ng-model=\"mDataCustomer.waitTime\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> <div class=\"form-group\"> <label>Model</label> <input ng-model=\"mDataCustomer.model\" skip-enable type=\"text\" disabled class=\"form-control\"> </div> </div> <div class=\"col-md-12\"> <div class=\"well\"> <div class=\"form-group\"> <label>Job Suggest</label> <textarea class=\"form-control\" skip-enable ng-model=\"mDataCustomer.Suggestion\" ng-disabled=\"true\"></textarea> </div> </div> </div> <div class=\"col-md-12\"> <uib-tabset active=\"activeJustified\" justified=\"true\"> <uib-tab index=\"0\" heading=\"Field Action\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Operation No.</font> </td> <td> <font color=\"white\">Description</font> </td> </tr> <tr ng-repeat=\"a in GetDataTowass\"> <td>{{a.OpeNo}}</td> <td>{{a.Description}}</td> </tr> </table> </div> </uib-tab> <uib-tab index=\"1\" heading=\"PSFU\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table id=\"QA\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>No.</th> <th>Pertanyaan</th> <th>Jawaban Customer</th> <!-- <th>Alasan Customer</th> --> </tr> </thead> <tbody> <tr ng-repeat=\"item in Question track by $index\"> <td width=\"5%\">{{$index + 1}}</td> <td width=\"40%\">{{item.Description}}</td> <td width=\"25%\"> <select class=\"form-control\" name=\"yesOrNo\" ng-disabled=\"true\" skip-enable> <option ng-disabled=\"true\" skip-enable ng-repeat=\"itemAnswer in item.Answer track by $index\" value=\"{{itemAnswer.AnswerId}}\">{{itemAnswer.Description}}</option> </select> </td> </tr> </tbody> </table> </div> </uib-tab> <uib-tab index=\"2\" heading=\"CS Survey\"> </uib-tab> <uib-tab index=\"3\" heading=\"MRS\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <table class=\"table table-bordered\"> <tr style=\"background-color: grey\"> <td> <font color=\"white\">Tanggal Reminder</font> </td> <td> <font color=\"white\">Tipe Pekerjaan</font> </td> <td> <font color=\"white\">Status</font> </td> </tr> <tr ng-repeat=\"a in GetDataMRS\"> <td>{{a.reminderDate}}</td> <td>{{a.Name}}</td> <td>{{a.StatusName}}</td> </tr> </table> </div> </uib-tab> </uib-tabset> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-02.html',
    "<div class=\"row\" id=\"coeg\"> <div class=\"col-md-12\"> <div class=\"col-md-9\"> <p id=\"demo\" hidden>aa</p> </div> <div class=\"col-md-3\"> <div class=\"form-group form-inline\"> <label>Estimasi Durasi</label> <input type=\"number\" min=\"0.1\" step=\"0.1\" class=\"form-control\" style=\"width:100px\" name=\"EstimateHour\" ng-disabled=\"true\" skip-enable ng-change=\"estimationDuration(mData.EstimateHour)\" ng-model=\"mData.EstimateHour\"> <label>Jam</label> </div> <div class=\"form-group\"> <bsdatepicker name=\"tes\" date-options=\"DateOptionstes\" ng-model=\"asb.startDate\" ng-change=\"reloadBoard(asb.startDate)\" min-date=\"minDateASB3\"></bsdatepicker> </div> </div> </div> <div class=\"col-md-12\" ontouchmove=\"myFunction(event)\" ontouchend=\"myFunctionStop()\"> <div class=\"panel panel-default\"> <div class=\"panel-heading\"> <div class=\"clearfix\"></div> </div> <div class=\"panel-body\"> <div id=\"plottingStallWO\" style=\"background-color: #ddd; border-width:0px; border-color: black; border-style: solid\"></div> </div> <div class=\"panel-footer\"> </div> </div> </div> </div> <script>function myFunction(event) {\r" +
    "\n" +
    "        var x = event.touches[0].clientX;\r" +
    "\n" +
    "        var y = event.touches[0].clientY;\r" +
    "\n" +
    "        document.getElementById(\"demo\").innerHTML = x + \", \" + y;\r" +
    "\n" +
    "        var h = document.documentElement.scrollHeight;\r" +
    "\n" +
    "\r" +
    "\n" +
    "        //   ======\r" +
    "\n" +
    "        var tmpheight = $(\"#coeg\").height() + 100;\r" +
    "\n" +
    "        var targetScrollTop = tmpheight - $('#layoutContainer_SAMainMenuForm > ui-layout').height();\r" +
    "\n" +
    "        var targetScrollTopStart = 0;\r" +
    "\n" +
    "        var distanceToTravel = targetScrollTop - $('#layoutContainer_SAMainMenuForm > ui-layout').scrollTop();\r" +
    "\n" +
    "        var animationDuration = (distanceToTravel / 1000) * 1000;\r" +
    "\n" +
    "        h = 0.8 * h;\r" +
    "\n" +
    "        console.log(h);\r" +
    "\n" +
    "        if (y >= h) {\r" +
    "\n" +
    "            console.log('woi');\r" +
    "\n" +
    "            $(\"#layoutContainer_SAMainMenuForm > ui-layout\").animate({\r" +
    "\n" +
    "                scrollTop: targetScrollTop\r" +
    "\n" +
    "            }, 1500, 'linear');\r" +
    "\n" +
    "        } else if (y <= (0.25 * h)) {\r" +
    "\n" +
    "            $(\"#layoutContainer_SAMainMenuForm > ui-layout\").animate({\r" +
    "\n" +
    "                scrollTop: targetScrollTopStart\r" +
    "\n" +
    "            }, 1500, 'linear');\r" +
    "\n" +
    "        } else {\r" +
    "\n" +
    "            $(\"#layoutContainer_SAMainMenuForm > ui-layout\").clearQueue();\r" +
    "\n" +
    "            $(\"#layoutContainer_SAMainMenuForm > ui-layout\").stop();\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        //   if (y >= h ){\r" +
    "\n" +
    "        //   alert('bawah');\r" +
    "\n" +
    "        //   } else if ( y <= (h*0.25)){\r" +
    "\n" +
    "        //       alert('atas');\r" +
    "\n" +
    "        //   }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    function myFunctionStop() {\r" +
    "\n" +
    "        $(\"#layoutContainer_SAMainMenuForm > ui-layout\").clearQueue();\r" +
    "\n" +
    "        $(\"#layoutContainer_SAMainMenuForm > ui-layout\").stop();\r" +
    "\n" +
    "    }</script>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-03.html',
    "<div class=\"col-md-9\"> <div class=\"col-md-12\"> <label class=\"control-label\">Riwayat Servis</label> </div> <div class=\"col-md-3\" style=\"padding-left: 0px\"> <bsdatepicker name=\"Tanggal1\" date-options=\"DateOptionsHistory\" ng-model=\"MRS.startDate\"> </bsdatepicker> </div> <div class=\"col-md-1\" style=\"margin-top: 5px;padding-left: 0px; text-align: center\"> <label>S/D</label> </div> <div class=\"col-md-3\" style=\"padding-left: 0px\"> <bsdatepicker name=\"Tanggal2\" date-options=\"DateOptionsHistory\" ng-model=\"MRS.endDate\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <bsreqlabel class=\"control-label\">Kategori : </bsreqlabel> <!-- ng-change=\"change()\" --> <select ng-model=\"MRS.category\" class=\"form-control\" convert-to-number> <option value=\"2\">ALL</option> <option value=\"1\">GR</option> <option value=\"0\">BP</option> </select> </div> <div class=\"col-md-12\" style=\"margin-top: 10px;text-align: right\"> <button class=\"rbtn btn\" ng-click=\"change(MRS.startDate,MRS.endDate,MRS.category,mDataCrm.Vehicle.VIN)\">Cari</button> </div> <div class=\"col-md-12\" style=\"margin-top:20px\"> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"item in mDataVehicleHistory\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> <table style=\"width:100%\"> <tr> <td>{{item.VehicleJobService[0].JobDescription}}</td> <th class=\"text-right\">{{item.ServiceNumber}} | {{item.ServiceDate |date:'dd-MMM-yyyy'}}</th> <!-- <th colspan=\"2\">{{item.ServiceNumber}} | {{item.ServiceDate |date:dd/mm/yyyy}}</th> --> </tr> <tr> <td>Lokasi services : {{item.ServiceLocation}}</td> <td class=\"text-right\">SA : {{item.ServiceAdvisor}}</td> </tr> <tr> <td>KM : {{item.Km}}</td> <!-- <td class=\"text-right\">Group : {{item.Group}}</td> --> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Teknisi : {{item.VehicleJobService[0].EmployeeName}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Group : {{item.namaGroup}}</td> <!-- <td class=\"text-right\">Tanggal Servis : {{item.ServiceDate |date:dd/mm/yyyy}}</td> --> </tr> <tr> <td>Job Suggest : {{item.JobSuggest}}</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 1\">Kategori : GR</td> <td class=\"text-right\" ng-if=\"item.KategoriServiceId == 0\">Kategori : BP</td> </tr> </table> </uib-accordion-heading> <uib-accordion close-others=\"oneAtATime\"> <div ng-repeat=\"itemJob in item.VehicleJobService\" is-open=\"status.open\" uib-accordion-group class=\"panel-default\"> <uib-accordion-heading> {{itemJob.JobDescription}} <i class=\"pull-right glyphicon\" ng-class=\"{'glyphicon-chevron-down': status.open, 'glyphicon-chevron-right': !status.open}\"></i> </uib-accordion-heading> <table class=\"table table-striped\"> <tr> <th>No. Material</th> <th>Material</th> <!-- <th>Nama Material</th> --> <th>Qty</th> <th>Satuan</th> </tr> <tr ng-repeat=\"tr in itemJob.VehicleServiceHistoryDetail\"> <!-- <td>{{tr.PartsCode}}</td> --> <td>{{tr.MaterialCode}}</td> <td>{{tr.Material}}</td> <td>{{tr.Quantity | number:2}}</td> <td>{{tr.Satuan}}</td> </tr> </table> </div> </uib-accordion> </div> </uib-accordion> <div class=\"row\" ng-hide=\"isPSFU\"> <div class=\"col-md-12\"> <bscheckbox class=\"pull-right\" ng-model=\"mRiwayatService.isNeedPrint\" label=\"Print Riwayat Service\" ng-show=\"mDataVehicleHistory.length > 0\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-04.html',
    "<!-- Request --> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-12\"> <bslist data=\"JobRequest\" m-id=\"JobRequestId\" list-model=\"reqModel\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" on-before-save=\"onBeforeSaveJobRequest\" confirm-save=\"true\" list-api=\"listApi\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Permintaan\" list-title=\"Permintaan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Permintaan\" ng-model=\"lmItem.RequestDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div> <div class=\"form-group\" show-errors> <bsreqlabel>Permintaan</bsreqlabel> <textarea ng-maxtlength=\"200\" maxlength=\"200\" class=\"form-control\" placeholder=\"Permintaan\" name=\"reqdesc\" ng-model=\"reqModel.RequestDesc\">\r" +
    "\n" +
    "                </textarea> <bserrmsg field=\"reqdesc\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <!-- Keluhan --> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-12\" style=\"text-align:right\"> <div class=\"btn-group\" style=\"margin-top:25px\"> <button type=\"button\" ng-click=\"preDiagnose()\" class=\"rbtn btn ng-binding\"> Pre Diagnose </button> </div> </div> </div> <br> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-12\"> <bslist data=\"JobComplaint\" m-id=\"JobComplaintId\" list-model=\"lmComplaint\" on-select-rows=\"onListSelectRows\" selected-rows=\"listComplaintSelectedRows\" confirm-save=\"false\" list-api=\"listApi\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Keluhan\" list-title=\"Keluhan\" list-height=\"200\"> <bslist-template> <div> <div class=\"form-group\"> <bsreqlabel>Kategori: </bsreqlabel> <bsselect data=\"vm.appScope.ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmItem.ComplaintCatg\" placeholder=\"Tipe Keluhan\" ng-disabled=\"true\" skip-enable> </bsselect> </div> <div class=\"form-group\"> <textarea class=\"form-control\" placeholder=\"Deskripsi\" ng-model=\"lmItem.ComplaintDesc\" ng-disabled=\"true\" skip-enable>\r" +
    "\n" +
    "                                            </textarea> </div> </div> </bslist-template> <bslist-modal-form> <div style=\"margin-bottom:90px\"> <div class=\"form-group\"> <bsreqlabel>Tipe Keluhan</bsreqlabel> <bsselect data=\"ComplaintCategory\" item-value=\"ComplaintCatg\" item-text=\"ComplaintCatg\" ng-model=\"lmComplaint.ComplaintCatg\" placeholder=\"Tipe Keluhan\" required> </bsselect> <bserrmsg field=\"ctype\"></bserrmsg> </div> <div class=\"form-group\" style=\"margin-bottom: 100px\"> <bsreqlabel>Deskripsi</bsreqlabel> <textarea class=\"form-control\" placeholder=\"Complaint Text\" name=\"ctext\" ng-model=\"lmComplaint.ComplaintDesc\" required>\r" +
    "\n" +
    "                                            </textarea> <bserrmsg field=\"ctext\"></bserrmsg> </div> </div> </bslist-modal-form> </bslist> </div> </div> <br> <!-- JobList --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nomor WO Estimasi</label> <input type=\"text\" min=\"0\" class=\"form-control\" ng-disabled=\"true\" skip-enable name=\"NomorWo\" ng-model=\"mData.EstimationNo\"> <bserrmsg field=\"NomorWo\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Kategori WO</bsreqlabel> <select class=\"form-control\" name=\"categoryWO\" convert-to-number ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\" ng-change=\"changeSelectedWoCategory(mData.WoCategoryId)\"> <option ng-repeat=\"cat in woCategory\" value=\"{{cat.MasterId}}\">{{cat.Name}}</option> </select> <!-- <bsselect data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\">\r" +
    "\n" +
    "            </bsselect> --> <!-- <bserrmsg field=\"ctype\"></bserrmsg> --> <bserrmsg field=\"categoryWO\"></bserrmsg> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWork\" m-id=\"tmpTaskId\" d-id=\"PartsId\" on-after-save=\"onAfterSave\" on-after-delete=\"onAfterDelete\" on-before-edit=\"onBeforeEditJob\" on-before-save=\"onBeforeSave\" on-after-cancel=\"onAfterCancel\" on-before-new=\"onBeforeNew\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetail\" on-select-rows=\"onListSelectRows\" selected-rows=\"listJoblistSelectedRows\" list-api=\"listApiJob\" confirm-save=\"false\" button-settings=\"ButtonList\" list-title=\"Order Pekerjaan\" modal-title=\"Job Data\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe : {{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate : {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran : {{lmItem.PaidBy}}</p> <p>Harga : {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }} </p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}} </p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}} </p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMaster.html'\"> </div> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\"> </div> </div> </bslist-modal-detail-form> </bslist> </div> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWork | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(totalWork - totalWorkDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterial | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ (totalMaterial - totalMaterialDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi OPL</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl - sumWorkOplDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOplDiscounted | currency:\"\":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalPPN + (sumWorkOplDiscounted*(10/100)) | currency:\"\":0}} </td></tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalEstimasi + sumWorkOplDiscounted + (sumWorkOplDiscounted*(10/100)) | currency:\"\":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDp | currency:\"\":0}}</td> </tr> </tbody> </table> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-05.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <bslist data=\"oplData\" form-name=\"oplWoGR\" m-id=\"Id\" list-model=\"lmModelOpl\" on-before-new=\"onBeforeNewOpl\" on-after-delete=\"onAfterDeleteOpl\" on-before-save=\"onBeforeSaveOpl\" on-select-rows=\"onListSelectRows\" selected-rows=\"listSelectedRows\" confirm-save=\"true\" on-after-save=\"onAfterSaveOpl\" list-api=\"listApiOpl\" button-settings=\"listButtonSettings\" modal-size=\"small\" modal-title=\"Order Pekerjaan Luar\" list-title=\"Order Pekerjaan Luar\" list-height=\"200\"> <bslist-template> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <strong>{{lmItem.OPLName}}</strong> </div> <div class=\"form-group\"> {{lmItem.VendorName}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Estimasi : </label> {{lmItem.TargetFinishDate | date:'dd-MM-yyyy'}} </div> <div class=\"form-group\"> <label>Quantity : </label> {{lmItem.QtyPekerjaan}} </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Harga : </label> {{lmItem.Price | currency:\"Rp. \":0}} </div> </div> </div> </bslist-template> <bslist-modal-form> <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMasterOpl.html'\"> </div></bslist-modal-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <tbody> <tr> <td width=\"20%\">Total</td> <td>{{sumWorkOpl * (1.1) | currency:\"Rp. \":0}}</td> </tr> </tbody> </table> </div> </div> "
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-06.html',
    "<div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">INTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>KM</bsreqlabel> <input type=\"text\" step=\"1\" min=\"0\" class=\"form-control\" name=\"KM\" placeholder=\"Km\" ng-model=\"mData.Km\" ng-maxlength=\"7\" maxlength=\"7\" ng-keyup=\"givePattern(mData.Km,$event)\"> <bserrmsg field=\"km\"></bserrmsg> <p ng-if=\"isKmHistory >= 1 && nilaiKM < KMTerakhir\" style=\"font-style:italic;margin-top:5px; color:  rgb(185, 74, 72)\">Tidak boleh kurang dari {{KMTerakhir | number}} KM</p> </div> </div> <div id=\"slide\" class=\"col-md-6\"> <label for=\"slider\">Fuel</label> <div class=\"scaleWarp\"> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> <garis>|</garis> <garis style=\"font-size: 20px;font-weight:bold\">|</garis> <garis>|</garis> <!-- <span style=\" font-size: 20px;font-weight:bold;\">|</span> --> </div> <rzslider class=\"custom-slider\" rz-slider-model=\"slider.value\" rz-slider-options=\"slider.options\"></rzslider> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.BanSerep\" label=\"Ban Serep\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.CDorKaset\" label=\"CD/Kaset\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Payung\" label=\"Payung\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.Dongkrak\" label=\"Dongkrak\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.STNK\" label=\"STNK\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\"> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.P3k\" label=\"P3K\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.KunciSteer\" label=\"Kunci Steer\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ToolSet\" label=\"Tool Set\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-2 col-xs-2\"> <bscheckbox ng-model=\"mData.ClipKarpet\" label=\"Clip Karpet\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> <div class=\"col-md-4 col-xs-4\"> <bscheckbox ng-model=\"mData.BukuService\" label=\"Buku Service\" ng-click=\"\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Alarm Condition</label> <!-- <bsselect name=\"alarmCondition\" ng-model=\"mData.AlarmCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Alarm Condition\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"alarmCondition\" ng-model=\"mData.AlarmCondition\" data=\"conditionData\" placeholder=\"Pilih Alarm Condition\"> <option ng-repeat=\"conditionData in conditionData\" value=\"{{conditionData.Id}}\">{{conditionData.Name}}</option> </select> <bserrmsg field=\"alarmCondition\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>AC Condition</label> <!-- <bsselect name=\"AcCondition\" ng-model=\"mData.AcCondition\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih AC Condition\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"AcCondition\" ng-model=\"mData.AcCondition\" data=\"conditionData\" placeholder=\"Pilih AC Condition\"> <option ng-repeat=\"conditionData in conditionData\" value=\"{{conditionData.Id}}\">{{conditionData.Name}}</option> </select> <bserrmsg field=\"AcCondition\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Power Window</label> <!-- <bsselect name=\"PowerWindow\" ng-model=\"mData.PowerWindow\" data=\"conditionData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Power Window Condition\">\r" +
    "\n" +
    "                </bsselect> --> <select convert-to-number class=\"form-control\" name=\"PowerWindow\" ng-model=\"mData.PowerWindow\" data=\"conditionData\" placeholder=\"Pilih Power Window Condition\"> <option ng-repeat=\"conditionData in conditionData\" value=\"{{conditionData.Id}}\">{{conditionData.Name}}</option> </select> <bserrmsg field=\"PowerWindow\"></bserrmsg> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Other</label> <input type=\"text\" class=\"form-control\" class=\"form-control\" placeholder=\"Other\" name=\"Other\" ng-model=\"mData.OtherDescription\"> <!-- <textarea class=\"form-control\" placeholder=\"Other\" name='Other' ng-model=\"mData.OtherDescription\">\r" +
    "\n" +
    "                                </textarea> --> <bserrmsg field=\"Other\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <label>Money Amount</label> <input type=\"text\" class=\"form-control\" class=\"form-control\" placeholder=\"Money Amount\" name=\"moneyAmount\" ng-model=\"mData.moneyAmount\" ng-keyup=\"givePatternMoney(mData.moneyAmount,$event)\"> <!-- <textarea class=\"form-control\" placeholder=\"Money Amount\" name='moneyAmount' ng-model=\"mData.moneyAmount\" ng-keyup=\"givePatternMoney(mData.moneyAmount,$event)\"> \r" +
    "\n" +
    "                            </textarea> --> <bserrmsg field=\"moneyAmount\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <table style=\"width:100%\"> <tr> <th class=\"active text-center header\">EXTERIOR</th> </tr> </table> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <!-- <label>Tipe Kendaraan</label> --> <bsreqlabel>Tipe Kendaraan</bsreqlabel> <bsselect name=\"filterType\" ng-model=\"filter.vType\" data=\"VTypeData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Tipe Kendaraan\" on-select=\"chooseVType(selected)\" icon=\"fa fa-car\"> </bsselect> <!-- <select class=\"form-control\" name=\"filterType\" ng-model=\"filter.vType\" data=\"VTypeData\" placeholder=\"Pilih Tipe Kendaraan\" ng-change=\"chooseVType(filter)\" icon=\"fa fa-car\">\r" +
    "\n" +
    "                        <option ng-repeat=\"VTypeData in VTypeData\" value=\"{{VTypeData.MasterId}}\">{{VTypeData.Name}}</option>\r" +
    "\n" +
    "                    </select> --> </div> </div> </div> <div class=\"row\"> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "                <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"750\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\"\r" +
    "\n" +
    "                    ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\">\r" +
    "\n" +
    "            </div> --> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"680\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> <div class=\"col-md-12\" ng-style=\"customHeight\" ng-if=\"!isTablet\"> <img ng-init=\"this.init\" ng-areas=\"areasArray\" ng-areas-width=\"750\" ng-areas-allow=\"{'edit':true, 'move':false, 'resize':false, 'select':false, 'remove':false, 'nudge':false}\" ng-src=\"{{imageSel}}\" ng-areas-on-add=\"onAddArea\" ng-areas-on-remove=\"onRemoveArea\" ng-areas-on-change=\"onChangeAreas\" ng-areas-on-choose=\"doClick\" ng-areas-extend-data=\"statusObj\" area-api=\"areaApi\"> </div> </div> <div class=\"row\" ng-show=\"!disableWACExt\"> <div class=\"col-md-12\"> <!-- <div ui-grid=\"grid\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellNav ui-grid-pinning ui-grid-auto-resize ui-grid-edit class=\"grid\" ng-style=\"getTableHeight()\">\r" +
    "\n" +
    "                    <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>Data Belum Tersedia</div>\r" +
    "\n" +
    "                    <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div>\r" +
    "\n" +
    "                </div> --> <table id=\"wacGrid\" width=\"100%\" class=\"table table-bordered table-hover table-responsive\"> <thead> <tr> <th ng-style=\"{'width':'{{col.width}}'}\" ng-hide=\"col.visible == false\" ng-repeat=\"col in grid.columnDefs\">{{col.name}}</th> </tr> </thead> <tbody> <tr style=\"height:40px\" ng-repeat=\"column in grid.data\"> <td ng-hide=\"true\" style=\"width:5%\">{{column.JobWACExtId}}</td> <td style=\"width:55%\">{{column.ItemName}}</td> <td style=\"width:45%\" style=\"padding:0px\"> <select class=\"form-control\" name=\"status\" convert-to-number ng-change=\"onDropdownChange(column)\" ng-model=\"column.Status\" placeholder=\"Pilih Kerusakan\" style=\"height:30px\"> <option ng-repeat=\"item in DamageVehicle\" value=\"{{item.Id}}\">{{item.Name}}</option> </select> </td> <td ng-hide=\"true\" style=\"width:5%\">{{column.typeName}}</td> </tr> <tr ng-if=\"!grid.data.length\"> <td colspan=\"{{grid.columnDefs.length}}\"> <div style=\"opacity: .25;font-size: 3em;width: 100%;text-align: center;z-index: 1000\">No Data Available</div> </td> </tr> </tbody> </table> </div> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"col-md-12\"> <div class=\"col-xs-2\"> <!-- <button class=\"btn rbtn\" ng-click=\"disableExt\">Clear</button> --> <!-- <button type=\"button\" class=\"btn rbtn\" ng-model=\"disableWACExt\" uib-btn-checkbox btn-checkbox-true=\"1\" btn-checkbox-false=\"0\">Clear</button> --> <!-- <button class=\"btn rbtn\" ng-disabled=\"disableBPCenter\" skip-enable ng-click=\"\">BP Center</button> --> <!-- added by sss on 2017-09-07 --> <bscheckbox ng-model=\"disableWACExt\" label=\"Clear\" true-value=\"true\" false-value=\"false\" ng-change=\"chgChk()\"> </bscheckbox> <!-- <div class=\"\" style=\"margin-top:50px;\">\r" +
    "\n" +
    "                <bscheckbox ng-model=\"mData.isBPToCenter\" ng-disabled=\"disableBPCenter\" ng-change=\"triggerBPC()\" label=\"BP Center\" true-value=\"1\" false-value=\"0\">\r" +
    "\n" +
    "                </bscheckbox>\r" +
    "\n" +
    "            </div> --> </div> <div class=\"col-xs-8\"> <div class=\"row\" style=\"height: 300px\"> <div class=\"containeramk\" ng-style=\"{'max-width': boundingBox.width + 'px', 'max-height': boundingBox.height + 'px'}\"> <signature-pad accept=\"accept\" clear=\"clear\" style=\"height:165px\" height=\"165\" width=\"500\" dataurl=\"signature.dataUrl\"></signature-pad> <div class=\"buttonsamk\"> <button class=\"btn wbtn\" ng-disabled=\"disableReset\" skip-enable ng-click=\"clear()\">Clear</button> <button class=\"btn rbtn\" ng-click=\"signature = accept(); open(signature)\">Sign</button> </div> </div> </div> </div> <div class=\"col-xs-2\"> <image-control max-upload=\"4\" is-image=\"1\" img-upload=\"uploadFiles\" multiple> <form-image-control> <input capture=\"camera\" type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"file\" ng-init=\"mData.UploadPhoto = uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> </div> </div> </div> <div class=\"row\" style=\"margin-top:15px\"> <div class=\"col-md-12\"> <div class=\"col-xs-2\"> <div class=\"\" style=\"margin-top:50px\"> <bscheckbox ng-model=\"mData.isBPToCenter\" ng-disabled=\"disableBPCenter\" ng-change=\"triggerBPC()\" label=\"BP Center\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"col-xs-10\"> <div ng-show=\"mData.isBPToCenter == 1\" class=\"\" style=\"margin-top:50px\"> <div class=\"form-group\"> <label>Request Appointment Date</label> <bsdatepicker name=\"bpCenterDate\" date-options=\"DateOptions\" ng-model=\"bpCenterDate.date\"> </bsdatepicker> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-xs-6\"> <select class=\"form-control\" ng-show=\"mData.isBPToCenter == 1\" ng-change=\"changeBPCenter(mData.BPCenter)\" data=\"BPCenterdata\" ng-model=\"mData.BPCenter\" placeholder=\"\"> <option ng-repeat=\"bp in BPCenterdata\" value=\"{{bp.OutletIdBPCenter}}\">{{bp.Name}}</option> </select> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-07.html',
    "<div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 10px\"> <div class=\"row\"> <div class=\"col-md-12 text-right\"> <button type=\"button\" ng-click=\"editSuggest()\" ng-hide=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-pencil\"></i>&nbsp;Edit</button> <button type=\"button\" ng-click=\"cancelSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"wbtn btn\">Batal</button> <button type=\"button\" ng-click=\"SimpanJobSuggest(mData.Suggestion)\" ng-show=\"!disSgs\" class=\"rbtn btn\"><i class=\"fa fa-fw fa-check\"></i>&nbsp;Save</button> </div> </div> <div class=\"row\" style=\"margin-top: 25px\"> <div class=\"col-md-12\"> <!-- <input  ng-readonly=\"disSgs\" type=\"text\" class=\"form-control\" class=\"form-control\" placeholder=\"Job Suggestion\" name='Suggestion' ng-model=\"mData.Suggestion\" >  --> <textarea ng-readonly=\"disSgs\" ng-model=\"mData.Suggestion\" maxlength=\"300\" style=\"width: 100%;height: 100px\"></textarea> </div> </div> </div> </div>"
  );


  $templateCache.put('app/services/reception/wo/includeForm/wo-08.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label>No. Polisi</label> </div> <div class=\"form-group\"> <label>No. WO</label> </div> <div class=\"form-group\"> <label>Kategori WO</label> </div> </div> <div class=\"col-xs-4\"> <div class=\"form-group\"> <label style=\"text-transform: uppercase\">{{mDataCrm.Vehicle.LicensePlate}}</label> <!-- <label>{{NopoliceSummary}}</label> --> </div> <div class=\"form-group\"> <label ng-if=\"mData.WoNo != Null\">{{mData.WoNo}}</label> <label ng-if=\"mData.WoNo == Null\">-</label> </div> <div class=\"form-group\"> <!-- <bsselect ng-disabled=\"true\" data=\"woCategory\" name=\"categoryWO\" item-value=\"MasterId\" item-text=\"Name\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\">\r" +
    "\n" +
    "                </bsselect> --> <select class=\"form-control\" ng-disabled=\"true\" convert-to-number data=\"woCategory\" name=\"categoryWOSUM\" ng-model=\"mData.WoCategoryId\" placeholder=\"WO Category\"> <option ng-repeat=\"woCategory in woCategory\" value=\"{{woCategory.MasterId}}\">{{woCategory.Name}}</option> </select> </div> </div> </div> </div> <div class=\"col-md-12\"> <bslist data=\"gridWorkSummary\" m-id=\"TaskId\" d-id=\"PartsId\" list-model=\"lmModel\" list-detail-model=\"ldModel\" grid-detail=\"gridPartsDetailView\" confirm-save=\"false\" list-title=\"Order Pekerjaan\" modal-title=\"Job Data\" button-settings=\"listView\" custom-button-settings-detail=\"listView\" modal-title-detail=\"Parts Data\"> <bslist-template> <div class=\"col-md-6\" style=\"text-align:left\"> <p class=\"name\"><strong>{{ lmItem.TaskName }}</strong></p> <div class=\"col-md-3\"> <p>Tipe :{{lmItem.catName}}</p> </div> <div class=\"col-md-3\"> <p>Flat Rate: {{lmItem.FlatRate}}</p> </div> </div> <div class=\"col-md-6\" style=\"text-align:right\"> <p>Pembayaran: {{lmItem.PaidBy}}</p> <p>Harga: {{lmItem.Summary | currency:\"Rp. \":0}}</p> </div> </bslist-template> <bslist-detail-template> <div class=\"col-md-3\"> <p><strong>{{ ldItem.PartsCode }}</strong></p> <p>{{ ldItem.PartsName }}</p> </div> <div class=\"col-md-3\"> <p>{{ ldItem.Availbility }}</p> <p>Pembayaran : {{ ldItem.PaidBy}}</p> </div> <div class=\"col-md-3\"> <p>Jumlah : {{ldItem.Qty}} {{ldItem.Name}}</p> <p>Harga : {{ ldItem.RetailPrice | currency:\"Rp. \":0}}</p> </div> <div class=\"col-md-3\"> <p>ETA : {{ ldItem.ETA | date:'dd-MM-yyyy'}}</p> <p>Reserve : {{ ldItem.Qty }}</p> </div> </bslist-detail-template> <bslist-modal-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/reception/wo/includeForm/modalFormMaster.html'\">\r" +
    "\n" +
    "                </div> --> </div> </bslist-modal-form> <bslist-modal-detail-form> <div class=\"row\"> <!-- <div ng-include=\"'app/services/reception/wo/includeForm/modalFormDetail.html'\">\r" +
    "\n" +
    "                </div> --> </div> </bslist-modal-detail-form> </bslist> <table id=\"example\" class=\"table table-striped table-bordered\" width=\"100%\"> <thead> <tr> <th>Item</th> <th>Harga</th> <th>Disc</th> <th>Sub Total</th> </tr> </thead> <tbody> <tr> <td>Estimasi Service / Jasa</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWork | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalWorkDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{(totalWork - totalWorkDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi Material (Parts / Bahan)</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterial | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalMaterialDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ (totalMaterial - totalMaterialDiscounted) | currency:\"\":0}}</td> </tr> <tr> <td>Estimasi OPL</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOplDiscounted | currency:\"\":0}}</td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{sumWorkOpl - sumWorkOplDiscounted | currency:\"\":0}}</td> </tr> <tr> <td>PPN</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalPPN + (sumWorkOplDiscounted*(10/100)) | currency:\"\":0}} </td></tr> <tr> <td>Total Estimasi</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{ totalEstimasi + sumWorkOplDiscounted + (sumWorkOplDiscounted*(10/100)) | currency:\"\":0}}</td> </tr> <tr> <td>Total DP</td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"></td> <td style=\"text-align: right\"><span style=\"float: left\">Rp. </span>{{totalDp | currency:\"\":0}}</td> </tr> </tbody> </table> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\"> <label>Lama Pekerjaan : </label> <input type=\"text\" ng-model=\"tmpActRate\" class=\"form-control\" placeholder=\"0\" name=\"\" disabled skip-enable> Jam </div> </div> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Part Bekas</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"radio1Create\" ng-model=\"isParts\" name=\"radio1Create\" value=\"1\" checked> <label for=\"radio1Create\">Kembalikan</label> <input type=\"radio\" id=\"radio2Create\" ng-model=\"isParts\" name=\"radio1Create\" value=\"0\"> <label for=\"radio2Create\">Tinggal</label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Waiting / Drop Off</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"wait1CreateWO\" ng-model=\"isWaiting\" name=\"radio2CreateWo\" value=\"1\" checked> <label for=\"wait1CreateWO\">Waiting</label> <input type=\"radio\" id=\"wait2CreateWo\" ng-model=\"isWaiting\" name=\"radio2CreateWo\" value=\"0\"> <label for=\"wait2CreateWo\">Drop Off</label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Car Wash</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"washing1CreateWo\" ng-model=\"isWashing\" name=\"radio3CreateWo\" value=\"1\" checked> <label for=\"washing1CreateWo\">Wash</label> <input type=\"radio\" id=\"washing2CreateWo\" ng-model=\"isWashing\" name=\"radio3CreateWo\" value=\"0\"> <label for=\"washing2CreateWo\">No Wash</label> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Penggantian Parts</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group radioCustomer\"> <input type=\"radio\" id=\"changepart1CreateWo\" ng-model=\"PermissionPartChange\" name=\"radio4Createwo\" value=\"1\" checked> <label for=\"changepart1CreateWo\">Langsung</label> <input type=\"radio\" id=\"changepart2CreateWo\" ng-model=\"PermissionPartChange\" name=\"radio4Createwo\" value=\"0\"> <label for=\"changepart2CreateWo\">Izin</label> <!-- {{PermissionPartChange}} --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Metode Pembayaran</label> </div> </div> <div class=\"col-md-6\"> <!-- <bsselect name=\"paymentType\" ng-model=\"mData.PaymentMethod\" data=\"paymentTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Type Pembayaran\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" convert-to-number name=\"paymentType\" ng-model=\"mData.PaymentMethod\" data=\"paymentTypeData\" placeholder=\"Pilih Type Pembayaran\"> <option ng-repeat=\"paymentTypeData in paymentTypeData\" value=\"{{paymentTypeData.Id}}\">{{paymentTypeData.Name}}</option> </select> <bserrmsg field=\"paymentType\"></bserrmsg> </div> </div> <div class=\"row\" style=\"margin-top:25px; margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Scheduled Service Time</label> </div> <div class=\"col-md-9 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"scheduled\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.firstDate\" ng-disabled=\"true\"> </bsdatepicker> </div> <div class=\"form-group text-center\" style=\"width:20px\"> <b>-</b> </div> <div class=\"form-group\"> <bsdatepicker name=\"scheduled2\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.lastDate\" ng-disabled=\"true\"> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Estimation Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"estDate\" date-options=\"DateOptionsWithHour\" ng-model=\"mData.EstimateDate\" ng-disabled=\"true\" skip-enable> </bsdatepicker> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Adjusment Delivery Time</label> </div> <div class=\"col-md-6 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bsdatepicker name=\"adjDate\" min-date=\"minDate\" date-options=\"DateOptions\" ng-model=\"mData.AdjusmentDate\" ng-disabled=\"adjustDate\" ng-change=\"adjustTime(mData.AdjusmentDate)\" skip-enable> </bsdatepicker> </div> <div class=\"form-group\" style=\"width:200px\"> <div uib-timepicker ng-model=\"mData.AdjusmentTime\" ng-change=\"changedTime(mData.AdjusmentTime)\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"false\" show-spinners=\"false\" ng-disabled=\"adjustDate\"></div> </div> </div> <div class=\"col-md-3 form-inline\"> <div class=\"form-group\" style=\"width:200px\"> <bscheckbox ng-model=\"mData.customerRequest\" label=\"Customer Request\" ng-click=\"adjRequest(mData.customerRequest)\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <!-- <div class=\"col-md-2 form-inline\">\r" +
    "\n" +
    "       \r" +
    "\n" +
    "    </div> --> </div> <div class=\"row\" style=\"margin-bottom:10px\"> <div class=\"col-md-3\"> <label>Kode Transaksi</label> </div> <div class=\"col-md-9 form-inline\"> <!-- <bsselect ng-disabled=\"false\" data=\"codeTransaction\" name=\"codeTrans\" item-value=\"Code\" item-text=\"Description\" ng-model=\"mData.CodeTransaction\" placeholder=\"Kode Transaksi\">\r" +
    "\n" +
    "            </bsselect> --> <select class=\"form-control\" ng-disabled=\"false\" data=\"codeTransaction\" name=\"codeTrans\" ng-model=\"mData.CodeTransaction\" placeholder=\"Kode Transaksi\"> <option ng-repeat=\"codeTransaction in codeTransaction\" value=\"{{codeTransaction.Code}}\">{{codeTransaction.Description}}</option> </select> </div> </div> </div> <div class=\"row\"> <div class=\"btn-group pull-right\"> <button type=\"button\" name=\"saveWO\" class=\"rbtn btn no-animate\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" ng-click=\"saveNewWO(isParts, isWashing, isWaiting, PermissionPartChange)\"> Simpan </button> <!-- ng-disabled=\"disableRelease\" --> <button type=\"button\" name=\"releaseWO\" class=\"rbtn btn no-animate\" ng-click=\"onValidateRelease(isParts, isWashing, isWaiting, PermissionPartChange)\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" skip-enable> Release WO </button> <!-- ng-show=\"mData.JobId >= 0\" --> <button type=\"button\" name=\"CloseWO\" ng-click=\"cancelWo(mData)\" ladda=\"disableRelease\" data-style=\"expand-left\" onclick=\"this.blur()\" class=\"rbtn btn no-animate\"> Batal WO </button> </div> </div>"
  );

}]);
