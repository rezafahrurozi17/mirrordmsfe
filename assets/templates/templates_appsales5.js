angular.module('templates_appsales5', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/sales/sales5/demandSupply/approvalReceiveQuotation/approvalReceiveQuotation.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "\t$('body .modals').remove();\r" +
    "\n" +
    "}</script> <style type=\"text/css\">/* @media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "} */\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "       -moz-border-radius: 4px;\r" +
    "\n" +
    "            border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".card{background:#fff;border-radius:2px;position:relative}\r" +
    "\n" +
    ".card-2\r" +
    "\n" +
    "{box-shadow:0 3px 3px rgba(0,0,0,.1),0 2px 8px rgba(0,0,0,.1)}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <!--<script type=\"text/javascript\" src=\"js/uiscript.js\"></script>--> <div ng-show=\"ApprovalReceiveQuotationMain\" style=\"min-height: 89vh\"> <div id=\"filterData\" class=\"row\" style=\"margin:0; margin-top:5px\"> <div id=\"comboBoxFilter\" style=\"float:left; width:20%; font-size:12px\"> <div class=\"form-group filterMarg\"> <select required ng-model=\"ApprovalReceiveQuotationSearchLimit\" class=\"form-control\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionApprovalReceiveQuotationSearchLimit\" placeholder=\"Filter\"> <!--<option label=\"Filter\" value=\"\" disabled>Limit</option>--> </select> </div> </div> <div id=\"comboBoxFilter\" style=\"float:left; width:30%; font-size:12px\"> <div class=\"form-group filterMarg\"> <select required ng-model=\"ApprovalReceiveQuotationSearchCriteria\" class=\"form-control\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionApprovalReceiveQuotationSearchCriteria\" placeholder=\"Filter\"> <!--<option label=\"Filter\" value=\"\" disabled>Filter</option>--> </select> </div> </div> <div id=\"inputFilter\" style=\"float:left; width:30%\"> <div class=\"form-group filterMarg\"> <input type=\"text\" required ng-model=\"ApprovalReceiveQuotationSearchValue\" class=\"form-control\" name=\"ModelKendaraan\" placeholder=\"Search\" ng-maxlength=\"100\"> </div> </div> <div id=\"btnFilter\" style=\"float:left; width:10%\"> <button style=\"margin-right: 0px!important\" class=\"rbtn btn mobileFilterBtn\" ng-click=\"SearchApprovalReceive()\"> <i class=\"fa fa-filter iconFilter\"></i> </button> </div> <div id=\"btnFilter\" style=\"float:left; width:10%\"> <button style=\"margin-right: 0px!important\" class=\"rbtn btn mobileFilterBtn\" ng-click=\"ApprovalReceiveQuotationRefresh()\"> <i class=\"fa fa-refresh iconFilter\"></i> </button> </div> </div> <br> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Approval Receive Quotation <span></span> <span style=\"float:right; margin-right:3px\"> </span> </button> </div> <div style=\"max-height:400px;overflow-y:auto;max-width: 100%;overflow-x: hidden\" class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div aria-labelledby=\"dropdownMenu1\" class=\"list-group-item card-2\" ng-repeat=\"ContentApprovalReceiveQuotationGabungan in ApprovalReceiveQuotationGabungan\" style=\"padding:10px 0px 10px 10px;position: relative;float:left;width:100%;margin-bottom:10px\"> <div style=\"float:left;width:94%\"> <ul class=\"list-inline\" style=\"font-size:16px\"> <li><label style=\"float: left\"><input style=\"margin-right:5px\" type=\"checkbox\" name=\"selectedSuspect[]\" value=\"{{ContentApprovalReceiveQuotationGabungan}}\" ng-checked=\"selectionApprovalReceiveQuotation.indexOf(ContentApprovalReceiveQuotationGabungan) > -1\" ng-click=\"toggleSelection(ContentApprovalReceiveQuotationGabungan)\">Received Quotation:{{ContentApprovalReceiveQuotationGabungan.QuotationNo}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:16px;margin-top:-10px\"> <li><label style=\"float: left\">No.Rangka:{{ContentApprovalReceiveQuotationGabungan.FrameNo}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Cabang Pemilik:{{ContentApprovalReceiveQuotationGabungan.OutletVendorName}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Model Tipe:{{ContentApprovalReceiveQuotationGabungan.VehicleTypeDescription}}</label></li> <li><label>Warna:{{ContentApprovalReceiveQuotationGabungan.ColorName}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Tanggal Pengiriman:{{ContentApprovalReceiveQuotationGabungan.SendDate| date:\"dd MMM yyyy\"}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Status Quotation:{{ContentApprovalReceiveQuotationGabungan.QuotationStatusName}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Grand Total:Rp.{{ContentApprovalReceiveQuotationGabungan.GrandTotal| number}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Status:{{ContentApprovalReceiveQuotationGabungan.StatusApprovalName}}</label></li> </ul> </div> <div style=\"display : inline-block;position:absolute;align-items : center;float:left;height: 100%;width:6%\"> <div style=\"float:left;height: 100%;display :flex;align-items : center\"> <span ng-click=\"ActionApprovalReceiveQuotation(ContentApprovalReceiveQuotationGabungan)\" class=\"glyphicon glyphicon-option-vertical\" style=\"float:center;font-size:16px\"> </span> </div> </div> </div> </div> <div style=\"margin-top:-28px\"> <div ng-hide=\"ApprovalReceiveQuotationHideIncrement\" ng-click=\"ApprovalReceiveQuotationIncrementLimit()\" class=\"list-group-item ng-scope\" role=\"button\" align=\"center\" style=\"background-color:#e0e2e6\"> <i class=\"fa fa-angle-double-down\"></i> </div> </div> <div style=\"margin-left:0px;margin-right:0px;margin-top:25px;border:none !important\" class=\"row list-group-item\"> <button ng-disabled=\"selectionApprovalReceiveQuotation.length<=0||SelectionAman_ApprovalReceiveQuotation==false\" ng-click=\"FromListSetujuApprovalReceiveQuotation()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Setuju </span><span class=\"ladda-spinner\"></span> </button> <button ng-disabled=\"selectionApprovalReceiveQuotation.length<=0||SelectionAman_ApprovalReceiveQuotation==false\" ng-click=\"FromListTidakSetujuApprovalReceiveQuotation()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Tolak </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div ng-show=\"ApprovalReceiveQuotationDetail\"> <div class=\"row\" style=\"margin-right:0px;margin-top:10px;margin-bottom:10px\"> <button ng-show=\"mApprovalReceiveQuotation.StatusApprovalName=='Diajukan'\" ng-click=\"SetujuApprovalReceiveQuotation()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Setuju </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"mApprovalReceiveQuotation.StatusApprovalName=='Diajukan'\" ng-click=\"TidakSetujuApprovalReceiveQuotation()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Tolak </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float:right\" ng-click=\"BatalApprovalReceiveQuotation()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\"> <p> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>No. Quotation:</b></li> <li>{{mApprovalReceiveQuotation.QuotationNo}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Tanggal Quotation:</b></li> <li>{{mApprovalReceiveQuotation.QuotationDate | date:\"dd MMM yyyy\"}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Cabang Pemilik:</b></li> <li>{{mApprovalReceiveQuotation.OutletVendorName}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Tipe Quotation:</b></li> <li>{{mApprovalReceiveQuotation.QuotationTypeName}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Status Quotation:</b></li> <li>{{mApprovalReceiveQuotation.QuotationStatusName}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Alasan Batal Quotation:</b></li> <li>{{mApprovalReceiveQuotation.NoteCancel}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Status:</b></li> <li>{{mApprovalReceiveQuotation.StatusApprovalName}}</li> </ul> </p> </div> <div class=\"listData\"> <button ng-click=\"Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Informasi Pengiriman <span></span> <span style=\"float:right; margin-right:3px\"> {{Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman_PanelIcon}} </span> </button> </div> <div ng-show=\"Show_Hide_ApprovalReceiveQuotation_Detail_InformasiPengiriman\" class=\"formContent\"> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\" style=\"\"> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Nama Penerima:</b></li> <li>{{mApprovalReceiveQuotation.ReceiveName}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Alamat Pengiriman:</b></li> <li>{{mApprovalReceiveQuotation.SentAddress}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Handphone Penerima:</b></li> <li>{{mApprovalReceiveQuotation.ReceiveHP}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Tanggal Pengiriman:</b></li> <li>{{mApprovalReceiveQuotation.SentDate| date:\"dd MMM yyyy\"}}</li> </ul> </p> </div> </div> <div class=\"listData\"> <button ng-click=\"Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Info Unit Yang Akan Dibeli <span></span> <span style=\"float:right; margin-right:3px\"> {{Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli_PanelIcon}} </span> </button> </div> <div ng-show=\"Show_Hide_ApprovalReceiveQuotation_Detail_InfoUnitYangAkanDibeli\" class=\"formContent\"> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\" style=\"\"> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Model Tipe:</b></li> <li>{{mApprovalReceiveQuotation.VehicleTypeDescription}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Warna:</b></li> <li>{{mApprovalReceiveQuotation.ColorName}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>No. Rangka:</b></li> <li>{{mApprovalReceiveQuotation.FrameNo}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Tahun Produksi:</b></li> <li>{{mApprovalReceiveQuotation.ProductionYear}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Lokasi Unit:</b></li> <li>{{mApprovalReceiveQuotation.LastLocation}}</li> </ul> </p> </div> </div> <div class=\"listData\"> <button ng-click=\"Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Rincian Harga <span></span> <span style=\"float:right; margin-right:3px\"> {{Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga_PanelIcon}} </span> </button> </div> <div ng-show=\"Show_Hide_ApprovalReceiveQuotation_Detail_RincianHarga\" class=\"formContent\"> <!--<div ng-include src=\"'app/sales/demandSupply/approvalReceiveQuotation/approvalReceiveQuotationRincianHarga.html'\"></div>--> <div id=\"tabContainer\"> <table style=\"width:100%\"> <tr> <th style=\"width: 30%\">Unit {{mApprovalReceiveQuotation.VehicleTypeDescription}}</th> <th style=\"width: 30%\"></th> <th style=\"width: 30%\"></th> </tr> <tr> <td> <p style=\"float:left\">Harga Kendaraan</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.SellPrice + mApprovalReceiveQuotation.ExpeditionSellPrice |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Diskon</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.Discount + mApprovalReceiveQuotation.ExpeditionDiscount |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Harga Stlh. Diskon</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{(mApprovalReceiveQuotation.SellPrice + mApprovalReceiveQuotation.ExpeditionSellPrice - mApprovalReceiveQuotation.Discount- mApprovalReceiveQuotation.ExpeditionDiscount) |number:2}}</p> </td> </tr> <!--<tr>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:left \">DPP</P>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:right \">Rp.</p>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:right \">{{mApprovalReceiveQuotation.TotalDPP |number:2}}</p>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                    </tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:left \">PPN</P>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:right \">Rp.</p>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:right \">{{mApprovalReceiveQuotation.TotalVAT |number:2}}</p>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                    </tr>--> <tr> <td> <p style=\"float:left\">PPH22</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.PPH22 |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Biaya Ekspedisi TAM</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.TAMExpedition |number:2}}</p> </td> </tr> </table> <!--<table ng-show=\"mApprovalReceiveQuotation.GrandTotal > 0\" style=\"width: 100%;background-color: #d53337 ;color: white;padding: 3px \">\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <th style=\"width: 30% \"></th>\r" +
    "\n" +
    "                                <th style=\"width: 30% \"></th>\r" +
    "\n" +
    "                                <th style=\"width: 30% \"></th>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td><b style=\"float:left \">Total Harga</b></td>\r" +
    "\n" +
    "                                <td><b style=\"float:right \">Rp.</b></td>\r" +
    "\n" +
    "                                <td><b style=\"float:right \"><u>{{mApprovalReceiveQuotation.GrandTotal |number:2}},-</u></b></td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </table>--> </div> </div> </div> <div class=\"ui modal ModalAlasanPenolakanApprovalReceiveQuotation\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"BatalAlasanApprovalReceiveQuotation()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Alasan Penolakan</h4> </div> <br> <div class=\"modal-body\"> <textarea style=\"width:100%\" rows=\"10\" cols=\"80\" maxlength=\"512\" placeholder=\"input alasan...\" ng-model=\"mApprovalReceiveQuotation.RejectReason\"></textarea> </div> <div class=\"modal-footer\"> <!--<button onclick=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"SubmitAlasanApprovalReceiveQuotation()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-disabled=\"mApprovalReceiveQuotation.RejectReason==undefined\">Simpan</button> <button ng-click=\"BatalAlasanApprovalReceiveQuotation()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div> <div class=\"ui modal ModalActionApprovalReceiveQuotation\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"BatalActionApprovalReceiveQuotation()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <!--<h4 class=\"modal-title\">Apa Yang Ingin Anda Lakukan?</h4>--> </div> <br> <div class=\"modal-body\"> <a ng-click=\"SelectApprovalReceiveQuotation()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Lihat Detail</li> </a> <!--<a ng-click=\"SetujuApprovalReceiveQuotation()\">\r" +
    "\n" +
    "                    <li class=\"list-group-item item-modal\">\r" +
    "\n" +
    "                        <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Setuju</li>\r" +
    "\n" +
    "                </a>\r" +
    "\n" +
    "\t\t\t\t<a ng-click=\"TidakSetujuApprovalReceiveQuotation()\">\r" +
    "\n" +
    "                    <li class=\"list-group-item item-modal\">\r" +
    "\n" +
    "                        <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Reject</li>\r" +
    "\n" +
    "                </a>--> </div> <div class=\"modal-footer\"> <!--<button ng-click=\"BatalActionApprovalReceiveQuotation()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right;\">Batal</button>--> </div> </div> <div class=\"ui modal ModalApprovalReceiveQuotationActionApprovalMulti\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"ModalApprovalReceiveQuotationActionApprovalMultiBatal()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Receive Quotation</h4> </div> <br> <br> <div class=\"modal-body\"> <p>Berikut Ini Harga Ekspedisi dari TAM</p> <p> <table border=\"1\"> <tr> <th>No.</th> <th>No. Quotation</th> <th>No. Rangka</th> <th>Harga Ekspedisi TAM</th> </tr> <tr ng-repeat=\"IndividualselectionApprovalReceiveQuotationBuatApprove in selectionApprovalReceiveQuotationTabel\"> <td>{{$index+1}}</td> <td>{{IndividualselectionApprovalReceiveQuotationBuatApprove.QuotationNo}}</td> <td>{{IndividualselectionApprovalReceiveQuotationBuatApprove.FrameNo}}</td> <td>{{IndividualselectionApprovalReceiveQuotationBuatApprove.TAMExpedition}}</td> </tr> </table> </p> <p> Apakah Anda Menyetujui harga diatas? </p> </div> <div class=\"modal-footer\"> <button ng-click=\"ModalApprovalReceiveQuotationActionApprovalMultiApprove()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"ModalApprovalReceiveQuotationActionApprovalMultiBatal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div> <div class=\"ui modal ModalApprovalReceiveQuotationActionApprovalIndividual\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"ModalApprovalReceiveQuotationActionApprovalIndividualBatal()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Receive Quotation</h4> </div> <br> <br> <div class=\"modal-body\"> <p>Berikut Ini Harga Ekspedisi dari TAM</p> <p> <table border=\"1\"> <tr> <th>No.</th> <th>Quotation No</th> <th>Frame No</th> <th>Harga Ekspedisi TAM</th> </tr> <tr> <td>1</td> <td>{{mApprovalReceiveQuotation.QuotationNo}}</td> <td>{{mApprovalReceiveQuotation.FrameNo}}</td> <td>{{mApprovalReceiveQuotation.TAMExpedition}}</td> </tr> </table> </p> <p> Apakah Anda Menyetujui harga diatas? </p> </div> <div class=\"modal-footer\"> <button ng-click=\"ModalApprovalReceiveQuotationActionApprovalIndividualApprove()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"ModalApprovalReceiveQuotationActionApprovalIndividualBatal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/approvalReceiveQuotation/approvalReceiveQuotationRincianHarga.html',
    "<style type=\"text/css\">/*@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    html,\r" +
    "\n" +
    "    body {\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        margin: 0;\r" +
    "\n" +
    "        padding: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    */\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .BodyRows{\r" +
    "\n" +
    "    line-height: 20px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ColOne{\r" +
    "\n" +
    "        border:none !Important; \r" +
    "\n" +
    "        text-align:left !Important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ColTwo{\r" +
    "\n" +
    "        border:none !Important; \r" +
    "\n" +
    "        text-align:right !Important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ColThree\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        border:none !Important;\r" +
    "\n" +
    "        text-align:right !Important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ColLine\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        border: 1px solid #000;\r" +
    "\n" +
    "    } \r" +
    "\n" +
    "    .containersign {\r" +
    "\n" +
    "        /*padding: 10px 10px 40px 10px;*/\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        margin: 0 auto 0 auto;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .signature {\r" +
    "\n" +
    "        border: 1px solid dimgrey;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .buttons{\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .signature_container{\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .signature canvas {\r" +
    "\n" +
    "        border: 1px solid #999;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .buttons {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        /*bottom: 10px;\r" +
    "\n" +
    "        left: 10px;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .sizes {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        /*bottom: 10px;\r" +
    "\n" +
    "        right: 10px;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .sizes input {\r" +
    "\n" +
    "        width: 4em;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .result {\r" +
    "\n" +
    "        border: 1px solid blue;\r" +
    "\n" +
    "        margin: 30px auto 0 auto;\r" +
    "\n" +
    "        height: 220px;\r" +
    "\n" +
    "        width: 568px;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/signature.js\"></script> <script type=\"text/javascript\" src=\"js/signature_pad.js\"></script> <div id=\"tabContainer\"> <table style=\"width:100%\"> <tr> <th style=\"width: 30%\">Unit {{mApprovalReceiveQuotation.VehicleTypeDescription}}</th> <th style=\"width: 30%\"></th> <th style=\"width: 30%\"></th> </tr> <tr> <td> <p style=\"float:left\">Harga Kendaraan</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.SellPrice |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Diskon</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.Discount |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Harga Stlh. Diskon</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{(mApprovalReceiveQuotation.SellPrice-mApprovalReceiveQuotation.Discount) |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">DPP</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.DPP |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">PPN</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.VAT |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">PPH22</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{mApprovalReceiveQuotation.PPH22 |number:2}}</p> </td> </tr> </table> <table ng-show=\"mApprovalReceiveQuotation.GrandTotal > 0\" style=\"width: 100%;background-color: #d53337 ;color: white;padding: 3px\"> <tr> <th style=\"width: 30%\"></th> <th style=\"width: 30%\"></th> <th style=\"width: 30%\"></th> </tr> <!--<tr>\r" +
    "\n" +
    "                                        <td><b style=\"float:left \">Total Per Unit</b></td>\r" +
    "\n" +
    "                                        <td><b style=\"float:right \">Rp.</b></td>\r" +
    "\n" +
    "                                        <td><b style=\"float:right \">184.000.000,-</b></td>\r" +
    "\n" +
    "                                    </tr>\r" +
    "\n" +
    "                                    <tr>\r" +
    "\n" +
    "                                        <td><b style=\"float:left \">Jumlah Unit Yang Dibeli</b></td>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                        <td><b style=\"float:right \">1</b></td>\r" +
    "\n" +
    "                                    </tr>--> <tr> <td><b style=\"float:left\">Total Harga</b></td> <td><b style=\"float:right\">Rp.</b></td> <td><b style=\"float:right\"><u>{{mApprovalReceiveQuotation.Total |number:2}},-</u></b></td> </tr> </table> </div>  "
  );


  $templateCache.put('app/sales/sales5/demandSupply/approvalSendingQuotation/approvalSendingQuotation.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "\t$('body .modals').remove();\r" +
    "\n" +
    "}</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "       -moz-border-radius: 4px;\r" +
    "\n" +
    "            border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".card{background:#fff;border-radius:2px;position:relative}\r" +
    "\n" +
    ".card-2\r" +
    "\n" +
    "{box-shadow:0 3px 3px rgba(0,0,0,.1),0 2px 8px rgba(0,0,0,.1)}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <!--<script type=\"text/javascript\" src=\"js/uiscript.js\"></script>--> <div ng-show=\"ApprovalSendingQuotationMain\" style=\"min-height: 89vh\"> <div id=\"filterData\" class=\"row\" style=\"margin:0; margin-top:5px\"> <div id=\"comboBoxFilter\" style=\"float:left; width:20%; font-size:12px\"> <div class=\"form-group filterMarg\"> <select required ng-model=\"ApprovalSendingQuotationSearchLimit\" class=\"form-control\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionApprovalSendingQuotationSearchLimit\" placeholder=\"Filter\"> <!--<option label=\"Filter\" value=\"\" disabled>Limit</option>--> </select> </div> </div> <div id=\"comboBoxFilter\" style=\"float:left; width:30%; font-size:12px\"> <div class=\"form-group filterMarg\"> <select required ng-model=\"ApprovalSendingQuotationSearchCriteria\" class=\"form-control\" ng-options=\"item.SearchOptionId as item.SearchOptionName for item in optionApprovalSendingQuotationSearchCriteria\" placeholder=\"Filter\"> <!--<option label=\"Filter\" value=\"\" disabled>Filter</option>--> </select> </div> </div> <div id=\"inputFilter\" style=\"float:left; width:30%\"> <div class=\"form-group filterMarg\"> <input type=\"text\" required ng-model=\"ApprovalSendingQuotationSearchValue\" class=\"form-control\" name=\"ModelKendaraan\" placeholder=\"Search\" ng-maxlength=\"100\"> </div> </div> <div id=\"btnFilter\" style=\"float:left; width:10%\"> <button style=\"margin-right: 0px!important\" class=\"rbtn btn mobileFilterBtn\" ng-click=\"SearchApprovalSending()\"> <i class=\"fa fa-filter iconFilter\"></i> </button> </div> <div id=\"btnFilter\" style=\"float:left; width:10%\"> <button style=\"margin-right: 0px!important\" class=\"rbtn btn mobileFilterBtn\" ng-click=\"ApprovalSendingQuotationRefresh()\"> <i class=\"fa fa-refresh iconFilter\"></i> </button> </div> </div> <br> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Approval Sending Quotation <span></span> <span style=\"float:right; margin-right:3px\"> </span> </button> </div> <div style=\"max-height:400px;overflow-y:auto;max-width: 100%;overflow-x: hidden\" class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div aria-labelledby=\"dropdownMenu1\" class=\"list-group-item card-2\" ng-repeat=\"ContentApprovalSendingQuotationGabungan in ApprovalSendingQuotationGabungan\" style=\"padding:10px 0px 10px 10px;position: relative;float:left;width:100%;margin-bottom:10px\"> <div style=\"float:left;width:94%\"> <ul class=\"list-inline\" style=\"font-size:16px\"> <li><label style=\"float: left\"><input style=\"margin-right:5px\" type=\"checkbox\" name=\"selectedSuspect[]\" value=\"{{ContentApprovalSendingQuotationGabungan}}\" ng-checked=\"selectionApprovalSendingQuotation.indexOf(ContentApprovalSendingQuotationGabungan) > -1\" ng-click=\"toggleSelection(ContentApprovalSendingQuotationGabungan)\">Sending Quotation:{{ContentApprovalSendingQuotationGabungan.QuotationNo}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:16px;margin-top:-10px\"> <li><label style=\"float: left\">No.Rangka: {{ContentApprovalSendingQuotationGabungan.FrameNo}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Cabang Yang Meminta: {{ContentApprovalSendingQuotationGabungan.OutletRequestorName}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Model Tipe: {{ContentApprovalSendingQuotationGabungan.VehicleTypeDescription}}</label></li> <li><label>Warna: {{ContentApprovalSendingQuotationGabungan.ColorName}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Tanggal Pengiriman: {{ContentApprovalSendingQuotationGabungan.SentDate| date:\"dd MMM yyyy\"}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Status Quotation: {{ContentApprovalSendingQuotationGabungan.QuotationStatusName}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Grand Total: Rp.{{ContentApprovalSendingQuotationGabungan.GrandTotal| number}}</label></li> </ul> <ul class=\"list-inline\" style=\"font-size:14px;margin-top:-10px\"> <li><label>Status: {{ContentApprovalSendingQuotationGabungan.StatusApprovalName}}</label></li> </ul> </div> <div style=\"display : inline-block;position:absolute;align-items : center;float:left;height: 100%;width:6%\"> <div style=\"float:left;height: 100%;display :flex;align-items : center\"> <span ng-click=\"ActionApprovalSendingQuotation(ContentApprovalSendingQuotationGabungan)\" class=\"glyphicon glyphicon-option-vertical\" style=\"float:center;font-size:16px\"> </span> </div> </div> </div> </div> <div style=\"margin-top:-28px\"> <div ng-hide=\"ApprovalSendingQuotationHideIncrement\" ng-click=\"ApprovalSendingQuotationIncrementLimit()\" class=\"list-group-item ng-scope\" role=\"button\" align=\"center\" style=\"background-color:#e0e2e6\"> <i class=\"fa fa-angle-double-down\"></i> </div> </div> <div style=\"margin-left:0px;margin-right:0px;margin-top:25px;border:none !important\" class=\"row list-group-item\"> <button ng-disabled=\"selectionApprovalSendingQuotation.length<=0||SelectionAman_ApprovalSendingQuotation==false\" ng-click=\"FromListSetujuApprovalSendingQuotation()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Setuju </span><span class=\"ladda-spinner\"></span> </button> <button ng-disabled=\"selectionApprovalSendingQuotation.length<=0||SelectionAman_ApprovalSendingQuotation==false\" ng-click=\"FromListTidakSetujuApprovalSendingQuotation()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Tolak </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div ng-show=\"ApprovalSendingQuotationDetail\"> <div class=\"row\" style=\"margin-right:0px;margin-top:10px;margin-bottom:10px\"> <button ng-show=\"mApprovalSendingQuotation.StatusApprovalName=='Diajukan'\" ng-click=\"SetujuApprovalSendingQuotation()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Setuju </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"mApprovalSendingQuotation.StatusApprovalName=='Diajukan'\" ng-click=\"TidakSetujuApprovalSendingQuotation()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Tolak </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float:right\" ng-click=\"BatalApprovalSendingQuotation()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> <!--<i class=\"fa fa-fw fa-refresh\"></i>-->Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"row\"> <p> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>No. Quotation:</b></li> <li>{{mApprovalSendingQuotation.QuotationNo}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Tanggal Quotation:</b></li> <li>{{mApprovalSendingQuotation.QuotationDate | date:\"dd MMM yyyy\"}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Cabang Yang Meminta:</b></li> <li>{{mApprovalSendingQuotation.OutletRequestorName}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Tipe Quotation:</b></li> <li>{{mApprovalSendingQuotation.QuotationTypeName}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Status Quotation:</b></li> <li>{{mApprovalSendingQuotation.QuotationStatusName}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Alasan Batal Quotation:</b></li> <li>{{mApprovalSendingQuotation.NoteCancel}}</li> </ul> <ul class=\"list-inline\" style=\"margin-left: 10px;font-size: 14px\"> <li><b>Status:</b></li> <li>{{mApprovalSendingQuotation.StatusApprovalName}}</li> </ul> </p> </div> <div class=\"listData\"> <button ng-click=\"Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Informasi Pengiriman <span></span> <span style=\"float:right; margin-right:3px\"> {{Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman_PanelIcon}} </span> </button> </div> <div ng-show=\"Show_Hide_ApprovalSendingQuotation_Detail_InformasiPengiriman\" class=\"formContent\"> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\" style=\"\"> <p> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Nama Penerima:</b></li> <li>{{mApprovalSendingQuotation.ReceiveName}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Alamat Pengiriman:</b></li> <li>{{mApprovalSendingQuotation.SentAddress}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Handphone Penerima:</b></li> <li>{{mApprovalSendingQuotation.ReceiveHP}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Tanggal Pengiriman:</b></li> <li>{{mApprovalSendingQuotation.SentDate| date:\"dd MMM yyyy\"}}</li> </ul> </p> </div> </div> <div class=\"listData\"> <button ng-click=\"Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Info Unit Yang Akan Dibeli <span></span> <span style=\"float:right; margin-right:3px\"> {{Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli_PanelIcon}} </span> </button> </div> <div ng-show=\"Show_Hide_ApprovalSendingQuotation_Detail_InfoUnitYangAkanDibeli\" class=\"formContent\"> <div class=\"list-group\" id=\"dropdownContent\" aria-labelledby=\"dropdownMenu1\" style=\"\"> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Model Tipe:</b></li> <li>{{mApprovalSendingQuotation.VehicleTypeDescription}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Warna:</b></li> <li>{{mApprovalSendingQuotation.ColorName}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>No. Rangka:</b></li> <li>{{mApprovalSendingQuotation.FrameNo}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Tahun Produksi:</b></li> <li>{{mApprovalSendingQuotation.ProductionYear}}</li> </ul> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li><b>Lokasi Unit:</b></li> <li>{{mApprovalSendingQuotation.LastLocation}}</li> </ul> </p> </div> </div> <div class=\"listData\"> <button ng-click=\"Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga_Clicked()\" class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Rincian Harga <span></span> <span style=\"float:right; margin-right:3px\"> {{Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga_PanelIcon}} </span> </button> </div> <div ng-show=\"Show_Hide_ApprovalSendingQuotation_Detail_RincianHarga\" class=\"formContent\"> <!--<div ng-include src=\"'app/sales/demandSupply/approvalSendingQuotation/approvalSendingQuotationRincianHarga.html'\"></div>--> <div id=\"tabContainer\"> <table style=\"width:100%\"> <tr> <th style=\"width: 30%\">Unit {{selected_data_ApprovalSendingQuotation.VehicleTypeDescription}}</th> <th style=\"width: 30%\"></th> <th style=\"width: 30%\"></th> </tr> <tr> <td> <p style=\"float:left\">Harga Kendaraan</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.SellPrice + selected_data_ApprovalSendingQuotation.ExpeditionSellPrice |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Diskon</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.Discount + selected_data_ApprovalSendingQuotation.ExpeditionDiscount |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Harga Stlh. Diskon</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{(selected_data_ApprovalSendingQuotation.SellPrice + selected_data_ApprovalSendingQuotation.ExpeditionSellPrice - selected_data_ApprovalSendingQuotation.Discount - selected_data_ApprovalSendingQuotation.ExpeditionDiscount) |number:2}}</p> </td> </tr> <!--<tr>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:left \">DPP</P>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:right \">Rp.</p>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:right \">{{selected_data_ApprovalSendingQuotation.TotalDPP |number:2}}</p>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                    </tr>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<tr>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:left \">PPN</P>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:right \">Rp.</p>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                        <td>\r" +
    "\n" +
    "                                            <p style=\"float:right \">{{selected_data_ApprovalSendingQuotation.TotalVAT |number:2}}</p>\r" +
    "\n" +
    "                                        </td>\r" +
    "\n" +
    "                                    </tr>--> <tr> <td> <p style=\"float:left\">PPH22</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.PPH22 |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Biaya Ekspedisi TAM</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.TAMExpedition |number:2}}</p> </td> </tr> </table> <!--<table ng-show=\"selected_data_ApprovalSendingQuotation.GrandTotal > 0\" style=\"width: 100%;background-color: #d53337 ;color: white;padding: 3px \">\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <th style=\"width: 30% \"></th>\r" +
    "\n" +
    "                                <th style=\"width: 30% \"></th>\r" +
    "\n" +
    "                                <th style=\"width: 30% \"></th>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                            <tr>\r" +
    "\n" +
    "                                <td><b style=\"float:left \">Total Harga</b></td>\r" +
    "\n" +
    "                                <td><b style=\"float:right \">Rp.</b></td>\r" +
    "\n" +
    "                                <td><b style=\"float:right \"><u>{{selected_data_ApprovalSendingQuotation.GrandTotal |number:2}},-</u></b></td>\r" +
    "\n" +
    "                            </tr>\r" +
    "\n" +
    "                        </table>--> </div> </div> </div> <div class=\"ui modal ModalAlasanPenolakanApprovalSendingQuotation\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"BatalAlasanApprovalSendingQuotation()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Alasan Penolakan</h4> </div> <br> <div class=\"modal-body\"> <textarea style=\"width:100%\" rows=\"10\" cols=\"80\" maxlength=\"512\" placeholder=\"input alasan...\" ng-model=\"mApprovalSendingQuotation.RejectReason\"></textarea> </div> <div class=\"modal-footer\"> <!--<button onclick=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"SubmitAlasanApprovalSendingQuotation()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-disabled=\"mApprovalSendingQuotation.RejectReason==undefined\">Simpan</button> <button ng-click=\"BatalAlasanApprovalSendingQuotation()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div> <div class=\"ui modal ModalActionApprovalSendingQuotation\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"BatalActionApprovalSendingQuotation()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <!--<h4 class=\"modal-title\">Apa Yang Ingin Anda Lakukan?</h4>--> </div> <br> <div class=\"modal-body\"> <a ng-click=\"SelectApprovalSendingQuotation()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Lihat Detail</li> </a> <!--<a ng-click=\"SetujuApprovalSendingQuotation()\">\r" +
    "\n" +
    "                    <li class=\"list-group-item item-modal\">\r" +
    "\n" +
    "                        <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Setuju</li>\r" +
    "\n" +
    "                </a>\r" +
    "\n" +
    "\t\t\t\t<a ng-click=\"TidakSetujuApprovalSendingQuotation()\">\r" +
    "\n" +
    "                    <li class=\"list-group-item item-modal\">\r" +
    "\n" +
    "                        <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Reject</li>\r" +
    "\n" +
    "                </a>--> </div> <div class=\"modal-footer\"> <!--<button ng-click=\"BatalActionApprovalSendingQuotation()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right;\">Batal</button>--> </div> </div> <div class=\"ui modal ModalApprovalSendingQuotationActionApprovalMulti\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"ModalApprovalSendingQuotationActionApprovalMultiBatal()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Sending Quotation</h4> </div> <div class=\"modal-body\"> <p>Berikut Ini Harga Ekspedisi dari TAM</p> <p> <table border=\"1\" style=\"width:100%\"> <tr> <th>No.</th> <th>No. Quotation</th> <th>No. Rangka</th> <th>Harga Ekspedisi TAM</th> </tr> <tr ng-repeat=\"IndividualselectionApprovalSendingQuotationBuatApprove in selectionApprovalSendingQuotation\"> <td>{{$index+1}}</td> <td>{{IndividualselectionApprovalSendingQuotationBuatApprove.QuotationNo}}</td> <td>{{IndividualselectionApprovalSendingQuotationBuatApprove.FrameNo}}</td> <td>{{IndividualselectionApprovalSendingQuotationBuatApprove.TAMExpedition}}</td> </tr> </table> </p> <p> Apakah Anda Menyetujui harga diatas? </p> </div> <div class=\"modal-footer\"> <button ng-click=\"ModalApprovalSendingQuotationActionApprovalMultiApprove()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"ModalApprovalSendingQuotationActionApprovalMultiBatal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div> <div class=\"ui modal ModalApprovalSendingQuotationActionApprovalIndividual\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" ng-click=\"ModalApprovalSendingQuotationActionApprovalIndividualBatal()\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Sending Quotation</h4> </div> <br> <br> <div class=\"modal-body\"> <p>Berikut Ini Harga Ekspedisi dari TAM</p> <p> <table border=\"1\"> <tr> <th>No.</th> <th>No. Quotation</th> <th>Frame No</th> <th>Harga Ekspedisi TAM</th> </tr> <tr> <td>1</td> <td>{{mApprovalSendingQuotation.QuotationNo}}</td> <td>{{mApprovalSendingQuotation.FrameNo}}</td> <td>{{mApprovalSendingQuotation.TAMExpedition}}</td> </tr> </table> </p> <p> Apakah Anda Menyetujui harga diatas? </p> </div> <div class=\"modal-footer\"> <button ng-click=\"ModalApprovalSendingQuotationActionApprovalIndividualApprove()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Ya</button> <button ng-click=\"ModalApprovalSendingQuotationActionApprovalIndividualBatal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Tidak</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/approvalSendingQuotation/approvalSendingQuotationRincianHarga.html',
    "<style type=\"text/css\">/*@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    html,\r" +
    "\n" +
    "    body {\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        margin: 0;\r" +
    "\n" +
    "        padding: 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    */\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .BodyRows{\r" +
    "\n" +
    "    line-height: 20px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ColOne{\r" +
    "\n" +
    "        border:none !Important; \r" +
    "\n" +
    "        text-align:left !Important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ColTwo{\r" +
    "\n" +
    "        border:none !Important; \r" +
    "\n" +
    "        text-align:right !Important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ColThree\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        border:none !Important;\r" +
    "\n" +
    "        text-align:right !Important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .ColLine\r" +
    "\n" +
    "    {\r" +
    "\n" +
    "        border: 1px solid #000;\r" +
    "\n" +
    "    } \r" +
    "\n" +
    "    .containersign {\r" +
    "\n" +
    "        /*padding: 10px 10px 40px 10px;*/\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        margin: 0 auto 0 auto;\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .signature {\r" +
    "\n" +
    "        border: 1px solid dimgrey;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .buttons{\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .signature_container{\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .signature canvas {\r" +
    "\n" +
    "        border: 1px solid #999;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .buttons {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        /*bottom: 10px;\r" +
    "\n" +
    "        left: 10px;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .sizes {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        /*bottom: 10px;\r" +
    "\n" +
    "        right: 10px;*/\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .containersign .sizes input {\r" +
    "\n" +
    "        width: 4em;\r" +
    "\n" +
    "        text-align: center;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .result {\r" +
    "\n" +
    "        border: 1px solid blue;\r" +
    "\n" +
    "        margin: 30px auto 0 auto;\r" +
    "\n" +
    "        height: 220px;\r" +
    "\n" +
    "        width: 568px;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/signature.js\"></script> <script type=\"text/javascript\" src=\"js/signature_pad.js\"></script> <div id=\"tabContainer\"> <table style=\"width:100%\"> <tr> <th style=\"width: 30%\">Unit {{selected_data_ApprovalSendingQuotation.VehicleTypeDescription}}</th> <th style=\"width: 30%\"></th> <th style=\"width: 30%\"></th> </tr> <tr> <td> <p style=\"float:left\">Harga Kendaraan</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.SellPrice |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Diskon</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.Discount |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">Harga Stlh. Diskon</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{(selected_data_ApprovalSendingQuotation.SellPrice-selected_data_ApprovalSendingQuotation.Discount) |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">DPP</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.DPP |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">PPN</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.VAT |number:2}}</p> </td> </tr> <tr> <td> <p style=\"float:left\">PPH22</p> </td> <td> <p style=\"float:right\">Rp.</p> </td> <td> <p style=\"float:right\">{{selected_data_ApprovalSendingQuotation.PPH22 |number:2}}</p> </td> </tr> </table> <table ng-show=\"selected_data_ApprovalSendingQuotation.GrandTotal > 0\" style=\"width: 100%;background-color: #d53337 ;color: white;padding: 3px\"> <tr> <th style=\"width: 30%\"></th> <th style=\"width: 30%\"></th> <th style=\"width: 30%\"></th> </tr> <!--<tr>\r" +
    "\n" +
    "                                        <td><b style=\"float:left \">Total Per Unit</b></td>\r" +
    "\n" +
    "                                        <td><b style=\"float:right \">Rp.</b></td>\r" +
    "\n" +
    "                                        <td><b style=\"float:right \">184.000.000,-</b></td>\r" +
    "\n" +
    "                                    </tr>\r" +
    "\n" +
    "                                    <tr>\r" +
    "\n" +
    "                                        <td><b style=\"float:left \">Jumlah Unit Yang Dibeli</b></td>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                        <td><b style=\"float:right \">1</b></td>\r" +
    "\n" +
    "                                    </tr>--> <tr> <td><b style=\"float:left\">Total Harga</b></td> <td><b style=\"float:right\">Rp.</b></td> <td><b style=\"float:right\"><u>{{selected_data_ApprovalSendingQuotation.Total |number:2}},-</u></b></td> </tr> </table> </div>  "
  );


  $templateCache.put('app/sales/sales5/demandSupply/approvalSwappingHo/approvalSwappingHo.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "       -moz-border-radius: 4px;\r" +
    "\n" +
    "            border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <!--<script type=\"text/javascript\" src=\"js/uiscript.js\"></script>--> <bsform-grid ng-form=\"ApprovalSwappingHoForm\" factory-name=\"ApprovalSwappingHoFactory\" model=\"mApprovalSwappingHo\" model-id=\"SpkId\" loading=\"loading\" hide-new-button=\"true\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ApprovalSwappingHoForm\" form-title=\"Approval Swapping Ho\" modal-title=\"Approval Swapping Ho\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> </bsform-grid> <div class=\"ui modal ModalAlasanPenolakanApprovalSwappingHo\" role=\"dialog\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Alasan Penolakan</h4> </div> <br> <div class=\"modal-body\"> <textarea rows=\"10\" cols=\"80\" maxlength=\"512\" placeholder=\"input alasan...\" ng-model=\"mApprovalSwappingHo.ApprovalNote\"></textarea> </div> <div class=\"modal-footer\"> <!--<button onclick=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"SubmitAlasanApprovalSwappingHo()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-disabled=\"mApprovalSwappingHo.ApprovalNote==undefined\">Simpan</button> <button ng-click=\"BatalAlasanApprovalSwappingHo()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/dsmaintainPO/dsmaintainPO.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">fieldset.scheduler-border {\r" +
    "\n" +
    "        border: 1px groove #ddd !important;\r" +
    "\n" +
    "        padding: 0 1em 2em 2em !important;\r" +
    "\n" +
    "        margin: 10px;\r" +
    "\n" +
    "        -webkit-box-shadow: 0px 0px 0px 0px #000;\r" +
    "\n" +
    "        box-shadow: 0px 0px 0px 0px #000;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    legend.scheduler-border {\r" +
    "\n" +
    "        width: inherit;\r" +
    "\n" +
    "        /* Or auto */\r" +
    "\n" +
    "        padding: 0 10px;\r" +
    "\n" +
    "        /* To give a bit of padding on the left and right */\r" +
    "\n" +
    "        border-bottom: none;\r" +
    "\n" +
    "    }</style> <div ng-show=\"MaintainForm\"> <bsform-grid ng-form=\"MaintainPOForm\" factory-name=\"DsMaintainPOFactory\" model=\"mDsMaintainPO\" model-id=\"POId\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" hide-new-button=\"true\" form-name=\"MaintainPOForm\" form-title=\"SO\" modal-title=\"MaintainPO\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal PO</bsreqlabel> <bsdatepicker ng-change=\"DsMaintainPoJagaFilterTanggal()\" ng-model=\"filter.PODateStart\" name=\"tanggal\" cdate-options=\"dateOptions\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Sampai Tanggal</bsreqlabel> <bsdatepicker ng-change=\"DsMaintainPoJagaFilterTanggal()\" ng-model=\"filter.PODateEnd\" name=\"tanggal\" cdate-options=\"dateOptions\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"LihatForm\"> <button id=\"btnKembaliFormMaintain\" ng-click=\"btnKembaliFormMaintain();\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px; width:11%\">Kembali</button> <fieldset ng-disabled=\"true\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Type PO</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.POTypeName\" ng-maxlength=\"30\" required> </div> <!-- <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.TypeId\" data=\"getTypePO\" item-text=\"Type\" item-value=\"TypeId\" on-select=\"TypeId\" placeholder=\"Type\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                    </bsselect> --> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Referensi PO</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.POReferenceName\" ng-maxlength=\"30\" required> </div> <!-- <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.TypeId\" data=\"selectionItem\" item-text=\"Type\" item-value=\"TypeId\" on-select=\"TypeId\" placeholder=\"Type\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                    </bsselect> --> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Kode Vendor</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.VendorName\" ng-maxlength=\"30\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Type Material</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.MaterialTypeName\" ng-maxlength=\"30\" required> </div> <!-- <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.TypeId\" data=\"selectionItem\" item-text=\"Type\" item-value=\"TypeId\" on-select=\"TypeId\" placeholder=\"Type\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                    </bsselect> --> </div> </div> </div> <hr> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Quotation No</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.QuotationNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\"> <label>Warna</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.ColorName\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\"> <label>Lokasi Unit</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.LastLocation\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tanggal Pengiriman</label> <bsdatepicker name=\"tanggal\" cdate-options=\"dateOptions\" ng-model=\"detailPOSwapping.SentDate\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> <div class=\"form-group\"> <label>No Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.FrameNo\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\"> <bsreqlabel>Term of Payment</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.TermOfPayment\" ng-maxlength=\"50\" required> </div> <!-- <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"detailPOSwapping.TermOfPayment\" data=\"selectionItem\" item-text=\"Type\" item-value=\"TypeId\" on-select=\"TypeId\" placeholder=\"Type\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                    </bsselect> --> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Model Tipe</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.DeKatSu\" ng-maxlength=\"50\" required> </div> <!-- <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.TypeId\" data=\"selectionItem\" item-text=\"Type\" item-value=\"TypeId\" on-select=\"TypeId\" placeholder=\"Type\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                    </bsselect> --> </div> <div class=\"form-group\"> <label>Tahun Produksi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.ProductionYear\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\"> <bsreqlabel>Metode Pembayaran</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.PaymentTypeName\" ng-maxlength=\"50\" required> </div> <!-- <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.TypeId\" data=\"selectionItem\" item-text=\"Type\" item-value=\"TypeId\" on-select=\"TypeId\" placeholder=\"Type\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                    </bsselect> --> </div> </div> </div> <hr> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>PO No</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.POCode\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\"> <label>Grand Total Rincian Harga</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input number-only input-currency type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.GrandTotal\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal PO</bsreqlabel> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\" ng-model=\"detailPOSwapping.PODate\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Note</label> <textarea type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"detailPOSwapping.Note\" rows=\"5\" cols=\"60\" required>\r" +
    "\n" +
    "                </textarea></div> </div> </div> <div class=\"row\" style=\"margin-left: 5px !important\"> <p style=\"background:white\"><b>Rincian Harga</b></p> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <!-- <th class=\"headerRow\">Type Model</th> --> <th class=\"headerRow\">Model Tipe</th> <th class=\"headerRow\">Keterangan</th> <th class=\"headerRow\">Mata Uang</th> <th class=\"headerRow\">Nominal</th> </tr> </thead> <!-- ng-if=\"unit.QuotationTypeId == 1 || unit.QuotationTypeId == 3 || unit.QuotationTypeId == 4\" --> <tbody> <tr> <!-- <th scope=\"row\">Avanza 1.3 E M/T</th> --> <td>{{detailPOSwapping.Dekatsuco}}</td> <td>Harga Jual</td> <td><b>Rp</b></td> <td style=\"text-align: right\">{{detailPOSwapping.Price}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>Diskon</td> <td><b>Rp</b></td> <td style=\"text-align: right\">({{detailPOSwapping.Discount}})</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>Price After Discount</td> <td><b>Rp</b></td> <td style=\"text-align: right\">{{DsMaintainPoTabel_AfterDiskon}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>DPP</td> <td><b>Rp</b></td> <td style=\"text-align: right\">{{detailPOSwapping.DPP}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>VAT</td> <td><b>Rp</b></td> <td style=\"text-align: right\">{{detailPOSwapping.PPN}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>PPH22</td> <td><b>Rp</b></td> <td style=\"text-align: right\">{{detailPOSwapping.PPH22}}</td> </tr> </tbody> </table> <h5>* Untuk invoice akan dikirim oleh TAM</h5> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Biaya TAM Ekspedisi</label> <div class=\"input-icon-right\"> <i class=\"fa\"></i> <input number-only input-currency type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"nama\" placeholder=\"Biaya\" ng-model=\"detailPOSwapping.TAMExpedition\" ng-maxlength=\"60\"> </div> </div> </div> </div> </div> </fieldset> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/findLocation/findLocation.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "          display:none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }</style> <bsform-grid ng-form=\"StockViewForm\" loading=\"loading\" factory-name=\"FindLocationFactory\" model=\"mFindLocation\" model-id=\"DaftarUnitRepairId\" get-data=\"getData\" selected-rows=\"selectedRows\" show-advsearch=\"on\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" form-name=\"StockViewForm\" form-title=\"\" modal-title=\"\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" hide-new-button=\"true\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Cabang</bsreqlabel> <bsselect id=\"comboBox\" name=\"Dealer\" ng-model=\"filter.OutletId\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Cabang\" icon=\"fa fa-sitemap\" required></bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model</bsreqlabel> <bsselect name=\"ModelKendaraan\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"filter.VehicleModelId\" on-select=\"filterModel(selected)\" placeholder=\"Model\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"ModelKendaraan\"></bserrmsg> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Tipe</label> <bsselect name=\"TypeKendaraan\" data=\"getTipe\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"filter.VehicleTypeId\" placeholder=\"Tipe\" ng-change=\"pilihType(selected)\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\"> </bsselect> <!-- <bserrmsg field=\"TypeKendaraan\"></bserrmsg> --> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Warna</label> <bsselect id=\"comboBox\" name=\"Warna\" ng-model=\"filter.ColorId\" data=\"listWarna\" item-text=\"ColorCodeName\" item-value=\"ColorId\" on-select=\"ColorId\" placeholder=\"Warna\" icon=\"fa fa-car glyph-left\"> </bsselect> <!-- <bserrmsg field=\"Warna\"></bserrmsg> --> </div> </div> </div> <div class=\"row\" style=\"margin-bottom:30px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Tahun Kendaraan</label> <bsselect name=\"Year\" data=\"dataYear\" item-text=\"Year\" item-value=\"Year\" ng-model=\"filter.Year\" placeholder=\"Pilih Tahun\" icon=\"fa fa-calender\" style=\"min-width: 150px\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>No. Rangka</label> <input type=\"text\" class=\"form-control\" ng-model=\"filter.FrameNo\" placeholder=\"No. Rangka\"> <!-- <bsdatepicker\r" +
    "\n" +
    "                        name=\"tanggalPDC\"\r" +
    "\n" +
    "                        date-options=\"dateOptionsStart\"\r" +
    "\n" +
    "                        ng-model=\"filter.PlanUnitatPDC\"\r" +
    "\n" +
    "                        icon=\"fa fa-calendar\">\r" +
    "\n" +
    "                    </bsdatepicker> --> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Plan Unit in Cabang</label> <bsdatepicker name=\"tanggalCabang\" date-options=\"dateOptionsStart\" ng-model=\"filter.PlanUnitinCabang\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Rencana Pengiriman</label> <bsdatepicker name=\"tanggalDelivery\" date-options=\"dateOptionsStart\" ng-model=\"filter.PlanDelivery\" icon=\"fa fa-calendar\" required> </bsdatepicker> </div> </div> </div> </bsform-advsearch> <bsform-above-grid> <button ng-click=\"UpdateLocation()\" class=\"rbtn btn\" style=\"float: right\"> Update Lokasi </button> </bsform-above-grid> </bsform-grid>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/findStock/findStock.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <style type=\"text/css\">.switch {\r" +
    "\n" +
    "  position: relative;\r" +
    "\n" +
    "  display: inline-block;\r" +
    "\n" +
    "  width: 60px;\r" +
    "\n" +
    "  height: 34px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ScrollStyleV{max-height:500px;overflow-y:scroll}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".switch input {display:none;}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  cursor: pointer;\r" +
    "\n" +
    "  top: 0;\r" +
    "\n" +
    "  left: 0;\r" +
    "\n" +
    "  right: 0;\r" +
    "\n" +
    "  bottom: 0;\r" +
    "\n" +
    "  background-color: #ccc;\r" +
    "\n" +
    "  -webkit-transition: .4s;\r" +
    "\n" +
    "  transition: .4s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider:before {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  content: \"\";\r" +
    "\n" +
    "  height: 26px;\r" +
    "\n" +
    "  width: 26px;\r" +
    "\n" +
    "  left: 4px;\r" +
    "\n" +
    "  bottom: 4px;\r" +
    "\n" +
    "  background-color: white;\r" +
    "\n" +
    "  -webkit-transition: .4s;\r" +
    "\n" +
    "  transition: .4s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:checked + .slider {\r" +
    "\n" +
    "  background-color: #2196F3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:focus + .slider {\r" +
    "\n" +
    "  box-shadow: 0 0 1px #2196F3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:checked + .slider:before {\r" +
    "\n" +
    "  -webkit-transform: translateX(26px);\r" +
    "\n" +
    "  -ms-transform: translateX(26px);\r" +
    "\n" +
    "  transform: translateX(26px);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* Rounded sliders */\r" +
    "\n" +
    ".slider.round {\r" +
    "\n" +
    "  border-radius: 34px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider.round:before {\r" +
    "\n" +
    "  border-radius: 50%;\r" +
    "\n" +
    "}</style> <div class=\"row\" style=\"float: right; margin: -28px 0 0 0\"> <div class=\"btn-group\"> <button class=\"ubtn btn mobilesearchBtn\" style=\"float: right; padding-bottom: 5px\" ng-click=\"refresh()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"focus\" tooltip-animation=\"true\" uib-tooltip=\"Refresh\" tabindex=\"0\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> <!-- <button ng-class=\"{\\'adv-srch-on\\' : toggle}\" ng-show=\"(enable_advsearch==\\'true\\')?true:false;\" class=\"ubtn btn mobilesearchBtn\" style=\"float: right; height: 34px;\" ng-click=\"toggle = !toggle; advSearch = !advSearch;\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"focus\" tooltip-animation=\"false\" uib-tooltip=\"Search Panel\" tabindex=\"0\">\r" +
    "\n" +
    "            <i ng-class=\"{\\'colorsrch\\' : toggle}\" class=\"fa fa-fw fa-search\" style=\"font-size:1.4em\"></i>\r" +
    "\n" +
    "        </button> --> </div> </div> <div class=\"row\" style=\"margin: 0 0 0 0; padding: 0 15px 0 15px\"> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\" id=\"myTab\"> <li class=\"active\" style=\"width: 50%\"><a data-toggle=\"tab\" ng-click=\"flag('stock')\" href=\"#\" onclick=\"navigateTab('tabContainerStock','findDealer')\">Stok Cabang</a></li> <li style=\"width: 50%\"><a data-toggle=\"tab\" href=\"#\" ng-click=\"flag('otherStock')\" onclick=\"navigateTab('tabContainerStock','findOtherDealer')\">Other Dealer Stock</a></li> </ul> </div> <div id=\"tabContainerStock\" class=\"tab-content\" style=\"padding-top: 15px\"> <div id=\"findDealer\" class=\"tab-pane fade in active\"> <div class=\"row\" style=\"padding-left:12px; padding-right:12px\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Model Kendaraan</bsreqlabel> <bsselect name=\"Model\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"filter.VehicleModelId\" on-select=\"filterModel(selected)\" placeholder=\"Model Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Model Tipe</label> <bsselect name=\"Type\" ng-disabled=\"filter.VehicleModelId == null\" data=\"listTipe\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"filter.VehicleTypeId\" ng-change=\"pilihType(filter.VehicleTypeId)\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Warna Kendaraan</label> <bsselect name=\"Warna\" ng-disabled=\"filter.VehicleTypeId == null\" data=\"listWarna\" item-text=\"ColorCodeName\" item-value=\"ColorId\" ng-model=\"filter.ColorId\" placeholder=\"Warna\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Area</bsreqlabel> <bsselect name=\"area\" data=\"getDataArea\" item-text=\"AreaName\" item-value=\"AreaId\" ng-model=\"filter.AreaId\" placeholder=\"Area\" icon=\"fa fa-sitemap glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Provinsi</label> <bsselect name=\"Provinsi\" ng-disabled=\"filter.AreaId == null\" data=\"getDataProvince\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" ng-model=\"filter.ProvinceId\" placeholder=\"Provinsi\" icon=\"fa fa-sitemap glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> </div> <div class=\"row\" style=\"padding-right: 8px;margin-right: 0px;margin-top: 5px\"> <button class=\"btn wbtn\" style=\"float:right\" ng-click=\"advancedSearch()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search\" tabindex=\"0\"> <i class=\"large icons\"> <i class=\"database icon\"></i> <i class=\"inverted corner filter icon\"></i> </i>Search </button> </div> <div class=\"listData\"> <button id=\"btn-append-to-body\" class=\"btn btn-default dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" aria-haspopup=\"true\" aria-expanded=\"false\" uib-dropdown-toggle=\"\"> <i class=\"fa fa-sitemap glyph-left\" aria-hidden=\"true\"></i>&nbspCabang<span style=\"float:right\" class=\"ng-binding\">( {{Qty}} )<span></span></span> </button> <!-- <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" ng-disabled=\"true\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; text-align:left; padding-top:15px; background-color:#888b91; color:white;\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Cabang</button> --> <div class=\"list-group ScrollStyleV\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-repeat=\"item in stockGroupDealer | orderBy : 'OutletName'\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item.OutletName}}</h7> </div> </div> </div> <div ng-if=\"isEmpty == true\" class=\"list-group-item\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">Stock Kosong</h7> </div> </div> </div> </div> </div> </div> <div id=\"findOtherDealer\" class=\"tab-pane fade\"> <div class=\"row\" style=\"padding-left:12px; padding-right:12px\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Model Kendaraan</bsreqlabel> <bsselect name=\"Model\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"filterOther.VehicleModelId\" on-select=\"filterModel(selected)\" placeholder=\"Model Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Model Tipe</label> <bsselect name=\"Type\" ng-disabled=\"filterOther.VehicleModelId == null\" data=\"listTipe\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"filterOther.VehicleTypeId\" ng-change=\"pilihType(filter.VehicleTypeId)\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Warna Kendaraan</label> <bsselect name=\"Warna\" ng-disabled=\"filterOther.VehicleTypeId == null\" data=\"listWarna\" item-text=\"ColorCodeName\" item-value=\"ColorId\" ng-model=\"filterOther.ColorId\" placeholder=\"Warna\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Area</bsreqlabel> <bsselect name=\"area\" data=\"getDataArea\" item-text=\"AreaName\" item-value=\"AreaId\" ng-model=\"filterOther.AreaId\" placeholder=\"Area\" icon=\"fa fa-sitemap glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Provinsi</label> <bsselect name=\"Provinsi\" ng-disabled=\"filterOther.AreaId == null\" data=\"getDataProvince\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" ng-model=\"filterOther.ProvinceId\" placeholder=\"Provinsi\" icon=\"fa fa-sitemap glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> </div> <div class=\"row\" style=\"padding-right: 8px;margin-right: 0px;margin-top: 5px\"> <button class=\"btn wbtn\" style=\"float:right\" ng-click=\"otherDealerSearch()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search\" tabindex=\"0\"> <i class=\"large icons\"> <i class=\"database icon\"></i> <i class=\"inverted corner filter icon\"></i> </i>Search </button> </div> <div class=\"listData\"> <button id=\"btn-append-to-body\" class=\"btn btn-default dropdown-toggle\" type=\"button\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" aria-haspopup=\"true\" aria-expanded=\"false\" uib-dropdown-toggle=\"\"> <i class=\"fa fa-sitemap glyph-left\" aria-hidden=\"true\"></i>&nbspCabang<span style=\"float:right\" class=\"ng-binding\">( {{QtyOther}} )<span></span></span> </button> <!-- <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" ng-disabled=\"true\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; text-align:left; padding-top:10px; background-color:#888b91; color:white;\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Cabang</button> --> <div class=\"list-group ScrollStyleV\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-repeat=\"item in stockOtherDealer | orderBy : 'OutletName'\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item.OutletName}}</h7> </div> </div> </div> <div ng-if=\"isOtherEmpty == true\" class=\"list-group-item\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">Stock Kosong</h7> </div> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/findStock/rowTemplate.html',
    "<div class=\"row\" style=\"padding-left:3%; font-size:14px\"> <ul class=\"list-inline\"> <li> <b>{{item.OutletName}}</b> </li> </ul> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/maintainQuotation/maintainQuotation.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\">function navigateTabMaintainQuotation(containerId, divId) {\r" +
    "\n" +
    "        $(\"#\" + containerId).children().each(function (n, i) {\r" +
    "\n" +
    "            var id = this.id;\r" +
    "\n" +
    "            console.log(id + ' - ' + divId);\r" +
    "\n" +
    "            if (id == divId) {\r" +
    "\n" +
    "                $('#' + divId).attr('class', 'tab-pane fade in active');\r" +
    "\n" +
    "            } else {\r" +
    "\n" +
    "                $('#' + id).attr('class', 'tab-pane fade');\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        });\r" +
    "\n" +
    "    }</script> <style type=\"text/css\"></style> <div ng-show=\"MaintainQuotationShow\"> <bsform-grid ng-form=\"MaintainQuotationForm\" factory-name=\"MaintainQuotationFactory\" model=\"mMaintainQuotation\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"selectedRows\" show-advsearch=\"on\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" form-name=\"MaintainQuotationForm\" form-title=\"Maintain Quotation\" modal-title=\"Maintain Quotation\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" hide-new-button=\"true\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Status Quotation</bsreqlabel> <bsselect name=\"statusquotation\" data=\"dataQuotation\" item-text=\"QuotationStatusName\" item-value=\"QuotationStatusId\" ng-model=\"filter.QuotationStatusId\" placeholder=\"Status Quotation\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\"> </bsselect> <!-- <bserrmsg field=\"statusquotation\"></bserrmsg> --> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Model</label> <bsselect name=\"ModelKendaraan\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"filter.VehicleModelId\" on-select=\"filterModel(selected)\" placeholder=\"Model Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\"> </bsselect> <!-- <bserrmsg field=\"ModelKendaraan\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Tipe</label> <bsselect name=\"TypeKendaraan\" data=\"listTipe\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"filter.VehicleTypeId\" ng-change=\"pilihType(filter.VehicleTypeId)\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\"> </bsselect> <!-- <bserrmsg field=\"TypeKendaraan\"></bserrmsg> --> </div> </div> </div> </bsform-advsearch> </bsform-grid> <!--<bsui-modal show=\"show_modal.show\" title=\"Alasan Batal Quotation\" data=\"modal_model\" on-save=\"onListSave\" on-cancel=\"onListCancel\" mode=\"modalMode\">\r" +
    "\n" +
    "        <div ng-form name=\"contactForm\">\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-6\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsreqlabel>Alasan</bsreqlabel>\r" +
    "\n" +
    "                        <bsselect name=\"Alasan\" ng-model=\"Selectedrow.QuotationCancelReasonId\" data=\"dataCancel\" item-text=\"QuotationCancelReasonName\" item-value=\"QuotationCancelReasonId\" on-select=\"QuotationCancelReasonId\" placeholder=\"Select\" required>\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsreqlabel>Keterangan</bsreqlabel>\r" +
    "\n" +
    "                        <textarea type=\"text\" class=\"form-control\" name=\"Keterangan\" placeholder=\"\" ng-model=\"Selectedrow.NoteCancel\" maxlength=\"250\" rows=\"5\" cols=\"60\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </bsui-modal>--> <div class=\"ui modal ModalMaintainQuotation_AlasanBatalQuotation\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"KembaliModalMaintainQuotation_AlasanBatalQuotation()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Alasan Batal Quotation</h4> </div> <div class=\"modal-body\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Alasan</bsreqlabel> <bsselect name=\"Alasan\" ng-model=\"Selectedrow.QuotationCancelReasonId\" data=\"dataCancel\" item-text=\"QuotationCancelReasonName\" item-value=\"QuotationCancelReasonId\" on-select=\"QuotationCancelReasonId\" placeholder=\"Select\" required> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Keterangan</bsreqlabel> <textarea type=\"text\" class=\"form-control\" name=\"Keterangan\" placeholder=\"\" ng-model=\"Selectedrow.NoteCancel\" maxlength=\"250\" rows=\"5\" cols=\"60\" required>\r" +
    "\n" +
    "                    </textarea></div> </div> </div> </div> <div class=\"modal-footer\"> <button ng-click=\"SimpanModalMaintainQuotation_AlasanBatalQuotation()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"><span><i class=\"fa fa-check\" aria-hidden=\"true\"></i>Simpan</span></button> <button ng-click=\"KembaliModalMaintainQuotation_AlasanBatalQuotation()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </div> </div> </div> <div ng-show=\"BuatQuotationShow\"> <button id=\"btnkembaliquo\" ng-click=\"btnkembaliQuotation()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px\">Kembali</button> <fieldset ng-disabled=\"true\"> <div class=\"row\" style=\"padding-top:12px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Quotation</label> <div class=\"input-icon-right\"> <i class=\"fa fa-sitemap\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.QuotationNo\" ng-maxlength=\"60\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Quotation</bsreqlabel> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\" ng-model=\"Selectedrow.QuotationDate\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Cabang yang meminta</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-sitemap\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.OutletRequestName\" ng-maxlength=\"60\" required> </div> </div> </div> <div class=\"col-md-3\" style=\"padding-right: 0px !important\"> <div class=\"form-group\"> <bsreqlabel>Tipe Quotation</bsreqlabel> <bsselect id=\"comboBox\" name=\"Type\" ng-model=\"Selectedrow.QuotationTypeId\" data=\"dataType\" item-text=\"QuotationTypeName\" item-value=\"QuotationTypeId\" on-select=\"QuotationTypeId\" placeholder=\"Pilih Tipe Quotation\" icon=\"fa fa-search\"></bsselect> </div> </div> </div> <div class=\"row\" style=\"padding-top:10px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status Quotation</label> <div class=\"input-icon-right\"> <i class=\"fa fa-sitemap\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.QuotationStatusName\" ng-maxlength=\"60\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Alasan Batal Quotation</bsreqlabel> <bsselect name=\"Alasan\" ng-model=\"Selectedrow.QuotationCancelReasonId\" data=\"dataCancel\" item-text=\"QuotationCancelReasonName\" item-value=\"QuotationCancelReasonId\" on-select=\"QuotationCancelReasonId\" placeholder=\"Select\"> </bsselect> </div> </div> </div> </fieldset> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabMaintainQuotation('tabContainermaintainQuo','menuInfoPengirimanquo')\">Info Pengiriman</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabMaintainQuotation('tabContainermaintainQuo','menuinfounityangakandibeliquo')\">Info Unit Yang Akan Dibeli</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabMaintainQuotation('tabContainermaintainQuo','menuRincianHargaquo')\">Rincian Harga</a></li> <li ng-if=\"Selectedrow.QuotationTypeId == 3 || Selectedrow.QuotationTypeId == 4\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabMaintainQuotation('tabContainermaintainQuo','menuEkspedisiTAMquo')\">Biaya Ekspedisi TAM</a></li> </ul> <fieldset ng-disabled=\"true\"> <div id=\"tabContainermaintainQuo\" class=\"tab-content\" style=\"padding-top: 15px\"> <div id=\"menuInfoPengirimanquo\" class=\"tab-pane fade in active\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Nama Penerima</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.ReceiveName\" ng-maxlength=\"60\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Alamat Penerima</label> <div class=\"input-icon-right\"> <textarea type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.SentAddress\" rows=\"5\" cols=\"60\" required>\r" +
    "\n" +
    "                            </textarea></div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Handphone Penerima</label> <div class=\"input-icon-right\"> <i class=\"fa fa-mobile\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.ReceiveHP\" ng-maxlength=\"60\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tanggal Pengiriman</label> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\" icon=\"fa fa-calendar\" ng-model=\"Selectedrow.SentDate\"> </bsdatepicker> </div> </div> </div> </div> <div style=\"min-height: 100vh\" id=\"menuinfounityangakandibeliquo\" class=\"tab-pane fade\"> <fieldset ng-disabled=\"true\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model Tipe</label> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.VehicleTypeDescription\" required> </div> </div> <div class=\"form-group\"> <label>Warna</label> <!-- <i class=\"fa fa-search\"></i> --> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.ColorName\" required> </div> </div> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.FrameNo\" required> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tahun Produksi</label> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.ProductionYear\" required> </div> </div> <div class=\"form-group\"> <label>Lokasi Unit</label> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.LocationName\" required> </div> </div> </div> </div> </fieldset> </div> <div id=\"menuRincianHargaquo\" class=\"tab-pane fade\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>TOP</label> <bsselect name=\"filterModel \" style=\"margin-bottom: 10px\" ng-model=\"Selectedrow.TOPId \" data=\"termOfPayment \" item-text=\"TermOfPayment \" item-value=\"TermOfPaymentId \" placeholder=\"Pilih TOP\" icon=\"fa fa-search \"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <!-- <label>TOP</label> --> <!-- <bsselect name=\"filterModel\" style=\"margin-bottom: 10px\" ng-model=\"dataQuotation.VehicleModelId\" data=\"productModelData\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"filter.productModel=selected\" placeholder=\"Select\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                            </bsselect> --> <bsreqlabel>Down Payment</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" style=\"text-align: right\" input-currency class=\"form-control\" name=\"DownPayment\" placeholder=\"Down Payment\" ng-model=\"Selectedrow.DownPayment\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-5\"> <div class=\"form-group\"> <div id=\"textArea\"> <label>Catatan</label> <div class=\"input-icon-right\"> <textarea class=\"form-control\" rows=\"5\" id=\"comment\" ng-model=\"Selectedrow.NotePrice\">\r" +
    "\n" +
    "                                    </textarea> </div> </div> </div> </div> </div> <!--- Disini Rincian Harganya --> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <!-- <th class=\"headerRow\">Type Model</th> --> <th class=\"headerRow\">Model Tipe</th> <th class=\"headerRow\">Keterangan</th> <th class=\"headerRow\">Nominal</th> </tr> </thead> <tbody ng-if=\"Selectedrow.QuotationTypeId == 1 || Selectedrow.QuotationTypeId == 3 || Selectedrow.QuotationTypeId == 4\"> <tr> <!-- <th scope=\"row\">Avanza 1.3 E M/T</th> --> <td>{{Selectedrow.VehicleTypeDescription}}</td> <td>Harga Jual</td> <td>{{Selectedrow.SellPrice | number: 0}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>Diskon</td> <td>({{Selectedrow.Discount | number: 0}})</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>Price After Discond</td> <td>{{Selectedrow.SellPrice - Selectedrow.Discount | number: 0}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>DPP</td> <td>{{Selectedrow.DPP | number: 0}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>VAT</td> <td>{{Selectedrow.VAT | number: 0}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>PPH22</td> <td>{{Selectedrow.PPH22 | number: 0}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>TotalInclVAT</td> <td>{{Selectedrow.Total | number: 0}}</td> </tr> </tbody> <tbody ng-if=\"Selectedrow.QuotationTypeId == 1 || Selectedrow.QuotationTypeId == 2\"> <tr> <!-- <th scope=\"row\">Avanza 1.3 E M/T</th> --> <td ng-if=\"Selectedrow.QuotationTypeId == 2\">Logistik</td> <td ng-if=\"Selectedrow.QuotationTypeId != 2\">Ekspedisi</td> <td>Harga Jual</td> <td>{{Selectedrow.ExpeditionSellPrice | number: 0}}</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>Diskon</td> <td>({{Selectedrow.ExpeditionDiscount | number: 0}})</td> </tr> <tr> <!-- <td></td> --> <td></td> <td>Price After Discond</td> <td>{{Selectedrow.ExpeditionSellPrice - Selectedrow.ExpeditionDiscount | number: 0}}</td> </tr> </tbody> </table> <!-- END  --> <fieldset ng-disabled=\"true\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label></label> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>DPP</label> <div class=\"input-icon-right\"> <input input-currency style=\"text-align: right\" type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.TotalDPP\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Total(Termasuk PPN)</label> <div class=\"input-icon-right\"> <input input-currency style=\"text-align: right\" type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.TotalVAT\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Grand Total</label> <div class=\"input-icon-right\"> <input input-currency style=\"text-align: right\" type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"Selectedrow.GrandTotal\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </fieldset> </div> <div id=\"menuEkspedisiTAMquo\" class=\"tab-pane fade\" style=\"min-height: 100vh\"> <h5>* Untuk invoice akan dikirim oleh TAM</h5> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Biaya Ekspedisi TAM</label> <div class=\"input-icon-right\"> <i class=\"fa\"></i> <input input-currency style=\"text-align: right\" type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"nama\" placeholder=\"Biaya\" ng-model=\"Selectedrow.TAMExpedition\" ng-maxlength=\"60\"> </div> </div> </div> </div> </div> </div> <!-- </div> --> </fieldset> </div> <!-- <div class=\"row\" style=\"padding-left: 15px\">\r" +
    "\n" +
    "        <div ui-grid=\"gridEkspedisi\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-auto-resize></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <button id=\"btnKalkulasi\" ng-click=\"btnKalkulasi()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin-bottom:12px; margin-top: 10px; margin-left:-2px; width:10%\">Kalkulasi</button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <fieldset ng-disabled=\"true\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-3\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <label></label>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-3\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>DPP</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"ViewDataSPK.TDP\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-3\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Total(Termasuk PPN)</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"ViewDataSPK.totalvat\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-3\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Grand Total</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"ViewDataSPK.totalgrand\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </fieldset> -->"
  );


  $templateCache.put('app/sales/sales5/demandSupply/maintainRequestSwapping/maintainRequestSwapping.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\">function navigateTabMaintainRequestSwapping(containerId, divId) {\r" +
    "\n" +
    "        $(\"#\" + containerId).children().each(function (n, i) {\r" +
    "\n" +
    "            var id = this.id;\r" +
    "\n" +
    "            console.log(id + ' - ' + divId);\r" +
    "\n" +
    "            if (id == divId) {\r" +
    "\n" +
    "                $('#' + divId).attr('class', 'tab-pane fade in active');\r" +
    "\n" +
    "            } else {\r" +
    "\n" +
    "                $('#' + id).attr('class', 'tab-pane fade');\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        });\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\tfunction MaintainRequestSwapping_Paksa_FokusDownpayment() \r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\tvar liat_awal=document.getElementById(\"MaintainRequestSwappingPaksaFokusDownpayment\").value;\r" +
    "\n" +
    "\t\tif(liat_awal==\"Rp. 0\")\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\tdocument.getElementById(\"MaintainRequestSwappingPaksaFokusDownpayment\").value=null;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\tfunction MaintainRequestSwapping_Paksa_FokusDiskonUnit() \r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\tvar liat_awal=document.getElementById(\"MaintainRequestSwappingPaksaFokusDiskonUnit\").value;\r" +
    "\n" +
    "\t\tif(liat_awal==\"Rp. 0\")\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\tdocument.getElementById(\"MaintainRequestSwappingPaksaFokusDiskonUnit\").value=null;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\tfunction MaintainRequestSwapping_Paksa_FokusDiskonLogistik() \r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\tvar liat_awal=document.getElementById(\"MaintainRequestSwappingPaksaFokusDiskonLogistik\").value;\r" +
    "\n" +
    "\t\tif(liat_awal==\"Rp. 0\")\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\tdocument.getElementById(\"MaintainRequestSwappingPaksaFokusDiskonLogistik\").value=null;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\tfunction MaintainRequestSwapping_Paksa_FokusHargaUnit() \r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\tvar liat_awal=document.getElementById(\"MaintainRequestSwappingPaksaFokusHargaUnit\").value;\r" +
    "\n" +
    "\t\tif(liat_awal==\"Rp. 0\")\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\tdocument.getElementById(\"MaintainRequestSwappingPaksaFokusHargaUnit\").value=null;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\t\r" +
    "\n" +
    "\tfunction MaintainRequestSwapping_Paksa_FokusHargaLogistik() \r" +
    "\n" +
    "\t{\r" +
    "\n" +
    "\t\tvar liat_awal=document.getElementById(\"MaintainRequestSwappingPaksaFokusHargaLogistik\").value;\r" +
    "\n" +
    "\t\tif(liat_awal==\"Rp. 0\")\r" +
    "\n" +
    "\t\t{\r" +
    "\n" +
    "\t\t\tdocument.getElementById(\"MaintainRequestSwappingPaksaFokusHargaLogistik\").value=null;\r" +
    "\n" +
    "\t\t}\r" +
    "\n" +
    "\t}</script> <style type=\"text/css\">.nopadding {\r" +
    "\n" +
    "        padding-right: 0 !important;\r" +
    "\n" +
    "        padding-top: 0 !important;\r" +
    "\n" +
    "        padding-bottom: 0 !important;\r" +
    "\n" +
    "        padding-left: 1px;\r" +
    "\n" +
    "        margin: 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .table {\r" +
    "\n" +
    "        font-family: arial, sans-serif;\r" +
    "\n" +
    "        border-collapse: collapse;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .td,\r" +
    "\n" +
    "    .th {\r" +
    "\n" +
    "        border: 1px solid #dddddd;\r" +
    "\n" +
    "        text-align: left;\r" +
    "\n" +
    "        padding: 8px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .tr:nth-child(even) {\r" +
    "\n" +
    "        background-color: #dddddd;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .gridStyle {\r" +
    "\n" +
    "        border: 1px solid rgb(212, 212, 212);\r" +
    "\n" +
    "        width: 400px;\r" +
    "\n" +
    "        height: 300px\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <div ng-show=\"MaintainRequestSwapping\"> <button ng-click=\"btnQuotation()\" ng-disabled=\"(selectedReq.length == 0) \" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px; width:11%\"><span><i class=\"fa fa-plus\"></i> &nbsp; Tambah</span></button> <bsform-grid ng-form=\"MaintainRequestSwappingForm \" factory-name=\"MaintainRequestSwappingFactory \" model=\"mMaintainRequestSwapping \" model-id=\"Id \" get-data=\"getData \" selected-rows=\"selectedRows \" on-select-rows=\"onSelectRows \" form-name=\"MaintainRequestSwappingForm \" show-advsearch=\"on\" grid-hide-action-column=\"true \" hide-new-button=\"true \" form-title=\" \" modal-title=\"Maintain Request Swapping \" modal-size=\"small \" icon=\"fa fa-fw fa-child \"> <bsform-advsearch> <p> <div> <div class=\"row\" style=\"margin: 0 0 5px 0;display: flex\"> <div class=\"col-md-4\"> <label>Tanggal Awal</label> <bsdatepicker ng-change=\"MaintainRequestSwappingPaksaFilterTanggal()\" ng-model=\"filter.StartRequestDate\" name=\"tanggal\" date-options=\"dateOptionsMaintainRequestSwapping\"> </bsdatepicker> </div> <div class=\"col-md-4\"> <label>Tanggal Akhir</label> <bsdatepicker ng-change=\"MaintainRequestSwappingPaksaFilterTanggal()\" ng-model=\"filter.EndRequestDate\" name=\"tanggal\" date-options=\"dateOptionsMaintainRequestSwapping\"> </bsdatepicker> </div> </div> </div> </p> </bsform-advsearch> </bsform-grid> <!-- <bsui-modal show=\"show_modal.show \" title=\"Alasan Batal SO \" data=\"modal_model \" on-save=\"onListSave \" on-cancel=\"onListCancel \" mode=\"modalMode \">\r" +
    "\n" +
    "        <div ng-form name=\"contactForm \">\r" +
    "\n" +
    "            <p style=\"font-size:initial; margin:0px; \">Silahkan mengisi No Rangka dahulu baru membuat Quotation</p>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </bsui-modal> --> </div> <div ng-show=\"StockView\"> <button ng-click=\"btnkembaliStockView() \" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px; width:11%\">Kembali</button> <button class=\"ubtn mobileFilterBtn\" ng-click=\"refreshFilter()\" onclick=\"this.blur()\" style=\"float:right; margin:-30px 0 0 10px\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" tabindex=\"0\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> <fieldset ng-disabled=\"true\"> <div class=\"row\" style=\"padding-top:12px\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Cabang</bsreqlabel> <bsselect id=\"comboBox \" name=\"Dealer \" ng-model=\"selectedrow.OutletId \" data=\"getOutlet \" item-text=\"Name \" item-value=\"OutletId \" on-select=\"OutletId \" placeholder=\"Dealer Branch \" icon=\"fa fa-sitemap \"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Tipe</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\"Plih Tipe\" ng-model=\"selectedrow.VehicleTypeDescription \" ng-maxlength=\"60 \" required> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Warna</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-search\"></i> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"selectedrow.ColorName \" ng-maxlength=\"60 \" required> </div> </div> </div> </div> </fieldset> <div id=\"filterData\" class=\"row\" style=\"margin:0; margin-top:5px\"> <div id=\"inputFilter\" style=\"float:left; width:20%\"> <div class=\"form-group filterMarg\"> <input type=\"text\" required ng-model=\"textFilter\" class=\"form-control\" placeholder=\"Search\" maxlength=\"100\" ng-change=\"filterCustom()\"> </div> </div> <div id=\"comboBoxFilter\" style=\"float:left; width:15%; font-size:12px\"> <div class=\"form-group filterMarg\"> <select required ng-model=\"selectedFilter\" class=\"form-control\" ng-options=\"item.value as item.name for item in filterData.defaultFilter\" placeholder=\"Filter\"> <option label=\"Filter\" value=\"\" disabled>Filter</option> </select> </div> </div> </div> <div ui-grid=\"gridStockView \" ui-grid-selection ui-grid-pinning ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> <div ng-show=\"BuatQuotationShow \"> <button ng-click=\"btnsimpanQuotation()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 10px 1px\">Simpan</button> <button id=\"btnkembaliquo\" ng-click=\"btnkembaliQuotation() \" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px\">Kembali</button> <fieldset ng-disabled=\"true \" style=\"width: 100%\"> <div class=\"row\" style=\"padding-top:12px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tipe Quotation</bsreqlabel> <bsselect id=\"comboBox \" name=\"Type \" ng-model=\"dataQuotation[0].QuotationTypeId \" data=\"dataType \" item-text=\"QuotationTypeName \" item-value=\"QuotationTypeId \" on-select=\"QuotationTypeId \" placeholder=\"Pilih Tipe Quotation\" icon=\"fa fa-search \"></bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Cabang yang meminta</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-sitemap\"></i> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].OutletRequestName \" ng-maxlength=\"60 \" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Quotation</bsreqlabel> <bsdatepicker name=\"tanggal \" date-options=\"dateOptions \" ng-model=\"dataQuotation[0].QuotationDate \" icon=\"fa fa-calendar \"> </bsdatepicker> </div> </div> </div> </fieldset> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabMaintainRequestSwapping( 'tabContainerSwapping', 'menuInfoPengirimanswap')\">Info Pengiriman</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabMaintainRequestSwapping( 'tabContainerSwapping', 'menuinfounityangakandibeliswap')\">Info Unit Yang Akan Dibeli</a></li> <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabMaintainRequestSwapping( 'tabContainerSwapping', 'menuRincianHargaswap')\">Rincian Harga</a></li> <li ng-if=\"dataQuotation[0].QuotationTypeId == 3 || dataQuotation[0].QuotationTypeId == 4\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTabMaintainRequestSwapping( 'tabContainerSwapping', 'menuEkspedisiTamswap')\">Biaya Ekspedisi TAM</a></li> </ul> <div id=\"tabContainerSwapping\" class=\"tab-content\" style=\"padding-top: 15px\"> <div id=\"menuInfoPengirimanswap\" class=\"tab-pane fade in active\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Nama Penerima</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text \" maxlength=\"100\" class=\"form-control\" name=\"nama \" placeholder=\"Input Nama Penerima\" ng-model=\"dataQuotation[0].ReceiveName \" ng-maxlength=\"60 \" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Alamat Penerima</label> <div class=\"input-icon-right\"> <!-- <i class=\"fa fa-pencil \"></i> --> <textarea type=\"text \" maxlength=\"255\" class=\"form-control\" name=\"nama \" placeholder=\"Input Alamat Penerima\" ng-model=\"dataQuotation[0].SentAddress \" rows=\"5\" cols=\"60\" required>\r" +
    "\n" +
    "                        </textarea></div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Handphone Penerima</label> <div class=\"input-icon-right\"> <i class=\"fa fa-mobile\"></i> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\"Input Handphone Penerima\" ng-model=\"dataQuotation[0].ReceiveHP \" maxlength=\"60\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tanggal Pengiriman</label> <bsdatepicker name=\"tanggal \" date-options=\"dateOptions\" icon=\"fa fa-calendar \" ng-model=\"dataQuotation[0].SentDate \"> </bsdatepicker> </div> </div> </div> </div> <div id=\"menuinfounityangakandibeliswap\" class=\"tab-pane fade\"> <fieldset ng-disabled=\"true \"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model Tipe</label> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].VehicleTypeDescription \" required> </div> </div> <div class=\"form-group\"> <label>Warna</label> <i class=\"fa fa-search\"></i> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].ColorName \" required> </div> </div> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].FrameNo \" required> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tahun Produksi</label> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].ProductionYear \" required> </div> </div> <div class=\"form-group\"> <label>Lokasi Unit</label> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].LocationName\" required> </div> </div> </div> </div> </fieldset> </div> <div id=\"menuRincianHargaswap\" class=\"tab-pane fade\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>TOP</label> <bsselect name=\"filterModel \" style=\"margin-bottom: 10px\" ng-model=\"dataQuotation[0].TOPId \" data=\"termOfPayment \" item-text=\"TermOfPayment \" item-value=\"TermOfPaymentId \" placeholder=\"Pilih TOP\" icon=\"fa fa-search \"> </bsselect> </div> <div class=\"form-group\" ng-if=\"dataQuotation[0].QuotationTypeId == 1 || dataQuotation[0].QuotationTypeId == 3 \"> <bsreqlabel>Harga Unit</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text \" onfocus=\"MaintainRequestSwapping_Paksa_FokusHargaUnit()\" id=\"MaintainRequestSwappingPaksaFokusHargaUnit\" input-currency number-only class=\"form-control\" style=\"text-align: right\" name=\"Unit Price\" placeholder=\"Unit Price\" ng-model=\"priceQuo.UnitPrice \" ng-maxlength=\"50 \" required> </div> </div> <div class=\"form-group\" ng-if=\"dataQuotation[0].QuotationTypeId == 1 || dataQuotation[0].QuotationTypeId == 2\"> <bsreqlabel>Harga Logistik</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text \" onfocus=\"MaintainRequestSwapping_Paksa_FokusHargaLogistik()\" id=\"MaintainRequestSwappingPaksaFokusHargaLogistik\" input-currency number-only class=\"form-control\" style=\"text-align: right\" name=\"Logistic \" placeholder=\"Harga Logistik\" ng-model=\"priceQuo.ExpeditionSellPrice \" ng-maxlength=\"50 \" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Down Payment</bsreqlabel> <div class=\"input-icon-right\"> <input onfocus=\"MaintainRequestSwapping_Paksa_FokusDownpayment()\" id=\"MaintainRequestSwappingPaksaFokusDownpayment\" type=\"text \" input-currency number-only class=\"form-control\" style=\"text-align: right\" name=\"DownPayment \" placeholder=\"Down Payment \" ng-model=\"dataQuotation[0].DownPayment \" ng-maxlength=\"50 \" required> </div> </div> <div class=\"form-group\" ng-if=\"dataQuotation[0].QuotationTypeId == 1 || dataQuotation[0].QuotationTypeId == 3\"> <bsreqlabel>Diskon (Unit)</bsreqlabel> <div class=\"input-icon-right\"> <input onfocus=\"MaintainRequestSwapping_Paksa_FokusDiskonUnit()\" id=\"MaintainRequestSwappingPaksaFokusDiskonUnit\" type=\"text \" input-currency number-only class=\"form-control\" style=\"text-align: right\" name=\"Diskon Unit \" placeholder=\"Discount\" ng-model=\"priceQuo.UnitDiscount \" ng-maxlength=\"50 \" required> </div> </div> <div class=\"form-group\" ng-if=\"dataQuotation[0].QuotationTypeId == 1 || dataQuotation[0].QuotationTypeId == 2\"> <bsreqlabel>Diskon (Logistic)</bsreqlabel> <div class=\"input-icon-right\"> <input onfocus=\"MaintainRequestSwapping_Paksa_FokusDiskonLogistik()\" id=\"MaintainRequestSwappingPaksaFokusDiskonLogistik\" type=\"text \" input-currency number-only class=\"form-control\" style=\"text-align: right\" name=\"Diskon Logistik \" placeholder=\"Discount\" ng-model=\"priceQuo.ExpeditionDiscount \" ng-maxlength=\"50 \" required> </div> </div> </div> <div class=\"col-md-5\"> <div class=\"form-group\"> <div id=\"textArea \"> <label>Catatan</label> <div class=\"input-icon-right\"> <textarea class=\"form-control\" maxlength=\"255\" rows=\"5\" id=\"comment \" ng-model=\"dataQuotation[0].NotePrice \"></textarea> </div> </div> </div> </div> </div> <!-- <pricing-engine js-data=\"jsData\" disable-button=\"true \" url-pricing=\"urlPricing\" jumlah-data=\"10 \">\r" +
    "\n" +
    "            <form-body-custom>\r" +
    "\n" +
    "            </form-body-custom>\r" +
    "\n" +
    "        </pricing-engine> --> <div class=\"row\" style=\"margin:10px 0 0 0\"> <button ng-click=\"Kalkulasi(); \" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin-right: 12px\"> <i class=\"fa fa-calculator\"></i>&nbspKalkulasi</button> </div> <div ng-if=\"dataQuotation[0].QuotationTypeId == 4\" class=\"row\"> <ul style=\"padding-left: 25px; padding-right: 20px; margin-bottom: 25px;list-style: none\"> <li> <div style=\"border:1px solid lightgrey; padding: 15px 15px 0 15px; margin-top: 10px\"> <i class=\"fa fa-car\"></i>&nbsp&nbsp <b>{{dataQuotation[0].Dekatsu}}</b> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <!-- <th class=\"headerRow\">Model Tipe</th> --> <th class=\"headerRow\">Keterangan</th> <th class=\"headerRow\">Mata Uang</th> <th class=\"headerRow\">Nominal</th> </tr> </thead> <tbody> <tr> <!-- <th scope=\"row\">Avanza 1.3 E M/T</th> --> <td><b>Harga dasar jual</b></td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].SellPrice | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td><b>Diskon</b></td> <td>Rp.</td> <td style=\"text-align:right\">({{dataQuotation[0].Discount | number:2}})</td> </tr> <tr> <!-- <td></td> --> <td>Harga Setelah Discount</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].PriceAfterDiscVehicle | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>DPP</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].DPP | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>VAT</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].VAT | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>PPH22</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].PPH22 | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Total DPP</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].TotalDPP | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Total VAT</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].TotalVAT | number:2}}</td> </tr> </tbody> </table> </div> </li> </ul> </div> <div ng-if=\"dataQuotation[0].QuotationTypeId == 3\" class=\"row\"> <ul style=\"padding-left: 25px; padding-right: 20px; margin-bottom: 25px;list-style: none\"> <li> <div style=\"border:1px solid lightgrey; padding: 15px 15px 0 15px; margin-top: 10px\"> <i class=\"fa fa-car\"></i>&nbsp&nbsp <b>{{dataQuotation[0].Dekatsu}}</b> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <!-- <th class=\"headerRow\">Model Tipe</th> --> <th class=\"headerRow\">Keterangan</th> <th class=\"headerRow\">Mata Uang</th> <th class=\"headerRow\">Nominal</th> </tr> </thead> <tbody> <tr> <!-- <th scope=\"row\">Avanza 1.3 E M/T</th> --> <td><b>Harga dasar jual</b></td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].SellPrice | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Diskon</td> <td>Rp.</td> <td style=\"text-align:right\">({{dataQuotation[0].Discount | number:2}})</td> </tr> <tr> <!-- <td></td> --> <td>Harga Setelah Discount</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].PriceAfterDiscVehicle | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>DPP</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].DPP | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>VAT</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].VAT | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>PPH22</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].PPH22 | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Total DPP</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].TotalDPP | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Total VAT</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].TotalVAT | number:2}}</td> </tr> </tbody> </table> </div> </li> </ul> </div> <div ng-if=\"dataQuotation[0].QuotationTypeId == 2\" class=\"row\"> <ul style=\"padding-left: 25px; padding-right: 20px; margin-bottom: 25px;list-style: none\"> <li> <div style=\"border:1px solid lightgrey; padding: 15px 15px 0 15px; margin-top: 10px\"> <!-- <i class=\"fa fa-car\"></i>&nbsp&nbsp --> <!-- <b>{{dataQuotation[0].Dekatsu}}</b> --> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <!-- <th class=\"headerRow\">Model Tipe</th> --> <th class=\"headerRow\">Keterangan</th> <th class=\"headerRow\">Mata Uang</th> <th class=\"headerRow\">Nominal</th> </tr> </thead> <tbody> <tr> <!-- <th scope=\"row\">Avanza 1.3 E M/T</th> --> <td><b>Harga Ekspedisi</b></td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].ExpeditionSellPrice | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Diskon Ekspedisi</td> <td>Rp.</td> <td style=\"text-align:right\">({{dataQuotation[0].ExpeditionDiscount | number:2}})</td> </tr> <tr> <!-- <td></td> --> <td>Harga Ekspedisi Setelah Discount</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].PriceAfterExpeditionDiscount | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>DPP</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].ExpeditionDPP | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>VAT</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].ExpeditionVAT | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Total Logistik</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].ExpeditionTotal | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Total DPP</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].TotalDPP | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Total VAT</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].TotalVAT | number:2}}</td> </tr> </tbody> </table> </div> </li> </ul> </div> <div ng-if=\"dataQuotation[0].QuotationTypeId == 1\" class=\"row\"> <ul style=\"padding-left: 25px; padding-right: 20px; margin-bottom: 25px;list-style: none\"> <li> <div style=\"border:1px solid lightgrey; padding: 15px 15px 0 15px; margin-top: 10px\"> <i class=\"fa fa-car\"></i>&nbsp&nbsp <b>{{dataQuotation[0].Dekatsu}}</b> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <!-- <th class=\"headerRow\">Model Tipe</th> --> <th class=\"headerRow\">Keterangan</th> <th class=\"headerRow\">Mata Uang</th> <th class=\"headerRow\">Nominal</th> </tr> </thead> <tbody> <tr> <!-- <th scope=\"row\">Avanza 1.3 E M/T</th> --> <td><b>Harga dasar jual</b></td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].SellPrice | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td><b>Diskon</b></td> <td>Rp.</td> <td style=\"text-align:right\">({{dataQuotation[0].Discount | number:2}})</td> </tr> <tr> <!-- <td></td> --> <td>Harga Setelah Discount</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].PriceAfterDiscVehicle | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>DPP</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].DPP | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>VAT</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].VAT | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>PPH22</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].PPH22 | number:2}}</td> </tr> <!-- <tr>\r" +
    "\n" +
    "                                   \r" +
    "\n" +
    "                                    <td>Total DPP</td>\r" +
    "\n" +
    "                                    <td>Rp.</td>\r" +
    "\n" +
    "                                    <td style=\"text-align:right\">{{dataQuotation[0].TotalDPP | number:2}}</td>\r" +
    "\n" +
    "                                </tr>\r" +
    "\n" +
    "                                <tr>\r" +
    "\n" +
    "                                   \r" +
    "\n" +
    "                                    <td>Total VAT</td>\r" +
    "\n" +
    "                                    <td>Rp.</td>\r" +
    "\n" +
    "                                    <td style=\"text-align:right\">{{dataQuotation[0].TotalVAT | number:2}}</td>\r" +
    "\n" +
    "                                </tr> --> </tbody> </table><br> <b>Ekspedisi</b> <table class=\"table table-bordered table-responsive\" style=\"margin-top:10px\"> <thead> <tr> <!-- <th class=\"headerRow\">Model Tipe</th> --> <th class=\"headerRow\">Keterangan</th> <th class=\"headerRow\">Mata Uang</th> <th class=\"headerRow\">Nominal</th> </tr> </thead> <tbody> <tr> <!-- <th scope=\"row\">Avanza 1.3 E M/T</th> --> <td><b>Harga Ekspedisi</b></td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].ExpeditionSellPrice | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Diskon Ekspedisi</td> <td>Rp.</td> <td style=\"text-align:right\">({{dataQuotation[0].ExpeditionDiscount | number:2}})</td> </tr> <tr> <!-- <td></td> --> <td>Harga Ekspedisi Setelah Discount</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].PriceAfterExpeditionDiscount | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>DPP</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].ExpeditionDPP | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>VAT</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].ExpeditionVAT | number:2}}</td> </tr> <tr> <!-- <td></td> --> <td>Total Logistik</td> <td>Rp.</td> <td style=\"text-align:right\">{{dataQuotation[0].ExpeditionTotal | number:2}}</td> </tr> <!-- <tr>\r" +
    "\n" +
    "                                        \r" +
    "\n" +
    "                                        <td>Total DPP</td>\r" +
    "\n" +
    "                                        <td>Rp.</td>\r" +
    "\n" +
    "                                        <td style=\"text-align:right\">{{dataQuotation[0].TotalDPP | number:2}}</td>\r" +
    "\n" +
    "                                    </tr>\r" +
    "\n" +
    "                                    <tr>\r" +
    "\n" +
    "                                       \r" +
    "\n" +
    "                                        <td>Total VAT</td>\r" +
    "\n" +
    "                                        <td>Rp.</td>\r" +
    "\n" +
    "                                        <td style=\"text-align:right\">{{dataQuotation[0].TotalVAT | number:2}}</td>\r" +
    "\n" +
    "                                    </tr> --> </tbody> </table> </div> <!-- <div style=\"border:1px solid lightgrey; padding: 15px 15px 0 15px; margin-top: 10px\">\r" +
    "\n" +
    "                           \r" +
    "\n" +
    "                            \r" +
    "\n" +
    "                        </div> --> </li> <!-- <li>\r" +
    "\n" +
    "                   \r" +
    "\n" +
    "                </li> --> </ul> </div> <!-- \r" +
    "\n" +
    "        <div class=\"row \" style=\"padding: 12px \">\r" +
    "\n" +
    "            <div ui-grid=\"gridrincianHarga \" ui-grid-grouping class=\"grid \" ui-grid-pagination ui-grid-auto-resize></div>\r" +
    "\n" +
    "        </div> --> <fieldset ng-disabled=\"true\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label></label> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>DPP</label> <div class=\"input-icon-right\"> <input type=\"text \" input-currency number-only style=\"text-align: right\" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].TotalDPP \" ng-maxlength=\"50 \" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Total(VAT)</label> <div class=\"input-icon-right\"> <input type=\"text \" input-currency number-only style=\"text-align: right\" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].TotalVAT \" ng-maxlength=\"50 \" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Grand Total</label> <div class=\"input-icon-right\"> <input type=\"text \" input-currency number-only style=\"text-align: right\" class=\"form-control\" name=\"nama \" placeholder=\" \" ng-model=\"dataQuotation[0].GrandTotal \" ng-maxlength=\"50 \" required> </div> </div> </div> </div> </fieldset> </div> <div id=\"menuEkspedisiTamswap\" class=\"tab-pane fade\"> <h5>* Untuk invoice akan dikirim oleh TAM</h5> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Biaya TAM Ekspedisi</label> <div class=\"input-icon-right\"> <i class=\"fa\"></i> <input input-currency number-only type=\"text\" ng-disabled=\"true\" class=\"form-control\" name=\"nama\" placeholder=\"Biaya\" ng-model=\"dataQuotation[0].TAMExpedition\" ng-maxlength=\"60\"> </div> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/maintainTS/maintainTS.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">fieldset.scheduler-border {\r" +
    "\n" +
    "        border: 1px groove #ddd !important;\r" +
    "\n" +
    "        padding: 0 1em 2em 2em !important;\r" +
    "\n" +
    "        margin: 10px;\r" +
    "\n" +
    "        -webkit-box-shadow: 0px 0px 0px 0px #000;\r" +
    "\n" +
    "        box-shadow: 0px 0px 0px 0px #000;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    legend.scheduler-border {\r" +
    "\n" +
    "        width: inherit;\r" +
    "\n" +
    "        /* Or auto */\r" +
    "\n" +
    "        padding: 0 10px;\r" +
    "\n" +
    "        /* To give a bit of padding on the left and right */\r" +
    "\n" +
    "        border-bottom: none;\r" +
    "\n" +
    "    }</style> <div ng-show=\"ShowMaintainForm\"> <button id=\"SuratKehilangan\" ng-click=\"btnBuatTS()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px\" ng-disabled=\"btnSuratKehilangan\"><i class=\"fa fa-fw fa-plus\"></i><span class=\"ladda-spinner\"></span>Tambah</button> <bsform-grid ng-form=\"MaintainTSForm\" factory-name=\"MaintainTSFactory\" model=\"mMaintainTS\" model-id=\"POId\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" hide-new-button=\"true\" form-name=\"MaintainTSForm\" form-title=\"Maintain TS\" modal-title=\"Maintain TS\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "                    <div class=\"form-group\">\r" +
    "\n" +
    "                        <bsreqlabel>Transfer Stock Status</bsreqlabel>\r" +
    "\n" +
    "                        <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.TSStatusId\" data=\"StatusTS\" item-text=\"TSStatusName\" item-value=\"TSStatusId\" on-select=\"Id\" placeholder=\"Status Transfer\" icon=\"fa fa-search\">\r" +
    "\n" +
    "                        </bsselect>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div> --> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Transfer Stock</bsreqlabel> <bsdatepicker name=\"Sdate\" date-options=\"dateOptions\" icon=\"fa fa-calendar\" ng-model=\"filter.TSDateStart\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Sampai Tanggal</bsreqlabel> <bsdatepicker name=\"Edate\" date-options=\"dateOptions\" icon=\"fa fa-calendar\" ng-model=\"filter.TSDateEnd\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. Transfer Stock</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"No\" placeholder=\"\" ng-model=\"filter.TSNo\" maxlength=\"20\"> </div> </div> </div> </div> <div class=\"row\"> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"ShowFormCreateTS\" style=\"height: 100vh\"> <button ng-hide=\"flag == 'lihat'\" ng-disabled=\"formHeader.$invalid || formHeaderDate.$invalid\" ng-click=\"btnSimpanTS()\" class=\"rbtn btn\" style=\"float:right; margin:-30px 0 0 1px\">Simpan</button> <button ng-if=\"flag == 'lihat'\" ng-click=\"btnCatak()\" class=\"rbtn btn\" style=\"float:right; margin:-30px 0 0 1px\">Cetak</button> <button ng-hide=\"flag == 'lihat'\" ng-disabled=\"formHeader.$invalid || formHeaderDate.$invalid\" ng-click=\"btnPreviewDokumen()\" class=\"rbtn btn\" style=\"float:right; margin:-30px 0 0 1px\">Simpan dan Cetak</button> <button ng-click=\"btnKembaliFormCreateTS()\" class=\"wbtn btn\" style=\"float:right; margin:-30px 0 0 1px\">Kembali</button> <fieldset ng-disabled=\"DisableBuatTS\" style=\"width:100%; padding-top: 15px\"> <form name=\"formHeader\"> <div class=\"row\"> <div class=\"col-md-4\" ng-if=\"flag == 'add'\"> <div class=\"form-group\"> <bsreqlabel>Cabang yang Meminta</bsreqlabel> <bsselect required id=\"comboBox\" name=\"Cabang\" ng-model=\"DetailQuotation.OutletId\" placeholder=\"Outlet Requestor\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"filterModel(selected)\" placeholder=\"\" icon=\"fa fa-sitemap\"> </bsselect> <div class=\"row\" ng-show=\"formHeader.Cabang.$touched && formHeader.Cabang.$invalid\" style=\"margin: 5px 0 0 0\"> <i style=\"color: rgb(185, 74, 72)\" aria-live=\"assertive\">Wajib diisi.</i> </div> </div> </div> <div class=\"col-md-4\" ng-if=\"flag != 'add'\"> <div class=\"form-group\"> <bsreqlabel>Cabang yang Meminta</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"No\" placeholder=\"Outlet Requestor\" ng-model=\"DetailQuotation.RequesterOutletName\" ng-maxlength=\"60\" required ng-disabled=\"true\"> </div> </div> <div class=\"col-md-4\" ng-if=\"flag == 'add'\"> <div class=\"form-group\"> <bsreqlabel>No. Quotation</bsreqlabel> <bsselect required id=\"comboBox\" ng-disabled=\"getQuot.length == 0 || DetailQuotation.OutletId == null\" name=\"Quotation\" ng-model=\"DetailQuotation.QuotationId\" data=\"getQuot\" item-text=\"QuotationNo\" item-value=\"QuotationId\" on-select=\"getDetailQuo()\" placeholder=\"No. Quotation\" icon=\"fa fa-search\"> </bsselect> <div class=\"row\" ng-if=\"formHeader.Quotation.$touched && formHeader.Quotation.$invalid\" style=\"margin: 5px 0 0 0\"> <i style=\"color: rgb(185, 74, 72)\" aria-live=\"assertive\">Wajib diisi.</i> </div> <div class=\"row\" ng-if=\"getQuot.length == 0 && DetailQuotation.OutletId != null\" style=\"margin: 5px 0 0 0\"> <i style=\"color: rgb(185, 74, 72)\" aria-live=\"assertive\">Tidak ada Quotation dari cabang ini.</i> </div> </div> </div> <div class=\"col-md-4\" ng-if=\"flag != 'add'\"> <div class=\"form-group\"> <bsreqlabel>No. Quotation</bsreqlabel> <input type=\"text\" class=\"form-control\" name=\"No\" placeholder=\"No. Quotation\" ng-model=\"DetailQuotation.QuotationNo\" ng-maxlength=\"60\" required ng-disabled=\"true\"> </div> </div> </div> </form> <hr style=\"margin-top: 10px !important; margin-buttom: 10px !important\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tipe Material</label> <input type=\"text\" class=\"form-control\" name=\"No\" placeholder=\"\" ng-model=\"DetailQuotation.MaterialTypeName\" ng-maxlength=\"60\" required ng-disabled=\"true\"> </div> </div> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <bsreqlabel>Tanggal Kirim</bsreqlabel>\r" +
    "\n" +
    "                    <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\" icon=\"fa fa-calendar\" ng-model=\"DetailQuotation.SentDate\">\r" +
    "\n" +
    "                    </bsdatepicker>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> <form name=\"formHeaderDate\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Kirim</bsreqlabel> <input type=\"datetime-local\" class=\"form-control\" name=\"sendDateTS\" ng-change=\"dateChange(DetailQuotation.SentDate)\" ng-model=\"DetailQuotation.SentDate\" placeholder=\"HH:mm\" min=\"minSendDate\" required style=\"line-height: 25px\"> </div> </div> </form> </div> <hr style=\"margin-top: 10px !important; margin-buttom: 10px !important\"> <div class=\"row\"> <div class=\"col-md-8\"> <div class=\"form-group\"> <label>Catatan</label> <textarea maxlength=\"100\" type=\"text\" class=\"form-control\" name=\"Note\" placeholder=\"\" ng-model=\"DetailQuotation.Note\" rows=\"5\" cols=\"60\">\r" +
    "\n" +
    "                </textarea></div> </div> </div> <fieldset ng-disabled=\"true\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Model Tipe</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Model\" placeholder=\"\" ng-model=\"DetailQuotation.VehicleTypeDescription\" ng-maxlength=\"60\"> </div> </div> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Rangka\" placeholder=\"\" ng-model=\"DetailQuotation.FrameNo\" ng-maxlength=\"60\"> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Warna</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Warna\" placeholder=\"\" ng-model=\"DetailQuotation.ColorName\" ng-maxlength=\"60\"> </div> </div> <div class=\"form-group\"> <label>Tahun Produksi</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"Produksi\" placeholder=\"\" ng-model=\"DetailQuotation.AssemblyYear\" ng-maxlength=\"60\"> </div> </div> </div> </div> </fieldset> </fieldset> </div> <div class=\"ui small modal batalTS\" role=\"dialog\" style=\"background-color: transparent !important\"> <!-- <div> --> <!-- <div class=\"row\" style=\"margin: 0 0 0 0\"> --> <!-- <p > --> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <!-- <button type=\"button\" ng-click=\"backFromterimaSTNK()\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> <h4>Konfirmasi</h4> </div> <div class=\"modal-body\" style=\"background-color: white;  max-height: 60%\"> Apakah anda yakin ingin membatalkan Transfer Stock ini ? </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <button ng-click=\"BatalTS()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right\">Setuju</button> <button ng-click=\"keluarBatalTS()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Batal</button> </div> <!-- </p> --> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/manualMatchingProsses/manualMatchingProsses.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">.nopadding {\r" +
    "\n" +
    "        padding-right: 0 !important;\r" +
    "\n" +
    "        padding-top: 0 !important;\r" +
    "\n" +
    "        padding-bottom: 0 !important;\r" +
    "\n" +
    "        padding-left: 1px;\r" +
    "\n" +
    "        margin: 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .table {\r" +
    "\n" +
    "        font-family: arial, sans-serif;\r" +
    "\n" +
    "        border-collapse: collapse;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .td,\r" +
    "\n" +
    "    .th {\r" +
    "\n" +
    "        border: 1px solid #dddddd;\r" +
    "\n" +
    "        text-align: left;\r" +
    "\n" +
    "        padding: 8px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .tr:nth-child(even) {\r" +
    "\n" +
    "        background-color: #dddddd;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .gridStyle {\r" +
    "\n" +
    "        border: 1px solid rgb(212, 212, 212);\r" +
    "\n" +
    "        width: 400px;\r" +
    "\n" +
    "        height: 300px\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <div id=\"cekValidSpk\"> <button id=\"btnPembeliIndividu\" onclick=\"\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px; width:11%\">Find Unit</button> <bsform-grid ng-form=\"ManualMatchingProssesForm\" factory-name=\"ManualMatchingProssesFactory\" loading=\"loading\" model=\"mmanualMatchingProsses\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" form-name=\"ManualMatchingProssesForm\" hide-new-button=\"true\" form-title=\"\" modal-title=\"ManualMatchingProsses\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <bsreqlabel>Tanggal SPK</bsreqlabel> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\" ng-model=\"mmanualMatchingProsses.VehicleModelId\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label> Sampai Tanggal </label> <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\" ng-model=\"mmanualMatchingProsses.VehicleModelId\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No SPK</label> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"No SPK\" ng-model=\"mmanualMatchingProsses.FormSPKNo\" ng-maxlength=\"60\" required> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Nama Sales</label> <bsselect name=\"Sales\" ng-model=\"mmanualMatchingProsses.OutletId\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"OutletId\" placeholder=\"Sales\"> </bsselect> </div> </div> </div> <div class=\"row\" style=\"margin-bottom: 20px\"> </div> </bsform-advsearch> </bsform-grid> <div class=\"row\" style=\"padding:10px\"> <button ng-click=\"btnPembeliIndividu\" class=\"rbtn btn ng-binding ladda-button\" style=\"width:15%; float:right\">Match</button> <button ng-click=\"btnPembeliIndividu\" class=\"rbtn btn ng-binding ladda-button\" style=\"width:15%; float:right\">Suggest Match</button> <button ng-click=\"btnPembeliIndividu\" class=\"rbtn btn ng-binding ladda-button\" style=\"width:15%; float:right\">Un-match</button> </div> <!-- Modal 2 Template: Confirmation Pop Up--> <div class=\"modal fade\" id=\"ModalConfirmation\" role=\"dialog\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">NO Rangka/RRN No.</h4> </div> <div class=\"modal-body\"> <div class=\"row\"> <div class=\"col-md-6 nopadding\"> <div class=\"filtersearch col-md-9 nopadding\" style=\"padding-bottom:5px;padding-top: 10px\"> <div style=\"padding-left: 15px\"> <div class=\"input-group\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control ng-pristine ng-valid ng-empty ng-touched\" placeholder=\"Filter\" ng-model=\"gridApi.grid.columns[filterColIdx].filters[0].term\" style=\"\"> <div class=\"input-group-btn dropdown\" uib-dropdown=\"\" style=\"\"> <button id=\"filter\" type=\"button\" class=\"btn wbtn ng-binding dropdown-toggle\" uib-dropdown-toggle=\"\" data-toggle=\"dropdown\" tabindex=\"-1\" aria-haspopup=\"true\" aria-expanded=\"false\">Pilih&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu=\"\" role=\"menu\" aria-labelledby=\"filter\"> <!-- ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">No Rangka<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[1].filters[0].term!='' &amp;&amp;gridApi.grid.columns[1].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">Model<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[2].filters[0].term!='' &amp;&amp;gridApi.grid.columns[2].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> <li role=\"menuitem\" class=\"ng-scope\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\" class=\"ng-binding\" tabindex=\"0\">Type<span class=\"pull-right\"><i class=\"fa fa-fw fa-filter ng-hide\" ng-show=\"gridApi.grid.columns[3].filters[0].term!='' &amp;&amp;gridApi.grid.columns[3].filters[0].term!=undefined\"></i></span> </a> </li> <!-- end ngRepeat: col in gridCols --> </ul> </div> </div> </div> </div> <div class=\"col-md-3 nopadding\" style=\"float: right;padding-top: 10px\"> <button id=\"cari\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; width: 100%\">Cari</button> </div> </div> </div> <br> <table class=\"table\"> <tr class=\"tr\"> <th class=\"th\" style=\"width: 2%\"></th> <th class=\"th\" style=\"width: 10%\">No Rangka/RRN</th> <th class=\"th\" style=\"width: 10%\">Model</th> <th class=\"th\" style=\"width: 15%\">Tipe</th> <th class=\"th\" style=\"width: 15%\">Warna</th> <th class=\"th\" style=\"width: 15%\">Tahun Produksi</th> <th class=\"th\" style=\"width: 15%\">Status Stock</th> </tr> <tr class=\"tr\"> <td class=\"td\"><input type=\"checkbox\" name=\"lunas\" value=\"\"></td> <td class=\"td\">MHX00016652418</td> <td class=\"td\">Avanza</td> <td class=\"td\">Avanza 1.3 M/T</td> <td class=\"td\">Abu-Abu</td> <td class=\"td\">2016</td> <td class=\"td\">Free</td> </tr> </table> </div> <div class=\"modal-footer\"> <p align=\"right\"> <button onclick=\"Tidak_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">Batal</button> <button onclick=\"\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">Pilih</button> </p> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/matchingStockDealer/matchingStockDealer.html',
    "<!-- <script data-require=\"angular.js@*\" data-semver=\"1.2.0-rc2\" src=\"http://code.angularjs.org/1.2.0-rc.2/angular.js\">\r" +
    "\n" +
    "</script> --> <script src=\"script.js\"></script> <style type=\"text/css\">.button .bordered,\r" +
    "\n" +
    "    .outlined {\r" +
    "\n" +
    "        display: table;\r" +
    "\n" +
    "        margin: 1em;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .Search {\r" +
    "\n" +
    "        position: absolute!important;\r" +
    "\n" +
    "        border: 1px solid #e2e2e2;\r" +
    "\n" +
    "        border-color: #e2e2e2 #e2e2e2 #fff!important;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "        height: 37px;\r" +
    "\n" +
    "        z-index: 1;\r" +
    "\n" +
    "        padding-top: 0!important;\r" +
    "\n" +
    "        left: -40px;\r" +
    "\n" +
    "        padding-right: 6px!important;\r" +
    "\n" +
    "        background-color: rgba(213, 51, 55, .02);\r" +
    "\n" +
    "    }</style> <div ng-show=\"MainFormShow\"> <button ng-click=\"btnsuggestSwapping()\" ng-disabled=\"filter.AreaId == null\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-35px 0 0 10px; width:13%\">Suggest Swapping</button> <div align=\"right\"> <button ng-click=\"ShowMatchingStockDealerAdvSearch=!ShowMatchingStockDealerAdvSearch\" class=\"Search\" style=\"margin-left: 82%; margin-top: -35px\"> <i class=\"fa fa-fw fa-search\" style=\"font-size:1.4em\"></i> </button> <button class=\"ubtn mobileFilterBtn\" ng-click=\"ActRefreshMatchingStockDealer()\" style=\"margin-left: 83%;margin-top: -35px\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> </div> <div ng-show=\"ShowMatchingStockDealerAdvSearch\" class=\"row\" style=\"padding-left: 12px; padding-right: 12px\"> <div class=\"col-md-12 advsearch\" style=\"padding:10px;margin-bottom:0px;border:1px solid #e2e2e2;background-color:rgba(213,51,55,0.02)\"> <div class=\"col-xs-12\"> <div class=\"col-xs-3\"> <div class=\"form-group\"> <bsreqlabel>Area</bsreqlabel> <bsselect name=\"Area\" ng-model=\"filter.AreaId\" data=\"getDataArea\" ng-change=\"getProvince(filter.AreaId)\" on-select=\"getAreaName(selected)\" item-text=\"AreaName\" item-value=\"AreaId\" placeholder=\"Area\" required icon=\"fa fa-globe\"> </bsselect> </div> </div> <div class=\"col-xs-3\"> <div class=\"form-group\"> <bslabel>Province</bslabel> <bsselect name=\"Province\" ng-model=\"filter.ProvinceId\" data=\"getDataProvince\" ng-change=\"getCityRegency(filter.ProvinceId)\" on-select=\"getProvinceName(selected)\" ng-disabled=\"filter.AreaId == null\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Province\" required icon=\"fa fa-globe\"> </bsselect> </div> </div> <div class=\"col-xs-3\"> <div class=\"form-group\"> <bslabel>Kota</bslabel> <bsselect name=\"Kota\" ng-model=\"filter.CityRegencyId\" data=\"getCity\" item-text=\"CityRegencyName\" on-select=\"getCityRegencyName(selected)\" ng-disabled=\"(filter.AreaId == null && filter.ProvinceId == null) || filter.AreaId == null\" item-value=\"CityRegencyId\" placeholder=\"Kota\" required icon=\"fa fa-globe\"> </bsselect> </div> </div> <div class=\"col-xs-3\"> <div class=\"form-group\"> <bslabel>Model</bslabel> <bsselect name=\"Model\" ng-model=\"filter.VehicleModelId\" data=\"getModel\" item-text=\"VehicleModelName\" on-select=\"getModelName(selected)\" ng-disabled=\"(filter.AreaId == null && filter.ProvinceId == null) || filter.AreaId == null\" item-value=\"VehicleModelId\" placeholder=\"Model\" required icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <div class=\"col-xs-12\"> <div class=\"col-xs-9\"> </div> <div class=\"col-xs-3\"> <button id=\"btncari\" ng-click=\"filteringData()\" ng-disabled=\"filter.AreaId == null\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width:40%; margin-top:22px\"><i class=\"fa fa-search\"></i></button> </div> </div> </div> </div> <fieldset> <div class=\"row\" style=\"padding-top:12px\"> <div class=\"col-md-6\"> <b align=\"center\" style=\"font-size: x-large\">Outstanding SO</b> <div class=\"bordered\" style=\"overflow-y:scroll;height: 460px\"> <div class=\"highlight\"> <div class=\"listData\" style=\"padding:0px\" ng-repeat=\"outstanding in outstandingSO\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride\" ng-click=\"ShowListGroup = !ShowListGroup\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{{$index+1}} - {{outstanding.OutletName}} <div style=\"float:right;width: auto\"> <!-- <span style=\"font-size: 15px;\">Total Record: {{outstanding.TotalData}}</span> --> <span ng-show=\"ShowListGroup!=true\" style=\"float:right; margin-top: 3px; padding-left: 10px\" class=\"fa fa-plus-square\"></span> <span ng-show=\"ShowListGroup==true\" style=\"float:right; margin-top: 3px; padding-left: 10px\" class=\"fa fa-minus-square\"></span> </div> </button> <div class=\"list-group\" id=\"data\" aria-labelledby=\"dropdownMenu1\" ng-repeat=\"outstandingDetail in outstanding.ListOfMatchingStockDealerSO\" ng-show=\"ShowListGroup\" style=\"margin-bottom:0px\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">{{outstandingDetail.VehicleTypeDescription}}-{{outstandingDetail.KatashikiCode}}-{{outstandingDetail.SuffixCode}} {{outstandingDetail.ColorName}} <div style=\"float:right;width: auto\"> <span style=\"font-size: 15px\">Total : {{outstandingDetail.TotalOutstandingSO}}</span> </div> </div> </div> </div> </div> </div> </div> </div> <div class=\"col-md-6\"> <b style=\"text-align: center; font-size: x-large\">Stock Free</b> <div class=\"bordered\" style=\"overflow-y:scroll;height: 460px\"> <div class=\"highlight\"> <div class=\"listData\" style=\"padding:0px\" ng-repeat=\"Stock in stockFree\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride\" ng-click=\"ShowListGroup1 = !ShowListGroup1\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{{$index+1}} - {{Stock.VehicleTypeDescription}}-{{Stock.KatashikiCode}}-{{Stock.SuffixCode}} {{Stock.ColorName}} <div style=\"float:right;width: auto\"> <!-- <span style=\"font-size: 15px;\">Total Record: {{Stock.TotalData}}</span> --> <span ng-show=\"ShowListGroup1!=true\" style=\"float:right; margin-top: 3px; padding-left: 10px\" class=\"fa fa-plus-square\"></span> <span ng-show=\"ShowListGroup1==true\" style=\"float:right; margin-top: 3px; padding-left: 10px\" class=\"fa fa-minus-square\"></span> </div> </button> <div class=\"list-group\" id=\"data\" aria-labelledby=\"dropdownMenu1\" ng-repeat=\"StockDetail in Stock.ListOfMatchingStockDealerStock\" ng-show=\"ShowListGroup1\" style=\"margin-bottom:0px\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">{{StockDetail.OutletName}} <div style=\"float:right;width: auto\"> <span style=\"font-size: 15px\">Total : {{StockDetail.TotalUnit}}</span> </div> </div> </div> </div> </div> </div> </div> </div> </div> <div class=\"row\" style=\"padding-top: 5px\"> <div class=\"col-md-6\"> <div class=\"form-group\" style=\"float:right\"> <label>Total Record: {{outstandingSO.length}}</label> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" style=\"float:right\"> <label>Total Record: {{stockFree.length}}</label> </div> </div> </div> </fieldset> </div> <div ng-show=\"SuggestFormShow\"> <button ng-click=\"btnRequestSwapping()\" ng-disabled=\"selectedRows.length == 0\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px\">Request Swapping</button> <button ng-click=\"btnkembaliSuggestSwapping()\" class=\"wbtn btn no-animate ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px\">Kembali</button> <div class=\"row\" style=\"padding:10px 12px 0 12px\"> <div class=\"col-md-12 advsearch\" style=\"padding:10px;margin-bottom:0px;border:1px solid #e2e2e2;background-color:rgba(213,51,55,0.02)\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Area Swapping</label> <div class=\"input-icon-right\"> <input type=\"text \" disabled class=\"form-control\" name=\"Area Swapping \" placeholder=\"Area Swapping\" ng-model=\"draft.AreaName\" ng-maxlength=\"50 \"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Provinsi</label> <div class=\"input-icon-right\"> <input type=\"text \" disabled class=\"form-control\" name=\"Provinsi \" placeholder=\"Provinsi \" ng-model=\"draft.ProvinceName \" ng-maxlength=\"50 \"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Kota/Kabupaten</label> <div class=\"input-icon-right\"> <input type=\"text \" disabled class=\"form-control\" name=\"Kota \" placeholder=\"Kota \" ng-model=\"draft.CityRegencyName \" ng-maxlength=\"50 \"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Model</label> <div class=\"input-icon-right\"> <input type=\"text \" disabled class=\"form-control\" name=\"Model \" placeholder=\"Model \" ng-model=\"SelectedModelName \" ng-maxlength=\"50 \"> </div> </div> </div> </div> </div> <!-- <div class=\"row\" style=\"padding-top: 15px\">\r" +
    "\n" +
    "        <div class=\"col-md-3\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <label>Swapping Area : {{draft.AreaName}}</label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"col-md-3\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <label>Province : {{draft.ProvinceName}}</label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"col-md-3\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <label>Kota/Kabupaten : {{draft.CityRegencyName}}</label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-md-3\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <label>Model: {{SelectedModelName}}</label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <hr> <bsform-grid ng-form=\"StockDealerForm\" factory-name=\"MatchingStockDealerFactory\" model=\"mStockDealer\" model-id=\"StockDealerId\" get-data=\"refreshListSwapping\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" form-name=\"StockDealerForm\" form-title=\"Stock Dealer\" modal-title=\"Stock Dealer\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" hide-new-button=\"true\"> </bsform-grid> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/modelAssignment/modelAssignment.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">.ui-grid-viewport {\r" +
    "\n" +
    "        overflow-anchor: none;\r" +
    "\n" +
    "    }</style> <div ng-show=\"targetAssignment\"> <bsform-grid ng-form=\"StockViewForm\" factory-name=\"TargetAssignmentFactory\" model=\"mTargetAssign\" model-id=\"EmployeeId\" get-data=\"getData\" selected-rows=\"selectedRows\" show-advsearch=\"on\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" form-name=\"StockViewForm\" form-title=\"\" modal-title=\"\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" hide-new-button=\"true\"> <bsform-advsearch> <div class=\"row\"> <div ng-if=\"Role.RoleName == 'SUPERVISOR SALES'\" class=\"col-md-3\"> <div class=\"form-group\"> <label>Sales</label> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.EmployeeId\" data=\"getDataSalesman\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" on-select=\"EmployeeId\" placeholder=\"Nama Sales\" icon=\"fa fa-user\"> </bsselect> </div> </div> <div ng-if=\"Role.RoleName == 'KACAB'\" class=\"col-md-3\"> <div class=\"form-group\"> <label>Supervisor</label> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.EmployeeId\" data=\"getDataSalesman\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" on-select=\"EmployeeId\" placeholder=\"Nama Supervisor\" icon=\"fa fa-user\"> </bsselect> </div> </div> <div ng-if=\"Role.RoleName == 'SUPERVISOR SALES'\" class=\"col-md-3\"> <div class=\"form-group\"> <label>Kategori Sales</label> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"filter.SalesCategoryId\" data=\"categoriSales\" item-text=\"GolonganSalesmanName\" item-value=\"GolonganSalesmanId\" on-select=\"GolonganSalesmanId\" placeholder=\"Kategori Sales\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Model</label> <bsselect name=\"ModelKendaraan\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"filter.VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Pilih Model\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> <bserrmsg field=\"VehicleModelName\"></bserrmsg> </div> </div> </bsform-advsearch> </bsform-grid> </div> <div ng-show=\"targetDetail\"> <button ng-click=\"btnSimpanAssign()\" ng-hide=\"disabledModel\" class=\"rbtn btn\" style=\"float:right; margin:-30px 0 0 1px\">Simpan</button> <button ng-click=\"btnBatalAssign()\" class=\"wbtn btn no-animate\" style=\"float:right; margin:-30px 0 0 1px\">Kembali</button> <fieldset ng-disabled=\"true\"> <div class=\"row\" style=\"padding-top:15px\"> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "                <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Sales ID</label>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                            <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"ViewDetail.EmployeeId\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> <div class=\"col-md-4\"> <div class=\"form-group\"> <label ng-if=\"Role.RoleName == 'KACAB'\">Supervisor</label> <label ng-if=\"Role.RoleName == 'SUPERVISOR SALES'\">Sales</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"ViewDetail.EmployeeName\" ng-maxlength=\"50\" required> </div> </div> </div> <div ng-if=\"Role.RoleName == 'SUPERVISOR SALES'\" class=\"col-md-4\"> <div class=\"form-group\"> <label>Kategori Sales</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"\" placeholder=\"\" ng-model=\"ViewDetail.SalesCategoryName\" ng-maxlength=\"50\" required> </div> </div> </div> </div> </fieldset> <fieldset ng-disabled=\"disabledModel\"> <div class=\"row\" style=\"margin: 0 0 0 8px\"> <!--<div class=\"switch\">\r" +
    "\n" +
    "                        <input  ng-disabled=\"isEdit == 'false'\" ng-change=\"trueAll()\" ng-model=\"mSelectAll\" class=\"cmn-toggle cmn-toggle-round-flat\" type=\"checkbox\">\r" +
    "\n" +
    "                        Select All</input>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                </div>--> <!-- <bscheckbox ng-model=\"\" label=\"Select All\" ng-change=\"\" true-value=\"true\" false-value=\"false\"></bscheckbox> --> </div> <div class=\"row\" style=\"margin: 0 0 0 0\"> <div ui-grid=\"gridModel\" style=\"height: 480px\" class=\"grid\" ui-grid-auto-resize ui-grid-selection></div> </div> </fieldset> </div> <!-- <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                    <div style=\"width: 14% ;float:left\">\r" +
    "\n" +
    "                            <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                    <div class=\"list-group\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom:0px;\">\r" +
    "\n" +
    "                                        <div class=\"list-group-item\" style=\"margin: 0 6px 0 0\" >\r" +
    "\n" +
    "                                            <div class=\"row\">\r" +
    "\n" +
    "                                                <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                        <i class=\"fa fa-car\"></i>&nbspSedan\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div style=\"width: 14% ;float:left\">\r" +
    "\n" +
    "                            <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                    <div class=\"list-group\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom:0px;\">\r" +
    "\n" +
    "                                        <div class=\"list-group-item\" style=\"margin: 0 6px 0 0\" >\r" +
    "\n" +
    "                                            <div class=\"row\">\r" +
    "\n" +
    "                                                <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                        <i class=\"fa fa-car\"></i>&nbspHatchBack\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div style=\"width: 14% ;float:left\">\r" +
    "\n" +
    "                            <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                    <div class=\"list-group\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom:0px;\">\r" +
    "\n" +
    "                                        <div class=\"list-group-item\" style=\"margin: 0 6px 0 0\" >\r" +
    "\n" +
    "                                            <div class=\"row\">\r" +
    "\n" +
    "                                                <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                        <i class=\"fa fa-car\"></i>&nbspMPV\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div style=\"width: 14% ;float:left\">\r" +
    "\n" +
    "                            <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                    <div class=\"list-group\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom:0px;\">\r" +
    "\n" +
    "                                        <div class=\"list-group-item\" style=\"margin: 0 6px 0 0\" >\r" +
    "\n" +
    "                                            <div class=\"row\">\r" +
    "\n" +
    "                                                <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                        <i class=\"fa fa-car\"></i>&nbspSUV\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div style=\"width: 14% ;float:left\">\r" +
    "\n" +
    "                            <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                    <div class=\"list-group\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom:0px;\">\r" +
    "\n" +
    "                                        <div class=\"list-group-item\" style=\"margin: 0 6px 0 0\" >\r" +
    "\n" +
    "                                            <div class=\"row\">\r" +
    "\n" +
    "                                                <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                        <i class=\"fa fa-car\"></i>&nbspCommercial\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div style=\"width: 14% ;float:left\">\r" +
    "\n" +
    "                            <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                    <div class=\"list-group\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom:0px;\">\r" +
    "\n" +
    "                                        <div class=\"list-group-item\" style=\"margin: 0 6px 0 0\" >\r" +
    "\n" +
    "                                            <div class=\"row\">\r" +
    "\n" +
    "                                                <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                        <i class=\"fa fa-car\"></i>&nbspSport\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div style=\"width: 14% ;float:left\">\r" +
    "\n" +
    "                            <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                    <div class=\"list-group\" aria-labelledby=\"dropdownMenu1\" style=\"margin-bottom:0px;\">\r" +
    "\n" +
    "                                        <div class=\"list-group-item\" >\r" +
    "\n" +
    "                                            <div class=\"row\">\r" +
    "\n" +
    "                                                <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                        <i class=\"fa fa-car\"></i>&nbspHybrid\r" +
    "\n" +
    "                                                </div>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"row\" style=\"margin: 0 0 0 0\">\r" +
    "\n" +
    "                <div style=\"width: 14%; float: left\">\r" +
    "\n" +
    "                        <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                    <div class=\"\" style=\"margin: 0 0px 0 0\"  ng-repeat=\"Model in getDataDetail\">\r" +
    "\n" +
    "                                      \r" +
    "\n" +
    "                                                <bscheckbox ng-model=\"Model.pilih\"\r" +
    "\n" +
    "                                                    label = \"{{Model.VehicleModelName}}\"\r" +
    "\n" +
    "                                                    true-value = \"true\"\r" +
    "\n" +
    "                                                    false-value = \"false\">\r" +
    "\n" +
    "                                           \r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                            \r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div style=\"width: 14%; float: left\">\r" +
    "\n" +
    "                        <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                    <div class=\"\" style=\"margin: 0 0px 0 0\"  ng-repeat=\"Model in getDataDetail\">\r" +
    "\n" +
    "                                        <div class=\"row\">\r" +
    "\n" +
    "                                            <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                <bscheckbox ng-model=\"Model.pilih\"\r" +
    "\n" +
    "                                                    label = \"{{Model.VehicleModelName}}\"\r" +
    "\n" +
    "                                                    true-value = \"true\"\r" +
    "\n" +
    "                                                    false-value = \"false\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div style=\"width: 14%; float: left\">\r" +
    "\n" +
    "                        <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                    <div class=\"\" style=\"margin: 0 0px 0 0\"  ng-repeat=\"Model in getDataDetail\">\r" +
    "\n" +
    "                                        <div class=\"row\">\r" +
    "\n" +
    "                                            <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                <bscheckbox ng-model=\"Model.pilih\"\r" +
    "\n" +
    "                                                    label = \"{{Model.VehicleModelName}}\"\r" +
    "\n" +
    "                                                    true-value = \"true\"\r" +
    "\n" +
    "                                                    false-value = \"false\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                               \r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div style=\"width: 14%; float: left\">\r" +
    "\n" +
    "                        <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                    <div class=\"\" style=\"margin: 0 0px 0 0\"  ng-repeat=\"Model in getDataDetail\">\r" +
    "\n" +
    "                                        <div class=\"row\">\r" +
    "\n" +
    "                                            <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                <bscheckbox ng-model=\"Model.pilih\"\r" +
    "\n" +
    "                                                    label = \"{{Model.VehicleModelName}}\"\r" +
    "\n" +
    "                                                    true-value = \"true\"\r" +
    "\n" +
    "                                                    false-value = \"false\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                               \r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div style=\"width: 14%; float: left\">\r" +
    "\n" +
    "                        <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                    <div class=\"\" style=\"margin: 0 0px 0 0\"  ng-repeat=\"Model in getDataDetail\">\r" +
    "\n" +
    "                                        <div class=\"row\">\r" +
    "\n" +
    "                                            <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                <bscheckbox ng-model=\"Model.pilih\"\r" +
    "\n" +
    "                                                    label = \"{{Model.VehicleModelName}}\"\r" +
    "\n" +
    "                                                    true-value = \"true\"\r" +
    "\n" +
    "                                                    false-value = \"false\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                               \r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div style=\"width: 14%; float: left\">\r" +
    "\n" +
    "                        <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                    <div class=\"\" style=\"margin: 0 0px 0 0\"  ng-repeat=\"Model in getDataDetail\">\r" +
    "\n" +
    "                                        <div class=\"row\">\r" +
    "\n" +
    "                                            <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                <bscheckbox ng-model=\"Model.pilih\"\r" +
    "\n" +
    "                                                    label = \"{{Model.VehicleModelName}}\"\r" +
    "\n" +
    "                                                    true-value = \"true\"\r" +
    "\n" +
    "                                                    false-value = \"false\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div style=\"width: 14%; float: left\">\r" +
    "\n" +
    "                        <div class=\"listData\" style=\"width:100%;\" ng-init=\"style={'color':'#3df747'}\">\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                                    <div class=\"\" style=\"margin: 0 6px 0 0\"  ng-repeat=\"Model in getDataDetail\">\r" +
    "\n" +
    "                                        <div class=\"row\">\r" +
    "\n" +
    "                                            <div class=\"col-sm-12\">\r" +
    "\n" +
    "                                                <bscheckbox ng-model=\"Model.pilih\"\r" +
    "\n" +
    "                                                    label = \"{{Model.VehicleModelName}}\"\r" +
    "\n" +
    "                                                    true-value = \"true\"\r" +
    "\n" +
    "                                                    false-value = \"false\">\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                \r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            </div> -->"
  );


  $templateCache.put('app/sales/sales5/demandSupply/salesCompositionHistory/salesCompositionHistory.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "          display:none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }</style> <div ng-show=\"KacabView\"> <fieldset ng-show=\"showSimpanKacab\" style=\"padding-bottom: 12px\"> <button ng-click=\"btnKacabSimpan()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:11%\" ng-disabled=\"DisabledbtnSave\">simpan</button> <button ng-click=\"btnKacabKembali()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:11%\">Kembali</button> </fieldset> <fieldset ng-disabled=\"disabledPeriod\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Cabang</label> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"Cabang\" ng-model=\"Role.OutletId\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Branch\" ng-disabled=\"true\" icon=\"fa fa-sitemap\" required></bsselect> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Periode Target</bsreqlabel> <bsselect name=\"Periode\" data=\"getPeriod\" item-text=\"PeriodDesc\" item-value=\"moon\" on-select=\"selectPeriod(selected)\" ng-model=\"Role.moon\" placeholder=\"Periode Target\" icon=\"fa fa-calendar\" style=\"min-width: 150px\"> </bsselect> </div> <!-- <bserrmsg field=\"VehicleModelName\"></bserrmsg> --> </div> <div class=\"col-md-4\" ng-show=\"showModel\"> <div class=\"form-group\"> <bsreqlabel>Model Kendaraan</bsreqlabel> <bsselect name=\"Model\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"kacabDetail.VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> </div> </div> </fieldset> <div class=\"row\" ng-show=\"showgridKacab\" style=\"padding: 12px\"> <div ui-grid=\"grid\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-auto-resize></div> </div> <div class=\"row\" ng-show=\"showgridKacabDetail\" style=\"padding: 12px\"> <div ui-grid=\"gridKacabedit\" ui-grid-grouping class=\"grid\" ui-grid-edit ui-grid-pagination ui-grid-auto-resize></div> </div> </div> <div ng-show=\"SupervisorView\"> <fieldset ng-show=\"showSimpanSPV\" style=\"padding-bottom: 12px\"> <button ng-click=\"btnSPVSimpan()\" ng-disabled=\"DisabledbtnSave\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:11%\">simpan</button> <button ng-click=\"btnKacabKembali()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px; width:11%\">Kembali</button> </fieldset> <fieldset ng-disabled=\"disabledPeriod\"> <div class=\"row\"> <div class=\"col-md-4\" ng-disabled=\"true\"> <div class=\"form-group\"> <label>Cabang</label> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"Cabang\" ng-model=\"Role.OutletId\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Branch\" ng-disabled=\"true\" icon=\"fa fa-sitemap\" required></bsselect> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Periode Target</bsreqlabel> <bsselect name=\"Periode\" data=\"getPeriod\" item-text=\"PeriodDesc\" item-value=\"moon\" on-select=\"selectPeriod(selected)\" ng-model=\"Role.moon\" placeholder=\"Periode Target\" icon=\"fa fa-calendar\" style=\"min-width: 150px\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Supervisor Name</bsreqlabel> <bsselect name=\"Supervisor\" data=\"getDataSalesman\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" ng-model=\"Role.EmployeeId\" placeholder=\"Supervisor Name\" icon=\"fa fa-user\" style=\"min-width: 150px\" required ng-disabled=\"true\"> </bsselect> </div> </div> <div class=\"col-md-4\" ng-show=\"showModel\"> <div class=\"form-group\"> <bsreqlabel>Model Kendaraan</bsreqlabel> <bsselect name=\"Model\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"spvDetail.VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\"> </bsselect> </div> </div> </div> </fieldset> <div class=\"row\" ng-show=\"showgridSPV\" style=\"padding: 12px\"> <div ui-grid=\"gridSPV\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-auto-resize></div> </div> <div class=\"row\" ng-show=\"showgridSPVDetail\" style=\"padding: 12px\"> <div ui-grid=\"gridSPVedit\" ui-grid-grouping class=\"grid\" ui-grid-edit ui-grid-pagination ui-grid-auto-resize></div> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/stockView/stockView.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }</style> <div ng-show=\"StockView\" class=\"row\" style=\"margin: 0 0 0 0\"> <bsform-grid ng-form=\"StockViewForm\" factory-name=\"StockViewFactory\" model=\"mStockView\" model-id=\"StockViewId\" get-data=\"getData\" selected-rows=\"selectedRows\" show-advsearch=\"on\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" form-name=\"StockViewForm\" form-title=\"Stock View\" modal-title=\"Stock View\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" hide-new-button=\"true\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Branch</bsreqlabel> <bsselect id=\"comboBox\" name=\"Branch\" ng-model=\"filter.OutletId\" data=\"getOutlet\" item-text=\"Name\" ng-change=\"\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Branch\" icon=\"fa fa-sitemap\" required></bsselect> <bserrmsg field=\"Branch\"></bserrmsg> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model Kendaraan</bsreqlabel> <bsselect name=\"ModelKendaraan\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"filter.VehicleModelId\" on-select=\"filterModel(selected)\" placeholder=\"Model Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> <bserrmsg field=\"ModelKendaraan\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Tipe Kendaraan</label> <bsselect name=\"TypeKendaraan\" data=\"getTipe\" item-text=\"TypeDescKatashikiSuffix\" item-value=\"VehicleTypeId\" ng-model=\"filter.VehicleTypeId\" ng-change=\"pilihType(filter.VehicleTypeId)\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\" required> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Warna Kendaraan</label> <bsselect id=\"comboBox\" name=\"Warna\" ng-model=\"filter.ColorId\" data=\"listWarna\" item-text=\"ColorCodeName\" item-value=\"ColorId\" on-select=\"ColorId\" placeholder=\"Warna Kendaraan\" icon=\"fa fa-car glyph-left\" required> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bslabel>Year</bslabel> <bsselect name=\"Year\" data=\"dataYear\" item-text=\"Year\" item-value=\"Year\" ng-model=\"filter.Year\" placeholder=\"Year\" icon=\"fa fa-calender\" style=\"min-width: 150px\"> </bsselect> </div> </div> </div> </bsform-advsearch> </bsform-grid> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "    <p>\r" +
    "\n" +
    "        <br>\r" +
    "\n" +
    "    </p>\r" +
    "\n" +
    "</div> --> <div class=\"ui modal show_modaldetailKendaraan\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <p style=\"font-size: 18px; font-weight:bold\"> {{detail.VehicleTypeDescription}} {{detail.ColorName}} </p> <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> --> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 16px; font-weight:bold\"> Katashiki </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.KatashikiCode}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Suffix </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.SuffixCode}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Warna </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.ColorCode}} - {{detail.ColorName}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> RRN </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.RRNNo}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> No. Rangka </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.FrameNo}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> No. Mesin </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.EngineNo}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> PLOD </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.PLOD | date:'dd-MM-yyyy'}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> TAM Delivery Order Date </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.DOTAMDate | date:'dd-MM-yyyy'}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Tanggal Matching </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\"><label>{{detail.MatchingDate | date:'dd-MM-yyyy'}}</label></li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Lokasi </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.LastLocation}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Kategori </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.StatusUnitName}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> No. Kunci </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.KeyNumber}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Lokasi Unit </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.DeliveryNoteLocation}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Alamat </p> <p> <ul class=\"list-inline\"> <li style=\"width:100%;font-size: 14px\">{{detail.DeliveryNoteAddress}}</li> </ul> </p> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <!-- <button ng-click=\"btnkehilanganSpk()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width:15%;\">Simpan</button> --> <button ng-click=\"keluarModal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Keluar</button> </div> </div> </div> </div> <!-- <bsui-modal show=\"show_modaldetailKendaraan.show\" title=\"Detail Kendaraan\" data=\"modal_model\" on-save=\"onListSave\" on-cancel=\"onListCancel\" mode=\"modalMode\" width=\"50%\">\r" +
    "\n" +
    "    <div ng-form name=\"contactForm\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "               \r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</bsui-modal> -->"
  );


  $templateCache.put('app/sales/sales5/demandSupply/stockViewMobile/stockViewMobile.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            /*display: none;*/\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }</style> <div ng-show=\"MainForm\"> <div class=\"row\" style=\"margin-left:10px\"> <b>Search</b> </div> <div class=\"row\" style=\"margin-left:0px;width:100%\"> <p align=\"center\"> <div id=\"comboBoxFilter\" style=\"float:left;width:49%; font-size:12px\"> <select ng-model=\"filter.VehicleModelId\" class=\"form-control\" ng-change=\"StockViewMobileDaFilterModelChanged()\" ng-options=\"item.VehicleModelId as item.VehicleModelName for item in optionsModelStockViewMobile\" placeholder=\"Model Kendaraan\"> </select> </div> <div id=\"comboBoxFilter\" style=\"float:left;width:49%; font-size:12px\"> <select ng-model=\"filter.dataYear\" class=\"form-control\" ng-options=\"item.Year as item.Year for item in dataYear\" placeholder=\"Tahun\"> </select> </div> <div id=\"btnFilter\" style=\"float:left;width:2%\"> <button style=\"!important;float:right\" class=\"rbtn btn mobileFilterBtn\" ng-click=\"filterModelStockViewMobile()\"> <i class=\"fa fa-filter iconFilter\"></i> </button> </div> </p> </div> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li class=\"active\" style=\"width:50%\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainer_StockViewMobile','menuOwnStock')\">Stok Cabang</a></li> <li style=\"width:50%\"><a data-toggle=\"tab\" href=\"#\" ng-click=\"\" onclick=\"navigateTab('tabContainer_StockViewMobile','menuFreeStock')\">Dealer Free Stock</a></li> </ul> <div id=\"tabContainer_StockViewMobile\" class=\"tab-content\" style=\"padding-top: 15px\"> <div id=\"menuOwnStock\" class=\"tab-pane fade in active\"> <div class=\"listData\" style=\"padding:0px\" ng-repeat=\"ownStockType in ownStock | orderBy:['KatashikiCode','SuffixCode']\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride\" ng-click=\"ShowListGroup = !ShowListGroup\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{{ownStockType.VehicleTypeDescription}}<br>{{ownStockType.KatashikiCode}} - {{ownStockType.SuffixCode}} <div style=\"float:right;width: auto\"> <span style=\"font-size: 15px\">Total : {{ownStockType.QtyStock}}</span> <span ng-if=\"ShowListGroup==false\" style=\"float:right; margin-top: 5px; padding-left: 10px\" class=\"fa fa-plus-square\"></span> <span ng-if=\"ShowListGroup==true\" style=\"float:right; margin-top: 5px; padding-left: 10px\" class=\"fa fa-minus-square\"></span> </div> </button> <div class=\"list-group\" id=\"data\" aria-labelledby=\"dropdownMenu1\" ng-repeat=\"ownColor in ownStockType.ListOfStockViewTypeColor | orderBy:'ColorName'\" ng-show=\"ShowListGroup\" style=\"margin-bottom:0px\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px\">{{ownColor.ColorName}} <div style=\"float:right;width: auto\"> <span style=\"font-size: 15px\">Total : {{ownColor.QtyStock}}</span> <button class=\"btn btn-default\" style=\"float:right; border:0; background-color:#e8eaed; box-shadow:none\" id=\"myButton\" type=\"button\" ng-click=\"openDetail(ownColor)\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"></span> </button> </div> </div> </div> </div> </div> </div> <div id=\"menuFreeStock\" class=\"tab-pane fade\"> <div class=\"listData\" style=\"padding:0px\" ng-repeat=\"stockBasketType in sBasket\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride2\" ng-click=\"ShowListGroup2 = !ShowListGroup2\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{{stockBasketType.VehicleTypeDescription}}<br>{{stockBasketType.KatashikiCode}} - {{stockBasketType.SuffixCode}} <div style=\"float:right; width: auto\"> <span style=\"font-size:15px\">Total : {{stockBasketType.Stock}}</span> <span ng-if=\"ShowListGroup2==false\" style=\"float:right; margin-top:5px; padding-left:10px\" class=\"fa fa-plus-square\"></span> <span ng-if=\"ShowListGroup2==true\" style=\"float:right; margin-top:5px; padding-left:10px\" class=\"fa fa-minus-square\"></span> </div> </button> <div class=\"list-group\" id=\"data2\" aria-labelledby=\"dropdownMenu2\" ng-repeat=\"sBasketColor in stockBasketType.ListOfStockBasketTypeColor\" ng-show=\"ShowListGroup2\" style=\"margin-bottom:0px\"> <div class=\"list-group-item\" style=\"padding:10px 0px 20px 10px; background-color:#e8eaed\"> <div style=\"font-size: 12px;float:left;width:100%;margin-bottom:5px\"> <div style=\"float:left;width:50%\"> {{sBasketColor.ColorName}} </div> <div style=\"float:left;width:50%\"> <div style=\"float:right\">Total : {{sBasketColor.Stock}}<span ng-click=\"StockViewMobileFreeStockDetail = !StockViewMobileFreeStockDetail\" style=\"margin-right:10px;margin-left:5px\" class=\"fa fa-plus-square\"></span></div> </div> </div> <div ng-show=\"StockViewMobileFreeStockDetail\" style=\"font-size: 10px\" ng-repeat=\"sBasketColorOutlet in sBasketColor.ListOfStockBasketOutlet\"> <ul class=\"list-inline\" style=\"font-size:10px;width:100%\"> <li style=\"font-size:13px;width:49%\">{{sBasketColorOutlet.OutletName}}</li> <li style=\"font-size:13px;width:50%\"><div style=\"float:right;margin-right:1%\">Total:{{sBasketColorOutlet.Stock}}</div></li> </ul> </div> </div> </div> </div> </div> </div> <div class=\"ui small modal Detail\" role=\"dialog\"> <div class=\"content\"> <ul class=\"list-group\"> <a ng-click=\"ViewDetailListData(StockViewMobilePaksaYearLihatDetail)\"><li class=\"list-group-item\">Lihat Detail</li></a> </ul> </div> </div> </div> <div ng-show=\"showDetail\" style=\"padding-top:20px\"> <button style=\"float:right; margin:-5px 0 15px 1px; width:35%\" class=\"wbtn btn ng-binding ladda-button\" ng-click=\"btnKembali()\">Kembali</button> <ul class=\"list-inline\"> <li style=\"font-size:16px; font-family: LatoLatinWeb\"><b>{{detailTypeColor[0].VehicleTypeDescription}}&nbsp-&nbsp{{detailTypeColor[0].SuffixCode}}&nbsp-&nbsp{{detailTypeColor[0].ColorName}}</b></li> </ul> <div class=\"listData\" style=\"padding:0px\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride2\" ng-click=\"ShowListGroup3 = !ShowListGroup3\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{{detailTypeColor[0].StatusStockName}} <div style=\"float:right; width: auto\"> <span style=\"font-size:15px\">Total : {{detailTypeColor.length}}</span> <span ng-if=\"ShowListGroup3==false\" style=\"float:right; margin-top:5px; padding-left:10px\" class=\"fa fa-plus-square\"></span> <span ng-if=\"ShowListGroup3==true\" style=\"float:right; margin-top:5px; padding-left:10px\" class=\"fa fa-minus-square\"></span> </div> </button> <div class=\"list-group\" id=\"data2\" aria-labelledby=\"dropdownMenu3\" ng-repeat=\"Detail in detailTypeColor\" ng-show=\"ShowListGroup3\" style=\"margin-bottom:0px\"> <div class=\"list-group-item\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div style=\"font-size: 13px\"> <ul class=\"list-inline\" style=\"font-size:13px\"> <li style=\"width:auto\"><b>Kategori</b> : {{Detail.StatusUnitName}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:13px\"> <li><b>RRN</b> : {{Detail.RRNNo}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:13px\"> <li><b>No. Rangka</b> : {{Detail.FrameNo}}</li> </ul> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/summaryStock/summaryStock.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">/* @media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "} */\r" +
    "\n" +
    "\r" +
    "\n" +
    ".Search{\r" +
    "\n" +
    "    position: absolute!important;\r" +
    "\n" +
    "    border: 1px solid #e2e2e2;\r" +
    "\n" +
    "    border-color: #e2e2e2 #e2e2e2 #fff!important;\r" +
    "\n" +
    "    border-radius: 3px;\r" +
    "\n" +
    "    height: 37px;\r" +
    "\n" +
    "    z-index: 1;\r" +
    "\n" +
    "    padding-top: 0!important;\r" +
    "\n" +
    "    left: -40px;\r" +
    "\n" +
    "    padding-right: 6px!important;\r" +
    "\n" +
    "    background-color: rgba(213,51,55,.02);\r" +
    "\n" +
    "}</style> <div ng-show=\"SummaryStock\"> <div align=\"right\"> <button class=\"Search\" style=\"margin-left: 96%; margin-top: -35px\"> <i class=\"fa fa-fw fa-search\" style=\"font-size:1.4em\"></i> </button> <button class=\"ubtn\" ng-click=\"actRefresh()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" tabindex=\"0\" style=\"float:right; margin-top:-35px\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> </div> <div class=\"row advsearch\" style=\"padding:10px; margin:0px 0 0 0; border:1px solid #e2e2e2;background-color:rgba(213,51,55,0.02)\"> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Cabang</label> <bsselect ng-disabled=\"SummaryStockDisableFilterCabang\" id=\"comboBox\" ng-model=\"filter.OutletId\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"OutletId\" placeholder=\"Cabang\" icon=\"fa fa-sitemap\"> </bsselect> </div> <div class=\"form-group\"> <label>Model</label> <bsselect id=\"comboBox\" ng-model=\"filter.VehicleModelId\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" on-select=\"filterModel(selected)\" placeholder=\"Model\" icon=\"fa fa-car glyph-left\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Provinsi</label> <bsselect id=\"comboBox\" ng-model=\"filter.ProvinceId\" data=\"getProvince\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" ng-change=\"filterKabupaten(filter.ProvinceId)\" placeholder=\"Provinsi\" icon=\"fa fa-globe\"> </bsselect> </div> <div class=\"form-group\"> <label>Tipe</label> <bsselect id=\"comboBox\" ng-model=\"filter.VehicleTypeId\" data=\"getTipe\" item-text=\"Description,KatashikiCode,SuffixCode\" item-value=\"VehicleTypeId\" ng-change=\"pilihType(filter.VehicleTypeId)\" on-select=\"VehicleTypeId\" placeholder=\"Tipe\" icon=\"fa fa-car glyph-left\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Kota</label> <bsselect id=\"comboBox\" ng-model=\"filter.CityRegencyId\" data=\"getCity\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" on-select=\"CityRegencyId\" placeholder=\"Kota\" icon=\"fa fa-globe\"> </bsselect> </div> <div class=\"form-group\"> <label>Warna</label> <bsselect id=\"comboBox\" ng-model=\"filter.ColorId\" data=\"listWarna\" item-text=\"ColorCodeName\" item-value=\"ColorId\" on-select=\"ColorId\" placeholder=\"Warna\" icon=\"fa fa-car glyph-left\"> </bsselect> </div> </div> </div> <div class=\"\" style=\"position:absolute;right:20px;margin-top:-30px\"> <div class=\"pull-right\"> <div class=\"btn-group\" style=\"\"> <button class=\"btn wbtn\" style=\"padding:4px\" ng-click=\"getData()()\" onclick=\"this.blur()\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Search\" tabindex=\"0\"><i class=\"large icons\"><i class=\"database icon\"></i><i class=\"inverted corner filter icon\"></i></i>Search</button> </div> </div> </div> </div> <div id=\"filterData\" class=\"row\" style=\"margin:0; margin-top:10px\"> <div style=\"float:left\" class=\"input-group-btn dropdown\" uib-dropdown=\"\"> <button style=\"margin-right:5px\" ng-disabled=\"true\" type=\"button\" class=\"btn wbtn dropdown-toggle\" onclick=\"this.blur()\" uib-dropdown-toggle=\"\" data-toggle=\"dropdown\" tabindex=\"0\" aria-haspopup=\"true\" aria-expanded=\"false\" disabled>Bulk Action&nbsp;<span class=\"caret\"></span> </button> </div> <div id=\"inputFilter\" style=\"margin-left:95px;float:left; width:20%\"> <div class=\"form-group filterMarg\"> <input type=\"text\" ng-change=\"filterSearch()\" required ng-model=\"textFilter\" class=\"form-control\" name=\"ModelKendaraan\" placeholder=\"Search\" maxlength=\"100\"> </div> </div> <div id=\"comboBoxFilter\" style=\"float:left; width:15%; font-size:12px\"> <div class=\"form-group filterMarg\"> <select required ng-model=\"selectedFilter\" ng-change=\"filterSearch()\" class=\"form-control\" ng-options=\"item.value as item.name for item in filterData.defaultFilter\" placeholder=\"Filter\"> <option label=\"Filter\" value=\"\" disabled>Filter</option> </select> </div> </div> </div> <div> <div ui-grid=\"gridSummary\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-pinning ui-grid-auto-resize></div> </div> </div> <div class=\"modal fade\" id=\"detailKendaraan\" role=\"dialog\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\" style=\"background-color:whitesmoke\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> </div> <div class=\"modal-body\" style=\"padding-bottom:20px\"> </div> <div class=\"modal-footer\" style=\"padding:10px\"> <div class=\"row\"> <button id=\"btnBatalModal\" ng-click=\"btnTutupDetailModal()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; bottom: 10%; width:15%\">Tutup </button> </div> </div> </div> </div> <div class=\"ui modal show_modal_detail_summary_stock\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <p style=\"font-size: 18px; font-weight:bold\"> {{detailRow.VehicleTypeDescription}} {{detailRow.ColorName}} </p> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Katashiki </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.KatashikiCode}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Suffix </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.SuffixCode}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Color Code / Name </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.ColorCode}}-{{detailRow.ColorName}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> No. Rangka /RRN </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.FrameNo}}-{{detailRow.RRNNo}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Chasis No </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li style=\"width:100px\">{{detailRow.ChasisNo}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Engine No </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li style=\"width:100px\">{{detailRow.EngineNo}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> PLOD </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.PLOD | date:'dd-MM-yyyy'}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> TAM Delivery Order Date </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.DOTAMDate | date:'dd-MM-yyyy'}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Matching Date </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.MatchingDate | date:'dd-MM-yyyy'}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Location </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.LastLocation}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Kategori </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.StatusUnitName}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> No Kunci </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.KeyNumber}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Posisi Unit </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.DeliveryNoteLocation}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Alamat </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.DeliveryNoteAddress}}</li> </ul> </p> </div> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <button ng-click=\"Tutup_show_modal_detail_summary_stock()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right\">Keluar</button> </div> </div> </div> </div> <bsui-modal show=\"show_modal.show\" title=\"Stock Detail\" data=\"modal_model\" on-save=\"onListSave\" on-cancel=\"onListCancel\" mode=\"modalMode\" width=\"50%\"> <div ng-form name=\"contactForm\"> <div class=\"row\"> <div class=\"col-md-6\"> <p style=\"font-size: 14px; font-weight:bold\"> {{detailRow.VehicleTypeDescription}} {{detailRow.ColorName}} </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Katashiki </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.KatashikiCode}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Suffix </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.SuffixCode}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Color Code / Name </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.ColorCode}}-{{detailRow.ColorName}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> No. Rangka /RRN </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.FrameNo}}-{{detailRow.RRNNo}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Chasis No </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li style=\"width:100px\">{{detailRow.ChasisNo}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Engine No </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li style=\"width:100px\">{{detailRow.EngineNo}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> PLOD </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.PLOD | date:'dd-MM-yyyy'}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> TAM Delivery Order Date </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.DOTAMDate | date:'dd-MM-yyyy'}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Matching Date </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.MatchingDate | date:'dd-MM-yyyy'}}</li> </ul> </p> </div> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Location </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.LastLocation}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Kategori </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.StatusUnitName}}</li> </ul> </p> </div> <div class=\"col-md-4\"> <p style=\"font-size: 14px; font-weight:bold\"> Key Number </p> <p style=\"font-size: 14px\"> <ul class=\"list-inline\" style=\"font-size: 14px\"> <li>{{detailRow.KeyNumber}}</li> </ul> </p> </div> </div> </div> </bsui-modal>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/swappingSugestion/swappingSugestion.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">.nopadding {\r" +
    "\n" +
    "        padding-right: 0 !important;\r" +
    "\n" +
    "        padding-top: 0 !important;\r" +
    "\n" +
    "        padding-bottom: 0 !important;\r" +
    "\n" +
    "        padding-left: 1px;\r" +
    "\n" +
    "        margin: 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .table {\r" +
    "\n" +
    "        font-family: arial, sans-serif;\r" +
    "\n" +
    "        border-collapse: collapse;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .td,\r" +
    "\n" +
    "    .th {\r" +
    "\n" +
    "        border: 1px solid #dddddd;\r" +
    "\n" +
    "        text-align: left;\r" +
    "\n" +
    "        padding: 8px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .tr:nth-child(even) {\r" +
    "\n" +
    "        background-color: #dddddd;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .gridStyle {\r" +
    "\n" +
    "        border: 1px solid rgb(212, 212, 212);\r" +
    "\n" +
    "        width: 400px;\r" +
    "\n" +
    "        height: 300px\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .ui-grid-viewport {\r" +
    "\n" +
    "        overflow-anchor : none;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <div ng-show=\"ManualMatching\"> <button ng-show=\"showfindUnit\" ng-disabled=\"SelectUnit.length == 0\" ng-click=\"findUnit()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 10px;width:11%\">Find Unit</button> <button ng-click=\"btnMatch()\" ng-disabled=\"SelectUnit.length<=0||disabledMatch||disabledbtnproces\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right;margin-top:-31px\">Match</button> <button ng-click=\"btnSuggest()\" ng-disabled=\"SelectUnit.length<=0||disabledsuggest||disabledbtnproces\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right;margin-top:-31px\">Suggest Match</button> <button ng-click=\"btnUnmatch()\" ng-disabled=\"SelectUnit.length<=0||disabledbtnproces\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right;margin-top:-31px\">Un-match</button> <div class=\"ui modal ModalUnmatchSwappingSuggestion\" role=\"dialog\"> <div class=\"modal-header\"> Alasan Batal Matching </div> <div class=\"modal-body\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>No. SPK</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"SelectUnit[0].FormSPKNo\" placeholder=\"No. SPK\" ng-disabled=\"true\"> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>No. Rangka</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"SelectUnit[0].FrameNo\" placeholder=\"No. Rangka\" ng-disabled=\"true\"> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>No. SO</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"SelectUnit[0].SOCode\" placeholder=\"No. SO\" ng-disabled=\"true\"> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Model Tipe Kendaraan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"SelectUnit[0].VehicleTypeDescription\" placeholder=\"Model Tipe Kendaraan\" ng-disabled=\"true\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Pelanggan</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"SelectUnit[0].CustomerName\" placeholder=\"Nama Pelanggan\" ng-disabled=\"true\"> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Salesman</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"SelectUnit[0].SalesName\" placeholder=\"Salesman\" ng-disabled=\"true\"> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Nama Supervisor</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"SelectUnit[0].SupervisorName\" placeholder=\"Supervisor\" ng-disabled=\"true\"> </div> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group\"> <bsreqlabel>Alasan</bsreqlabel> <textarea type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Alasan\" ng-model=\"SwappingSuggestionUnmatchReason\" rows=\"6\" cols=\"60\" required>\r" +
    "\n" +
    "\t\t\t\t\t</textarea></div> </div> </div> </div> <div class=\"modal-footer\"> <button ng-disabled=\"SwappingSuggestionUnmatchReason==null || SwappingSuggestionUnmatchReason==undefined ||SwappingSuggestionUnmatchReason==''\" ng-click=\"SwappingSuggestionUnmatchSimpan()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Simpan</button> <button ng-click=\"SwappingSuggestionUnmatchBatal()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div> <bsform-grid ng-form=\"SwappingSugestionForm\" factory-name=\"SwappingSugestionFactory\" model=\"mSwappingSugestion\" model-id=\"Id\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" show-advsearch=\"on\" hide-new-button=\"true\" form-name=\"SwappingSugestionForm\" form-title=\"Maintain Matching\" modal-title=\"MaintainSO\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <bsreqlabel>Tanggal SPK</bsreqlabel> <bsdatepicker name=\"tanggal\" date-options=\"dateOptionsStart\" ng-change=\"tanggalmin(dt)\" ng-model=\"filter.SPKDateStart\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Sampai Tanggal</bsreqlabel> <bsdatepicker name=\"tanggal\" date-options=\"dateOptionsEnd\" ng-model=\"filter.SPKDateEnd\" icon=\"fa fa-calendar\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>No. SPK</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" ng-model=\"filter.SPKNo\" name=\"SPK\" placeholder=\"No. SPK\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Status Matching</label> <bsselect name=\"Status\" ng-model=\"filter.StatusMatchingId\" data=\"statusMatching\" item-text=\"StatusMatchName\" item-value=\"StatusMatchId\" placeholder=\"Select\" icon=\"fa fa-search\" required> </bsselect> </div> </div> </div> <div class=\"row\" style=\"margin-bottom: 20px\"> </div> </bsform-advsearch> </bsform-grid> <!-- <bsui-modal show=\"show_modal.show\" title=\"No Rangka/RRN No\" data=\"modal_model\" on-save=\"onListSave\" on-cancel=\"onListCancel\" mode=\"modalMode\" width=\"50%\"> --> <div class=\"ui modal NoRangka\"> <div> <div class=\"modal-header\" style=\"padding: 1.25rem 1.5rem ;background-color: whitesmoke; border-bottom: 1px solid rgba(34,36,38,.15)\"> <h6> <b>No. Rangka / No. RRN</b> </h6> </div> <div class=\"modal-body\"> <div class=\"row\" style=\"padding-bottom: 13px\"> <div class=\"col-md-12 nopadding\"> <div class=\"filtersearch col-md-9 nopadding\" style=\"padding-bottom:5px;padding-top: 10px\"> <div style=\"padding-left: 12px\"> <div id=\"comboBoxFilter\" style=\"float:left; width:10%\"> <div style=\"margin-top:15px\" class=\"form-group filterMarg\"> <b>No. Rangka</b> </div> </div> <div id=\"comboBoxFilter\" style=\"float:left; width:45%\"> <div class=\"form-group filterMarg\"> <input class=\"form-control\" style=\"width:100%\" type=\"text\" ng-model=\"SwappingSuggestionRangkaRrnSearchValue\"> </div> </div> <div id=\"btnFilter\" style=\"float:left; width:10%\"> <button ng-click=\"SwappingSuggestionRangkaRrnSearchThingy()\" style=\"margin-let: 10px\" id=\"cari\" class=\"rbtn btn ng-binding ladda-button\" style=\"width: 60%\"> <i class=\"fa fa-search\"></i> </button> </div> </div> </div> </div> </div> <div style=\"display:block\" ui-grid=\"gridMatchingVehicle\" ui-grid-edit ui-grid-selection ui-grid-pinning ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> <div class=\"modal-footer\" style=\"padding:1rem 1rem !important; background-color: #f9fafb; border-top: 1px solid rgba(34,36,38,.15)\"> <div class=\"row\" style=\"margin:  0 0 0 0\"> <button ng-click=\"onListSave()\" ng-disabled=\"selectRowRRN.length < 1\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Simpan</button> <button ng-click=\"onListCancel()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right\">Kembali</button> </div> </div> </div> </div> <!-- </bsui-modal> --> <div class=\"ui small modal matching\" role=\"dialog\" style=\"width:25%; margin-left: -157px\"> <i class=\"close icon\"></i> <div class=\"header\">Matching</div> <div class=\"content\" style=\"text-align: center\"> <p style=\"font-size:initial; margin:0px\">Tidak ada SO No yang Status</p> <p style=\"font-size:initial; margin:0px\">matching : \"Outstanding\".</p> </div> <div class=\"actions\"> <div class=\"wbtn btn ng-binding ng-scope\" style=\"width:100%\" ng-click=\"TutupModalMatching()\">Kembali</div> </div> </div> </div> <div ng-show=\"SwappingSugestion\"> <button ng-disabled=\"selectedSwappingSugestion.length == 0\" ng-click=\"btnRequestSwapping();\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px;width:13%\">Request Swapping</button> <button ng-click=\"KembaliMaintain();\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; margin:-30px 0 0 1px;width:13%\">Kembali</button> <fieldset ng-disabled=\"true\" style=\"padding-top:12px; padding-bottom:15px\"> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "           <div class=\"form-group\">\r" +
    "\n" +
    "                <bsreqlabel>Tanggal SPK</bsreqlabel>\r" +
    "\n" +
    "                <bsdatepicker name=\"tanggal\" date-options=\"dateOptions\" icon=\"fa fa-calendar\" ng-model=\"SelectUnit[0].SpkDate\">\r" +
    "\n" +
    "                </bsdatepicker>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Status Matching</bsreqlabel> <bsselect name=\"Status\" ng-model=\"SelectUnit[0].StatusMatchId\" data=\"statusMatching\" item-text=\"StatusMatchName\" item-value=\"StatusMatchId\" on-select=\"StatusMatchId\" placeholder=\"Select\"> </bsselect> </div> </div> </fieldset> <div ui-grid=\"gridSwappingSugestion\" ui-grid-selection ui-grid-pinning ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-cloak> </div> <div class=\"ui modal GroupDealer\" role=\"dialog\"> <i class=\"close icon\"></i> <div class=\"header\">Group Dealer</div> <div class=\"content\"> <div ui-grid=\"gridFindGroupDealer\" ui-grid-selection ui-grid-pinning ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> <div class=\"actions\"> <div class=\"wbtn btn ng-binding ng-scope\" style=\"width:11%\" ng-click=\"btnTutupGroupDealer()\">Kembali</div> <div class=\"rbtn btn ng-binding ng-scope\" style=\"width:11%\" ng-click=\"btnPilihGroupDealer()\">Pilih</div> </div> </div> <div class=\"ui small modal OtherDealer\" role=\"dialog\"> <i class=\"close icon\"></i> <div class=\"header\">Other Dealer</div> <div class=\"content\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Area</bsreqlabel> <bsselect id=\"comboBox\" name=\"filterModel\" ng-model=\"OtherDealer.AreaId\" data=\"DataArea\" ng-change=\"getProvince(OtherDealer.AreaId)\" item-text=\"AreaName\" item-value=\"AreaId\" on-select=\"\" placeholder=\"Area\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect id=\"comboBox\" name=\"filterModel\" ng-disabled=\"OtherDealer.AreaId == null\" ng-model=\"OtherDealer.ProvinceId\" data=\"getDataProvince\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" on-select=\"\" placeholder=\"Provinsi\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <button id=\"cari\" ng-disabled=\"OtherDealer.ProvinceId==null || OtherDealer.ProvinceId==undefined\" ng-click=\"SearchOtherDealer()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; width: 100%; margin-top: 22px\">Cari</button> </div> </div> <div ui-grid=\"gridFindOtherDealer\" ui-grid-selection ui-grid-pinning ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> <div class=\"actions\"> <div class=\"wbtn btn ng-binding ng-scope\" style=\"width:11%\" ng-click=\"btnTutupGroupDealer()\">Kembali</div> <div class=\"rbtn btn ng-binding ng-scope\" style=\"width:11%\" ng-click=\"btnPilihOtherDealer()\">Pilih</div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales5/demandSupply/targetAssignment/targetAssignment.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">.ui-grid-viewport {\r" +
    "\n" +
    "        overflow-anchor: none;\r" +
    "\n" +
    "    }</style> <div> </div> <div ng-if=\"KacabView\"> <!-- <button ng-click=\"tst()\">test</button> --> <div class=\"row\" style=\"margin: -31px 0 15px 0\"> <button ng-if=\"isEdit\" style=\"float:right\" ng-click=\"btnKacabSimpan()\" class=\"rbtn btn\">Simpan</button> <button ng-if=\"!isEdit\" ng-disabled=\"(Role.moon == null || Role.moon == undefined) || isNotValid == false\" style=\"float:right\" ng-click=\"edit()\" class=\"rbtn btn\"><i class=\"fa fa-pencil\"></i>&nbsp&nbsp&nbspUbah</button> <button ng-if=\"!isEdit\" class=\"ubtn mobileFilterBtn\" ng-click=\"TargetAssignmentRefresh()\" onclick=\"this.blur()\" style=\"float:right\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" tabindex=\"0\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> <button ng-if=\"isEdit\" style=\"float:right\" ng-click=\"batal()\" class=\"wbtn btn\">Batal</button> </div> <fieldset ng-disabled=\"disabledPeriod\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <label>Cabang</label> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"Cabang\" ng-model=\"Role.OutletId\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Branch\" ng-disabled=\"true\" icon=\"fa fa-sitemap\" required></bsselect> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Periode Target</bsreqlabel> <bsselect ng-disabled=\"isEdit\" ng-model=\"Role.moon\" ng-change=\"selectPeriod(Role.moon)\" data=\"getPeriod\" item-text=\"PeriodDesc\" item-value=\"PeriodDate\" placeholder=\"Periode Target\"> </bsselect> <!-- ng-options=\"item.PeriodDate as item.PeriodDesc for item in getPeriod\" --> </div> </div> <div class=\"col-md-4\" style=\"margin-top: 30px\" ng-if=\"targetkacabview\"> <!-- <label><b>Target Adjustment : {{tagetAdjusmentKacab}}</b></label> --> <label><b>Total : {{tagetAdjusmentKacab | number:0}}</b></label> </div> </div> </fieldset> <div class=\"row\" ng-show=\"showgridKacabEdit\" style=\"padding: 12px\"> <div class=\"row\" style=\"padding: 0 0 0 0; margin: 0 0 0 0\"> <div style=\"font-size:10px\"> <p align=\"right\" style=\"color:red\"> NOTE * : Klik 2 kali pada field Target untuk mengubah data. </p> </div> </div> <div ui-grid=\"grid\" style=\"height: 470px\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-selection ui-grid-edit ui-grid-auto-resize></div> </div> <div class=\"row\" ng-show=\"showgridKacab\" style=\"padding: 12px\"> <div ui-grid=\"gridview\" style=\"height: 450px\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-selection ui-grid-auto-resize></div> </div> </div> <div ng-if=\"SupervisorView\"> <div class=\"row\" style=\"margin: -31px 0 15px 0\"> <button ng-if=\"isEdit\" style=\"float:right\" ng-click=\"btnSPVSimpan()\" class=\"rbtn btn\">Simpan</button> <button ng-if=\"!isEdit\" ng-disabled=\"(Role.Period == null || Role.Period == undefined) || isNotValid == false\" style=\"float:right\" ng-click=\"editspv()\" class=\"rbtn btn\">Ubah</button> <button ng-if=\"!isEdit\" class=\"ubtn mobileFilterBtn\" ng-click=\"TargetAssignmentRefresh()\" onclick=\"this.blur()\" style=\"float:right\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" tabindex=\"0\"> <i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em\"></i> </button> <button ng-if=\"isEdit\" style=\"float:right\" ng-click=\"batalspv()\" class=\"wbtn btn\">Batal</button> </div> <fieldset ng-disabled=\"disabledPeriod\"> <div class=\"row\"> <div class=\"col-md-4\" ng-disabled=\"true\"> <div class=\"form-group\"> <label>Cabang</label> <div class=\"input-icon-right\"> <bsselect id=\"comboBox\" name=\"Cabang\" ng-model=\"Role.OutletId\" data=\"getOutlet\" item-text=\"Name\" item-value=\"OutletId\" on-select=\"branchId\" placeholder=\"Branch\" ng-disabled=\"true\" icon=\"fa fa-sitemap\" required></bsselect> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Periode Target</bsreqlabel> <bsselect name=\"Periode\" ng-disabled=\"isEdit\" data=\"getPeriod\" item-text=\"PeriodDesc\" item-value=\"PeriodDate\" on-select=\"selectPeriod(Role.Period)\" ng-model=\"Role.Period\" placeholder=\"Periode Target\" icon=\"fa fa-calendar\" style=\"min-width: 150px\"> </bsselect> </div> </div> <div class=\"col-md-4\" style=\"margin-top: 30px\" ng-if=\"targetspvview\"> <label><b>Target SPV : {{tagetAdjusmentSPV | number:0}} &nbsp&nbsp&nbsp&nbsp</b></label> <label><b>Total : {{tagetSPV | number:0}}</b></label> </div> <!-- <div class=\"col-md-4\" ng-show=\"showModel\">\r" +
    "\n" +
    "                <div class=\"form-group\" >\r" +
    "\n" +
    "                    <bsreqlabel>Model Kendaraan</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"Model\" data=\"optionsModel\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" ng-model=\"spvDetail.VehicleModelId\" on-select=\"value(selected)\" placeholder=\"Tipe Kendaraan\" icon=\"fa fa-car glyph-left\" style=\"min-width: 150px\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> </div> </fieldset> <div class=\"row\" ng-show=\"showgridSPV\" style=\"padding: 12px\"> <div ui-grid=\"gridSPV\" ui-grid-grouping class=\"grid\" style=\"height: 470px\" ui-grid-pagination ui-grid-auto-resize></div> </div> <div class=\"row\" ng-show=\"showgridSPVDetail\" style=\"padding: 12px\"> <div class=\"row\" style=\"padding: 0 0 0 0; margin: 0 0 0 0\"> <div style=\"font-size:10px\"> <p align=\"right\" style=\"color: red\"> NOTE * : Klik 2 kali pada field Traget untuk mengubah data. </p> </div> </div> <div ui-grid=\"gridSPVedit\" style=\"height: 450px\" ui-grid-grouping class=\"grid\" ui-grid-pagination ui-grid-edit ui-grid-selection ui-grid-auto-resize></div> </div> </div>"
  );

}]);
