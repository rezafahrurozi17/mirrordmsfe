angular.module('templates_app_gate_towing', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/services/reception/towing/towing.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <!--  on-before-edit-mode=\"onBeforeEdit\" --> <bsform-grid ng-form=\"TowingForm\" factory-name=\"Towing\" model=\"mData\" model-id=\"TowingId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-before-new-mode=\"onBeforeNewMode\" on-validate-save=\"onValidateSave\" on-show-detail=\"onShowDetail\" do-custom-save=\"doCustomSave\" form-name=\"TowingForm\" form-title=\"Towing\" modal-title=\"Towing\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <div class=\"row\" ng-form=\"row1\"> <div class=\"col-md-12\"> <div class=\"col-md-12\"> <bsreqlabel>No. Polisi</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" style=\"text-transform: uppercase\" name=\"PoliceNumber\" placeholder=\"No Polisi\" ng-model=\"mData.PoliceNumber\" maxlength=\"15\" required> </div> <div class=\"col-md-12\"> <!-- <bserrmsg field=\"PoliceNumber\"></bserrmsg> --> <em ng-messages=\"row1.PoliceNumber.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"col-md-12\"> <bsreqlabel>Model</bsreqlabel> </div> <div class=\"col-md-4\"> <!-- on-select=\"selectVehicleModel()\" --> <bsselect name=\"Model\" ng-model=\"mData.VehicleModelId\" data=\"VMD\" item-text=\"VehicleModelName\" item-value=\"VehicleModelId\" placeholder=\"Pilih Model\" required> </bsselect> </div> <div class=\"col-md-12\"> <!-- <bserrmsg field=\"Model\"></bserrmsg> --> <em ng-messages=\"row1.Model.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"col-md-12\"> <bsreqlabel>Warna</bsreqlabel> </div> <div class=\"col-md-4\"> <!-- on-select=\"selectVehicleModel()\" --> <!-- <bsselect name=\"Color\"\r" +
    "\n" +
    "                  ng-model=\"mData.Color\"\r" +
    "\n" +
    "                  data=\"CLR\"\r" +
    "\n" +
    "                  item-text=\"ColorName\"\r" +
    "\n" +
    "                  item-value=\"ColorName\"\r" +
    "\n" +
    "                  placeholder=\"Pilih Warna\"\r" +
    "\n" +
    "                  required=\"true\">\r" +
    "\n" +
    "          </bsselect> --> <input type=\"text\" class=\"form-control\" name=\"Color\" placeholder=\"Warna\" ng-model=\"mData.Color\" maxlength=\"50\" required> </div> <div class=\"col-md-12\"> <!-- <bserrmsg field=\"Color\"></bserrmsg> --> <em ng-messages=\"row1.Color.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"col-md-12\"> <bsreqlabel>Tgl. Masuk</bsreqlabel> </div> <div class=\"col-md-4\"> <!-- on-select=\"selectVehicleModel()\" --> <bsdatepicker name=\"EntryDate\" max-date=\"maxDateOption.maxDate\" date-options=\"DateOptions\" ng-model=\"mData.EntryDate\" required> </bsdatepicker> </div> <div class=\"col-md-12\"> <!-- <bserrmsg field=\"EntryDate\"></bserrmsg> --> <em ng-messages=\"row1.EntryDate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"col-md-12\"> <bsreqlabel>Jam Masuk</bsreqlabel> </div> <div class=\"col-md-4\"> <!-- on-select=\"selectVehicleModel()\"  ng-change=\"fThrough()\" --> <div uib-timepicker show-spinner=\"true\" max=\"maxFrom\" name=\"EntryTime\" ng-model=\"mData.EntryTime\" ng-change=\"fFrom()\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" required></div> </div> <div class=\"col-md-12\"> <!-- <bserrmsg field=\"EntryTime\"></bserrmsg> --> <em ng-messages=\"row1.EntryTime.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"col-md-12\"> <bsreqlabel>Nama Pelanggan</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" name=\"OwnerName\" placeholder=\"Nama Pelanggan\" ng-model=\"mData.OwnerName\" maxlength=\"50\" required> </div> <div class=\"col-md-12\"> <!-- <bserrmsg field=\"OwnerName\"></bserrmsg> --> <em ng-messages=\"row1.OwnerName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"col-md-12\"> <bsreqlabel>No. Telepon</bsreqlabel> </div> <div class=\"col-md-4\"> <input type=\"text\" class=\"form-control\" name=\"OwnerPhone\" placeholder=\"Phone\" ng-model=\"mData.OwnerPhone\" maxlength=\"15\" numbers-only required> </div> <div class=\"col-md-12\"> <!-- <bserrmsg field=\"Phone\"></bserrmsg> --> <em ng-messages=\"row1.OwnerPhone.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"col-md-12\"> <bsreqlabel>Keterangan</bsreqlabel> </div> <div class=\"col-md-4\"> <textarea class=\"form-control\" name=\"OwnerDetail\" placeholder=\"Detail\" ng-model=\"mData.OwnerDetail\" style=\"height: 100px;width: 500px\" required></textarea> </div> <div class=\"col-md-12\"> <!-- <bserrmsg field=\"Detail\"></bserrmsg> --> <em ng-messages=\"row1.OwnerDetail.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"col-md-12\" ng-hide=\"true\"> Towing Id </div> <div class=\"col-md-4\"> <textarea class=\"form-control\" name=\"TowingId\" placeholder=\"TowingId\" ng-model=\"mData.TowingId\" style=\"height: 100px;width: 500px\" ng-hide=\"true\"></textarea> </div> </div> </div> <!-- IF NOT USING FORMLY --> <!--             <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <label>Parent Role</label>\r" +
    "\n" +
    "                <bsselect\r" +
    "\n" +
    "                          name=\"parentRole\"\r" +
    "\n" +
    "                          data=\"grid.data\"\r" +
    "\n" +
    "                          item-text=\"title\"\r" +
    "\n" +
    "                          item-value=\"id\"\r" +
    "\n" +
    "                          ng-model=\"mRole.pid\"\r" +
    "\n" +
    "                          placeholder=\"Select Parent Role\"\r" +
    "\n" +
    "                          icon=\"fa fa-child glyph-left\"\r" +
    "\n" +
    "                          style=\"min-width: 150px;\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "                <bserrmsg field=\"parentRole\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> </bsform-grid>"
  );

}]);
