angular.module('templates_appsales6', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/sales/sales6/dec/approvalPengirimanDenganPengecualian/approvalPengirimanDenganPengecualian.html',
    "<script type=\"text/javascript\">function IfGotProblemWithModal() {\r" +
    "\n" +
    "\t$('body .modals').remove();\r" +
    "\n" +
    "}</script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "       -moz-border-radius: 4px;\r" +
    "\n" +
    "            border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <bsform-grid ng-show=\"ApprovalPengirimanDenganPengecualianMain\" ng-form=\"ApprovalPengirimanDenganPengecualianForm\" factory-name=\"ApprovalPengirimanDenganPengecualianFactory\" model=\"mApprovalPengirimanDenganPengecualian\" model-id=\"SpkId\" loading=\"loading\" hide-new-button=\"true\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"ApprovalPengirimanDenganPengecualianForm\" form-title=\"Approval Pengiriman Dengan Pengecualian\" modal-title=\"Approval Pengiriman Dengan Pengecualian\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> </bsform-grid> <div ng-show=\"ApprovalPengirimanDenganPengecualianDetail\"> <div class=\"row\"> <button ng-show=\"UserIsKacab\" ng-click=\"SetujuApprovalPengirimanDenganPengecualian()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Approve </span><span class=\"ladda-spinner\"></span> </button> <button ng-show=\"UserIsKacab\" ng-click=\"TidakSetujuApprovalPengirimanDenganPengecualian()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\">Reject </span><span class=\"ladda-spinner\"></span> </button> <button style=\"float:right\" ng-click=\"BatalApprovalPengirimanDenganPengecualian()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"list-group\" id=\"formGroupPanelContent\" aria-labelledby=\"dropdownMenu2\" style=\"\"> <p> <ul class=\"list-inline\"> <li><b>No.SPK</b></li> </ul> <ul class=\"list-inline\"> <li>{{mApprovalPengirimanDenganPengecualian.FormSPKNo}}</li> </ul> <ul class=\"list-inline\"> <li><b>No.Rangka</b></li> </ul> <ul class=\"list-inline\"> <li>{{mApprovalPengirimanDenganPengecualian.FrameNo}}</li> </ul> <ul class=\"list-inline\"> <li><b>Nama Pelanggan</b></li> </ul> <ul class=\"list-inline\"> <li>{{mApprovalPengirimanDenganPengecualian.CustomerName}}</li> </ul> </p> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Nama Penerima</bsreqlabel> <input ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NamaPenerima_EnableDisable\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"Nama Penerima\" ng-model=\"mApprovalPengirimanDenganPengecualian.ReceiveName\" ng-maxlength=\"50\" style=\"margin-bottom: 10px\" required> </div> </div> </div> <p> <ul class=\"list-inline\"> <li><b>Model</b></li> </ul> <ul class=\"list-inline\"> <li>{{mApprovalPengirimanDenganPengecualian.VehicleModelName}}</li> </ul> <ul class=\"list-inline\"> <li><b>Tipe</b></li> </ul> <ul class=\"list-inline\"> <li>{{mApprovalPengirimanDenganPengecualian.Description}}</li> </ul> <ul class=\"list-inline\"> <li><b>Warna</b></li> </ul> <ul class=\"list-inline\"> <li>{{mApprovalPengirimanDenganPengecualian.ColorName}}</li> </ul> </p> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Pengiriman</bsreqlabel> <bsdatepicker name=\"tanggal\" ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable\" date-options=\"dateOptions\" ng-model=\"mApprovalPengirimanDenganPengecualian.SentUnitDate\"> </bsdatepicker> </div> </div> </div> <bsreqlabel>ETA Customer</bsreqlabel> <div class=\"row\" style=\"margin-bottom: 10px\"> <div class=\"col-xs-4\" align=\"center\"> <div class=\"form-group\"> <input ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_WaktuPengiriman_EnableDisable\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"No Rangka\" ng-model=\"mApprovalPengirimanDenganPengecualian.ETACustomer\" ng-maxlength=\"50\" required> </div> </div> <div class=\"col-xs-1\" align=\"center\"> <div class=\"form-group\"> <label>-</label> </div> </div> <div class=\"col-xs-4\" align=\"center\"> <div class=\"form-group\"> <input ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_WaktuPengiriman_EnableDisable\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"No Rangka\" ng-model=\"mApprovalPengirimanDenganPengecualian.ETACustomerBuntut\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bslabel>Alamat Pengiriman</bslabel> <textarea ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_AlamatPengiriman_EnableDisable\" type=\"text\" class=\"form-control\" name=\"AlamatPengiriman\" placeholder=\"Alamat Pengiriman\" ng-model=\"mApprovalPengirimanDenganPengecualian.SentAddress\" ng-maxlength=\"50\" style=\"margin-bottom: 10px\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <p> <ul class=\"list-inline\"> <li><b>Direct Delivery Dari PDC?</b></li> </ul> <ul class=\"list-inline\"> <li> <bscheckbox ng-model=\"mApprovalPengirimanDenganPengecualian.DirectDeliveryPDC\" label=\"Ya\" true-value=\"true\" false-value=\"false\" ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable\"> </bscheckbox> </li> </ul> <ul class=\"list-inline\"> <li><b>Driver</b></li> </ul> <ul class=\"list-inline\"> <li>{{mApprovalPengirimanDenganPengecualian.DriverPDSName}}</li> </ul> <ul class=\"list-inline\"> <li id=\"KeteranganPengecualian\"> <p>Keterangan Pengecualian</p> <p style=\"margin-left:3px\"> <div> <input type=\"checkbox\" ng-checked=\"mApprovalPengirimanDenganPengecualian.DECDetailDeliveryExcReasonView[0].DetailDeliveryExcReasonId == 1\" ng-model=\"mApprovalPengirimanDenganPengecualian.DECDetailDeliveryExcReasonView[0].DetailDeliveryExcReasonId\" ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable\"> <label>Pengiriman Tanpa Aksesoris Lengkap</label> </div> <div> <input type=\"checkbox\" ng-checked=\"mApprovalPengirimanDenganPengecualian.DECDetailDeliveryExcReasonView[1].DetailDeliveryExcReasonId == 2\" ng-model=\"mApprovalPengirimanDenganPengecualian.DECDetailDeliveryExcReasonView[1].DetailDeliveryExcReasonId\" ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable\"> <label>Pengiriman tanpa STNK tanpa dan No Polisi</label> </div> <div> <input type=\"checkbox\" ng-checked=\"mApprovalPengirimanDenganPengecualian.DECDetailDeliveryExcReasonView[2].DetailDeliveryExcReasonId == 3\" ng-model=\"mApprovalPengirimanDenganPengecualian.DECDetailDeliveryExcReasonView[2].DetailDeliveryExcReasonId\" ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable\"> <label>Pengiriman Tanpa Washing Drying</label> </div> </p> </li> </ul> </p> </div> </div> <div class=\"ui modal ModalAlasanPenolakanApprovalPengirimanDenganPengecualian\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"BatalAlasanApprovalPengirimanDenganPengecualian()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Alasan Penolakan</h4> </div> <br> <div class=\"modal-body\"> <textarea rows=\"10\" cols=\"80\" maxlength=\"512\" placeholder=\"input alasan...\" ng-model=\"mApprovalPengirimanDenganPengecualian.RejectReason\"></textarea> </div> <div class=\"modal-footer\"> <!--<button onclick=\"Lanjut_Clicked()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right;\">Lanjutkan</button>--> <button ng-click=\"SubmitAlasanApprovalPengirimanDenganPengecualian()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-disabled=\"mApprovalPengirimanDenganPengecualian.RejectReason==undefined\">Simpan</button> <button ng-click=\"BatalAlasanApprovalPengirimanDenganPengecualian()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div> <div class=\"ui modal ModalActionApprovalPengirimanDenganPengecualian\" role=\"dialog\"> <div class=\"modal-header\"> <button ng-click=\"BatalActionApprovalPengirimanDenganPengecualian()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Apa Yang Ingin Anda Lakukan?</h4> </div> <br> <div class=\"modal-body\"> <a ng-click=\"SelectApprovalPengirimanDenganPengecualian()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Lihat Detail</li> </a> <a ng-click=\"SetujuApprovalPengirimanDenganPengecualian()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Approve</li> </a> <a ng-click=\"TidakSetujuApprovalPengirimanDenganPengecualian()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span>Reject</li> </a> </div> <div class=\"modal-footer\"> <button ng-click=\"BatalActionApprovalPengirimanDenganPengecualian()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">Batal</button> </div> </div>"
  );


  $templateCache.put('app/sales/sales6/dec/cetakBstkb/cetakBstkb.html',
    "<script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "    .nav.nav-tabs {\r" +
    "\n" +
    "      display:none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <div ng-show=\"ListCetakBSTKBShow\"> <bsform-grid ng-form=\"CetakBstkbForm\" factory-name=\"CetakBstkbFactory\" model=\"mcetakBstkb\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"selectedRows\" grid-hide-action-column=\"true\" on-select-rows=\"onSelectRows\" hide-new-button=\"true\" form-name=\"CetakBstkbForm\" form-title=\"Dokumen\" modal-title=\"CetakBstkb\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> </bsform-grid> <div class=\"ui modal CetakBSTKB\" role=\"dialog\"> <i class=\"close icon\"></i> <div class=\"header\">Cetak BSTKB</div> <div class=\"content\"> <div class=\"row\"> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Nama Pelanggan</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.CustomerName}}</li> </ul> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. SPK</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.FormSPKNo}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. Rangka</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.FrameNo}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. Kunci</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.KeyNumber}}</li> </ul> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Model</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.VehicleModelName}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Tipe</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.Description}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Warna</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.ColorName}}</li> </ul> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Driver</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.DriverPDSName}}</li> </ul> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Tanggal Pengiriman</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.SentUnitDate | date:'dd-MM-yyyy'}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Waktu Pengiriman</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.SentUnitTime| date:'h:mm'}}</li> </ul> </div> <div class=\"col-md-6\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Lokasi Pengiriman</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.SentAddress}}</li> </ul> </div> </div> <hr> <div class=\"row\"> <div class=\"col-md-4\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Metode Pembayaran</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{detail.PaymentTypeName}}</li> </ul> </div> <div class=\"col-md-4\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Status Full Payment</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li><!--<input type=\"checkbox\" ng-disabled=\"true\" ng-checked=\"detail.StatusFullPayment\">--> {{detail.StatusFullPayment}}</li> </ul> </div> </div> </div> <div class=\"actions\"> <div class=\"wbtn btn ng-binding ng-scope\" style=\"width:15%\" ng-click=\"btnCancelBSTKB()\">Kembali</div> <!-- <div ng-disabled=\"detail.StatusFullPayment!='Sudah Dibayar' && detail.StatusApproval != '1'\" class=\"rbtn btn ng-binding ng-scope\" style=\"width:15%\" ng-click=\"btnCetakBSTKB()\">Cetak BSTKB</div> --> <div class=\"rbtn btn ng-binding ng-scope\" style=\"width:15%\" ng-click=\"btnCetakBSTKB()\">Cetak BSTKB</div> </div> </div> <div class=\"ui modal CetakThankYouLetter\" role=\"dialog\"> <i class=\"close icon\"></i> <div class=\"header\">Cetak Thank You Letter</div> <div class=\"content\"> <div class=\"row\"> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Nama Pelanggan</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{thankyou.CustomerName}}</li> </ul> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. SPK</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{thankyou.FormSPKNo}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. Rangka</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{thankyou.FrameNo}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. Kunci</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{thankyou.KeyNumber}}</li> </ul> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Model</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{thankyou.VehicleModelName}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Tipe</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{thankyou.Description}}</li> </ul> </div> <div class=\"col-md-3\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Warna</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{thankyou.ColorName}}</li> </ul> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-3\">    \r" +
    "\n" +
    "                    <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li><b>Driver</b></li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "\t\t\t\t\t<ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li>{{thankyou.DriverPDSName}}</li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-3\">    \r" +
    "\n" +
    "                    <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li><b>Tanggal Pengiriman</b></li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "\t\t\t\t\t<ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li>{{thankyou.SentUnitDate | date:'dd-MM-yyyy'}}</li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-3\">    \r" +
    "\n" +
    "                    <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li><b>Waktu Pengiriman</b></li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "\t\t\t\t\t<ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li>{{thankyou.SentUnitTime| date:'h:mm'}}</li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-6\">    \r" +
    "\n" +
    "                    <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li><b>Lokasi Pengiriman</b></li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "\t\t\t\t\t<ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li>{{thankyou.SentAddress}}</li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <hr></hr>\r" +
    "\n" +
    "\t\t\t<div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-4\">    \r" +
    "\n" +
    "                    <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li><b>Metode Pembayaran</b></li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "\t\t\t\t\t<ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li>{{thankyou.PaymentTypeName}}</li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-md-4\">    \r" +
    "\n" +
    "                    <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li><b>Status Full Payment</b></li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "\t\t\t\t\t<ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<li>{{thankyou.StatusFullPayment}}</li>\r" +
    "\n" +
    "\t\t\t\t\t</ul>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\t --> </div> <div class=\"actions\"> <div class=\"wbtn btn ng-binding ng-scope\" style=\"width:15%\" ng-click=\"btnCancelThankYou()\">Kembali</div> <div class=\"rbtn btn ng-binding ng-scope\" style=\"width:20%\" ng-click=\"btnCetakThankYou()\">Cetak Thank You Letter</div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales6/dec/deliveryUnit/deliveryUnit.html',
    "<script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/signature.js\"></script> <script type=\"text/javascript\" src=\"js/signature_pad.js\"></script> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\"></script> <style type=\"text/css\">/*.glyphicon-star, .glyphicon-star-empty {\r" +
    "\n" +
    "    color:red;\r" +
    "\n" +
    "}*/\r" +
    "\n" +
    "\r" +
    "\n" +
    ".switch {\r" +
    "\n" +
    "  position: relative;\r" +
    "\n" +
    "  display: inline-block;\r" +
    "\n" +
    "  width: 60px;\r" +
    "\n" +
    "  height: 34px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".switch input {display:none;}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  cursor: pointer;\r" +
    "\n" +
    "  top: 0;\r" +
    "\n" +
    "  left: 0;\r" +
    "\n" +
    "  right: 0;\r" +
    "\n" +
    "  bottom: 0;\r" +
    "\n" +
    "  background-color: #ccc;\r" +
    "\n" +
    "  -webkit-transition: .4s;\r" +
    "\n" +
    "  transition: .4s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider:before {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  content: \"\";\r" +
    "\n" +
    "  height: 26px;\r" +
    "\n" +
    "  width: 26px;\r" +
    "\n" +
    "  left: 4px;\r" +
    "\n" +
    "  bottom: 4px;\r" +
    "\n" +
    "  background-color: white;\r" +
    "\n" +
    "  -webkit-transition: .4s;\r" +
    "\n" +
    "  transition: .4s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:checked + .slider {\r" +
    "\n" +
    "  background-color: #2196F3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:focus + .slider {\r" +
    "\n" +
    "  box-shadow: 0 0 1px #2196F3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:checked + .slider:before {\r" +
    "\n" +
    "  -webkit-transform: translateX(26px);\r" +
    "\n" +
    "  -ms-transform: translateX(26px);\r" +
    "\n" +
    "  transform: translateX(26px);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* Rounded sliders */\r" +
    "\n" +
    ".slider.round {\r" +
    "\n" +
    "  border-radius: 34px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider.round:before {\r" +
    "\n" +
    "  border-radius: 50%;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "table {\r" +
    "\n" +
    "    font-family: arial, sans-serif;\r" +
    "\n" +
    "    border-collapse: collapse;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "td, th {\r" +
    "\n" +
    "   text-align: left;\r" +
    "\n" +
    "   padding: 8px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "td.signaturetable{\r" +
    "\n" +
    "    padding: 0px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "tr:nth-child(even) {\r" +
    "\n" +
    "    background-color: #dddddd;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".container .signature {\r" +
    "\n" +
    "    border: 1px solid black;\r" +
    "\n" +
    "    margin: 0 auto;\r" +
    "\n" +
    "    cursor: pointer;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".box-show-setup,\r" +
    "\n" +
    ".box-hide-setup {\r" +
    "\n" +
    "    -webkit-transition: all linear 0.3s;\r" +
    "\n" +
    "    -moz-transition: all linear 0.3s;\r" +
    "\n" +
    "    -ms-transition: all linear 0.3s;\r" +
    "\n" +
    "    -o-transition: all linear 0.3s;\r" +
    "\n" +
    "    transition: all linear 0.3s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "    -moz-border-radius: 4px;\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".adv-srch {\r" +
    "\n" +
    "    padding: 10px;\r" +
    "\n" +
    "    margin-bottom: 0px;\r" +
    "\n" +
    "    border: 1px solid #e2e2e2;\r" +
    "\n" +
    "    background-color: rgba(213, 51, 55, 0.02);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".adv-srch-on {\r" +
    "\n" +
    "    padding-top: 4px !important;\r" +
    "\n" +
    "    padding: 6px;\r" +
    "\n" +
    "    margin: 0 3px 0 0;\r" +
    "\n" +
    "    margin-bottom: -5px !important;\r" +
    "\n" +
    "    border: 1px solid #e2e2e2;\r" +
    "\n" +
    "    border-bottom-style: none !important;\r" +
    "\n" +
    "    background-color: #FEFBFB;\r" +
    "\n" +
    "    border-top-left-radius: 3px;\r" +
    "\n" +
    "    border-top-right-radius: 3px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".colorsrch {\r" +
    "\n" +
    "    color: red;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".modalpadding {\r" +
    "\n" +
    "    padding-top: 0px !important;\r" +
    "\n" +
    "    padding-right: 0px !important;\r" +
    "\n" +
    "    padding-bottom: 0px !important;\r" +
    "\n" +
    "    padding-left: 0px !important;\r" +
    "\n" +
    "    border-radius: 3px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "/*@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin-top: 10px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}*/\r" +
    "\n" +
    "\r" +
    "\n" +
    ".noppadingFormGroup {\r" +
    "\n" +
    "    margin-bottom: 0px !important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ScrollStyle {\r" +
    "\n" +
    "    max-height: 600px;\r" +
    "\n" +
    "    overflow-y: scroll;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".item-modal {\r" +
    "\n" +
    "    border: none;\r" +
    "\n" +
    "    padding-bottom: 15px;\r" +
    "\n" +
    "    border-bottom: 1px solid #ddd !important;\r" +
    "\n" +
    "    margin: 10px 30px 10px 30px !important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".mobileAddBtn {\r" +
    "\n" +
    "    padding: 0 0 0 1!important;\r" +
    "\n" +
    "    font-size: 15px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".mobileFilterBtn {\r" +
    "\n" +
    "    padding: 0 0 0 1!important;\r" +
    "\n" +
    "    font-size: 13px;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "    float: right;\r" +
    "\n" +
    "    /*background-color: silver;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".filterMarg {\r" +
    "\n" +
    "    margin-bottom: 5px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media only screen and (max-width: 430px) {\r" +
    "\n" +
    "    .iconFilter {\r" +
    "\n" +
    "        margin-left: -4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin: -28px 0px 0 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .iconFilter {\r" +
    "\n" +
    "        margin-left: -4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #desktopderectivemobile {\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #mobilederectivemobile {\r" +
    "\n" +
    "        display: block !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media (min-width: 501px) and (max-width: 600px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin: -36px 0px 0 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #desktopderectivemobile {\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #mobilederectivemobile {\r" +
    "\n" +
    "        display: block !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media (min-width: 601px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin: -33px 0px 0 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #mobilederectivemobile {\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".mobilesearchBtn {\r" +
    "\n" +
    "    text-decoration: none;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media (max-width: 350px) {\r" +
    "\n" +
    "    .bsbreadcrumb .bsbreadcrumb-path,\r" +
    "\n" +
    "    .bsbreadcrumb i {\r" +
    "\n" +
    "        display: block !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "/*Modal*/\r" +
    "\n" +
    "\r" +
    "\n" +
    ".vertical-alignment-helper {\r" +
    "\n" +
    "    display: table;\r" +
    "\n" +
    "    height: 100%;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "    pointer-events: none;\r" +
    "\n" +
    "    /* This makes sure that we can still click outside of the modal to close it */\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".vertical-align-center {\r" +
    "\n" +
    "    /* To center vertically */\r" +
    "\n" +
    "    display: table-cell;\r" +
    "\n" +
    "    vertical-align: middle;\r" +
    "\n" +
    "    pointer-events: none;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".modal-content {\r" +
    "\n" +
    "    /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */\r" +
    "\n" +
    "    width: inherit;\r" +
    "\n" +
    "    height: inherit;\r" +
    "\n" +
    "    /* To center horizontally */\r" +
    "\n" +
    "    margin: 0 auto;\r" +
    "\n" +
    "    pointer-events: all;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@-webkit-keyframes spin {\r" +
    "\n" +
    "    0% {\r" +
    "\n" +
    "        -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    100% {\r" +
    "\n" +
    "        -webkit-transform: rotate(360deg);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@keyframes spin {\r" +
    "\n" +
    "    0% {\r" +
    "\n" +
    "        transform: rotate(0deg);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    100% {\r" +
    "\n" +
    "        transform: rotate(360deg);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div ng-show=\"daftarPengirimanShow\"> <bsform-mobile factory-name=\"DeliveryUnitFactory\" bind-data=\"bind_data\" info-totaldata=\"true\" filter-data=\"filterData\" custom-modallihat=\"true\" custom-modalubah=\"true\" custom-modalhapus=\"true\" enable-addbutton=\"disable\" service-enable=\"true\" selected-data=\"selected_data\" after-refresh=\"afterGetData()\" updated-data=\"updated_data\" form-name=\"daftarPengiriman\" list-title=\"List Unit Untuk Delivery\" row-template-url=\"app/sales/sales6/dec/deliveryUnit/rowTemplatePengirimanUnit.html\"> <bsform-mobilemodal> <a ng-click=\"mulaiDECModal()\"> <li class=\"list-group-item\">Mulai DEC</li> </a> </bsform-mobilemodal> </bsform-mobile> </div> <div ng-show=\"dataPelangganShow\"> <div class=\"row\" style=\"padding : 10px\"> <button style=\"float: right; width: 30%\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"NextCeklistDEC()\">Berikutnya</button> <button style=\"float: right; width: 30%\" class=\"wbtn btn ng-binding ladda-button\" ng-click=\"kembaliDataPelanggan()\">Kembali</button> </div> <div class=\"row\" style=\"padding:15px\"> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Toyota ID</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.ToyotaId}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Nama Pelanggan</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.CustomerName}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Tanggal Lahir</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.BirthDate | date: 'dd-MM-yyyy'}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Alamat</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.CustomerAddress}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Unit</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.Description}} - {{selected_data.ColorName}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. Telepon</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.Handphone1}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. Handphone</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.Handphone2}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Email</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.Email}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Nama Penerima</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.ReceiveName}}</li> </ul> </div> </div> <div ng-show=\"ceklistDEC\"> <div class=\"row\" style=\"padding:10px\"> <button class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width:30%\" ng-click=\"NextCeklistDEC2()\">Berikutnya</button> <button class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; width:30%\" ng-click=\"kembaliCeklistDEC()\">Kembali</button> </div> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" ng-disabled=\"true\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; text-align:left; padding-top:15px; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{{getDataInterior[0].GroupChecklistDECName}}</button> <div class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-repeat=\"item in getDataInterior\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleChecked(item);\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item.ChecklistItemDECName}}</h7> <span style=\"float:right\"><i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.checked == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i></span> </div> </div> </div> </div> </div> </div> <div ng-show=\"ceklistDEC2\"> <div class=\"row\" style=\"padding:10px\"> <button style=\"float:right; width:30%\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"NextCeklistKuisionerDEC()\">Berikutnya</button> <button class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; width:30%\" ng-click=\"kembaliCeklistDEC2()\">Kembali</button> </div> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" ng-disabled=\"true\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; text-align:left; padding-top:15px; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{{getDataDokumen[0].GroupChecklistDECName}}</button> <div class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-repeat=\"item1 in getDataDokumen\" style=\"padding:10px 5px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleChecked1(item1);\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item1.ChecklistItemDECName}}</h7> <span style=\"float:right\"><i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item1.checked == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i></span> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-xs-3\"> <div class=\"from-group\"> <image-control max-upload=\"1\" is-image=\"1\" img-upload=\"uploadFiles\"> <form-image-control> <input type=\"file\" bs-upload=\"onFileSelect($files)\" img-upload=\"uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1\"> <input type=\"file\" ng-model=\"selected_data.UploadPhoto = uploadFiles\" name=\"\" id=\"pickfiles-nav1\" style=\"z-index: 1; display:none\"> </form-image-control> </image-control> </div> </div> </div> </div> <div ng-show=\"KuisionerDEC\"> <div class=\"row\" style=\"padding:10px\"> <button style=\"float:right; width:30%\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"NextCeklistFeedbackDEC()\">Berikutnya</button> <button class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; width:30%\" ng-click=\"kembaliCeklistKuisionerDEC()\">Kembali</button> </div> <div style=\"padding-top:15px; padding-bottom:10px\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride3\" style=\"width:100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Info Pengiriman </button> </div> <div class=\"row\" style=\"padding-left: 1px\"> <div class=\"col-md-5\"> <div class=\"from-group\"> <label><b>Nama Pelanggan</b></label> </div> </div> </div> <div class=\"row\" style=\"padding-left: 1px\"> <div class=\"col-md-5\"> <div class=\"from-group\"> <label>{{selected_data.CustomerName}}</label> </div> </div> </div> <div class=\"row\" style=\"padding-left: 1px\"> <div class=\"col-md-5\"> <div class=\"from-group\"> <label><b>Tanggal Pengiriman</b></label> </div> </div> </div> <div class=\"row\" style=\"padding-left: 1px\"> <div class=\"col-md-5\"> <div class=\"from-group\"> <label>{{selected_data.SentUnitDate | date: 'dd-MM-yyyy'}} &nbsp; {{selected_data.SentUnitTime}} </label> </div> </div> </div> <div class=\"row\" style=\"padding:15px\"> <div class=\"col-md-12\"> <div class=\"from-group\" style=\"font-size: 45px; color: #d53337; text-align:center\"> <span uib-rating ng-model=\"DeliveryUnitRate\" max=\"max\" read-only=\"isReadonly\" on-hover=\"hoveringOver(value)\" on-leave=\"overStar = null\" titles=\"['Sangat Tidak Puas', 'Tidak Puas', 'Cukup Puas', 'Puas', 'Sangat Puas']\" aria-labelledby=\"default-rating\" style=\"width:15%\"></span> </div> </div> </div> </div> <div ng-show=\"FeedbackDEC\"> <div class=\"row\" style=\"padding:10px\"> <button style=\"float:right; width:30%\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"NextCeklistSignatureDEC();\">Berikutnya</button> <button class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; width:30%\" ng-click=\"kembaliCeklistFeedbackDEC();\">Kembali</button> </div> <div style=\"padding-top:15px; padding-bottom:10px\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"dropdownBtnOveride3\" style=\"width:100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Info Pengiriman </button> </div> <div class=\"row\" style=\"padding-left:1px\"> <div class=\"col-md-5\"> <div class=\"from-group\"> <label><b>Nama Pelanggan</b></label> </div> </div> </div> <div class=\"row\" style=\"padding-left: 1px\"> <div class=\"col-md-5\"> <div class=\"from-group\"> <label>{{selected_data.CustomerName}}</label> </div> </div> </div> <div class=\"row\" style=\"padding-left: 1px\"> <div class=\"col-md-7\"> <div class=\"from-group\"> <bsreqlabel>Saran</bsreqlabel> <div class=\"input-icon-right\"> <textarea type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"Input Saran\" ng-model=\"selected_data.Feedback\" maxlength=\"50\" rows=\"15\" cols=\"50\" required>\r" +
    "\n" +
    "                </textarea></div> </div> </div> </div> </div> <div ng-show=\"SignatureDEC\"> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div style=\"float: right\"> <div class=\"row\"> <button class=\"btn wbtn\" ng-click=\"kembaliCeklistBintang()\">Kembali</button> <button ng-click=\"dataurl = signature.dataUrl\" style=\"display: none\"></button> <button class=\"btn wbtn\" ng-click=\"clear()\">Batal</button> <button class=\"btn rbtn\" ng-click=\"simpanDariSign()\">Simpan</button> </div> </div> </div> <div style=\"margin-bottom:10px; padding-top:15px\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" id=\"btnSigning\" style=\"width:100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Tanda Tangan Pelanggan</button> </div> <div class=\"row\" style=\"margin: 20px 0 0 0\"> <div class=\"col-xs-12\" style=\"margin: 0 0 0 0\"> <table width=\"100%\"> <tr> <td style=\"width:30%\" class=\"signaturetable\"> <label>Toyota ID</label> </td> <td style=\"width:35%\" class=\"signaturetable\"> <label>Nama Pelanggan</label> </td> <td style=\"width:35%\" class=\"signaturetable\"> <label>Nama Penerima</label> </td> </tr> <tr style=\"background-color:transparent\"> <td style=\"width:30%\" class=\"signaturetable\"> <label>{{selected_data.ToyotaId}}</label> </td> <td style=\"width:35%\" class=\"signaturetable\"> <label>{{selected_data.CustomerName}}</label> </td> <td style=\"width:35%\" class=\"signaturetable\"> <label>{{selected_data.ReceiveName}}</label> </td> </tr> </table> </div> <i>&nbsp&nbsp&nbsp&nbsp* Silahkan memberi tanda tangan pada kotak dibawah ini.</i> <div class=\"containersign\" ng-style=\"{'max-width': '700px', 'max-height': boundingBox.height + 'px','border': 'thin solid black'}\"> <signature-pad accept=\"accept\" clear=\"clear\" height=\"400\" width=\"700\" dataurl=\"signature.dataUrl\"></signature-pad> <div class=\"row\" style=\"padding:10px 0 0 0\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-8\" style=\"padding-left:15px; padding-top:10px\"> <bsreqlabel>Dengan memberikan tanda tangan digital, menandakan proses pengiriman unit sudah selesai dan unit sudah di terima</bsreqlabel> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales6/dec/deliveryUnit/rowTemplatePengirimanUnit.html',
    "<!--Harus item--> <div style=\"font-size: 12px\">Nama Pelanggan : {{item.CustomerName}} <button ng-if=\"item.StatusDECName!='Selesai DEC'\" ng-click=\"openModal(item)\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:white; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:10px\"> <li style=\"width:auto\">No. SPK / No. Rangka : {{item.FormSPKNo}} / {{item.FrameNo}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:10px\"> <li>Tanggal Pengiriman : {{item.SentUnitDate | date:'dd-MM-yyyy'}} &nbsp; {{item.SentUnitTime}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:10px\"> <li style=\"width:auto\">Status DEC : {{item.StatusDECName}}</li> </ul> </p>"
  );


  $templateCache.put('app/sales/sales6/dec/followUpDec/followUpDec.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"FollowUpDEC\"> <bsform-grid ng-form=\"FollowUpDecForm\" factory-name=\"FollowUpDecFactory\" model=\"mfollowUpDec\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"selectedRows\" grid-hide-action-column=\"true\" on-select-rows=\"onSelectRows\" hide-new-button=\"true\" form-name=\"FollowUpDecForm\" form-title=\"Follow UP\" modal-title=\"Follow UP\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> </bsform-grid> </div> <div ng-show=\"surveyAfterDEC\"> <button id=\"saveBtn\" ng-if=\"status != 'LihatDetail'\" class=\"rbtn btn\" style=\"float:right; margin:-30px 0 0 1px\" ng-click=\"btnSimpan()\">Simpan</button> <button id=\"btnBatal\" ng-click=\"btnKembali()\" class=\"wbtn btn\" style=\"float:right; margin:-30px 0 0 0\">Kembali</button> <div class=\"row\" style=\"padding-bottom:15px; padding-top:15px\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label><b>Toyota ID :</b></label> </div> <div class=\"from-group\"> <label>{{mfollowUpDec.ToyotaId}}</label> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label><b>Nama Pelanggan :</b></label> </div> <div class=\"from-group\"> <label>{{mfollowUpDec.CustomerName}}</label> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label><b>Sales :</b></label> </div> <div class=\"from-group\"> <label>{{mfollowUpDec.EmployeeName}}</label> </div> </div> </div> <div class=\"row\" style=\"padding-bottom: 15px\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label><b>Tanggal Pengiriman :</b></label> </div> <div class=\"from-group\"> <label><!--{{mfollowUpDec.SentUnitDate|date:'dd-MM-yyyy'}} - {{mfollowUpDec.SentUnitTimeConverted|date:'HH:mm'}}-->{{mfollowUpDec.ActualPDD|date:'dd-MM-yyyy - HH:mm'}}</label> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label><b>Nama Penerima :</b></label> </div> <div class=\"from-group\"> <label>{{mfollowUpDec.ReceiverName}}</label> </div> </div> </div> <div class=\"row\" style=\"padding-bottom: 15px\"> <div class=\"col-md-3\"> <div class=\"from-group\"> <label><b>Model :</b></label> </div> <div class=\"from-group\"> <label>{{mfollowUpDec.VehicleModelName}}</label> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label><b>Tipe :</b></label> </div> <div class=\"from-group\"> <label>{{mfollowUpDec.Description}}</label> </div> </div> <div class=\"col-md-3\"> <div class=\"from-group\"> <label><b>Warna :</b></label> </div> <div class=\"from-group\"> <label>{{mfollowUpDec.ColorName}}</label> </div> </div> </div> <hr> <div class=\"listData\"> <div class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-if=\"status != 'LihatDetail'\" ng-repeat=\"item in mfollowUpDec.ListDECFollowUpDetailView\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleChecked(item);\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item.Question}}</h7> <span style=\"float:right\"><i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.IsTrue == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i></span> </div> </div> </div> <div class=\"list-group-item\" ng-if=\"status == 'LihatDetail'\" ng-repeat=\"item in mfollowUpDec.ListDECFollowUpDetailView\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item.Question}}</h7> <span ng-if=\"item.IsTrue == true\" style=\"float:right\"><i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.IsTrue == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i></span> <span ng-if=\"item.IsTrue == false\" style=\"float:right\"><i id=\"itemBtnRadio1\" class=\"fa fa-times-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.IsTrue == false ? 'red':'#000000'}\" aria-hidden=\"true\"></i></span> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/sales/sales6/dec/getInOut/getInOut.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <link rel=\"stylesheet\" href=\"css/uitimelinestyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <link rel=\"styleSheet\" href=\"release/ui-grid.min.css\"> <script src=\"/release/ui-grid.min.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <div ng-show=\"gateInOutShow\" style=\"width:100%\"> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li style=\"width:50%; text-align:center\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainerInOut','GetInTab')\">GATE IN</a></li> <li style=\"width:50%; text-align:center\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('tabContainerInOut','GetOutTab')\">GATE OUT</a></li> </ul> <div id=\"tabContainerInOut\" class=\"tab-content\" style=\"padding-left:13px\"> <div id=\"GetInTab\" class=\"tab-pane fade in active\" style=\"\"> <div class=\"row\" style=\"margin-top:12px; margin-bottom:11px\"> <div class=\"col-md-4\"> <div class=\"input-icon-right\"> <i class=\"fa fa-search\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" ng-model=\"\" ng-maxlength=\"30\" required> </div> </div> </div> <div class=\"row\"> <table style=\"width:100%; background-color:#d53337\"> <tr> <td align=\"center\"> <font color=\"black\">Booking Services</font> </td> </tr> </table> <table class=\"table table-bordered table-responsive\" style=\"\"> <thead> <tr> <th class=\"headerRow\">Model</th> <th class=\"headerRow\">No. Polisi</th> <th class=\"headerRow\">Nama</th> <th class=\"headerRow\">Janji</th> <th class=\"headerRow\">CheckIn</th> </tr> </thead> <tbody> <tr> <th scope=\"row\">Camry</th> <td>B 2320 TYP</td> <td>Luis Garcia</td> <td>08.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\">Avanza</th> <td>B 2234 POD</td> <td>Jesey Garcia</td> <td>09.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\"></th> <td></td> <td></td> <td></td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\"></th> <td></td> <td></td> <td></td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\"></th> <td></td> <td></td> <td></td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> </tbody> </table> </div> <div class=\"row\" style=\"padding-bottom:5px\"> <table style=\"width:100%; background-color:#d53337\"> <tr> <td align=\"center\"> <font color=\"black\">Walk In</font> </td> </tr> </table> </div> <div class=\"row\"> <div align=\"center\" stytle=\"font-size:initial;\">Input no. Polisi</div> <div class=\"col-md-4\" style=\"padding-bottom:10px\"> <div class=\"from-group\"> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"No. Polisi\" ng-model=\"\" required> </div> </div> </div> </div> <div class=\"row\"> <table style=\"width:100%\"> <tr> <td style=\"width:32%\"> <button id=\"getInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">beli Mobil</button> </td> <td style=\"width:32%\"> <button id=\"getOutbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Services GR</button> </td> <td style=\"width:32%\"> <button id=\"getOutbtn\" ng-click=\"\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Other GR</button> </td> </tr> </table> </div> <div class=\"row\"> <table style=\"width:100%\"> <tr> <td style=\"width:32%\"> <button id=\"getOutbtn\" onclick=\"\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Beli Parts</button> </td> <td style=\"width:32%\"> <button id=\"getOutbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Services BP</button> </td> <td style=\"width:32%\"> <button id=\"getInbtn\" onclick=\"\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Complaint</button> </td> </tr> </table> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <table style=\"width:100%\"> <tr> <td style=\"width:25%\"> <button id=\"getOutbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Pengambilan BP</button> </td> <td style=\"width:25%\"> <button id=\"getOutbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Penerimaan Satelite</button> </td> </tr> </table> </div> <div class=\"row\" style=\"padding-bottom:10px\"> <table style=\"width:100%\"> <tr> <td align=\"left\" style=\"width:12%; padding-right:15px\">Visitor </td> <td width=\"48%\" align=\"center\"> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"\" ng-model=\"\" required style=\"width:100%\"> </div> </td> <td width=\"25%\" style=\"text-align:center\"> <button id=\"simpanBtn\" ng-click=\"();\" class=\"rbtn btn ng-binding ladda-button\" style=\"bottom:0px\">Simpan</button> </td> <td width=\"15%\"> </td> </tr> </table> </div> <div class=\"row\" style=\"padding-bottom:5px\"> <table style=\"width:100%; background-color:#d53337\"> <tr> <td align=\"center\"> <font color=\"black\">Scan In</font> </td> </tr> </table> </div> <div class=\"row\" style=\"text-align:center\"> <button id=\"getInbtn\" ng-click=\"scanGetInBtn()\" class=\"rbtn btn ng-binding ladda-button\" style=\"bottom:0px\">Scan</button> </div> </div> <!--GET OUT--> <div id=\"GetOutTab\" class=\"tab-pane fade\" style=\"\"> <div class=\"row\" style=\"margin-top:12px; margin-bottom:11px\"> <div class=\"col-md-4\"> <div class=\"input-icon-right\"> <i class=\"fa fa-search\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" ng-model=\"\" ng-maxlength=\"30\" required> </div> </div> </div> <div class=\"row\"> <table style=\"width:100%; background-color:#d53337\"> <tr> <td align=\"center\" color=\"white\"> <font color=\"black\">Selesai services</font> </td> </tr> </table> <table class=\"table table-bordered table-responsive\"> <thead> <tr> <th class=\"headerRow\">Model</th> <th class=\"headerRow\">No. Polisi</th> <th class=\"headerRow\">Nama</th> <th class=\"headerRow\">Jam SIKK</th> <th class=\"headerRow\">CheckOut</th> </tr> </thead> <tbody> <tr> <th scope=\"row\">Camry</th> <td>B 2320 TYP</td> <td>Luis Garcia</td> <td>08.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\">Avanza</th> <td>B 5234 POD</td> <td>Jesey Garcia</td> <td>09.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\">Fortuner</th> <td>B 1872 RUS</td> <td>Bailly</td> <td>12.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\">Corrolla</th> <td>B 9384 IG</td> <td>Marchisio</td> <td>14.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> </tbody> </table> </div> <div class=\"row\"> <table style=\"width:100%; background-color:#d53337\"> <tr> <td align=\"center\" color=\"white\"> <font color=\"black\">Test Drive Final Inspection</font> </td> </tr> </table> <table class=\"table table-bordered table-responsive\"> <thead> <tr> <th class=\"headerRow\">Model</th> <th class=\"headerRow\">No. Polisi</th> <th class=\"headerRow\">Nama</th> <th class=\"headerRow\">Jam SIKK</th> <th class=\"headerRow\">CheckOut</th> </tr> </thead> <tbody> <tr> <th scope=\"row\">Camry</th> <td>B 2320 TYP</td> <td>Luis Garcia</td> <td>08.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\">Avanza</th> <td>B 5234 POD</td> <td>Jesey Garcia</td> <td>09.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\">Fortuner</th> <td>B 1872 RUS</td> <td>Bailly</td> <td>12.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> <tr> <th scope=\"row\">Corrolla</th> <td>B 9384 IG</td> <td>Marchisio</td> <td>14.00</td> <td> <button id=\"checkInbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Check In</button> </td> </tr> </tbody> </table> </div> <div class=\"row\" style=\"padding-bottom:5px\"> <table style=\"width:100%; background-color:#d53337\"> <tr> <td align=\"center\"> <font color=\"black\">Unit Keluar</font> </td> </tr> </table> </div> <div class=\"row\"> <div align=\"center\" stytle=\"font-size:initial;\">Input no. Polisi</div> <div class=\"col-md-4\" style=\"padding-bottom:10px\"> <div class=\"from-group\"> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"nama\" placeholder=\"No. Polisi\" ng-model=\"\" required> </div> </div> </div> </div> <div class=\"row\" style=\"padding-bottom:5px\"> <table style=\"width:100%; background-color:#d53337\"> <tr> <td align=\"center\"> <font color=\"black\">Scan Out</font> </td> </tr> </table> </div> <div class=\"row\" style=\"text-align:center\"> <button id=\"getInbtn\" ng-click=\"scanGetOutBtn()\" class=\"btn btn-default btn-sm\" style=\"bottom:0px\">Scan</button> </div> </div> </div> </div> <div ng-show=\"getInScanShow\" style=\"display:none; width:100%; margin-left: 11px\"> <div class=\"row\"> <button id=\"btnBack\" ng-click=\"kembaliScanGetInBtn()\" class=\"btn btn-default btn-sm\" style=\"float: right; bottom: 2px\">Kembali </button> </div> <div> <svg id=\"ean-13\"></svg> </div> <table style=\"width:100%\"> <tr> <td width=\"50%\" style=\"text-align:center\"> No Rangka </td> <td width=\"50%\" style=\"text-align:left\"> MHF3829223FU9003 </td> </tr> </table> <div class=\"row\" style=\"text-align:center\"> <div class=\"col-md-4\"> <div class=\"from-group\"> <label>2 Februari 2017 09:37</label> </div> </div> </div> <div class=\"row\" style=\"padding-top:2px; padding-bottom:10px\"> <table style=\"width:100%\"> <tr> <td style=\"width:25%\"> <button id=\"getOutbtn\" ng-click=\"();\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Good Received</button> </td> <td style=\"width:25%\"> <button id=\"getOutbtn\" ng-click=\"();\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Repair In</button> </td> </tr> <tr> <td style=\"width:25%\"> <button id=\"getOutbtn\" ng-click=\"();\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Services GR</button> </td> <td style=\"width:25%\"> <button id=\"getOutbtn\" ng-click=\"();\" class=\"btn btn-default btn-sm\" style=\"float:center; bottom:0px; width:100%\">Services BP</button> </td> </tr> </table> </div> </div> <div ng-show=\"getOutScanShow\" style=\"display:none; width:100%; margin-left: 11px\"> <div class=\"row\"> <button id=\"kembaliGetOutbtn\" ng-click=\"kembaliScanGetOutbtn();\" class=\"btn btn-default btn-sm\" style=\"float:right; bottom: 2px\">Kembali</button> </div> <div> <svg id=\"ean-13\"></svg> </div> <table style=\"width:100%\"> <tr> <td width=\"50%\" style=\"text-align:center\"> No Rangka </td> <td width=\"50%\" style=\"text-align:left\"> MHF3829223FU9003 </td> </tr> </table> <div class=\"row\" style=\"text-align:center\"> <div class=\"col-md-4\"> <div class=\"from-group\"> <label>2 Februari 2017 09:37</label> </div> </div> </div> <div class=\"row\" style=\"padding-top:2px; padding-bottom:10px\"> <table style=\"width:100%\"> <tr> <td style=\"width:25%\"> <button id=\"getOutbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"float:center;bottom:0px; width:100%\">Delivery Out</button> </td> <td style=\"width:25%\"> <button id=\"getOutbtn\" onclick=\"()\" class=\"btn btn-default btn-sm\" style=\"float:center;bottom:0px; width:100%\">Repair Out</button> </td> </tr> </table> </div> </div>"
  );


  $templateCache.put('app/sales/sales6/dec/konfirmasiWaktuDanAlasanPengiriman/konfirmasiWaktuDanAlasanPengiriman.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\"></script> <style type=\"text/css\">.box-show-setup,\r" +
    "\n" +
    ".box-hide-setup {\r" +
    "\n" +
    "    -webkit-transition: all linear 0.3s;\r" +
    "\n" +
    "    -moz-transition: all linear 0.3s;\r" +
    "\n" +
    "    -ms-transition: all linear 0.3s;\r" +
    "\n" +
    "    -o-transition: all linear 0.3s;\r" +
    "\n" +
    "    transition: all linear 0.3s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group.input-group-unstyled input.form-control {\r" +
    "\n" +
    "    -webkit-border-radius: 4px;\r" +
    "\n" +
    "    -moz-border-radius: 4px;\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".input-group-unstyled .input-group-addon {\r" +
    "\n" +
    "    border-radius: 4px;\r" +
    "\n" +
    "    border: 0px;\r" +
    "\n" +
    "    background-color: transparent;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".adv-srch {\r" +
    "\n" +
    "    padding: 10px;\r" +
    "\n" +
    "    margin-bottom: 0px;\r" +
    "\n" +
    "    border: 1px solid #e2e2e2;\r" +
    "\n" +
    "    background-color: rgba(213, 51, 55, 0.02);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".adv-srch-on {\r" +
    "\n" +
    "    padding-top: 4px !important;\r" +
    "\n" +
    "    padding: 6px;\r" +
    "\n" +
    "    margin: 0 3px 0 0;\r" +
    "\n" +
    "    margin-bottom: -5px !important;\r" +
    "\n" +
    "    border: 1px solid #e2e2e2;\r" +
    "\n" +
    "    border-bottom-style: none !important;\r" +
    "\n" +
    "    background-color: #FEFBFB;\r" +
    "\n" +
    "    border-top-left-radius: 3px;\r" +
    "\n" +
    "    border-top-right-radius: 3px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".colorsrch {\r" +
    "\n" +
    "    color: red;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".modalpadding {\r" +
    "\n" +
    "    padding-top: 0px !important;\r" +
    "\n" +
    "    padding-right: 0px !important;\r" +
    "\n" +
    "    padding-bottom: 0px !important;\r" +
    "\n" +
    "    padding-left: 0px !important;\r" +
    "\n" +
    "    border-radius: 3px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "/*@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin-top: 10px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}*/\r" +
    "\n" +
    "\r" +
    "\n" +
    ".noppadingFormGroup {\r" +
    "\n" +
    "    margin-bottom: 0px !important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".ScrollStyle {\r" +
    "\n" +
    "    max-height: 600px;\r" +
    "\n" +
    "    overflow-y: scroll;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".item-modal {\r" +
    "\n" +
    "    border: none;\r" +
    "\n" +
    "    padding-bottom: 15px;\r" +
    "\n" +
    "    border-bottom: 1px solid #ddd !important;\r" +
    "\n" +
    "    margin: 10px 30px 10px 30px !important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".mobileAddBtn {\r" +
    "\n" +
    "    padding: 0 0 0 1!important;\r" +
    "\n" +
    "    font-size: 15px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".mobileFilterBtn {\r" +
    "\n" +
    "    padding: 0 0 0 1!important;\r" +
    "\n" +
    "    font-size: 13px;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "    float: right;\r" +
    "\n" +
    "    /*background-color: silver;*/\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".filterMarg {\r" +
    "\n" +
    "    margin-bottom: 5px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media only screen and (max-width: 430px) {\r" +
    "\n" +
    "    .iconFilter {\r" +
    "\n" +
    "        margin-left: -4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media only screen and (max-width: 500px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin: -28px 0px 0 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .iconFilter {\r" +
    "\n" +
    "        margin-left: -4px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #desktopderectivemobile {\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #mobilederectivemobile {\r" +
    "\n" +
    "        display: block !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media (min-width: 501px) and (max-width: 600px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin: -36px 0px 0 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #desktopderectivemobile {\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #mobilederectivemobile {\r" +
    "\n" +
    "        display: block !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media (min-width: 601px) {\r" +
    "\n" +
    "    .btnUp {\r" +
    "\n" +
    "        margin: -33px 0px 0 0 !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    #mobilederectivemobile {\r" +
    "\n" +
    "        display: none !important\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".mobilesearchBtn {\r" +
    "\n" +
    "    text-decoration: none;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@media (max-width: 350px) {\r" +
    "\n" +
    "    .bsbreadcrumb .bsbreadcrumb-path,\r" +
    "\n" +
    "    .bsbreadcrumb i {\r" +
    "\n" +
    "        display: block !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "/*Modal*/\r" +
    "\n" +
    "\r" +
    "\n" +
    ".vertical-alignment-helper {\r" +
    "\n" +
    "    display: table;\r" +
    "\n" +
    "    height: 100%;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "    pointer-events: none;\r" +
    "\n" +
    "    /* This makes sure that we can still click outside of the modal to close it */\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".vertical-align-center {\r" +
    "\n" +
    "    /* To center vertically */\r" +
    "\n" +
    "    display: table-cell;\r" +
    "\n" +
    "    vertical-align: middle;\r" +
    "\n" +
    "    pointer-events: none;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".modal-content {\r" +
    "\n" +
    "    /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */\r" +
    "\n" +
    "    width: inherit;\r" +
    "\n" +
    "    height: inherit;\r" +
    "\n" +
    "    /* To center horizontally */\r" +
    "\n" +
    "    margin: 0 auto;\r" +
    "\n" +
    "    pointer-events: all;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@-webkit-keyframes spin {\r" +
    "\n" +
    "    0% {\r" +
    "\n" +
    "        -webkit-transform: rotate(0deg);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    100% {\r" +
    "\n" +
    "        -webkit-transform: rotate(360deg);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "@keyframes spin {\r" +
    "\n" +
    "    0% {\r" +
    "\n" +
    "        transform: rotate(0deg);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    100% {\r" +
    "\n" +
    "        transform: rotate(360deg);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".switch {\r" +
    "\n" +
    "  position: relative;\r" +
    "\n" +
    "  display: inline-block;\r" +
    "\n" +
    "  width: 60px;\r" +
    "\n" +
    "  height: 34px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".switch input {display:none;}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  cursor: pointer;\r" +
    "\n" +
    "  top: 0;\r" +
    "\n" +
    "  left: 0;\r" +
    "\n" +
    "  right: 0;\r" +
    "\n" +
    "  bottom: 0;\r" +
    "\n" +
    "  background-color: #ccc;\r" +
    "\n" +
    "  -webkit-transition: .4s;\r" +
    "\n" +
    "  transition: .4s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider:before {\r" +
    "\n" +
    "  position: absolute;\r" +
    "\n" +
    "  content: \"\";\r" +
    "\n" +
    "  height: 26px;\r" +
    "\n" +
    "  width: 26px;\r" +
    "\n" +
    "  left: 4px;\r" +
    "\n" +
    "  bottom: 4px;\r" +
    "\n" +
    "  background-color: white;\r" +
    "\n" +
    "  -webkit-transition: .4s;\r" +
    "\n" +
    "  transition: .4s;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:checked + .slider {\r" +
    "\n" +
    "  background-color: #2196F3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:focus + .slider {\r" +
    "\n" +
    "  box-shadow: 0 0 1px #2196F3;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "input:checked + .slider:before {\r" +
    "\n" +
    "  -webkit-transform: translateX(26px);\r" +
    "\n" +
    "  -ms-transform: translateX(26px);\r" +
    "\n" +
    "  transform: translateX(26px);\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* Rounded sliders */\r" +
    "\n" +
    ".slider.round {\r" +
    "\n" +
    "  border-radius: 34px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".slider.round:before {\r" +
    "\n" +
    "  border-radius: 50%;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".nopadding {\r" +
    "\n" +
    "    padding-right: 0 !important;\r" +
    "\n" +
    "    padding-top: 0 !important;\r" +
    "\n" +
    "    padding-bottom: 0 !important;\r" +
    "\n" +
    "    padding-left: 1px;\r" +
    "\n" +
    "    margin: 0 !important;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".modalpost {\r" +
    "\n" +
    "    position: fixed;\r" +
    "\n" +
    "    left: 4%;\r" +
    "\n" +
    "    top: 15%;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".table {\r" +
    "\n" +
    "    font-family: arial, sans-serif;\r" +
    "\n" +
    "    border-collapse: collapse;\r" +
    "\n" +
    "    width: 100%;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    ".td,\r" +
    "\n" +
    "\r" +
    "\n" +
    ".th {\r" +
    "\n" +
    "    text-align: left;\r" +
    "\n" +
    "    padding: 8px;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "/* .tr:nth-child(even) {\r" +
    "\n" +
    "    background-color: #dddddd;\r" +
    "\n" +
    "} */\r" +
    "\n" +
    "\r" +
    "\n" +
    "/*.list-inline {\r" +
    "\n" +
    "    font-size: 13px;\r" +
    "\n" +
    "    margin-bottom: 2px;\r" +
    "\n" +
    "}*/</style> <div ng-show=\"ShowListUnitDelivery\"> <bsform-mobile factory-name=\"KonfirmasiWaktuDanAlasanPengirimanFactory\" bind-data=\"bind_data\" info-totaldata=\"true\" filter-data=\"filterData\" custom-modallihat=\"true\" custom-modalubah=\"true\" custom-modalhapus=\"true\" enable-addbutton=\"disable\" service-enable=\"true\" selected-data=\"selected_data\" updated-data=\"updated_data\" form-name=\"KonfirmasiWaktu\" after-refresh=\"KonfirmasiWaktu_Paksa_Tanggal()\" list-title=\"List Unit Untuk Delivery\" row-template-url=\"app/sales/sales6/dec/konfirmasiWaktuDanAlasanPengiriman/rowTemplateKonfirmasiWaktudanAlasanPengiriman.html\"> <bsform-mobilemodal> <a ng-click=\"KesiapanUnitModal()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span> Cek Kesiapan Unit </li> </a> <a ng-click=\"KonfirmasiWaktuModal()\"> <li class=\"list-group-item item-modal\"> <span><i class=\"fa fa-fw fa-lg fa-list-alt\" style=\"margin-right: 15px\"></i></span> Konfirmasi Waktu Dan Lokasi </li> </a> </bsform-mobilemodal> </bsform-mobile> </div> <div ng-show=\"ShowUnitReadiness\"> <div class=\"row\" style=\"padding:10px\"> <button id=\"btnKembali\" ng-click=\"BackToListUntukDelivery()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right;width:30%\">Kembali</button> </div> <div class=\"row\" style=\"margin-left:5px\"> <p> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Nama Pelanggan</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.CustomerName}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. SPK</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.FormSPKNo}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>No. Rangka</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.FrameNo}}</li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px; margin-bottom:2px\"> <li><b>Tanggal Pengiriman</b></li> </ul> <ul class=\"list-inline\" style=\"font-family:LatoLatinWeb; font-size:13px\"> <li>{{selected_data.SentUnitDate | date:'dd-MM-yyyy'}} {{selected_data.SentUnitTime_Akalin | date :'h:mm'}}</li> </ul> </p> </div> <div class=\"row\" style=\"padding:15px\"> <table border=\"1\" style=\"margin-left:3px; width:100%\"> <tr style=\"background-color: #dddddd\"> <th style=\"text-align:center\"><label>Kesiapan Unit (inspeksi)</label></th> <th style=\"text-align:center\"><label>Administrasi</label></th> <th style=\"text-align:center\"><label>Konfirmasi Waktu dan lokasi pengiriman</label></th> </tr> <tr align=\"center\"> <td><bscheckbox ng-model=\"selected_data.StatusUnitInspeksi\" label=\"\" ng-disabled=\"true\" true-value=\"true\" false-value=\"false\"> </bscheckbox></td> <td><bscheckbox ng-model=\"selected_data.StatusAdministrasi\" label=\"\" ng-disabled=\"true\" true-value=\"true\" false-value=\"false\"> </bscheckbox></td> <td><bscheckbox ng-model=\"selected_data.StatusKonfirmasiWaktu\" label=\"\" ng-disabled=\"true\" true-value=\"true\" false-value=\"false\"> </bscheckbox></td> </tr> </table> </div> </div> <div ng-show=\"ShowDetailWaktuDanLokasiPengiriman\"> <div class=\"row\" style=\"float:right; padding:5px; width:100%\"> <button ng-click=\"Simpan()\" style=\"float:right; bottom:0px; width:30%\" class=\"rbtn btn ng-binding ladda-button\">Simpan</button> <!-- <button ng-show=\"btnUbahShow\" ng-click=\"Ubah()\" style=\"float:right; bottom:0px; width:30%\" class=\"rbtn btn ng-binding ladda-button\">Ubah</button> --> <button ng-click=\"BackToListUntukDelivery()\" style=\"float:right; bottom:0px; width:30%\" class=\"wbtn btn ng-binding ladda-button\">Kembali</button> </div> <div class=\"list-group\" id=\"formGroupPanelContent\" aria-labelledby=\"dropdownMenu2\" style=\"\"> <div class=\"row\" style=\"margin-left: 2px\"> <b><bslabel>No. SPK</bslabel></b> </div> <div class=\"row\" style=\"margin-left: 2px\"> {{selected_data.FormSPKNo}} </div> <br> <div class=\"row\" style=\"margin-left: 2px\"> <b><bslabel>No.Rangka</bslabel></b> </div> <div class=\"row\" style=\"margin-left: 2px\"> {{selected_data.FrameNo}} </div> <br> <div class=\"row\" style=\"margin-left: 2px\"> <b><bslabel>Nama Pelanggan</bslabel></b> </div> <div class=\"row\" style=\"margin-left: 2px\"> {{selected_data.CustomerName}} </div> <br> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"form-group\" style=\"margin-left:-11px\"> <b><bsreqlabel>Nama Penerima</bsreqlabel></b> <input type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"Nama Penerima\" ng-model=\"selected_data.ReceiverName\" maxlength=\"30\" style=\"margin-bottom: 10px\" alpha-numeric mask=\"Huruf\" required> </div> </div> </div> <br> <div class=\"row\" style=\"margin-left: 2px\"> <b><bslabel>Model</bslabel></b> </div> <div class=\"row\" style=\"margin-left: 2px\"> {{selected_data.VehicleModelName}} </div> <br> <div class=\"row\" style=\"margin-left: 2px\"> <b><bslabel>Tipe</bslabel></b> </div> <div class=\"row\" style=\"margin-left: 2px\"> {{selected_data.Description}} </div> <br> <div class=\"row\" style=\"margin-left: 2px\"> <b><bslabel>Warna</bslabel></b> </div> <div class=\"row\" style=\"margin-left: 2px\"> {{selected_data.ColorName}} </div> <br> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" style=\"margin-left:-10px\"> <bsreqlabel>Tanggal Pengiriman</bsreqlabel> <!-- <bsdatepicker name=\"tanggal\" ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable\" date-options=\"dateOptionsDECmin1\" ng-model=\"selected_data.SentUnitDate\">\r" +
    "\n" +
    "\t\t\t\t\t</bsdatepicker> --> <input onkeydown=\"return false\" min=\"{{TodayDate}}\" type=\"date\" ng-model=\"selected_data.SentUnitDate\"> </div> </div> </div> <bsreqlabel style=\"margin-left:5px;margin-bottom: 20px\">ETA Pelanggan</bsreqlabel> <div class=\"col-xs-12\" style=\"margin-top: -25px\"> <div class=\"col-xs-4\" align=\"center\"> <div class=\"form-group\"> <!-- <input ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_WaktuPengiriman_EnableDisable\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"ETA Awal\" ng-model=\"selected_data.ETACustomer1\" ng-maxlength=\"50\" required> --> <div uib-timepicker ng-model=\"mytime\" ng-change=\"changed()\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" style=\"float: left\"> - </div> </div> </div> <div class=\"col-xs-1\" align=\"center\"> <div class=\"form-group\"> <label style=\"padding-top: 40px;padding-left: 7px\"> - </label> </div> </div> <div class=\"col-xs-4\" align=\"center\"> <div class=\"form-group\"> <!-- <input ng-disabled=\"!Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_WaktuPengiriman_EnableDisable\" type=\"text\" class=\"form-control\" name=\"NoRangka\" placeholder=\"ETA Akhir\" ng-model=\"selected_data.ETACustomer2\" ng-maxlength=\"50\" required> --> <div uib-timepicker ng-model=\"mytime2\" ng-change=\"changed()\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" style=\"float: left\"></div> </div> </div> </div> <div class=\"row col-xs-12\" style=\"margin-top:-5px;margin-left:-25px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bslabel>Alamat Pengiriman</bslabel> <textarea type=\"text\" class=\"form-control\" name=\"AlamatPengiriman\" placeholder=\"Alamat Pengiriman\" ng-model=\"selected_data.SentAddress\" maxlength=\"250\" style=\"margin-bottom: 10px\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <p> <div class=\"row\" style=\"margin-left: 2px\"> <b><bslabel>Direct Delivery Dari PDC</bslabel></b> </div> <ul class=\"list-inline\"> <li> <bscheckbox ng-model=\"selected_data.DirectDeliveryPDC\" label=\"Ya\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </li> </ul> <div class=\"row\" style=\"margin-left: 2px\"> <b><bslabel>Driver</bslabel></b> </div> <div class=\"row\" style=\"margin-left: 2px\"> {{selected_data.DriverPDSName}} </div> <br> <div class=\"row\" style=\"padding-top:12px;margin-left: 2px;width: 100%\"> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" ng-disabled=\"true\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Keterangan Pengecualian </button> <div class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-repeat=\"item in DataKetPengecualian\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleChecked(item)\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item.DeliveryExcReasonName}}</h7> <span style=\"float:right\"><i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.checked == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i></span> </div> </div> </div> </div> </div> </div> </p> </div> </div>"
  );


  $templateCache.put('app/sales/sales6/dec/konfirmasiWaktuDanAlasanPengiriman/rowTemplateKonfirmasiWaktudanAlasanPengiriman.html',
    "<!--Harus item--> <div style=\"font-size: 12px\">Nama Pelanggan : {{item.CustomerName}} <button ng-click=\"openModal(item)\" ng-if=\"item.TombolEdit==true\" class=\"btn btn-default\" style=\"float:right; border:0; background-color:transparent; box-shadow:none\" id=\"myButton\" type=\"button\"> <span class=\"glyphicon glyphicon-option-vertical\" style=\"float:center\"> </span> </button> </div> <p> <ul class=\"list-inline\" style=\"font-size:10px\"> <li style=\"width:auto\">No. SPK / No. Rangka : {{item.FormSPKNo}} - {{item.FrameNo}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:10px\"> <li>Tanggal, Jam Pengiriman : {{item.SentUnitDate | date:'dd-MM-yyyy'}} {{item.SentUnitTime_Akalin| date :'h:mm'}}</li> </ul> <ul class=\"list-inline\" style=\"font-size:10px\"> <li>Tipe Pengiriman : {{item.DirectDeliveryStatus}}</li> </ul> </p>"
  );


  $templateCache.put('app/sales/sales6/dec/listDec/listDec.html',
    "<script type=\"text/javascript\" src=\"js/print_min.js\"></script> <style type=\"text/css\">.table {\r" +
    "\n" +
    "        font-family: arial, sans-serif;\r" +
    "\n" +
    "        border-collapse: collapse;\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .td,\r" +
    "\n" +
    "    .th {\r" +
    "\n" +
    "        border: 1px solid #dddddd;\r" +
    "\n" +
    "        text-align: left;\r" +
    "\n" +
    "        padding: 8px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .tr:nth-child(even) {\r" +
    "\n" +
    "        background-color: #dddddd;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    .gridStyle {\r" +
    "\n" +
    "        border: 1px solid rgb(212, 212, 212);\r" +
    "\n" +
    "        width: 400px;\r" +
    "\n" +
    "        height: 300px\r" +
    "\n" +
    "    }</style> <div ng-show=\"ShowListDEC\"> <bsform-grid ng-form=\"ListDecForm\" factory-name=\"ListDecFactory\" model=\"mlistDec\" model-id=\"Id\" get-data=\"getData\" selected-rows=\"selectedRows\" grid-hide-action-column=\"true\" on-select-rows=\"onSelectRows\" form-name=\"ListDecForm\" hide-new-button=\"true\" form-title=\"\" modal-title=\"ListDec\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" show-advsearch=\"on\"> <bsform-advsearch> <div class=\"row\" style=\"margin-left:10px\"> <div id=\"comboBoxFilter\" style=\"float:left;width:30%; font-size:12px;margin-right:10px\"> <div style=\"margin-left:5px\" class=\"form-group filterMarg\"> <bslabel>Status DEC</bslabel> <select ng-model=\"filter.StatusDECId\" class=\"form-control\" ng-options=\"item.StatusDECId as item.StatusDECName for item in optionStatusDecName\" placeholder=\"Status DEC\"> </select> </div> </div> <div id=\"comboBoxFilter\" style=\"float:left;width:30%; font-size:12px\"> <div class=\"form-group filterMarg\"> <bslabel>Tanggal DEC</bslabel> <bsdatepicker name=\"tanggal\" ng-model=\"filter.ActualPDD\" date-options=\"dateOptions\"> </bsdatepicker> </div> </div> </div> </bsform-advsearch> </bsform-grid> <div class=\"ui modal TerimaBSTKB\" role=\"dialog\"> <i class=\"close icon\"></i> <div class=\"header\">Pendataan BSTKB</div> <div class=\"content\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Toyota ID</b> <p>{{TerimaBSTKB.ToyotaId}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Nama Pelanggan</b> <p>{{TerimaBSTKB.CustomerName}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Nama Penerima</b> <p>{{TerimaBSTKB.ReceiverName}}</p> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>No. Handphone</b> <p>{{TerimaBSTKB.Handphone1}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Alamat</b> <p>{{TerimaBSTKB.Address}}</p> </div> </div> <div class=\"col-md-4\"> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Model</b> <p>{{TerimaBSTKB.VehicleModelName}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Tipe</b> <p>{{TerimaBSTKB.Description}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Warna</b> <p>{{TerimaBSTKB.ColorName}}</p> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Tanggal DEC</b> <p>{{TerimaBSTKB.ActualPDD | date:'dd-MM-yyyy'}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Jam Selesai DEC</b> <p>{{TerimaBSTKB.ActualPDD| date:'h:mm a'}}</p> </div> </div> <div class=\"col-md-4\"> <!--<div class=\"form-group\">\r" +
    "\n" +
    "                        <b>BSTKB Sudah Diterima</b>\r" +
    "\n" +
    "                    <p>{{TerimaBSTKB.TerimaBSTKBStatusName}}</p>\r" +
    "\n" +
    "                    </div>--> </div> </div> </div> <div class=\"actions\"> <div class=\"wbtn btn ng-binding ladda-button\" style=\"width:130px\" ng-click=\"onListCancel()\">Kembali</div> <div class=\"rbtn btn ng-binding ladda-button\" style=\"width:130px\" ng-click=\"btnTerimaBSTKB()\">Terima BSTKB</div> </div> </div> <div class=\"ui modal CetakThankYou\" role=\"dialog\"> <i class=\"close icon\"></i> <div class=\"header\">Pendataan BSTKB</div> <div class=\"content\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Toyota ID</b> <p>{{TerimaBSTKB.ToyotaId}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Nama Pelanggan</b> <p>{{TerimaBSTKB.CustomerName}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Nama Penerima</b> <p>{{TerimaBSTKB.ReceiverName}}</p> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>No. Handphone</b> <p>{{TerimaBSTKB.Handphone1}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Alamat</b> <p>{{TerimaBSTKB.Address}}</p> </div> </div> <div class=\"col-md-4\"> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Model</b> <p>{{TerimaBSTKB.VehicleModelName}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Tipe</b> <p>{{TerimaBSTKB.Description}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Warna</b> <p>{{TerimaBSTKB.ColorName}}</p> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Tanggal DEC</b> <p>{{TerimaBSTKB.ActualPDD | date:'dd-MM-yyyy'}}</p> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <b>Jam Selesai DEC</b> <p><!--{{TerimaBSTKB.ChangeDeliveryTime | date:'h:mm a'}}-->{{TerimaBSTKB.ActualPDD | date:'h:mm a'}}</p> </div> </div> <div class=\"col-md-4\"> <!--<div class=\"form-group\">\r" +
    "\n" +
    "                        <b>BSTKB Sudah Diterima</b>\r" +
    "\n" +
    "                    <p>{{TerimaBSTKB.TerimaBSTKBStatusName}}</p>\r" +
    "\n" +
    "                    </div>--> </div> </div> </div> <div class=\"actions\"> <div class=\"wbtn btn ng-binding ladda-button\" style=\"width:170px\" ng-click=\"onListCancel()\">Kembali</div> <div class=\"rbtn btn ng-binding ladda-button\" style=\"width:170px\" ng-click=\"btnCetakThankYou()\">Cetak Thank You Letter</div> </div> </div> </div> <div ng-show=\"false\"> <div class=\"row\"> <button ng-click=\"btncetakPreviewListDecPanda();\" style=\"float:right; width:120px\" class=\"rbtn btn ng-binding ladda-button\">Cetak</button> <button ng-click=\"onListCancel()\" class=\"wbtn btn ng-binding ladda-button\" style=\"float:right; width:120px\">Kembali</button> </div> <hr> <table width=\"100%\" border=\"0\" style=\"font-family:LatoLatinWeb\"> <tr> <td width=\"491\">Kepada Yth.</td> <td width=\"400\"><div align=\"right\">(kota),({{TodayDate}}) </div></td> </tr> <tr> <td>{{TerimaBSTKB.CustomerName}}</td> <td>&nbsp;</td> </tr> <tr> <td>{{TerimaBSTKB.Address}}</td> <td>&nbsp;</td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td>Dengan hormat, </td> <td>&nbsp;</td> </tr> <tr> <td height=\"62\" colspan=\"2\"><div align=\"justify\">Atas nama jajaran manajemen dan staff, kami mengucapkan terima kasih atas kepercayaan <b>{{TerimaBSTKB.CustomerName}}</b> yang telah memilih dealer <b>{{TerimaBSTKB.Name}}</b> dalam pembelian kendaraan TOYOTA idaman Anda dan keluarga. Semoga kehadiran kendaraan tersebut membawa manfaat dalam keceriaan bagi Anda dalam beraktifitas sehari - hari.</div></td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td colspan=\"2\">Berikut kami sampaikan informasi mengenai DATA KENDARAAN Anda, sebagai berikut : </td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td> <table width=\"491\" border=\"0\"> <tr> <td width=\"226\" align=\"center\">&nbsp;&nbsp;- Tipe / Warna</td> <td width=\"17\">:</td> <td width=\"231\">{{TerimaBSTKB.Description}} / {{TerimaBSTKB.ColorName}}</td> </tr> <tr> <td width=\"226\" align=\"center\">- No. Rangka</td> <td>:</td> <td>{{TerimaBSTKB.FrameNo}}</td> </tr> </table> </td> <td>&nbsp;</td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td colspan=\"2\"><div align=\"justify\">Dalam kesempatan ini kami juga ingin menyampaikan bahwa dalam upaya untuk terus meningkatkan pelayanan kepada pelanggan, Anda berkesempatan untuk disurvei oleh lembaga independent yang ditunjuk TOYOTA, untuk melukan servey kepada pelanggan terkait dengan kualitas pelayanan dealer kami. </div></td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td colspan=\"2\"><div align=\"justify\">Sekali lagi kami mengucapkan teriman kasih yang sebesar - besarnya atas kepercayaan Anda memilih dealer kami, dan mohon maaf apabila terdapat hal yang kurang berkenan dalam pelayanan kami. </div></td> </tr> <tr> <td colspan=\"2\">&nbsp;</td> </tr> <tr> <td><div align=\"left\">Salam Hangat,</div></td> <td>&nbsp;</td> </tr> <tr> <td height=\"90\" valign=\"bottom\">(Nama Branch Manager)</td> <td>&nbsp;</td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr> </table> </div>"
  );


  $templateCache.put('app/sales/sales6/dec/listDeliveryUnit/listDeliveryUnit.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/javascript\" src=\"js/print_min.js\"></script> <script type=\"text/javascript\">function UlalaToBase64(buffer) {\r" +
    "\n" +
    "        var binary = '';\r" +
    "\n" +
    "        var bytes = new Uint8Array(buffer);\r" +
    "\n" +
    "        var len = bytes.byteLength;\r" +
    "\n" +
    "        for (var i = 0; i < len; i++) {\r" +
    "\n" +
    "            binary += String.fromCharCode(bytes[i]);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "        return window.btoa(binary);\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    function dataURItoBlob(dataURI) {\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // convert base64/URLEncoded data component to raw binary data held in a string\r" +
    "\n" +
    "        var byteString;\r" +
    "\n" +
    "        if (dataURI.split(',')[0].indexOf('base64') >= 0)\r" +
    "\n" +
    "            byteString = atob(dataURI.split(',')[1]);\r" +
    "\n" +
    "        else\r" +
    "\n" +
    "            byteString = unescape(dataURI.split(',')[1]);\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // separate out the mime component\r" +
    "\n" +
    "        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // write the bytes of the string to a typed array\r" +
    "\n" +
    "        var ia = new Uint8Array(byteString.length);\r" +
    "\n" +
    "        for (var i = 0; i < byteString.length; i++) {\r" +
    "\n" +
    "            ia[i] = byteString.charCodeAt(i);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        return new Blob([ia], {\r" +
    "\n" +
    "            type: mimeString\r" +
    "\n" +
    "        });\r" +
    "\n" +
    "    }</script> <style type=\"text/css\"></style> <div ng-show=\"ShowListDelivery\"> <bsform-grid ng-form=\"ListDeliveryUnitForm\" factory-name=\"ListDeliveryUnitFactory\" model=\"mListDeliveryUnit\" model-id=\"DECId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" hide-new-button=\"true\" form-name=\"ListDeliveryUnitForm\" form-title=\"ListDeliveryUnit\" modal-title=\"ListDeliveryUnit\" modal-size=\"small\" loading=\"loading\" icon=\"fa fa-fw fa-child\"> </bsform-grid> </div> <div ng-show=\"ShowCeklistAdministrasi\"> <button ng-disabled=\"selectedItems.length<=0\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"btnSimpan()\" style=\"float:right; margin:-30px 0 0 1px; width:11%\">Simpan</button> <button id=\"saveBtn\" class=\"wbtn btn ng-binding ladda-button\" ng-click=\"btnkembaliCheckListAdmin()\" style=\"float:right; margin:-30px 0 0 1px; width:11%\">Kembali</button> <div class=\"row\" style=\"padding-top:12px;margin-left:0px;margin-right:0px\"> <div class=\"listData\"> <button class=\"btn btn-default dropdown-toggle dropdown-toggle\" type=\"button\" ng-disabled=\"true\" id=\"dropdownBtnListDataRadio\" style=\"width: 100%; text-align:left; background-color:#888b91; color:white\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Checklist Admin </button> <div class=\"list-group\" id=\"dropdownContentListDataRadio\" aria-labelledby=\"dropdownMenu1\"> <div class=\"list-group-item\" ng-repeat=\"item in getAdministrasi\" style=\"padding:10px 0px 10px 10px; background-color:#e8eaed\" ng-click=\"toggleChecked(item)\"> <div class=\"row\"> <div class=\"col-sm-12\"> <h7 style=\"float: left\">{{item.ChecklistAdminPaymentName}}</h7> <span style=\"float:right\"><i id=\"itemBtnRadio1\" class=\"fa fa-check-circle-o fa-2x fa-lg\" ng-style=\"{'color': item.checked == true ? '#3df747':'#000000'}\" aria-hidden=\"true\"></i></span> </div> </div> </div> </div> </div> </div> <div class=\"row\" style=\"margin-left:-5px\"> <table class=\"table table-bordered table-responsive\" border=\"1\" style=\"border: none !important\"> <tbody style=\"border: none !important\"> <td style=\"border: none !important; vertical-align:middle\"> <input ng-disabled=\"selectedItems.length<=0\" style=\"margin-right:5px\" type=\"checkbox\" ng-model=\"ViewDetail.DokumenLengkapBit\"> <label>Dokumen sudah lengkap</label> </td> </tbody> </table> </div> </div> <div style=\"margin: 0 0 0 0\" class=\"row\" ng-show=\"ShowListDeliveryUnitPrintPreview\"> <div class=\"row\" style=\"margin: -30px 0 0 0\"> <button ng-click=\"btnCetakListDeliveryUnitDonlot()\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right; width: 10%\">Download</button> <button ng-click=\"ClosePreviewListDeliveryUnit()\" style=\"float:right; width:10%\" class=\"wbtn btn ng-binding ladda-button\">Kembali</button> </div> <div class=\"row\" style=\"margin: 100px 0 0 0\"> <p align=\"center\"> <img style=\"margin: auto\" ng-src=\"{{ListDeliveryUnitGambarnya}}\"> </p> </div> </div>"
  );


  $templateCache.put('app/sales/sales6/dec/templateDigitalSignature/templateDigitalSignature.html',
    "<style type=\"text/css\">.signature_container {\r" +
    "\n" +
    "        border: 1px solid red;\r" +
    "\n" +
    "        padding: 10px 10px 40px 10px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        /*margin: 0 auto 0 auto;*/\r" +
    "\n" +
    "        height: 100%;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .signature_container .signature {\r" +
    "\n" +
    "            border: 1px solid orange;\r" +
    "\n" +
    "            margin: 0 auto;\r" +
    "\n" +
    "            cursor: pointer;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            .signature_container .signature canvas {\r" +
    "\n" +
    "                border: 1px solid #999;\r" +
    "\n" +
    "                margin: 0 auto;\r" +
    "\n" +
    "                cursor: pointer;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .signature_container .buttons {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            bottom: 10px;\r" +
    "\n" +
    "            left: 10px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        .signature_container .sizes {\r" +
    "\n" +
    "            position: absolute;\r" +
    "\n" +
    "            bottom: 10px;\r" +
    "\n" +
    "            right: 10px;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            .signature_container .sizes input {\r" +
    "\n" +
    "                width: 4em;\r" +
    "\n" +
    "                text-align: center;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .signature_result {\r" +
    "\n" +
    "        border: 1px solid blue;\r" +
    "\n" +
    "        margin: 30px auto 0 auto;\r" +
    "\n" +
    "        height: 220px;\r" +
    "\n" +
    "        width: 568px;\r" +
    "\n" +
    "    }</style> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <!--script type=\"text/javascript\" src=\"js/signature.js\"></script>\r" +
    "\n" +
    "<script type=\"text/javascript\" src=\"js/signature_pad.js\"></script--> <!-- <div class=\"formHeader\">\r" +
    "\n" +
    "    <div id=\"addMode\">\r" +
    "\n" +
    "        <button id=\"addBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; bottom: 10px;\">Tambah </button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div id=\"editMode\">\r" +
    "\n" +
    "        <button id=\"saveBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; bottom: 10px;\">Simpan </button>\r" +
    "\n" +
    "        <button id=\"cancelBtn\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; bottom: 10px;\">Batal </button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <!-- edited by K\r" +
    "\n" +
    "    <div class=\"row filtersearch\" style=\"margin-right:0px;padding-bottom:5px; width:100%;\">\r" +
    "\n" +
    "        <div class=\"col-md-12\" style=\"padding-right:0px;\">\r" +
    "\n" +
    "            <div class=\"input-group\">\r" +
    "\n" +
    "                <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control ng-pristine ng-valid ng-empty ng-touched\" placeholder=\"Filter\" ng-model=\"gridApi.grid.columns[filterColIdx].filters[0].term\" style=\"\">\r" +
    "\n" +
    "                <div class=\"input-group-btn dropdown\" uib-dropdown=\"\" style=\"\">\r" +
    "\n" +
    "                    <button id=\"filter\" type=\"button\" class=\"btn wbtn ng-binding\" uib-dropdown-toggle=\"\" data-toggle=\"dropdown\" tabindex=\"-1\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-search\" aria-hidden=\"true\"></i>\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <ul class=\"nav nav-tabchild\" style=\"margin-top:10px\"> <li class=\"active\"><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('menuSigning')\">Signing</a></li> <!--<li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('menuProspect');\">Prospect</a></li>\r" +
    "\n" +
    "        <li><a data-toggle=\"tab\" href=\"#\" onclick=\"navigateTab('menuPelanggan');\">Pelanggan</a></li>--> </ul> <div id=\"tabContainer\" class=\"tab-content\"> <div id=\"menuSigning\" class=\"tab-pane fade in active\"> <h3>Customer Signing</h3> <p>Tab Signing</p> <div> <!--p>My first{{boundingBox.width}} expression: {{ 5 + 5 + Hasil.satu}} </p--> <div class=\"signature_container\" ng-style=\"{'max-width': boundingBox.width + 'px', 'max-height': boundingBox.height + 'px'}\"> <signature-pad accept=\"accept\" clear=\"clear\" height=\"220\" width=\"568\" dataurl=\"dataurl\"></signature-pad> <!--<div class=\"buttons\">\r" +
    "\n" +
    "                        <button ng-click=\"clear()\">Clear</button>\r" +
    "\n" +
    "                        <button ng-click=\"dataurl = signature.dataUrl\" ng-disabled=\"!signature\">Reset</button>\r" +
    "\n" +
    "                        <button ng-click=\"signature = accept()\">Sign</button>\r" +
    "\n" +
    "                    </div>--> <!--<div class=\"sizes\">\r" +
    "\n" +
    "                        <input type=\"text\" ng-model=\"boundingBox.width\"> x <input type=\"text\" ng-model=\"boundingBox.height\">\r" +
    "\n" +
    "                    </div>--> </div> <!--<div class=\"result img-responsive\" ng-show=\"signature\">\r" +
    "\n" +
    "                    <img class=\"img-responsive\" ng-src=\"{{ signature.dataUrl }}\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"signature_container\">\r" +
    "\n" +
    "                    <p style=\"word-wrap: break-word;\">Image Data (Copy This string as result) <br /> {{signature.dataUrl}}</p>\r" +
    "\n" +
    "                </div>--> </div> </div> </div>"
  );

}]);
