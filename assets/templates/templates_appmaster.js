angular.module('templates_appmaster', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/master/AfterSales/CounterBP/masterCounterBP.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <style type=\"text/css\">.bs-navbar {\r" +
    "\n" +
    "      background-color: #fff;\r" +
    "\n" +
    "      margin: 0;\r" +
    "\n" +
    "      margin-top: 0px;\r" +
    "\n" +
    "      margin-right: 0px;\r" +
    "\n" +
    "      margin-bottom: 0px;\r" +
    "\n" +
    "      margin-left: 0px;\r" +
    "\n" +
    "      padding: 7px 7px 220px 7px;\r" +
    "\n" +
    "      padding-top: 7px;\r" +
    "\n" +
    "      padding-right: 7px;\r" +
    "\n" +
    "      padding-bottom: 220px;\r" +
    "\n" +
    "      padding-left: 7px;\r" +
    "\n" +
    "  }</style> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div ng-show=\"Show_MasterCounterBP_Tambah\" ng-form=\"AddCounter\" novalidate> <div class=\"row\"> <div class=\"col-md-12\"> <button ng-hide=\"MasterCounterBP_CounterId != 0\" id=\"MasterCounterBPTambah_Simpan_Button\" ng-disabled=\"AddCounter.$invalid\" ng-click=\"MasterCounterBPTambah_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button ng-hide=\"MasterCounterBP_CounterId == 0\" id=\"MasterCounterBPTambah_Update_Button\" ng-disabled=\"AddCounter.$invalid\" ng-click=\"MasterCounterBPTambah_Update_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Update </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MasterCounterBPTambah_Batal_Button\" ng-click=\"MasterCounterBPTambah_Batal_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <bsreqlabel>Nama Counter</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NamaCounterBP\" placeholder=\"Nama Counter\" ng-model=\"masterCounterBP_CounterName\" ng-maxlength=\"50\" ng-keypress=\"allowPatternFilter($event)\" required> <em ng-messages=\"AddCounter.NamaCounterBP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em></div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <bsreqlabel>Nama SA</bsreqlabel> <bsselect name=\"NamaSACounterBP\" ng-model=\"TambahCounterBP_SAName\" on-select=\"TambahCounterBP_SAName_Changed(selected)\" data=\"optionsTambahCounterBPSAName\" item-text=\"Name\" item-value=\"EmployeeId\" placeholder=\"Nama SA\" icon=\"fa fa-search\" required> </bsselect> <em ng-messages=\"AddCounter.NamaSACounterBP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em></div> <!--<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\t<bsreqlabel>Nama SA</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"NamaSA\" placeholder=\"Nama SA\" ng-model=\"masterCounterBP_SAName\" ng-maxlength=\"50\" required ng-blur=\"masterCounterBP_SAName_LostFocus()\">\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"MasterCounterBPTambah_CariSA_Button\" ng-click=\"MasterCounterBPTambah_CariSA_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tCari\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>--> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <bsreqlabel>Nama Inisial SA</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NamaInisialSA\" placeholder=\"Nama Inisial SA\" ng-model=\"masterCounterBP_InitialSAName\" ng-maxlength=\"50\" ng-disabled=\"!masterCounterBP_InitialSAName_isDisabled\"> <!-- <input type=\"text\" class=\"form-control\" name=\"NamaInisialSA\" placeholder=\"Nama Inisial SA\" ng-model=\"masterCounterBP_InitialSAName\" ng-maxlength=\"50\" required ng-disabled=\"!masterCounterBP_InitialSAName_isDisabled\"> --> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top:15px\"> <div class=\"col-md-4\"> <bsreqlabel>Kategori Antrian</bsreqlabel> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-8\"> <form> <div class=\"row\"> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterBP_isBooking\">Booking </div> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterBP_isWalkIn\">Walk In </div> <!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "                                <input type=\"checkbox\" ng-model=\"masterCounterBP_isBookingOnTime\">Booking On Time\r" +
    "\n" +
    "                            </div> --> </div> <div class=\"row\"> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterBP_isOther\">Others </div> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterBP_isTakeOut\">Pengambilan </div> </div> </form> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                         <div class=\"col-xs-2\" ng-repeat=\"category in CounterCatTemp\" index='$index'>\r" +
    "\n" +
    "                            <bscheckbox ng-model=\"nData[$index][category.Name]\" label=\"{{category.Name}}\" ng-change=\"checkCategory([$index],category.QCategoryId)\">\r" +
    "\n" +
    "                                <bscheckbox ng-model=\"CounterCatTemp[$index].Checked\" label=\"{{category.Name}}\" true-value='1' false-value='0' ng-change=\"checkCategory(CounterCatTemp[$index].Checked)\">\r" +
    "\n" +
    "                            </bscheckbox>\r" +
    "\n" +
    "                        </div> \r" +
    "\n" +
    "                        <div class=\"col-xs-2\">\r" +
    "\n" +
    "                            <bscheckbox ng-model=\"nData.WalkIn\" label=\"Walk In\"  true-value=1 false-value=0>\r" +
    "\n" +
    "                            </bscheckbox>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"col-xs-2\">\r" +
    "\n" +
    "                            <bscheckbox ng-model=\"nData.Other\" label=\"Other\"  true-value=1 false-value=0>\r" +
    "\n" +
    "                            </bscheckbox>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"col-xs-2\">\r" +
    "\n" +
    "                            <bscheckbox ng-model=\"nData.Pengambilan\" label=\"Pengambilan\"  true-value=1 false-value=0>\r" +
    "\n" +
    "                            </bscheckbox>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div> --> <br> <br> <div class=\"row\"></div> <div class=\"col-md-12\"> <div>Status Aktif</div> <div class=\"ui toggle checkbox\" style=\"margin:-5px 0 0\"> <input type=\"checkbox\" ng-model=\"masterCounterBP_StatusCode\" ng-true-value=\"1\" ng-false-value=\"0\" name=\"status\"> <label> <span style=\"color:#2185D0\" ng-if=\"masterCounterBP_StatusCode == 1\">Active</span> <span style=\"color:gray\" ng-if=\"masterCounterBP_StatusCode == 0\">Inactive</span> </label> </div> </div> <!-- </div> --> </div> </div> </div> <div ng-show=\"Show_MasterCounterBP_Main\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"MasterCounterBPMain_Tambah_Button\" ng-click=\"MasterCounterBPMain_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-4\" style=\"position: relative; left: 20px\"> <div class=\"input-group\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-change=\"test(GridAPICounterBPGrid.grid.columns[filterColIdx].filters[0].term)\" ng-model=\"GridAPICounterBPGrid.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\"></i> </span> </a> </li> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"CounterBPGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> </div> <div class=\"ui modal\" id=\"ModalHapusMasterCounterBP\" role=\"dialog\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Konfirmasi Hapus</h4> </div> <div class=\"modal-body\"> <p> Apakah anda yakin ingin menghapus Counter {{MainCounterBPHapus_CounterName}} ? </p> </div> <div class=\"modal-footer\"> <p align=\"center\"> <button ng-click=\"Batal_MainCounterBP()\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">Tidak</button> <button ng-click=\"Hapus_MainCounterBP()\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">Ya</button> </p> </div> </div> </div> <div class=\"ui modal\" id=\"ModalCounterBPTambahSearchEmployee\" role=\"dialog\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <bsreqlabel>Cari Employee</bsreqlabel> <div class=\"form-group\" ui-grid=\"ModalCounterBPTambahSearchEmployee_UIGrid\" ui-grid-auto-resize ui-grid-selection ui-grid-pagination></div> <p align=\"center\"> <button class=\"col-sm-6\" ng-click=\"ModalCounterBPTambahSearchEmployee_Batal()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> Batal </button> </p> </ul> </div> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/CounterGR/masterCounterGR.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div ng-show=\"Show_MasterCounterGR_Tambah\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"MasterCounterGRTambah_Simpan_Button\" ng-click=\"MasterCounterGRTambah_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MasterCounterGRTambah_Batal_Button\" ng-click=\"MasterCounterGRTambah_Batal_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-4\"> <bsreqlabel>Nama Counter</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NamaCounterGR\" placeholder=\"Nama Counter\" ng-model=\"masterCounterGR_CounterName\" ng-maxlength=\"50\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-4\"> <bsreqlabel>Nama SA</bsreqlabel> <bsselect ng-model=\"TambahCounterGR_SAName\" ng-change=\"TambahCounterGR_SAName_Changed()\" data=\"optionsTambahCounterGRSAName\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Nama SA\" icon=\"fa fa-search\"> </bsselect> </div> <!--<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\t<bsreqlabel>Nama SA</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"NamaSA\" placeholder=\"Nama SA\" ng-model=\"masterCounterGR_SAName\" ng-maxlength=\"50\" required ng-blur=\"masterCounterGR_SAName_LostFocus()\">\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"MasterCounterGRTambah_CariSA_Button\" ng-click=\"MasterCounterGRTambah_CariSA_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tCari\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>--> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-4\"> <bsreqlabel>Nama Inisial SA</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NamaInisialSA\" placeholder=\"Nama Inisial SA\" ng-model=\"masterCounterGR_InitialSAName\" ng-maxlength=\"50\" disabled required skip-enabled> <!-- ng-disabled= \"!masterCounterGR_InitialSAName_isDisabled\" --> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top:15px\"> <div class=\"col-md-4\"> <bsreqlabel>Kategori Antrian</bsreqlabel> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <form> <div class=\"row\"> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterGR_isEM\">EM </div> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterGR_isOther\">Other </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterGR_isSBI\">EM On Time </div> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterGR_isWalkIn\">Walk In </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterGR_isSBE\">Booking On Time </div> <div class=\"col-md-4\"> <input type=\"checkbox\" ng-model=\"masterCounterGR_isBooking\">Booking </div> </div> </form> </div> </div> <div class=\"col-md-12\" style=\"margin-top:15px\"> <div class=\"col-md-4\"> <div class=\"ui toggle checkbox\" style=\"margin:-5px 0 0\"> <input type=\"checkbox\" ng-model=\"masterCounterGR_StatusCode\" ng-true-value=\"1\" ng-false-value=\"0\" name=\"status\"> <label> <span style=\"color:#2185D0\" ng-if=\"row.entity.status == 1\">Active</span> <span style=\"color:gray\" ng-if=\"row.entity.status == 0\">Inactive</span> </label> </div> </div> </div> </div> </div> <div ng-show=\"Show_MasterCounterGR_Main\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"MasterCounterGRMain_Tambah_Button\" ng-click=\"MasterCounterGRMain_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"CounterGRGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> </div> <div class=\"ui modal\" id=\"ModalHapusMasterCounterGR\" role=\"dialog\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Konfirmasi Hapus</h4> </div> <div class=\"modal-body\"> <p> Apakah anda yakin ingin menghapus Counter {{MainCounterGRHapus_CounterName}} ? </p> </div> <div class=\"modal-footer\"> <p align=\"center\"> <button ng-click=\"Batal_MainCounterGR()\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">Tidak</button> <button ng-click=\"Hapus_MainCounterGR()\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">Ya</button> </p> </div> </div> </div> <div class=\"ui modal\" id=\"ModalCounterGRTambahSearchEmployee\" role=\"dialog\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <bsreqlabel>Cari Employee</bsreqlabel> <div class=\"form-group\" ui-grid=\"ModalCounterGRTambahSearchEmployee_UIGrid\" ui-grid-auto-resize ui-grid-selection ui-grid-pagination></div> <p align=\"center\"> <button class=\"col-sm-6\" ng-click=\"ModalCounterGRTambahSearchEmployee_Batal()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> Batal </button> </p> </ul> </div> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/MasterDiscount/MasterDiscount.html',
    "<link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    tr:nth-child(even) {\r" +
    "\n" +
    "        background-color: #dddddd;\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    fieldset.scheduler-border {\r" +
    "\n" +
    "        width: auto;\r" +
    "\n" +
    "        border: 1px groove #ffffff !important;\r" +
    "\n" +
    "        padding: 0 1.4em 1.4em 1.4em !important;\r" +
    "\n" +
    "        margin: 0 0 1.5em 0 !important;\r" +
    "\n" +
    "        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;\r" +
    "\n" +
    "        box-shadow: 0px 0px 0px 0px #ffffff;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    fieldset{\r" +
    "\n" +
    "        background-color: #ffffff ;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    legend.scheduler-border {\r" +
    "\n" +
    "        width: auto;\r" +
    "\n" +
    "        /* Or auto */\r" +
    "\n" +
    "        padding: 0 10px;\r" +
    "\n" +
    "        /* To give a bit of padding on the left and right */\r" +
    "\n" +
    "        border-bottom: none;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"DiskonForm\" factory-name=\"MasterDiscountFactory\" model=\"mDiscount\" model-id=\"AppointmentDiscountId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-before-new=\"actNew\" on-before-edit=\"actEdit\" on-show-detail=\"actView\" form-name=\"DiskonForm\" form-title=\"Diskon\" modal-title=\"Master Diskon\" modal-size=\"small\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" icon=\"fa fa-fw fa-child\" o n-validate-save=\"onValidateSave\"> <bsform-above-grid> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Data Filter</legend> <div class=\"row\"> <div class=\"col-md-10\"> <div class=\"input-icon-right\"> <label>Non-Aktif / Aktif</label> <div class=\"switch\"> <input id=\"cmn-toggle-7\" class=\"cmn-toggle cmn-toggle-round-flat\" ng-change=\"showAllData(toggleShowData);\" ng-model=\"toggleShowData\" type=\"checkbox\"> <label for=\"cmn-toggle-7\">Aktif</label> </div> </div> </div> <!-- search button --> <div class=\"col-md-2\"> <!-- <button class=\"btn wbtn pull-right\" ng-click=\"getData()\" onclick=\"this.blur()\"\r" +
    "\n" +
    "                    style=\"margin:20px -9px 0 0;\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\"\r" +
    "\n" +
    "                    tooltip-animation=\"false\" uib-tooltip=\"Search\">\r" +
    "\n" +
    "                    <i class=\"large icons\">\r" +
    "\n" +
    "                        <i class=\"database icon\"></i>\r" +
    "\n" +
    "                        <i class=\"inverted corner filter icon\"></i>\r" +
    "\n" +
    "                    </i>Search</button> --> </div> </div> </fieldset> </bsform-above-grid> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kategori Diskon</bsreqlabel> <bsselect style=\"margin-bottom: 10px\" ng-model=\"mDiscount.Category\" ng-disabled=\"ONmode =='view' || ONmode =='edit'\" data=\"DiscountCategory\" item-text=\"CategoryName\" item-value=\"Category\" skip-enabled placeholder=\"Pilih Kategori Diskon\" icon=\"fa fa-search\" required> </bsselect> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Diskon (%)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input onkeypress=\"return event.charCode >= 46 && event.charCode <= 57\" type=\"number\" name=\"Discount\" class=\"form-control\" ng-change=\"onTypeDiscount(mDiscount.Discount);onToggleSwitch(mDiscount.Discount)\" placeholder=\"Input Diskon dalam Satuan Persen\" ng-model=\"mDiscount.Discount\" maxlength=\"5\" required ng-disabled=\"mode =='view'\"> </div> <div ng-if=\"mDiscount.Discount >=101\"><input type=\"hidden\" ng-model=\"fakeModel\" required><p style=\"color: red; font-size: 12px; margin-top:7px\"><i>Discount tidak boleh melebihi 100 </i></p></div> <!-- <div ng-if=\"mDiscount.Discount == 0\"><input  type=\"hidden\" ng-model=\"fakeModel\" required><p style=\"color: red; font-size: 12px; margin-top:7px\" ><i>Discount tidak boleh nol </i></p></div>  --> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Outlet ID</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"OutletId\" placeholder=\"Input Outlet ID\"\r" +
    "\n" +
    "                        ng-model=\"mDiscount.OutletId\" maxlength=\"15\" alpha-numeric mask=\"Huruf\" required>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"OutletId\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Mulai</bsreqlabel> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"startDate\" date-options=\"DateOptionsASB\" ng-model=\"mDiscount.DateFrom\" min-date=\"minDateASB\" skip-enabled required ng-disabled=\"ONmode == 'view' || ONmode == 'edit'\"></bsdatepicker> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Berakhir</bsreqlabel> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"endDate\" date-options=\"DateOptionsASB\" ng-model=\"mDiscount.DateTo\" min-date=\"minDateASB\" skip-enabled required ng-disabled=\"ONmode == 'view' || ONmode == 'edit'\"></bsdatepicker> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"input-icon-right\"> <label>Non-Aktif / Aktif</label> <div class=\"switch\"> <input id=\"cmn-toggle-6\" class=\"cmn-toggle cmn-toggle-round-flat\" ng-model=\"mDiscount.IsActive\" ng-change=\"onSwitchActive(mDiscount.IsActive)\" type=\"checkbox\"> <label for=\"cmn-toggle-6\">Aktif</label> </div> </div> <div ng-if=\"mode == 'add' && mDiscount.IsActive == false\"><input type=\"hidden\" ng-model=\"fakeModel\" required><p style=\"color: red; font-size: 12px; margin-top:7px\"><i>Status Harus Aktif </i></p></div> <!-- ini kosongan aja biar pada geser ke bawah --> <div style=\"height: 500px\"></div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/AfterSales/MasterPRR/MasterPRR.html',
    "<link rel=\"stylesheet\" href=\"css/uitogglestyle.css\"> <link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\"></script> <style type=\"text/css\">@media only screen and (max-width: 600px) {\r" +
    "\n" +
    "        .nav.nav-tabs {\r" +
    "\n" +
    "            display: none;\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    tr:nth-child(even) {\r" +
    "\n" +
    "        background-color: #dddddd;\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    fieldset.scheduler-border {\r" +
    "\n" +
    "        width: auto;\r" +
    "\n" +
    "        border: 1px groove #ffffff !important;\r" +
    "\n" +
    "        padding: 0 1.4em 1.4em 1.4em !important;\r" +
    "\n" +
    "        margin: 0 0 1.5em 0 !important;\r" +
    "\n" +
    "        -webkit-box-shadow: 0px 0px 0px 0px #ffffff;\r" +
    "\n" +
    "        box-shadow: 0px 0px 0px 0px #ffffff;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t.containerAtasGrid{\r" +
    "\n" +
    "\t\t/* border: 1px solid black; */\r" +
    "\n" +
    "\t\theight:37px;\r" +
    "\n" +
    "\t}\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    fieldset {\r" +
    "\n" +
    "        background-color: #ffffff;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    legend.scheduler-border {\r" +
    "\n" +
    "        width: auto;\r" +
    "\n" +
    "        padding: 0 10px;\r" +
    "\n" +
    "        border-bottom: none;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .border-medium {\r" +
    "\n" +
    "        border-width: 2px;\r" +
    "\n" +
    "        /* border-style: solid; */\r" +
    "\n" +
    "        border-radius: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    /*.ag-tool-panel{\r" +
    "\n" +
    "        width: 0px !important;\r" +
    "\n" +
    "    }*/\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .border-thin {\r" +
    "\n" +
    "        border-width: 1px;\r" +
    "\n" +
    "        border-style: solid;\r" +
    "\n" +
    "        border-radius: 3px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-area-field-label {\r" +
    "\n" +
    "        font-size: 11px;\r" +
    "\n" +
    "        background-color: floralwhite;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "        position: relative;\r" +
    "\n" +
    "        top: -20px;\r" +
    "\n" +
    "        left: -40px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-areas-overlay {\r" +
    "\n" +
    "        /*background-color: #ccc;*/\r" +
    "\n" +
    "        background-color: #fff;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-areas-outline {\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        padding: 2px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .choosen-area {\r" +
    "\n" +
    "        /*background-color: red;*/\r" +
    "\n" +
    "        border: 2px solid red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-areas-resize-handler {\r" +
    "\n" +
    "        border: 2px #fff solid;\r" +
    "\n" +
    "        height: 10px;\r" +
    "\n" +
    "        width: 10px;\r" +
    "\n" +
    "        overflow: hidden;\r" +
    "\n" +
    "        background-color: #000;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .select-areas-delete-area {\r" +
    "\n" +
    "        background: url('../images/bt-delete.png');\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        height: 20px;\r" +
    "\n" +
    "        width: 20px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .delete-area {\r" +
    "\n" +
    "        position: absolute;\r" +
    "\n" +
    "        cursor: pointer;\r" +
    "\n" +
    "        padding: 1px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    .WACImageBorder {\r" +
    "\n" +
    "        width: 100%;\r" +
    "\n" +
    "        border: 1px solid grey;\r" +
    "\n" +
    "        margin: 0 auto;\r" +
    "\n" +
    "    }</style> <bsform-base ng-form=\"DiskonForm\" factory-name=\"MasterPRRFactory\" model=\"AEstimationBP\" model-id=\"mPRR\" loading=\"loading\" get-data=\"MasterSelling_PRR_Generate_Clicked\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" do-custom-save=\"doCustomSave\" hide-save-button=\"true\" hide-cancel-button=\"true\" form-name=\"MasterPRRForm\" form-title=\"Diskon\" modal-title=\"Master Diskon\" modal-size=\"small\" form-api=\"formApi\" icon=\"fa fa-fw fa-child\" show-advsearch=\"on\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Nama Outlet</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" placeholder=\"Outlet Name\" ng-model=\"FilterPrr.OutletCode\" style=\"text-transform: uppercase\" required> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Tanggal Mulai</label> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"TanggalFilterStart\" date-options=\"dateOptions\" ng-model=\"FilterPrr.TanggalFilterStart\"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <label>Tanggal Berakhir</label> <div class=\"input-icon-right form-group\"> <bsdatepicker name=\"TanggalFilterEnd\" date-options=\"dateOptions\" ng-model=\"FilterPrr.TanggalFilterEnd\"> </bsdatepicker> </div> </div> </div> </div> </bsform-advsearch> <bsform-base-main> <fieldset class=\"scheduler-border\" style=\"margin-top: 10px\"> <legend style=\"background:white\" class=\"scheduler-border\">Upload File</legend> <div class=\"col-md-6\"> <input type=\"file\" style=\"width: 90%\" accept=\".xlsx\" id=\"uploadSellingFile_PRR\" onchange=\"angular.element(this).scope().loadXLS(this)\"> </div> <div class=\"col-md-6\"> <button id=\"MasterSelling_Simpan_Button\" ng-click=\"MasterSelling_Simpan_Clicked()\" type=\"button\" skip-enable ng-disabled=\"isDisable == true\" class=\"rbtn btn ng-binding ladda-button pull-right\" style=\"width:101px\"> Simpan </button> <button id=\"MasterSelling_Batal_Button\" ng-click=\"MasterSelling_Batal_Clicked()\" type=\"button\" class=\"wbtn btn ng-binding ladda-button pull-right\" style=\"width:101px\"> Batal </button> </div> </fieldset> <div class=\"containerAtasGrid row\"> <div class=\"col-md-2\"> <bsselect ng-model=\"ComboBulkAction_PRR\" data=\"optionsComboBulkAction_PRR\" ng-disabled=\"true\" skip-enable ng-change=\"ComboBulkAction_PRR_Changed()\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" icon=\"fa fa-search\" placeholder=\"Bulk Action\"> </bsselect> </div> <div class=\"col-md-2\"> <div class=\"form-group filterMarg\"> <input type=\"text\" required ng-model=\"textFilterMasterPRR\" class=\"form-control\" placeholder=\"Search\" maxlength=\"100\" ng-change=\"FilterGridMasterPRR(textFilterMasterPRR,selectedGridFilter)\"> </div> </div> <div class=\"col-md-2\"> <bsselect ng-model=\"selectedGridFilter\" data=\"optionsComboFilterGrid_PRR\" ng-change=\"onFilterGridSelected(selectedGridFilter)\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" icon=\"fa fa-search\" placeholder=\"Bulk Action\"> </bsselect> </div> <div class=\"col-md-2\"> &nbsp; </div> <div class=\"col-md-4\"> <button id=\"MasterSelling_PRR_Download_Button\" ng-click=\"MasterSelling_PRR_Download_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button pull-right\"> <span class=\"ladda-label\"> Download Template </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div id=\"myGrid\" ui-grid=\"grid\" ui-grid-edit ui-grid-selection ui-grid-resize-columns ui-grid-auto-resize ui-grid-pagination class=\"Mygrid\" style=\"width:100%;height: 250px; margin-top:10px; margin-bottom:20px\"> <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </bsform-base-main> </bsform-base>"
  );


  $templateCache.put('app/master/AfterSales/asuransiRekanan/asuransiRekanan.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <bsform-grid ng-form=\"AsuransiRekananForm\" factory-name=\"AsuransiRekananFactory\" model=\"mAsuransiRekanan\" model-id=\"InsuranceId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"AsuransiRekananForm\" form-title=\"AsuransiRekanan\" modal-title=\"AsuransiRekanan\" do-custom-save=\"doCustomSave\" form-api=\"formApi\" grid-custom-action-button-template=\"gridActionTemplate\" grid-hide-action-column=\"true\" grid-custom-action-button-colwidth=\"170\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Asuransi Rekanan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <!-- <input type=\"text\" class=\"form-control\" name=\"NamaAsuransiRekanan\" ng-keypress=\"allowPatternFilter($event)\" placeholder=\"Nama Asuransi Rekanan\" ng-model=\"mAsuransiRekanan.InsuranceName\" ng-maxlength=\"50\" required> --> <input type=\"text\" class=\"form-control\" name=\"NamaAsuransiRekanan\" placeholder=\"Nama Asuransi Rekanan\" ng-model=\"mAsuransiRekanan.InsuranceName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"InsuranceName\"></bserrmsg> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat Penagihan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <!-- <input type=\"text\" class=\"form-control\" name=\"AlamatPenagihan\" ng-keypress=\"allowPatternFilter2($event)\" placeholder=\"Alamat Penagihan\" ng-model=\"mAsuransiRekanan.BillAddress\" ng-maxlength=\"500\" required> --> <input type=\"text\" class=\"form-control\" name=\"AlamatPenagihan\" placeholder=\"Alamat Penagihan\" ng-model=\"mAsuransiRekanan.BillAddress\" ng-maxlength=\"500\" required> </div> <bserrmsg field=\"BillAddress\"></bserrmsg> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>NPWP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"NPWP\" ng-keypress=\"allowPatternFilter3($event)\" placeholder=\"NPWP\" ng-model=\"mAsuransiRekanan.NPWP\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" required> </div> <bserrmsg field=\"NPWP\"></bserrmsg> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Diskon Part (%)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"DiscountPart\" placeholder=\"Diskon Part\" ng-model=\"mAsuransiRekanan.DiscountPart\" max=\"100\" min=\"0\" ng-maxlength=\"4\" step=\"0.1\" required> </div> <bserrmsg field=\"DiscountPart\"></bserrmsg> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Diskon Jasa (%)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"DiscountJasa\" placeholder=\"Diskon Jasa\" ng-model=\"mAsuransiRekanan.DiscountJasa\" max=\"100\" min=\"0\" ng-maxlength=\"4\" step=\"0.1\" required> </div> <bserrmsg field=\"DiscountJasa\"></bserrmsg> </div> </div> </div> </div></bsform-grid> </div>"
  );


  $templateCache.put('app/master/AfterSales/bpCenterSatelite/bpCenterSatelite.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div class=\"row\" ng-show=\"MainBPCenterSatelite_Show\"> <div class=\"col-md-12\"> <div class=\"col-md-1\"> Dealer : </div> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"MainBPCenterSatelliteName\" placeholder=\"\" ng-model=\"MainBPCenterSatellite_Name\" ng-maxlength=\"50\" ng-disabled=\"!MainBPCenterSatelite_EnableDisable\"> </div> </div> <!--<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t<bsselect ng-model=\"MainBPCenterSatelite_Dealer\"\r" +
    "\n" +
    "\t\t\t\t\t data=\"optionsMainBPCenterSatelite_Dealer\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Bulk Action\"\r" +
    "\n" +
    "\t\t\t\t\t ng-change=\"MainBPCenterSatelite_Dealer_Changed()\" icon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t</div>--> </div> <div class=\"col-md-12\"> <button id=\"MainBPCenterSatelite_Upload_Button\" ng-click=\"MainBPCenterSatelite_DownloadExcelTemplate()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Download Template </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <button id=\"MainBPCenterSatelite_Upload_Button\" ng-click=\"MainBPCenterSatelite_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Upload dari Excel </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <button id=\"MainBPCenterSatelite_Tambah_Button\" ng-click=\"MainBPCenterSatelite_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div id=\"myGrid\" ui-grid=\"BPCenter_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:98%;height: 250px\"></div> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div id=\"myGrid\" ui-grid=\"BPSatelite_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:98%;height: 250px\"></div> </div> </div> <div class=\"row\" ng-show=\"TambahBPCenterSatelite_Center_Show\"> <div class=\"col-md-12\"> <button id=\"TambahBPCenterSatelite_Center_Simpan_Button\" ng-click=\"TambahBPCenterSatelite_Center_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahBPCenterSatelite_Center_Kembali_Button\" ng-click=\"TambahBPCenterSatelite_Center_Kembali_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> Dealer : {{TambahBPCenterSatelite_Center_DealerName}} </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-1\"> BP Center </div> <div class=\"col-md-3\"> <bsselect ng-model=\"TambahBPCenterSatelite_Center_BPCenter\" data=\"optionsTambahBPCenterSatelite_Center_BPCenter\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Pilih BP Center\" ng-change=\"TambahBPCenterSatelite_Center_BPCenter_Changed(TambahBPCenterSatelite_Center_BPCenter)\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-1\"> Alamat </div> <div class=\"col-md-3\"> <textarea type=\"text\" class=\"form-control\" name=\"TambahBPCenterSateliteCenterAlamat\" placeholder=\"\" ng-model=\"TambahBPCenterSatelite_Center_Alamat\" ng-maxlength=\"300\" style=\"margin-bottom: 10px\" ng-disabled=\"!TambahBPCenterSatelite_Center_Alamat_isDisabled\">\r" +
    "\n" +
    "\t\t\t\t</textarea> </div> </div> </div> <div class=\"row\" ng-show=\"TambahBPCenterSatelite_Satelite_Show\"> <div class=\"col-md-12\"> <button id=\"TambahBPCenterSatelite_Satelite_Simpan_Button\" ng-click=\"TambahBPCenterSatelite_Satelite_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\" ng-disabled=\"disableBtn\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahBPCenterSatelite_Satelite_Kembali_Button\" ng-click=\"TambahBPCenterSatelite_Satelite_Kembali_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-6\" ng-form=\"rowBPCenter\"> <div class=\"form-group row\"> <label class=\"col-sm-2 col-form-label\">BP Center</label> <div class=\"col-sm-10\"> {{TambahBPCenterSatelite_Satelite_CenterName}} </div> </div> <div class=\"form-group row\"> <label class=\"col-sm-2 col-form-label\">BP Satellite <span style=\"color:#b94a48\"> *</span></label> <div class=\"col-sm-10\"> <bsselect required name=\"sattelite\" name=\"satelite\" ng-model=\"TambahBPCenterSatelite_Satelite_BPSatelite\" data=\"optionsTambahBPCenterSatelite_Satelite_BPSatelite\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Pilih BP Satellite\" ng-change=\"TambahBPCenterSatelite_Satelite_BPSatelite_Changed(TambahBPCenterSatelite_Satelite_BPSatelite)\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"form-group row\"> <label class=\"col-sm-2 col-form-label\">Alamat</label> <div class=\"col-sm-10\"> <textarea type=\"text\" class=\"form-control\" name=\"TambahBPCenterSateliteSateliteAlamat\" placeholder=\"\" ng-model=\"TambahBPCenterSatelite_Satelite_Alamat\" ng-maxlength=\"300\" style=\"margin-bottom: 10px\" ng-disabled=\"!TambahBPCenterSatelite_Satelite_Alamat_isDisabled\">\r" +
    "\n" +
    "                    </textarea> </div> </div> <div class=\"form-group row\"> <label class=\"col-sm-2 col-form-label\"> SA Satellite <span style=\"color:#b94a48\"> *</span></label> <div class=\"col-sm-10\"> <bsselect required ng-model=\"TambahBPCenterSatelite_Satelite_SASatelite\" data=\"optionsTambahBPCenterSatelite_Satelite_SASatelite\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Pilih SA\" ng-change=\"TambahBPCenterSatelite_Satelite_SASatelite_Changed()\" icon=\"fa fa-search\" ng-disabled=\"disableSASatellite\"> </bsselect> </div> </div> </div> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "            <div class=\"col-md-1\">\r" +
    "\n" +
    "                BP Center\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-4\">\r" +
    "\n" +
    "                {{TambahBPCenterSatelite_Satelite_CenterName}}\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"col-md-12\">\r" +
    "\n" +
    "            <div class=\"col-md-1\">\r" +
    "\n" +
    "                BP Satellite\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-4\">\r" +
    "\n" +
    "                <bsselect ng-model=\"TambahBPCenterSatelite_Satelite_BPSatelite\" data=\"optionsTambahBPCenterSatelite_Satelite_BPSatelite\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Bulk Action\" ng-change=\"TambahBPCenterSatelite_Satelite_BPSatelite_Changed(TambahBPCenterSatelite_Satelite_BPSatelite)\"\r" +
    "\n" +
    "                    icon=\"fa fa-search\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"col-md-12\">\r" +
    "\n" +
    "            <div class=\"col-md-1\">\r" +
    "\n" +
    "                Alamat\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-3\">\r" +
    "\n" +
    "                <textarea type=\"text\" class=\"form-control\" name=\"TambahBPCenterSateliteSateliteAlamat\" placeholder=\"\" ng-model=\"TambahBPCenterSatelite_Satelite_Alamat\" ng-maxlength=\"300\" style=\"margin-bottom: 10px\" ng-disabled=\"!TambahBPCenterSatelite_Satelite_Alamat_isDisabled\">\r" +
    "\n" +
    "\t\t\t\t</textarea>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"col-md-12\">\r" +
    "\n" +
    "            <div class=\"col-md-1\">\r" +
    "\n" +
    "                SA Satellite\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-4\">\r" +
    "\n" +
    "                <bsselect ng-model=\"TambahBPCenterSatelite_Satelite_SASatelite\" data=\"optionsTambahBPCenterSatelite_Satelite_SASatelite\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Bulk Action\" ng-change=\"TambahBPCenterSatelite_Satelite_SASatelite_Changed()\"\r" +
    "\n" +
    "                    icon=\"fa fa-search\" ng-disabled=\"disableSASatellite\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"col-md-2\">\r" +
    "\n" +
    "                <button id=\"TambahBPCenterSatelite_Satelite_Tambah_Button\" ng-click=\"TambahBPCenterSatelite_Satelite_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\">\r" +
    "\n" +
    "\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\tTambah\r" +
    "\n" +
    "\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t</button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div> --> <div class=\"col-md-12\"> <button id=\"TambahBPCenterSatelite_Satelite_Tambah_Button\" ng-disabled=\"rowBPCenter.$invalid\" ng-click=\"TambahBPCenterSatelite_Satelite_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px;margin-bottom:10px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div id=\"myGrid\" ui-grid=\"BPSateliteBaru_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:98%;height: 250px\"></div> </div> </div> </div> <div class=\"ui modal\" id=\"ModalUploadbpCenterSatelitte\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadbpCenterSatelitte\" onchange=\"angular.element(this).scope().loadXLSBpCenterSatellite(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"UploadBpCenterSatellite_Upload_Button\" ng-click=\"UploadMasterVendor_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div> <div class=\"ui modal\" id=\"ModalSatelliteBPDelete\" role=\"dialog\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <bslabel>Hapus Data</bslabel> <div class=\"form-group\"> Anda yakin ingin menghapus {{ModalSatelliteBPDelete_Data}} ? </div> <p align=\"center\"> <button class=\"col-sm-6\" ng-click=\"ModalSatelliteBPDelete_Ya(DeleteSatelliteData)\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> Ya </button> <button class=\"col-sm-6\" ng-click=\"ModalSatelliteBPDelete_Tidak()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> Tidak </button> </p> </ul> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/customerDiscount/customerDiscount.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <!-- custom-action-button-settings='actionButtonSettingsDetail' --> <bsform-grid ng-form=\"CustomerDiscount\" factory-name=\"CustomerDiscountFactory\" model=\"mData\" model-id=\"GroupCustomerId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-show-detail=\"onShowDetail\" on-validate-save=\"onValidateSave\" on-before-new-mode=\"onBeforeNewMode\" do-custom-save=\"doCustomSave\" grid-custom-action-button-template=\"gridActionTemplate\" grid-hide-action-column=\"true\" grid-custom-action-button-colwidth=\"120\" form-grid-api=\"formGridApi\" form-api=\"formApi\" action-button-caption=\"actionButtonCaption\" form-name=\"CustomerDiscountForm\" form-title=\"Customer Discount\" modal-title=\"Customer Discount\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div ng-form=\"rowfirstDiscount\" class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Group Customer</bsreqlabel> <input type=\"text\" required class=\"form-control\" name=\"TambahCustomerDiscountGroupCustomer \" placeholder=\"Nama Group Customer\" ng-model=\"mData.GroupCustomerName\" ng-maxlength=\"50\"> <em ng-messages=\"rowfirstDiscount.TambahCustomerDiscountGroupCustomer.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Kategori</bsreqlabel> <bsselect ng-model=\"mData.Category\" data=\"optionsTambahCustomerDiscount_GRBP\" name=\"kategori\" required item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Pilih Kategori\" icon=\"fa fa-search\"> </bsselect> <em ng-messages=\"rowfirstDiscount.kategori.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> </div> <div ng-form=\"diskonForm\" class=\"row\"> <div class=\"col-md-12\"> <fieldset style=\"border-top:0;border-color:#eee;padding:0;margin:0\"> <legend>Diskon</legend> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Jasa</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" step=\"0.01\" min=\"0\" class=\"form-control\" name=\"TambahCustomerDiscountJasa\" placeholder=\"\" ng-model=\"mData.DiscountJasa\" required max=\"100\"> <em ng-messages=\"diskonForm.TambahCustomerDiscountJasa.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"max\">Diskon maksimal 100%</p> <p ng-message=\"min\">Diskon minimal 0%</p> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Diskon kelipatan 0.01</p> </em> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Parts</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" step=\"0.01\" min=\"0\" class=\"form-control\" name=\"TambahCustomerDiscountPart\" placeholder=\"\" ng-model=\"mData.DiscountParts\" required max=\"100\"> <em ng-messages=\"diskonForm.TambahCustomerDiscountPart.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"max\">Diskon maksimal 100%</p> <p ng-message=\"min\">Diskon minimal 0%</p> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Diskon kelipatan 0.01</p> </em> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>OPL</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" step=\"0.01\" min=\"0\" class=\"form-control\" name=\"TambahCustomerDiscountOPL\" placeholder=\"\" ng-model=\"mData.DiscountOPL\" required max=\"100\"> <em ng-messages=\"diskonForm.TambahCustomerDiscountOPL.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"max\">Diskon maksimal 100%</p> <p ng-message=\"min\">Diskon minimal 0%</p> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Diskon kelipatan 0.01</p> </em> </div> </div> </div> </fieldset> <div class=\"col-md-12\"> <button id=\"TambahCustomerDiscount_Tambah_Button\" ng-click=\"tambahCustomerBaru()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:24px;margin-left:5px\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-plus\"></i> Tambah Customer </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahCustomerDiscount_Upload_Button\" ng-click=\"tambahUpload()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:24px;margin-left:5px\"> <span class=\"ladda-label\"> Upload Dari Excel </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahCustomerDiscount_Download_Button\" ng-click=\"TambahCustomerDiscount_Download_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:24px;margin-left:5px\"> <span class=\"ladda-label\"> Download Template </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"TambahCustomerGrid\" ui-grid-resize-columns ui-grid-auto-resize ui-grid-pagination class=\"Mygrid\" style=\"width:100%;height: 250px; margin-top:10px; margin-bottom:20px\"></div> </div> </div> </div> </bsform-grid> <bsui-modal show=\"showTambahCustomer\" title=\"Tambah Customer\" data=\"dataCustomer\" on-save=\"saveCustomer\" on-cancel=\"cancelTambahCustomer\" button-settings=\"listEstimationButton\" mode=\"modeModalCustomer\"> <div ng-form=\"dataCust\" novalidate> <div class=\"row\"> <div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <!-- <div class=\"row\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<div class=\"col-md-6\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<div class=\"form-group\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<bsreqlabel>Cari Customer</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<bstypeahead \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tplaceholder=\"Nama Pelanggan..\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tname=\"custName\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tng-model=\"dataCustomer.Customer\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tget-data=\"getCustomer\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\titem-text=\"TaskName\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tloading=\"loading\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tnoresult=\"noResults\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\tselected=\"selected\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\ton-select=\"onSelectCust\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\ton-noresult=\"onNoResult\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\ton-gotresult=\"onGotResult\"\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\ticon=\"fa-id-card-o\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\trequired>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</bstypeahead>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<em ng-messages=\"dataCust.custName.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t<p ng-message=\"required\">Wajib diisi</p>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t<div class=\"col-md-6\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<div class=\"form-group\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<bsreqlabel>Berlaku Sampai</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<bsdatepicker style= \"margin-bottom: 80px\" name=\"TambahCustomerDataBerlakuSampai\" date-options=\"dateOptions\" ng-model=\"dataCustomer.BerlakuSampai\" required >\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</bsdatepicker>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\t</div> --> <!-- <fieldset style=\"border-top:0;border-color:#eee;padding:0;margin:0;\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<legend>Data CUstomer</legend> --> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Pemilik</bsreqlabel> <div class=\"input-icon-right\"> <bstypeahead placeholder=\"Cari Nama Pelanggan..\" name=\"custName\" ng-model=\"dataCustomer.CustomerName\" get-data=\"getCustomer\" item-text=\"CustomerName\" loading=\"loadingCustomer\" noresult=\"noResults\" selected typeahead-min-length=\"3\" on-select=\"onSelectCust\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" icon=\"fa-user\" required> </bstypeahead> <input type=\"text\" ng-hide=\"true\" class=\"form-control\" name=\"TambahCustomerDataNamaPemilik\" placeholder=\"\" ng-model=\"dataCustomer.CustomerName\" ng-maxlength=\"50\" ng-disabled=\"TambahCustomerDiscount_View_Disable\" required> <!-- <i class=\"fa fa-user\"></i> --> <em ng-messages=\"dataCust.custName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"form-group\"> <label>Nomor Telepon</label> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"notelp\" placeholder=\"\" ng-model=\"dataCustomer.PhoneNumber\" maxlength=\"20\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> <i class=\"fa fa-phone\"></i> <em ng-messages=\"dataCust.notelp.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"form-group\"> <bsreqlabel>Tanggal Lahir</bsreqlabel> <div class=\"input-icon-right\"> <bsdatepicker required name=\"TambahCustomerDataTanggalLahir\" required date-options=\"dateOptions\" ng-model=\"dataCustomer.BirthDate\"> </bsdatepicker> <em ng-messages=\"dataCust.TambahCustomerDataTanggalLahir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <div class=\"form-group\"> <bsreqlabel>Nomor KTP</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahCustomerDataNomorKTP\" placeholder=\"\" numbers-only ng-model=\"dataCustomer.IdNumber\" ng-maxlength=\"16\" required> <i class=\"fa fa-id-card\"></i> <em ng-messages=\"dataCust.TambahCustomerDataNomorKTP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"form-group\"> <bsreqlabel>NPWP</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" minlength=\"20\" maxlength=\"20\" class=\"form-control\" name=\"TambahCustomerDataNomorNPWP\" placeholder=\"\" ng-model=\"dataCustomer.Npwp\" maskme=\"99.999.999.9-999.999\" style=\"text-transform: uppercase\" required> <i class=\"fa fa-id-card\"></i> <em ng-messages=\"dataCust.TambahCustomerDataNomorNPWP.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em> </div> </div> <div class=\"form-group\"> <label>Alamat Lengkap</label> <textarea type=\"text\" class=\"form-control\" name=\"TambahCustomerDataAlamat\" placeholder=\"\" ng-model=\"dataCustomer.Address\" ng-maxlength=\"250\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t</textarea> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Provinsi</label> <bsselect name=\"propinsi\" ng-model=\"dataCustomer.ProvinceId\" data=\"provinsiData\" item-text=\"ProvinceName\" item-value=\"ProvinceId\" placeholder=\"Pilih Provinsi...\" on-select=\"selectProvince(selected)\"> </bsselect> </div> <div class=\"form-group\"> <label>Kabupaten/Kota</label> <bsselect name=\"kabupaten\" ng-model=\"dataCustomer.CityRegencyId\" data=\"kabupatenData\" item-text=\"CityRegencyName\" item-value=\"CityRegencyId\" placeholder=\"Pilih Kabupaten/Kota...\" ng-disabled=\"dataCustomer.ProvinceId == undefined\" on-select=\"selectRegency(selected)\" ng-required=\"dataCustomer.ProvinceId !== undefined\"> </bsselect> <em ng-messages=\"dataCust.kabupaten.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <label>Kecamatan</label> <bsselect style=\"margin-bottom: 15px\" name=\"kecamatan\" ng-model=\"dataCustomer.DistrictId\" data=\"kecamatanData\" item-text=\"DistrictName\" item-value=\"DistrictId\" placeholder=\"Pilih Kecamatan...\" ng-disabled=\"dataCustomer.CityRegencyId == undefined\" on-select=\"selectDistrict(selected)\" ng-required=\"dataCustomer.ProvinceId !== undefined\"> </bsselect> <em ng-messages=\"dataCust.kecamatan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <label>Kelurahan</label> <bsselect style=\"margin-bottom: 15px\" name=\"kelurahan\" ng-model=\"dataCustomer.VillageId\" data=\"kelurahanData\" item-text=\"VillageName\" item-value=\"VillageId\" placeholder=\"Pilih Kelurahan...\" ng-disabled=\"dataCustomer.DistrictId == undefined\" on-select=\"selectVillage(selected)\" ng-required=\"dataCustomer.ProvinceId !== undefined\"> </bsselect> <em ng-messages=\"dataCust.kelurahan.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> <div class=\"form-group\"> <label>Kode Pos</label> <input type=\"text\" name=\"PostalCodeId\" class=\"form-control\" ng-model=\"dataCustomer.PostalCode\" disabled> </div> <div class=\"form-group\" style=\"margin-bottom: 80px\"> <bsreqlabel>Berlaku Sampai</bsreqlabel> <bsdatepicker name=\"TambahCustomerDataBerlakuSampai\" date-options=\"dateOptions\" ng-model=\"dataCustomer.ValidTo\" required> </bsdatepicker> <em ng-messages=\"dataCust.TambahCustomerDataBerlakuSampai.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em> </div> </div> <!-- </fieldset> --> </div> <!-- ============== --> </div> </div> </div> </bsui-modal> <bsui-modal show=\"showUploadCUstomer\" title=\"Upload File\" data=\"dataUpload\" on-save=\"uploadExcel\" on-cancel=\"cancelUpload\" button-settings=\"buttonSettingUpload\" mode=\"modeModalCustomer\"> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadMasterCustomerDiscount\" onchange=\"angular.element(this).scope().loadXLSMasterCustomerDiscount(this)\"> </div> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t<button id=\"UploadMasterCustomerDiscount_Upload_Button\"\r" +
    "\n" +
    "\t\tng-click=\"UploadMasterCustomerDiscount_Upload_Clicked()\"\r" +
    "\n" +
    "\t\t type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\">\r" +
    "\n" +
    "\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\tUpload\r" +
    "\n" +
    "\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t</button>\r" +
    "\n" +
    "\t</div> --> </bsui-modal>"
  );


  $templateCache.put('app/master/AfterSales/customerPKS/customerPKS.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <!-- MAIN MENU --> <div ng-show=\"CPKS_Main_Body_Show\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:45px\"> <!-- \r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\" style=\"margin-top:8px\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"FilterText\" placeholder=\"Filter\" ng-model=\"CPKS_MainFilterText\" ng-maxlength=\"150\">\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\" style=\"margin-top:8px\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect ng-model=\"CPKS_MainFilterColumn\"\r" +
    "\n" +
    "\t\t\t\t\t data=\"CPKS_MainFilterColumnList\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Semua Kolom\"\r" +
    "\n" +
    "\t\t\t\t\t icon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\" style=\"margin-top:8px\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"CPKS_MainFilterButton\" class=\"btn ng-binding ladda-button\" ng-click=\"CPKS_Main_Filter_Button_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\">\r" +
    "\n" +
    "\t\t\t\t\t\tCari\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div> --> <!-- <div class=\"col-md-5\" style=\"margin-top:8px\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"CPKS_Main_Add_Button\" ng-click=\"CPKS_Main_Add_Button_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-right:-65px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<i class=\"fa fa-fw fa-plus\"></i>Tambah Customer PKS\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div> --> <div class=\"pull-right\"> <div class=\"btn-group\" style=\"margin-top:-40px;margin-right:5px\"> <button class=\"ubtn\" ng-click=\"CPKS_Main_UIGrid_Paging(1)\" style=\"padding-bottom:6px\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" tabindex=\"0\"><i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em;margin-top:20px\"></i></button> <button id=\"Add\" ng-click=\"CPKS_Main_Add_Button_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:20px\"> <span class=\"ladda-label\"> <span class=\"fa fa-fw fa-plus\"></span>Tambah Customer PKS </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div class=\"col-md-2\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"FilterText\" placeholder=\"Filter\" ng-model=\"CPKS_MainFilterText\" ng-maxlength=\"150\"> </div> </div> <div class=\"col-md-2\" style=\"margin-top:8px\"> <bsselect ng-model=\"CPKS_MainFilterColumn\" data=\"CPKS_MainFilterColumnList\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Semua Kolom\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-2\" style=\"margin-top:8px\"> <button id=\"CPKS_MainFilterButton\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"CPKS_Main_Filter_Button_Clicked()\" type=\"button\"> Cari </button> </div> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"CPKS_Main_UIGrid\" ui-grid-auto-resize ui-grid-auto-resize ui-grid-resize-columns ui-grid-pagination ui-grid-selection class=\"Mygrid\" ng-style=\"gridPKSTableHeight()\"> <!-- style=\"width:95%;height: 250px; margin-bottom:20px; margin-left:13px\" --> </div> </div> </div> </div> <!-- ADD MENU --> <div ng-show=\"CPKS_Add_Body_Show\" ng-form=\"revForm\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:5px\"> <div ng-disabled=\"CPKS_AddDisableGroupType\" class=\"col-md-1\" style=\"margin-top:8px\"> <bsreqlabel>Tipe Group</bsreqlabel> </div> <div class=\"col-md-2\" style=\"margin-top:8px\"> <bsselect ng-model=\"CPKS_AddGroupType\" ng-disabled=\"CPKS_AddDisableGroupType\" data=\"CPKS_AddGroupTypeOption\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" icon=\"fa fa-search\" ng-change=\"CPKS_AddGroupTypeChange()\"> </bsselect> </div> <div ng-show=\"CPKS_AddSaveCancel_Button_Show\" class=\"col-md-8\" style=\"margin-top:8px\"> <button id=\"CPKS_Add_Save_Button\" ng-click=\"CPKS_Add_Save_Button_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-right:5px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> </div> <div ng-show=\"CPKS_AddSaveCancel_Button_Show\" class=\"col-md-1\" style=\"margin-top:8px\"> <button id=\"CPKS_Add_Cancel_Button\" ng-click=\"CPKS_Add_Cancel_Button_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> <div ng-show=\"CPKS_AddBack_Button_Show\" class=\"col-md-8\" style=\"margin-top:8px\"> <button id=\"CPKS_Add_Back_Button\" ng-click=\"CPKS_Add_Cancel_Button_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:5px\"> <div class=\"col-md-1\" style=\"margin-top:8px\"> <bslabel>Toyota ID</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"ToyotaIDText\" placeholder=\"Toyota ID\" ng-model=\"CPKS_AddToyotaIDText\" ng-change=\"alphaNumericOnly(CPKS_AddToyotaIDText,'CPKS_AddToyotaIDText')\" maxlength=\"20\" ng-disabled=\"CPKS_AddDisableToyotaID\" required> </div> </div> <div class=\"col-md-1\" style=\"margin-top:8px\"> <button id=\"CPKS_AddFindToyotaIDButton\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"CPKS_Add_FindToyotaID_Button_Clicked()\" ng-disabled=\"CPKS_AddDisableToyotaID\" type=\"button\"> Cari </button> </div> <div class=\"col-md-2\" style=\"margin-top:8px\"> <bslabel>Contact Person</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"ContactPersonText\" placeholder=\"Contact Person\" ng-model=\"CPKS_AddContactPersonText\" maxlength=\"20\" ng-disabled=\"CPKS_AddDisableField\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:5px\"> <div ng-show=\"CPKS_Add_NoPKS_Show\"> <div class=\"col-md-1\" style=\"margin-top:8px\"> <bsreqlabel>No PKS</bsreqlabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"PKSNumText\" placeholder=\"\" ng-model=\"CPKS_AddPKSNumText\" ng-change=\"alphaNumericOnly(CPKS_AddPKSNumText,'CPKS_AddPKSNumText')\" maxlength=\"20\" ng-disabled=\"CPKS_AddDisableField\" required> </div> </div> </div> <div ng-show=\"CPKS_Add_NOAFCO_Show\"> <div class=\"col-md-1\" style=\"margin-top:8px\"> <bsreqlabel>No Afco</bsreqlabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"PKSNumText\" placeholder=\"\" ng-model=\"CPKS_AddPKSNumText\" ng-change=\"alphaNumericOnly(CPKS_AddPKSNumText,'CPKS_AddPKSNumText')\" ng-maxlength=\"150\" ng-disabled=\"CPKS_AddDisableField\" required> </div> </div> </div> <div ng-show=\"CPKS_Add_StartPeriod_Show\"> <div class=\"col-md-2\" style=\"margin-top:8px\"> <bsreqlabel>Periode Mulai</bsreqlabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"AddStartPeriod\" date-options=\"DateOptions\" ng-model=\"CPKS_AddStartPeriod\" ng-disabled=\"CPKS_AddDisableField\" required> </bsdatepicker> <em ng-messages=\"revForm.AddStartPeriod.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"date\">Can not fill letters</p> <p ng-message=\"minlength\">Jumlah karakter kurang</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:5px\"> <div class=\"col-md-1\" style=\"margin-top:8px\"> <bsreqlabel>Nama Perusahaan</bsreqlabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <!-- <input type=\"text\" class=\"form-control\" name=\"PKSNumText\" placeholder=\"\" ng-model=\"CPKS_AddCompanyNameText\" ng-maxlength=\"150\" ng-disabled=\"CPKS_AddDisableField\" required> --> <input type=\"text\" style=\"text-transform: uppercase\" class=\"form-control\" name=\"PKSNumText\" placeholder=\"\" ng-model=\"CPKS_AddCompanyNameText\" maxlength=\"30\" ng-disabled=\"disableNamaAlamat\" required> </div> </div> <div ng-show=\"CPKS_Add_EndPeriod_Show\"> <div class=\"col-md-2\" style=\"margin-top:8px\"> <bsreqlabel>Periode Berakhir</bsreqlabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"AddEndPeriod\" date-options=\"DateOptions\" ng-model=\"CPKS_AddEndPeriod\" ng-disabled=\"CPKS_AddDisableField\" required> </bsdatepicker> <em ng-messages=\"revForm.Tanggal1.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"date\">Can not fill letters</p> <p ng-message=\"minlength\">Jumlah karakter kurang</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:5px\"> <div class=\"col-md-1\" style=\"margin-top:8px\"> <bsreqlabel>Alamat Penagihan</bsreqlabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <!-- <textarea type=\"text\" class=\"form-control\" name=\"billingAddress\" placeholder=\"\" ng-model=\"CPKS_AddBillingAddress\" ng-maxlength=\"350\" style=\"resize:none;\" ng-disabled=\"CPKS_AddDisableField\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> --> <textarea type=\"text\" class=\"form-control\" name=\"billingAddress\" placeholder=\"\" ng-model=\"CPKS_AddBillingAddress\" maxlength=\"50\" style=\"resize:none\" ng-disabled=\"disableNamaAlamat\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> <div ng-show=\"CPKS_Add_PaymentMethod_Show\"> <div class=\"col-md-2\" style=\"margin-top:8px\"> <bsreqlabel>Metode Pembayaran</bsreqlabel> <label>:</label> </div> <div class=\"col-md-4\"> <form> <div class=\"radio\"> <label><input name=\"AddPaymentMethod\" ng-model=\"CPKS_AddPaymentMethod\" value=\"saldo\" type=\"radio\" ng-change=\"CPKS_Add_PaymentMethod_Radio_Changed()\" ng-disabled=\"CPKS_AddDisableField\">Saldo</label> <input type=\"text\" name=\"BalanceText\" placeholder=\"\" ng-model=\"CPKS_AddBalanceText\" ng-maxlength=\"50\" ng-disabled=\"!CPKS_AddDisableTOPNum\" style=\"margin-left:55px\"> </div> <div class=\"radio\"> <label><input name=\"AddPaymentMethod\" ng-model=\"CPKS_AddPaymentMethod\" value=\"top\" type=\"radio\" ng-change=\"CPKS_Add_PaymentMethod_Radio_Changed()\" ng-disabled=\"CPKS_AddDisableField\">Masa berlaku</label> <input type=\"number\" name=\"TOPNum\" placeholder=\"\" ng-model=\"CPKS_AddTOPNum\" ng-disabled=\"CPKS_AddDisableTOPNum\" style=\"margin-left: 10px\"> </div> </form> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:5px\"> <div class=\"col-md-1\" style=\"margin-top:8px\"> <bsreqlabel>NPWP</bsreqlabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"PKSNPWPText\" placeholder=\"\" ng-model=\"CPKS_AddNPWPText\" ng-disabled=\"CPKS_AddDisableField\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:5px\"> <div class=\"col-md-1\" style=\"margin-top:8px\"> <bsreqlabel>Alamat NPWP</bsreqlabel> </div> <div class=\"col-md-3\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <textarea type=\"text\" class=\"form-control\" name=\"npwpAddress\" placeholder=\"\" ng-model=\"CPKS_AddNPWPAddress\" maxlength=\"250\" style=\"resize:none\" ng-disabled=\"CPKS_AddDisableField\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top:5px\"> <div class=\"col-md-2\" style=\"margin-top:15px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"AddFilterText\" placeholder=\"Filter\" ng-model=\"CPKS_AddFilterText\" ng-maxlength=\"150\"> </div> </div> <div class=\"col-md-2\" style=\"margin-top:15px\"> <bsselect ng-model=\"CPKS_AddFilterColumn\" data=\"CPKS_AddFilterColumnOption\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Semua Kolom\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-2\" style=\"margin-top:15px\"> <button id=\"CPKS_AddFilterButton\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"CPKS_Add_Filter_Button_Clicked()\" type=\"button\"> Cari </button> </div> <div ng-show=\"CPKS_AddButtons_Show\" class=\"col-md-2\" style=\"margin-top:8px\"> <button id=\"CPKS_Add_UploadExcel_Button\" ng-click=\"CPKS_Add_UploadExcel_Button_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:5px; margin-left:200px\"> <span class=\"ladda-label\"> Upload Dari Excel </span><span class=\"ladda-spinner\"></span> </button> </div> <div ng-show=\"CPKS_AddButtons_Show\" class=\"col-md-2\" style=\"margin-top:8px\"> <button id=\"CPKS_Add_DownloadTemplate_Button\" ng-click=\"CPKS_Add_DownloadTemplate_Button_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left; margin-top:5px; margin-right:5px\"> <span class=\"ladda-label\"> Download Template </span><span class=\"ladda-spinner\"></span> </button> </div> <div ng-show=\"CPKS_AddButtons_Show\" class=\"col-md-2\" style=\"margin-top:8px\"> <button id=\"CPKS_Add_AddUnitPKS_Button\" ng-click=\"CPKS_Add_AddUnitPKS_Button_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left; margin-top:5px; margin-left:0px\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-plus\"></i>Tambah Kendaraan </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-12\" style=\"margin-top:8px\"> <div id=\"myGrid\" ui-grid=\"CPKS_Add_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-bottom:20px; margin-left:13px\"></div> </div> </div> </div> <!-- ADD UNIT PKS MENU --> <div ng-show=\"CPKS_AddUnitPKS_Show\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:8px\"> <div class=\"col-md-2\" style=\"margin-top:15px\"> <bsreqlabel>Nomor Polisi</bsreqlabel> </div> <div class=\"col-md-4\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"LicenseNumText\" placeholder=\"Nomor Polisi\" ng-model=\"CPKS_AddUnitPKSLicenseNumText\" maxlength=\"15\" required> </div> </div> <div class=\"col-md-1\" style=\"margin-top:8px\"> <button id=\"CPKS_AddUnitPKSFindLicenseNumButton\" class=\"rbtn btn ng-binding ladda-button\" ng-click=\"CPKS_AddUnitPKS_Find_Button_Clicked()\" type=\"button\"> Cari </button> </div> <!-- <div class=\"col-md-12\" style=\"margin-bottom:5px\"></div> --> <div class=\"col-md-4\" style=\"margin-top:8px\"> <button id=\"CPKS_AddUnitPKS_Save_Button\" ng-click=\"CPKS_AddUnitPKS_Save_Button_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Simpan </span> </button> </div> <div class=\"col-md-1\" style=\"margin-top:8px\"> <button id=\"CPKS_AddUnitPKS_Cancel_Button\" ng-click=\"CPKS_AddUnitPKS_Cancel_Button_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left\"> <span class=\"ladda-label\"> Batal </span> </button> </div> </div> <!-- </div> --> <div class=\"col-md-12\" style=\"margin-bottom:8px\"> <div class=\"col-md-2\" style=\"margin-top:15px\"> <bsreqlabel>Model</bsreqlabel> </div> <div class=\"col-md-4\" style=\"margin-top:8px\"> <bsselect ng-model=\"CPKS_AddUnitPKSModel\" data=\"CPKS_AddUnitPKSModelOption\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"\" icon=\"fa fa-search\" ng-disabled=\"disableModelVin\"> <!-- ng-disabled=false --> </bsselect> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:8px\"> <div class=\"col-md-2\" style=\"margin-top:15px\"> <bsreqlabel>VIN</bsreqlabel> </div> <div class=\"col-md-4\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"VINText\" placeholder=\"\" ng-model=\"CPKS_AddUnitPKSVINText\" maxlength=\"20\" ng-disabled=\"disableModelVin\"> <!-- ng-disabled=false --> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:8px\"> <div class=\"col-md-2\" style=\"margin-top:15px\"> <bslabel>Penanggung Jawab</bslabel> </div> <div class=\"col-md-4\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"picText\" placeholder=\"\" ng-model=\"CPKS_AddUnitPKSPICText\" maxlength=\"30\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:8px\"> <div class=\"col-md-2\" style=\"margin-top:15px\"> <bslabel>Nomor Handphone</bslabel> </div> <div class=\"col-md-4\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"phoneNumText\" placeholder=\"\" ng-model=\"CPKS_AddUnitPKSPhoneNumText\" ng-change=\"phoneChange(CPKS_AddUnitPKSPhoneNumText,'CPKS_AddUnitPKSPhoneNumText')\" maxlength=\"15\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:8px\"> <div class=\"col-md-2\" style=\"margin-top:15px\"> <bslabel>Alamat</bslabel> </div> <div class=\"col-md-4\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <textarea type=\"text\" class=\"form-control\" name=\"picAddress\" placeholder=\"\" ng-model=\"CPKS_AddUnitPKSPICAddress\" maxlength=\"50\" style=\"resize:none\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <div ng-show=\"CPKS_AddUnitPKS_ValidUntil_Show\" class=\"col-md-12\" style=\"margin-bottom:8px\"> <div class=\"col-md-2\" style=\"margin-top:15px\"> <bsreqlabel>Berlaku Sampai</bsreqlabel> </div> <div class=\"col-md-4\" style=\"margin-top:8px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"AddUnitPKSValidUntil\" date-options=\"DateOptions\" ng-model=\"CPKS_AddUnitPKSValidUntil\"> </bsdatepicker> </div> </div> </div> </div> </div> </div> <div class=\"ui modal\" id=\"CPKS_ToyotaID_Modal\" role=\"dialog\" style=\"margin-top: 20px; height: 350px; width:750px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:20px\"> <div id=\"myGrid\" ui-grid=\"CPKS_ToyotaID_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 300px; margin-bottom:50px; margin-left:20px\"></div> </div> </ul> </div> </div> <div class=\"ui modal\" id=\"CPKS_LicenseNum_Modal\" role=\"dialog\" style=\"margin-top: -70px; height: 300px; width:600px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:8px\"> <div id=\"myGrid\" ui-grid=\"CPKS_LicenseNum_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-bottom:0px; margin-left:13px\"></div> </div> </ul> </div> </div> <div class=\"ui modal\" id=\"CPKS_Add_Upload_Modal\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"CPKS_Add_UploadVehicleList\" onchange=\"angular.element(this).scope().loadXLS(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"CPKS_Add_Upload_Button\" ng-click=\"CPKS_Add_Upload_Button_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div> <!-- <div class=\"ui modal\" id=\"CPKS_Main_DeleteQuestion_Modal\" role=\"dialog\" style=\"margin-top: -50px; height: 250px; width:300px;\">\r" +
    "\n" +
    "\t<div class=\"modal-content\">\r" +
    "\n" +
    "\t\t<ul class=\"list-group\">\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\" style=\"text-align: center; margin-top: 10px;\">\r" +
    "\n" +
    "\t\t\t\t<h2>Konfirmasi Hapus</h2>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\" style=\"text-align: center;\">\r" +
    "\n" +
    "\t\t\t\t<h3>Apakah anda yakin ingin menghapus?</h3>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\" style=\"margin-bottom:10px\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-6\" style=\"margin-top:12px\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"CKPS_Main_No_Delete_Button\" ng-click=\"CPKS_Main_No_Delete_Button_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-right:5px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tTidak\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-6\" style=\"margin-top:12px\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"CPKS_Main_Yes_Delete_Button\" ng-click=\"CPKS_Main_Yes_Delete_Button_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left; margin-left:5px;\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tYa\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</ul>\r" +
    "\n" +
    "\t</div>\r" +
    "\n" +
    "</div> --> <div class=\"ui modal\" id=\"CPKS_Add_DeleteQuestion_Modal\" role=\"dialog\" style=\"margin-top: -50px; height: 250px; width:300px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"text-align: center; margin-top: 10px\"> <h2>Konfirmasi Hapus</h2> </div> <div class=\"col-md-12\" style=\"text-align: center\"> <h3>Apakah anda yakin ingin menghapus?</h3> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div class=\"col-md-6\" style=\"margin-top:12px\"> <button id=\"CKPS_Add_No_Delete_Button\" ng-click=\"CPKS_Add_No_Delete_Button_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-right:5px\"> <span class=\"ladda-label\"> Tidak </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-6\" style=\"margin-top:12px\"> <button id=\"CPKS_Add_Yes_Delete_Button\" ng-click=\"CPKS_Add_Yes_Delete_Button_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left; margin-left:5px\"> <span class=\"ladda-label\"> Ya </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/hargaRetailParts/hargaRetailParts.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div ng-show=\"MainHargaRetailParts_Show\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bslabel>Tanggal Berlaku Dari</bslabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"MainHargaRetailPartsTanggalAwal\" date-options=\"dateOptions\" ng-model=\"MainHargaRetailParts_TanggalAwal\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <bslabel>Tanggal Berlaku Sampai</bslabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"MainHargaRetailPartsTanggalAkhir\" date-options=\"dateOptions\" ng-model=\"MainHargaRetailParts_TanggalAkhir\"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bslabel>No Material</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"MainHargaRetailPartsNoMaterial \" placeholder=\"\" ng-model=\"MainHargaRetail_PartsNoMaterial\" ng-maxlength=\"50\"> </div> </div> <div class=\"col-md-3\"> <bslabel>Nama Material</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"MainHargaRetailPartsNamaMaterial \" placeholder=\"\" ng-model=\"MainHargaRetail_PartsNamaMaterial\" ng-maxlength=\"50\"> </div> </div> <div class=\"col-md-6\"> <button id=\"MainHargaRetailParts_Hapus_Button\" ng-click=\"MainHargaRetailParts_Hapus_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Hapus </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MainHargaRetailParts_Cari_Button\" ng-click=\"MainHargaRetailParts_Cari_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Cari </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"MainHargaRetailParts_Tambah_Button\" ng-click=\"MainHargaRetailParts_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Input Harga Baru </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MainHargaRetailParts_Upload_Button\" ng-click=\"MainHargaRetailParts_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Upload Harga Baru </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MainHargaRetailParts_Download_Button\" ng-click=\"MainHargaRetailParts_Download_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Download Template </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"MainHargaRetailParts_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> </div> <div class=\"row\" ng-show=\"TambahHargaRetailParts_Show\"> <div class=\"col-md-12\"> <button id=\"TambahHargaRetailParts_Simpan_Button\" ng-click=\"TambahHargaRetailParts_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-show=\"TambahHargaRetailParts_Simpan_Show\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahHargaRetailParts_Kembali_Button\" ng-click=\"TambahHargaRetailParts_Kembali_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bslabel>Tanggal Berlaku Dari</bslabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahHargaRetailPartsTanggalAwal\" date-options=\"dateOptions\" ng-model=\"TambahHargaRetailParts_TanggalAwal\" ng-disabled=\"TambahHargaRetailParts_TanggalAwal_isDisabled\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <bslabel>Tanggal Berlaku Sampai</bslabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahHargaRetailPartsTanggalAkhir\" date-options=\"dateOptions\" ng-model=\"TambahHargaRetailParts_TanggalAkhir\" ng-disabled=\"TambahHargaRetailParts_TanggalAkhir_isDisabled\"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bsreqlabel>No Material untuk Parts</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahHargaRetailPartsNoMaterial \" placeholder=\"\" ng-model=\"TambahHargaRetail_PartsNoMaterial\" ng-maxlength=\"50\" ng-disabled=\"TambahHargaRetail_PartsNoMaterial_isDisabled\"> </div> </div> <div class=\"col-md-1\"> <button id=\"TambahHargaRetailParts_Cari_Button\" ng-click=\"TambahHargaRetailParts_Cari_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left\" ng-show=\"TambahHargaRetailParts_Cari_Show\"> <span class=\"ladda-label\"> Cari </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bsreqlabel>Nama Material untuk Parts</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahHargaRetailPartsNamaMaterial \" placeholder=\"\" ng-model=\"TambahHargaRetail_PartsNamaMaterial\" ng-maxlength=\"50\" ng-disabled=\"!TambahHargaRetail_PartsNamaMaterial_isDisabled\"> </div> </div> </div> <!--<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t<bsreqlabel>Satuan Beli</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t<bsselect ng-model=\"TambahHargaRetail_Satuan\"\r" +
    "\n" +
    "\t\t\t\t\t\t data=\"optionsTambahHargaRetail_Satuan\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Satuan Beli\"\r" +
    "\n" +
    "\t\t\t\t\t\t icon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</div>--> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bsreqlabel>Harga Retail Parts</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahHargaRetailPartsHarga \" placeholder=\"\" ng-model=\"TambahHargaRetail_Harga\" ng-maxlength=\"50\" ng-disabled=\"TambahHargaRetail_Harga_isDisabled\"> </div> </div> </div> </div> <div ng-show=\"UploadHargaRetailParts_Show\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"UploadHargaRetailParts_Simpan_Button\" ng-click=\"UploadHargaRetailParts_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"UploadHargaRetailParts_Kembali_Button\" ng-click=\"UploadHargaRetailParts_Kembali_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bslabel>Tanggal Berlaku Dari</bslabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"UploadHargaRetailPartsTanggalAwal\" date-options=\"dateOptions\" ng-model=\"UploadHargaRetailParts_TanggalAwal\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <bslabel>Tanggal Berlaku Sampai</bslabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"UploadHargaRetailPartsTanggalAkhir\" date-options=\"dateOptions\" ng-model=\"UploadHargaRetailParts_TanggalAkhir\"> </bsdatepicker> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-6\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadHargaRetail\" onchange=\"angular.element(this).scope().loadXLS(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"UploadHargaRetailParts_Upload_Button\" ng-click=\"UploadHargaRetailParts_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> <div class=\"row\"> <div class=\"row\" style=\"border:1px solid lightgrey; width:60%; margin-left:10px; margin-top:15px;margin-bottom:10px\"> <label style=\"margin-left:5px;margin-top:-9px;float:left;background-color:white\">Filter</label> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div class=\"col-md-4\"> <bslabel>No Material</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"UploadHargaRetailPartsFilterNoMaterial \" placeholder=\"\" ng-model=\"UploadHargaRetailParts_Filter_NoMaterial\" ng-maxlength=\"50\"> </div> </div> <div class=\"col-md-4\"> <bslabel>Nama Material</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"UploadHargaRetailPartsFilterNamaMaterial \" placeholder=\"\" ng-model=\"UploadHargaRetailParts_Filter_NamaMaterial\" ng-maxlength=\"50\"> </div> </div> <div class=\"col-md-4\"> <button id=\"UploadHargaRetailParts_Filter_Button\" ng-click=\"UploadHargaRetailParts_Filter_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Filter </span><span class=\"ladda-spinner\"></span> </button> <button id=\"UploadHargaRetailParts_HapusFilter_Button\" ng-click=\"UploadHargaRetailParts_HapusFilter_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left; margin-top:10px; margin-left:5px\"> <span class=\"ladda-label\"> Hapus Filter </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> </div> <div class=\"row\"> <div id=\"myGrid\" ui-grid=\"UploadHargaRetailParts_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> </div> <div class=\"ui modal\" id=\"ModalSearchParts\" role=\"dialog\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <bsreqlabel>Cari Parts</bsreqlabel> <div class=\"form-group\" ui-grid=\"SearchPartsGrid\" ui-grid-auto-resize ui-grid-selection ui-grid-pagination></div> <p align=\"center\"> <button class=\"col-sm-6\" ng-click=\"ModalSearchParts_Batal()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> Batal </button> </p> </ul> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/jawabanFIR/jawabanFIR.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div ng-show=\"Show_JawabanFIR_Tambah\"> <div class=\"row\"> <div class=\"col-md-12\" ng-show=\"JawabanFIRTambah_ShowTambahButton\"> <button id=\"JawabanFIRTambah_Simpan_Button\" ng-click=\"JawabanFIRTambah_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-left:20px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"JawabanFIRTambah_Batal_Button\" ng-click=\"JawabanFIRTambah_Batal_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" ng-show=\"JawabanFIRTambah_ShowKembaliButton\"> <button id=\"JawabanFIRTambah_Kembali_Button\" ng-click=\"JawabanFIRTambah_Kembali_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <bslabel>{{JawabanFIRTambah_PertanyaanFIR}}</bslabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"Jawaban FIR\" placeholder=\"\" ng-model=\"JawabanFIRTambah_JawabanFIR\" ng-maxlength=\"100\" ng-disabled=\"JawabanFIRTambah_JawabanFIR_isDisabled\">\r" +
    "\n" +
    "\t\t\t\t\t</textarea></div> <br> Positif <input type=\"radio\" ng-model=\"JawabanFIRTambah_IsNegativeFIR\" ng-value=\"0\"> &nbsp;&nbsp;&nbsp;&nbsp; Negatif <input type=\"radio\" ng-model=\"JawabanFIRTambah_IsNegativeFIR\" ng-value=\"1\"> </div> </div> </div> </div> <div ng-show=\"Show_JawabanFIR_Main\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"JawabanFIRMain_Tambah_Button\" ng-hide=\"disableAddButton\" ng-click=\"JawabanFIRMain_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> <i class=\"fa fa-fw fa-plus\"></i> Tambah Jawaban FIR </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" style=\"margin-left:5px\"> <div class=\"col-md-4\"> <bslabel>Pilihan Pertanyaan FIR</bslabel> <bsselect ng-model=\"JawabanFIRMain_DaftarPertanyaan\" data=\"optionsJawabanFIRMain_DaftarPertanyaan\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Select Daftar Pertanyaan\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-4\"> <button id=\"JawabanFIRMain_Search_Button\" ng-click=\"JawabanFIRMain_Search_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left; margin-top:20px\"> <span class=\"ladda-label\"> Search </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-12\" style=\"margin-top:30px; margin-left:-8px\"> <div class=\"col-md-6\"> <div class=\"col-md-3\"> <bsselect ng-model=\"JawabanFIRMain_BulkAction\" data=\"optionsJawabanFIRMain_BulkAction\" item-text=\"name\" item-value=\"value\" on-select=\"JawabanFIRMain_BulkAction_Changed(selected)\" placeholder=\"bulk action\" icon=\"fa fa-search\"> </bsselect> <!-- <div class=\"input-group-btn\" uib-dropdown>\r" +
    "\n" +
    "\t\t\t\t\t\t\t<button style=\"margin-right:5px;\" ng-disabled=\"!selectedRows.length>0\" type=\"button\" class=\"btn wbtn\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tBulk Action&nbsp;<span class=\"caret\"></span>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t<li role=\"menuitem\" ng-if=\"allowApprove\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t<a href=\"#\" ng-click=\"deleteJawaban(selectedRows,0)\" ng-if=\"selectedRows.length>0\">Hapus</a>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t</li>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</ul>\r" +
    "\n" +
    "\t\t\t\t\t\t</div> --> </div> <div class=\"input-group\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-change=\"test(GridAPIJawabanFIRGrid.grid.columns[filterColIdx].filters[0].term)\" ng-model=\"GridAPIJawabanFIRGrid.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\"></i> </span> </a> </li> </ul> </div> </div> <!--<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"\" placeholder=\"Teks Filter...\" ng-model=\"JawabanFIRMain_TextFilter\" ng-maxlength=\"50 \">\r" +
    "\n" +
    "\t\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t</div>--> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\t\t<bsselect ng-model=\"JawabanFIRMain_SelectKolom\" data=\"optionsJawabanFIRMain_SelectKolom\"\r" +
    "\n" +
    "\t\t\t\t\t\t item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Select Search Criteria\" icon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t\t\t</div> --> <!-- \t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\t\t<button id=\"\" ng-click=\"JawabanFIRMain_Filter_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tFilter\r" +
    "\n" +
    "\t\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t\t</div>--> </div> </div> <div class=\"col-md-12\" style=\"margin-top:-10px\"> <div id=\"myGrid\" ui-grid=\"JawabanFIRGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> </div> <div class=\"ui modal\" id=\"ModalHapusJawabanFIR\" role=\"dialog\"> <div class=\"modalDefault-dialog\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button> <h4 class=\"modal-title\">Konfirmasi Hapus</h4> </div> <div class=\"modal-body\"> <p> Apakah anda yakin ingin menghapus jawaban FIR ? </p> </div> <div class=\"modal-footer\"> <p align=\"center\"> <button ng-click=\"Batal_ModalJawabanFIR()\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">Tidak</button> <button ng-click=\"Hapus_ModalJawabanFIR()\" class=\"rbtn btn ng-binding ladda-button\" type=\"button\">Ya</button> </p> </div> </div> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/masterApprovalDiscount/masterApprovalDiscount.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"MasterApprovalDiscountForm\" factory-name=\"MasterApprovalDiscountFactory\" model=\"mMasterApprovalDiscount\" model-id=\"MasterApprovalDiscountId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xMasterApprovalDiscount.selected\" on-before-new=\"beforeNew\" on-show-detail=\"beforeEdit\" do-custom-save=\"doCustomSave\" on-select-rows=\"onSelectRows\" on-validate-save=\"validateSave\" on-bulk-delete=\"onBulkDelete\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div ng-form=\"appDiskon\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Jabatan</bsreqlabel> <bsselect ng-model=\"mMasterApprovalDiscount.RoleId\" data=\"RoleData\" item-text=\"RoleName\" item-value=\"RoleId\" placeholder=\"Pilih Role\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Range Diskon Minimal</bsreqlabel> <input type=\"number\" step=\"0.01\" min=\"0.01\" max=\"100\" class=\"form-control\" placeholder=\"%\" name=\"diskonMinimal\" ng-model=\"mMasterApprovalDiscount.MinimalDiscount\" required> <em ng-messages=\"appDiskon.diskonMinimal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Range diskon harus kelipatan 0.01 </p> <p ng-message=\"max\">Maximal Diskon 100% </p> </em> <!-- <bsselect ng-model=\"mMasterApprovalDiscount.MinimalDiscount\" data=\"MinimalDiscount\" item-text=\"name\" item-value=\"id\" placeholder=\"%\" required>\r" +
    "\n" +
    "                    </bsselect> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <!-- <bsreqlabel>Range Diskon Maximal</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect ng-model=\"mMasterApprovalDiscount.MaximalDiscount\" data=\"MaximalDiscount\" item-text=\"name\" item-value=\"id\" placeholder=\"%\" required>\r" +
    "\n" +
    "                    </bsselect> --> <bsreqlabel>Range Diskon Maximal</bsreqlabel> <input type=\"number\" step=\"0.01\" min=\"{{mMasterApprovalDiscount.MinimalDiscount}}\" max=\"100\" class=\"form-control\" placeholder=\"%\" name=\"diskonMaximal\" ng-model=\"mMasterApprovalDiscount.MaximalDiscount\" required> <em ng-messages=\"appDiskon.diskonMaximal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"step\">Range diskon harus kelipatan 0.01 </p> <p ng-message=\"max\">Maximal Diskon 100% </p> <p ng-message=\"min\">Minimal diskon tidak boleh kurang dari {{mMasterApprovalDiscount.MinimalDiscount}} </p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>GR / BP</bsreqlabel> <bsselect ng-model=\"mMasterApprovalDiscount.IsGR\" data=\"IsGRBP\" item-text=\"name\" item-value=\"id\" placeholder=\"\" required> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Approval Diskon Ke :</bsreqlabel> <bsselect ng-model=\"mMasterApprovalDiscount.Sequence\" data=\"countDiscount\" item-text=\"id\" item-value=\"id\" placeholder=\"\" required> </bsselect> </div> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/AfterSales/masterTemplateMessage/masterTemplateMessage.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div class=\"row\"> <div ng-show=\"TemplateMessageMain_Show\"> <div class=\"col-md-12\" style=\"margin-bottom: 35px; margin-top: 15px\"> <div class=\"col-md-2\" style=\"margin-top: 5px;padding-right: 0px\"> Kategori Template SMS : </div> <div class=\"col-md-3\"> <bsselect ng-model=\"TemplateMessageEdit_MessageType\" data=\"optionsTemplateMessageEdit_MessageType\" ng-change=\"ShowDataMain()\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Message Type\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-7\"> <button id=\"TemplateMessageMain_EditSMS_Button\" ng-click=\"TemplateMessageMain_EditSMS_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Edit Template SMS </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <bslabel ng-hide=\"!hideSmsPsfu\">Template SMS MRS</bslabel> <bslabel ng-hide=\"hideSmsPsfu\">Template SMS PSFU</bslabel> <div class=\"input-icon-right\"> <textarea type=\"text\" class=\"form-control\" name=\"TemplateMessageMain_SMS\" placeholder=\"\" ng-maxlength=\"2000\" ng-model=\"TemplateMessageMain_SMS\" style=\"height:200px\" ng-disabled=\"!TemplateMessageMain_SMS_Disabled\">\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-12\"> <button id=\"TemplateMessageMain_EditEmail_Button\" ng-click=\"TemplateMessageMain_EditEmail_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Edit Template E-Mail </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <bslabel ng-hide=\"!hideSmsPsfu\">Template Email MRS</bslabel> <bslabel ng-hide=\"hideSmsPsfu\">Template Email PSFU</bslabel> <div class=\"input-icon-right\"> <textarea type=\"text\" class=\"form-control\" name=\"TemplateMessageMain_Email\" placeholder=\"\" ng-model=\"TemplateMessageMain_Email\" style=\"height:200px\" ng-disabled=\"!TemplateMessageMain_Email_Disabled\">\r" +
    "\n" +
    "\t\t\t\t\t\t</textarea> </div> </div> </div> </div> <div ng-show=\"TemplateMessageEdit_Show\" ng-form=\"tmpMessage\" novalidate> <div class=\"col-md-12\"> <div class=\"col-md-11\"> <button id=\"TemplateMessageEdit_Simpan_Button\" ng-click=\"TemplateMessageEdit_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\" ng-disabled=\"tmpMessage.$invalid\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-1\"> <button id=\"TemplateMessageEdit_Batal_Button\" ng-click=\"TemplateMessageEdit_Batal_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <!-- ini dicomen karena berdasarkan tiket #55132 <div ng-show=\"TemplateMessageEdit_ShowButton\"> --> <div class=\"col-md-12\"> <label>Parameter Template</label> {{tempString}} </div> <div> <!--<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-4\">\r" +
    "\n" +
    "\t\t\t\t\t<div ui-grid=\"TemplateMessageParam_UIGrid\" class=\"form-group\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-selection ui-grid-pagination style=\"width:100%;height:200px\">\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"TemplateMessageEdit_Insert_Button\" ng-click=\"TemplateMessageEdit_Insert_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tInsert\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>--> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <button id=\"TemplateMessageEdit_Outlet_Button\" ng-click=\"TemplateMessageEdit_Outlet_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"display:block; margin-left:auto;margin-right:auto; margin-top:20px; width:70%\"> <span class=\"ladda-label\"> Outlet </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-3\"> <button id=\"TemplateMessageEdit_NomorHPCustomer_Button\" ng-click=\"TemplateMessageEdit_NomorHPCustomer_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"display:block; margin-left:auto;margin-right:auto; margin-top:20px; width:70%\"> <span class=\"ladda-label\"> Nomor HP Customer </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-3\"> <button id=\"TemplateMessageEdit_TanggalJatuhTempo_Button\" ng-click=\"TemplateMessageEdit_TanggalJatuhTempo_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"display:block; margin-left:auto;margin-right:auto; margin-top:20px; width:70%\"> <span class=\"ladda-label\"> Tanggal Jatuh Tempo </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <button id=\"TemplateMessageEdit_JenisSB_Button\" ng-click=\"TemplateMessageEdit_JenisSB_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"display:block; margin-left:auto;margin-right:auto; margin-top:20px; width:70%\"> <span class=\"ladda-label\"> Jenis SB </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-3\"> <button id=\"TemplateMessageEdit_WarnaKendaraan_Button\" ng-click=\"TemplateMessageEdit_WarnaKendaraan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"display:block; margin-left:auto;margin-right:auto; margin-top:20px; width:70%\"> <span class=\"ladda-label\"> Warna Kendaraan </span><span class=\"ladda-spinner\"></span> </button> </div> <!-- \r" +
    "\n" +
    "                    <div class=\"col-md-3\">\r" +
    "\n" +
    "                        <button id=\"TemplateMessageEdit_NomorPolisi_Button\" ng-click=\"TemplateMessageEdit_NomorPolisi_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"display:block; margin-left:auto;margin-right:auto; margin-top:20px; width:70%\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\tNomor Polisi\r" +
    "\n" +
    "\t\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t\t</button>\r" +
    "\n" +
    "                    </div> --> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <button id=\"TemplateMessageEdit_ModelKendaraan_Button\" ng-click=\"TemplateMessageEdit_ModelKendaraan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"display:block; margin-left:auto;margin-right:auto; margin-top:20px; width:70%\"> <span class=\"ladda-label\"> Model Kendaraan </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-3\"> <button id=\"TemplateMessageEdit_NamaCustomer_Button\" ng-click=\"TemplateMessageEdit_NamaCustomer_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"display:block; margin-left:auto;margin-right:auto; margin-top:20px; width:70%\"> <span class=\"ladda-label\"> Nama Customer </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> <div class=\"col-md-12\"> <!--   <div class=\"col-md-7\">\r" +
    "\n" +
    "                    <bslabel>Isi Pesan</bslabel>\r" +
    "\n" +
    "                </div> --> <!--  <div class=\"col-md-2\">\r" +
    "\n" +
    "                    <button id=\"TemplateMessageEdit_DefaultTemplate_Clicked\" ng-click=\"TemplateMessageEdit_DefaultTemplate_Clicked()\" ng-hide=\"\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-left:5px; margin-top:-32px;float:right; width:70%\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tDefault HO\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                </div> --> </div> <div class=\"col-md-12\"> <div class=\"input-icon-right\"> <textarea my-text=\"\" type=\"text\" class=\"form-control\" name=\"TemplateMessageEdit_Message\" placeholder=\"\" ng-maxlength=\"2000\" ng-model=\"TemplateMessageEdit_Message\" style=\"height:300px; margin-top:10px\" ng-blur=\"TemplateMessageEdit_Message_LostFocus\">\r" +
    "\n" +
    "\t\t\t\t\t</textarea> <em ng-messages=\"tmpMessage.TemplateMessageEdit_Message.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> <p ng-message=\"minlength\">Jml. Karakter kurang</p> <p ng-message=\"maxlength\">Jml. Karakter terlalu banyak</p> </em></div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/masterWAC/masterWAC.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"MasterWACForm\" factory-name=\"MasterWACFactory\" model=\"mMasterWAC\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-show-detail=\"onShowDetail\" on-before-new=\"beforeNew\" on-before-edit=\"beforeEdit\" do-custom-save=\"doCustomSave\" form-name=\"MasterWACForm\" form-title=\"Master WAC\" modal-title=\"Master WAC\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Group</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control col-md-3\" style=\"float:left\" name=\"GroupName\" placeholder=\"Group Name\" ng-model=\"mMasterWAC.GroupName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GroupName\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Quantity</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" min=\"0\" class=\"form-control col-md-3\" style=\"float:left\" name=\"Quantity\" placeholder=\"Quantity\" ng-model=\"mMasterWAC.Quantity\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"GroupName\"></bserrmsg> </div> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-8\"> <div id=\"gridDetail\" ui-grid-edit ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" ng-show=\"!gridDetail.data.length\">No data available</div> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/AfterSales/pemeriksaanBP/pemeriksaanBP.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div class=\"row\"> <div ng-show=\"PemeriksaanBPMain_Show\"> <div class=\"col-md-12\" style=\"margin-top:-10px\"> <div id=\"myGrid\" ui-grid=\"PemeriksaanBPGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> <div ng-show=\"PemeriksaanBPEdit_Show\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <textarea type=\"text\" class=\"form-control\" name=\"Detail Penjelasan Saat Penyerahan\" placeholder=\"\" ng-model=\"PemeriksaanBPEdit_Detail\" ng-maxlength=\"200\">\r" +
    "\n" +
    "\t\t\t\t</textarea></div> <div class=\"col-md-9\"> <button id=\"PemeriksaanBPEdit_Simpan_Button\" ng-click=\"PemeriksaanBP_Edit_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"PemeriksaanBP_Edit_Batal_Button\" ng-click=\"PemeriksaanBP_Edit_Batal_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/pemeriksaanGR/pemeriksaanGR.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div class=\"row\"> <div ng-show=\"PemeriksaanGRMain_Show\"> <div class=\"col-md-12\" style=\"margin-top:-10px\"> <div id=\"myGrid\" ui-grid=\"PemeriksaanGRGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> <div ng-show=\"PemeriksaanGREdit_Show\"> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <textarea type=\"text\" class=\"form-control\" name=\"Detail Penjelasan Saat Penyerahan\" placeholder=\"\" ng-model=\"PemeriksaanGREdit_Detail\" ng-maxlength=\"200\">\r" +
    "\n" +
    "\t\t\t\t</textarea></div> <div class=\"col-md-9\"> <button id=\"PemeriksaanGREdit_Simpan_Button\" ng-click=\"PemeriksaanGR_Edit_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"PemeriksaanGR_Edit_Batal_Button\" ng-click=\"PemeriksaanGR_Edit_Batal_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/penyerahanFIR/penyerahanFIR.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <bsform-grid ng-form=\"PenyerahanFIRForm\" factory-name=\"PenyerahanFIRFactory\" model=\"mPenyerahanFIR\" model-id=\"QuestionId\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"PenyerahanFIRForm\" form-title=\"Pertanyaan FIR\" modal-title=\"PenyerahanFIR\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" on-bulk-delete=\"CheckOutlet\" grid-hide-action-column=\"true\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Pertanyaan FIR</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <textarea type=\"text\" class=\"form-control\" name=\"Pertanyaan FIR\" placeholder=\"\" ng-model=\"mPenyerahanFIR.Description\" ng-maxlength=\"100\">\r" +
    "\n" +
    "\t\t                </textarea></div> <bserrmsg field=\"FIRQuestion\"></bserrmsg> </div> </div> </div> </div> </bsform-grid> </div>"
  );


  $templateCache.put('app/master/AfterSales/specialCampaign/specialCampaign.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div ng-show=\"MainSpecialCampaign_Show\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"MainSpecialCampaign_Tambah_Button\" ng-click=\"MainSpecialCampaign_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MainSpecialCampaign_Upload_Button\" ng-click=\"MainSpecialCampaign_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MainSpecialCampaign_Download_Button\" ng-click=\"MainSpecialCampaign_Download_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Download Template </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-4\" style=\"margin-left:20px\"> <div class=\"input-group\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-change=\"test(GridApiMainSpecialCampaign.grid.columns[filterColIdx].filters[0].term)\" ng-model=\"GridApiMainSpecialCampaign.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\"></i> </span> </a> </li> </ul> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top:0px\"> <div id=\"myGrid\" ui-grid=\"MainSpecialCampaign_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> <div class=\"col-md-12\" style=\"margin-left: 20px; font-size: 15px\"> <label>List Model dalam Campaign : {{NamaCampaign}}</label> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"MainSpecialCampaignModel_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> <div class=\"col-md-12\" style=\"margin-left: 20px\"> <h2>MODEL : {{NamaModel | uppercase}}</h2> </div> <div class=\"col-md-12\" style=\"margin-left: 20px; font-size: 15px\"> <label>Diskon Jasa</label> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"MainSpecialCampaignJasa_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> <div class=\"col-md-12\" style=\"margin-left: 20px; font-size: 15px\"> <label>Diskon Parts</label> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"MainSpecialCampaignParts_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> </div> <div class=\"row\" ng-show=\"TambahSpecialCampaign_Show\" ng-form=\"CampaignDisc\" novalidate> <div class=\"col-md-12\"> <button id=\"TambahSpecialCampaign_Simpan_Button\" ng-click=\"TambahSpecialCampaign_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-show=\"TambahSpecialCampaign_ViewMode_Show\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahSpecialCampaign_Kembali_Button\" ng-click=\"TambahSpecialCampaign_Kembali_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-right: 3px\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsreqlabel>Nama Campaign</bsreqlabel> </div> <div class=\"col-md-5\"> <div class=\"input-icon-right\"> <input required type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaignNamaCampaign \" placeholder=\"\" ng-model=\"TambahSpecialCampaign_NamaCampaign\" ng-maxlength=\"50\" ng-disabled=\"TambahSpecialCampaign_NamaCampaign_isDisabled\"> <em ng-messages=\"CampaignDisc.TambahSpecialCampaignNamaCampaign.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em></div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsreqlabel>Range KM</bsreqlabel> </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <input required type=\"number\" min=\"0\" class=\"form-control\" name=\"TambahSpecialCampaignRangeKMAwal \" placeholder=\"\" ng-model=\"TambahSpecialCampaign_RangeKMAwal\" ng-maxlength=\"50\" ng-disabled=\"TambahSpecialCampaign_RangeKMAwal_isDisabled\"> <em ng-messages=\"CampaignDisc.TambahSpecialCampaignRangeKMAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em></div> </div> <div class=\"col-md-1\" style=\"text-align: center\"> s/d </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <input required type=\"number\" min=\"0\" class=\"form-control\" name=\"TambahSpecialCampaignRangeKMAkhir \" placeholder=\"\" ng-model=\"TambahSpecialCampaign_RangeKMAkhir\" ng-maxlength=\"50\" ng-disabled=\"TambahSpecialCampaign_RangeKMAkhir_isDisabled\"> <em ng-messages=\"CampaignDisc.TambahSpecialCampaignRangeKMAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em></div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsreqlabel>Tahun Rakit</bsreqlabel> </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <bsdatepicker required name=\"TambahSpecialCampaignTahunRakitAwal\" date-options=\"dateOptionsYear\" ng-model=\"TambahSpecialCampaign_TahunRakitAwal\" ng-disabled=\"TambahSpecialCampaign_ViewMode\"> </bsdatepicker> <em ng-messages=\"CampaignDisc.TambahSpecialCampaignTahunRakitAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em></div> </div> <div class=\"col-md-1\" style=\"text-align: center\"> s/d </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <bsdatepicker required name=\"TambahSpecialCampaignTahunRakitAkhir\" date-options=\"dateOptionsYear\" ng-model=\"TambahSpecialCampaign_TahunRakitAkhir\" ng-disabled=\"TambahSpecialCampaign_ViewMode\"> </bsdatepicker> <em ng-messages=\"CampaignDisc.TambahSpecialCampaignTahunRakitAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em></div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsreqlabel>Periode Campaign</bsreqlabel> </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <bsdatepicker required name=\"TambahSpecialCampaignPeriodeAwal\" date-options=\"dateOptions\" ng-model=\"TambahSpecialCampaign_PeriodeAwal\" ng-disabled=\"TambahSpecialCampaign_ViewMode\"> </bsdatepicker> <em ng-messages=\"CampaignDisc.TambahSpecialCampaignPeriodeAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em></div> </div> <div class=\"col-md-1\" style=\"text-align: center\"> s/d </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <bsdatepicker required name=\"TambahSpecialCampaignPeriodeAkhir\" date-options=\"dateOptions\" ng-model=\"TambahSpecialCampaign_PeriodeAkhir\" ng-disabled=\"TambahSpecialCampaign_ViewMode\"> </bsdatepicker> <em ng-messages=\"CampaignDisc.TambahSpecialCampaignPeriodeAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"required\">Wajib diisi</p> </em></div> </div> </div> <div class=\"col-md-11\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px; margin-left:20px;background:white\"> <div class=\"form-group col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-1\"> Model </div> <div class=\"col-md-3\"> <bsselect name ng-model=\"TambahSpecialCampaign_Model\" data=\"optionsTambahSpecialCampaign_Model\" ng-change=\"TambahSpecialCampaign_Model_Changed()\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Pilih Model\" icon=\"fa fa-search\" ng-disabled=\"TambahSpecialCampaign_ViewMode\"> <!-- <em ng-messages=\"CampaignDisc.NamaCounterBP.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t<p ng-message=\"required\">Wajib diisi</p>   </bsselect> --> </bsselect></div> </div> <!-- <div class=\"col-md-12\"></div>\r" +
    "\n" +
    "        <div class=\"col-md-12\"></div>\r" +
    "\n" +
    "        <div class=\"col-md-12\"></div>\r" +
    "\n" +
    "        <div class=\"col-md-12\"></div>\r" +
    "\n" +
    "        <div class=\"col-md-12\"></div>\r" +
    "\n" +
    "        <div class=\"col-md-12\"></div> --> <div class=\"form-group col-md-12\"> <div class=\"col-md-1\"> Full Model </div> <div class=\"col-md-3\"> <bsselect ng-model=\"TambahSpecialCampaign_FullModel\" data=\"optionsTambahSpecialCampaign_FullModel\" ng-change=\"TambahSpecialCampaign_FullModel_Changed()\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Pilih Full Model\" icon=\"fa fa-search\" ng-disabled=\"!TambahSpecialCampaign_Model\"> <!-- <bsselect ng-model=\"TambahSpecialCampaign_FullModel\" data=\"optionsTambahSpecialCampaign_FullModel\" ng-change=\"TambahSpecialCampaign_FullModel_Changed()\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Pilih Full Model\" icon=\"fa fa-search\"\r" +
    "\n" +
    "                    ng-disabled=\"TambahSpecialCampaign_ViewMode\" ng-disabled=\"disFullModel\" required> --> <!-- <em ng-messages=\"CampaignDisc.NamaCounterBP.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t<p ng-message=\"required\">Wajib diisi</p>     </bsselect> --> </bsselect></div> <div class=\"col-md-1\"> Deskripsi </div> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" name=\"TambahSpecialCampaignDeskripsiModel \" placeholder=\"\" ng-model=\"TambahSpecialCampaign_DeskripsiModel\" ng-maxlength=\"50\" ng-disabled=\"DeskripsiModel_isDisabled\"> </div> </div> <div class=\"col-md-4\"> <button id=\"TambahSpecialCampaign_Tambah_Button\" ng-disabled=\"CampaignDisc.$invalid\" ng-click=\"TambahSpecialCampaign_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-show=\"TambahSpecialCampaign_ViewMode_Show\"> <span class=\"ladda-label\"> Tambah Model </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "            <button id=\"TambahSpecialCampaign_Tambah_Button\" ng-disabled=\"CampaignDisc.$invalid\" ng-click=\"TambahSpecialCampaign_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-show=\"TambahSpecialCampaign_ViewMode_Show\">\r" +
    "\n" +
    "\t\t\t\t<span class=\"ladda-label\" >\r" +
    "\n" +
    "\t\t\t\t\tTambah Model\r" +
    "\n" +
    "\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "        </div> --> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"TambahSpecialCampaign_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:94%;height: 250px; margin-top:20px; margin-bottom:20px; margin-left:7px\"></div> </div> </div> <div class=\"row\" ng-show=\"TambahSpecialCampaignItems_Show\"> <div class=\"col-md-12\" style=\"margin-bottom: 10pt\"> <button id=\"TambahSpecialCampaignItems_Simpan_Button\" ng-click=\"TambahSpecialCampaignItems_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-show=\"TambahSpecialCampaign_ViewMode_Show\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahSpecialCampaignItems_Kembali_Button\" ng-click=\"TambahSpecialCampaignItems_Kembali_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-1\"> Nama Campaign </div> <div class=\"col-md-5\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaignItemsNamaCampaign \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_NamaCampaign\" ng-maxlength=\"50\" ng-disabled=\"!TambahSpecialCampaignItems_NamaCampaign_isDisabled\"> </div> </div> <div class=\"col-md-1\"> Periode Campaign </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahSpecialCampaignItemsPeriodeAwal\" date-options=\"dateOptions\" ng-model=\"TambahSpecialCampaignItems_PeriodeAwal\" ng-disabled=\"!TambahSpecialCampaignItems_PeriodeAwal_isDisabled\"> </bsdatepicker> </div> </div> <div class=\"col-md-1\" style=\"text-align: center\"> s/d </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahSpecialCampaignItemsPeriodeAkhir\" date-options=\"dateOptions\" ng-model=\"TambahSpecialCampaignItems_PeriodeAkhir\" ng-disabled=\"!TambahSpecialCampaignItems_PeriodeAkhir_isDisabled\"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-1\"> Range KM </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaignItemsRangeKMAwal \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_RangeKMAwal\" ng-maxlength=\"50\" ng-disabled=\"!TambahSpecialCampaignItems_RangeKMAwal_isDisabled\"> </div> </div> <div class=\"col-md-1\" style=\"text-align: center\"> s/d </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaigItemsnRangeKMAkhir \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_RangeKMAkhir\" ng-maxlength=\"50\" ng-disabled=\"!TambahSpecialCampaignItems_RangeKMAwal_isDisabled\"> </div> </div> <div class=\"col-md-1\"> Model </div> <div class=\"col-md-5\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaignItemsModel \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_Model\" ng-maxlength=\"50\" ng-disabled=\"!TambahSpecialCampaignItems_Model_isDisabled\"> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-1\"> Tahun Rakit </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahSpecialCampaignItemsTahunRakitAwal\" date-options=\"dateOptionsYear\" ng-model=\"TambahSpecialCampaignItems_TahunRakitAwal\" ng-disabled=\"!TambahSpecialCampaignItems_TahunRakitAwal_isDisabled\"> </bsdatepicker> </div> </div> <div class=\"col-md-1\" style=\"text-align: center\"> s/d </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahSpecialCampaignItemsTahunRakitAkhir\" date-options=\"dateOptionsYear\" ng-model=\"TambahSpecialCampaignItems_TahunRakitAkhir\" ng-disabled=\"!TambahSpecialCampaignItems_TahunRakitAkhir_isDisabled\"> </bsdatepicker> </div> </div> </div> <br> <br> <br> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-1\"> Task Name </div> <div class=\"col-md-3\"> <!-- <bsselect ng-model=\"TambahSpecialCampaignItems_Openo\" data=\"optionsTambahSpecialCampaignItems_Openo\" ng-change=\"TambahSpecialCampaignItems_Openo_Changed()\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Pilih Task Name\" icon=\"fa fa-search\"\r" +
    "\n" +
    "                    ng-disabled=\"TambahSpecialCampaign_ViewMode\">\r" +
    "\n" +
    "                </bsselect> --> <bstypeahead placeholder=\"Nama Pekerjaan\" name=\"jobName\" ng-model=\"TambahSpecialCampaignItems_Openo\" get-data=\"getWork\" item-text=\"TaskName\" loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectWork\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" icon=\"fa fa-search\" template-url=\"customTemplate2.html\" ng-disabled=\"TambahSpecialCampaign_ViewMode\"> </bstypeahead> </div> <div class=\"col-md-1\"> Deskripsi Jasa </div> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaignItemsDeskripsiJasa \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_DeskripsiJasa\" ng-maxlength=\"50\" ng-disabled=\"TambahSpecialCampaignItems_DeskripsiJasa_isDisabled\"> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-1\"> Discount Jasa </div> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <input type=\"number\" min=\"0\" class=\"form-control\" name=\"TambahSpecialCampaignItemsDiscountJasa \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_DiscountJasa\" ng-maxlength=\"50\" ng-disabled=\"TambahSpecialCampaignItems_DiscountJasa_isDisabled\"> </div> </div> <div class=\"col-md-8\"> <button id=\"TambahSpecialCampaignItems_TambahJasa_Button\" ng-click=\"TambahSpecialItems_TambahJasa_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-show=\"TambahSpecialCampaign_ViewMode_Show\"> <span class=\"ladda-label\"> Tambah Jasa </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"TambahSpecialCampaignJasa_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:98%; height: 250px; margin-top:20px; margin-bottom:20px; margin-left:10px\"></div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-1\"> Nama Parts </div> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaignItemsNamaParts \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_NamaParts\" ng-maxlength=\"50\" ng-disabled=\"TambahSpecialCampaignItems_NamaParts_isDisabled\"> </div> </div> <div class=\"col-md-1\"> <button id=\"TambahSpecialCampaignItems_CariParts_Button\" ng-click=\"TambahSpecialItems_CariParts_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-show=\"TambahSpecialCampaign_ViewMode_Show\"> <span class=\"ladda-label\"> Cari </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-1\" style=\"margin-left: 10%\"> Deskripsi Parts </div> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaignItemsDeskripsiParts \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_DeskripsiParts\" ng-maxlength=\"50\" ng-disabled=\"!TambahSpecialCampaignItems_DeskripsiParts_isDisabled\"> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-1\"> No Parts </div> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahSpecialCampaignItemsNoParts \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_NoParts\" ng-maxlength=\"50\" ng-disabled=\"!TambahSpecialCampaignItems_NoParts_isDisabled\"> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-1\"> Discount Parts </div> <div class=\"col-md-3\"> <div class=\"input-icon-right\"> <input type=\"number\" min=\"0\" class=\"form-control\" name=\"TambahSpecialCampaignItemsDiscountParts \" placeholder=\"\" ng-model=\"TambahSpecialCampaignItems_DiscountParts\" ng-maxlength=\"50\" ng-disabled=\"TambahSpecialCampaignItems_DiscountParts_isDisabled\"> </div> </div> <div class=\"col-md-8\"> <button id=\"TambahSpecialCampaignItems_TambahParts_Button\" ng-click=\"TambahSpecialItems_TambahParts_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\" ng-show=\"TambahSpecialCampaign_ViewMode_Show\"> <span class=\"ladda-label\"> Tambah Parts </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-12\"> <div id=\"myGrid\" ui-grid=\"TambahSpecialCampaignParts_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:98%; height: 250px; margin-top:20px; margin-bottom:20px; margin-left:10px\"></div> </div> </div> </div> <div class=\"ui modal\" id=\"ModalEditSpecialCampaignItemsJasa\" role=\"dialog\"> <div class=\"modal-dialog\"> <div class=\"modal-header\"> <label> <h3> <strong>Edit Jasa</strong> </h3> </label> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px; margin-top: 20px\"> <div class=\"col-md-3\" style=\"margin-top: 10px\"> Nama Jasa </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" name=\"EditNamaJasa\" ng-model=\"ModalEditSpecialCampaignItemsJasa_NamaJasa\" placeholder=\"Nama Jasa\" ng-disabled=\"true\"> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-3\" style=\"margin-top: 10px\"> Deskripsi Jasa </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" name=\"EditDeskripsiJasa\" ng-model=\"ModalEditSpecialCampaignItemsJasa_DeskripsiJasa\" placeholder=\"Deskripsi\" ng-disabled=\"true\"> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 20px\"> <div class=\"col-md-3\" style=\"margin-top: 10px\"> Discount Jasa </div> <div class=\"col-md-6\"> <input type=\"number\" min=\"0\" class=\"form-control\" name=\"EditDiscountJasa\" ng-model=\"ModalEditSpecialCampaignItemsJasa_DiscountJasa\" placeholder=\"Diskon\"> </div> </div> <div class=\"modal-footer\"> <button ng-click=\"ModalEditSpecialCampaignItemsJasa_Simpan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" ng-click=\"ModalEditSpecialCampaignItemsJasa_Batal()\" class=\"btn btn-default\">Batal</button> </div> </div> </div> <div class=\"ui modal\" id=\"ModalEditSpecialCampaignItemsParts\" role=\"dialog\"> <div class=\"modal-dialog\"> <div class=\"modal-header\"> <label> <h3> <strong>Edit Parts</strong> </h3> </label> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px; margin-top: 20px\"> <div class=\"col-md-3\" style=\"margin-top: 10px\"> Nama Parts </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" name=\"EditNamaParts\" ng-model=\"ModalEditSpecialCampaignItemsParts_NamaParts\" placeholder=\"Nama Parts\" ng-disabled=\"true\"> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-3\" style=\"margin-top: 10px\"> No Parts </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" name=\"EditNoParts\" ng-model=\"ModalEditSpecialCampaignItemsParts_NoParts\" placeholder=\"No Parts\" ng-disabled=\"true\"> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <div class=\"col-md-3\" style=\"margin-top: 10px\"> Deskripsi Parts </div> <div class=\"col-md-6\"> <input type=\"text\" class=\"form-control\" name=\"EditDeskripsiParts\" ng-model=\"ModalEditSpecialCampaignItemsParts_DeskripsiParts\" placeholder=\"Deskripsi\" ng-disabled=\"true\"> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 20px\"> <div class=\"col-md-3\" style=\"margin-top: 10px\"> Discount Parts </div> <div class=\"col-md-6\"> <input type=\"number\" min=\"0\" class=\"form-control\" name=\"EditDiscountParts\" ng-model=\"ModalEditSpecialCampaignItemsParts_DiscountParts\" placeholder=\"Diskon\"> </div> </div> <div class=\"modal-footer\"> <button ng-click=\"ModalEditSpecialCampaignItemsParts_Simpan()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button type=\"button\" ng-click=\"ModalEditSpecialCampaignItemsParts_Batal()\" class=\"btn btn-default\">Batal</button> </div> </div> </div> <div class=\"ui modal\" id=\"ModalSpecialCampaignSearchParts\" role=\"dialog\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <bsreqlabel>Cari Parts</bsreqlabel> <div class=\"form-group\" ui-grid=\"SearchSpecialCampaignPartsGrid\" ui-grid-auto-resize ui-grid-selection ui-grid-pagination></div> <p align=\"center\"> <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignSearchParts_Batal()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> Batal </button> </p> </ul> </div> </div> <!-- <div class=\"ui modal\" id=\"ModalSpecialCampaignModelDelete\" role=\"dialog\">\r" +
    "\n" +
    "    <div class=\"modal-content\">\r" +
    "\n" +
    "        <ul class=\"list-group\">\r" +
    "\n" +
    "            <bslabel>Hapus Data</bslabel>\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                Anda yakin ingin menghapus {{ModalSpecialCampaignModelDelete_Data}} ?\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <p align=\"center\">\r" +
    "\n" +
    "                <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignModelDelete_Ya()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\tYa\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "                <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignModelDelete_Tidak()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\tTidak\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "            </p>\r" +
    "\n" +
    "        </ul>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <!-- <div class=\"ui modal\" id=\"ModalSpecialCampaignMainDelete\" role=\"dialog\">\r" +
    "\n" +
    "    <div class=\"modal-content\">\r" +
    "\n" +
    "        <ul class=\"list-group\">\r" +
    "\n" +
    "            <bslabel>Hapus Data</bslabel>\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                Anda yakin ingin menghapus {{ModalSpecialCampaignMainDelete_Data}} ?\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <p align=\"center\">\r" +
    "\n" +
    "                <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignMainDelete_Ya()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\tYa\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "                <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignMainDelete_Tidak()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\tTidak\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "            </p>\r" +
    "\n" +
    "        </ul>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <!-- <div class=\"ui modal\" id=\"ModalSpecialCampaignJasaDelete\" role=\"dialog\">\r" +
    "\n" +
    "    <div class=\"modal-content\">\r" +
    "\n" +
    "        <ul class=\"list-group\">\r" +
    "\n" +
    "            <bslabel>Hapus Data</bslabel>\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                Anda yakin ingin menghapus {{ModalSpecialCampaignJasaDelete_Data}} ?\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <p align=\"center\">\r" +
    "\n" +
    "                <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignJasaDelete_Ya()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\tYa\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "                <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignJasaDelete_Tidak()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\tTidak\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "            </p>\r" +
    "\n" +
    "        </ul>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <!-- <div class=\"ui modal\" id=\"ModalSpecialCampaignPartsDelete\" role=\"dialog\">\r" +
    "\n" +
    "    <div class=\"modal-content\">\r" +
    "\n" +
    "        <ul class=\"list-group\">\r" +
    "\n" +
    "            <bslabel>Hapus Data</bslabel>\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                Anda yakin ingin menghapus {{ModalSpecialCampaignPartsDelete_Data}} ?\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <p align=\"center\">\r" +
    "\n" +
    "                <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignPartsDelete_Ya()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\tYa\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "                <button class=\"col-sm-6\" ng-click=\"ModalSpecialCampaignPartsDelete_Tidak()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "\t\t\t\tTidak\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "            </p>\r" +
    "\n" +
    "        </ul>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <!-- MODAL UPLOAD SPECIAL CAMPAIGN  --> <div class=\"ui modal\" id=\"ModalUploadSpecialCampaignDiscount\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadSpecialCampaignDiscount\" onchange=\"angular.element(this).scope().loadXLSSpecialCampaignDiscount(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"MainSpecialCampaign_Upload_Button\" ng-click=\"UploadSpecialCampaignDiscount()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div> <script type=\"text/ng-template\" id=\"customTemplate2.html\"><a style=\"white-space:normal;\">\r" +
    "\n" +
    "        <span>{{match.model.TaskCode}}</span>\r" +
    "\n" +
    "    </a></script>"
  );


  $templateCache.put('app/master/AfterSales/stallBP/stallBP.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div ng-show=\"MainMasterStallBP_Show\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"MainMasterStallBP_Tambah_Button\" ng-click=\"MainMasterStallBP_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; height: 35px; margin-right:10px\"> <span class=\"ladda-label\"> <i class=\"fa fa-plus\" aria-hidden=\"true\" style=\"margin-right:5px\"></i> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-4\"> <div class=\"input-group\" style=\"margin-bottom:10px\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-change=\"test(GridApiStallBP.grid.columns[filterColIdx].filters[0].term)\" ng-model=\"GridApiStallBP.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\"></i> </span> </a> </li> </ul> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div id=\"myGrid\" ui-grid=\"StallBP_List_UIGrid\" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:99%;height: 400px\"></div> </div> </div> </div> <div ng-show=\"TambahMasterStallBP_Show\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"TambahMasterStallBP_Simpan_Button\" ng-click=\"TambahMasterStallBP_Simpan_Clicked()\" ng-hide=\"ViewMode == true\" type=\"button\" ng-disabled=\"stallBPFormInput.$invalid\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahMasterStallBP_Kembali_Button\" ng-click=\"TambahMasterStallBP_Kembali_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float:right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <br> <div ng-form=\"stallBPFormInput\" novalidate> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div class=\"col-md-2\"> <bsreqlabel>Jenis Stall</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect ng-model=\"TambahMasterStallBP_JenisStall\" data=\"optionsTambahMasterStallBP_JenisStall\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Jenis Stall\" icon=\"fa fa-search\" ng-disabled=\"ViewMode\" ng-required=\"true\"> </bsselect> </div> </div> <br> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div class=\"col-md-2\"> <bsreqlabel>Job Stall</bsreqlabel> </div> <div class=\"col-md-4\"> <bsselect ng-model=\"TambahMasterStallBP_JobStall\" data=\"optionsTambahMasterStallBP_JobStall\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Jenis Stall\" icon=\"fa fa-search\" ng-disabled=\"ViewMode\" ng-required=\"true\"> </bsselect> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:10px\"> <div class=\"col-md-2\"> <bsreqlabel>Nama Stall</bsreqlabel> </div> <div class=\"col-md-4\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahMasterStallBPNamaStall\" placeholder=\"\" ng-model=\"TambahMasterStallBP_NamaStall\" ng-maxlength=\"50\" ng-disabled=\"ViewMode\" ng-required=\"true\"> </div> </div> </div> <br><br> <br><br> <br><br> <!--\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\tTeknisi BP\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control \" name=\"TambahMasterStallBPTeknisiBP\" placeholder=\"\" ng-model=\"TambahMasterStallBP_TeknisiBP\"\r" +
    "\n" +
    "\t\t\t\t\t\t ng-maxlength=\"50\" />\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"TambahMasterStallBP_CariTeknisi_Button\"\r" +
    "\n" +
    "\t\t\t\t\tng-click=\"TambahMasterStallBP_CariTeknisi_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float:left;margin-top:5px;margin-right:5px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tCari\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t --> <!--\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\tGrup BP\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control \" name=\"TambahMasterStallBPGrupBP\" placeholder=\"\" ng-model=\"TambahMasterStallBP_GrupBP\"\r" +
    "\n" +
    "\t\t\t\t\t\t ng-maxlength=\"50\" />\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"TambahMasterStallBP_CariGrupBP_Button\"\r" +
    "\n" +
    "\t\t\t\t\tng-click=\"TambahMasterStallBP_CariGrupBP_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float:left;margin-top:5px;margin-right:5px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tCari\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t --> <!--\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\tKategori WO\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-4\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect ng-model=\"$scope.TambahMasterStallBP_KategoriWO\"\r" +
    "\n" +
    "\t\t\t\t\t data=\"optionsTambahMasterStallBP_KategoriWO\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Kategori WO\"\r" +
    "\n" +
    "\t\t\t\t\t icon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t --> <!--\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\tNama Vendor\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-4\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control \" name=\"TambahMasterStallBPNamaVendor\" placeholder=\"\" ng-model=\"TambahMasterStallBP_NamaVendor\"\r" +
    "\n" +
    "\t\t\t\t\t\t ng-maxlength=\"50\" />\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t --> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsreqlabel>Jam Tersedia</bsreqlabel> </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamTersedia\" placeholder=\"\" ng-model=\"TambahMasterStallBP_JamTersedia\" min=\"0\" max=\"23\" ng-disabled=\"ViewMode\" ng-required=\"true\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamTersedia.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsreqlabel>Jam Istirahat 1</bsreqlabel> </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat1JamAwal\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat1JamAwal\" min=\"0\" max=\"23\" ng-required=\"true\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat1JamAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Jam </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat1MenitAwal\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat1MenitAwal\" min=\"0\" max=\"59\" required ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat1MenitAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Menit </div> <div class=\"col-md-1\"> s/d </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat1JamAkhir\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat1JamAkhir\" min=\"0\" max=\"23\" required ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat1JamAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Jam </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat1MenitAkhir\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat1MenitAkhir\" min=\"0\" max=\"59\" required ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat1MenitAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Menit </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> Jam Istirahat 2 </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat2JamAwal\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat2JamAwal\" min=\"0\" max=\"23\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat2JamAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Jam </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat2MenitAwal\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat2MenitAwal\" min=\"0\" max=\"59\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat2MenitAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Menit </div> <div class=\"col-md-1\"> s/d </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat2JamAkhir\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat2JamAkhir\" min=\"0\" max=\"23\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat2JamAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Jam </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat2MenitAkhir\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat2MenitAkhir\" min=\"0\" max=\"59\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat2MenitAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Menit </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> Jam Istirahat 3 </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat3JamAwal\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat3JamAwal\" min=\"0\" max=\"23\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat3JamAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Jam </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat3MenitAwal\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat3MenitAwal\" min=\"0\" max=\"59\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat3MenitAwal.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Menit </div> <div class=\"col-md-1\"> s/d </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat3JamAkhir\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat3JamAkhir\" min=\"0\" max=\"23\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat3JamAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Jam </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamIstirahat3MenitAkhir\" placeholder=\"\" ng-model=\"TambahMasterStallBP_Istirahat3MenitAkhir\" min=\"0\" max=\"59\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamIstirahat3MenitAkhir.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Menit </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsreqlabel>Jam Buka Default</bsreqlabel> </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamBukaDefaultJam\" placeholder=\"\" ng-model=\"TambahMasterStallBP_JamBukaDefaultJam\" min=\"0\" max=\"23\" ng-required=\"true\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamBukaDefaultJam.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Jam </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamBukaDefaultMenit\" placeholder=\"\" ng-model=\"TambahMasterStallBP_JamBukaDefaultMenit\" min=\"0\" max=\"59\" required ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamBukaDefaultMenit.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Menit </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <bsreqlabel>Jam Tutup Default</bsreqlabel> </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamTutupDefaultJam\" placeholder=\"\" ng-model=\"TambahMasterStallBP_JamTutupDefaultJam\" min=\"0\" max=\"23\" ng-required=\"true\" ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamTutupDefaultJam.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Jam </div> <div class=\"col-md-1\"> <!-- <div class=\"input-icon-right \"> --> <input type=\"number\" class=\"form-control\" name=\"TambahMasterStallBPJamTutupDefaultMenit\" placeholder=\"\" ng-model=\"TambahMasterStallBP_JamTutupDefaultMenit\" min=\"0\" max=\"59\" required ng-disabled=\"ViewMode\"> <em ng-messages=\"stallBPFormInput.TambahMasterStallBPJamTutupDefaultMenit.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <!-- </div> --> </div> <div class=\"col-md-1\"> Menit </div> </div> <div class=\"col-md-12\"> <bsreqlabel>Status</bsreqlabel> <div class=\"ui-grid-cell-contents\" style=\"cursor:pointer\"> <div class=\"ui toggle checkbox\" style=\"margin:-5px 0 0\"> <!-- <input type=\"checkbox\" ng-model=\"mStallMst.StatusCode\" ng-true-value =1 ng-false-value =0 name=\"status\" ng-disabled=\"ViewMode\"> --> <input type=\"checkbox\" ng-model=\"TambahStatusCode\" ng-true-value=\"1\" ng-false-value=\"0\" name=\"status\" ng-disabled=\"ViewMode\"> <label> <span style=\"color:#2185D0\" ng-if=\"row.entity.StatusCode == 1\">Active</span> <span style=\"color:gray\" ng-if=\"row.entity.StatusCode == 0\">Inactive</span> </label> </div> </div> </div> </div> </div> </div> <!-- yap --> <div ng-include=\"'app/master/AfterSales/stallBP/stall_schedule.html'\" ng-if=\"JadwalStallBP_Show==true\"></div> <!-- end yap --> <div class=\"ui modal\" id=\"ModalSearchTeknisiBP\" role=\"dialog\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <bsreqlabel>Cari Teknisi BP</bsreqlabel> <div class=\"form-group\" ui-grid=\"TeknisiBPGrid\" ui-grid-auto-resize ui-grid-selection ui-grid-pagination></div> <p align=\"center\"> <button class=\"col-sm-6\" ng-click=\"ModalSearchTeknisiBP_Batal()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> Batal </button> </p> </ul> </div> </div> <div class=\"ui modal\" id=\"ModalSearchGrupBP\" role=\"dialog\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <bsreqlabel>Cari Grup BP</bsreqlabel> <div class=\"form-group\" ui-grid=\"GrupBPGrid\" ui-grid-auto-resize ui-grid-selection ui-grid-pagination></div> <p align=\"center\"> <button class=\"col-sm-6\" ng-click=\"ModalSearchGrupBP_Batal()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\"> Batal </button> </p> </ul> </div> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/stallBP/stall_schedule.html',
    "<div class=\"row\"> <div class=\"col-md-12\"> <button ng-click=\"saveJadwalStall()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float:right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button ng-click=\"backFromJadwalStall()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float:right;margin-top:5px;margin-right:5px\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"row\"> <div class=\"col-lg-6\"> <h1>Nama Stall : {{mStallMst.StallName}}</h1> </div> </div> <div ng-form=\"listTambah\"> <div class=\"row\"> <div class=\"col-lg-2\" style=\"margin-bottom:10px\"> <bsreqlabel>Jam Dari</bsreqlabel> <div uib-timepicker show-spinners=\"false\" name=\"FromTime\" ng-model=\"mStallMst.FromTime\" hour-step=\"1\" minute-step=\"10\" show-meridian=\"ismeridian\" required></div> </div> </div> <div class=\"row\"> <div class=\"col-lg-6\" style=\"margin-bottom:10px\"> <bsreqlabel>Tanggal Dari</bsreqlabel> <bsdatepicker name=\"FromDate\" date-options=\"dateOptions\" ng-model=\"mStallMst.FromDate\" required> </bsdatepicker> </div> </div> <div class=\"row\"> <div class=\"col-lg-2\" style=\"margin-bottom:10px\"> <bsreqlabel>Jam Sampai</bsreqlabel> <div uib-timepicker show-spinners=\"false\" name=\"ToTime\" ng-model=\"mStallMst.ToTime\" hour-step=\"1\" minute-step=\"10\" show-meridian=\"ismeridian\" required></div> </div> </div> <div class=\"row\" show-errors> <div class=\"col-lg-6\" style=\"margin-bottom:10px\"> <bsreqlabel>Tanggal Sampai</bsreqlabel> <bsdatepicker name=\"ToDate\" date-options=\"dateOptions\" ng-model=\"mStallMst.ToDate\" required> </bsdatepicker> </div> </div> <div class=\"row\" show-errors> <div class=\"col-lg-6\" style=\"margin-bottom:10px\"> <bsreqlabel>Catatan</bsreqlabel> <input type=\"textarea\" class=\"form-control\" ng-model=\"mStallMst.InactiveReason\" required> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom: 10px\"> <button class=\"rbtn btn\" ng-click=\"addJadwal(mStallMst)\" ng-disabled=\"listTambah.$invalid\">Tambah</button> <!-- <button class=\"rbtn btn\" ng-click=\"addGroupTechnician(mGroupBP)\">Batal</button> --> </div> </div> <div id=\"gridJadwal\" ui-grid=\"gridJadwal\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"gridJadwalTableHeight()\" style=\"margin-bottom:10px\"> <div class=\"ui-grid-nodata\" ng-show=\"!gridJadwal.data.length\">No data available</div> </div>"
  );


  $templateCache.put('app/master/AfterSales/taskListBP/taskListBP.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <style>.sk-three-bounce #loading-bar-spinner{\r" +
    "\n" +
    "\t\tposition: relative !important;\r" +
    "\n" +
    "\t\tdisplay: block;\r" +
    "\n" +
    "\t\ttop:0 !important;\r" +
    "\n" +
    "\t\tleft: 30% !important;\r" +
    "\n" +
    "\t}</style> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <bsform-grid ng-form=\"TaskListBP\" factory-name=\"TaskListBPFactory\" model=\"TaskListBPEdit\" model-id=\"TaskId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-show-detail=\"onShowDetail\" on-validate-save=\"onValidateSave\" on-before-new-mode=\"onBeforeNewMode\" show-advsearch=\"on\" custom-action-button-settings=\"actionButtonSettingsDetail\" grid-custom-action-button-template=\"gridActionTemplate\" grid-hide-action-column=\"true\" grid-custom-action-button-colwidth=\"120\" form-grid-api=\"formGridApi\" form-api=\"formApi\" form-name=\"TaskListBPForm\" form-title=\"Task List BP\" modal-title=\"Task List BP\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-bottom:50px\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Model Kendaraan</bsreqlabel> <bsselect ng-model=\"TaskListBPMain.ModelKendaraan\" data=\"optionsTaskListBPMain_ModelKendaraan\" item-text=\"name\" item-value=\"value\" placeholder=\"Select Model Kendaraan\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Nama Pekerjaan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text\" class=\"form-control\" name=\"ModelKendaraan \" placeholder=\"Operation Description\" ng-model=\"TaskListBPMain.OperationDescription\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Activity Type</bsreqlabel> <bsselect ng-model=\"TaskListBPMain.ActivityType\" data=\"optionsTaskListBPMain_ActivityType\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Select Activity Type\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> </div> </bsform-advsearch> <div class=\"no-add\"> <div class=\"row\"> <div> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-9\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"TaskListBPEdit_Simpan_Button\" ng-click=\"TaskListBP_Edit_Simpan_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" ng-disabled=\"tasklistBPFormInput.$invalid\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tSimpan\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"TaskListBP_Edit_Batal_Button\" ng-click=\"TaskListBP_Edit_Batal_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tBatal\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div> --> <div ng-form=\"tasklistBPFormInput\" novalidate> <div class=\"col-md-12\" style=\"border-bottom:2px solid #eeeeee\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>OperationNo</label> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text\" class=\"form-control\" name=\"OperationNumber \" placeholder=\"\" ng-model=\"TaskListBPEdit.OperationNo\" ng-maxlength=\"50\"> <em ng-messages=\"tasklistBPFormInput.OperationNumber.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> <div class=\"form-group\"> <bsreqlabel>Valid From</bsreqlabel> <bsdatepicker name=\"TaskListBPEditTanggalAwal\" date-options=\"dateOptions\" ng-model=\"TaskListBPEdit.TanggalAwal\"> </bsdatepicker> </div> <div class=\"form-group\"> <bsreqlabel>Valid To</bsreqlabel> <bsdatepicker name=\"TaskListBPEditTanggalAkhir\" date-options=\"dateOptions\" ng-model=\"TaskListBPEdit.TanggalAkhir\"> </bsdatepicker> </div> </div> <div class=\"col-md-3\"> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<bsreqlabel>Tipe Pekerjaan</bsreqlabel>\r" +
    "\n" +
    "\t\t\t\t\t\t\t<bsselect ng-model=\"TaskListBPEdit.TipePekerjaan\" data=\"optionsTaskListBPEdit_TipePekerjaan\" \r" +
    "\n" +
    "\t\t\t\t\t\t\t\titem-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Tipe Pekerjaan\" icon=\"fa fa-search\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t\t\t\t</div> --> <div class=\"form-group\"> <bsreqlabel>UOM</bsreqlabel> <bsselect ng-model=\"TaskListBPEdit.OperationUOM\" data=\"optionsTaskListBPEdit_OperationUOM\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"UOM\" icon=\"fa fa-search\" required> </bsselect> </div> <div class=\"form-group\"> <label>Warranty Rate</label> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"WarrantyRate \" placeholder=\"\" ng-model=\"TaskListBPEdit.WarrantyRate\" ng-maxlength=\"20\" ng-min=\"0\" min=\"0\"> <em ng-messages=\"tasklistBPFormInput.WarrantyRate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Nama Pekerjaan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-car\"></i> <input type=\"text\" class=\"form-control\" name=\"TaskName \" placeholder=\"\" ng-model=\"TaskListBPEdit.TaskName\" ng-maxlength=\"50\" required> <em ng-messages=\"tasklistBPFormInput.TaskName.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> <div class=\"form-group\"> <bsreqlabel>Model Kendaraan</bsreqlabel> <bsselect ng-model=\"TaskListBPEdit.Model\" data=\"optionsTaskListBPMain_ModelKendaraan\" item-text=\"name\" item-value=\"value\" placeholder=\"Select Model Kendaraan\" icon=\"fa fa-search\" on-select=\"onSelectModel(selected)\"> </bsselect> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Harga Jasa</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"ServiceRate \" placeholder=\"\" ng-model=\"TaskListBPEdit.ServiceRate\" ng-maxlength=\"20\" ng-min=\"0\" min=\"0\" required> <em ng-messages=\"tasklistBPFormInput.ServiceRate.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> <div class=\"form-group\"> <bsreqlabel>Full Model</bsreqlabel> <bsselect ng-model=\"TaskListBPEdit.FullModel\" data=\"optionsTaskListBPMain_FullModel\" item-text=\"name\" item-value=\"value\" placeholder=\"Select Full Model\" icon=\"fa fa-search\" ng-disabled=\"optionsTaskListBPMain_FullModel.length == 0\"> </bsselect> </div> </div> </div> <hr> <div class=\"col-md-12\" style=\"display: flex;align-items: center\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>No. Area</bsreqlabel> <bsselect ng-model=\"TaskListBPEdit.AreaId\" data=\"optionsTaskListBPEdit_OperationAreaId\" item-text=\"Name\" item-value=\"id\" on-select=\"value(selected)\" placeholder=\"Area\" icon=\"fa fa-search\" required> </bsselect> </div> <div class=\"form-group\"> <bsreqlabel>Metode Perbaikan</bsreqlabel> <bsselect ng-model=\"TaskListBPEdit.StatusVehicleId\" data=\"optionsTaskListBPEdit_OperationStatusVehicleId\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"cekMetodePerbaikan(selected)\" placeholder=\"StatusVehicle\" icon=\"fa fa-search\" required> </bsselect> </div> <div class=\"form-group\"> <label>Tingkat Kerusakan</label> <bsselect ng-model=\"TaskListBPEdit.PointId\" data=\"optionsTaskListBPEdit_OperationPointId\" item-text=\"Name\" item-value=\"MasterId\" on-select=\"value(selected)\" placeholder=\"PointId\" icon=\"fa fa-search\" ng-disabled=\"isStatusVehicleId\"> </bsselect> </div> </div> <div class=\"col-md-8\"> <img width=\"100%\" height=\"100%\" src=\"images/gambarWAC.png\"> </div> </div> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <fieldset> <legend>Time Estimation</legend> </fieldset> <div class=\"col-md-2\"> <div class=\"form-group\"> <bsreqlabel>Body Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control\" name=\"EstimasiBody \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiBody\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required> <em ng-messages=\"tasklistBPFormInput.EstimasiBody.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-2\"> <div class=\"form-group\"> <bsreqlabel>Putty Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control\" name=\"EstimasiPutty \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiPutty\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required> <em ng-messages=\"tasklistBPFormInput.EstimasiPutty.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-2\"> <div class=\"form-group\"> <bsreqlabel>Surfacer Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control\" name=\"EstimasiSurfacer \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiSurfacer\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required> <em ng-messages=\"tasklistBPFormInput.EstimasiSurfacer.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-2\"> <div class=\"form-group\"> <bsreqlabel>Spraying Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control\" name=\"EstimasiSpraying \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiSpraying\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required> <em ng-messages=\"tasklistBPFormInput.EstimasiSpraying.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-2\"> <div class=\"form-group\"> <bsreqlabel>Polishing Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control\" name=\"EstimasiPolishing \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiPolishing\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required> <em ng-messages=\"tasklistBPFormInput.EstimasiPolishing.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-2\"> <div class=\"form-group\"> <bsreqlabel>Reassembly Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control\" name=\"EstimasiReassembly \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiReassembly\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required> <em ng-messages=\"tasklistBPFormInput.EstimasiReassembly.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-2\"> <div class=\"form-group\"> <bsreqlabel>Final Inspection Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control\" name=\"EstimasiFI \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiFI\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required> <em ng-messages=\"tasklistBPFormInput.EstimasiFI.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"col-md-2\"> <div class=\"form-group\"> <bsreqlabel>Total Time</bsreqlabel> <div class=\"input-icon-right\"> <input ng-disabled=\"true\" skip-enable type=\"number\" class=\"form-control\" name=\"TotalEstimasi\" placeholder=\"\" ng-model=\"TaskListBPEdit.TotalEstimasi\"> </div> </div> </div> </div> <!-- <div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tWMI\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input ng-disabled=\"true\" skip-enable type=\"text\" class=\"form-control \" name=\"WMI \" placeholder=\"\" ng-model=\"TaskListBPEdit.WMI\" ng-maxlength=\"50\">\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.WMI.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tBody Time\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control \" name=\"EstimasiBody \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiBody\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.EstimasiBody.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tVDS\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input ng-disabled=\"true\" skip-enable type=\"text\" class=\"form-control \" name=\"VDS \" placeholder=\"\" ng-model=\"TaskListBPEdit.VDS\" ng-maxlength=\"50\">\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.VDS.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tPutty Time\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control \" name=\"EstimasiPutty \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiPutty\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.EstimasiPutty.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tOperation Number\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control \" name=\"OperationNumber \" placeholder=\"\" ng-model=\"TaskListBPEdit.OperationNumber\" ng-maxlength=\"50\">\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.OperationNumber.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tSurfacer Time\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control \" name=\"EstimasiSurfacer \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiSurfacer\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.EstimasiSurfacer.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tTask Name\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"text\" class=\"form-control \" name=\"TaskName \" placeholder=\"\" ng-model=\"TaskListBPEdit.TaskName\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.TaskName.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tSpraying Time\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control \" name=\"EstimasiSpraying \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiSpraying\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.EstimasiSpraying.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tTipe Pekerjaan\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect ng-model=\"TaskListBPEdit.TipePekerjaan\" data=\"optionsTaskListBPEdit_TipePekerjaan\" \r" +
    "\n" +
    "\t\t\t\t\t\titem-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Tipe Pekerjaan\" icon=\"fa fa-search\" required>\r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\t\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tPolishing Time\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control \" name=\"EstimasiPolishing \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiPolishing\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.EstimasiPolishing.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tService Rate\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" class=\"form-control \" name=\"ServiceRate \" placeholder=\"\" ng-model=\"TaskListBPEdit.ServiceRate\" ng-maxlength=\"20\" ng-min=\"0\" min=\"0\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.ServiceRate.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tReassembly Time\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control \" name=\"EstimasiReassembly \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiReassembly\" ng-maxlength=\"4\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.EstimasiReassembly.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tWarranty Rate\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" class=\"form-control \" name=\"WarrantyRate \" placeholder=\"\" ng-model=\"TaskListBPEdit.WarrantyRate\"  ng-maxlength=\"20\" ng-min=\"0\" min=\"0\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.WarrantyRate.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tFinal Inspection Time\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"number\" ng-change=\"calculateTotalTime()\" class=\"form-control \" name=\"EstimasiFI \" placeholder=\"\" ng-model=\"TaskListBPEdit.EstimasiFI\" ng-maxlength=\"4\" ng-min=\"0\" min=\"0\" required>\r" +
    "\n" +
    "\t\t\t\t\t\t<em ng-messages=\"tasklistBPFormInput.EstimasiFI.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t<p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p>\r" +
    "\n" +
    "\t\t\t\t\t\t</em>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tOperation UOM\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect ng-model=\"TaskListBPEdit.OperationUOM\" data=\"optionsTaskListBPEdit_OperationUOM\" \r" +
    "\n" +
    "\t\t\t\t\t\titem-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"UOM\" icon=\"fa fa-search\" required>\r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\t\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tTotal Time\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t\t<input ng-disabled=\"true\" skip-enable type=\"text\" class=\"form-control \" name=\"TotalEstimasi\" placeholder=\"\" ng-model=\"TaskListBPEdit.TotalEstimasi\" >\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tModel\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect \r" +
    "\n" +
    "\t\t\t\t\t\t ng-model=\"TaskListBPEdit.Model\" \r" +
    "\n" +
    "\t\t\t\t\t\t data=\"optionsTaskListBPMain_ModelKendaraan\" \r" +
    "\n" +
    "\t\t\t\t\t\t item-text=\"name\" \r" +
    "\n" +
    "\t\t\t\t\t\t item-value=\"value\" \r" +
    "\n" +
    "\t\t\t\t\t\t placeholder=\"Select Model Kendaraan\" \r" +
    "\n" +
    "\t\t\t\t\t\t icon=\"fa fa-search\"\r" +
    "\n" +
    "\t\t\t\t\t\t on-select=\"onSelectModel(selected)\"> \r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tPointId\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect ng-model=\"TaskListBPEdit.PointId\" data=\"optionsTaskListBPEdit_OperationPointId\" \r" +
    "\n" +
    "\t\t\t\t\t\titem-text=\"Name\" item-value=\"MasterId\" on-select=\"value(selected)\" placeholder=\"PointId\" icon=\"fa fa-search\" required>\r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\t\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tFull Model\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect \r" +
    "\n" +
    "\t\t\t\t\t\t ng-model=\"TaskListBPEdit.FullModel\" \r" +
    "\n" +
    "\t\t\t\t\t\t data=\"optionsTaskListBPMain_FullModel\" \r" +
    "\n" +
    "\t\t\t\t\t\t item-text=\"name\" \r" +
    "\n" +
    "\t\t\t\t\t\t item-value=\"value\" \r" +
    "\n" +
    "\t\t\t\t\t\t placeholder=\"Select Full Model\" \r" +
    "\n" +
    "\t\t\t\t\t\t icon=\"fa fa-search\"\r" +
    "\n" +
    "\t\t\t\t\t\t ng-disabled=\"optionsTaskListBPMain_FullModel.length == 0\"> \r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tArea\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect ng-model=\"TaskListBPEdit.AreaId\" data=\"optionsTaskListBPEdit_OperationAreaId\" \r" +
    "\n" +
    "\t\t\t\t\t\titem-text=\"Name\" item-value=\"id\" on-select=\"value(selected)\" placeholder=\"Area\" icon=\"fa fa-search\" required>\r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\t\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tValid From\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsdatepicker\r" +
    "\n" +
    "\t\t\t\t\t\tname=\"TaskListBPEditTanggalAwal\" date-options=\"dateOptions\" ng-model=\"TaskListBPEdit.TanggalAwal\">\r" +
    "\n" +
    "\t\t\t\t\t</bsdatepicker>\t\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tStatus Vehicle\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsselect ng-model=\"TaskListBPEdit.StatusVehicleId\" data=\"optionsTaskListBPEdit_OperationStatusVehicleId\" \r" +
    "\n" +
    "\t\t\t\t\t\titem-text=\"Name\" item-value=\"MasterId\" on-select=\"value(selected)\" placeholder=\"StatusVehicle\" icon=\"fa fa-search\" required>\r" +
    "\n" +
    "\t\t\t\t\t</bsselect>\t\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-1\">\r" +
    "\n" +
    "\t\t\t\t\tValid To\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<bsdatepicker\r" +
    "\n" +
    "\t\t\t\t\t\tname=\"TaskListBPEditTanggalAkhir\" date-options=\"dateOptions\" ng-model=\"TaskListBPEdit.TanggalAkhir\">\r" +
    "\n" +
    "\t\t\t\t\t</bsdatepicker>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</div> --> </div> </div> </div> </div></bsform-grid> <bsui-modal show=\"showUploadFileTaskBP\" title=\"Upload File\" data=\"dataExcel\" on-save=\"uploadExcel\" on-cancel=\"cancelUploadExcel\" button-settings=\"settingButton\" form-name=\"UploadExcelTasklist\" mode=\"modeModalUploadExcelBP\"> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadTaskListBP\" onchange=\"angular.element(this).scope().loadXLSTaskListBP(this)\" required> </div> </div> </bsui-modal> <!-- <div class=\"ui modal\" id=\"ModalUploadTaskListBP\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px;\">\r" +
    "\n" +
    "\t<div class=\"modal-content\" ng-form =\"UploadExcel\" novalidate > \r" +
    "\n" +
    "\t\t<ul class=\"list-group\">\t\t\t\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-12\" style=\"margin-top:40px;\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\" style=\"margin-top:12px\">\r" +
    "\n" +
    "\t\t\t\t\tUpload File\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\">\r" +
    "\n" +
    "\t\t\t\t\t<div class=\"input-icon-right\">\r" +
    "\n" +
    "\t\t\t\t\t\t<input type=\"file\" accept=\".xlsx\" ng-model=\"dataExcel\" id=\"UploadTaskListBP\" onchange=\"angular.element(this).scope().loadXLSTaskListBP(this)\" required>\r" +
    "\n" +
    "\t\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t\r" +
    "\n" +
    "\t\t\t\t<div class=\"col-md-2\">\r" +
    "\n" +
    "\t\t\t\t\t<button id=\"UploadTaskListBP_Upload_Button\" \r" +
    "\n" +
    "\t\t\t\t\tng-click=\"UploadTaskListBP_Upload_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t\t type=\"button\" ng-disable=\"UploadExcel.$invalid\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\">\r" +
    "\n" +
    "\t\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\t\tUpload\r" +
    "\n" +
    "\t\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</ul>\r" +
    "\n" +
    "\t</div>\r" +
    "\n" +
    "</div> -->"
  );


  $templateCache.put('app/master/AfterSales/taskListGR/taskListGR.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "        <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "        <span ng-bind-html=\"match.model.PartsName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "        <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "    </a></script> <style type=\"text/css\">.adding-margin{\r" +
    "\n" +
    "        margin-bottom:10px;\r" +
    "\n" +
    "    }</style> <div class=\"no-add\"> <div class=\"row\"> <div ng-show=\"TaskListGRMain_Show\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Model Kendaraan</label> <div class=\"col-sm-8 adding-margin\"> <bsselect ng-model=\"TaskListGRMain_ModelKendaraan\" data=\"optionsTaskListGRMain_ModelKendaraan\" item-text=\"name\" item-value=\"value\" placeholder=\"Select Model Kendaraan\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Operation Description</label> <div class=\"col-sm-8 adding-margin\"> <bsselect ng-model=\"TaskListGRMain_OperationDescription\" data=\"optionsTaskListGRMain_OperationDescription\" item-text=\"name\" item-value=\"name\" placeholder=\"Select Operation Description\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Tipe Pekerjaan</label> <div class=\"col-sm-8 adding-margin\"> <bsselect ng-model=\"TaskListGRMain_TipePekerjaan\" data=\"optionsTaskListGRMain_TipePekerjaan\" item-text=\"name\" item-value=\"value\" placeholder=\"Select Tipe Pekerjaan\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Operation Number</label> <div class=\"col-sm-8 adding-margin\"> <bsselect ng-model=\"TaskListGRMain_OperationNumber\" data=\"optionsTaskListGRMain_OperationNumber\" item-text=\"name\" item-value=\"name\" placeholder=\"Select Operation Number\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"col-md-12\"> <button id=\"TaskListGRMain_Tambah_Button\" ng-click=\"TaskListGRMain_Tambah_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TaskListGRMain_Download_Button\" ng-click=\"TaskListGRMain_Download_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Download Template </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TaskListGRMain_Upload_Button\" ng-click=\"TaskListGRMain_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Upload Dari Excel </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TaskListGRMain_Cari_Button\" ng-click=\"TaskListGRMain_Cari_Clicked(TaskListGRMain_ModelKendaraan)\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Cari </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\" style=\"margin-top:-10px\"> <div id=\"myGrid\" ui-grid=\"TaskListGRGrid\" ui-grid-auto-resize ui-grid-resize-columns ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 400px; margin-top:20px; margin-bottom:20px; margin-left:20px\"></div> <!-- <div ui-grid=\"gridPartsFront\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellNav ui-grid-pinning ui-grid-auto-resize class=\"grid\" style=\"width:95%;height: 400px; margin-top:20px; margin-bottom:20px; margin-left:20px\">\r" +
    "\n" +
    "                    <div class=\"ui-grid-nodata\" ng-if=\"!gridPartsFront.data.length && !loading\" ng-cloak>No data available</div>\r" +
    "\n" +
    "                    <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div>\r" +
    "\n" +
    "                </div> --> <div ui-grid=\"gridPartsDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" style=\"width:95%;height: 400px; margin-top:20px; margin-bottom:20px; margin-left:20px\"> <div class=\"ui-grid-nodata\" ng-if=\"!gridPartsDetail.data.length && !loading\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </div> </div> <div ng-show=\"TaskListGREdit_Show\"> <div class=\"col-md-12\"> <div class=\"col-md-9\"> <button id=\"TaskListGREdit_Simpan_Button\" ng-click=\"TaskListGR_Edit_Simpan_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> </div> <button id=\"TaskListGR_Edit_Batal_Button\" ng-click=\"TaskListGR_Edit_Batal_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"margin-top:20px\"> <span class=\"ladda-label\"> Batal </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <div class=\"col-md-7\" ng-show=\"TaskListGREdit_Show\"> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">WMI</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <div class=\"input-icon-left\"> <i class=\"fa fa-search\"></i> <input type=\"text\" class=\"form-control\" name=\"TaskListGREdit_WMI\" placeholder=\"\" ng-model=\"TaskListGREdit_WMI\" maxlength=\"20\"> </div> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">VDS</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <div class=\"input-icon-left\"> <i class=\"fa fa-search\"></i> <input type=\"text\" class=\"form-control\" name=\"TaskListGREdit_VDS\" placeholder=\"\" ng-model=\"TaskListGREdit_VDS\" maxlength=\"20\"> </div> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Operation Number</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <div class=\"input-icon-right\"> <!-- <input type=\"number\" class=\"form-control\" name=\"TaskListGREdit_OperationNumber\" placeholder=\"\" ng-model=\"TaskListGREdit_OperationNumber\" ng-maxlength=\"50\" ng-disabled=\"TaskListGREdit_OperationNumber\"> --> <!-- <input type=\"text\" class=\"form-control\" name=\"TaskListGREdit_OperationNumber\" placeholder=\"\" ng-model=\"TaskListGREdit_OperationNumber\" maxlength=\"20\" ng-disabled=\"\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"> --> <input type=\"text\" class=\"form-control\" name=\"TaskListGREdit_OperationNumber\" placeholder=\"\" ng-model=\"TaskListGREdit_OperationNumber\" maxlength=\"20\"> </div> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Operation Description</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TaskListGREdit_OperationDescription\" placeholder=\"\" ng-model=\"TaskListGREdit_OperationDescription\" maxlength=\"30\"> </div> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Activity Type</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <bsselect ng-model=\"TaskListGREdit_ActivityTypeCode\" data=\"optionsTaskListGREdit_ActivityTypeCode\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Activity Type Code\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Flat Rate</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"TaskListGREdit_FlatRate\" placeholder=\"\" ng-model=\"TaskListGREdit_FlatRate\" maxlength=\"20\"> </div> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Standard Actual Rate</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"TaskListGREdit_StandardActualRate\" placeholder=\"\" ng-model=\"TaskListGREdit_StandardActualRate\" maxlength=\"20\"> </div> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Operation UOM</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <bsselect ng-model=\"TaskListGREdit_OperationUOM\" data=\"optionsTaskListGREdit_OperationUOM\" item-text=\"name\" item-value=\"value\" on-select=\"selectedUOM(selected)\" placeholder=\"UOM\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Model</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <bsselect ng-model=\"TaskListGREdit_ModelName\" data=\"optionsTaskListGRMain_ModelKendaraan\" item-text=\"name\" item-value=\"value\" placeholder=\"Select Model Kendaraan\" icon=\"fa fa-search\" on-select=\"selectedModel(selected)\"> </bsselect> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Type</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <!-- <bsselect ng-model=\"TaskListGREdit_TypeName\" data=\"optionsTaskListGRMain_TypeKendaraan\" item-text=\"Description\" item-value=\"VehicleTypeId\" placeholder=\"Select Type Kendaraan\" icon=\"fa fa-search\" on-select=\"selectedType(selected)\" ng-disabled=\"disVehic\">\r" +
    "\n" +
    "                       </bsselect> --> <bsselect ng-model=\"TaskListGREdit_FullModelCode\" data=\"optionsTaskListGRMain_TypeKendaraan\" item-text=\"Description\" item-value=\"VehicleTypeId\" placeholder=\"Select Type Kendaraan\" icon=\"fa fa-search\" on-select=\"selectedType(selected)\" ng-disabled=\"disVehic\"> </bsselect> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Katashiki Code</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TaskListGREdit_KatashikiCode\" placeholder=\"\" ng-model=\"TaskListGREdit_KatashikiCode\" ng-maxlength=\"50\" ng-disabled=\"true\"> </div> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Kilometer</label> <div class=\"col-sm-8 adding-margin\"> <div class=\"input-icon-right\"> <input type=\"number\" min=\"0\" class=\"form-control\" name=\"TaskListGREdit_Kilometer\" placeholder=\"\" ng-model=\"TaskListGREdit_Kilometer\" ng-maxlength=\"50\"> </div> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Valid From</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <bsdatepicker name=\"TaskListGREditTanggalAwal\" date-options=\"dateOptions\" ng-model=\"TaskListGREdit_TanggalAwal\"> </bsdatepicker> </div> </div> <div class=\"form-group\"> <bsreqlabel class=\"col-sm-4 col-form-label\">Valid To</bsreqlabel> <div class=\"col-sm-8 adding-margin\"> <bsdatepicker name=\"TaskListGREditTanggalAkhir\" date-options=\"dateOptions\" ng-model=\"TaskListGREdit_TanggalAkhir\"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\" ng-show=\"TaskListGREdit_Show\"> <div ng-form=\"listPart\" novalidate class=\"col-md-12\" style=\"padding:10px;margin-bottom:0px;border:1px solid #e2e2e2;background-color:rgba(213,51,55,0.02)\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Tipe Material</label> <div class=\"col-sm-8 adding-margin\"> <bsselect name=\"tipe\" ng-model=\"mData.MaterialTypeId\" data=\"materialCategory\" item-text=\"Name\" item-value=\"MaterialTypeId\" placeholder=\"Pilih Tipe\" required> </bsselect> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Nomor Part</label> <div class=\"col-sm-8 adding-margin\"> <bstypeahead placeholder=\"Kode Parts\" name=\"noMaterial\" ng-model=\"mData.PartsCode\" get-data=\"getParts\" item-text=\"PartsCode\" ng-disabled=\"mData.MaterialTypeId == undefined || mData.MaterialTypeId == null\" ng-maxlength=\"250\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"onSelectParts\" on-noresult=\"onNoPartResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\" template-url=\"customTemplate.html\" icon=\"fa-id-card-o\"> </bstypeahead> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Nama Parts</label> <div class=\"col-sm-8 adding-margin\"> <input type=\"text\" class=\"form-control\" name=\"PartsName\" placeholder=\"\" ng-model=\"mData.PartsName\" ng-maxlength=\"50\" ng-disabled=\"true\" required> </div> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Quantity</label> <div class=\"col-sm-8 adding-margin\"> <input type=\"number\" min=\"1\" class=\"form-control\" name=\"Qty\" placeholder=\"\" ng-model=\"mData.Qty\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\"> <label class=\"col-sm-4 col-form-label\">Satuan</label> <div class=\"col-sm-8 adding-margin\"> <bsselect name=\"satuan\" ng-model=\"mData.SatuanId\" on-select=\"selectTypeUnitParts(selected)\" data=\"unitData\" item-text=\"Name\" item-value=\"MasterId\" ng-disabled=\"true\" required placeholder=\"Pilih Tipe\"> </bsselect> <!-- <input type=\"text\" class=\"form-control\" name=\"PartsName\" placeholder=\"\" ng-model=\"mData.Satuan\" ng-maxlength=\"50\" ng-disabled=\"true\"> --> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"btn-group pull-right\"> <button type=\"button\" ng-disabled=\"listPart.$invalid\" ng-click=\"addParts(mData)\" class=\"rbtn btn ng-binding ladda-button\"> <span class=\"ladda-label\"> Tambah Parts </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </div> </div> <div class=\"col-md-12\" ng-show=\"TaskListGREdit_Show\"> <div ui-grid=\"gridPartsDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" ng-if=\"!gridPartsDetail.data.length && !loading\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </div> </div> </div>  <div class=\"ui modal\" id=\"ModalUploadTaskListGR\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadTaskListGR\" onchange=\"angular.element(this).scope().loadXLS(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"UploadTaskListGR_Upload_Button\" ng-click=\"UploadTaskListGR_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div>"
  );


  $templateCache.put('app/master/AfterSales/vendor/masterVendor.html',
    "<link rel=\"stylesheet\" href=\"css/uistyle.css\"> <script type=\"text/javascript\" src=\"js/uiscript.js\"></script> <div class=\"no-add\"> <div ng-show=\"MainVendor_Show\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"border:1px solid lightgrey; padding: 10px 0 0 0; margin-top: 10px; margin-left:15px;background:white; width:95%\"> <p style=\"width:50px; margin-top:-18px; margin-left:10px; background:white\">Filter</p> <div class=\"col-md-3\" style=\"margin-bottom:10px\"> <bslabel>Kode Vendor</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"KodeVendor \" placeholder=\"\" ng-model=\"MainVendor_KodeVendor\" maxlength=\"20\"> </div> </div> <div class=\"col-md-3\"> <bslabel>Nama Vendor</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"NamaVendor \" placeholder=\"\" ng-model=\"MainVendor_NamaVendor\" maxlength=\"50\"> </div> </div> <div class=\"col-md-3\"> <bslabel>Tipe Vendor</bslabel> <bsselect ng-model=\"MainVendor_JenisVendor\" data=\"optionsMainVendorJenisVendor\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Tipe Vendor\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-3\"> <bslabel>Business Type</bslabel> <bsselect ng-model=\"MainVendor_BusinessType\" data=\"optionsMainVendorBusinessType\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Tipe Bisnis\" icon=\"fa fa-search\"> </bsselect> </div> <div class=\"col-md-10\"> <button id=\"MainVendor_HapusFilter_Button\" ng-click=\"MainVendor_HapusFilter_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left; margin-top:15px; margin-right:5px\"> <span class=\"ladda-label\"> Hapus Filter </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MainVendor_Filter_Button\" ng-click=\"MainVendor_Filter_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left; margin-top:15px; margin-right:5px\"> <span class=\"ladda-label\"> Filter </span><span class=\"ladda-spinner\"></span> </button> </div> </div> <!-- Tidak dipakai sementara --> <!-- <div class=\"col-md-12\" style=\"margin-top:20px\">\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t<div class=\"input-icon-right \">\r" +
    "\n" +
    "\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"TextCariData \" placeholder=\"\" ng-model=\"MainVendor_FilterText\" ng-maxlength=\"50 \">\r" +
    "\n" +
    "\t\t\t\t</div>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t<bsselect ng-model=\"MainVendor_NamaKolom\"\r" +
    "\n" +
    "\t\t\t\t data=\"optionsMainVendorNamaKolom\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Nama Kolom\"\r" +
    "\n" +
    "\t\t\t\t icon=\"fa fa-search\">\r" +
    "\n" +
    "\t\t\t\t</bsselect>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t\t<div class=\"col-md-3\">\r" +
    "\n" +
    "\t\t\t\t<button id=\"MainVendor_Cari_Button\" ng-click=\"MainVendor_Cari_Clicked()\"\r" +
    "\n" +
    "\t\t\t\t type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: left\">\r" +
    "\n" +
    "\t\t\t\t\t<span class=\"ladda-label\">\r" +
    "\n" +
    "\t\t\t\t\t\tCari\r" +
    "\n" +
    "\t\t\t\t\t</span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "\t\t\t\t</button>\r" +
    "\n" +
    "\t\t\t</div>\r" +
    "\n" +
    "\t\t</div> --> <div class=\"col-md-12\" style=\"margin-left:-2%\"> <button id=\"MainVendor_New_Button\" ng-click=\"MainVendor_New_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px\"> <span class=\"ladda-label\"> + Buat Vendor Baru </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MainVendor_Upload_Button\" ng-click=\"MainVendor_Upload_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> <button id=\"MainVendor_DownloadTemplate_Button\" ng-click=\"MainVendor_DownloadTemplate_Clicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px\"> <span class=\"ladda-label\"> Download Template </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <form class=\"form-inline\" role=\"form\">\r" +
    "\n" +
    "                            <div class=\"row filtersearch\" style=\"margin-right:0px;padding-bottom:5px;\">\r" +
    "\n" +
    "                                <div class=\"col-md-12\">\r" +
    "\n" +
    "                                    <div class=\"col-md-12\" style=\"padding-left:25px;\">\r" +
    "\n" +
    "                                        <div class=\"input-group\">\r" +
    "\n" +
    "                                            <div class=\"input-group-btn\" uib-dropdown>\r" +
    "\n" +
    "                                                <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\">\r" +
    "\n" +
    "                                                  Bulk Action&nbsp;<span class=\"caret\"></span>\r" +
    "\n" +
    "                                          </button>\r" +
    "\n" +
    "                                                <ul class=\"dropdown-menu pull-left\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\">\r" +
    "\n" +
    "                                                    <li role=\"menuitem\">\r" +
    "\n" +
    "                                                        <a href=\"#\" ng-click=\"filterBtnChange(col)\">Hapus\r" +
    "\n" +
    "                                                      </a>\r" +
    "\n" +
    "                                                    </li>\r" +
    "\n" +
    "                                                </ul>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                            <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"GridApiVendorList.grid.columns[filterColIdx].filters[0].term\">\r" +
    "\n" +
    "                                            <div class=\"input-group-btn\" uib-dropdown>\r" +
    "\n" +
    "                                                <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\">\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span>\r" +
    "\n" +
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</button>\r" +
    "\n" +
    "                                                <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\">\r" +
    "\n" +
    "                                                    <li role=\"menuitem\" ng-repeat=\"col in gridCols\">\r" +
    "\n" +
    "                                                        <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}}\r" +
    "\n" +
    "                                                            <span class=\"pull-right\">\r" +
    "\n" +
    "                                                            </span>\r" +
    "\n" +
    "                                                        </a>\r" +
    "\n" +
    "                                                    </li>\r" +
    "\n" +
    "                                                </ul>\r" +
    "\n" +
    "                                            </div>\r" +
    "\n" +
    "                                        </div>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </form> --> </div> </div> <div id=\"myGrid\" ui-grid=\"VendorList_UIGrid\" ui-grid-resize-columns ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:0px; margin-bottom:20px; margin-left:20px\"></div> </div> </div> </div> <div ng-show=\"TambahVendor_Show\"> <div class=\"row\"> <div class=\"col-md-12\"> <button id=\"TambahVendor_Simpan_Button\" ng-click=\"TambahVendor_Simpan_Clicked()\" ng-show=\"!TambahVendor_isDisable_ViewMode\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px\"> <span class=\"ladda-label\"> Simpan </span><span class=\"ladda-spinner\"></span> </button> <button id=\"TambahVendor_Kembali_Button\" ng-click=\"TambahVendor_Kembali_Clicked()\" type=\"button\" class=\"btn ng-binding ladda-button\" style=\"float: right; margin-top:10px\"> <span class=\"ladda-label\"> Kembali </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"col-md-3\"> <bsreqlabel>Kode Vendor</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorKodeVendor \" placeholder=\"\" ng-model=\"TambahVendor_KodeVendor\" maxlength=\"20\" ng-disabled=\"TambahVendor_isDisable_ViewMode || TambahVendor_BusinessUnit == 18\" required> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <!-- <div class=\"col-md-3\" ng-if='TambahVendor_BusinessUnit != 18 '> --> <div class=\"col-md-3\" ng-show=\"TambahVendor_BusinessUnit != 18\"> <bsreqlabel>Nama Vendor</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorNamaVendor\" placeholder=\"\" ng-model=\"TambahVendor_NamaVendor\" maxlength=\"50\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" required> </div> </div> <!-- <div class=\"col-md-3\" ng-if='TambahVendor_BusinessUnit == 18'> --> <div class=\"col-md-3\" ng-show=\"TambahVendor_BusinessUnit == 18\"> <bsreqlabel>Nama Vendor</bsreqlabel> <bsselect ng-model=\"TambahVendor_NamaVendor\" ng-change=\"TambahVendor_NamaVendor_Changed(TambahVendor_NamaVendor)\" data=\"optionsTambahVendorOutlet\" item-text=\"name\" item-value=\"name\" on-select=\"value(selected)\" placeholder=\"Nama Vendor\" icon=\"fa fa-search\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" required> </bsselect> </div> <div class=\"col-md-3\" hidden> <bsreqlabel>Vendor OutletId</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorVendorOutlet \" placeholder=\"\" ng-model=\"TambahVendor_VendorOutlet\" maxlength=\"50\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" required> </div> </div> <div class=\"col-md-3\"> <bsreqlabel>Provinsi</bsreqlabel> <bsselect ng-model=\"TambahVendor_Provinsi\" ng-change=\"TambahVendor_Provinsi_Changed()\" data=\"optionsTambahVendorProvinsi\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Provinsi\" icon=\"fa fa-search\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" required> </bsselect> </div> <div class=\"col-md-1\"> </div> <div class=\"col-md-3\"> <bsreqlabel>Business Unit</bsreqlabel> <bsselect ng-model=\"TambahVendor_BusinessUnit\" data=\"optionsTambahVendorBusinessUnit\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Business Unit\" icon=\"fa fa-search\" ng-change=\"TambahVendor_BusinessUnit_Changed()\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </bsselect> </div> </div> <br><br> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-3\"> <bsreqlabel>Alamat Kantor</bsreqlabel> <textarea type=\"text\" class=\"form-control\" name=\"TambahVendorAlamatKantor\" placeholder=\"\" ng-model=\"TambahVendor_AlamatKantor\" maxlength=\"100\" style=\"margin-bottom: 10px\" ng-disabled=\"TambahVendor_isDisable_ViewMode\">\r" +
    "\n" +
    "\t\t\t\t</textarea> </div> <div class=\"col-md-3\"> <bsreqlabel>Kota</bsreqlabel> <bsselect ng-model=\"TambahVendor_Kota\" data=\"optionsTambahVendorKota\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Kota\" icon=\"fa fa-search\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </bsselect> </div> <div class=\"col-md-1\"> </div> <div class=\"col-md-3\" ng-show=\"TambahVendorNotInternal_Show\"> <bsreqlabel>Tipe Vendor</bsreqlabel> <bsselect ng-model=\"TambahVendor_JenisVendor\" data=\"optionsTambahVendorJenisVendor\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Tipe Vendor\" icon=\"fa fa-search\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </bsselect> </div> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-3\"> <bsreqlabel>Nama NPWP</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorNamaNPWP \" placeholder=\"\" ng-model=\"TambahVendor_NamaNPWP\" maxlength=\"100\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" required> </div> </div> <div class=\"col-md-3\"> <bsreqlabel>Nomor NPWP</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorNPWP \" placeholder=\"\" ng-model=\"TambahVendor_NPWP\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" minlength=\"20\" maxlength=\"20\" ng-keypress=\"allowPattern($event,1)\" maskme=\"99.999.999.9-999.999\" required> </div> </div> <div class=\"col-md-1\"> </div> <div class=\"col-md-3\" ng-show=\"TambahVendorNotInternal_Show\" style=\"margin-top:10px\"> <bslabel>Term of Payment</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorTermofPayment \" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" placeholder=\"\" ng-model=\"TambahVendor_TermofPayment\" maxlength=\"9\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </div> </div> <div class=\"col-md-3\" ng-show=\"!TambahVendorNotInternal_Show\"> <input type=\"checkbox\" ng-model=\"TambahVendor_isBlocking\" ng-disabled=\"TambahVendor_isDisable_ViewMode\">Blocking Status </div> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-3\"> <bsreqlabel>Alamat NPWP</bsreqlabel> <textarea type=\"text\" class=\"form-control\" name=\"TambahVendorAlamatNPWP\" placeholder=\"\" ng-model=\"TambahVendor_NPWPAddress\" maxlength=\"250\" style=\"margin-bottom: 10px\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" required>\r" +
    "\n" +
    "\t\t\t\t</textarea> </div> <div class=\"col-md-3\"> <label>Kode Pos</label> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorKodePos \" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" placeholder=\"\" ng-model=\"TambahVendor_KodePos\" max=\"999999999\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" required> </div> </div> <div class=\"col-md-1\"> </div> <div class=\"col-md-3\" ng-show=\"TambahVendorNotInternal_Show\"> <input type=\"checkbox\" ng-model=\"TambahVendor_isPerformanceEvaluation\" ng-disabled=\"TambahVendor_isDisable_ViewMode\">Performance Evaluation <br> <input type=\"checkbox\" ng-show=\"TambahVendorNotInternal_Show\" ng-model=\"TambahVendor_isBlocking\" ng-disabled=\"TambahVendor_isDisable_ViewMode\">Blocking Status </div> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-3\"> <bslabel>Email</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorEmail\" placeholder=\"\" ng-model=\"TambahVendor_Email\" maxlength=\"50\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </div> </div> <div class=\"col-md-3\"> <bslabel>Telepon</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorTelepon\" placeholder=\"\" ng-model=\"TambahVendor_Telepon\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"20\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </div> </div> <div class=\"col-md-1\"> </div> <div class=\"col-md-3\"> <bsreqlabel class=\"control-label\">Metode Pembayaran</bsreqlabel> <bsselect ng-model=\"TambahVendor_JenisPayment\" data=\"PaymentMethodData\" item-text=\"Name\" item-value=\"PaymentMethodId\" placeholder=\"\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </bsselect> </div> <!-- <div class=\"col-md-3\" ng-show=\"TambahVendorNotInternal_Show\">\r" +
    "\n" +
    "\t\t\t\t<input type=\"checkbox\" ng-model=\"TambahVendor_isBlocking\" ng-disabled=\"TambahVendor_isDisable_ViewMode\">Blocking Status\r" +
    "\n" +
    "\t\t\t</div> --> </div> <div class=\"col-md-12\" style=\"margin-top:10px\"> <div class=\"col-md-3\"> <bslabel>Nomor Kontak Person</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorNomorKontakPerson\" placeholder=\"\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"TambahVendor_NomorKontakPerson\" maxlength=\"16\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </div> </div> <div class=\"col-md-3\"> <bslabel>Fax</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorNomorFax\" placeholder=\"\" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" ng-model=\"TambahVendor_Fax\" maxlength=\"20\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </div> </div> <div class=\"col-md-1\"> </div> <div class=\"col-md-3\" ng-show=\"TambahVendor_Jasa_Show\"> <bslabel>Tipe Sublet</bslabel> <bsselect ng-model=\"TambahVendor_TipeSublet\" data=\"optionsTambahVendorTipeSublet\" item-text=\"name\" item-value=\"value\" on-select=\"value(selected)\" placeholder=\"Tipe Sublet\" icon=\"fa fa-search\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </bsselect> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\" style=\"margin-top:10px\"> <div class=\"col-md-3\" style=\"margin-top:10px\"> <bslabel>Kontak Person</bslabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"TambahVendorKontakPerson\" placeholder=\"\" ng-model=\"TambahVendor_KontakPerson\" maxlength=\"30\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </div> </div> <div class=\"col-md-3\" style=\"margin-top:10px\"> <bslabel>Group List</bslabel> <bsselect ng-model=\" TambahVendor_GroupList\" data=\"optionsTambahVendorGroupList\" item-text=\"GroupName\" item-value=\"GroupId\" on-select=\"selectedGroup(selected)\" placeholder=\"Group List\" icon=\"fa fa-search\" ng-disabled=\"TambahVendor_isDisable_ViewMode\"> </bsselect> </div> <div class=\"col-md-1\"> </div> <div class=\"col-md-3\" style=\"margin-top:10px\"> <div> <bslabel>Prosentase Bagi Hasil (%)</bslabel> </div> <div class=\"input-icon-right col-md-6\" style=\"padding-left:0px; padding-right:0px\"> <input type=\"text \" class=\"form-control\" name=\"TambahVendorProsentase \" placeholder=\" \" ng-model=\"TambahVendor_Prosentase \" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\" maxlength=\"3\" ng-disabled=\"TambahVendor_isDisable_ViewMode \"> </div> </div> </div> <form name=\"outerForm\" class=\"tab-form-demo\"> <uib-tabset active=\"activeJustified\"> <uib-tab index=\"0\" heading=\"Satuan per Vendor per Material\" ng-show=\"TambahVendor_Satuan_Show \"> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Kode Material</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" name=\"TambahVendorKodeMaterial \" placeholder=\"Kode Material \" ng-model=\"mData.TambahVendor_KodeMaterial \" ng-maxlength=\"50 \" ng-disabled=\"TambahVendor_isDisable_ViewMode \"> </div> </div> <div class=\"col-md-1\"> <button id=\"TambahVendor_PartsCari_Button \" ng-click=\"TambahVendor_PartsCari_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left; margin-top:12px\"> <span class=\"ladda-label\"> Cari </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-5\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <!-- <input type=\"text \" class=\"form-control \" name=\"TambahVendorNamaMaterial \" placeholder=\"Nama Material \" ng-model=\"TambahVendor_NamaMaterial \" ng-maxlength=\"50 \" ng-disabled=\"TambahVendor_isDisable_ViewMode \"> --> <input type=\"text \" class=\"form-control\" name=\"TambahVendorNamaMaterial \" placeholder=\"Nama Material \" ng-model=\"Form.TambahVendor_NamaMaterial \" ng-maxlength=\"50 \" ng-disabled=\"true \"> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <!-- <div class=\"col-md-2 \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div> --> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Satuan Dasar</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <!--<input type=\"text \" class=\"form-control \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorSatuanDasar \" placeholder=\"Satuan Dasar \" ng-model=\"TambahVendor_SatuanDasar \" ng-maxlength=\"50 \">--> <select class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorSatuanDasar \" ng-model=\"Form.TambahVendor_SatuanDasar \" data=\"MasterVendorUoMList \" placeholder=\"Pilih Satuan Dasar\r" +
    "\n" +
    "                            \" ng-change=\"selectTypeSatuan(Form.TambahVendor_SatuanDasar) \"> <option ng-repeat=\"data in MasterVendorUoMList \" value=\"{{data.value}} \">{{data.name}}</option> </select> </div> </div> <div class=\"col-md-1\"> </div> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Default Satuan</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <!--<input type=\"text \" class=\"form-control \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorDefaultSatuan \" placeholder=\" \" ng-model=\"Form.TambahVendor_DefaultSatuan \" ng-maxlength=\"50 \">--> <select class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode || TambahVendor_Satuan_Show \" name=\"TambahVendorDefaultSatuan \" ng-model=\"Form.TambahVendor_DefaultSatuan \" data=\"MasterVendorUoMList\r" +
    "\n" +
    "                            \" placeholder=\"Pilih Default Satuan \" ng-change=\"selectTypeSatuanDefault(Form.TambahVendor_DefaultSatuan) \"> <option ng-repeat=\"data in MasterVendorUoMList \" value=\"{{data.value}} \">{{data.name}}</option> </select> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 25px\"> <button id=\"TambahVendor_TambahParts_Button \" ng-click=\"TambahVendor_TambahParts_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:10px\"> <span class=\"ladda-label\"> Tambah Parts </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" ui-grid=\"PartsList_UIGrid \" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:10px; margin-bottom:20px;\r" +
    "\n" +
    "                            margin-left:20px\"></div> </div> </uib-tab> <uib-tab index=\"1\" heading=\"List Jasa\" ng-show=\"TambahVendor_Jasa_Show \"> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Kode Jasa</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" class=\"form-control\" name=\"TambahVendorKodeJasa \" placeholder=\"Kode Jasa \" ng-model=\"Form.TambahVendor_KodeJasa \" maxlength=\"8\"> </div> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> <bslabel>Model</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <bsselect ng-model=\"Form.TambahVendor_Model \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" data=\"optionsTambahVendorModel \" item-text=\"name \" item-value=\"value \" on-select=\"MasterVendorGetVFullModel(selected) \" placeholder=\"Model\r" +
    "\n" +
    "                            \" icon=\"fa fa-search \"> </bsselect> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <!-- <div class=\"col-md-2 \" style=\"margin-top:18px \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div> --> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Nama Pekerjaan</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorNamaPekerjaan \" placeholder=\"Nama Pekerjaan \" ng-model=\"Form.TambahVendor_NamaPekerjaan \" maxlength=\"50\"> </div> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> <bslabel>Tipe Jasa</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <bsselect ng-model=\"Form.TambahVendor_TipeJasa \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" data=\"optionsTambahVendor_TipeJasa \" item-text=\"name \" item-value=\"value \" on-select=\"value(selected) \" placeholder=\"Tipe Jasa\r" +
    "\n" +
    "                            \" icon=\"fa fa-search \"> </bsselect> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 25px\"> <button id=\"TambahVendor_TambahJasa_Button \" ng-click=\"TambahVendor_TambahJasa_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:10px\"> <span class=\"ladda-label\"> Tambah Jasa </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid \" ui-grid=\"JasaList_UIGrid \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:10px; margin-bottom:20px;\r" +
    "\n" +
    "                            margin-left:20px\"></div> </div> </uib-tab> <uib-tab index=\"2\" heading=\"List Karoseri\" ng-show=\"TambahVendor_Karoseri_Show \"> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Model</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <bsselect ng-model=\"Form.TambahVendor_KaroseriModel \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" data=\"optionsTambahVendorKaroseriModel \" item-text=\"name \" item-value=\"value \" on-select=\"MasterVendorGetVFullModel(selected)\r" +
    "\n" +
    "                            \" placeholder=\"Model \" icon=\"fa fa-search \"> </bsselect> </div> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Tipe Karoseri</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <bsselect ng-model=\"Form.TambahVendor_TipeKaroseri \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" data=\"optionsTipeKaroseri \" item-text=\"KaroseriName \" item-value=\"KaroseriId \" on-select=\"value(selected) \" placeholder=\"Tipe\r" +
    "\n" +
    "                            Karoseri \" icon=\"fa fa-search \"> </bsselect> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Tipe Kendaraan</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <bsselect ng-model=\"Form.TambahVendor_KaroseriFullModel \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" data=\"optionsTambahVendorKaroseriFullModel \" item-text=\"name \" item-value=\"value \" on-select=\"value(selected)\r" +
    "\n" +
    "                            \" placeholder=\"Tipe Kendaraan \" icon=\"fa fa-search \"> </bsselect> </div> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Deskripsi</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" data=\"optionsTambahDeskripsiKaroseriModel\" name=\"TambahVendorKaroseriDeskripsi \" placeholder=\" \" ng-model=\"Form.TambahVendor_KaroseriDeskripsi \" maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> </div> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Kode Karoseri</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorKaroseriCode \" placeholder=\" \" ng-model=\"Form.TambahVendor_KaroseriCode \" maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-2\"> <button id=\"TambahVendor_TambahKaroseri_Button \" ng-click=\"TambahVendor_TambahKaroseri_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:10px\"> <span class=\"ladda-label\"> Tambah Karoseri </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid \" ui-grid=\"KaroseriList_UIGrid \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:10px; margin-bottom:20px;\r" +
    "\n" +
    "                            margin-left:20px\"></div> </div> </uib-tab> <uib-tab index=\"3\" heading=\"Informasi Bank\" ng-show=\"TambahVendor_Bank_Show \"> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Nama Bank</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <bsselect ng-model=\"Form.TambahVendor_NamaBank \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" data=\"optionsTambahVendorNamaBank \" item-text=\"name \" item-value=\"value \" on-select=\"value(selected) \" placeholder=\"Nama Bank\r" +
    "\n" +
    "                            \" icon=\"fa fa-search \"> </bsselect> </div> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Atas nama Bank</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorAtasNamaBank \" placeholder=\" \" ng-model=\"Form.TambahVendor_AtasNamaBank \" maxlength=\"50\"> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Nomor Rekening</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorNomorRekening \" placeholder=\"Nomor Rekening \" ng-model=\"Form.TambahVendor_NomorRekening \" maxlength=\"20\"> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 25px\"> <button id=\"TambahVendor_TambahBank_Button \" ng-click=\"TambahVendor_TambahBank_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:10px\"> <span class=\"ladda-label\"> Tambah Bank </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid \" ui-grid=\"BankAccountList_UIGrid \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:10px; margin-bottom:20px;\r" +
    "\n" +
    "                            margin-left:20px\"></div> </div> </uib-tab> <uib-tab index=\"4\" heading=\"Harga Beli & Jual\" ng-show=\"TambahVendor_SatuanJB_Show \"> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>No Material</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorNoMaterial \" placeholder=\"Kode Material \" ng-model=\"Form.TambahVendor_NoMaterialJB \" ng-maxlength=\"50 \"> </div> </div> <div class=\"col-md-1\"> <button id=\"TambahVendor_CariPartsJB_Button \" ng-click=\"TambahVendor_CariPartsJB_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left; margin-top:10px\"> <span class=\"ladda-label\"> Cari </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> <bslabel>Harga Beli</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorHargaBeli \" placeholder=\" \" ng-model=\"Form.TambahVendor_HargaBeli \" maxlength=\"50\" currency-formating number-only> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <!-- <div class=\"col-md-1 \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div> --> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Nama Material</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <!-- <input type=\"text \" class=\"form-control \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorNamaMaterialJB \" placeholder=\" \" ng-model=\"TambahVendor_NamaMaterialJB \" ng-maxlength=\"50 \"> --> <input type=\"text \" class=\"form-control\" ng-disabled=\"true \" name=\"TambahVendorNamaMaterialJB \" placeholder=\" \" ng-model=\"Form.TambahVendor_NamaMaterialJB \" ng-maxlength=\"50 \"> </div> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> <bslabel>Harga Jual</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorHargaJual \" placeholder=\" \" ng-model=\"Form.TambahVendor_HargaJual \" ng-maxlength=\"50 \" currency-formating number-only> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <!-- <div class=\"col-md-1 \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div> --> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Tanggal Berlaku</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahVendorTanggalAwal \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" date-options=\"dateOptions \" ng-model=\"Form.TambahVendor_TanggalAwal \"> </bsdatepicker> </div> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> s/d </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahVendorTanggalAkhir \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" date-options=\"dateOptions \" ng-model=\"Form.TambahVendor_TanggalAkhir \"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 25px\"> <button id=\"TambahVendor_TambahPartsJB_Button \" ng-click=\"TambahVendor_TambahPartsJB_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:10px\"> <span class=\"ladda-label\"> Tambah Parts </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid \" ui-grid=\"PartsJBList_UIGrid \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:10px; margin-bottom:20px;\r" +
    "\n" +
    "                            margin-left:20px\"></div> </div> </uib-tab> <uib-tab index=\"5\" heading=\"Harga Beli & Jual\" ng-show=\"TambahVendor_JasaJB_Show \"> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Kode Jasa</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <!-- <input type=\"text\" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode\" name=\"TambahVendorKodeJasaJB\" placeholder=\"Kode Jasa\" ng-model=\"Form.TambahVendor_KodeJasaJB\" maxlength=\"8\"> --> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" name=\"TambahVendorKodeJasaJB\" placeholder=\"Kode Jasa\" ng-model=\"Form.TambahVendor_KodeJasaJB\" maxlength=\"8\"> </div> </div> <div class=\"col-md-1\"> <button id=\"TambahVendor_CariJasaJB_Button \" ng-click=\"TambahVendor_CariJasaJB_Clicked()\" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left; margin-top:10px\"> <span class=\"ladda-label\"> Cari </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> <bslabel>Harga Beli</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorHargaBeliJasa \" placeholder=\" \" ng-model=\"Form.TambahVendor_HargaBeliJasa \" ng-maxlength=\"50 \" currency-formating number-only> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <!-- <div class=\"col-md-1 \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div> --> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Nama Jasa</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorNamaJasaJB \" placeholder=\" \" ng-model=\"Form.TambahVendor_NamaJasaJB \" ng-maxlength=\"50 \"> </div> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> <bslabel>Harga Jual</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorHargaJualJasa \" placeholder=\" \" ng-model=\"Form.TambahVendor_HargaJualJasa \" ng-maxlength=\"50 \" currency-formating number-only> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 10px\"> <!-- <div class=\"col-md-1 \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div> --> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Tanggal Berlaku</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahVendorTanggalAwalJasaJB \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" date-options=\"dateOptions \" ng-model=\"Form.TambahVendor_TanggalAwalJasaJB \"> </bsdatepicker> </div> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> s/d </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahVendorTanggalAkhirJasaJB \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" date-options=\"dateOptions \" ng-model=\"Form.TambahVendor_TanggalAkhirJasaJB \"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px;margin-left: 25px\"> <button id=\"TambahVendor_TambahJasaJB_Button \" ng-click=\"TambahVendor_TambahJasaJB_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:10px\"> <span class=\"ladda-label\"> Tambah Harga </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid \" ui-grid=\"JasaJBList_UIGrid \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:10px; margin-bottom:20px;\r" +
    "\n" +
    "                            margin-left:20px\"></div> </div> </uib-tab> <uib-tab index=\"6\" heading=\"Harga Beli & Jual\" ng-show=\"TambahVendor_KaroseriJB_Show \"> <div class=\"col-md-12\" style=\"margin-bottom:20px ;margin-left: 10px\"> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Kode Karoseri</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <bsselect ng-model=\"Form.TambahVendor_KodeKaroseriJB \" ng-change=\"TambahVendor_KodeKaroseriJB_Changed() \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" data=\"optionsTambahVendorKodeKaroseriJB \" item-text=\"name \" item-value=\"value\r" +
    "\n" +
    "                            \" on-select=\"value(selected) \" placeholder=\"Kode Karoseri \" icon=\"fa fa-search \"> </bsselect> </div> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Harga Beli</bslabel> </div> <div class=\"col-md-2\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorHargaBeliKaroseri \" placeholder=\" \" ng-model=\"Form.TambahVendor_HargaBeliKaroseri \" ng-maxlength=\"50 \" currency-formating number-only> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px ;margin-left: 10px\"> <!-- <div class=\"col-md-1 \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div> --> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Tipe Karoseri</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" skip-enable disabled name=\"TambahVendorTipeKaroseriJB \" placeholder=\" \" ng-model=\"Form.TambahVendor_TipeKaroseriJB \" ng-maxlength=\"50 \"> </div> </div> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Harga Jual</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <input type=\"text \" class=\"form-control\" ng-disabled=\"TambahVendor_isDisable_ViewMode \" name=\"TambahVendorHargaJualKaroseri \" placeholder=\" \" ng-model=\"Form.TambahVendor_HargaJualKaroseri \" ng-maxlength=\"50 \" currency-formating number-only> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px ;margin-left: 10px\"> <!-- <div class=\"col-md-1 \">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            </div> --> <div class=\"col-md-2\" style=\"margin-top:18px\"> <bslabel>Tanggal Berlaku</bslabel> </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahVendorTanggalAwalKaroseri \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" date-options=\"dateOptions \" ng-model=\"Form.TambahVendor_TanggalAwalKaroseri \"> </bsdatepicker> </div> </div> <div class=\"col-md-1\" style=\"margin-top:18px\"> s/d </div> <div class=\"col-md-3\" style=\"margin-top:12px\"> <div class=\"input-icon-right\"> <bsdatepicker name=\"TambahVendorTanggalAkhirKaroseri \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" date-options=\"dateOptions \" ng-model=\"Form.TambahVendor_TanggalAkhirKaroseri \"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom:20px ;margin-left: 25px\"> <button id=\"TambahVendor_TambahKaroseriJB_Button \" ng-click=\"TambahVendor_TambahKaroseriJB_Clicked() \" ng-show=\"!TambahVendor_isDisable_ViewMode \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:10px\"> <span class=\"ladda-label\"> Tambah Harga </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div id=\"myGrid \" ui-grid=\"KaroseriJBList_UIGrid \" ng-disabled=\"TambahVendor_isDisable_ViewMode \" ui-grid-auto-resize ui-grid-pagination ui-grid-selection class=\"Mygrid\" style=\"width:95%;height: 250px; margin-top:60px; margin-bottom:20px;\r" +
    "\n" +
    "                            margin-left:20px\"></div> </div> </uib-tab> </uib-tabset> </form> </div> </div> </div> <bsui-modal show=\"ModalTambahVendorSearchParts\" title=\"Cari Parts dan Bahan\" data=\"mData\" on-cancel=\"ModalTambahVendorSearchParts_Batal()\" mode=\"modalMode\"> <!-- <div class=\"ui modal \" id=\"ModalTambahVendorSearchParts \" role=\"dialog \">\r" +
    "\n" +
    "    <div class=\"modal-content \"> --> <ul class=\"list-group\"> <bsreqlabel>Cari Parts dan Bahan</bsreqlabel> <div class=\"form-group\" style=\"margin-top:40px\"> <div ui-grid=\"ModalTambahVendorSearchParts_UIGrid\" ui-grid-auto-resize ui-grid-selection ui-grid-pagination> </div> </div> <!-- <p align=\"center \">\r" +
    "\n" +
    "                <button class=\"col-sm-6 \" ng-click=\"ModalTambahVendorSearchParts_Batal() \" type=\"button \" class=\"rbtn btn ng-binding ladda-button \">\r" +
    "\n" +
    "\t\t\t\tBatal\r" +
    "\n" +
    "\t\t\t</button>\r" +
    "\n" +
    "            </p> --> </ul> <!-- </div>\r" +
    "\n" +
    "</div> --> </bsui-modal> <bsui-modal show=\"ModalTambahVendorSearchJasa\" title=\"Cari Jasa\" data=\"MasterVendorJasaData\" on-save=\"SaveCariJasa()\" on-cancel=\"ModalTambahVendorSearchJasa_Batal()\" mode=\"modalMode\"> <!-- <div class=\"ui modal \" id=\"ModalTambahVendorSearchJasa \" role=\"dialog \"> --> <!-- <div class=\"modal-content \"> --> <ul class=\"list-group\"> <div> <bsreqlabel>Cari Jasa</bsreqlabel> </div> <div class=\"form-group\"> <div class=\"col-md-4\" style=\"margin-bottom:0px\"> <div class=\"input-group\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-change=\"test(grid3Api.grid.columns[filterColIdx].filters[0].term)\" ng-model=\"grid3Api.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\"></i> </span> </a> </li> </ul> </div> </div> </div> </div> <div class=\"form-group\" style=\"margin-top:40px\"> <div ui-grid=\"ModalTambahVendorSearchJasa_UIGrid\" ui-grid-auto-resize ui-grid-selection ui-grid-pagination></div> <!-- <p align=\"center\">\r" +
    "\n" +
    "                            <button class=\"col-sm-6\" ng-click=\"ModalTambahVendorSearchJasa_Batal()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\">\r" +
    "\n" +
    "                            Batal\r" +
    "\n" +
    "                            </button>\r" +
    "\n" +
    "                        </p> --> </div> </ul> <!-- </div> --> <!-- </div> --> </bsui-modal> <bsui-modal show=\"ModalTambahVendorPartsUoMConversion\" title=\"Tambah\" data=\"modal_model\" on-save=\"ModalTambahVendorPartsUoMConversion_Simpan()\" on-cancel=\"ModalTambahVendorPartsUoMConversion_Kembali()\" mode=\"modalMode\"> <!-- <div class=\"ui modal \" id=\"ModalTambahVendorPartsUoMConversion \" role=\"dialog \">\r" +
    "\n" +
    "    <div class=\"modal-content \"> --> <ul class=\"list-group\"> <div class=\"row\" style=\"margin-top:20px\"> <div class=\"col-md-12\"> {{ModalTambahVendorPartsUoMConversion_KodeParts}} </div> <div class=\"col-md-12\"> <button id=\"ModalTambahVendorPartsUoMConversion_Tambah_Button \" ng-click=\"ModalTambahVendorPartsUoMConversion_Tambah_Clicked() \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:10px\"> <span class=\"ladda-label\"> Tambah </span><span class=\"ladda-spinner\"></span> </button> </div> <div class=\"col-md-12\"> <div class=\"form-group\" ui-grid=\"ModalTambahVendorPartsUoMConversionUIGrid \" ui-grid-edit ui-grid-auto-resize ui-grid-selection ui-grid-pagination></div> </div> <!-- <div class=\"col-md-12 \">\r" +
    "\n" +
    "                    <p align=\"center \">\r" +
    "\n" +
    "                        <button class=\"col-sm-6 \" ng-click=\"ModalTambahVendorPartsUoMConversion_Kembali() \" type=\"button \" class=\"rbtn btn ng-binding ladda-button \">\r" +
    "\n" +
    "\t\t\t\t\tKembali\r" +
    "\n" +
    "\t\t\t\t</button>\r" +
    "\n" +
    "                        <button class=\"col-sm-6 \" ng-click=\"ModalTambahVendorPartsUoMConversion_Simpan() \" type=\"button \" class=\"rbtn btn ng-binding ladda-button \">\r" +
    "\n" +
    "\t\t\t\t\tSimpan\r" +
    "\n" +
    "\t\t\t\t</button>\r" +
    "\n" +
    "                    </p>\r" +
    "\n" +
    "                </div> --> </div> </ul> <!-- </div>\r" +
    "\n" +
    "</div> --> </bsui-modal> <bsui-modal show=\"ModalUploadMasterVendor\" title=\"Upload File\" data=\"modal_model\" on-save=\"SaveUploadFile()\" on-cancel=\"CancelUploadFile()\" mode=\"modalMode\"> <!-- <div class=\"ui modal \" id=\"ModalUploadMasterVendor \" role=\"dialog \" style=\"margin-top: -50px; height: 100px; width:800px; \">\r" +
    "\n" +
    "                <div class=\"modal-content \"> --> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadMasterVendor\" class=\"form-control-file border\" onchange=\"angular.element(this).scope().loadXLSMasterVendor(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"UploadMasterVendor_Upload_Button\" ng-click=\"UploadMasterVendor_Upload_Clicked() \" type=\"button \" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> <!-- </div>\r" +
    "\n" +
    "            </div> --> </bsui-modal>"
  );


  $templateCache.put('app/master/LTDelivery/ltDelivery.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"OrderLeadTimeForm\" factory-name=\"OrderLeadTime\" model=\"mOrderLeadTime\" loading=\"loading\" get-data=\"getData\" on-validate-save=\"onValidateSave\" selected-rows=\"xOrderLeadTime.selected\" on-before-new=\"beforeNew\" on-before-edit=\"beforeEdit\" do-custom-save=\"doCustomSave\" on-show-detail=\"onShowDetail\" on-select-rows=\"onSelectRows\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Berlaku</bsreqlabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"EffectiveDate\" date-options=\"dateOptions\" ng-model=\"saveEffDate.EffectiveDate\" required> <!-- {{mOrderLeadTime.EffectiveDate}} --> </bsdatepicker> </div> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bsreqlabel>Berlaku Sampai</bsreqlabel> <div class=\"input-icon-right\"> <bsdatepicker name=\"ValidThru\" date-options=\"dateOptions\" ng-model=\"saveEffDate.ValidThru\" required> <!-- {{mOrderLeadTime.EffectiveDate}} --> </bsdatepicker> </div> </div> </div> </div> <!-- <pre>{{saveEffDate.EffectiveDate}}</pre> --> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/OPL/OPL.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.right-align {\r" +
    "\n" +
    "    text-align: right;\r" +
    "\n" +
    "}</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"OplForm\" factory-name=\"OplFactory\" model=\"mOpl\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"OplForm\" form-title=\"OPL\" modal-title=\"OPL\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Pekerjaan OPL</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"OPLWorkName\" placeholder=\"Nama Pekerjaan\" ng-model=\"mOpl.OPLWorkName\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"OPLWorkName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"number\" class=\"form-control\" name=\"Price\" placeholder=\"Harga\" ng-model=\"mOpl.Price\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"Price\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Vendor</bsreqlabel> <bsselect ng-model=\"mOpl.VendorId\" data=\"VendorData\" item-text=\"Name\" item-value=\"VendorId\" placeholder=\"Nama Vendor\" icon=\"fa fa-search\"> <bserrmsg field=\"VendorId\"></bserrmsg> </bsselect></div> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat Penagihan</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"BillingAddress\" placeholder=\"Alamat\" ng-model=\"mOpl.BillingAddress\" ng-maxlength=\"50\" required> </div> <bserrmsg field=\"BillingAddress\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/SCCRetailDisc/SCCRetailDisc.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"SCCRetailDiscForm\" factory-name=\"SCCRetailDiscount\" model=\"mSCCRetailDisc\" model-id=\"PartPriceDiscountPerSCCId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"SCCRetailDiscForm\" form-title=\"SCCRetailDisc\" modal-title=\"SCCRetailDisc\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Tanggal Berlaku</bsreqlabel>\r" +
    "\n" +
    "                    <bsdatepicker\r" +
    "\n" +
    "                            name=\"EffectiveDate\"\r" +
    "\n" +
    "                            date-options=\"dateOptions\"\r" +
    "\n" +
    "                            ng-model=\"mSCCRetailDisc.EffectiveDate\"\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                    </bsdatepicker>\r" +
    "\n" +
    "                    <bserrmsg field=\"EffectiveDate\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Range Tanggal Berlaku</bsreqlabel> <div class=\"row\"> <div class=\"col-md-5\"> <bsdatepicker name=\"EffectiveDate\" date-options=\"dateOptions\" ng-model=\"mSCCRetailDisc.EffectiveDate\" ng-required=\"true\"> </bsdatepicker> </div> <div class=\"col-md-2\"> S/D </div> <div class=\"col-md-5\"> <bsdatepicker name=\"ValidThru\" date-options=\"dateOptions\" ng-model=\"mSCCRetailDisc.ValidThru\" ng-required=\"true\"> </bsdatepicker> </div> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>SCC</bsreqlabel> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"PartsClassId\" placeholder=\"Material\" ng-model=\"mSCCRetailDisc.PartsClassId\" ng-maxlength=\"50\" required\r" +
    "\n" +
    "                        >\r" +
    "\n" +
    "                    </div> --> <bsselect ng-model=\"mSCCRetailDisc.PartsClassId\" data=\"dataPartsClass\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Tipe Material\" icon=\"fa fa-search\"> </bsselect> <!--  <bstypeahead \r" +
    "\n" +
    "                    placeholder=\"Nama SCC\" \r" +
    "\n" +
    "                    name=\"partsClass\" \r" +
    "\n" +
    "                    ng-model=\"mSCCRetailDisc.PartsClassId\" \r" +
    "\n" +
    "                    get-data=\"getPartsClassSCCSearch\" \r" +
    "\n" +
    "                    item-text=\"Description\" \r" +
    "\n" +
    "                    loading=\"loading Parts Class\" \r" +
    "\n" +
    "                    noresult=\"no Parts Class Available\" \r" +
    "\n" +
    "                    selected=\"selected\" \r" +
    "\n" +
    "                    on-select=\"onSelectPartsClass\" \r" +
    "\n" +
    "                    on-noresult=\"onNoResult\" \r" +
    "\n" +
    "                    on-gotresult=\"onGotResult\"\r" +
    "\n" +
    "                    icon=\"fa fa-search\"  \r" +
    "\n" +
    "                    required>\r" +
    "\n" +
    "                    </bstypeahead>\r" +
    "\n" +
    "                    <bserrmsg field=\"PartsClassId\"></bserrmsg> --> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Discount</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"discount\" placeholder=\"Discount\" ng-model=\"mSCCRetailDisc.Discount\" min=\"0\" max=\"100\" required> </div> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> <input type=\"file\" accept=\".xlsx\" id=\"uploadSCC\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/customerFleet/customer_fleet.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"CustomerFleetForm\" factory-name=\"CustomerFleet\" model=\"mFleet\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xFleet.selected\" on-select-rows=\"onSelectRows\" form-name=\"CustomerFleetForm\" form-title=\"Customer Fleet\" modal-title=\"Customer Fleet\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Fleet Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"code\" placeholder=\"Auto Generate\" ng-model=\"mFleet.CustomerCode\" ng-disabled=\"true\"> </div> <bserrmsg field=\"code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Customer Fleet Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Customer Fleet Name\" ng-model=\"mFleet.CustomerName\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                <bsreqlabel>Description</bsreqlabel>\r" +
    "\n" +
    "                <div class=\"input-icon-right\">\r" +
    "\n" +
    "                    <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mFleet.desc\" required>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <bserrmsg field=\"desc\"></bserrmsg>\r" +
    "\n" +
    "            </div> --> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/customer_fleet/customer_fleet.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"CustomerFleetForm\" factory-name=\"CustomerFleet\" model=\"mFleet\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xFleet.selected\" on-select-rows=\"onSelectRows\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Fleet Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"code\" placeholder=\"Fleet Code\" ng-model=\"mFleet.code\" required> </div> <bserrmsg field=\"code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Fleet Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Fleet Name\" ng-model=\"mFleet.name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Description</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mFleet.desc\" required> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/defect/defect.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"DefectForm\" factory-name=\"Defect\" model=\"mDefect\" model-id=\"Id\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xDefect.selected\" on-select-rows=\"onSelectRows\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control\" name=\"Name\" placeholder=\"Name\" ng-model=\"mDefect.Name\" required> </div> <bserrmsg field=\"Name\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/dpParts/dpParts.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"DpPartsForm\" factory-name=\"DpPartsFactory\" model=\"mDpParts\" model-id=\"PartPriceDPId\" loading=\"loading\" on-show-detail=\"onShowDetail\" on-validate-save=\"onValidateSave\" show-advsearch=\"on\" grid-hide-action-column=\"gridHideActionColumn\" form-api=\"formApi\" get-data=\"getData\" selected-rows=\"xDpParts.selected\" on-select-rows=\"onSelectRows\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Tanggal Berlaku</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <bsdatepicker\r" +
    "\n" +
    "                            name=\"EffectiveDate\" \r" +
    "\n" +
    "                            date-options=\"dateOptions\"\r" +
    "\n" +
    "                            ng-model=\"mDpParts.EffectiveDate\" \r" +
    "\n" +
    "                        >\r" +
    "\n" +
    "                        {{mDpParts.EffectiveDate}}\r" +
    "\n" +
    "                        </bsdatepicker>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"EffectiveDate\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Range Tanggal Berlaku</bsreqlabel> <div class=\"row\"> <div class=\"col-md-5\"> <bsdatepicker name=\"EffectiveDate\" date-options=\"validDateOptions\" ng-model=\"mDpParts.EffectiveDate\"> {{mDpParts.EffectiveDate}} </bsdatepicker> </div> <div class=\"col-md-2\"> S/D </div> <div class=\"col-md-5\"> <bsdatepicker name=\"ValidThru\" date-options=\"validDateOptions\" ng-model=\"mDpParts.ValidThru\"> {{mDpParts.ValidThru}} </bsdatepicker> </div> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Range Nilai Dikenakan DP</bsreqlabel> <div class=\"row\"> <div class=\"col-md-5\"> <!-- <input style=\"\" type=\"text\" class=\"form-control\" name=\"PriceFrom\" placeholder=\"Range From\" ng-model=\"mDpParts.PriceFrom\" ng-change=\"numberWithCommas\" ng-min=\"0\" min=\"0\" required> --> <input style=\"\" type=\"text\" class=\"form-control\" name=\"PriceFrom\" placeholder=\"Range From\" ng-model=\"mDpParts.PriceFrom\" ng-keyup=\"givePatternpriceFrom(mDpParts.PriceFrom,$event)\" ng-min=\"0\" min=\"0\" required> </div> <div class=\"col-md-2\"> S/D </div> <div class=\"col-md-5\"> <!-- <input style=\"\" type=\"text\" class=\"form-control\" name=\"PriceTo\" ng-model=\"mDpParts.PriceTo\" placeholder=\"Range Trhu\" ng-change=\"numberWithCommas\" ng-min=\"0\" min=\"0\" required> --> <input style=\"\" type=\"text\" class=\"form-control\" name=\"PriceTo\" ng-model=\"mDpParts.PriceTo\" placeholder=\"Range Trhu\" ng-keyup=\"givePatternpriceTo(mDpParts.PriceTo,$event)\" ng-min=\"0\" min=\"0\" required> </div> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Persentase DP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"PercentDP\" placeholder=\"\" ng-model=\"mDpParts.PercentDP\" min=\"0\" max=\"100\" required> </div> <bserrmsg field=\"PercentDP\"></bserrmsg> </div> </div> </div> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-2\"> <label class=\"control-label\">Tanggal Berlaku Dari</label> <div class=\"tanggal\"> <bsdatepicker name=\"startDate\" date-options=\"dateOptions\" ng-model=\"mDpParts.startDate\"> </bsdatepicker> </div> </div> <div class=\"col-md-2\"> <label class=\"control-label\">Tanggal Berlaku Sampai</label> <div class=\"endDate\"> <bsdatepicker name=\"endDate\" date-options=\"dateOptions\" ng-model=\"mDpParts.endDate\"> </bsdatepicker> </div> </div> </div> <!-- end row --> <!-- Row Tengah --> <div class=\"row\"> <div class=\"col-md-offset-10\"> <!--<div class=\"input-group\">--> <label class=\"control-label\">&nbsp</label> <div> <button class=\"btn wbtn\" ng-click=\"onDeleteFilter(mDpParts)\">Hapus Filter</button> </div> <!--</div>--> </div> </div> <br> <!-- end row --> </bsform-advsearch> </bsform-grid>"
  );


  $templateCache.put('app/master/groupBP/groupBP.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"GroupBPForm\" factory-name=\"GroupBPFactory\" model=\"mGroupBP\" model-id=\"GroupTypeId\" loading=\"loading\" form-api=\"formApi\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-show-detail=\"onShowDetail\" do-custom-save=\"doCustomSave\" form-name=\"GroupBPForm\" form-title=\"Group BP\" modal-title=\"Group BP\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" custom-action-button-settings=\"actionButtonSettings\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-12\"> <!-- <button id=\"MainGroupBP_Upload_Button\" ng-click=\"MainGroupBP_Upload_Clicked()\" type=\"button\"\r" +
    "\n" +
    "                class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">\r" +
    "\n" +
    "            <span class=\"ladda-label\">\r" +
    "\n" +
    "                Upload\r" +
    "\n" +
    "                </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "            </button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <button id=\"MainGroupBP_Download_Button\" ng-click=\"MainGroupBP_Download_Clicked()\" type=\"button\"\r" +
    "\n" +
    "                class=\"rbtn btn ng-binding ladda-button\" style=\"float: right\">\r" +
    "\n" +
    "            <span class=\"ladda-label\">\r" +
    "\n" +
    "                Download Template\r" +
    "\n" +
    "                </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "            </button> --> <div class=\"row\"> <div class=\"col-lg-5\"> <div class=\"form-group\"> <bsreqlabel>Nama Group</bsreqlabel> <!-- <div class=\"input-icon-right\" > --> <input type=\"text\" class=\"form-control col-lg-6\" name=\"GroupName\" placeholder=\"Group Name\" ng-model=\"mGroupBP.GroupName\" ng-maxlength=\"50\" required> <!-- </div> --> <bserrmsg field=\"GroupName\"></bserrmsg> </div> </div> <div class=\"col-lg-5 col-lg-offset-1\"> <div class=\"form-group\"> <bsreqlabel>Nama Foreman</bsreqlabel> <bsselect name=\"QC\" ng-model=\"mGroupBP.ForemanId\" data=\"ForemanData\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" placeholder=\"Pilih Foreman\" required icon=\"fa fa-user\"> </bsselect> </div> </div> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                    <bsreqlabel>Technician</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"Technician\"\r" +
    "\n" +
    "                            ng-model=\"mGroupBP.Technician\"\r" +
    "\n" +
    "                            data=\"techData\"\r" +
    "\n" +
    "                            item-text=\"Initial\"\r" +
    "\n" +
    "                            item-value=\"EmployeeId\"\r" +
    "\n" +
    "                            placeholder=\"Pilih Teknisi\"\r" +
    "\n" +
    "                            required=\"true\"\r" +
    "\n" +
    "                            icon=\"fa fa-car\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                </div> --> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                    <bsreqlabel>QC</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"QC\"\r" +
    "\n" +
    "                            ng-model=\"mGroupBP.QC\"\r" +
    "\n" +
    "                            data=\"qcData\"\r" +
    "\n" +
    "                            item-text=\"Initial\"\r" +
    "\n" +
    "                            item-value=\"EmployeeId\"\r" +
    "\n" +
    "                            placeholder=\"Pilih QC\"\r" +
    "\n" +
    "                            required=\"true\"\r" +
    "\n" +
    "                            icon=\"fa fa-car\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                </div> --> <div class=\"row\"> <div class=\"col-lg-5\"> <div class=\"form-group\"> <bsreqlabel>Tipe Group</bsreqlabel> <bsselect name=\"QC\" ng-model=\"mGroupBP.GroupTypeId\" data=\"GroupTypeData\" item-text=\"GroupTypeDescription\" item-value=\"GroupTypeId\" placeholder=\"Pilih Group Type\" required icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <br><br> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                    <bsreqlabel>Foreman</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"Foreman\"\r" +
    "\n" +
    "                            ng-model=\"mGroupBP.Foreman\"\r" +
    "\n" +
    "                            data=\"foreData\"\r" +
    "\n" +
    "                            item-text=\"Initial\"\r" +
    "\n" +
    "                            item-value=\"EmployeeId\"\r" +
    "\n" +
    "                            placeholder=\"Pilih Foreman\"\r" +
    "\n" +
    "                            required=\"true\"\r" +
    "\n" +
    "                            icon=\"fa fa-car\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                </div> --> <div class=\"row\"> <div class=\"col-lg-5\"> <div class=\"form-group\"> <bsreqlabel>Nama Teknisi</bsreqlabel> <bsselect name=\"Teknisi\" ng-model=\"mGroupBP.TechnicianId\" on-select=\"getTechnicianInitial(selected)\" data=\"TechnicianData\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" placeholder=\"Pilih Teknisi\" icon=\"fa fa-user\"> </bsselect> </div> </div> <div class=\"col-lg-5 col-lg-offset-1\"> <div class=\"form-group\"> <bsreqlabel>Nama Stall</bsreqlabel> <bsselect name=\"Stall\" ng-model=\"mGroupBP.StallId\" data=\"StallData\" item-text=\"Name\" item-value=\"StallId\" placeholder=\"Pilih Stall\" icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-lg-5\" style=\"margin-bottom:20px\"> <div class=\"form-group\"> <bsreqlabel>Inisial Teknisi</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control col-md-3\" style=\"float:left\" name=\"GroupName\" placeholder=\"Inisial Teknisi\" ng-model=\"mGroupBP.TechInitial\" ng-maxlength=\"50\" ng-disabled=\"true\" skip-enabled> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-lg-4\" style=\"margin-bottom:10px\"> <button class=\"rbtn btn\" ng-click=\"addGroupTechnician(mGroupBP)\" ng-disabled=\"!mGroupBP.TechInitial || !mGroupBP.StallId || !mGroupBP.TechnicianId\" skip-enabled>Tambah</button> </div> </div> <div id=\"gridTechnician\" ui-grid=\"gridTechnician\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"gridTechnicianTableHeight()\"> <div class=\"ui-grid-nodata\" ng-show=\"!gridTechnician.data.length\">No data available</div> </div> </div> </div> <div class=\"ui modal\" id=\"ModalUploadGroupBP\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadGroupBP\" onchange=\"angular.element(this).scope().loadXLSGroupBP(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"UploadBpCenterSatellite_Upload_Button\" ng-click=\"UploadGroupBPClicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div> <!--\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group col-md-6\" show-errors>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"form-group col-md-6\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Teknisi</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect\r" +
    "\n" +
    "                        ng-model=\"mGroupBP.Technician\"\r" +
    "\n" +
    "                        data=\"getTeknisi\"\r" +
    "\n" +
    "                        item-text=\"namaTeknisi\"\r" +
    "\n" +
    "                        item-value=\"teknisiID\"\r" +
    "\n" +
    "                        on-select=\"teknisiID(selected)\"\r" +
    "\n" +
    "                        placeholder=\"Select Teknisi\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                    <bserrmsg field=\"namaTeknisi\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"form-group col-md-6\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Technician</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"Technician\" placeholder=\"Technician\" ng-model=\"mGroupBP.Technician\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"Technician\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"clearfix\"></div>\r" +
    "\n" +
    "                <div class=\"form-group col-md-6\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Outlet</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"OutletId\" placeholder=\"Technician\" ng-model=\"mGroupBP.OutletId\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"OutletId\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"clearfix\"></div>\r" +
    "\n" +
    "                <div class=\"form-group col-md-6\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Foreman</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"foreman\" placeholder=\"Foreman\" ng-model=\"mGroupBP.Foreman\" ng-maxlength=\"50\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"foreman\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"clearfix\"></div>\r" +
    "\n" +
    "                <div class=\"form-group col-md-6\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Group Head</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"GroupHead\" placeholder=\"Group Head\" ng-model=\"mGroupBP.GroupHead\" ng-maxlength=\"50\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"GroupHead\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        --> <bsform-below-grid> <div class=\"row\" style=\"margin-top:30px\"> <div class=\"col-md-12\"> <label><h2>Group BP : List Teknisi</h2></label> </div> <div class=\"col-md-4\"> <div class=\"input-group\" style=\"margin-bottom:10px\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"gridnyaAPI.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}}</a> </li> </ul> </div> </div> </div> <div class=\"col-md-12\"> <div id=\"gridTechnicianAku\" ui-grid=\"gridTechnicianAku\" get-data=\"gridTechnicianAku\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\"></div> </div> </div> </bsform-below-grid> </bsform-grid> <!--\r" +
    "\n" +
    "<div id=\"gridDetail\" ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination\r" +
    "\n" +
    "    ui-grid-auto-resize class=\"grid\" ng-style=\"gridDetailTableHeight()\">\r" +
    "\n" +
    "    <div class=\"ui-grid-nodata\" ng-show=\"!gridDetail.data.length\">No data available</div>\r" +
    "\n" +
    "</div> -->"
  );


  $templateCache.put('app/master/groupGR/groupGR.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"GroupGRForm\" factory-name=\"GroupGRFactory\" model=\"mGroupGR\" model-id=\"GroupId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-show-detail=\"onShowDetail\" do-custom-save=\"doCustomSave\" grid-hide-action-column=\"true\" on-validate-save=\"onValidateSave\" form-name=\"GroupGRForm\" form-title=\"Group GR\" modal-title=\"Group GR\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Group</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"text\" class=\"form-control col-md-3\" style=\"float:left\" name=\"GroupName\" placeholder=\"Group Name\" ng-model=\"mGroupGR.GroupName\" ng-maxlength=\"50\"> </div> <bserrmsg field=\"GroupName\"></bserrmsg> </div> <div class=\"form-group\"> <bsreqlabel>Foreman</bsreqlabel> <bsselect name=\"Foreman\" ng-model=\"mGroupGR.ForemanId\" data=\"foreData\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" placeholder=\"Pilih Foreman\" icon=\"fa fa-car\"> </bsselect> </div> <!-- <div class=\"form-group\">\r" +
    "\n" +
    "                    <label>Nama Teknisi</label>\r" +
    "\n" +
    "                    <bstypeahead placeholder=\"Nama Teknisi\" name=\"Teknisi\" get-data=\"getTechnician\" item-text=\"PartsCode\" ng-maxlength=\"250\" disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected=\"selected\" on-select=\"onSelectTech\"\r" +
    "\n" +
    "                        on-noresult=\"onNoTechResult\" on-gotresult=\"onGotResult\" ta-minlength=\"4\" template-url=\"customTemplate.html\" icon=\"fa-id-card-o\">\r" +
    "\n" +
    "                    </bstypeahead>\r" +
    "\n" +
    "                </div> --> <div class=\"form-group\"> <bsreqlabel>Nama Teknisi</bsreqlabel> <bsselect name=\"Teknisi\" ng-model=\"mGroupGR.Technician\" data=\"techData\" item-text=\"EmployeeName\" item-value=\"TheId\" ng-change=\"selectTechnician(selected)\" placeholder=\"Pilih Teknisi\" icon=\"fa fa-car\"> <!-- =\"true\" --> </bsselect> </div> <div class=\"form-group\" ng-show=\"ButtonAdd == 1\"> <button class=\"rbtn btn\" ng-click=\"addGroupTechnician(mGroupGR)\">Tambah</button> </div> </div> </div>  <!-- <div class=\"row\"> --> <div id=\"gridDetail\" ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-auto-resize class=\"grid\"> <div class=\"ui-grid-nodata\" ng-show=\"!gridDetail.data.length\">No data available</div> </div> <!-- </div> -->  <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "            <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "            <span ng-bind-html=\"match.model.PartsName | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "            <!-- <span ng-bind-html=\"match.model.FullName | uibTypeaheadHighlight:query\"></span> -->\r" +
    "\n" +
    "        </a></script></bsform-grid>"
  );


  $templateCache.put('app/master/holiday/holiday.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"HolidayForm\" factory-name=\"HolidayFactory\" model=\"mData\" model-id=\"HolidayId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-show-detail=\"onShowDetail\" on-validate-save=\"onValidateSave\" on-before-new-mode=\"onBeforeNewMode\" form-name=\"HolidayForm\" form-title=\"Holiday\" modal-title=\"Holiday\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <div class=\"col-md-3 form-group\"> <label>Tanggal</label> <bsdatepicker name=\"HolidayDate\" ng-disabled=\"EnableEdit\" skip-enable date-options=\"dateOptions\" ng-model=\"mData.HolidayDate\"> </bsdatepicker> </div> <div class=\"col-md-3 form-group\"> <label>Keterangan</label> <input ng-model=\"mData.HolidayDesc\" style=\"width:100%\" type=\"text\" class=\"form-control\" ng-required=\"true\"> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/masterAreaDealer/masterAreaDealer.html',
    "<style type=\"text/css\">.layoutContainer_MasterAreaDealer_Small {\r" +
    "\n" +
    "        height: 420px !important;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .layoutContainer_MasterAreaDealer_Large {\r" +
    "\n" +
    "        height: 686px !important;\r" +
    "\n" +
    "    }</style> <bsform-grid ng-form=\"MasterAreaDealerForm\" factory-name=\"MasterAreaDealerFactory\" model=\"mData\" get-data=\"getData\" model-id=\"AreaDealerId\" loading=\"loading\" selected-rows=\"selectedRows\" form-api=\"formApi\" on-before-new=\"onBeforeNewMode\" on-before-edit=\"onBeforeEditMode\" on-show-detail=\"onShowDetail\" form-title=\"Master Area Dealer\" modal-size=\"small\" icon=\"fa fa-fw fa-users\" do-custom-save=\"doCustomSave\" on-select-rows=\"onSelectRows\" on-bulk-delete=\"onBulkDelete\"> <div> <div ng-if=\"crud_mode\" ng-include=\"'app/master/masterAreaDealer/masterAreaDealer_crud.html'\"></div> </div> </bsform-grid> <!--         <form class=\"form-inline\" role=\"form\" ng-hide=\"hideGrid == 0\">\r" +
    "\n" +
    "            <br><br>\r" +
    "\n" +
    "            <div class=\"row filtersearch\" style=\"margin-right:0px;padding-bottom:5px;\">\r" +
    "\n" +
    "                <div class=\"col-md-12\">\r" +
    "\n" +
    "                    <div class=\"col-md-12\" style=\"padding-left:1px;\">\r" +
    "\n" +
    "                        <div class=\"input-group\">\r" +
    "\n" +
    "                            <div class=\"input-group-btn\" uib-dropdown>\r" +
    "\n" +
    "                                <button style=\"margin-right:5px;\" ng-disabled=\"!selectedRows.length>=0\" type=\"button\" class=\"btn wbtn\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\">\r" +
    "\n" +
    "                                Bulk Action&nbsp;<span class=\"caret\"></span>\r" +
    "\n" +
    "                                </button>\r" +
    "\n" +
    "                                <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\">\r" +
    "\n" +
    "                                    <li role=\"menuitem\" ng-if=\"allowApprove\">\r" +
    "\n" +
    "                                        <a href=\"#\" ng-click=\"actApprovel(selectedRows,0)\" ng-if=\"selectedRows.length>=0\">{{actBtnCaption['approve']}}</a>\r" +
    "\n" +
    "                                    </li>\r" +
    "\n" +
    "                                    <li role=\"menuitem\" ng-if=\"allowApprove\">\r" +
    "\n" +
    "                                        <a href=\"#\" ng-click=\"actApprovel(selectedRows,1)\" ng-if=\"selectedRows.length>=0\">{{actBtnCaption['reject']}}</a>\r" +
    "\n" +
    "                                    </li>\r" +
    "\n" +
    "                                </ul>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"GridAPIOutlet2.grid.columns[filterCol2Idx].filters[0].term\">\r" +
    "\n" +
    "                            <div class=\"input-group-btn\" uib-dropdown>\r" +
    "\n" +
    "                                <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\">\r" +
    "\n" +
    "                                    {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span>\r" +
    "\n" +
    "                                </button>\r" +
    "\n" +
    "                                <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\">\r" +
    "\n" +
    "                                    <li role=\"menuitem\" ng-repeat=\"col in grid2Cols\">\r" +
    "\n" +
    "                                        <a href=\"#\" ng-click=\"filterBtnChange2(col)\">{{col.name|uppercase}}\r" +
    "\n" +
    "                                        <span class=\"pull-right\">\r" +
    "\n" +
    "                                          <i class=\"fa fa-fw fa-filter\"\r" +
    "\n" +
    "                                              \r" +
    "\n" +
    "                                          ></i>\r" +
    "\n" +
    "                                        </span>\r" +
    "\n" +
    "                                      </a>\r" +
    "\n" +
    "                                    </li>\r" +
    "\n" +
    "                                </ul>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </form>\r" +
    "\n" +
    "punya billy\r" +
    "\n" +
    "    <div ng-hide=\"hideGrid == 0\">\r" +
    "\n" +
    "        <div id=\"gridOutlet2\" \r" +
    "\n" +
    "            ui-grid=\"gridOutlet2\" \r" +
    "\n" +
    "            ui-grid-move-columns \r" +
    "\n" +
    "            ui-grid-resize-columns \r" +
    "\n" +
    "            ui-grid-selection \r" +
    "\n" +
    "            ui-grid-pagination\r" +
    "\n" +
    "            ui-grid-auto-resize class=\"grid\" ng-style=\"gridOutletTableHeight()\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <!-- COMMENT BY ANITA 20180709 --> <!-- <script type=\"text/javascript\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "</script>\r" +
    "\n" +
    "<style type=\"text/css\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "</style>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-include=\"'app/master/masterAreaDealer/masterAreaDealer_crud.html'\" ng-if=\"crud_mode == true\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-if=\"crud_mode == false\">\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-lg-12\">\r" +
    "\n" +
    "            <div class=\"pull-right\">\r" +
    "\n" +
    "                <div class=\"btn-group\" style=\"margin-top:-40px;margin-right:5px;\">\r" +
    "\n" +
    "                    <button class=\"ubtn\" ng-click=\"getData()\" style=\"padding-bottom:6px;\" tooltip-placement=\"bottom\" tooltip-trigger=\"mouseenter\" tooltip-animation=\"false\" uib-tooltip=\"Refresh\" tabindex=\"0\"><i class=\"fa fa-fw fa-refresh\" style=\"font-size:1.4em;margin-top:20px;\"></i></button>\r" +
    "\n" +
    "                    \r" +
    "\n" +
    "                    <button id=\"Add\" ng-click=\"newAreaDealer()\"\r" +
    "\n" +
    "                     type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"margin-top:20px\">\r" +
    "\n" +
    "                        <span class=\"ladda-label\">\r" +
    "\n" +
    "                            <span class=\"fa fa-fw fa-plus\"></span>Tambah Area Dealer\r" +
    "\n" +
    "                        </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "                    </button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-md-12\" style=\"margin-top: 20px;\">\r" +
    "\n" +
    "            <div class=\"col-md-3 form-group\">\r" +
    "\n" +
    "                <bsreqlabel>Deskripsi</bsreqlabel>\r" +
    "\n" +
    "                <input type=\"textArea\" width=\"150\" height=\"350\" class=\"form-control col-lg-6\" name=\"Description\" placeholder=\"Description\"\r" +
    "\n" +
    "                    ng-model=\"mData.Description\" ng-maxlength=\"50\" required ng-disabled=\"flagEdit == false\">\r" +
    "\n" +
    "            <div class=\"col-md-3 form-group\">\r" +
    "\n" +
    "                <label>Nama Area</label>\r" +
    "\n" +
    "                <input ng-model=\"mData.AreaDealerName\" style=\"width:100%\"  type=\"text\"\r" +
    "\n" +
    "                class=\"form-control\" ng-required=\"true\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-3 form-group\">\r" +
    "\n" +
    "                <label>Total Outlet</label>\r" +
    "\n" +
    "                <input ng-model=\"mData.TotalOutlet\" style=\"width:100%\"  type=\"text\"\r" +
    "\n" +
    "                class=\"form-control\" ng-required=\"true\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-3 form-group\">\r" +
    "\n" +
    "                <label>Deskripsi</label>\r" +
    "\n" +
    "                <input ng-model=\"mData.Description\" style=\"width:100%\"  type=\"text\"\r" +
    "\n" +
    "                class=\"form-control\" ng-required=\"true\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-md-12\" style=\"margin-top: 20px;\">\r" +
    "\n" +
    "            <div class=\"col-md-3 form-group\">\r" +
    "\n" +
    "                <bsreqlabel>Nama Outlet</bsreqlabel>\r" +
    "\n" +
    "                <bsselect name=\"Outlet\" on-select=\"onSelectOutlet(selected)\" ng-model=\"mData.OutletId\" data=\"OutletData\" item-text=\"Name\"\r" +
    "\n" +
    "                    item-value=\"OutletId\" placeholder=\"Pilih Outlet\" icon=\"fa fa-car\" ng-disabled=\"flagEdit == false\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "        on-bulk-delete=\"bulkDelete\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    " -->"
  );


  $templateCache.put('app/master/masterAreaDealer/masterAreaDealer_crud.html',
    "<!-- Grid 1 --><!-- <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-lg-12\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <button id=\"Add\" ng-click=\"doCustomSave(mData)\"\r" +
    "\n" +
    "             type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\" ng-disabled=\"gridOutlet.data.length == 0 || !mData.AreaDealerName || !mData.Description\" ng-if=\"flagEdit == true\">\r" +
    "\n" +
    "                <span class=\"ladda-label\">\r" +
    "\n" +
    "                    Simpan\r" +
    "\n" +
    "                </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "            </button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <button id=\"Add\" ng-click=\"goBack()\"\r" +
    "\n" +
    "             type=\"button\" class=\"wbtn btn ng-binding ladda-button\" style=\"float: right; margin-top:20px\">\r" +
    "\n" +
    "                <span class=\"ladda-label\">\r" +
    "\n" +
    "                    Batal\r" +
    "\n" +
    "                </span><span class=\"ladda-spinner\"></span>\r" +
    "\n" +
    "            </button>\r" +
    "\n" +
    "            \r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <div class=\"row\"> <div class=\"col-lg-12\"> <div class=\"row\"> <div class=\"col-lg-5\"> <div class=\"form-group\"> <bsreqlabel>Nama Area</bsreqlabel> <!-- <div class=\"input-icon-right\" > --> <input type=\"text\" class=\"form-control col-lg-6\" name=\"AreaName\" placeholder=\"Area Dealer Name\" ng-model=\"mData.AreaDealerName\" ng-maxlength=\"50\" required ng-disabled=\"flagEdit == false\"> <!-- </div> --> <!-- <bserrmsg field=\"AreaName\"></bserrmsg> --> </div> </div> </div> <br><br> <div class=\"row\"> <div class=\"col-lg-5\"> <div class=\"form-group\"> <bsreqlabel>Deskripsi</bsreqlabel> <!-- <div class=\"input-icon-right\" > --> <input type=\"textArea\" width=\"150\" height=\"350\" class=\"form-control col-lg-6\" name=\"Description\" placeholder=\"Description\" ng-model=\"mData.Description\" ng-maxlength=\"50\" required ng-disabled=\"flagEdit == false\"> <!-- </div> --> <!-- <bserrmsg field=\"Description\"></bserrmsg> --> </div> </div> </div> <br><br> <br><br> <br><br> <div class=\"row\"> <div class=\"col-lg-5\"> <div class=\"form-group\"> <bsreqlabel>Nama Outlet</bsreqlabel> <bsselect name=\"Outlet\" on-select=\"onSelectOutlet(selected)\" ng-model=\"mData.OutletId\" data=\"OutletData\" item-text=\"Name\" item-value=\"OutletId\" placeholder=\"Pilih Outlet\" icon=\"fa fa-car\" ng-disabled=\"flagEdit == false\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-lg-5\"> <div class=\"form-group\"> <bsreqlabel>Alamat</bsreqlabel> <!-- <div class=\"input-icon-right\" > --> <input type=\"textArea\" width=\"150\" height=\"350\" class=\"form-control col-lg-6\" name=\"Address\" placeholder=\"Address\" ng-model=\"mData.Address\" ng-maxlength=\"50\" disabled skip-enable> <!-- </div> --> <!-- <bserrmsg field=\"GroupName\"></bserrmsg> --> </div> </div> </div> <br> <div class=\"row\"> <div class=\"col-lg-4\"> <button class=\"rbtn btn\" ng-click=\"addOutlet(mData)\" ng-disabled=\"flagEdit == false\">Tambah</button> </div> </div> <br> <form class=\"form-inline\" role=\"form\"> <div class=\"row filtersearch\" style=\"margin-right:0px;padding-bottom:5px\"> <div class=\"col-md-12\"> <div class=\"col-md-12\" style=\"padding-left:1px\"> <div class=\"input-group\"> <div class=\"input-group-btn\" uib-dropdown> <button style=\"margin-right:5px\" ng-disabled=\"(selectedRowsOutlet.length==0) || disableView\" type=\"button\" class=\"btn wbtn\" onclick=\"this.blur()\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> Bulk Action&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\"> <li role=\"menuitem\"> <a href=\"#\" ng-click=\"actDelete(selectedRowsOutlet,0)\" ng-if=\"selectedRowsOutlet.length>0\">Hapus</a> </li> </ul> </div> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"GridAPIOutlet.gridOutlet.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}} <span class=\"pull-right\"> <i class=\"fa fa-fw fa-filter\"></i> </span> </a> </li> </ul> </div> </div> </div> </div> </div> </form> <div id=\"gridOutlet\" ui-grid=\"gridOutlet\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"gridOutletTableHeight()\"> </div> </div> </div>"
  );


  $templateCache.put('app/master/material/kelompok.html',
    "<div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Material</bsreqlabel> <bsselect ng-model=\"mMaterial.PartsClassId1\" data=\"MaterialTypeData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Tipe Material\" icon=\"fa fa-search\" ng-disabled=\"list_PartsClassId5_disable.indexOf(parts_class_id5_edit) != -1\" required> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Stocking Policy</bsreqlabel> <bsselect ng-model=\"mMaterial.PartsClassId4\" data=\"StockPData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Stocking Policy\" icon=\"fa fa-search\" ng-disabled=\"list_PartsClassId5_disable.indexOf(parts_class_id5_edit) != -1\" required> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>SCC</bsreqlabel> <bsselect ng-model=\"mMaterial.PartsClassId3\" data=\"SCCData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih SCC\" icon=\"fa fa-search\" required> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>ICC</bsreqlabel> <bsselect ng-model=\"mMaterial.PartsClassId2\" data=\"ICCData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih ICC\" icon=\"fa fa-search\" ng-disabled=\"list_PartsClassId5_disable.indexOf(parts_class_id5_edit) != -1\" required> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Franchise (Report)</label> <bsselect ng-model=\"mMaterial.PartsClassId6\" data=\"FranchiseData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Franchise\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Clasification</bsreqlabel> <bsselect ng-model=\"mMaterial.PartsClassId5\" data=\"ClassifData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Clasification\" icon=\"fa fa-search\" ng-disabled=\"list_PartsClassId5_disable.indexOf(parts_class_id5_edit) != -1\" required> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> </div>"
  );


  $templateCache.put('app/master/material/lokasi.html',
    "<!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "        <bsreqlabel>Lokasi Gudang</bsreqlabel>\r" +
    "\n" +
    "        <bstypeahead placeholder=\"Nama Gudang\" name=\"WarehouseId\"\r" +
    "\n" +
    "            ng-model=\"mMaterial.WarehouseId\"\r" +
    "\n" +
    "            get-data=\"getWarehouse\"\r" +
    "\n" +
    "            model-key=\"WarehouseId\"\r" +
    "\n" +
    "            ng-minlength=\"2\" ng-maxlength=\"10\" required disallow-space\r" +
    "\n" +
    "            loading=\"loadingUsers\"\r" +
    "\n" +
    "            noresult=\"noResults\"\r" +
    "\n" +
    "            selected=\"selected\"\r" +
    "\n" +
    "            icon=\"fa-id-card-o\"\r" +
    "\n" +
    "        >\r" +
    "\n" +
    "        </bstypeahead>\r" +
    "\n" +
    "        <bserrmsg field=\"WarehouseId\"></bserrmsg>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div class=\"col-md-4\">\r" +
    "\n" +
    "    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "        <bsreqlabel>Lokasi Rak</bsreqlabel>\r" +
    "\n" +
    "        <bstypeahead placeholder=\"Nama Rak\" name=\"WarehouseShelfId\"\r" +
    "\n" +
    "            ng-model=\"mMaterial.WarehouseShelfId\"\r" +
    "\n" +
    "            get-data=\"getWarehouseShelf\"\r" +
    "\n" +
    "            model-key=\"WarehouseShelfId\"\r" +
    "\n" +
    "            ng-minlength=\"2\" ng-maxlength=\"10\" required disallow-space\r" +
    "\n" +
    "            loading=\"loadingUsers\"\r" +
    "\n" +
    "            noresult=\"noResults\"\r" +
    "\n" +
    "            selected=\"selected\"\r" +
    "\n" +
    "            icon=\"fa-id-card-o\"\r" +
    "\n" +
    "        >\r" +
    "\n" +
    "        </bstypeahead>\r" +
    "\n" +
    "        <bserrmsg field=\"WarehouseShelfId\"></bserrmsg>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>  --><!-- <div class=\"row\">\r" +
    "\n" +
    "    <div class=\"col-md-4\">\r" +
    "\n" +
    "        <div class=\"form-group\">\r" +
    "\n" +
    "            <bsreqlabel>Gudang</bsreqlabel>\r" +
    "\n" +
    "            <bsselect \r" +
    "\n" +
    "                ng-model=\"warehouse.WarehouseId\" \r" +
    "\n" +
    "                data=\"whData\" \r" +
    "\n" +
    "                item-text=\"WarehouseName\" \r" +
    "\n" +
    "                item-value=\"WarehouseId\" \r" +
    "\n" +
    "                placeholder=\"Pilih Gudang\" \r" +
    "\n" +
    "                on-select=\"setShelf(selected)\"\r" +
    "\n" +
    "                icon=\"fa fa-search\">\r" +
    "\n" +
    "            </bsselect>\r" +
    "\n" +
    "            <bserrmsg field=\"WarehouseId\"></bserrmsg>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"col-md-4\">\r" +
    "\n" +
    "        <div class=\"form-group\">\r" +
    "\n" +
    "            <bsreqlabel>Rak</bsreqlabel>\r" +
    "\n" +
    "            <bsselect \r" +
    "\n" +
    "                ng-model=\"warehouse.ShelfId\" \r" +
    "\n" +
    "                data=\"shData\" \r" +
    "\n" +
    "                item-text=\"ShelfName\" \r" +
    "\n" +
    "                item-value=\"ShelfId\" \r" +
    "\n" +
    "                placeholder=\"Pilih Satuan Dasar\"\r" +
    "\n" +
    "                icon=\"fa fa-search\">\r" +
    "\n" +
    "            </bsselect>\r" +
    "\n" +
    "            <bserrmsg field=\"ShelfId\"></bserrmsg>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"col-md-4\">\r" +
    "\n" +
    "        <div class=\"form-group\" style=\"margin-top:22.1px\">\r" +
    "\n" +
    "            <button class=\"rbtn btn\" \r" +
    "\n" +
    "                onclick=\"this.blur();\"\r" +
    "\n" +
    "                ng-click=\"addAlloc()\">\r" +
    "\n" +
    "                <i></i>&nbsp;Tambah Alokasi Gudang\r" +
    "\n" +
    "            </button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridDetailLoc\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div>"
  );


  $templateCache.put('app/master/material/material.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <bsform-grid ng-form=\"MaterialForm\" factory-name=\"Material\" model=\"mMaterial\" model-id=\"PartsId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xMaterial.selected\" on-select-rows=\"onSelectRows\" on-show-detail=\"beforeView\" on-before-new=\"beforeNew\" on-before-edit=\"beforeEdit\" do-custom-save=\"doCustomSave\" modal-title=\"Material\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" form-api=\"formApi\" form-grid-api=\"formGridApi\" custom-action-button-settings=\"actionButtonSettings\"> <bsform-advsearch> <!-- Row Atas --> <div class=\"row\" hidden> <div class=\"col-md-2\"> <label class=\"control-label\">Tipe Material *</label> <select class=\"form-control\" ng-model=\"mData.MaterialTypeId[0]\"> <!--<option value=\"\">-- pilih tipe --</option> --> <!-- <option value=\"1\">Parts</option> \r" +
    "\n" +
    "                    <option value=\"2\">Bahan</option> --> <option value=\"\" selected>{{mData.MaterialName}}</option> </select> </div> <div class=\"col-md-2\"> <label class=\"control-label\">Lokasi Gudang *</label> <select class=\"form-control\" ng-model=\"mData.MaterialTypeId[0]\"> <!-- ng-options=\"mData.WarehouseId as mData.WarehouseName for mData in mData.WarehouseData\" --> <option value=\"\" selected>{{mData.WarehouseName}}</option> </select> </div> </div> <!-- end row --> <br> <!-- Row Tengah --> <div class=\"row\"> <div class=\"col-md-2\"> <div class=\"input-group\"> <label class=\"control-label\">No. Material *</label> <input type=\"text\" class=\"form-control\" onkeyup=\"this.value = this.value.toUpperCase()\" ng-model=\"mData.PartsCode\"> </div> </div> <div class=\"col-md-2\"> <div class=\"input-group\"> <label class=\"control-label\">Nama Material *</label> <input type=\"text\" class=\"form-control\" ng-model=\"mData.PartsName\"> </div> </div> <!-- <div class=\"col-md-2\">\r" +
    "\n" +
    "                    <div class=\"input-group\">\r" +
    "\n" +
    "                      <label class=\"control-label\">&nbsp</label>\r" +
    "\n" +
    "                        <div>\r" +
    "\n" +
    "                          <button class=\"btn btn-default\" ng-click=\"hapusFilter()\">Hapus </button>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                  </div> --> <div class=\"col-md-7 text-right\"> <!--<div class=\"input-group\">--> <label class=\"control-label\">&nbsp</label> <div class=\"\"> <!-- <button class=\"btn wbtn pull-right\" ng-click=\"onDeleteFilter()\">Hapus Filter</button> --> </div> <!--</div>--> </div> </div> <!--<div class=\"row\">\r" +
    "\n" +
    "                <div class=\"col-md-2\">\r" +
    "\n" +
    "                  <button class=\"btn wbtn\" ng-click=\"getDataWH()\">getDataWH</button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "              </div>--> </bsform-advsearch> <!-- <bsform-base-main>\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"col-md-12\">\r" +
    "\n" +
    "                            <div id=\"grid0\" ui-grid=\"grid\" ui-grid-pagination ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellNav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-style=\"getTableHeight()\">\r" +
    "\n" +
    "                                <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>Data Belum Tersedia</div>\r" +
    "\n" +
    "                                <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "        </bsform-base-main> --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No Material</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"PartsCode\" placeholder=\"No Material\" ng-model=\"mMaterial.PartsCode\" ng-disabled=\"list_PartsClassId5_disable.indexOf(parts_class_id5_edit) != -1\" required> </div> <bserrmsg field=\"PartsCode\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bscheckbox ng-model=\"mMaterial.Active\"\r" +
    "\n" +
    "                            label = \"Aktif\">\r" +
    "\n" +
    "                    </bscheckbox>\r" +
    "\n" +
    "                </div> \r" +
    "\n" +
    "            </div> --> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Material</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"PartsName\" placeholder=\"Nama Material\" ng-model=\"mMaterial.PartsName\" ng-disabled=\"list_PartsClassId5_disable.indexOf(parts_class_id5_edit) != -1\" required> </div> <bserrmsg field=\"PartsName\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Vendor</bsreqlabel> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                        <input type=\"number\" class=\"form-control\" name=\"VendorId\" placeholder=\"Nama Vendor\" ng-model=\"mMaterial.VendorId\">\r" +
    "\n" +
    "                    </div> --> <bsselect ng-model=\"mMaterial.VendorId\" data=\"VendorData\" item-text=\"Name\" item-value=\"VendorId\" placeholder=\"Pilih Vendor\" icon=\"fa fa-search\" required> </bsselect> <bserrmsg field=\"VendorId\"></bserrmsg> </div> </div> <!-- <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Dibuat di</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"MadeIn\" placeholder=\"Dibuat di\" ng-model=\"mMaterial.MadeIn\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"MadeIn\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div> --> </div> <!-- <pre>\r" +
    "\n" +
    "            {{mMaterial}}\r" +
    "\n" +
    "        </pre>\r" +
    "\n" +
    " --> <div class=\"row\" style=\"padding-top:50px; padding-left: 15px\"> <uib-tabset active=\"activePill\"> <uib-tab index=\"0\" heading=\"Kelompok\"> <br> <div ng-include=\"'app/master/material/kelompok.html'\"></div> </uib-tab> <uib-tab index=\"1\" heading=\"Lokasi\"> <br> <div ng-include=\"'app/master/material/lokasi.html'\"></div> </uib-tab> <uib-tab index=\"2\" heading=\"Nilai\"> <br> <div ng-include=\"'app/master/material/nilai.html'\"></div> </uib-tab> <uib-tab index=\"3\" heading=\"Satuan\"> <br> <div ng-include=\"'app/master/material/satuan.html'\"></div> </uib-tab> </uib-tabset> </div> <div class=\"ui modal\" id=\"ModalUploadGroupBP\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadGroupBP\" onchange=\"angular.element(this).scope().loadXLSGroupBP(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"UploadBpCenterSatellite_Upload_Button\" ng-click=\"UploadGroupBPClicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> \\ </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div> </bsform-grid> <!-- <bsmodal id=\"modal_alloc\" title=\"Alokasi Gudang\" mode=\"\"\r" +
    "\n" +
    "        visible=\"modalShow\" \r" +
    "\n" +
    "        size=\"small\"\r" +
    "\n" +
    "        hide-footer=\"true\"\r" +
    "\n" +
    ">\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-md-4\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <bsreqlabel>Gudang</bsreqlabel>\r" +
    "\n" +
    "                <bsselect \r" +
    "\n" +
    "                    ng-model=\"warehouse.WarehouseId\" \r" +
    "\n" +
    "                    data=\"whData\" \r" +
    "\n" +
    "                    item-text=\"WarehouseName\" \r" +
    "\n" +
    "                    item-value=\"WarehouseId\" \r" +
    "\n" +
    "                    placeholder=\"Pilih Gudang\" \r" +
    "\n" +
    "                    on-select=\"setShelf(selected)\"\r" +
    "\n" +
    "                    icon=\"fa fa-search\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-md-4\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <bsreqlabel>Rak</bsreqlabel>\r" +
    "\n" +
    "                <bsselect \r" +
    "\n" +
    "                    ng-model=\"warehouse.ShelfId\" \r" +
    "\n" +
    "                    data=\"shData\" \r" +
    "\n" +
    "                    item-text=\"ShelfName\" \r" +
    "\n" +
    "                    item-value=\"ShelfId\" \r" +
    "\n" +
    "                    placeholder=\"Pilih Satuan Dasar\"\r" +
    "\n" +
    "                    icon=\"fa fa-search\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</bsmodal> -->"
  );


  $templateCache.put('app/master/material/nilai.html',
    "<div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Retail</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"number\" class=\"form-control\" name=\"RetailPrice\" placeholder=\"Harga Retail\" ng-model=\"mMaterial.RetailPrice\" min=\"0\" required> </div> <bserrmsg field=\"RetailPrice\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Rended Cost</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"number\" class=\"form-control\" name=\"StandardCost\" placeholder=\"Rended Cost\" ng-model=\"mMaterial.StandardCost\" min=\"0\" required> </div> <bserrmsg field=\"StandardCost\"></bserrmsg> </div> </div>"
  );


  $templateCache.put('app/master/material/satuan.html',
    "<div class=\"row\"> <!--  --> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "        <div class=\"form-group\">\r" +
    "\n" +
    "            <bsreqlabel>Satuan Beli</bsreqlabel>\r" +
    "\n" +
    "            <bsselect\r" +
    "\n" +
    "                ng-model=\"uom.UomId\"\r" +
    "\n" +
    "                data=\"UomData\"\r" +
    "\n" +
    "                item-text=\"Name\"\r" +
    "\n" +
    "                item-value=\"MasterId\"\r" +
    "\n" +
    "                placeholder=\"Pilih Satuan\"\r" +
    "\n" +
    "                icon=\"fa fa-search\">\r" +
    "\n" +
    "            </bsselect>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"col-md-3\">\r" +
    "\n" +
    "        <div class=\"form-group\">\r" +
    "\n" +
    "        <bsreqlabel>Quantity</bsreqlabel>\r" +
    "\n" +
    "        <input type=\"number\" class=\"form-control\" name=\"ConversionVal\" ng-model=\"uom.Qty\" min=\"1\" style=\"height:50\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <!--  --> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Satuan Dasar</bsreqlabel> <bsselect ng-model=\"mMaterial.UomId\" data=\"UomData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Satuan Dasar\" on-select=\"setUomTgt(selected)\" icon=\"fa fa-search\" required> </bsselect> <bserrmsg field=\"UomId\"></bserrmsg> </div> </div> <!--  --> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "        <div class=\"form-group\" style=\"margin-top:22.1px\">\r" +
    "\n" +
    "            <button class=\"rbtn btn\" \r" +
    "\n" +
    "                onclick=\"this.blur();\"\r" +
    "\n" +
    "                ng-click=\"addUom()\">\r" +
    "\n" +
    "                <i></i>&nbsp;Tambah Konversi\r" +
    "\n" +
    "            </button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <!--  --> </div> <!-- <pre>{{saveEffDate.EffectiveDate}}</pre> --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <bscheckbox ng-model=\"mMaterial.Active\" label=\"Setting Konversi Satuan 'General per Material'\"> </bscheckbox> </div> <div ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div>"
  );


  $templateCache.put('app/master/mtrlRetailDisc/mtrlRetailDisc.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"MtrlRetailDiscForm\" factory-name=\"MaterialRetailDiscount\" model=\"mMtrlRetailDisc\" model-id=\"PartPriceDiscountPerMaterialId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" custom-action-button-settings=\"customActionSettings\" form-name=\"MtrlRetailDiscForm\" form-title=\"Diskon Material\" modal-title=\"Diskon Material\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" on-validate-save=\"onValidateSave\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Tanggal Berlaku</bsreqlabel>\r" +
    "\n" +
    "                    <bsdatepicker\r" +
    "\n" +
    "                            name=\"EffectiveDate\"\r" +
    "\n" +
    "                            date-options=\"dateOptions\"\r" +
    "\n" +
    "                            ng-model=\"mMtrlRetailDisc.EffectiveDate\"\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                    </bsdatepicker>\r" +
    "\n" +
    "                    <bserrmsg field=\"EffectiveDate\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Range Tanggal Berlaku</bsreqlabel> <div class=\"row\"> <div class=\"col-md-5\"> <bsdatepicker name=\"EffectiveDate\" min-date=\"startMinOption\" max-date=\"startMaxOption\" date-options=\"DateFilterOptions1\" ng-change=\"startDateChange\" ng-model=\"mMtrlRetailDisc.EffectiveDate\" ng-required=\"true\"> </bsdatepicker> </div> <div class=\"col-md-2\"> S/D </div> <div class=\"col-md-5\"> <bsdatepicker name=\"ValidThru\" min-date=\"endMinOption\" max-date=\"endMaxOption\" date-options=\"DateFilterOptions2\" ng-change=\"endDateChange\" ng-model=\"mMtrlRetailDisc.ValidThru\" ng-required=\"true\"> </bsdatepicker> </div> </div> </div> <div class=\"form-group\" show-errors> <label>Tipe Material</label> <bsselect ng-model=\"mMtrlRetailDisc.PartsClassId\" data=\"MaterialTypeData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Tipe Material\" on-select=\"setType(selected)\" icon=\"fa fa-search\"> </bsselect> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>No. Material</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-pencil\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"PartId\" placeholder=\"Material\" ng-model=\"mMtrlRetailDisc.PartId\" ng-maxlength=\"50\" required>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"PartId\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>No. Material</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect \r" +
    "\n" +
    "                        ng-model=\"mMtrlRetailDisc.PartId\" \r" +
    "\n" +
    "                        data=\"typeSetData\" \r" +
    "\n" +
    "                        item-text=\"PartsCode\" \r" +
    "\n" +
    "                        item-value=\"PartsId\" \r" +
    "\n" +
    "                        placeholder=\"Pilih No. Material\" \r" +
    "\n" +
    "                        icon=\"fa fa-search\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Material</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-search\" ng-click=\"searchMaterial(mMtrlRetailDisc.PartsClassId,mMtrlRetailDisc.PartsCode)\"></i> <input type=\"text\" class=\"form-control\" name=\"PartId\" placeholder=\"Material\" ng-model=\"mMtrlRetailDisc.PartsCode\" ng-maxlength=\"50\" required> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Material</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control disable\" name=\"PartsName\" placeholder=\"Material\" ng-model=\"mMtrlRetailDisc.PartsName\" ng-maxlength=\"50\" disabled skip-enable> </div> <div class=\"input-icon-right\" ng-hide=\"true\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PartId\" placeholder=\"Material\" ng-model=\"mMtrlRetailDisc.PartId\" ng-maxlength=\"50\" disabled required skip-enable> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Discount</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"discount\" placeholder=\"Discount\" ng-model=\"mMtrlRetailDisc.Discount\" min=\"0\" max=\"100\" required> </div> <bserrmsg field=\"Discount\"></bserrmsg> </div> </div> </div> <!-- <input type=\"file\" accept=\".xlsx\" id=\"uploadMRD\" onchange=\"angular.element(this).scope().loadXLS(this)\"\r" +
    "\n" +
    "                    style=\"display: none;\"> --> <div class=\"ui modal\" id=\"ModalUploadDiscount\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <!-- <input type=\"file\" accept=\".xlsx\" id=\"UploadDiscount\" onchange=\"loadXLSDiscount(this)\"> --> <input type=\"file\" accept=\".xlsx\" id=\"UploadDiscount\" onchange=\"angular.element(this).scope().loadXLSDiscount(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"UploadDiscount_Upload_Button\" ng-click=\"UploadDiscountClicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div>  <!-- <bsmodal id=\"modal_report\" title=\"Pembuatan Report\" mode=\"\"\r" +
    "\n" +
    "        visible=\"sm_show2\" \r" +
    "\n" +
    "        size=\"small\"\r" +
    "\n" +
    "        hide-footer=\"true\"\r" +
    "\n" +
    ">\r" +
    "\n" +
    "    <p style=\"font-size:15px;\">Mohon menunggu pembuatan report....</p>\r" +
    "\n" +
    "</bsmodal> --></bsform-grid>"
  );


  $templateCache.put('app/master/parts/kelompok.html',
    "<div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Parts</bsreqlabel> <bsselect ng-model=\"mParts.PartsClassId1\" data=\"PartsTypeData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Tipe Parts\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isViewKelompok == 0\"> <!-- ng-disabled=\"list_PartsClassId5_disable.indexOf(parts_class_id5_edit) != -1\"> --> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Stocking Policy</bsreqlabel> <bsselect ng-model=\"mParts.PartsClassId4\" data=\"StockPData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Stocking Policy\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isViewKelompok == 0\"> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>SCC</bsreqlabel> <bsselect ng-model=\"mParts.PartsClassId3\" data=\"SCCData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih SCC\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isViewKelompok == 0\"> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>ICC</bsreqlabel> <bsselect ng-model=\"mParts.PartsClassId2\" data=\"ICCData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih ICC\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isViewKelompok == 0\"> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Franchise (Report)</label> <!-- ng-model=\"mParts.ReportGroup\"  --> <bsselect ng-model=\"mParts.PartsClassId6\" data=\"FranchiseData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Franchise\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isViewKelompok == 0\"> </bsselect> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Clasification</bsreqlabel> <bsselect ng-model=\"mParts.PartsClassId5\" data=\"ClassifData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Clasification\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isViewKelompok == 0\"> </bsselect> <!-- <bserrmsg field=\"VendorId\"></bserrmsg> --> </div> </div> </div>"
  );


  $templateCache.put('app/master/parts/lokasi.html',
    "<!-- <div class=\"col-md-4\">\r" +
    "\n" +
    "    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "        <bsreqlabel>Lokasi Gudang</bsreqlabel>\r" +
    "\n" +
    "        <bstypeahead placeholder=\"Nama Gudang\" name=\"WarehouseId\"\r" +
    "\n" +
    "            ng-model=\"mMaterial.WarehouseId\"\r" +
    "\n" +
    "            get-data=\"getWarehouse\"\r" +
    "\n" +
    "            model-key=\"WarehouseId\"\r" +
    "\n" +
    "            ng-minlength=\"2\" ng-maxlength=\"10\" required disallow-space\r" +
    "\n" +
    "            loading=\"loadingUsers\"\r" +
    "\n" +
    "            noresult=\"noResults\"\r" +
    "\n" +
    "            selected=\"selected\"\r" +
    "\n" +
    "            icon=\"fa-id-card-o\"\r" +
    "\n" +
    "        >\r" +
    "\n" +
    "        </bstypeahead>\r" +
    "\n" +
    "        <bserrmsg field=\"WarehouseId\"></bserrmsg>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div class=\"col-md-4\">\r" +
    "\n" +
    "    <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "        <bsreqlabel>Lokasi Rak</bsreqlabel>\r" +
    "\n" +
    "        <bstypeahead placeholder=\"Nama Rak\" name=\"WarehouseShelfId\"\r" +
    "\n" +
    "            ng-model=\"mMaterial.WarehouseShelfId\"\r" +
    "\n" +
    "            get-data=\"getWarehouseShelf\"\r" +
    "\n" +
    "            model-key=\"WarehouseShelfId\"\r" +
    "\n" +
    "            ng-minlength=\"2\" ng-maxlength=\"10\" required disallow-space\r" +
    "\n" +
    "            loading=\"loadingUsers\"\r" +
    "\n" +
    "            noresult=\"noResults\"\r" +
    "\n" +
    "            selected=\"selected\"\r" +
    "\n" +
    "            icon=\"fa-id-card-o\"\r" +
    "\n" +
    "        >\r" +
    "\n" +
    "        </bstypeahead>\r" +
    "\n" +
    "        <bserrmsg field=\"WarehouseShelfId\"></bserrmsg>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>  --><!-- <div class=\"row\">\r" +
    "\n" +
    "    <div class=\"col-md-4\">\r" +
    "\n" +
    "        <div class=\"form-group\">\r" +
    "\n" +
    "            <bsreqlabel>Gudang</bsreqlabel>\r" +
    "\n" +
    "            <bsselect \r" +
    "\n" +
    "                ng-model=\"warehouse.WarehouseId\" \r" +
    "\n" +
    "                data=\"whData\" \r" +
    "\n" +
    "                item-text=\"WarehouseName\" \r" +
    "\n" +
    "                item-value=\"WarehouseId\" \r" +
    "\n" +
    "                placeholder=\"Pilih Gudang\" \r" +
    "\n" +
    "                on-select=\"setShelf(selected)\"\r" +
    "\n" +
    "                icon=\"fa fa-search\">\r" +
    "\n" +
    "            </bsselect>\r" +
    "\n" +
    "            <bserrmsg field=\"WarehouseId\"></bserrmsg>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"col-md-4\">\r" +
    "\n" +
    "        <div class=\"form-group\">\r" +
    "\n" +
    "            <bsreqlabel>Rak</bsreqlabel>\r" +
    "\n" +
    "            <bsselect \r" +
    "\n" +
    "                ng-model=\"warehouse.ShelfId\" \r" +
    "\n" +
    "                data=\"shData\" \r" +
    "\n" +
    "                item-text=\"ShelfName\" \r" +
    "\n" +
    "                item-value=\"ShelfId\" \r" +
    "\n" +
    "                placeholder=\"Pilih Satuan Dasar\"\r" +
    "\n" +
    "                icon=\"fa fa-search\">\r" +
    "\n" +
    "            </bsselect>\r" +
    "\n" +
    "            <bserrmsg field=\"ShelfId\"></bserrmsg>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"col-md-4\">\r" +
    "\n" +
    "        <div class=\"form-group\" style=\"margin-top:22.1px\">\r" +
    "\n" +
    "            <button class=\"rbtn btn\" \r" +
    "\n" +
    "                onclick=\"this.blur();\"\r" +
    "\n" +
    "                ng-click=\"addAlloc()\">\r" +
    "\n" +
    "                <i></i>&nbsp;Tambah Alokasi Gudang\r" +
    "\n" +
    "            </button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div> --> <br> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridDetailLoc\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div>"
  );


  $templateCache.put('app/master/parts/nilai.html',
    "<div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Retail</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"number\" skip-enable class=\"form-control\" name=\"RetailPrice\" placeholder=\"Harga Retail\" ng-model=\"mParts.RetailPrice\" min=\"0\" ng-required=\"!(list_PartsClassId5_disable.indexOf(mParts.PartsClassId5) != -1)\" ng-disabled=\"isView\"> </div> <bserrmsg field=\"RetailPrice\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Lended Cost</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"number\" class=\"form-control\" name=\"StandardCost\" placeholder=\"Lended Cost\" ng-model=\"mParts.StandardCost\" min=\"0\" ng-required=\"!(list_PartsClassId5_disable.indexOf(mParts.PartsClassId5) != -1)\" skip-enable ng-disabled=\"isView\"> </div> <bserrmsg field=\"StandardCost\"></bserrmsg> </div> </div>"
  );


  $templateCache.put('app/master/parts/parameter.html',
    "<fieldset class=\"scheduler-border\"> <legend class=\"scheduler-border\">Parameter MIP</legend> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\"> <bscheckbox ng-model=\"mParts.paramByParts\" label=\"Parameter By Parts\"> </bscheckbox> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Order Cycle</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"QtyParamOrderCycle\" placeholder=\"Order Cycle\" ng-model=\"mParts.QtyParamOrderCycle\" required> </div> <bserrmsg field=\"QtyParamOrderCycle\"></bserrmsg> </div> </div> <div class=\"col-md-1\"> <div class=\"form-group\" style=\"margin-top:20px\"> Kali per Hari </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>SS Demand (Max)</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"QtyParamDemandSS\" placeholder=\"SS Demand (Max)\" ng-model=\"mParts.QtyParamDemandSS\" required> </div> <bserrmsg field=\"QtyParamDemandSS\"></bserrmsg> </div> </div> <div class=\"col-md-1\"> <div class=\"form-group\" style=\"margin-top:25px\"> Hari </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\"> <bscheckbox ng-model=\"mParts.paramByParts\" label=\"New Model / Special Campaign / Others\"> </bscheckbox> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Lead Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"QtyParamLeadTime\" placeholder=\"Lead Time\" ng-model=\"mParts.QtyParamLeadTime\" required> </div> <bserrmsg field=\"QtyParamLeadTime\"></bserrmsg> </div> </div> <div class=\"col-md-1\"> <div class=\"form-group\" style=\"margin-top:25px\"> Hari </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>SS Lead Time</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"QtyParamLeadTimeSS\" placeholder=\"SS Lead Time\" ng-model=\"mParts.QtyParamLeadTimeSS\" required> </div> <bserrmsg field=\"QtyParamLeadTimeSS\"></bserrmsg> </div> </div> <div class=\"col-md-1\"> <div class=\"form-group\" style=\"margin-top:25px\"> Hari </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Min Qty MIP</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"QtyParamMinMIP\" placeholder=\"Min Qty MIP\" ng-model=\"mParts.QtyParamMinMIP\" required> </div> <bserrmsg field=\"QtyParamMinMIP\"></bserrmsg> </div> </div> <div class=\"col-md-1\"> <div class=\"form-group\" style=\"margin-top:25px\"> Hari </div> </div> </div> </fieldset> <fieldset class=\"scheduler-border\"> <legend class=\"scheduler-border\">Info</legend> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>DAD</bsreqlabel> <div class=\"input-icon-right\"> <input type=\"number\" class=\"form-control\" name=\"Dad\" placeholder=\"DAD\" ng-model=\"mParts.Dad\" ng-disabled=\"mParts.disable_mip_dad == 1\"> QTY </div> <bserrmsg field=\"Dad\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>MIP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"number\" class=\"form-control\" name=\"Mip\" placeholder=\"MIP\" ng-model=\"mParts.Mip\" ng-disabled=\"mParts.disable_mip_dad == 1\"> </div> <bserrmsg field=\"Mip\"></bserrmsg> </div> </div> </div> </fieldset>"
  );


  $templateCache.put('app/master/parts/parts.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">fieldset.scheduler-border {\r" +
    "\n" +
    "        border: 1px groove #ddd !important;\r" +
    "\n" +
    "        padding: 0 1.4em 1.4em 1.4em !important;\r" +
    "\n" +
    "        margin: 0 0 1.5em 0 !important;\r" +
    "\n" +
    "        -webkit-box-shadow:  0px 0px 0px 0px #000;\r" +
    "\n" +
    "                box-shadow:  0px 0px 0px 0px #000;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    legend.scheduler-border {\r" +
    "\n" +
    "        width:inherit; /* Or auto */\r" +
    "\n" +
    "        padding:0 10px; /* To give a bit of padding on the left and right */\r" +
    "\n" +
    "        border-bottom:none;\r" +
    "\n" +
    "    }</style> <!-- on-before-cancel=\"afterCancel\" --> <!-- udah ga di pake--> <bsform-grid ng-form=\"PartsForm\" factory-name=\"Parts\" model=\"mParts\" model-id=\"PartsId\" loading=\"loading\" show-advsearch=\"on\" selected-rows=\"xParts.selected\" on-select-rows=\"onSelectRows\" get-data=\"getData(mParts)\" on-show-detail=\"beforeView\" on-before-new=\"beforeNew\" on-before-edit=\"beforeEdit\" do-custom-save=\"doCustomSave\" modal-title=\"Parts\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" custom-action-button-settings=\"actionButtonSettings\"> <bsform-advsearch> <div class=\"row\"> <div class=\"col-md-2\"> <div class=\"input-group\"> <label class=\"control-label\">No. Material *</label> <input type=\"text\" class=\"form-control\" onkeyup=\"this.value = this.value.toUpperCase()\" placeholder=\"Input Nomor Material\" ng-model=\"mParts.PartsCode\"> </div> </div> <div class=\"col-md-2\"> <div class=\"input-group\"> <label class=\"control-label\">Nama Material *</label> <input type=\"text\" class=\"form-control\" placeholder=\"Input Nama Material\" ng-model=\"mParts.PartsName\"> </div> </div> <div class=\"col-md-7 text-right\"> <label class=\"control-label\">&nbsp</label> <div class=\"\"> <button class=\"btn wbtn pull-right\" ng-click=\"onDeleteFilter()\">Hapus Filter</button> </div> <!--</div>--> </div> </div> </bsform-advsearch> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>No Parts</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"PartsCode\" placeholder=\"No Parts\" ng-model=\"mParts.PartsCode\" ng-disabled=\"isView\" skip-enable> </div> <bserrmsg field=\"PartsCode\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>No. Material Lama</label> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"OldPartsCode\" placeholder=\"No Material Lama\" ng-model=\"mParts.OldPartsCode\" disabled>\r" +
    "\n" +
    "                    </div> --> <bsselect ng-model=\"mParts.SubtitudeFromId\" data=\"PartsData\" item-text=\"PartsCode\" item-value=\"PartsId\" placeholder=\"Pilih Parts\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isView\"> </bsselect> <bserrmsg field=\"SubtitudeFromId\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bscheckbox ng-model=\"mParts.Active\" label=\"Aktif\"> </bscheckbox> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Parts</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" skip-enable name=\"PartsName\" placeholder=\"Nama Parts\" ng-model=\"mParts.PartsName\" ng-disabled=\"isView\"> </div> <bserrmsg field=\"PartsName\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <label>Nama Material Lama</label> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"OldPartsName\" placeholder=\"Nama Material Lama\" ng-model=\"mParts.OldPartsName\" disabled>\r" +
    "\n" +
    "                    </div> --> <bsselect ng-model=\"mParts.SubtitudeFromId\" data=\"PartsData\" item-text=\"PartsName\" item-value=\"PartsId\" placeholder=\"Pilih Parts\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isView\"> </bsselect> <bserrmsg field=\"OldPartsName\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Vendor</bsreqlabel> <!-- <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                        <input type=\"number\" class=\"form-control\" name=\"VendorId\" placeholder=\"Nama Vendor\" ng-model=\"mParts.VendorId\">\r" +
    "\n" +
    "                    </div> --> <bsselect ng-model=\"mParts.DefaultVendorId\" data=\"VendorData\" item-text=\"Name\" item-value=\"VendorId\" placeholder=\"Pilih Vendor\" icon=\"fa fa-search\" skip-enable ng-disabled=\"isView\"> </bsselect> <bserrmsg field=\"VendorId\"></bserrmsg> </div> </div> <div class=\"col-md-4\"> <div class=\"form-group\" show-errors> <bsreqlabel>Dibuat di</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" skip-enable name=\"MadeIn\" placeholder=\"Dibuat di\" ng-model=\"mParts.MadeIn\" ng-disabled=\"isView\"> </div> <bserrmsg field=\"MadeIn\"></bserrmsg> </div> </div> </div> <!-- <pre>\r" +
    "\n" +
    "            {{mParts}}\r" +
    "\n" +
    "        </pre>\r" +
    "\n" +
    " --> <div class=\"row\" style=\"padding-top:50px; padding-left: 15px\"> <uib-tabset active=\"activePill\"> <uib-tab index=\"0\" heading=\"Kelompok\"> <br> <div ng-include=\"'app/master/parts/kelompok.html'\"></div> </uib-tab> <uib-tab index=\"1\" heading=\"Lokasi\"> <br> <div ng-include=\"'app/master/parts/lokasi.html'\"></div> </uib-tab> <uib-tab index=\"2\" heading=\"Nilai\"> <br> <div ng-include=\"'app/master/parts/nilai.html'\"></div> </uib-tab> <uib-tab index=\"3\" heading=\"Satuan\"> <br> <div ng-include=\"'app/master/parts/satuan.html'\"></div> </uib-tab> <uib-tab index=\"4\" heading=\"Parameter\"> <br> <div ng-include=\"'app/master/parts/parameter.html'\"></div> </uib-tab> </uib-tabset> </div> <div class=\"ui modal\" id=\"ModalUploadGroupBP\" role=\"dialog\" style=\"margin-top: -50px; height: 100px; width:800px\"> <div class=\"modal-content\"> <ul class=\"list-group\"> <div class=\"col-md-12\" style=\"margin-top:40px\"> <div class=\"col-md-2\" style=\"margin-top:12px\"> Upload File </div> <div class=\"col-md-8\" style=\"border:1px solid lightgrey;padding: 10px 0 10px 10px\"> <div class=\"input-icon-right\"> <input type=\"file\" accept=\".xlsx\" id=\"UploadGroupBP\" onchange=\"angular.element(this).scope().loadXLSGroupBP(this)\"> </div> </div> <div class=\"col-md-2\"> <button id=\"UploadBpCenterSatellite_Upload_Button\" ng-click=\"UploadGroupBPClicked()\" type=\"button\" class=\"rbtn btn ng-binding ladda-button\" style=\"float: left;margin-top:5px\"> <span class=\"ladda-label\"> Upload </span><span class=\"ladda-spinner\"></span> </button> </div> </div> </ul> </div> </div> </bsform-grid> <!-- <bsmodal id=\"modal_alloc\" title=\"Alokasi Gudang\" mode=\"\"\r" +
    "\n" +
    "        visible=\"modalShow\"\r" +
    "\n" +
    "        size=\"small\"\r" +
    "\n" +
    "        hide-footer=\"true\"\r" +
    "\n" +
    ">\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-md-4\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <bsreqlabel>Gudang</bsreqlabel>\r" +
    "\n" +
    "                <bsselect\r" +
    "\n" +
    "                    ng-model=\"warehouse.WarehouseId\"\r" +
    "\n" +
    "                    data=\"whData\"\r" +
    "\n" +
    "                    item-text=\"WarehouseName\"\r" +
    "\n" +
    "                    item-value=\"WarehouseId\"\r" +
    "\n" +
    "                    placeholder=\"Pilih Gudang\"\r" +
    "\n" +
    "                    on-select=\"setShelf(selected)\"\r" +
    "\n" +
    "                    icon=\"fa fa-search\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-md-4\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <bsreqlabel>Rak</bsreqlabel>\r" +
    "\n" +
    "                <bsselect\r" +
    "\n" +
    "                    ng-model=\"warehouse.ShelfId\"\r" +
    "\n" +
    "                    data=\"shData\"\r" +
    "\n" +
    "                    item-text=\"ShelfName\"\r" +
    "\n" +
    "                    item-value=\"ShelfId\"\r" +
    "\n" +
    "                    placeholder=\"Pilih Satuan Dasar\"\r" +
    "\n" +
    "                    icon=\"fa fa-search\">\r" +
    "\n" +
    "                </bsselect>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</bsmodal> -->"
  );


  $templateCache.put('app/master/parts/satuan.html',
    "<div class=\"row\"> <!--  --> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "        <div class=\"form-group\">\r" +
    "\n" +
    "            <bsreqlabel>Satuan Beli</bsreqlabel>\r" +
    "\n" +
    "            <bsselect\r" +
    "\n" +
    "                ng-model=\"uom.UomId\"\r" +
    "\n" +
    "                data=\"UomData\"\r" +
    "\n" +
    "                item-text=\"Name\"\r" +
    "\n" +
    "                item-value=\"MasterId\"\r" +
    "\n" +
    "                placeholder=\"Pilih Satuan\"\r" +
    "\n" +
    "                icon=\"fa fa-search\">\r" +
    "\n" +
    "            </bsselect>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"col-md-3\">\r" +
    "\n" +
    "        <div class=\"form-group\">\r" +
    "\n" +
    "        <bsreqlabel>Quantity</bsreqlabel>\r" +
    "\n" +
    "        <input type=\"number\" class=\"form-control\" name=\"ConversionVal\" ng-model=\"uom.Qty\" min=\"1\" style=\"height:50\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <!--  --> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Satuan Dasar</bsreqlabel> <bsselect ng-model=\"mParts.UomId\" data=\"UomData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Satuan Dasar\" on-select=\"setUomTgt(selected)\" icon=\"fa fa-search\"> </bsselect> <bserrmsg field=\"UomId\"></bserrmsg> </div> </div> <!--  --> <!-- <div class=\"col-md-3\">\r" +
    "\n" +
    "        <div class=\"form-group\" style=\"margin-top:22.1px\">\r" +
    "\n" +
    "            <button class=\"rbtn btn\" \r" +
    "\n" +
    "                onclick=\"this.blur();\"\r" +
    "\n" +
    "                ng-click=\"addUom()\">\r" +
    "\n" +
    "                <i></i>&nbsp;Tambah Konversi\r" +
    "\n" +
    "            </button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div> --> <!--  --> </div> <!-- <pre>{{saveEffDate.EffectiveDate}}</pre> --> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\" show-errors> <bscheckbox ng-model=\"mParts.Active\" label=\"Setting Konversi Satuan 'General per Parts'\"> </bscheckbox> </div> <div ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div>"
  );


  $templateCache.put('app/master/product/productCat.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid-tree ng-form=\"ProCatForm\" factory-name=\"ProductCat\" model=\"mProCat\" loading=\"loading\" hide-new-button=\"true\" get-data=\"getData\" selected-rows=\"xProCat.selected\" on-select-rows=\"onSelectRows\" modal-title=\"Vehicle Category\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Category Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Category Name\" ng-model=\"mProCat.name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Parent Category</label> <bsselect data=\"grid.data\" item-text=\"name\" item-value=\"id\" ng-model=\"mProCat.pid\" placeholder=\"Select Parent Category\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Description</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mProCat.desc\"> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> </div> </div> </bsform-grid-tree>"
  );


  $templateCache.put('app/master/product/productModel.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid-tree ng-form=\"ProductModelForm\" factory-name=\"ProductModel\" model=\"mProductModel\" loading=\"loading\" hide-new-button=\"true\" get-data=\"getData\" selected-rows=\"xProductModel.selected\" on-select-rows=\"onSelectRows\" modal-title=\"Vehicle Model\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Model Name\" ng-model=\"mProductModel.name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Parent Model</label> <bsselect data=\"grid.data\" item-text=\"name\" item-value=\"id\" ng-model=\"mProductModel.pid\" placeholder=\"Select Parent Model\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"parentName\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Description</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mProductModel.desc\"> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> </div> </div> </bsform-grid-tree>"
  );


  $templateCache.put('app/master/region/region.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid-tree ng-form=\"RegionForm\" factory-name=\"Region\" model=\"mRegion\" loading=\"loading\" hide-new-button=\"true\" get-data=\"getData\" selected-rows=\"xRegion.selected\" on-select-rows=\"onSelectRows\" modal-title=\"Region\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Region Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Region Name\" ng-model=\"mRegion.name\" required> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <label>Parent Region</label> <bsselect data=\"grid.data\" item-text=\"name\" item-value=\"id\" ng-model=\"mRegion.pid\" placeholder=\"Select Parent Region\" icon=\"fa fa-child glyph-left\" style=\"min-width: 150px\"> </bsselect> <bserrmsg field=\"name\"></bserrmsg> </div> </div> </div> </bsform-grid-tree>"
  );


  $templateCache.put('app/master/retailParts/retailParts.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"RetailPartsForm\" factory-name=\"RetailPartsFactory\" model=\"mRetailParts\" model-id=\"PartPriceRetailId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xRetailParts.selected\" on-select-rows=\"onSelectRows\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Range Tanggal Berlaku</bsreqlabel> <div class=\"row\"> <div class=\"col-md-5\"> <bsdatepicker name=\"EffectiveDate\" date-options=\"validDateOptions\" ng-model=\"mRetailParts.EffectiveDate\"> {{mRetailParts.EffectiveDate}} </bsdatepicker> </div> <div class=\"col-md-2\"> S/D </div> <div class=\"col-md-5\"> <bsdatepicker name=\"ValidThru\" date-options=\"validDateOptions\" ng-model=\"mRetailParts.ValidThru\"> {{mRetailParts.ValidThru}} </bsdatepicker> </div> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>No. Material</bsreqlabel> <!-- <bsselect \r" +
    "\n" +
    "                        ng-model=\"mRetailParts.PartId\" \r" +
    "\n" +
    "                        data=\"partsData\" \r" +
    "\n" +
    "                        item-text=\"PartsCode\" \r" +
    "\n" +
    "                        item-value=\"PartsId\" \r" +
    "\n" +
    "                        placeholder=\"Pilih No. Material\" \r" +
    "\n" +
    "                        on-select=\"selectMtrl(selected)\"\r" +
    "\n" +
    "                        icon=\"fa fa-search\">\r" +
    "\n" +
    "                    </bsselect> --> <bstypeahead placeholder=\"Part ID\" name=\"xusername\" ng-model=\"mRetailParts.PartsCode\" get-data=\"getPartsByCode\" model-key=\"PartsId\" required disallow-space loading=\"loadingUsers\" noresult=\"noResults\" selected on-select=\"selectMtrl\" on-noresult=\"onNoResult\" on-gotresult=\"onGotResult\" template-url=\"customTemplate.html\" ta-minlength=\"1\" icon=\"fa-id-card-o\"> <bserrmsg field=\"PartId\"></bserrmsg> </bstypeahead></div> <div class=\"form-group\" show-errors> <label>Nama Material</label> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"PartsName\" placeholder=\"Nama Part\" ng-model=\"mRetailParts.PartsName\" disabled> </div> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Satuan Beli</bsreqlabel> <bsselect ng-model=\"mRetailParts.UomId\" data=\"UomData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Satuan Dasar\" on-select=\"setUomTgt(selected)\" icon=\"fa fa-search\"> </bsselect> <bserrmsg field=\"UomId\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Harga Retail Parts</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" class=\"form-control\" name=\"PartsName\" placeholder=\"Nama Part\" ng-model=\"mRetailParts.RetailPrice\" min=\"0\"> </div> <bserrmsg field=\"RetailPrice\"></bserrmsg> </div> </div> </div> </bsform-grid> <script type=\"text/ng-template\" id=\"customTemplate.html\"><a>\r" +
    "\n" +
    "      <span>{{match.label}}&nbsp;&bull;&nbsp;</span>\r" +
    "\n" +
    "      <span ng-bind-html=\"match.model.PartsCode | uibTypeaheadHighlight:query\"></span>\r" +
    "\n" +
    "  </a></script>"
  );


  $templateCache.put('app/master/settingParameterICC/settingParameterICC.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid grid-name=\"gridParent\" ng-form=\"SettingParameterICCForm\" factory-name=\"SettingParameterICC\" model=\"mSettingParameterICC\" model-id=\"PartParamICCId\" loading=\"loading\" grid-hide-action-column=\"true\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-before-new=\"onBeforeNew\" form-name=\"SettingParameterICCForm\" form-title=\"SettingParameterICC\" modal-title=\"SettingParameterICC\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" on-validate-save=\"onValidateSave\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Tanggal Berlaku</bsreqlabel>\r" +
    "\n" +
    "                    <bsdatepicker\r" +
    "\n" +
    "                        name=\"EffectiveDate\"\r" +
    "\n" +
    "                        date-options=\"validDateOptions\"\r" +
    "\n" +
    "                        ng-model=\"mSettingParameterICC.EffectiveDate\"\r" +
    "\n" +
    "                    >\r" +
    "\n" +
    "                    </bsdatepicker>\r" +
    "\n" +
    "                    <bserrmsg field=\"EffectiveDate\"></bserrmsg>\r" +
    "\n" +
    "                </div> --> <div class=\"form-group\" show-errors> <bsreqlabel>Range Tanggal Berlaku</bsreqlabel> <div class=\"row\"> <div class=\"col-md-5\"> <bsdatepicker name=\"EffectiveDate\" date-options=\"validDateOptions\" ng-model=\"mSettingParameterICC.EffectiveDate\" ng-required=\"true\"> {{mSettingParameterICC.EffectiveDate}} </bsdatepicker> </div> <div class=\"col-md-2\"> S/D </div> <div class=\"col-md-5\"> <bsdatepicker name=\"ValidThru\" date-options=\"validDateOptions\" ng-model=\"mSettingParameterICC.ValidThru\" ng-required=\"true\"> {{mSettingParameterICC.ValidThru}} </bsdatepicker> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>ICC</bsreqlabel> <bsselect name=\"PartsClassId\" ng-model=\"mSettingParameterICC.PartsClassId\" data=\"optionsTypeICC\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih ICC\" icon=\"fa fa-search\" ng-required=\"true\"> </bsselect> <bserrmsg field=\"ICCId\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" style=\"padding-left: 60px\"> <div style=\"padding-top: 25px\"> <!-- <input type=\"checkbox\" name=\"IsDeadStock\" checked=\"mSettingParameterICC.IsDeadStock\">\r" +
    "\n" +
    "                        <label style=\"padding-left: 10px;\">Kategori Dead Stock</label> --> <!-- tes --> <bscheckbox ng-model=\"mSettingParameterICC.IsDeadStock\" label=\"Kategori Dead Stock\"> </bscheckbox> </div> </div> </div> </div> <div class=\"row\" style=\"padding-top:5px; padding-left: 15px\"> <uib-tabset active=\"activePill\" type=\"pills\"> <uib-tab index=\"0\" heading=\"Perhitungan ICC\"> <br> <div class=\"panel panel-default\"> <div class=\"panel-body\"> <div class=\"col-md-6\"> <div class=\"row\" style=\"\"> <div class=\"form-group form-inline\"> <bsreqlabel style=\"padding-left: 12px; margin-bottom: 5px\">Range Frekuensi</bsreqlabel> <br> <div class=\"col-md-2\"> <input type=\"number\" class=\"form-control\" ng-maxlength=\"7\" name=\"FrequencyMin\" ng-model=\"mSettingParameterICC.FrequencyMin\" min=\"0\" style=\"text-align:center; width:130%\" required> <bserrmsg field=\"FrequencyMin\"></bserrmsg> </div> <div class=\"col-md-1\" style=\"text-align:center; padding-top:10px; padding-left: 22px\"> s/d </div> <div class=\"col-md-2\"> <input type=\"number\" ng-maxlength=\"7\" class=\"form-control\" name=\"FrequencyMax\" ng-model=\"mSettingParameterICC.FrequencyMax\" min=\"0\" style=\"text-align:center; width:130%\" required> <bserrmsg field=\"FrequencyMax\"></bserrmsg> </div> <div class=\"col-md-1\" style=\"text-align:center; padding-top:10px; padding-left: 17px\">Transaksi</div> </div> </div> <br> <div class=\"row\" style=\"\"> <div class=\"form-group form-inline\"> <bsreqlabel style=\"padding-left: 12px; margin-bottom: 5px\">Range Quantity</bsreqlabel> <br> <div class=\"col-md-2\"> <input type=\"number\" ng-maxlength=\"7\" class=\"form-control\" name=\"QtyMin\" ng-model=\"mSettingParameterICC.QtyMin\" min=\"0\" style=\"text-align:center; width:130%\" required> <bserrmsg field=\"QtyMin\"></bserrmsg> </div> <div class=\"col-md-1\" style=\"text-align:center; padding-top:10px; padding-left: 22px\"> s/d </div> <div class=\"col-md-2\"> <input type=\"number\" ng-maxlength=\"7\" class=\"form-control\" name=\"QtyMax\" ng-model=\"mSettingParameterICC.QtyMax\" min=\"0\" style=\"text-align:center; width:130%\" required> <bserrmsg field=\"QtyMax\"></bserrmsg> </div> <div class=\"col-md-1\" style=\"text-align:center; padding-top:10px; padding-left: 17px\">Qty</div> </div> </div> </div> </div> </div> </uib-tab> <uib-tab index=\"1\" heading=\"Parameter MIP\"> <br> <div class=\"panel panel-default\"> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-md-6\" style=\"\"> <div class=\"form-group\" show-errors> <label>Order Cycle</label> <div class=\"form-inline\"> <input type=\"number\" step=\"0.1\" ng-maxlength=\"10\" class=\"form-control\" ng-model=\"mSettingParameterICC.QtyParamOrderCycle\" name=\"QtyParamOrderCycle\" min=\"0\" style=\"width:30%\"> <label style=\"padding-left: 10px\">Kali per Hari</label> <bserrmsg field=\"QtyParamOrderCycle\"></bserrmsg> </div> </div> </div> <div class=\"col-md-6\" style=\"\"> <div class=\"form-group\" show-errors> <label>SS Demand (Max)</label> <div class=\"form-inline\"> <input type=\"number\" step=\"0.1\" ng-maxlength=\"10\" class=\"form-control\" ng-model=\"mSettingParameterICC.QtyParamDemandSS\" name=\"QtyParamDemandSS\" min=\"0\" style=\"width:30%\"> <label style=\"padding-left: 10px\">Hari</label> <bserrmsg field=\"QtyParamDemandSS\"></bserrmsg> </div> </div> </div> </div> <br> <div class=\"row\"> <div class=\"col-md-6\" style=\"\"> <div class=\"form-group\" show-errors> <label>Lead Time</label> <div class=\"form-inline\"> <input type=\"number\" ng-maxlength=\"10\" step=\"0.1\" class=\"form-control\" ng-model=\"mSettingParameterICC.QtyParamLeadTime\" name=\"QtyParamLeadTime\" min=\"0\" style=\"width:30%\"> <label style=\"padding-left: 10px\">Hari</label> <bserrmsg field=\"QtyParamLeadTime\"></bserrmsg> </div> </div> </div> <div class=\"col-md-6\" style=\"\"> <div class=\"form-group\" show-errors> <label>SS Lead Time</label> <div class=\"form-inline\"> <input type=\"number\" step=\"0.1\" ng-maxlength=\"10\" class=\"form-control\" ng-model=\"mSettingParameterICC.QtyParamLeadTimeSS\" name=\"QtyParamLeadTimeSS\" min=\"0\" style=\"width:30%\"> <label style=\"padding-left: 10px\">Hari</label> <bserrmsg field=\"QtyParamLeadTimeSS\"></bserrmsg> </div> </div> </div> </div> </div> </div> <div class=\"panel panel-default\"> <div class=\"panel-body\"> <div class=\"row\"> <div class=\"col-md-6\" style=\"\"> <div class=\"form-group\"> <div style=\"padding-top: 25px\"> <!-- <input type=\"checkbox\" ng-model=\"mSettingParameterICC.MinQtyMIP\" onchange=\"disableIpt()\">\r" +
    "\n" +
    "                                            <label style=\"padding-left: 10px;\">New Model / Special Campaign / Other </label> --> <bscheckbox ng-model=\"mSettingParameterICC.UsePartMinMIP\" label=\"New Model / Special Campaign / Other\" true-value=\"1\" false-value=\"0\"> </bscheckbox> </div> </div> <div class=\"form-group\" show-errors> <label>MIN Qty MIP</label> <input type=\"number\" step=\"0.1\" ng-maxlength=\"10\" ng-disabled=\"mSettingParameterICC.UsePartMinMIP == 0 \" class=\"form-control\" ng-model=\"mSettingParameterICC.QtyParamMinMIP\" id=\"QtyParamMinMIP\" name=\"QtyParamMinMIP\" min=\"0\" style=\"width:30%\"> <bserrmsg field=\"QtyParamMinMIP\"></bserrmsg> </div> </div> </div> </div> </div> </uib-tab> </uib-tabset> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/stall/stall.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">input[type=number]::-webkit-inner-spin-button,\r" +
    "\n" +
    "    input[type=number]::-webkit-outer-spin-button {\r" +
    "\n" +
    "        -wekbit-appearance: none;\r" +
    "\n" +
    "        margin: 0;\r" +
    "\n" +
    "    }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"StallForm\" form-name=\"StallForm\" factory-name=\"StallMst\" model=\"mStallMst\" model-id=\"StallId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" grid-hide-action-column=\"true\" on-show-detail=\"onShowDetail\" action-button-settings-detail=\"actionButtonSettingsDetail\" form-api=\"formApi\" do-custom-save=\"doCustomSave\" on-validate-save=\"onValidateSave\" form-title=\"StallMst\" modal-title=\"Stall\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div ng-include=\"'app/master/stall/stall_schedule.html'\" ng-if=\"schedule_mode==true\"></div> <div ng-if=\"schedule_mode==false\"> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Jenis Stall</bsreqlabel> <bsselect name=\"QC\" ng-model=\"mStallMst.Type\" data=\"StallTypeData\" item-text=\"Description\" item-value=\"StallTypeGRId\" placeholder=\"Pilih Group Type\" icon=\"fa fa-car\" required> </bsselect> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-bottom: 15px\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Nama Stall</bsreqlabel> <!-- <div class=\"input-icon-right\"> --> <input type=\"text\" class=\"form-control col-md-3\" style=\"float:left\" name=\"GroupName\" placeholder=\"StallName\" ng-model=\"mStallMst.Name\" ng-maxlength=\"50\" required> <!-- </div> --> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Teknisi 1</bsreqlabel> <bsselect name=\"QC\" ng-model=\"mStallMst.Technician1\" data=\"TechnicianData\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" placeholder=\"Pilih Group Type\" icon=\"fa fa-car\" required> </bsselect> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Teknisi 2</label> <bsselect name=\"QC\" ng-model=\"mStallMst.Technician2\" data=\"TechnicianData\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" placeholder=\"Pilih Group Type\" icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <label>Teknisi 3</label> <bsselect name=\"QC\" ng-model=\"mStallMst.Technician3\" data=\"TechnicianData\" item-text=\"EmployeeName\" item-value=\"EmployeeId\" placeholder=\"Pilih Group Type\" icon=\"fa fa-car\"> </bsselect> </div> </div> </div> <br> <br> <div class=\"col-md-12\" style=\"margin-bottom: 15px\"> <div class=\"col-md-2\"> Jam Tersedia </div> <div class=\"col-md-2\"> <div class=\"input-icon-right\"> <!-- <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.AvailableTime\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"24\"  style=\"display:inline;\"> --> <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"HH\" ng-model=\"mStallMst.AvailableTime\" ng-min=\"0\" ng-max=\"24\" style=\"display:inline\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\"> </div> </div> <div class=\"col-md-2\"> Jam </div> </div> <!-- jam istirahat 1 --> <!-- <div class=\"row\"> --> <!-- <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"col-lg-5\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Jam Istirahat 1</bsreqlabel>\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.StartBreakTime1Hour\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-lg-5 col-lg-offset-1\">\r" +
    "\n" +
    "                                <bsreqlabel>\r" +
    "\n" +
    "                                    <span style=\"color:white\">\r" +
    "\n" +
    "                                        Jam Istirahat 1\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                </bsreqlabel>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"MM\" ng-model=\"mStallMst.StartBreakTime1Minute\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div> --> <div class=\"col-md-12\" style=\"margin-bottom: 15px\"> <div class=\"col-md-2\"> <bsreqlabel>Jam Istirahat 1</bsreqlabel> </div> <div class=\"col-md-1\"> <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" name=\"AvailableTime\" ng-model=\"mStallMst.StartBreakTime1\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\" required></div> </div> <div class=\"col-md-1\" style=\"text-align:center\"> s/d </div> <!-- <div class=\"col-md-1\"> --> <!-- <bsreqlabel>*</bsreqlabel> --> <!-- </div> --> <div class=\"col-md-1\"> <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" name=\"AvailableTime\" ng-model=\"mStallMst.EndBreakTime1\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\" required></div> </div> </div> <!-- <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"col-lg-5\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <bsreqlabel>\r" +
    "\n" +
    "                                            <span style=\"color:white\">\r" +
    "\n" +
    "                                                Jam Istirahat 1\r" +
    "\n" +
    "                                            </span>\r" +
    "\n" +
    "                                        </bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.EndBreakTime1Hour\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-lg-5 col-lg-offset-1\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <bsreqlabel>\r" +
    "\n" +
    "                                            <span style=\"color:white\">\r" +
    "\n" +
    "                                                Jam Istirahat 1\r" +
    "\n" +
    "                                            </span>\r" +
    "\n" +
    "                                        </bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"MM\" ng-model=\"mStallMst.EndBreakTime1Minute\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div> --> <!-- </div> --> <!-- jam istirahat 2 --> <!-- <div class=\"row\"> --> <!-- \r" +
    "\n" +
    "                        <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"col-lg-5\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Jam Istirahat 2</bsreqlabel>\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.StartBreakTime2Hour\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-lg-5 col-lg-offset-1\">\r" +
    "\n" +
    "                                <bsreqlabel>\r" +
    "\n" +
    "                                    <span style=\"color:white\">\r" +
    "\n" +
    "                                        Jam Istirahat 2\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                </bsreqlabel>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"MM\" ng-model=\"mStallMst.StartBreakTime2Minute\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <div class=\"col-lg-4\" style=\"text-align:center;\">\r" +
    "\n" +
    "                            s/d\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"col-lg-5\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <bsreqlabel>\r" +
    "\n" +
    "                                            <span style=\"color:white\">\r" +
    "\n" +
    "                                                Jam Istirahat 2\r" +
    "\n" +
    "                                            </span>\r" +
    "\n" +
    "                                        </bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.EndBreakTime2Hour\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-lg-5 col-lg-offset-1\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <bsreqlabel>\r" +
    "\n" +
    "                                            <span style=\"color:white\">\r" +
    "\n" +
    "                                                Jam Istirahat 2\r" +
    "\n" +
    "                                            </span>\r" +
    "\n" +
    "                                        </bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"MM\" ng-model=\"mStallMst.EndBreakTime2Minute\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    " --> <div class=\"col-md-12\" style=\"margin-bottom: 15px\"> <div class=\"col-md-2\"> <bslabel>Jam Istirahat 2</bslabel> </div> <div class=\"col-md-1\"> <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\" name=\"AvailableTime\" ng-model=\"mStallMst.StartBreakTime2\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\"></div> </div> <div class=\"col-md-1\" style=\"text-align:center\"> s/d </div> <div class=\"col-md-1\"> <!-- <bsreqlabel>*</bsreqlabel> --> <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\" name=\"AvailableTime\" ng-model=\"mStallMst.EndBreakTime2\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\"></div> </div> </div> <!-- </div> --> <!-- jam istirahat 3 --> <!-- <div class=\"row\"> --> <!-- \r" +
    "\n" +
    "                        <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"col-lg-5\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Jam Istirahat 3</bsreqlabel>\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.StartBreakTime3Hour\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-lg-5 col-lg-offset-1\">\r" +
    "\n" +
    "                                <bsreqlabel>\r" +
    "\n" +
    "                                    <span style=\"color:white\">\r" +
    "\n" +
    "                                        Jam Istirahat 3\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                </bsreqlabel>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"MM\" ng-model=\"mStallMst.StartBreakTime3Minute\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <div class=\"col-lg-4\" style=\"text-align:center;\">\r" +
    "\n" +
    "                            s/d\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"col-lg-5\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <bsreqlabel>\r" +
    "\n" +
    "                                            <span style=\"color:white\">\r" +
    "\n" +
    "                                                Jam Istirahat 3\r" +
    "\n" +
    "                                            </span>\r" +
    "\n" +
    "                                        </bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.EndBreakTime3Hour\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-lg-5 col-lg-offset-1\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <bsreqlabel>\r" +
    "\n" +
    "                                            <span style=\"color:white\">\r" +
    "\n" +
    "                                                Jam Istirahat 3\r" +
    "\n" +
    "                                            </span>\r" +
    "\n" +
    "                                        </bsreqlabel>\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"MM\" ng-model=\"mStallMst.EndBreakTime3Minute\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    " --> <div class=\"col-md-12\" style=\"margin-bottom: 15px\"> <div class=\"col-md-2\"> <bslabel>Jam Istirahat 3</bslabel> </div> <div class=\"col-md-1\"> <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\" name=\"AvailableTime\" ng-model=\"mStallMst.StartBreakTime3\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\"></div> </div> <div class=\"col-md-1\" style=\"text-align:center\"> s/d </div> <div class=\"col-md-1\"> <!-- <bsreqlabel>*</bsreqlabel> --> <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" name=\"AvailableTime\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\" ng-model=\"mStallMst.EndBreakTime3\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\"></div> </div> <!-- </div> --> </div> <!-- jam Buka Default --> <!-- <div class=\"row\"> --> <!-- \r" +
    "\n" +
    "                        <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"col-lg-5\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Jam Buka Default</bsreqlabel>\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.DefaultOpenTimeHour\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-lg-5 col-lg-offset-1\">\r" +
    "\n" +
    "                                <bsreqlabel>\r" +
    "\n" +
    "                                    <span style=\"color:white\">\r" +
    "\n" +
    "                                        Jam Buka Default\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                </bsreqlabel>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"MM\" ng-model=\"mStallMst.DefaultOpenTimeMinute\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    " --> <div class=\"col-md-12\" style=\"margin-bottom: 15px\"> <div class=\"col-md-2\"> <bsreqlabel>Jam Buka Default</bsreqlabel> </div> <div class=\"col-md-1\"> <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\" name=\"AvailableTime\" ng-model=\"mStallMst.DefaultOpenTime\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" required></div> </div> </div> <!-- </div> --> <!-- jam Tutup Default --> <!-- <div class=\"row\"> --> <!-- \r" +
    "\n" +
    "                        <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"col-lg-5\">\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <bsreqlabel>Jam Tutup Default</bsreqlabel>\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control\" name=\"GroupName\"  placeholder=\"HH\" ng-model=\"mStallMst.DefaultCloseTimeHour\" ng-maxlength=\"50\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"col-lg-5 col-lg-offset-1\">\r" +
    "\n" +
    "                                <bsreqlabel>\r" +
    "\n" +
    "                                    <span style=\"color:white\">\r" +
    "\n" +
    "                                        Jam Tutup Default\r" +
    "\n" +
    "                                    </span>\r" +
    "\n" +
    "                                </bsreqlabel>\r" +
    "\n" +
    "                                <div class=\"form-group\">\r" +
    "\n" +
    "                                    <div class=\"input-icon-right\" >\r" +
    "\n" +
    "                                        <input type=\"number\" class=\"form-control col-lg-2\" name=\"GroupName\" placeholder=\"MM\" ng-model=\"mStallMst.DefaultCloseTimeMinute\" ng-min=\"0\" ng-max=\"5\"  style=\"display:inline;\">\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    " --> <div class=\"col-md-12\" style=\"margin-bottom: 15px\"> <div class=\"col-md-2\"> <bsreqlabel>Jam Tutup Default</bsreqlabel> </div> <div class=\"col-md-2\"> <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" onkeypress=\"return event.charCode >=48 && event.charCode <=57\" name=\"AvailableTime\" ng-model=\"mStallMst.DefaultCloseTime\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" required></div> </div> </div> <!-- \r" +
    "\n" +
    "                    <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <input type=\"checkbox\" ng-model=\"mStallMst.StatusCode\">\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    " --> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <div class=\"form-group\" style=\"margin-top: 15px\"> <bsreqlabel>Status</bsreqlabel> <div class=\"ui-grid-cell-contents\" style=\"cursor:pointer\"> <div class=\"ui toggle checkbox\" style=\"margin:-5px 0 0\"> <input type=\"checkbox\" ng-model=\"mStallMst.StatusCode\" ng-true-value=\"1\" ng-false-value=\"0\" name=\"status\"> <label> <span style=\"color:#2185D0\" ng-if=\"row.entity.status == 1\">Active</span> <span style=\"color:gray\" ng-if=\"row.entity.status == 0\">Inactive</span> </label> </div> </div> </div> </div> </div> <!-- <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"col-lg-4\">\r" +
    "\n" +
    "                            <div class=\"ui-grid-cell-contents\" style=\"cursor:pointer\">\r" +
    "\n" +
    "                                <div class=\"ui toggle checkbox\" style=\"margin:-5px 0 0;\" >\r" +
    "\n" +
    "                                    <input type=\"checkbox\" ng-model=\"row.entity.StatusCode\" ng-true-value =1 ng-false-value =0 name=\"status\" ng-click=\"grid.appScope.acteditStatus(COL_FIELD,row.entity.ID)\">\r" +
    "\n" +
    "                                    <label>\r" +
    "\n" +
    "                                        <span style=\"color:#2185D0;\" ng-if=\"mStallMst.StatusCode == 1\">\r" +
    "\n" +
    "                                            Active\r" +
    "\n" +
    "                                        </span>\r" +
    "\n" +
    "                                        <span style=\"color:gray;\" ng-if=\"mStallMst.StatusCode == 0\">\r" +
    "\n" +
    "                                            Inactive\r" +
    "\n" +
    "                                        </span>\r" +
    "\n" +
    "                                    </label>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    </div> --> </div>  <!-- \r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-6\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Jenis Stall</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"Type\"\r" +
    "\n" +
    "                            ng-model=\"mStallMst.Type\"\r" +
    "\n" +
    "                            data=\"TypeData\"\r" +
    "\n" +
    "                            item-text=\"Name\"\r" +
    "\n" +
    "                            item-value=\"Id\"\r" +
    "\n" +
    "                            placeholder=\"Pilih Tipe\"\r" +
    "\n" +
    "                            on-select=\"selectType(selected)\"\r" +
    "\n" +
    "                            \r" +
    "\n" +
    "                            icon=\"fa fa-car\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Nama Stall</bsreqlabel>\r" +
    "\n" +
    "                    <div class=\"input-icon-right\">\r" +
    "\n" +
    "                        <i class=\"fa fa-user\"></i>\r" +
    "\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"namaStall\" placeholder=\"Nama Stall\" ng-model=\"mStallMst.Name\"  ng-maxlength=\"50\" >\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <bserrmsg field=\"namaStall\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"form-group\" ng-show=\"isBP\">\r" +
    "\n" +
    "                    <label>Grup BP</label>\r" +
    "\n" +
    "                    <bsselect name=\"GroupBPId\"\r" +
    "\n" +
    "                            ng-model=\"mStallMst.GroupBPId\"\r" +
    "\n" +
    "                            data=\"GroupBPData\"\r" +
    "\n" +
    "                            item-text=\"GroupName\"\r" +
    "\n" +
    "                            item-value=\"Id\"\r" +
    "\n" +
    "                            placeholder=\"Pilih Grup BP\"\r" +
    "\n" +
    "                            icon=\"fa fa-car\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>   \r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-2\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Jam Tersedia</bsreqlabel>\r" +
    "\n" +
    "                    <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" name=\"AvailableTime\" ng-model=\"mStallMst.AvailableTime\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" ></div>\r" +
    "\n" +
    "                    <bserrmsg field=\"AvailableTime\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-2\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Jam Buka Default</bsreqlabel>\r" +
    "\n" +
    "                    <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" name=\"OpenTime\" ng-model=\"mStallMst.OpenTime\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" ></div>\r" +
    "\n" +
    "                    <bserrmsg field=\"OpenTime\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-2\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Jam Tutup Default</bsreqlabel>\r" +
    "\n" +
    "                     <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" name=\"CloseTime\" ng-model=\"mStallMst.CloseTime\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" ></div>\r" +
    "\n" +
    "                    <bserrmsg field=\"CloseTime\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div class=\"col-md-2\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Jam Istirahat Mulai</bsreqlabel>\r" +
    "\n" +
    "                    <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" name=\"StartBreakTime\" ng-model=\"mStallMst.StartBreakTime\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" ></div>\r" +
    "\n" +
    "                    <bserrmsg field=\"StartBreakTime\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-2\">\r" +
    "\n" +
    "                <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Jam Istirahat Selesai</bsreqlabel>\r" +
    "\n" +
    "                    <div uib-timepicker show-spinners=\"false\" max=\"maxFrom\" name=\"EndBreakTime\" ng-model=\"mStallMst.EndBreakTime\" hour-step=\"hstep\" minute-step=\"mstep\" show-meridian=\"ismeridian\" ></div>\r" +
    "\n" +
    "                    <bserrmsg field=\"EndBreakTime\"></bserrmsg>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"col-md-2\">\r" +
    "\n" +
    "                <div class=\"form-group\" style=\"margin-top: 25px;\">\r" +
    "\n" +
    "                    <bscheckbox \r" +
    "\n" +
    "                        ng-model=\"mStallMst.Status\"\r" +
    "\n" +
    "                        true-value = \"1\"\r" +
    "\n" +
    "                        false-value = \"0\"\r" +
    "\n" +
    "                        label = \"Status\">\r" +
    "\n" +
    "                    </bscheckbox>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    " --> <!-- IF NOT USING FORMLY ------------------------ -->  <!-- \r" +
    "\n" +
    "\r" +
    "\n" +
    "<sm-modal id=\"deleteStall\" class=\"modalListAllUser small\" ng-show=\"showModal_deleteStall\" \r" +
    "\n" +
    "          settings=\"{closable: false,transition:'horizontal flip',blurring: true}\">\r" +
    "\n" +
    "    <div class=\"header\" style=\"background-color: whitesmoke;\">Delete Stall</div>\r" +
    "\n" +
    "    <div class=\"content\">\r" +
    "\n" +
    "        Are you Sure you want to delete {{tempStallNameDelete}} ?\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"actions\">\r" +
    "\n" +
    "        <div class=\"ui ok right labeled icon blue basic button\" ng-click=\"deleteStall()\">Yes\r" +
    "\n" +
    "            <i class=\"checkmark icon\"></i>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"ui cancel blue basic button\">No</div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</sm-modal> --></bsform-grid>"
  );


  $templateCache.put('app/master/stall/stall_schedule.html',
    "<div> <div class=\"row\"> <div class=\"col-lg-6\"> <h1>Nama Stall: {{mStallMst.Name}}</h1> </div> </div> <div class=\"row\"> <div class=\"col-lg-2\" style=\"margin-bottom: 15px\"> <div class=\"form-group\"> <bsreqlabel>Jam Dari</bsreqlabel> <div uib-timepicker show-spinners=\"false\" name=\"FromTime\" ng-model=\"mStallMst.FromTime\" hour-step=\"1\" minute-step=\"10\" show-meridian=\"ismeridian\"></div> <!-- <em ng-messages=\"StallSchedule.FromTime.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\"> --> <!-- <p ng-message=\"\">Wajib diisi</p> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-lg-3\" style=\"margin-bottom: 15px\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Dari</bsreqlabel> <bsdatepicker date-options=\"dateOptions\" ng-model=\"mStallMst.FromDate\" class=\"form-control\" ng-click=\"open1()\" is-open=\"popup1.opened\" close-text=\"Close\"> </bsdatepicker> <!-- <p class=\"input-group\">\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" ng-click=\"open1()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"mStallMst.FromDate\" is-open=\"popup1.opened\" datepicker-options=\"dateOptions\" close-text=\"Close\" />\r" +
    "\n" +
    "                    <span class=\"input-group-btn\">\r" +
    "\n" +
    "                      <button type=\"button\" class=\"btn btn-default\" ng-click=\"open1()\"><i class=\"glyphicon glyphicon-calendar\"></i></button>\r" +
    "\n" +
    "                    </span>\r" +
    "\n" +
    "                </p> --> <!-- <em ng-messages=\"StallSchedule.FromDate.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\"> --> <!-- <p ng-message=\"\">Wajib diisi</p> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-lg-2\" style=\"margin-bottom: 15px\"> <div class=\"form-group\"> <bsreqlabel>Jam Sampai</bsreqlabel> <div uib-timepicker show-spinners=\"false\" name=\"ToTime\" ng-model=\"mStallMst.ToTime\" hour-step=\"1\" minute-step=\"10\" show-meridian=\"ismeridian\"></div> <!-- <em ng-messages=\"StallSchedule.ToTime.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\"> --> <!-- <p ng-message=\"\">Wajib diisi</p> --> </div> </div> </div> <div class=\"row\" show-errors> <div class=\"col-lg-3\" style=\"margin-bottom: 15px\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Sampai</bsreqlabel> <bsdatepicker date-options=\"dateOptions\" ng-model=\"mStallMst.ToDate\" class=\"form-control\" ng-click=\"open2()\" is-open=\"popup2.opened\" close-text=\"Close\"> </bsdatepicker> <!-- <p class=\"input-group\">\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" ng-click=\"open2()\" uib-datepicker-popup=\"{{format}}\" ng-model=\"mStallMst.ToDate\" is-open=\"popup2.opened\" datepicker-options=\"dateOptions\" close-text=\"Close\" />\r" +
    "\n" +
    "                    <span class=\"input-group-btn\">\r" +
    "\n" +
    "                      <button type=\"button\" class=\"btn btn-default\" ng-click=\"open2()\"><i class=\"glyphicon glyphicon-calendar\"></i></button>\r" +
    "\n" +
    "                    </span>\r" +
    "\n" +
    "                </p> --> <!-- <em ng-messages=\"StallSchedule.ToDate.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\"> --> <!-- <p ng-message=\"\">Wajib diisi</p> --> </div> </div> </div> <div class=\"row\" show-errors> <div class=\"col-lg-9\" style=\"margin-bottom: 15px\"> <div class=\"form-group\"> <bsreqlabel>Catatan</bsreqlabel> <div> <textarea name=\"Catatan\" type=\"text\" cols=\"70\" ng-model=\"mStallMst.InactiveReason\"></textarea> </div> <!-- <em ng-messages=\"StallSchedule.Catatan.$error\" style=\"color: rgb(185, 74, 72);\" class=\"help-block\" role=\"alert\"> --> <!-- <p ng-message=\"\">Wajib diisi</p> --> </div> </div> </div> <div class=\"row\"> <div class=\"col-lg-4\" style=\"margin-bottom: 15px; margin-right:5px\"> <button class=\"rbtn btn\" ng-click=\"addJadwal(mStallMst)\">Tambah</button> <!-- <button class=\"rbtn btn\" ng-click=\"addGroupTechnician(mGroupBP)\">Batal</button> --> </div> </div> <!-- <div class=\"row\"> --> <bsform-below-grid> <div class=\"row\" style=\"margin-top:30px\"> <div class=\"col-md-4\"> <div class=\"input-group\" style=\"margin-bottom:10px\"> <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"gridnyaAPI.grid.columns[filterColIdx].filters[0].term\"> <div class=\"input-group-btn\" uib-dropdown> <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\"> {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span> </button> <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\"> <li role=\"menuitem\" ng-repeat=\"col in gridCols\"> <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}}</a> </li> </ul> </div> </div> </div> </div> <div id=\"gridJadwal\" ui-grid=\"gridJadwal\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"gridJadwalTableHeight()\"> <div class=\"ui-grid-nodata\" ng-show=\"!gridJadwal.data.length\">No data available</div> </div> </bsform-below-grid> <!-- <div id=\"gridJadwal\" ui-grid=\"gridJadwal\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-auto-resize class=\"grid\" ng-style=\"gridJadwalTableHeight()\">\r" +
    "\n" +
    "        <div class=\"ui-grid-nodata\" ng-show=\"!gridJadwal.data.length\">No data available</div>\r" +
    "\n" +
    "    </div> --> </div>"
  );


  $templateCache.put('app/master/stockingPolicy/stockingPolicy.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"StockingPolicyForm\" factory-name=\"StockingPolicy\" model=\"mStockingPolicy\" model-id=\"PartStockingPolicyId\" loading=\"loading\" show-advsearch=\"on\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" form-name=\"StockingPolicyForm\" form-title=\"StockingPolicy\" modal-title=\"Stocking Policy\" modal-size=\"small\" icon=\"fa fa-fw fa-child\" on-validate-save=\"onValidateSave\"> <!-- Advance Search --> <bsform-advsearch> <!-- Row Atas --> <div class=\"row\" ng-form=\"FilterForm\"> <div class=\"col-md-2 form-group\" show-errors> <bsreqlabel class=\"control-label\">Tanggal Stocking Policy</bsreqlabel> <div class=\"tanggal\"> <bsdatepicker name=\"startDatef\" date-options=\"dateOptions2\" ng-model=\"mFilter.startDate\" required> </bsdatepicker> <em ng-messages=\"FilterForm.startDatef.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"date\">Can not fill letters</p> <p ng-message=\"minlength\">Jumlah karakter kurang</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <bserrmsg field=\"startDatef\"></bserrmsg> </div> </div> <div class=\"col-md-2 form-group\" show-errors> <label class=\"control-label\">&nbsp</label> <div class=\"endDate\"> <bsdatepicker name=\"endDatef\" date-options=\"dateOptions2\" ng-model=\"mFilter.endDate\" required> </bsdatepicker> <em ng-messages=\"FilterForm.endDatef.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"date\">Can not fill letters</p> <p ng-message=\"minlength\">Jumlah karakter kurang</p> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> <bserrmsg field=\"endDatef\"></bserrmsg> </div> </div> <div class=\"col-md-2 form-group\" show-errors> <label>SCC</label> <bsselect name=\"SCC\" ng-model=\"mFilter.SCC\" data=\"optionsTypeSCC\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih SCC\" icon=\"fa fa-car\"> </bsselect> <bserrmsg field=\"SCC\"></bserrmsg> </div> <div class=\"col-md-2\"> <div class=\"col-md-offset-1\"> <div class=\"\"> <label class=\"control-label\">&nbsp</label> <button class=\"btn wbtn form-control pull-right\" ng-click=\"onDeleteFilter(mFilter)\">Hapus Filter</button> </div> </div> </div> </div> <!-- end row --> </bsform-advsearch> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tanggal Berlaku</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <bsdatepicker ng-model=\"mStockingPolicy.EffectiveDate\" date-options=\"dateOptions\"> </bsdatepicker> </div> <bserrmsg field=\"EffectiveDate\"></bserrmsg> </div> <!-- <div class=\"form-group\" show-errors>\r" +
    "\n" +
    "                    <bsreqlabel>Stocking Policy</bsreqlabel>\r" +
    "\n" +
    "                    <bsselect name=\"Foreman\"\r" +
    "\n" +
    "                            ng-model=\"mStockingPolicy.Stocking\"\r" +
    "\n" +
    "                            data=\"stockingData\"\r" +
    "\n" +
    "                            item-text=\"Name\"\r" +
    "\n" +
    "                            item-value=\"Id\"\r" +
    "\n" +
    "                            placeholder=\"Pilih Stocking Policy\"\r" +
    "\n" +
    "                            required=\"true\"\r" +
    "\n" +
    "                            icon=\"fa fa-car\">\r" +
    "\n" +
    "                    </bsselect>\r" +
    "\n" +
    "                    <bserrmsg field=\"scc\"></bserrmsg>\r" +
    "\n" +
    "                </div>   --> <div class=\"form-group\" show-errors> <bsreqlabel>SCC</bsreqlabel> <bsselect name=\"PartsClassId\" ng-model=\"mStockingPolicy.PartsClassId\" data=\"optionsTypeSCC\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih SCC\" required icon=\"fa fa-car\"> </bsselect> <bserrmsg field=\"PartsClassId\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Berlaku Sampai</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <bsdatepicker ng-model=\"mStockingPolicy.ValidThru\" date-options=\"dateOptions\"> </bsdatepicker> </div> <bserrmsg field=\"ValidThru\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Frekuensi (Min)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"number\" min=\"0\" class=\"form-control\" name=\"MinFrequency\" placeholder=\"Frekuensi\" ng-model=\"mStockingPolicy.MinFrequency\" ng-pattern=\"/^-?[0-9][^\\.]*$/\" required> <em ng-messages=\"StockingPolicyForm.MinFrequency.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Tidak Boleh Pakai titik atau koma</p> </em> </div> <bserrmsg field=\"MinFrequency\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Harga (Max)</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" min=\"0\" class=\"form-control\" name=\"MaxPrice\" placeholder=\"Harga\" ng-model=\"mStockingPolicy.MaxPrice\" ng-keyup=\"givePattern(mStockingPolicy.MaxPrice,$event)\" required> </div> <bserrmsg field=\"MaxPrice\"></bserrmsg> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/taskList/taskList.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"TaskListForm\" factory-name=\"TaskListFactory\" model=\"mTaskList\" model-id=\"TaskId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xTaskList.selected\" on-select-rows=\"onSelectRows\"> <input type=\"file\" accept=\".xlsx\" id=\"uploadTask\" onchange=\"angular.element(this).scope().loadXLS(this)\" style=\"display: none\"> </bsform-grid>"
  );


  $templateCache.put('app/master/vehicleModel/vehicleModel.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"VehicleModelForm\" factory-name=\"VehicleModel\" model=\"mVehicleModel\" model-id=\"VehicleModelId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xVehicleModel.selected\" on-select-rows=\"onSelectRows\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"code\" placeholder=\"Fleet Code\" ng-model=\"mVehicleModel.VehicleModelCode\" disabled> </div> <bserrmsg field=\"code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Model Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Fleet Name\" ng-model=\"mVehicleModel.VehicleModelName\" disabled> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Brand Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mVehicleModel.BrandName\" disabled> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> <bscheckbox ng-model=\"mVehicleModel.SpotOrder\" label=\"Spot Order\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/vehicle_model/vehicleModel.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"VehicleModelForm\" factory-name=\"VehicleModel\" model=\"mVehicleModel\" model-id=\"VehicleModelId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xVehicleModel.selected\" on-select-rows=\"onSelectRows\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Model Code</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"code\" placeholder=\"Fleet Code\" ng-model=\"mVehicleModel.VehicleModelCode\" disabled> </div> <bserrmsg field=\"code\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Model Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Fleet Name\" ng-model=\"mVehicleModel.VehicleModelName\" disabled> </div> <bserrmsg field=\"name\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Brand Name</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-pencil\"></i> <input type=\"text\" class=\"form-control\" name=\"desc\" placeholder=\"Description\" ng-model=\"mVehicleModel.BrandName\" disabled> </div> <bserrmsg field=\"desc\"></bserrmsg> </div> <bscheckbox ng-model=\"mVehicleModel.SpotOrder\" label=\"Spot Order\" true-value=\"true\" false-value=\"false\"> </bscheckbox> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/vendor/satuan.html',
    "<div class=\"row\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Satuan Dasar</bsreqlabel> <bsselect ng-model=\"mVendorMst.UomId\" data=\"UomData\" item-text=\"Name\" item-value=\"MasterId\" placeholder=\"Pilih Satuan Dasar\" on-select=\"setUomTgt(selected)\" icon=\"fa fa-search\"> </bsselect> <bserrmsg field=\"UomId\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div>"
  );


  $templateCache.put('app/master/vendor/vendor.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">fieldset.scheduler-border {\r" +
    "\n" +
    "    border: 1px groove #ddd !important;\r" +
    "\n" +
    "    padding: 0 1.4em 1.4em 1.4em !important;\r" +
    "\n" +
    "    margin: 0 0 1.5em 0 !important;\r" +
    "\n" +
    "    -webkit-box-shadow:  0px 0px 0px 0px #000;\r" +
    "\n" +
    "            box-shadow:  0px 0px 0px 0px #000;\r" +
    "\n" +
    "}\r" +
    "\n" +
    "\r" +
    "\n" +
    "legend.scheduler-border {\r" +
    "\n" +
    "    width:inherit; /* Or auto */\r" +
    "\n" +
    "    padding:0 10px; /* To give a bit of padding on the left and right */\r" +
    "\n" +
    "    border-bottom:none;\r" +
    "\n" +
    "}</style> <bsform-grid ng-form=\"VendorMstForm\" factory-name=\"VendorMst\" model=\"mVendorMst\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xData.selected\" on-select-rows=\"onSelectRows\" modal-title=\"Vendor\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Vendor</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"VendorCode\" placeholder=\"No Vendor\" ng-model=\"mVendorMst.VendorCode\"> </div> <bserrmsg field=\"VendorCode\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bscheckbox ng-model=\"mVendorMst.SPLDBit\" label=\"SPLD\"> </bscheckbox> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Vendor</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"VendorName\" placeholder=\"Nama Vendor\" ng-model=\"mVendorMst.VendorName\"> </div> <bserrmsg field=\"VendorName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Vendor</bsreqlabel> <bsselect ng-model=\"mVendorMst.VendorType\" data=\"TypeData\" item-text=\"Description\" item-value=\"TypeId\" placeholder=\"Pilih Tipe Vendor\" icon=\"fa fa-search\"> </bsselect> <bserrmsg field=\"UomId\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat Kantor</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <textarea class=\"form-control\" name=\"Address\" placeholder=\"Alamat\" ng-model=\"mVendorMst.Address\"></textarea> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Email</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Email\" placeholder=\"Email\" ng-model=\"mVendorMst.Email\"> </div> <bserrmsg field=\"Email\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Alamat NPWP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <textarea class=\"form-control\" name=\"AddressNPWP\" placeholder=\"Alamat NPWP\" ng-model=\"mVendorMst.AddressNPWP\"></textarea> </div> <bserrmsg field=\"AddressNPWP\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. NPWP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"NoNPWP\" placeholder=\"No. NPWP\" ng-model=\"mVendorMst.NoNPWP\"> </div> <bserrmsg field=\"NoNPWP\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Email</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Email\" placeholder=\"Email\" ng-model=\"mVendorMst.Email\"> </div> <bserrmsg field=\"Email\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Contact Person</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"CP\" placeholder=\"Contact Person\" ng-model=\"mVendorMst.CP\"> </div> <bserrmsg field=\"CP\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>No. Telp CP</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"No. Telp\" ng-model=\"mVendorMst.Phone\"> </div> <bserrmsg field=\"Phone\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Bank 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"No. Telp\" ng-model=\"mVendorMst.Phone\"> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>No Rek Bank 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"No. Telp\" ng-model=\"mVendorMst.Phone\"> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Atas Nama Bank 1</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"No. Telp\" ng-model=\"mVendorMst.Phone\"> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Bank 2</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"No. Telp\" ng-model=\"mVendorMst.Phone\"> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>No Rek Bank 2</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"No. Telp\" ng-model=\"mVendorMst.Phone\"> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Atas Nama Bank 2</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"Phone\" placeholder=\"No. Telp\" ng-model=\"mVendorMst.Phone\"> </div> <bserrmsg field=\"Address\"></bserrmsg> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <fieldset class=\"scheduler-border\"> <legend class=\"scheduler-border\">Satuan per vendor per material</legend> <div ui-grid=\"gridVdrMtrl\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </fieldset> </div> </div> </div> </bsform-grid> <bsui-modal id=\"modal_report\" title=\"Nilai Konversi\" mode=\"\" visible size=\"small\" hide-footer=\"true\"> <p ng-model=\"selectedMtrl\"></p> <div ng-include=\"'app/master/vendor/satuan.html'\"></div> </bsui-modal>"
  );


  $templateCache.put('app/master/warehouseShelf/warehouseShelf.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">.ui-grid-viewport{\r" +
    "\n" +
    "    overflow-anchor: none;\r" +
    "\n" +
    "}</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"WarehouseShelfForm\" factory-name=\"WarehouseShelfFactory\" model=\"mWarehouseShelf\" model-id=\"WarehouseId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"xWarehouseShelf.selected\" on-before-new=\"beforeNew\" on-show-detail=\"beforeEdit\" do-custom-save=\"doCustomSave\" on-select-rows=\"onSelectRows\" on-validate-save=\"validateSave\"> <!-- IF USING FORMLY --> <!--\r" +
    "\n" +
    "        <formly-form fields=\"vm.fields\" model=\"vm.model\" form=\"vm.form\" options=\"vm.options\" novalidate>\r" +
    "\n" +
    "        </formly-form>\r" +
    "\n" +
    "    --> <!-- IF NOT USING FORMLY --> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Kode Lokasi Gudang</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"WarehouseCode\" placeholder=\"Kode Lokasi Gudang\" ng-model=\"mWarehouseShelf.WarehouseCode\" required> </div> <bserrmsg field=\"WarehouseCode\"></bserrmsg> </div> <div class=\"form-group\" show-errors> <bsreqlabel>Nama Lokasi Gudang</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <input type=\"text\" class=\"form-control\" name=\"WarehouseName\" placeholder=\"Nama Lokasi Gudang\" ng-model=\"mWarehouseShelf.WarehouseName\" required> </div> <bserrmsg field=\"WarehouseName\"></bserrmsg> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bscheckbox ng-model=\"mWarehouseShelf.Active\" label=\"Aktif\"> </bscheckbox> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Material</bsreqlabel> <bsselect ng-model=\"mWarehouseShelf.MaterialTypeId\" data=\"MaterialTypeData\" item-text=\"Description\" item-value=\"PartsClassId\" placeholder=\"Pilih Tipe Material\" icon=\"fa fa-search\"> </bsselect> </div> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <bsreqlabel>Tipe Service</bsreqlabel> <bsselect ng-model=\"mWarehouseShelf.ServiceTypeId\" data=\"ServiceTypeData\" item-text=\"Name\" item-value=\"Id\" placeholder=\"Pilih Tipe Service\" icon=\"fa fa-search\"> </bsselect> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\" show-errors> <label>Notes</label> <div class=\"input-icon-right\"> <i class=\"fa fa-user\"></i> <textarea class=\"form-control\" name=\"Notes\" placeholder=\"Notes\" ng-model=\"mWarehouseShelf.Notes\">\r" +
    "\n" +
    "                    </textarea></div> <bserrmsg field=\"Notes\"></bserrmsg> </div> </div> </div> <!-- <pre>{{saveWHShelf}}</pre> --> <div class=\"row\"> <div class=\"col-md-12\"> <div ui-grid=\"gridDetail\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-edit ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize class=\"grid\" ng-cloak> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/workHour/workHour.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"WorkHourForm\" factory-name=\"WorkHourFactory\" model=\"mData\" model-id=\"WorkHourId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-show-detail=\"onShowDetail\" on-validate-save=\"onValidateSave\" on-before-new-mode=\"onBeforeNewMode\" form-name=\"WorkHourForm\" form-title=\"Work Hour\" modal-title=\"Work Hour\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <div class=\"col-md-3 form-group\"> <label>Kode Jam Kerja</label> <input ng-model=\"mData.WorkHourCode\" style=\"width:100%\" type=\"text\" name=\"KodeJamkerja\" class=\"form-control\" ng-required=\"true\" ng-maxlength=\"20\" maxlength=\"20\"> <em ng-messages=\"KodeJamkerja.$error\" style=\"color: rgb(185, 74, 72)\" class=\"help-block\" role=\"alert\"> <p ng-message=\"maxlength\">Jumlah karakter terlalu banyak</p> </em> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 10px\"> <div class=\"col-md-3 form-group\"> <label>Jam Kerja</label> <table> <tr> <td><span uib-timepicker ng-model=\"mData.WorkHourStart\" max=\"maxFrom\" ng-change=\"fFrom()\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"ismeridian\" show-spinners=\"true\" ng-required=\"true\"></span></td> <td>&nbsp;&nbsp;-&nbsp;&nbsp;</td> <td><span uib-timepicker ng-model=\"mData.WorkHourEnd\" min=\"minThru\" ng-change=\"fThrough()\" hour-step=\"1\" minute-step=\"1\" show-meridian=\"ismeridian\" show-spinners=\"true\" ng-required=\"true\"></span></td> </tr> </table> </div> </div> </div> </bsform-grid>"
  );


  $templateCache.put('app/master/workingCalender/workingCalender.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\">li.liReminderType div.checkbox {\r" +
    "\n" +
    "    margin: 0px;\r" +
    "\n" +
    "  }\r" +
    "\n" +
    "\r" +
    "\n" +
    "  li.checkbox {\r" +
    "\n" +
    "    margin: 0px;\r" +
    "\n" +
    "  }</style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"WorkingCalenderForm\" factory-name=\"WorkingCalenderFactory\" model=\"mData\" model-id=\"WorkingCalenderId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-before-edit=\"onBeforeEdit\" on-show-detail=\"onShowDetail\" on-validate-save=\"onValidateSave\" on-before-new=\"onBeforeNewMode\" form-name=\"WorkingCalenderForm\" form-title=\"Working Calendar\" modal-title=\"Working Calendar\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <div class=\"col-md-3\"> <div class=\"form-group\"> <label>Outlet</label> <div class=\"input-icon-right\"> <i class=\"fa fa-id-card-o\"></i> <input type=\"text\" class=\"form-control\" ng-disabled=\"true\" name=\"Appointment\" placeholder=\"Nama Outlet\" skip-enable ng-model=\"mData.Outlet\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tipe Jadwal Kerja</bsreqlabel> <bsselect name=\"Category\" ng-disabled=\"isEdit\" ng-model=\"mData.WorkScheduleTypeId\" data=\"DataWorkScheduleTypes\" item-text=\"WorkScheduleTypeDesc\" item-value=\"WorkScheduleTypeId\" placeholder=\"Pilih Kategori\"> </bsselect> <bserrmsg field=\"Category\"></bserrmsg> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\" show-errors> <bsreqlabel>Tahun</bsreqlabel> <div class=\"input-icon-right\"> <i class=\"fa fa-id-card-o\"></i> <input type=\"number\" ng-disabled=\"isEdit\" disallow-space ng-minlength=\"4\" ng-model-options=\"{debounce: 250}\" maxlength=\"4\" required class=\"form-control\" ng-change=\"checkYear(mData.Year)\" name=\"Appointment\" placeholder=\"Tahun\" skip-enable ng-model=\"mData.Year\"> </div> </div> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal</bsreqlabel> <bsdatepicker min-date=\"minDateASB\" max-date=\"maxDate\" name=\"DateAppoint\" date-options=\"DateOptions\" ng-model=\"mData.StartDate\" ng-disabled=\"isYear\" ng-change=\"DateChange(dt)\"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> </div> <div class=\"col-md-3\"> <div class=\"form-group\"> <bsreqlabel>Tanggal Akhir</bsreqlabel> <bsdatepicker min-date=\"minDateASBAkhir\" max-date=\"maxDateAkhir\" name=\"DateAppointAkhir\" date-options=\"DateOptionsAkhir\" ng-model=\"mData.EndDate\" ng-disabled=\"isYearAkhir\" ng-change=\"DateChangeAkhir(dt)\"> </bsdatepicker> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-2\"> <fieldset style=\"border:1px solid #e2e2e2\"> <legend style=\"margin-left:10px;width:inherit;border-bottom:0px;margin-bottom:0px\">Set hari libur</legend> <ul> <li ng-repeat=\"item in dataDay\" style=\"list-style-type:none\"> <bscheckbox ng-model=\"mData.checked[item.NameEng]\" label=\"{{item.Name}}\" ng-true-value=\"true\" ng-false-value=\"false\" ng-click=\"check(item.NameEng,mData.checked[item.NameEng])\"> </bscheckbox> </li> </ul> </fieldset> </div> <div class=\"col-md-10\"> <fieldset style=\"border:1px solid #e2e2e2\"> <legend style=\"margin-left:10px;width:inherit;border-bottom:0px;margin-bottom:0px\">Set jam kerja</legend> <div class=\"form-group col-md-4\" ng-repeat=\"item in dataDay\" show-errors> <label>{{item.Name}}</label> <bsselect name=\"Category\" ng-required=\"!mData.checked[item.NameEng]\" ng-disabled=\"mData.checked[item.NameEng]\" ng-model=\"mData[item.NameEng]\" data=\"DataWorkHours\" item-text=\"finalDesc\" item-value=\"WorkHourId\" placeholder=\"Pilih Jam\"> </bsselect> <bserrmsg field=\"Category\"></bserrmsg> </div> </fieldset> </div> </div> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <div class=\"btn-group pull-right\"> <button type=\"button\" name=\"generate\" skip-enable ng-disabled=\"WorkingCalenderForm.$invalid\" ng-click=\"generate(mData)\" class=\"rbtn btn\">Generate</button> <button type=\"button\" name=\"tambahHari\" ng-click=\"addNewDay()\" class=\"rbtn btn\">Tambah Hari Libur</button> </div> </div> </div> <div class=\"row\" style=\"margin-top:10px\"> <!--  --> <!--             <input type=\"text\" ng-model-options=\"{debounce: 250}\" class=\"form-control\" placeholder=\"Filter\" ng-model=\"gridApi.grid.columns[filterColIdx].filters[0].term\">\r" +
    "\n" +
    "            <div class=\"input-group-btn\" uib-dropdown>\r" +
    "\n" +
    "                <button id=\"filter\" type=\"button\" class=\"btn wbtn\" uib-dropdown-toggle data-toggle=\"dropdown\" tabindex=\"-1\">\r" +
    "\n" +
    "                    {{filterBtnLabel|uppercase}}&nbsp;<span class=\"caret\"></span>\r" +
    "\n" +
    "                </button>\r" +
    "\n" +
    "                <ul class=\"dropdown-menu pull-right\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"filter\">\r" +
    "\n" +
    "                    <li role=\"menuitem\" ng-repeat=\"col in gridCols\">\r" +
    "\n" +
    "                      <a href=\"#\" ng-click=\"filterBtnChange(col)\">{{col.name|uppercase}}\r" +
    "\n" +
    "                        <span class=\"pull-right\">\r" +
    "\n" +
    "                          <i class=\"fa fa-fw fa-filter\"\r" +
    "\n" +
    "                              ng-show=\"\r" +
    "\n" +
    "                                    gridApi.grid.columns[{{$index1}}].filters[0].term!=\\\\ &&\r" +
    "\n" +
    "                                    gridApi.grid.columns[{{$index1}}].filters[0].term!=undefined\r" +
    "\n" +
    "                              \"\r" +
    "\n" +
    "                          ></i>\r" +
    "\n" +
    "                        </span>\r" +
    "\n" +
    "                      </a>\r" +
    "\n" +
    "                    </li>\r" +
    "\n" +
    "                </ul>\r" +
    "\n" +
    "            </div> --> <!--  --> <div class=\"col-md-12\"> <div class=\"col-md-12\"> <div ui-grid=\"gridGenerate\" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection ui-grid-pagination ui-grid-cellnav ui-grid-pinning ui-grid-auto-resize ui-grid-edit class=\"grid\" ng-style=\"getTableHeight()\"> <div class=\"ui-grid-nodata\" ng-if=\"!grid.data.length && !loading\" ng-cloak>No data available</div> <div class=\"ui-grid-nodata\" ng-if=\"loading\" ng-cloak>Loading data... <i class=\"fa fa-spinner fa-spin\"></i></div> </div> </div> </div> </div> </bsform-grid> <bsui-modal show=\"show_modal.show\" title=\"Tambah Hari Libur\" data=\"modal_model\" on-save=\"WeekEndSave\" on-cancel=\"WeekEndCancel\" mode=\"modalMode\"> <div ng-form name=\"contactForm\"> <div class=\"col-md-12\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <div class=\"col-md-6 form-group\"> <label>Tanggal</label> <bsdatepicker name=\"HolidayDate\" skip-enable date-options=\"dateOptions\" ng-model=\"modal_model.HolidayDate\"> </bsdatepicker> </div> </div> <div class=\"col-md-12\"> <div class=\"col-md-12 form-group\"> <label>Keterangan</label> <textarea ng-model=\"modal_model.HolidayDesc\" style=\"width:100%\" class=\"form-control\" ng-required=\"true\"></textarea> </div> </div> </div> </div> </div> </bsui-modal>"
  );


  $templateCache.put('app/master/workingScheduleType/workingScheduleType.html',
    "<script type=\"text/javascript\"></script> <style type=\"text/css\"></style> <!-- ng-form=\"RoleForm\" is a MUST, must be unique to differentiate from other form --> <!-- factory-name=\"Role\" is a MUST, this is the factory name that will be used to exec CRUD method --> <!-- model=\"vm.model\" is a MUST if USING FORMLY --> <!-- model=\"mRole\" is a MUST if NOT USING FORMLY --> <!-- loading=\"loading\" is a MUST, this will be used to show loading indicator on the grid --> <!-- get-data=\"getData\" is OPTIONAL, default=\"getData\" --> <!-- selected-rows=\"selectedRows\" is OPTIONAL, default=\"selectedRows\" => this will be an array of selected rows --> <!-- on-select-rows=\"onSelectRows\" is OPTIONAL, default=\"onSelectRows\" => this will be exec when select a row in the grid --> <!-- on-popup=\"onPopup\" is OPTIONAL, this will be exec when the popup window is shown, can be used to init selected if using ui-select --> <!-- form-name=\"RoleForm\" is OPTIONAL default=\"menu title and without any space\", must be unique to differentiate from other form --> <!-- form-title=\"Form Title\" is OPTIONAL (NOW DEPRECATED), default=\"menu title\", to show the Title of the Form --> <!-- modal-title=\"Modal Title\" is OPTIONAL, default=\"form-title\", to show the Title of the Modal Form --> <!-- modal-size=\"small\" is OPTIONAL, default=\"small\" [\"small\",\"\",\"large\"] -> \"\" means => \"medium\" , this is the size of the Modal Form --> <!-- icon=\"fa fa-fw fa-child\" is OPTIONAL, default='no icon', to show the icon in the breadcrumb --> <bsform-grid ng-form=\"WorkingScheduleTypeForm\" factory-name=\"WorkingScheduleTypeFactory\" model=\"mData\" model-id=\"WorkScheduleTypeId\" loading=\"loading\" get-data=\"getData\" selected-rows=\"selectedRows\" on-select-rows=\"onSelectRows\" on-show-detail=\"onShowDetail\" on-validate-save=\"onValidateSave\" on-before-new-mode=\"onBeforeNewMode\" on-bulk-delete=\"bulkDelete\" form-name=\"WorkingScheduleTypeForm\" form-title=\"Working Schedule Type\" modal-title=\"Working Schedule Type\" modal-size=\"small\" icon=\"fa fa-fw fa-child\"> <div class=\"row\"> <div class=\"col-md-12\" style=\"margin-top: 20px\"> <div class=\"col-md-3 form-group\"> <label>Kode Tipe Jadwal Kerja</label> <input ng-model=\"mData.WorkScheduleTypeCode\" style=\"width:100%\" type=\"text\" class=\"form-control\" ng-required=\"true\" maxlength=\"50\"> </div> <div class=\"col-md-3 form-group\"> <label>Keterangan</label> <input ng-model=\"mData.WorkScheduleTypeDesc\" style=\"width:100%\" type=\"text\" class=\"form-control\" ng-required=\"true\" maxlength=\"50\"> </div> </div> </div> </bsform-grid>"
  );

}]);
