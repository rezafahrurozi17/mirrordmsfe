/* tslint:disable:no-implicit-dependencies */
/**
 * Index tests
 */
var _this = this;
import * as tslib_1 from "tslib";
import { test } from "ava";
import * as ScanditSDK from "scandit-sdk";
// Set inside setupBrowserEnv.js
var baseUrl = "https://example.com/";
test.serial("configure", function (t) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
    var error;
    return tslib_1.__generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, t.throws(ScanditSDK.configure(""))];
            case 1:
                error = _a.sent();
                t.is(error.name, "UnsupportedBrowserError");
                ScanditSDK.BrowserHelper.checkBrowserCompatibility = function () {
                    return {
                        fullSupport: true,
                        scannerSupport: true,
                        missingFeatures: []
                    };
                };
                return [4 /*yield*/, t.throws(ScanditSDK.configure(null))];
            case 2:
                error = _a.sent();
                t.is(error.name, "NoLicenseKeyError");
                return [4 /*yield*/, t.throws(ScanditSDK.configure(""))];
            case 3:
                error = _a.sent();
                t.is(error.name, "NoLicenseKeyError");
                return [4 /*yield*/, t.throws(ScanditSDK.configure(" "))];
            case 4:
                error = _a.sent();
                t.is(error.name, "NoLicenseKeyError");
                return [4 /*yield*/, t.notThrows(ScanditSDK.configure("license_key"))];
            case 5:
                _a.sent();
                t.is(ScanditSDK.userLicenseKey, "license_key");
                t.is(ScanditSDK.scanditEngineLocation, baseUrl);
                return [4 /*yield*/, t.notThrows(ScanditSDK.configure("license_key", { engineLocation: "" }))];
            case 6:
                _a.sent();
                t.is(ScanditSDK.scanditEngineLocation, baseUrl);
                return [4 /*yield*/, t.notThrows(ScanditSDK.configure("license_key", { engineLocation: "/" }))];
            case 7:
                _a.sent();
                t.is(ScanditSDK.scanditEngineLocation, baseUrl);
                return [4 /*yield*/, t.notThrows(ScanditSDK.configure("license_key", { engineLocation: "test" }))];
            case 8:
                _a.sent();
                t.is(ScanditSDK.scanditEngineLocation, baseUrl + "test/");
                return [4 /*yield*/, t.notThrows(ScanditSDK.configure("license_key", { engineLocation: "https://another_example.com" }))];
            case 9:
                _a.sent();
                t.is(ScanditSDK.scanditEngineLocation, "https://another_example.com/");
                return [4 /*yield*/, t.notThrows(ScanditSDK.configure("license_key", { engineLocation: "https://another_example.com/" }))];
            case 10:
                _a.sent();
                t.is(ScanditSDK.scanditEngineLocation, "https://another_example.com/");
                return [2 /*return*/];
        }
    });
}); });
//# sourceMappingURL=index.spec.js.map