"use strict";
/* tslint:disable:no-implicit-dependencies */
/**
 * Parser tests
 */
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var ava_1 = require("ava");
var scandit_sdk_1 = require("scandit-sdk");
var sinon = require("sinon");
global.Worker = sinon.stub().returns({
    postMessage: sinon.stub(),
    terminate: sinon.stub()
});
URL.createObjectURL = sinon.stub();
function prepareBrowserAndLibrary() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    scandit_sdk_1.BrowserHelper.checkBrowserCompatibility = function () {
                        return {
                            fullSupport: true,
                            scannerSupport: true,
                            missingFeatures: []
                        };
                    };
                    return [4 /*yield*/, scandit_sdk_1.configure("license_key")];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
ava_1.test("constructor", function (t) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
    var s, p;
    return tslib_1.__generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, prepareBrowserAndLibrary()];
            case 1:
                _a.sent();
                s = new scandit_sdk_1.Scanner();
                p = new scandit_sdk_1.Parser(s, scandit_sdk_1.Parser.DataFormat.DLID);
                t.is(p.scanner, s);
                t.is(p.dataFormat, scandit_sdk_1.Parser.DataFormat.DLID);
                t.is(p.options, undefined);
                return [2 /*return*/];
        }
    });
}); });
ava_1.test("setOptions", function (t) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
    var s, p;
    return tslib_1.__generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, prepareBrowserAndLibrary()];
            case 1:
                _a.sent();
                s = new scandit_sdk_1.Scanner();
                p = new scandit_sdk_1.Parser(s, scandit_sdk_1.Parser.DataFormat.DLID);
                t.is(p.options, undefined);
                p.setOptions({});
                t.deepEqual(p.options, {});
                p.setOptions({
                    option1: true
                });
                t.deepEqual(p.options, {
                    option1: true
                });
                return [2 /*return*/];
        }
    });
}); });
ava_1.test("parseString", function (t) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
    var s, p, parseStringSpy;
    return tslib_1.__generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, prepareBrowserAndLibrary()];
            case 1:
                _a.sent();
                s = new scandit_sdk_1.Scanner();
                p = new scandit_sdk_1.Parser(s, scandit_sdk_1.Parser.DataFormat.DLID);
                parseStringSpy = sinon.spy(s, "parseString");
                p.parseString("");
                t.is(parseStringSpy.callCount, 1);
                t.deepEqual(parseStringSpy.getCall(0).args, [scandit_sdk_1.Parser.DataFormat.DLID, "", undefined]);
                p.parseString("abcd");
                t.is(parseStringSpy.callCount, 2);
                t.deepEqual(parseStringSpy.getCall(1).args, [scandit_sdk_1.Parser.DataFormat.DLID, "abcd", undefined]);
                p.setOptions({
                    option1: true
                });
                p.parseString("abcd");
                t.is(parseStringSpy.callCount, 3);
                t.deepEqual(parseStringSpy.getCall(2).args, [
                    scandit_sdk_1.Parser.DataFormat.DLID,
                    "abcd",
                    {
                        option1: true
                    }
                ]);
                return [2 /*return*/];
        }
    });
}); });
ava_1.test("parseRawData", function (t) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
    var s, p, parseStringSpy;
    return tslib_1.__generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, prepareBrowserAndLibrary()];
            case 1:
                _a.sent();
                s = new scandit_sdk_1.Scanner();
                p = new scandit_sdk_1.Parser(s, scandit_sdk_1.Parser.DataFormat.DLID);
                parseStringSpy = sinon.spy(s, "parseString");
                p.parseRawData(new Uint8Array([]));
                t.is(parseStringSpy.callCount, 1);
                t.deepEqual(parseStringSpy.getCall(0).args, [scandit_sdk_1.Parser.DataFormat.DLID, "", undefined]);
                p.parseRawData(new Uint8Array([97, 98, 99, 100]));
                t.is(parseStringSpy.callCount, 2);
                t.deepEqual(parseStringSpy.getCall(1).args, [scandit_sdk_1.Parser.DataFormat.DLID, "abcd", undefined]);
                p.setOptions({
                    option1: true
                });
                p.parseRawData(new Uint8Array([97, 98, 99, 100]));
                t.is(parseStringSpy.callCount, 3);
                t.deepEqual(parseStringSpy.getCall(2).args, [
                    scandit_sdk_1.Parser.DataFormat.DLID,
                    "abcd",
                    {
                        option1: true
                    }
                ]);
                p.setOptions();
                p.parseRawData(new Uint8Array([255]));
                t.is(parseStringSpy.callCount, 4);
                t.deepEqual(parseStringSpy.getCall(3).args, [scandit_sdk_1.Parser.DataFormat.DLID, "", undefined]);
                p.parseRawData(new Uint8Array([97, 98, 99, 100, 128]));
                t.is(parseStringSpy.callCount, 5);
                t.deepEqual(parseStringSpy.getCall(4).args, [scandit_sdk_1.Parser.DataFormat.DLID, "", undefined]);
                p.parseRawData(new Uint8Array([1, 2, 9, 32, 13, 10]));
                t.is(parseStringSpy.callCount, 6);
                t.deepEqual(parseStringSpy.getCall(5).args, [scandit_sdk_1.Parser.DataFormat.DLID, "\u0001\u0002\t \r\n", undefined]);
                return [2 /*return*/];
        }
    });
}); });
//# sourceMappingURL=parser.spec.js.map