"use strict";
/* tslint:disable:no-implicit-dependencies */
/**
 * CustomError tests
 */
Object.defineProperty(exports, "__esModule", { value: true });
var ava_1 = require("ava");
var scandit_sdk_1 = require("scandit-sdk");
ava_1.test("constructor", function (t) {
    var ce = new scandit_sdk_1.CustomError();
    t.deepEqual(ce.name, "");
    t.deepEqual(ce.message, "");
    t.is(ce.data, undefined);
    ce = new scandit_sdk_1.CustomError({ name: "test" });
    t.deepEqual(ce.name, "test");
    t.deepEqual(ce.message, "");
    t.is(ce.data, undefined);
    ce = new scandit_sdk_1.CustomError({ message: "test" });
    t.deepEqual(ce.name, "");
    t.deepEqual(ce.message, "test");
    t.is(ce.data, undefined);
    ce = new scandit_sdk_1.CustomError({ name: "test1", message: "test2" });
    t.deepEqual(ce.name, "test1");
    t.deepEqual(ce.message, "test2");
    t.is(ce.data, undefined);
    ce = new scandit_sdk_1.CustomError({ name: "test1", message: "test2", data: "test3" });
    t.deepEqual(ce.name, "test1");
    t.deepEqual(ce.message, "test2");
    t.deepEqual(ce.data, "test3");
});
//# sourceMappingURL=customError.spec.js.map