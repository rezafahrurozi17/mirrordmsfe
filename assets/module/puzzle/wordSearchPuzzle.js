(function(angular) {
    'use strict';

    var module = angular.module('wordSearchPuzzle', []);

    /**
     * Service
     */
    module.factory('wordSearchPuzzle', function() {


        function randomRange(min, max) {
            if (min && max) {
                return (min + Math.floor(Math.random() * (max - min + 1)));
            } else if (min) {
                return (Math.floor(Math.random() * min + 1))
            } else {
                return (Math.floor(Math.random() * 101));
            }
        }
        function fillRandom(matrix,maxRow,maxCol){
            for(var i=0;i<maxRow;i++){
                for(var j=0;j<maxCol;j++){
                    var ch= 65 + Math.floor(Math.random()* 26 );
                    if(!matrix[i][j].used)
                        matrix[i][j].letter = String.fromCharCode(ch);
                }
            }
        }
        function placeWord(word,matrix,maxRow,maxCol,dir){
            //console.log("matrix=>",word,dir,matrix);
            var used = false; var _try = 0;
            var len=word.length;
            while(!used && _try<1000){
                var r=0;var c=0;var x=0;var y=0;
                if(dir==0){
                    x=0;y=1;
                    r = Math.floor(Math.random()* maxRow);
                    c = Math.floor(Math.random()* (maxCol - len) );
                }else if(dir==1){
                    x=1;y=0;
                    r = Math.floor(Math.random()* (maxRow - len -1));
                    c = Math.floor(Math.random()* (maxCol) );
                }else if(dir==2){
                    x=1;y=1;
                    r = Math.floor(Math.random()* (maxRow - len -1));
                    c = Math.floor(Math.random()* (maxCol - len -1) );
                }else if(dir==3){
                    x=-1;y=1;
                    r = Math.floor(Math.random()* (maxRow - len -1) + len);
                    c = Math.floor(Math.random()* (maxCol - len -1) );
                }
                //Fill word to matrix
                var _used=false;
                if(!matrix[r][c].used){
                    //console.log('=>',r,c,word);
                    for(var i=0;i<len;i++){
                        var _r=r+(i*x); var _c=c+(i*y);
                        if(matrix[_r][_c].used && (matrix[_r][_c].letter != word.charAt(i))){
                            //console.log("used=>",r,c,word);
                            _used=true;
                            _try++;
                            break;
                        }
                    }
                    if(!_used){
                        used = true;
                        //console.log("place=>",r,c,word);
                        //this.matrix[r][c].used = true;
                        for(var i=0;i<len;i++){
                            var _r=r+(i*x); var _c=c+(i*y);
                            matrix[_r][_c].used = true;
                            matrix[_r][_c].letter = word.charAt(i);
                        }
                    }
                }
            }
            return (!_used);
        };

        /**
         * Word search puzzle
         * @param matrix
         * @param words
         * @constructor
         */

        function WordSearchPuzzle(matrix, words) {
            this.matrix = [];
            this.words = [];
            this.solved = false;
            var maxRow = 12;
            var maxCol = 12;

            this.startGame = function(){
                this.matrix = [];
                this.words = [];
                this.solved = false;
                // parse matrix and words
                angular.forEach(words, function(word) {
                    this.words.push({
                        name: word
                    });
                }, this);

                //setup blank matrix - JH
                for(var r=0;r<maxRow;r++){
                    //console.log('create row=>',r);
                    this.matrix.push([]);
                    for(var c=0;c<maxCol;c++){
                        //console.log('create col=>',c);
                        var item = {
                            letter: '',
                            row: r,
                            col: c,
                            used: false
                        };
                        this.matrix[r].push(item);
                    }
                }
                // place each word to the matrix (randomly) - JH
                angular.forEach(words, function(word) {
                    var len = word.length;
                    //console.log(word,len);
                    var rev=Math.floor(Math.random()* 2);
                    if(rev){
                        var new_word = '';
                        for(var i=0;i<len;i++){
                            new_word+=word.charAt(len-1-i);
                        }
                        word = new_word;
                    }
                    var dir=0 + Math.floor(Math.random()* 4);

                    //console.log("dir:",dir);
                    var used = false; var _try = 0;var success=false;
                    success=placeWord(word,this.matrix,maxRow,maxCol,dir);
                    //console.log("result placeWord=>",word,success);
                    if(!success){
                        for(i=0;i<4;i++){
                            if(i!=dir)
                                console.log("failed, now try dir = "+word+' dir'+i);
                                success=placeWord(word,this.matrix,maxRow,maxCol,i);
                                if(success)
                                    break
                                else
                                    console.log('failed to place word!!!');
                        }
                    }
                }, this);

                //Fill the rest of the matrix with random char
                fillRandom(this.matrix,maxRow,maxCol);
            };

            /**
             * Returns matrix item by coords
             * @param col
             * @param row
             * @return {*}
             */
            this.getItem = function(col, row) {
                return (this.matrix[row] ? this.matrix[row][col] : undefined);
            };

            /**
             * Returns matrix items by start/end coords
             * @param colFrom
             * @param rowFrom
             * @param colTo
             * @param rowTo
             * @return {Array}
             */
            this.getItems = function(colFrom, rowFrom, colTo, rowTo) {
                var items = [];

                if (rowTo > maxRow) {
                    rowTo = maxRow;
                }
                if (colTo > maxCol) {
                    colTo = maxCol;
                }

                if (this.getItem(colTo, rowTo) === undefined) {
                    return items;
                }

                if (colFrom === colTo || rowFrom === rowTo || Math.abs(colTo - colFrom) === Math.abs(rowTo - rowFrom)) {
                    var shiftX = (colFrom === colTo ? 0 : (colTo > colFrom ? 1 : -1)),
                        shiftY = (rowFrom === rowTo ? 0 : (rowTo > rowFrom ? 1 : -1)),
                        col = colFrom,
                        row = rowFrom;

                    items.push(this.getItem(col, row));
                    do {
                        col += shiftX;
                        row += shiftY;
                        items.push(this.getItem(col, row));
                    } while (col !== colTo || row !== rowTo);
                }

                return items;
            };

            /**
             * Check items - find word
             * @param items
             */
            this.lookup = function(items) {
                if (!items.length) {
                    return;
                }

                // create words
                var words = [''];
                angular.forEach(items, function(item) {
                    words[0] += item.letter;
                });
                words.push(words[0].split('').reverse().join(''));	// word in reverse order (when selecting)

                // check words
                this.solved = true;
                angular.forEach(this.words, function(word) {
                    if (word.found) {
                        return;
                    }
                    angular.forEach(words, function(itemWord) {
                        if (word.name === itemWord) {
                            word.found = true;
                            angular.forEach(items, function(item) {
                                item.found = true;
                            });
                        }
                    });
                    if (!word.found) {
                        this.solved = false;
                    }
                }, this);
            };

            /**
             * Solves puzzle
             */
            this.solve = function() {
                var start = {},
                    directions = {
                        N: [0, -1], E: [1, 0], S: [0, 1], W: [-1, 0],
                        NE: [1, -1], NW: [-1, -1], SE: [1, 1], SW: [-1, 1]
                    };

                // group items by letters for faster search
                angular.forEach(this.matrix, function(items) {
                    angular.forEach(items, function(item) {
                        if (!start[item.letter]) {
                            start[item.letter] = [];
                        }
                        start[item.letter].push(item);
                    });
                });

                angular.forEach(this.words, function(word) {
                    angular.forEach(start[word.name.charAt(0)], function(start) {
                        if (word.found) {
                            return;
                        }
                        angular.forEach(directions, function(shift) {
                            if (word.found) {
                                return;
                            }
                            this.lookup(this.getItems(
                                start.col, start.row,
                                start.col + (word.name.length - 1) * shift[0],
                                start.row + (word.name.length - 1) * shift[1]
                            ));
                        }, this);
                    }, this);
                }, this);
            };
        }

        return function(matrix, words) {
            return new WordSearchPuzzle(matrix, words);
        };
    });

    /**
     * Directive
     */
    module.directive('wordSearchPuzzle', function(wordSearchPuzzle) {
        return {
            restrict: 'EA',
            replace: true,
            template: '<table class="word-search-puzzle" cellspacing="0" ng-class="{\'puzzle-solved\': puzzle.solved}">' +
                '<tr ng-repeat="items in puzzle.matrix">' +
                '<td ng-repeat="item in items" unselectable="on"' +
                ' ng-class="{\'puzzle-found\': item.found, \'puzzle-selected\': item.selected, \'puzzle-message\': puzzle.solved && !item.found}"' +
                ' ng-mousedown="selectStart(item)" ng-mouseup="selectEnd()" ng-mouseenter="selectEnter(item)">' +
                ' <span>{{item.letter}}</span>' +
                '</td>' +
                '</tr>' +
                '</table>',
            scope: {
                matrix: '=',
                words: '=',
                api: '='
            },
            link: function(scope, element, attrs) {
                var selectFrom;

                // setup puzzle
                scope.$watch('matrix', function(matrix) {
                    scope.puzzle = wordSearchPuzzle(matrix, scope.words);

                    // link service
                    if (attrs.api) {
                        scope.api = scope.puzzle;
                    }
                });

                scope.start = function(){
                    scope.puzzle = wordSearchPuzzle(scope.matrix, scope.words);
                    if (attrs.api) {
                        scope.api = scope.puzzle;
                    }
                    scope.puzzle.startGame(scope.matrix,scope.words);
                }

                /**
                 * Selected items
                 * @type {Array}
                 */
                scope.selected = [];

                /**
                 * Selection start
                 * @param item
                 */
                scope.selectStart = function(item) {
                    selectFrom = item;
                };

                /**
                 * Selection enter (over)
                 * @param item
                 */
                scope.selectEnter = function(item) {
                    if (selectFrom) {
                        scope.selected = scope.puzzle.getItems(selectFrom.col, selectFrom.row, item.col, item.row);
                    }
                };

                /**
                 * Selection end
                 */
                scope.selectEnd = function() {
                    selectFrom = null;
                    scope.puzzle.lookup(scope.selected);
                    scope.selected = [];
                };

                // propagate selection state to matrix
                scope.$watch('selected', function(newItems, oldItems) {
                    angular.forEach(oldItems, function(item) {
                        item.selected = false;
                    });
                    angular.forEach(newItems, function(item) {
                        item.selected = true;
                    });
                });
            }
        };
    });
})(window.angular);
