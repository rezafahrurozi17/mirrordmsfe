//-- routes untuk part di sini
angular.module('app')
    .config(['$locationProvider','$stateProvider','$urlRouterProvider', function($locationProvider,$stateProvider, $urlRouterProvider) {
	/*
	// contoh definisi state
    $stateProvider
        .state('app.irregularity', {
            data: {
                title: 'Irregularity Board',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "irregularity@app": {
                    xtemplateUrl: 'app/master/customerFleet/customer_fleet.html',
                    templateUrl: 'app/boards/irregularity/irregularity.html',
                    controller: 'IrregularityController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('boards');
                    }],
            },
            params: { tab : null },
        })
	*/
    $stateProvider
        .state('app.pomaintenance', {
            data: {
                title: 'PO Maintenance',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "pomaintenance@app": {                    
                    templateUrl: 'app/parts/pomaintenance/pomaintenance.html',
                    controller: 'POMaintenanceController' 
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })                     
        
        .state('app.parts_mipmaintenance', {
            data: {
                title: 'MIP Maintenance',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_mipmaintenance@app": {                    
                    templateUrl: 'app/parts/procurementplanning/mipmaintenance/mipmaintenance.html',
                    controller: 'MIPMaintenanceController' 
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_soqmaintenance', {
            data: {
                title: 'SOQ Maintenance',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_soqmaintenance@app": {                    
                    templateUrl: 'app/parts/procurementplanning/soqmaintenance/soqmaintenance.html',
                    controller: 'SOQMaintenanceController' 
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.disposal', {
            data: {
                title: 'Disposal',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "disposal@app": {                    
                    templateUrl: 'app/parts/disposal/disposal.html',
                    controller: 'DisposalController' 
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        // ...
        .state('app.parts_orderstatusinquiry', {
            data: {
                title: 'Inquiry',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_orderstatusinquiry@app": {                    
                    templateUrl: 'app/parts/inquiry/orderStatus/orderStatusInquiry.html',
                    controller: 'PartsOSInquiryController' 
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        //app.parts_orderstatusinquiry
        .state('app.stockadjustment', {
            data: {
                title: 'Stock Adjustment',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "stockadjustment@app": {                    
                    templateUrl: 'app/parts/stockadjustment/stockadjustment.html',
                    controller: 'StockAdjustmentController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
		.state('app.goodsissue', {
                data: {
                    title: 'Good Issue',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "goodsissue@app": {
                        templateUrl: 'app/parts/goodsissue/goodsissue.html',
                        controller: 'GoodsIssueController'
                    }
                },
                resolve: {
                        sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('sysapp')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                        }],
                        AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('app_parts')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                        }]
                },
                params: { tab : null },
            })
		.state('app.purchaseorderparts', {
            data: {
                title: 'Purchase Order',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "purchaseorderparts@app": {
                    templateUrl: 'app/parts/purchaseorder/purchaseorder.html',
                    controller: 'PurchaseOrderController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
		
		.state('app.parts_statusstock', {
            //url: '/kb',
            data: {
                title: 'Parts Status Stock',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_statusstock@app": {
                     templateUrl: 'app/parts/stockinfo/statusstock/status_stock.html',
                     controller: 'PartsStatusStockController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('sysapp')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }],
                        AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('app_parts')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                        }]
            },
            params: { tab : null },
        })  // app.parts_statusstockcablain
        .state('app.parts_statusstockcablain', {
            //url: '/kb',
            data: {
                title: 'Parts Status Stock Cab Lain',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_statusstockcablain@app": {
                     templateUrl: 'app/parts/stockinfo/statusstock_cabanglain/statusstock_cablain.html',
                     controller: 'PartsStatusStockCabLainController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('sysapp')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }],
                        AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('app_parts')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                        }]
            },
            params: { tab : null },
        })        
        .state('app.parts_deadstock', {
            //url: '/kb',
            data: {
                title: 'Parts Status Stock',
                access: 1
            }, 
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "parts_deadstock@app": {
                     templateUrl: 'app/parts/stockinfo/deadstock/dead_stock.html',
                     controller: 'PartsDeadStockController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('sysapp')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }],
                       AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('app_parts')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }]
            },
            params: { tab : null },
        })
		.state('app.parts_materialrequest', {
            //url: '/kb',
            data: { 
                title: 'Parts Material Request',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "parts_materialrequest@app": {
                     templateUrl: 'app/parts/materialrequest/materialrequest.html',
                     controller: 'MaterialRequestController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('sysapp')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }],
                       AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('app_parts')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }]
            },
            params: { tab : null },
        })
        // app.parts_sslistqtyreserved - temporary
        .state('app.parts_sslistqtyreserved', {
            //url: '/kb',
            data: {
                title: 'Parts SS List Qty Reserved', 
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_sslistqtyreserved@app": {
                     templateUrl: 'app/parts/stockinfo/statusstock/ss_listqtyreserved.html',
                     controller: 'SS_ListQtyReservedController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('sysapp')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }],
                        AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                            return $ocLazyLoad.load('app_parts')
                            .then(function(res){

                            },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                        }]
            }
        })
		
		//[23 Februari 2017 - Okyza]
		.state('app.parts_parts_claim', {
            data: {
                title: 'Parts Claim',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_parts_claim@app": {
                    templateUrl: 'app/parts/claim/parts_claim/partsClaim.html',
                    controller: 'PartsClaimController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_rpp', {
            data: {
                title: 'RPP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_rpp@app": {
                    templateUrl: 'app/parts/claim/rpp/rppClaim.html',
                    controller: 'RppClaimController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_list_to_do', {
            data: {
                title: 'RPP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_list_to_do@app": {
                    templateUrl: 'app/parts/list_to_do/list_to_do.html',
                    controller: 'ListToDoController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_stock_opname', {
            data: {
                title: 'Stock Opname',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_stock_opname@app": {
                    templateUrl: 'app/parts/stock_opname/stock_opname_m/stockOpname.html',
                    controller: 'PartsStockOpnameController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
		
		.state('app.parts_input_hasil', {
            data: {
                title: 'Stock Opname - Input Hasil',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_input_hasil@app": {
                    templateUrl: 'app/parts/stock_opname/input_hasil/inputHasil.html',
                    controller: 'InputHasilController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
		//[23 Februari 2017 - Okyza]
        // 23 02 2017 ddg
        .state('app.parts_goodsreceipt', {
            //url: '/kb',
            data: { 
                title: 'Parts Goods Receipt',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "parts_goodsreceipt@app": {
                     templateUrl: 'app/parts/goodsreceipt/goodsreceipt.html',
                     controller: 'GoodsReceiptController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }],
                       AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                       }]
            },
            params: { tab : null },
        })
        // 23 02 2017 ddg
		
		// start. rizki - 23 feb 2017
		.state('app.parts_downloadpo', {
            data: {
                title: 'Download PO (DEPO)',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_downloadpo@app": {                    
                    templateUrl: 'app/parts/downloadpo/downloadpo.html',
                    controller: 'PartsDownloadPOController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_transferorder', {
            data: {
                title: 'Transfer Order',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_transferorder@app": {                    
                    templateUrl: 'app/parts/transferorder/transferorder.html',
                    controller: 'PartsTransferOrderController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_inquiry', {
            data: {
                title: 'Parts Inquiry',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_inquiry@app": {                    
                    templateUrl: 'app/parts/inquiry/partsInquiry/partsInquiry.html',
                    controller: 'PartsInquiryController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_etdetainquiry', {
            data: {
                title: 'ETD ETA Inquiry',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_etdetainquiry@app": {                    
                    templateUrl: 'app/parts/inquiry/etdetaInquiry/etdetaInquiry.html',
                    controller: 'PartsETDETAInquiryController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_packinglistinquiry', {
            data: {
                title: 'Packing List Inquiry',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_packinglistinquiry@app": {                    
                    templateUrl: 'app/parts/inquiry/packingList/packingListInquiry.html',
                    controller: 'PartsPackingListInquiryController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        .state('app.parts_claiminquiry', {
            data: {
                title: 'Claim Inquiry',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_claiminquiry@app": {                    
                    templateUrl: 'app/parts/inquiry/claim/claimInquiry.html',
                    controller: 'PartsClaimInquiryController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
		// end. rizki - 23 feb 2017
		// start okyza 24 02 2017
        .state('app.parts_pre_picking', {
            data: {
                title: 'Pre Picking',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_pre_picking@app": {
                    templateUrl: 'app/parts/prepicking/prePicking.html',
                    controller: 'PrePickingController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
		// end okyza 24 02 2017
        //start. rizki - 27 feb 2017
        .state('app.parts_salesorder', {
            data: {
                title: 'Sales Order',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_salesorder@app": {                    
                    templateUrl: 'app/parts/directpartssales/salesorder/salesOrder.html',
                    controller: 'PartsSalesOrderController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
        // ddg 28-02-2017 app.parts_salesbilling
        .state('app.parts_salesbilling', {
            data: {
                title: 'Sales Billing',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_salesbilling@app": {                    
                    templateUrl: 'app/parts/directpartssales/salesbilling/salesbilling.html',
                    controller: 'PartsSalesBillingController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })
		.state('app.parts_deadstockmaintain', {
            data: {
                title: 'Dead Stock Maintain',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parts_deadstockmaintain@app": {                    
                    templateUrl: 'app/parts/deadstockmaintain/deadstockmaintain.html',
                    controller: 'PartsDeadStockMaintainController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_parts')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
            },
            params: { tab : null },
        })

        function errorPopUp(err, ngDialog, cfpLoadingBar){
            if (ngDialog.getOpenDialogs().length==0) {                        
                ngDialog.open({ 
                    template: '_sys/auth/dialog_err_default.html',
                    controller: ['$scope', '$timeout', function($scope) {
                        $scope.status = "Error Koneksi";
                        $scope.message = "Mohon cek koneksi anda / aplikasi sedang dalam maintenance. Silakan coba beberapa saat lagi, Terima kasih";
                        $scope.debugMode = false;
                    }] 
                });
                cfpLoadingBar.complete();
            }
        }
  }])