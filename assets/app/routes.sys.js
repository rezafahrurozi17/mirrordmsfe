//"use strict";

//angular.module('app', ['ui.router','ngResource']);

angular.module('app')
  .config(['$locationProvider','$stateProvider','$urlRouterProvider', function($locationProvider,$stateProvider, $urlRouterProvider) {

$stateProvider
        // .state('login', {
        //         //url: '/login',
        //         views: {
        //             root: {
        //                 templateUrl: '_sys/auth/login.html',
        //                 controller: 'LoginController'
        //             }
        //         },
        //         data: {
        //             title: 'Login',
        //             htmlId: 'extr-page',
        //             access: 0
        //         },
        // })
        .state('app', {
                // abstract: true,
                data: {
                    access: 1
                },
                views: {
                    root: {
                        templateUrl: '_sys/themes/layout/layout.tpl.html',
                        controller: 'IdleCtrl'
                    }
                },
                // resolve: {
                //     scripts: function(lazyScript){
                //         return lazyScript.register([
                //                 'sparkline',
                //                 'easy-pie'
                //             ]);
                //     }
                // }
                resolve: {
                   dashboardModule: ['$ocLazyLoad',function($ocLazyLoad){
                     return $ocLazyLoad.load('sysdashboard');
                   }],
                }
        })
        .state('app.dashboard', {
            // url: '/',
            data:{
                title: 'Dashboard'
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "dashboard@app": {
                    templateUrl: 'app_dashboard/dashboard.html',
                    controller: 'DashboardCtrl'
                }
            },
            // resolve: {
            //     scripts: function(lazyScript){
            //         return lazyScript.register([
            //                 'jquery-jvectormap-world-mill-en',
            //                 'flot-time',
            //                 'flot-resize'
            //             ]);
            //     }
            // }
            resolve: {
                   // templates_dashboard: ['$ocLazyLoad',function($ocLazyLoad){
                   //   return $ocLazyLoad.load('templates_dashboard');
                   // }],
                   // sysdashboard: ['$ocLazyLoad',function($ocLazyLoad){
                   //   return $ocLazyLoad.load('sysdashboard');
                   // }],
            },
            params: { tab : null },
        })
        // -------------------------- //
        // System Application Modules //
        // -------------------------- //
        .state('app.role', {
            //url: '/role',
            data: {
                    title: 'Role Management',
                    access: 1,
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "role@app": {
                    templateUrl: '_sys/role/role.html',
                    controller: 'RoleController as vm'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp')
                         .then(
                            function success(args) {
                              //console.log('success');
                              return args;
                            },
                            function error(err) {
                              //console.log("err=>",err);
                            return err;
                         });
                       }],
            },
            params: { tab : null },
        })
        .state('app.org', {
            // url: '/org',
            data: {
                title: 'Organization Chart Management',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "org@app": {
                    templateUrl: '_sys/org/org.html',
                    controller: 'OrgChartController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
            },
            params: { tab : null },
        })
        .state('app.orgtype', {
            // url: '/orgtype',
            data: {
                title: 'Organization Type Management',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "orgtype@app": {
                    templateUrl: '_sys/org/orgType.html',
                    controller: 'OrgTypeController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
            },
            params: { tab : null },
        })
        .state('app.userlist', {
            //url: '/user',
            data: {
                title: 'User List',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "userlist@app": {
                    templateUrl: '_sys/user/user.html',
                    controller: 'UserController as vm'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }]
            },
            params: { tab : null },
        })
        .state('app.userorgrole', {
            //url: '/user',
            data: {
                title: 'User Management',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "userorgrole@app": {
                    templateUrl: '_sys/user/userOrgRole.html',
                    controller: 'UserOrgRoleController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }]
            },
            params: { tab : null },
        })
        //---------------------------------------------------------------------
        .state('app.userorgrole.linkview', {
            //url: '/user',
            data: {
                title: 'User Management - Link',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "userorgrole.linkview": {
                    templateUrl: '_sys/role/role.html',
                    controller: 'RoleController as vm'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }]
            },
            params: { tab : null },
        })
        //---------------------------------------------------------------------
        .state('app.userloginlog', {
            //url: '/user',
            data: {
                title: 'User Login Log',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "userloginlog@app": {
                    templateUrl: '_sys/user/userLoginLog.html',
                    controller: 'UserLoginLogController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }]
            },
            params: { tab : null },
        })
        .state('app.am', {
            //url: '/am',
            data: {
                title: 'Access Matrix',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "am@app": {
                    templateUrl: '_sys/menu/accessMatrix.html',
                    controller: 'AccessMatrixController as vm'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        // kata faisal remark aja supaya aggridnya ga manggil 2x , kalo manggil 2x jadi ga bisa aggridnya
                        // agGrid: ['$ocLazyLoad', function($ocLazyLoad) {
                        //     return $ocLazyLoad.load('agGrid');
                        // }],
                       
            },
            params: { tab : null },
        })
        .state('app.svclist', {
            //url: '/svc',
            data: {
                title: 'Service',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "svclist@app": {
                    templateUrl: '_sys/service/serviceList.html',
                    controller: 'ServiceListController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }]
            },
            params: { tab : null },
        })
        .state('app.resourcelist', {
            //url: '/appl',
            data: {
                title: 'App Resource',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "resourcelist@app": {
                    templateUrl: '_sys/service/appResourceList.html',
                    controller: 'AppResourceListController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }]
            },
            params: { tab : null },
        })
        .state('app.sam', {
            //url: '/sam',
            data: {
                title: 'Service Access Matrix',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "sam@app": {
                    templateUrl: '_sys/service/serviceAccessMatrix.html',
                    controller: 'ServiceAccessMatrixController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }]
            },
            params: { tab : null },
        })
        .state('app.menulist', {
            //url: '/sam',
            data: {
                title: 'Menu Manager',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "menulist@app": {
                    templateUrl: '_sys/menu/menulist.html',
                    controller: 'MenuListController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }]
            },
            params: { tab : null },
        })

        if(window.history && window.history.pushState){
          $locationProvider.html5Mode({enabled: true,requireBase:false});
        };
        $urlRouterProvider.deferIntercept();
  }]);