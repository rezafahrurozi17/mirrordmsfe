// chip definition
JsBoard.Chips.jpcbbp = {}
JsBoard.Chips.jpcbbp.jpcb = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    
    {name: 'tglcetakwo', type: 'text', text: {data: 'data', field: 'tglcetakwo'}, x: 4, y: 2, align: 'right', width: {data: 'chip', field: 'width'}, 
        margin: {right: 10},
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'type'}, x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    {name: 'jamcetakwo', type: 'text', text: {data: 'data', field: 'jamcetakwo'}, x: 4, y: 16, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 10},
    },
    {name: 'wotype', type: 'text', text: {data: 'data', field: 'wotype'}, x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 10},
    },
    // {name: 'start2', type: 'text', text: {data: 'data', field: 'start2'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}},
    // {name: 'jamjanjiserah', type: 'text', text: {data: 'data', field: 'jamjanjiserah'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}, 
    //     visible: function(base, name, data, me){
    //         if (data['data']['width']>0.5){
    //             return true
    //         } else {
    //             return false;
    //         }
    //     }
    // },
    // {name: 'pekerjaan', type: 'text', text: {data: 'data', field: 'pekerjaan'}, x: 0, y: 51, width: {data: 'chip', field: 'width'}},
    // {name: 'teknisi', type: 'text', text: {data: 'data', field: 'teknisi'}, x: 30, y: 51, align: 'right', 
    //     wrap: 'none',
    //     xwidth: {data: 'chip', field: 'width'},
    //     width: function(base, name, data, me){
    //         return data['chip']['width']-this.x;
    //     }
    // },
    {name: 'normal', type: 'rect', x: 0, y:36, 
      width: function(base, name, data, me){
        console.log('chip-rect-2', data['chip']['width'])
        var vext = 0;
        var vwidth = 1;
        var vcolwidth = 50;
        var vnormal = data['data']['width'];
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        return vcolwidth*vnormal;
      }, 
      height: 21,
      xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      fill: function(base, name, data, me){
          return JsBoard.Common.RandomColorList[data['data']['color_idx']]
      },
    //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
      margin: {left: 8, right: 6, top: 0, bottom: 0}
    },
    // {name: 'ext', type: 'rect', x: 0, y:32, 
    //   x: function(base, name, data, me){
    //     var vnormal = 0;
    //     var vcolwidth = 50;
    //     if (typeof data['data']['normal']!=='undefined'){
    //       vnormal = data['data']['normal'];
    //     }
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     return vcolwidth*vnormal;
    //   }, 
    //   width: function(base, name, data, me){
    //     var vext = 0;
    //     var vcolwidth = 50;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vnormal = data['data']['ext'];
    //     }
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     return vcolwidth*vnormal;
    //   }, 
    //   visible: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return 1;
    //     } else {
    //       return 0
    //     }
    //   },
    //   height: 21,
    //   fill: '#ffA520', 
    //   margin: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return {left: 0, top: 0, bottom: 0}
    //     } else {
    //       return {top: 0, bottom: 0}
    //     }
    //   }
    // },
    // {name: 'type', type: 'icons', 
    //     x: 0, y: 26, 
    //     icons: {data: 'data', field: 'statusicon', default: []},
    //     width: function(base, name, data, me){
    //         console.log('chip-rect-2', data['chip']['width'])
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //         vnormal = data['data']['normal'];
    //         }
    //         return vcolwidth*vnormal;
    //     }, 
    // },
    // {name: 'type', type: 'icons', 
    //     xxx: 25,
    //     x: function(base, name, data, me){
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //             vnormal = data['data']['normal'];
    //         }
    //         if (typeof data['data']['ext']!=='undefined'){
    //             vext = data['data']['ext'];
    //         }
    //         vwidth = vnormal+vext;
    //         return Math.round(vwidth*vcolwidth/2)-14;
    //     }, 
    //     y: 46, 
    //     icons: ['play2'],
    //     width: 20,
    //     height: 16,
    //     iconHeight: 14,
    //     iconWidth: 14,
    //     visible: function(base, name, data, me){
    //         vallocated = 1;
    //         if (typeof data['data']['allocated']!=='undefined'){
    //             vallocated = data['data']['allocated'];
    //         }
    //         if (vallocated){
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // },
  ]
}

JsBoard.Chips.jpcbbp.jpcb_stall_base = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
  ]
}

JsBoard.Chips.jpcbbp.jpcb_stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, 
        width: function(base, name, data, me){
            return data['chip']['width'] - data['chip']['stallTechWidth'];
        },
        fontSize: 14,
        margin: {left: 5, top: 25, right: 5, bottom: 25}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
    {name: 'technician', type: 'text', text: {data: 'data', field: 'technician'}, x: 0, y: 0, 
        width: {data: 'chip', field: 'stallTechWidth'}, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['chip']['stallTechWidth'];
        },
        fontSize: 14,
        margin: {left: 5, top: 25, right: 5, bottom: 25}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
  ]
}

    //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
JsBoard.Chips.jpcbbp.jpcb_footer = {
  items: [
    // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
    //   fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
    //   margin: {left: 1, right: 1, top: 0, bottom: 1}
    // },

    {name: 'jobstoppage', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, 
        xheight: function(base, name, data, me){
            return data['chip']['height'] - 30 -5;
        },
        height: 45,
        xheight: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'jobstoppagetitle', type: 'text', x: 0, y:5, 
        width: {data: 'chip', field: 'width'}, 
        height: 30, fontSize: 20,
        text: 'Job Stoppage', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 8, right: 8, top: 10, bottom: 0}
    },
    {name: 'stoppagearea', type: 'rect', x: 0, y:44, width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - 30 -15;
        },
        xheight: {data: 'chip', field: 'height'},
        fill: '#f5f5f5', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
  ]
}

JsBoard.Chips.jpcbbp.jpcb_fixed_header = {
  items: [
    {name: 'selectarea', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 2, 
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}


JsBoard.Chips.jpcbbp.jpcb_blocked = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f00', stroke: '#777', cornerRadius: 2, strokeWidth: 1, opacity: 0.5,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}





JsBoard.Chips.jpcbbp.blank = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f0f0f0', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}

JsBoard.Chips.jpcbbp.red = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: 'red', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}

JsBoard.Chips.jpcbbp.break = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#555', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'breakText', type: 'text', 
        x: 20, y:{data: 'chip', field: 'height'}, 
        height: {data: 'chip', field: 'width'}, 
        width: {data: 'chip', field: 'height'},
      text: 'BREAK',
      fontSize: 30,
      rotation: 270,
      align: 'center',
      fill: 'white', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}


JsBoard.Chips.jpcbbp.jpcb_first_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ddd',
        xfill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'base2', type: 'rect', x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#bbb',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'base3', type: 'rect', x: 0, 
        y:{data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#eee',
        stroke: '#888',
        margin: {left: 0, right: 0, top: 0, bottom: 1}
    },
    {name: 'maintitle', type: 'text', text: {data: 'chip', field: 'teamName'}, x: 0, 
        y:{data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 24,
        margin: {left: 15, top: 10, right: 5, bottom: 5}, 
        align: 'center',
        fill: '#338',
    },
    {name: 'subtitle1', type: 'text', text: {data: 'data', field: 'subtitle1'}, x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    {name: 'subtitle1', type: 'text', text: {data: 'data', field: 'subtitle2'}, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['chip']['stallTechWidth'];
        },
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'stallTechWidth'},
        fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.jpcbbp.jpcb_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#bb8',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    {name: 'parentBase', type: 'rect', x: 0, 
        visible: function (base, name, data, me){
            if (data['data']['newDay']){
                return true;
            }
            return false;
        },
        y:{data: 'chip', field: 'titleTop'}, 
        width: function(base, name, data, me){
            return data['chip']['width'] * data['data']['subCount'];
        },
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#eee',
        stroke: '#888',
        margin: {left: 0, right: 0, top: 0, bottom: 1}
    },
    {name: 'parentText', type: 'text', text: {data: 'data', field: 'parentText'}, x: 0, 
        y:{data: 'chip', field: 'titleTop'}, 
        width: function(base, name, data, me){
            return data['chip']['width'] * data['data']['subCount'];
        },
        align: 'center',
        fontSize: 18,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
  ]
}


JsBoard.Chips.jpcbbp.jpcb2 = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    
    {name: 'tglcetakwo', type: 'text', text: {data: 'data', field: 'tglcetakwo'}, x: 4, y: 2, align: 'right', width: {data: 'chip', field: 'width'}, 
        margin: {right: 10},
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'type'}, x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    {name: 'jamcetakwo', type: 'text', text: {data: 'data', field: 'jamcetakwo'}, x: 4, y: 16, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 10},
    },
    {name: 'wotype', type: 'text', text: {data: 'data', field: 'workname'}, x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 10},
    },
    {name: 'normal', type: 'rect', x: 0, y:36, 
      width: function(base, name, data, me){
        console.log('chip-rect-2', data['chip']['width'])
        var vext = 0;
        var vwidth = 1;
        var vcolwidth = 50;
        var vnormal = 1;
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        return vcolwidth*vnormal;
      }, 
      height: 21,
      xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      fill: function(base, name, data, me){
          return JsBoard.Common.RandomColorList[data['data']['color_idx']]
      },
      margin: {left: 8, right: 6, top: 0, bottom: 0}
    },
  ]
}