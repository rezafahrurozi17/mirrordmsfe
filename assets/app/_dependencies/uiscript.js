
function navigateDropDown(containerId) //dropdown content class="caret" | collapse/expand
{
	if($('#'+containerId).css('display') == 'none')
	{
		$('#'+containerId).show();
	}
	else
	{
		$('#'+containerId).hide();
	}
}

function navigateDropDownGroupPanel(containerId) //dropdown content +/- | collapse/expand
{
	//console.log("test "+containerId);
	if($("#"+containerId+" #groupPanelContent").css('display') == 'none')
	{
		$("#"+containerId+" #groupPanelContent").show();
		$("#"+containerId+" #groupPanelIcon").text("-");
	}
	else
	{
		$("#"+containerId+" #groupPanelContent").hide();
		$("#"+containerId+" #groupPanelIcon").text("+");
	}
}

//To Implement Navigation Tab
function navigateTab(containerId,divId)
{
	$("#"+containerId).children().each(function(n, i) {
		var id = this.id;
		console.log(id+' - '+divId);
		if(id == divId)
		{
			$('#'+divId).attr('class', 'tab-pane fade in active');
		}
		else
		{
			$('#'+id).attr('class', 'tab-pane fade');
		}
	});
}
//---------------EDIT FINISH UNIT-------------//
function navigateTab_FinishUnit(containerId,divId)
{
	$("#"+containerId).children().each(function(n, i) {
		var id = this.id;
		console.log(id+' - '+divId);
		if(id == divId)
		{
			$('#'+divId).attr('class', 'tab-pane fade in active');
		}
		else
		{
			$('#'+id).attr('class', 'tab-pane fade');
		}
	});
}

//Number Spinner
(function ($) {
  $('.spinner .btn:first-of-type').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
  });
  $('.spinner .btn:last-of-type').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
  });
})(jQuery);