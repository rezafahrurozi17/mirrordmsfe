JsBoard.Chips.status = {}

JsBoard.Chips.status.blank = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f0f0f0', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}

var lebar_waktu = 150;
JsBoard.Chips.status.header = { //<--- status adalah nama group chip. header adalah nama chip
    items: [
        {name: 'base', type: 'rect', x: 0, y:0,
            width: {data: 'chip', field: 'width'},
            height: {data: 'chip', field: 'height'},
            fill: '#f0f0f0', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
            margin: {left: 2, right: 2, top: 2, bottom: 2}
        },
        {name: 'title', type: 'text', x: 0, y: 0,
            text: {data: 'data', field: 'title'},
            width: {data: 'chip', field: 'width'},
            height: {data: 'chip', field: 'height'},
            fill: '#000',
            fontStyle: 'bold',
            fontSize: 44,
            margin: {left: 120, right: 100, top: 12, bottom: 0},
            align: 'center',
        },
        // background dari logo
        {name: 'logo-base', type: 'rect', x: 0, y:0,
            width: 170,
            height: 70,
            fill: '#fff',
            margin: {left: 3, right: 3, top: 3, bottom: 3}
        },
        // image logo (file png)
        {name: 'logo', type: 'icons',
            x: 0,
            y: 0,
            width: 160,
            height: 52,
            iconWidth: 160, iconHeight: 52,
            margin: {left: 2, right: 0, top: 5, bottom: 0},
            icons: ['logo-tam']
        },
        // background time
        {name: 'time-base', type: 'rect',
            y:0,
            width: lebar_waktu,
            x: function(base, name, data, me){
                return data['chip']['width'] - data['config']['width']
            },
            height: 70,
            fill: '#fff',
            stroke: '#777', cornerRadius: 4, strokeWidth: 1,
            margin: {left: 2, right: 5, top: 2, bottom: 2}
        },
        // jam
        {name: 'jam', type: 'text',
            width: lebar_waktu,
            x: function(base, name, data, me){
                return data['chip']['width'] - data['config']['width']
            },
            y:0,
            text: {data: 'data', field: 'jam'},
            height: {data: 'chip', field: 'height'},
            fill: '#000',
            fontSize: 38,
            fontStyle: 'bold',
            align: 'center',
        }, 
        // tanggal
        {name: 'tanggal', type: 'text',
            width: lebar_waktu,
            height: 28,
            x: function(base, name, data, me){
                return data['chip']['width'] - data['config']['width']
            },
            y: function(base, name, data, me){
                return data['chip']['height'] - data['config']['height']
            },
            text: {data: 'data', field: 'tanggal'},
            fill: '#000',
            fontSize: 16,
            align: 'center',
        },
    ]
}

var status_chip_lebar_caption = 160;
JsBoard.Chips.status.subheader = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#f0f0f0', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
            margin: {left: 2, right: 2, top: 2, bottom: 2}
        },
        {name: 'title', type: 'text', x: 0, y:0, 
            text: {data: 'data', field: 'title'}, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#000',
            fontSize: 28,
            margin: {left: 0, right: 0, top: 8, bottom: 0},
            align: 'center',
        },
        {name: 'caption', type: 'rect', 
            x: 0, y:0, 
            width: status_chip_lebar_caption,
            height: {data: 'chip', field: 'height'},
            fill: '#d00',
            stroke: '#333', strokeWidth: 1,
            margin: {left: 1, right: 0, top: 0, bottom: 0}
        },
        {name: 'captiontext', type: 'text', 
            x: 0, y:0, 
            width: status_chip_lebar_caption,
            height: {data: 'chip', field: 'height'},
            text: {data: 'data', field: 'caption'}, 
            fill: '#fff',
            fontSize: 24,
            margin: {left: 0, right: 0, top: 8, bottom: 0},
            align: 'center',
        },
        {name: 'content', type: 'rect', 
            x: status_chip_lebar_caption, 
            y: 0, 
            width: function(base, name, data, me){
                return data['chip']['width'] - status_chip_lebar_caption
            },
            height: {data: 'chip', field: 'height'},
            fill: '#ddd',
            stroke: '#333', strokeWidth: 1,
            margin: {left: 0, right: 1, top: 0, bottom: 0}
        },
        {name: 'contenttext', type: 'text', 
            x: status_chip_lebar_caption, 
            y: 0, 
            width: function(base, name, data, me){
                return data['chip']['width'] - status_chip_lebar_caption
            },
            height: {data: 'chip', field: 'height'},
            text: {data: 'data', field: 'text'}, 
            fill: '#c00',
            fontSize: 18,
            margin: {left: 12, right: 0, top: 12, bottom: 0},
            align: 'left',
        },
    ]
}

JsBoard.Chips.status.footer = {
    items: [
        // {name: 'base', type: 'rect', x: 0, y:0, 
        //     width: {data: 'chip', field: 'width'}, 
        //     height: {data: 'chip', field: 'height'},
        //     fill: '#333', 
        //     margin: {left: 0, right: 0, top: 0, bottom: 0}
        // },
        {name: 'base', type: 'icons', 
            x: -3, 
            y:-3, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            iconWidth: {data: 'chip', field: 'width'}, 
            iconHeight: {data: 'chip', field: 'height'},
            margin: {left: 0, right: 0, top: 0, bottom: 0},
            icons: ['footerbkg']
       },
        {name: 'text1', type: 'text', 
            x: function(base, name, data, me){
                return (data['chip']['width']/5)*0
            },
            y: 8, 
            width: function(base, name, data, me){
                return (data['chip']['width']/5)*1.3;
            }, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff',
            // fill: '#000',
            margin: {left: 0, right: 0, top: 0, bottom: 0},
            text: function(base, name, data, me){
                if(typeof data['data']['items'][0] !== 'undefined'){
                    return data['data']['items'][0];
                }
                return '';
            },
            fontStyle: 'bold',
            fontSize: 14,
            align: 'center',
       },
        {name: 'text2', type: 'text', 
            x: function(base, name, data, me){
                return (data['chip']['width']/5)*1.3
            },
            y: 8, 
            width: function(base, name, data, me){
                return (data['chip']['width']/5)*0.7;
            }, 
            height: {data: 'chip', field: 'height'},
            fill: '#000',
            margin: {left: 0, right: 0, top: 0, bottom: 0},
            text: function(base, name, data, me){
                if(typeof data['data']['items'][1] !== 'undefined'){
                    return data['data']['items'][1];
                }
                return '';
            },
            fontSize: 14,
            fontStyle: 'bold',
            align: 'center',
       },
        {name: 'text3', type: 'text', 
            x: function(base, name, data, me){
                return (data['chip']['width']/5)*2
            },
            y: 8, 
            width: function(base, name, data, me){
                return data['chip']['width']/5;
            }, 
            height: {data: 'chip', field: 'height'},
            fill: '#000',
            margin: {left: 10, right: 0, top: 0, bottom: 0},
            text: function(base, name, data, me){
                if(typeof data['data']['items'][2] !== 'undefined'){
                    return data['data']['items'][2];
                }
                return '';
            },
            fontSize: 14,
            fontStyle: 'bold',
            align: 'center',
       },
        {name: 'text4', type: 'text', 
            x: function(base, name, data, me){
                return (data['chip']['width']/5)*3
            },
            y: 8, 
            width: function(base, name, data, me){
                return (data['chip']['width']/5)*0.8;
            }, 
            height: {data: 'chip', field: 'height'},
            // fill: '#fff',
            fill: '#000',
            margin: {left: 0, right: 0, top: 0, bottom: 0},
            text: function(base, name, data, me){
                if(typeof data['data']['items'][3] !== 'undefined'){
                    return data['data']['items'][3];
                }
                return '';
            },
            fontSize: 14,
            fontStyle: 'bold',
            align: 'center',
       },
        {name: 'text5', type: 'text', 
            x: function(base, name, data, me){
                return (data['chip']['width']/5)*3.8
            },
            y: 8, 
            width: function(base, name, data, me){
                return (data['chip']['width']/5)*1.2;
            }, 
            height: {data: 'chip', field: 'height'},
            // fill: '#fff',
            fill: '#000',
            margin: {left: 0, right: 0, top: 0, bottom: 0},
            text: function(base, name, data, me){
                if(typeof data['data']['items'][4] !== 'undefined'){
                    return data['data']['items'][4];
                }
                return '';
            },
            fontSize: 14,
            fontStyle: 'bold',
            align: 'center',
       },
        {name: 'base2', type: 'rect', x: 0, y:0, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            stroke: '#000', strokeWidth: 2, fill: null,
            margin: {left: 1, right: 1, top: 1, bottom: 0}
        },
       
    ]
}

JsBoard.Chips.status.runningtext = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#555', stroke: null,
            // stroke: '#777', strokeWidth: 1,
            margin: {left: 0, right: 0, top: 0, bottom: 0}
        },
        {name: 'text', type: 'text', 
            y: 0, 
            text: {data: 'data', field: 'text'}, 
            fontStyle: 'bold',
            width: function(base, name, data, me){
                var vtext = data['config']['text'];
                var xtest = new Konva.Text({
                    left: 0, top: 0, height: 10, text: data['data']['text'],
                    fontStyle: 'bold',
                    fontSize: 14,
                });
                var vwidth = Math.round((xtest.width()+20)/10)*10;
                console.log('text-width', vwidth);
                return vwidth;
            }, 
            x: function(base, name, data, me){
                if (typeof data['data']['left'] == 'undefined' || data['data']['left']===false){
                    if (typeof this.width=='function'){
                        var vwidth = this.width(base, name, data, me);
                    } else {
                        var vwidth = this.width;
                    }
                    return vwidth;
                } else {
                    return data['data']['left'] - data['data']['deltaJarak'];
                }
                // var vcounter = 0;
                // if (typeof data['data']['counter'] !== 'undefined'){
                //     vcounter = data['data']['counter'];
                // }
                // var vx = (vcounter * data['data']['deltaJarak']) % vwidth;


                // if (vx>vwidth){
                //     vx = 0;
                // } else {
                //     vx = -vx;
                // }
                // vx = data['chip']['width']-vx
                return vx;
            }, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff',
            fontSize: 14,
            margin: {left: 10, right: 0, top: 2, bottom: 0},
            align: 'left',
        },
        // {name: 'base2', type: 'rect', x: 0, y:0, 
        //     width: {data: 'chip', field: 'width'}, 
        //     height: {data: 'chip', field: 'height'},
        //     stroke: '#000', strokeWidth: 2, fill: null,
        //     margin: {left: 1, right: 1, top: 1, bottom: 0}
        // },
    ]
}

JsBoard.Chips.status.listbg = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff', stroke: '#777', strokeWidth: 1,
            margin: {left: 0, right: 1, top: 1, bottom: 0}
        },
    ]
}

var statusCustomerColorMap = {default: '#f0f0f0', wrecept: '#db0000', production: '#ff0', finspection: '#feb856', dokumen: '#0082e1', siapserah: '#00b925'}
var statusCustomerColorMapText = {default: '#fff', wrecept: '#fff', production: '#000', finspection: '#000', dokumen: '#fff', siapserah: '#fff'}
JsBoard.Chips.status.chip = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, 
            visible: function(base, name, data, me){
                console.log("nopol", data['data']['nopol']);
                console.log("showPageIndex", data['chip']['showPageIndex']);
                console.log("data.pageIndex", data['data']['pageIndex']);
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#f0f0f0', stroke: '#777', cornerRadius: 4, strokeWidth: 2,
            margin: {left: 5, right: 5, top: 4, bottom: 2}
        },
        {name: 'processtype', type: 'rect', x: 0, y:0, 
            visible: function(base, name, data, me){
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            width: {data: 'chip', field: 'width'}, 
            height: 40,
            fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
            colorMap: statusCustomerColorMap,
            stroke: '#777', cornerRadius: 4, strokeWidth: 1,
            margin: {left: 5, right: 5, top: 4, bottom: 2}
        },
        {name: 'nopol', type: 'text', x: 0, y:0, 
            visible: function(base, name, data, me){
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
            colorMap: statusCustomerColorMapText,
            fontSize: 18,
            align: 'center',
            fontStyle: 'bold',
            text: {data: 'data', field: 'nopol'}, 
            margin: {left: 5, right: 5, top: 12, bottom: 2}
        },
        {name: 'subjudul', type: 'text', x: 0, 
            visible: function(base, name, data, me){
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            y:40, 
            width: {data: 'chip', field: 'width'}, 
            height: 20,
            fill: '#000',
            text: function(base, name, data, me){
                return 'Janji selesai: '+data['data']['janjiselesai'];
            },
            xtext: {data: 'data', field: 'janjiselesai'}, 
            fontSize: 14,
            align: 'left',
            fontStyle: 'bold',
            margin: {left: 8, right: 20, top: 2, bottom: 2}
        },
        {name: 'subjudul', type: 'text', x: 0, 
            visible: function(base, name, data, me){
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            y:40, 
            width: {data: 'chip', field: 'width'}, 
            height: 20,
            fill: '#000',
            text: {data: 'data', field: 'sa_name'}, 
            fontSize: 14,
            align: 'right',
            margin: {left: 5, right: 8, top: 2, bottom: 2}
        },
    ]    
}

JsBoard.Chips.status.chip_bp = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, 
            visible: function(base, name, data, me){
                console.log("nopol", data['data']['nopol']);
                console.log("showPageIndex", data['chip']['showPageIndex']);
                console.log("data.pageIndex", data['data']['pageIndex']);
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#f0f0f0', stroke: '#777', cornerRadius: 4, strokeWidth: 2,
            margin: {left: 5, right: 5, top: 4, bottom: 2}
        },
        {name: 'processtype', type: 'rect', x: 0, y:0, 
            visible: function(base, name, data, me){
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            width: {data: 'chip', field: 'width'}, 
            height: 40,
            fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
            colorMap: statusCustomerColorMap,
            stroke: '#777', cornerRadius: 4, strokeWidth: 1,
            margin: {left: 5, right: 5, top: 4, bottom: 2}
        },
        {name: 'nopol', type: 'text', x: 0, y:0, 
            visible: function(base, name, data, me){
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
            colorMap: statusCustomerColorMapText,
            fontSize: 18,
            align: 'center',
            fontStyle: 'bold',
            text: {data: 'data', field: 'nopol'}, 
            margin: {left: 5, right: 5, top: 12, bottom: 2}
        },
        {name: 'subjudul', type: 'text', x: 0, 
            visible: function(base, name, data, me){
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            y:40, 
            width: {data: 'chip', field: 'width'}, 
            height: 20,
            fill: '#000',
            text: function(base, name, data, me){
                if (data['data']['status']=='wrecept'){
                    return 'Jam datang: '+data['data']['datang'];
                } else {
                    return 'Janji Selesai: '+data['data']['janjiselesai'];
                }
            },
            xtext: {data: 'data', field: 'janjiselesai'}, 
            fontSize: 14,
            align: 'left',
            fontStyle: 'bold',
            margin: {left: 8, right: 40, top: 2, bottom: 2}
        },
        {name: 'subjudul', type: 'text', x: 0, 
            visible: function(base, name, data, me){
                if (data['chip']['showPageIndex']==data['data']['pageIndex']){
                    return true;
                }
                return false;
            },
            y:40, 
            width: {data: 'chip', field: 'width'}, 
            height: 20,
            fill: '#000',
            text: {data: 'data', field: 'sa_name'}, 
            fontSize: 14,
            align: 'right',
            margin: {left: 5, right: 8, top: 2, bottom: 2}
        },
    ]    
}

