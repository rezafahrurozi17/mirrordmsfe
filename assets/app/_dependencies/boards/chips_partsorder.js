// chip for iregularity BP

JsBoard.Chips.parts_order = {}

JsBoard.Common.partColorRange = function(day, hour, dayrange, hourrange){
    for(var i=0; i<dayrange.length; i++){
        drange = dayrange[i];
        if (day<drange.day){
            var vval = drange.color;
            if (vval=='hour_color'){
                for(var j=0; j<hourrange.length; j++){
                    var hrange = hourrange[j];
                    if (hour<hrange.hour){
                        return hrange.color;
                    }
                }
            } else {
                return drange.color;
            }
        }
    }
}

JsBoard.Chips.parts_order.waiting_part_no_info = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 4, width: {data: 'chip', field: 'width'}, fontSize: 22,
        margin: {left: 8}},
    {name: 'norangka', type: 'text', text: {data: 'data', field: 'no_rangka'}, x: 0, y: 28, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        fill: '#fff',
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#e00';
            } else {
                return '#fff';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 4, top: 4, bottom: 0}
    },
    {name: 'timestatustext', type: 'text', 
        text: function(base, name, data, me){
            return data['data']['timewait'];
            // if (data['data']['daywait']!='TODAY'){
            //     return data['data']['daywait'];
            // } else {
            //     var vval = data['data']['timewait'];
            //     var vmin = JsBoard.Common.getMinute(vval);
            //     var vhours = Math.round(vmin/60);
            //     if (vhours>0){
            //         return 'J+'+vhours;
            //     } else if (vhours<0){
            //         return 'J'+vhours;
            //     } else {
            //         return 'J';
            //     }
            //     return data['data']['timewait'];
            // }
        }, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        align: 'center',
        fill: '#000',
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#fff';
            } else {
                return '#000';
            }
        },
        fontSize: 20, 
        margin: {top: 10}
    },
    {name: 'liner1', type: 'rect', x: 0, y:50, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'tgl_kirim', type: 'text', text: {data: 'data', field: 'docno'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'tgl_diperlukan'}, x: 0, y: 70, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    // {name: 'icons', type: 'icons', 
    //     width: 90, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-16
    //     }, 
    //     y:39, 
    //     height: 40,
    //     iconWidth: 32, iconHeight: 32,
    //     margin: {left: 5, right: 0, top: 0, bottom: 0},
    //     icons: {data: 'data', field: 'statusicon', default: []}
    // },

    // {name: 'timestatus', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 40,
    //     fill: '#fff', 
    //     colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'timestatus', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 40,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'category'},
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },
    {name: 'custtype', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 40, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-60-8
        }, 
        y:56, 
        height: 20,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'custtypetext', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['cust_type'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 40, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-60-8
        }, 
        y:56, 
        height: 20,
        fill: 'black', 
        align: 'center', fontSize: 14, fontStyle: 'bold',
        text: {data: 'data', field: 'cust_type'},
        margin: {left: 10, right: 10, top: 3, bottom: 0}
    },
    {name: 'sa_name', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 135,
        xxx: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-130-8
        }, 
        y:70, 
        height: 24,
        fill: 'black', 
        align: 'left', fontSize: 12, 
        text: {data: 'data', field: 'sa_name'},
        wrap: 'none',
        margin: {left: 0, right: 0}
    },

  ]
}

JsBoard.Chips.parts_order.waiting_order = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 4, width: {data: 'chip', field: 'width'}, fontSize: 22,
        margin: {left: 8}},
    {name: 'norangka', type: 'text', text: {data: 'data', field: 'no_rangka'}, x: 0, y: 28, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        fill: function(base, name, data, me){
            var vdays = data['data']['dday'];
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            var vhours = Math.round(vmin/60);
            return JsBoard.Common.partColorRange(vdays, vhours, data['data']['dayRange'], data['data']['hourRange']);
        },
        xxfill: '#fff',
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#e00';
            } else {
                return '#fff';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#fff';
            } else {
                return '#000';
            }
        },
        xstroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 4, top: 4, bottom: 0}
    },
    {name: 'timestatustext', type: 'text', 
        text: function(base, name, data, me){
            if (data['data']['daywait']!='TODAY'){
                return data['data']['daywait'];
            } else {
                var vval = data['data']['timewait'];
                var vmin = JsBoard.Common.getMinute(vval);
                var vhours = Math.round(vmin/60);
                if (vhours>0){
                    return 'J+'+vhours;
                } else if (vhours<0){
                    return 'J'+vhours;
                } else {
                    return 'J';
                }
                return data['data']['timewait'];
            }
        }, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        align: 'center',
        fill: function(base, name, data, me){
            var vdays = data['data']['dday'];
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            var vhours = Math.round(vmin/60);
            return JsBoard.Common.partColorRange(vdays, vhours, data['data']['textDayRange'], data['data']['textHourRange']);
        },
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#fff';
            } else {
                return '#000';
            }
        },
        fontSize: 20, 
        margin: {top: 12}
    },
    {name: 'liner1', type: 'rect', x: 0, y:50, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'tgl_kirim', type: 'text', text: {data: 'data', field: 'docno'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'tgl_diperlukan'}, x: 0, y: 70, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        visible: true,
        // visible: function(base, name, data, me){
        //     if(typeof data['data']['need_dp'] !== 'undefined'){
        //         if(data['data']['need_dp'] == 1){
        //             return 1;
        //         }
        //     }
        //     return 0;
        // },
        width: 67, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-16
        }, 
        y:52, 
        height: 40,
        iconWidth: 22, iconHeight: 22,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        // icons: ['dp'],
        icons: {data: 'data', field: 'statusicon', default: []}
    },

    // {name: 'timestatus', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: '#fff', 
    //     colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'timestatus', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'category'},
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },

    {name: 'custtype', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 40, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-60-8
        }, 
        y:56, 
        height: 20,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'custtypetext', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['cust_type'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 40, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-60-8
        }, 
        y:56, 
        height: 20,
        fill: 'black', 
        align: 'center', fontSize: 14, fontStyle: 'bold',
        text: {data: 'data', field: 'cust_type'},
        margin: {left: 10, right: 10, top: 3, bottom: 0}
    },
    {name: 'sa_name', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 130,
        xxx: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-130-8
        }, 
        y:70, 
        height: 24,
        fill: 'black', 
        align: 'left', fontSize: 12, 
        text: {data: 'data', field: 'sa_name'},
        wrap: 'none',
        margin: {left: 0, right: 0}
    },
    // {name: 'dpoptions', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['need_dp'] !== 'undefined'){
    //             if(data['data']['need_dp'] == 1){
    //                 return 1;
    //             }
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-36-8
    //     }, 
    //     y:74, 
    //     height: 20,
    //     fill: '#e00', 
    //     stroke: '#e00', cornerRadius: 0, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'dpoptionstext', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['need_dp'] !== 'undefined'){
    //             if(data['data']['need_dp'] == 1){
    //                 return 1;
    //             }
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-36-8
    //     }, 
    //     y:74, 
    //     height: 20,
    //     fill: '#fff', 
    //     align: 'center', fontSize: 12, //fontStyle: 'bold',
    //     text: 'DP',
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },

  ]
}

JsBoard.Chips.parts_order.status_order_partsx = {
      items: [
        {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
          fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
          margin: {left: 2, right: 2, top: 2, bottom: 2}
        },
        {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 4, width: {data: 'chip', field: 'width'}, fontSize: 22,
            margin: {left: 8}},    
      ]
    }


JsBoard.Chips.parts_order.status_order_parts = {
//   listeners: {
//     eta_link: {
//         mouseup: function(evt){
//             evt.evt.preventDefault();
//             alert('click eta');
//         }
//     }
//   },
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 4, width: {data: 'chip', field: 'width'}, fontSize: 22,
        margin: {left: 8}},
    {name: 'norangka', type: 'text', text: {data: 'data', field: 'no_rangka'}, x: 0, y: 28, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 105, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        fill: function(base, name, data, me){
            var vdays = data['data']['dday'];
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            var vhours = Math.round(vmin/60);
            return JsBoard.Common.partColorRange(vdays, vhours, data['data']['dayRange'], data['data']['hourRange']);
        },
        xxfill: '#fff',
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#e00';
            } else {
                return '#fff';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#fff', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 4, top: 4, bottom: 0}
    },
    {name: 'timestatustext', type: 'text', 
        text: function(base, name, data, me){
            if (data['data']['daywait']!='TODAY'){
                return data['data']['daywait'];
            } else {
                var vval = data['data']['timewait'];
                var vmin = JsBoard.Common.getMinute(vval);
                var vhours = Math.round(vmin/60);
                if (vhours>0){
                    return 'J+'+vhours;
                } else if (vhours<0){
                    return 'J'+vhours;
                } else {
                    return 'J';
                }
                return data['data']['timewait'];
            }
        }, 
        width: 105, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        align: 'center',
        fill: function(base, name, data, me){
            var vdays = data['data']['dday'];
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            var vhours = Math.round(vmin/60);
            return JsBoard.Common.partColorRange(vdays, vhours, data['data']['textDayRange'], data['data']['textHourRange']);
        },
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#fff';
            } else {
                return '#000';
            }
        },
        fontSize: 20, 
        margin: {top: 12}
    },
    {name: 'liner1', type: 'rect', x: 0, y:50, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 105-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'tgl_kirim', type: 'text', text: {data: 'data', field: 'docno'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'tgl_diperlukan'}, x: 0, y: 70, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'tgl_eta'}, x: 0, y: 88, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        width: 82, 
        x: function(base, name, data, me){
            // return data['chip']['width'] - data['config']['width']-16
            return data['chip']['width'] - data['config']['width']-30-5
        }, 
        y:52, 
        height: 40,
        iconWidth: 22, iconHeight: 22,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: {data: 'data', field: 'statusicon', default: []}
    },

    // {name: 'timestatus', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: '#fff', 
    //     colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'timestatus', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'category'},
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },

    // {name: 'custtype', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['sa_name'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-60-8
    //     }, 
    //     y:52, 
    //     height: 20,
    //     fill: '#fff', 
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'custtypetext', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['cust_type'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-60-8
    //     }, 
    //     y:52, 
    //     height: 20,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'cust_type'},
    //     margin: {left: 10, right: 10, top: 3, bottom: 0}
    // },
    {name: 'sa_name', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 130,
        xxx: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-130-8
        }, 
        y:70, 
        height: 24,
        fill: 'black', 
        align: 'left', fontSize: 12, 
        text: {data: 'data', field: 'sa_name'},
        wrap: 'none',
        margin: {left: 0, right: 0}
    },
    {name: 'eta_link', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 130,
        xxx: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-130-8
        }, 
        y:88, 
        height: 24,
        fill: '#04a', 
        align: 'left', fontSize: 12, 
        text: 'ALL ETA',
        wrap: 'none',
        //draggable: false,
        margin: {left: 0, right: 0}
    },
    {name: 'vendor', type: 'text', 
        width: 60, 
        x: function(base, name, data, me){
            // return data['chip']['width'] - data['config']['width']-16
            return data['chip']['width'] - data['config']['width']-30-8
        }, 
        y:88, 
        height: 24,
        fill: '#000', 
        align: 'left', fontSize: 12, 
        text: {data: 'data', field: 'vendor'},
        wrap: 'none',
        margin: {left: 0, right: 0}
    },
    // {name: 'dpoptions', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['need_dp'] !== 'undefined'){
    //             if(data['data']['need_dp'] == 1){
    //                 return 1;
    //             }
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-36-8
    //     }, 
    //     y:74, 
    //     height: 20,
    //     fill: '#e00', 
    //     stroke: '#e00', cornerRadius: 0, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'dpoptionstext', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['need_dp'] !== 'undefined'){
    //             if(data['data']['need_dp'] == 1){
    //                 return 1;
    //             }
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-36-8
    //     }, 
    //     y:74, 
    //     height: 20,
    //     fill: '#fff', 
    //     align: 'center', fontSize: 12, //fontStyle: 'bold',
    //     text: 'DP',
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },

  ]
}


JsBoard.Chips.parts_order.status_rpp_claim = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 4, width: {data: 'chip', field: 'width'}, fontSize: 22,
        margin: {left: 8}},
    {name: 'norangka', type: 'text', text: {data: 'data', field: 'no_rangka'}, x: 0, y: 28, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        fill: function(base, name, data, me){
            var vdays = data['data']['dday'];
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            var vhours = Math.round(vmin/60);
            return JsBoard.Common.partColorRange(vdays, vhours, data['data']['dayRange'], data['data']['hourRange']);
        },
        xxfill: '#fff',
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#e00';
            } else {
                return '#fff';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#fff', 
        cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 4, top: 4, bottom: 0}
    },
    {name: 'timestatustext', type: 'text', 
        text: function(base, name, data, me){
            if (data['data']['daywait']!='TODAY'){
                return data['data']['daywait'];
            } else {
                var vval = data['data']['timewait'];
                var vmin = JsBoard.Common.getMinute(vval);
                var vhours = Math.round(vmin/60);
                if (vhours>0){
                    return 'J+'+vhours;
                } else if (vhours<0){
                    return 'J'+vhours;
                } else {
                    return 'J';
                }
                return data['data']['timewait'];
            }
        }, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        align: 'center',
        fill: function(base, name, data, me){
            var vdays = data['data']['dday'];
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            var vhours = Math.round(vmin/60);
            return JsBoard.Common.partColorRange(vdays, vhours, data['data']['textDayRange'], data['data']['textHourRange']);
        },
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#fff';
            } else {
                return '#000';
            }
        },
        fontSize: 20, 
        margin: {top: 12}
    },
    {name: 'liner1', type: 'rect', x: 0, y:50, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'tgl_kirim', type: 'text', text: {data: 'data', field: 'docno'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'no_gr'}, x: 0, y: 68, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'date_gr'}, x: 0, y: 84, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        width: 67, 
        x: function(base, name, data, me){
            // return data['chip']['width'] - data['config']['width']-16
            return data['chip']['width'] - data['config']['width']-30-8
        }, 
        y:52, 
        height: 40,
        iconWidth: 22, iconHeight: 22,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: function(base, name, data, me){
            if (data['data']['statusname']=='rpp'){
                return ['rpp']
            } else {
                return ['claim']
            }
        }
    },

    // {name: 'timestatus', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: '#fff', 
    //     colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'timestatus', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'category'},
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },

    // {name: 'custtype', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['sa_name'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-60-8
    //     }, 
    //     y:52, 
    //     height: 20,
    //     fill: '#fff', 
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'custtypetext', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['cust_type'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-60-8
    //     }, 
    //     y:52, 
    //     height: 20,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'cust_type'},
    //     margin: {left: 10, right: 10, top: 3, bottom: 0}
    // },
    {name: 'sa_name', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 130,
        xxx: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-130-8
        }, 
        y:68, 
        height: 24,
        fill: 'black', 
        align: 'left', fontSize: 12, 
        text: {data: 'data', field: 'sa_name'},
        wrap: 'none',
        margin: {left: 0, right: 0}
    },
    // {name: 'eta_link', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['sa_name'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: 130,
    //     xxx: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-130-8
    //     }, 
    //     y:84, 
    //     height: 24,
    //     fill: '#04a', 
    //     align: 'left', fontSize: 12, 
    //     text: 'ALL ETA',
    //     wrap: 'none',
    //     margin: {left: 0, right: 0}
    // },
    {name: 'vendor', type: 'text', 
        width: 60, 
        x: 130,
        y:84, 
        height: 24,
        fill: '#000', 
        align: 'left', fontSize: 12, 
        text: {data: 'data', field: 'vendor'},
        wrap: 'none',
        margin: {left: 0, right: 0}
    },
    // {name: 'dpoptions', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['need_dp'] !== 'undefined'){
    //             if(data['data']['need_dp'] == 1){
    //                 return 1;
    //             }
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-36-8
    //     }, 
    //     y:74, 
    //     height: 20,
    //     fill: '#e00', 
    //     stroke: '#e00', cornerRadius: 0, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'dpoptionstext', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['need_dp'] !== 'undefined'){
    //             if(data['data']['need_dp'] == 1){
    //                 return 1;
    //             }
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-36-8
    //     }, 
    //     y:74, 
    //     height: 20,
    //     fill: '#fff', 
    //     align: 'center', fontSize: 12, //fontStyle: 'bold',
    //     text: 'DP',
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },

  ]
}


