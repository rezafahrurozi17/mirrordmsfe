// chip definition
var JsBoard = JsBoard || {}
var baseColorMap = {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#ffA520', done: '#787878'}

JsBoard.Chips = JsBoard.Chips || {}
JsBoard.Chips.jpcb = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 2, bottom: 2}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 1, right: 1, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    
    {name: 'jamcetakwo', type: 'text', text: {data: 'data', field: 'jamcetakwo'}, x: 0, y: 0, align: 'right', width: {data: 'chip', field: 'width'}, 
        visible: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return true
            } else {
                return false;
            }
        }
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'type'}, x: 0, y: 13, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    // {name: 'start2', type: 'text', text: {data: 'data', field: 'start2'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}},
    {name: 'jamjanjiserah', type: 'text', text: {data: 'data', field: 'jamjanjiserah'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}, 
        visible: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return true
            } else {
                return false;
            }
        }
    },
    {name: 'pekerjaan', type: 'text', text: {data: 'data', field: 'pekerjaan'}, x: 0, y: 51, width: {data: 'chip', field: 'width'}},
    {name: 'teknisi', type: 'text', text: {data: 'data', field: 'teknisi'}, x: 30, y: 51, align: 'right', 
        wrap: 'none',
        xwidth: {data: 'chip', field: 'width'},
        width: function(base, name, data, me){
            return data['chip']['width']-this.x;
        }
    },
    {name: 'normal', type: 'rect', x: 0, y:32, 
      width: function(base, name, data, me){
        console.log('chip-rect-2', data['chip']['width'])
        var vext = 0;
        var vwidth = 1;
        var vcolwidth = 50;
        var vnormal = 1;
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        return vcolwidth*vnormal;
      }, 
      height: 21,
      fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      colorMap: baseColorMap,
    //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
      xxmargin: {left: 0, right: 0, top: 0, bottom: 0},
      margin: function(base, name, data, me){
        var vext = 0;
        if (typeof data['data']['ext']!=='undefined'){
          vext = data['data']['ext'];
        }
        if (vext>0){
          return {right: 0, top: 0, bottom: 0}
        } else {
          return {top: 0, bottom: 0}
        }
      }
    },
    {name: 'ext', type: 'rect', x: 0, y:32, 
      x: function(base, name, data, me){
        var vnormal = 0;
        var vcolwidth = 50;
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        return vcolwidth*vnormal;
      }, 
      width: function(base, name, data, me){
        var vext = 0;
        var vcolwidth = 50;
        if (typeof data['data']['ext']!=='undefined'){
          vnormal = data['data']['ext'];
        }
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        return vcolwidth*vnormal;
      }, 
      visible: function(base, name, data, me){
        var vext = 0;
        if (typeof data['data']['ext']!=='undefined'){
          vext = data['data']['ext'];
        }
        if (vext>0){
          return 1;
        } else {
          return 0
        }
      },
      height: 21,
      fill: '#ffA520', 
      margin: function(base, name, data, me){
        var vext = 0;
        if (typeof data['data']['ext']!=='undefined'){
          vext = data['data']['ext'];
        }
        if (vext>0){
          return {left: 0, top: 0, bottom: 0}
        } else {
          return {top: 0, bottom: 0}
        }
      }
    },
    {name: 'type', type: 'icons', 
        x: 0, y: 26, 
        icons: {data: 'data', field: 'statusicon', default: []},
        width: function(base, name, data, me){
            console.log('chip-rect-2', data['chip']['width'])
            var vext = 0;
            var vwidth = 1;
            var vcolwidth = 50;
            var vnormal = 1;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            if (typeof data['data']['normal']!=='undefined'){
            vnormal = data['data']['normal'];
            }
            return vcolwidth*vnormal;
        }, 
    },
    {name: 'type', type: 'icons', 
        xxx: 25,
        x: function(base, name, data, me){
            var vext = 0;
            var vwidth = 1;
            var vcolwidth = 50;
            var vnormal = 1;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            if (typeof data['data']['normal']!=='undefined'){
                vnormal = data['data']['normal'];
            }
            if (typeof data['data']['ext']!=='undefined'){
                vext = data['data']['ext'];
            }
            vwidth = vnormal+vext;
            return Math.round(vwidth*vcolwidth/2)-14;
        }, 
        y: 46, 
        icons: ['play2'],
        width: 20,
        height: 16,
        iconHeight: 14,
        iconWidth: 14,
        visible: function(base, name, data, me){
            vallocated = 1;
            if (typeof data['data']['allocated']!=='undefined'){
                vallocated = data['data']['allocated'];
            }
            if (vallocated){
                return false;
            } else {
                return true;
            }
        }
    },
  ]
}

JsBoard.Chips.jpcb_stall_base = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fefefe'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    // {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
    //     margin: {left: 5, top: 5, right: 5, bottom: 5}, 
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    // },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.jpcb_stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fefefe'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 5, right: 5, bottom: 5}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
    {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
        width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
        bullet: '-',
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
        margin: {left: 25}
    },
  ]
}

    //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
JsBoard.Chips.jpcb_footer = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'rect1', type: 'rect', x: 0, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['belumdatang'], stroke: '#000',
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 30, y:0, 
        width: 90, height: 30,
        text: 'Belum Datang', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 120, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['sudahdatang'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 150, y:0, 
        width: 90, height: 30,
        text: 'Sudah Datang', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 240, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['booking'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 270, y:0, 
        width: 60, height: 30,
        text: 'Booking', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 330, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['walkin'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 360, y:0, 
        width: 60, height: 30,
        text: 'Walk In', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 420, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['inprogress'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 450, y:0, 
        width: 60, height: 30,
        text: 'In Progress', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 510, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['irregular'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 540, y:0, 
        width: 60, height: 30,
        text: 'Irregular', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 600, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['extension'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 630, y:0, 
        width: 60, height: 30,
        text: 'Extension', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 690, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['done'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 720, y:0, 
        width: 60, height: 30,
        text: 'Done', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'separator', type: 'rect', x: 0, y:30, width: {data: 'chip', field: 'width'}, height: 5, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', 
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'jobstoppage', type: 'rect', x: 0, y:35, width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - 30 -5;
        },
        xheight: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'jobstoppagetitle', type: 'text', x: 0, y:40, 
        width: {data: 'chip', field: 'width'}, 
        height: 30, fontSize: 20,
        text: 'Job Stoppage', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 8, right: 8, top: 10, bottom: 0}
    },
    {name: 'stoppagearea', type: 'rect', x: 0, y:80, width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - 30 -50;
        },
        xheight: {data: 'chip', field: 'height'},
        fill: '#f5f5f5', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
  ]
}

JsBoard.Chips.jpcb_blocked = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f00', stroke: '#777', cornerRadius: 2, strokeWidth: 1, opacity: 0.5,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}



JsBoard.Chips.blank = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f0f0f0', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}


JsBoard.Chips.jpcb_first_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ccc',
        xfill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fefefe'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 15, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.jpcb_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ccc',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.jpcb_footer_view = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'rect1', type: 'rect', x: 0, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['belumdatang'], stroke: '#000',
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 30, y:0, 
        width: 90, height: 30,
        text: 'Belum Datang', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 120, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['sudahdatang'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 150, y:0, 
        width: 90, height: 30,
        text: 'Sudah Datang', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 240, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['booking'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 270, y:0, 
        width: 60, height: 30,
        text: 'Booking', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 330, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['walkin'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 360, y:0, 
        width: 60, height: 30,
        text: 'Walk In', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 420, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['inprogress'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 450, y:0, 
        width: 60, height: 30,
        text: 'In Progress', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 510, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['irregular'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 540, y:0, 
        width: 60, height: 30,
        text: 'Irregular', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 600, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['extension'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 630, y:0, 
        width: 60, height: 30,
        text: 'Extension', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 690, y:0, 
        width: 30, height: 30,
        fill: baseColorMap['done'],
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 720, y:0, 
        width: 60, height: 30,
        text: 'Done', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
  ]
}


