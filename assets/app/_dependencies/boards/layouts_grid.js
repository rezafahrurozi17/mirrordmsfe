//// Layout for status Board
//// dependent on layouts.js
JsBoard.Layouts.grid = function(config){
    this.parent = JsBoard.Layouts.base;
    // call parent's constructor
    this.parent.call(this, config);
    //JsBoard.Layouts.base.call(this);

}

// inherit status board from base board
JsBoard.Layouts.grid.prototype = Object.create(JsBoard.Layouts.base.prototype);

// correct constructor pointer because it points to parent
JsBoard.Layouts.grid.prototype.constructor = JsBoard.Layouts.status;

JsBoard.Layouts.grid.prototype.createStage = function(){
    vconfig = this.stage; // <--- config.stage
    vcontainer = this.board.container;

    result = new Konva.Stage({
      container: vcontainer,
      width: vconfig.width,
      height: vconfig.height
    });
    return result;

}

JsBoard.Layouts.grid.prototype.initLayout = function(){
    this.parent.prototype.initLayout.call(this);
    var defconfig = {
        headerHeight: 100,
        subHeaderHeight: 40,
        rowHeight: 26,
        chipItemHeight: 64,
        listWidth: 240,
        footerHeight: 160,
        counterHeaderHeight: 30,
        lastCallHeight: 24,
        runningTextHeight: 24,
        maxCounterWidth: 120,
        counterMargin: 20,
    }
    this.board = JsBoard.Common.extend(defconfig, this.board);
}

/*
lokasi
- headers, array 
  * bisa lebih dari satu baris
  * header paling atas adalah 0
- column header 
  * bisa lebih dari 1
  * berada di header yang mana 
  * didefinisikan lebarnya yang akan dinormalisasi dengan lebar total kolom (bila columnAutoSize==1)

- column
  * minimal 1
  * masing-masing kolom didefinisikan lebarnya
  * lebar totalnya akan dinormalisasi ke lebar grid

- rows
  * minimal 1
  * masing-masing row didefinisikan tingginya
  * tinggi total tidak akan dinormalisasi

- cell
  * lokasi dari setiap row

- column footer
  * bisa lebih dari 1
  * berada di footer yang mana
  * didefinisikan lebarnya yang akan dinormalisasi dengan lebar total kolom.

- footer
  * bisa lebih dari 1 baris
  * footer ke 0 adalah footer paling bawah
  



*/

JsBoard.Layouts.grid.prototype.chipDimension = function(vitem){
    var result = {x:0, y:0, width: 100, height: 100};
    // return result;
    if (vitem.lokasi=='header'){
        result = {
            x: 0,
            y: vitem.calcTop,
            width: this.stage.width,
            height: vitem.calcHeight,
        }
    } else if (vitem.lokasi=='footer'){
        result = {
            x: 0,
            y: vitem.calcTop,
            width: this.stage.width,
            height: vitem.calcHeight,
        }
    } else if (vitem.lokasi=='columnHeader'){
        result = {
            x: vitem.calcLeft,
            y: vitem.calcTop,
            width: vitem.calcWidth,
            height: vitem.calcHeight,
        }
    } else if (vitem.lokasi=='column'){
        result = {
            x: vitem.calcLeft,
            y: this.totalHeaderHeight,
            width: vitem.calcWidth,
            height: this.stage.height - this.totalHeaderHeight-this.totalFooterHeight,
        }
    } else if (vitem.lokasi=='row'){
        // var vheight;
        // if (typeof vitem.height !== 'undefined'){
        //     vheight = vitem.height;
        //     this.sisaTinggiItem = this.sisaTinggiItem - vheight;
        //     this.sisaCalculatedRows = this.sisaCalculatedRows - 1;
        // } else {
        //     vheight = this.stage.height-this.totalHeaderHeight-this.totalHeaderHeight;
        //     if (this.sisaCalculatedRows>0){
        //         vheight = this.sisaTinggiItem/this.sisaCalculatedRows;
        //     } else {
        //         vheight = 10;
        //     }
        // }

        result = {
            x: 0,
            y: vitem.calcTop,
            width: this.stage.width,
            height: vitem.calcHeight,
        }
    } else if (vitem.lokasi=='cell'){
        result = {
            x: vitem.calcLeft,
            y: vitem.calcTop+(vitem.cellIndex-1)*vitem.calcHeight,
            width: vitem.calcWidth,
            height: vitem.calcHeight,
        }
    }
    return result;
}

JsBoard.Layouts.grid.prototype.prepareBoardLayout = function(vjsboard){


    console.log('prepare board layout');
    var vchips = this.container.chipItems;
    this.columns = [];

    // hitung total column
    this.totalColWidths = 0;
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.group=='columns') {
            if (!vitem.width){
                vitem.width = 1;
            }
            this.totalColWidths = this.totalColWidths+vitem.width;
        }
    }

    // update calc left & calc width untuk columns

    var stageWidth = this.stage.width;
    var vleft = 0;
    this.columnInfo = []
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.group=='columns'){
            var vtop = 0;
            if(typeof vitem.top !== 'undefined'){
                vtop = vitem.top;
            }
            var xwidth = vitem.width*stageWidth/this.totalColWidths;
            vitem.calcWidth = xwidth;
            vitem.calcLeft = vleft;
            vleft = vleft+xwidth;
            this.columnInfo.push({left: vitem.calcLeft, width: vitem.calcWidth, top: vtop})
        }
    }

    // update calc top dan calc height untuk header
    // *** total header height
    this.totalHeaderHeight = 0;
    // *** total footer height
    this.totalFooterHeight = 0;
    var vtop = 0;
    var vftop = this.stage.height;
    this.headerInfo = []
    this.footerInfo = []
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.group=='headers'){
            this.headerInfo.push({top: vtop, height: vitem.height});
            vitem.calcTop = vtop;
            vitem.calcHeight = vitem.height;
            vtop = vtop + vitem.height;
            this.totalHeaderHeight = this.totalHeaderHeight+vitem.height;
        }
        if (vitem.group=='footers'){
            this.footerInfo.push({top: vftop, height: vitem.height});
            vitem.calcTop = vftop-vitem.height;
            vitem.calcHeight = vitem.height;
            vftop = vftop - vitem.height;
            this.totalFooterHeight = this.totalFooterHeight+vitem.height;
        }
    }


    // update calc left & calc width untuk columns
    var varrleft = {};
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.group=='columnHeaders'){
            var vHeaderIdx = 0;
            if(typeof vitem.headerIdx !== 'undefined'){
                vHeaderIdx = vitem.headerIdx;
            }
            if (typeof varrleft[vHeaderIdx] == 'undefined'){
                varrleft[vHeaderIdx] = 0;
            }
            var xwidth = vitem.width*stageWidth/this.totalColWidths;
            vitem.calcWidth = xwidth;
            vitem.calcLeft = varrleft[vHeaderIdx];
            var vtop = 0;
            var vheight = 1;
            if (typeof this.headerInfo[vHeaderIdx] !== 'undefined'){
                vtop = this.headerInfo[vHeaderIdx].top;
                vheight = this.headerInfo[vHeaderIdx].height;
            }
            vitem.calcTop = vtop;
            vitem.calcHeight = vheight;

            varrleft[vHeaderIdx] = varrleft[vHeaderIdx] + xwidth;
        }
    }

    // vCounterCount = 0;
    // for (var i=0; i<vchips.length; i++){
    //     var vitem = vchips[i];
    //     if (vitem.lokasi=='counter'){
    //         vCounterCount = vCounterCount+1;
    //     }
    // }
    // this.chip.counterCount = vCounterCount;


    // hitung top dan height dari row
    var vtinggi = 0;
    var vcount = 0;
    this.rowInfo = []
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.group=='rows'){
            if (typeof vitem.height!=='undefined'){
                vtinggi = vtinggi+vitem.height;
            } else {
                vcount = vcount+1;
            }
        }
    }

    var vdefault = 10;
    if (vcount>0){
        vdefault = (this.stage.height - this.totalHeaderHeight - this.totalFooterHeight - vtinggi)/vcount;
    }
    var vtop = this.totalHeaderHeight;
    var vheight;
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.group=='rows'){
            if (typeof vitem.height!=='undefined'){
                vheight = vitem.height;
            } else {
                vheight = vdefault;
            }
            vitem.calcTop = vtop;
            vitem.calcHeight = vheight;
            vCellHeight = 50;
            if (typeof vitem.cellHeight !== 'undefined'){
                vCellHeight = vitem.cellHeight;
            }
            this.rowInfo.push({top: vtop, height: vheight, cellHeight: vCellHeight})
            vtop = vtop + vheight;
        }
    }

    // processing cells
    var vidxList = {}
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.group=='cells'){
            var vcol = vitem.col-1;
            var vrow = vitem.row-1;
            if ((typeof this.columnInfo[vcol] !== 'undefined') && (typeof this.rowInfo[vrow] !== 'undefined')){
                var xname = vcol+'.'+vrow;
                if (typeof vidxList[xname] == 'undefined'){
                    vidxList[xname] = 1;
                }
                vitem.cellIndex = vidxList[xname];
                vitem.calcLeft = this.columnInfo[vcol].left;
                vitem.calcWidth = this.columnInfo[vcol].width;
                vitem.calcTop = this.rowInfo[vrow].top;
                vitem.calcHeight = this.rowInfo[vrow].cellHeight;
                vidxList[xname] = vidxList[xname] +1;
            }
        }
    }


    

    
}

JsBoard.Layouts.grid.prototype.autoResizeStage = function(vjsboard){
    var vcontainer = vjsboard.config.board.container;
    var vobj = document.getElementById(vcontainer);
    if(vobj){
        var vwidth = vobj.clientWidth;
        if (vwidth>0){
            vjsboard.stage.setWidth(vwidth);
            vjsboard.config.board.width = vwidth;
            // vjsboard.stage.width = vwidth;
            var xscale = vwidth/this.stage.width;
            console.log("scale: ", xscale);
            vjsboard.stage.scaleX(xscale);
            vjsboard.stage.scaleY(xscale);
            vjsboard.stage.setHeight(xscale*this.stage.height);
            // vjsboard.board.scrollable.position({x: 0, y:0})
            // vjsboard.stage.height = this.stage.height*xscale;

            this.updateContainerLayout(vjsboard);
            vjsboard.redraw();
        }
    }
}