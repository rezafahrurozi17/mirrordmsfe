// copy dari default
JsBoard.Chips.jpcbv = JsBoard.Common.clone(JsBoard.Chips.jpcbdefault);

var jpcbBaseColorMap = {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#ffA520', done: '#787878'}
var jpcbLineColorMap = {default: '#000', belumdatang: '#000', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#ffA520', done: '#787878'}


JsBoard.Chips.jpcbv.footer = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    // {name: 'rect1', type: 'rect', x: 0, y:0, 
    //     width: 50, height: 30,
    //     fill: '#fff', stroke: '#000', strokeWidth: 0.7,
    //     margin: {left: 7, right: 7, top: 7, bottom: 7}
    // },
    // {name: 'text1', type: 'text', x: 50, y:0, 
    //     width: 90, height: 30,
    //     text: 'Appointment', 
    //     fill: '#000',
    //     wrap: 'none',
    //     margin: {left: 0, right: 0, top: 10, bottom: 0}
    // },
  ]
}

JsBoard.Chips.jpcbv.footer_stopage = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
  ]
}

var itemJPCB_footer_list = [
    {name: 'belumdatang', text: 'Belum Datang'},
    {name: 'sudahdatang', text: 'Sudah Datang'},
    {name: 'booking', text: 'Booking'},
    {name: 'walkin', text: 'Walk In'},
    {name: 'inprogress', text: 'In Progress'},
    {name: 'irregular', text: 'Irregular'},
    {name: 'extension', text: 'Extension'},
    {name: 'done', text: 'Done'},
]

var vleft=0;
for (var i=0; i<itemJPCB_footer_list.length; i++){
    var vitem = itemJPCB_footer_list[i];
    var vwidth = vitem.text.length * 9+30;
    JsBoard.Chips.jpcbv.footer.items.push({
        name: 'rect-'+i, type: 'rect', x: vleft, y:0, 
        width: 50, height: 30,
        fill: jpcbBaseColorMap[vitem.name], 
        stroke: jpcbLineColorMap[vitem.name], 
        strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    });
    JsBoard.Chips.jpcbv.footer.items.push({name: 'text1', type: 'text', x: vleft+50, y:0, 
        width: vwidth, height: 30,
        text: vitem.text, 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    });
    JsBoard.Chips.jpcbv.footer_stopage.items.push({
        name: 'rect-'+i, type: 'rect', x: vleft, y:0, 
        width: 50, height: 30,
        fill: jpcbBaseColorMap[vitem.name], 
        stroke: jpcbLineColorMap[vitem.name], 
        strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    });
    JsBoard.Chips.jpcbv.footer_stopage.items.push({name: 'text1', type: 'text', x: vleft+50, y:0, 
        width: vwidth, height: 30,
        text: vitem.text, 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    });
    vleft = vleft+vwidth+20;
    
}

// JsBoard.Chips.jpcbv.footer_stopage = JsBoard.Common.extend({}, JsBoard.Chips.jpcbv.footer);

JsBoard.Chips.jpcbv.footer_stopage.items.push(
    {name: 'separator', type: 'rect', x: 0, y:30, width: {data: 'chip', field: 'width'}, height: 5, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', 
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    }
)
JsBoard.Chips.jpcbv.footer_stopage.items.push(
    {name: 'jobstoppage', type: 'rect', x: 0, y:35, width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - 30 -5;
        },
        xheight: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    }
)
JsBoard.Chips.jpcbv.footer_stopage.items.push(
    {name: 'jobstoppagetitle', type: 'text', x: 0, y:40, 
        width: {data: 'chip', field: 'width'}, 
        height: 30, fontSize: 20,
        // text: 'Job Stoppage', 
        text: function(base, name, data, me) {
            var JobOnStoppage = 0;
            // for(var i=0; i<me.chipItems.length; i++){
            //     if(me.chipItems[i].isInStopPageGr == 1){
            //         JobOnStoppage = JobOnStoppage + 1;
            //     }
            // }
            JobOnStoppage = me.config.allChipsTemp.length;
            console.log('ada apa dengan jobstopage',me);
            return 'Job Stoppage' + ' ('+ JobOnStoppage +')';
        },
        fill: '#000',
        wrap: 'none',
        margin: {left: 8, right: 8, top: 10, bottom: 0}
    }
)
JsBoard.Chips.jpcbv.footer_stopage.items.push(
    {name: 'stoppagearea', type: 'rect', x: 0, y:80, width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - 30 -50;
        },
        xheight: {data: 'chip', field: 'height'},
        fill: '#f5f5f5', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    }
)    




JsBoard.Chips.jpcbv.stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#fafafa'
            } else {
                return '#ddd'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 18,fontStyle: 'bold',
        margin: {left: 10, top: 5, right: 5, bottom: 5}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#4141df', blank: '#aaa', blocked: '#a00'},
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'person'}, x: 0, y: 30, width: {data: 'chip', field: 'width'}, fontSize: 18, fontStyle: 'bold',
        margin: {left: 10, top: 5, right: 5, bottom: 5}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
  ]
}

JsBoard.Chips.jpcbv.chip = {
  items: [
    // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
    //   fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1, opacity: 0.9,
    //   margin: {left: 1, right: 1, top: 2, bottom: 2}
    // },

    {name: 'base', type: 'rect', x: 0, y:0, 
      width: {data: 'chip', field: 'width'}, 
      height: {data: 'chip', field: 'height'},
      
    //   fill: null,
      fill: function (base, name, data, me){
        console.log('data base coy', data);
        if (data['data']['isInStopPageGr'] == 1){
            return '#fff'
        } else {
            return null;
        }
      }, 
    //   stroke: '#ddd',
      stroke: '#686868', 
      cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
      {name: 'base1', type: 'rect', x: 0, y:0, 

            visible: function(base, name, data, me){
                if (data['data']['width1']){
                    return true
                } 
                return false;
            },
            width: function(base, name, data, me){
                if (data['data']['width1']){
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth']!=='undefined'){
                        vcolwidth = data['chip']['columnWidth'];
                    } 
                    return data['data']['width1']*vcolwidth;
                } 
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff', stroke: '#777', 
            cornerRadius: 2, strokeWidth: 1,
            margin: {left: 2, right: 2, top: 2, bottom: 2}
        },
        {name: 'base2', type: 'rect', 
            x: function(base, name, data, me){
                if (data['data']['start2']){
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth']!=='undefined'){
                        vcolwidth = data['chip']['columnWidth'];
                    } 
                    return data['data']['start2']*vcolwidth;
                } 
                return 0;
            },
            
            y:0, 
            visible: function(base, name, data, me){
                if (data['data']['width2']){
                    return true
                } 
                return false;
            },
            width: function(base, name, data, me){
                if (data['data']['width2']){
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth']!=='undefined'){
                        vcolwidth = data['chip']['columnWidth'];
                    } 
                    return data['data']['width2']*vcolwidth;
                } 
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff', stroke: '#777', 
            cornerRadius: 2, strokeWidth: 1,
            margin: {left: 2, right: 2, top: 2, bottom: 2}
        },
        {name: 'normal2', type: 'rect', y:32, 
        x: function(base, name, data, me){
            if (data['data']['start2']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['start2']*vcolwidth;
            } 
            return 0;
        },
        visible: function(base, name, data, me){
            if (data['data']['width2']){
                return true
            } 
            return false;
        },
        width: function(base, name, data, me){
            if (data['data']['width2']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['width2']*vcolwidth;
            } 
            return 50;
        },
      height: 21,
    //   fill: function(base, name, data, me){
    //         return JsBoard.Common.RandomColorList[data['data']['color_idx']]
    //     },
    fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      colorMap: jpcbBaseColorMap,
      lineColorMap: asbLineColorMap,
    //   xfill: '#fff',
    //   xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //   stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
      strokeWidth: 1,
      margin: {left: 8, right: 8, top: 0, bottom: 0}
    },


    // =============nih visualisasi 3 istirahat ================
    // {name: 'base3', type: 'rect', x: 0, y:0, 

    //         visible: function(base, name, data, me){
    //             if (data['data']['width3']){
    //                 return true
    //             } 
    //             return false;
    //         },
    //         width: function(base, name, data, me){
    //             if (data['data']['width3']){
    //                 var vcolwidth = 50;
    //                 if (typeof data['chip']['columnWidth']!=='undefined'){
    //                     vcolwidth = data['chip']['columnWidth'];
    //                 } 
    //                 return data['data']['width3']*vcolwidth;
    //             } 
    //             return 50;
    //         },
    //         //cwidth: {data: 'chip', field: 'width1'}, 
    //         height: {data: 'chip', field: 'height'},
    //         fill: '#fff', stroke: '#777', 
    //         cornerRadius: 2, strokeWidth: 1,
    //         margin: {left: 2, right: 2, top: 2, bottom: 2}
    //     },
        {name: 'base3', type: 'rect', 
            x: function(base, name, data, me){
                if (data['data']['start3']){
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth']!=='undefined'){
                        vcolwidth = data['chip']['columnWidth'];
                    } 
                    return data['data']['start3']*vcolwidth;
                } 
                return 0;
            },
            
            y:0, 
            visible: function(base, name, data, me){
                if (data['data']['width3']){
                    return true
                } 
                return false;
            },
            width: function(base, name, data, me){
                if (data['data']['width3']){
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth']!=='undefined'){
                        vcolwidth = data['chip']['columnWidth'];
                    } 
                    return data['data']['width3']*vcolwidth;
                } 
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff', stroke: '#777', 
            cornerRadius: 2, strokeWidth: 1,
            margin: {left: 2, right: 2, top: 2, bottom: 2}
        },
        {name: 'normal3', type: 'rect', y:32, 
        x: function(base, name, data, me){
            if (data['data']['start3']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['start3']*vcolwidth;
            } 
            return 0;
        },
        visible: function(base, name, data, me){
            if (data['data']['width3']){
                return true
            } 
            return false;
        },
        width: function(base, name, data, me){
            if (data['data']['width3']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['width3']*vcolwidth;
            } 
            return 50;
        },
        height: 21,
        //   fill: function(base, name, data, me){
        //         return JsBoard.Common.RandomColorList[data['data']['color_idx']]
        //     },
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: jpcbBaseColorMap,
        lineColorMap: asbLineColorMap,
        //   xfill: '#fff',
        //   xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        //   stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
        strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
        },

        {name: 'base4', type: 'rect', 
            x: function(base, name, data, me){
                if (data['data']['start4']){
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth']!=='undefined'){
                        vcolwidth = data['chip']['columnWidth'];
                    } 
                    return data['data']['start4']*vcolwidth;
                } 
                return 0;
            },
            
            y:0, 
            visible: function(base, name, data, me){
                if (data['data']['width4']){
                    return true
                } 
                return false;
            },
            width: function(base, name, data, me){
                if (data['data']['width4']){
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth']!=='undefined'){
                        vcolwidth = data['chip']['columnWidth'];
                    } 
                    return data['data']['width4']*vcolwidth;
                } 
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff', stroke: '#777', 
            cornerRadius: 2, strokeWidth: 1,
            margin: {left: 2, right: 2, top: 2, bottom: 2}
        },
        {name: 'normal4', type: 'rect', y:32, 
        x: function(base, name, data, me){
            if (data['data']['start4']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['start4']*vcolwidth;
            } 
            return 0;
        },
        visible: function(base, name, data, me){
            if (data['data']['width4']){
                return true
            } 
            return false;
        },
        width: function(base, name, data, me){
            if (data['data']['width4']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['width4']*vcolwidth;
            } 
            return 50;
        },
        height: 21,
        //   fill: function(base, name, data, me){
        //         return JsBoard.Common.RandomColorList[data['data']['color_idx']]
        //     },
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: jpcbBaseColorMap,
        lineColorMap: asbLineColorMap,
        //   xfill: '#fff',
        //   xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        //   stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
        strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
        },


    //==============end visualisasi 3 istrirapat ====================


    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    
    {name: 'jamcetakwo', type: 'text', text: {data: 'data', field: 'jamcetakwo'}, x: 0, y: 0, align: 'right', width: {data: 'chip', field: 'width'}, 
        visible: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return true
            } else {
                return false;
            }
        }
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'type'}, x: 0, y: 13, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    // {name: 'start2', type: 'text', text: {data: 'data', field: 'start2'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}},
    {name: 'jamjanjiserah', type: 'text', text: {data: 'data', field: 'jamjanjiserah'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}, 
        visible: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return true
            } else {
                return false;
            }
        }
    },
    {name: 'pekerjaan', type: 'text', text: {data: 'data', field: 'pekerjaan'}, x: 0, y: 51, width: {data: 'chip', field: 'width'}},
    {name: 'teknisi', type: 'text', text: {data: 'data', field: 'teknisi'}, x: 30, y: 51, align: 'right', 
        wrap: 'none',
        xwidth: {data: 'chip', field: 'width'},
        width: function(base, name, data, me){
            return data['chip']['width']-this.x;
        }
    },
    {name: 'normal', type: 'rect', x: 0, y:32, 
      width: function(base, name, data, me){
        console.log('chip-rect-2', data['chip']['width'])
        var vext = 0;
        var vwidth = 1;
        var vcolwidth = 50;
        var vnormal = 1;
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        if (typeof data['data']['width1']!=='undefined'){
            vnormal = data['data']['width1'];
          }
        return vcolwidth*vnormal;
      }, 
      height: 21,
      fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      xxfill: '#f00',
      colorMap: jpcbBaseColorMap,
    //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
      xxmargin: {left: 0, right: 0, top: 0, bottom: 0},
      margin: function(base, name, data, me){
        var vext = 0;
        if (typeof data['data']['ext']!=='undefined'){
          vext = data['data']['ext'];
        }
        if (vext>0){
          return {right: 0, top: 0, bottom: 0}
        } else {
          return {top: 0, bottom: 0}
        }
      }
    },
    {name: 'ext', type: 'rect', x: 0, y:32, 
      x: function(base, name, data, me){
        var vnormal = 0;
        var vcolwidth = 50;
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        console.log('jajajaja', data['data'])
        var cekJamAkhir = data['data']['PlanFinish']
        var splitJam = cekJamAkhir.split(':');
        console.log('ini data ku', data['data']['jobId'], 'ini data mu', parseInt(splitJam[0]))
        if (parseInt(splitJam[0]) < 12){
            return vcolwidth*vnormal;
        } else {
            return vcolwidth* (vnormal + data['data']['wBreak']);
        }
      }, 
      width: function(base, name, data, me){
        var vext = 0;
        var vcolwidth = 50;
        var breakWidth = 0;
        if (typeof data['data']['ext']!=='undefined'){
          vnormal = data['data']['ext'];
        }
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        }
        if (typeof data['data']['width2']!=='undefined'){
            breakWidth = data['chip']['columnWidth'];
          } 
        console.log('ruar binasa', vcolwidth)
        var cekJamAkhir = data['data']['PlanFinish']
        var splitJam = cekJamAkhir.split(':');
        if (parseInt(splitJam[0]) < 12){
            return vcolwidth*vnormal + breakWidth;
        } else {
            return vcolwidth*vnormal + 5;        
        }
      }, 
      visible: function(base, name, data, me){
        var vext = 0;
        if (typeof data['data']['ext']!=='undefined'){
          vext = data['data']['ext'];
        }
        if (vext>0){
          return 1;
        } else {
          return 0
        }
      },
      height: 21,
      fill: '#ffA520', 
      margin: function(base, name, data, me){
        var vext = 0;
        if (typeof data['data']['ext']!=='undefined'){
          vext = data['data']['ext'];
        }
        if (vext>0){
          return {left: 0, top: 0, bottom: 0}
        } else {
          return {top: 0, bottom: 0}
        }
      }
    },
    {name: 'main_icons', type: 'icons', 
        x: 0, y: 26, 
        icons: {data: 'data', field: 'statusicon', default: []},
        xwidth: function(base, name, data, me){
            console.log('chip-rect-2', data['chip']['width'])
            var vext = 0;
            var vwidth = 1;
            var vcolwidth = 50;
            var vnormal = 1;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            if (typeof data['data']['normal']!=='undefined'){
            vnormal = data['data']['normal'];
            }
            return vcolwidth*vnormal;
        }, 
        width: {data: 'chip', field: 'width'},
    },
    {name: 'bottom_icon', type: 'icons', 
        xxx: 25,
        x: function(base, name, data, me){
            var vext = 0;
            var vwidth = 1;
            var vcolwidth = 50;
            var vnormal = 1;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            if (typeof data['data']['normal']!=='undefined'){
                vnormal = data['data']['normal'];
            }
            if (typeof data['data']['ext']!=='undefined'){
                vext = data['data']['ext'];
            }
            vwidth = vnormal+vext;
            return Math.round(vwidth*vcolwidth/2)-14;
        }, 
        y: 46, 
        icons: ['play2'],
        width: 20,
        height: 16,
        iconHeight: 14,
        iconWidth: 14,
        visible: function(base, name, data, me){
            var vstat = data['data']['intStatus'];
            console.log('ada apa dengan muuuuu', data['data']);
            if (((vstat>=5)&&(vstat<=12))||(vstat==22)){
                if (data['data']['PreDiagnoseStallId'] != 0){
                    return false;
                } else {
                    return true;
                }

            } else {
                return false;
            }
            // if ((vstat==4)||(vstat==5)||(vstat==6)){
            //     vallocated = 1;
            //     if (typeof data['data']['allocated']!=='undefined'){
            //         vallocated = data['data']['allocated'];
            //     }
            //     if (vallocated){
            //         return false;
            //     } else {
            //         return true;
            //     }

            // } else {
            //     return false;
            // }
        }
    },
  ]
}

    // {name: 'base2', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 300, xheight: {data: 'chip', field: 'height'},
    //     fill: '#dd0', stroke: '#000',
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },

JsBoard.Chips.jpcbv.header = {
  items: [
    {name: 'base', type: 'rect', x: 0, 
        y: {data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['titleTop'];
        },
        fill: '#ccc',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, 
        y: {data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 25, fontStyle: 'bold',
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000', align: 'center',
    },
  ]
}

JsBoard.Chips.jpcbv.first_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, 
        y: {data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['titleTop'];
        },
        fill: '#ccc',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, 
        y: {data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 25, fontStyle: 'bold',
        margin: {left: 15, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
  ]
}

JsBoard.Chips.jpcbv.fix_header = {
  items: [
    {name: 'selectarea', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 2, opacity: 0.5,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
    // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
    //     fill: '#440', opacity: 0.5,
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    // {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
    //     margin: {left: 5, top: 10, right: 5, bottom: 5}, 
    //     fill: '#fff', align: 'center',
    // },
  ]
}

JsBoard.Chips.jpcbv.blank = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#777', stroke: '#777', 
        strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
        },
        
    ]    
}

// JsBoard.Chips.jpcbv.chip = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', 
//       cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//         fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, 
//         visible: function(base, name, data, me){
//             if (data['data']['selected']){
//                 return true
//             } 
//             return false;
//         },
//         margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
//         wrap: 'none', fontStyle: 'bold',
//         fontSize: function(base, name, data, me){
//             if (data['data']['width']>0.5){
//                 return 11
//             } else {
//                 return 10;
//             }
//         }
//     },
//     {name: 'type', type: 'text', 
//         text: function(base, name, data, me){
//             if (data['data']['row']<0){
//                 return data['data']['tipe']
//             } else {
//                 var vmin = data['chip']['incrementMinute'] * data['data']['col'];
//                 var vminhour = JsBoard.Common.getMinute(data['chip']['startHour'])+vmin;
//                 var vres = JsBoard.Common.getTimeString(vminhour);
//                 return vres;
//             }
//         }, 
//         x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
//     {name: 'stallperson', type: 'text', 
//         text: function(base, name, data, vcont){
//             console.log("container", vcont);
//             var vres = '';
//             if (typeof vcont.layout.getInfoRowData !== 'undefined'){
//                 var vrow = data['data']['row'];
//                 var vdata = vcont.layout.getInfoRowData(vrow);
//                 if (vdata && vdata['person']){
//                     vres = vdata['person'];
//                 }
//             }
//             return vres;
//         },
//         x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
//         margin: {right: 14},
//     },
//     {name: 'stallname', type: 'text', 
//         text: function(base, name, data, vcont){
//             console.log("container", vcont);
//             var vres = '';
//             if (typeof vcont.layout.getInfoRowData !== 'undefined'){
//                 var vrow = data['data']['row'];
//                 var vdata = vcont.layout.getInfoRowData(vrow);
//                 if (vdata && vdata['title']){
//                     vres = vdata['title'];
//                 }
//             }
//             return vres;
//         },
//         x: 4, y: 54, align: 'left', width: {data: 'chip', field: 'width'},
//     },
//     {name: 'normal', type: 'rect', x: 0, y:36, 
//       width: function(base, name, data, me){
//         console.log('chip-rect-2', data['chip']['width'])
//         var vext = 0;
//         var vwidth = 1;
//         var vcolwidth = 50;
//         var vnormal = data['data']['width'];
//         if (typeof data['chip']['columnWidth']!=='undefined'){
//             vcolwidth = data['chip']['columnWidth'];
//         } 
//         if (typeof data['data']['normal']!=='undefined'){
//           vnormal = data['data']['normal'];
//         }
//         return vcolwidth*vnormal;
//       }, 
//       height: 21,
//       fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
//       colorMap: tab_Fill_colorMap,
//       lineColorMap: tab_Line_colorMap,
//       xfill: '#fff',
//       stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
//       strokeWidth: 1,
//       margin: {left: 8, right: 6, top: 0, bottom: 0}
//     },
//     {name: 'white_icon', type: 'icons', 
//         x: 4, y: 30, 
//         icons: ['customer'],
//         width: function(base, name, data, me){
//             var vext = 0;
//             var vwidth = 1;
//             var vcolwidth = 50;
//             var vnormal = 1;
//             if (typeof data['chip']['columnWidth']!=='undefined'){
//                 vcolwidth = data['chip']['columnWidth'];
//             } 
//             if (typeof data['data']['normal']!=='undefined'){
//             vnormal = data['data']['normal'];
//             }
//             return vcolwidth*vnormal;
//         }, 
//         visible: {type: 'map', mapdata: 'config', mapfield: 'statusMap', data: 'data', field: 'status'}, 
//         statusMap: {default: false, appointment: true, no_show: false},
//     },
//     {name: 'gray_icon', type: 'icons', 
//         x: 4, y: 30, 
//         icons: ['customer_gray'],
//         width: function(base, name, data, me){
//             var vext = 0;
//             var vwidth = 1;
//             var vcolwidth = 50;
//             var vnormal = 1;
//             if (typeof data['chip']['columnWidth']!=='undefined'){
//                 vcolwidth = data['chip']['columnWidth'];
//             } 
//             if (typeof data['data']['normal']!=='undefined'){
//             vnormal = data['data']['normal'];
//             }
//             return vcolwidth*vnormal;
//         }, 
//         visible: {type: 'map', mapdata: 'config', mapfield: 'statusMap', data: 'data', field: 'status'}, 
//         statusMap: {default: true, appointment: false, no_show: true},
//     },
//   ]
// }

