JsBoard.Chips.wolist = {}

JsBoard.Chips.wolist.none = {
  items: [
  ]
}


JsBoard.Chips.wolist.blank = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f0f0f0', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}

JsBoard.Chips.wolist.blank2 = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f00000', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}

JsBoard.Chips.wolist.simple = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#333', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'basetext', type: 'text', 
        x: 0, y:0, width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'text'},
        fill: '#000',
        align: 'center',
        margin: {left: 0, right: 5, top: 10, bottom: 0}
    }
  ]
}

JsBoard.Chips.wolist.white = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}




JsBoard.Chips.wolist.header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#666', 
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    }
  ]
}

JsBoard.Chips.wolist.columnHeader = {
  items: [
    // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
    //   fill: null, stroke: '#fff',
    //   margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    {name: 'basetext', type: 'text', 
        x: 0, y:0, width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'title'},
        fill: '#fff',
        align: 'center',
        fontSize: 16,
        margin: {left: 0, right: 0, top: 16, bottom: 0}
    }
  ]
}

var wolistColorMap = {default: '#fff', normal: '#0b0', problem: '#b00', paused: '#ff0'}
// var wolistLineColorMap = {default: '#000', block: '#b00', extension: '#db0', appoinment: '#000'}

var margin_cell = 10;
var margin_item = 18;
JsBoard.Chips.wolist.cellChip = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', 
      cornerRadius: 2, strokeWidth: 1,
      margin: {left: margin_cell, right: margin_cell, top: 10, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 14, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        align: 'left',
        fontSize: 14,
        margin: {left: margin_item, right: margin_item, top: 2, bottom: 2}
    },
    {name: 'jamdatang', type: 'text', text: {data: 'data', field: 'jamdatang'}, x: 0, y: 14, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', 
        align: 'center',
        fontSize: 14,
        margin: {left: margin_item, right: 80, top: 2, bottom: 2}
    },
    {name: 'janjiserah', type: 'text', text: {data: 'data', field: 'janjiserah'}, x: 0, y: 14, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', 
        align: 'right',
        fontSize: 14,
        margin: {left: margin_item, right: margin_item, top: 2, bottom: 2}
    },
    {name: 'tipe', type: 'text', text: {data: 'data', field: 'tipe'}, x: 0, y: 34, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', 
        align: 'left',
        fontSize: 14,
        margin: {left: margin_item, right: margin_item, top: 2, bottom: 2}
    },
    {name: 'waktupengerjaan', type: 'text', text: {data: 'data', field: 'waktupengerjaan'}, x: 0, y: 34, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', 
        align: 'right',
        fontSize: 14,
        margin: {left: margin_item, right: margin_item, top: 2, bottom: 2}
    },
    {name: 'pekerjaan', type: 'text', text: {data: 'data', field: 'pekerjaan'}, x: 0, y: 80, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', 
        align: 'left',
        fontSize: 14,
        margin: {left: margin_item, right: margin_item, top: 2, bottom: 2}
    },
    {name: 'saname', type: 'text', text: {data: 'data', field: 'saname'}, x: 0, y: 80, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', 
        align: 'right',
        fontSize: 14,
        margin: {left: margin_item, right: margin_item, top: 2, bottom: 2}
    },
    // {name: 'type', type: 'text', 
    //     text: function(base, name, data, me){
    //         if (data['data']['row']<0){
    //             return data['data']['tipe']
    //         } else {
    //             var vmin = data['chip']['incrementMinute'] * data['data']['col'];
    //             var vminhour = JsBoard.Common.getMinute(data['chip']['startHour'])+vmin;
    //             var vres = JsBoard.Common.getTimeString(vminhour);
    //             return vres;
    //         }
    //     }, 
    //     x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    // {name: 'stallperson', type: 'text', 
    //     text: function(base, name, data, vcont){
    //         console.log("container", vcont);
    //         var vres = '';
    //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
    //             var vrow = data['data']['row'];
    //             var vdata = vcont.layout.getInfoRowData(vrow);
    //             if (vdata && vdata['person']){
    //                 vres = vdata['person'];
    //             }
    //         }
    //         return vres;
    //     },
    //     x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
    //     margin: {right: 14},
    // },
    // {name: 'stallname', type: 'text', 
    //     text: function(base, name, data, vcont){
    //         console.log("container", vcont);
    //         var vres = '';
    //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
    //             var vrow = data['data']['row'];
    //             var vdata = vcont.layout.getInfoRowData(vrow);
    //             if (vdata && vdata['title']){
    //                 vres = vdata['title'];
    //             }
    //         }
    //         return vres;
    //     },
    //     x: 4, y: 54, align: 'left', width: {data: 'chip', field: 'width'},
    // },
    {name: 'normal', type: 'rect', x: 0, y:56, 
        width: {data: 'chip', field: 'width'},
        height: 21,
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: wolistColorMap,
        //lineColorMap: asbLineColorMap,
        //xfill: '#fff',
        stroke: null, //{type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
        //strokeWidth: 1,
        margin: {left: margin_item, right: margin_item, top: 0, bottom: 0}
    },
    {name: 'icon', type: 'icons', 
        x: margin_item, y: 50, 
        icons: {data: 'data', field: 'statusicon', default: ['customer']}, 
        width: function(base, name, data, me){
            console.log('chip-rect-2', data['chip']['width'])
            var vext = 0;
            var vwidth = 1;
            var vcolwidth = 50;
            var vnormal = 1;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            if (typeof data['data']['normal']!=='undefined'){
            vnormal = data['data']['normal'];
            }
            return vcolwidth*vnormal;
        }, 
    },
  ]

}

////

JsBoard.Chips.wolist.row = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#fff'
            } else {
                return '#ddd'
            }
        },
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'basetext', type: 'text', x: 0, y:0, 
        width: {data: 'chip', field: 'numberWidth'}, 
        height: {data: 'chip', field: 'height'},
        text: {data: 'data', field: 'text'},
        xtext: function(base, name, data, me){
            return (data['data']['index'] %2)+'';
        },
        fill: '#000',
        align: 'right',
        margin: {left: 0, right: 5, top: 8, bottom: 0}
    }
  ]
}

JsBoard.Chips.wolist.counterheader = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#b00', 
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'basetext', type: 'text', 
        x: 0, y:0, width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'title'},
        fill: '#fff',
        align: 'center',
        fontSize: 18,
        margin: {left: 0, right: 5, top: 6, bottom: 0}
    }
  ]
}


JsBoard.Chips.wolist.column = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#333', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'basetext', type: 'text', 
        x: 0, y:0, width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'title'},
        fill: '#fff',
        align: 'center',
        margin: {left: 0, right: 5, top: 10, bottom: 0}
    }
  ]
}

JsBoard.Chips.wolist.counterbg = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#ddd', stroke: '#777', strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 0}
    },
    {name: 'base1', type: 'rect', x: 0, y:24, width: {data: 'chip', field: 'width'}, height: 30,
      fill: '#fff', 
      margin: {left: 2, right: 2, top: 0, bottom: 0}
    }
  ]
}

JsBoard.Chips.wolist.chip = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'counterName', type: 'text', 
        x: 0, y:0, 
        width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'counterName'},
        fill: '#000',
        align: 'center',
        fontSize: 11,
        margin: {left: 0, right: 0, top: 5, bottom: 0}
    },
    {name: 'nopol', type: 'text', 
        x: 0, 
        y: function(base, name, data, me){
            return Math.round(data['chip']['height']*0.25);
        },
        width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'nopol'},
        fill: '#000',
        align: 'center',
        fontSize: 14,
        fontStyle: 'bold',
        margin: {left: 0, right: 0, top: 5, bottom: 0}
    },
    {name: 'person', type: 'text', 
        x: 0, 
        y: function(base, name, data, me){
            return data['chip']['height']-Math.round(data['chip']['height']*0.3);
        },
        width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'person'},
        fill: '#000',
        align: 'center',
        fontSize: 11,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    }
  ]
}

var status_chip_lebar_caption = 120;
JsBoard.Chips.wolist.lastcall = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff', stroke: '#333', strokeWidth: 1,
            margin: {left: 1, right: 1, top: 0, bottom: 0}
        },
        // {name: 'caption', type: 'rect', 
        //     x: 0, y:0, 
        //     width: status_chip_lebar_caption,
        //     height: {data: 'chip', field: 'height'},
        //     fill: '#fff',
        //     // stroke: '#333', strokeWidth: 1,
        //     margin: {left: 1, right: 0, top: 0, bottom: 0}
        // },
        {name: 'captiontext', type: 'text', 
            x: 0, y:0, 
            width: status_chip_lebar_caption,
            height: {data: 'chip', field: 'height'},
            text: {data: 'data', field: 'caption'}, 
            fill: '#000',
            fontSize: 16,
            margin: {left: 0, right: 0, top: 4, bottom: 0},
            align: 'center',
        },
        // {name: 'content', type: 'rect', 
        //     x: status_chip_lebar_caption, 
        //     y: 0, 
        //     width: function(base, name, data, me){
        //         return data['chip']['width'] - status_chip_lebar_caption
        //     },
        //     height: {data: 'chip', field: 'height'},
        //     fill: '#ddd',
        //     stroke: '#333', strokeWidth: 1,
        //     margin: {left: 0, right: 1, top: 0, bottom: 0}
        // },
        {name: 'contenttext', type: 'text', 
            x: status_chip_lebar_caption, 
            y: 0, 
            width: function(base, name, data, me){
                return data['chip']['width'] - status_chip_lebar_caption
            },
            height: {data: 'chip', field: 'height'},
            text: {data: 'data', field: 'text'}, 
            fill: '#000',
            fontSize: 16,
            margin: {left: 12, right: 0, top: 4, bottom: 0},
            align: 'left',
        },
    ]
}

JsBoard.Chips.wolist.runningtext = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#555', stroke: null,
            // stroke: '#777', strokeWidth: 1,
            margin: {left: 0, right: 0, top: 0, bottom: 0}
        },
        {name: 'text', type: 'text', 
            y: 0, 
            text: {data: 'data', field: 'text'}, 
            fontStyle: 'bold',
            width: function(base, name, data, me){
                var vtext = data['config']['text'];
                var xtest = new Konva.Text({
                    left: 0, top: 0, height: 10, text: data['data']['text'],
                    fontStyle: 'bold',
                    fontSize: 14,
                });
                var vwidth = Math.round((xtest.width()+20)/10)*10;
                console.log('text-width', vwidth);
                return vwidth;
            }, 
            x: function(base, name, data, me){
                if (typeof data['data']['left'] == 'undefined' || data['data']['left']===false){
                    if (typeof this.width=='function'){
                        var vwidth = this.width(base, name, data, me);
                    } else {
                        var vwidth = this.width;
                    }
                    return vwidth;
                } else {
                    return data['data']['left'] - data['data']['deltaJarak'];
                }
                return vx;
            }, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff',
            fontSize: 14,
            margin: {left: 10, right: 0, top: 6, bottom: 0},
            align: 'left',
        },
    ]
}

JsBoard.Chips.wolist.nopol = {
  items: [
    {name: 'basetext', type: 'text', 
        x: 0, y:0, width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'nopol'},
        fill: '#000',
        align: 'center',
        fontSize: 16,
        margin: {left: 0, right: 0, top: 6, bottom: 0}
    }
  ]
}


JsBoard.Chips.wolist.footer = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, 
            width: {data: 'chip', field: 'width'}, 
            height: {data: 'chip', field: 'height'},
            fill: '#fff', 
            stroke: '#000', 
            strokeWidth: 0.7,
            margin: {left: 2, right: 2, top: 2, bottom: 2}
        },
    ]
}

//{default: '#fff', normal: '#0b0', problem: '#b00', paused: '#ff0'}
//var wolistColorMap = {default: '#fff', normal: '#0b0', problem: '#b00', paused: '#ff0'}
var itemWoList_footer_list = [
    {name: 'normal', text: 'Normal'},
    {name: 'problem', text: 'Problem'},
    {name: 'paused', text: 'Paused'},
]

var vleft=0;
for (var i=0; i<itemWoList_footer_list.length; i++){
    var vitem = itemWoList_footer_list[i];
    var vwidth = vitem.text.length * 9+30;
    JsBoard.Chips.wolist.footer.items.push({
        name: 'rect-'+i, type: 'rect', x: vleft, y:0, 
        width: 50, height: 30,
        fill: wolistColorMap[vitem.name], 
        stroke: '#000', 
        strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    });
    JsBoard.Chips.wolist.footer.items.push({name: 'text1', type: 'text', x: vleft+50, y:0, 
        width: vwidth, height: 30,
        text: vitem.text, 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    });
    vleft = vleft+vwidth+20;
    
}


// JsBoard.Chips.wolist.footer = {
//     items: [
//         // {name: 'base', type: 'rect', x: 0, y:0, 
//         //     width: {data: 'chip', field: 'width'}, 
//         //     height: {data: 'chip', field: 'height'},
//         //     fill: '#333', 
//         //     margin: {left: 0, right: 0, top: 0, bottom: 0}
//         // },
//         {name: 'base', type: 'icons', 
//             x: -3, 
//             y:-3, 
//             width: {data: 'chip', field: 'width'}, 
//             height: {data: 'chip', field: 'height'},
//             iconWidth: {data: 'chip', field: 'width'}, 
//             iconHeight: {data: 'chip', field: 'height'},
//             margin: {left: 0, right: 0, top: 0, bottom: 0},
//             icons: ['footerbkg']
//        },
//         {name: 'text1', type: 'text', 
//             x: function(base, name, data, me){
//                 return (data['chip']['width']/5)*0
//             },
//             y: 8, 
//             width: function(base, name, data, me){
//                 return (data['chip']['width']/5)*1.3;
//             }, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#fff',
//             margin: {left: 0, right: 0, top: 0, bottom: 0},
//             text: function(base, name, data, me){
//                 if(typeof data['data']['items'][0] !== 'undefined'){
//                     return data['data']['items'][0];
//                 }
//                 return '';
//             },
//             fontSize: 14,
//             align: 'center',
//        },
//         {name: 'text2', type: 'text', 
//             x: function(base, name, data, me){
//                 return (data['chip']['width']/5)*1.3
//             },
//             y: 8, 
//             width: function(base, name, data, me){
//                 return (data['chip']['width']/5)*0.7;
//             }, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#000',
//             margin: {left: 0, right: 0, top: 0, bottom: 0},
//             text: function(base, name, data, me){
//                 if(typeof data['data']['items'][1] !== 'undefined'){
//                     return data['data']['items'][1];
//                 }
//                 return '';
//             },
//             fontSize: 14,
//             align: 'center',
//        },
//         {name: 'text3', type: 'text', 
//             x: function(base, name, data, me){
//                 return (data['chip']['width']/5)*2
//             },
//             y: 8, 
//             width: function(base, name, data, me){
//                 return data['chip']['width']/5;
//             }, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#000',
//             margin: {left: 10, right: 0, top: 0, bottom: 0},
//             text: function(base, name, data, me){
//                 if(typeof data['data']['items'][2] !== 'undefined'){
//                     return data['data']['items'][2];
//                 }
//                 return '';
//             },
//             fontSize: 14,
//             align: 'center',
//        },
//         {name: 'text4', type: 'text', 
//             x: function(base, name, data, me){
//                 return (data['chip']['width']/5)*3
//             },
//             y: 8, 
//             width: function(base, name, data, me){
//                 return (data['chip']['width']/5)*0.8;
//             }, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#fff',
//             margin: {left: 0, right: 0, top: 0, bottom: 0},
//             text: function(base, name, data, me){
//                 if(typeof data['data']['items'][3] !== 'undefined'){
//                     return data['data']['items'][3];
//                 }
//                 return '';
//             },
//             fontSize: 14,
//             align: 'center',
//        },
//         {name: 'text5', type: 'text', 
//             x: function(base, name, data, me){
//                 return (data['chip']['width']/5)*3.8
//             },
//             y: 8, 
//             width: function(base, name, data, me){
//                 return (data['chip']['width']/5)*1.2;
//             }, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#fff',
//             margin: {left: 0, right: 0, top: 0, bottom: 0},
//             text: function(base, name, data, me){
//                 if(typeof data['data']['items'][4] !== 'undefined'){
//                     return data['data']['items'][4];
//                 }
//                 return '';
//             },
//             fontSize: 14,
//             align: 'center',
//        },
//         {name: 'base2', type: 'rect', x: 0, y:0, 
//             width: {data: 'chip', field: 'width'}, 
//             height: {data: 'chip', field: 'height'},
//             stroke: '#000', strokeWidth: 2, fill: null,
//             margin: {left: 1, right: 1, top: 1, bottom: 0}
//         },
       
//     ]
// }

// JsBoard.Chips.wolist.runningtext = {
//     items: [
//         {name: 'base', type: 'rect', x: 0, y:0, 
//             width: {data: 'chip', field: 'width'}, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#555', stroke: null,
//             // stroke: '#777', strokeWidth: 1,
//             margin: {left: 0, right: 0, top: 0, bottom: 0}
//         },
//         {name: 'text', type: 'text', 
//             y: 0, 
//             text: {data: 'data', field: 'text'}, 
//             fontStyle: 'bold',
//             width: function(base, name, data, me){
//                 var vtext = data['config']['text'];
//                 var xtest = new Konva.Text({
//                     left: 0, top: 0, height: 10, text: data['data']['text'],
//                     fontStyle: 'bold',
//                     fontSize: 14,
//                 });
//                 var vwidth = Math.round((xtest.width()+20)/10)*10;
//                 console.log('text-width', vwidth);
//                 return vwidth;
//             }, 
//             x: function(base, name, data, me){
//                 if (typeof data['data']['left'] == 'undefined' || data['data']['left']===false){
//                     if (typeof this.width=='function'){
//                         var vwidth = this.width(base, name, data, me);
//                     } else {
//                         var vwidth = this.width;
//                     }
//                     return vwidth;
//                 } else {
//                     return data['data']['left'] - data['data']['deltaJarak'];
//                 }
//                 // var vcounter = 0;
//                 // if (typeof data['data']['counter'] !== 'undefined'){
//                 //     vcounter = data['data']['counter'];
//                 // }
//                 // var vx = (vcounter * data['data']['deltaJarak']) % vwidth;


//                 // if (vx>vwidth){
//                 //     vx = 0;
//                 // } else {
//                 //     vx = -vx;
//                 // }
//                 // vx = data['chip']['width']-vx
//                 return vx;
//             }, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#fff',
//             fontSize: 14,
//             margin: {left: 10, right: 0, top: 2, bottom: 0},
//             align: 'left',
//         },
//         // {name: 'base2', type: 'rect', x: 0, y:0, 
//         //     width: {data: 'chip', field: 'width'}, 
//         //     height: {data: 'chip', field: 'height'},
//         //     stroke: '#000', strokeWidth: 2, fill: null,
//         //     margin: {left: 1, right: 1, top: 1, bottom: 0}
//         // },
//     ]
// }

// JsBoard.Chips.wolist.listbg = {
//     items: [
//         {name: 'base', type: 'rect', x: 0, y:0, 
//             width: {data: 'chip', field: 'width'}, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#fff', stroke: '#777', strokeWidth: 1,
//             margin: {left: 0, right: 1, top: 1, bottom: 0}
//         },
//     ]
// }



// var statusCustomerColorMap = {default: '#f0f0f0', wrecept: '#db0000', production: '#ff0', finspection: '#feb856', dokumen: '#0082e1', siapserah: '#00b925'}
// var statusCustomerColorMapText = {default: '#fff', wrecept: '#fff', production: '#000', finspection: '#000', dokumen: '#fff', siapserah: '#fff'}
// JsBoard.Chips.wolist.chip = {
//     items: [
//         {name: 'base', type: 'rect', x: 0, y:0, 
//             visible: function(base, name, data, me){
//                 console.log("nopol", data['data']['nopol']);
//                 console.log("showPageIndex", data['chip']['showPageIndex']);
//                 console.log("data.pageIndex", data['data']['pageIndex']);
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             width: {data: 'chip', field: 'width'}, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#f0f0f0', stroke: '#777', cornerRadius: 4, strokeWidth: 2,
//             margin: {left: 5, right: 5, top: 4, bottom: 2}
//         },
//         {name: 'processtype', type: 'rect', x: 0, y:0, 
//             visible: function(base, name, data, me){
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             width: {data: 'chip', field: 'width'}, 
//             height: 40,
//             fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
//             colorMap: statusCustomerColorMap,
//             stroke: '#777', cornerRadius: 4, strokeWidth: 1,
//             margin: {left: 5, right: 5, top: 4, bottom: 2}
//         },
//         {name: 'nopol', type: 'text', x: 0, y:0, 
//             visible: function(base, name, data, me){
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             width: {data: 'chip', field: 'width'}, 
//             height: {data: 'chip', field: 'height'},
//             fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
//             colorMap: statusCustomerColorMapText,
//             fontSize: 18,
//             align: 'center',
//             fontStyle: 'bold',
//             text: {data: 'data', field: 'nopol'}, 
//             margin: {left: 5, right: 5, top: 12, bottom: 2}
//         },
//         {name: 'subjudul', type: 'text', x: 0, 
//             visible: function(base, name, data, me){
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             y:40, 
//             width: {data: 'chip', field: 'width'}, 
//             height: 20,
//             fill: '#000',
//             text: function(base, name, data, me){
//                 return 'Janji selesai: '+data['data']['janjiselesai'];
//             },
//             xtext: {data: 'data', field: 'janjiselesai'}, 
//             fontSize: 14,
//             align: 'left',
//             fontStyle: 'bold',
//             margin: {left: 8, right: 80, top: 2, bottom: 2}
//         },
//         {name: 'subjudul', type: 'text', x: 0, 
//             visible: function(base, name, data, me){
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             y:40, 
//             width: {data: 'chip', field: 'width'}, 
//             height: 20,
//             fill: '#000',
//             text: {data: 'data', field: 'sa_name'}, 
//             fontSize: 14,
//             align: 'right',
//             margin: {left: 5, right: 8, top: 2, bottom: 2}
//         },
//     ]    
// }

// JsBoard.Chips.wolist.chip_bp = {
//     items: [
//         {name: 'base', type: 'rect', x: 0, y:0, 
//             visible: function(base, name, data, me){
//                 console.log("nopol", data['data']['nopol']);
//                 console.log("showPageIndex", data['chip']['showPageIndex']);
//                 console.log("data.pageIndex", data['data']['pageIndex']);
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             width: {data: 'chip', field: 'width'}, 
//             height: {data: 'chip', field: 'height'},
//             fill: '#f0f0f0', stroke: '#777', cornerRadius: 4, strokeWidth: 2,
//             margin: {left: 5, right: 5, top: 4, bottom: 2}
//         },
//         {name: 'processtype', type: 'rect', x: 0, y:0, 
//             visible: function(base, name, data, me){
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             width: {data: 'chip', field: 'width'}, 
//             height: 40,
//             fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
//             colorMap: statusCustomerColorMap,
//             stroke: '#777', cornerRadius: 4, strokeWidth: 1,
//             margin: {left: 5, right: 5, top: 4, bottom: 2}
//         },
//         {name: 'nopol', type: 'text', x: 0, y:0, 
//             visible: function(base, name, data, me){
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             width: {data: 'chip', field: 'width'}, 
//             height: {data: 'chip', field: 'height'},
//             fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
//             colorMap: statusCustomerColorMapText,
//             fontSize: 18,
//             align: 'center',
//             fontStyle: 'bold',
//             text: {data: 'data', field: 'nopol'}, 
//             margin: {left: 5, right: 5, top: 12, bottom: 2}
//         },
//         {name: 'subjudul', type: 'text', x: 0, 
//             visible: function(base, name, data, me){
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             y:40, 
//             width: {data: 'chip', field: 'width'}, 
//             height: 20,
//             fill: '#000',
//             text: function(base, name, data, me){
//                 if (data['data']['status']=='wrecept'){
//                     return 'Jam datang: '+data['data']['datang'];
//                 } else {
//                     return 'Janji Selesai: '+data['data']['janjiselesai'];
//                 }
//             },
//             xtext: {data: 'data', field: 'janjiselesai'}, 
//             fontSize: 14,
//             align: 'left',
//             fontStyle: 'bold',
//             margin: {left: 8, right: 40, top: 2, bottom: 2}
//         },
//         {name: 'subjudul', type: 'text', x: 0, 
//             visible: function(base, name, data, me){
//                 if (data['chip']['showPageIndex']==data['data']['pageIndex']){
//                     return true;
//                 }
//                 return false;
//             },
//             y:40, 
//             width: {data: 'chip', field: 'width'}, 
//             height: 20,
//             fill: '#000',
//             text: {data: 'data', field: 'sa_name'}, 
//             fontSize: 14,
//             align: 'right',
//             margin: {left: 5, right: 8, top: 2, bottom: 2}
//         },
//     ]    
// }

JsBoard.Chips.wolist.columntitle = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#333', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'basetext', type: 'text', 
        x: 0, y:0, width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'title'},
        fill: '#fff',
        align: 'center',
        fontSize: 20,
        margin: {left: 0, right: 5, top: 6, bottom: 0}
    }
  ]
}

JsBoard.Chips.wolist.columnbp = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 32,
      fill: null, stroke: '#333', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#333', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'basetext', type: 'text', 
        x: 0, y:0, width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'title'},
        fill: '#fff',
        align: 'center',
        fontSize: 20,
        margin: {left: 0, right: 5, top: 6, bottom: 0}
    }
  ]
}

JsBoard.Chips.wolist.columnbp2 = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 24,
      fill: null, stroke: '#333', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#333', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'basetext', type: 'text', 
        x: 0, y:0, width: {data: 'chip', field: 'width'},
        text: {data: 'data', field: 'title'},
        fill: '#000',
        align: 'center',
        fontSize: 16,
        fontStyle: 'bold',
        margin: {left: 0, right: 5, top: 4, bottom: 0}
    }
  ]
}

