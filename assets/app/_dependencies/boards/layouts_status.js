//// Layout for status Board
//// dependent on layouts.js
JsBoard.Layouts.status = function(config){
    this.parent = JsBoard.Layouts.base;
    // call parent's constructor
    this.parent.call(this, config);
    //JsBoard.Layouts.base.call(this);

}

// inherit status board from base board
JsBoard.Layouts.status.prototype = Object.create(JsBoard.Layouts.base.prototype);

// correct constructor pointer because it points to parent
JsBoard.Layouts.status.prototype.constructor = JsBoard.Layouts.status;

JsBoard.Layouts.status.prototype.createStage = function(){
    vconfig = this.stage; // <--- config.stage
    vcontainer = this.board.container;

    result = new Konva.Stage({
      container: vcontainer,
      width: vconfig.width,
      height: vconfig.height
    });
    return result;

}

JsBoard.Layouts.status.prototype.initLayout = function(){
    this.parent.prototype.initLayout.call(this);
    var defconfig = {
        headerHeight: 100,
        subHeaderHeight: 40,
        chipItemHeight: 64,
        listWidth: 240,
        footerHeight: 30,
        runningTextHeight: 20,
    }
    this.board = JsBoard.Common.extend(defconfig, this.board);
}

JsBoard.Layouts.status.prototype.chipDimension = function(vitem){
    var result = {}
    if (vitem.lokasi=='header'){
        result = {
            x: 0,
            y: 0,
            width: this.stage.width,
            height: this.board.headerHeight,
        }
    } else if (vitem.lokasi=='subheader'){
        result = {
            x: 0,
            y: this.board.headerHeight,
            width: this.stage.width,
            height: this.board.subHeaderHeight,
        }
    } else if (vitem.lokasi=='listbg'){
        result = {
            x: this.stage.width-this.board.listWidth,
            y: this.board.headerHeight+this.board.subHeaderHeight,
            width: this.board.listWidth,
            height: this.stage.height-this.board.subHeaderHeight-this.board.headerHeight-this.board.footerHeight,
        }
    } else if (vitem.lokasi=='listitem'){
        if (typeof vitem.indexByPage == 'undefined'){
            vitem.indexByPage = 1;
        }
        result = {
            x: this.stage.width-this.board.listWidth,
            y: this.board.headerHeight+this.board.subHeaderHeight+this.board.chipItemHeight*(vitem.indexByPage),
            width: this.board.listWidth,
            height: this.board.chipItemHeight,
        }
    } else if (vitem.lokasi=='runningtext'){
        result = {
            x: 0,
            y: this.stage.height-this.board.footerHeight-this.board.runningTextHeight,
            width: this.stage.width-this.board.listWidth,
            height: this.board.runningTextHeight,
        }
    } else if (vitem.lokasi=='footer'){
        result = {
            x: 0,
            y: this.stage.height-this.board.footerHeight,
            width: this.stage.width,
            height: this.board.footerHeight,
        }
    }
    return result;
}

JsBoard.Layouts.status.prototype.autoResizeStage = function(vjsboard){
    var vcontainer = vjsboard.config.board.container;
    var vobj = document.getElementById(vcontainer);
    if(vobj){
        var vwidth = vobj.clientWidth;
        if (vwidth>0){
            vjsboard.stage.setWidth(vwidth);
            vjsboard.config.board.width = vwidth;
            // vjsboard.stage.width = vwidth;
            var xscale = vwidth/this.stage.width;
            console.log("scale: ", xscale);
            vjsboard.stage.scaleX(xscale);
            vjsboard.stage.scaleY(xscale);
            vjsboard.stage.setHeight(xscale*this.stage.height);
            // vjsboard.board.scrollable.position({x: 0, y:0})
            // vjsboard.stage.height = this.stage.height*xscale;

            this.updateContainerLayout(vjsboard);
            vjsboard.redraw();
        }
    }
}