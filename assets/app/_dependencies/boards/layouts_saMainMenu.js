//// Layout for saMainMenu Board
//// dependent on layouts.js
JsBoard.Layouts.saMainMenu = function(config) {
  this.parent = JsBoard.Layouts.base;
  // call parent's constructor
  this.parent.call(this, config);
  //JsBoard.Layouts.base.call(this);

}

// inherit saMainMenu board from base board
JsBoard.Layouts.saMainMenu.prototype = Object.create(JsBoard.Layouts.base.prototype);

// correct constructor pointer because it points to parent
JsBoard.Layouts.saMainMenu.prototype.constructor = JsBoard.Layouts.saMainMenu;

JsBoard.Layouts.saMainMenu.prototype.createStage = function() {
  vconfig = this.stage; // <--- config.stage
  vcontainer = this.board.container;

  result = new Konva.Stage({
    container: vcontainer,
    width: vconfig.width,
    height: vconfig.height
  });
  return result;

}

// // disini didefinisikan headerHeight untuk mendefinisikan tinggi header.
JsBoard.Layouts.saMainMenu.prototype.initLayout = function() {
  this.parent.prototype.initLayout.call(this);
  var defconfig = {
    headerHeight: 100,
  }
  this.board = JsBoard.Common.extend(defconfig, this.board);
}

JsBoard.Layouts.saMainMenu.prototype.chipDimension = function(vitem) {
  var result = {};

  if (vitem.lokasi == 'header') {
    result = {
      x: 0,
      y: 0,
      width: this.stage.width,
      height: this.board.headerHeight,
    }
  } else if (vitem.lokasi == 'subheader') {
    result = {
      x: 0,
      y: this.board.headerHeight,
      width: this.stage.width,
      height: this.board.subHeaderHeight,
    }
  } else if (vitem.lokasi == 'listbg') {
    result = {
      x: this.stage.width - this.board.listWidth,
      y: this.board.headerHeight + this.board.subHeaderHeight,
      width: this.board.listWidth,
      height: this.stage.height - this.board.subHeaderHeight - this.board.headerHeight - this.board.footerHeight,
    }
  } else if (vitem.lokasi == 'listitem') {
    if (typeof vitem.index == 'undefined') {
      vitem.index = 1;
    }
    result = {
      x: this.stage.width - this.board.listWidth,
      y: this.board.headerHeight + this.board.subHeaderHeight + this.board.chipItemHeight * (vitem.index - 1),
      width: this.board.listWidth,
      height: this.board.chipItemHeight,
    }
  } else if (vitem.lokasi == 'runningtext') {
    result = {
      x: 0,
      y: this.stage.height - this.board.footerHeight - this.board.runningTextHeight,
      width: this.stage.width - this.board.listWidth,
      height: this.board.runningTextHeight,
    }
  } else if (vitem.lokasi == 'footer') {
    result = {
      x: 0,
      y: this.stage.height - this.board.footerHeight,
      width: this.stage.width,
      height: this.board.footerHeight,
    }
  }

  

  return result;
}
