
// copy dari default
JsBoard.Chips.jscb = JsBoard.Common.extend(JsBoard.Chips.jpcbdefault, {});

JsBoard.Chips.jscb.blank = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#777', stroke: '#777', 
        strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
        },
        
    ]    
}

JsBoard.Chips.jscb.header = {
  items: [
    {name: 'base', type: 'rect', x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#bb8',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    {name: 'parentBase', type: 'rect', x: 0, 
        visible: function (base, name, data, me){
            if (data['data']['newDay']){
                return true;
            }
            return false;
        },
        y:{data: 'chip', field: 'titleTop'}, 
        width: function(base, name, data, me){
            return data['chip']['width'] * data['data']['subCount'];
        },
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#eee',
        stroke: '#888',
        margin: {left: 0, right: 0, top: 0, bottom: 1}
    },
    {name: 'parentText', type: 'text', text: {data: 'data', field: 'parentText'}, x: 0, 
        y:{data: 'chip', field: 'titleTop'}, 
        width: function(base, name, data, me){
            return data['chip']['width'] * data['data']['subCount'];
        },
        align: 'center',
        fontSize: 16,
        margin: {left: 5, top: 10, right: 15, bottom: 5}, 
        fill: '#000',
    },
  ]
}

JsBoard.Chips.jscb.stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#fff'
            } else {
                return '#ddd'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    // {name: 'xbase', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, 
    //     xvisible: {data: 'data', field: 'visible'},
    //     height: {data: 'chip', field: 'height'},
    //     xheight: function(base, name, data, me){
    //         return data['chip']['height'] * data['data']['itemCount'];
    //     },
    //     fill: '#ddd',
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, xtext: 'TPS!!',
        x: 0, 
        xxxy: 100, 
        y: function(base, name, data, me){
            var vhtext = data['data']['textCount'] * 60;
            var vcount = data['data']['itemCount']-2;
            var vytext = (data['chip']['height'] *  vcount)/2+vhtext/2;
            return -vytext-50;
        },
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] * data['data']['itemCount'];
        },
        fontSize: 60,
        align: 'center',
        visible: {data: 'data', field: 'visible'},
        margin: {left: 10, top: 5, right: 5, bottom: 5}, 
        fill: '#333',
    },
  ]
}


var asbColorMap = {default: '#fff', block: '#b00', extension: '#ffA520', appoinment: '#fff'}
var asbLineColorMap = {default: '#000', block: '#b00', extension: '#db0', appoinment: '#000'}

JsBoard.Chips.jscb.produksi = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', 
      cornerRadius: 2, strokeWidth: 1, opacity: 0.9,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },

    {name: 'line_1', type: 'rect', y:24, 
        xdelta: 0,
        xdata: 0,
        x: function(base, name, data, me){
            var vleft = 0;
            if (typeof data['data']['Chip1Left']!=='undefined'){
                var vleft = data['data']['Chip1Left'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vleft;
            if (vresult<2){
                base.xdelta = vresult-2;
                vresult = 2;
            }
            this.xdata = vresult;
            return vresult;
        }, 
        width: function(base, name, data, me){
            var vwidth = 1;
            if (typeof data['data']['Chip1Width']!=='undefined'){
                var vwidth = data['data']['Chip1Width'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vwidth + base.xdelta;
            if (typeof data['data']['width']!=='undefined'){
                var vmax = data['data']['width']*vcolwidth;
                if (base.xdata+vresult>vmax-3){
                    var vdelta = base.xdata+vresult - (vmax-3);
                    vresult = vresult-vdelta;
                }
            }
            if (vresult<1){
                vresult = 1;
            }
            return vresult;
        }, 
        visible: {data: 'data', field: 'Chip1Visible'},
        xwidth: 24, 
        height: 40,
        fill: asbProduksiColorMap['line1'], 
        stroke: '#555', strokeWidth:0.225, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },

    {name: 'line_2', type: 'rect', y:24, 
        xdelta: 0,
        xdata: 0,
        x: function(base, name, data, me){
            var vleft = 0;
            if (typeof data['data']['Chip2Left']!=='undefined'){
                var vleft = data['data']['Chip2Left'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vleft;
            if (vresult<2){
                base.xdelta = vresult-2;
                vresult = 2;
            }
            this.xdata = vresult;
            return vresult;
        }, 
        width: function(base, name, data, me){
            var vwidth = 1;
            if (typeof data['data']['Chip2Width']!=='undefined'){
                var vwidth = data['data']['Chip2Width'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vwidth + base.xdelta;
            if (typeof data['data']['width']!=='undefined'){
                var vmax = data['data']['width']*vcolwidth;
                if (base.xdata+vresult>vmax-3){
                    var vdelta = base.xdata+vresult - (vmax-3);
                    vresult = vresult-vdelta;
                }
            }
            if (vresult<1){
                vresult = 1;
            }
            return vresult;
        }, 
        visible: {data: 'data', field: 'Chip2Visible'},
        xwidth: 24, 
        height: 40,
        fill: asbProduksiColorMap['line2'], 
        stroke: '#555', strokeWidth:0.225, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },

    {name: 'line_3', type: 'rect', y:24, 
        xdelta: 0,
        xdata: 0,
        x: function(base, name, data, me){
            var vleft = 0;
            if (typeof data['data']['Chip3Left']!=='undefined'){
                var vleft = data['data']['Chip3Left'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vleft;
            if (vresult<2){
                base.xdelta = vresult-2;
                vresult = 2;
            }
            this.xdata = vresult;
            return vresult;
        }, 
        width: function(base, name, data, me){
            var vwidth = 1;
            if (typeof data['data']['Chip3Width']!=='undefined'){
                var vwidth = data['data']['Chip3Width'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vwidth + base.xdelta;
            if (typeof data['data']['width']!=='undefined'){
                var vmax = data['data']['width']*vcolwidth;
                if (base.xdata+vresult>vmax-3){
                    var vdelta = base.xdata+vresult - (vmax-3);
                    vresult = vresult-vdelta;
                }
            }
            if (vresult<1){
                vresult = 1;
            }
            return vresult;
        }, 
        visible: {data: 'data', field: 'Chip3Visible'},
        xwidth: 24, 
        height: 40,
        fill: asbProduksiColorMap['line3'], 
        stroke: '#555', strokeWidth:0.225, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },


    {name: 'line_4', type: 'rect', y:24, 
        xdelta: 0,
        xdata: 0,
        x: function(base, name, data, me){
            var vleft = 0;
            if (typeof data['data']['Chip4Left']!=='undefined'){
                var vleft = data['data']['Chip4Left'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vleft;
            if (vresult<2){
                base.xdelta = vresult-2;
                vresult = 2;
            }
            this.xdata = vresult;
            return vresult;
        }, 
        width: function(base, name, data, me){
            var vwidth = 1;
            if (typeof data['data']['Chip4Width']!=='undefined'){
                var vwidth = data['data']['Chip4Width'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vwidth + base.xdelta;
            if (typeof data['data']['width']!=='undefined'){
                var vmax = data['data']['width']*vcolwidth;
                if (base.xdata+vresult>vmax-3){
                    var vdelta = base.xdata+vresult - (vmax-3);
                    vresult = vresult-vdelta;
                }
            }
            if (vresult<1){
                vresult = 1;
            }
            return vresult;
        }, 
        visible: {data: 'data', field: 'Chip4Visible'},
        xwidth: 24, 
        height: 40,
        fill: asbProduksiColorMap['line4'], 
        stroke: '#555', strokeWidth:0.225, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },

    {name: 'line_5', type: 'rect', y:24, 
        xdelta: 0,
        xdata: 0,
        x: function(base, name, data, me){
            var vleft = 0;
            if (typeof data['data']['Chip5Left']!=='undefined'){
                var vleft = data['data']['Chip5Left'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vleft;
            if (vresult<2){
                base.xdelta = vresult-2;
                vresult = 2;
            }
            this.xdata = vresult;
            return vresult;
        }, 
        width: function(base, name, data, me){
            var vwidth = 1;
            if (typeof data['data']['Chip5Width']!=='undefined'){
                var vwidth = data['data']['Chip5Width'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vwidth + base.xdelta;
            if (typeof data['data']['width']!=='undefined'){
                var vmax = data['data']['width']*vcolwidth;
                if (base.xdata+vresult>vmax-3){
                    var vdelta = base.xdata+vresult - (vmax-3);
                    vresult = vresult-vdelta;
                }
            }
            if (vresult<1){
                vresult = 1;
            }
            return vresult;
        }, 
        visible: {data: 'data', field: 'Chip5Visible'},
        xwidth: 24, 
        height: 40,
        fill: asbProduksiColorMap['line5'], 
        stroke: '#555', strokeWidth:0.225, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },


    {name: 'line_6', type: 'rect', y:24, 
        xdelta: 0,
        xdata: 0,
        x: function(base, name, data, me){
            var vleft = 0;
            if (typeof data['data']['Chip6Left']!=='undefined'){
                var vleft = data['data']['Chip6Left'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vleft;
            if (vresult<2){
                base.xdelta = vresult-2;
                vresult = 2;
            }
            this.xdata = vresult;
            return vresult;
        }, 
        width: function(base, name, data, me){
            var vwidth = 1;
            if (typeof data['data']['Chip6Width']!=='undefined'){
                var vwidth = data['data']['Chip6Width'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vwidth + base.xdelta;
            if (typeof data['data']['width']!=='undefined'){
                var vmax = data['data']['width']*vcolwidth;
                if (base.xdata+vresult>vmax-3){
                    var vdelta = base.xdata+vresult - (vmax-3);
                    vresult = vresult-vdelta;
                }
            }
            if (vresult<1){
                vresult = 1;
            }
            return vresult;
        }, 
        visible: {data: 'data', field: 'Chip6Visible'},
        xwidth: 24, 
        height: 40,
        fill: asbProduksiColorMap['line6'], 
        stroke: '#555', strokeWidth:0.225, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },

    {name: 'line_7', type: 'rect', y:24, 
        xdelta: 0,
        xdata: 0,
        x: function(base, name, data, me){
            var vleft = 0;
            if (typeof data['data']['Chip7Left']!=='undefined'){
                var vleft = data['data']['Chip7Left'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vleft;
            if (vresult<2){
                base.xdelta = vresult-2;
                vresult = 2;
            }
            this.xdata = vresult;
            return vresult;
        }, 
        width: function(base, name, data, me){
            var vwidth = 1;
            if (typeof data['data']['Chip7Width']!=='undefined'){
                var vwidth = data['data']['Chip7Width'];
            }
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            var vresult = vcolwidth*vwidth + base.xdelta;
            if (typeof data['data']['width']!=='undefined'){
                var vmax = data['data']['width']*vcolwidth;
                if (base.xdata+vresult>vmax-3){
                    var vdelta = base.xdata+vresult - (vmax-3);
                    vresult = vresult-vdelta;
                }
            }
            if (vresult<1){
                vresult = 1;
            }
            return vresult;
        }, 
        visible: {data: 'data', field: 'Chip7Visible'},
        xwidth: 24, 
        height: 40,
        fill: asbProduksiColorMap['line7'],
        stroke: '#555', strokeWidth:0.225, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },


    
    // {name: 'line_2', type: 'rect', x: 24, y:24, 
    //     width: 24, 
    //     height: 40,
    //     fill: asbProduksiColorMap['line2'], 
    //     stroke: '#555', strokeWidth: 1, 
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    
    // {name: 'line_3', type: 'rect', x: 48, y:24, 
    //     width: 24, 
    //     height: 40,
    //     fill: asbProduksiColorMap['line3'], 
    //     stroke: '#555', strokeWidth: 1, 
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    
    // {name: 'line_4', type: 'rect', x: 72, y:24, 
    //     width: 24, 
    //     height: 40,
    //     fill: asbProduksiColorMap['line4'], 
    //     stroke: '#555', strokeWidth: 1, 
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    
    // {name: 'line_5', type: 'rect', x: 96, y:24, 
    //     width: 24, 
    //     height: 40,
    //     fill: asbProduksiColorMap['line5'], 
    //     stroke: '#555', strokeWidth: 1, 
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    
    // {name: 'line_6', type: 'rect', x: 120, y:24, 
    //     width: 24, 
    //     height: 40,
    //     fill: asbProduksiColorMap['line6'], 
    //     stroke: '#555', strokeWidth: 1, 
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    
    // {name: 'line_7', type: 'rect', x: 142, y:24, 
    //     width: 24, 
    //     height: 40,
    //     fill: asbProduksiColorMap['line7'], 
    //     stroke: '#555', strokeWidth: 1, 
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, 
        visible: function(base, name, data, me){
            if (data['data']['selected']){
                return true
            } 
            return false;
        },
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    // {name: 'type', type: 'text', 
    //     text: function(base, name, data, me){
    //         return data['data']['col'];
    //         console.log("col", data['data']['col']);
    //         console.log("col info", me.layout.getInfoColData(data['data']['col']))
    //         if (data['data']['row']<0){
    //             return data['data']['tipe']
    //         } else {
    //             var pad = function(num, size) {
    //                 var s = "000000000" + num;
    //                 return s.substr(s.length-size);
    //             }        
                
    //             var vinfo = me.layout.getInfoColData(data['data']['col']+1);
    //             if (vinfo){
    //                 return pad(vinfo.title, 2)+':00';
    //             }
    //             return '*';
    //             // var vmin = data['chip']['incrementMinute'] * data['data']['col'];
    //             // var vminhour = JsBoard.Common.getMinute(data['chip']['startHour'])+vmin;
    //             // var vres = JsBoard.Common.getTimeString(vminhour);
    //             // return vres;
    //         }
    //     }, 
    //     x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    // {name: 'stallperson', type: 'text', 
    //     text: function(base, name, data, vcont){
    //         console.log("container", vcont);
    //         var vres = '';
    //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
    //             var vrow = data['data']['row'];
    //             var vdata = vcont.layout.getInfoRowData(vrow);
    //             if (vdata && vdata['person']){
    //                 vres = vdata['person'];
    //             }
    //         }
    //         return vres;
    //     },
    //     x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
    //     margin: {right: 14},
    // },
    // {name: 'stallname', type: 'text', 
    //     text: function(base, name, data, vcont){
    //         console.log("container", vcont);
    //         var vres = '';
    //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
    //             var vrow = data['data']['row'];
    //             var vdata = vcont.layout.getInfoRowData(vrow);
    //             if (vdata && vdata['title']){
    //                 vres = vdata['title'];
    //             }
    //         }
    //         return vres;
    //     },
    //     x: 4, y: 54, align: 'left', width: {data: 'chip', field: 'width'},
    // },
    {name: 'tipe_vhc', type: 'text', 
        text: function(base, name, data, vcont){
            if (data['data']['sa_name']){
                return data['data']['tipe'] + ' - '+data['data']['sa_name'];
            }
            return data['data']['tipe'];
            // console.log("container", vcont);
            // var vres = '';
            // if (typeof vcont.layout.getInfoRowData !== 'undefined'){
            //     var vrow = data['data']['row'];
            //     var vdata = vcont.layout.getInfoRowData(vrow);
            //     if (vdata && vdata['title']){
            //         vres = vdata['title'];
            //     }
            // }
            // return vres;
        },
        wrap: 'none',
        x: 4, y: 64, align: 'left', width: {data: 'chip', field: 'width'},
    },
    // {name: 'normal', type: 'rect', x: 0, y:36, 
    //   width: function(base, name, data, me){
    //     console.log('chip-rect-2', data['chip']['width'])
    //     var vext = 0;
    //     var vwidth = 1;
    //     var vcolwidth = 50;
    //     var vnormal = data['data']['width'];
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     if (typeof data['data']['normal']!=='undefined'){
    //       vnormal = data['data']['normal'];
    //     }
    //     return vcolwidth*vnormal;
    //   }, 
    //   height: 21,
    //   fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //   colorMap: asbColorMap,
    //   lineColorMap: asbLineColorMap,
    //   xfill: '#fff',
    //   stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
    //   strokeWidth: 1,
    //   margin: {left: 8, right: 6, top: 0, bottom: 0}
    // },
    // {name: 'icon', type: 'icons', 
    //     x: 4, y: 30, 
    //     icons: {data: 'data', field: 'statusicon', default: []}, 
    //     width: function(base, name, data, me){
    //         console.log('chip-rect-2', data['chip']['width'])
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //         vnormal = data['data']['normal'];
    //         }
    //         return vcolwidth*vnormal;
    //     }, 
    // },
  ]
}



var asbProduksiColorDistance = 100;

// JsBoard.Chips.jscb.footer = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 1, right: 1, top: 0, bottom: 1}
//     },
//     {name: 'rect1', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line1'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text1', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line1'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect2', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line2'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text2', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line2'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//         {name: 'rect3', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line3'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text3', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line3'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect4', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line4'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text4', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line4'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect5', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line5'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text5', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line5'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect6', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line6'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text6', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line6'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect7', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line7'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text7', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line7'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },

//   ]
// }

JsBoard.Chips.jscb.footer = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
        },
    ]
}

var vtemp1 = {name: 'rect1', type: 'rect', x: 0, y:0, 
        width: 50, height: 30,
        fill: asbProduksiColorMap['line1'], 
        stroke: '#000', strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
};
var vtemp2 = {name: 'text1', type: 'text', x: 50, y:0, 
        width: 100, height: 30,
        text: asbProduksiColorNames['line1'], 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
};

for (var i=1; i<8; i++){
    vleft = (i-1)*140;
    vtemp1.name = 'rect'+i;
    vtemp1.fill = asbProduksiColorMap['line'+i];
    vtemp1.x = vleft;
    vtemp2.name = 'text'+i;
    //vtemp2.text = asbProduksiColorNames['line'+i];
    vtemp2.dname = 'line'+i;
    vtemp2.text = function(base, name, data, me){
        if (asbProduksiColorNames[base.dname]){
            return asbProduksiColorNames[base.dname];
        }
        return '';
    }
    vtemp2.x = vleft+50;
    var xtemp1 = JsBoard.Common.extend({}, vtemp1);
    var xtemp2 = JsBoard.Common.extend({}, vtemp2);
    JsBoard.Chips.jscb.footer.items.push(xtemp1);
    JsBoard.Chips.jscb.footer.items.push(xtemp2);
}

JsBoard.Chips.jscb.break = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#555', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'breakText', type: 'text', 
        x: 3, y:{data: 'chip', field: 'height'}, 
        height: {data: 'chip', field: 'width'}, 
        width: {data: 'chip', field: 'height'},
      text: 'B R E A K',
      fontSize: 13,
      rotation: 270,
      align: 'center',
      fill: 'white', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}



// // copy dari default
// JsBoard.Chips.jscb = JsBoard.Common.extend(JsBoard.Chips.jpcbdefault, {});
// var asbColorMap = {default: '#fff', block: '#b00', extension: '#ffA520', appoinment: '#fff'}
// var asbLineColorMap = {default: '#000', block: '#b00', extension: '#db0', appoinment: '#000'}

// JsBoard.Chips.jscb.footer = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 1, right: 1, top: 0, bottom: 1}
//     },
//     {name: 'rect1', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: '#fff', stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text1', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: 'Appointment', 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//   ]
// }

// // JsBoard.Chips.jscb.stall = {
// //   items: [
// //     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
// //         fill: function(base, name, data, me){
// //             if (data['data']['index'] %2 == 0){
// //                 return '#fff'
// //             } else {
// //                 return '#ddd'
// //             }
// //         },
// //         margin: {left: 0, right: 0, top: 0, bottom: 0}
// //     },
// //     {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
// //         margin: {left: 10, top: 5, right: 5, bottom: 5}, 
// //         fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
// //         colorMap: {default: '#000', normal: '#33b', blank: '#aaa', blocked: '#a00'},
// //     },
// //     {name: 'text', type: 'text', text: {data: 'data', field: 'person'}, x: 0, y: 24, width: {data: 'chip', field: 'width'}, fontSize: 14,
// //         margin: {left: 10, top: 5, right: 5, bottom: 5}, 
// //         fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
// //         colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
// //     },
// //   ]
// // }


//     // {name: 'base2', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 300, xheight: {data: 'chip', field: 'height'},
//     //     fill: '#dd0', stroke: '#000',
//     //     margin: {left: 0, right: 0, top: 0, bottom: 0}
//     // },

// // JsBoard.Chips.jscb.header = {
// //   items: [
// //     {name: 'base', type: 'rect', x: 0, 
// //         y: {data: 'chip', field: 'titleTop'}, 
// //         width: {data: 'chip', field: 'width'}, 
// //         height: function(base, name, data, me){
// //             return data['chip']['height'] - data['chip']['titleTop'];
// //         },
// //         fill: '#ccc',
// //         margin: {left: 0, right: 0, top: 0, bottom: 0}
// //     },
// //     {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, 
// //         y: {data: 'chip', field: 'titleTop'}, 
// //         width: {data: 'chip', field: 'width'}, fontSize: 14,
// //         margin: {left: 5, top: 10, right: 5, bottom: 5}, 
// //         fill: '#000', align: 'center',
// //     },
// //   ]
// // }

// JsBoard.Chips.jscb.first_header = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, 
//         y: {data: 'chip', field: 'titleTop'}, 
//         width: {data: 'chip', field: 'width'}, 
//         height: function(base, name, data, me){
//             return data['chip']['height'] - data['chip']['titleTop'];
//         },
//         fill: '#ccc',
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
//     {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, 
//         y: {data: 'chip', field: 'titleTop'}, 
//         width: {data: 'chip', field: 'width'}, fontSize: 14,
//         margin: {left: 15, top: 10, right: 5, bottom: 5}, 
//         fill: '#000',
//     },
//   ]
// }

// JsBoard.Chips.jscb.fix_header = {
//   items: [
//     // {name: 'selectarea', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//     //   fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 2, opacity: 0.5,
//     //   margin: {left: 2, right: 2, top: 2, bottom: 2}
//     // }
//     // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//     //     fill: '#440', opacity: 0.5,
//     //     margin: {left: 0, right: 0, top: 0, bottom: 0}
//     // },
//     // {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
//     //     margin: {left: 5, top: 10, right: 5, bottom: 5}, 
//     //     fill: '#fff', align: 'center',
//     // },
//   ]
// }


// ///// ASB Produksi ///////
// // copy dari default
// // JsBoard.Chips.asb_produksi = JsBoard.Common.extend(JsBoard.Chips.jscb, {});

// JsBoard.Chips.jscb.header = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, 
//         y: {data: 'chip', field: 'subtitleTop'}, 
//         width: {data: 'chip', field: 'width'}, 
//         height: function(base, name, data, me){
//             return data['chip']['height'] - data['chip']['titleTop'];
//         },
//         fill: '#ccc',
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
//     {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, 
//         y: {data: 'chip', field: 'subtitleTop'}, 
//         width: {data: 'chip', field: 'width'}, fontSize: 14,
//         margin: {left: 0, top: 4, right: 0, bottom: 0}, 
//         fill: '#000', align: 'center',
//     },
//     {name: 'parentBase', type: 'rect', x: 0, 
//         visible: {data: 'data', field: 'showParent'}, 
//         y: {data: 'chip', field: 'titleTop'}, 
//         width: function(base, name, data, me){
//             var vwidth = data['chip']['width'] * data['data']['itemCount'];
//             return vwidth;
//         },
//         height: function(base, name, data, me){
//             return data['chip']['subtitleTop'] - data['chip']['titleTop'];
//         },
//         fill: '#ccc', stroke: '#333',strokeWidth: 1,
//         margin: {left: 0, right: 0, top: 0, bottom: 1}
//     },
//     {name: 'parentText', type: 'text', text: {data: 'data', field: 'parentText'}, x: 0, 
//         visible: {data: 'data', field: 'showParent'}, 
//         y: {data: 'chip', field: 'titleTop'}, 
//         width: function(base, name, data, me){
//             var vwidth = data['chip']['width'] * data['data']['itemCount'];
//             return vwidth;
//         },
//         fontSize: 14,
//         margin: {left: 0, top: 4, right: 0, bottom: 0}, 
//         fill: '#000', align: 'center',
//     },
//   ]
// }

// JsBoard.Chips.jscb.stall = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//         fill: function(base, name, data, me){
//             if (data['data']['index'] %2 == 0){
//                 return '#fff'
//             } else {
//                 return '#ddd'
//             }
//         },
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
//     // {name: 'xbase', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, 
//     //     xvisible: {data: 'data', field: 'visible'},
//     //     height: {data: 'chip', field: 'height'},
//     //     xheight: function(base, name, data, me){
//     //         return data['chip']['height'] * data['data']['itemCount'];
//     //     },
//     //     fill: '#ddd',
//     //     margin: {left: 0, right: 0, top: 0, bottom: 0}
//     // },
//     {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, 
//         xxxy: 100, 
//         xxy: 10,
//         y: function(base, name, data, me){
//             return 30 - (data['chip']['height'] * data['data']['itemCount']) + data['chip']['height'];
//         },
//         width: {data: 'chip', field: 'width'}, 
//         height: function(base, name, data, me){
//             return data['chip']['height'] * data['data']['itemCount'];
//         },
//         fontSize: 54,
//         align: 'center',
//         visible: {data: 'data', field: 'visible'},
//         margin: {left: 5, top: 5, right: 5, bottom: 5}, 
//         fill: '#333',
//     },
//   ]
// }

// var asbProduksiColorMap = {default: '#fff', line1: '#b00', line2: '#ddd', line3: '#ff0', line4: '#0b0', line5: '#005', line6: '#b0b', line7: '#431'}
// var asbProduksiColorNames = {default: '', line1: 'Body Repair (1)', line2: 'Putty Sanding (1)', line3: 'Surfacer (1)', line4: 'Spraying (1)', 
//     line5: 'Polish (1)', line6: 'Re-Assy (1)', line7: 'Final Inspection (1)'}
// JsBoard.Chips.jscb.produksi = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', 
//       cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },

//     {name: 'line_1', type: 'rect', x: 2, y:24, 
//         width: 24, 
//         height: 40,
//         fill: asbProduksiColorMap['line1'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_2', type: 'rect', x: 24, y:24, 
//         width: 24, 
//         height: 40,
//         fill: asbProduksiColorMap['line2'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_3', type: 'rect', x: 48, y:24, 
//         width: 24, 
//         height: 40,
//         fill: asbProduksiColorMap['line3'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_4', type: 'rect', x: 72, y:24, 
//         width: 24, 
//         height: 40,
//         fill: asbProduksiColorMap['line4'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_5', type: 'rect', x: 96, y:24, 
//         width: 24, 
//         height: 40,
//         fill: asbProduksiColorMap['line5'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_6', type: 'rect', x: 120, y:24, 
//         width: 24, 
//         height: 40,
//         fill: asbProduksiColorMap['line6'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_7', type: 'rect', x: 142, y:24, 
//         width: 24, 
//         height: 40,
//         fill: asbProduksiColorMap['line7'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//         fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, 
//         visible: function(base, name, data, me){
//             if (data['data']['selected']){
//                 return true
//             } 
//             return false;
//         },
//         margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
//         wrap: 'none', fontStyle: 'bold',
//         fontSize: function(base, name, data, me){
//             if (data['data']['width']>0.5){
//                 return 11
//             } else {
//                 return 10;
//             }
//         }
//     },
//     // {name: 'type', type: 'text', 
//     //     text: function(base, name, data, me){
//     //         return data['data']['col'];
//     //         console.log("col", data['data']['col']);
//     //         console.log("col info", me.layout.getInfoColData(data['data']['col']))
//     //         if (data['data']['row']<0){
//     //             return data['data']['tipe']
//     //         } else {
//     //             var pad = function(num, size) {
//     //                 var s = "000000000" + num;
//     //                 return s.substr(s.length-size);
//     //             }        
                
//     //             var vinfo = me.layout.getInfoColData(data['data']['col']+1);
//     //             if (vinfo){
//     //                 return pad(vinfo.title, 2)+':00';
//     //             }
//     //             return '*';
//     //             // var vmin = data['chip']['incrementMinute'] * data['data']['col'];
//     //             // var vminhour = JsBoard.Common.getMinute(data['chip']['startHour'])+vmin;
//     //             // var vres = JsBoard.Common.getTimeString(vminhour);
//     //             // return vres;
//     //         }
//     //     }, 
//     //     x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
//     // {name: 'stallperson', type: 'text', 
//     //     text: function(base, name, data, vcont){
//     //         console.log("container", vcont);
//     //         var vres = '';
//     //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
//     //             var vrow = data['data']['row'];
//     //             var vdata = vcont.layout.getInfoRowData(vrow);
//     //             if (vdata && vdata['person']){
//     //                 vres = vdata['person'];
//     //             }
//     //         }
//     //         return vres;
//     //     },
//     //     x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
//     //     margin: {right: 14},
//     // },
//     // {name: 'stallname', type: 'text', 
//     //     text: function(base, name, data, vcont){
//     //         console.log("container", vcont);
//     //         var vres = '';
//     //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
//     //             var vrow = data['data']['row'];
//     //             var vdata = vcont.layout.getInfoRowData(vrow);
//     //             if (vdata && vdata['title']){
//     //                 vres = vdata['title'];
//     //             }
//     //         }
//     //         return vres;
//     //     },
//     //     x: 4, y: 54, align: 'left', width: {data: 'chip', field: 'width'},
//     // },
//     {name: 'tipe_vhc', type: 'text', 
//         text: function(base, name, data, vcont){
//             return data['data']['tipe'] + ' - '+data['data']['sa_name']
//             console.log("container", vcont);
//             var vres = '';
//             if (typeof vcont.layout.getInfoRowData !== 'undefined'){
//                 var vrow = data['data']['row'];
//                 var vdata = vcont.layout.getInfoRowData(vrow);
//                 if (vdata && vdata['title']){
//                     vres = vdata['title'];
//                 }
//             }
//             return vres;
//         },
//         x: 4, y: 64, align: 'left', width: {data: 'chip', field: 'width'},
//     },
//     // {name: 'normal', type: 'rect', x: 0, y:36, 
//     //   width: function(base, name, data, me){
//     //     console.log('chip-rect-2', data['chip']['width'])
//     //     var vext = 0;
//     //     var vwidth = 1;
//     //     var vcolwidth = 50;
//     //     var vnormal = data['data']['width'];
//     //     if (typeof data['chip']['columnWidth']!=='undefined'){
//     //         vcolwidth = data['chip']['columnWidth'];
//     //     } 
//     //     if (typeof data['data']['normal']!=='undefined'){
//     //       vnormal = data['data']['normal'];
//     //     }
//     //     return vcolwidth*vnormal;
//     //   }, 
//     //   height: 21,
//     //   fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
//     //   colorMap: asbColorMap,
//     //   lineColorMap: asbLineColorMap,
//     //   xfill: '#fff',
//     //   stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
//     //   strokeWidth: 1,
//     //   margin: {left: 8, right: 6, top: 0, bottom: 0}
//     // },
//     // {name: 'icon', type: 'icons', 
//     //     x: 4, y: 30, 
//     //     icons: {data: 'data', field: 'statusicon', default: []}, 
//     //     width: function(base, name, data, me){
//     //         console.log('chip-rect-2', data['chip']['width'])
//     //         var vext = 0;
//     //         var vwidth = 1;
//     //         var vcolwidth = 50;
//     //         var vnormal = 1;
//     //         if (typeof data['chip']['columnWidth']!=='undefined'){
//     //             vcolwidth = data['chip']['columnWidth'];
//     //         } 
//     //         if (typeof data['data']['normal']!=='undefined'){
//     //         vnormal = data['data']['normal'];
//     //         }
//     //         return vcolwidth*vnormal;
//     //     }, 
//     // },
//   ]
// }

// var xLebarChipBoard = 80;
// var vLebarChipBoard = Math.round(xLebarChipBoard/7*5);
// JsBoard.Chips.jscb.medium1 = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', 
//       cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },

//     {name: 'line_1', type: 'rect', 
//         x: 2, 
//         y:24, 
//         width: vLebarChipBoard-30, 
//         height: 40,
//         fill: asbProduksiColorMap['line1'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_2', type: 'rect', 
//         x: vLebarChipBoard-30, y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line2'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_3', type: 'rect', x: vLebarChipBoard*2-30, y:24, 
//         width: vLebarChipBoard+50, 
//         height: 40,
//         fill: asbProduksiColorMap['line3'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_4', type: 'rect', x: vLebarChipBoard*3+20, y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line4'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_5', type: 'rect', x: vLebarChipBoard*4, y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line5'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_6', type: 'rect', x: vLebarChipBoard*5, y:24, 
//         width: vLebarChipBoard+30, 
//         height: 40,
//         fill: asbProduksiColorMap['line6'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_7', type: 'rect', x: vLebarChipBoard*6+30, y:24, 
//         width: vLebarChipBoard-2-30, 
//         height: 40,
//         fill: asbProduksiColorMap['line7'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//         fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, 
//         visible: function(base, name, data, me){
//             if (data['data']['selected']){
//                 return true
//             } 
//             return false;
//         },
//         margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
//         wrap: 'none', fontStyle: 'bold',
//         fontSize: function(base, name, data, me){
//             if (data['data']['width']>0.5){
//                 return 11
//             } else {
//                 return 10;
//             }
//         }
//     },
//     {name: 'tipe_vhc', type: 'text', 
//         text: function(base, name, data, vcont){
//             return data['data']['tipe'] + ' - '+data['data']['sa_name']
//             console.log("container", vcont);
//             var vres = '';
//             if (typeof vcont.layout.getInfoRowData !== 'undefined'){
//                 var vrow = data['data']['row'];
//                 var vdata = vcont.layout.getInfoRowData(vrow);
//                 if (vdata && vdata['title']){
//                     vres = vdata['title'];
//                 }
//             }
//             return vres;
//         },
//         x: 4, y: 64, align: 'left', width: {data: 'chip', field: 'width'},
//     },
//   ]
// }

// var vLebarChipBoard = xLebarChipBoard/7*5;
// JsBoard.Chips.jscb.medium2 = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', 
//       cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },

//     {name: 'line_1', type: 'rect', 
//         x: 2, 
//         y:24, 
//         width: vLebarChipBoard-20, 
//         height: 40,
//         fill: asbProduksiColorMap['line1'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_2', type: 'rect', 
//         x: vLebarChipBoard-20, y:24, 
//         width: vLebarChipBoard+20, 
//         height: 40,
//         fill: asbProduksiColorMap['line2'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_3', type: 'rect', x: vLebarChipBoard*2, y:24, 
//         width: vLebarChipBoard-25, 
//         height: 40,
//         fill: asbProduksiColorMap['line3'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_4', type: 'rect', x: vLebarChipBoard*3-20, y:24, 
//         width: vLebarChipBoard-25, 
//         height: 40,
//         fill: asbProduksiColorMap['line4'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_5', type: 'rect', x: vLebarChipBoard*4-20, y:24, 
//         width: vLebarChipBoard-20, 
//         height: 40,
//         fill: asbProduksiColorMap['line5'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_6', type: 'rect', x: vLebarChipBoard*5+10, y:24, 
//         width: vLebarChipBoard+20, 
//         height: 40,
//         fill: asbProduksiColorMap['line6'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_7', type: 'rect', x: vLebarChipBoard*6+20, y:24, 
//         width: vLebarChipBoard-2-20, 
//         height: 40,
//         fill: asbProduksiColorMap['line7'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//         fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, 
//         visible: function(base, name, data, me){
//             if (data['data']['selected']){
//                 return true
//             } 
//             return false;
//         },
//         margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
//         wrap: 'none', fontStyle: 'bold',
//         fontSize: function(base, name, data, me){
//             if (data['data']['width']>0.5){
//                 return 11
//             } else {
//                 return 10;
//             }
//         }
//     },
//     {name: 'tipe_vhc', type: 'text', 
//         text: function(base, name, data, vcont){
//             return data['data']['tipe'] + ' - '+data['data']['sa_name']
//             console.log("container", vcont);
//             var vres = '';
//             if (typeof vcont.layout.getInfoRowData !== 'undefined'){
//                 var vrow = data['data']['row'];
//                 var vdata = vcont.layout.getInfoRowData(vrow);
//                 if (vdata && vdata['title']){
//                     vres = vdata['title'];
//                 }
//             }
//             return vres;
//         },
//         x: 4, y: 64, align: 'left', width: {data: 'chip', field: 'width'},
//     },
//   ]
// }

// var vLebarChipBoard = xLebarChipBoard/7*3;
// JsBoard.Chips.jscb.medium3 = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', 
//       cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },

//     {name: 'line_1', type: 'rect', 
//         x: 2, 
//         y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line1'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_2', type: 'rect', 
//         x: vLebarChipBoard, y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line2'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_3', type: 'rect', x: vLebarChipBoard*2, y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line3'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_4', type: 'rect', x: vLebarChipBoard*3, y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line4'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_5', type: 'rect', x: vLebarChipBoard*4, y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line5'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_6', type: 'rect', x: vLebarChipBoard*5, y:24, 
//         width: vLebarChipBoard, 
//         height: 40,
//         fill: asbProduksiColorMap['line6'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'line_7', type: 'rect', x: vLebarChipBoard*6, y:24, 
//         width: vLebarChipBoard-2, 
//         height: 40,
//         fill: asbProduksiColorMap['line7'], 
//         stroke: '#555', strokeWidth: 1, 
//         margin: {left: 0, right: 0, top: 0, bottom: 0}
//     },
    
//     {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//       fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
//       margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
//         fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, 
//         visible: function(base, name, data, me){
//             if (data['data']['selected']){
//                 return true
//             } 
//             return false;
//         },
//         margin: {left: 2, right: 2, top: 2, bottom: 2}
//     },
//     {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
//         wrap: 'none', fontStyle: 'bold',
//         fontSize: function(base, name, data, me){
//             if (data['data']['width']>0.5){
//                 return 11
//             } else {
//                 return 10;
//             }
//         }
//     },
//     {name: 'tipe_vhc', type: 'text', 
//         text: function(base, name, data, vcont){
//             return data['data']['tipe'] + ' - '+data['data']['sa_name']
//             console.log("container", vcont);
//             var vres = '';
//             if (typeof vcont.layout.getInfoRowData !== 'undefined'){
//                 var vrow = data['data']['row'];
//                 var vdata = vcont.layout.getInfoRowData(vrow);
//                 if (vdata && vdata['title']){
//                     vres = vdata['title'];
//                 }
//             }
//             return vres;
//         },
//         x: 4, y: 64, align: 'left', width: {data: 'chip', field: 'width'},
//     },
//   ]
// }


// var asbProduksiColorDistance = 100;

// // JsBoard.Chips.jscb.footer = {
// //   items: [
// //     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
// //       fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
// //       margin: {left: 1, right: 1, top: 0, bottom: 1}
// //     },
// //     {name: 'rect1', type: 'rect', x: 0, y:0, 
// //         width: 50, height: 30,
// //         fill: asbProduksiColorMap['line1'], 
// //         stroke: '#000', strokeWidth: 0.7,
// //         margin: {left: 7, right: 7, top: 7, bottom: 7}
// //     },
// //     {name: 'text1', type: 'text', x: 50, y:0, 
// //         width: 90, height: 30,
// //         text: asbProduksiColorNames['line1'], 
// //         fill: '#000',
// //         wrap: 'none',
// //         margin: {left: 0, right: 0, top: 10, bottom: 0}
// //     },
// //     {name: 'rect2', type: 'rect', x: 0, y:0, 
// //         width: 50, height: 30,
// //         fill: asbProduksiColorMap['line2'], 
// //         stroke: '#000', strokeWidth: 0.7,
// //         margin: {left: 7, right: 7, top: 7, bottom: 7}
// //     },
// //     {name: 'text2', type: 'text', x: 50, y:0, 
// //         width: 90, height: 30,
// //         text: asbProduksiColorNames['line2'], 
// //         fill: '#000',
// //         wrap: 'none',
// //         margin: {left: 0, right: 0, top: 10, bottom: 0}
// //     },
// //         {name: 'rect3', type: 'rect', x: 0, y:0, 
// //         width: 50, height: 30,
// //         fill: asbProduksiColorMap['line3'], 
// //         stroke: '#000', strokeWidth: 0.7,
// //         margin: {left: 7, right: 7, top: 7, bottom: 7}
// //     },
// //     {name: 'text3', type: 'text', x: 50, y:0, 
// //         width: 90, height: 30,
// //         text: asbProduksiColorNames['line3'], 
// //         fill: '#000',
// //         wrap: 'none',
// //         margin: {left: 0, right: 0, top: 10, bottom: 0}
// //     },
// //     {name: 'rect4', type: 'rect', x: 0, y:0, 
// //         width: 50, height: 30,
// //         fill: asbProduksiColorMap['line4'], 
// //         stroke: '#000', strokeWidth: 0.7,
// //         margin: {left: 7, right: 7, top: 7, bottom: 7}
// //     },
// //     {name: 'text4', type: 'text', x: 50, y:0, 
// //         width: 90, height: 30,
// //         text: asbProduksiColorNames['line4'], 
// //         fill: '#000',
// //         wrap: 'none',
// //         margin: {left: 0, right: 0, top: 10, bottom: 0}
// //     },
// //     {name: 'rect5', type: 'rect', x: 0, y:0, 
// //         width: 50, height: 30,
// //         fill: asbProduksiColorMap['line5'], 
// //         stroke: '#000', strokeWidth: 0.7,
// //         margin: {left: 7, right: 7, top: 7, bottom: 7}
// //     },
// //     {name: 'text5', type: 'text', x: 50, y:0, 
// //         width: 90, height: 30,
// //         text: asbProduksiColorNames['line5'], 
// //         fill: '#000',
// //         wrap: 'none',
// //         margin: {left: 0, right: 0, top: 10, bottom: 0}
// //     },
// //     {name: 'rect6', type: 'rect', x: 0, y:0, 
// //         width: 50, height: 30,
// //         fill: asbProduksiColorMap['line6'], 
// //         stroke: '#000', strokeWidth: 0.7,
// //         margin: {left: 7, right: 7, top: 7, bottom: 7}
// //     },
// //     {name: 'text6', type: 'text', x: 50, y:0, 
// //         width: 90, height: 30,
// //         text: asbProduksiColorNames['line6'], 
// //         fill: '#000',
// //         wrap: 'none',
// //         margin: {left: 0, right: 0, top: 10, bottom: 0}
// //     },
// //     {name: 'rect7', type: 'rect', x: 0, y:0, 
// //         width: 50, height: 30,
// //         fill: asbProduksiColorMap['line7'], 
// //         stroke: '#000', strokeWidth: 0.7,
// //         margin: {left: 7, right: 7, top: 7, bottom: 7}
// //     },
// //     {name: 'text7', type: 'text', x: 50, y:0, 
// //         width: 90, height: 30,
// //         text: asbProduksiColorNames['line7'], 
// //         fill: '#000',
// //         wrap: 'none',
// //         margin: {left: 0, right: 0, top: 10, bottom: 0}
// //     },

// //   ]
// // }

// JsBoard.Chips.jscb.footer = {
//     items: [
//         {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
//         fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
//         margin: {left: 1, right: 1, top: 0, bottom: 1}
//         },
//     ]
// }

// var vtemp1 = {name: 'rect1', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line1'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
// };
// var vtemp2 = {name: 'text1', type: 'text', x: 50, y:0, 
//         width: 100, height: 30,
//         text: asbProduksiColorNames['line1'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
// };

// for (var i=1; i<8; i++){
//     vleft = (i-1)*140;
//     vtemp1.name = 'rect'+i;
//     vtemp1.fill = asbProduksiColorMap['line'+i];
//     vtemp1.x = vleft;
//     vtemp2.name = 'text'+i;
//     vtemp2.text = asbProduksiColorNames['line'+i];
//     vtemp2.x = vleft+50;
//     var xtemp1 = JsBoard.Common.extend({}, vtemp1);
//     var xtemp2 = JsBoard.Common.extend({}, vtemp2);
//     JsBoard.Chips.jscb.footer.items.push(xtemp1);
//     JsBoard.Chips.jscb.footer.items.push(xtemp2);
// }
