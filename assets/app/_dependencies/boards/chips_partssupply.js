// chip for iregularity BP

JsBoard.Chips.parts_supply = {}

JsBoard.Common.partColorRange = function(day, hour, dayrange, hourrange){
    for(var i=0; i<dayrange.length; i++){
        drange = dayrange[i];
        if (day<drange.day){
            var vval = drange.color;
            if (vval=='hour_color'){
                for(var j=0; j<hourrange.length; j++){
                    var hrange = hourrange[j];
                    if (hour<hrange.hour){
                        return hrange.color;
                    }
                }
            } else {
                return drange.color;
            }
        }
    }
}

JsBoard.Chips.parts_supply.waiting_pre_picking = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 4, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'norangka', type: 'text', text: {data: 'data', field: 'no_rangka'}, x: 0, y: 18, width: {data: 'chip', field: 'width'}, fontSize: 9,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 44, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 25,
        fill: function(base, name, data, me){
            var vdays = data['data']['dday'];
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            var vhours = Math.round(vmin/60);
            return JsBoard.Common.partColorRange(vdays, vhours, data['data']['dayRange'], data['data']['hourRange']);
        },
        xxfill: '#fff',
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#e00';
            } else {
                return '#fff';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#fff', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 4, top: 4, bottom: 0}
    },
    {name: 'timestatustext', type: 'text', 
        text: function(base, name, data, me){
            if (data['data']['daywait']!='TODAY'){
                return data['data']['daywait'];
            } else {
                return 'H'
                // var vval = data['data']['timewait'];
                // var vmin = JsBoard.Common.getMinute(vval);
                // var vhours = Math.round(vmin/60);
                // if (vhours>0){
                //     return 'J+'+vhours;
                // } else if (vhours<0){
                //     return 'J'+vhours;
                // } else {
                //     return 'J';
                // }
                // return data['data']['timewait'];
            }
        }, 
        width: 48, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 25,
        align: 'center',
        fill: function(base, name, data, me){
            var vdays = data['data']['dday'];
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            var vhours = Math.round(vmin/60);
            return JsBoard.Common.partColorRange(vdays, vhours, data['data']['textDayRange'], data['data']['textHourRange']);
        },
        xfill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['dday'];
            if (vval>=-1){
                return '#fff';
            } else {
                return '#000';
            }
        },
        fontSize: 12, 
        margin: {top: 12}
    },
    {name: 'liner1', type: 'rect', x: 0, y:20, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'tgl_kirim', type: 'text', text: {data: 'data', field: 'docno'}, x: 0, y: 28, width: {data: 'chip', field: 'width'}, fontSize: 9,
        margin: {left: 8}}, 
    {name: 'start', type: 'text', text: {data: 'data', field: 'time_booking'}, x: 0, y: 38, width: {data: 'chip', field: 'width'}, fontSize: 9,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        width: 57, 
        x: function(base, name, data, me){
            // return data['chip']['width'] - data['config']['width']-16
            return data['chip']['width'] - data['config']['width']+10
        }, 
        y:38, 
        height: 30,
        iconWidth: 18, iconHeight: 18,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: function(base, name, data, me){
            if (data['data']['statusname']=='partial'){
                return ['partial_prepick']
            } else {
                return ['no_prepick']
            }
        }
    },

    // {name: 'timestatus', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: '#fff', 
    //     colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'timestatus', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'category'},
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },

    // {name: 'custtype', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['sa_name'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-60-8
    //     }, 
    //     y:52, 
    //     height: 20,
    //     fill: '#fff', 
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'custtypetext', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['cust_type'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-60-8
    //     }, 
    //     y:52, 
    //     height: 20,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'cust_type'},
    //     margin: {left: 10, right: 10, top: 3, bottom: 0}
    // },
    // {name: 'sa_name', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['sa_name'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: 150,
    //     xxx: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-130-8
    //     }, 
    //     y:52, 
    //     height: 24,
    //     fill: 'black', 
    //     align: 'left', fontSize: 14, 
    //     text: {data: 'data', field: 'sa_name'},
    //     wrap: 'none',
    //     margin: {left: 0, right: 0}
    // },
    {name: 'sa_name', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 350, 
        x: 0,
        xxx: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-130-8
        }, 
        y:48, 
        height: 24,
        fill: 'black', 
        align: 'left', fontSize: 9, 
        text: {data: 'data', field: 'sa_name'},
        wrap: 'none',
        margin: {left: 8, right: 0}
    },
  ]
}


JsBoard.Chips.parts_supply.waiting_issuing = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 4, width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 8}},
    {name: 'norangka', type: 'text', text: {data: 'data', field: 'no_rangka'}, x: 0, y: 18, width: {data: 'chip', field: 'width'}, fontSize: 9,
        margin: {left: 8}},
    // {name: 'timestatus', type: 'rect', 
    //     width: 90, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:8, 
    //     height: 42,
    //     fill: function(base, name, data, me){
    //         var vdays = data['data']['dday'];
    //         var vval = data['data']['timewait'];
    //         var vmin = JsBoard.Common.getMinute(vval);
    //         var vhours = Math.round(vmin/60);
    //         return JsBoard.Common.partColorRange(vdays, vhours, data['data']['dayRange'], data['data']['hourRange']);
    //     },
    //     xxfill: '#fff',
    //     xfill: function(base, name, data, me){
    //         //return data['chip']['width'] - data['config']['width']-8
    //         var vval = data['data']['dday'];
    //         if (vval>=-1){
    //             return '#e00';
    //         } else {
    //             return '#fff';
    //         }
    //     },
    //     colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
    //     stroke: '#777', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 0, right: 4, top: 4, bottom: 0}
    // },
    // {name: 'timestatustext', type: 'text', 
    //     text: function(base, name, data, me){
    //         if (data['data']['daywait']!='TODAY'){
    //             return data['data']['daywait'];
    //         } else {
    //             return 'H'
    //             // var vval = data['data']['timewait'];
    //             // var vmin = JsBoard.Common.getMinute(vval);
    //             // var vhours = Math.round(vmin/60);
    //             // if (vhours>0){
    //             //     return 'J+'+vhours;
    //             // } else if (vhours<0){
    //             //     return 'J'+vhours;
    //             // } else {
    //             //     return 'J';
    //             // }
    //             // return data['data']['timewait'];
    //         }
    //     }, 
    //     width: 90, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:8, 
    //     height: 42,
    //     align: 'center',
    //     fill: function(base, name, data, me){
    //         var vdays = data['data']['dday'];
    //         var vval = data['data']['timewait'];
    //         var vmin = JsBoard.Common.getMinute(vval);
    //         var vhours = Math.round(vmin/60);
    //         return JsBoard.Common.partColorRange(vdays, vhours, data['data']['textDayRange'], data['data']['textHourRange']);
    //     },
    //     xfill: function(base, name, data, me){
    //         //return data['chip']['width'] - data['config']['width']-8
    //         var vval = data['data']['dday'];
    //         if (vval>=-1){
    //             return '#fff';
    //         } else {
    //             return '#000';
    //         }
    //     },
    //     fontSize: 20, 
    //     margin: {top: 12}
    // },
    {name: 'liner1', type: 'rect', x: 0, y:20, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'tgl_kirim', type: 'text', text: {data: 'data', field: 'docno'}, x: 0, y: 28, width: {data: 'chip', field: 'width'}, fontSize: 9,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'time_request'}, x: 0, y: 38, width: {data: 'chip', field: 'width'}, fontSize: 9,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        visible: function(base, name, data, me){
            if(typeof data['data']['prepicking'] !== 'undefined'){
                if (data['data']['prepicking']){
                    return 1;
                }
            }
            return 0;
        },
        width: 67, 
        x: function(base, name, data, me){
            // return data['chip']['width'] - data['config']['width']-16
            return data['chip']['width'] - data['config']['width']-30-8
        }, 
        y:42, 
        height: 40,
        iconWidth: 18, iconHeight: 18,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: ['prepicking']
    },

    // {name: 'timestatus', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: '#fff', 
    //     colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'timestatus', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['category'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-8
    //     }, 
    //     y:46, 
    //     height: 44,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'category'},
    //     margin: {left: 10, right: 10, top: 5, bottom: 0}
    // },

    // {name: 'custtype', type: 'rect', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['sa_name'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-60-8
    //     }, 
    //     y:52, 
    //     height: 20,
    //     fill: '#fff', 
    //     stroke: '#333', cornerRadius: 2, strokeWidth: 1,
    //     margin: {left: 10, right: 10, top: 0, bottom: 0}
    // },
    // {name: 'custtypetext', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['cust_type'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 40, 
    //     x: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-60-8
    //     }, 
    //     y:52, 
    //     height: 20,
    //     fill: 'black', 
    //     align: 'center', fontSize: 14, fontStyle: 'bold',
    //     text: {data: 'data', field: 'cust_type'},
    //     margin: {left: 10, right: 10, top: 3, bottom: 0}
    // },
    // {name: 'sa_name', type: 'text', 
    //     visible: function(base, name, data, me){
    //         if(typeof data['data']['sa_name'] !== 'undefined'){
    //             return 1;
    //         }
    //         return 0;
    //     },
    //     width: 60, 
    //     x: 0,
    //     xxx: function(base, name, data, me){
    //         return data['chip']['width'] - data['config']['width']-130-8
    //     }, 
    //     y:82, 
    //     height: 24,
    //     fill: 'black', 
    //     align: 'left', fontSize: 14, 
    //     text: {data: 'data', field: 'sa_name'},
    //     wrap: 'none',
    //     margin: {left: 0, right: 0}
    // },
    {name: 'sa_name', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 350, 
        x: 0,
        xxx: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-130-8
        }, 
        y:48, 
        height: 24,
        fill: 'black', 
        align: 'left', fontSize: 9, 
        text: {data: 'data', field: 'sa_name'},
        wrap: 'none',
        margin: {left: 8, right: 0}
    },
  ]
}

