
// Javascript board UI
// Kusnassriyanto S Bahri
// 1 Jan 2017

// Namespace

var JsBoard = JsBoard || {}

JsBoard.Layouts = {}

JsBoard.Chips = {}

// common functions
JsBoard.Common = {
    minuteDelay: 0,
    // obj1: base; obj2: options
    extend: function(obj1, obj2){
        var obj3 = {};
        for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
        for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
        return obj3;    
    },
    clone: function (obj) {
        var copy;

        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = this.clone(obj[i]);
            }
            return copy;
        }

        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = this.clone(obj[attr]);
            }
            return copy;
        }

        throw new Error("Unable to copy obj! Its type isn't supported.");
    },    
    registerLayout: function(vname, vlayout){
        JsBoard.Layouts[vname] = vlayout;
    },
    registerChips: function(vname, chipdef){
        JsBoard.ChipDefs[vname] = chipdef;
    },
    getMinute: function(strTime){
        if (!strTime){
            return 0;
        }
        var sign = 1;
        if (strTime.charAt(0)=='-'){
            sign = -1;
            strTime = strTime.substr(1);
        }
        var atime = strTime.split(':');
        return sign * ((+atime[0]) * 60 + (+atime[1]));
    },
    getTimeString: function(vminute){
        var pad = function(num, size) {
            var s = "000000000" + num;
            return s.substr(s.length-size);
        }        
        var xsign = '';
        if (vminute<0) {
            xsign = '-';
        }
        vminute = Math.round(Math.abs(vminute));
        var vhour = Math.floor(vminute/60);
        var vmin = vminute % 60;
        vmin = Math.round(vmin);
        return xsign+pad(vhour, 2)+':'+pad(vmin, 2);
    },
    generateDayHours: function(config){
        var defconfig = {
            startHour: '08:00',
            endHour: '16:00',
            increment: 30,
        }

        vconfig = JsBoard.Common.extend(defconfig, config);
        var vminute = this.getMinute(vconfig.startHour);
        var vmax = this.getMinute(vconfig.endHour);
        var vincrement = vconfig.increment;
        var result = []
        while (vminute<=vmax){
            result.push(this.getTimeString(vminute));
            vminute = vminute+vincrement;
        }
        return result;
    },
    localDayName: function(vname){
        var xname = vname.toLowerCase();
        var dayMap = {
            'sunday': 'Minggu',
            'monday': 'Senin',
            'tuesday': 'Selasa',
            'wednesday': 'Rabu',
            'thursday': 'Kamis',
            'friday': 'Jumat',
            'saturday': 'Sabtu',
        }
        return dayMap[xname];
    },
    createLocalTime: function(){
        // untuk mengcreate waktu saat ini sesuai timezone lokal
        // note: karena Date() menghasilkan waktu UTC
        var vdate = new Date();
        vdate.setTime(vdate.getTime()-vdate.getTimezoneOffset()*60000);
        return vdate;
    }
}


// container
JsBoard.Container = function(config){
    // container config
    // var defconfig = {
    //     container: "container",
    //     width: window.innerWidth,
    //     height: window.innerHeight
    // }

    this.objDefault = {
        text: {visible: 1, fontSize: 11, align: 'left', fill: 'black', text: '***', x: 0, y: 0},
        rect: {visible: 1, fontSize: 11, fill: 'black', chipWidth: 1, colorMap: []},
        icons: {visible: 1, fontSize: 11, align: 'left', chipWidth: 1, iconMaps: {}},
    }
    this.marginDefault= {
        left: 5, right: 5, top: 5, bottom: 5
    }
    var defconfig = {
        layout: {},
        board: {iconFolder: 'img', chipGroup: ''},
    }

    this.chipItems = []

    vconfig = {}

    vconfig = JsBoard.Common.extend(defconfig, config);
    vconfig.layout = JsBoard.Common.extend(defconfig.layout, config.layout);
    vconfig.board = JsBoard.Common.extend(defconfig.board, config.board);

    this.listeners = JsBoard.Common.extend({}, config.listeners);

    this.chipGroup = vconfig.board.chipGroup;
    // get container width
    // create konva stage
    
    this.config = vconfig;
    this.layout = false;
    if (typeof JsBoard.Layouts[vconfig.type] !== 'undefined'){
        this.layout = new JsBoard.Layouts[vconfig.type](vconfig);
        this.layout.container = this;
    }

    if (this.layout){
        this.config.board = this.layout.updateConfigBoard(this.config.board);
        this.stage = this.layout.createStage(this.config.board);

        this.board = this.layout.createBoard(this.config.board);

        this.layout.prepareBoard(this.board);

        this.chips = this.board.chips;

        this.layer = new Konva.Layer();

        this.base = new Konva.Group({x: 0, y: 0});
        this.base.add(this.board);
        this.dragGroup = new Konva.Group({x: 0, y: 0});
        this.base.add(this.dragGroup);
        this.layer.add(this.base);
        this.stage.add(this.layer);
        
        // this.stage.setClip({
        //     x: 0, y: 0,
        //     width: this.stage.width(),
        //     height: this.stage.height(),
        // })

        this.layout.updateContainer(this);

    }
    

}

JsBoard.Container.prototype.scrollNext = function(){    
    this.layout.scrollNext(this.board);
}

JsBoard.Container.prototype.scrollPrev = function(){
    this.layout.scrollPrev(this.board);
}

// JsBoard.Container.prototype.setChipEvent = function(vchip, eventname, method){
//     vchip.onclick = method;
//     vchip.on('click', function() {
//         this.onclick(this);
//     });
//     // if (typeof vchip.itemNames !== 'undefined'){
//     //     if (typeof vchip.itemNames['base'] !== 'undefined'){
//     //         var vbase = vchip.itemNames['base'];
//     //         vbase.onclick = method;
//     //         vchip.itemNames['base'].on('click', function() {
//     //             this.onclick(this);
//     //         });
//     //     }
//     // }
// }

JsBoard.Container.prototype.processChips = function(vcheck, vaction, vredraw, vwhich){
    //vwhich: 0 all, 1 only chips, 2 only stopage (-1), 3 only selection chip (-2)
    if (typeof vredraw=='undefined'){
        vredraw = true;
    }
    if (typeof vwhich=='undefined'){
        vwhich = 0;
    }
    if (vwhich==0 || vwhich==1){
        var vchipList = this.chips.children;
        for (var i=0; i<vchipList.length; i++){
            var vchip = vchipList[i];
            var vok = vcheck(vchip);
            if (vok){
                vaction(vchip);
            }
        }
    }
    if (vwhich==0 || vwhich==2){
        var vchipList = this.board.stoppage.children;
        for (var i=0; i<vchipList.length; i++){
            var vchip = vchipList[i];
            var vok = vcheck(vchip);
            if (vok){
                vaction(vchip);
            }
        }
    }
    if (vwhich==0 || vwhich==3){
        var vchipList = this.board.selectbox.children;
        for (var i=0; i<vchipList.length; i++){
            var vchip = vchipList[i];
            var vok = vcheck(vchip);
            if (vok){
                vaction(vchip);
            }
        }
    }
    if (vredraw){
        this.redraw();
    }
}

//function(base, name, data, vdefault)
    // var xrect = {isCreated: true}
    // for (var idx in vconfig){
    //     xrect[idx] = this.getValue(vconfig, idx, vdata);
    // }


JsBoard.shapeMap = {}
//map-->{konva-method: item-data}
    // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
    //   fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
    //   margin: {left: 1, right: 1, top: 2, bottom: 2}
    // },

JsBoard.shapeMap.rect = {visible: 'visible', fill: 'fill', stroke: 'stroke', 
    // x: 'x', y:'y', 
    //width: 'width', height: 'height', // untuk width dan height harus melibatkan margin
    cornerRadius: 'cornerRadius', strokeWidth: 'strokeWidth'
}

JsBoard.shapeMap.text = {visible: 'visible', fill: 'fill', stroke: 'stroke', 
    //x: 'x', y:'y', 
    text: 'text',
}

JsBoard.shapeMap.icons = {
    visible: 'visible',
}
    
JsBoard.Container.prototype.getShapeMap = function(vshape){
    if (vshape.initConfig){
        if (vshape.initConfig.type){
            var vtype = vshape.initConfig.type;
            if (typeof JsBoard.shapeMap[vtype] !== 'undefined'){
                return JsBoard.shapeMap[vtype];
            }
        }
    }
    return false;
}

JsBoard.Container.prototype.updateChip = function(vchip){
    for (var i=0; i<vchip.children.length; i++){
        var vchipitem = {}
        var vitem = vchip.children[i];
        var vconfig = vitem.initConfig;
        var vdata = {}
        vdata['chip'] = JsBoard.Common.extend(this.config.chip, vchip.attrs);
        vdata['data'] = vchip.data;
        vdata['config'] = vconfig;
        for (var idx in vconfig){
            vchipitem[idx] = this.getValue(vconfig, idx, vdata);
        }
        //console.log(vchipitem);
        var vmap = this.getShapeMap(vitem);
        if (vmap){
            for(var idx in vmap){
                if (typeof vchipitem[vmap[idx]] !== 'undefined'){
                    vitem[idx](vchipitem[vmap[idx]]);
                }
            }
        }
    }
}

JsBoard.Container.prototype.draggableChips = function(vcheck){
    var vchipList = this.chips.children;
    for (var i=0; i<vchipList.length; i++){
        var vchip = vchipList[i];
        var vdrag = vcheck(vchip);
        vchip.draggable(vdrag);
    }
    // this.redraw();
}

JsBoard.Container.prototype.blinkChips = function(vcheck, blinkStyle, durationms, blinkcount){
    if (typeof durationms==='undefined'){
        durationms = 300;
    }
    if (typeof blinkcount==='undefined'){
        blinkcount = 12;
    }
    var me = this;
    
    // me.hideBlinkChips();

    // me.showBlinkChips(vcheck, blinkStyle);
    // var code = 0;
    // time_blink = function(){
    //     if (code==1) {
    //         me.showBlinkChips(vcheck, blinkStyle);
    //         code = 0;
    //     } else if (code==0) {
    //         //me.showUnBlinkChips(vcheck, blinkStyle);
    //         me.hideBlinkChips(vcheck);
    //         code = 1;
    //     } else {
    //         // me.showUnBlinkChips(vcheck, blinkStyle);
    //         me.hideBlinkChips(vcheck);
    //     }
    //     if (code!==-1) {
    //         setTimeout(time_blink, durationms);
    //     }
    // }
    // setTimeout(time_blink, durationms);

    // setTimeout(function(){
    //     code = -1;
    // }, durationms*40);

    me.showBlinkChips(vcheck, blinkStyle);
    for(var i=0; i<=blinkcount; i++){
        if ( (i % 2)==0){
            setTimeout(function(){ 
                me.hideBlinkChips(vcheck);
            }, durationms * (i+1));
        } else {
            setTimeout(function(){ 
                me.showBlinkChips(vcheck, blinkStyle);
            }, durationms * (i+1));
        }
    }

    // me.showBlinkChips(vcheck, blinkStyle);
    // setTimeout(function(){ 
    //     me.hideBlinkChips(vcheck);
    // }, durationms);
    // setTimeout(function(){ 
    //     me.showBlinkChips(vcheck, blinkStyle);
    // }, durationms*2);
    // setTimeout(function(){ 
    //     me.hideBlinkChips(vcheck);
    // }, durationms*3);
    // setTimeout(function(){ 
    //     me.showBlinkChips(vcheck, blinkStyle);
    // }, durationms*4);
    // setTimeout(function(){ 
    //     me.hideBlinkChips(vcheck);
    // }, durationms*5);
}

JsBoard.Container.prototype.showBlinkChips = function(vcheck, blinkStyle){
    defstyle = {stroke:'red', strokeWidth: 2, fill: null, opacity: 1}
    var style = JsBoard.Common.extend(defstyle, blinkStyle);
    var vchipList = this.chips.children;
    var vcount = 0;
    for (var i=0; i<vchipList.length; i++){
        var vchip = vchipList[i];
        var vdrag = vcheck(vchip);
        if (vdrag){
            if (typeof vchip.shapes.blink !=='undefined'){
                vchip.shapes.blink.stroke(style.stroke);
                vchip.shapes.blink.strokeWidth(style.strokeWidth);
                vchip.shapes.blink.fill(style.fill);
                vchip.shapes.blink.opacity(style.opacity);
                vchip.shapes.blink.show();
                vcount = vcount+1;
            }
        }
    }
    this.redraw();
}

JsBoard.Container.prototype.hideBlinkChips = function(vcheck){
    if (typeof vcheck === 'undefined'){
        vcheck = function(){return true}
    }
    var vchipList = this.chips.children;
    for (var i=0; i<vchipList.length; i++){
        var vchip = vchipList[i];
        var vdrag = vcheck(vchip);
        if (vdrag){
            if (typeof vchip.shapes.blink !=='undefined'){
                vchip.shapes.blink.hide();
            }
        }
    }
    this.redraw();
}

JsBoard.Container.prototype.setupClickEvent = function(vchip){
    vchip.on('mousedown', function(evt){
        var vTime = new Date();
        this.mouseDownInfo = {time: vTime.getTime()}
    });
    vchip.on('mouseup', function(evt){
        if ((typeof this.mouseDownInfo !== 'undefined') && (typeof this.mouseDownInfo.time !=='undefined') ){
            vNow = new Date();
            vdt = vNow.getTime() - this.mouseDownInfo.time;
            if (vdt<300){
                this.fire('click');
            }
        }
    });
}

JsBoard.Container.prototype.drawChips = function(items){
    var indexArray = {}
    var vitem;
    this.chips.destroyChildren();
    for (var i=0; i<items.length; i++){
        // copy
        vitem = JsBoard.Common.extend(null, items[i]);
        var vlocation = 'default';
        if (items[i].lokasi){
            vlocation = items[i].lokasi;
        }
        if (!indexArray[vlocation]){
            indexArray[vlocation] = 1;
        }
        vitem.index = indexArray[vlocation];
        vitem.dataIndex = i;
        vdim = this.layout.chipDimension(vitem);
        var vchip = this.createChip(vdim, vitem);
        vchip.container = this;
        // console.log('***chip***', vchip);
        // this.setChipEvent(vchip, 'click', function(){
        //     console.log("chip-click");
        //     console.log("this", this);
        // });
        // this.layout.updateByChipDimension(vchip.attrs, vchip);
        this.layout.finalizeChip(vchip);
        this.setupClickEvent(vchip);
        this.chips.add(vchip);
        if ((typeof this.config.board !=='undefined') && (typeof this.config.board.onChipCreated !=='undefined')){
            // console.log("*******chip created ***");
            try {
                this.config.board.onChipCreated(vchip);
            } catch (err){
                console.error(err);
            }

        }
        indexArray[vlocation] = indexArray[vlocation]+1;
    }
    //this.layout.updateBoardDimension(this.stage, this.board);
}

JsBoard.Container.prototype.createChips = function(items, mode){
    if (typeof mode == 'undefined'){
        mode = 'array';
    }
    // if (typeof this.chips !=='undefined'){
    //     this.chips.destroy();
    // }
    
    // this.chips = this.layout.chipContainer(this.board);
    if (mode=='array'){
        this.chipItems = items;
    } else if (mode=='objarray'){
        this.chipItems = []
        for(var idx in items){
            for (var i=0; i<items[idx].length; i++){
                items[idx][i]['group'] = idx;
                this.chipItems.push(items[idx][i]);
            }
        }
    } else {
        this.chipItems = []
    }
    this.layout.prepareBoardLayout(this.board.bg, this.config.board);
    this.drawChips(this.chipItems);
    this.layout.updateContainerLayout(this);
}

JsBoard.Container.prototype.isRedrawingChips = false;
JsBoard.Container.prototype.redrawChips = function(){
    if (this.isRedrawingChips){
        return;
    }
    this.isRedrawingChips = true;
    try {
        this.layout.prepareBoardLayout(this.board.bg, this.config.board);
        this.drawChips(this.chipItems);
        this.layout.updateContainerLayout(this);
        this.isRedrawingChips = false;
    } catch (err){
        this.isRedrawingChips = false;
    }
}

JsBoard.Container.prototype.autoResizeStage = function(){
    //this.layout.initLayout();
    //this.drawChips(this.chipItems);
    this.layout.autoResizeStage(this);
}

JsBoard.Container.prototype.redraw = function(){
    this.stage.draw();
}

JsBoard.Container.prototype.createChip = function(dim, item){
    var defdim = {x: 0, y: 0, width: 50, height: 20, fill: 'white', stroke: 'black', strokeWidth:1, cornerRadius: 2}
    vdim = JsBoard.Common.extend(defdim, dim);
    
    var vgroup = new Konva.Group(vdim);
    vgroup.shapes= {}
    // vdim.x = 0;
    // vdim.y = 0;
    // var vrect = new Konva.Rect(vdim);
    // vgroup.add(vrect);

    // vtpls = this.config.chips.items;
    var vtpls;
    var vlisteners = false;
    var vchipname;
    if (this.chipGroup){
        vchipname = this.chipGroup+'.'+item.chip;
        if (typeof JsBoard.Chips[this.chipGroup] !== 'undefined'){
            if (typeof JsBoard.Chips[this.chipGroup][item.chip] !== 'undefined'){
                vtpls = JsBoard.Chips[this.chipGroup][item.chip].items;
                vlisteners = JsBoard.Chips[this.chipGroup][item.chip].listeners;
            } else {
                console.error('Chip '+vchipname+' tidak diketemukan.');
                return vgroup;
            }
            
        } else {
            console.error('Chip group '+this.chipGroup+' tidak diketemukan.');
            return vgroup;
        }
    } else {
        vchipname = item.chip;
        if (typeof JsBoard.Chips[item.chip]!=='undefined'){
            vtpls = JsBoard.Chips[item.chip].items;
            vlisteners = JsBoard.Chips[item.chip].listeners;
        } else {
            console.error('chip '+vchipname+' tidak diketemukan.');
            return vgroup;
        }        
    }
    
    if((typeof vtpls=='undefined') || (vtpls===null)){
        console.error('Field items dari chip '+vchipname+' tidak diketemukan.');
        return vgroup;
    }
    for (var i=0; i<vtpls.length; i++){
        vtpl = JsBoard.Common.extend(null, vtpls[i]);
        var vitem = false;
        if(vtpl.type=='text'){
            vitem = this.createTextItem(vdim, vtpl, item);
            if (vitem){
                vgroup.add(vitem);
            }
        } else if(vtpl.type=='rect'){
            vitem = this.createRectItem(vdim, vtpl, item);
            if (vitem){
                vgroup.add(vitem);
            }
        } else if(vtpl.type=='icons'){
            vitem = this.createIconsItem(vdim, vtpl, item);
            if (vitem){
                vgroup.add(vitem);
            }
        } else if(vtpl.type=='list'){
            vitem = this.createListItem(vdim, vtpl, item);
            if (vitem){
                vgroup.add(vitem);
            }
        }
        if (vitem){
            vitem.initConfig = vtpls[i];
            if (vtpl.name){
                vgroup.shapes[vtpl.name] = vitem;
            }
            if (vtpl.name=='eta_link'){
                if (vtpl.name=='eta_link'){
                }
            }
            if ((vlisteners !== false)){
                if ((typeof vlisteners !== 'undefined') && (typeof vlisteners[vtpl.name] !== 'undefined')){
                    for (vname in vlisteners[vtpl.name]) {
                        vevent = vlisteners[vtpl.name][vname];
                        vitem.on(vname, vevent);
                    }
                }
            }
        }
    }

    vgroup.data = item;
    return vgroup;
}

JsBoard.Container.prototype.getValue = function(base, name, data, vdefault){
    var result = null;
    try {
        if (typeof vdefault!=='undefined'){
            result = vdefault;
        }
        
        if (typeof vdefault=='undefined'){
            vdefault = '';
        }
        vtype = typeof base[name];
        if (vtype=='function'){
            result = base[name](base, name, data, this);
        } else if (base[name]===null) {
            result = null
        } else if (vtype=='object'){
            var defs = base[name];
            if ((typeof defs['data']!=='undefined') && (typeof defs['field']!=='undefined')){
                if ((typeof defs['type']!=='undefined') && (defs['type']=='map')){
                    result = this.getValueByMap(base, name, data, this);
                } else {
                    if (typeof defs['default'] !== 'undefined'){
                        result = defs['default'];
                    }
                    if (typeof data[defs['data']][defs['field']] !== 'undefined'){
                        result = data[defs['data']][defs['field']];
                    }                    

                }
            } else {
                result = base[name];
            }        
        } else if (vtype=='undefined'){
            result = vdefault;
        } else {
            result = base[name];
        }
    } catch(err){
        console.log("exception---", base, name, data, vdefault);
        console.log(err);
        console.error(err.message);
        result = null;
        //return null;
    }
    if (typeof result=='undefined'){
        console.error('getValue return undefined', base, name, data, vdefault);
        console.error('getValue return undefined');
    }
    return result;
}


//vitem = this.createTextItem(vdim, vtpl, item);
JsBoard.Container.prototype.createTextItem = function(vchip, config, data){
    vcolWidth = this.colWidth;
    //var vchip: {width: this.colWidth}
    
    var defconfig = this.objDefault.text;
    var vconfig = JsBoard.Common.extend(defconfig, config);

    var vdata = {}
    // vdata['chip'] = vchip;
    vdata['chip'] = JsBoard.Common.extend(this.config.chip, vchip);
    vdata['data'] = data;
    vdata['config'] = vconfig;

    var xrect = {isCreated: true}
    for (var idx in vconfig){
        xrect[idx] = this.getValue(vconfig, idx, vdata);
    }

    var xmargin = this.getValue(vconfig, 'margin', vdata);
    var margin = JsBoard.Common.extend(this.marginDefault, xmargin);
    //var margin = JsBoard.Common.extend(this.config.chips.margin, config.margin);

    // proprocess rect
    // clone
    var drect = JsBoard.Common.extend(null, xrect);
    drect.x = xrect.x+margin.left;
    drect.y = xrect.y+margin.top;
    drect.width = drect.width-margin.left-margin.right;
    drect.height = drect.height-margin.top-margin.bottom;
    
    if (drect.isCreated){
        var vtext = new Konva.Text(drect);
        return vtext;
    }

    return false;
}


JsBoard.Container.prototype.createRectItem = function(vchip, config, data){
    vcolWidth = this.colWidth;
    //var vchip: {width: this.colWidth}
    
    var defconfig = this.objDefault.rect;
    var vconfig = JsBoard.Common.extend(defconfig, config);
    // var margin = JsBoard.Common.extend(this.chipConfig.margin, config.margin);

    var vdata = {}
    vdata['chip'] = JsBoard.Common.extend(this.config.chip, vchip);
    vdata['data'] = data;
    vdata['config'] = vconfig;

    // get rect values
    var xrect = {isCreated: true}
    //xrect.x = this.getValue(vconfig, 'x', vdata);
    //xrect.visible = this.getValue(vconfig, 'visible', vdata);
    for (var idx in vconfig){
        xrect[idx] = this.getValue(vconfig, idx, vdata);
    }

    var xmargin = this.getValue(vconfig, 'margin', vdata);
    var margin = JsBoard.Common.extend(this.marginDefault, xmargin);
    //var margin = JsBoard.Common.extend(this.chipConfig.margin, config.margin);


    // proprocess rect
    // clone
    var drect = JsBoard.Common.extend(null, xrect);
    drect.x = xrect.x+margin.left;
    drect.y = xrect.y+margin.top;
    drect.width = drect.width-margin.left-margin.right;
    drect.height = drect.height-margin.top-margin.bottom;
    
    if (drect.isCreated){
        var vrect = new Konva.Rect(drect);
        return vrect;
    }

    return false;

}

JsBoard.Container.prototype.createListItem = function(vchip, config, data){
    vcolWidth = this.colWidth;
    //var vchip: {width: this.colWidth}
    
    var defconfig = this.objDefault.text;
    var vconfig = JsBoard.Common.extend(defconfig, config);

    var vdata = {}
    vdata['chip'] = JsBoard.Common.extend(this.config.chip, vchip);
    vdata['data'] = data;
    vdata['config'] = vconfig;

    var xrect = {isCreated: true}
    for (var idx in vconfig){
        xrect[idx] = this.getValue(vconfig, idx, vdata);
    }

    var xmargin = this.getValue(vconfig, 'margin', vdata);
    var margin = JsBoard.Common.extend(this.marginDefault, xmargin);
    //var margin = JsBoard.Common.extend(this.config.chips.margin, config.margin);

    // proprocess rect
    // clone
    var drect = JsBoard.Common.extend(null, xrect);
    drect.x = xrect.x+margin.left;
    drect.y = xrect.y+margin.top;
    drect.width = drect.width-margin.left-margin.right;
    drect.height = drect.height-margin.top-margin.bottom;
    
    if (drect.isCreated){
        var vlist = this.createList(drect);
        return vlist;
    }

    return false;
}

JsBoard.Container.prototype.createList = function(config){
    var me = this;
    defconfig = {width: 0, items: [], x:0, y:0, width: 40, height: 16, bullet: '*'}
    var vconfig = JsBoard.Common.extend(defconfig, config);

    var vcount = vconfig.items.length;
    var vwidth = vconfig.width;
    var vleft = 0;
    var vtop = 0;
    if (vwidth>0 && vcount>0){
        var vgroup = new Konva.Group({x: vconfig.x, y: vconfig.y});
        var xtext = vconfig;
        for (var i=0; i<vcount; i++){
            xtext.x = vleft;
            xtext.y = vtop;
            xtext.text = vconfig.bullet+' '+vconfig.items[i];
            // var xtext = {
            //     x: vleft, y: vtop, 
            //     width: vwidth, 
            //     height: vconfig.height,
            //     text: vconfig.items[i],
            // }
            // xtext = JsBoard.Common.extend(vconfig, xtext);
            var vtext = new Konva.Text(xtext);
            //var xheight = Math.max(vtext.getHeight, vconfig.height);
            vgroup.add(vtext);
            var xheight = vconfig.height;
            vtop = vtop+xheight;
        }
        vgroup.setHeight(vtop);
        return vgroup;
    }
    return false;



    vcount = vconfig.icons.length;
    if (vwidth>0 && vcount>0){
        var vgroup = new Konva.Group({x: vconfig.x, y: vconfig.y});



        //var vtextx = this.text({x: 5, y: 5, text: 'AAA', fontSize: 11, fill: 'black'});
        //vgroup.add(vtextx);
        //vgroup.testing = "TESTING";
        //return vgroup;
        vicons = vconfig.icons;
        
        defxw = 20;
        xw = Math.min(vwidth/(vcount+1), defxw);
        vleft = 3;
        vtop = 3;
        vw = vconfig.iconWidth;
        vh = vconfig.iconHeight ;
        for (var i=0; i<vcount; i++){
            vimgObj = new Image();
            vimgObj.config = {x: vleft, y: vtop, width: vw, height: vh}
            vimgObj.onload = function() {
                me.drawImage(this, vgroup);
            }
            vimgObj.src = this.config.board.iconFolder+'/'+vicons[i]+'.png';
            // console.log('img src: ', vimgObj.src);
            vleft = vleft+xw;
        }
        return vgroup;
    }
    return false;
}

JsBoard.Container.prototype.createIconsItem = function(vchip, config, data){
    vcolWidth = this.colWidth;
    //var vchip: {width: this.colWidth}
    
    var defconfig = this.objDefault.icons;
    var vconfig = JsBoard.Common.extend(defconfig, config);
    // var margin = JsBoard.Common.extend(this.chipConfig.margin, config.margin);

    var vdata = {}
    vdata['chip'] = JsBoard.Common.extend(this.config.chip, vchip);
    vdata['data'] = data;
    vdata['config'] = vconfig;

    // get rect values
    var xrect = {isCreated: true}
    //xrect.x = this.getValue(vconfig, 'x', vdata);
    //xrect.visible = this.getValue(vconfig, 'visible', vdata);
    for (var idx in vconfig){
        if (idx=='icons'){
            if (idx=='icons'){
            }
        }
        xrect[idx] = this.getValue(vconfig, idx, vdata);
    }

    var xmargin = this.getValue(vconfig, 'margin', vdata);
    var margin = JsBoard.Common.extend(this.marginDefault, xmargin);
    //var margin = JsBoard.Common.extend(this.chipConfig.margin, config.margin);


    // proprocess rect
    // clone
    var drect = JsBoard.Common.extend(null, xrect);
    drect.x = xrect.x+margin.left;
    drect.y = xrect.y+margin.top;
    drect.width = drect.width-margin.left-margin.right;
    drect.height = drect.height-margin.top-margin.bottom;
    if (xrect.icons){
        drect.icons = xrect.icons;
    }
    
    if (drect.isCreated){
        //var vrect = new Konva.Rect(drect);
        var vicons = this.createIcons(drect);

        return vicons;
    }

    return false;

}

JsBoard.Container.prototype.createIcons = function(config){
    var me = this;
    defconfig = {width: 0, icons: [], x:0, y:0, iconWidth: 16, iconHeight: 16, iconGap: 4}
    var vconfig = JsBoard.Common.extend(defconfig, config);

    vwidth = vconfig.width;
    vcount = vconfig.icons.length;
    if (vwidth>0 && vcount>0){
        var vvisible = false;
        if (typeof config.visible=='undefined'){
            vvisible = true;
        } else {
            if (config.visible){
                vvisible = true;
            } else {
                vvisible = false;
            }
        }
        var vgroup = new Konva.Group({x: vconfig.x, y: vconfig.y, visible: vvisible});
        //var vtextx = this.text({x: 5, y: 5, text: 'AAA', fontSize: 11, fill: 'black'});
        //vgroup.add(vtextx);
        //vgroup.testing = "TESTING";
        //return vgroup;
        vicons = vconfig.icons;
        
        defxw = 20;
        xw = Math.min(vwidth/(vcount), defxw);
        vleft = 3;
        vtop = 3;
        vgap = vconfig.iconGap;
        vw = vconfig.iconWidth;
        vh = vconfig.iconHeight ;

        var varFolder = false;
        if (typeof JsBoard.Common.imageList[this.config.board.iconFolder] !== 'undefined'){
            varFolder = true;
        }
        

        for (var i=0; i<vcount; i++){
            var vImage = false;
            var vicon = vicons[i];
            if (varFolder){
                if (typeof JsBoard.Common.imageList[this.config.board.iconFolder][vicon] !== 'undefined'){
                    vImage = JsBoard.Common.imageList[this.config.board.iconFolder][vicon];
                }
            }

            if (vImage){
                var vconfig = {x: vleft, y: vtop, width: vw, height: vh}
                me.drawImage2(vImage, vgroup, vconfig);
                
            } else {
                vimgObj = new Image();
                vimgObj.config = {x: vleft, y: vtop, width: vw, height: vh}
                vimgObj.onload = function() {
                    me.drawImage(this, vgroup);
                }
                vimgObj.src = this.config.board.iconFolder+'/'+vicons[i]+'.png';
                console.log('img src: ', vimgObj.src);
                
            }

            // if (typeof JsBoard.Common.tempImage !== 'undefined'){
            //     var vconfig = {x: vleft, y: vtop, width: vw, height: vh}
            //     me.drawImage2(JsBoard.Common.tempImage, vgroup, vconfig);
            // }
            // vimgObj = new Image();
            // vimgObj.config = {x: vleft, y: vtop, width: vw, height: vh}
            // vimgObj.onload = function() {
            //     me.drawImage(this, vgroup);
            // }
            // vimgObj.src = this.config.board.iconFolder+'/'+vicons[i]+'.png';
            // console.log('img src: ', vimgObj.src);
            vleft = vleft+xw+vgap;
        }

        // for (var i=0; i<vcount; i++){
        //     vimgObj = new Image();
        //     vimgObj.config = {x: vleft, y: vtop, width: vw, height: vh}
        //     vimgObj.onload = function() {
        //         me.drawImage(this, vgroup);
        //     }
        //     vimgObj.src = this.config.board.iconFolder+'/'+vicons[i]+'.png';
        //     console.log('img src: ', vimgObj.src);
        //     vleft = vleft+xw+vgap;
        // }
        return vgroup;
    }
    return false;
}

JsBoard.Container.prototype.drawImage2 = function(imageObj, vgroup, config) {
    console.log('---drawImage2---');
    var vImage = new Konva.Image({
        image: imageObj,
        x: config.x,
        y: config.y,
        width: config.width,
        height: config.height
    });

    vgroup.add(vImage);
    // this.redraw();
}

JsBoard.Container.prototype.drawImage = function(imageObj, vgroup) {
    console.log('---drawImage---');
    var vImage = new Konva.Image({
        image: imageObj,
        x: imageObj.config.x,
        y: imageObj.config.y,
        width: imageObj.config.width,
        height: imageObj.config.height
    });

    vgroup.add(vImage);
    if (vgroup.parent.container.chipGroup != "irregular_gr" && vgroup.parent.container.chipGroup != "irregular_bp"){
        this.redraw();
    }
}

JsBoard.Container.prototype.mapItem = function(maps, item){
    if (typeof maps[item] !== 'undefined'){
        return maps[item];
    } else {
        return item;
    }
}

JsBoard.Container.prototype.getValueByMap = function(base, name, data, vdefault){
    var vobj = base[name];
    var xmap = false;
    var xindex = false;
    if ((vobj['mapdata']) && (vobj['mapfield'])){
        var xdata = vobj['mapdata'];
        var xfield = vobj['mapfield'];
        if ((data[xdata]) && (data[xdata][xfield])){
            var xmap = data[xdata][xfield];
        }
    }
    if ((vobj['data']) && (vobj['field'])){
        var xdata = vobj['data'];
        var xfield = vobj['field'];
        if ((data[xdata]) && (typeof data[xdata][xfield] !== 'undefined')){
            var xindex = data[xdata][xfield];
        }
    }
    if (xindex!==false && xmap!==false){
        if (typeof xmap[xindex]!=='undefined') {
            return xmap[xindex];
        } else {
            if (typeof xmap['default']!=='undefined'){
                return xmap['default'];
            }
        }
    }
    return 'blue';
}




/*




// init new JS Board
JsBoard.Container = function(config){
    //default config
    var defconfig = {
        container: "container",
        width: window.innerWidth,
        height: window.innerHeight,
        chipConfig: {
            default: {
                text: {visible: 1, fontSize: 11, align: 'left', fill: 'black', text: '***', x: 0, y: 0},
                rect: {visible: 1, fontSize: 11, fill: 'black', chipWidth: 1, colorMap: []},
                icons: {visible: 1, fontSize: 11, align: 'left', chipWidth: 1, iconMaps: {}},
            },
            items: [],
            margin: {
                left: 5, right: 5, top: 5, bottom: 5
            }            
        }
    }
    this.colors = ['#fff', '#eee', '#33a', '#3a3', '#ff5', '#d33', '#fd9', '#555'];
    if (typeof config !=='undefined' && typeof config.container!=='undefined'){
        xcontainer = config.container;
    } else {
        xcontainer = defconfig.container;
    }
    var vobj = document.getElementById(xcontainer);
    if(vobj){
        defconfig.width = vobj.clientWidth+1;
    }

    var defchipConfig = defconfig.chipConfig;
    var xchipConfig = config.chipConfig;
    var vchipConfig = JsBoard.Common.extend(defchipConfig, xchipConfig);

    var vconfig = JsBoard.Common.extend(defconfig, config);
    vconfig.chipConfig = vchipConfig;

    this.chipConfig = vconfig.chipConfig;
    //var vwidth = window.innerWidth;
    this.initConfig = vconfig;
    this.stage = new Konva.Stage({
      container: vconfig.container,
      width: vconfig.width,
      height: vconfig.height
    });
    this.layer = new Konva.Layer();
    this.stage.add(this.layer);
    this.boardType = config.type;
    this.cellBased = false;
    if (config.type=='table'){
        this.cellBased = false;
        this.createBoard(config);
    } else if (config.type=='kanban'){
        this.createKanbanBoard(config);
    }

}

JsBoard.Container.prototype.getCol = function(col){
    if (this.cellBased){
        return col;
    } else {
        return 0;
    }
}

JsBoard.Container.prototype.getRow = function(row){
    if (this.cellBased){
        return row;
    } else {
        return 0;
    }
}

JsBoard.Container.prototype.rect = function(config){
    var defconfig = {
      x: 50,
      y: 50,
      width: 100,
      height: 50,
      fill: 'green',
      stroke: 'black',
      strokeWidth: 1
    }
    var vconfig = JsBoard.Common.extend(defconfig, config);
    vconfig.height = this.calcHeight(config);
    var rect = new Konva.Rect(vconfig);
    //this.layer.add(rect);
    //this.stage.draw();
    //this.stage.rect(vconfig);

}

JsBoard.Container.prototype.createBoard = function(config){
    var vfix = this.createFixedCol(config.fixedCol);
    this.fixcol = vfix.fixedCol;
    this.rows = vfix.config.items;
    //this.scrollable = this.createScrollable();
    //this.scrollbox = this.createScrollBox(config);
    //this.scrollbox.add(this.scrollable);
    //this.layer.add(this.scrollbox);
    var xwidth = this.stage.getWidth()-vfix.config.left-2;
    ctconfig = JsBoard.Common.extend(config.content, {width: xwidth});
    this.content = this.createContentBoard(ctconfig, vfix.config);
    this.chips = new Konva.Group({x: 0, y:0});
    this.content.add(this.chips);
    this.layer.add(this.content);
    this.layer.add(this.fixcol);
}

JsBoard.Container.prototype.calcHeight = function(config){
    return 100;
    result = 0;
    vheight = 30;
    if (config.defaultRowHeight){
        vheight = config.defaultRowHeight;
    }
    if (config.fixedCols){
        var vcols = vconfig.fixedCols;
        for(var i = 0; i<vcols.length; i++){
            vcol = vcols[i];
            xheight = 30;
            if(vcol.height){
                xheight = vcol.height;
            }
            result = result+xheight;
        }
    }
}

JsBoard.Container.prototype.createFixedCol = function(config){
    var defconfig = {
        width: 100,
        oddFill: '#fff',
        evenFill: '#eee',
        titleFill: '#ccc',
        minHeight: 50,
        fontSize: 15,
        items: [{title: "Test"}],
        titleCell: {title: "Test", height: 20}
    }
    var vconfig = JsBoard.Common.extend(defconfig, config);
    var vcols = vconfig.items;
    var vcoldef = {title: "<none>", height: 30}
    var vleft = 10;
    var vgap = 10;
    var vtop = vgap;
    var vwidth = vconfig.width-vleft*2;
    var vgroup = new Konva.Group({x: 0, y:0});
    var xrect = {
        x: 1, y: 1, width: vconfig.width, height: 0, 
        //stroke: 'black',
        fill: 'red',
        strokeWidth: 0
    }
    // First Row
    var vcol = JsBoard.Common.extend(vcoldef, vconfig.titleCell);
    vtext = this.text({x: vleft, y: vtop, width: vwidth, text: vcol.title, fontSize: vconfig.fontSize});
    var xheight = Math.max(vtext.getHeight(), vcol.height)
    xrect.y = vtop-vgap+1;
    xrect.height = xheight+vgap*2;
    xrect.fill = vconfig.titleFill;
    var vrect = new Konva.Rect(xrect);
    vcol.rect = JsBoard.Common.extend(null, xrect);
    vgroup.add(vrect);
    vgroup.add(vtext);
    vtop = vtop+xheight+vgap+vgap;
    xconfig = {
        left: xrect.width+1,
        titleRect: JsBoard.Common.extend(null, vcol)
    }
    this.rowMinHeight = vconfig.minHeight+vgap*2;

    for(var i = 0; i<vcols.length; i++){        
        var vcol = JsBoard.Common.extend(vcoldef, vcols[i]);
        vtext = this.text({x: vleft, y: vtop, width: vwidth, text: vcol.title, fontSize: vconfig.fontSize});
        var xheight = Math.max(vtext.getHeight(), vconfig.minHeight)
        xrect.y = vtop-vgap+1;
        xrect.height = xheight+vgap*2;
        if(i % 2 == 0){
            xrect.fill = vconfig.evenFill;
        } else {
            xrect.fill = vconfig.oddFill;
        }
        var vrect = new Konva.Rect(xrect);
        //vcol.rect = {x: xrect.x, y: xrect.y, width: xrect.width, height: xrect.height}
        vcols[i].rect = JsBoard.Common.extend(null, xrect);
        vgroup.add(vrect);
        vgroup.add(vtext);
        vtop = vtop+xheight+vgap+vgap;

    }
    xconfig.items = vcols;
    vGridHeight = vtop-vgap+2;
    this.stage.setHeight(vGridHeight);
    result = {
        gridHeight: vGridHeight,
        fixedCol: vgroup,
        config: xconfig
    }
    return result;
}

JsBoard.Container.prototype.text = function(config){
    var defconfig = {
        x: 0, y: 0,
        fontSize: 18,
        fontFamily: 'Calibri',
        fill: 'green'
    }
    var vconfig = JsBoard.Common.extend(defconfig, config);
    var vtext = new Konva.Text(vconfig);
    return vtext;
}

JsBoard.Container.prototype.createContentBoardx = function(config, fixconfig){
    var defconfig = {
        width: 500,
        colWidth: 120,
        fontSize: 15,
        items: [{title: "Testing"}]
    }
    
    var vconfig = JsBoard.Common.extend(defconfig, config);

}

JsBoard.Container.prototype.createContentBoard = function(config, fixconfig){
    var defconfig = {
        width: 500,
        colWidth: 120,
        fontSize: 15,
        items: [{title: "Testing"}]
    }
    
    var vconfig = JsBoard.Common.extend(defconfig, config);

    this.colWidth = vconfig.colWidth;
    var vgroup = new Konva.Group({x: fixconfig.left, y:0});
    var xrect = fixconfig.titleRect.rect;
    xrect.width = vconfig.width;
    xrect.x = 0;
    // xrect.stroke = 'black';
    // xrect.strokeWidth = 1;
    var vcols = fixconfig.items;
    var vrect = new Konva.Rect(xrect);
    vgroup.add(vrect);
    vmax = 0;
    for(var i=0; i<vcols.length; i++){
        vcol = vcols[i];
        var xrect = vcol.rect;
        xrect.width = vconfig.width;
        xrect.x = 0;
        // xrect.stroke = 'black';
        // xrect.strokeWidth = 1;
        var vrect = new Konva.Rect(xrect);
        vgroup.add(vrect);
        vmax = xrect.y+xrect.height;
    }

    var xline = {
        points: [5, 70, 140, 23],
        stroke: '#888',
        strokeWidth: 1,
        lineCap: 'round',
        lineJoin: 'round'
    }
    var vleft = 0;
    for(var i=0; i<vconfig.items.length; i++){
        var vitem = vconfig.items[i];
        vtext = this.text({x: vleft+10, y: 10, width: vconfig.colWidth, text: vitem.title, fontSize: vconfig.fontSize});        
        xline.points = [vleft, 0, vleft, vmax];
        var vline = new Konva.Line(xline);
        vgroup.add(vline);
        vgroup.add(vtext);
        vleft = vleft+vconfig.colWidth;
    }

    xline.points = [vleft, 0, vleft, vmax];
    var vline = new Konva.Line(xline);
    vgroup.add(vline);
    vleft = vleft+vconfig.colWidth;

    return vgroup;
}

JsBoard.Container.prototype.redraw = function(){
    this.stage.draw();
}

JsBoard.Container.prototype.updateChips = function(chips){
    for(var i=0; i<chips.length; i++){
        var vchip = this.createChip(chips[i]);
        this.chips.add(vchip);
    }
}


JsBoard.Container.prototype.drawImage = function(imageObj, vgroup) {
    var vImage = new Konva.Image({
        image: imageObj,
        x: imageObj.config.x,
        y: imageObj.config.y,
        width: imageObj.config.width,
        height: imageObj.config.height
    });

    vgroup.add(vImage);
    this.redraw();
}

JsBoard.Container.prototype.createIcons = function(config){
    var me = this;
    defconfig = {width: 0, icons: [], x:0, y:0}
    var vconfig = JsBoard.Common.extend(defconfig, config);

    vwidth = vconfig.width;
    vcount = vconfig.icons.length;
    if (vwidth>0 && vcount>0){
        var vgroup = new Konva.Group({x: vconfig.x, y: vconfig.y});
        //var vtextx = this.text({x: 5, y: 5, text: 'AAA', fontSize: 11, fill: 'black'});
        //vgroup.add(vtextx);
        //vgroup.testing = "TESTING";
        //return vgroup;
        vicons = vconfig.icons;
        
        defxw = 20;
        xw = Math.min(vwidth/(vcount+1), defxw);
        vleft = 3;
        vtop = 3;
        vw = 16;
        vh = 16 ;
        for (var i=0; i<vcount; i++){
            vimgObj = new Image();
            vimgObj.config = {x: vleft, y: vtop, width: vw, height: vh}
            vimgObj.onload = function() {
                me.drawImage(this, vgroup);
            }
            vimgObj.src = 'img/'+vicons[i]+'.png';
            console.log('img src: ', vimgObj.src);
            vleft = vleft+xw;
        }
        return vgroup;
    }
    return false;
}


JsBoard.Container.prototype.createChip = function(config){
    var vcolWidth = this.colWidth;
    var vrowHeight = this.rowHeight;

    var defconfig = {row: 1, col: 1, status: 0, width: 1, extWidth: 0, nopol: 'B 9999 XXX', start: '00:00', start2: '', type: '-----', opr: 'XX', teknisi: '***', icons: []}
    var vconfig = JsBoard.Common.extend(defconfig, config);

    if (this.cellBased){
        var vgroup = new Konva.Group({
            x: (vconfig.col-1)*vcolWidth,
            y: this.rows[vconfig.row-1].rect.y,
        });
        var vchip = {
            x: 0,
            y: 0,
            absx: vgroup.x,
            absy: vgroup.y,
            defColWidth: vcolWidth,
            width: (vconfig.width)*vcolWidth,
            height: this.rowMinHeight
        }
    } else {
        var vgroup = new Konva.Group({
            x: 0,
            y: 0,
        });        
        var vchip = {
            x: 0,
            y: 0,
            absx: vgroup.x,
            absy: vgroup.y,
            defColWidth: vcolWidth,
            width: (vconfig.width)*vcolWidth,
            height: vrowHeight
        }
    }


    vtpls = this.chipConfig.items;
    for (var i=0; i<vtpls.length; i++){
        vtpl = JsBoard.Common.extend(null, vtpls[i]);
        if(vtpl.type=='text'){
            vitem = this.createTextItem(vchip, vtpl, vconfig);
            if (vitem){
                vgroup.add(vitem);
            }
        } else if(vtpl.type=='rect'){
            vitem = this.createRectItem(vchip, vtpl, vconfig);
            if (vitem){
                vgroup.add(vitem);
            }
        } else if(vtpl.type=='icons'){
            vitem = this.createIconsItem(vchip, vtpl, vconfig);
            if (vitem){
                vgroup.add(vitem);
            }
        }
    }

    return vgroup;

}

JsBoard.Container.prototype.createTextItem = function(vchip, config, data){
    vcolWidth = this.colWidth;
    //var vchip: {width: this.colWidth}
    
    var defconfig = this.chipConfig.default.text;
    var vconfig = JsBoard.Common.extend(defconfig, config);

    var vdata = {}
    vdata['chip'] = vchip;
    vdata['data'] = data;
    vdata['config'] = vconfig;

    var xrect = {}
    for (var idx in vconfig){
        xrect[idx] = this.getValue(vconfig, idx, vdata);
    }

    var xmargin = this.getValue(vconfig, 'margin', vdata);
    var margin = JsBoard.Common.extend(this.chipConfig.margin, xmargin);
    //var margin = JsBoard.Common.extend(this.chipConfig.margin, config.margin);

    // proprocess rect
    // clone
    var drect = JsBoard.Common.extend(null, xrect);
    drect.x = xrect.x+margin.left;
    drect.y = xrect.y+margin.top;
    drect.width = drect.width-margin.left-margin.right;
    drect.height = drect.height-margin.top-margin.bottom;
    
    if (drect.visible){
        var vtext = new Konva.Text(drect);
        return vtext;
    }

    return false;
}


JsBoard.Container.prototype.getByField = function(defs, data, vdefault){
    return data[defs['data']][defs['field']];
}

JsBoard.Container.prototype.getValueByMap = function(base, name, data, vdefault){
    var vobj = base[name];
    var xmap = false;
    var xindex = '';
    if ((vobj['mapdata']) && (vobj['mapfield'])){
        var xdata = vobj['mapdata'];
        var xfield = vobj['mapfield'];
        if ((data[xdata]) && (data[xdata][xfield])){
            var xmap = data[xdata][xfield];
        }
    }
    if ((vobj['data']) && (vobj['field'])){
        var xdata = vobj['data'];
        var xfield = vobj['field'];
        if ((data[xdata]) && (data[xdata][xfield])){
            var xindex = data[xdata][xfield];
        }
    }
    if (xindex && xmap){
        if (typeof xmap[xindex]!=='undefined') {
            return xmap[xindex];
        } else {
            if (typeof xmap['default']!=='undefined'){
                return xmap['default'];
            }
        }
    }
    return 'blue';
}

JsBoard.Container.prototype.getValue = function(base, name, data, vdefault){
    if (typeof vdefault=='undefined'){
        vdefault = '';
    }
    vtype = typeof base[name];
    if (vtype=='function'){
        return base[name](base, name, data, this);
    } else if (vtype=='object'){
        var defs = base[name];
        if ((typeof defs['data']!=='undefined') && (typeof defs['field']!=='undefined')){
            if ((typeof defs['type']!=='undefined') && (defs['type']=='map')){
                return this.getValueByMap(base, name, data, this);
            } else {
                return data[defs['data']][defs['field']];
            }
        } else {
            return base[name];
        }        
    } else if (vtype=='undefined'){
        return vdefault;
    } else {
        return base[name];
    }
}

JsBoard.Container.prototype.createRectItem = function(vchip, config, data){
    vcolWidth = this.colWidth;
    //var vchip: {width: this.colWidth}
    
    var defconfig = this.chipConfig.default.rect;
    var vconfig = JsBoard.Common.extend(defconfig, config);
    // var margin = JsBoard.Common.extend(this.chipConfig.margin, config.margin);

    var vdata = {}
    vdata['chip'] = vchip;
    vdata['data'] = data;
    vdata['config'] = vconfig;

    // get rect values
    var xrect = {}
    //xrect.x = this.getValue(vconfig, 'x', vdata);
    //xrect.visible = this.getValue(vconfig, 'visible', vdata);
    for (var idx in vconfig){
        xrect[idx] = this.getValue(vconfig, idx, vdata);
    }

    var xmargin = this.getValue(vconfig, 'margin', vdata);
    var margin = JsBoard.Common.extend(this.chipConfig.margin, xmargin);
    //var margin = JsBoard.Common.extend(this.chipConfig.margin, config.margin);


    // proprocess rect
    // clone
    var drect = JsBoard.Common.extend(null, xrect);
    drect.x = xrect.x+margin.left;
    drect.y = xrect.y+margin.top;
    drect.width = drect.width-margin.left-margin.right;
    drect.height = drect.height-margin.top-margin.bottom;
    
    if (drect.visible){
        var vrect = new Konva.Rect(drect);
        return vrect;
    }

    return false;

}

JsBoard.Container.prototype.mapItem = function(maps, item){
    if (typeof maps[item] !== 'undefined'){
        return maps[item];
    } else {
        return item;
    }
}

JsBoard.Container.prototype.createIconsItem = function(vchip, config, data){
    vcolWidth = this.colWidth;
    

    var defconfig = this.chipConfig.default.icons;
    var vconfig = JsBoard.Common.extend(defconfig, config);

    var margin = this.chipConfig.margin;
    if(typeof data['width']!=='undefined'){
        vconfig.chipWidth = data['width']; 
    }
    if(typeof data[vconfig.field]!=='undefined'){
        var vicons = data[vconfig.field];
        vconfig.icons = [];
        for (var i=0; i<vicons.length; i++){
            vconfig.icons.push(this.mapItem(vconfig.iconMaps, vicons[i]));
        }
    } else {
        vconfig.value = [];
    }
    vconfig.y = vconfig.y+margin.top;
    vconfig.x = vconfig.x+margin.left;
    vconfig.width = vconfig.chipWidth*vcolWidth-(margin.left+margin.right)+2;

    var vicons = this.createIcons(vconfig);

    return vicons;
}

JsBoard.Container.prototype.createKanbanBoard = function(config){
    var xwidth = this.stage.getWidth();
    this.colWidth = xwidth;
    this.content = new Konva.Group({x: 0, y:0});
    this.chips = new Konva.Group({x: 0, y:0});
    this.content.add(this.chips);
    this.layer.add(this.content);
}


*/