var saCapacityColorMap = {default: '#ff0000', penyerahan: '#ddffdd', penerimaan: '#ffdd22'}

JsBoard.Chips.sacapacity = JsBoard.Common.clone(JsBoard.Chips.jpcbdefault);

JsBoard.Chips.sacapacity.sachip = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'},
      xfill: '#f00',
      colorMap: saCapacityColorMap,
      stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      opacity: 0.8,
      margin: {left: 1, right: 1, top: 2, bottom: 2}
    },
    // {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
    //   fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
    //   margin: {left: 1, right: 1, top: 2, bottom: 2}
    // },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'type'}, x: 0, y: 15, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    // {name: 'start2', type: 'text', text: {data: 'data', field: 'start2'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}},
    {name: 'jamjanjiserah', type: 'text', text: {data: 'data', field: 'jamjanjiserah'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}, 
        visible: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return true
            } else {
                return false;
            }
        }
    },
    // {name: 'teknisi', type: 'text', text: {data: 'data', field: 'teknisi'}, x: 0, y: 30, align: 'left', 
    //     wrap: 'none',
    //     xwidth: {data: 'chip', field: 'width'},
    //     width: function(base, name, data, me){
    //         return data['chip']['width']-this.x;
    //     }
    // },
    
    {name: 'jamdata', type: 'text', text: {data: 'data', field: 'dispDate'}, x: 0, y: 0, align: 'right', width: {data: 'chip', field: 'width'}, 
        visible: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return true
            } else {
                return false;
            }
        }
    },
    {name: 'tanggaldata', type: 'text', text: {data: 'data', field: 'jam'}, x: 0, y: 16, align: 'right', width: {data: 'chip', field: 'width'}, 
    visible: function(base, name, data, me){
        if (data['data']['width']>0.5){
            return true
        } else {
            return false;
        }
    }
},
{name: 'SAName', type: 'text', text: {data: 'data', field: 'SAName'}, x: 0, y: 30, align: 'right', width: {data: 'chip', field: 'width'}, 
    visible: function(base, name, data, me){
        if (data['data']['width']>0.5){
            return true
        } else {
            return false;
        }
    }
},

// {name: 'pekerjaan', type: 'text', text: {data: 'data', field: 'pekerjaan'}, x: 0, y: 51, width: {data: 'chip', field: 'width'}},
    // {name: 'normal', type: 'rect', x: 0, y:32, 
    //   width: function(base, name, data, me){
    //     console.log('chip-rect-2', data['chip']['width'])
    //     var vext = 0;
    //     var vwidth = 1;
    //     var vcolwidth = 50;
    //     var vnormal = 1;
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     if (typeof data['data']['normal']!=='undefined'){
    //       vnormal = data['data']['normal'];
    //     }
    //     return vcolwidth*vnormal;
    //   }, 
    //   height: 21,
    //   fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //   colorMap: baseColorMap,
    // //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    // //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
    //   xxmargin: {left: 0, right: 0, top: 0, bottom: 0},
    //   margin: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return {right: 0, top: 0, bottom: 0}
    //     } else {
    //       return {top: 0, bottom: 0}
    //     }
    //   }
    // },
    // {name: 'ext', type: 'rect', x: 0, y:32, 
    //   x: function(base, name, data, me){
    //     var vnormal = 0;
    //     var vcolwidth = 50;
    //     if (typeof data['data']['normal']!=='undefined'){
    //       vnormal = data['data']['normal'];
    //     }
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     return vcolwidth*vnormal;
    //   }, 
    //   width: function(base, name, data, me){
    //     var vext = 0;
    //     var vcolwidth = 50;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vnormal = data['data']['ext'];
    //     }
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     return vcolwidth*vnormal;
    //   }, 
    //   visible: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return 1;
    //     } else {
    //       return 0
    //     }
    //   },
    //   height: 21,
    //   fill: '#ffA520', 
    //   margin: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return {left: 0, top: 0, bottom: 0}
    //     } else {
    //       return {top: 0, bottom: 0}
    //     }
    //   }
    // },
    // {name: 'type', type: 'icons', 
    //     x: 0, y: 26, 
    //     icons: {data: 'data', field: 'statusicon', default: []},
    //     width: function(base, name, data, me){
    //         console.log('chip-rect-2', data['chip']['width'])
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //         vnormal = data['data']['normal'];
    //         }
    //         return vcolwidth*vnormal;
    //     }, 
    // },
    // {name: 'type', type: 'icons', 
    //     xxx: 25,
    //     x: function(base, name, data, me){
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //             vnormal = data['data']['normal'];
    //         }
    //         if (typeof data['data']['ext']!=='undefined'){
    //             vext = data['data']['ext'];
    //         }
    //         vwidth = vnormal+vext;
    //         return Math.round(vwidth*vcolwidth/2)-14;
    //     }, 
    //     y: 46, 
    //     icons: ['play2'],
    //     width: 20,
    //     height: 16,
    //     iconHeight: 14,
    //     iconWidth: 14,
    //     visible: function(base, name, data, me){
    //         vallocated = 1;
    //         if (typeof data['data']['allocated']!=='undefined'){
    //             vallocated = data['data']['allocated'];
    //         }
    //         if (vallocated){
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // },
  ]
}

JsBoard.Chips.sacapacity.footer = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'rect1', type: 'rect', x: 0, y:0, 
        width: 50, height: 30,
        fill: saCapacityColorMap['penyerahan'], stroke: '#000', strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 50, y:0, 
        width: 90, height: 30,
        text: 'Penyerahan', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect1', type: 'rect', x: 120, y:0, 
        width: 50, height: 30,
        fill: saCapacityColorMap['penerimaan'], stroke: '#000', strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 170, y:0, 
        width: 120, height: 30,
        text: 'Penerimaan/Estimasi', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
  ]
}

JsBoard.Chips.sacapacity.stall_base = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#fff'
            } else {
                return '#ddd'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    // {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
    //     margin: {left: 5, top: 5, right: 5, bottom: 5}, 
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    // },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.sacapacity.stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#fff'
            } else {
                return '#ddd'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 5, right: 5, bottom: 5}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}


JsBoard.Chips.sacapacity.first_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ccc',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'base2', type: 'rect', x: 0, 
        y: function(base, name, data, me){
            return data['chip']['height']/2
        }, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height']/2
        },
        fill: '#ddd',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 15, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.sacapacity.header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ccc',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'base2', type: 'rect', x: 0, 
        y: function(base, name, data, me){
            return data['chip']['height']/2
        }, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height']/2
        },
        fill: '#ddd',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000', align: 'center',
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'date'}, x: 0, 
        y: function(base, name, data, me){
            return data['chip']['height']/2
        }, 
        height: function(base, name, data, me){
            return data['chip']['height']/2
        },
        width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        align: 'center',
        fill: '#000',
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

