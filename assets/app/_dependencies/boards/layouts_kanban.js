//// Layout for Kanban Board
//// Dependent on layouts.js
JsBoard.Layouts.kanban = function(config){
    this.parent = JsBoard.Layouts.base;
    // call parent's constructor
    this.parent.call(this, config);
    //JsBoard.Layouts.base.call(this);

}

// inherit kanban board from base board
JsBoard.Layouts.kanban.prototype = Object.create(JsBoard.Layouts.base.prototype);

// correct constructor pointer because it points to Person
JsBoard.Layouts.kanban.prototype.constructor = JsBoard.Layouts.kanban;

JsBoard.Layouts.kanban.prototype.createBoard = function(config){
    config.scroll = 'vertical';
    var board = this.parent.prototype.createBoard.call(this, config);
    return board;
}

JsBoard.Layouts.kanban.prototype.prepareBoard = function(vboard){
    var vboard_bg = vboard.bg;
    // console.log("**kanban***vboard_bg", vboard_bg);
    //console.log("**kanban***vconfig", vconfig);
}

JsBoard.Layouts.kanban.prototype.prepareBoardLayout = function(vboard_bg){
    this.parent.prototype.prepareBoardLayout.call(this, vboard_bg);
    // this.chipHeight = vconfig.chipHeight || 100;
    // this.chipGap = vconfig.chipGap || 5;
    // this.chipTop = this.marginTop;
    // this.chipWidth = vconfig.width;
    //this.chip.width = this.stage.width;
    this.chipTop = this.margin.top;
    this.chipWidth = this.chip.width;
    this.chipHeight = this.chip.height;
    this.chipGap = this.chip.gap;

}

JsBoard.Layouts.kanban.prototype.chipDimension = function(vitem){
    // result = {
    //     x: this.marginLeft, 
    //     y: this.chipTop, 
    //     width: this.chipWidth-this.marginTop-this.marginRight, 
    //     height: this.chipHeight
    // }
    var result = {
        x: this.margin.left,
        y: this.chipTop,
        width: this.chipWidth-this.margin.left-this.margin.right,
        height: this.chipHeight
    }
    return result;
}

// update layout by chip dimension after chip has been created and drawn
// JsBoard.Layouts.kanban.prototype.updateByChipDimension = function(dim, vchip){
//     this.chipTop = this.chipTop+dim.height+this.chipGap;
// }

JsBoard.Layouts.kanban.prototype.finalizeChip = function(vchip){
    var dim = vchip.attrs;
    this.chipTop = this.chipTop+dim.height+this.chipGap;
}

JsBoard.Layouts.kanban.prototype.updateContainerLayout = function(vcontainer){
    // var vheight = Math.min(this.maxStageHeight, this.chipTop);
    vxHeight = this.chipTop+this.margin.bottom;
    // var vheight = Math.min(this.stage.maxHeight, vxHeight*this.container.stage.getScale().x+10);
    var vheight = Math.min(this.stage.maxHeight, vxHeight*this.container.stage.getScale().x);
    vheight = Math.max(vheight, this.stage.minHeight)
    this.stageHeight = vheight;
    vxHeight = Math.max(vheight, vxHeight);
    //this.stageHeight = this.stage.maxHeight;
    this.boardHeight = vxHeight;
    this.stageWidth = this.stage.width;
    this.boardWidth = this.chipWidth;
    // vcontainer.board.scrollable.setHeight(vxHeight);
    // vcontainer.board.scrollable.setWidth(this.chipWidth);
    vcontainer.board.scrollable.base.setHeight(vxHeight);
    vcontainer.board.scrollable.base.setWidth(this.chipWidth);
    vcontainer.stage.setHeight(vheight);
    if (vcontainer.config && vcontainer.config.board && vcontainer.config.board.optimizeVisibility){
        var vHeightVisible = 1000;
        if (vcontainer.config.board.heightVisible){
            vHeightVisible = vcontainer.config.board.heightVisible;
        }
        var vchips = vcontainer.chips.children;
        for(var i=0; i<vchips.length; i++){
            var xchip = vchips[i];
            if (xchip.y()>vHeightVisible){
                xchip.hide();
            }
        }
    }
}

JsBoard.Layouts.kanban.prototype.autoResizeStage = function(vjsboard){
    var vcontainer = vjsboard.config.board.container;
    var vobj = document.getElementById(vcontainer);
    if(vobj){
        var vwidth = vobj.clientWidth;
        if (vwidth>0){
            vjsboard.stage.setWidth(vwidth);
            vjsboard.config.board.width = vwidth;
            this.stage.width = vwidth;
            var xscale = vwidth/this.chip.width;
            vjsboard.stage.scaleX(xscale);
            vjsboard.stage.scaleY(xscale);
            vjsboard.board.scrollable.position({x: 0, y:0})
            // var vheight = Math.min(this.boardHeight*xscale*2, this.stage.maxHeight);
            // vjsboard.stage.setHeight(vheight);
            // vjsboard.stage.scale(0.5);
            this.updateContainerLayout(vjsboard);
            // var vheight = Math.min(this.stage.maxHeight, vxHeight*this.container.stage.getScale().x);
            // this.stageHeight = vheight;
            // vjsboard.stage.setHeight(vheight);
        }
    }
}

JsBoard.Layouts.kanban.prototype.isOverlapped = function(v1min, v1max, v2min, v2max){
    var v1n, v1x, v2n, v2x;
    if (v1min>v1max){
        v1n = v1max;
        v1x = v1min;
    } else {
        v1n = v1min;
        v1x = v1max;
    }
    if (v2min>v2max){
        v2n = v2max;
        v2x = v2min;
    } else {
        v2n = v2min;
        v2x = v2max;
    }
    // x1,  x2,  y1,  y2
    // v1n, v1x, v2n, v2x
    return (v1n < v2x) && (v2n < v1x);
}

JsBoard.Layouts.kanban.prototype.updateVisibility = function(vcontainer){
    var vchips = vcontainer.board.chips.children;
    var vscroll = vcontainer.board.scrollable;
    var vstage = vcontainer.stage;
    var vheight = vstage.height();
    var v1min = 0-vscroll.y();
    var v1max = v1min+vheight;
    console.log('vcontainer', vcontainer);
    for (var i=0; i<vchips.length; i++){
        var xitem = vchips[i];
        var v2min = xitem.y();
        var v2max = v2min+xitem.height();
        console.log('v1min', v1min)
        console.log('v1max', v1max)
        console.log('v2min', v2min)
        console.log('v2max', v2max)

        if(vcontainer.chipGroup == 'parts_order'){
        //ini di komen 19/11/2018 soalnya di irregular board scrolling nya jadi aneh kalo ada ini. kalau menu lain ada ngaco buka lg aja
            if (this.isOverlapped(v1min, v1max, v2min, v2max)){
                xitem.show();
            } else {
                xitem.hide();
            }
        }
        else{
            
        }
        
    }

}

JsBoard.Layouts.kanban.prototype.afterScrollUp = function(obj, evt, vcount){
    console.log('kanban after scroll up', obj);
    if (obj.board.chipGroup != "parts_supply"){
        this.updateVisibility(obj.container);
    }
}

JsBoard.Layouts.kanban.prototype.afterScrollDown = function(obj, evt, vcount){
    console.log('kanban after scroll down', obj);
    // this.updateVisibility(obj.container);
    if (obj.board.chipGroup != "parts_supply"){
        this.updateVisibility(obj.container);
    }
}

JsBoard.Layouts.kanban.prototype.afterScrollDrag = function(obj, evt){
    console.log('kanban after scroll drag', obj);
    // this.updateVisibility(obj.container);
    if (obj.board.chipGroup != "parts_supply"){
        this.updateVisibility(obj.container);
    }
    
}