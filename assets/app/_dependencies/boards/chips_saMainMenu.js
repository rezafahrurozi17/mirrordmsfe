JsBoard.Chips.saMainMenu = {}

JsBoard.Chips.saMainMenu.header = { //<--- status adalah nama group chip. header adalah nama chip
  items: [{
      name: 'base',
      type: 'rect',
      x: 0,
      y: 0,
      width: {
        data: 'chip',
        field: 'width'
      },
      height: {
        data: 'chip',
        field: 'height'
      },
      fill: 'red',
      stroke: '#777',
      cornerRadius: 2,
      strokeWidth: 1,
      margin: {
        left: 2,
        right: 2,
        top: 2,
        bottom: 2
      }
    },
    // teks judul
    {
      name: 'title',
      type: 'text',
      x: 0,
      y: 0,
      text: {
        data: 'data',
        field: 'title'
      },
      width: {
        data: 'chip',
        field: 'width'
      },
      height: {
        data: 'chip',
        field: 'height'
      },
      fill: 'green',
      fontSize: 28,
      margin: {
        left: 100,
        right: 100,
        top: 8,
        bottom: 0
      },
      align: 'center',
    },
    {
      name: 'jam',
      type: 'text',
      width: 120,
      x: function(base, name, data, me) {
        return data['chip']['width'] - data['config']['width']
      },
      y: 0,
      text: {
        data: 'data',
        field: 'jam'
      },
      height: {
        data: 'chip',
        field: 'height'
      },
      fill: '#000',
      fontSize: 24,
      align: 'center',
    },
    // tanggal
    {
      name: 'tanggal',
      type: 'text',
      width: 120,
      height: 24,
      x: function(base, name, data, me) {
        return data['chip']['width'] - data['config']['width']
      },
      y: function(base, name, data, me) {
        return data['chip']['height'] - data['config']['height']
      },
      text: {
        data: 'data',
        field: 'tanggal'
      },
      fill: '#000',
      fontSize: 12,
      align: 'center',
    },
  ]
}

/*JsBoard.Chips.saMainMenu.test = { //<--- status adalah nama group chip. header adalah nama chip
  items: [{
    name: 'base',
    type: 'rect',
    x: 0,
    y: 100,
    width: {
      data: 'chip',
      field: 'width'
    },
    height: {
      data: 'chip',
      field: 'height'
    },
    fill: 'blue',
    stroke: '#777',
    cornerRadius: 2,
    strokeWidth: 1,
    margin: {
      left: 2,
      right: 2,
      top: 2,
      bottom: 2
    }
  }]
}*/
