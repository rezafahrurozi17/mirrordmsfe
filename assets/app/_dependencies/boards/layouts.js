
// Javascript board UI
// Layout Implementation
// dependent on jboard.js
// Kusnassriyanto S Bahri
// 13 Jan 2017

JsBoard.Layouts = JsBoard.Layouts || {}


JsBoard.Layouts.base = function(layout_config){
    this.container = null;
    var defconfig = {
    }
    var vconfig = JsBoard.Common.extend(defconfig, layout_config);
    this.layoutConfig = vconfig;
    this.initLayout();
}

JsBoard.Layouts.base.prototype.initLayout = function(){
    vconfig = this.layoutConfig;
    // this.chipWidth = vconfig.chipWidth || 100;
    // this.chipHeight = vconfig.chipHeight || 100;
    // this.maxStageHeight = vconfig.maxStageHeight || 800;
    // this.minStageHeight = vconfig.minStageHeight || 200;
    // this.marginLeft = vconfig.marginLeft || 5;
    // this.marginRight = vconfig.marginRight || 5;
    // this.marginTop = vconfig.marginTop || 5;
    // this.marginBottom = vconfig.marginBottom || 5;
    
    //margin
    var defmargin = {
        left: 5, top: 5, right: 5, bottom: 5
    }
    this.margin = JsBoard.Common.extend(defmargin, vconfig.margin);

    // chip
    defchip = {
        width: 200,
        height: 100,
        gap: 5
    }
    this.chip = JsBoard.Common.extend(defchip, vconfig.chip);

    // stage
    defstage = {
        container: "container",
        maxHeight: 800,
        minHeight: 200,
        height: 100,
    }
    this.stage = JsBoard.Common.extend(defstage, vconfig.stage);

    // definisi board yang bersifat umum
    defboard = {
        container: "container",
        iconFolder: 'images',
    }
    this.board = JsBoard.Common.extend(defboard, vconfig.board);

}

JsBoard.Layouts.base.prototype.updateConfigBoard = function(vconfig_board){
    return vconfig_board;
    // var defconfig = {
    //     container: "container",
    //     width: window.innerWidth,
    //     height: this.minStageHeight
    // }
    // var config = vconfig_board;
    // var vconfig = JsBoard.Common.extend(defconfig, config);
    // return vconfig;
}

JsBoard.Layouts.base.prototype.createStage = function(){
    vconfig = this.stage;
    vcontainer = this.board.container;
    var vobj = document.getElementById(vcontainer);
    if(vobj){
        vconfig.width = vobj.clientWidth;
        this.stage.width = vconfig.width;
    }
    this.stageHeight = vconfig.height;
    this.boardHeight = vconfig.height;

    this.stageWidth = vconfig.width;
    this.boardWidth = vconfig.width;
    
    //this.chipWidth = vconfig.width;

    result = new Konva.Stage({
      container: vcontainer,
      width: vconfig.width,
      height: vconfig.height
    });
    return result;

}

JsBoard.Layouts.base.prototype.createBoard = function(config){
    // console.log("***config***", config);
    var defconfig = {
        scroll: 'none'
    }
    var vconfig = JsBoard.Common.extend(defconfig, config);

    //var vconfig = vconfig_board;
    // scrollable didefinisikan di sini!
    var chips = new Konva.Group({x: 0, y:0});
    var scrollable = new Konva.Group({x: 0, y:0});
    var scrollable_base = new Konva.Rect({x: 0, y:0, width: 100, height: 100});
    scrollable.add(scrollable_base);
    scrollable.base = scrollable_base;
    var board = new Konva.Group({x: 0, y:0});
    var board_bg = new Konva.Group({x: 0, y:0});
    var board_bg2 = new Konva.Group({x: 0, y:0});

    board.scrollable = scrollable;
    this.scrollType = vconfig.scroll;
    if (vconfig.scroll == 'horizontal'){
        scrollable.dragBoundFunc(function(pos){

            // var vmax = me.boardHeight-me.stageHeight/me.container.stage.getScale().y;
            var vmax = (me.board.contentWidth-me.stageWidth/me.container.stage.getScale().x);
            if (vmax>=0){
                var newY = this.getAbsolutePosition().y;
                var newX = pos.x+vmax < 0 ? -vmax : pos.x;
                newX = pos.x+vmax > vmax ? 0 : newX;
                return {
                    x: newX,
                    y: newY
                };
            } else {
                return {
                    x: this.getAbsolutePosition().x,
                    y: this.getAbsolutePosition().y
                };
            }


            // return {
            //     x: pos.x,
            //     y: this.getAbsolutePosition().y
            // }
        });
        scrollable.draggable(true);
    } else if (vconfig.scroll == 'vertical'){
        var me = this;
        scrollable.dragBoundFunc(function(pos){
            var vmax = me.boardHeight-me.stageHeight/me.container.stage.getScale().y;
            if (vmax>=0){
                var vy = this.getAbsolutePosition().y;
                var newX = this.getAbsolutePosition().x;
                var newY = pos.y+vmax < 0 ? -vmax : pos.y;
                newY = pos.y+vmax > vmax ? 0 : newY;
                return {
                    x: newX,
                    y: newY
                };
            } else {
                return {
                    x: this.getAbsolutePosition().x,
                    y: this.getAbsolutePosition().y
                };
            }
        });
        scrollable.draggable(true);
    }

    var me = this;
    //scrollable.preventDefault(true);
    scrollable.on('wheel', function(evt){
            if (evt.evt.wheelDelta>0){
                vwheelup = 1;
                console.log('wheelup');
                me.scrollUp(evt.evt);
            } else if (evt.evt.wheelDelta<0){
                console.log('wheeldown');
                me.scrollDown(evt.evt);
            }
        // if (me.boardHeight>me.stageHeight/me.container.stage){
        //     evt.evt.preventDefault();
        //     var vwheelup = 0;
        //     if (evt.evt.wheelDelta>0){
        //         vwheelup = 1;
        //         console.log('wheelup');
        //         me.scrollUp();
        //     } else {
        //         console.log('wheeldown');
        //         me.scrollDown();
        //     }
        // }
        // console.log('wheel', evt);
    })

    scrollable.on('dragend', function(evt){
        console.log('scrollable dragend');
        
        if (typeof me.afterScrollDrag === 'function'){            
            me.afterScrollDrag(me, evt);
            me.container.redraw();
        }
    })

    // var vtext = new Konva.Text({x: 5, y: 5, text: 'AAA', fontSize: 11, fill: 'black'});
    // board.add(vtext);

    board.bg = board_bg;
    board.bg2 = board_bg2;
    // this.prepareBoard(board);
    scrollable.add(board_bg);
    scrollable.add(board_bg2);
    scrollable.add(chips);
    board.add(scrollable);
    
    board.chips = chips;
    board.scrollable = scrollable;
    // this.prepareBoard(board_bg);
    // board.add(board_bg);
    // board.bg = board_bg;
    // board.add(chips);
    // board.chips = chips;
    return board;
}

JsBoard.Layouts.base.prototype.scrollUp = function(evt, vcount){
    if (typeof vcount =='undefined'){
        vcount = 150;
    }
    console.log("scrollup event", evt);
    var vscrollable = this.container.board.scrollable;
    var me = this;
    if (this.scrollType=='vertical' && !evt.shiftKey){
        var vmax = (me.boardHeight-me.stageHeight/me.container.stage.getScale().y);
        vpos = vscrollable.y()+vcount
        var newY = vpos+vmax < 0 ? -vmax : vpos;
        newY = vpos+vmax > vmax ? 0 : newY;
        if (newY!==vscrollable.y()){
            evt.preventDefault();
        }
        vscrollable.y(newY);
        if (typeof me.afterScrollUp=='function'){
            me.afterScrollUp(me, evt, vcount);
        }
        this.container.redraw();
    } else if (this.scrollType=='horizontal' && evt.shiftKey){        
        var vmax = (me.board.contentWidth-me.stageWidth/me.container.stage.getScale().x);
        vpos = vscrollable.x()+vcount;
        var newX = vpos+vmax < 0 ? -vmax : vpos;
        newX = vpos+vmax > vmax ? 0 : newX;
        //if (newX!==vscrollable.x()){
            evt.preventDefault();
        //}
        vscrollable.x(newX);
        if (typeof me.afterScrollUp=='function'){
            me.afterScrollUp(me, evt, vcount);
        }
        this.container.redraw();
    }
    // if (typeof vcount === 'undefined'){
    //     this.container.scroller.x
    // }
}

JsBoard.Layouts.base.prototype.scrollDown = function(evt, vcount){
    if (typeof vcount =='undefined'){
        vcount = 150;
    }
    var me = this;
    var vscrollable = this.container.board.scrollable;
    if (this.scrollType=='vertical' && !evt.shiftKey){
        var vmax = (me.boardHeight-me.stageHeight/me.container.stage.getScale().y);
        vpos = vscrollable.y()-vcount;
        var newY = vpos+vmax < 0 ? -vmax : vpos;
        newY = vpos+vmax > vmax ? 0 : newY;
        if (newY!==vscrollable.y()){
            evt.preventDefault();
        }
        if (newY>0){
            newY = 0;
        }
        vscrollable.y(newY);
        if (typeof me.afterScrollDown=='function'){
            me.afterScrollDown(me, evt, vcount);
        }
        this.container.redraw();
    } else if (this.scrollType=='horizontal'&& evt.shiftKey){
        var vmax = (me.board.contentWidth-me.stageWidth/me.container.stage.getScale().x);
        // vmax = Math.max(0, vmax);
        vpos = vscrollable.x()-vcount;
        var newX = vpos+vmax < 0 ? -vmax : vpos;
        newX = vpos+vmax > vmax ? 0 : newX;
        //if (newX!==vscrollable.x()){
            evt.preventDefault();
        // }
        if (newX>0){
            newX = 0;
        }
        vscrollable.x(newX);
        if (typeof me.afterScrollDown=='function'){
            me.afterScrollDown(me, evt, vcount);
        }
        this.container.redraw();
    }
    // if (typeof vcount === 'undefined'){
    //     this.container.scroller.x
    // }
}

JsBoard.Layouts.base.prototype.mapChipListeners = function(vchip, vlisteners){
    for (var vidx in vlisteners){
        // console.log(vidx, vlisteners[vidx]);
        //vchip.listeners.vidx = vlisteners[vidx];
        vchip.on(vidx, vlisteners[vidx]);
        vchip.setListening(true);
    }
}

// menyiapkan tampilan dari board (background)
// method ini dipanggil sekali, pada saat board di-create
JsBoard.Layouts.base.prototype.prepareBoard = function(vboard){
    // console.log("***vboard_bg", vboard_bg);
    // console.log("***vconfig", vconfig);
}

// menyiapkan tampilan layout dari board (background)
// method ini akan dipanggil setiap kali drawing/redrawing
JsBoard.Layouts.base.prototype.prepareBoardLayout = function(vboard_bg){
    // this.marginLeft = vconfig.marginLeft || 5;
    // this.marginRight = vconfig.marginRight || 5;
    // this.marginTop = vconfig.marginTop || 5;
    // this.marginBottom = vconfig.marginBottom || 5;
}

// method ini digunakan untuk mengupdate board, bila diperlukan.
// dan dipanggil di setelah layout di-create
JsBoard.Layouts.base.prototype.updateContainer = function(vcontainer){
}

// method ini digunakan untuk mengupdate board, bila diperlukan.
// dan dipanggil setelah board di-redraw tapi sebelum stage di-redraw
// update yang perlu dilakukan terhadap objek di level board/container
// dilakukan di sini. Misalnya mengubah ukuran stage sesuai dengan ukuran 
// item-item hasil proses di layout.
JsBoard.Layouts.base.prototype.updateContainerLayout = function(vcontainer){
}

// default chip dimension
JsBoard.Layouts.base.prototype.chipDimension = function(vitem){
    result = {
        x: 0,
        y: 0,
        width: 100,
        height: 100
    }
    return result;
}

// update layout by chip dimension after chip has been created and drawn
JsBoard.Layouts.base.prototype.updateByChipDimension = function(dim, vchip){

}

JsBoard.Layouts.base.prototype.finalizeChip = function(vchip){

}

JsBoard.Layouts.base.prototype.autoResizeStage = function(vjsboard){
    var vcontainer = vjsboard.config.board.container;
    var vobj = document.getElementById(vcontainer);
    if(vobj){
        var vwidth = vobj.clientWidth;
        vjsboard.stage.setWidth(vwidth);
        vjsboard.config.board.width = vwidth;
        this.stage.width = vwidth;
    }    
}

JsBoard.Layouts.base.prototype.moveChipsToSpecificArea = function(){
    //
}





