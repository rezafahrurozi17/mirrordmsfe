// chip for iregularity GR

JsBoard.Chips.irregular_gr = {}

JsBoard.Chips.irregular_gr.waiting_reception = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 8, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            if (vmin>10){
                return '#e00';
            } else if (vmin>5){
                return '#ec0';
            } else {
                return '#0e0';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'timewait', type: 'text', text: {data: 'data', field: 'timewait'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        align: 'center',
        fill: '#fff',
        fontSize: 18,
        margin: {top: 7}
    },
    {name: 'liner1', type: 'rect', x: 0, y:35, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'start', type: 'text', text: {data: 'data', field: 'start'}, x: 0, y: 40, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-16
        }, 
        y:39, 
        height: 40,
        iconWidth: 32, iconHeight: 32,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: {data: 'data', field: 'statusicon', default: []}
    },

    {name: 'timestatus', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['category'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:46, 
        height: 24,
        fill: '#fff', 
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'timestatus', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['category'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:46, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14, fontStyle: 'bold',
        text: {data: 'data', field: 'category'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },

  ]
}

JsBoard.Chips.irregular_gr.waiting_production = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 8, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['timestart_minus'];
            if (vval.includes('-')){
                vval = vval.replace('-','')
                var vmin = JsBoard.Common.getMinute(vval);
                if (vmin<=10){
                    return '#e00'; // merah
                } else {
                    return '#fff'; // putih
                }
            } else {
                return '#e00'; // merah
            }
          
        },
        colorMap: {default: '#f0f0f0', 1: '#e00', 0: '#fff'},
        stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'timewait', type: 'text', text: {data: 'data', field: 'timestart'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        align: 'center',
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['timestart_minus'];
            if (vval.includes('-')){
                vval = vval.replace('-','')
                var vmin = JsBoard.Common.getMinute(vval);
                if (vmin<=10){
                    return '#fff'; // putih
                } else {
                    return '#000'; // hitam
                }
            } else {
                return '#fff'; // putih
            }
            
        },
        xfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'timestatus'}, 
        colorMap: {default: '#f0f0f0', 1: '#fff', 0: '#000'},
        fontSize: 18,
        margin: {top: 7}
    },
    {name: 'liner1', type: 'rect', x: 0, y:35, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'start', type: 'text', text: {data: 'data', field: 'workrelease'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'workstart'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-16
        }, 
        y:39, 
        height: 40,
        iconWidth: 32, iconHeight: 32,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: {data: 'data', field: 'statusicon', default: []}
    },

    {name: 'timestatus', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['wotype'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:46, 
        height: 24,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'timestatus', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['wotype'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:46, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14, fontStyle: 'bold',
        text: {data: 'data', field: 'wotype'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },
    {name: 'person', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'persontext', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14,
        text: {data: 'data', field: 'sa_name'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },

  ]
}

JsBoard.Chips.irregular_gr.waiting_inspection = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 8, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            if (vmin>5){
                return '#e00';
            } else {
                return '#fff';
            }
        },
        colorMap: {default: '#f0f0f0', 1: '#e00', 0: '#fff'},
        stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'timewait', type: 'text', text: {data: 'data', field: 'timewait'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        align: 'center',
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            if (vmin>5){
                return '#fff';
            } else {
                return '#000';
            }
        },
        colorMap: {default: '#f0f0f0', 1: '#fff', 0: '#000'},
        fontSize: 18, 
        margin: {top: 7}
    },
    {name: 'liner1', type: 'rect', x: 0, y:35, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    // {name: 'start', type: 'text', text: {data: 'data', field: 'start'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 18,
    //     margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'workrelease'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'workstart'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-16
        }, 
        y:39, 
        height: 40,
        iconWidth: 32, iconHeight: 32,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: {data: 'data', field: 'statusicon', default: []}
    },

    {name: 'timestatus', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['wotype'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:46, 
        height: 24,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'timestatus', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['wotype'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:46, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14, fontStyle: 'bold',
        text: {data: 'data', field: 'wotype'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },
    {name: 'person', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'persontext', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14,
        text: {data: 'data', field: 'sa_name'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },

    {name: 'foreman', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['InitialForeman'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-50
        }, 
        y:46, 
        height: 24,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'foremantext', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['InitialForeman'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-50
        }, 
        y:46, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14,
        text: {data: 'data', field: 'InitialForeman'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },

  ]
}

JsBoard.Chips.irregular_gr.waiting_teco = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 8, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            if (vmin<=5){
                return '#0a0';
                //'#e00', 1: '#ec0', 0: '#0a0'
            } else if (vmin<=15){
                return '#ec0';
            } else {
                return '#e00';
            }
        },
        stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'timewait', type: 'text', text: {data: 'data', field: 'timewait'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        align: 'center',
        fill: '#fff', 
        fontSize: 18, 
        margin: {top: 7}
    },
    {name: 'liner1', type: 'rect', x: 0, y:35, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    // {name: 'start', type: 'text', text: {data: 'data', field: 'start'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 14,
    //     margin: {left: 8}},

    {name: 'start', type: 'text', text: {data: 'data', field: 'workrelease'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'workstart'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},

    {name: 'icons', type: 'icons', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-16
        }, 
        y:39, 
        height: 40,
        iconWidth: 32, iconHeight: 32,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: {data: 'data', field: 'statusicon', default: []}
    },

    {name: 'person', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'persontext', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14,
        text: {data: 'data', field: 'sa_name'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },


  ]
}

JsBoard.Chips.irregular_gr.waiting_notif = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 8, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            if (vmin<-15){
                return '#fff';
                //'#e00', 1: '#ec0', 0: '#0a0'
            } else if (vmin<-10){
                return '#0a0';
            } else if (vmin<-5){
                return '#ec0';
            } else {
                return '#e00';
            }
        },
        stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'timewait', type: 'text', text: {data: 'data', field: 'timewait'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        align: 'center',
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            var vval = data['data']['timewait'];
            var vmin = JsBoard.Common.getMinute(vval);
            if (vmin<-15){
                return '#000';
                //'#e00', 1: '#ec0', 0: '#0a0'
            } else if (vmin<-10){
                return '#000';
            } else if (vmin<-5){
                return '#fff';
            } else {
                return '#fff';
            }
        }, 
        fontSize: 18, 
        margin: {top: 7}
    },
    {name: 'liner1', type: 'rect', x: 0, y:35, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    // {name: 'start', type: 'text', text: {data: 'data', field: 'start'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 14,
    //     margin: {left: 8}},

    {name: 'start', type: 'text', text: {data: 'data', field: 'workrelease'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'workstart'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},

    {name: 'icons', type: 'icons', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-16
        }, 
        y:39, 
        height: 40,
        iconWidth: 32, iconHeight: 32,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: {data: 'data', field: 'statusicon', default: []}
    },

    {name: 'person', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'persontext', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14,
        text: {data: 'data', field: 'sa_name'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },


  ]
}

JsBoard.Chips.irregular_gr.waiting_delivery = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 8, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'timestatus', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        fill: function(base, name, data, me){
            //return data['chip']['width'] - data['config']['width']-8
            if (data['data']['LamaHariCalltoSE'] == 0){
                var vval = data['data']['timewait'];
                var vmin = JsBoard.Common.getMinute(vval);
                if (vmin<=15){
                    return '#ec0';
                } else {
                    return '#e00';
                }
            } else {
                return '#e00';
            }
            
        },
        stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'timewait', type: 'text', text: {data: 'data', field: 'timewait'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 32,
        align: 'center',
        fill: '#fff', 
        fontSize: 18, 
        margin: {top: 7}
    },
    {name: 'liner1', type: 'rect', x: 0, y:35, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    // {name: 'start', type: 'text', text: {data: 'data', field: 'start'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 14,
    //     margin: {left: 8}},

    {name: 'start', type: 'text', text: {data: 'data', field: 'workrelease'}, x: 0, y: 36, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},
    {name: 'start', type: 'text', text: {data: 'data', field: 'workstart'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 18,
        margin: {left: 8}},

    {name: 'icons', type: 'icons', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-16
        }, 
        y:39, 
        height: 40,
        iconWidth: 32, iconHeight: 32,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: {data: 'data', field: 'statusicon', default: []}
    },

    {name: 'person', type: 'rect', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: '#fff', 
        stroke: '#333', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 10, right: 10, top: 0, bottom: 0}
    },
    {name: 'persontext', type: 'text', 
        visible: function(base, name, data, me){
            if(typeof data['data']['sa_name'] !== 'undefined'){
                return 1;
            }
            return 0;
        },
        width: 60, 
        x: 0, 
        y:80, 
        height: 24,
        fill: 'black', 
        align: 'center', fontSize: 14,
        text: {data: 'data', field: 'sa_name'},
        margin: {left: 10, right: 10, top: 5, bottom: 0}
    },


  ]
}