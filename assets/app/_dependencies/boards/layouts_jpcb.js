//// Layout for jpcb Board
//// dependent on layouts.js
JsBoard.Layouts.jpcb = function(config) {
    this.parent = JsBoard.Layouts.base;
    // call parent's constructor
    this.parent.call(this, config);
    //JsBoard.Layouts.base.call(this);

}

// inherit jpcb board from base board
JsBoard.Layouts.jpcb.prototype = Object.create(JsBoard.Layouts.base.prototype);

// correct constructor pointer because it points to Person
JsBoard.Layouts.jpcb.prototype.constructor = JsBoard.Layouts.jpcb;

JsBoard.Layouts.jpcb.prototype.initLayout = function() {
    this.parent.prototype.initLayout.call(this);
    var defconfig = {
        columnWidth: 90,
        headerHeight: 20,
        firstColumnWidth: 80,
        showVerticalLine: true,
        headerFill: '#bbb',
        firstColumnTitle: '',
        firstColumnItems: [],
        contentWidth: 1000,
        rowHeight: 50,
        firstColumnGap: 5,
        firstColumnChipName: 'jpcb_stall',
        firstColumnBaseChipName: 'jpcb_stall_base',
        blockerChipName: 'jpcb_blocked',
        firstHeaderChipName: 'jpcb_first_header',
        headerChipName: 'jpcb_header',
        footerHeight: 30,
        stopageChipTop: 40,
        stopageChipLeft: 20,
        stopageHorizontalGap: 20,
        stopageVerticalGap: 10,
        fixedHeaderHeight: 0,
    }
    this.board = JsBoard.Common.extend(defconfig, this.board);
    this.timeLine = 0;
}

JsBoard.Layouts.jpcb.prototype.createBoard = function(config) {
    if (typeof config.scroll == 'undefined') {
        config.scroll = 'horizontal';
    }
    //config.scroll = 'horizontal';
    var board = this.parent.prototype.createBoard.call(this, config);
    var defconfig = {
        timeLineColor: '#0d0'
    }
    vconfig = JsBoard.Common.extend(defconfig, this.layoutConfig.board);
    var xline = {
        points: [50, -100, 50, 0],
        stroke: vconfig.timeLineColor,
        strokeWidth: 2.5,
        lineCap: 'round',
        lineJoin: 'round'
    }

    this.board.stoppagearea = { left: 0, top: 0, width: 0, height: 0 }
    this.board.selectarea = { left: 0, top: 0, width: 0, height: 0 }

    var vblocker = new Konva.Group({ x: 0, y: 0 })
    board.blocker = vblocker;
    // board.add(vblocker);
    board.scrollable.add(vblocker);

    var vline = new Konva.Line(xline);
    board.timeLine = vline;
    // board.add(vline);
    board.scrollable.add(vline);


    var vfooter = new Konva.Group({ x: 0, y: 0 })
    board.fixedFooter = vfooter;
    board.add(vfooter);

    var fixedGroup = new Konva.Group({ x: 0, y: 0 })
    board.fixedGroup = fixedGroup;
    board.add(fixedGroup);

    var stoppage = new Konva.Group({ x: 0, y: 0 })
    board.stoppage = stoppage;
    board.add(stoppage);

    var vfixheader = new Konva.Group({ x: 0, y: 0 })
    board.fixedHeader = vfixheader;
    board.add(vfixheader);

    var selectbox = new Konva.Group({ x: 0, y: 0 })
    board.selectbox = selectbox;
    board.add(selectbox);

    return board;

}

JsBoard.Layouts.jpcb.prototype.updateTimeLine = function(vboard, vTimeIndex) {
    this.timeLine = vTimeIndex;
    var vleft = vTimeIndex * this.board.columnWidth + this.board.firstColumnWidth;
    var vtop = this.board.fixedHeaderHeight;
    vpoints = [vleft, vtop, vleft, this.board.contentHeight]
    vboard.timeLine.points(vpoints);
}

// JsBoard.Layouts.jpcb.prototype.updateDraggable = function(){
//     var vchipList = this.container.chips.children;
//     for (var i=0; i<vchipList.length; i++){
//         var vchip = vchipList[i];
//         var vdrag = false;
//         if (typeof vchip !== 'undefined'){
//             if (typeof vchip.data !== 'undefined'){
//                 if (typeof vchip.data.col !== 'undefined'){
//                     if (vchip.data.col>this.timeLine){
//                         vdrag = true;
//                     }
//                 }
//             }
//             vchip.draggable(vdrag);
//         }
//     }
// }

// JsBoard.Layouts.jpcb.prototype.xdrawSingleDayHeader = function(vboard_bg){
//     var defconfig = {
//         columnWidth: 90,
//         headerHeight: 20,
//         firstColumnWidth: 80,
//         headerFill: '#bbb',
//         firstColumnTitle: '',
//     }
//     vconfig = JsBoard.Common.extend(defconfig, this.board);
//     var vmargin = {left: 2, right: 2, top: 2, bottom: 2}
//     vconfig.margin = JsBoard.Common.extend(vmargin, this.margin);

//     this.infoCols = []
//     //console.log("header-config", config);
//     //console.log("header-vconfig", vconfig);
//     var vitems = JsBoard.Common.generateDayHours(vconfig);
//     var vtop = 0;
//     var vleft = 0;
//     var drect = {x: 0, y: 0,
//         width: 10,
//         height: vconfig.headerHeight,
//         fill: vconfig.headerFill
//     }
//     var hdback = new Konva.Rect(drect);
//     console.log(drect);
//     vboard_bg.add(hdback);
//     var dtext = {
//         x: vleft+this.margin.left,
//         y: vtop+this.margin.top,
//         width: vconfig.columnWidth-vconfig.margin.left-vconfig.margin.top,
//         height: vconfig.headerHeight-vconfig.margin.top-vconfig.margin.bottom,
//         text: vconfig.firstColumnTitle,
//     }

//     drect.width = vconfig.firstColumnWidth;
//     var vobj = new Konva.Rect(drect);
//     this.container.board.fixedGroup.add(vobj);

//     var vobj = new Konva.Text(dtext);
//     this.container.board.fixedGroup.add(vobj);
//     // vboard_bg.add(vobj);
//     this.infoCols.push({
//         left: 0,
//         width: vconfig.firstColumnWidth
//     })
//     vleft = vleft+vconfig.firstColumnWidth;
//     for(var i=0; i<vitems.length; i++){
//         dtext.text = vitems[i];
//         dtext.x = vleft+vconfig.margin.left;
//         var vobj = new Konva.Text(dtext);
//         vboard_bg.add(vobj);
//         this.infoCols.push({
//             left: vleft,
//             width: vconfig.columnWidth
//         })
//         vleft = vleft+vconfig.columnWidth;
//     }
//     this.board.contentWidth = vleft;
//     hdback.setWidth(this.board.contentWidth);

//     // var xline = {
//     //     points: [5, 70, 140, 23],
//     //     stroke: vconfig.lineColor,
//     //     strokeWidth: 1,
//     //     lineCap: 'round',
//     //     lineJoin: 'round'
//     // }


// }

JsBoard.Layouts.jpcb.prototype.drawSingleDayHeader = function(vboard) {
    var vitems = JsBoard.Common.generateDayHours(vconfig);
    this.defaultHeader(vboard, vitems, this.board.firstHeaderChipName, this.board.headerChipName);
}

JsBoard.Layouts.jpcb.prototype.drawSimpleHeader = function(vboard) {
    //var vitems = JsBoard.Common.generateDayHours(vconfig);
    if (typeof this.board.headerItems !== 'undefined') {
        vitems = this.board.headerItems;
        this.defaultHeader(vboard, vitems, this.board.firstHeaderChipName, this.board.headerChipName);
    }
}


// JsBoard.Layouts.jpcb.prototype.drawFixFooter = function(vfooter){
//     vfooter.destroyChildren();
//     vwidth = this.stage.width;
//     var dtext = {
//         x: 0, y: this.board.contentHeight, width: vwidth, height: this.board.footerHeight
//     }

//     if (typeof this.board.footerChipName !== 'undefined'){
//         vitem = {chip: this.board.footerChipName};
//         var vchip = this.container.createChip(dtext, vitem);
//         if (typeof vchip.shapes.stoppagearea !== 'undefined'){
//             var vstop = vchip.shapes.stoppagearea;
//             this.board.stoppagearea = {left: vstop.x(), right: vstop.x()+vstop.width(),
//                 top: vstop.y()+this.board.contentHeight, bottom: vstop.y()+vstop.height()+this.board.contentHeight}
//         } else {
//             this.board.stoppagearea = {left: 0, top: this.board.contentHeight,
//                 right: this.board.contentWidth, bottom: this.board.contentHeight+this.board.footerHeight}

//         }
//         vfooter.add(vchip);
//     }

// }

JsBoard.Layouts.jpcb.prototype.defaultHeader = function(vboard, vitems, firstchip, hdchip) {
    var vboard_bg = vboard.bg;
    var defconfig = {
        columnWidth: 90,
        headerHeight: 20,
        firstColumnWidth: 80,
        headerFill: '#bbb',
        firstColumnTitle: '',
    }
    vconfig = JsBoard.Common.extend(defconfig, this.board);
    var vmargin = { left: 2, right: 2, top: 2, bottom: 2 }
    vconfig.margin = JsBoard.Common.extend(vmargin, this.margin);

    this.infoCols = []
        // var vitems = JsBoard.Common.generateDayHours(vconfig);
    var vtop = 0;
    var vleft = 0;
    var drect = {
        x: 0,
        y: 0,
        width: 10,
        height: vconfig.headerHeight,
        fill: vconfig.headerFill
    }
    var hdback = new Konva.Rect(drect);
    console.log(drect);
    vboard_bg.add(hdback);
    var dtext = {
        x: vleft + this.margin.left,
        y: vtop + this.margin.top,
        width: vconfig.columnWidth - vconfig.margin.left - vconfig.margin.top,
        height: vconfig.headerHeight - vconfig.margin.top - vconfig.margin.bottom,
        text: vconfig.firstColumnTitle,
    }

    // first column, fixed header
    if (typeof vconfig.firstHeaderData !== 'undefined') {
        var vitem = vconfig.firstHeaderData;
        vitem.chip = firstchip;
    } else {
        var vitem = { chip: firstchip, text: vconfig.firstColumnTitle };
    }
    drect.width = vconfig.firstColumnWidth;
    var vchip = this.container.createChip(drect, vitem);
    this.container.board.fixedGroup.add(vchip);
    this.infoCols.push({
        left: 0,
        width: vconfig.firstColumnWidth,
        data: vitem,
    })

    /*
    var vobj = new Konva.Rect(drect);
    this.container.board.fixedGroup.add(vobj);

    var vobj = new Konva.Text(dtext);
    this.container.board.fixedGroup.add(vobj);
    // vboard_bg.add(vobj);
    this.infoCols.push({
        left: 0,
        width: vconfig.firstColumnWidth
    })

    */

    // header
    vleft = vleft + vconfig.firstColumnWidth;
    for (var i = 0; i < vitems.length; i++) {
        // dtext.text = vitems[i];
        // dtext.x = vleft+vconfig.margin.left;
        if (typeof vitems[i] == 'object') {
            vitem = vitems[i];
            vitem.chip = hdchip;
        } else {
            var vitem = { chip: hdchip, text: vitems[i] };
        }
        drect.width = vconfig.columnWidth
        drect.x = vleft;
        var vchip = this.container.createChip(drect, vitem);
        vboard_bg.add(vchip);


        // var vobj = new Konva.Text(dtext);
        // vboard_bg.add(vobj);

        var colinfo = {
            left: vleft,
            width: vconfig.columnWidth
        }
        if (typeof vitem.colInfo !== 'undefined') {
            colinfo = JsBoard.Common.extend(vitem.colInfo, colinfo);
        }
        colinfo.data = vitem;
        this.infoCols.push(colinfo)
        vleft = vleft + vconfig.columnWidth;
    }
    this.board.contentWidth = vleft;
    // this.board.contentWidth = Math.max(vleft, this.boardWidth)
    hdback.setWidth(this.board.contentWidth);


}

JsBoard.Layouts.jpcb.prototype.drawVerticalLines = function(vboard_bg) {
    var defconfig = {
        lineColor: '#888',
        contentHeight: 100,
    }

    vconfig = JsBoard.Common.extend(defconfig, this.board);
    var vheight = vconfig.contentHeight;
    var xline = {
        points: [5, 70, 140, 23],
        stroke: vconfig.lineColor,
        strokeWidth: 1,
        lineCap: 'round',
        lineJoin: 'round'
    }
    var vtop = 0;
    if (typeof this.board.verticalLineTop !== 'undefined') {
        vtop = this.board.verticalLineTop;
    }
    var vbreaktop = this.board.headerHeight;
    for (var i = 1; i < this.infoCols.length; i++) {
        var vleft = this.infoCols[i].left;
        var vwidth = this.infoCols[i].width;
        xline.points = [vleft, vtop, vleft, vconfig.contentHeight];
        var vline = new Konva.Line(xline);
        vboard_bg.add(vline);
        if (this.infoCols[i].isBreak) {
            console.log('is Break');
            var vdim = { x: vleft, y: vbreaktop, width: vwidth, height: vconfig.contentHeight - vbreaktop }
            var vdata = this.infoCols[i];
            vdata.chip = this.board.breakChipName;
            var vchip = this.container.createChip(vdim, vdata);
            vboard_bg.add(vchip);
        }
    }
    // var vmargin = {left: 2, right: 2, top: 2, bottom: 2}
    // vconfig.margin = JsBoard.Common.extend(vmargin, this.margin);

    //console.log("header-config", config);
    //console.log("header-vconfig", vconfig);

    // var vitems = JsBoard.Common.generateDayHours(vconfig);
    // var vtop = 0;
    // var vleft = 0;
    // var drect = {x: 0, y: 0,
    //     width: 10,
    //     height: vconfig.headerHeight,
    //     fill: vconfig.headerFill
    // }
    // var hdback = new Konva.Rect(drect);
    // console.log(drect);
    // vboard_bg.add(hdback);
    // var dtext = {
    //     x: vleft+this.margin.left,
    //     y: vtop+this.margin.top,
    //     width: vconfig.columnWidth-vconfig.margin.left-vconfig.margin.top,
    //     height: vconfig.headerHeight-vconfig.margin.top-vconfig.margin.bottom,
    //     text: vconfig.firstColumnTitle
    // }
    // var vobj = new Konva.Text(dtext);
    // vboard_bg.add(vobj);
    // vleft = vleft+vconfig.firstColumnWidth;
    // for(var i=0; i<vitems.length; i++){
    //     dtext.text = vitems[i];
    //     dtext.x = vleft+vconfig.margin.left;
    //     var vobj = new Konva.Text(dtext);
    //     vboard_bg.add(vobj);
    //     vleft = vleft+vconfig.columnWidth;
    // }
    // this.board.contentWidth = vleft;
    // hdback.setWidth(this.board.contentWidth);

}

JsBoard.Layouts.jpcb.prototype.makeFirstColumnTitle = function(vconfig, vItem) {
    if (typeof vconfig.firstColumnFormat == 'function') {
        return vconfig.firstColumnFormat(vconfig, vItem);
    } else {
        return vItem.title;
    }
}

JsBoard.Layouts.jpcb.prototype.drawFixFooter = function(vfooter) {
    vfooter.destroyChildren();
    vwidth = this.stage.width;
    var dtext = {
        x: 0,
        y: this.board.contentHeight,
        width: vwidth,
        height: this.board.footerHeight
    }

    if (typeof this.board.footerChipName !== 'undefined') {
        vitem = { chip: this.board.footerChipName };
        var vchip = this.container.createChip(dtext, vitem);
        if (typeof vchip.shapes.stoppagearea !== 'undefined') {
            var vstop = vchip.shapes.stoppagearea;
            this.board.stoppagearea = {
                left: vstop.x(),
                right: vstop.x() + vstop.width(),
                top: vstop.y() + this.board.contentHeight,
                bottom: vstop.y() + vstop.height() + this.board.contentHeight
            }
        } else {
            this.board.stoppagearea = {
                left: 0,
                top: this.board.contentHeight,
                right: this.board.contentWidth,
                bottom: this.board.contentHeight + this.board.footerHeight
            }

        }
        vfooter.add(vchip);
    }

}

JsBoard.Layouts.jpcb.prototype.drawFixHeader = function(vheader) {
    vheader.destroyChildren();
    vwidth = this.stage.width;
    var dtext = {
        x: 0,
        y: 0,
        width: vwidth,
        height: this.board.fixedHeaderHeight
    }

    if (typeof this.board.fixedHeaderChipName !== 'undefined') {
        vitem = { chip: this.board.fixedHeaderChipName };
        var vchip = this.container.createChip(dtext, vitem);
        if (typeof vchip.shapes.selectarea !== 'undefined') {
            var vstop = vchip.shapes.selectarea;
            this.board.selectarea = {
                left: vstop.x(),
                right: vstop.x() + vstop.width(),
                top: vstop.y(),
                bottom: vstop.y() + vstop.height()
            }
        } else {
            this.board.selectarea = {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
            }

        }
        vheader.add(vchip);
    }

}

JsBoard.Layouts.jpcb.prototype.drawFirstColumn = function(vboard_bg) {
    var defconfig = {
        firstColumnItems: [],
        contentWidth: 1000,
        rowHeight: 50,
        headerHeight: 20,
        firstColumnGap: 5,
        firstColumnWidth: 80,
        // firstColumnChipName: 'jpcb_stall',
    }

    var vconfig = JsBoard.Common.extend(defconfig, this.board);
    this.infoRows = []
    var vleft = 0;
    var vtop = vconfig.headerHeight;
    var vwidth = vconfig.firstColumnWidth;
    var vheight = vconfig.rowHeight;
    var dtext = {
        x: vleft,
        y: vtop,
        width: vwidth,
        height: vheight
    }

    var vFirstCol = new Konva.Group({ x: 0, y: 0 });
    this.container.board.fixedGroup.add(vFirstCol);
    for (var i = 0; i < vconfig.firstColumnItems.length; i++) {
        dtext.y = vtop;
        // dtext.text = this.makeFirstColumnTitle(vconfig, vconfig.firstColumnItems[i]);
        // var vtext = new Konva.Text(dtext);
        vitem = vconfig.firstColumnItems[i];
        var xdataItem = JsBoard.Common.extend({}, vitem);
        vitem['chip'] = vconfig.firstColumnBaseChipName;
        vitem.index = i;
        var vchip = this.container.createChip(dtext, vitem);
        vitem['chip'] = vconfig.firstColumnChipName;
        var xchip = this.container.createChip(dtext, vitem);
        vboard_bg.add(vchip);
        vFirstCol.add(xchip);
        //this.container.board.fixedFooter.add(vchip);
        //var xheight = vtext.getHeight()+this.margin.top+this.margin.bottom;
        var xheight = 0;
        if (xchip.shapes['textlist']) {
            var vchild = xchip.shapes['textlist'];
            xheight = vchild.y() + vchild.height();
        }
        xheight = Math.max(xheight, vheight);
        if (vchip.shapes['base']) {
            var vchild = vchip.shapes['base'];
            vchild.setHeight(xheight);
            vchild.setWidth(this.board.contentWidth);
        }
        if (xchip.shapes['base']) {
            var xchild = xchip.shapes['base'];
            xchild.setHeight(xheight);
            //vchild.setWidth(this.board.contentWidth);
        }
        // if (typeof vchip.children[2] !== 'undefined'){
        //     var vchild = vchip.children[2];
        //     xheight = vchild.y()+vchild.height();
        // } else {
        //     xheight = 0;
        // }
        if ((typeof this.board !== 'undefined') && (typeof this.board.onStallItemCreated !== 'undefined')) {
            try {
                var frontChip = xchip;
                var backChip = vchip;
                this.board.onStallItemCreated(frontChip, backChip);
            } catch (err) {
                console.error(err);
            }

        }
        this.infoRows.push({
            top: vtop,
            height: xheight,
            data: xdataItem,
            rowConstant: vconfig.rowHeight,
        });
        vtop = vtop + xheight;
        if (this.container.listeners) {
            if (this.container.listeners.stall) {
                this.mapChipListeners(vchip, this.container.listeners.stall);
            }

        }
    }
    this.board.contentHeight = vtop + 1;
}

JsBoard.Layouts.jpcb.prototype.prepareBoard = function(vboard) {
    var vboard_bg = vboard.bg;
    var board_config = this.board;
    var defconfig = { headerType: 'singleday' }
    var hdConfig = JsBoard.Common.extend(defconfig, board_config);
    this.infoCols = []
    if (hdConfig.headerType == 'singleday') {
        this.drawSingleDayHeader(vboard);
    }
    if (hdConfig.headerType == 'simple') {
        this.drawSimpleHeader(vboard);
    }
    this.drawFirstColumn(vboard_bg);
    if (this.board.showVerticalLine) {
        this.drawVerticalLines(vboard_bg);
    }


    // console.log("**jpcb***vboard_bg", vboard_bg);
    // console.log("**jpcb***vconfig", vconfig);
}

JsBoard.Layouts.jpcb.prototype.prepareBoardLayout = function(vboard_bg) {
    this.parent.prototype.prepareBoardLayout.call(this, vboard_bg);
    this.stopageLeft = this.board.stopageHorizontalGap;
    this.stopageTop = this.board.stopageChipTop;
    this.stopageWidth = this.stage.width - this.board.stopageVerticalGap * 2;

}


JsBoard.Layouts.jpcb.prototype.createBlockers = function() {
    vblockergroup = this.container.board.blocker;
    for (var i = 0; i < this.infoRows.length; i++) {
        var vinfo = this.infoRows[i];
        var vdef = {
            listening: false,
            x: this.board.firstColumnWidth,
            y: vinfo.top,
            width: this.board.contentWidth,
            height: vinfo.height,
            visible: false,
        }
        var xitem = { chip: this.board.blockerChipName }
        var vblocker = this.container.createChip(vdef, xitem)
        vblockergroup.add(vblocker);
        this.infoRows[i].blocker = vblocker;
    }
    // var xitem = {chip: vconfig.blockerChipName}
    // var vblocker = this.container.createChip({x: 0, y: vtop, width: this.board.contentWidth, height: xheight}, xitem)
    // // if (vblocker.shapes['base']){
    // //     var vchild = vblocker.shapes['base'];
    // //     vchild.setHeight(xheight);
    // //     vchild.setWidth(this.board.contentWidth);
    // // }

}

JsBoard.Layouts.jpcb.prototype.updateContainer = function(vcontainer) {
    // var vheight = Math.min(this.maxStageHeight, this.chipTop);
    vcontainer.stage.setHeight(this.board.contentHeight + this.board.footerHeight);

    this.drawFixFooter(vcontainer.board.fixedFooter);
    this.drawFixHeader(vcontainer.board.fixedHeader);

    this.createBlockers();

    // var vclip = {
    //     x: 0, y: 0,
    //     width: vcontainer.stage.width(),
    //     height: vcontainer.stage.height(),
    // }
    // vcontainer.base.clip(vclip);
    // vcontainer.stage.setClip({
    //     x: 0, y: 0,
    //     width: vcontainer.stage.width(),
    //     height: vcontainer.stage.height(),
    // })

}

JsBoard.Layouts.jpcb.prototype.updateContainerLayout = function() {
    // pindahkan dengan row -2 ke selectarea
    var compare = function(a, b) {
        // console.log('ini aA b', a)
        // console.log('ini a bB', b)
        vidx1 = 9999;
        vidx2 = 9999;
        if ((a.data.chip == 'jpcb2') && (b.data.chip == 'jpcb2')){
            if (a.data && a.data.dataIndex !== undefined && a.data.dataIndex !== null) {
                vidx1 = a.data.dataIndex;
            }
            if (b.data && b.data.dataIndex !== undefined && b.data.dataIndex !== null) {
                vidx2 = b.data.dataIndex;
            }
            if (vidx1 < vidx2)
                return 1;
            if (vidx1 > vidx2)
                return -1;
            return 0;

        } else {
            if (a.data && a.data.dataIndex) {
                vidx1 = a.data.dataIndex;
            }
            if (b.data && b.data.dataIndex) {
                vidx2 = b.data.dataIndex;
            }
            if (vidx1 < vidx2)
                return 1;
            if (vidx1 > vidx2)
                return -1;
            return 0;

        }
        
    }

    var vcontainer = this.container;
    var vlayout = this;
    var vlist1 = []
    var vlist2 = []
    vcontainer.board.stoppage.destroyChildren();
    vcontainer.processChips(function(vchip) {
        if (vchip.data.row < 0) {
            return true;
        }
        return false;
    }, function(vchip) {
        // vchip.moveTo(vcontainer.board.selectbox);
        if (vchip.data.row == -1) {
            vlist1.push(vchip);
        }
        if (vchip.data.row == -2) {
            vlist2.push(vchip);
        }
    })
    for (var i = 0; i < vlist1.length; i++) {
        vlist1[i].moveTo(vcontainer.board.stoppage);
    }
    var vp = 100;
    if (typeof this.board.maxSelectCol !== 'undefined') {
        var vp = this.board.maxSelectCol;
    }
    var vleft = 0;
    vlist2.sort(compare);
    for (var i = 0; i < vlist2.length; i++) {
        var vitem = vlist2[i];
        vx = vleft;
        vy = 0;
        vitem.moveTo(vcontainer.board.selectbox);
        vitem.x(vx + 20);
        vitem.y(vy + 20);


        vleft = vleft + vitem.width() + 10;

        // var vx = (i % vp)*vitem.width();
        // if (vx<vleft){
        //     vx = vleft;
        // }
        // var vy = Math.floor(i/vp) * vitem.height();
        // vitem.moveTo(vcontainer.board.selectbox);
        // vitem.x(vx+20);
        // vitem.y(vy+10);
        // vleft = vleft+vitem.width()+10;

        var vIsDrag = true; // default true
        if ((typeof vitem.data !== 'undefined') && (typeof vitem.data.isDraggable !== 'undefined') && (!vitem.data.isDraggable)) {
            vIsDrag = false;
        }
        vitem.draggable(vIsDrag);
    }

    // for (var i=0; i<vlist2.length; i++){
    //     var vitem = vlist2[i];
    //     var vx = (i % vp)*vitem.width();
    //     var vy = Math.floor(i/vp) * vitem.height();
    //     vitem.moveTo(vcontainer.board.selectbox);
    //     vitem.x(vx+20);
    //     vitem.y(vy+10);
    //     vitem.draggable(true);
    // }


    // for (var i=0; i<vcontainer.chips.length; i++){
    //     vchip = vcontainer.chips[i];
    //     if (vchip.data) {
    //         if (vchip.data.row < 0){
    //             vchip.moveTo(vcontainer.board.selectbox);
    //         } else if (vchip.data.row == -1){
    //             vchip.moveTo(vcontainer.board.stoppage);
    //         }
    //     }
    // }


}

JsBoard.Layouts.jpcb.prototype.getInfoRowData = function(vcol) {
    if (typeof this.infoRows[vcol] !== 'undefined') {
        if (typeof this.infoRows[vcol]['data'] !== 'undefined') {
            return this.infoRows[vcol]['data'];
        }
    }
    return false;
}

JsBoard.Layouts.jpcb.prototype.getInfoColData = function(vcol) {
    if (typeof this.infoCols[vcol] !== 'undefined') {
        if (typeof this.infoCols[vcol]['data'] !== 'undefined') {
            return this.infoCols[vcol]['data'];
        }
    }
    return false;
}

JsBoard.Layouts.jpcb.prototype.x2col = function(vx) {
    vdivider = 2;
    if (this.board.snapDivider) {
        vdivider = this.board.snapDivider;
    }

    vxx = (vx - this.board.firstColumnWidth) / this.board.columnWidth;
    vxx = Math.round(vxx * vdivider);
    vxx = vxx / vdivider;
    return vxx;
    // vxx = (vx-this.board.firstColumnWidth)/this.board.columnWidth;
    // vxx = Math.round(vxx*2);
    // vxx = vxx/2;
    // return vxx;
}

JsBoard.Layouts.jpcb.prototype.y2row = function(vy) {
    for (var i = 0; i < this.infoRows.length; i++) {
        vitem = this.infoRows[i];
        if ((vy > vitem.top) && (vy < vitem.top + vitem.height)) {
            return i;
        }
    }
    return false;
}

JsBoard.Layouts.jpcb.prototype.col2x = function(vcol) {
    vresult = vcol * this.board.columnWidth + this.board.firstColumnWidth;
    return vresult;
}

JsBoard.Layouts.jpcb.prototype.row2y = function(xrow) {
    var vrow = Math.ceil(xrow);
    var vdelta = xrow - vrow;
    var vresult = false;
    if (typeof this.infoRows[vrow] !== 'undefined') {
        vresult = this.infoRows[vrow].top;
        if (typeof this.infoRows[vrow].rowConstant !== 'undefined') {
            vresult = vresult + vdelta * this.infoRows[vrow].rowConstant;
        }
    }
    return vresult;
}



JsBoard.Layouts.jpcb.prototype.chipDimension = function(vitem) {
    var vIsWrap = this.board.wrapStopage;
    if (vIsWrap) {
        var vtop = 0;
        var vleft = 0;
        var xleftStat = false;
        var vwidth = vitem.width * this.board.columnWidth;
        if (vitem.row == -1) {
            if ((this.stopageLeft + vwidth) > this.stopageWidth) {
                this.stopageTop = this.stopageTop + this.board.rowHeight + this.board.stopageVerticalGap;
                this.stopageLeft = this.board.stopageHorizontalGap;
                this.stopageMaxHeight = this.stopageTop - this.board.stopageChipTop
            }
            vleft = this.stopageLeft;
            vtop = this.board.contentHeight + this.stopageTop;
            // vtop = this.board.contentHeight+this.board.stopageChipTop;
            xleftStat = true;
        } else {
            vleft = this.col2x(vitem.col);
            vtop = this.row2y(vitem.row);
        }
        var result = {
                x: vleft,
                y: vtop,
                width: vwidth,
                height: this.board.rowHeight,
                columnWidth: this.board.columnWidth,
            }
            // stopageChipTop: 40,
            // stopageChipLeft: 20,
            // stopageHorizontalGap: 20,
        if (xleftStat) {
            this.stopageLeft = this.stopageLeft + result.width + this.board.stopageHorizontalGap;
        }
        return result;

    } else {

        // console.log("***vitem***", vitem);
        var vtop = 0;
        var vleft = 0;
        var xleftStat = false;
        if (vitem.row == -1) {
            vleft = this.stopageLeft;
            vtop = this.board.contentHeight + this.board.stopageChipTop;
            xleftStat = true;
        } else {
            vleft = this.col2x(vitem.col);
            vtop = this.row2y(vitem.row);
        }
        var result = {
                x: vleft,
                y: vtop,
                width: vitem.width * this.board.columnWidth,
                height: this.board.rowHeight,
                columnWidth: this.board.columnWidth,
            }
            // stopageChipTop: 40,
            // stopageChipLeft: 20,
            // stopageHorizontalGap: 20,
        if (xleftStat) {
            this.stopageLeft = this.stopageLeft + result.width + this.board.stopageHorizontalGap;
        }
        return result;


    }

}

// update layout by chip dimension after chip has been created and drawn
// JsBoard.Layouts.jpcb.prototype.updateByChipDimension = function(dim, vchip){
//     this.chipTop = this.chipTop+dim.height+this.chipGap;
// }

JsBoard.Layouts.jpcb.prototype.finalizeChip = function(vchip) {
    var dim = vchip.attrs;
    this.chipTop = this.chipTop + dim.height + this.chipGap;
    //console.log("~~~~~~this.listeners~~~~~", this.container.listeners);
    //vchip.draggable(true);
    if (this.container.listeners) {
        if (this.container.listeners.chip) {
            this.mapChipListeners(vchip, this.container.listeners.chip);
        }
    }
    this.dragDropListeners(vchip);
}

JsBoard.Layouts.jpcb.prototype.blockRow = function(vrow, vcolstart) {
    vblockerlist = this.infoRows;
    if (typeof vblockerlist[vrow] !== 'undefined') {
        vblocker = vblockerlist[vrow];
        vleft = this.col2x(vcolstart);
        vblocker.blocker.x(vleft);
        vblocker.blocker.show();
    }
}

JsBoard.Layouts.jpcb.prototype.intersectRect = function(r1, r2) {
    return !(r2.left >= r1.right ||
        r2.right <= r1.left ||
        r2.top >= r1.bottom ||
        r2.bottom <= r1.top);
}

JsBoard.Layouts.jpcb.prototype.intersectRect2 = function(r1, r2) { // cuma buat cek yg chip jpcb bp biar chip tidak di plot di jam yg lbh kcil dari chip sebelumnya 
    // return !(r2.left >= r1.right ||
    return !(
        r2.right <= r1.left);
        //  ||
        // r2.top >= r1.bottom ||
        // r2.bottom <= r1.top);
}

JsBoard.Layouts.jpcb.prototype.pointInsideRect = function(x, y, rect) {
    return (x >= rect.left && x <= rect.right && y >= rect.top && y <= rect.bottom)
}

JsBoard.Layouts.jpcb.prototype.getChipCoord = function(vchip) {
    var vresult = {
        left: vchip.attrs.x,
        top: vchip.attrs.y,
        right: vchip.attrs.x + vchip.attrs.width,
        bottom: vchip.attrs.y + vchip.attrs.height
    }
    return vresult;

}

JsBoard.Layouts.jpcb.prototype.cekMenumpuk = function(vchip, vx, vy) {
    var vidx1 = vchip.data.index;
    var vchipList = this.container.chips.children;
    var vbreak = 0;
    if (vchip.attrs.breakWidth) {
        vbreak = vchip.attrs.breakWidth;
    }
    var minBreak = 0;
    if (vchip.attrs.minBreakWidth) {
        minBreak = vchip.attrs.minBreakWidth;
    }
    //var vr1 = this.getChipCoord(vchip);
    vr1 = {}
    vr1.left = vx;
    vr1.top = vy;
    vr1.right = vx + vchip.attrs.width + vbreak - minBreak;
    vr1.bottom = vy + vchip.attrs.height;
    for (var i = 0; i < vchipList.length; i++) {
        var vidx2 = vchipList[i].data.index;
        vr2 = this.getChipCoord(vchipList[i]);
        if (vidx1 !== vidx2) {
            var vintersect = this.intersectRect(vr1, vr2);
            if (vintersect) {
                this.intersectRect(vr1, vr2);
                console.log('menumpuk', vchip, vchipList[i])
                return true;
            }
        }
    }
    return false;

}
JsBoard.Layouts.jpcb.prototype.cekStall = function(vchip, vx, vy) {
    console.log("cekStalll ====  vchip", vchip, vx, vy);
    if (vchip.data.chip == 'jpcb2') {
        if (vchip.data.tmpStall !== undefined && vchip.data.tmpStall !== null) {
            if (vchip.data.tmpStall !== vx) {
                return true
            } else {
                // console.log("SAMA WOIIIIII");
                return false
            }
        }
    } else if (vchip.data.chip == 'estimasi' || vchip.data.chip == 'chip') {
        console.log('cek sini buat estimasi chip', vchip)
        console.log('cek sini buat estimasi chip prediagnose', vchip.data.isPrediagnose);
        var tempIndexStallPreDiagnose = [];
        for (var i = 0; i < vchip.container.layout.board.firstColumnItems.length; i++) {
            // if (vchip.container.layout.board.firstColumnItems[i].title == "Stall Pre Diagnose") { //stalltype 4 itu prediagnose
            if (vchip.container.layout.board.firstColumnItems[i].type == 4) { //stalltype 4 itu prediagnose
                tempIndexStallPreDiagnose.push(vchip.container.layout.board.firstColumnItems[i].index);
            }
        }
        console.log('tempIndexStallPreDiagnose', tempIndexStallPreDiagnose);

        // un-comment aja kl mau di pake
        // // =============== cek di plot melebihi jam tutup stall gak? ============================ start
        // if (vy >= (vchip.container.layout.board.firstColumnItems[vx].colEndWorkHour + 1)){ // di tambah 1 karena design di board na ketambah 1 entah drmana
        //     // kl di plot lebih ato sama jam tutup ga blh, mending di besokin
        //     console.log('plot nya jng di jam tutup apalagi lebih')
        //     return true
        // }
        // // =============== cek di plot melebihi jam tutup stall gak? ============================ end

        // chip prediagnose ==1 kalau service biasa ==undefined
        if (vchip.data.isPrediagnose == 1) {
            hasil = true;

            for (var i = 0; i < tempIndexStallPreDiagnose.length; i++) {
                if (vx == tempIndexStallPreDiagnose[i]) {
                    console.log(' prediagnose  ke stall prediagnose')

                    hasil = false;
                } else {
                    console.log(' prediagnose  ke stall service')

                    // hasil = true;
                }
            }

            return hasil

        } else if (vchip.data.isPrediagnose == undefined) {
            hasil = false;

            for (var i = 0; i < tempIndexStallPreDiagnose.length; i++) {
                if (vx == tempIndexStallPreDiagnose[i]) {
                    console.log('bukan prediagnose tapi ke stall prediagnose')
                    hasil = true;
                } else {
                    console.log('bukan prediagnose ke stall service')
                        // hasil = false;
                }
            }

            return hasil
        }


    }

}

JsBoard.Layouts.jpcb.prototype.cekBPChip = function(vchip, vx, vy) { // buat cek chip di bp ga boleh di plot di jam yg lbh kecil dari chip sebelumnya
    for (var i = 0; i < vchip.container.chipItems.length; i++){
        if (vchip.container.chipItems[i].nopol == vchip.data.nopol){
            if (vchip.container.chipItems[i].chipType < vchip.data.chipType){
                // ini di bawah copy dari cek menumpuk
                var vidx1 = vchip.data.index;
                var vtype1 = vchip.data.chipType;
                var nopol1 = vchip.data.nopol;
                var vchipList = this.container.chips.children;
                var vbreak = 0;
                if (vchip.attrs.breakWidth) {
                    vbreak = vchip.attrs.breakWidth;
                }
                var minBreak = 0;
                if (vchip.attrs.minBreakWidth) {
                    minBreak = vchip.attrs.minBreakWidth;
                }
                //var vr1 = this.getChipCoord(vchip);
                vr1 = {}
                vr1.left = vx;
                vr1.top = vy;
                vr1.right = vx + vchip.attrs.width + vbreak - minBreak;
                vr1.bottom = vy + vchip.attrs.height;
                for (var i = 0; i < vchipList.length; i++) {
                    var vidx2 = vchipList[i].data.index;
                    var vtype2 = vchipList[i].data.chipType;
                    var nopol2 = vchipList[i].data.nopol;
                    vr2 = this.getChipCoord(vchipList[i]);
                    if (nopol1 == nopol2){
                        if (vtype2 < vtype1){
                            if (vidx1 !== vidx2) {
                                var vintersect = this.intersectRect2(vr1, vr2);
                                if (vintersect) {
                                    this.intersectRect2(vr1, vr2);
                                    console.log('menumpuk', vchip, vchipList[i])
                                    return true;
                                }
                            }
                        }
                    }
                    
                }
                return false;
            }
        }
    }

}

JsBoard.Layouts.jpcb.prototype.cekCol = function(vchip, vx, vy) {
    console.log("cekCol ====  vchip", vchip, vx, vy);
    if (vchip.data.chip == 'jpcb2') {
        for (var i = 0; i < vchip.container.chipItems.length; i++) {
            if (vchip.container.chipItems[i].jobId == vchip.data.jobId) {
                if (vchip.container.chipItems[i].col == vchip.data.col) {
                    return false
                } else {
                    return true
                }
            }
        }
    }

}

JsBoard.Layouts.jpcb.prototype.dragDropListeners = function(vchip) {
    vchip.on('dragend', function(evt) {
        // return;
        console.log("dragend");
        vchip.moveTo(vchip.container.board.chips);
        // vchip.moveTo(vchip.container.board.stoppage);
        // vdx = Math.abs(vchip.dragStartPos.x-this.x());
        // vdy = Math.abs(vchip.dragStartPos.x-this.x());
        vNow = new Date();
        vdt = vNow.getTime() - vchip.dragStartPos.time;
        if (vdt < 300) {
            this.fire('click');
        }
        // if ((vdx<10) && (vdy<10)){
        //     //alert('onclick');

        //     this.fire('click');
        // }
        // console.log('x', this.x());
        // console.log('y', this.y());
        // console.log('drag-chip', this);


        var vlayout = this.container.layout;
        var vcol = vlayout.x2col(this.x() - this.container.board.scrollable.x());
        var vx = vlayout.col2x(vcol);
        // console.log('drag-col', vcol, vx);
        var cy = this.y() + this.height() / 2;
        var vrow = vlayout.y2row(cy);
        var vy = vlayout.row2y(vrow);

        // console.log('drag-row', vrow, vy);
        if (vchip.dragStartPos.row >= 0) {
            vstarty = vlayout.row2y(vchip.dragStartPos.row);
            vstartx = vlayout.col2x(vchip.dragStartPos.col);
        } else {
            vstarty = vchip.dragStartPos.y;
            vstartx = vchip.dragStartPos.x;
        }
        //if (cy>vlayout.board.contentHeight) {
        var vrect = vlayout.getChipCoord(this);
        if ((vrect.right - vrect.left) > 400) {
            vrect.right = vrect.left + 400;
        }
        var moveback = false;
        if (vlayout.pointInsideRect(vrect.left, vrect.top, vlayout.board.stoppagearea) &&
            vlayout.pointInsideRect(vrect.right, vrect.bottom, vlayout.board.stoppagearea)) {
            console.log("area footer");
            if (vchip.data.chip == "jpcb2"){
                if(vchip.data.intStatus != 2){
                    moveback = true;
                } else {
                    // berada di area footer
                    vchip.moveTo(vchip.container.board.stoppage);
                    // vchip.stoppage = 1;
                    vchip.data.row = -1;
                }
            } else {
                // berada di area footer
                vchip.moveTo(vchip.container.board.stoppage);
                // vchip.stoppage = 1;
                vchip.data.row = -1;
            }
            // // berada di area footer
            // vchip.moveTo(vchip.container.board.stoppage);
            // // vchip.stoppage = 1;
            // vchip.data.row = -1;
        } else if (vlayout.pointInsideRect(vrect.left, vrect.top, vlayout.board.selectarea) &&
            vlayout.pointInsideRect(vrect.right, vrect.bottom, vlayout.board.selectarea)) {
            console.log("area selectbox");
            // berada di area selectbox
            vchip.moveTo(vchip.container.board.selectbox);
            // vchip.stoppage = 1;
            vchip.data.row = -2;
        } else if (vcol === false || vrow === false) {
            console.log("invalid row/col", "col", vcol, "row", vrow);
            console.log('move back 0', vchip);
            if (vchip.data.chip == "jpcb2" && vcol != false && vrow == false) {
                if (vchip.data.intStatus != 2){
                    vchip.data.stallId = -2;
                    vchip.data.row = -2;
                    moveback = false;
                } else {
                    moveback = true;
                }
                
            } else {
                moveback = true;
            }
        } else if (!((vy !== false) && (vx !== false))) {
            // ada di luar stall
            console.log("di luar stall", vx, vy);
            console.log('move back 1');

            moveback = true;
            // this.x(vstartx);
            // this.y(vstarty);

            // this.x(this.dragStartPos.x);
            // this.y(this.dragStartPos.y);
        } else if (!(vcol >= vlayout.timeLine)) {
            // < dari timeline
            console.log('row kah', vrow);
            console.log('vchip kah', vchip);
            console.log("<timeline", vcol, vlayout.timeLine);

            console.log("vcol=>", vcol);
            console.log("vlayout.timeline=>", vlayout.timeLine);
            console.log("vchip.data.chip=>", vchip.data.chip);
            if (vchip.data.chip == 'jpcb2') {
                moveback = true;
            } else if (vchip.data.chip == 'estimasi') {
                moveback = true;
            } else if (vchip.data.chip == 'chip') {
                moveback = true;
            }

            //============ untuk chip dari stopage pindah ke board lagi=======
            if (vchip.data.chip != 'estimasi') {
                vchip.data.col = vcol;
                vchip.data.row = this.data.row;
            }
            // var vboard = vchip.container;
            // vboard.onDragDrop(vchip);
            //============================================
            //moveback = true;
            // this.x(vstartx);
            // this.y(vstarty);

        } else if (vlayout.cekMenumpuk(this, vx, vy)) {
            // menumpuk
            console.log("Menumpuk", this, vstartx, vstarty);
            console.log('move back 2');

            moveback = true;

            // this.x(vstartx);
            // this.y(vstarty);

            // this.x(this.dragStartPos.x);
            // this.y(this.dragStartPos.y);
        } else if (vlayout.cekBPChip(this, vx, vy)){
            if (this.data.chip = "jpcb2"){
                // cek itu BP boy
                console.log('ini cuma buat cek yg BP jam plot < jam chip sebelum')
                moveback = true;

            }

        } else if (vlayout.cekStall(this, vrow, vcol)) {
            console.log("dichek sih");
            moveback = true;
        } else {
            console.log("No Problem", this);
            console.log("vlayout.board.checkDragDrop", vlayout.board.checkDragDrop);
            vok = true;
            if (typeof vlayout.board.checkDragDrop == 'function') {
                vok = vlayout.board.checkDragDrop(this, vcol, vrow);
                console.log("vok", vok);
            }
            if (vok == true) {
                this.x(vx);
                this.y(vy);
                this.data.col = vcol;
                this.data.row = vrow;
            } else if (vok == false){
                console.log('move back 3', vcol, vrow);
                this.x(vx);
                this.y(vy);
                this.data.col = vcol;
                this.data.row = vrow;
                // moveback = true;

            } else if (vok == null){
                moveback = true;
            } else {
                console.log('move back 3', vcol, vrow);
                this.x(vx);
                this.y(vy);
                this.data.col = vcol;
                this.data.row = vrow;
                // moveback = true;
            }
            // this.x(vstartx);
            // this.y(vstarty);
        }
        // if (!moveback){
        //     console.log("!moveback");
        //     if (typeof vlayout.board.onCustomCheckDragDrop == 'function'){
        //         var vres = vlayout.board.onCustomCheckDragDrop(vchip);
        //         if (!vres){
        //             console.log("custom move back");
        //             moveback = true;
        //         }
        //     }
        // }
        // moveback = true;
        if (moveback) {
            console.log("do moveback", vchip);
            this.x(vstartx);
            this.y(vstarty);
            var vboard = vchip.container;
            if (vchip.dragStartPos.row == -1) {
                vchip.moveTo(vchip.container.board.stoppage);
            }
            if (vchip.dragStartPos.row == -2) {
                vchip.moveTo(vchip.container.board.selectbox);
                if (vboard.layout.updateContainerLayout) {
                    vboard.layout.updateContainerLayout();
                }
                // } else {
                //     vchip.moveTo(vchip.container.board.chips);
            }
            if (typeof vboard.onDragCancel == 'function') {
                vboard.onDragCancel(vchip);
            }
        } else {
            ///////
            console.log("dont move back");
            var vboard = vchip.container;
            vboard.updateChip(vchip);
            if (typeof vboard.onDragDrop == 'function') {
                vboard.onDragDrop(vchip);
            }
        }
        // if ((vy!==false) && (vx!==false) && (vcol>vlayout.timeLine)){
        //     this.x(vx);
        //     this.y(vy);
        //     this.data.col = vcol;
        //     this.data.row = vrow;
        // } else {
        //     this.x(this.dragStartPos.x);
        //     this.y(this.dragStartPos.y);
        // }
        this.container.redraw();
        // vlayout.cekStall(this, vx, vy);
        //var vcol = this.x2col(this.x())
        // var rowcol = this.calcRowCol(this.x(), this.y());
        // console.log('rowcol', rowcol);
    })
    vchip.on('dragstart', function(evt) {
        // return;
        var vTime = new Date();
        var xrow = vchip.data.row;
        // if (vchip.stoppage){
        //     xrow = -1;
        // }
        vchip.dragStartPos = {
            x: this.x(),
            y: this.y(),
            time: vTime.getTime(),
            col: vchip.data.col,
            row: xrow
        }
        vchip.moveToTop();
        vchip.moveTo(vchip.container.dragGroup);
        var vdx = vchip.container.board.scrollable.x();
        var vxx = vchip.x() + vdx;
        vchip.x(vxx);
        console.log(vchip.container);
        console.log("drag start", vchip.dragStartPos);
    })
}

JsBoard.Layouts.jpcb.prototype.calcRowCol = function(vx, vy) {
    // vcol = vy-board.
    // return {col: vcol, row: vrow}
}

JsBoard.Layouts.jpcb.prototype.autoResizeStage = function(vjsboard) {
    var vcontainer = vjsboard.config.board.container;
    var vobj = document.getElementById(vcontainer);
    if (vobj) {
        var vwidth = vobj.clientWidth;
        if (vwidth > 0) {
            vjsboard.stage.setWidth(vwidth);
            vjsboard.config.board.width = vwidth;
            this.stage.width = vwidth;
            this.stageWidth = vwidth;
            // this.boardWidth = vwidth;
            this.drawFixFooter(vjsboard.board.fixedFooter);
            this.drawFixHeader(vjsboard.board.fixedHeader);
        }
    }
}