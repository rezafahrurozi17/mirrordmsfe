var testing_sound_lib = function(){
	alert('test');

}

var JsSprite = {
		1: [53000, 700],
		2: [55700, 600],
		3: [57800, 600],
		4: [59500, 500],
		5: [61000, 750],
		6: [62500, 650],
		7: [64500, 600],
		8: [66600, 820],
		9: [68600, 800],
		0: [70100, 700],
		A: [940, 500],
		B: [3200, 550],
		C: [4900, 480],
		D: [6200, 520],
		E: [7800, 500],
		F: [10000, 480],
		G: [11800, 500],
		H: [13600, 480],
		I: [16000, 500],
		J: [18000, 520],
		K: [19800, 480],
		L: [21800, 500],
		M: [23700, 480],
		N: [25800, 550],
		O: [28000, 550],
		P: [30000, 550],
		Q: [32100, 460],
		R: [33950, 450],
		S: [36100, 550],
		T: [38300, 440],
		U: [40400, 450],
		V: [42500, 560],
		W: [44200, 480],
		X: [46100, 650],
		Y: [48700, 500],
		Z: [50300, 500],
		'@': [76249, 1260],
		'#': [79550, 1400],
		' ': [0, 20],
		'-': [0, 500],
	}

Howl.prototype.doPlayString = function(str){
	if(str.length>1){
		this.once('end', function(){
			this.doPlayString(str.substring(1,str.length));
		});                
	}
	if(str.length>0){
		this.play(str.substring(0,1));
	}
};

Howl.prototype.playString = function(str){
	// string preprocessing
	var vstr = '';
	var varr = str.split('');
	for (var i=0; i<varr.length; i++){
		var vs = varr[i];
		if (typeof JsSprite[vs] !== 'undefined'){
			vstr = vstr+vs;
		} else {
			vstr = vstr+' ';
		}
	}
	if (vstr!==''){
		this.doPlayString(vstr);
	}
}	
var JsSoundFile = 'sound/boards/sound-sprite.mp3';
var JsSound = new Howl({
	src: [JsSoundFile],
	sprite: JsSprite
});

