//// Layout for status Board
//// dependent on layouts.js
JsBoard.Layouts.queue = function(config){
    this.parent = JsBoard.Layouts.base;
    // call parent's constructor
    this.parent.call(this, config);
    //JsBoard.Layouts.base.call(this);

}

// inherit status board from base board
JsBoard.Layouts.queue.prototype = Object.create(JsBoard.Layouts.base.prototype);

// correct constructor pointer because it points to parent
JsBoard.Layouts.queue.prototype.constructor = JsBoard.Layouts.status;

JsBoard.Layouts.queue.prototype.createStage = function(){
    vconfig = this.stage; // <--- config.stage
    vcontainer = this.board.container;

    result = new Konva.Stage({
      container: vcontainer,
      width: vconfig.width,
      height: vconfig.height
    });
    return result;

}

JsBoard.Layouts.queue.prototype.initLayout = function(){
    this.parent.prototype.initLayout.call(this);
    var defconfig = {
        headerHeight: 100,
        subHeaderHeight: 40,
        rowHeight: 26,
        chipItemHeight: 64,
        listWidth: 240,
        footerHeight: 160,
        counterHeaderHeight: 30,
        lastCallHeight: 24,
        runningTextHeight: 24,
        maxCounterWidth: 120,
        counterMargin: 20,
    }
    this.board = JsBoard.Common.extend(defconfig, this.board);
}

JsBoard.Layouts.queue.prototype.chipDimension = function(vitem){
    var result = {}
    if (vitem.lokasi=='header'){
        result = {
            x: 0,
            y: 0,
            width: this.stage.width,
            height: this.board.headerHeight,
        }
    } else if (vitem.lokasi=='subheader'){
        result = {
            x: 0,
            y: this.board.headerHeight,
            width: this.stage.width,
            height: this.board.subHeaderHeight,
        }
    } else if (vitem.lokasi=='row'){
        result = {
            x: 0,
            y: this.board.headerHeight+this.board.subHeaderHeight+(vitem.index-1)*this.board.rowHeight,
            width: this.stage.width,
            height: this.board.rowHeight,
        }
    } else if (vitem.lokasi=='column'){
        result = {
            x: vitem.calcLeft,
            y: this.board.headerHeight,
            width: vitem.calcWidth,
            height: this.stage.height-this.board.headerHeight-this.board.footerHeight,
        }
    } else if (vitem.lokasi=='column2'){
        result = {
            x: vitem.calcLeft,
            y: this.board.headerHeight+this.board.subHeaderHeight,
            width: vitem.calcWidth,
            height: this.stage.height-this.board.headerHeight-this.board.footerHeight-this.board.subHeaderHeight,
        }
    } else if (vitem.lokasi=='columntitle'){
        result = {
            x: vitem.calcLeft,
            y: this.board.headerHeight,
            width: vitem.calcWidth,
            height: this.board.subHeaderHeight,
        }
    } else if (vitem.lokasi=='counterheader'){
        result = {
            x: 0,
            y: this.stage.height-this.board.footerHeight,
            width: this.stage.width,
            height: this.board.counterHeaderHeight,
        }
    } else if (vitem.lokasi=='counterbg'){
        result = {
            x: 0,
            y: this.stage.height-this.board.footerHeight+this.board.counterHeaderHeight,
            width: this.stage.width,
            height: this.board.footerHeight-this.board.lastCallHeight-this.board.runningTextHeight-this.board.counterHeaderHeight,
        }
    } else if (vitem.lokasi=='counter'){
        var vmargin = this.board.counterMargin;
        var maxwidth = this.board.maxCounterWidth;
        var ctrCount = 1;
        if (this.chip.counterCount){
            ctrCount = this.chip.counterCount;
        }

        var vspace = (this.stage.width-(vmargin*2))/ctrCount;
        if (vspace<maxwidth){
            vwidth = vspace;
        } else {
            maxwidth;
        }
        // vwidth = vspace;
        var vpadding = Math.round((vspace-vwidth)/2);
        var xheight = this.board.footerHeight-this.board.lastCallHeight-this.board.runningTextHeight-this.board.counterHeaderHeight;
        var vheight = Math.round(vwidth*0.53);
        var xpad = (xheight-vheight)/2;
        result = {
            x: (vitem.index-1)*vspace+vmargin+vpadding,
            y: (this.stage.height-this.board.footerHeight+this.board.counterHeaderHeight)+xpad,
            width: vwidth,
            height: vheight,
        }
    } else if (vitem.lokasi=='lastcall'){
        result = {
            x: 0,
            y: this.stage.height-this.board.lastCallHeight-this.board.runningTextHeight,
            width: this.stage.width,
            height: this.board.lastCallHeight,
        }
    } else if (vitem.lokasi=='runningtext'){
        result = {
            x: 0,
            y: this.stage.height-this.board.runningTextHeight,
            width: this.stage.width,
            height: this.board.runningTextHeight,
        }
    } else if (vitem.lokasi=='nopol'){
        if (typeof this.columnInfo[vitem.col] !== 'undefined'){
            var vcol = this.columnInfo[vitem.col];
            var vrow = 0;
            if (typeof vitem.row != 'undefined'){
                vrow = vitem.row;
            }
            result = {
                x: vcol.left,
                y: this.board.headerHeight+this.board.subHeaderHeight+(vrow-1)*this.board.rowHeight+vcol.top,
                width: vcol.width,
                height: this.board.rowHeight,
            }
        } else {
            result = false;
        }
    } else if (vitem.lokasi=='listbg'){
        result = {
            x: this.stage.width-this.board.listWidth,
            y: this.board.headerHeight+this.board.subHeaderHeight,
            width: this.board.listWidth,
            height: this.stage.height-this.board.subHeaderHeight-this.board.headerHeight-this.board.footerHeight,
        }
    } else if (vitem.lokasi=='listitem'){
        if (typeof vitem.indexByPage == 'undefined'){
            vitem.indexByPage = 1;
        }
        result = {
            x: this.stage.width-this.board.listWidth,
            y: this.board.headerHeight+this.board.subHeaderHeight+this.board.chipItemHeight*(vitem.indexByPage),
            width: this.board.listWidth,
            height: this.board.chipItemHeight,
        }
    } else if (vitem.lokasi=='footer'){
        result = {
            x: 0,
            y: this.stage.height-this.board.footerHeight,
            width: this.stage.width,
            height: this.board.footerHeight,
        }
    }
    return result;
}

JsBoard.Layouts.queue.prototype.prepareBoardLayout = function(vjsboard){
    console.log('prepare board layout');
    var vchips = this.container.chipItems;  
    this.columns = []
    this.totalColWidths = 0;
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if ((vitem.lokasi=='column') || (vitem.lokasi=='column2')){
            if (!vitem.width){
                vitem.width = 1;
            }
            this.totalColWidths = this.totalColWidths+vitem.width;
        }
    }
    var stageWidth = this.stage.width;
    var vleft = 0;
    this.columnInfo = []
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if ((vitem.lokasi=='column') || (vitem.lokasi=='column2')){
            var vtop = 0;
            if (vitem.lokasi=='column2'){
                vtop = this.board.rowHeight;
            }
            var xwidth = vitem.width*stageWidth/this.totalColWidths;
            vitem.calcWidth = xwidth;
            vitem.calcLeft = vleft;
            vleft = vleft+xwidth;
            this.columnInfo.push({left: vitem.calcLeft, width: vitem.calcWidth, top: vtop})
        }
    }

    vCounterCount = 0;
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.lokasi=='counter'){
            vCounterCount = vCounterCount+1;
        }
    }
    this.chip.counterCount = vCounterCount;

    var vleft = 0;
    for (var i=0; i<vchips.length; i++){
        var vitem = vchips[i];
        if (vitem.lokasi=='columntitle'){
            var xwidth = vitem.width*stageWidth/this.totalColWidths;
            vitem.calcWidth = xwidth;
            vitem.calcLeft = vleft;
            vleft = vleft+xwidth;
        }
    }

}

JsBoard.Layouts.queue.prototype.autoResizeStage = function(vjsboard){
    var vcontainer = vjsboard.config.board.container;
    var vobj = document.getElementById(vcontainer);
    if(vobj){
        var vwidth = vobj.clientWidth;
        if (vwidth>0){
            vjsboard.stage.setWidth(vwidth);
            vjsboard.config.board.width = vwidth;
            // vjsboard.stage.width = vwidth;
            var xscale = vwidth/this.stage.width;
            console.log("scale: ", xscale);
            vjsboard.stage.scaleX(xscale);
            vjsboard.stage.scaleY(xscale);
            vjsboard.stage.setHeight(xscale*this.stage.height);
            // vjsboard.board.scrollable.position({x: 0, y:0})
            // vjsboard.stage.height = this.stage.height*xscale;

            this.updateContainerLayout(vjsboard);
            vjsboard.redraw();
        }
    }
}