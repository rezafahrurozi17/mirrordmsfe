// copy dari default
JsBoard.Chips.jpcb_bp = JsBoard.Common.clone(JsBoard.Chips.jpcbdefault);

// var jpcbBaseColorMap = {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
//     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#ffA520', done: '#787878'}
// var jpcbLineColorMap = {default: '#000', belumdatang: '#000', sudahdatang: '#C0C0C0', booking: '#4169E1', 
//     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#ffA520', done: '#787878'}


// JsBoard.Chips.jpcb_bp.footer = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 1, right: 1, top: 0, bottom: 1}
//     },
//     // {name: 'rect1', type: 'rect', x: 0, y:0, 
//     //     width: 50, height: 30,
//     //     fill: '#fff', stroke: '#000', strokeWidth: 0.7,
//     //     margin: {left: 7, right: 7, top: 7, bottom: 7}
//     // },
//     // {name: 'text1', type: 'text', x: 50, y:0, 
//     //     width: 90, height: 30,
//     //     text: 'Appointment', 
//     //     fill: '#000',
//     //     wrap: 'none',
//     //     margin: {left: 0, right: 0, top: 10, bottom: 0}
//     // },
//   ]
// }

JsBoard.Chips.jpcb_bp.footer_stopage = {
    items: [{
        name: 'base',
        type: 'rect',
        x: 0,
        y: 0,
        width: { data: 'chip', field: 'width' },
        height: 30,
        xheight: { data: 'chip', field: 'height' },
        fill: '#fff',
        stroke: '#777',
        cornerRadius: 2,
        strokeWidth: 1,
        margin: { left: 1, right: 1, top: 0, bottom: 1 }
    }, ]
}

var itemJPCB_footer_list = [
    { name: 'belumdatang', text: 'Belum Datang' },
    { name: 'sudahdatang', text: 'Sudah Datang' },
    { name: 'booking', text: 'Booking' },
    { name: 'walkin', text: 'Walk In' },
    { name: 'inprogress', text: 'In Progress' },
    { name: 'irregular', text: 'Irregular' },
    { name: 'extension', text: 'Extension' },
    { name: 'done', text: 'Done' },
]

/*var vleft=0;
for (var i=0; i<itemJPCB_footer_list.length; i++){
    var vitem = itemJPCB_footer_list[i];
    var vwidth = vitem.text.length * 9+30;
    JsBoard.Chips.jpcb_bp.footer.items.push({
        name: 'rect-'+i, type: 'rect', x: vleft, y:0, 
        width: 50, height: 30,
        fill: jpcbBaseColorMap[vitem.name], 
        stroke: jpcbLineColorMap[vitem.name], 
        strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    });
    JsBoard.Chips.jpcb_bp.footer.items.push({name: 'text1', type: 'text', x: vleft+50, y:0, 
        width: vwidth, height: 30,
        text: vitem.text, 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    });
    JsBoard.Chips.jpcb_bp.footer_stopage.items.push({
        name: 'rect-'+i, type: 'rect', x: vleft, y:0, 
        width: 50, height: 30,
        fill: jpcbBaseColorMap[vitem.name], 
        stroke: jpcbLineColorMap[vitem.name], 
        strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    });
    JsBoard.Chips.jpcb_bp.footer_stopage.items.push({name: 'text1', type: 'text', x: vleft+50, y:0, 
        width: vwidth, height: 30,
        text: vitem.text, 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    });
    vleft = vleft+vwidth+20;
    
}
*/

// JsBoard.Chips.jpcb_bp.footer_stopage = JsBoard.Common.extend({}, JsBoard.Chips.jpcb_bp.footer);

JsBoard.Chips.jpcb_bp.footer_stopage.items.push({
    name: 'separator',
    type: 'rect',
    x: 0,
    y: 30,
    width: { data: 'chip', field: 'width' },
    height: 5,
    xheight: { data: 'chip', field: 'height' },
    fill: '#fff',
    margin: { left: 0, right: 0, top: 0, bottom: 0 }
})
JsBoard.Chips.jpcb_bp.footer_stopage.items.push({
    name: 'jobstoppage',
    type: 'rect',
    x: 0,
    y: 35,
    width: { data: 'chip', field: 'width' },
    height: function(base, name, data, me) {
        return data['chip']['height'] - 30 - 5;
    },
    xheight: { data: 'chip', field: 'height' },
    fill: '#fff',
    stroke: '#777',
    cornerRadius: 2,
    strokeWidth: 1,
    margin: { left: 1, right: 1, top: 0, bottom: 1 }
})
JsBoard.Chips.jpcb_bp.footer_stopage.items.push({
    name: 'jobstoppagetitle',
    type: 'text',
    x: 0,
    y: 40,
    width: { data: 'chip', field: 'width' },
    height: 30,
    fontSize: 20,
    // text: 'Job Stoppage',
    text: function(base, name, data, me) {
        var JobOnStoppage = 0;
        for(var i=0; i<me.chipItems.length; i++){
            // if(me.chipItems[i].intStatus == 2 && me.chipItems[i].row == -1 && me.chipItems[i].stallId == -1){
            if(me.chipItems[i].row == -1 && me.chipItems[i].stallId == -1){
                JobOnStoppage = JobOnStoppage + 1;
            }
        }
        console.log('ada apa dengan dms',me);
        return 'Job Stoppage' + ' ('+ JobOnStoppage +')';
    },

    fill: '#000',
    wrap: 'none',
    margin: { left: 8, right: 8, top: 10, bottom: 0 }
})
JsBoard.Chips.jpcb_bp.footer_stopage.items.push({
    name: 'stoppagearea',
    type: 'rect',
    x: 0,
    y: 80,
    width: { data: 'chip', field: 'width' },
    height: function(base, name, data, me) {
        return data['chip']['height'] - 30 - 50;
    },
    xheight: { data: 'chip', field: 'height' },
    fill: '#f5f5f5',
    stroke: '#777',
    cornerRadius: 2,
    strokeWidth: 1,
    margin: { left: 1, right: 1, top: 0, bottom: 1 }
})




JsBoard.Chips.jpcb_bp.stall = {
    items: [{
            name: 'base',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: function(base, name, data, me) {
                if (data['data']['index'] % 2 == 0) {
                    return '#fafafa'
                } else {
                    return '#ddd'
                }
            },
            margin: { left: 0, right: 0, top: 0, bottom: 0 }
        },
        {
            name: 'text',
            type: 'text',
            text: { data: 'data', field: 'title' },
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            fontSize: 14,
            margin: { left: 10, top: 5, right: 5, bottom: 5 },
            fill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            colorMap: { default: '#000', normal: '#33b', blank: '#aaa', blocked: '#a00' },
        },
        {
            name: 'text',
            type: 'text',
            text: { data: 'data', field: 'person' },
            x: 0,
            y: 24,
            width: { data: 'chip', field: 'width' },
            fontSize: 14,
            margin: { left: 10, top: 5, right: 5, bottom: 5 },
            fill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            colorMap: { default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00' },
        },
    ]
}

JsBoard.Chips.jpcb_bp.chip = {
    items: [{
            name: 'base',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 1, right: 1, top: 2, bottom: 2 }
        },
        {
            name: 'blink',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: null,
            stroke: '#0f0',
            cornerRadius: 2,
            strokeWidth: 2,
            visible: false,
            margin: { left: 1, right: 1, top: 2, bottom: 2 }
        },
        {
            name: 'nopol',
            type: 'text',
            text: { data: 'data', field: 'nopol' },
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            wrap: 'none',
            fontStyle: 'bold',
            fontSize: function(base, name, data, me) {
                if (data['data']['width'] > 0.5) {
                    return 11
                } else {
                    return 10;
                }
            }
        },

        {
            name: 'jamcetakwo',
            type: 'text',
            text: { data: 'data', field: 'jamcetakwo' },
            x: 0,
            y: 0,
            align: 'right',
            width: { data: 'chip', field: 'width' },
            visible: function(base, name, data, me) {
                if (data['data']['width'] > 0.5) {
                    return true
                } else {
                    return false;
                }
            }
        },
        { name: 'type', type: 'text', text: { data: 'data', field: 'type' }, x: 0, y: 13, width: { data: 'chip', field: 'width' }, wrap: 'none' },
        // {name: 'start2', type: 'text', text: {data: 'data', field: 'start2'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}},
        {
            name: 'jamjanjiserah',
            type: 'text',
            text: { data: 'data', field: 'jamjanjiserah' },
            x: 0,
            y: 13,
            align: 'right',
            width: { data: 'chip', field: 'width' },
            visible: function(base, name, data, me) {
                if (data['data']['width'] > 0.5) {
                    return true
                } else {
                    return false;
                }
            }
        },
        { name: 'pekerjaan', type: 'text', text: { data: 'data', field: 'pekerjaan' }, x: 0, y: 51, width: { data: 'chip', field: 'width' } },
        {
            name: 'teknisi',
            type: 'text',
            text: { data: 'data', field: 'teknisi' },
            x: 30,
            y: 51,
            align: 'right',
            wrap: 'none',
            xwidth: { data: 'chip', field: 'width' },
            width: function(base, name, data, me) {
                return data['chip']['width'] - this.x;
            }
        },
        {
            name: 'normal',
            type: 'rect',
            x: 0,
            y: 32,
            width: function(base, name, data, me) {
                console.log('chip-rect-2', data['chip']['width'])
                var vext = 0;
                var vwidth = 1;
                var vcolwidth = 50;
                var vnormal = 1;
                if (typeof data['chip']['columnWidth'] !== 'undefined') {
                    vcolwidth = data['chip']['columnWidth'];
                }
                if (typeof data['data']['normal'] !== 'undefined') {
                    vnormal = data['data']['normal'];
                }
                return vcolwidth * vnormal;
            },
            height: 21,
            fill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            xxfill: '#f00',
            colorMap: jpcbBaseColorMap,
            //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
            //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
            xxmargin: { left: 0, right: 0, top: 0, bottom: 0 },
            margin: function(base, name, data, me) {
                var vext = 0;
                if (typeof data['data']['ext'] !== 'undefined') {
                    vext = data['data']['ext'];
                }
                if (vext > 0) {
                    return { right: 0, top: 0, bottom: 0 }
                } else {
                    return { top: 0, bottom: 0 }
                }
            }
        },
        {
            name: 'ext',
            type: 'rect',
            x: 0,
            y: 32,
            x: function(base, name, data, me) {
                var vnormal = 0;
                var vcolwidth = 50;
                if (typeof data['data']['normal'] !== 'undefined') {
                    vnormal = data['data']['normal'];
                }
                if (typeof data['chip']['columnWidth'] !== 'undefined') {
                    vcolwidth = data['chip']['columnWidth'];
                }
                return vcolwidth * vnormal;
            },
            width: function(base, name, data, me) {
                var vext = 0;
                var vcolwidth = 50;
                if (typeof data['data']['ext'] !== 'undefined') {
                    vnormal = data['data']['ext'];
                }
                if (typeof data['chip']['columnWidth'] !== 'undefined') {
                    vcolwidth = data['chip']['columnWidth'];
                }
                return vcolwidth * vnormal;
            },
            visible: function(base, name, data, me) {
                var vext = 0;
                if (typeof data['data']['ext'] !== 'undefined') {
                    vext = data['data']['ext'];
                }
                if (vext > 0) {
                    return 1;
                } else {
                    return 0
                }
            },
            height: 21,
            fill: '#ffA520',
            margin: function(base, name, data, me) {
                var vext = 0;
                if (typeof data['data']['ext'] !== 'undefined') {
                    vext = data['data']['ext'];
                }
                if (vext > 0) {
                    return { left: 0, top: 0, bottom: 0 }
                } else {
                    return { top: 0, bottom: 0 }
                }
            }
        },
        {
            name: 'main_icons',
            type: 'icons',
            x: 0,
            y: 26,
            icons: { data: 'data', field: 'statusicon', default: [] },
            width: function(base, name, data, me) {
                console.log('chip-rect-2 amk', data['chip']['width'])
                var vext = 0;
                var vwidth = 1;
                var vcolwidth = 50;
                var vnormal = 1;
                if (typeof data['chip']['columnWidth'] !== 'undefined') {
                    vcolwidth = data['chip']['columnWidth'];
                }
                if (typeof data['data']['normal'] !== 'undefined') {
                    vnormal = data['data']['normal'];
                }
                return vcolwidth * vnormal;
            },
        },
        {
            name: 'bottom_icon',
            type: 'icons',
            xxx: 25,
            x: function(base, name, data, me) {
                console.log('data chip bp 1', data);
                var vext = 0;
                var vwidth = 1;
                var vcolwidth = 50;
                var vnormal = 1;
                if (typeof data['chip']['columnWidth'] !== 'undefined') {
                    vcolwidth = data['chip']['columnWidth'];
                }
                if (typeof data['data']['normal'] !== 'undefined') {
                    vnormal = data['data']['normal'];
                }
                if (typeof data['data']['ext'] !== 'undefined') {
                    vext = data['data']['ext'];
                }
                vwidth = vnormal + vext;
                return Math.round(vwidth * vcolwidth / 2) - 14;
            },
            y: 46,
            icons: ['play2'],
            width: 20,
            height: 16,
            iconHeight: 14,
            iconWidth: 14,
            visible: function(base, name, data, me) {
                console.log('data chip bp 2', data);
                vallocated = 1;
                if (typeof data['data']['allocated'] !== 'undefined') {
                    vallocated = data['data']['allocated'];
                }
                if (vallocated) {
                    return false;
                } else {
                    return true;
                }
            }
        },
    ]
}

// {name: 'base2', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 300, xheight: {data: 'chip', field: 'height'},
//     fill: '#dd0', stroke: '#000',
//     margin: {left: 0, right: 0, top: 0, bottom: 0}
// },

JsBoard.Chips.jpcb_bp.header = {
    items: [{
            name: 'base',
            type: 'rect',
            x: 0,
            y: { data: 'chip', field: 'titleTop' },
            width: { data: 'chip', field: 'width' },
            height: function(base, name, data, me) {
                return data['chip']['height'] - data['chip']['titleTop'];
            },
            fill: '#ccc',
            margin: { left: 0, right: 0, top: 0, bottom: 0 }
        },
        {
            name: 'text',
            type: 'text',
            text: { data: 'data', field: 'title' },
            x: 0,
            y: { data: 'chip', field: 'titleTop' },
            width: { data: 'chip', field: 'width' },
            fontSize: 14,
            margin: { left: 5, top: 10, right: 5, bottom: 5 },
            fill: '#000',
            align: 'center',
        },
    ]
}

JsBoard.Chips.jpcb_bp.first_header = {
    items: [{
            name: 'base',
            type: 'rect',
            x: 0,
            y: { data: 'chip', field: 'titleTop' },
            width: { data: 'chip', field: 'width' },
            height: function(base, name, data, me) {
                return data['chip']['height'] - data['chip']['titleTop'];
            },
            fill: '#ccc',
            margin: { left: 0, right: 0, top: 0, bottom: 0 }
        },
        {
            name: 'text',
            type: 'text',
            text: { data: 'data', field: 'text' },
            x: 0,
            y: { data: 'chip', field: 'titleTop' },
            width: { data: 'chip', field: 'width' },
            fontSize: 14,
            margin: { left: 15, top: 10, right: 5, bottom: 5 },
            fill: '#000',
        },
    ]
}

JsBoard.Chips.jpcb_bp.fix_header = {
    items: [{
            name: 'selectarea',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 2,
            opacity: 0.5,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        }
        // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        //     fill: '#440', opacity: 0.5,
        //     margin: {left: 0, right: 0, top: 0, bottom: 0}
        // },
        // {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        //     margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        //     fill: '#fff', align: 'center',
        // },
    ]
}

JsBoard.Chips.jpcb_bp.blank = {
    items: [{
            name: 'base',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: '#eee',
            stroke: '#777',
            strokeWidth: 1,
            margin: { left: 0, right: 0, top: 0, bottom: 0 }
        },

    ]
}


JsBoard.Chips.jpcb_bp.jpcb_header = {
    items: [{
            name: 'base',
            type: 'rect',
            x: 0,
            y: { data: 'chip', field: 'subtitleTop' },
            width: { data: 'chip', field: 'width' },
            height: function(base, name, data, me) {
                return data['chip']['height'] - data['chip']['subtitleTop'];
            },
            fill: '#bb8',
            margin: { left: 0, right: 0, top: 0, bottom: 0 }
        },
        {
            name: 'text',
            type: 'text',
            text: { data: 'data', field: 'text' },
            x: 0,
            y: { data: 'chip', field: 'subtitleTop' },
            width: { data: 'chip', field: 'width' },
            fontSize: 14,
            margin: { left: 5, top: 10, right: 5, bottom: 5 },
            fill: '#000',
            align: 'center',
        },
        {
            name: 'parentBase',
            type: 'rect',
            x: 0,
            visible: function(base, name, data, me) {
                if (data['data']['newDay']) {
                    return true;
                }
                return false;
            },
            y: { data: 'chip', field: 'titleTop' },
            width: function(base, name, data, me) {
                return data['chip']['width'] * data['data']['subCount'];
            },
            height: function(base, name, data, me) {
                return data['chip']['height'] - data['chip']['subtitleTop'];
            },
            fill: '#eee',
            stroke: '#888',
            margin: { left: 0, right: 0, top: 0, bottom: 1 }
        },
        {
            name: 'parentText',
            type: 'text',
            text: { data: 'data', field: 'parentText' },
            x: 0,
            y: { data: 'chip', field: 'titleTop' },
            width: function(base, name, data, me) {
                return data['chip']['width'] * data['data']['subCount'];
            },
            align: 'center',
            fontSize: 18,
            margin: { left: 5, top: 10, right: 5, bottom: 5 },
            fill: '#000',
        },
    ]
}


JsBoard.Chips.jpcb_bp.jpcb_first_header = {
    items: [{
            name: 'base',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: '#ddd',
            xfill: function(base, name, data, me) {
                if (data['data']['index'] % 2 == 0) {
                    return '#ddd'
                } else {
                    return '#fff'
                }
            },
            margin: { left: 0, right: 0, top: 0, bottom: 0 }
        },
        {
            name: 'base2',
            type: 'rect',
            x: 0,
            y: { data: 'chip', field: 'subtitleTop' },
            width: { data: 'chip', field: 'width' },
            height: function(base, name, data, me) {
                return data['chip']['height'] - data['chip']['subtitleTop'];
            },
            fill: '#bbb',
            margin: { left: 0, right: 0, top: 0, bottom: 0 }
        },
        {
            name: 'base3',
            type: 'rect',
            x: 0,
            y: { data: 'chip', field: 'titleTop' },
            width: { data: 'chip', field: 'width' },
            height: function(base, name, data, me) {
                return data['chip']['height'] - data['chip']['subtitleTop'];
            },
            fill: '#eee',
            stroke: '#888',
            margin: { left: 0, right: 0, top: 0, bottom: 1 }
        },
        {
            name: 'maintitle',
            type: 'text',
            text: { data: 'chip', field: 'teamName' },
            x: 0,
            y: { data: 'chip', field: 'titleTop' },
            width: { data: 'chip', field: 'width' },
            fontSize: 24,
            margin: { left: 15, top: 10, right: 5, bottom: 5 },
            align: 'center',
            fill: '#338',
        },
        {
            name: 'subtitle1',
            type: 'text',
            xtext: { data: 'data', field: 'subtitle1' },
            text: 'STALL',
            x: 0,
            y: { data: 'chip', field: 'subtitleTop' },
            width: { data: 'chip', field: 'width' },
            fontSize: 14,
            margin: { left: 5, top: 10, right: 5, bottom: 5 },
            fill: '#000',
        },
        {
            name: 'subtitle1',
            type: 'text',
            xtext: { data: 'data', field: 'subtitle2' },
            text: 'Tech',
            x: function(base, name, data, me) {
                return data['chip']['width'] - data['chip']['stallTechWidth'];
            },
            y: { data: 'chip', field: 'subtitleTop' },
            width: { data: 'chip', field: 'stallTechWidth' },
            fontSize: 14,
            margin: { left: 5, top: 10, right: 5, bottom: 5 },
            fill: '#000',
        },
        // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
        //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
        //     bullet: '-',
        //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
        //     margin: {left: 25}
        // },
    ]
}

JsBoard.Chips.jpcb_bp.jpcb_stall = {
    items: [{
            name: 'base',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: function(base, name, data, me) {
                if (data['data']['index'] % 2 == 0) {
                    return '#fff'
                    // return '#ddd' // sini bill
                } else {
                    return '#ddd' // sini bill
                    // return '#fff'
                }
            },
            margin: { left: 0, right: 0, top: 0, bottom: 0 }
        },
        {
            name: 'text',
            type: 'text',
            text: { data: 'data', field: 'title' },
            x: 0,
            y: 0,
            width: function(base, name, data, me) {
                return data['chip']['width'] - data['chip']['stallTechWidth'];
            },
            fontSize: 14,
            margin: { left: 5, top: 25, right: 5, bottom: 25 },
            fill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            colorMap: { default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00' },
        },
        {
            name: 'technician',
            type: 'text',
            text: { data: 'data', field: 'technician' },
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'stallTechWidth' },
            x: function(base, name, data, me) {
                return data['chip']['width'] - data['chip']['stallTechWidth'];
            },
            fontSize: 14,
            margin: { left: 5, top: 25, right: 5, bottom: 25 },
            fill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            colorMap: { default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00' },
        },
        // sini bill
        // {name: 'base_filler_left', type: 'rect', x: 0, y:0, 
        //     visible: function(base, name, data, me){
        //         if (data['data']['leftMargin']> 0){
        //             return true;
        //         }
        //         return false;
        //     },
        //     width: function(base, name, data, me){
        //         console.log('data chip filler', data['chip'])
        //         if (data['chip']['incrementMinute']){
        //             console.log('filler left', data['chip']['columnWidth']+data['data']['leftMargin']*data['chip']['columnWidth']/data['chip']['incrementMinute'])
        //             return data['chip']['columnWidth']+data['data']['leftMargin']*data['chip']['columnWidth']/data['chip']['incrementMinute'];
        //         }
        //         return 4;
        //     },
        //     height: {data: 'chip', field: 'height'},
        //     opacity: 0.7,
        //     fill: '#aaa',
        //     margin: {left: 0, right: 0, top: 0, bottom: 0}
        // },

        // {name: 'base_filler_right', type: 'rect', 
        //     //x: 3000,
        //     x: function(base, name, data, me){
        //         if (data['chip']['incrementMinute']){
        //             console.log('filler x', data['chip']['columnWidth']+data['data']['rightMarginPos']*data['chip']['columnWidth']/data['chip']['incrementMinute'])
        //             return data['chip']['columnWidth']+data['data']['rightMarginPos']*data['chip']['columnWidth']/data['chip']['incrementMinute'];
        //         }
        //         return 4;
        //     },
        //     y:0, 
        //     visible: true,
        //     xvisible: function(base, name, data, me){
        //         if (data['data']['leftMargin']> 0){
        //             return true;
        //         }
        //         return false;
        //     },
        //     width: function(base, name, data, me){
        //         if (data['chip']['incrementMinute']){
        //             console.log('filler right', data['chip']['columnWidth']+data['data']['rightMargin']*data['chip']['columnWidth']/data['chip']['incrementMinute'])                
        //             return data['chip']['columnWidth']+data['data']['rightMargin']*data['chip']['columnWidth']/data['chip']['incrementMinute'];
        //         }
        //         return 4;
        //     },
        //     height: {data: 'chip', field: 'height'},
        //     opacity: 0.7,
        //     fill: '#aaa',
        //     margin: {left: 0, right: 0, top: 0, bottom: 0}
        // },
        // {name: 'base_break_4', type: 'rect', 
        //     //x: 3000,
        //     x: function(base, name, data, me){
        //         console.log('data chip filler', data['chip'])
        //         if (data['chip']['incrementMinute']){
        //             return data['chip']['columnWidth']+data['data']['startBreak4']*data['chip']['columnWidth']/data['chip']['incrementMinute']+120;
        //         }
        //         return 4;
        //     },
        //     y:0, 
        //     visible: function(base, name, data, me){
        //         if (data['data']['durationBreak4']> 0){
        //             return true;
        //         }
        //         return false;
        //     },
        //     width: function(base, name, data, me){
        //         if (data['chip']['incrementMinute']){
        //             return data['data']['durationBreak4']*data['chip']['columnWidth']/data['chip']['incrementMinute'];
        //         }
        //         return 4;
        //     },
        //     height: {data: 'chip', field: 'height'},
        //     opacity: 0.7,
        //     fill: 'RED',
        //     margin: {left: 0, right: 0, top: 0, bottom: 0}
        // },
        // {name: 'base_breaktext_4', type: 'text', 
        //     //x: 3000,
        //     x: function(base, name, data, me){
        //         if (data['chip']['incrementMinute']){
        //             return data['chip']['columnWidth']+data['data']['startBreak4']*data['chip']['columnWidth']/data['chip']['incrementMinute']+120;
        //         }
        //         return 4;
        //     },
        //     y:0, 
        //     visible: function(base, name, data, me){
        //         if (data['data']['durationBreak4']> 0){
        //             return true;
        //         }
        //         return false;
        //     },
        //     width: function(base, name, data, me){
        //         if (data['chip']['incrementMinute']){
        //             return data['data']['durationBreak4']*data['chip']['columnWidth']/data['chip']['incrementMinute'];
        //         }
        //         return 4;
        //     },
        //     height: {data: 'chip', field: 'height'},
        //     text: 'Chip Extend Menumpuk',
        //     fill: '#fff',
        //     margin: {left: 10, right: 0, top: 10, bottom: 0}
        // },
    ]
}


JsBoard.Chips.jpcb_bp.jpcb2 = {
    items: [
        //   {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        //     fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        //     margin: {left: 2, right: 2, top: 2, bottom: 2}
        //   },
        {
            name: 'base',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: null,
            stroke: '#ddd',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'base1',
            type: 'rect',
            x: 0,
            y: 0,

            visible: function(base, name, data, me) {
                if (data['data']['width1']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width1']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width1'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'base2',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start2']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start2'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width2']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width2']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width2'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal2',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start2']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start2'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width2']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width2']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width2'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },

        // =========== start multiple istrirapat ==========================
        {
            name: 'base3',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start3']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start3'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width3']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width3']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width3'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal3',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start3']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start3'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width3']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width3']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width3'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base4',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start4']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start4'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width4']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width4']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width4'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal4',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start4']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start4'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width4']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width4']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width4'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base5',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start5']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start5'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width5']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width5']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width5'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal5',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start5']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start5'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width5']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width5']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width5'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base6',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start6']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start6'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width6']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width6']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width6'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal6',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start6']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start6'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width6']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width6']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width6'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base7',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start7']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start7'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width7']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width7']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width7'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal7',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start7']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start7'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width7']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width7']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width7'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base8',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start8']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start8'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width8']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width8']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width8'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal8',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start8']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start8'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width8']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width8']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width8'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base9',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start9']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start9'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width9']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width9']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width9'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal9',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start9']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start9'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width9']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width9']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width9'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base10',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start10']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start10'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width10']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width10']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width10'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal10',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start10']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start10'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width10']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width10']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width10'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base11',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start11']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start11'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width11']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width11']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width11'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal11',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start11']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start11'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width11']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width11']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width11'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base12',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start12']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start12'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width12']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width12']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width12'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal12',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start12']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start12'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width12']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width12']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width12'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base13',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start13']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start13'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width13']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width13']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width13'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal13',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start13']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start13'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width13']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width13']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width13'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base14',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start14']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start14'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width14']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width14']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width14'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal14',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start14']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start14'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width14']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width14']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width14'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        {
            name: 'base15',
            type: 'rect',
            x: function(base, name, data, me) {
                if (data['data']['start15']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start15'] * vcolwidth;
                }
                return 0;
            },

            y: 0,
            visible: function(base, name, data, me) {
                if (data['data']['width15']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width15']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width15'] * vcolwidth;
                }
                return 50;
            },
            //cwidth: {data: 'chip', field: 'width1'}, 
            height: { data: 'chip', field: 'height' },
            fill: '#fff',
            stroke: '#777',
            cornerRadius: 2,
            strokeWidth: 1,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'normal15',
            type: 'rect',
            y: 36,
            x: function(base, name, data, me) {
                if (data['data']['start15']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['start15'] * vcolwidth;
                }
                return 0;
            },
            visible: function(base, name, data, me) {
                if (data['data']['width15']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width15']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width15'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            //   xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            margin: { left: 8, right: 8, top: 0, bottom: 0 }
        },


        // =========== end multiple istrirapat ==========================


        {
            name: 'blink',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: null,
            stroke: '#0f0',
            cornerRadius: 2,
            strokeWidth: 2,
            visible: false,
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'nopol',
            type: 'text',
            text: { data: 'data', field: 'nopol' },
            x: 4,
            y: 2,
            width: { data: 'chip', field: 'width' },
            wrap: 'none',
            fontStyle: 'bold',
            fontSize: function(base, name, data, me) {
                if (data['data']['width'] > 0.5) {
                    return 11
                } else {
                    return 10;
                }
            }
        },

        {
            name: 'tglcetakwo',
            type: 'text',
            text: { data: 'data', field: 'tgljanjiserah' },
            x: 4,
            y: 2,
            align: 'right',
            width: { data: 'chip', field: 'width' },
            margin: { right: 10 },
        },
        { name: 'type', type: 'text', text: { data: 'data', field: 'type' }, x: 4, y: 16, width: { data: 'chip', field: 'width' }, wrap: 'none' },
        {
            name: 'jamcetakwo',
            type: 'text',
            text: { data: 'data', field: 'jamjanjiserah' },
            x: 4,
            y: 16,
            align: 'right',
            width: { data: 'chip', field: 'width' },
            margin: { right: 10 },
        },
        {
            name: 'wotype',
            type: 'text',
            text: { data: 'data', field: 'workname' },
            x: 4,
            y: 54,
            align: 'right',
            width: { data: 'chip', field: 'width' },
            margin: { right: 10 },
        },
        //   {name: 'normal', type: 'rect', x: 0, y:36, 
        //     width: function(base, name, data, me){
        //         return data['chip']['width'];
        //     //   console.log('chip-rect-2', data['chip']['width'])
        //     //   var vext = 0;
        //     //   var vwidth = 1;
        //     //   var vcolwidth = 50;
        //     //   var vnormal = 1;
        //     //   if (typeof data['chip']['columnWidth']!=='undefined'){
        //     //       vcolwidth = data['chip']['columnWidth'];
        //     //   } 
        //     //   if (typeof data['data']['normal']!=='undefined'){
        //     //     vnormal = data['data']['normal'];
        //     //   }
        //     //   return vcolwidth*vnormal;
        //     }, 
        //     height: 21,
        //     xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        //     fill: function(base, name, data, me){
        //         return JsBoard.Common.RandomColorList[data['data']['color_idx']]
        //     },
        //     margin: {left: 8, right: 6, top: 0, bottom: 0}
        //   },
        {
            name: 'normal',
            type: 'rect',
            x: 0,
            y: 36,
            xwidth: { data: 'chip', field: 'width' },
            visible: function(base, name, data, me) {
                if (data['data']['width1']) {
                    return true
                }
                return false;
            },
            width: function(base, name, data, me) {
                if (data['data']['width1']) {
                    var vcolwidth = 50;
                    if (typeof data['chip']['columnWidth'] !== 'undefined') {
                        vcolwidth = data['chip']['columnWidth'];
                    }
                    return data['data']['width1'] * vcolwidth;
                }
                return 50;
            },
            height: 21,
            fill: function(base, name, data, me) {
                return JsBoard.Common.RandomColorList[data['data']['color_idx']]
            },
            colorMap: jpcbBaseColorMap,
            lineColorMap: asbLineColorMap,
            // xfill: '#fff',
            xxfill: { type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status' },
            stroke: { type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status' },
            strokeWidth: 1,
            // margin: {left: 8, right: 8, top: 0, bottom: 0}
            xxmargin: { left: 0, right: 0, top: 0, bottom: 0 },
            margin: function(base, name, data, me) {
                var vext = 0;
                if (typeof data['data']['ext'] !== 'undefined') {
                    vext = data['data']['ext'];
                }
                if (vext > 0) {
                    return { right: 8, top: 0, bottom: 0 }
                } else {
                    return { top: 0, bottom: 0 }
                }
            }
        },
        {
            name: 'ext',
            type: 'rect',
            x: 0,
            y: 36,
            x: function(base, name, data, me) {
                var vnormal = 0;
                var vcolwidth = 50;
                var breakwidthX = 0;
                if (typeof data['data']['normal'] !== 'undefined') {
                    vnormal = data['data']['normal'];
                }
                if (typeof data['chip']['columnWidth'] !== 'undefined') {
                    vcolwidth = data['chip']['columnWidth'];
                }
                // return vcolwidth*vnormal;
                // console.log('ext apa lo', data['data']['ext'])
                // console.log('return X', vcolwidth * (vnormal - data['data']['ext'] / 60))

                if (typeof data['data']['breakWidth'] != 'undefined') {
                    breakwidthX = data['data']['breakWidth'];
                }

                // return vcolwidth * (vnormal - data['data']['ext'] / 60) + (breakwidthX * 120);
                if (data.data.PlanDateTimeStart.getTime() == data.data.PlanDateTimeFinish.getTime() && data.data.ext > 0){
                    return vcolwidth * (vnormal - vnormal );
                } else {
                    return vcolwidth * (vnormal - data['data']['ext'] / 60);
                }
            },
            width: function(base, name, data, me) {
                var vext = 0;
                var vcolwidth = 50;
                var breakwidthX = 0;
                // console.log('vnormal apa lo', data['data']['ext'])
                // console.log('data apa lo', data['data'])
                // console.log('vcolwidth apa lo', data['chip']['columnWidth'])
                if (typeof data['data']['ext'] !== 'undefined') {
                    vnormal = data['data']['ext'];
                }
                if (typeof data['chip']['columnWidth'] !== 'undefined') {
                    vcolwidth = data['chip']['columnWidth'];
                }
                if (typeof data['data']['breakWidth'] != 'undefined') {
                    breakwidthX = data['data']['breakWidth'];
                }
                if (data.data.PlanDateTimeStart.getTime() == data.data.PlanDateTimeFinish.getTime() && data.data.ext > 0){
                    return (vcolwidth * data['data']['normal'])  + (breakwidthX * 120);
                } else {
                    return (vcolwidth * vnormal / 60)  + (breakwidthX * 120);
                }
            },
            visible: function(base, name, data, me) {
                var vext = 0;
                if (typeof data['data']['ext'] !== 'undefined') {
                    vext = data['data']['ext'];
                }
                if (vext > 0) {
                    return 1;
                } else {
                    return 0
                }
            },
            height: 21,
            fill: '#ffA520',
            margin: function(base, name, data, me) {
                var vext = 0;
                if (typeof data['data']['ext'] !== 'undefined') {
                    vext = data['data']['ext'];
                }
                if (vext > 0) {
                    return { left: 0, top: 0, bottom: 0 }
                } else {
                    return { top: 0, bottom: 0 }
                }
            }
        },
        {
            name: 'highlight',
            type: 'rect',
            x: 0,
            y: 0,
            width: { data: 'chip', field: 'width' },
            height: { data: 'chip', field: 'height' },
            fill: null,
            stroke: '#f66',
            cornerRadius: 2,
            strokeWidth: 2,
            visible: function(base, name, data, me) {
                if (data['data']['selected']) {
                    return true;
                } else {
                    return false;
                }
            },
            margin: { left: 2, right: 2, top: 2, bottom: 2 }
        },
        {
            name: 'widecontrol',
            type: 'rect',
            x: function(base, name, data, me) {
                return data['chip']['width'] - 8;
            },
            y: 0,
            width: 8,
            height: { data: 'chip', field: 'height' },
            fill: '#f66',
            stroke: '#f66',
            cornerRadius: 2,
            strokeWidth: 2,
            visible: function(base, name, data, me) {
                if (data['data']['selected']) {
                    return true;
                } else {
                    return false;
                }
            },
            margin: { left: 2, right: 2, top: 4, bottom: 4 }
        },

        {
            name: 'main_icons',
            type: 'icons',
            x: 4,
            y: 31,
            icons: { data: 'data', field: 'statusicon', default: [] },
            width: function(base, name, data, me) {
                console.log('chip-rect-2', data['chip']['width'])
                var vext = 0;
                var vwidth = 1;
                var vcolwidth = 50;
                var vnormal = 1;
                if (typeof data['chip']['columnWidth'] !== 'undefined') {
                    vcolwidth = data['chip']['columnWidth'];
                }
                if (typeof data['data']['normal'] !== 'undefined') {
                    vnormal = data['data']['normal'];
                }
                return vcolwidth * vnormal;
            },
        },
        {name: 'bottom_icon', type: 'icons', 
            xxx: 25,
            x: function(base, name, data, me){
                var vext = 0;
                var vwidth = 1;
                var vcolwidth = 50;
                var vnormal = 1;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                if (typeof data['data']['normal']!=='undefined'){
                    vnormal = data['data']['normal'];
                }
                if (typeof data['data']['ext']!=='undefined'){
                    vext = data['data']['ext'] / 60; // di bagi 60 karena ext nya dalam menit, jadi convert dl biar jd jam
                }
                vwidth = vnormal;
                return Math.round(vwidth*vcolwidth/2)-14;
            }, 
            y: 50, 
            icons: ['play2'],
            width: 20,
            height: 16,
            iconHeight: 12,
            iconWidth: 12,
            visible: function(base, name, data, me){
                console.log('cek dispatch', data['data'])
                var vstat = data['data']['isDispatched'];
                if (vstat != null && vstat != 0){
                    return true;

                } else {
                    return false;
                }
                // if ((vstat==4)||(vstat==5)||(vstat==6)){
                //     vallocated = 1;
                //     if (typeof data['data']['allocated']!=='undefined'){
                //         vallocated = data['data']['allocated'];
                //     }
                //     if (vallocated){
                //         return false;
                //     } else {
                //         return true;
                //     }

                // } else {
                //     return false;
                // }
            }
        },

    ]
}

/*
// chip definition
JsBoard.Chips.jpcbbp_x = {}
JsBoard.Chips.jpcbbp_x.jpcb = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    
    {name: 'tglcetakwo', type: 'text', text: {data: 'data', field: 'tglcetakwo'}, x: 4, y: 2, align: 'right', width: {data: 'chip', field: 'width'}, 
        margin: {right: 10},
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'type'}, x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    {name: 'jamcetakwo', type: 'text', text: {data: 'data', field: 'jamcetakwo'}, x: 4, y: 16, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 10},
    },
    {name: 'wotype', type: 'text', text: {data: 'data', field: 'wotype'}, x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 10},
    },
    // {name: 'start2', type: 'text', text: {data: 'data', field: 'start2'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}},
    // {name: 'jamjanjiserah', type: 'text', text: {data: 'data', field: 'jamjanjiserah'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}, 
    //     visible: function(base, name, data, me){
    //         if (data['data']['width']>0.5){
    //             return true
    //         } else {
    //             return false;
    //         }
    //     }
    // },
    // {name: 'pekerjaan', type: 'text', text: {data: 'data', field: 'pekerjaan'}, x: 0, y: 51, width: {data: 'chip', field: 'width'}},
    // {name: 'teknisi', type: 'text', text: {data: 'data', field: 'teknisi'}, x: 30, y: 51, align: 'right', 
    //     wrap: 'none',
    //     xwidth: {data: 'chip', field: 'width'},
    //     width: function(base, name, data, me){
    //         return data['chip']['width']-this.x;
    //     }
    // },
    {name: 'normal', type: 'rect', x: 0, y:36, 
      width: function(base, name, data, me){
        console.log('chip-rect-2', data['chip']['width'])
        var vext = 0;
        var vwidth = 1;
        var vcolwidth = 50;
        var vnormal = data['data']['width'];
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        return vcolwidth*vnormal;
      }, 
      height: 21,
      xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      fill: function(base, name, data, me){
          return JsBoard.Common.RandomColorList[data['data']['color_idx']]
      },
    //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
      margin: {left: 8, right: 6, top: 0, bottom: 0}
    },
    {name: 'higlight', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#f66', cornerRadius: 2, strokeWidth: 2, 
      visible: function(base, name, data, me){
        if (data['data']['selected']){
            return true;
        } else {
            return false;
        }
      },
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'widecontrol', type: 'rect', 
        x: function(base, name, data, me){
            return data['chip']['width'] - 8;
        }, 
        y:0, 
        width: 8, 
        height: {data: 'chip', field: 'height'},
        fill: '#f66', stroke: '#f66', cornerRadius: 2, strokeWidth: 2, 
        visible: function(base, name, data, me){
            if (data['data']['selected']){
                return true;
            } else {
                return false;
            }
        },
        margin: {left: 2, right: 2, top: 4, bottom: 4}
    },
    // {name: 'ext', type: 'rect', x: 0, y:32, 
    //   x: function(base, name, data, me){
    //     var vnormal = 0;
    //     var vcolwidth = 50;
    //     if (typeof data['data']['normal']!=='undefined'){
    //       vnormal = data['data']['normal'];
    //     }
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     return vcolwidth*vnormal;
    //   }, 
    //   width: function(base, name, data, me){
    //     var vext = 0;
    //     var vcolwidth = 50;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vnormal = data['data']['ext'];
    //     }
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     return vcolwidth*vnormal;
    //   }, 
    //   visible: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return 1;
    //     } else {
    //       return 0
    //     }
    //   },
    //   height: 21,
    //   fill: '#ffA520', 
    //   margin: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return {left: 0, top: 0, bottom: 0}
    //     } else {
    //       return {top: 0, bottom: 0}
    //     }
    //   }
    // },
    // {name: 'type', type: 'icons', 
    //     x: 0, y: 26, 
    //     icons: {data: 'data', field: 'statusicon', default: []},
    //     width: function(base, name, data, me){
    //         console.log('chip-rect-2', data['chip']['width'])
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //         vnormal = data['data']['normal'];
    //         }
    //         return vcolwidth*vnormal;
    //     }, 
    // },
    // {name: 'type', type: 'icons', 
    //     xxx: 25,
    //     x: function(base, name, data, me){
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //             vnormal = data['data']['normal'];
    //         }
    //         if (typeof data['data']['ext']!=='undefined'){
    //             vext = data['data']['ext'];
    //         }
    //         vwidth = vnormal+vext;
    //         return Math.round(vwidth*vcolwidth/2)-14;
    //     }, 
    //     y: 46, 
    //     icons: ['play2'],
    //     width: 20,
    //     height: 16,
    //     iconHeight: 14,
    //     iconWidth: 14,
    //     visible: function(base, name, data, me){
    //         vallocated = 1;
    //         if (typeof data['data']['allocated']!=='undefined'){
    //             vallocated = data['data']['allocated'];
    //         }
    //         if (vallocated){
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // },
  ]
}

JsBoard.Chips.jpcbbp_x.jpcb_stall_base = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
  ]
}

JsBoard.Chips.jpcbbp_x.jpcb_stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, 
        width: function(base, name, data, me){
            return data['chip']['width'] - data['chip']['stallTechWidth'];
        },
        fontSize: 14,
        margin: {left: 5, top: 25, right: 5, bottom: 25}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
    {name: 'technician', type: 'text', text: {data: 'data', field: 'technician'}, x: 0, y: 0, 
        width: {data: 'chip', field: 'stallTechWidth'}, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['chip']['stallTechWidth'];
        },
        fontSize: 14,
        margin: {left: 5, top: 25, right: 5, bottom: 25}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
  ]
}

    //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
JsBoard.Chips.jpcbbp_x.jpcb_footer = {
  items: [
    // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
    //   fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
    //   margin: {left: 1, right: 1, top: 0, bottom: 1}
    // },

    {name: 'jobstoppage', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, 
        xheight: function(base, name, data, me){
            return data['chip']['height'] - 30 -5;
        },
        height: 45,
        xheight: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'jobstoppagetitle', type: 'text', x: 0, y:5, 
        width: {data: 'chip', field: 'width'}, 
        height: 30, fontSize: 20,
        text: 'Job Stoppage', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 8, right: 8, top: 10, bottom: 0}
    },
    {name: 'stoppagearea', type: 'rect', x: 0, y:44, width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - 30 -15;
        },
        xheight: {data: 'chip', field: 'height'},
        fill: '#f5f5f5', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
  ]
}

JsBoard.Chips.jpcbbp_x.jpcb_fixed_header = {
  items: [
    {name: 'selectarea', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 2, opacity: 0.5,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}


JsBoard.Chips.jpcbbp_x.jpcb_blocked = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f00', stroke: '#777', cornerRadius: 2, strokeWidth: 1, opacity: 0.5,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}





JsBoard.Chips.jpcbbp_x.blank = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f0f0f0', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}

JsBoard.Chips.jpcbbp_x.red = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: 'red', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}

JsBoard.Chips.jpcbbp_x.break = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#555', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'breakText', type: 'text', 
        x: 20, y:{data: 'chip', field: 'height'}, 
        height: {data: 'chip', field: 'width'}, 
        width: {data: 'chip', field: 'height'},
      text: 'BREAK',
      fontSize: 30,
      rotation: 270,
      align: 'center',
      fill: 'white', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}


JsBoard.Chips.jpcbbp_x.jpcb_first_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ddd',
        xfill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'base2', type: 'rect', x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#bbb',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'base3', type: 'rect', x: 0, 
        y:{data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#eee',
        stroke: '#888',
        margin: {left: 0, right: 0, top: 0, bottom: 1}
    },
    {name: 'maintitle', type: 'text', text: {data: 'chip', field: 'teamName'}, x: 0, 
        y:{data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 24,
        margin: {left: 15, top: 10, right: 5, bottom: 5}, 
        align: 'center',
        fill: '#338',
    },
    {name: 'subtitle1', type: 'text', text: {data: 'data', field: 'subtitle1'}, x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    {name: 'subtitle1', type: 'text', text: {data: 'data', field: 'subtitle2'}, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['chip']['stallTechWidth'];
        },
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'stallTechWidth'},
        fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.jpcbbp_x.jpcb_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#bb8',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    {name: 'parentBase', type: 'rect', x: 0, 
        visible: function (base, name, data, me){
            if (data['data']['newDay']){
                return true;
            }
            return false;
        },
        y:{data: 'chip', field: 'titleTop'}, 
        width: function(base, name, data, me){
            return data['chip']['width'] * data['data']['subCount'];
        },
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#eee',
        stroke: '#888',
        margin: {left: 0, right: 0, top: 0, bottom: 1}
    },
    {name: 'parentText', type: 'text', text: {data: 'data', field: 'parentText'}, x: 0, 
        y:{data: 'chip', field: 'titleTop'}, 
        width: function(base, name, data, me){
            return data['chip']['width'] * data['data']['subCount'];
        },
        align: 'center',
        fontSize: 18,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
  ]
}


JsBoard.Chips.jpcbbp_x.jpcb2 = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    
    {name: 'tglcetakwo', type: 'text', text: {data: 'data', field: 'tglcetakwo'}, x: 4, y: 2, align: 'right', width: {data: 'chip', field: 'width'}, 
        margin: {right: 10},
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'type'}, x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    {name: 'jamcetakwo', type: 'text', text: {data: 'data', field: 'jamcetakwo'}, x: 4, y: 16, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 10},
    },
    {name: 'wotype', type: 'text', text: {data: 'data', field: 'workname'}, x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 10},
    },
    {name: 'normal', type: 'rect', x: 0, y:36, 
      width: function(base, name, data, me){
        console.log('chip-rect-2', data['chip']['width'])
        var vext = 0;
        var vwidth = 1;
        var vcolwidth = 50;
        var vnormal = 1;
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        return vcolwidth*vnormal;
      }, 
      height: 21,
      xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      fill: function(base, name, data, me){
          return JsBoard.Common.RandomColorList[data['data']['color_idx']]
      },
      margin: {left: 8, right: 6, top: 0, bottom: 0}
    },
    {name: 'highlight', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#f66', cornerRadius: 2, strokeWidth: 2, 
      visible: function(base, name, data, me){
        if (data['data']['selected']){
            return true;
        } else {
            return false;
        }
      },
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'widecontrol', type: 'rect', 
        x: function(base, name, data, me){
            return data['chip']['width'] - 8;
        }, 
        y:0, 
        width: 8, 
        height: {data: 'chip', field: 'height'},
        fill: '#f66', stroke: '#f66', cornerRadius: 2, strokeWidth: 2, 
        visible: function(base, name, data, me){
            if (data['data']['selected']){
                return true;
            } else {
                return false;
            }
        },
        margin: {left: 2, right: 2, top: 4, bottom: 4}
    },
  ]
}
*/