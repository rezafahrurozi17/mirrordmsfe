// copy dari default
JsBoard.Chips.asb_estimasi = JsBoard.Common.extend(JsBoard.Chips.jpcbdefault, {});
var asbColorMap = {default: '#fff', block: '#b00', extension: '#ffA520', appoinment: '#fff'}
var asbLineColorMap = {default: '#000', block: '#b00', extension: '#db0', appoinment: '#000'}

JsBoard.Chips.asb_estimasi.footer = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'rect1', type: 'rect', x: 0, y:0, 
        width: 50, height: 30,
        fill: '#fff', stroke: '#000', strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 50, y:0, 
        width: 90, height: 30,
        text: 'Appointment', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
  ]
}

JsBoard.Chips.asb_estimasi.stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#fff'
            } else {
                return '#ddd'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 10, top: 5, right: 5, bottom: 5}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#33b', blank: '#aaa', blocked: '#a00'},
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'person'}, x: 0, y: 30, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 10, top: 5, right: 5, bottom: 5}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
    // {name: 'text', type: 'text', text: {data: 'data', field: 'person1'}, x: 0, y: 20, width: {data: 'chip', field: 'width'}, fontSize: 10,
    //     margin: {left: 10, top: 5, right: 5, bottom: 5}, 
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    // },
    // {name: 'text', type: 'text', text: {data: 'data', field: 'person2'}, x: 0, y: 40, width: {data: 'chip', field: 'width'}, fontSize: 10,
    //     margin: {left: 10, top: 5, right: 5, bottom: 5}, 
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    // },
    //  {name: 'text', type: 'text', text: {data: 'data', field: 'person3'}, x: 0, y: 60, width: {data: 'chip', field: 'width'}, fontSize: 10,
    //     margin: {left: 10, top: 5, right: 5, bottom: 5}, 
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    // },
  ]
}

JsBoard.Chips.asb_estimasi.estimasi = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, 
      width: {data: 'chip', field: 'width'}, 
      height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#ddd', 
      cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'base1', type: 'rect', x: 0, y:0, 

        visible: function(base, name, data, me){
            if (data['data']['width1']){
                return true
            } 
            return false;
        },
        width: function(base, name, data, me){
            if (data['data']['width1']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['width1']*vcolwidth;
            } 
            return 50;
        },
        //cwidth: {data: 'chip', field: 'width1'}, 
        height: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', 
        cornerRadius: 2, strokeWidth: 1,
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'base2', type: 'rect', 
        x: function(base, name, data, me){
            if (data['data']['start2']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['start2']*vcolwidth;
            } 
            return 0;
        },
        
        y:0, 
        visible: function(base, name, data, me){
            if (data['data']['width2']){
                return true
            } 
            return false;
        },
        width: function(base, name, data, me){
            if (data['data']['width2']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['width2']*vcolwidth;
            } 
            return 50;
        },
        //cwidth: {data: 'chip', field: 'width1'}, 
        height: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', 
        cornerRadius: 2, strokeWidth: 1,
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: null, 
        //stroke: '#0f0', 
        stroke: function(base, name, data, me){
            if (data['data']['isPrediagnose']){
                return '#f6a'
            } else {
                return '#0f0';
            }
        },
        cornerRadius: 2, strokeWidth: 2, 
        visible: function(base, name, data, me){
            if (data['data']['selected']){
                return true
            } 
            return false;
        },
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    {name: 'type', type: 'text', 
        text: function(base, name, data, me){
            if (data['data']['row']<0){
                return data['data']['tipe']
            } else {
                var vmin = data['chip']['incrementMinute'] * data['data']['col'];
                var vminhour = JsBoard.Common.getMinute(data['chip']['startHour'])+vmin;
                var vres = JsBoard.Common.getTimeString(vminhour);
                return vres;
            }
        }, 
        x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    {name: 'prediagnose_type', type: 'text', text: 'PD', xtext: {data: 'data', field: 'pdText'}, x: 0, y: 6, align: 'right', width: {data: 'chip', field: 'width'}, 
        fontStyle: 'bold',
        fill: '#f6a',
        margin: {left: 8, right: 8, top: 0, bottom: 0},
        visible: function(base, name, data, me){
            if (data['data']['isPrediagnose']){
                return true
            } else {
                return false;
            }
        }
    },
    {name: 'stallperson', type: 'text', 
        text: function(base, name, data, vcont){
            //console.log("container", vcont);
            var vres = '';
            if (typeof vcont.layout.getInfoRowData !== 'undefined'){
                var vrow = data['data']['row'];
                var vdata = vcont.layout.getInfoRowData(vrow);
                if (vdata && vdata['person']){
                    vres = vdata['person'];
                }
            }
            return vres;
        },
        x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 14},
    },
    // di komen karena katanya ga mau nama stall yang tampil di ploting wo, tapi kategori wo nya
    // {name: 'stallname', type: 'text', 
    //     text: function(base, name, data, vcont){
    //         // console.log("container", vcont);
    //         var vres = '';
    //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
    //             var vrow = data['data']['row'];
    //             var vdata = vcont.layout.getInfoRowData(vrow);
    //             if (vdata && vdata['title']){
    //                 vres = vdata['title'];
    //             }
    //         }
    //         return vres;
    //     },
    //     x: 4, y: 54, align: 'left', width: {data: 'chip', field: 'width'},
    // },
    {name: 'pekerjaan', type: 'text', 
        text: function(base, name, data, vcont){
            // console.log("data biii", data);
            var vres = '';
            if (data.data.pekerjaan != undefined){
                vres = data.data.pekerjaan;
            } 
            return vres;
        },
        x: 4, y: 54, align: 'left', width: {data: 'chip', field: 'width'},
    },
    {name: 'normal', type: 'rect', x: 0, y:36, 
      xwidth: {data: 'chip', field: 'width'}, 
      visible: function(base, name, data, me){
        if (data['data']['width1']){
            return true
        } 
        return false;
      },
      width: function(base, name, data, me){
        if (data['data']['width1']){
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            return data['data']['width1']*vcolwidth;
        } 
        return 50;
        },
      height: 21,
      fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      colorMap: asbColorMap,
      lineColorMap: asbLineColorMap,
      xfill: '#fff',
      stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
      strokeWidth: 1,
      margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'normal2', type: 'rect', y:36, 
        x: function(base, name, data, me){
            if (data['data']['start2']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['start2']*vcolwidth;
            } 
            return 0;
        },
        visible: function(base, name, data, me){
            if (data['data']['width2']){
                return true
            } 
            return false;
        },
        width: function(base, name, data, me){
            if (data['data']['width2']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['width2']*vcolwidth;
            } 
            return 50;
        },
      height: 21,
      fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      colorMap: asbColorMap,
      lineColorMap: asbLineColorMap,
      xfill: '#fff',
      stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
      strokeWidth: 1,
      margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'icon', type: 'icons', 
        x: 4, y: 30, 
        icons: {data: 'data', field: 'statusicon', default: []}, 
        width: function(base, name, data, me){
            // console.log('chip-rect-2', data['chip']['width'])
            var vext = 0;
            var vwidth = 1;
            var vcolwidth = 50;
            var vnormal = 1;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            if (typeof data['data']['normal']!=='undefined'){
            vnormal = data['data']['normal'];
            }
            return vcolwidth*vnormal;
        }, 
    },
    {name: 'err_info_box', type: 'rect', x: 0, y:25, 
        width: function(base, name, data, me){
            var vcolwidth = 120;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            return vcolwidth;
        }, 
        height: 50,
        fill: '#ddd', stroke: '#ddd', 
        opacity: 0.75,
        cornerRadius: 2, strokeWidth: 1,
        visible: function(base, name, data, me){
            if (data['data']['width']<0.25){
                return true
            } 
            return false;
        },
        align: 'center',
        text: 'Target Actual Rate (Job Plan) '+"\r\n"+'harus >= 15 menit',
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'err_info', type: 'text', x: 0, y:30, 
        width: function(base, name, data, me){
            var vcolwidth = 120;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            return vcolwidth;
        }, 
        height: 40,
        fill: '#f00', stroke: '#f00', 
        cornerRadius: 2, strokeWidth: 1,
        visible: function(base, name, data, me){
            if (data['data']['width']<0.25){
                return true
            } 
            return false;
        },
        align: 'center',
        text: 'Target Actual Rate (Job Plan) '+"\r\n"+'harus >= 15 menit',
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
  ]
}


JsBoard.Chips.asb_estimasi.estimasi2 = {
    items: [
      {name: 'base', type: 'rect', x: 0, y:0, 
        width: {data: 'chip', field: 'width'}, 
        height: {data: 'chip', field: 'height'},
        visible: function(base, name, data, me){
            if (data['data']['row']==-2){
                return true;
            }
            return false;
        },
        fill: null, stroke: '#888', 
        cornerRadius: 2, strokeWidth: 1,
        margin: {left: 2, right: 2, top: 2, bottom: 2}
      },
      {name: 'judul', type: 'text', text: {data: 'data', field: 'title'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
          wrap: 'none', fontStyle: 'bold',
          visible: function(base, name, data, me){
            if (data['data']['row']==-2){
                return true;
            }
            return false;
          },
          fontSize: function(base, name, data, me){
              if (data['data']['width']>0.5){
                  return 11
              } else {
                  return 10;
              }
          }
      },
    {name: 'err_info_box', type: 'rect', x: 0, y:22, 
        width: {data: 'chip', field: 'width'},
        height: 58,
        fill: '#ddd', stroke: '#888', 
        opacity: 1,
        cornerRadius: 2, strokeWidth: 1,
        visible: function(base, name, data, me){
            if (data['data']['row']==-2){
                return true;
            }
            return false;
        },
        align: 'center',
        text: 'Target Actual Rate (Job Plan) '+"\r\n"+'harus >= 15 menit',
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'err_info1', type: 'text', x: 0, y:28, 
        width: {data: 'chip', field: 'width'},
          height: 16,
          wrap: 'none', fontStyle: 'bold',
          FontSize: 10,
          cornerRadius: 2, strokeWidth: 1,
          visible: function(base, name, data, me){
              if (data['data']['row']==-2){
                  return true;
              }
              return false;
          },
          align: 'left',
          text: {data: 'data', field: 'message1'},
          margin: {left: 8, right: 8, top: 2, bottom: 2}
      },
      {name: 'err_info2', type: 'text', x: 0, y:28+16, 
          width: {data: 'chip', field: 'width'},
          height: 16,
          wrap: 'none', fontStyle: 'bold',
          FontSize: 10,
          cornerRadius: 2, strokeWidth: 1,
          visible: function(base, name, data, me){
            if (data['data']['row']==-2){
                return true;
            }
            return false;
          },
          align: 'left',
          text: {data: 'data', field: 'message2'},
          margin: {left: 8, right: 8, top: 2, bottom: 2}
      },
      {name: 'err_info3', type: 'text', x: 0, y:28+16+16, 
          width: {data: 'chip', field: 'width'},      
          height: 16,
          wrap: 'none', fontStyle: 'bold',
          FontSize: 10,
          cornerRadius: 2, strokeWidth: 1,
          visible: function(base, name, data, me){
            if (data['data']['row']==-2){
                return true;
            }
            return false;
          },
          align: 'left',
          text: {data: 'data', field: 'message3'},
          margin: {left: 8, right: 8, top: 2, bottom: 2}
      },
    ]
  }
  

    // {name: 'base2', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 300, xheight: {data: 'chip', field: 'height'},
    //     fill: '#dd0', stroke: '#000',
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },

JsBoard.Chips.asb_estimasi.header = {
  items: [
    {name: 'base', type: 'rect', x: 0, 
        y: {data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['titleTop'];
        },
        fill: '#ccc',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, 
        y: {data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000', align: 'center',
    },
  ]
}

JsBoard.Chips.asb_estimasi.first_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, 
        y: {data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['titleTop'];
        },
        fill: '#ccc',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, 
        y: {data: 'chip', field: 'titleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 15, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
  ]
}

JsBoard.Chips.asb_estimasi.fix_header = {
  items: [
    {name: 'selectarea', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 2, opacity: 0.5,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
    // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
    //     fill: '#440', opacity: 0.5,
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    // {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
    //     margin: {left: 5, top: 10, right: 5, bottom: 5}, 
    //     fill: '#fff', align: 'center',
    // },
  ]
}

///// ASB GR ///////
// copy dari default
JsBoard.Chips.asb_gr = JsBoard.Common.extend(JsBoard.Chips.asb_estimasi, {});

JsBoard.Chips.asb_gr.footer = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'rect1', type: 'rect', x: 0, y:0, 
        width: 50, height: 30,
        fill: asbColorMap['block'], 
        stroke: '#000', strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 50, y:0, 
        width: 90, height: 30,
        text: 'Block', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect2', type: 'rect', x: 100, y:0, 
        width: 50, height: 30,
        fill: asbColorMap['extension'], 
        stroke: '#000', strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text2', type: 'text', x: 150, y:0, 
        width: 90, height: 30,
        text: 'Extension', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect3', type: 'rect', x: 200, y:0, 
        width: 50, height: 30,
        fill: asbColorMap['appointment'], 
        stroke: '#000', strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text3', type: 'text', x: 250, y:0, 
        width: 90, height: 30,
        text: 'Appointment', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
  ]
}


// TAB
var tab_Fill_colorMap = {default: '#fff', no_showx: '#ff0', appointment: '#03f', do_no_show: '#ff0'}
var tab_Line_colorMap = {default: '#333', no_showx: '#333', appointment: '#03f', do_no_show: '#ff0'}
JsBoard.Chips.asb_gr.none = {
    items: []    
}
JsBoard.Chips.asb_gr.blank = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#777', stroke: '#777', 
        strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
        },
        
    ]    
}
JsBoard.Chips.asb_gr.tab = {
  items: [
    // {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
    //   fill: '#fff', stroke: '#777', 
    //   cornerRadius: 2, strokeWidth: 1,
    //   margin: {left: 2, right: 2, top: 2, bottom: 2}
    // },
    {name: 'base', type: 'rect', x: 0, y:0, 
      width: {data: 'chip', field: 'width'}, 
      height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#ddd', 
      cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'base1', type: 'rect', x: 0, y:0, 

        visible: function(base, name, data, me){
            if (data['data']['width1']){
                return true
            } 
            return false;
        },
        width: function(base, name, data, me){
            if (data['data']['width1']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['width1']*vcolwidth;
            } 
            return 50;
        },
        //cwidth: {data: 'chip', field: 'width1'}, 
        height: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', 
        cornerRadius: 2, strokeWidth: 1,
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'base2', type: 'rect', 
        x: function(base, name, data, me){
            if (data['data']['start2']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['start2']*vcolwidth;
            } 
            return 0;
        },
        
        y:0, 
        visible: function(base, name, data, me){
            if (data['data']['width2']){
                return true
            } 
            return false;
        },
        width: function(base, name, data, me){
            if (data['data']['width2']){
                var vcolwidth = 50;
                if (typeof data['chip']['columnWidth']!=='undefined'){
                    vcolwidth = data['chip']['columnWidth'];
                } 
                return data['data']['width2']*vcolwidth;
            } 
            return 50;
        },
        //cwidth: {data: 'chip', field: 'width1'}, 
        height: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', 
        cornerRadius: 2, strokeWidth: 1,
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, 
        visible: function(base, name, data, me){
            if (data['data']['selected']){
                return true
            } 
            return false;
        },
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    {name: 'type', type: 'text', 
        text: function(base, name, data, me){
            if (data['data']['row']<0){
                //return data['data']['AppointmentTime'];
                var vminhour = JsBoard.Common.getMinute(data['data']['AppointmentTime']);
                var vres = JsBoard.Common.getTimeString(vminhour);
                return vres;
            } else {
                var vmin = data['chip']['incrementMinute'] * data['data']['col'];
                var vminhour = JsBoard.Common.getMinute(data['chip']['startHour'])+vmin;
                var vres = JsBoard.Common.getTimeString(vminhour);
                return vres;
            }
        }, 
        x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    {name: 'stallperson', type: 'text', 
        text: function(base, name, data, vcont){
            // console.log("container", vcont);
            var vres = '';
            if (typeof vcont.layout.getInfoRowData !== 'undefined'){
                var vrow = data['data']['row'];
                var vdata = vcont.layout.getInfoRowData(vrow);
                if (vdata && vdata['person']){
                    vres = vdata['person'];
                }
            }
            return vres;
        },
        x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 14},
    },
    {name: 'stallname', type: 'text', 
        text: function(base, name, data, vcont){
            // console.log("container", vcont);
            var vres = '';
            if (typeof vcont.layout.getInfoRowData !== 'undefined'){
                var vrow = data['data']['row'];
                var vdata = vcont.layout.getInfoRowData(vrow);
                if (vdata && vdata['title']){
                    vres = vdata['title'];
                }
            }
            return vres;
        },
        x: 4, y: 54, align: 'left', width: {data: 'chip', field: 'width'},
    },
    // {name: 'normal', type: 'rect', x: 0, y:36, 
    //   width: function(base, name, data, me){
    //     console.log('chip-rect-2', data['data'])
    //     var vext = 0;
    //     var vwidth = 1;
    //     var vcolwidth = 50;
    //     var vnormal = data['data']['width'];
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     if (typeof data['data']['normal']!=='undefined'){
    //       vnormal = data['data']['normal'];
    //     }
    //     if (data['data']['status']=='do_no_show'){
    //         var xxtest = 1000;
    //     }
    //     return vcolwidth*vnormal;
    //   }, 
    //   height: 21,
    //   fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //   colorMap: tab_Fill_colorMap,
    //   lineColorMap: tab_Line_colorMap,
    //   xfill: '#fff',
    //   stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
    //   strokeWidth: 1,
    //   margin: {left: 8, right: 6, top: 0, bottom: 0}
    // },
    {name: 'normal', type: 'rect', x: 0, y:36, 
      xwidth: {data: 'chip', field: 'width'}, 
      visible: function(base, name, data, me){
        if (data['data']['width1']){
            return true
        } 
        return false;
      },
      width: function(base, name, data, me){
        if (data['data']['width1']){
            var vcolwidth = 50;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            return data['data']['width1']*vcolwidth;
        } 
        return 50;
        },
      height: 21,
      fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      colorMap: tab_Fill_colorMap,
      lineColorMap: tab_Line_colorMap,
      xfill: '#fff',
      stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
      strokeWidth: 1,
      margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'white_icon', type: 'icons', 
        x: 4, y: 30, 
        icons: ['customer'],
        width: function(base, name, data, me){
            var vext = 0;
            var vwidth = 1;
            var vcolwidth = 50;
            var vnormal = 1;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            if (typeof data['data']['normal']!=='undefined'){
            vnormal = data['data']['normal'];
            }
            return vcolwidth*vnormal;
        }, 
        visible: {type: 'map', mapdata: 'config', mapfield: 'statusMap', data: 'data', field: 'status'}, 
        statusMap: {default: false, appointment: true, no_show: false},
    },
    {name: 'gray_icon', type: 'icons', 
        x: 4, y: 30, 
        icons: ['customer_gray'],
        width: function(base, name, data, me){
            var vext = 0;
            var vwidth = 1;
            var vcolwidth = 50;
            var vnormal = 1;
            if (typeof data['chip']['columnWidth']!=='undefined'){
                vcolwidth = data['chip']['columnWidth'];
            } 
            if (typeof data['data']['normal']!=='undefined'){
            vnormal = data['data']['normal'];
            }
            return vcolwidth*vnormal;
        }, 
        visible: {type: 'map', mapdata: 'config', mapfield: 'statusMap', data: 'data', field: 'status'}, 
        statusMap: {default: true, appointment: false, no_show: true},
    },
  ]
}

JsBoard.Chips.asb_gr.tab_footer = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'rect1', type: 'rect', x: 0, y:0, 
        width: 50, height: 30,
        fill: tab_Fill_colorMap['appointment'], 
        stroke: tab_Line_colorMap['appointment'], 
        strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text1', type: 'text', x: 50, y:0, 
        width: 90, height: 30,
        text: 'Appointment', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'rect2', type: 'rect', x: 120, y:0, 
        width: 50, height: 30,
        fill: tab_Fill_colorMap['no_show'], 
        stroke: tab_Line_colorMap['no_show'], 
        strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
    },
    {name: 'text2', type: 'text', x: 170, y:0, 
        width: 90, height: 30,
        text: 'Potensi No Show', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
    },
    {name: 'separator', type: 'rect', x: 0, y:30, width: {data: 'chip', field: 'width'}, height: 5, xheight: {data: 'chip', field: 'height'},
      fill: '#fff', 
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'jobstoppage', type: 'rect', x: 0, y:35, width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - 30 -5;
        },
        xheight: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
    {name: 'jobstoppagetitle', type: 'text', x: 0, y:40, 
        width: {data: 'chip', field: 'width'}, 
        height: 30, fontSize: 20,
        text: 'Customer No Show', 
        fill: '#000',
        wrap: 'none',
        margin: {left: 8, right: 8, top: 10, bottom: 0}
    },
    {name: 'stoppagearea', type: 'rect', x: 0, y:80, width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - 30 -50;
        },
        xheight: {data: 'chip', field: 'height'},
        fill: '#f5f5f5', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
    },
  ]
}


///// ASB Produksi ///////
// copy dari default
JsBoard.Chips.asb_produksi = JsBoard.Common.extend(JsBoard.Chips.asb_estimasi, {});

JsBoard.Chips.asb_produksi.blank = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#777', stroke: '#777', 
        strokeWidth: 1,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
        },
        
    ]    
}

JsBoard.Chips.asb_produksi.header = {
  items: [
    {name: 'base', type: 'rect', x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#bb8',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, 
        y:{data: 'chip', field: 'subtitleTop'}, 
        width: {data: 'chip', field: 'width'}, fontSize: 12,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    {name: 'parentBase', type: 'rect', x: 0, 
        visible: function (base, name, data, me){
            if (data['data']['newDay']){
                return true;
            }
            return false;
        },
        y:{data: 'chip', field: 'titleTop'}, 
        width: function(base, name, data, me){
            return data['chip']['width'] * data['data']['subCount'];
        },
        height: function(base, name, data, me){
            return data['chip']['height'] - data['chip']['subtitleTop'];
        },
        fill: '#eee',
        stroke: '#888',
        margin: {left: 0, right: 0, top: 0, bottom: 1}
    },
    {name: 'parentText', type: 'text', text: {data: 'data', field: 'parentText'}, x: 0, 
        y:{data: 'chip', field: 'titleTop'}, 
        width: function(base, name, data, me){
            return data['chip']['width'] * data['data']['subCount'];
        },
        align: 'center',
        fontSize: 16,
        margin: {left: 5, top: 10, right: 15, bottom: 5}, 
        fill: '#000',
    },
  ]
}

JsBoard.Chips.asb_produksi.stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#fff'
            } else {
                return '#ddd'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    // {name: 'xbase', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, 
    //     xvisible: {data: 'data', field: 'visible'},
    //     height: {data: 'chip', field: 'height'},
    //     xheight: function(base, name, data, me){
    //         return data['chip']['height'] * data['data']['itemCount'];
    //     },
    //     fill: '#ddd',
    //     margin: {left: 0, right: 0, top: 0, bottom: 0}
    // },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, xtext: 'TPS!!',
        x: 0, 
        xxxy: 100, 
        y: function(base, name, data, me){
            var vhtext = data['data']['textCount'] * 60;
            var vcount = data['data']['itemCount']-2;
            var vytext = (data['chip']['height'] *  vcount)/2+vhtext/2;
            return -vytext-50;
        },
        width: {data: 'chip', field: 'width'}, 
        height: function(base, name, data, me){
            return data['chip']['height'] * data['data']['itemCount'];
        },
        fontSize: 60,
        align: 'center',
        visible: {data: 'data', field: 'visible'},
        margin: {left: 10, top: 5, right: 5, bottom: 5}, 
        fill: '#333',
    },
  ]
}

var asbProduksiColorMap = {default: '#fff', line1: '#b00', line2: '#bbb', line3: '#ff0', line4: '#0b0', line5: '#038', line6: '#b0b', line7: '#840'}
var asbProduksiColorNames = {default: '', line1: 'Body Repair (1)', line2: 'Putty Sanding (1)', line3: 'Surfacer (1)', line4: 'Spraying (1)', 
    line5: 'Polish (1)', line6: 'Re-Assy (1)', line7: 'Final Inspection (1)'}
JsBoard.Chips.asb_produksi.produksi = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', 
      cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },

    {name: 'line_1', type: 'rect', x: 2, y:24, 
        width: 24, 
        height: 40,
        fill: asbProduksiColorMap['line1'], 
        stroke: '#555', strokeWidth: 1, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    
    {name: 'line_2', type: 'rect', x: 24, y:24, 
        width: 24, 
        height: 40,
        fill: asbProduksiColorMap['line2'], 
        stroke: '#555', strokeWidth: 1, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    
    {name: 'line_3', type: 'rect', x: 48, y:24, 
        width: 24, 
        height: 40,
        fill: asbProduksiColorMap['line3'], 
        stroke: '#555', strokeWidth: 1, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    
    {name: 'line_4', type: 'rect', x: 72, y:24, 
        width: 24, 
        height: 40,
        fill: asbProduksiColorMap['line4'], 
        stroke: '#555', strokeWidth: 1, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    
    {name: 'line_5', type: 'rect', x: 96, y:24, 
        width: 24, 
        height: 40,
        fill: asbProduksiColorMap['line5'], 
        stroke: '#555', strokeWidth: 1, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    
    {name: 'line_6', type: 'rect', x: 120, y:24, 
        width: 24, 
        height: 40,
        fill: asbProduksiColorMap['line6'], 
        stroke: '#555', strokeWidth: 1, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    
    {name: 'line_7', type: 'rect', x: 142, y:24, 
        width: 24, 
        height: 40,
        fill: asbProduksiColorMap['line7'], 
        stroke: '#555', strokeWidth: 1, 
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'selected', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, 
        visible: function(base, name, data, me){
            if (data['data']['selected']){
                return true
            } 
            return false;
        },
        margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 4, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    // {name: 'type', type: 'text', 
    //     text: function(base, name, data, me){
    //         return data['data']['col'];
    //         console.log("col", data['data']['col']);
    //         console.log("col info", me.layout.getInfoColData(data['data']['col']))
    //         if (data['data']['row']<0){
    //             return data['data']['tipe']
    //         } else {
    //             var pad = function(num, size) {
    //                 var s = "000000000" + num;
    //                 return s.substr(s.length-size);
    //             }        
                
    //             var vinfo = me.layout.getInfoColData(data['data']['col']+1);
    //             if (vinfo){
    //                 return pad(vinfo.title, 2)+':00';
    //             }
    //             return '*';
    //             // var vmin = data['chip']['incrementMinute'] * data['data']['col'];
    //             // var vminhour = JsBoard.Common.getMinute(data['chip']['startHour'])+vmin;
    //             // var vres = JsBoard.Common.getTimeString(vminhour);
    //             // return vres;
    //         }
    //     }, 
    //     x: 4, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    // {name: 'stallperson', type: 'text', 
    //     text: function(base, name, data, vcont){
    //         console.log("container", vcont);
    //         var vres = '';
    //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
    //             var vrow = data['data']['row'];
    //             var vdata = vcont.layout.getInfoRowData(vrow);
    //             if (vdata && vdata['person']){
    //                 vres = vdata['person'];
    //             }
    //         }
    //         return vres;
    //     },
    //     x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
    //     margin: {right: 14},
    // },
    // {name: 'stallname', type: 'text', 
    //     text: function(base, name, data, vcont){
    //         console.log("container", vcont);
    //         var vres = '';
    //         if (typeof vcont.layout.getInfoRowData !== 'undefined'){
    //             var vrow = data['data']['row'];
    //             var vdata = vcont.layout.getInfoRowData(vrow);
    //             if (vdata && vdata['title']){
    //                 vres = vdata['title'];
    //             }
    //         }
    //         return vres;
    //     },
    //     x: 4, y: 54, align: 'left', width: {data: 'chip', field: 'width'},
    // },
    {name: 'tipe_vhc', type: 'text', 
        text: function(base, name, data, vcont){
            return data['data']['tipe'] + ' - '+data['data']['sa_name']
            // console.log("container", vcont);
            var vres = '';
            if (typeof vcont.layout.getInfoRowData !== 'undefined'){
                var vrow = data['data']['row'];
                var vdata = vcont.layout.getInfoRowData(vrow);
                if (vdata && vdata['title']){
                    vres = vdata['title'];
                }
            }
            return vres;
        },
        x: 4, y: 64, align: 'left', width: {data: 'chip', field: 'width'},
    },
    // {name: 'normal', type: 'rect', x: 0, y:36, 
    //   width: function(base, name, data, me){
    //     console.log('chip-rect-2', data['chip']['width'])
    //     var vext = 0;
    //     var vwidth = 1;
    //     var vcolwidth = 50;
    //     var vnormal = data['data']['width'];
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     if (typeof data['data']['normal']!=='undefined'){
    //       vnormal = data['data']['normal'];
    //     }
    //     return vcolwidth*vnormal;
    //   }, 
    //   height: 21,
    //   fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //   colorMap: asbColorMap,
    //   lineColorMap: asbLineColorMap,
    //   xfill: '#fff',
    //   stroke: {type: 'map', mapdata: 'config', mapfield: 'lineColorMap', data: 'data', field: 'status'}, 
    //   strokeWidth: 1,
    //   margin: {left: 8, right: 6, top: 0, bottom: 0}
    // },
    // {name: 'icon', type: 'icons', 
    //     x: 4, y: 30, 
    //     icons: {data: 'data', field: 'statusicon', default: []}, 
    //     width: function(base, name, data, me){
    //         console.log('chip-rect-2', data['chip']['width'])
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //         vnormal = data['data']['normal'];
    //         }
    //         return vcolwidth*vnormal;
    //     }, 
    // },
  ]
}



var asbProduksiColorDistance = 100;

// JsBoard.Chips.asb_produksi.footer = {
//   items: [
//     {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
//       fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
//       margin: {left: 1, right: 1, top: 0, bottom: 1}
//     },
//     {name: 'rect1', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line1'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text1', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line1'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect2', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line2'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text2', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line2'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//         {name: 'rect3', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line3'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text3', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line3'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect4', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line4'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text4', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line4'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect5', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line5'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text5', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line5'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect6', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line6'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text6', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line6'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },
//     {name: 'rect7', type: 'rect', x: 0, y:0, 
//         width: 50, height: 30,
//         fill: asbProduksiColorMap['line7'], 
//         stroke: '#000', strokeWidth: 0.7,
//         margin: {left: 7, right: 7, top: 7, bottom: 7}
//     },
//     {name: 'text7', type: 'text', x: 50, y:0, 
//         width: 90, height: 30,
//         text: asbProduksiColorNames['line7'], 
//         fill: '#000',
//         wrap: 'none',
//         margin: {left: 0, right: 0, top: 10, bottom: 0}
//     },

//   ]
// }

JsBoard.Chips.asb_produksi.footer = {
    items: [
        {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: 30, xheight: {data: 'chip', field: 'height'},
        fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
        margin: {left: 1, right: 1, top: 0, bottom: 1}
        },
    ]
}

var vtemp1 = {name: 'rect1', type: 'rect', x: 0, y:0, 
        width: 50, height: 30,
        fill: asbProduksiColorMap['line1'], 
        stroke: '#000', strokeWidth: 0.7,
        margin: {left: 7, right: 7, top: 7, bottom: 7}
};
var vtemp2 = {name: 'text1', type: 'text', x: 50, y:0, 
        width: 100, height: 30,
        text: asbProduksiColorNames['line1'], 
        fill: '#000',
        wrap: 'none',
        margin: {left: 0, right: 0, top: 10, bottom: 0}
};

for (var i=1; i<8; i++){
    vleft = (i-1)*140;
    vtemp1.name = 'rect'+i;
    vtemp1.fill = asbProduksiColorMap['line'+i];
    vtemp1.x = vleft;
    vtemp2.name = 'text'+i;
    vtemp2.text = asbProduksiColorNames['line'+i];
    vtemp2.x = vleft+50;
    var xtemp1 = JsBoard.Common.extend({}, vtemp1);
    var xtemp2 = JsBoard.Common.extend({}, vtemp2);
    JsBoard.Chips.asb_produksi.footer.items.push(xtemp1);
    JsBoard.Chips.asb_produksi.footer.items.push(xtemp2);
}

JsBoard.Chips.asb_produksi.break = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#555', strokeWidth: 1,
      margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'breakText', type: 'text', 
        x: 7, y:{data: 'chip', field: 'height'}, 
        height: {data: 'chip', field: 'width'}, 
        width: {data: 'chip', field: 'height'},
      text: 'B R E A K',
      fontSize: 16,
      rotation: 270,
      align: 'center',
      fill: 'white', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}

