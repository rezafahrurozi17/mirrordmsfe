// chip for iregularity GR

JsBoard.Chips.satelitemon = {}

JsBoard.Chips.satelitemon.ready_pickup = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'branch', type: 'text', text: {data: 'data', field: 'branchname'}, x: 0, y: 4, 
        width: {data: 'chip', field: 'width'}, fontSize: 16,
        align: 'center', fontStyle: 'bold',
        margin: {left: 8, right: 90}},
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 24, width: {data: 'chip', field: 'width'}, 
        fontSize: 18, align: 'center',
        margin: {left: 8, right: 90}},
    {name: 'statusrect', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        fill: function(base, name, data, me){
            var vval = data['data']['daydiff'];
            if (vval==0){
                return '#0c0';
            } else {
                return '#c00';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#000', cornerRadius: 2, strokeWidth: 0.5,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'statustext', type: 'text', text: {data: 'data', field: 'status'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        align: 'center',
        fill: '#fff',
        fontSize: 20,
        margin: {top: 12}
    },
    {name: 'liner1', type: 'rect', x: 0, y:52, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'tipe'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'type', type: 'text', text: {data: 'data', field: 'tgl_masuk'}, x: 0, y: 72, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'icons', type: 'icons', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-16
        }, 
        y:54, 
        height: 24,
        iconWidth: 24, iconHeight: 24,
        margin: {left: 5, right: 0, top: 0, bottom: 0},
        icons: {data: 'data', field: 'statusicon', default: []}
    },
    {name: 'sarect', type: 'rect', 
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:56, 
        height: 28,
        fill: '#fff',
        stroke: '#000', cornerRadius: 2, strokeWidth: 0.5,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'satext', type: 'text', text: {data: 'data', field: 'sa_name'}, 
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:56, 
        height: 24,
        align: 'center',
        fill: '#000',
        fontSize: 16,
        margin: {top: 7}
    },
  ]
}

JsBoard.Chips.satelitemon.wait_delivery = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'branch', type: 'text', text: {data: 'data', field: 'branchname'}, x: 0, y: 4, 
        width: {data: 'chip', field: 'width'}, fontSize: 16,
        align: 'center', fontStyle: 'bold',
        margin: {left: 8, right: 90}},
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 24, width: {data: 'chip', field: 'width'}, 
        fontSize: 18, align: 'center',
        margin: {left: 8, right: 90}},
    {name: 'statusrect', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        fill: function(base, name, data, me){
            var vval = data['data']['daydiff'];
            if (vval==0){
                return '#fff';
            } else {
                return '#c00';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#000', 
        cornerRadius: 2, strokeWidth: 0.5,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'statustext', type: 'text', text: {data: 'data', field: 'status'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        align: 'center',
        fill: function(base, name, data, me){
            var vval = data['data']['daydiff'];
            if (vval==0){
                return '#000';
            } else {
                return '#fff';
            }
        },
        fontSize: 20,
        margin: {top: 12}
    },
    {name: 'liner1', type: 'rect', x: 0, y:52, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'tipe'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'tgl_masuk', type: 'text', text: {data: 'data', field: 'tgl_masuk'}, x: 0, y: 72, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'tgl_serah', type: 'text', text: {data: 'data', field: 'tgl_serah'}, x: 0, y: 92, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'sarect', type: 'rect', 
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:56, 
        height: 28,
        fill: '#fff',
        stroke: '#000', cornerRadius: 2, strokeWidth: 0.5,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'satext', type: 'text', text: {data: 'data', field: 'sa_name'}, 
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:56, 
        height: 24,
        align: 'center',
        fill: '#000',
        fontSize: 16,
        margin: {top: 7}
    },
  ]
}

JsBoard.Chips.satelitemon.ready_delivery = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'branch', type: 'text', text: {data: 'data', field: 'branchname'}, x: 0, y: 4, 
        width: {data: 'chip', field: 'width'}, fontSize: 16,
        align: 'center', fontStyle: 'bold',
        margin: {left: 8, right: 90}},
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 24, width: {data: 'chip', field: 'width'}, 
        fontSize: 18, align: 'center',
        margin: {left: 8, right: 90}},
    {name: 'statusrect', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        fill: function(base, name, data, me){
            var vval = data['data']['daydiff'];
            if (vval==0){
                return '#0c0';
            } else {
                return '#c00';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#000', cornerRadius: 2, strokeWidth: 0.5,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'statustext', type: 'text', text: {data: 'data', field: 'status'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        align: 'center',
        fill: '#fff',
        fontSize: 20,
        margin: {top: 12}
    },
    {name: 'liner1', type: 'rect', x: 0, y:52, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'tipe'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'type', type: 'text', text: {data: 'data', field: 'tgl_serah'}, x: 0, y: 72, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'sarect', type: 'rect', 
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:56, 
        height: 28,
        fill: '#fff',
        stroke: '#000', cornerRadius: 2, strokeWidth: 0.5,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'satext', type: 'text', text: {data: 'data', field: 'sa_name'}, 
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:56, 
        height: 24,
        align: 'center',
        fill: '#000',
        fontSize: 16,
        margin: {top: 7}
    },
  ]
}

JsBoard.Chips.satelitemon.wait_reception = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    },
    {name: 'branch', type: 'text', text: {data: 'data', field: 'branchname'}, x: 0, y: 4, 
        width: {data: 'chip', field: 'width'}, fontSize: 16,
        align: 'center', fontStyle: 'bold',
        margin: {left: 8, right: 90}},
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 0, y: 24, width: {data: 'chip', field: 'width'}, 
        fontSize: 18, align: 'center',
        margin: {left: 8, right: 90}},
    {name: 'statusrect', type: 'rect', 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        fill: function(base, name, data, me){
            var xval = data['data']['status'];
            var vval = JsBoard.Common.getMinute(xval);
            if (vval<=15){
                return '#0c0';
            } else if (vval<=30){
                return '#dc0';
            } else {
                return '#c00';
            }
        },
        colorMap: {default: '#f0f0f0', 2: '#e00', 1: '#ec0', 0: '#0a0'},
        stroke: '#000', cornerRadius: 2, strokeWidth: 0.5,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'statustext', type: 'text', text: {data: 'data', field: 'status'}, 
        width: 90, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:8, 
        height: 42,
        align: 'center',
        fill: '#fff',
        fontSize: 20,
        margin: {top: 12}
    },
    {name: 'liner1', type: 'rect', x: 0, y:52, 
        width: function(base, name, data, me){
            return data['chip']['width'] - 90-8
        }, 
        height: 1,
        fill: '#fff', stroke: '#ccc', strokeWidth: 1,
        margin: {left: 8, right: 8, top: 0, bottom: 0}
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'tipe'}, x: 0, y: 52, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'type', type: 'text', text: {data: 'data', field: 'tgl_masuk'}, x: 0, y: 72, width: {data: 'chip', field: 'width'}, fontSize: 16,
        margin: {left: 8}},
    {name: 'sarect', type: 'rect', 
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:56, 
        height: 28,
        fill: '#fff',
        stroke: '#000', cornerRadius: 2, strokeWidth: 0.5,
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'satext', type: 'text', text: {data: 'data', field: 'sa_name'}, 
        width: 60, 
        x: function(base, name, data, me){
            return data['chip']['width'] - data['config']['width']-8
        }, 
        y:56, 
        height: 24,
        align: 'center',
        fill: '#000',
        fontSize: 16,
        margin: {top: 7}
    },
  ]
}

