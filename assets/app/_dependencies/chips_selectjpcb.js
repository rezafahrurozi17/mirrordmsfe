// chip definition
JsBoard.Chips.selectboard = {}
JsBoard.Common.RandomColorList = ["#FF0000","#FFA800","#FAFF00","#33FF00","#00FFB3","#00B3FF","#0019FF","#AD00FF","#FF00E6","#888888","#990000","#996500","#969900","#1F9900","#00996B","#006B99","#000F99","#680099","#99008A","#525252","#FF9999","#FFDC99","#FDFF99","#ADFF99","#99FFE1","#99E1FF","#99A3FF","#DE99FF","#FF99F5","#DADADA","#CC0000","#CC8600","#C8CC00","#29CC00","#00CC8F","#008FCC","#0014CC","#8A00CC","#CC00B8","#6D6D6D","#FF6666","#FFCB66","#FCFF66","#85FF66","#66FFD1","#66D1FF","#6675FF","#CE66FF","#FF66F0","#BEBEBE","#FFCCCC","#FFEECC","#FEFFCC","#D6FFCC","#CCFFF0","#CCF0FF","#CCD1FF","#EFCCFF","#FFCCFA","#F5F5F5","#660000","#664300","#646600","#146600","#006648","#004866","#000A66","#450066","#66005C","#363636","#330000","#332200","#323300","#0A3300","#003324","#002433","#000533","#230033","#33002E","#1B1B1B"]


JsBoard.Chips.selectboard.jpcb = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#fff', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 6, right: 4, top: 2, bottom: 22}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#0f0', cornerRadius: 2, strokeWidth: 2, visible: false,
      margin: {left: 6, right: 4, top: 2, bottom: 22}
    },
    {name: 'blink', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: null, stroke: '#f66', cornerRadius: 2, strokeWidth: 2, 
      visible: function(base, name, data, me){
        if (data['data']['selected']){
            return true;
        } else {
            return false;
        }
      },
      margin: {left: 6, right: 4, top: 2, bottom: 22}
    },
    {name: 'nopol', type: 'text', text: {data: 'data', field: 'nopol'}, x: 8, y: 2, width: {data: 'chip', field: 'width'}, 
        wrap: 'none', fontStyle: 'bold',
        fontSize: function(base, name, data, me){
            if (data['data']['width']>0.5){
                return 11
            } else {
                return 10;
            }
        }
    },
    
    {name: 'tglcetakwo', type: 'text', text: {data: 'data', field: 'tglcetakwo'}, x: 4, y: 2, align: 'right', width: {data: 'chip', field: 'width'}, 
        margin: {right: 16},
    },
    {name: 'type', type: 'text', text: {data: 'data', field: 'type'}, x: 8, y: 16, width: {data: 'chip', field: 'width'}, wrap: 'none'},
    {name: 'jamcetakwo', type: 'text', text: {data: 'data', field: 'jamcetakwo'}, x: 4, y: 16, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 16},
    },
    {name: 'wotype', type: 'text', text: {data: 'data', field: 'wotype'}, x: 4, y: 54, align: 'right', width: {data: 'chip', field: 'width'},
        margin: {right: 16},
    },
    // {name: 'start2', type: 'text', text: {data: 'data', field: 'start2'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}},
    // {name: 'jamjanjiserah', type: 'text', text: {data: 'data', field: 'jamjanjiserah'}, x: 0, y: 13, align: 'right', width: {data: 'chip', field: 'width'}, 
    //     visible: function(base, name, data, me){
    //         if (data['data']['width']>0.5){
    //             return true
    //         } else {
    //             return false;
    //         }
    //     }
    // },
    // {name: 'pekerjaan', type: 'text', text: {data: 'data', field: 'pekerjaan'}, x: 0, y: 51, width: {data: 'chip', field: 'width'}},
    // {name: 'teknisi', type: 'text', text: {data: 'data', field: 'teknisi'}, x: 30, y: 51, align: 'right', 
    //     wrap: 'none',
    //     xwidth: {data: 'chip', field: 'width'},
    //     width: function(base, name, data, me){
    //         return data['chip']['width']-this.x;
    //     }
    // },
    {name: 'normal', type: 'rect', x: 0, y:36, 
      width: function(base, name, data, me){
        console.log('chip-rect-2', data['chip']['width'])
        var vext = 0;
        var vwidth = 1;
        var vcolwidth = 50;
        var vnormal = 1;
        if (typeof data['chip']['columnWidth']!=='undefined'){
            vcolwidth = data['chip']['columnWidth'];
        } 
        if (typeof data['data']['normal']!=='undefined'){
          vnormal = data['data']['normal'];
        }
        return vcolwidth*vnormal;
      }, 
      height: 21,
      xxfill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
      fill: function(base, name, data, me){
          return JsBoard.Common.RandomColorList[data['data']['color_idx']]
      },
    //   colorMap: {default: '#f0f0f0', belumdatang: '#fff', sudahdatang: '#C0C0C0', booking: '#4169E1', 
    //     walkin: '#3a3', inprogress: '#ffdd22', irregular: '#d33', extension: '#DDAA00', done: '#787878'},
      margin: {left: 12, right: 10, top: 0, bottom: 0}
    },
    // {name: 'ext', type: 'rect', x: 0, y:32, 
    //   x: function(base, name, data, me){
    //     var vnormal = 0;
    //     var vcolwidth = 50;
    //     if (typeof data['data']['normal']!=='undefined'){
    //       vnormal = data['data']['normal'];
    //     }
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     return vcolwidth*vnormal;
    //   }, 
    //   width: function(base, name, data, me){
    //     var vext = 0;
    //     var vcolwidth = 50;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vnormal = data['data']['ext'];
    //     }
    //     if (typeof data['chip']['columnWidth']!=='undefined'){
    //         vcolwidth = data['chip']['columnWidth'];
    //     } 
    //     return vcolwidth*vnormal;
    //   }, 
    //   visible: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return 1;
    //     } else {
    //       return 0
    //     }
    //   },
    //   height: 21,
    //   fill: '#ffA520', 
    //   margin: function(base, name, data, me){
    //     var vext = 0;
    //     if (typeof data['data']['ext']!=='undefined'){
    //       vext = data['data']['ext'];
    //     }
    //     if (vext>0){
    //       return {left: 0, top: 0, bottom: 0}
    //     } else {
    //       return {top: 0, bottom: 0}
    //     }
    //   }
    // },
    // {name: 'type', type: 'icons', 
    //     x: 0, y: 26, 
    //     icons: {data: 'data', field: 'statusicon', default: []},
    //     width: function(base, name, data, me){
    //         console.log('chip-rect-2', data['chip']['width'])
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //         vnormal = data['data']['normal'];
    //         }
    //         return vcolwidth*vnormal;
    //     }, 
    // },
    // {name: 'type', type: 'icons', 
    //     xxx: 25,
    //     x: function(base, name, data, me){
    //         var vext = 0;
    //         var vwidth = 1;
    //         var vcolwidth = 50;
    //         var vnormal = 1;
    //         if (typeof data['chip']['columnWidth']!=='undefined'){
    //             vcolwidth = data['chip']['columnWidth'];
    //         } 
    //         if (typeof data['data']['normal']!=='undefined'){
    //             vnormal = data['data']['normal'];
    //         }
    //         if (typeof data['data']['ext']!=='undefined'){
    //             vext = data['data']['ext'];
    //         }
    //         vwidth = vnormal+vext;
    //         return Math.round(vwidth*vcolwidth/2)-14;
    //     }, 
    //     y: 46, 
    //     icons: ['play2'],
    //     width: 20,
    //     height: 16,
    //     iconHeight: 14,
    //     iconWidth: 14,
    //     visible: function(base, name, data, me){
    //         vallocated = 1;
    //         if (typeof data['data']['allocated']!=='undefined'){
    //             vallocated = data['data']['allocated'];
    //         }
    //         if (vallocated){
    //             return false;
    //         } else {
    //             return true;
    //         }
    //     }
    // },
  ]
}

JsBoard.Chips.selectboard.jpcb_stall_base = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    // {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
    //     margin: {left: 5, top: 5, right: 5, bottom: 5}, 
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    // },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.selectboard.jpcb_stall = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'title'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 5, right: 5, bottom: 5}, 
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    },
    {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
        width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
        bullet: ' ',
        fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
        colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
        margin: {left: 25}
    },
  ]
}


JsBoard.Chips.selectboard.jpcb_blocked = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f00', stroke: '#777', cornerRadius: 2, strokeWidth: 1, opacity: 0.5,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}



JsBoard.Chips.selectboard.blank = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
      fill: '#f0f0f0', stroke: '#777', cornerRadius: 2, strokeWidth: 1,
      margin: {left: 2, right: 2, top: 2, bottom: 2}
    }
  ]
}


JsBoard.Chips.selectboard.jpcb_first_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ddd',
        xfill: function(base, name, data, me){
            if (data['data']['index'] %2 == 0){
                return '#ddd'
            } else {
                return '#fff'
            }
        },
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 15, top: 10, right: 5, bottom: 5}, 
        fill: '#f00',
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

JsBoard.Chips.selectboard.jpcb_header = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ddd',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
  ]
}

JsBoard.Chips.selectboard.jpcb_footer = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#aaa',
        margin: {left: 0, right: 0, top: -2, bottom: 0}
    },
  ]
}



JsBoard.Chips.selectboard.jpcb_headerx = {
  items: [
    {name: 'base', type: 'rect', x: 0, y:0, width: {data: 'chip', field: 'width'}, height: {data: 'chip', field: 'height'},
        fill: '#ccc',
        margin: {left: 0, right: 0, top: 0, bottom: 0}
    },
    {name: 'text', type: 'text', text: {data: 'data', field: 'text'}, x: 0, y: 0, width: {data: 'chip', field: 'width'}, fontSize: 14,
        margin: {left: 5, top: 10, right: 5, bottom: 5}, 
        fill: '#000',
    },
    // {name: 'textlist', type: 'list', items: {data: 'data', field: 'items'}, x: 0, y: 16, 
    //     width: {data: 'chip', field: 'width'}, height: 28, fontSize: 12,
    //     bullet: '-',
    //     fill: {type: 'map', mapdata: 'config', mapfield: 'colorMap', data: 'data', field: 'status'}, 
    //     colorMap: {default: '#000', normal: '#333', blank: '#aaa', blocked: '#a00'},
    //     margin: {left: 25}
    // },
  ]
}

