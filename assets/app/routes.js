//"use strict";

//angular.module('app', ['ui.router','ngResource']);

angular.module('app')
  .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function($locationProvider, $stateProvider, $urlRouterProvider) {

    $stateProvider.state('app.formly', {
        //url: '/formly',
        data: {
          title: 'Formly Demo',
          access: 1
        },
        views: {
          "formly@app": {
            templateUrl: '_sys/_demo/formlyDemo.html',
            controller: 'FormlyDemoController as vm'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('sysapp');
          }]
        }
      })
      .state('app.bsform', {
        data: {
          title: 'bsForm Demo',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "bsform@app": {
            templateUrl: 'app/bsform_demo/bsForm.html',
            controller: 'bsFormController'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('sysapp');
          }],
          AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]
        },
        params: {
          tab: null
        },
      })
      .state('app.bsformbase', {
        data: {
          title: 'bsFormBase Demo',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "bsformbase@app": {
            templateUrl: 'app/bsformbase_demo/bsFormBase.html',
            controller: 'bsFormBaseController'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('sysapp');
          }],
          AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]
        },
        params: {
          tab: null
        },
      })
      .state('app.bsformManual', {
        data: {
          title: 'bsForm Demo',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "bsformManual@app": {
            templateUrl: 'app/bsform_demo/bsForm_manual.html',
            controller: 'bsFormController_manual'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('sysapp');
          }],
          AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]
        },
        params: {
          tab: null
        },
      })
      .state('app.bsformPopup', {
        data: {
          title: 'bsForm Demo',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "bsformPopup@app": {
            templateUrl: 'app/bsform_demo/bsForm_popup.html',
            controller: 'bsFormController_popup'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('sysapp');
          }],
          AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]
        },
        params: {
          tab: null
        },
      })
      .state('app.kb', {
        //url: '/kb',
        data: {
          title: 'Knowledge Base Editor',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "kb@app": {
            templateUrl: 'app/kb/kb.html',
            controller: 'KBController'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('sysapp');
          }],
          AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]

        },
        params: {
          tab: null
        },
      })
      .state('app.kbview', {
        //url: '/kbview',
        data: {
          title: 'Knowledge Base',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "kbview@app": {
            templateUrl: 'app/kb/kb_view.html',
            controller: 'KBViewController'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            var x = $ocLazyLoad.load('sysapp');
            console.log("lazy load sysapp=>", x);
            return x;
          }],
          AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }],
        },
        params: {
          tab: null
        },
      })
      .state('app.tag', {
        //url: '/tag',
        data: {
          title: 'Tag Management',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "tag@app": {
            templateUrl: 'app/tag/tag.html',
            controller: 'TagController as vm'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('sysapp');
          }],
          AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }],
        },
        params: {
          tab: null
        },
      })
      // -------------------------- //
      // Application Module - Games //
      // -------------------------- //
      .state('app.game', {
        data: {
          title: 'Game Demo',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "game@app": {
            templateUrl: 'app/games/gameDemo.html',
            controller: 'GameDemoController'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('sysapp');
          }],
          AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]
        }
      })
      .state('app.game2', {
        data: {
          title: 'Game2 Demo',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "game2@app": {
            templateUrl: 'app/games/gameDemo2.html',
            controller: 'GameDemoController'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]
        }
      })
      .state('app.game3', {
        data: {
          title: 'Game3 Demo',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "game3@app": {
            templateUrl: 'app/games/gameDemo3.html',
            controller: 'GameDemoController'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]
        }
      })
      .state('app.wacom', {
        data: {
          title: 'Wacom',
          access: 1
        },
        deepStateRedirect: true,
        sticky: true,
        views: {
          "wacom@app": {
            templateUrl: 'app/games/wacom.html',
            //controller: 'TrainingController'
          }
        },
        resolve: {
          sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load('app');
          }]
        }
      })

    if (window.history && window.history.pushState) {
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });
    };
    $urlRouterProvider.deferIntercept();
  }]);



//   angular.module('app.layout', ['ui.router'])

// .config(function ($stateProvider, $urlRouterProvider) {


//     $stateProvider
//         .state('app', {
//             abstract: true,
//             views: {
//                 root: {
//                     templateUrl: 'app/themes/layout/layout.tpl.html'
//                 }
//             },
//             data: {
//                 access: 1
//             },
//             resolve: {
//                 scripts: function(lazyScript){
//                     return lazyScript.register([
//                             'sparkline',
//                             'easy-pie'
//                         ]);
//                 }
//             }
//         });
//     //$urlRouterProvider.otherwise('/dashboard');

// })


// $stateProvider
//         .state('sys', {
//                 abstract: true,
//                 views: {
//                     root: {
//                         templateUrl: 'app/themes/layout/layout.tpl.html'
//                     }
//                 },
//                 data: {
//                     access: 1
//                 },
//                 resolve: {
//                     scripts: function(lazyScript){
//                         return lazyScript.register([
//                                 'sparkline',
//                                 'easy-pie'
//                             ]);
//                     }
//                 }
//         })
