angular.module('LoginModule')
    .factory('AuthInterceptor', function($rootScope, $q, $injector, LocalService) {
        return {
            request: function(config) {
                //console.log('intercept request config:',config);
                var usePort = true;
                var port = '';
                if (LocalService.get('Svc_IP')) {
                    var svc_IP = LocalService.get('Svc_IP');
                    //console.log("svc_IP=>",svc_IP);
                    //var url='https://dms-training.toyota.astra.co.id';
                } else {
                    //var svc_IP = 'http://192.168.43.113';
                    //var svc_IP='http://10.85.40.30';  
                    var svc_IP = 'http://localhost';
                    //var svc_IP = 'https://dms-sit.toyota.astra.co.id';
                }
                // ---------------------------------------------
                var token;
                if (LocalService.get('BSLocalData')) {
                    token = angular.fromJson(LocalService.get('BSLocalData')).access_token;
                }
                if (token) {
                    config.headers.Authorization = 'Bearer ' + token;
                }
                //config.headers['Content-Type'] = 'application/json';
                //config.headers.Authorization = "ApiKey A23#qeJdho6X1ieKhjc8mZyxQ9+x5fsLHdoRBWz09RiY0832MIhgDIBz2fN4HCF9wHeyeM8BNCnN6k/6gp5NBHSvu7J88+ducD5EkX9Pz09KeFe7xSEZkNhRwZRU5gyKCTIjbsF3S9nKgJ6+vDCsS5yAbv6jwumajug9756Dw3ZC79g=";
                // ---------------------------------------------
                var xurl = config.url.split('/');
                //console.log('xurl:',xurl);
                if (xurl[1] == 'token' || xurl[2] == 'Logout') {
                    svc_IP = 'https://dms-sit.toyota.astra.co.id';
                    config.url = svc_IP + config.url;
                } else if (xurl[1] == 'api') {
                    //kalo publish ke sit atau test, jangan lupa dimatikan yang dibawah ini
                     switch (xurl[2]) {
                     case 'fw':
                     port = ':38001';
                     break;
                     case 'param':
                     port = ':38002';
                     break;
                     case 'ds':
                     port = ':38003';
                     break;
                     case 'rpt':
                     port = ':38004';
                     break;
                     case 'as':
                     port = ':38007';
                     break;
                     case 'sales':
                     port = ':38006';
                     break;
                     case 'fe':
                     port = ':38008';
                     break;
                     case 'crm':
                     svc_IP = 'https://dms-sit.toyota.astra.co.id';
                     port = '';
                     break;
                     case 'GetMenu':
                     svc_IP = 'https://dms-sit.toyota.astra.co.id';
                     port = '';
                     break;
                     }
                    config.url = svc_IP + (usePort ? port : '') + config.url;
                    //config.url = svc_IP + config.url;
                    //config.headers['Content-Type'] = 'application/json';
                }
                //console.log('intercept request config url:',config);
                return config;
            },
            responseError: function(response) {
                console.log("factory responseError=>", response);
                if (response.status <= 0) { // Request timeout
                    // $rootScope.ngDialog.open({ template: '_sys/auth/dialog_err_default.html' });
                    if ($rootScope.ngDialog.getOpenDialogs().length==0) {                        
                        $rootScope.ngDialog.open({ 
                            template: '_sys/auth/dialog_err_default.html',
                            controller: ['$scope', '$timeout', function($scope) {
                                $scope.status = "Error Koneksi";
                                $scope.message = "Mohon cek koneksi anda / aplikasi sedang dalam maintenance. Silakan coba beberapa saat lagi, Terima kasih";
                                $scope.debugMode = false;
                            }] 
                        });
                    }
                } else if (response.status == 401 && response.data.Message == "jwt expired") { // token expired
                    LocalService.unset('auth_token');
                    window.location = '/sys/auth/logout';
                } else if (response.status == 400) {
                    $rootScope.ngDialog.open({
                        template: '_sys/auth/dialog_err_default.html',
                        controller: ['$scope', '$timeout', function($scope) {
                            console.log("response=>", response);
                            // var msg=(msg==undefined?"":response.Message);
                            // var code=(response.data==""?"":" ("+response.data.code+")");
                            $scope.status = response.status + " (" + response.statusText + ")";
                            var msg = JSON.parse(response.data.Message);
                            console.log("msg,config=>", msg, response.config);
                            $scope.message = msg.Response.ResponseMessage;
                            $scope.sql = msg['Object'];
                            $scope.data = JSON.stringify(response.config.data);
                            $scope.url = response.config.url;
                            $scope.method = response.config.method;
                            $scope.debugMode = $rootScope.DEBUG_MODE.enabled;
                        }],
                    });
                } else if (response.status == 303) {
                    $rootScope.ngDialog.open({
                        template: '_sys/auth/dialog_err_default.html',
                        controller: ['$scope', '$timeout', function($scope) {
                            console.log("response=>", response);
                            // var msg=(msg==undefined?"":response.Message);
                            // var code=(response.data==""?"":" ("+response.data.code+")");
                            $scope.status = response.status + " (" + response.statusText + ")";
                            var msg = JSON.parse(response.data.Message);
                            console.log("msg,config=>", msg, response.config);
                            $scope.message = msg.Response.ResponseMessage;
                            $scope.sql = msg['Object'];
                            $scope.data = JSON.stringify(response.config.data);
                            $scope.url = response.config.url;
                            $scope.method = response.config.method;
                            $scope.debugMode = false;
                        }],
                    });
                } else {
                    try {
                        $rootScope.ngDialog.open({
                            template: '_sys/auth/dialog_err_default.html',
                            controller: ['$scope', '$timeout', function($scope) {
                                //console.log("respon=>",response);
                                var msg = (response.data.message == undefined ? "" : response.data.Message);
                                var code = (response.data == "" ? "" : " (" + response.data.code + ")");
                                $scope.status = response.status + " (" + response.statusText + ")";
                                $scope.message = response.data.Message;
                                $scope.data = JSON.stringify(response.config.data);
                                if ($scope.data == undefined)
                                {
                                    $scope.data = response.data.ExceptionMessage;
                                    if ($scope.data == undefined){ 
                                        $scope.data = new TextDecoder().decode(response.data);
                                    }
                                }
                                $scope.url = response.config.url;
                                $scope.method = response.config.method;
                                $scope.debugMode = $rootScope.DEBUG_MODE.enabled;
                            }],
                        });
                    } catch (err) {

                    } finally {
                        console.log("ResponseError=>", response);
                        if (response.status === 401) {
                            if (LocalService.get('BSLocalData')) {
                                // window.location = '/sys/auth/logout';
                            } else {
                                LocalService.unset('BSLocalData');
                            }
                            //$injector.get('$state').go('login');
                        }
                    }
                }
                return $q.reject(response);
            },
        }
    })
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });