angular.module('app')
.controller('spkOutstandingDetailBoard', function($scope,$interval,AfterSalesDashboard) {
	
	//awal nested pie prototype
		$scope.ShowLoadingDashboard_SpkOutstandingDetail=true;
		var DaCat1=AfterSalesDashboard.parsedData[8388608][0].Category.split(' ');
		var DaCat2=AfterSalesDashboard.parsedData[8388608][8].Category.split(' ');
		var DaCat3=AfterSalesDashboard.parsedData[8388608][4].Category.split(' ');
		
		$scope.SpkOutstandingDetailBoardc2=[{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][0].Fld2,
			"color": "#00AF50"
		  }, {
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][3].Fld2,
			"color": "#FFFF00"
		  }, {
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][6].Fld2,
			"color": "#FF0000"
		  }];
		$scope.SpkOutstandingDetailBoardc3=[{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][2].Fld2,
			"color": "#00AF50"
		  },{
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][8].Fld2,
			"color": "#FFFF00"
		  }, {
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][5].Fld2,
			"color": "#FF0000"
		  }];
		$scope.SpkOutstandingDetailBoardc4=[{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][1].Fld2,
			"color": "#00AF50"
		  },{
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][7].Fld2,
			"color": "#FFFF00"
		  },{
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][4].Fld2,
			"color": "#FF0000"
		  }];

		var chart2=AmCharts.makeChart("chart2", {
		  "type": "pie",
		  "bringToFront": true,
		  "allLabels": [{
						"text": DaCat1[3] + " hari",
						"align": "center",
						"bold": true,
						"color":"#DAC958",
						"y": 290
					  }],
		  "dataProvider": $scope.SpkOutstandingDetailBoardc2,
		  "startDuration": 1,
		  "pullOutRadius": 0,
		  "color": "#000000",
		  "fontSize": 12,
		  "titleField": "title",
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": -28,
		  "labelColor": "#000000",
			"radius": 80,
			"autoMargins": false,
		  "innerRadius": 0,
		  "outlineAlpha": 1,
		  "outlineThickness": 4,
		  "labelText": "[[value]]",
		  "balloonText": "[[title]]: ([[percents]]%)"
		});
		
		

		var chart3=AmCharts.makeChart("chart3", {
		  "type": "pie",
		  "bringToFront": true,
		  "allLabels": [{
						"text": DaCat2[3] + " hari",
						"align": "center",
						"bold": true,
						"color":"#DAC958",
						"y": 360
					  }],
		  "dataProvider": $scope.SpkOutstandingDetailBoardc3,
		  "startDuration": 1,
		  "pullOutRadius": 0,
		  "color": "#000000",
		  "fontSize": 12,
		  "titleField": "title",
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": -27,
		  "labelColor": "#000000",
			"radius": 175,
			"autoMargins": false,
		  "innerRadius": 82,
		  "outlineAlpha": 1,
		  "outlineThickness": 4,
		  "labelText": "[[value]]",
		  "balloonText": "[[title]]: ([[percents]]%)"
		});

		var chart4=AmCharts.makeChart("chart4", {
		  "type": "pie",
		  "bringToFront": true,
		  "legend": {
			"position": "bottom",
			"markerSize": 10,
			"valueText" : "",
			"divId": "legenddiv"
		  },
		  "dataProvider": $scope.SpkOutstandingDetailBoardc4,
		  "startDuration": 1,
		  "pullOutRadius": 0,
		  "color": "#000000",
		  "fontSize": 12,
		  "titleField": "title",
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": -27,
		  "labelColor": "#000000",
			"radius": 250,
			"autoMargins": false,
		  "innerRadius": 177,
		  "outlineAlpha": 1,
		  "outlineThickness": 4,
		  "labelText": "[[value]]",
		  "balloonText": "[[title]]: ([[percents]]%)",
		  "allLabels": [{
			"text": "",
			"bold": true,
			"size": 18,
			"color": "#404040",
			"x": 0,
			"align": "center",
			"y": 20
		  },{
						"text": DaCat3[3] + " hari",
						"align": "center",
						"bold": true,
						"color":"#DAC958",
						"y": 430
					  }]
		});
		$scope.ShowLoadingDashboard_SpkOutstandingDetail=false;
		//akhir nested pie prototype

    function refreshData(){
		
		//awal nested pie prototype
		DaCat1=AfterSalesDashboard.parsedData[8388608][0].Category.split(' ');
		DaCat2=AfterSalesDashboard.parsedData[8388608][8].Category.split(' ');
		DaCat3=AfterSalesDashboard.parsedData[8388608][4].Category.split(' ');
		
		$scope.SpkOutstandingDetailBoardc2=[{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][0].Fld2,
			"color": "#00AF50"
		  }, {
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][3].Fld2,
			"color": "#FFFF00"
		  }, {
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][6].Fld2,
			"color": "#FF0000"
		  }];
		  
		chart2.dataProvider = [];
		chart2.dataProvider.push($scope.SpkOutstandingDetailBoardc2[0]);
		chart2.dataProvider.push($scope.SpkOutstandingDetailBoardc2[1]);
		chart2.dataProvider.push($scope.SpkOutstandingDetailBoardc2[2]);
		chart2.allLabels[0].text = "";
		chart2.allLabels[0].text = DaCat1[3] + " hari";
		chart2.validateData();
		  
		$scope.SpkOutstandingDetailBoardc3=[{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][2].Fld2,
			"color": "#00AF50"
		  },{
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][8].Fld2,
			"color": "#FFFF00"
		  }, {
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][5].Fld2,
			"color": "#FF0000"
		  }];
		  
		chart3.dataProvider = [];
		chart3.dataProvider.push($scope.SpkOutstandingDetailBoardc3[0]);
		chart3.dataProvider.push($scope.SpkOutstandingDetailBoardc3[1]);
		chart3.dataProvider.push($scope.SpkOutstandingDetailBoardc3[2]);
		chart3.allLabels[0].text = "";
		chart3.allLabels[0].text = DaCat2[3] + " hari";
		chart3.validateData();
		  
		$scope.SpkOutstandingDetailBoardc4=[{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][1].Fld2,
			"color": "#00AF50"
		  },{
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][7].Fld2,
			"color": "#FFFF00"
		  },{
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][4].Fld2,
			"color": "#FF0000"
		  }];
		  
		chart4.dataProvider = [];
		chart4.dataProvider.push($scope.SpkOutstandingDetailBoardc4[0]);
		chart4.dataProvider.push($scope.SpkOutstandingDetailBoardc4[1]);
		chart4.dataProvider.push($scope.SpkOutstandingDetailBoardc4[2]);
		chart4.allLabels[1].text = "";
		chart4.allLabels[1].text = DaCat3[3] + " hari";
		chart4.validateData();

		
		//akhir nested pie prototype

		
    }
    refreshData();

    $interval(function(){
        refreshData();
    },5000);
});