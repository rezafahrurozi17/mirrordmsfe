angular.module('app')
  .factory('BoardSalesGlobalParameterDashBoardFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
	  getDataBoardSalesArOverdue: function() {
                    var res=$http.get('/api/sales/GlobalParameterAROverdue');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesProspect: function() {
                    var res=$http.get('/api/sales/GlobalParameterProspect');
                    //console.log('res=>',res);
                return res;
      },
	  getDataBoardSalesSpkRsBoard: function() {
					var res=$http.get('/api/sales/GlobalParameterTop5TargetSPKRS');
                    //console.log('res=>',res);
                return res;
      },
    }
  });
 //ddd