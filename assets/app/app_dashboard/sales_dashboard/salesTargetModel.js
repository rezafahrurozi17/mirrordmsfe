angular.module('app')
.controller('DashboardSalesTargetModel', function($scope,$interval,AfterSalesDashboard) {
	$scope.ShowLoadingDashboard_SalesTargetModel=true;
	

    function refreshData(){
		AfterSalesDashboard.getData().then(function(data){
			parseData(data.data);
		})
		//target monthly fld 1
		//monthly data fld 2
		//Weekly data fld 2
		//persen monthly fld 3
		//target dailt fld 4
		//persen Weekly fld 5
		//paling atas 9 paling bawah 5
		
		//awal stacked bar prototype
		var DaHotProspectToSplit = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-1].Category;
		var DaHotProspectToSplitUdaDipisah = DaHotProspectToSplit.split("?");
		$scope.SalesTargetModelJoedoel="";
		$scope.SalesTargetModelJoedoel=angular.copy(DaHotProspectToSplitUdaDipisah[0]);
		
		
		var DaHotProspectToSplitUdaDipisahNongolin=DaHotProspectToSplitUdaDipisah[1];
		DaHotProspectToSplitUdaDipisah = DaHotProspectToSplit.split(" ");
		DaHotProspectToSplitUdaDipisahNongolin = DaHotProspectToSplitUdaDipisahNongolin.replace(" ","\n");
		// for(var i = 1; i < DaHotProspectToSplitUdaDipisah.length; ++i)
		// {
		// 	DaHotProspectToSplitUdaDipisahNongolin+="\n"+DaHotProspectToSplitUdaDipisah[i];
		// }
		
		var DaSPKToSplit = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-2].Category;
		var DaSPKToSplitUdaDipisah = DaSPKToSplit.split("?");
		//var DaSPKToSplitUdaDipisahNongolin=DaSPKToSplitUdaDipisah[0];
		var DaSPKToSplitUdaDipisahNongolin=DaSPKToSplitUdaDipisah[1];
		DaSPKToSplitUdaDipisah = DaSPKToSplit.split(" ");
		for(var i = 1; i < DaSPKToSplitUdaDipisah.length; ++i)
		{
			DaSPKToSplitUdaDipisahNongolin+="\n"+DaSPKToSplitUdaDipisah[i];
		}
		
		var DaDOToSplit = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-3].Category;
		var DaDOToSplitUdaDipisah = DaDOToSplit.split("?");
		
		var DaDOToSplitUdaDipisahNongolin=DaDOToSplitUdaDipisah[1];
		DaDOToSplitUdaDipisah = DaDOToSplit.split(" ");
		DaDOToSplitUdaDipisahNongolin = DaDOToSplitUdaDipisahNongolin.replace(" ","\n");
		
		
		// var DaSpkToSplit = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-4].Category;
		// var DaSpkToSplitUdaDipisah = DaSpkToSplit.split("?");
		// //var DaSpkToSplitUdaDipisahNongolin=DaSpkToSplitUdaDipisah[0];
		// var DaSpkToSplitUdaDipisahNongolin=DaSpkToSplitUdaDipisah[1];
		// DaSpkToSplitUdaDipisah = DaSpkToSplit.split(" ");
		// for(var i = 1; i < DaSpkToSplitUdaDipisah.length; ++i)
		// {
			// DaSpkToSplitUdaDipisahNongolin+="\n"+DaSpkToSplitUdaDipisah[i];
		// }
		
		var DaProspectToSplit = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-5].Category;
		var DaProspectToSplitUdaDipisah = DaProspectToSplit.split("?");
		//var DaProspectToSplitUdaDipisahNongolin=DaProspectToSplitUdaDipisah[0];
		var DaProspectToSplitUdaDipisahNongolin=DaProspectToSplitUdaDipisah[1];
		DaProspectToSplitUdaDipisah = DaProspectToSplit.split(" ");
		for(var i = 1; i < DaProspectToSplitUdaDipisah.length; ++i)
		{
			DaProspectToSplitUdaDipisahNongolin+="\n"+DaProspectToSplitUdaDipisah[i];
		}
		
		var chartstackedbarSalesTarget = AmCharts.makeChart("chartstackedbarSalesTargetModel", {
					  "type": "serial",
					  "theme": "light",
					  "rotate": true,
					  "legend":{
						"position":"bottom",
						"align":"center",
						"valueText" : "",
						"autoMargins":false,
						"marginTop":10
					  },
					  "marginBottom": 50,
					  "dataProvider": [
					  {
						"Tampang": DaProspectToSplitUdaDipisahNongolin,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-5].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-5].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-5].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-5].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-5].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-5].Fld1
					  },
					  {
						"Tampang": DaHotProspectToSplitUdaDipisahNongolin,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-1].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-1].Fld5),
						"MonthlyData": Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-1].Fld2),
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-1].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-1].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-1].Fld1
					  },{
						"Tampang": DaSPKToSplitUdaDipisahNongolin,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-2].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-2].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-2].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-2].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-2].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-2].Fld1
					  },{
						"Tampang": DaDOToSplitUdaDipisahNongolin,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-3].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-3].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-3].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-3].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-3].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-3].Fld1
					  },
					  // {
						// "Tampang": DaSpkToSplitUdaDipisahNongolin,
						// "WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-4].Fld2),
						// "PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-4].Fld5),
						// "MonthlyData": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-4].Fld2,
						// "PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-4].Fld3,
						// "TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-4].Fld4),
						// "TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-4].Fld1
					  // },
					  ],
					  "startDuration": 0,
					  "graphs": [{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "TargetWeekly",
						"title": "Weekly Target",
						"labelText": " ",
						"labelFunction": function(data) {return '';},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + ' ('+Math.abs(data.dataContext.PercentWeekly)+'%)';},
						"clustered": false,
						"fillColors": "#8DD7F7",
						"legendColor": "#8DD7F7"


					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "WeeklyData",
						"title": "Weekly Achievement",
						"labelText": " ",
						"labelFunction": function(data) {return Math.abs(data.values.value) + '';},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + '';},
						"clustered": false,
						"fillColors": "#FFF8AD",
						"legendColor": "#FFF8AD"


					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "TargetMonthly",
						"title": "Monthly Target",
						"labelText": " ",
						"labelFunction": function(data) {return '';},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + ' ('+data.dataContext.PercentMonthly+'%)';},
						"clustered": false,
						"fillColors": "#8DD7F7",
						"legendColor": "#8DD7F7"

					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "MonthlyData",
						"title": "Monthly Achievement",
						"labelText": "",
						"clustered": false,
						"fillColors": "#FFF8AD",
						"legendColor": "#FFF8AD"

					  }],
					  "categoryField": "Tampang",
					  "categoryAxis": {
						"gridPosition": "start",
						"gridAlpha": 0.2,
						"axisAlpha": 0
					  },
					  "valueAxes": [{
						"gridAlpha": 0,
						"ignoreAxisWidth": true,
						"labelFunction": function(value) {
						  return Math.abs(value) + '';
						},
						"guides": [{
						  "value": 0,
						  "lineAlpha": 0.2
						}]
					  }],
					  "balloon": {
						"fixedPosition": true
					  },
					  "chartCursor": {
						"valueBalloonsEnabled": false,
						"cursorAlpha": 0.05,
						"fullWidth": true
					  },
					  "allLabels": [{
						"text": "Weekly",
						"x": "28%",
						"y": "97%",
						"bold": true,
						"align": "middle"
					  }, {
						"text": "Monthly",
						"x": "75%",
						"y": "97%",
						"bold": true,
						"align": "middle"
					  }],
					 "export": {
						"enabled": false
					  }

					});
					$scope.ShowLoadingDashboard_SalesTargetModel=false;
		//akhir stacked bar prototype
    }
    refreshData();

    $interval(function(){
        refreshData();
    },60000);
});