angular.module('app')
.controller('DashboardSpkBoard', function($scope,$interval,AfterSalesDashboard) {

	$scope.getAllCar = AfterSalesDashboard.parsedData[2097152];
	$scope.SelectRSCar = [];
	$scope.SelectSPKCar = [];
	for (var i = 0; i < $scope.getAllCar.length; i++) {
		var a = $scope.getAllCar[i].Category.substring(12, 9);
		if (a !== 'SPK') {
			$scope.SelectRSCar.push($scope.getAllCar[i]);
		} else {
			$scope.SelectSPKCar.push($scope.getAllCar[i]);
		}
	}


	console.log('ini mobil SPK muwhehe ===>', $scope.SelectSPKCar);
	console.log('ini mobil RS  muwehehe===>', $scope.SelectRSCar);

	$scope.ShowLoadingDashboard_SpkBoard=true;
	$scope.TodayDateBoardSpk_Raw=new Date();
	$scope.TodayMonthMinus1_Raw=new Date($scope.TodayDateBoardSpk_Raw.setMonth($scope.TodayDateBoardSpk_Raw.getMonth()-1));
	try
	{

		for (var i = 0; i < $scope.SelectSPKCar.length; i++) {
			$scope.SelectSPKCar[i].carName = $scope.SelectSPKCar[i].Category.substring(14); 
		}


	}
	catch(ExceptionComment)
	{

		$scope.SelectSPKCar.warna_target_spk = "warna_birulangit";
		$scope.SelectSPKCar.ach_total_spk = "warna_merah";
		$scope.SelectSPKCar.ach_total_last_spk = "warna_last_baru";


		$scope.BoardSpkAchLastPercentage = 0;
		$scope.BoardSpkAchPercentage = 0;

		$scope.BoardSpkTargetSpkLastPercentage = 0;
		$scope.BoardSpkTotalAchievementPercent = 0;
		$scope.BoardSpkTargetSpk = 0;

		$scope.BoardSpkAchTarget = 0;
		$scope.BoardSpkAchLast = 0;


		
	}
	
	

    function refreshData(){
		try
		{


			$scope.BoardSpkTotalAchievement = $scope.SelectSPKCar[0].Fld10;
			$scope.BoardSpkTotalAchievementPercent = $scope.SelectSPKCar[0].Fld12;
			if ($scope.BoardSpkTotalAchievementPercent > 90) {
				$scope.ach_total_spk = "warna_hijau";
			}
			else if ($scope.BoardSpkTotalAchievementPercent <= 90 && $scope.BoardSpkTotalAchievementPercent >= 50) {
				$scope.ach_total_spk = "warna_kuning";
			}
			else if ($scope.BoardSpkTotalAchievementPercent < 50) {
				$scope.ach_total_spk = "warna_merah";
			}


			$scope.BoardSpkTargetSpk = $scope.SelectSPKCar[0].Fld11;
			$scope.BoardSpkTargetSpkLast = $scope.SelectSPKCar[0].Fld4;
			$scope.BoardSpkTargetSpkLastPercentage = $scope.SelectSPKCar[0].Fld6;
			if ($scope.BoardSpkTargetSpkLastPercentage > 90) {
				$scope.ach_total_last_spk = "warna_last_baru";
			}
			else if ($scope.BoardSpkTargetSpkLastPercentage <= 90 && $scope.BoardSpkTargetSpkLastPercentage >= 50) {
				$scope.ach_total_last_spk = "warna_last_baru";
			}
			else if ($scope.BoardSpkTargetSpkLastPercentage < 50) {
				$scope.ach_total_last_spk = "warna_last_baru";
			}




			//FOR LOOP
		for (var i = 0; i < $scope.SelectSPKCar.length; i++) {
			$scope.SelectSPKCar[i].BoardSpkAch = $scope.SelectSPKCar[i].Fld7;
			$scope.SelectSPKCar[i].CategoryBoardSpkAchPercentage = $scope.SelectSPKCar[i].Fld9;
			$scope.SelectSPKCar[i].carName = $scope.SelectSPKCar[i].Category.substring(14);

			$scope.SelectSPKCar[i].warna_target_spk = "warna_birulangit";
			if ($scope.SelectSPKCar[i].CategoryBoardSpkAchPercentage > 90) {
				$scope.SelectSPKCar[i].ach_total_spk = "warna_hijau";
			}
			else if ($scope.SelectSPKCar[i].CategoryBoardSpkAchPercentage <= 90 && $scope.SelectSPKCar[i].CategoryBoardSpkAchPercentage >= 50) {
				$scope.SelectSPKCar[i].ach_total_spk = "warna_kuning";
			}
			else if ($scope.SelectSPKCar[i].CategoryBoardSpkAchPercentage < 50) {
				$scope.SelectSPKCar[i].ach_total_spk = "warna_merah";
			}
			$scope.SelectSPKCar[i].BoardSpkAchTarget = $scope.SelectSPKCar[i].Fld8;
			$scope.SelectSPKCar[i].BoardSpkAchLast = $scope.SelectSPKCar[i].Fld1;
			$scope.SelectSPKCar[i].BoardSpkAchLastPercentage = $scope.SelectSPKCar[i].Fld3;


			if ($scope.SelectSPKCar[i].BoardSpkAchLastPercentage > 90) {
				$scope.SelectSPKCar[i].ach_total_last_spk = "warna_last_baru";
			}
			else if ($scope.SelectSPKCar[i].BoardSpkAchLastPercentage <= 90 && $scope.SelectSPKCar[i].BoardSpkAchLastPercentage >= 50) {
				$scope.SelectSPKCar[i].ach_total_last_spk = "warna_last_baru";
			}
			else if ($scope.SelectSPKCar[i].BoardSpkAchLastPercentage < 50) {
				$scope.SelectSPKCar[i].ach_total_last_spk = "warna_last_baru";
			}
		}
			
		




			$scope.ShowLoadingDashboard_SpkBoard=false;
		}
		catch(ExceptionComment)
		{

			$scope.ach_total_spk = "warna_merah";
			$scope.ach_total_last_spk = "warna_last_baru";


			$scope.BoardSpkAchLastPercentage = 0;
			$scope.BoardSpkAchPercentage = 0;

			$scope.BoardSpkTargetSpkLastPercentage = 0;
			$scope.BoardSpkTotalAchievementPercent = 0;
			$scope.BoardSpkTargetSpk = 0;

			$scope.BoardSpkAchTarget = 0;
			$scope.BoardSpkAchLast = 0;

			
			$scope.ShowLoadingDashboard_SpkBoard=false;
		}
		
		var chart2 = AmCharts.makeChart("SpkDashboardGantiTabelKeGrafik", {
  "type": "serial",
  "theme": "light",
  "dataProvider": [{
    "VehicleModelName": $scope.BoardSpkCat6,
    "PenjualanBulanIni": $scope.BoardSpkAch6
  }, {
    "VehicleModelName": $scope.BoardSpkCat1,
    "PenjualanBulanIni": $scope.BoardSpkAch1
  }, {
    "VehicleModelName": $scope.BoardSpkCat2,
    "PenjualanBulanIni": $scope.BoardSpkAch2
  }, {
    "VehicleModelName": $scope.BoardSpkCat3,
    "PenjualanBulanIni": $scope.BoardSpkAch3
  }, {
    "VehicleModelName": $scope.BoardSpkCat4,
    "PenjualanBulanIni": $scope.BoardSpkAch4
  }, {
    "VehicleModelName": $scope.BoardSpkCat5,
    "PenjualanBulanIni": $scope.BoardSpkAch5
  }, {
    "VehicleModelName": "Total",
    "PenjualanBulanIni": $scope.BoardSpkTotalAchievement
  }],
  "categoryField": "VehicleModelName",
  "graphs": [{
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "PenjualanBulanIni",
	"labelText": "[[category]]"
  }],
  "categoryAxis": {
    "labelsEnabled": false,
  },
  "rotate": true
});
    }
    refreshData();

    $interval(function(){
        refreshData();
    },5000);
});