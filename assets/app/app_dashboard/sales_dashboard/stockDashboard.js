angular.module('app')
.controller('StockDashboard', function($scope,$interval,AfterSalesDashboard) {
	$scope.ShowLoadingDashboard_StockDashboard=true;
	

    function refreshData(){
		
		//awal batang
		
		//"totalText": "[[total]]"
		//More_Than_30 tu fld1
		//Ten_To_30_Days tu fld2
		//Zero_To_10_Days tu fld3
		//Matching tu fld4
		$scope.Data_Dummy_Chart_Batang=[{
				"Model": AfterSalesDashboard.parsedData[4194304][0].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][0].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][0].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][0].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][0].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][0].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][1].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][1].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][1].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][1].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][1].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][1].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][2].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][2].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][2].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][2].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][2].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][2].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][3].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][3].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][3].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][3].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][3].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][3].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][4].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][4].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][4].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][4].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][4].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][4].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][5].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][5].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][5].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][5].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][5].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][5].Fld5
			}];
			
		for(var i = 0; i < $scope.Data_Dummy_Chart_Batang.length; ++i)
		{
			if($scope.Data_Dummy_Chart_Batang[i]['Total_Taon_Lalu']!=0)
			{
				$scope.Data_Dummy_Chart_Batang[i]['Itungan_Taon_Lalu']="S/R:"+($scope.Data_Dummy_Chart_Batang[i]['Matching']+$scope.Data_Dummy_Chart_Batang[i]['Zero_To_10_Days']+$scope.Data_Dummy_Chart_Batang[i]['Ten_To_30_Days']+$scope.Data_Dummy_Chart_Batang[i]['More_Than_30'])/$scope.Data_Dummy_Chart_Batang[i]['Total_Taon_Lalu']+" bln)";
			}
			else if($scope.Data_Dummy_Chart_Batang[i]['Total_Taon_Lalu']==0)
			{
				$scope.Data_Dummy_Chart_Batang[i]['Itungan_Taon_Lalu']="Data bulan lalu <br> tak ditemukan) <br><br>";
			}
			
		}	
		
		var chart = AmCharts.makeChart("chartdivbatang", {
			"type": "serial",
			"theme": "light",
			"legend": {
				"horizontalGap": 10,
				"maxColumns": 4,
				"position": "bottom",
				"align":"center",
				"useGraphSettings": true,
				"markerSize": 10
			},
			"dataProvider": $scope.Data_Dummy_Chart_Batang,
			"valueAxes": [{
				"stackType": "regular",
				"axisAlpha": 0.3,
				"gridAlpha": 0,
				"totalText": "[[total]]  ([[Itungan_Taon_Lalu]]<br><br>",
			}],
			"graphs": [{
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": "Matching",
				"type": "column",
				"color": "#000000",
				"fillColors": "#00bfff",
				"valueField": "Matching"
			}, {
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": ""+AfterSalesDashboard.parsedData[4194304][0].Fld10+"-"+AfterSalesDashboard.parsedData[4194304][0].Fld11+" Days",
				"type": "column",
				"color": "#000000",
				"fillColors": "#00AF50",
				"valueField": "Zero_To_10_Days"
			}, {
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": ""+AfterSalesDashboard.parsedData[4194304][0].Fld11+"-"+AfterSalesDashboard.parsedData[4194304][0].Fld12+" Days",
				"type": "column",
				"color": "#000000",
				"fillColors": "#FFFF00",
				"valueField": "Ten_To_30_Days"
			}, {
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": ">"+AfterSalesDashboard.parsedData[4194304][0].Fld12+" Days",
				"type": "column",
				"color": "#000000",
				"fillColors": "#FF0000",
				"valueField": "More_Than_30"
			}],
			"categoryField": "Model",
			"categoryAxis": {
				"gridPosition": "start",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"position": "left"
			},
			"export": {
				"enabled": false
			 }

		});
		$scope.ShowLoadingDashboard_StockDashboard=false;
		//akhir batang
    }
    refreshData();

    $interval(function(){
        refreshData();
    },5000);
});