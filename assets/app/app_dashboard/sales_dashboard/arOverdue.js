angular.module('app')
.controller('DashboardArOverdue', function($scope,$filter,CurrentUser,$http,$interval,AfterSalesDashboard,BoardSalesGlobalParameterDashBoardFactory,LocalService) {
	var chartpie3d="";
	$scope.ShowLoadingDashboard_ArOverdue=true;
	
	// if(JSON.parse(LocalService.get('CategoryArOverdue1'))==null||JSON.parse(LocalService.get('CategoryArOverdue2'))==null||JSON.parse(LocalService.get('CategoryArOverdue3'))==null)
	// {
		// BoardSalesGlobalParameterDashBoardFactory.getDataBoardSalesArOverdue().then(function (res) {
					// $scope.BoardSalesGlobalParamArOverdue = res.data.Result[0];
					
					// var DaCategoryArOverdue1 = JSON.stringify(""+res.data.Result[0].AgingPeriod1+"-"+res.data.Result[0].AgingPeriod2);
					// LocalService.unset('CategoryArOverdue1');
					// LocalService.set('CategoryArOverdue1', DaCategoryArOverdue1);
					
					// var DaCategoryArOverdue2 = JSON.stringify(""+res.data.Result[0].AgingPeriod2+"-"+res.data.Result[0].AgingPeriod3);
					// LocalService.unset('CategoryArOverdue2');
					// LocalService.set('CategoryArOverdue2', DaCategoryArOverdue2);
					
					// var DaCategoryArOverdue3 = JSON.stringify(">"+res.data.Result[0].AgingPeriod3);
					// LocalService.unset('CategoryArOverdue3');
					// LocalService.set('CategoryArOverdue3', DaCategoryArOverdue3);
					// return res.data;
				// });
	// }
			
	//awal pie chart 3d
	$scope.IsiDataPieChart3d=[ {
					"CategoryArOverdue": ""+AfterSalesDashboard.parsedData[16777216][0].Fld10+"-"+AfterSalesDashboard.parsedData[16777216][0].Fld11,
					"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld3
					},{
					"CategoryArOverdue": ""+AfterSalesDashboard.parsedData[16777216][0].Fld11+"-"+AfterSalesDashboard.parsedData[16777216][0].Fld12,
					"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld2
					}, {
					"CategoryArOverdue": ">"+AfterSalesDashboard.parsedData[16777216][0].Fld12,
					"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld1
					} ];
	$scope.TotalIsichartpie3d=0;
	for(var x = 0; x < $scope.IsiDataPieChart3d.length; ++x)
	{
		$scope.TotalIsichartpie3d+=$scope.IsiDataPieChart3d[x].ArOverdueValue;
	}
	var ulala=angular.copy($scope.TotalIsichartpie3d);
	$scope.TotalIsichartpie3d=$filter('currency')(ulala, "Rp. ");			
	chartpie3d = AmCharts.makeChart( "chartdivpie3d", {
				  "type": "pie",
				  "theme": "light",
				  "legend":{
					"position":"bottom",
					"valueText" : "",
					"align": "center",
					"autoMargins":false
					},
					"dataProvider": $scope.IsiDataPieChart3d,
					"valueField": "ArOverdueValue",
					"titleField": "CategoryArOverdue",
				 "labelText": "Rp. [[value]], ([[percents]]%)",
				 "outlineAlpha": 0.4,
				 "depth3D": 15,
					"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
				 "angle": 30,
				"export": {
				"enabled": false
			  }
			} );
			$scope.ShowLoadingDashboard_ArOverdue=false;
	//akhir pie chart 3d			

    function refreshData(){
	
		
					$scope.IsiDataPieChart3d=[ {
									"CategoryArOverdue": ""+AfterSalesDashboard.parsedData[16777216][0].Fld10+"-"+AfterSalesDashboard.parsedData[16777216][0].Fld11,
									"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld3
								  },{
									"CategoryArOverdue": ""+AfterSalesDashboard.parsedData[16777216][0].Fld11+"-"+AfterSalesDashboard.parsedData[16777216][0].Fld12,
									"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld2
								  }, {
									"CategoryArOverdue": ">"+AfterSalesDashboard.parsedData[16777216][0].Fld12,
									"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld1
								  } ];
					$scope.TotalIsichartpie3d=0;
					for(var x = 0; x < $scope.IsiDataPieChart3d.length; ++x)
					{
						$scope.TotalIsichartpie3d+=$scope.IsiDataPieChart3d[x].ArOverdueValue;
					}
					var ulala=angular.copy($scope.TotalIsichartpie3d);
					$scope.TotalIsichartpie3d=$filter('currency')(ulala, "Rp. ");
					chartpie3d.dataProvider = [];
					chartpie3d.dataProvider.push($scope.IsiDataPieChart3d[0]);
					chartpie3d.dataProvider.push($scope.IsiDataPieChart3d[1]);
					chartpie3d.dataProvider.push($scope.IsiDataPieChart3d[2]);
					chartpie3d.validateData();
    }
    //refreshData();

    $interval(function(){
        refreshData();
    },5000);
});