angular.module('app')
.controller('DashboardSalesTarget', function($scope,$interval,AfterSalesDashboard) {
	$scope.ShowLoadingDashboard_SalesTarget=true;


    function refreshData(){
		AfterSalesDashboard.getData().then(function(data){
			parseData(data.data);
		})
		//target monthly fld 1
		//monthly data fld 2
		//Weekly data fld 2
		//persen monthly fld 3
		//target dailt fld 4
		//persen Weekly fld 5
		//paling atas 4 paling bawah 0

		//awal stacked bar prototype

		var DaProspectHotToSplit1 = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-6].Category;
		var DaProspectHotToSplitUdaDipisah1 = DaProspectHotToSplit1.split(" ");
		var DaProspectHotToSplitUdaDipisahNongolin1=DaProspectHotToSplitUdaDipisah1[0];
		for(var i = 1; i < DaProspectHotToSplitUdaDipisah1.length; ++i)
		{
			DaProspectHotToSplitUdaDipisahNongolin1+="\n"+DaProspectHotToSplitUdaDipisah1[i];
			
		}

		var DaProspectHotToSplit2 = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-7].Category;
		var DaProspectHotToSplitUdaDipisah2 = DaProspectHotToSplit2.split(" ");
		var DaProspectHotToSplitUdaDipisahNongolin2=DaProspectHotToSplitUdaDipisah2[0];
		for(var i = 1; i < DaProspectHotToSplitUdaDipisah2.length; ++i)
		{
			DaProspectHotToSplitUdaDipisahNongolin2+="\n"+DaProspectHotToSplitUdaDipisah2[i];
			
		}

		var DaProspectHotToSplit3 = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-8].Category;
		var DaProspectHotToSplitUdaDipisah3 = DaProspectHotToSplit3.split(" ");
		var DaProspectHotToSplitUdaDipisahNongolin3=DaProspectHotToSplitUdaDipisah3[0];
		for(var i = 1; i < DaProspectHotToSplitUdaDipisah3.length; ++i)
		{
			DaProspectHotToSplitUdaDipisahNongolin3+="\n"+DaProspectHotToSplitUdaDipisah3[i];
			
		}

		var DaProspectHotToSplit4 = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-9].Category;
		var DaProspectHotToSplitUdaDipisah4 = DaProspectHotToSplit4.split(" ");
		var DaProspectHotToSplitUdaDipisahNongolin4=DaProspectHotToSplitUdaDipisah4[0];
		for(var i = 1; i < DaProspectHotToSplitUdaDipisah4.length; ++i)
		{
			DaProspectHotToSplitUdaDipisahNongolin4+="\n"+DaProspectHotToSplitUdaDipisah4[i];
			
		}

		var DaProspectHotToSplit5 = AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-10].Category;
		var DaProspectHotToSplitUdaDipisah5 = DaProspectHotToSplit5.split(" ");
		var DaProspectHotToSplitUdaDipisahNongolin5=DaProspectHotToSplitUdaDipisah5[0];
		for(var i = 1; i < DaProspectHotToSplitUdaDipisah5.length; ++i)
		{
			DaProspectHotToSplitUdaDipisahNongolin5+="\n"+DaProspectHotToSplitUdaDipisah5[i];
			
		}


		var chartstackedbarSalesTarget = AmCharts.makeChart("chartstackedbarSalesTarget", {
					  "type": "serial",
					  "theme": "light",
					  "rotate": true,
					  "legend":{
						"position":"bottom",
						"align":"center",
						"valueText" : "",
						"autoMargins":false,
						"marginTop":10
					  },
					  "marginBottom": 50,
					  "dataProvider": [{
						//"Tampang": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-6].Category,
						"Tampang": DaProspectHotToSplitUdaDipisahNongolin1,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-6].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-6].Fld5),
						"MonthlyData": Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-6].Fld2),
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-6].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-6].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-6].Fld1,
					  },{
						"Tampang": DaProspectHotToSplitUdaDipisahNongolin2,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-7].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-7].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-7].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-7].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-7].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-7].Fld1
					  },{
						"Tampang": DaProspectHotToSplitUdaDipisahNongolin3,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-8].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-8].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-8].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-8].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-8].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-8].Fld1
					  },{
						"Tampang": DaProspectHotToSplitUdaDipisahNongolin4,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-9].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-9].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-9].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-9].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-9].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-9].Fld1
					  },{
						"Tampang": DaProspectHotToSplitUdaDipisahNongolin5,
						"WeeklyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-10].Fld2),
						"PercentWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-10].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-10].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-10].Fld3,
						"TargetWeekly": -Math.abs(AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-10].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][AfterSalesDashboard.parsedData[1048576].length-10].Fld1
					  }],
					  "startDuration": 0,
					  "graphs": [{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "TargetWeekly",
						"title": "Weekly Target",
						"labelText": " ",
						"labelFunction": function(data) {return '';},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + ' ('+Math.abs(data.dataContext.PercentWeekly)+'%)';},
						"clustered": false,
						"fillColors": "#8DD7F7",
						"legendColor": "#8DD7F7"



					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "WeeklyData",
						"title": "Weekly Achievement",
						"labelText": " ",
						"labelFunction": function(data) {return Math.abs(data.values.value) + '';;},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + '';},
						"clustered": false,
						"fillColors": "#FFF8AD",
						"legendColor": "#FFF8AD"


					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "TargetMonthly",
						"title": "Monthly Target",
						"labelText": " ",
						"labelFunction": function(data) {return '';},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + ' ('+data.dataContext.PercentMonthly+'%)';},
						"clustered": false,
						"fillColors": "#8DD7F7",
						"legendColor": "#8DD7F7"

					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "MonthlyData",
						"title": "Monthly Achievement",
						"labelText": "",
						"clustered": false,
						"fillColors": "#FFF8AD",
						"legendColor": "#FFF8AD"

					  }],
					  "categoryField": "Tampang",
					  "categoryAxis": {
						"gridPosition": "start",
						"gridAlpha": 0.2,
						"axisAlpha": 0,
						
					  },
					  "valueAxes": [{
						"gridAlpha": 0,
						"ignoreAxisWidth": true,
						"labelFunction": function(value) {
						  return Math.abs(value) + '';
						},
						"guides": [{
						  "value": 0,
						  "lineAlpha": 0.2
						}]
					  }],
					  "balloon": {
						"fixedPosition": true
					  },
					  "chartCursor": {
						"valueBalloonsEnabled": false,
						"cursorAlpha": 0.05,
						"fullWidth": true
					  },
					  "allLabels": [{
						"text": "Weekly",
						"x": "28%",
						"y": "97%",
						"bold": true,
						"align": "middle"
					  }, {
						"text": "Monthly",
						"x": "75%",
						"y": "97%",
						"bold": true,
						"align": "middle"
					  }],
					 "export": {
						"enabled": false
					  }

					});
					$scope.ShowLoadingDashboard_SalesTarget=false;
		//akhir stacked bar prototype
    }
    refreshData();

    $interval(function(){
        refreshData();
    },60000);
});