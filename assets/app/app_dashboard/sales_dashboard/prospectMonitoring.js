angular.module('app')
.controller('DashboardProspectMonitoring', function($scope,$interval,AfterSalesDashboard,LocalService,BoardSalesGlobalParameterDashBoardFactory) {
	$scope.ShowLoadingDashboard_ProspectMonitoring1=true;
	$scope.ShowLoadingDashboard_ProspectMonitoring2=true;
	// if(JSON.parse(LocalService.get('CategoryProspectMonitoring1'))==null||JSON.parse(LocalService.get('CategoryProspectMonitoring2'))==null||JSON.parse(LocalService.get('CategoryProspectMonitoring3'))==null)
	// {
		// BoardSalesGlobalParameterDashBoardFactory.getDataBoardSalesProspect().then(function (res) {
			// $scope.BoardSalesGlobalParamProspectMonitoring = res.data.Result[0];
						
			// var DaCategoryProspectMonitoring1 = JSON.stringify(""+res.data.Result[0].AgingPeriod1+"-"+res.data.Result[0].AgingPeriod2);
			// LocalService.unset('CategoryProspectMonitoring1');
			// LocalService.set('CategoryProspectMonitoring1', DaCategoryProspectMonitoring1);
						
			// var DaCategoryProspectMonitoring2 = JSON.stringify(""+res.data.Result[0].AgingPeriod2+"-"+res.data.Result[0].AgingPeriod3);
			// LocalService.unset('CategoryProspectMonitoring2');
			// LocalService.set('CategoryProspectMonitoring2', DaCategoryProspectMonitoring2);
						
			// var DaCategoryProspectMonitoring3 = JSON.stringify(">"+res.data.Result[0].AgingPeriod3);
			// LocalService.unset('CategoryProspectMonitoring3');
			// LocalService.set('CategoryProspectMonitoring3', DaCategoryProspectMonitoring3);
			// return res.data;
		// });
	// }

	try
	{
		$scope.ProspectMonitoringDropHot=AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld7;
	}
	catch (HotDropProspectMonitoring) 
	{
		$scope.ProspectMonitoringDropHot=0;
	}
	//awal pie chart with label1
	try
	{
		console.log('AfterSalesDashboard',AfterSalesDashboard);
		$scope.IsiDataPieChartWithLabel1=[ {
						"Prospect": "Hot",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld1,
						"color": "#FF0000"
					  },{
						"Prospect": "Medium",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld2,
						"color": "#FFFF00"
					  },{
						"Prospect": "Low",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld3,
						"color": "#00AF50"
					  } ];
	}
	catch (ulala) 
	{
		$scope.IsiDataPieChartWithLabel1=[ {
						"Prospect": "Hot",
						"Jumlah": 0,
						"color": "#FF0000"
					  },{
						"Prospect": "Medium",
						"Jumlah": 0,
						"color": "#FFFF00"
					  },{
						"Prospect": "Low",
						"Jumlah": 0,
						"color": "#00AF50"
					  } ];
	}
		

		var chartpieProspectWithLabel1 = AmCharts.makeChart( "chartdivProspectWithLabel1", {
					  "type": "pie",
					  "labelRadius": -25,
					  "legend":{
						"position":"bottom",
						"valueText" : "",
						"align":"center",
						"autoMargins":true,
						"maxColumns": 1
					  },
					  "labelText": "[[value]]",
					  "dataProvider":$scope.IsiDataPieChartWithLabel1 ,
					  "valueField": "Jumlah",
					  "titleField": "Prospect",
					  "colorField": "color",
					   "balloon":{
					   "fixedPosition":true
					  }
					} );
					$scope.ShowLoadingDashboard_ProspectMonitoring1=false;
		//akhir pie chart with label
		
		//awal pie chart with label2
					  
		try
		{
			$scope.IsiDataPieChartWithLabel2=[ {
						"Prospect": ">="+(AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld10+1)+" Days",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld6,
						"color": "#FF0000"
					  },{
						"Prospect": ""+(AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld9+1)+"-"+AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld10+" Days",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld5,
						"color": "#FFFF00"
					  },{
						"Prospect": ""+AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld8+"-"+AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld9+" Days",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld4,
						"color": "#00AF50"
					  } ];
		}
		catch(ulala) 
		{
			$scope.IsiDataPieChartWithLabel2=[ {
						"Prospect": "Data >=20 Tidak Tersedia",
						"Jumlah": 0,
						"color": "#FF0000"
					  },{
						"Prospect": "Data 11-20 Tidak Tersedia",
						"Jumlah": 0,
						"color": "#FFFF00"
					  },{
						"Prospect": "Data 3-10 Tidak Tersedia",
						"Jumlah": 0,
						"color": "#00AF50"
					  } ];
		}
		var chartpieProspectWithLabel2 = AmCharts.makeChart( "chartdivProspectWithLabel2", {
					  "type": "pie",
					  "labelRadius": -25,
					  "legend":{
						"position":"bottom",
						"valueText" : "",
						"align":"center",
						"autoMargins":true,
						"maxColumns": 1
					  },
					  "labelText": "[[value]]",
					  "dataProvider":$scope.IsiDataPieChartWithLabel2 ,
					  "valueField": "Jumlah",
					  "titleField": "Prospect",
					  "colorField": "color",
					   "balloon":{
					   "fixedPosition":true
					  }
					} );
					$scope.ShowLoadingDashboard_ProspectMonitoring2=false;
		//akhir pie chart with label

    function refreshData(){
		AfterSalesDashboard.getData().then(function(data){
			parseData(data.data);
		})
		try
		{
			$scope.ProspectMonitoringDropHot=AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld7;
		}
		catch (HotDropProspectMonitoring) 
		{
			$scope.ProspectMonitoringDropHot=0;
		}

		//Fld1 hot
		//Fld2 medium
		//Fld3 low
		
        //awal pie chart with label1
		try
		{
			$scope.IsiDataPieChartWithLabel1=[ {
						"Prospect": "Hot",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld1,
						"color": "#FF0000"
					  },{
						"Prospect": "Medium",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld2,
						"color": "#FFFF00"
					  },{
						"Prospect": "Low",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld3,
						"color": "#00AF50"
					  } ];
		}
		catch(ulala)
		{
			$scope.IsiDataPieChartWithLabel1=[ {
						"Prospect": "Hot",
						"Jumlah": 0,
						"color": "#FF0000"
					  },{
						"Prospect": "Medium",
						"Jumlah": 0,
						"color": "#FFFF00"
					  },{
						"Prospect": "Low",
						"Jumlah": 0,
						"color": "#00AF50"
					  } ];
		}
		
		
		chartpieProspectWithLabel1.dataProvider = [];
		chartpieProspectWithLabel1.dataProvider.push($scope.IsiDataPieChartWithLabel1[0]);
		chartpieProspectWithLabel1.dataProvider.push($scope.IsiDataPieChartWithLabel1[1]);
		chartpieProspectWithLabel1.dataProvider.push($scope.IsiDataPieChartWithLabel1[2]);
		chartpieProspectWithLabel1.validateData();

		//akhir pie chart with label
		
		//awal pie chart with label2
		try
		{
			$scope.IsiDataPieChartWithLabel2=[ {
						"Prospect": ">="+(AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld10+1)+" Days",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld6,
						"color": "#FF0000"
					  },{
						"Prospect": ""+(AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld9+1)+"-"+AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld10+" Days",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld5,
						"color": "#FFFF00"
					  },{
						"Prospect": ""+AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld8+"-"+AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld9+" Days",
						"Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-2].Fld4,
						"color": "#00AF50"
					  } ];
		}
		catch(ulala)
		{
			$scope.IsiDataPieChartWithLabel2=[ {
						"Prospect": "Data >=20 Tidak Tersedia",
						"Jumlah": 0,
						"color": "#FF0000"
					  },{
						"Prospect": "Data 11-20 Tidak Tersedia",
						"Jumlah": 0,
						"color": "#FFFF00"
					  },{
						"Prospect": "Data 3-10 Tidak Tersedia",
						"Jumlah": 0,
						"color": "#00AF50"
					  } ];
		}
		
		
		chartpieProspectWithLabel2.dataProvider = [];
		chartpieProspectWithLabel2.dataProvider.push($scope.IsiDataPieChartWithLabel2[0]);
		chartpieProspectWithLabel2.dataProvider.push($scope.IsiDataPieChartWithLabel2[1]);
		chartpieProspectWithLabel2.dataProvider.push($scope.IsiDataPieChartWithLabel2[2]);
		chartpieProspectWithLabel2.validateData();
		//akhir pie chart with label
    }
    refreshData();

    $interval(function(){
        refreshData();
    },60000);
});