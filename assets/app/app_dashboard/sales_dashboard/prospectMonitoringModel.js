angular.module('app')
    .controller('DashboardProspectMonitoringModel', function($scope, $interval, AfterSalesDashboard, LocalService, BoardSalesGlobalParameterDashBoardFactory) {
		$scope.ShowLoadingDashboard_ProspectMonitoringModel1=true;
		$scope.ShowLoadingDashboard_ProspectMonitoringModel2=true;
        // if (JSON.parse(LocalService.get('CategoryProspectMonitoringModel1')) == null || JSON.parse(LocalService.get('CategoryProspectMonitoringModel2')) == null || JSON.parse(LocalService.get('CategoryProspectMonitoringModel3')) == null) {
            // BoardSalesGlobalParameterDashBoardFactory.getDataBoardSalesProspect().then(function(res) {
                // $scope.BoardSalesGlobalParamProspectMonitoringModel = res.data.Result[0];

                // var DaCategoryProspectMonitoringModel1 = JSON.stringify("" + res.data.Result[0].AgingPeriod1 + "-" + res.data.Result[0].AgingPeriod2);
                // LocalService.unset('CategoryProspectMonitoringModel1');
                // LocalService.set('CategoryProspectMonitoringModel1', DaCategoryProspectMonitoringModel1);

                // var DaCategoryProspectMonitoringModel2 = JSON.stringify("" + res.data.Result[0].AgingPeriod2 + "-" + res.data.Result[0].AgingPeriod3);
                // LocalService.unset('CategoryProspectMonitoringModel2');
                // LocalService.set('CategoryProspectMonitoringModel2', DaCategoryProspectMonitoringModel2);

                // var DaCategoryProspectMonitoringModel3 = JSON.stringify(">" + res.data.Result[0].AgingPeriod3);
                // LocalService.unset('CategoryProspectMonitoringModel3');
                // LocalService.set('CategoryProspectMonitoringModel3', DaCategoryProspectMonitoringModel3);
                // return res.data;
            // });
        // }

        try
		{
			$scope.ProspectMonitoringModelDropHot=AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld7;
		}
		catch (HotDropProspectMonitoring) 
		{
			$scope.ProspectMonitoringModelDropHot=0;
		}
		try {
            //awal pie chart with label1
            
            
			var DaShitToSplit = angular.copy(AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Category);
			DaShitToSplit = DaShitToSplit.split("?");
	
			$scope.ProspectMonitoringNamaModel_1=DaShitToSplit[0];
			$scope.ProspectMonitoringNamaModel_2=DaShitToSplit[1];
			$scope.ProspectMonitoringNamaModel_3=DaShitToSplit[2];

			
			$scope.IsiDataPieChartWithLabel1 = [{
                "Prospect": "Hot",
                "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld1,
				"color": "#FF0000"
            }, {
                "Prospect": "Medium",
                "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld2,
				"color": "#FFFF00"
            }, {
                "Prospect": "Low",
                "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld3,
				"color": "#00AF50"
            }];



            var chartpieProspectWithLabel1 = AmCharts.makeChart("chartdivProspectWithLabel1_2", {
                "type": "pie",
                "labelRadius": -25,
                "legend": {
                    "position": "bottom",
                    "valueText": "",
					"align":"center",
                    "autoMargins": true,
					"maxColumns": 1
                },
                "labelText": "[[value]]",
                "dataProvider": $scope.IsiDataPieChartWithLabel1,
                "valueField": "Jumlah",
                "titleField": "Prospect",
				"colorField": "color",
                "balloon": {
                    "fixedPosition": true
                }
            });
			$scope.ShowLoadingDashboard_ProspectMonitoringModel1=false;
		
            //akhir pie chart with label

            //awal pie chart with label2
            $scope.IsiDataPieChartWithLabel2 = [{
                "Prospect": ">=" + (AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld10+1)+" Days",
                "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld6,
				"color": "#FF0000"
            },{
                "Prospect": "" + (AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld9+1) + "-" + AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld10+" Days",
                "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld5,
				"color": "#00AF50"
            },{
                "Prospect": "" + AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld8 + "-" + AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld9+" Days",
                "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld4,
				"color": "#FFFF00"
            }];



            var chartpieProspectWithLabel2 = AmCharts.makeChart("chartdivProspectWithLabel2_2", {
                "type": "pie",
                "labelRadius": -25,
                "legend": {
                    "position": "bottom",
                    "valueText": "",
					"align":"center",
                    "autoMargins": true,
					"maxColumns": 1
                },
                "labelText": "[[value]]",
                "dataProvider": $scope.IsiDataPieChartWithLabel2,
                "valueField": "Jumlah",
                "titleField": "Prospect",
				"colorField": "color",
                "balloon": {
                    "fixedPosition": true
                }
            });
			$scope.ShowLoadingDashboard_ProspectMonitoringModel2=false;
            //akhir pie chart with label
        } catch (DaExceptionAtas) {
            //$scope.ProspectMonitoringNamaModelProspectMonitoringNamaModel = "";
			$scope.ProspectMonitoringNamaModel = "";
        }


        function refreshData() {
			AfterSalesDashboard.getData().then(function(data){
				parseData(data.data);
			})
			
			try
			{
				var DaShitToSplit = angular.copy(AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Category);
				DaShitToSplit = DaShitToSplit.split("?");
		
				$scope.ProspectMonitoringNamaModel_1=DaShitToSplit[0];
				$scope.ProspectMonitoringNamaModel_2=DaShitToSplit[1];
				$scope.ProspectMonitoringNamaModel_3=DaShitToSplit[2];
				$scope.ProspectMonitoringModelDropHot=AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld7;
			}
			catch (HotDropProspectMonitoring) 
			{
				$scope.ProspectMonitoringModelDropHot=0;
			}
            try {
                //awal pie chart with label1
                //$scope.ProspectMonitoringNamaModelProspectMonitoringNamaModel = AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Category;
                $scope.ProspectMonitoringNamaModel = AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Category;
				$scope.IsiDataPieChartWithLabel1 = [{
                    "Prospect": "Hot",
                    "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld1,
					"color": "#FF0000"
                }, {
                    "Prospect": "Medium",
                    "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld2,
					"color": "#FFFF00"
                }, {
                    "Prospect": "Low",
                    "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld3,
					"color": "#00AF50"
                }];
                chartpieProspectWithLabel1.dataProvider = [];
                chartpieProspectWithLabel1.dataProvider.push($scope.IsiDataPieChartWithLabel1[0]);
                chartpieProspectWithLabel1.dataProvider.push($scope.IsiDataPieChartWithLabel1[1]);
                chartpieProspectWithLabel1.dataProvider.push($scope.IsiDataPieChartWithLabel1[2]);
                chartpieProspectWithLabel1.validateData();

                //akhir pie chart with label

                //awal pie chart with label2
                $scope.IsiDataPieChartWithLabel2 = [{
                    "Prospect": ">=" + (AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld10+1)+" Days",
                    "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld6,
					"color": "#FF0000"
                },{
                    "Prospect": "" + (AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld9+1) + "-" + AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld10+" Days",
                    "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld5,
					"color": "#FFFF00"
                },{
                    "Prospect": "" + AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld8 + "-" + AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld9+" Days",
                    "Jumlah": AfterSalesDashboard.parsedData[524288][AfterSalesDashboard.parsedData[524288].length-1].Fld4,
					"color": "#00AF50"
                }];
                chartpieProspectWithLabel2.dataProvider = [];
                chartpieProspectWithLabel2.dataProvider.push($scope.IsiDataPieChartWithLabel2[0]);
                chartpieProspectWithLabel2.dataProvider.push($scope.IsiDataPieChartWithLabel2[1]);
                chartpieProspectWithLabel2.dataProvider.push($scope.IsiDataPieChartWithLabel2[2]);
                chartpieProspectWithLabel2.validateData();

                //akhir pie chart with label
            } catch (DaException) {
                $scope.ProspectMonitoringNamaModelProspectMonitoringNamaModel = "";
            }

        }
        refreshData();

        $interval(function() {
            refreshData();
        }, 60000);
    });