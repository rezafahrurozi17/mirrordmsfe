angular.module('app')
.controller('DashboardCtrl2', function($scope,Auth,$state,$timeout,bsBadge) {
    //console.log("dashboard controller=>");
          var resizeNodes=function() {
              //$("#layoutContainer_"+attrs.formName).height($(window).height() - 174); //- $("#footerDiv").height() - $("#menuDiv").height()
          };
          $timeout(function () {
              // Set height initially
              resizeNodes();
              // Add a watch on window.resize callback
              $(window).resize(function(){
                  resizeNodes();
              })
          });
          $scope.incBadge = function(n){
              var s=n||1;
              var m = bsBadge.set('app.stall',s);
              bsBadge.set('app.orgtype',s);
              bsBadge.set('app.role',s);
              //console.log('m inc=>',m);
          }
          $scope.decBadge = function(n){
              var s=n*-1||-1;
              var m = bsBadge.set('app.stall',s);
              bsBadge.set('app.orgtype',s);
              bsBadge.set('app.role',s);
              //console.log('m dec=>',m);
          }
});