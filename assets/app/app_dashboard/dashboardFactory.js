angular.module('app')
.factory('AfterSalesDashboard', function($http) {
    return {
    	parsedData: {},
      	getData: function() {
        	// return $http.get('/images/upload/DashboardResult.json');
        	return $http.get('/api/fwdms/dashboard');
		  },
		  updateDeviceId: function(obj){
			return $http.put('/api/fwdms/updateDeviceId?devType='+obj.devType+'&deviceId='+obj.deviceId)
		}
    }
});