angular.module('app')
.controller('PartsmanBP', function($scope,$interval,AfterSalesDashboard) {

    $scope.options1 = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#38c';
        //   else
            return '#5b9435';
        },
        scaleColor:false,
        lineWidth:10,
        lineCap:'circle',
        size:140,
    };
    $scope.options2 = angular.copy($scope.options1);
    $scope.options3 = angular.copy($scope.options1);
    $scope.options4 = angular.copy($scope.options1);

    $scope.options2.barColor = function(percent){
        // if(percent>=70)
        //     return '#3c8';
        // else
            return '#d36e2a';
    };
    $scope.options3.barColor = function(percent){
        // if(percent>=70)
        //     return '#c5c';
        // else
            return '#d36e2a';
    };
    $scope.options4.barColor = function(percent){
        // if(percent>=70)
        //     return '#fc0';
        // else
            return '#508abc';
    };

    $scope.barOptionsFillRate = {
        data: [],
        type: "serial",
        theme: "light",
        categoryField: "month",
        // rotate: true,
        legend: {
            enabled: true
        },
        categoryAxis: {
            gridPosition: "start",
            parseDates: false
        },
        // valueAxes: [{
        //     position: "top",
        //     title: "Million USD"
        // }],
        graphs: [{
            type: "column",
            // title: "Income",
            valueField: "value",
            fillAlphas: 1,
        }]
    }

    $scope.barOptionsImFillRate = angular.copy($scope.barOptionsFillRate);
    $scope.barOptionsSvcRate = angular.copy($scope.barOptionsFillRate);
    $scope.barOptionsStockDay = angular.copy($scope.barOptionsFillRate);
    // $scope.barOptionsImFillRate.graphs[0].valueField = "Fld7";
    // $scope.barOptionsSvcRate.graphs[0].valueField = "Fld8";
    // $scope.barOptionsStockDay.graphs[0].valueField = "Fld9";

    $scope.multilineOptions = {
        type: "serial",
        theme: "light",
        marginTop: 0,
        marginRight: 80,
        dataProvider: [],
        valueAxes: [{
            "axisAlpha": 0,
            "position": "left"
        }],
        graphs: [{
            "id": "g1",
            "balloonText": "Tipe 1 :<br><b><span style='font-size:14px;'>[[value1]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#09bdb1",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value1"
        }, {
            "id": "g2",
            "balloonText": "Tipe 2 :<br><b><span style='font-size:14px;'>[[value2]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#38474a",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value2"
        }, {
            "id": "g3",
            "balloonText": "Tipe 3 :<br><b><span style='font-size:14px;'>[[value3]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#ff6261",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value3"
        },{
            "id": "g4",
            "balloonText": "Non TAM :<br><b><span style='font-size:14px;'>[[value4]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#eecc13",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value4"
        },{
            "id": "g5",
            "balloonText": "Tipe T :<br><b><span style='font-size:14px;'>[[value5]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#A020F0",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value5"
        },

        {
            "id": "g6",
            "balloonText": "Campaign 3 & C :<br><b><span style='font-size:14px;'>[[value6]]</span></b>",
            "bullet": "round",
            "bulletSize": 8,
            "lineColor": "#4086ff",
            "lineThickness": 2,
            "negativeLineColor": "#637bb6",
            "type": "smoothedLine",
            "valueField": "value6"

        }
    ],
        chartCursor: {
            "categoryBalloonDateFormat": "YYYY",
            "cursorAlpha": 0,
            "valueLineEnabled": true,
            "valueLineBalloonEnabled": true,
            "valueLineAlpha": 0.5,
            "fullWidth": true
        },
        dataDateFormat: "MMM",
        categoryField: "month",
        categoryAxis: {
            "minPeriod": "MMM",
            "parseDates": false,
            "minorGridAlpha": 0.1,
            "minorGridEnabled": true
        },
    }
    function pisahData(data) {
        var tmpDataFillRate = [
            { id: 1, value: 0, month: "January" },
            { id: 2, value: 0, month: "February" },
            { id: 3, value: 0, month: "March" },
            { id: 4, value: 0, month: "April" },
            { id: 5, value: 0, month: "May" },
            { id: 6, value: 0, month: "June" },
            { id: 7, value: 0, month: "July" },
            { id: 8, value: 0, month: "August" },
            { id: 9, value: 0, month: "September" },
            { id: 10, value: 0, month: "October" },
            { id: 11, value: 0, month: "November" },
            { id: 12, value: 0, month: "December" },
        ];
        var tmpDataImmediate = [
            { id: 1, value: 0, month: "January" },
            { id: 2, value: 0, month: "February" },
            { id: 3, value: 0, month: "March" },
            { id: 4, value: 0, month: "April" },
            { id: 5, value: 0, month: "May" },
            { id: 6, value: 0, month: "June" },
            { id: 7, value: 0, month: "July" },
            { id: 8, value: 0, month: "August" },
            { id: 9, value: 0, month: "September" },
            { id: 10, value: 0, month: "October" },
            { id: 11, value: 0, month: "November" },
            { id: 12, value: 0, month: "December" },
        ];
        var tmpDataServiceRate = [
            { id: 1, value: 0, month: "January" },
            { id: 2, value: 0, month: "February" },
            { id: 3, value: 0, month: "March" },
            { id: 4, value: 0, month: "April" },
            { id: 5, value: 0, month: "May" },
            { id: 6, value: 0, month: "June" },
            { id: 7, value: 0, month: "July" },
            { id: 8, value: 0, month: "August" },
            { id: 9, value: 0, month: "September" },
            { id: 10, value: 0, month: "October" },
            { id: 11, value: 0, month: "November" },
            { id: 12, value: 0, month: "December" },
        ];
        var tmpDataComposition = [
            { id: 1, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "January" },
            { id: 2, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "February" },
            { id: 3, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "March" },
            { id: 4, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "April" },
            { id: 5, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "May" },
            { id: 6, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "June" },
            { id: 7, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "July" },
            { id: 8, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "August" },
            { id: 9, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "September" },
            { id: 10, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "October" },
            { id: 11, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "November" },
            { id: 12, value1: 0, value2:0, value3:0, value4:0, value5:0, value6:0, month: "December" },
        ];
        var tmpDataStockDay = angular.copy(tmpDataFillRate);
        for (var i in data) {
            if (data[i].Category == 'BAR_FILLRATE') {
                for (var j in tmpDataFillRate) {
                    if (tmpDataFillRate[j].id == data[i].Fld1) {
                        tmpDataFillRate[j].value = data[i].Fld2
                    }
                }
            } else if (data[i].Category == "BAR_IMMEDIATE") {
                for (var j in tmpDataImmediate) {
                    if (tmpDataImmediate[j].id == data[i].Fld1) {
                        tmpDataImmediate[j].value = data[i].Fld2
                    }
                }
            } else if (data[i].Category == "BAR_SERVICERATE") {
                for (var j in tmpDataServiceRate) {
                    if (tmpDataServiceRate[j].id == data[i].Fld1) {
                        tmpDataServiceRate[j].value = data[i].Fld2
                    }
                }
            } else if (data[i].Category == "BAR_COMPOSITIONORDER") {
                for (var j in tmpDataComposition) {
                    if (tmpDataComposition[j].id == data[i].Fld1) {
                        tmpDataComposition[j].value1 = data[i].Fld2
                        tmpDataComposition[j].value2 = data[i].Fld3
                        tmpDataComposition[j].value3 = data[i].Fld4
                        tmpDataComposition[j].value4 = data[i].Fld5
                        tmpDataComposition[j].value5 = data[i].Fld6 //type T
                        tmpDataComposition[j].value6 = data[i].Fld7 //Type C
                    }
                }
            // } else if (data[i].Category == "BAR_BACKORDER") {
            //     for (var j in tmpDataStockDay) {
            //         if (tmpDataStockDay[j].id == data[i].Fld1) {
            //             tmpDataStockDay[j].value = data[i].Fld2
            //         }
            //     }
            }else if (data[i].Category == "BAR_STOCKDAY") {
                for (var j in tmpDataStockDay) {
                    if (tmpDataStockDay[j].id == data[i].Fld1) {
                        tmpDataStockDay[j].value = data[i].Fld2
                    }
                }
            }
        }
        $scope.barOptionsFillRate.data = tmpDataFillRate;
        $scope.barOptionsStockDay.data = tmpDataStockDay;
        $scope.barOptionsImFillRate.data = tmpDataImmediate;
        $scope.barOptionsSvcRate.data = tmpDataServiceRate;
        $scope.multilineOptions.dataProvider = tmpDataComposition;
        console.log('tmpDataFillRate ====', tmpDataFillRate);
        console.log('tmpDataStockDay ====', tmpDataStockDay);
        console.log('tmpDataImmediate ====', tmpDataImmediate);
        console.log('tmpDataServiceRate ====', tmpDataServiceRate);
        
    }
    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[2048][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[2048][0].Fld2;
        $scope.data3 = AfterSalesDashboard.parsedData[2048][0].Fld3;
        $scope.data4 = AfterSalesDashboard.parsedData[2048][0].Fld4;
        $scope.data5 = AfterSalesDashboard.parsedData[2048][0].Fld5;
        // $scope.barOptionsSvcRate.data = AfterSalesDashboard.parsedData[2048];
        // $scope.barOptionsFillRate.data = AfterSalesDashboard.parsedData[2048];
        // $scope.barOptionsStockDay.data = AfterSalesDashboard.parsedData[2048];
        // $scope.barOptionsImFillRate.data = AfterSalesDashboard.parsedData[2048];
        // $scope.multilineOptions.dataProvider = AfterSalesDashboard.parsedData[2048];
        pisahData(AfterSalesDashboard.parsedData[2048]);
    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);

});
