angular.module('app')
.controller('SABP', function($scope,$interval,AfterSalesDashboard,CurrentUser) {
    $scope.options1 = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#38c';
        //   else
            return '#d46d29';
        },
        scaleColor:false,
        lineWidth:10,
        lineCap:'circle',
        size:150,
    };
    $scope.options2 = angular.copy($scope.options1);
    $scope.options3 = angular.copy($scope.options1);
    $scope.options4 = angular.copy($scope.options1);
    $scope.options5 = angular.copy($scope.options1);
    $scope.options2.barColor = function(percent){
        // if(percent>=70)
        //     return '#3c8';
        // else
            return '#568ab7';
    };
    $scope.options3.barColor = function(percent){
        // if(percent>=70)
        //     return '#c5c';
        // else
            return '#5e9739';
    };
    $scope.options4.barColor = function(percent){
        // if(percent>=70)
        //     return '#fc0';
        // else
        //     return '#f33';
        return '#568ab7';
    };
    $scope.options5.barColor = function(percent){
        if(percent>=70)
            return '#5cf';
        else
            return '#f33';
    };

    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[32768][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[32768][0].Fld2;
        $scope.data3 = AfterSalesDashboard.parsedData[32768][0].Fld3;
        $scope.data4 = AfterSalesDashboard.parsedData[32768][0].Fld4;
    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);


});
