angular.module('app')
.controller('DashboardAppointmentBP', function($scope,$interval,AfterSalesDashboard) {
    $scope.options1 = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#38c';
        //   else
            return '#568ab7';
        },
        scaleColor:false,
        lineWidth:12,
        lineCap:'circle',
        size:150,
    };
    $scope.options2 = angular.copy($scope.options1);
    $scope.options2.barColor = function(percent){
        // if(percent>=70)
        //     return '#3c8';
        // else
        return '#5e9739';
    };
    function refreshData(){
        console.log('masuk refreshData');
        $scope.data1 = AfterSalesDashboard.parsedData[131072][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[131072][0].Fld2;
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});