angular.module('app')
.controller('UnitEntryBP', function($scope,$timeout,$interval,AfterSalesDashboard) {
	refreshData = function(){
	    $scope.data1 = AfterSalesDashboard.parsedData[1024][0].Fld1;
	    $scope.data2 = AfterSalesDashboard.parsedData[1024][0].Fld2;
	    $scope.data3 = AfterSalesDashboard.parsedData[1024][0].Fld3;
	    $scope.data4 = AfterSalesDashboard.parsedData[1024][0].Fld4;
	    $scope.data5 = AfterSalesDashboard.parsedData[1024][0].Fld5;
	    $scope.data6 = AfterSalesDashboard.parsedData[1024][0].Fld10;
	    $scope.data7 = AfterSalesDashboard.parsedData[1024][0].Fld11;
	    $scope.data8 = AfterSalesDashboard.parsedData[1024][0].Fld6;
	    $scope.data9 = AfterSalesDashboard.parsedData[1024][0].Fld7;
		$scope.data10 = AfterSalesDashboard.parsedData[1024][0].Fld8;
		$scope.data11 = AfterSalesDashboard.parsedData[1024][0].Fld9;
	}
	refreshData();
    $interval(function(){
    	refreshData();
	},5000);
});
