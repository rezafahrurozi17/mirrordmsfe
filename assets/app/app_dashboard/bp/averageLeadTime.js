angular.module('app')
.controller('AverageLeadTimeBP', function($scope,$interval,AfterSalesDashboard,CurrentUser) {
	// function secondsToHHMMSS(secs){
	//     var hours   = Math.floor(secs / 3600) % 24;
	//     hours = hours<10?"0"+hours:hours;
	//     var minutes = Math.floor(secs / 60) % 60;
	//     minutes = minutes<10?"0"+minutes:minutes;
	//     var seconds = secs % 60;
	//     seconds = seconds<10?"0"+seconds:seconds;
	//     return [hours,minutes,seconds].join(":");
	//    }
	function secondsToHms(d) {
		d = Number(d);
		var h = Math.floor(d / 3600);
		var m = Math.floor(d % 3600 / 60);
		var s = Math.floor(d % 3600 % 60);
		var hDisplay = h < 10 ? '0' + h : h;
		var mDisplay = m < 10 ? '0' + m : m;
		var sDisplay = s < 10 ? '0' + s : s;
		return hDisplay +":" + mDisplay+":"+ sDisplay; 
	}

		$scope.roleId = CurrentUser.user();

    refreshData = function(){
	    $scope.data1 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld1);
	    $scope.data2 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld2);
	    $scope.data3 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld3);
	    $scope.data4 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld4);
		$scope.data5 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld5);
		$scope.data6 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld6);
		$scope.data7 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld7);
		$scope.data8 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld8);
		$scope.data9 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld9);
		$scope.data10 = secondsToHms(AfterSalesDashboard.parsedData[4096][0].Fld10);
    }
    refreshData();
    $interval(function(){
    	refreshData();
    },5000);

});
