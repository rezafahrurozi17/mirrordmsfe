angular.module('app')
.controller('DashboardGroupForemanBP', function($scope,$interval,AfterSalesDashboard) {

    $scope.options = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#38c';
        //   else
            return '#568ab7';
        },
        scaleColor:false,
        lineWidth:10,
        lineCap:'circle',
        size:140,
    };
    $scope.options2 = angular.copy($scope.options);
    $scope.options2.barColor = function(percent){
        return '#5e9739';
      };

    $scope.gaugeOptions1 = {
        type: "gauge",
        theme: "light",
        autoResize:false,
        marginTop: 40,
        marginLeft: 40,
        marginRight: 40,
        marginBottom:20,
        axes: [ {
            axisThickness: 1,
            axisAlpha: 0.2,
            tickAlpha: 0.2,
            valueInterval: 20,
            bands: [ {
                color: "#cc4748",
                endValue: 40,
                startValue: 0,
                radius: "130%",
                innerRadius: "100%",
            }, 
            // {
            //     color: "#fdd400",
            //     endValue: 80,
            //     startValue: 40
            // }, 
            {
                color: "#84b761",
                endValue: 120,
                radius: "130%",
                innerRadius: "100%",
                startValue: 40
            } ],
            bottomText: "0 %",
            bottomTextYOffset: -20,
            endValue: 120
        } ],
        arrows: [ {} ],
        export: {
            enabled: true
        }
    }
    $scope.gaugeOptions2 = angular.copy($scope.gaugeOptions1);

    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[268435456][0].Fld1; // Stall Capacity
        $scope.data2 = AfterSalesDashboard.parsedData[268435456][0].Fld2; // Q1
        $scope.data3 = AfterSalesDashboard.parsedData[268435456][0].Fld3; // RUM
        $scope.data4 = AfterSalesDashboard.parsedData[268435456][0].Fld4; // RPS
        $scope.data5 = AfterSalesDashboard.parsedData[268435456][0].Fld5; // TECH EFICIENCY
        $scope.data6 = AfterSalesDashboard.parsedData[268435456][0].Fld6; // OVERALL PRODUCTIVITY 
        $scope.data7 = AfterSalesDashboard.parsedData[268435456][0].Fld7; // LU
        $scope.data8 = AfterSalesDashboard.parsedData[268435456][0].Fld8; // RPS REGULAR
    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);

    $scope.gaugeOptions1.arrows[0].value=$scope.data5;
    $scope.gaugeOptions1.axes[0].bottomText=$scope.data5+" %";

    $scope.gaugeOptions2.arrows[0].value=$scope.data6;
    $scope.gaugeOptions2.axes[0].bottomText=$scope.data6+" %";

});
