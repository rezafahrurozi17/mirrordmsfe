angular.module('app')
.controller('PSFUBP', function($scope,$interval,AfterSalesDashboard) {

    $scope.options1 = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#38c';
        //   else
        //     return '#f33';
            return '#d36d27';
        },
        scaleColor:false,
        lineWidth:10,
        lineCap:'circle',
        size:140,
    };
    $scope.options2 = angular.copy($scope.options1);
    $scope.options3 = angular.copy($scope.options1);
    $scope.options4 = angular.copy($scope.options1);
    $scope.options5 = angular.copy($scope.options1);
    $scope.options2.barColor = function(percent){
        // if(percent>=70)
        //     return '#3c8';
        // else
        //     return '#f33';
        return '#508abb';
    };
    $scope.options3.barColor = function(percent){
        // if(percent>=70)
        //     return '#c5c';
        // else
        //     return '#f33';
        return '#508abb';
    };
    $scope.options4.barColor = function(percent){
        // if(percent>=70)
        //     return '#fc0';
        // else
        //     return '#f33';
        return '#62993e';
    };
    $scope.options5.barColor = function(percent){
        // if(percent>=70)
        //     return '#5cf';
        // else
        //     return '#f33';
        return '#d36e2a';
    };

    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[16384][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[16384][0].Fld2;
        $scope.data3 = AfterSalesDashboard.parsedData[16384][0].Fld3;
        $scope.data4 = AfterSalesDashboard.parsedData[16384][0].Fld4;
        $scope.data5 = AfterSalesDashboard.parsedData[16384][0].Fld5;        
    }
    refreshData();

    $interval(function(){
        refreshData();
    },5000);


});