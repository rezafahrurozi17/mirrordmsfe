angular.module('app')
.controller('ProgressToTargetSaBP', function($scope,$interval,AfterSalesDashboard) {
    $scope.targetunit=AfterSalesDashboard.parsedData[67108864][0].Fld10;
    console.log("$scope.targetunit",$scope.targetunit);
    $scope.warna=$scope.targetunit/3;
    $scope.targetrevenue=AfterSalesDashboard.parsedData[67108864][0].Fld11;
    $scope.revenue=AfterSalesDashboard.parsedData[67108864][0].Fld7;
    $scope.unit=AfterSalesDashboard.parsedData[67108864][0].Fld5;
    console.log("$scope.revenue",$scope.revenue);
    console.log("$scope.revenue",$scope.revenue*1.2);
    var num = $scope.targetrevenue.toLocaleString();
    var targetrevenue = num.toString();
    var num1 = $scope.revenue.toLocaleString()

    var num2 = $scope.unit;
    var unit = num2.toString();
    var num3 =$scope.targetunit;
    var targetunit = num3.toString();
    $scope.gaugeOptions1 = {
        type: "gauge",
        theme: "light",
        marginTop: 55,
        marginLeft: 50,
        marginRight: 50,
        color:'#a20000',
        marginBottom: 0,
        // marginBottom: 50,  
        autoResize:false,
        axes: [ {
            axisThickness: 0.2,
            axisAlpha: 0.2,
            bandAlpha:1,
            tickAlpha: 0.2,
            inside:false,
            gridInside: false,
            labelOffset: 40,
            bandOutlineThickness:50,
            // startAngle: -90,
            // endAngle: 90,
            topTextYOffset: 75,
            valueInterval: Math.round($scope.targetunit/2),
            bands: [ {
                color: "#feaa85",
                endValue: Math.round($scope.targetunit/2),
                startValue: 0,
                innerRadius: "105%",
                radius: "155%",
                balloonText:unit
            },{
                color: "#f6d341",
                endValue: Math.round($scope.targetunit-3),
                startValue: Math.round($scope.targetunit/2),
                innerRadius: "105%",
                radius: "155%"
            }, {
                color: "#000",
                endValue:  Math.round($scope.targetunit),
                startValue: Math.round($scope.targetunit-3),
                innerRadius: "105%",
                radius: "155%",
                balloonText: targetunit
            }, {
                color: "#bdff00",
                endValue: Math.round($scope.targetunit*1.5),
                innerRadius: "105%",
                radius: "155%",
                startValue: Math.round($scope.targetunit)
            } ],
            endValue: Math.round($scope.targetunit*1.5)
        } ],
        arrows: [ {} ],
    }

    $scope.gaugeOptions2 = {
        type: "gauge",
        theme: "light",
        marginTop: 55,
        marginLeft: 60,
        marginRight: 60,
        marginBottom: 0,
        // marginBottom: 50,    
        // autoTransform:true,
        fontSize:8,
        color:'#a20000',
        autoResize:false,
        axes: [ {
            axisThickness: 0.2,
            axisAlpha: 0.2,
            bandAlpha:1,
            tickAlpha: 0.2,
            inside: false,
            bandOutlineThickness:50,
            // showFirstLabel: false,
            // showLastLabel: true,
            labelOffset: 37,
            // startAngle: -90,
            // endAngle: 90,
            topTextYOffset: 75,
            valueInterval: Math.round($scope.targetrevenue/2),
            bands: [ {
                color: "#cbff00",
                endValue: Math.round($scope.targetrevenue/2),
                startValue: 0,
                innerRadius: "105%",
                radius: "155%",
                balloonText:num1
            },{
                color: "#cbff00",
                endValue: Math.round($scope.targetrevenue-15000000),
                startValue: Math.round($scope.targetrevenue/2),
                innerRadius: "105%",
                radius: "155%"
            }, 
            {
                color: "#000",
                endValue:  $scope.targetrevenue,
                startValue: Math.round($scope.targetrevenue-15000000),
                innerRadius: "105%",
                radius: "155%",
                balloonText: targetrevenue
            }, 
            {
                color: "#cbff00",
                endValue: Math.round($scope.targetrevenue*1.5),
                innerRadius: "105%",
                radius: "155%",
                startValue: Math.round($scope.targetrevenue)
            } ],
            endValue: Math.round($scope.targetrevenue*1.5)
        } ],
        arrows: [{
            // alpha: 1,
            // innerRadius: "50%",
            // nailRadius: 0,
            // radius: "120%"
          }]
    }
    // $scope.gaugeOptions2 = {
    //     type: "gauge",
    //     theme: "light",
    //     marginTop: 0,
    //     marginLeft: -10,
    //     marginRight: -20,
    //     marginBottom: -200,
    //     axes: [{
    //         topTextFontSize: 15,
    //         topTextYOffset: 40,
    //         axisColor: "#31d6ea",
    //         axisThickness: 1,
    //         endValue: 50000000,
    //         gridInside: true,
    //         inside: true,
    //         radius: "50%",
    //         valueInterval: 50000000,
    //         tickColor: "#67b7dc",
    //         bandOutlineAlpha: 0,
    //         bands: [{
    //             color: "#ddd",
    //             endValue: 50000000,
    //             innerRadius: "105%",
    //             radius: "170%",
    //             gradientRatio: [0.5, 0, -0.2],
    //             startValue: 0
    //         }, {
    //             color: "#3d9",
    //             endValue: 0,
    //             innerRadius: "105%",
    //             radius: "170%",
    //             gradientRatio: [0.5, 0, -0.3],
    //             startValue: 0
    //         }]
    //     }],
    //     arrows: [{
    //         value: 60,
    //         alpha: 1,
    //         innerRadius: "35%",
    //         nailRadius: 0,
    //         radius: "170%"
    //     }]
    // }
    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[67108864][0].Fld1;  //Data Unit Out Yesterday
        $scope.data2 = AfterSalesDashboard.parsedData[67108864][0].Fld2;  //Data Unit in Yesterday
        $scope.data3 = AfterSalesDashboard.parsedData[67108864][0].Fld3;  //Data out Today
        $scope.data4 = AfterSalesDashboard.parsedData[67108864][0].Fld4;  //Data in Today
        $scope.data5 = AfterSalesDashboard.parsedData[67108864][0].Fld5;  //Data Progress Unit Entry
        $scope.data6 = AfterSalesDashboard.parsedData[67108864][0].Fld6;  //daysLeft
        $scope.data7 = AfterSalesDashboard.parsedData[67108864][0].Fld7;  //Data Progress Revenue
        $scope.data8 = AfterSalesDashboard.parsedData[67108864][0].Fld8;  //Revenue Today
        $scope.data9 = AfterSalesDashboard.parsedData[67108864][0].Fld9;  //Data Target Progress Revenue
        $scope.data10 = AfterSalesDashboard.parsedData[67108864][0].Fld10;  //Data Target Progress unit entry


        //interval gauge options 2
        var interval1 = Math.round($scope.data9 / 5);

        //  end Value untuk band pertama dan startvalue untuk band kedua
        var value1 = Math.round($scope.data10 * 0.33);

        //  end Value untuk band ketiga dan startvalue untuk band ketiga
        var value2 = Math.round($scope.data10 * 0.66);

        /*Daysleft

        */
          //Current Date
          var currentDate = new Date();

          var oneDay=1000*60*60*24;

          //Last Date
          var lastDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);

          var currentDateTime = currentDate.getTime();
          var lastDateTime = lastDate.getTime();
          var diff = lastDateTime - currentDateTime;

          //Daysleft
          var daysLeft = Math.round(diff / oneDay);
          console.log(daysLeft);
          $scope.daysLeft = daysLeft;

        //Data Progress Revenue
        //Data Progress Revenue
        $scope.gaugeOptions2.arrows[0].value=$scope.data7;
        $scope.gaugeOptions2.axes[0].topText=$scope.data7.toLocaleString();
        // $scope.gaugeOptions2.axes[0].bands[1].endValue=$scope.data7;
        // $scope.gaugeOptions2.axes[0].topText = $scope.data7;
        // $scope.gaugeOptions2.arrows[0].value=$scope.data7;

        //Data target Progress
        // $scope.gaugeOptions2.axes[0].endValue = $scope.data9;
        // $scope.gaugeOptions2.axes[0].bands[0].endValue=$scope.data9;

        // $scope.gaugeOptions2.axes[0].valueInterval = interval1;

        //Data Progress Unit Entry
        $scope.gaugeOptions1.arrows[0].value=$scope.data5;
        $scope.gaugeOptions1.axes[0].topText=$scope.data5; 

        //interval band pertama
        // $scope.gaugeOptions1.axes[0].bands[0].endValue=value1;

        //interval band kedua
        // $scope.gaugeOptions1.axes[0].bands[1].startValue=value1;
        // $scope.gaugeOptions1.axes[0].bands[1].endValue=value2;

        //interval band ketiga
        // $scope.gaugeOptions1.axes[0].bands[2].startValue=value2;
        // $scope.gaugeOptions1.axes[0].bands[2].endValue=$scope.data10;

        //Data Target Progress unit entry
        // $scope.gaugeOptions1.axes[0].endValue=$scope.data10;


        // $scope.gaugeOptions1.axes[0].bottomText=$scope.data5;
    }
    function resize() {
        $('tempatChart').width();
        $scope.gaugeOptions2.invalidateSize();
      }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);

});
