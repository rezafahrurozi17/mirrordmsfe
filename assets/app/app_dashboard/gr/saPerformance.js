angular.module('app')
.controller('SAGR', function($scope,$interval,AfterSalesDashboard) {

    $scope.options1 = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#d2691e';
        //   else
        //     return '#f33';
        return '#d2691e';

        },
        scaleColor:false,
        lineWidth:10,
        lineCap:'circle',
        size:150,
    };
    $scope.options2 = angular.copy($scope.options1);
    $scope.options3 = angular.copy($scope.options1);
    $scope.options4 = angular.copy($scope.options1);
    $scope.options5 = angular.copy($scope.options1);
    $scope.options2.barColor = function(percent){
        // if(percent>=70)
        //     return '#1874cd';
        // else
        //     return '#f33';
        return '#1874cd';

    };
    $scope.options3.barColor = function(percent){
        // if(percent>=70)
        //     return '#458b00';
        // else
        //     return '#f33';
        return '#458b00';
    };
    $scope.options4.barColor = function(percent){
        // if(percent>=70)
        //     return '#1874cd';
        // else
        //     return '#f33';
        return '#1874cd';
    };
    $scope.options5.barColor = function(percent){
        // if(percent>=70)
        //     return '#458b00';
        // else
        //     return '#f33';
        return '#458b00';
    };

    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[512][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[512][0].Fld2;
        $scope.data3 = AfterSalesDashboard.parsedData[512][0].Fld3;
        $scope.data4 = AfterSalesDashboard.parsedData[512][0].Fld4;
        $scope.data5 = AfterSalesDashboard.parsedData[512][0].Fld5;
    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);


});
