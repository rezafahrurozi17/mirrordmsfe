angular.module('app')
.controller('DashboardArOverdue', function($scope,$interval,AfterSalesDashboard,BoardSalesGlobalParameterFactory) {

	

    function refreshData(){
        console.log('masuk refreshData');
		BoardSalesGlobalParameterFactory.getDataBoardSalesArOverdue().then(function (res) {
					$scope.BoardSalesGlobalParamArOverdue = res.data.Result[0]
					
					//awal pie chart 3d
					$scope.IsiDataPieChart3d=[ {
									"CategoryArOverdue": ""+res.data.Result[0].AgingPeriod1+"-"+res.data.Result[0].AgingPeriod2,
									"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld3
								  },{
									"CategoryArOverdue": ""+res.data.Result[0].AgingPeriod2+"-"+res.data.Result[0].AgingPeriod3,
									"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld2
								  }, {
									"CategoryArOverdue": ">"+res.data.Result[0].AgingPeriod3,
									"ArOverdueValue": AfterSalesDashboard.parsedData[16777216][0].Fld1
								  } ];
					$scope.TotalIsichartpie3d=0;
					for(var x = 0; x < $scope.IsiDataPieChart3d.length; ++x)
					{
						$scope.TotalIsichartpie3d+=$scope.IsiDataPieChart3d[x].ArOverdueValue;
					}
					
					var chartpie3d = AmCharts.makeChart( "chartdivpie3d", {
								  "type": "pie",
								  "theme": "light",
								  "labelRadius": -65,
								  "legend":{
									"position":"bottom",
									"valueText" : "",
									"autoMargins":false
								  },
								  "dataProvider": $scope.IsiDataPieChart3d,
								  "valueField": "ArOverdueValue",
								  "titleField": "CategoryArOverdue",
								  "labelText": "[[value]], ([[percents]]%)",
								  "outlineAlpha": 0.4,
								  "depth3D": 15,
								  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
								  "angle": 30,
								  "export": {
									"enabled": false
								  }
								} );
					//akhir pie chart 3d
					
					return res.data;
				});
		
		
		
		
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});