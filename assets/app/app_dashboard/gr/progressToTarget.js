angular.module('app')
.controller('ProgressToTargetGR', function($scope,$interval,AfterSalesDashboard) {

     // Fld1   unit Entry                        
    // Fld2   unit NonCPUS                 
    // Fld3 RevenueAmount
    // Fld4 Amount Non CPUS
    // Fld5 Unit CPUS Yesterday         
    // Fld6 Unit CPUS Today               
    // Fld7  Revenue CPUS Yesterday
    // Fld8 Revenue CPUS Unit
    // Fld9 LEFT_TILL_OF_MONTH      
    // Fld10 Target Unit Entry             
    // Fld11 Target Revenue

    $scope.targetunit=AfterSalesDashboard.parsedData[16][0].Fld10;
    console.log("$scope.targetunit",$scope.targetunit);
    $scope.warna=$scope.targetunit/3;
    $scope.targetrevenue=AfterSalesDashboard.parsedData[16][0].Fld11;
    $scope.revenue=AfterSalesDashboard.parsedData[16][0].Fld7;
    $scope.unit=AfterSalesDashboard.parsedData[16][0].Fld5;
    console.log("$scope.revenue",$scope.revenue);
    console.log("$scope.revenue",$scope.revenue*1.2);

    var num = $scope.targetrevenue.toLocaleString();
    var targetrevenue = num.toString();
    var num1 = $scope.revenue.toLocaleString()

    var num2 = $scope.unit;
    var unit = num2.toString();
    var num3 =$scope.targetunit;
    var targetunit = num3.toString();

    $scope.gaugeOptions1 = {
        type: "gauge",
        theme: "light",
        marginTop: 50,
        // marginLeft: 50,
        // marginRight: 50,
        marginBottom: 20,    
        autoResize:false,
        axes: [ {
            axisThickness: 0.2,
            axisAlpha: 0.2,
            bandAlpha:1,
            tickAlpha: 0.2,
            inside:false,
            gridInside: false,
            labelOffset: 45,
            bandOutlineThickness:50,
            valueInterval: Math.round($scope.targetunit/2),
            bands: [ {
                color: "#c55",
                endValue: Math.round($scope.targetunit/2),
                startValue: 0,
                innerRadius: "105%",
                radius: "155%",
                balloonText:unit
            },{
                color: "#cc0",
                endValue: Math.round($scope.targetunit-10),
                startValue: Math.round($scope.targetunit/2),
                innerRadius: "105%",
                radius: "155%"
            }, {
                color: "#000",
                endValue:  Math.round($scope.targetunit),
                startValue: Math.round($scope.targetunit-10),
                innerRadius: "105%",
                radius: "155%",
                balloonText: targetunit
            }, {
                color: "#0d5",
                endValue: Math.round($scope.targetunit*1.5),
                innerRadius: "105%",
                radius: "155%",
                startValue: Math.round($scope.targetunit)
            } ],
            endValue: Math.round($scope.targetunit*1.5)
        } ],
        arrows: [ {} ],
    }

    $scope.gaugeOptions2 = {
        type: "gauge",
        theme: "light",
        marginTop: 50,
        marginLeft: 40,
        marginRight: 40,
        marginBottom: 20,   
        autoResize:false,
        axes: [ {
            axisThickness: 0.2,
            axisAlpha: 0.2,
            bandAlpha:1,
            tickAlpha: 0.2,
            inside: false,
            bandOutlineThickness:50,
            // showFirstLabel: false,
            // showLastLabel: false,
            // labelsEnabled:false,
            labelOffset: 45,
            valueInterval: Math.round($scope.targetrevenue/2),
            bands: [ {
                color: "#3d9",
                endValue: Math.round($scope.targetrevenue/2),
                startValue: 0,
                innerRadius: "105%",
                radius: "155%",
                balloonText:num1
            },{
                color: "#3d9",
                endValue: Math.round($scope.targetrevenue-30000000),
                startValue: Math.round($scope.targetrevenue/2),
                innerRadius: "105%",
                radius: "155%"
            }, 
            {
                color: "#000",
                endValue:  $scope.targetrevenue,
                startValue: Math.round($scope.targetrevenue-30000000),
                innerRadius: "105%",
                radius: "155%",
                balloonText: targetrevenue
            }, 
            {
                color: "#3d9",
                endValue: Math.round($scope.targetrevenue*1.5),
                innerRadius: "105%",
                radius: "155%",
                startValue: Math.round($scope.targetrevenue)
            } ],
            endValue: Math.round($scope.targetrevenue*1.5)
        } ],
        arrows: [ {} ],
    }
    // $scope.gaugeOptions1 = {
    //     type: "gauge",
    //     theme: "light",
    //     axes: [ {
    //         axisThickness: 1,
    //         axisAlpha: 0.2,
    //         tickAlpha: 0.2,
    //         valueInterval: 500,
    //         labelOffset: -40,
    //         bands: [ {
    //             color: "#c55",
    //             endValue: 500,
    //             startValue: 0
    //         }, {
    //             color: "#cc0",
    //             endValue: 1000,
    //             startValue: 500
    //         }, {
    //             color: "#0d5",
    //             endValue: 1500,
    //             innerRadius: "95%",
    //             startValue: 1000
    //         } ],
    //         endValue: 1500
    //     } ],
    //     arrows: [ {} ],
    // }

    // $scope.gaugeOptions2 = {
    //     type: "gauge",
    //     theme: "light",
    //     axes: [{
    //         topTextFontSize: 15,
    //         topTextYOffset: 40,
    //         axisColor: "#31d6ea",
    //         axisThickness: 1,
    //         endValue: 100,
    //         gridInside: true,
    //         inside: true,
    //         radius: "50%",
    //         valueInterval: 20,
    //         tickColor: "#67b7dc",
    //         startAngle: -90,
    //         endAngle: 90,
    //         unit: "%",
    //         bandOutlineAlpha: 0,
    //         labelOffset: -95,
    //         bands: [{
    //             color: "#ddd",
    //             endValue: 100,
    //             innerRadius: "105%",
    //             radius: "170%",
    //             gradientRatio: [0.5, 0, -0.2],
    //             startValue: 0
    //         }, {
    //             color: "#3d9",
    //             endValue: 0,
    //             innerRadius: "105%",
    //             radius: "170%",
    //             gradientRatio: [0.5, 0, -0.3],
    //             startValue: 0
    //         }]
    //     }],
    //     arrows: [{
    //         value: 60,
    //         alpha: 1,
    //         innerRadius: "35%",
    //         nailRadius: 0,
    //         radius: "170%"
    //     }]
    // }
    $scope.checkvalueforRevenuvsTarget = function(){
        var currentValue = AfterSalesDashboard.parsedData[16][0].Fld3;
        var targetValue = AfterSalesDashboard.parsedData[16][0].Fld11
        var endNilai =  Math.round($scope.targetrevenue*1.5);
        var interval = Math.round($scope.targetrevenue/2);
        var calonInterval =  Math.round(currentValue/4);
        // endvalue
        if(endNilai < currentValue){
            $scope.gaugeOptions2.axes[0].endValue = currentValue * 1.3;
            // return currentValue * 1.3;
        }
        // interval &  start(endValue)
        if(calonInterval > targetValue){
                $scope.gaugeOptions2.axes[0].valueInterval = currentValue / 4;
                $scope.gaugeOptions2.axes[0].bands[0].endValue = targetValue;
                $scope.gaugeOptions2.axes[0].bands[0].balloonText = targetValue;
                $scope.gaugeOptions2.axes[0].bands[0].color = '#000';

                $scope.gaugeOptions2.axes[0].bands[1].startValue = targetValue;
                $scope.gaugeOptions2.axes[0].bands[1].endValue = currentValue / 4;
                $scope.gaugeOptions2.axes[0].bands[1].color = '#3d9';

                $scope.gaugeOptions2.axes[0].bands[2].startValue = currentValue / 4;
                $scope.gaugeOptions2.axes[0].bands[2].endValue = currentValue / 2 ;
                $scope.gaugeOptions2.axes[0].bands[2].color = '#3d9';

                $scope.gaugeOptions2.axes[0].bands[3].endValue = currentValue / 2;
                $scope.gaugeOptions2.axes[0].bands[3].endValue = currentValue * 1.3;
                $scope.gaugeOptions2.axes[0].bands[3].color = '#3d9';
        }
        // if(currentValue > targetValue ){
        //     $scope.gaugeOptions2.axes[0].valueInterval = currentValue / 4;
        //     $scope.gaugeOptions2.axes[0].bands[0].endValue = targetValue;
        //     $scope.gaugeOptions2.axes[0].bands[0].balloonText = targetValue;
        //     $scope.gaugeOptions2.axes[0].bands[0].color = '#000';

        //     $scope.gaugeOptions2.axes[0].bands[1].startValue = targetValue;
        //     $scope.gaugeOptions2.axes[0].bands[1].endValue = currentValue / 4;
        //     $scope.gaugeOptions2.axes[0].bands[1].color = '#3d9';

        //     $scope.gaugeOptions2.axes[0].bands[2].startValue = currentValue / 4;
        //     $scope.gaugeOptions2.axes[0].bands[2].endValue = currentValue / 2 ;
        //     $scope.gaugeOptions2.axes[0].bands[2].color = '#3d9';

        //     $scope.gaugeOptions2.axes[0].bands[3].endValue = currentValue / 2;
        //     $scope.gaugeOptions2.axes[0].bands[3].endValue = currentValue * 1.3;
        //     $scope.gaugeOptions2.axes[0].bands[3].color = '#3d9';


        // }
        // if(param == 'end'){
            
        // }else if(param == 'target1'){

        // }else if(param == 'interval' || param == 'start'){
        //     if(currentValue > targetValue){
        //         return currentValue / 4;
        //     }else{
        //         return Math.round(val1/2);
        //     }
        // }
    }
    
    refreshData = function(){
        // Fld1   unit Entry                        
        // Fld2   unit NonCPUS                 
        // Fld3 RevenueAmount
        // Fld4 Amount Non CPUS
        // Fld5 Unit CPUS Yesterday         
        // Fld6 Unit CPUS Today               
        // Fld7  Revenue CPUS Yesterday
        // Fld8 Revenue CPUS Unit
        // Fld9 LEFT_TILL_OF_MONTH      
        // Fld10 Target Unit Entry             
        // Fld11 Target Revenue
        $scope.data1 = AfterSalesDashboard.parsedData[16][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[16][0].Fld2;
        $scope.data3 = AfterSalesDashboard.parsedData[16][0].Fld3;
        $scope.data4 = AfterSalesDashboard.parsedData[16][0].Fld4;
        $scope.data5 = AfterSalesDashboard.parsedData[16][0].Fld5;
        $scope.data6 = AfterSalesDashboard.parsedData[16][0].Fld6;
        $scope.data7 = AfterSalesDashboard.parsedData[16][0].Fld7;
        $scope.data8 = AfterSalesDashboard.parsedData[16][0].Fld8;
        $scope.data9 = AfterSalesDashboard.parsedData[16][0].Fld9;
        $scope.data10 = AfterSalesDashboard.parsedData[16][0].Fld10;

        var value = Math.round(Math.random() * 100);
        // $scope.gaugeOptions2.arrows[0].value=value;
        // $scope.gaugeOptions2.axes[0].bands[1].endValue=value;
        // $scope.gaugeOptions2.axes[0].topText = value;

         //Data Progress Revenue
         $scope.gaugeOptions2.arrows[0].value=$scope.data3;
         $scope.gaugeOptions2.axes[0].bottomText=$scope.data3.toLocaleString();

         $scope.gaugeOptions1.arrows[0].value=$scope.data1;
         $scope.gaugeOptions1.axes[0].bottomText=$scope.data1;  

         $scope.checkvalueforRevenuvsTarget();
        
        // $scope.gaugeOptions1.arrows[0].value=value;
        // $scope.gaugeOptions1.axes[0].bottomText=value;        
    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);

});