angular.module('app')
.controller('DashboardSalesTargetModel', function($scope,$interval,AfterSalesDashboard) {

	

    function refreshData(){
        console.log('masuk refreshData');
		//target monthly fld 1
		//monthly data fld 2
		//daily data fld 2
		//persen monthly fld 3
		//target dailt fld 4
		//persen daily fld 5
		//paling atas 9 paling bawah 5
		
		//awal stacked bar prototype
		var chartstackedbarSalesTarget = AmCharts.makeChart("chartstackedbarSalesTargetModel", {
					  "type": "serial",
					  "theme": "light",
					  "rotate": true,
					  "legend":{
						"position":"bottom",
						"valueText" : "",
						"autoMargins":true
					  },
					  "marginBottom": 50,
					  "dataProvider": [{
						"Tampang": AfterSalesDashboard.parsedData[1048576][9].Category,
						"DailyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][9].Fld2),
						"PercentDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][9].Fld5),
						"MonthlyData": Math.abs(AfterSalesDashboard.parsedData[1048576][9].Fld2),
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][9].Fld3,
						"TargetDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][9].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][9].Fld1
					  },{
						"Tampang": AfterSalesDashboard.parsedData[1048576][8].Category,
						"DailyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][8].Fld2),
						"PercentDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][8].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][8].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][8].Fld3,
						"TargetDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][8].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][8].Fld1
					  },{
						"Tampang": AfterSalesDashboard.parsedData[1048576][7].Category,
						"DailyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][7].Fld2),
						"PercentDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][7].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][7].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][7].Fld3,
						"TargetDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][7].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][7].Fld1
					  },{
						"Tampang": AfterSalesDashboard.parsedData[1048576][6].Category,
						"DailyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][6].Fld2),
						"PercentDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][6].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][6].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][6].Fld3,
						"TargetDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][6].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][6].Fld1
					  },{
						"Tampang": AfterSalesDashboard.parsedData[1048576][5].Category,
						"DailyData": -Math.abs(AfterSalesDashboard.parsedData[1048576][5].Fld2),
						"PercentDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][5].Fld5),
						"MonthlyData": AfterSalesDashboard.parsedData[1048576][5].Fld2,
						"PercentMonthly": AfterSalesDashboard.parsedData[1048576][5].Fld3,
						"TargetDaily": -Math.abs(AfterSalesDashboard.parsedData[1048576][5].Fld4),
						"TargetMonthly": AfterSalesDashboard.parsedData[1048576][5].Fld1
					  }],
					  "startDuration": 1,
					  "graphs": [{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "TargetDaily",
						"title": "Daily Target",
						"labelText": " ",
						"labelFunction": function(data) {return Math.abs(data.values.value) + ' ('+Math.abs(data.dataContext.PercentDaily)+'%)';},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + ' ('+Math.abs(data.dataContext.PercentDaily)+'%)';},
						"clustered": false


					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "DailyData",
						"title": "Achievement",
						"labelText": " ",
						"labelFunction": function(data) {return Math.abs(data.values.value) + '';},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + '';},
						"clustered": false,


					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "TargetMonthly",
						"title": "Monthly Target",
						"labelText": " ",
						"labelFunction": function(data) {return Math.abs(data.values.value) + ' ('+data.dataContext.PercentMonthly+'%)';},
						"balloonText": " ",
						"balloonFunction": function(data) {return Math.abs(data.values.value) + ' ('+data.dataContext.PercentMonthly+'%)';},
						"clustered": false

					  },{
						"fillAlphas": 0.8,
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "MonthlyData",
						"title": "Achievement",
						"labelText": "[[value]]",
						"clustered": false

					  }],
					  "categoryField": "Tampang",
					  "categoryAxis": {
						"gridPosition": "start",
						"gridAlpha": 0.2,
						"axisAlpha": 0
					  },
					  "valueAxes": [{
						"gridAlpha": 0,
						"ignoreAxisWidth": true,
						"labelFunction": function(value) {
						  return Math.abs(value) + '';
						},
						"guides": [{
						  "value": 0,
						  "lineAlpha": 0.2
						}]
					  }],
					  "balloon": {
						"fixedPosition": true
					  },
					  "chartCursor": {
						"valueBalloonsEnabled": false,
						"cursorAlpha": 0.05,
						"fullWidth": true
					  },
					  "allLabels": [{
						"text": "Daily",
						"x": "28%",
						"y": "97%",
						"bold": true,
						"align": "middle"
					  }, {
						"text": "Monthly",
						"x": "75%",
						"y": "97%",
						"bold": true,
						"align": "middle"
					  }],
					 "export": {
						"enabled": false
					  }

					});
		//akhir stacked bar prototype
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});