angular.module('app')
.controller('StockDashboard', function($scope,$interval,AfterSalesDashboard) {

	

    function refreshData(){
        console.log('masuk refreshData');
		
		//awal batang
		
		//"totalText": "[[total]]"
		//More_Than_30 tu fld1
		//Ten_To_30_Days tu fld2
		//Zero_To_10_Days tu fld3
		//Matching tu fld4
		$scope.Data_Dummy_Chart_Batang=[{
				"Model": AfterSalesDashboard.parsedData[4194304][0].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][0].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][0].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][0].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][0].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][0].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][1].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][1].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][1].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][1].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][1].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][1].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][2].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][2].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][2].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][2].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][2].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][2].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][3].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][3].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][3].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][3].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][3].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][3].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][4].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][4].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][4].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][4].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][4].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][4].Fld5
			}, {
				"Model": AfterSalesDashboard.parsedData[4194304][5].Category,
				"Matching": AfterSalesDashboard.parsedData[4194304][5].Fld4,
				"Zero_To_10_Days": AfterSalesDashboard.parsedData[4194304][5].Fld3,
				"Ten_To_30_Days": AfterSalesDashboard.parsedData[4194304][5].Fld2,
				"More_Than_30": AfterSalesDashboard.parsedData[4194304][5].Fld1,
				"Total_Taon_Lalu": AfterSalesDashboard.parsedData[4194304][5].Fld5
			}];
			
		for(var i = 0; i < $scope.Data_Dummy_Chart_Batang.length; ++i)
		{
			if($scope.Data_Dummy_Chart_Batang[i]['Total_Taon_Lalu']!=0)
			{
				$scope.Data_Dummy_Chart_Batang[i]['Itungan_Taon_Lalu']=($scope.Data_Dummy_Chart_Batang[i]['Matching']+$scope.Data_Dummy_Chart_Batang[i]['Zero_To_10_Days']+$scope.Data_Dummy_Chart_Batang[i]['Ten_To_30_Days']+$scope.Data_Dummy_Chart_Batang[i]['More_Than_30'])/$scope.Data_Dummy_Chart_Batang[i]['Total_Taon_Lalu'];
			}
			else if($scope.Data_Dummy_Chart_Batang[i]['Total_Taon_Lalu']==0)
			{
				$scope.Data_Dummy_Chart_Batang[i]['Itungan_Taon_Lalu']="Data bulan lalu tak ditemukan";
			}
			
		}	
		
		var chart = AmCharts.makeChart("chartdivbatang", {
			"type": "serial",
			"theme": "light",
			"legend": {
				"horizontalGap": 10,
				"maxColumns": 1,
				"position": "right",
				"useGraphSettings": true,
				"markerSize": 10
			},
			"dataProvider": $scope.Data_Dummy_Chart_Batang,
			"valueAxes": [{
				"stackType": "regular",
				"axisAlpha": 0.3,
				"gridAlpha": 0,
				"totalText": "[[total]]  ([[Itungan_Taon_Lalu]] mth)"
			}],
			"graphs": [{
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": "Matching",
				"type": "column",
				"color": "#000000",
				"valueField": "Matching"
			}, {
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": "0-10 Days",
				"type": "column",
				"color": "#000000",
				"valueField": "Zero_To_10_Days"
			}, {
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": "10-30 Days",
				"type": "column",
				"color": "#000000",
				"valueField": "Ten_To_30_Days"
			}, {
				"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
				"fillAlphas": 0.8,
				"labelText": "[[value]]",
				"lineAlpha": 0.3,
				"title": ">30 Days",
				"type": "column",
				"color": "#000000",
				"valueField": "More_Than_30"
			}],
			"categoryField": "Model",
			"categoryAxis": {
				"gridPosition": "start",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"position": "left"
			},
			"export": {
				"enabled": false
			 }

		});
		//akhir batang
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});