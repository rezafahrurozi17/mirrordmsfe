angular.module('app')
.controller('DashboardRsBoard', function($scope,$interval,AfterSalesDashboard) {
	$scope.TodayDateBoardRs_Raw=new Date();
	$scope.TodayMonthMinus1_Raw=new Date($scope.TodayDateBoardRs_Raw.setMonth($scope.TodayDateBoardRs_Raw.getMonth()-1));

	$scope.BoardRsCat1=AfterSalesDashboard.parsedData[2097152][6].Category.substring(10);
	$scope.BoardRsCat2=AfterSalesDashboard.parsedData[2097152][7].Category.substring(10);
	$scope.BoardRsCat3=AfterSalesDashboard.parsedData[2097152][8].Category.substring(10);
	$scope.BoardRsCat4=AfterSalesDashboard.parsedData[2097152][9].Category.substring(10);
	$scope.BoardRsCat5=AfterSalesDashboard.parsedData[2097152][10].Category.substring(10);
	$scope.BoardRsCat6=AfterSalesDashboard.parsedData[2097152][11].Category.substring(10);
	

    function refreshData(){
        console.log('masuk refreshData');
		$scope.warna_target_rs = "warna_coklat";
		
		$scope.BoardRsTotalAchievement=AfterSalesDashboard.parsedData[2097152][6].Fld10;
		$scope.BoardRsTotalAchievementPercent=AfterSalesDashboard.parsedData[2097152][6].Fld12;
		if($scope.BoardRsTotalAchievementPercent>90)
		{
			$scope.ach_total_rs = "warna_hijau";
		}
		else if($scope.BoardRsTotalAchievementPercent<=90 && $scope.BoardRsTotalAchievementPercent>=50)
		{
			$scope.ach_total_rs = "warna_kuning";
		}
		else if($scope.BoardRsTotalAchievementPercent<50)
		{
			$scope.ach_total_rs = "warna_merah";
		}

		
		$scope.BoardRsTargetRs=AfterSalesDashboard.parsedData[2097152][6].Fld11;
		$scope.BoardRsTargetRsLast=AfterSalesDashboard.parsedData[2097152][6].Fld4;
		$scope.BoardRsTargetRsLastPercentage=AfterSalesDashboard.parsedData[2097152][6].Fld6;
		if($scope.BoardRsTargetRsLastPercentage>90)
		{
			$scope.ach_total_last_rs = "warna_hijau";
		}
		else if($scope.BoardRsTargetRsLastPercentage<=90 && $scope.BoardRsTargetRsLastPercentage>=50)
		{
			$scope.ach_total_last_rs = "warna_kuning";
		}
		else if($scope.BoardRsTargetRsLastPercentage<50)
		{
			$scope.ach_total_last_rs = "warna_merah";
		}
		
		$scope.BoardRsAch1=AfterSalesDashboard.parsedData[2097152][6].Fld7;
		$scope.BoardRsAch1Percentage=AfterSalesDashboard.parsedData[2097152][6].Fld9;
		if($scope.BoardRsAch1Percentage>90)
		{
			$scope.ach_total_t1_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch1Percentage<=90 && $scope.BoardRsAch1Percentage>=50)
		{
			$scope.ach_total_t1_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch1Percentage<50)
		{
			$scope.ach_total_t1_rs = "warna_merah";
		}
		$scope.BoardRsAch1Target=AfterSalesDashboard.parsedData[2097152][6].Fld8;
		$scope.BoardRsAch1Last=AfterSalesDashboard.parsedData[2097152][6].Fld1;
		$scope.BoardRsAch1LastPercentage=AfterSalesDashboard.parsedData[2097152][6].Fld3;
		if($scope.BoardRsAch1LastPercentage>90)
		{
			$scope.ach_total_t1_last_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch1LastPercentage<=90 && $scope.BoardRsAch1LastPercentage>=50)
		{
			$scope.ach_total_t1_last_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch1LastPercentage<50)
		{
			$scope.ach_total_t1_last_rs = "warna_merah";
		}
		
		$scope.BoardRsAch2=AfterSalesDashboard.parsedData[2097152][7].Fld7;
		$scope.BoardRsAch2Percentage=AfterSalesDashboard.parsedData[2097152][7].Fld9;
		if($scope.BoardRsAch2Percentage>90)
		{
			$scope.ach_total_t3_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch2Percentage<=90 && $scope.BoardRsAch2Percentage>=50)
		{
			$scope.ach_total_t3_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch2Percentage<50)
		{
			$scope.ach_total_t3_rs = "warna_merah";
		}
		$scope.BoardRsAch2Target=AfterSalesDashboard.parsedData[2097152][7].Fld8;
		$scope.BoardRsAch2Last=AfterSalesDashboard.parsedData[2097152][7].Fld1;
		$scope.BoardRsAch2LastPercentage=AfterSalesDashboard.parsedData[2097152][7].Fld3;
		if($scope.BoardRsAch2LastPercentage>90)
		{
			$scope.ach_total_t3_last_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch2LastPercentage<=90 && $scope.BoardRsAch2LastPercentage>=50)
		{
			$scope.ach_total_t3_last_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch2LastPercentage<50)
		{
			$scope.ach_total_t3_last_rs = "warna_merah";
		}
		
		$scope.BoardRsAch3=AfterSalesDashboard.parsedData[2097152][8].Fld7;
		$scope.BoardRsAch3Percentage=AfterSalesDashboard.parsedData[2097152][8].Fld9;
		if($scope.BoardRsAch3Percentage>90)
		{
			$scope.ach_total_t5_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch3Percentage<=90 && $scope.BoardRsAch3Percentage>=50)
		{
			$scope.ach_total_t5_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch3Percentage<50)
		{
			$scope.ach_total_t5_rs = "warna_merah";
		}
		$scope.BoardRsAch3Target=AfterSalesDashboard.parsedData[2097152][8].Fld8;
		$scope.BoardRsAch3Last=AfterSalesDashboard.parsedData[2097152][8].Fld1;
		$scope.BoardRsAch3LastPercentage=AfterSalesDashboard.parsedData[2097152][8].Fld3;
		if($scope.BoardRsAch2LastPercentage>90)
		{
			$scope.ach_total_t5_last_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch2LastPercentage<=90 && $scope.BoardRsAch2LastPercentage>=50)
		{
			$scope.ach_total_t5_last_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch2LastPercentage<50)
		{
			$scope.ach_total_t5_last_rs = "warna_merah";
		}
		
		$scope.BoardRsAch4=AfterSalesDashboard.parsedData[2097152][9].Fld7;
		$scope.BoardRsAch4Percentage=AfterSalesDashboard.parsedData[2097152][9].Fld9;
		if($scope.BoardRsAch4Percentage>90)
		{
			$scope.ach_total_t2_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch4Percentage<=90 && $scope.BoardRsAch4Percentage>=50)
		{
			$scope.ach_total_t2_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch4Percentage<50)
		{
			$scope.ach_total_t2_rs = "warna_merah";
		}
		$scope.BoardRsAch4Target=AfterSalesDashboard.parsedData[2097152][9].Fld8;
		$scope.BoardRsAch4Last=AfterSalesDashboard.parsedData[2097152][9].Fld1;
		$scope.BoardRsAch4LastPercentage=AfterSalesDashboard.parsedData[2097152][9].Fld3;
		if($scope.BoardRsAch4LastPercentage>90)
		{
			$scope.ach_total_t2_last_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch4LastPercentage<=90 && $scope.BoardRsAch4LastPercentage>=50)
		{
			$scope.ach_total_t2_last_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch4LastPercentage<50)
		{
			$scope.ach_total_t2_last_rs = "warna_merah";
		}
		
		$scope.BoardRsAch5=AfterSalesDashboard.parsedData[2097152][10].Fld7;
		$scope.BoardRsAch5Percentage=AfterSalesDashboard.parsedData[2097152][10].Fld9;
		if($scope.BoardRsAch5Percentage>90)
		{
			$scope.ach_total_t4_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch5Percentage<=90 && $scope.BoardRsAch5Percentage>=50)
		{
			$scope.ach_total_t4_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch5Percentage<50)
		{
			$scope.ach_total_t4_rs = "warna_merah";
		}
		$scope.BoardRsAch5Target=AfterSalesDashboard.parsedData[2097152][10].Fld8;
		$scope.BoardRsAch5Last=AfterSalesDashboard.parsedData[2097152][10].Fld1;
		$scope.BoardRsAch5LastPercentage=AfterSalesDashboard.parsedData[2097152][10].Fld3;
		if($scope.BoardRsAch5LastPercentage>90)
		{
			$scope.ach_total_t4_last_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch5LastPercentage<=90 && $scope.BoardRsAch5LastPercentage>=50)
		{
			$scope.ach_total_t4_last_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch5LastPercentage<50)
		{
			$scope.ach_total_t4_last_rs = "warna_merah";
		}
		
		$scope.BoardRsAch6=AfterSalesDashboard.parsedData[2097152][11].Fld7;
		$scope.BoardRsAch6Percentage=AfterSalesDashboard.parsedData[2097152][11].Fld9;
		if($scope.BoardRsAch6Percentage>90)
		{
			$scope.ach_total_t6_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch6Percentage<=90 && $scope.BoardRsAch6Percentage>=50)
		{
			$scope.ach_total_t6_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch6Percentage<50)
		{
			$scope.ach_total_t6_rs = "warna_merah";
		}
		$scope.BoardRsAch6Target=AfterSalesDashboard.parsedData[2097152][11].Fld8;
		$scope.BoardRsAch6Last=AfterSalesDashboard.parsedData[2097152][11].Fld1;
		$scope.BoardRsAch6LastPercentage=AfterSalesDashboard.parsedData[2097152][11].Fld3;
		if($scope.BoardRsAch6LastPercentage>90)
		{
			$scope.ach_total_t6_last_rs = "warna_hijau";
		}
		else if($scope.BoardRsAch6LastPercentage<=90 && $scope.BoardRsAch6LastPercentage>=50)
		{
			$scope.ach_total_t6_last_rs = "warna_kuning";
		}
		else if($scope.BoardRsAch6LastPercentage<50)
		{
			$scope.ach_total_t6_last_rs = "warna_merah";
		}
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});