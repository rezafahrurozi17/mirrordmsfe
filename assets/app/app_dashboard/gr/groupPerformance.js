angular.module('app')
.controller('DashboardGroupGR', function($scope,$interval,AfterSalesDashboard,CurrentUser) {

    $scope.options = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#38c';
        //   else
            return '#508abc';
        },
        scaleColor:false,
        lineWidth:10,
        lineCap:'circle',
        size:140,
    };
    $scope.user = CurrentUser.user();
    $scope.options2 = angular.copy($scope.options);
    $scope.options2.barColor = function(){
        return '#92d14f';
    }
    var valueSekarang = 0;
    $scope.gaugeOptions1 = {
        type: "gauge",
        theme: "light",
        autoResize:false,
        marginTop: 40,
        marginLeft: 40,
        marginRight: 40,
        marginBottom:20,
        axes: [ {
            axisThickness: 1,
            axisAlpha: 0.2,
            tickAlpha: 0.2,
            startValue:0,
            labelOffset:0,
            minorTickInterval:60,
            fontSize:8,
            // labelsEnabled:false,
            // valueInterval: 20,
            bands: [ {
                color: "#cc4748",
                endValue: 40,
                radius: "130%",
                innerRadius: "100%",
                startValue: 0
            },
            //  {
            //     color: "#fdd400",
            //     endValue: 80,
            //     startValue: 40
            // },
             {
                color: "#84b761",
                endValue: 120,
                radius: "130%",
                innerRadius: "100%",
                startValue: 40
            } ],
            bottomText: "0 %",
            bottomTextYOffset: -20,
            endValue: 120
        } ],
        arrows: [ {} ],
        export: {
            enabled: true
        }
    }
    $scope.gaugeOptions2 = angular.copy($scope.gaugeOptions1);

    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[64][0].Fld1; //Stall Capacity
        $scope.data2 = AfterSalesDashboard.parsedData[64][0].Fld2; //Q1
        $scope.data3 = AfterSalesDashboard.parsedData[64][0].Fld3; //RUM
        $scope.data4 = AfterSalesDashboard.parsedData[64][0].Fld4; //RPS
        $scope.data5 = AfterSalesDashboard.parsedData[64][0].Fld5; //Technician efficiency
        $scope.data6 = AfterSalesDashboard.parsedData[64][0].Fld6; //OP
        $scope.data7 = AfterSalesDashboard.parsedData[64][0].Fld7; //LU

        var tmpValueAwal1 = 0
        var tmpValueAkhir1 = 0
        var tmpValueAwal2 = 0
        var tmpValueAkhir2 = 0
        
        if($scope.data5 < 50){
            tmpValueAwal1 = 0;
        }else{
            tmpValueAwal1 = $scope.data5 - 50;
        }

        if($scope.data6 < 50){
            tmpValueAwal2 = 0;
        }else{
            tmpValueAwal2 = $scope.data6 - 50;
        }
        tmpValueAkhir1 = $scope.data5;

        if(tmpValueAkhir1 < 1000){
            tmpValueAkhir1 = $scope.data5 +50
        }else if(tmpValueAkhir1 >= 1000){
            tmpValueAkhir1 = 1000
        }
        // if(tmpValueAkhir1 > 100){
        //     tmpValueAkhir1 = Math.round(tmpValueAkhir1/100).toFixed(0)
        // }
        valueSekarang= $scope.data5;
        // if($scope.data5 > 100){
        //     valueSekarang  = Math.round($scope.data5/100).toFixed(1)
        // }
        tmpValueAkhir2 = $scope.data6

        if(tmpValueAkhir2 < 200){
            tmpValueAkhir2 = $scope.data6 +50
        }else if(tmpValueAkhir2 >=200){
            tmpValueAkhir2 = 200
        }

        console.log('===>',tmpValueAkhir1,'-----', valueSekarang);

        if($scope.data5 > 0){
            //$scope.gaugeOptions1.axes[0].bands[0].startValue = 0;
            //$scope.gaugeOptions1.axes[0].bands[0].endValue = (parseInt(tmpValueAkhir1) + 50)/4; //tmpValueAwal1 + 60;
            

            // $scope.gaugeOptions1.axes[0].bands[1].startValue = (tmpValueAkhir1 + 50)/4; //tmpValueAwal1 + 50;
            // $scope.gaugeOptions1.axes[0].bands[1].endValue = tmpValueAkhir1 + 50;
            // $scope.gaugeOptions1.axes[0].endValue = tmpValueAkhir1 + 50;

            //$scope.gaugeOptions1.axes[0].bands[1].startValue = (parseInt(tmpValueAkhir1) + 50)/4; //tmpValueAwal1 + 50;
            // $scope.gaugeOptions1.axes[0].bands[1].endValue = 1000;
            // $scope.gaugeOptions1.axes[0].endValue = 1000;
            $scope.gaugeOptions1.axes[0].bands[0].startValue = 0;
            $scope.gaugeOptions1.axes[0].bands[0].endValue = (tmpValueAkhir1 + 50)/4; //tmpValueAwal2 + 50;

            $scope.gaugeOptions1.axes[0].bands[1].startValue = (tmpValueAkhir1 + 50)/4; //tmpValueAwal2 + 50;
            $scope.gaugeOptions1.axes[0].bands[1].endValue = tmpValueAkhir1;
            $scope.gaugeOptions1.axes[0].endValue = tmpValueAkhir1;
            console.log('===> asu',$scope.gaugeOptions1, tmpValueAkhir1);

            $scope.gaugeOptions1.axes[0].valueInterval = 50
        }else{
            $scope.gaugeOptions1.axes[0].bands[0].startValue = 0;
            $scope.gaugeOptions1.axes[0].bands[0].endValue = 40;
            $scope.gaugeOptions1.axes[0].bands[1].startValue = 40;
            $scope.gaugeOptions1.axes[0].bands[1].endValue = 120;
            $scope.gaugeOptions1.axes[0].endValue = 120;
            $scope.gaugeOptions1.axes[0].valueInterval = 20;
        }
        console.log('===>',$scope.gaugeOptions1);
        if($scope.data6 > 0){
            $scope.gaugeOptions2.axes[0].bands[0].startValue = 0;
            $scope.gaugeOptions2.axes[0].bands[0].endValue = (tmpValueAkhir2 + 50)/4; //tmpValueAwal2 + 50;

            $scope.gaugeOptions2.axes[0].bands[1].startValue = (tmpValueAkhir2 + 50)/4; //tmpValueAwal2 + 50;
            $scope.gaugeOptions2.axes[0].bands[1].endValue = tmpValueAkhir2;
            $scope.gaugeOptions2.axes[0].endValue = tmpValueAkhir2;
            $scope.gaugeOptions2.axes[0].valueInterval = 20
        }else{
            $scope.gaugeOptions2.axes[0].bands[0].startValue = 0;
            $scope.gaugeOptions2.axes[0].bands[0].endValue = 40;
            $scope.gaugeOptions2.axes[0].bands[1].startValue = 40;
            $scope.gaugeOptions2.axes[0].bands[1].endValue = 120;
            $scope.gaugeOptions2.axes[0].endValue = 120;
            $scope.gaugeOptions2.axes[0].valueInterval = 20;
        }

    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);

    $scope.gaugeOptions1.arrows[0].value=$scope.data5;
    $scope.gaugeOptions1.axes[0].bottomText=$scope.data5+" %";

    // $scope.gaugeOptions1.axes[0].bands[1].endValue = $scope.data5 + 100;

    $scope.gaugeOptions2.arrows[0].value=$scope.data6;
    $scope.gaugeOptions2.axes[0].bottomText=$scope.data6+" %";
});
