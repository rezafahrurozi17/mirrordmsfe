angular.module('app')
.controller('DashboardAppointmentGR', function($scope,$interval,AfterSalesDashboard) {
    $scope.options1 = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#1874cd';
        //   else
        //     return '#f33';
            return '#1874cd';
        },
        scaleColor:false,
        lineWidth:12,
        lineCap:'circle',
        size:150,
    };
    $scope.options2 = angular.copy($scope.options1);
    $scope.options2.barColor = function(percent){
        // if(percent>=70)
        //     return '#458b00';
        // else
        //     return '#f33';
        return '#458b00';
    };
    function refreshData(){
        console.log('masuk refreshData');
        $scope.data1 = AfterSalesDashboard.parsedData[2][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[2][0].Fld2;
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});