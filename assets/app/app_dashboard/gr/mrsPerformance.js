angular.module('app')
.controller('MRSPerformance', function($scope,$interval,AfterSalesDashboard) {

    $scope.options1 = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
            return '#458b00';
        //   else
            // return '#f33';
        },
        scaleColor:false,
        lineWidth:10,
        // lineWidth:6,
        lineCap:'circle',
        size:120,
    };
    $scope.options2 = angular.copy($scope.options1);
    $scope.options3 = angular.copy($scope.options1);
    $scope.options4 = angular.copy($scope.options1);
    $scope.options5 = angular.copy($scope.options1);
    $scope.options2.barColor = function(percent){
        // if(percent>=70)
            return '#1874cd';
            // return '#458b00';
        // else
        //     return '#f33';
    };
    $scope.options3.barColor = function(percent){
        // if(percent>=70)
            return '#d2691e';
        // else
        //     return '#f33';
    };
    $scope.options4.barColor = function(percent){
        if(percent>=70)
            return '#1874cd';
        else
            return '#f33';
    };
    $scope.options5.barColor = function(percent){
        if(percent>=70)
            return '#458b00';
        else
            return '#f33'; 
    };
    //
    // $scope.gaugeOptions = {
    //     type: "gauge",
    //     theme: "light",
    //     axes: [ {
    //         axisThickness: 1,
    //         axisAlpha: 0.2,
    //         tickAlpha: 100,
    //         valueInterval: 20,
    //         bands: [ {
    //             color: "#84b761",
    //             endValue: 40,
    //             startValue: 0
    //         }, {
    //             color: "#fdd400",
    //             endValue: 80,
    //             startValue: 40
    //         }, {
    //             color: "#cc4748",
    //             endValue: 120,
    //             innerRadius: "95%",
    //             startValue: 80
    //         } ],
    //         bottomText: "0 %",
    //         bottomTextYOffset: -20,
    //         endValue: 120
    //     } ],
    //     arrows: [ {} ],
    //     export: {
    //         enabled: true
    //     }
    // }
    var mrsTarget;
    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[128][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[128][0].Fld2;
        $scope.data3 = AfterSalesDashboard.parsedData[128][0].Fld3;
        $scope.data4 = AfterSalesDashboard.parsedData[128][0].Fld4;
        $scope.data5 = AfterSalesDashboard.parsedData[128][0].Fld5;
        $scope.data6 = AfterSalesDashboard.parsedData[128][0].Fld6;
        $scope.data7 = AfterSalesDashboard.parsedData[128][0].Fld7;
        $scope.data8 = AfterSalesDashboard.parsedData[128][0].Fld8;    
        $scope.data9 = AfterSalesDashboard.parsedData[128][0].Fld9; 
        $scope.data10 = AfterSalesDashboard.parsedData[128][0].Fld10;   
        $scope.data11 = AfterSalesDashboard.parsedData[128][0].Fld11;
        mrsTarget = $scope.data11;
    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);

    
    $scope.gaugeOptions = {
        type: "gauge",
        theme: "light",
        // marginTop: 0,
        // marginLeft: 50,
        // marginRight: 50,
        // marginBottom: 60,
        marginTop: 0,
        marginRight:0,
        marginLeft:0,
        marginBottom:-100,
        autoResize:false,
        axes: [ {
            axisThickness: 0.2,
            axisAlpha: 0.2,
            bandAlpha:1,
            tickAlpha: 0.2,
            bandOutlineThickness:50,
            valueInterval: 500,
            // ===============
            topTextFontSize: 20,
            // topTextYOffset: 30, 
            axisColor: "#31d6ea",
            axisThickness: 1,
            gridInside: true,
            inside: false,
            radius: '50%',
            tickColor: "#67b7dc",
            startAngle: -90,
            endAngle: 90,
            bandOutlineAlpha: 0,
            labelOffset:60,
            // =======
            bands: [ {
                color: "#c55",
                endValue: 500,
                startValue: 0,
                innerRadius: "100%",
                radius: "175%",
            }, {
                color: "#cc0",
                endValue: 990,
                startValue: 500,
                innerRadius: "100%",
                radius: "175%",
            }, 
            {
                color: "#000",
                endValue: 1000,
                startValue: 990,
                innerRadius: "100%",
                radius: "175%",
            },
            {
                color: "#0d5",
                endValue: 1500,
                innerRadius: "100%",
                radius: "175%",
                startValue: 1000
            } ],
            endValue: 1500
        } ], 
        arrows: [ {} ],
        // "theme": "light",
        // "type": "gauge",
        // "axes": [{
        //     "topTextFontSize": 20,
        //     "topTextYOffset": 70,
        //     "axisColor": "#31d6ea",
        //     "axisThickness": 1,
        //     "endValue": 100,
        //     "gridInside": true,
        //     "inside": true,
        //     "radius": "50%",
        //     "valueInterval": 10,
        //     "tickColor": "#67b7dc",
        //     "startAngle": -90,
        //     "endAngle": 90,
        //     "unit": "%",
        //     "bandOutlineAlpha": 0,
        //     "bands": [{
        //     "color": "#0080ff",
        //     "endValue": 100,
        //     "innerRadius": "105%",
        //     "radius": "170%",
        //     "gradientRatio": [0.5, 0, -0.5],
        //     "startValue": 0
        //     }, {
        //     "color": "#3cd3a3",
        //     "endValue": 0,
        //     "innerRadius": "105%",
        //     "radius": "170%",
        //     "gradientRatio": [0.5, 0, -0.5],
        //     "startValue": 0
        //     }]
        // }],
        // "arrows": [{
        //     "alpha": 1,
        //     "innerRadius": "35%",
        //     "nailRadius": 0,
        //     "radius": "170%"
        // }]
    }
    refreshData = function(){
        console.log("AfterSalesDashboard.parsedData ====>",AfterSalesDashboard.parsedData);
        $scope.data7 = AfterSalesDashboard.parsedData[128][0].Fld7;
        var value = $scope.data7    ;
        $scope.gaugeOptions.arrows[0].value=value;
        $scope.gaugeOptions.axes[0].bottomText=value; 
    }
    refreshData();
    $interval(function(){
        refreshData();
        resize();
    },5000);
    // var value = Math.round(Math.random() * 100); 

    
});