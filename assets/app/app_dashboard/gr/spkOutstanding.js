angular.module('app')
.controller('spkOutstandingBoard', function($scope,$interval,AfterSalesDashboard) {
	
	

    function refreshData(){
        console.log('masuk refreshData');
		
		var DaCat1=AfterSalesDashboard.parsedData[8388608][0].Category.split(' ');
		var DaCat2=AfterSalesDashboard.parsedData[8388608][3].Category.split(' ');
		var DaCat3=AfterSalesDashboard.parsedData[8388608][6].Category.split(' ');
		
		var DaVal1=0;
		var DaVal2=0;
		var DaVal3=0;
		
		DaVal1+=AfterSalesDashboard.parsedData[8388608][0].Fld2+AfterSalesDashboard.parsedData[8388608][1].Fld2+AfterSalesDashboard.parsedData[8388608][2].Fld2;
		DaVal2+=AfterSalesDashboard.parsedData[8388608][3].Fld2+AfterSalesDashboard.parsedData[8388608][4].Fld2+AfterSalesDashboard.parsedData[8388608][5].Fld2;
		DaVal3+=AfterSalesDashboard.parsedData[8388608][6].Fld2+AfterSalesDashboard.parsedData[8388608][7].Fld2+AfterSalesDashboard.parsedData[8388608][8].Fld2;
		
		//awal pie chart pie chart with legend
		$scope.IsiDataPieChartWithLegend=[{
					"OutStandingMonitoringCategory": DaCat1[2],
					"OutStandingMonitoringValue": DaVal1,
					"color": "#FF0000"
				  }, {
					"OutStandingMonitoringCategory": DaCat2[2],
					"OutStandingMonitoringValue": DaVal2,
					"color": "#00AF50"
				  }, {
					"OutStandingMonitoringCategory": DaCat3[2],
					"OutStandingMonitoringValue": DaVal3,
					"color": "#FFFF00"
				  }];
		$scope.TotalIsiDataPieChartWithLegend=0;
		for(var x = 0; x < $scope.IsiDataPieChartWithLegend.length; ++x)
		{
			$scope.TotalIsiDataPieChartWithLegend+=$scope.IsiDataPieChartWithLegend[x].OutStandingMonitoringValue;
		}
				
		var chartPieWithLegend =AmCharts.makeChart("chartdiv3", {
					  "type": "pie",
					  "labelRadius": -37,
					  "theme": "light",
					  "allLabels": [{
						"text": "Total O/S:",
						"align": "center",
						"size": 22,
						"bold": true,
						"y": 220
					  }, {
						"text": ""+$scope.TotalIsiDataPieChartWithLegend,
						"align": "center",
						"size": 22,
						"bold": false,
						"y": 250
					  }],
					  "legend":{
						"position":"bottom",
						"valueText" : "",
						"autoMargins":true
					  },
					  "dataProvider": $scope.IsiDataPieChartWithLegend,
					  "labelText": "[[value]],[[percents]]%",
					  "titleField": "OutStandingMonitoringCategory",
					  "valueField": "OutStandingMonitoringValue",
					  "colorField": "color",
					  "radius": "42%",
					  "innerRadius": "60%",
					});
		
		
		//akhir pie chart pie chart with legend

		
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});