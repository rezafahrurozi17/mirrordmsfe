angular.module('app')
.controller('DashboardSpkBoard', function($scope,$interval,AfterSalesDashboard) {
	$scope.TodayDateBoardSpk_Raw=new Date();
	$scope.TodayMonthMinus1_Raw=new Date($scope.TodayDateBoardSpk_Raw.setMonth($scope.TodayDateBoardSpk_Raw.getMonth()-1));

	$scope.BoardSpkCat1=AfterSalesDashboard.parsedData[2097152][0].Category.substring(14);
	$scope.BoardSpkCat2=AfterSalesDashboard.parsedData[2097152][1].Category.substring(14);
	$scope.BoardSpkCat3=AfterSalesDashboard.parsedData[2097152][2].Category.substring(14);
	$scope.BoardSpkCat4=AfterSalesDashboard.parsedData[2097152][3].Category.substring(14);
	$scope.BoardSpkCat5=AfterSalesDashboard.parsedData[2097152][4].Category.substring(14);
	$scope.BoardSpkCat6=AfterSalesDashboard.parsedData[2097152][5].Category.substring(14);
	

    function refreshData(){
        console.log('masuk refreshData');
		$scope.warna_target_spk = "warna_abu_abu";
		
		$scope.BoardSpkTotalAchievement=AfterSalesDashboard.parsedData[2097152][0].Fld10;
		$scope.BoardSpkTotalAchievementPercent=AfterSalesDashboard.parsedData[2097152][0].Fld12;
		if($scope.BoardSpkTotalAchievementPercent>90)
		{
			$scope.ach_total_spk = "warna_hijau";
		}
		else if($scope.BoardSpkTotalAchievementPercent<=90 && $scope.BoardSpkTotalAchievementPercent>=50)
		{
			$scope.ach_total_spk = "warna_kuning";
		}
		else if($scope.BoardSpkTotalAchievementPercent<50)
		{
			$scope.ach_total_spk = "warna_merah";
		}
		
		$scope.BoardSpkTargetSpk=AfterSalesDashboard.parsedData[2097152][0].Fld11;
		$scope.BoardSpkTargetSpkLast=AfterSalesDashboard.parsedData[2097152][0].Fld4;
		$scope.BoardSpkTargetSpkLastPercentage=AfterSalesDashboard.parsedData[2097152][0].Fld6;
		if($scope.BoardSpkTargetSpkLastPercentage>90)
		{
			$scope.ach_total_last_spk = "warna_hijau";
		}
		else if($scope.BoardSpkTargetSpkLastPercentage<=90 && $scope.BoardSpkTargetSpkLastPercentage>=50)
		{
			$scope.ach_total_last_spk = "warna_kuning";
		}
		else if($scope.BoardSpkTargetSpkLastPercentage<50)
		{
			$scope.ach_total_last_spk = "warna_merah";
		}
		
		$scope.BoardSpkAch1=AfterSalesDashboard.parsedData[2097152][0].Fld7;
		$scope.BoardSpkAch1Percentage=AfterSalesDashboard.parsedData[2097152][0].Fld9;
		if($scope.BoardSpkAch1Percentage>90)
		{
			$scope.ach_total_t1_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch1Percentage<=90 && $scope.BoardSpkAch1Percentage>=50)
		{
			$scope.ach_total_t1_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch1Percentage<50)
		{
			$scope.ach_total_t1_spk = "warna_merah";
		}
		$scope.BoardSpkAch1Target=AfterSalesDashboard.parsedData[2097152][0].Fld8;
		$scope.BoardSpkAch1Last=AfterSalesDashboard.parsedData[2097152][0].Fld1;
		$scope.BoardSpkAch1LastPercentage=AfterSalesDashboard.parsedData[2097152][0].Fld3;
		if($scope.BoardSpkAch1LastPercentage>90)
		{
			$scope.ach_total_t1_last_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch1LastPercentage<=90 && $scope.BoardSpkAch1LastPercentage>=50)
		{
			$scope.ach_total_t1_last_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch1LastPercentage<50)
		{
			$scope.ach_total_t1_last_spk = "warna_merah";
		}
		
		$scope.BoardSpkAch2=AfterSalesDashboard.parsedData[2097152][1].Fld7;
		$scope.BoardSpkAch2Percentage=AfterSalesDashboard.parsedData[2097152][1].Fld9;
		if($scope.BoardSpkAch2Percentage>90)
		{
			$scope.ach_total_t3_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch2Percentage<=90 && $scope.BoardSpkAch2Percentage>=50)
		{
			$scope.ach_total_t3_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch2Percentage<50)
		{
			$scope.ach_total_t3_spk = "warna_merah";
		}
		$scope.BoardSpkAch2Target=AfterSalesDashboard.parsedData[2097152][1].Fld8;
		$scope.BoardSpkAch2Last=AfterSalesDashboard.parsedData[2097152][1].Fld1;
		$scope.BoardSpkAch2LastPercentage=AfterSalesDashboard.parsedData[2097152][1].Fld3;
		if($scope.BoardSpkAch2LastPercentage>90)
		{
			$scope.ach_total_t3_last_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch2LastPercentage<=90 && $scope.BoardSpkAch2LastPercentage>=50)
		{
			$scope.ach_total_t3_last_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch2LastPercentage<50)
		{
			$scope.ach_total_t3_last_spk = "warna_merah";
		}
		
		$scope.BoardSpkAch3=AfterSalesDashboard.parsedData[2097152][2].Fld7;
		$scope.BoardSpkAch3Percentage=AfterSalesDashboard.parsedData[2097152][2].Fld9;
		if($scope.BoardSpkAch3Percentage>90)
		{
			$scope.ach_total_t5_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch3Percentage<=90 && $scope.BoardSpkAch3Percentage>=50)
		{
			$scope.ach_total_t5_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch3Percentage<50)
		{
			$scope.ach_total_t5_spk = "warna_merah";
		}
		$scope.BoardSpkAch3Target=AfterSalesDashboard.parsedData[2097152][2].Fld8;
		$scope.BoardSpkAch3Last=AfterSalesDashboard.parsedData[2097152][2].Fld1;
		$scope.BoardSpkAch3LastPercentage=AfterSalesDashboard.parsedData[2097152][2].Fld3;
		if($scope.BoardSpkAch2LastPercentage>90)
		{
			$scope.ach_total_t5_last_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch2LastPercentage<=90 && $scope.BoardSpkAch2LastPercentage>=50)
		{
			$scope.ach_total_t5_last_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch2LastPercentage<50)
		{
			$scope.ach_total_t5_last_spk = "warna_merah";
		}
		
		$scope.BoardSpkAch4=AfterSalesDashboard.parsedData[2097152][3].Fld7;
		$scope.BoardSpkAch4Percentage=AfterSalesDashboard.parsedData[2097152][3].Fld9;
		if($scope.BoardSpkAch4Percentage>90)
		{
			$scope.ach_total_t2_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch4Percentage<=90 && $scope.BoardSpkAch4Percentage>=50)
		{
			$scope.ach_total_t2_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch4Percentage<50)
		{
			$scope.ach_total_t2_spk = "warna_merah";
		}
		$scope.BoardSpkAch4Target=AfterSalesDashboard.parsedData[2097152][3].Fld8;
		$scope.BoardSpkAch4Last=AfterSalesDashboard.parsedData[2097152][3].Fld1;
		$scope.BoardSpkAch4LastPercentage=AfterSalesDashboard.parsedData[2097152][3].Fld3;
		if($scope.BoardSpkAch4LastPercentage>90)
		{
			$scope.ach_total_t2_last_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch4LastPercentage<=90 && $scope.BoardSpkAch4LastPercentage>=50)
		{
			$scope.ach_total_t2_last_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch4LastPercentage<50)
		{
			$scope.ach_total_t2_last_spk = "warna_merah";
		}
		
		$scope.BoardSpkAch5=AfterSalesDashboard.parsedData[2097152][4].Fld7;
		$scope.BoardSpkAch5Percentage=AfterSalesDashboard.parsedData[2097152][4].Fld9;
		if($scope.BoardSpkAch5Percentage>90)
		{
			$scope.ach_total_t4_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch5Percentage<=90 && $scope.BoardSpkAch5Percentage>=50)
		{
			$scope.ach_total_t4_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch5Percentage<50)
		{
			$scope.ach_total_t4_spk = "warna_merah";
		}
		$scope.BoardSpkAch5Target=AfterSalesDashboard.parsedData[2097152][4].Fld8;
		$scope.BoardSpkAch5Last=AfterSalesDashboard.parsedData[2097152][4].Fld1;
		$scope.BoardSpkAch5LastPercentage=AfterSalesDashboard.parsedData[2097152][4].Fld3;
		if($scope.BoardSpkAch5LastPercentage>90)
		{
			$scope.ach_total_t4_last_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch5LastPercentage<=90 && $scope.BoardSpkAch5LastPercentage>=50)
		{
			$scope.ach_total_t4_last_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch5LastPercentage<50)
		{
			$scope.ach_total_t4_last_spk = "warna_merah";
		}
		
		$scope.BoardSpkAch6=AfterSalesDashboard.parsedData[2097152][5].Fld7;
		$scope.BoardSpkAch6Percentage=AfterSalesDashboard.parsedData[2097152][5].Fld9;
		if($scope.BoardSpkAch6Percentage>90)
		{
			$scope.ach_total_t6_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch6Percentage<=90 && $scope.BoardSpkAch6Percentage>=50)
		{
			$scope.ach_total_t6_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch6Percentage<50)
		{
			$scope.ach_total_t6_spk = "warna_merah";
		}
		$scope.BoardSpkAch6Target=AfterSalesDashboard.parsedData[2097152][5].Fld8;
		$scope.BoardSpkAch6Last=AfterSalesDashboard.parsedData[2097152][5].Fld1;
		$scope.BoardSpkAch6LastPercentage=AfterSalesDashboard.parsedData[2097152][5].Fld3;
		if($scope.BoardSpkAch6LastPercentage>90)
		{
			$scope.ach_total_t6_last_spk = "warna_hijau";
		}
		else if($scope.BoardSpkAch6LastPercentage<=90 && $scope.BoardSpkAch6LastPercentage>=50)
		{
			$scope.ach_total_t6_last_spk = "warna_kuning";
		}
		else if($scope.BoardSpkAch6LastPercentage<50)
		{
			$scope.ach_total_t6_last_spk = "warna_merah";
		}
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});