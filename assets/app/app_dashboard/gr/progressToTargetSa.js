angular.module('app')
.controller('ProgressToTargetSaGR', function($scope,$interval,AfterSalesDashboard) {

    $scope.targetunit=AfterSalesDashboard.parsedData[33554432][0].Fld10;
    console.log("$scope.targetunit",$scope.targetunit);
    $scope.warna=$scope.targetunit/3;
    $scope.targetrevenue=AfterSalesDashboard.parsedData[33554432][0].Fld11;
    var bagian1=0;
    var bagian2=0;
    var bagian3=0;
    var tmpBands = [];
    if($scope.targetrevenue < 3000000){
        bagian1 = Math.round(($scope.targetrevenue*100)/2);
        bagian2 = Math.round(($scope.targetrevenue*100)-3000000);
        bagian3 = Math.round(($scope.targetrevenue*100)*1.5);
        tmpBands = [
        {
            color: "#cbff00",
            endValue: $scope.targetrevenue,
            startValue: 0,
            innerRadius: "105%",
            radius: "155%",
            balloonText:num1
        },
        {
            color: "#000",
            endValue:  $scope.targetrevenue,
            startValue: $scope.targetrevenue,
            innerRadius: "105%",
            radius: "155%", 
            balloonText: targetrevenue
        }, 
        {
            color: "#cbff00",
            endValue: bagian1,
            startValue: $scope.targetrevenue,
            innerRadius: "105%",
            radius: "155%",
            balloonText:bagian1
        },
        {
            color: "#cbff00",
            endValue: bagian2,
            innerRadius: "105%",
            radius: "155%",
            startValue: bagian1,
            balloonText:bagian2,
        },
        {
            color: "#cbff00",
            endValue: bagian3,
            innerRadius: "105%",
            radius: "155%",
            startValue: bagian2,
            balloonText:bagian3
        }  ]
    }else{
        bagian1 = Math.round($scope.targetrevenue/2);
        bagian2 = Math.round($scope.targetrevenue-3000000);
        bagian3 = Math.round($scope.targetrevenue*1.5);
        tmpBands = [ {
            color: "#cbff00",
            endValue: bagian1,
            startValue: 0,
            innerRadius: "105%",
            radius: "155%",
            balloonText:num1
        },{
            color: "#cbff00",
            endValue: bagian2,
            startValue: bagian1,
            innerRadius: "105%",
            radius: "155%"
        }, 
        {
            color: "#000",
            endValue:  $scope.targetrevenue,
            startValue: bagian2,
            innerRadius: "105%",
            radius: "155%",
            balloonText: targetrevenue
        }, 
        {
            color: "#cbff00",
            endValue: bagian3,
            innerRadius: "105%",
            radius: "155%",
            startValue: Math.round($scope.targetrevenue)
        } ]

    }
    $scope.revenue=AfterSalesDashboard.parsedData[33554432][0].Fld7;
    $scope.unit=AfterSalesDashboard.parsedData[33554432][0].Fld5;
    console.log("$scope.revenue",$scope.revenue);
    console.log("$scope.revenue",$scope.revenue*1.2);
    var num = $scope.targetrevenue.toLocaleString();
    var targetrevenue = num.toString();
    var num1 = $scope.revenue.toLocaleString()

    var num2 = $scope.unit;
    var unit = num2.toString();
    var num3 =$scope.targetunit;
    var targetunit = num3.toString();
    console.log("$scope.warna",Math.round($scope.warna+0.5.toFixed(2)));
    $scope.gaugeOptions1 = {
        type: "gauge",
        theme: "light",
        marginTop: 55,
        marginLeft: 50,
        marginRight: 50,
        marginBottom: 0,      
        autoResize:false,
        axes: [ {
            axisThickness: 0.2,
            axisAlpha: 0.2,
            bandAlpha:1,
            tickAlpha: 0.2,
            inside:false,
            gridInside: false,
            labelOffset: 37,
            bandOutlineThickness:50,
            valueInterval: Math.round($scope.targetunit/2),
            bands: [ {
                color: "#feaa85",
                endValue: Math.round($scope.targetunit/2),
                startValue: 0,
                innerRadius: "105%",
                radius: "155%",
                balloonText:unit
            },{
                color: "#f6d341",
                endValue: Math.round($scope.targetunit-3),
                startValue: Math.round($scope.targetunit/2),
                innerRadius: "105%",
                radius: "155%"
            }, {
                color: "#000",
                endValue:  Math.round($scope.targetunit),
                startValue: Math.round($scope.targetunit-3),
                innerRadius: "105%",
                radius: "155%",
                balloonText: targetunit
            }, {
                color: "#bdff00",
                endValue: Math.round($scope.targetunit*1.5),
                innerRadius: "105%",
                radius: "155%",
                startValue: Math.round($scope.targetunit)
            } ],
            endValue: Math.round($scope.targetunit*1.5)
        } ],
        arrows: [ {} ],
    }

    $scope.gaugeOptions2 = {
        type: "gauge",
        theme: "light",
        marginTop: 55,
        marginLeft: 60,
        marginRight: 60,
        marginBottom: 0,      
        fontSize:8,
        autoResize:false,
        axes: [ {
            axisThickness: 0.2,
            axisAlpha: 0.2,
            bandAlpha:1,
            tickAlpha: 0.2,
            inside: false,
            bandOutlineThickness:50,
            // showFirstLabel: false,
            // showLastLabel: false,
            // labelsEnabled:false,
            labelOffset: 37,
            valueInterval: bagian1,
            bands: tmpBands,
            endValue: bagian3
        } ],
        arrows: [ {} ],
        // allLabels: [{
        //     "text": "Target: "+targetrevenue,
        //     "bold": true,
        //     "x": "50%",
        //     "y": "10.5%",
        //     "rotation": 0,
        //     "width": "100%",
        //     color:"#00000",
        //     "align": "middle"
        // }],
    }
    // $scope.gaugeOptions2 = {
    //     type: "gauge",
    //     theme: "light",
    //     marginTop: 0,
    //     marginLeft: -10,
    //     marginRight: -20,
    //     marginBottom: -200,
    //     axes: [{
    //         topTextFontSize: 15,
    //         topTextYOffset: 40,
    //         axisColor: "#31d6ea",
    //         axisThickness: 1,
    //         endValue: 50000000,
    //         gridInside: true,
    //         inside: true,
    //         radius: "50%",
    //         valueInterval: 50000000,
    //         tickColor: "#67b7dc",
    //         bandOutlineAlpha: 0,
    //         bands: [{
    //             color: "#ddd",
    //             endValue: 50000000,
    //             innerRadius: "105%",
    //             radius: "170%",
    //             gradientRatio: [0.5, 0, -0.2],
    //             startValue: 0
    //         }, {
    //             color: "#3d9",
    //             endValue: 0,
    //             innerRadius: "105%",
    //             radius: "170%",
    //             gradientRatio: [0.5, 0, -0.3],
    //             startValue: 0
    //         }]
    //     }],
    //     arrows: [{
    //         value: 60,
    //         alpha: 1,
    //         innerRadius: "35%",
    //         nailRadius: 0,
    //         radius: "170%"
    //     }]
    // }
    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[33554432][0].Fld1;  //Data Unit Out Yesterday
        $scope.data2 = AfterSalesDashboard.parsedData[33554432][0].Fld2;  //Data Unit in Yesterday
        $scope.data3 = AfterSalesDashboard.parsedData[33554432][0].Fld3;  //Data out Today
        $scope.data4 = AfterSalesDashboard.parsedData[33554432][0].Fld4;  //Data in Today
        $scope.data5 = AfterSalesDashboard.parsedData[33554432][0].Fld5;  //Data Progress Unit Entry
        $scope.data6 = AfterSalesDashboard.parsedData[33554432][0].Fld6;  //daysLeft
        $scope.data7 = AfterSalesDashboard.parsedData[33554432][0].Fld7;  //Data Progress Revenue
        $scope.data8 = AfterSalesDashboard.parsedData[33554432][0].Fld8;  //Revenue Today
        $scope.data9 = AfterSalesDashboard.parsedData[33554432][0].Fld9;  //Data Target Progress Revenue
        $scope.data10 = AfterSalesDashboard.parsedData[33554432][0].Fld10;  //Data Target Progress unit entry

        console.log("$scope.data10",$scope.data10);
        //interval gauge options 2
        // var interval1 = Math.round($scope.data9 / 5);

        //  end Value untuk band pertama dan startvalue untuk band kedua
        var value1 = Math.round($scope.data10 * 0.33);

        //  end Value untuk band ketiga dan startvalue untuk band ketiga
        var value2 = Math.round($scope.data10 * 0.66);

        /*Daysleft

        */
          //Current Date
          var currentDate = new Date();

          var oneDay=1000*60*60*24;

          //Last Date
          var lastDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);

          var currentDateTime = currentDate.getTime();
          var lastDateTime = lastDate.getTime();
          var diff = lastDateTime - currentDateTime;

          //Daysleft
          var daysLeft = Math.round(diff / oneDay);
          $scope.daysLeft = daysLeft;
         
          var datarevenue = $scope.data7.toLocaleString();

        //Data Progress Revenue
        $scope.gaugeOptions2.arrows[0].value=$scope.data7;
        $scope.gaugeOptions2.axes[0].bottomText=$scope.data7.toLocaleString();

        // $scope.gaugeOptions2.axes[0].bands[1].endValue=$scope.data7//datarevenue;
        // $scope.gaugeOptions2.axes[0].topText =$scope.data7.toLocaleString();//datarevenue;
        // $scope.gaugeOptions2.arrows[0].value=$scope.data7//datarevenue;
        // $scope.gaugeOptions2.axes[0].bands[2].balloonText="166";
        console.log("$scope.data7",$scope.data7.toLocaleString());
        //Data target Progress
        // $scope.gaugeOptions2.axes[0].endValue = $scope.data9;
        // $scope.gaugeOptions2.axes[0].bands[0].endValue=$scope.data9;

        // $scope.gaugeOptions2.axes[0].valueInterval = interval1;
        // var value = Math.round(Math.random() * 100);
        $scope.gaugeOptions1.arrows[0].value=$scope.data5;
        $scope.gaugeOptions1.axes[0].bottomText=$scope.data5;  

        //Data Progress Unit Entry
        // $scope.gaugeOptions1.arrows[0].value=$scope.data5;

        //interval band pertama
        // $scope.gaugeOptions1.axes[0].bands[0].endValue=value1;

        //interval band kedua
        // $scope.gaugeOptions1.axes[0].bands[1].startValue=value1;
        // $scope.gaugeOptions1.axes[0].bands[1].endValue=value2;

        //interval band ketiga
        // $scope.gaugeOptions1.axes[0].bands[2].startValue=value2;
        // $scope.gaugeOptions1.axes[0].bands[2].endValue=$scope.data10;

        //Data Target Progress unit entry
        // $scope.gaugeOptions1.axes[0].endValue=$scope.data10;


        // $scope.gaugeOptions1.axes[0].bottomText=$scope.data5;
    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);

});
