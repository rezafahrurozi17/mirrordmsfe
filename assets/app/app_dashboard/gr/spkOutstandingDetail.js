angular.module('app')
.controller('spkOutstandingDetailBoard', function($scope,$interval,AfterSalesDashboard) {
	
	

    function refreshData(){
        console.log('masuk refreshData');
		
		//awal nested pie prototype
		// AmCharts.makeChart("chart1", {
		  // "type": "pie",
		  // "bringToFront": true,
		  // "dataProvider": [{
			// "title": "$",
			// "value": 100,
			// "color": "#090E0F"
		  // }],
		  // "startDuration": 0,
		  // "pullOutRadius": 0,
		  // "color": "#fff",
		  // "fontSize": 14,
		  // "titleField": "title",
		  // "valueField": "value",
		  // "colorField": "color",
		  // "labelRadius": -25,
		  // "labelColor": "#fff",
		  // "radius": 25,
		  // "innerRadius": 0,
		  // "labelText": "[[title]]",
		  // "balloonText": "[[title]]: [[value]]"
		// });
		
		var DaCat1=AfterSalesDashboard.parsedData[8388608][0].Category.split(' ');
		var DaCat2=AfterSalesDashboard.parsedData[8388608][4].Category.split(' ');
		var DaCat3=AfterSalesDashboard.parsedData[8388608][8].Category.split(' ');

		var chart2=AmCharts.makeChart("chart2", {
		  "type": "pie",
		  "bringToFront": true,
		  "allLabels": [{
						"text": DaCat1[3],
						"align": "center",
						"bold": true,
						"color":"#DAC958",
						"y": 290
					  }],
		  "dataProvider": [{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][0].Fld2,
			"color": "#FF0000"
		  }, {
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][3].Fld2,
			"color": "#00AF50"
		  }, {
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][6].Fld2,
			"color": "#FFFF00"
		  }],
		  "startDuration": 1,
		  "pullOutRadius": 0,
		  "color": "#000000",
		  "fontSize": 12,
		  "titleField": "title",
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": -28,
		  "labelColor": "#000000",
		  "radius": 80,
		  "innerRadius": 27,
		  "outlineAlpha": 1,
		  "outlineThickness": 4,
		  "labelText": "[[value]]",
		  "balloonText": "[[title]]: ([[percents]]%)"
		});

		var chart3=AmCharts.makeChart("chart3", {
		  "type": "pie",
		  "bringToFront": true,
		  "allLabels": [{
						"text": DaCat2[3],
						"align": "center",
						"bold": true,
						"color":"#DAC958",
						"y": 350
					  }],
		  "dataProvider": [{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][1].Fld2,
			"color": "#FF0000"
		  },{
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][4].Fld2,
			"color": "#00AF50"
		  }, {
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][7].Fld2,
			"color": "#FFFF00"
		  }],
		  "startDuration": 1,
		  "pullOutRadius": 0,
		  "color": "#000000",
		  "fontSize": 12,
		  "titleField": "title",
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": -27,
		  "labelColor": "#000000",
		  "radius": 135,
		  "innerRadius": 82,
		  "outlineAlpha": 1,
		  "outlineThickness": 4,
		  "labelText": "[[value]]",
		  "balloonText": "[[title]]: ([[percents]]%)"
		});

		var chart4=AmCharts.makeChart("chart4", {
		  "type": "pie",
		  "bringToFront": true,
		  "legend": {
			"position": "bottom",
			"markerSize": 10,
			"valueText" : "",
			"divId": "legenddiv"
		  },
		  "dataProvider": [{
			"title": DaCat1[2],
			"value": AfterSalesDashboard.parsedData[8388608][2].Fld2,
			"color": "#FF0000"
		  },{
			"title": DaCat2[2],
			"value": AfterSalesDashboard.parsedData[8388608][5].Fld2,
			"color": "#00AF50"
		  },{
			"title": DaCat3[2],
			"value": AfterSalesDashboard.parsedData[8388608][8].Fld2,
			"color": "#FFFF00"
		  }],
		  "startDuration": 1,
		  "pullOutRadius": 0,
		  "color": "#000000",
		  "fontSize": 12,
		  "titleField": "title",
		  "valueField": "value",
		  "colorField": "color",
		  "labelRadius": -27,
		  "labelColor": "#000000",
		  "radius": 190,
		  "innerRadius": 137,
		  "outlineAlpha": 1,
		  "outlineThickness": 4,
		  "labelText": "[[value]]",
		  "balloonText": "[[title]]: ([[percents]]%)",
		  "allLabels": [{
			"text": "Detail Outstanding Monitoring",
			"bold": true,
			"size": 18,
			"color": "#404040",
			"x": 0,
			"align": "center",
			"y": 20
		  },{
						"text": DaCat3[3],
						"align": "center",
						"bold": true,
						"color":"#DAC958",
						"y": 400
					  }]
		});
		//akhir nested pie prototype

		
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});