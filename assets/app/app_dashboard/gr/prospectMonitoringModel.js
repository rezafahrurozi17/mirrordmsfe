angular.module('app')
.controller('DashboardProspectMonitoringModel', function($scope,$interval,AfterSalesDashboard) {

	

    function refreshData(){
        console.log('masuk refreshData');
        //awal pie chart with label1
		$scope.ProspectMonitoringNamaModel=AfterSalesDashboard.parsedData[524288][1].Category
		$scope.IsiDataPieChartWithLabel1=[ {
						"Prospect": "Hot",
						"Jumlah": AfterSalesDashboard.parsedData[524288][1].Fld1
					  },{
						"Prospect": "Medium",
						"Jumlah": AfterSalesDashboard.parsedData[524288][1].Fld2
					  },{
						"Prospect": "Low",
						"Jumlah": AfterSalesDashboard.parsedData[524288][1].Fld3
					  } ];
		
		var chartpieProspectWithLabel1 = AmCharts.makeChart( "chartdivProspectWithLabel1_2", {
					  "type": "pie",
					  "labelRadius": -25,
					  "legend":{
						"position":"bottom",
						"valueText" : "",
						"autoMargins":true
					  },
					  "labelText": "[[value]]",
					  "dataProvider":$scope.IsiDataPieChartWithLabel1 ,
					  "valueField": "Jumlah",
					  "titleField": "Prospect",
					   "balloon":{
					   "fixedPosition":true
					  }
					} );
		//akhir pie chart with label
		
		//awal pie chart with label2
		$scope.IsiDataPieChartWithLabel2=[ {
						"Prospect": "1-3",
						"Jumlah": AfterSalesDashboard.parsedData[524288][1].Fld4
					  },{
						"Prospect": "4-5",
						"Jumlah": AfterSalesDashboard.parsedData[524288][1].Fld5
					  },{
						"Prospect": ">5",
						"Jumlah": AfterSalesDashboard.parsedData[524288][1].Fld6
					  } ];
		
		var chartpieProspectWithLabel2 = AmCharts.makeChart( "chartdivProspectWithLabel2_2", {
					  "type": "pie",
					  "labelRadius": -25,
					  "legend":{
						"position":"bottom",
						"valueText" : "",
						"autoMargins":true
					  },
					  "labelText": "[[value]]",
					  "dataProvider":$scope.IsiDataPieChartWithLabel2 ,
					  "valueField": "Jumlah",
					  "titleField": "Prospect",
					   "balloon":{
					   "fixedPosition":true
					  }
					} );
		//akhir pie chart with label
    }
    refreshData();

    $interval(function(){
        console.log('masuk interval');
        refreshData();
    },5000);
});