angular.module('app')
.controller('groupPerformanceGRController', function($scope,$interval,AfterSalesDashboard) {

    $scope.options = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#38c';
        //   else
            return '#508abc';
        },
        scaleColor:false,
        // lineWidth:6,
        lineWidth:10,
        lineCap:'circle',
        size:140,
    };

    $scope.gaugeOptions1 = {
        type: "gauge",
        theme: "light",
        // marginTop:40,
        marginBottom:-30,
        marginTop: 40,
        marginLeft: 40,
        marginRight: 40,
        // marginLeft:20,
        // marginRight:20,
        // adjustSize:false,
        autoResize:false,
        axes: [ {
            axisThickness: 1,
            axisAlpha: 0.2,
            tickAlpha: 0.2,
            valueInterval: 20,
            // bandOutlineThickness:90,
            bands: [ {
                color: "#cc4748",
                endValue: 60,
                radius: "130%",
                innerRadius: "100%",
                startValue: 0
            }, 
            // {
            //     color: "#fdd400",
            //     endValue: 80,
            //     radius: "130%",
            //     innerRadius: "100%",
            //     startValue: 40
            // }, 
            {
                color: "#84b761",
                endValue: 120,
                radius: "130%",
                innerRadius: "100%",
                startValue: 60
            } ],
            bottomText: "0 %",
            bottomTextYOffset: -20,
            endValue: 120
        } ],
        responsive: {
            enabled: true
          },
        arrows: [ {} ]
    }
    $scope.gaugeOptions2 = angular.copy($scope.gaugeOptions1);

    refreshData = function(){
        console.log("AfterSalesDashboard.parsedData ====>",AfterSalesDashboard.parsedData);
        $scope.data1 = AfterSalesDashboard.parsedData[134217728][0].Fld1; //Stall Capacity
        $scope.data2 = AfterSalesDashboard.parsedData[134217728][0].Fld2; //Q1
        $scope.data3 = AfterSalesDashboard.parsedData[134217728][0].Fld3; //RUM
        $scope.data4 = AfterSalesDashboard.parsedData[134217728][0].Fld4; //RPS
        $scope.data5 = AfterSalesDashboard.parsedData[134217728][0].Fld5; //Technician efficiency
        $scope.data6 = AfterSalesDashboard.parsedData[134217728][0].Fld6; //OP
        $scope.data7 = AfterSalesDashboard.parsedData[134217728][0].Fld7; //LU

        var tmpValueAwal1 = 0
        var tmpValueAkhir1 = 0
        if($scope.data5 < 50){
            tmpValueAwal1 = 0;
        }else{
            tmpValueAwal1 = $scope.data5 - 50;
        }
        tmpValueAkhir1 = $scope.data5 + 20;
        if($scope.data5 > 0){
            
            $scope.gaugeOptions1.axes[0].bands[0].startValue = 0;
            $scope.gaugeOptions1.axes[0].bands[0].endValue = (tmpValueAkhir1 + 20)/2; //tmpValueAwal1 + 60;
            $scope.gaugeOptions1.axes[0].valueInterval = Math.round((tmpValueAkhir1 + 20)/4).toFixed(0);

            $scope.gaugeOptions1.axes[0].bands[1].startValue = (tmpValueAkhir1 + 20)/2; //tmpValueAwal1 + 50;
            $scope.gaugeOptions1.axes[0].bands[1].endValue = tmpValueAkhir1 + 20;
            $scope.gaugeOptions1.axes[0].endValue = tmpValueAkhir1 + 20;
        }else{
            $scope.gaugeOptions1.axes[0].bands[0].startValue = 0;
            $scope.gaugeOptions1.axes[0].bands[0].endValue = 40;
            $scope.gaugeOptions1.axes[0].bands[1].startValue = 40;
            $scope.gaugeOptions1.axes[0].bands[1].endValue = 120;
            $scope.gaugeOptions1.axes[0].endValue = 120;
            $scope.gaugeOptions1.axes[0].valueInterval = 20;
        }
        $scope.gaugeOptions1.arrows[0].value=$scope.data5;
        $scope.gaugeOptions1.axes[0].bottomText=$scope.data5+" %";
    }
    refreshData();
    function resize() {
        // var w =$( "#chart1" ).parent( ".row" ).width();
        // var h = $( "#chart1" ).parent( ".row" ).height();
        // $('#chart1').width(w - w/3);
        // $('#chart1').height(h - h/3);
        // console.log("======",$scope.gaugeOptions1);
        // $scope.gaugeOptions1.marginBottom = w/5;
        // $scope.gaugeOptions1.marginLeft = h/5;
        // $scope.gaugeOptions1.marginRight = w/5; 
        // $scope.gaugeOptions1.marginTop = h/5;
        // $('#chart1 .amchart').invalidateSize();
        // $scope.gaugeOptions1.invalidateSize();
    }
    $interval(function(){
        refreshData();
        resize();
    },5000);

    $scope.gaugeOptions2.arrows[0].value=$scope.data6;
    $scope.gaugeOptions2.axes[0].bottomText=$scope.data6+" %";
});
