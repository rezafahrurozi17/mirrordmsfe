angular.module('app')
.controller('WorkshopPerformanceGR', function($scope,$interval,AfterSalesDashboard) {

    $scope.options1 = {
        animate:{
            duration:1000,
            enabled:true
        },
        trackColor:'#eee',
        barColor:function(percent){
        //   if(percent>=70)
        //     return '#38c';
        //   else
        //     return '#f33';
            return '#508abc';
        },
        scaleColor:false,
        // lineWidth:6,
        lineWidth:10,
        lineCap:'circle',
        size:140,
    };
    $scope.options2 = angular.copy($scope.options1);
    $scope.options3 = angular.copy($scope.options1);
    $scope.options4 = angular.copy($scope.options1);
    $scope.options5 = angular.copy($scope.options1);
    $scope.options6 = angular.copy($scope.options1);
    $scope.options7 = angular.copy($scope.options1);
    $scope.options2.barColor = function(percent){
        // if(percent>=70)
        //     return '#3c8';
        // else
        //     return '#f33';
        return '#d36e2a';
    };
    $scope.options3.barColor = function(percent){
        return '#508abc';
    };
    $scope.options4.barColor = function(percent){
        // if(percent>=70)
        //     return '#fc0';
        // else
        //     return '#f33';
        return '#62993e';
    };
    $scope.options5.barColor = function(percent){
        // if(percent>=70)
        //     return '#5cf';
        // else
        //     return '#f33';
        return '#508abc';
    };
    $scope.options6.barColor = function(percent){
        // if(percent>=70)
        //     return '#5eb';
        // else
        //     return '#f33';
        return '#d36e2a';
    };
    $scope.options7.barColor = function(percent){
        // if(percent>=70)
        //     return '#3c8';
        // else
        //     return '#f33';
        return '#62993e';
    };
    refreshData = function(){
        $scope.data1 = AfterSalesDashboard.parsedData[8][0].Fld1;
        $scope.data2 = AfterSalesDashboard.parsedData[8][0].Fld2;
        $scope.data3 = AfterSalesDashboard.parsedData[8][0].Fld3;
        $scope.data4 = AfterSalesDashboard.parsedData[8][0].Fld4;
        $scope.data5 = AfterSalesDashboard.parsedData[8][0].Fld5;
        $scope.data6 = AfterSalesDashboard.parsedData[8][0].Fld6;
        $scope.data7 = AfterSalesDashboard.parsedData[8][0].Fld7;
    }
    refreshData();
    $interval(function(){
        refreshData();
    },5000);

});
