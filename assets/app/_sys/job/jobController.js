angular.module('app')
    .controller('JobController', function($scope, $http, CurrentUser, Job) {
        $scope.user = CurrentUser.user();

        $scope.$on('$viewContentLoaded', function() {
            $scope.showModal_addEditJob = false;
            $scope.showModal_deleteJob = false;
        });

        $scope.mode = "";
        $scope.mJob = null;

        $scope.getAllJob = function() {
            Job.getJob().then(function(result) {
                console.log('job=>', result.data);
                $scope.allJob = result.data;
                $scope.gridOptions.data = result.data;
            });
        }

        $scope.selectRow = function(row) {
            $scope.mJob = row;
            console.log($scope.mJob);
        }
        $scope.doEditJob = function() {
            $scope.newJob = $scope.mJob;
            console.log("job function", $scope.mJob.id);
            $scope.newJob.id = $scope.newJob.id;
            $scope.showModal_addEditJob = true;
        }
        $scope.doAddJob = function() {
            $scope.newJob = [];
            $scope.mJob = null;
            $scope.showModal_addEditJob = true;
            console.log("do add job ");
        }
        $scope.doSaveJob = function(newJob) {
            if (newJob.id) {
                $scope.doEdit(newJob);
            } else {
                $scope.doSave(newJob);
            }
            // console.log("id job", $scope.mJob.id);

            // if($scope.mode=='delete'){
            //   Job.remove( $scope.mJob.id).then(function(result) {
            //      $scope.getAllJob();
            //      $scope.mJob = null;
            //   }); 
            // }else{
            //   if(typeof $scope.mJob.id === 'undefined'){
            //     Job.create($scope.mJob).then(function(result) {
            //            $scope.getAllJob();
            //            $scope.mJob = null;
            //         }); 
            //   } 
            //   else{
            //     Job.update($scope.mJob).then(function(result) {
            //         $scope.getAllJob();
            //            $scope.mJob = null;
            //         }); 
            //   }
            // }
            // $scope.toggle();  
        }
        $scope.doSave = function(job) {
            Job.create(job).then(function(result) {
                $scope.getAllJob();
                $scope.mJob = null;
                $scope.showModal_addEditJob = false;
            });
        }
        $scope.doEdit = function(job) {
            Job.update(job).then(function(result) {
                $scope.getAllJob();
                $scope.mJob = null;
                $scope.showModal_addEditJob = false;
            });
        }
        $scope.doDelete = function() {
            Job.remove($scope.mJob.id).then(function(result) {
                $scope.mJob = null;
                $scope.showModal_deleteJob = false;
                $scope.getAllJob();
            });
        }

        //   $scope.doCancel = function(){
        //     console.log($scope.mJob);  
        //     $scope.toggle();
        //   }

        //   $scope.actDelete = function(){      
        //     $scope.btnText =  "Yes";
        //     $scope.mode = "delete";
        //     $scope.toggle();
        //   }

        //   $scope.actEdit = function(){   
        //     console.log($scope.mJob);   
        //     $scope.btnText =  "Update";
        //     $scope.mode = "edit";
        //     $scope.toggle();
        //   }

        //   $scope.actNew = function(){
        //     //$scope.myForm.$setPristine(true);
        //     //$scope.myForm.$setUntouched();
        //     $scope.mJob={};
        //     $scope.gridApi.selection.clearSelectedRows();
        //     $scope.btnText =  "Save";
        //     $scope.mode = "new";
        //     $scope.toggle();   
        //   }

        // $scope.side = 'front';
        // $scope.toggle = function() {
        //   $scope.side = $scope.side == 'back' ? 'front' : 'back';
        // }

        $scope.getTableHeight = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 40; // your header height
            var filterHeight = 40; // your filter height
            var pageSize = $scope.gridOptions.paginationPageSize;
            if ($scope.gridOptions.paginationPageSize > $scope.gridOptions.data.length) {
                pageSize = $scope.gridOptions.data.length;
            }
            return {
                height: (pageSize * rowHeight + headerHeight) + 39 + "px"
            };
        };

        $scope.gridOptions = {
            enableSorting: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            multiSelect: false,
            paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100],
            paginationPageSize: 15,

            columnDefs: [{
                name: 'id',
                field: 'id'
            }, {
                name: 'title',
                field: 'title'
            }, {
                name: 'description',
                field: 'description'
            }]
        };

        $scope.gridOptions.onRegisterApi = function(gridApi) {
            // set gridApi on scope
            $scope.gridApi = gridApi;

            $scope.gridApi.selection.on.rowSelectionChanged($scope,
                function(row) {
                    $scope.mJob = row.entity;
                    console.log('selected row=>', row.entity);
                });
            if ($scope.gridApi.selection.selectRow) {
                $scope.gridApi.selection.selectRow($scope.gridOptions.data[0]);
            }
        };

        var vm = this;
        vm.mode = 'delete';

        vm.fields = [{
            type: 'input',
            key: 'title',
            templateOptions: {
                required: true,
                label: 'Title',
                placeholder: 'Title',
                //width: 'three'  //optional
            },
            expressionProperties: {
                "templateOptions.disabled": function($viewValue, $modelValue, $scope) {
                    console.log('exp $scope', $scope);
                    return ($scope.mode == 'delete');
                }
            }
        }, {
            type: 'input',
            key: 'description',
            templateOptions: {
                required: true,
                label: 'Description',
                placeholder: 'Description',
                width: 'four' //optional
            }
        }];

        $scope.getAllJob();

    });
