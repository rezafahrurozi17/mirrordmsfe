angular.module('app')
  .factory('Job', function($http) {
    return {
      getJob: function() { 
        return $http.get('/job');
      },
      create: function(newJob) {
        return $http.post('/job/create/', {title: newJob.title,description: newJob.description});
      },
      remove: function(id) {
        console.log("delete job=>",id);
        return $http.post('/job/delete/',{id: id});
      },
      update: function(tag){
        // console.log('call update', id+','+title+', '+parentid);
        return $http.post('/job/update/', {id:tag.id , title: tag.title, description: tag.description});
      }
    }
  });

