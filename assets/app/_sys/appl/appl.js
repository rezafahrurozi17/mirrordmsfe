angular.module('app')
  .factory('Appl', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var r=$http.get('/sys/appl');
        //console.log('r=>',r);
        return r;
      },
      create: function(appl) {
        return $http.post('/sys/appl/create', {
                                            name: appl.name,
                                            desc: appl.desc});
      },
      update: function(appl){
        return $http.post('/sys/role/update', {
                                            id: appl.id,
                                            name: appl.name,
                                            desc: appl.desc});
      },
      delete: function(id) {
        return $http.post('/sys/appl/delete',{id:id});
      },
    }
  });