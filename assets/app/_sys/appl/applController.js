angular.module('app')
    .controller('ApplController', function($scope, $http, CurrentUser, Appl) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAppl = null; //Model

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        $scope.loading=true;
        Appl.getData().then(function(result) {
            console.log('appl=>', result.data);
            $scope.grid.data = result.data;
            $scope.loading=false;
        });
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100],
        // paginationPageSize: 15,

        columnDefs: [
            { name:'id',    field:'id', width:'7%' },
            { name:'name', field:'name' },
            { name:'desc',  field: 'desc' }
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //----------------------------------
    var vm=this;
    vm.model = $scope.mAppl;
    vm.fields = [
                    {
                        className: 'input-icon-right',
                        type: 'input',
                        key: 'name',
                        templateOptions: {
                                            type:'text',
                                            label: 'Name',
                                            placeholder: 'Name',
                                            required: true,
                                            minlength:4,
                                            //maxlength:7,
                                            icon:'fa fa-child'
                        }
                    },
                    {
                        className: 'input-icon-right',
                        type: 'input',
                        key: 'desc',
                        templateOptions: {
                                            type:'text',
                                            label: 'Description',
                                            placeholder: 'Description',
                                            required: true,
                                            icon:'glyphicon glyphicon-pencil'
                        }
                    },
    ];
});
