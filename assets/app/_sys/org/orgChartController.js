angular.module('app')
  .controller('OrgChartController', function($scope, $http, $sce,$filter,$timeout,$window, CurrentUser, OrgType, OrgChart) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mOrg = null; //Model
    $scope.selOrg = {selected:[]}; //Array of Selected Model

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.dataOrgType = [];
    $scope.getData = function() {
        OrgType.getData().then(
            function(res){
                console.log("orgtype=>",res.data.Result);
                $scope.dataOrgType = res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
        OrgChart.getData().then(
            function(res){
                gridData = [];
                console.log("org=>",res.data.Result);
                $scope.grid.data = FlattenAndSetLevel(angular.copy(res.data.Result),0);
                //$scope.grid.data = res.data.result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    function FlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].Child.length>0){
                FlattenAndSetLevel(node[i].Child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    // $scope.selectRole = function(rows){
    //     console.log("onSelectRows=>",rows);
    //     $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    // }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 15,
        columnDefs: [
            { name:'id',   field:'Id', width:'7%'}, //, visible:false
            { name:'code', field:'Code' },
            { name:'name', field:'Name' },
            //{ name:'orgtype', field:'orgTypeId' },
            { name:'type', field:'OrgTypeId' , cellFilter:'mapOrgType:row.entity.OrgTypeId:grid.appScope.$parent.dataOrgType'},
        ]
    };
})
.filter('mapOrgType', function() {
        return function(input,id,arr) {
            //console.log("input=>",input,id,arr);
            if (!input){
                return '';
            } else {
                var res = _.find(arr,{Id:id});
                //console.log('res=>',res);
                return res.Name;
            }
        };
});