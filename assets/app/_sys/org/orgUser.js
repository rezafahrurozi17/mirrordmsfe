angular.module('app')
  .factory('OrgUser', function($http) {
    return {
      add: function(orgId,roleId, user){
        return $http.post('/sys/orguser/add',{orgid:orgId, roleid:roleId, user: user});
      },
      delete: function(id, userId) {
        return $http.post('/sys/orguser/delete',{id:id, userid: userId});
      },
      getOrgUserbyUserId: function(userId) {
        return $http.post('/sys/orguser/getOrgUserbyUserId',{userid:userId});
      },
      getOrgUserbyOrgRole: function(orgId,roleId) {
        return $http.post('/sys/orguser/getOrgUserbyOrgRole',{orgid:orgId,roleid:roleId});
      },
      assignUser: function(orgId,roleId, userIds) {
        return $http.post('/sys/orguser/assignUser',{orgid:orgId,roleid:roleId, userid: userIds});
      },
      removeUser: function(Ids, userIds,deleteUser) {
        console.log("ids=>",Ids);
        return $http.post('/sys/orguser/removeUser',{id:Ids, userid: userIds,deleteUser:deleteUser});
      },
    }
  });