angular.module('app')
  .factory('OrgType', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fwdms/OrgType');
        //console.log('res=>',res);
        return res;
      },
      create: function(m) {
        return $http.post('/api/fwdms/OrgType', [{
                                            name: m.Name}]);
      },
      update: function(m){
        return $http.put('/api/fwdms/OrgType', [{
                                            id: m.Id,
                                            name: m.Name}]);
      },
      delete: function(id) {
        return $http.delete('/api/fwdms/OrgType',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });