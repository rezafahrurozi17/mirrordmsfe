angular.module('app')
  .controller('OrgTypeController', function($scope, $http, $sce,$filter,$timeout,$window, CurrentUser,OrgType) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mOrgType = null; //Model
    $scope.selOrgType = {selected:[]}; //Array of Selected Model

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        OrgType.getData().then(
            function(res){
                gridData = [];
                console.log("orgtypes=>",res.data.Result);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 15,
        columnDefs: [
            { name:'id',   field:'Id', width:'7%'}, //, visible:false
            // { name:'code', field:'code' },
            { name:'name', field:'Name' }
        ]
    };
});