angular.module('app')
  .controller('OrgChartControllerx', function($scope, $http, $sce,$filter,$timeout,$window, CurrentUser, User, Role, OrgUser, OrgChart, Auth) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
                    $scope.getOrgData();
                    $scope.getRoleData();
                    console.log("content Loaded=>");
                    resizeLayout();
    });
    $scope.$on('tabChange', function(){
        resizeLayout();
    });
    $timeout(function () {
        // Set height initially
        resizeLayout();
    },0);
    var resizeLayout=function() {
        $timeout(function(){
          var main= $("#header").outerHeight()
                      + $("#page-footer").outerHeight()
                      + $(".nav.nav-tabs").outerHeight()
                      + $(".bs-navbar").outerHeight();
          $("#layoutContainer_org").height($(window).height()-main-10); //- $("#footerDiv").height() - $("#menuDiv").height()
        },0);
    };
    angular.element($window).bind('resize', resizeLayout);
    $scope.$on('$destroy', function() {
        angular.element($window).unbind('resize', resizeLayout);
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    $scope.sm_showOrg=false;
    $scope.sm_modeOrg=false;
    $scope.sm_actOrg=null;
    $scope.mUserEmpty={
                        id:null,
                        name:null,
                        email:null,
                        password:null,
                        phone:null,
                        address:null
                      };
    $scope.mOrg = null;  //OrgChat Model
    $scope.mOrgCopy = null;  //OrgChat Model
    $scope.orgData = []; //OrgChart Collection
    $scope.ctlOrg = {}; //OrgChart Tree Controller
    $scope.selOrg = null;  //Selected Org

    $scope.roleData = []; //Role Collection
    $scope.selRole = null; //Selected Role

    $scope.sm_showUser=false;
    $scope.sm_showUserList=false;
    $scope.sm_showRemoveUser=false;
    $scope.sm_modeUser=false;
    $scope.sm_actUser=null;

    $scope.mUser = null; //User Model
    $scope.userData = []; //User Collection
    $scope.selUser = null //Selected User

    // get Role
    $scope.getRoleData = function(){
      Role.getData().then(function(result) {
          $scope.roleData = result.data;
          //console.log("roleData=>",result.data);
          //$scope.ctlOrg.expand_all();
    });

    $scope.selectRole = function(role){
          //console.log("sel role->",role);
          $scope.selRole = role;
          $scope.getOrgUserbyOrgRole(role);
    }
}
    // ---------------------------------------------------------------------------------
    // --------------------------- Organization Chart  ---------------------------------
    // ---------------------------------------------------------------------------------

    //----------------------------------
    // Button Action
    //----------------------------------
    $scope.actNewOrg = function(){
         // $scope.mOrg = {
         //                  pid:$scope.selOrg.id,
         //                  id:null,
         //                  title:null,
         //                  desc:null
         //               };
         $scope.mOrg={};
         $scope.mOrg.pid = $scope.selOrg.id;
         $scope.mOrgCopy = angular.copy($scope.mOrg);
         $scope.sm_modeOrg='new';
         $scope.sm_actOrg = $scope.doSaveOrg;
         $scope.sm_showOrg=true;
    }

    $scope.actEditOrg = function(){
         $scope.mOrg = angular.copy($scope.selOrg);
         $scope.mOrgCopy = angular.copy($scope.mOrg);
         $scope.sm_modeOrg='edit';
         $scope.sm_actOrg = $scope.doSaveOrg;
         $scope.sm_showOrg=true;
    }

    $scope.actDelOrg = function(){
         $scope.mOrg = angular.copy($scope.selOrg);
         $scope.mOrgCopy = angular.copy($scope.mOrg);
         $scope.sm_modeOrg='del';
         $scope.sm_actOrg = $scope.doDeleteOrg;
         $scope.sm_showOrg=true;
    }
    //----------------------------------
    // Backend Operation
    //----------------------------------
    // $scope.getOrgData = function(){
    //     console.log('getorgdata',$scope.user);
    //     OrgChart.getData($scope.user).then(function(result) {
    //       $scope.orgData = result.data;
    //       $scope.ctlOrg.expand_all();
    //       console.log("orgChartData all=>",result.data);
    //     });
    // }
    $scope.getOrgData = function(){
        console.log('getorgdata',$scope.user);
        OrgChart.getData().then(function(result) {
        //OrgChart.getDataByRoleUser($scope.user).then(function(result) {
          $scope.orgData = result.data;
          console.log("orgData=>",result.data);
          // $scope.ctlOrg.expand_all();
        });
    }
    $scope.doSaveOrg = function(org) {
        $scope.sm_showOrg=false;
        if (org.id) {
            OrgChart.update(org).then(function(result) {
                $scope.doRefreshOrg();
             });
        } else {
            //console.log("create org=>",org);
            OrgChart.create(org).then(function(result) {
                $scope.doRefreshOrg();
            });
        }
    }
    $scope.doDeleteOrg = function(org){
        $scope.sm_showOrg=false;
        OrgChart.delete(org).then(function(result) {
            $scope.doRefreshOrg();
        });
    }
    $scope.doRefreshOrg = function(){
        resetForm($scope.OrgForm);
        $scope.mOrg = null;
        $scope.selOrg = null;
        $scope.getOrgData();
        $scope.getOrgUserbyOrgRole($scope.selRole);
    }
    $scope.doCancelOrg = function(){
        $scope.sm_showOrg=false;
        resetForm($scope.OrgForm);
        $scope.mOrg = null;
    }
    $scope.showSelOrg = function(org){
      //console.log('selected org:',org);
        $scope.selOrg = org;
        if($scope.selRole){
          $scope.getOrgUserbyOrgRole($scope.selRole);
        }
    }

//------------------------------------------------------
// ------------------ OrgUser List ---------------------
//------------------------------------------------------

    //----------------------------------
    // Button Action
    //----------------------------------
    $scope.actAssignUser = function(){
        $scope.getUserData();
        $scope.sm_showUserList = true;
    }

    $scope.actAddUser = function(){
        //console.log("actAddUser=>");
        $scope.mUser = {};
        $scope.selectedRow = {};
        $scope.sm_modeUser='new';
        $scope.sm_actUser = $scope.doSaveUser;
        //resetFormValid($scope.UserForm);
        //console.log('UserForm=>',$scope.UserForm);
        $scope.sm_showUser=true;
    }

    $scope.actEditUser = function(){
        var selectedRow = $scope.gridApiOrgUser.selection.getSelectedRows();
        //console.log("selectRow doEditUser", selectedRow);
        $scope.mUser = angular.copy(selectedRow[0]);
        $scope.sm_modeUser='edit';
        $scope.sm_actUser = $scope.doSaveUser;
        $scope.sm_showUser=true;
    }

    $scope.actRemoveUser = function(){
        var selectedRow = $scope.gridApiOrgUser.selection.getSelectedRows();
        //console.log("selectRow doRemoveUser", selectedRow);
        $scope.mUser = angular.copy(selectedRow[0]);
        $scope.sm_showRemoveUser=true;
    }

    //----------------------------------
    // Backend Operation UserOrg
    //----------------------------------

    $scope.getOrgUserbyOrgRole = function(role){
        //console.log("selected org=>",$scope.selOrg);
        //console.log("selected role=>",role);

        if($scope.selOrg && role){
          OrgUser.getOrgUserbyOrgRole($scope.selOrg.id, role.id).then(function(result) {
              //console.log("userData=>",JSON.stringify(result.data));
              $scope.gridOrgUser.data = result.data;
          });
        }else{
          $scope.gridOrgUser.data=[];
        }
    }
    $scope.doAssignUser = function(){
        $scope.sm_showUserList = false;
        var arrSelected = [];
        var selectedRow = $scope.gridApiUser.selection.getSelectedRows();
        //console.log("selectedRow:",selectedRow);
        for (var i = selectedRow.length - 1; i >= 0; i--) {
          arrSelected.push(selectedRow[i].id);
        };

        //console.log("selectedRole=>",$scope.selRole);
        //console.log("org=>",$scope.selOrg.id);
        //console.log("di chek=>",arrSelected);

        OrgUser.assignUser($scope.selOrg.id,$scope.selRole.id, arrSelected).then(function(result) {
          // $scope.gridUser.data = result.data;
          // console.log("userData=>",JSON.stringify(result.data));
          $scope.getOrgUserbyOrgRole($scope.selRole);
        });
    }
    $scope.doRemoveUser = function(){
        $scope.sm_showRemoveUser=false;
        //console.log("delete user table=>",$scope.chkDelete);
        var arrSelectedId = [];
        var arrSelectedUserId = [];
        var selectedRow = $scope.gridApiOrgUser.selection.getSelectedRows();
        console.log("selectedRow->",selectedRow);
        for (var i = selectedRow.length - 1; i >= 0; i--) {
          arrSelectedId.push(selectedRow[i].id);
          arrSelectedUserId.push(selectedRow[i].userId);
        };
        console.log("arrSelectedId=>",arrSelectedId);
        OrgUser.removeUser(arrSelectedId, arrSelectedUserId,$scope.chkDelete)
        .then(function(result) {
          $scope.getOrgUserbyOrgRole($scope.selRole);
        });
    }
    //----------------------------------
    // Backend Operation User List
    //----------------------------------

    $scope.getUserData = function(){
        User.getData().then(function(result) {
            $scope.gridUser.data = result.data;
            $scope.userData = result.data;
            //console.log("userData=>",JSON.stringify(result.data));
            $scope.getAvailableUser();
        });
    }
    $scope.getAvailableUser = function() {
        $scope.userIdList = [];
        for (var i = $scope.gridOrgUser.data.length - 1; i >= 0; i--) {
          //console.log($scope.gridOrgUser.data[i].userId);
          var searchValue = {'id': $scope.gridOrgUser.data[i].userId};
          var index = _.findIndex( $scope.gridUser.data, searchValue);
          //console.log("result=>",index);
          $scope.gridUser.data.splice(index, 1);
        }
    }
    $scope.doSaveUser = function(user) {
        if(user.userId) {
            user.id = user.userId;
            //console.log('update user:',user);
            User.update(user).then(function(result) {
                // $scope.getUserData($scope.selectedOrg.id);
                $scope.getOrgUserbyOrgRole($scope.selRole);
                $scope.doRefreshUser();
            });
        }else{
            //console.log('add user:',user);
            User.create(user).then(function(result) {
                $scope.getUserData();
                $scope.doRefreshUser();
            });
        }
    }
    $scope.doRefreshUser = function(){
        //$scope.addEditUserForm.$rollbackViewValue();
        $scope.UserForm.$setUntouched();
        $scope.UserForm.$setPristine();
        $scope.mUser = {};
        $scope.sm_showUser=false;
    }
    $scope.doCancelUser = function(){
        //console.log('cancel user');
        //$scope.mOrg = angular.copy($scope.mOrgCopy);
        // $scope.UserForm.$setUntouched();
        // $scope.UserForm.$setPristine();
        // $scope.mUser = {};
        resetForm($scope.UserForm);

        //console.log('user',$scope.mUser);
        $scope.sm_showUser=false;
    }
    $scope.setChkDelete = function(chk){
        $scope.chkDelete = chk;
        //console.log("chkDelete",$scope.chkDelete);
    }

    $scope.selectRow = function(row){
        $scope.selectedRow = row;
        //console.log(row);
    }

    $scope.doGeneratePass = function(){
        $scope.mUser.password = Math.random().toString(36).slice(-8);
    }

    //------------------------------------------------------
    // ------------------- Grids--- ------------------------
    //------------------------------------------------------

    //grid user list
    $scope.getTableHeightUser = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 40; // your header height
           var filterHeight = 40; // your filter height
           var pageSize = $scope.gridUser.paginationPageSize;
           if($scope.gridUser.paginationPageSize > $scope.gridUser.data.length){
                pageSize = $scope.gridUser.data.length;
           }
           if(pageSize<4){
              pageSize=3;
           }
           return {
              height: (pageSize * rowHeight + headerHeight)+42 + "px"
           };
    };

    $scope.gridUser = {
            enableSorting: true,
            enableRowSelection: true,
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5,10,25,50,75,100],
            paginationPageSize: 10,
            enableColumnMenus: false,

            columnDefs: [
              // { name:'id', field: 'id',width:'7%' },
              { displayName:'User ID',name:'user id',  field: 'username'},
              { name:'name',  field: 'name'},
              { name:'email', field: 'email'}
            ]
    };

    $scope.gridUser.onRegisterApi = function(gridApi) {
        // set gridApi on scope
        $scope.gridApiUser = gridApi;

        $scope.gridApiUser.selection.on.rowSelectionChanged($scope,
                function(row) {
                    // $scope.mJob = row.entity;
                    // $scope.selectedRow = row.entity;;
                    // $scope.newTag = row.entity;;
                    //console.log('selected row user list=>',row);
                });
            if($scope.gridApiUser.selection.selectRow){
              $scope.gridApiUser.selection.selectRow($scope.gridUser.data[0]);
            }
        }

    $scope.selectAll = function() {
        $scope.gridApiUser.selection.selectAllRows();
    };

    $scope.clearAll = function() {
        $scope.gridApiUser.selection.clearSelectedRows();
    };

    //-----------------------------------

    $scope.getTableHeight = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 40; // your header height
           var filterHeight = 40; // your filter height
           var pageSize = $scope.gridOrgUser.paginationPageSize;
           if($scope.gridOrgUser.paginationPageSize > $scope.gridOrgUser.data.length){
                pageSize = $scope.gridOrgUser.data.length;
           }
           if(pageSize<4){
              pageSize=3;
           }
           return {
              height: (pageSize * rowHeight + headerHeight)+42 + "px"
           };
    };

    $scope.gridOrgUser = {
            enableSorting: true,
            enableRowSelection: true,
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            multiSelect: true,
            paginationPageSizes: [5,10,25,50,75,100],
            paginationPageSize: 10,
            enableColumnMenus: false,

            columnDefs: [
              //{ name:'id', field: 'id',width:'7%' },
              { displayName:'User ID',name:'user id',  field: 'username'},
              { name:'name',  field: 'name'},
              { name:'email', field: 'email'}
            ]
    };

    $scope.gridOrgUser.onRegisterApi = function(gridApi) {
        // set gridApi on scope
        $scope.gridApiOrgUser = gridApi;

        $scope.gridApiOrgUser.selection.on.rowSelectionChanged($scope,
                function(row) {
                    //$scope.mJob = row.entity;
                    $scope.selectedRow = row.entity;
                    //console.log('selected row=>',row.entity);
                });
            if($scope.gridApiOrgUser.selection.selectRow){
              $scope.gridApiOrgUser.selection.selectRow($scope.gridOrgUser.data[0]);
            }
    };

});