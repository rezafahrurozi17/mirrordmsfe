angular.module('app')
  .factory('OrgChart', function($http, bsTransTree, CurrentUser) {
    if(_myCache==undefined)
      var _myCache = [];
    return {
      getData: function(user) {
        console.log("org user=>",user);
         return $http.get('/api/fwdms/Organization/Tree');
      },
      getDataChildOnly: function(user) {
        console.log("org user=>",user);
         return $http.get('/api/fwdms/Organization/Tree?childOnly=0');
      },
      getDataTree: function(){
        var res = null;
        if(_myCache.length==0){
          var user = CurrentUser.user();
          console.log("user=>",user);
          var outletId = (user.id==1 || user.id==47?-1:user.orgId);
          if(outletId>1000) outletId-=1000;
          res=$http.get('/images/upload/outlet2.json');
          // res=$http.get('/api/param/Outlets/GetTreeOutlets?outletId='+outletId);
          res.then(function(res){
            console.log("outlet=>",res.data.Result);
            var treeIn = bsTransTree.translate(res.data.Result, {
              id:['GroupDealerId','OutletId'],
              code:['GroupDealerCode','OutletCode'],
              title:['GroupDealerName','Name'],
              child:{isChild:true, fld:['listOutlet']}
            });
            _myCache = treeIn;
          });
        }else{
          res = {
            then:function(cbOk,cbErr){
              cbOk({data:{Result:_myCache}});
            }
          }
        }
        return res;
      },
      getDataByRoleUser: function(user) {
        return $http.post('/sys/orgchart/getOrgByRoleUser',{roleid: user.roleId,userid: user.id });
      },
      create: function(org) {
        console.log("orgChart factory create=>",org);
        return $http.post('/api/fwdms/Organization', [{
                                                    Code: org.Code,
                                                    Name: org.Name,
                                                    OrgTypeId: org.OrgTypeId,
                                                    StatusCode: true,
                                                    ParentId: org.ParentId
                                                  }]);
      },
      update: function(org){
        return $http.put('/api/fwdms/Organization', [{
                                                    Id: org.Id,
                                                    Code: org.Code,
                                                    Name: org.Name,
                                                    OrgTypeId: org.OrgTypeId,
                                                    StatusCode: true,
                                                    ParentId: org.ParentId
                                                 }]);
      },
      delete: function(id) {
        return $http.delete('/api/fwdms/Organization',{data:id,headers: {'Content-Type': 'application/json'}});
      },

    }
  });

// angular.module('app')
//   .factory('OrgChart', function($http, bsTransTree) {
//     if(_myCache==undefined)
//       var _myCache = [];
//       var _myCache2 = [];
//     return {
//       getData: function(user) {
//          //console.log("org user=>",user);
//          return $http.post('/sys/dealer/getData');
//        },
//       // getData: function(user) {
//       //   //console.log("org user=>",user);
//       //   res=$http.get('/images/upload/org.json');

//       //    return $http.post('/sys/orgchart/getOrgAll',{roleid: user.roleId });
//       //  },
//       // getData: function(){
//       //   var res = null;
//       //   if(_myCache2.length==0){
//       //     //res=$http.get('/api/param/Outlets/GetTreeOutlets?orgId=8');
//       //     res=$http.get('/images/upload/org_be.json');
//       //     res.then(function(res){
//       //       //res.data.Result.splice(0,30);
//       //       //console.log("org=>",JSON.stringify(res.data.Result));
//       //       var treeIn = bsTransTree.translate(res.data.Result, {
//       //         id:['GroupDealerId','OutletId'],
//       //         code:['GroupDealerCode','OutletCode'],
//       //         title:['GroupDealerName','Name'],
//       //         child:{isChild:true, fld:['listOutlet']}
//       //       });
//       //       _myCache2 = treeIn;
//       //     });
//       //   }else{
//       //     res = {
//       //       then:function(cbOk,cbErr){
//       //         cbOk({data:{Result:_myCache2}});
//       //       }
//       //     }
//       //   }
//       //   return res;
//       // },
//       getOrg: function(user) {
//         //console.log("org user=>",user);
//          return $http.post('/sys/orgchart/getOrg');
//        },
//       getDataTree: function(){
//         var res = null;
//         if(_myCache.length==0){
//           res=$http.get('/api/param/Outlets/GetTreeOutlets?orgId=8');
//           res.then(function(res){
//             //res.data.Result.splice(0,30);
//             //var res2=angular.copy(res);
//             //console.log("org from be=>",JSON.stringify(res2.data.Result));
//             var treeIn = bsTransTree.translate(res.data.Result, {
//               id:['GroupDealerId','OutletId'],
//               code:['GroupDealerCode','OutletCode'],
//               title:['GroupDealerName','Name'],
//               child:{isChild:true, fld:['listOutlet']}
//             });
//             _myCache = treeIn;
//           });
//         }else{
//           res = {
//             then:function(cbOk,cbErr){
//               cbOk({data:{Result:_myCache}});
//             }
//           }
//         }
//         return res;
//       },
//       getDataByRoleUser: function(user) {
//         return $http.post('/sys/orgchart/getOrgByRoleUser',{roleid: user.roleId,userid: user.id });
//       },
//       create: function(org) {
//         console.log("orgChart factory create=>",org);
//         return $http.post('/sys/orgchart/create/', {title: org.title,pid: org.pid });
//       },
//       delete: function(org) {
//         return $http.post('/sys/orgchart/delete/',{id: org.id,pid: org.pid});
//       },
//       update: function(org){
//         return $http.post('/sys/orgchart/update/', {id:org.id , title: org.title, pid: org.pid});
//       }
//     }
//   });


