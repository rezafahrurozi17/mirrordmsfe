angular.module('app')
    .controller('MenuListController', function($scope, $http, CurrentUser, Menu,$timeout,bsTransTree) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mMenu = null; //Model
    $scope.xMenu={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        Menu.getMenuList()
        .then(
            function(res){
                gridData = [];
                //
                //$scope.grid.data = res.data.result;
                console.log("menu=>",res.data.result);
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                var resx = bsTransTree.createTree(res.data.Result,'Id','ParentId');
                console.log("res=>>",resx);
                $scope.grid.data = roleFlattenAndSetLevel(angular.copy(resx),0);
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'Id', width:'7%', visible:false },
            { name:'title', field:'Name' },
            { name:'sref',  field: 'Sref' },
            { name:'sequence',  field: 'Sequence' },
            { name:'byteAvailable',  field: 'ByteAvailable' },
            { name:'icon',  field: 'Icon' },
        ]
    };
});
