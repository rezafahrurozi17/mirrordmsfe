angular.module('MenuModule')
  .controller('MenuManagerController', function($scope, $http, $sce,$filter,$timeout,CurrentUser,RightsByte,Menu) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
                    $scope.getData();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    $scope.sm_show=false;
    $scope.sm_mode='idle';
    $scope.sm_act=null;
    $scope.mData = {};  //Model
    $scope.mDataCopy = null;  //Model Copy
    $scope.cData = []; //Collection


    var MAX_RB = 10;
    RightsByte.getData().then(function(result){
        $scope.rbData = result.data;
        MAX_RB = result.data.length;
    })

    //----------------------------------
    // Backend Operation
    //----------------------------------
    $scope.getData = function(){
        var expandedNodes=[];
        function checkRbyte(rb,b){
            var p = rb & Math.pow(2,b);
            return (   ( p == Math.pow(2,b)) );
        }
        function expandAll(n,p){
          for(var i=0;i<n.length;i++){
                n[i].bitArr=[];
                for(var j=0;j<MAX_RB;j++){
                    var b=checkRbyte(n[i].byte_available,j);
                    n[i].bitArr.push( b );
                }
                //expandedNodes.push(n[i]);
                var xp=null;
                if(p!=null){
                    var xp=angular.copy(p);
                    xp.child=null;
                    n[i].parent = angular.copy(xp);
                }else{
                    n[i].parent = null;
                }
                if(n[i].child){
                  expandAll(n[i].child,n[i]);
                }
          }
        };

        Menu.getMenuList().then(function(result) {
          var cData = angular.copy(result.data);
          expandAll(cData,null);
          $scope.cData = angular.copy(cData);
          console.log("cData=>",$scope.cData);
          //$scope.expandedNodes = expandedNodes;
        });
    }
    //--------------------------------------
    $scope.checkRB = function(available,rb){
        var p=available&rb;
        return(p==rb);
    }
    $scope.chgRbyte = function(menu){
        var res=0;
        for(var i=0;i<MAX_RB;i++){
           var b = menu.bitArr[i]?1:0;
           if (b==1){
              res = res + Math.pow(2,i);
           }
        }
        menu.byte_available = res;
    }
    //----------------------------------
    // Form Fields Setup
    //----------------------------------
    // $scope.treeOptions = {
    //   nodeChildren: "child",
    //   dirSelectable: true,
    //   allowDeselect: false,
    //   multiSelection: false,
    //   injectClasses: {
    //       ul: "nav nav-list nav-pills nav-stacked bs-tree",
    //       li: "bs-tree-row",
    //       //liSelected: "h2",
    //       iExpanded: "glyphicon glyphicon-minus-sign",
    //       iCollapsed: "glyphicon glyphicon-plus-sign",
    //       iLeaf: "fa fa-fw fa-dot-circle-o",
    //       label: "a6",
    //       labelSelected: "a8"
    //   }
    // }

    var vm=this;
    vm.fields = [
        {
            className:'row',
            fieldGroup: [{
                            className: 'col-xs-6',
                            type: 'input',
                            key: 'title',
                            templateOptions: {
                                type:'text',
                                label: 'Menu Title',
                                placeholder: 'Menu Title',
                                required: true,
                                icon:'fa fa-bars'
                            }
                        },
                        {
                            className: 'col-xs-6',
                            type: 'input',
                            key: 'description',
                            templateOptions: {
                                type:'text',
                                label: 'Description',
                                placeholder: 'Description',
                                icon:'fa fa-pencil'
                            }
                        }
            ]
        },
        {
            className:'row',
            fieldGroup: [{
                            className: 'col-xs-4',
                            type: 'input',
                            key: 'sref',
                            templateOptions: {
                                type:'text',
                                label: 'Menu URL (s-ref)',
                                placeholder: 'Menu URL (s-ref)',
                                icon:'fa fa-link'
                            }
                        },
                        {
                            className: 'col-xs-4',
                            type: 'input',
                            key: 'icon',
                            templateOptions: {
                                type:'text',
                                label: 'Icon',
                                placeholder: 'Icon',
                                icon:'fa fa-fonticons'
                            }
                        },
                        {
                            className: 'col-xs-4',
                            type: 'input',
                            key: 'seq',
                            templateOptions: {
                                type:'number',
                                label: 'Sequence',
                                placeholder: 'Sequence',
                                required: true,
                                icon:'fa fa-sort-numeric-asc',
                                min:'1'
                            }
                        }
            ]
        }
    ]


});