//agGrid.initialiseAgGridWithAngular1(angular);
angular.module('MenuModule')
  .controller('AccessMatrixController', function($scope, bsNotify, Auth,OrgChart,Role,RoleRights,RightsByte, CurrentUser,$state,$q,$interval,$timeout,$window) {
    $scope.$on('tabChange', function(){
        console.log("tabChange");
        $("#content").click();
        resizeLayout();
        //$scope.updateDisplay();
    })
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.auth = Auth;
    $scope.user = CurrentUser.user();

    $scope.orgData = []; //OrgChart Collection
    $scope.ctlOrg = {}; //OrgChart Tree Controller
    $scope.selOrg = null;  //Selected Org
    $scope.selRole = null;  //Selected Role

    $scope.orgid=null;
    $scope.roleid=null;

    //---- AccessMatrix
    var MAX_RB = 20;
    //-------------------------------------

    //-------------------------------------

    $scope.$on('tabChange', function(){
        resizeLayout();
    });
    $timeout(function () {
          // Set height initially
        resizeLayout();
        $scope.grid.api.sizeColumnsToFit();
        $scope.grid.api.refreshCells();
    },0);
    var resizeLayout=function() {
        $timeout(function(){
          var main=
                      $("#page-footer").outerHeight()
                      + $(".nav.nav-tabs").outerHeight()
                      + $(".ui.breadcrumb").outerHeight()
                      + $("#topnav_am").outerHeight() + 50;
          $("#layoutContainer_am").height($(window).height() - main ); //- $("#footerDiv").height() - $("#menuDiv").height()
          $("#agGrid_am").height($(window).height() - main - 40);
          $scope.grid.api.sizeColumnsToFit();
        },0);
    };
    angular.element($window).bind('resize', resizeLayout);
    $scope.$on('$destroy', function() {
        angular.element($window).unbind('resize', resizeLayout);
    });


    //----------------------------------
    //----- Org Tree
    //----------------------------------
    // $scope.getOrgChart = function(){
    //     console.log('getorgdata',$scope.user);
    //     OrgChart.getDataByRoleUser($scope.user).then(function(result) {
    //       console.log("orgData=>",result.data);
    //       $scope.orgData = result.data;
    //       $scope.ctlOrg.expand_all();
    //     });
    // }
    // $scope.getOrgChart = function(){
    //     console.log('getorgdata',$scope.user);
    //     OrgChart.getData().then(function(res) {
    //       console.log("orgData=>",res.data.Result);
    //       $scope.orgData = res.data.Result;
    //       $scope.ctlOrg.expand_all();
    //     });
    // }
    $scope.getOrgChart = function(){
        console.log('getorgdata',$scope.user);
        OrgChart.getDataChildOnly().then(function(res) {
          console.log("orgData=>",res.data.Result);
          $scope.orgData = res.data.Result;
          //$scope.ctlOrg.expand_all();
        });
    }
    $scope.getOrgChart();
    $scope.showSelOrg = function(org){
        //console.log('selected:',org,$scope.roleid);
        //console.log("selected org:",org);
        $scope.selOrg = org;
        if(org){
            $scope.orgid = org.Id;
            if($scope.roleid){
              $scope.getRoleRights($scope.orgid,$scope.roleid);
            }
        }else{
            $scope.orgid = null;
            $scope.grid.api.setRowData(null);
        }

    }
    //----------------------------------
    //----- Role
    //----------------------------------
    Role.getData().then(function(res){
        $scope.roleData = res.data.Result;
    })
    $scope.selectRole = function(role){
        console.log("sel role->",role);
        $scope.selRole = role;
        if(role){
            $scope.roleid = role.Id;
            if($scope.orgid){
                $scope.getRoleRights($scope.orgid,role.Id);
            }
        }else{
            $scope.roleid = null;
            $scope.grid.api.setRowData(null);
        }
    }
    //----------------------------------
    //----- Rights Byte
    //----------------------------------
    RightsByte.getData().then(function(res){
        console.log("rb1=>",res.data.Result);
        //$scope.rbData = result.data;
        $scope.rbData = res.data.Result.slice(1); //delete "VIEW"
        console.log("rb2=>",$scope.rbData);
    })
    //----------------------------------
    //----- Role Rights
    //----------------------------------
    $scope.getRoleRights = function(orgid,roleid){
        RoleRights.getData(orgid,roleid).then(function(res) {
            $scope.data = res.data.Result;
            rgetChild($scope.data,null);
            console.log("getRoleRights=>",$scope.data);
            //$scope.gridData = angular.copy($scope.data);
            $scope.grid.api.setRowData($scope.data);
            $scope.grid.api.sizeColumnsToFit();
        });
    }

    $scope.saveRoleRights = function(){
        $scope.ndata = [];
        ngetChild($scope.data);
        //console.log("ndata=>",$scope.ndata);

        RoleRights.save($scope.ndata,$scope.orgid,$scope.roleid)
            .then(function(res){
                bsNotify.show({
                    title: "Sukses",
                    content: "Update data matrix berhasil",
                    type: 'success'
                });
                $scope.getRoleRights($scope.orgid,$scope.roleid);
            },
            function(err) {
                bsNotify.show({
                    title: "Gagal",
                    content: "Update data matrix gagal",
                    type: 'danger'
                });
            });
    }

    $scope.refreshRoleRights = function(){
        $scope.getRoleRights($scope.orgid,$scope.roleid);
    }
    //----------------------------------
    //----- Role Rights Helpers
    //----------------------------------
    function rgetChild(node,parent){
    	for(var i=0;i<node.length;i++){
            //console.log("rgetChild node[i].child=>",node[i].child);
        	if(node[i].Child.length>0){

        		node[i].ByteEnable = checkAvailable(node[i]);
        		node[i].bitArr = [];
                node[i].visible = false;
                node[i].parent = parent;

                //console.log(node[i].title,node[i].rightsByte);
        		for(var j=0;j<MAX_RB;j++){
        		  var b=checkRbyte(node[i].ByteEnable,j);
        		  node[i].bitArr.push( b );
        		}
        		rgetChild(node[i].Child,node[i]);
        	}else{
        		node[i].ByteEnable = checkAvailable(node[i]);
        		node[i].bitArr = [];
                node[i].visible = false;
                node[i].parent = parent;
                //console.log(node[i].title,node[i].rightsByte);
        		for(var j=0;j<MAX_RB;j++){
                  var b=checkRbyte(node[i].ByteEnable,j);
        		  node[i].bitArr.push( b );
        		}
        	}
        }
    }

    function rsetChild(node,b){
        console.log("nodenya",node,b);
        for(var i=0;i<node.length;i++){
            //console.log("node[i].child=>",node[i].child);
            if(node[i].Child.length>0){

                node[i].ByteEnable = node[i].ByteAvailable;
                node[i].bitArr = [];
                //console.log(node[i].title,node[i].rightsByte);
                for(var j=0;j<MAX_RB;j++){
                  var bb=checkRbyte(node[i].ByteEnable,j);
                  node[i].bitArr.push( b && bb )
                }
                if(!b) node[i].ByteEnable = 0;
                rsetChild(node[i].Child,b)
            }else{
                node[i].ByteEnable = node[i].ByteAvailable;
                node[i].bitArr = [];
                //console.log(node[i].title,node[i].rightsByte);
                for(var j=0;j<MAX_RB;j++){
                  var bb=checkRbyte(node[i].ByteEnable,j);
                  node[i].bitArr.push( b && bb );
                }
                if(!b) node[i].ByteEnable = 0;
            }
        }
    }

    function rsetParent(data,node,b){
        //console.log("rsetParent:",node);
        for(var i=0;i<data.length;i++){
            var nd=data[i];
            //console.log("nd:",nd);
            if(nd.Id == node.ParentId){
                if(b){
                    nd.ByteEnable = 1;
                    nd.bitArr = [];
                    nd.bitArr.push(true);
                }else{
                    if(isAccess(nd)){
                        //console.log('aready access');
                    }else{
                        nd.ByteEnable = 0;
                        nd.bitArr = [];
                        nd.bitArr.push(false);
                    }
                }
                if(nd.ParentId==0){
                    break;
                } else {
                    rsetParent($scope.data,nd,b);
                }
            }else{
                if(nd.Child.length>0){
                    rsetParent(nd.Child,node,b);
                }else{
                    break;
                }
            }
        }
    }

    function isAccess(node){
        var res=false;
        for(var i=0;i<node.Child.length;i++){
            var nd=node.Child[i];
            //console.log("isAccess nd.rightsByte",nd.rightsByte);
            if(nd.ByteEnable & 1 == 1){
                res=true;
                break;
            }
        }
        //console.log("isAccess:",node,res);
        return res;
    }

    function ngetChild(node){
        console.log("===>",node);
        // // {Id:node[i].RrId,
        //                            OrgId:$scope.OrgId,
        //                            RoleId:$scope.RoleId,
        //                            MenuId:node[i].Id,
        //                            ParentId:node[i].ParentId,
        //                            ByteEnable:node[i].ByteEnable}
        for(var i=0;i<node.length;i++){
            if(node[i].Child.length>0){
                console.log("ngetChild=>",node[i]);
                $scope.ndata.push({Id:node[i].rrId,
                                   OrgId:$scope.OrgId,
                                   RoleId:$scope.RoleId,
                                   MenuId:node[i].Id,
                                   ParentId:node[i].ParentId,
                                   ByteEnable:node[i].ByteEnable});
                ngetChild(node[i].Child);
            }else{$scope.ndata.push({Id:node[i].rrId,
                                   OrgId:$scope.OrgId,
                                   RoleId:$scope.RoleId,
                                   MenuId:node[i].Id,
                                   ParentId:node[i].ParentId,
                                   ByteEnable:node[i].ByteEnable});

            }
        }
    }

    function checkRbyte(rb,b){
        var p = rb & Math.pow(2,b);
        return (   ( p == Math.pow(2,b)) );
    }

    function checkAvailable(node){
        var res=0;
        for(var i=0;i<MAX_RB;i++){
            //console.log("i:",i," title:",node.title,' rb:',Math.pow(2,i) & node.rightsByte,' av:',Math.pow(2,i) & node.byteAvailable);
            if( ( (Math.pow(2,i) & node.ByteEnable) <= (Math.pow(2,i) & node.ByteAvailable) &&
                  ( (Math.pow(2,i) & node.ByteEnable)>0 &&  (Math.pow(2,i) & node.ByteAvailable)>0 ) )
                ){
                res+=Math.pow(2,i);
            }
        }
        return res;
    }

    $scope.chgRbyte = function(node){
        console.log("rb change=>",node);
        var res=0;
        var tmpId = angular.copy(node.Id);
    	for(var i=0;i<MAX_RB;i++){
    	   var b = node.bitArr[i]?1:0;
    	   if (b==1){
    	      res = res + Math.pow(2,i);
    	   }
    	}
        if(res <= node.ByteAvailable){
            node.ByteEnable = res;
            if(node.Child.length>0){
                if(checkRbyte(res,0)){
                    console.log('access all',node);
                    // rsetChild(node.Child,true);
                }else{
                    //console.log('deny all');
                    // rsetChild(node.Child,false);
                }
            }
            console.log("parentId=>",node.ParentId);
            if(node.ParentId>0)
            {
                console.log("res=>",res,' checkRbyte:',checkRbyte(res,0));
                // rsetParent($scope.data,node,checkRbyte(res,0));
            }
            var updatedNodes = [];
            $scope.grid.api.forEachNode( function(node) {
                var data = node.data;
                updatedNodes.push(node);
            });
            var rowNode = $scope.grid.api.getRowNode(tmpId);
            rowNode.setDataValue('ByteEnable', res);

            var params = {
                force: false,
                suppressFlash: false,
                // rowNodes: updatedNodes,
                columns: ['ByteEnable']
              };
            $scope.grid.api.refreshCells(params);
        }
    }

    $scope.checkRB = function(available,rb){
        var p=available&rb;
        return(p==rb);
    }

    //------------------------------
    // --- Role Rights Grid
    //------------------------------
    var gridColumnDefs = [
        {headerName: "Title", field: "Name",cellRenderer: "group",width:300,maxWidth:300},
        {headerName: "RB", field: "ByteEnable",width:30,suppressMenu:true,sortable:true},
        {headerName: "RB Available", field: "ByteAvailable",width:30,suppressMenu:true,sortable:false},
        {headerName: "Access", field: "bitArr[0]",suppressMenu:true,sortable:false, width:40, cellStyle: {textAlign:'center'}, cellRenderer: accessCellRenderer},
        {headerName: "Action", field: "bitArr", cellRenderer:bitArrRenderer,suppressMenu:true,sortable:false}
    ];

    $scope.grid = {
        columnDefs: gridColumnDefs,
        enableSorting: true,
        enableFilter: true,
        enableColResize: true,
        //rowSelection: 'multiple',
        rowDeselection: true,
        angularCompileRows: true,

        getNodeChildDetails: function(node) {
            if (node.Child.length>0) {
                console.log("node=>",node);
                return {
                    group: true,
                    children: node.Child,
                    field: 'name',
                    key: node.Name,
                    expanded: true
                };
            } else {
                return null;
            }
        },
        getRowNodeId: function(data) {
            return data.Id;
        },

    };

    function accessCellRenderer(params) {
        //params.$scope.ageClicked = ageClicked;
        //console.log("params=>",params);
        var html='<div class="ui form">'+
                      '<div class="field">'+
                        '<div class="ui toggle checkbox" style="margin:0px;">'+
                            '<input type="checkbox" style="margin-left:0px;" ng-model="data.bitArr[0]" ng-change="chgRbyte(data)">'+
                            '</input><label></label>'+
                        '</div></div></div>';
        return html;
    }

    function bitArrRenderer(params) {
        var html='<div class="ui form" style="font-size:1.2rem">'+
                    '<div class="inline fields">'+
                        '<div class="field" ng-repeat="rb in rbData" ng-if="checkRB(data.ByteAvailable,rb.Code)">'+
                            '<div class="ui checkbox" >'+
                                '<input type="checkbox" tabindex="0" style="margin-left:0px;" ng-model="data.bitArr[$index+1]"'+
                                        ' ng-change="chgRbyte(data)">'+
                                '<label style="font-size:1.3em;">{{rb.Name}}</label>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                  '</div>'
        return html;
    }

    //Unused Yet
    $scope.treeOptions = {
        nodeChildren: "child",
        dirSelectable: true,
        allowDeselect: false,
        multiSelection: false,
        injectClasses: {
            ul: "nav nav-list nav-pills nav-stacked bs-tree",
            li: "bs-tree-row",
            //liSelected: "h2",
            iExpanded: "glyphicon glyphicon-minus-sign",
            iCollapsed: "glyphicon glyphicon-plus-sign",
            iLeaf: "fa fa-fw fa-dot-circle-o",
            label: "a6",
            labelSelected: "a8"
        }
    }
    // // $scope.ctlOrg = null;
    // $scope.clearSearch = function(){
    //             $scope.searchText = null;
    //             $scope.selOrg = null;
    //             $scope.ctlOrg.set_selected_branch(null);
    // }

  });