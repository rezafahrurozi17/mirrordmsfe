//agGrid.initialiseAgGridWithAngular1(angular);
angular.module('MenuModule')
  .controller('AccessMatrixController_v1', function($scope, Auth,OrgChart,Role,RoleRights,RightsByte, CurrentUser,$state,$q,$interval,$timeout) {

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.auth = Auth;
    $scope.user = CurrentUser.user();

    $scope.orgData = []; //OrgChart Collection
    $scope.ctlOrg = {}; //OrgChart Tree Controller
    $scope.selOrg = null;  //Selected Org

    $scope.orgid=null;
    $scope.roleid=null;

    $scope.l2data=[];
    var lastVisibleId=0;
    //-------------------------------------
    Role.getData().then(function(result){
        $scope.roleData = result.data;
    })
    RightsByte.getData().then(function(result){
        console.log("rb=>",result.data);
        //$scope.rbData = result.data;
        $scope.rbData = result.data.slice(1);
    })
    //-------------------------------------
    var resizeNodes=function() {
        $("#layoutContainer_am").height($(window).height() - 205); //- $("#footerDiv").height() - $("#menuDiv").height()
    };
    $timeout(function () {
        // Set height initially
        resizeNodes();
        $scope.updateDisplay();
        console.log('resizeNodes=>');
        // Add a watch on window.resize callback
        $(window).resize(function(){
            resizeNodes();
            $scope.updateDisplay();
        })
    });

    //----- Org Tree
    $scope.getOrgChart = function(){
        OrgChart.getDataByRoleUser($scope.user).then(function(result) {
          $scope.orgData = result.data;
          $scope.ctlOrg.expand_all();
        });
    }
    $scope.getOrgChart();
    $scope.showSelOrg = function(org){
        //console.log('selected:',org,$scope.roleid);
        console.log("selected:",org);
        $scope.searchText = org.title;
        $scope.selOrg = org;
        $scope.orgid = org.id;
        if($scope.roleid){
          $scope.getRoleRights($scope.orgid,$scope.roleid);
        }
    }
    // $scope.orgSelected = function(node){
    //     console.log("selected Org:",node);
    //     $scope.selectedOrgNode = node;
    //     $scope.orgid = node.id;
    //     $scope.getRoleRights($scope.orgid,$scope.roleid);
    //     console.log("klik tree");
    // };

    //---- AccessMatrix
    var MAX_RB = 10;

    function rgetChild(node,parent){
    	for(var i=0;i<node.length;i++){
            console.log("rgetChild node[i].child=>",node[i].child);
        	if(node[i].child!=undefined){

        		node[i].rights_byte = checkAvailable(node[i]);
        		node[i].bitArr = [];
                node[i].visible = false;
                node[i].parent = parent;

                //console.log(node[i].title,node[i].rights_byte);
        		for(var j=0;j<MAX_RB;j++){
        		  var b=checkRbyte(node[i].rights_byte,j);
        		  node[i].bitArr.push( b );
        		}
        		rgetChild(node[i].child,node[i]);
        	}else{
        		node[i].rights_byte = checkAvailable(node[i]);
        		node[i].bitArr = [];
                node[i].visible = false;
                node[i].parent = parent;
                //console.log(node[i].title,node[i].rights_byte);
        		for(var j=0;j<MAX_RB;j++){
                  var b=checkRbyte(node[i].rights_byte,j);
        		  node[i].bitArr.push( b );
        		}
        	}
        }
    }

    function rsetChild(node,b){
        for(var i=0;i<node.length;i++){
            console.log("node[i].child=>",node[i].child);
            if(node[i].child!=undefined){

                node[i].rights_byte = node[i].byte_available;
                node[i].bitArr = [];
                console.log(node[i].title,node[i].rights_byte);
                for(var j=0;j<MAX_RB;j++){
                  var bb=checkRbyte(node[i].rights_byte,j);
                  node[i].bitArr.push( b && bb )
                }
                if(!b) node[i].rights_byte = 0;
                rsetChild(node[i].child,b)
            }else{
                node[i].rights_byte = node[i].byte_available;
                node[i].bitArr = [];
                console.log(node[i].title,node[i].rights_byte);
                for(var j=0;j<MAX_RB;j++){
                  var bb=checkRbyte(node[i].rights_byte,j);
                  node[i].bitArr.push( b && bb );
                }
                if(!b) node[i].rights_byte = 0;
            }
        }
    }

    function rsetParent(data,node,b){
        //console.log("rsetParent:",node);
        for(var i=0;i<data.length;i++){
            var nd=data[i];
            //console.log("nd:",nd);
            if(nd.menu_id == node.pid){
                if(b){
                    nd.rights_byte = 1;
                    nd.bitArr = [];
                    nd.bitArr.push(true);
                }else{
                    if(isAccess(nd)){
                        //console.log('aready access');
                    }else{
                        nd.rights_byte = 0;
                        nd.bitArr = [];
                        nd.bitArr.push(false);
                    }
                }
                if(nd.pid==null){
                    break;
                } else {
                    rsetParent($scope.data,nd,b);
                }
            }else{
                if(nd.child!=undefined){
                    rsetParent(nd.child,node,b);
                }else{
                    break;
                }
            }
        }
    }

    function isAccess(node){
        var res=false;
        for(var i=0;i<node.child.length;i++){
            var nd=node.child[i];
            //console.log("isAccess nd.rights_byte",nd.rights_byte);
            if(nd.rights_byte & 1 == 1){
                res=true;
                break;
            }
        }
        //console.log("isAccess:",node,res);
        return res;
    }

    function ngetChild(node){
        for(var i=0;i<node.length;i++){
            if(node[i].child!=undefined){
                console.log("ngetChild=>",node[i]);
                $scope.ndata.push({rrid:node[i].rrid,
                                   //org_id:node[i].org_id,
                                   role_id:node[i].role_id,
                                   menu_id:node[i].menu_id,
                                   pid:node[i].pid,
                                   rights_byte:node[i].rights_byte});
                ngetChild(node[i].child);
            }else{$scope.ndata.push({rrid:node[i].rrid,
                                   //org_id:node[i].org_id,
                                   role_id:node[i].role_id,
                                   menu_id:node[i].menu_id,
                                   pid:node[i].pid,
                                   rights_byte:node[i].rights_byte});

            }
        }
    }

    function filterLevel2(node){
        for(var i=0;i<node.length;i++){
            if(node[i].child!=undefined){
                if(node[i].level==2) node[i].child = [];
                filterLevel2(node[i].child);
            }
        }
    }

    $scope.getRoleRights = function(orgid,roleid){
        RoleRights.getData(orgid,roleid).then(function(result) {
            $scope.data = result.data;
            //$scope.l2data = JSON.parse(JSON.stringify(result.data));
            //filterLevel2($scope.l2data);

            rgetChild($scope.data,null);
            console.log("getRoleRights=>",$scope.data);
            $scope.grid.api.setRowData($scope.data);
            $scope.grid.api.sizeColumnsToFit();
            // for(var i=0;i<$scope.l2data.length;i++){
            //     $scope.expandedNodes.push($scope.l2data[i]);
            // }
        });
    }

    $scope.saveRoleRights = function(){
        $scope.ndata = [];
        ngetChild($scope.data);
        //console.log("ndata=>",$scope.ndata);

        RoleRights.save($scope.ndata,$scope.orgid,$scope.roleid).then(function(result){
        $scope.getRoleRights($scope.orgid,$scope.roleid);
        });
    }

    $scope.refreshRoleRights = function(){
        $scope.getRoleRights($scope.orgid,$scope.roleid);
    }

    function checkRbyte(rb,b){
        var p = rb & Math.pow(2,b);
        return (   ( p == Math.pow(2,b)) );
    }

    function checkAvailable(node){
        var res=0;
        for(var i=0;i<MAX_RB;i++){
            //console.log("i:",i," title:",node.title,' rb:',Math.pow(2,i) & node.rights_byte,' av:',Math.pow(2,i) & node.byte_available);
            if( ( (Math.pow(2,i) & node.rights_byte) <= (Math.pow(2,i) & node.byte_available) &&
                  ( (Math.pow(2,i) & node.rights_byte)>0 &&  (Math.pow(2,i) & node.byte_available)>0 ) )
                ){
                res+=Math.pow(2,i);
            }
        }
        return res;
    }

    $scope.chgRbyte = function(node){
        //console.log("rb change=>",node);
    	var res=0;
    	for(var i=0;i<MAX_RB;i++){
    	   var b = node.bitArr[i]?1:0;
    	   if (b==1){
    	      res = res + Math.pow(2,i);
    	   }
    	}
    	node.rights_byte = res;
        if(node.child.length>0){
            if(checkRbyte(res,0)){
                //console.log('access all');
                rsetChild(node.child,true);
            }else{
                //console.log('deny all');
                rsetChild(node.child,false);
            }
        }
        //console.log(node.parent);
        if(node.parent)
        {
            //console.log("res=>",res,' checkRbyte:',checkRbyte(res,0));
            rsetParent($scope.data,node,checkRbyte(res,0));
        }
        var updatedNodes = [];
        $scope.grid.api.forEachNode( function(node) {
            var data = node.data;
            updatedNodes.push(node);
        });
        $scope.grid.api.refreshCells(updatedNodes,['rights_byte']);

    }

    $scope.selectRole = function(role){
        //console.log("sel role->",role);
        $scope.roleid = role.id;
        $scope.getRoleRights($scope.orgid,role.id);
    }

    $scope.modelChange = function(rr){
        //console.log("rr change=>",rr);
    }

    $scope.checkRB = function(available,rb){
        var p=available&rb;
        return(p==rb);
    }

    //------------------------------
    // --- Org Chart Tree
    //------------------------------
    //
    //
    //
    var gridColumnDefs = [
        {headerName: "Title", field: "title",cellRenderer: "group"},
        {headerName: "RB", field: "rights_byte",width:30},
        {headerName: "RB Available", field: "byte_available",width:30},
        {headerName: "Access", field: "bitArr[0]", width:50, cellStyle: {textAlign:'center'}, cellRenderer: accessCellRenderer},
        {headerName: "Action", field: "bitArr", cellRenderer:bitArrRenderer}
    ];

    $scope.grid = {
        columnDefs: gridColumnDefs,
        enableSorting: true,
        enableFilter: true,
        enableColResize: true,
        rowSelection: 'multiple',
        rowDeselection: true,
        angularCompileRows: true,

        getNodeChildDetails: function(node) {
            console.log("node=>",node);
            if (node.child.length>0) {
                return {
                    group: true,
                    children: node.child,
                    field: 'title',
                    key: node.title,
                    expanded: true
                };
            } else {
                return null;
            }
        },

    };

    function accessCellRenderer(params) {
        //params.$scope.ageClicked = ageClicked;
        //console.log("params=>",params);
        var html='<div class="ui form">'+
                      '<div class="field">'+
                        '<div class="ui toggle checkbox" style="margin:0px;">'+
                            '<input type="checkbox" ng-model="data.bitArr[0]" ng-change="chgRbyte(data)">'+
                            '</input><label></label>'+
                        '</div></div></div>';
        return html;
    }

    function bitArrRenderer(params) {
        var html='<div class="ui form" style="font-size:1.2rem">'+
                    '<div class="inline fields">'+
                        '<div class="field" ng-repeat="rb in rbData" ng-if="checkRB(data.byte_available,rb.rights_byte)">'+
                            '<div class="ui checkbox" >'+
                                '<input type="checkbox" tabindex="0" ng-model="data.bitArr[$index+1]"'+
                                        ' ng-change="chgRbyte(data)">'+
                                '<label style="font-size:1.3em;">{{rb.rights_desc}}</label>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                  '</div>'
        return html;
    }

    $scope.treeOptions = {
        nodeChildren: "child",
        dirSelectable: true,
        allowDeselect: false,
        multiSelection: false,
        injectClasses: {
            ul: "nav nav-list nav-pills nav-stacked bs-tree",
            li: "bs-tree-row",
            //liSelected: "h2",
            iExpanded: "glyphicon glyphicon-minus-sign",
            iCollapsed: "glyphicon glyphicon-plus-sign",
            iLeaf: "fa fa-fw fa-dot-circle-o",
            label: "a6",
            labelSelected: "a8"
        }
    }

    function deepSet(node,vis){
        //console.log("deepSet:",node,vis);
        node.visible=vis;
        if(node.child != undefined){
            for(var i=0;i<node.child.length;i++){
                node.child[i].visible=vis;
                if(node.child[i].child != undefined && node.child[i].child.length>0){
                    deepSet(node.child[i],vis);
                }
            }
        }
    }

    function deepFind(node,id,vis){
        //console.log("deepFind:",node,id,vis);
        for(var i=0;i<node.length;i++){
            if(node[i].menu_id == id){
                //console.log("found==>")
                //console.log(node[i]);
                if(vis) lastVisibleId=node[i].menu_id;
                //console.log("lastVisibleId:",lastVisibleId);
                //console.log("parent:",node[i].parent);
                if(node[i].parent != null)
                  node[i].parent.visible = vis;
                deepSet(node[i],vis);
                break;
            }else{
                    if(node[i].child!=undefined){
                        deepFind(node[i].child,id,vis);
                    }
            }
        }
    }

    $scope.l2Selected = function(node){
        console.log("selected:",node);
        $timeout(function(){$scope.searchText = node.title},0
        );
        
        if(lastVisibleId!=0){
            deepFind($scope.data,lastVisibleId,false);
        }
        deepFind($scope.data,node.menu_id,true);
    }

    $scope.clearSearch = function(){
                $scope.searchText = "";
    }

  });