angular.module('app')
    .controller('SvcMappingController', function($scope, $http, $q, CurrentUser, SvcMapping, Appl, AppResource, OrgChart, Menu, Role, bsTransTree) {

    //----------------------------------
    // Start-Up
    //----------------------------------

    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mService = null; //Model

    $scope.ctlMenu = {};
    $scope.ctlOrg = {};

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        return $q.resolve(
            AppResource.getData().then(
                function(res){
                    $scope.grid.data=res.data.Result;
                    $scope.loading=false;
                }
            )
        );
    }

    Menu.getMenuList().then(
        function(res){
            console.log("menu=>",res.data.Result);
            //console.log("grid data=>",$scope.grid.data);            
            var resx = bsTransTree.createTree(res.data.Result,'Id','ParentId');
            $scope.menuData = angular.copy(resx);
            // console.log("res=>>",resx);
            // $scope.grid.data = roleFlattenAndSetLevel(angular.copy(resx),0);
            $scope.loading=false;
        },
        function(err){
            console.log("err=>",err);
        }
    );

    OrgChart.getDataChildOnly().then(function(res) {
        console.log("orgData=>",res.data.Result);
        $scope.orgData = res.data.Result;
        //$scope.ctlOrg.expand_all();
    });

    Role.getData().then(function(res){
        $scope.roleData = bsTransTree.createTree(res.data.Result,'Id','ParentId');
    });

    AppResource.getData().then(function(res){
        $scope.appResourceData=res.data.Result;
        $scope.loading=false;
    });

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        enableRowHeaderSelection: true,
        multiSelect: true,
        enableSelectAll:true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100],
        paginationPageSize: 15,

        columnDefs: [
            { name:'id',    field:'Id', width:'7%' },
            { name:'app resource name', field:'Name' },
            { name:'description',  field: 'Description' },
            { name:'Enabled',  field: 'StatusCode' },


        ]
    };
    //----------------------------------
    // Form Fields Setup
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'name',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Service Name',
    //                                         placeholder: 'Service Name',
    //                                         required: true,
    //                                         minlength:4,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'url',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Service URL',
    //                                         placeholder: 'Service URL',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    //                 {
    //                     type: 'ui-select-single-select2',
    //                     key: 'menuid',
    //                     templateOptions: {
    //                                         optionsAttr: 'bs-options',
    //                                         ngOptions: 'option[to.valueProp] as option in to.options | filter: $select.search',
    //                                         label: 'Menu Title',
    //                                         valueProp: 'id',
    //                                         labelProp: 'name',
    //                                         placeholder: 'Select Menu Title',
    //                                         required: true,
    //                                         options: [],
    //                                         icon: 'fa fa-user',
    //                                         refresh: refreshMenu,
    //                                         refreshDelay: 0
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'rights_byte',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Rights',
    //                                         placeholder: 'Rights',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    //                 {
    //                     type: 'ui-select-single-select2',
    //                     key: 'appid',
    //                     templateOptions: {
    //                                         optionsAttr: 'bs-options',
    //                                         ngOptions: 'option[to.valueProp] as option in to.options | filter: $select.search',
    //                                         label: 'Application Title',
    //                                         valueProp: 'id',
    //                                         labelProp: 'name',
    //                                         placeholder: 'Select Application',
    //                                         required: true,
    //                                         options: [],
    //                                         refresh: refreshAppl,
    //                                         refreshDelay: 0
    //                     }
    //                 }
    // ];

    // function refreshAppl(appl, field) {
    //   var promise = Appl.getData();
    //   return promise.then(function(result) {
    //     console.log('appl=>', JSON.stringify(result.data));
    //     field.templateOptions.options = result.data;
    //   });
    // }
    // function refreshMenu(appl, field) {
    //   var promise = Appl.getData();
    //   return promise.then(function(result) {
    //     console.log('appl=>', JSON.stringify(result.data));
    //     field.templateOptions.options = result.data;
    //   });
    // }

});
