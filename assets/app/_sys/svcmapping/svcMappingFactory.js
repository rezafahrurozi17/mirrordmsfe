angular.module('app')
  .factory('SvcMapping', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        // var r=$http.get('/api/fwdms/AppResource');
        // console.log('r=>',r);
        // return r;

        var defer = $q.defer();
            defer.resolve(
              {
                "data" : {
                  "Result": [        
                      {
                          "Id": 196,
                          "StatusCode": true,
                          "Method": "GET",
                          "ControllerName": "/Areas",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.36",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 197,
                          "StatusCode": true,
                          "Method": "GET",
                          "ControllerName": "/Areas",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.367",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 198,
                          "StatusCode": true,
                          "Method": "PUT",
                          "ControllerName": "/Areas",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.373",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 199,
                          "StatusCode": true,
                          "Method": "POST",
                          "ControllerName": "/Areas",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.383",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 200,
                          "StatusCode": true,
                          "Method": "DELETE",
                          "ControllerName": "/Areas",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.39",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 194,
                          "StatusCode": true,
                          "Method": "POST",
                          "ControllerName": "/Areas",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.34",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 195,
                          "StatusCode": true,
                          "Method": "DELETE",
                          "ControllerName": "/Areas/DeleteMultipleArea",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.35",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 191,
                          "StatusCode": true,
                          "Method": "GET",
                          "ControllerName": "/Areas/GetAreaProvince&areaId={areaId}&provi",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.317",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 192,
                          "StatusCode": true,
                          "Method": "POST",
                          "ControllerName": "/Areas/InputDataArea",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.327",
                          "LastModifiedUserId": null
                      },
                      {
                          "Id": 193,
                          "StatusCode": true,
                          "Method": "PUT",
                          "ControllerName": "/Areas/PutMultipleArea",
                          "ActionName": "",
                          "LastModifiedDate": "2016-11-22T12:12:05.333",
                          "LastModifiedUserId": null
                      },
                  ],
                  "Start": 1,
                  "Limit": 10,
                  "Total": 10
                }
              }
            );
        return defer.promise;
      },
      create: function(service) {
        return $http.post('/api/fwdms/AppResource', [{
                                                Name: service.Name,
                                                Description: service.Description,
                                                StatusCode: service.StatusCode
                                              }]);
      },
      update: function(service){
        return $http.put('/api/fwdms/AppResource', [{
                                                Id: service.Id,
                                                Name: service.Name,
                                                Description: service.Description,
                                                StatusCode: service.StatusCode
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fwdms/AppResource',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });