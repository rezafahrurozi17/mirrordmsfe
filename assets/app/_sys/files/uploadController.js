// angular.module('app')
//   .controller('MessagesController', function($scope, Messages) {
//     Messages.getAll().then(function(result) {
//       $scope.messages = result.data;
//     });

angular.module('app')
  .controller('UploadController', function($scope, Upload,$state,$q,$interval,$timeout) {
    // $scope.isCollapsed = true;
    // $scope.auth = Auth;
    // $scope.user = CurrentUser.user;

    $scope.errorMsg = '';
    $scope.errors = [];


    function mFile() {
        this.fileName = null;
        this.f = null;
    }

    $scope.cFiles = [ new mFile() ];

    $scope.upload = function(file) {

      file.errorMsg = '';
      if (file && !file.$error) {
          file.upload = Upload.upload({
              url: '/file/upload',
              file: file
          });

          file.upload.then(function (response) {
              $timeout(function () {
                  console.log('response:',response);
                  file.result = response.data;
                  file.sizeKB = Math.round(response.data.file[0].size/1024);
                  $scope.cFiles.push( new mFile());
              });
          }, function (response) {
              if (response.status > 0)
                  console.log('err response=>',response);
                  file.sizeKB = Math.round(file.size/1024);
                  file.errorMsg = response.data.message;
          });

          file.upload.progress(function (evt) {
              file.progress = Math.min(100, parseInt(100.0 * 
                                                     evt.loaded / evt.total));
          });
      }   
    }

  });