angular.module('app')
  .controller('DownloadController', function($scope, Upload,$state,$q,$interval,$timeout) {
    // $scope.isCollapsed = true;
    // $scope.auth = Auth;
    // $scope.user = CurrentUser.user;
    
    $scope.upload = function(file) {
      $scope.f = file;
      if (file && !file.$error) {
          file.upload = Upload.upload({
              //url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
              url: '/file/upload',
              file: file
          });

          file.upload.then(function (response) {
              $timeout(function () {
                  console.log('response:',response);
                  file.result = response.data;
              });
          }, function (response) {
              if (response.status > 0)
                  $scope.errorMsg = response.status + ': ' + response.data;
          });

          file.upload.progress(function (evt) {
              file.progress = Math.min(100, parseInt(100.0 * 
                                                     evt.loaded / evt.total));
          });
      }   
    }



  });