angular.module('app')
    .controller('UserOrgRoleController', function($scope, $http, CurrentUser, Role,OrgChart,User,UserOrgRole,$q,$state) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mUser = null; //Model
    $scope.orgData = [];
    $scope.roleData = [];
    $scope.selOrg = {selected:null};
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getSupportingData = function(){
        Role.getData().then(
            function(res){
                console.log("role=>",res);
                $scope.roleData = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
        OrgChart.getData().then(
            function(res){
                console.log("org=>",res);
                $scope.orgData = res.data.Result;
                $scope.orgData2 = angular.copy(res.data.Result);
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.getData = function() {
        console.log("selOrg=>",$scope.selOrg.selected);
        if($scope.selOrg.selected){
            UserOrgRole.getDataByOrg($scope.selOrg.selected).then(
                function(res){
                    console.log("user=>",res);
                    $scope.grid.data = res.data.Result;
                    $scope.loading=false;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }else{
            UserOrgRole.getData().then(
                function(res){
                    console.log("user=>",res);
                    $scope.grid.data = res.data.Result;
                    $scope.loading=false;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }
    }

    // $scope.modelOptions = {
    //     debounce: {
    //         default: 500,
    //         blur: 250
    //     },
    //     getterSetter: true
    // };
    $scope.noResults=false;
    $scope.newUserMode=true;
    $scope.formApi={};

    $scope.getUserByUserName = function(username) {
        console.log("ctrl getData=>",username);
        var res=User.getDataByUserName(username);
        // .then(function(xres){
        //     $scope.fnd = xres.data.Result.length>0;
        //     console.log("fnd=>",$scope.fnd);
        //     if(!$scope.fnd){
        //       $scope.mUser.UserId=null;
        //       //$scope.mUser.userName=null;
        //       $scope.mUser.FullName=null;
        //       $scope.mUser.Email=null;
        //       $scope.mUser.Password=null;
        //       $scope.mUser.Phone1=null;
        //       $scope.mUser.Phone2=null;
        //       $scope.newUserMode=true;
        //     }
        //     return xres.data.Result;
        // });
        return res;
    };
    var linkSaveCb = function(mode,model){
        console.log("linkSaveCb=>",mode,model);
    }
    var linkBackCb = function(){
        console.log("linkBackCb=>");
    }
    $scope.onNoResult = function(){
        console.log("onNoResult=>");
        $scope.mUser.UserId=null;
        //$scope.mUser.userName=null;
        $scope.mUser.FullName=null;
        $scope.mUser.Email=null;
        $scope.mUser.Password=null;
        $scope.mUser.Phone1=null;
        $scope.mUser.Phone2=null;
        $scope.newUserMode=true;
        // showLinkView(callback function after Save, callback function when Back, EditMode,CloseAfterSave,CloseAfterCancel)
        $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'new',true,true);
    }
    $scope.onGotResult = function(){
        console.log("onGotResult=>");
        $scope.mUser.UserId=null;
        //$scope.mUser.userName=null;
        $scope.mUser.FullName=null;
        $scope.mUser.Email=null;
        $scope.mUser.Password=null;
        $scope.mUser.Phone1=null;
        $scope.mUser.Phone2=null;
        $scope.newUserMode=true;
    }
    $scope.selected = {};
    $scope.onSelectUser = function(item, model, label){
        console.log("onSelectUser=>",item,model,label);
        $scope.mUser.UserId = item.Id;
        $scope.mUser.FullName = item.FullName;
        $scope.mUser.Email=item.Email;
        $scope.mUser.Password=null;
        $scope.mUser.Phone1=item.Phone1;
        $scope.mUser.Phone2=item.Phone2;
        $scope.newUserMode=false;
    }

    $scope.selectOrg = function(selected){
        $scope.selectedOrg = selected;
        console.log("selectedOrg=>",selected);
    }
    $scope.selectOrg2 = function(selected){
        $scope.selectedOrg2 = selected;
        console.log("selectedOrg=>",selected);
    }

    //------------------------------------------------------------
    $scope.formmode={mode:''};
    $scope.doGeneratePass = function(){
        $scope.mUser.password = Math.random().toString(36).slice(-8);
    }
    $scope.onBeforeNewMode = function(){
        console.log("mode=>",$scope.formmode);
        $scope.newUserMode=true;
        $scope.editMode=false;
        //$scope.mode='new';
    }
    $scope.onBeforeEditMode = function(){
        //$scope.mode='edit';
        $scope.newUserMode=false;
        console.log("mode=>",$scope.formmode);
        $scope.editMode=true;
    }
    $scope.onBeforeDeleteMode = function(){
        console.log("mode=>",$scope.formmode);
        //$scope.mode='del';
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        enableRowHeaderSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100],
        // paginationPageSize: 15,

        columnDefs: [
            { name:'id',    field:'Id', width:'7%' },
            { name:'user id', displayName:'User ID', field:'UserName' },
            { name:'name', field:'FullName' },
            { name:'email',  field: 'Email' },
            { name:'Organization', field:'OrgName' },
            { name:'role',  field: 'RoleName' },
        ]
    };
});
