angular.module('app')
    .controller('UserLoginLogController', function($scope, $http, CurrentUser, User) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mUser = null; //Model

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        // User.generatePwd();
        User.getLoginLog().then(
            function(res){
                console.log("user=>",res);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }

    $scope.doGeneratePass = function(){
        $scope.mUser.password = Math.random().toString(36).slice(-8);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        enableRowHeaderSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100],
        paginationPageSize: 30,

        columnDefs: [
            //{ name:'id',    field:'id', width:'7%' },
            { name:'user id', displayName:'User ID', field:'userID' },
            { name:'user name', field:'userName' },
            { name:'dealer',  field: 'orgCode' },
            { name:'dealer name',  field: 'orgName' },
            { name:'role',  field: 'roleName' },
            { name:'last login',  field: 'updatedAt', type: 'date', cellFilter: 'date:"dd-MM-yyyy HH-mm-ss Z"'},
            { name:'IP Addr',  field: 'ipAddr' },
        ]
    };
});
