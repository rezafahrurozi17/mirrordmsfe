angular.module('app')
  .factory('UserOrgRole', function($http) {
    return {
      getData: function() {
        return $http.get('/api/fwdms/OrgUser');
      },
      getDataByOrg: function(orgid) {
        if(orgid)
          return $http.get('/api/fwdms/OrgUser?orgid='+orgid)
        else
          return $http.get('/api/fwdms/OrgUser')
      },
      updatePassword: function() {
        return $http.get('/sys/user/updatePassword');
      },
      create: function(user) {
        return $http.post('/api/fwdms/OrgUser', [{
                                            OrgId: user.OrgId,
                                            RoleId: user.RoleId,
                                            UserId: (user.UserId==null?0:user.UserId),
                                            UserName: user.UserName,
                                            FullName: user.FullName,
                                            Email: user.Email,
                                            Password:user.Password,
                                            Phone1: user.Phone1,
                                            Phone2: user.Phone2
                                          }]);
      },
      update: function(user){
        console.log('User Factory=>update:',angular.copy(user));
        if(user.Password=='') user.Password=null;
        return $http.put('/api/fwdms/OrgUser', [{
                                            id: user.Id,
                                            OrgId: user.OrgId,
                                            RoleId: user.RoleId,
                                            UserId: (user.UserId==null?0:user.UserId),
                                            UserName: user.UserName,
                                            FullName: user.FullName,
                                            Email: user.Email,
                                            Password:user.Password,
                                            Phone1: user.Phone1,
                                            Phone2: user.Phone2
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/fwdms/OrgUser',{data:id,headers: {'Content-Type': 'application/json'}});
      },

    }
  });
