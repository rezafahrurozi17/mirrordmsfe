angular.module('app')
    .controller('UserController', function($scope, $http, CurrentUser, Role,OrgChart,User) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mUser = null; //Model
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        User.getData().then(
            function(res){
                console.log("user=>",res);
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.formmode={mode:''};
    $scope.doGeneratePass = function(){
        $scope.mUser.password = Math.random().toString(36).slice(-8);
    }
    $scope.onBeforeNewMode = function(){
        console.log("mode=>",$scope.formmode);
        //$scope.mode='new';
    }
    $scope.onBeforeEditMode = function(){
        //$scope.mode='edit';
        console.log("mode=>",$scope.formmode);
    }
    $scope.onBeforeDeleteMode = function(){
        console.log("mode=>",$scope.formmode);
        //$scope.mode='del';
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        enableRowHeaderSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100],
        // paginationPageSize: 15,

        columnDefs: [
            { name:'id',    field:'Id', width:'7%' },
            { name:'user id', displayName:'User ID', field:'UserName' },
            { name:'name', field:'FullName' },
            { name:'email',  field: 'Email' },
            { name:'phone',  field: 'Phone1' },
        ]
    };
});
