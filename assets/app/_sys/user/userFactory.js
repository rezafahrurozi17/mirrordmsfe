angular.module('app')
  .factory('User', function($http) {
    return {
      getData: function() {
        return $http.get('/api/fwdms/UserPeople');
      },
      getLoginLog: function() {
        return $http.get('/sys/user/getLoginLog');
      },
      getDataById: function(id) {
        return $http.get('/api/fwdms/UserPeople?id='+id);
      },
      getDataByUserName: function(username) {
        return $http.get('/api/fwdms/UserPeople?username='+username);
      },
      updatePassword: function() {
        return $http.get('/sys/user/updatePassword');
      },
      create: function(user) {
        return $http.post('/api/fwdms/UserPeople', [{
                                                  UserName: user.UserName,
                                                  FullName: user.FullName,
                                                  Password:user.Password,
                                                  Email: user.Email,
                                                  Phone1: user.Phone1,
                                                  Phone2: user.Phone2
                                                }]);
      },
      update: function(user){
        //console.log('User Factory=>update:',user);
        return $http.put('/api/fwdms/UserPeople', [{
                                                  Id: user.Id,
                                                  UserName: user.UserName,
                                                  FullName: user.FullName,
                                                  Password:user.Password,
                                                  Email: user.Email,
                                                  Phone1: user.Phone1,
                                                  Phone2: user.Phone2
                                                }]);
      },
      delete: function(id) {
        return $http.delete('/api/fwdms/UserPeople',{data:id,headers: {'Content-Type': 'application/json'}});
      },

    }
  });
