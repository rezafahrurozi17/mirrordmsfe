angular.module('app')
  .factory('RoleRights', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(orgid,roleid) {
        //console.log("orgid=",orgid);
        //console.log("roleid=",roleid);
        //var rr=$http.post('/sys/roleRights',{orgid:orgid,roleid:roleid});
        var rr=$http.get('/api/fwdms/RoleMenu/GetByOrgIdRoleId?orgId='+orgid+'&roleId='+roleid);
        //console.log('rr=>',rr);
        return rr;
      },
      // getMenuList: function(){
      //   return $http.get('/menu/menulist');
      // },
      getRights: function(orgid,roleid,menuid){
        var rr=$http.post('/sys/roleRights/getRights',{orgid:orgid,roleid:roleid,menuid:menuid});
        return rr;
      },
      save: function(rr,orgid,roleid) {
        //console.log("save rr->",rr);
        var rrArr=[];
        for(var i=0;i<rr.length;i++){
          rrArr.push({Id:rr[i].Id, MenuId:rr[i].MenuId, OrgId:orgid, RoleId:roleid, AppResourceId:0, ByteEnable:rr[i].ByteEnable})
        }
        return $http.put('/api/fwdms/RoleMenu', rrArr);
      },
      // edit: function(menu) {
      //   return $http.post('/menu/edit', {body: menu});
      // },      
      // remove: function(message) {
      //   return $http.delete('/user/' + currentUser().id + '/messages/' + message.id);
      // }
    }
  });