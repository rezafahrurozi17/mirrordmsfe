//agGrid.initialiseAgGridWithAngular1(angular);

angular.module('app')
    .controller('RoleController_AgGrid', function($scope, $http, CurrentUser, Role,$timeout) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
        $scope.getData();
    });
    // var resizeNodes=function() {
    //     $("#role_layoutContainer").height($(window).height() - 205); //- $("#footerDiv").height() - $("#menuDiv").height()
    // };
    // $timeout(function () {
    //     // Set height initially
    //     resizeNodes();
    //     $scope.grid.api.sizeColumnsToFit();
    //     // Add a watch on window.resize callback
    //     $(window).resize(function(){
    //         resizeNodes();
    //         $scope.grid.api.sizeColumnsToFit();
    //     })
    // });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRole = null; //Model

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        $scope.loading=true;
        var data = Role.getData();
        // data.then(function(result) {
            // console.log('role=>', result.data);
            //$scope.gridData = result.data;
            // $scope.grid.api.setRowData(result.data);
            // $scope.loading=false;
            //return result.data;
        // });
        return data;
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    //
    //
    var gridColumnDefs = [
        {headerName: "id", field: "id"},
        {headerName: "Title", field: "title"},
        {headerName: "Desc", field: "desc"}
    ];

    $scope.grid = {
        columnDefs: gridColumnDefs,
        enableSorting: true,
        enableFilter: true,
        enableColResize: true,
        rowSelection: 'multiple',
        rowDeselection: true,
    };

    // $scope.grid = {
    //     enableSorting: true,
    //     enableRowSelection: true,
    //     enableRowHeaderSelection: false,
    //     multiSelect: false,
    //     paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100],
    //     paginationPageSize: 15,

    //     columnDefs: [
    //         { name:'id',    field:'id', width:'7%' },
    //         { name:'title', field:'title' },
    //         { name:'desc',  field: 'desc' }
    //     ]
    // };
    //----------------------------------
    // Form Fields Setup
    //----------------------------------
    var vm=this;
    vm.model = $scope.mRole;
    vm.fields = [
                    {
                        className: 'input-icon-right',
                        type: 'input',
                        key: 'title',
                        templateOptions: {
                                            type:'text',
                                            label: 'Name',
                                            placeholder: 'Name',
                                            required: true,
                                            minlength:4,
                                            //maxlength:7,
                                            icon:'fa fa-child'
                        }
                    },
                    {
                        className: 'input-icon-right',
                        type: 'input',
                        key: 'desc',
                        templateOptions: {
                                            type:'text',
                                            label: 'Description',
                                            placeholder: 'Description',
                                            required: true,
                                            icon:'glyphicon glyphicon-pencil'
                        }
                    },
                    {
                        className: 'input-icon-right',
                        type: 'input',
                        key: 'desc2',
                        templateOptions: {
                                            type:'text',
                                            label: 'Description 2',
                                            placeholder: 'Description 2',
                                            required: true,
                                            icon:'glyphicon glyphicon-pencil'
                        }
                    },
    ];
});
