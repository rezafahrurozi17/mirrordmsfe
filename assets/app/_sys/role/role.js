angular.module('app')
  .factory('Role', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fwdms/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(role) {
        return $http.post('/api/fwdms/Role', [{
                                            AppId: 1,
                                            ParentId: 0,
                                            Name: role.Name,
                                            Description: role.Description}]);
      },
      update: function(role){
        return $http.put('/api/fwdms/Role', [{
                                            Id: role.Id,
                                            //pid: role.pid,
                                            Name: role.Name,
                                            Description: role.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fwdms/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });