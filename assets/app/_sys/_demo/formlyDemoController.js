// angular.module('app')
//   .controller('MessagesController', function($scope, Messages) {
//     Messages.getAll().then(function(result) {
//       $scope.messages = result.data;
//     });

var app = angular.module('app');

  app.controller('FormlyDemoController', function MenuListCtl($scope, Auth,Menu, CurrentUser,$state,$q,$interval,formlyVersion) {
    $scope.isCollapsed = true;
    $scope.auth = Auth;
    $scope.user = CurrentUser.user;

//------------------------------------------------------------//

    var vm = this;

    // funcation assignment
    vm.submit = submit;

    // variable assignment
    vm.formTitle = 'Angular Formly Bootstrap Demo';
    vm.env = {
      angularVersion: angular.version.full,
      formlyVersion: formlyVersion
    };
    vm.model = {
        'firstName':'Andy',
        'lastName':'Go',
        'city':'AK'
    };
    vm.options = {
      formState: {
        awesomeIsForced: false
      }
    };
    
    vm.fields = [

          { template:'<br><h3 class="ui dividing header"><a class="ui red circular label">1 </a> Multi Fields - Segment</h3>'},
          // {
          //   type: 'segment',
          //   templateOptions: {
          //      type: 'stacked blue', //optional
          //     fields: 'three'          //optional 
          //   },
          //   data: {
          //     fields: [
          //               {
          //                 type: 'input',
          //                 key: 'firstName',
          //                 templateOptions: {
          //                   required:true,
          //                   label: 'First Name',
          //                   placeholder: 'First Name',
          //                   //width: 'three'  //optional
          //                 }
          //               },
          //               {
          //                 type: 'input',
          //                 key: 'lastName',
          //                 templateOptions: {
          //                   label: 'Last Name',
          //                   placeholder: 'Last Name',
          //                   //width: 'four', //optional
          //                   minlength: 5,    //optional
          //                   maxlength: 10    //optional
          //                 },
          //                 expressionProperties: {
          //                   'templateOptions.disabled': '!model.firstName'
          //                 }
          //               },
          //     ]
          //   }
          // },
          { template:'<br><h3 class="ui dividing header"><a class="ui red circular label">2 </a> Multi Fields (using fieldGroup)</h3>'},
          {
            className: 'row',
            fieldGroup: [
              {
                className: 'col-md-5',
                type: 'input',
                key: 'firstName',
                templateOptions: {
                  required:true,
                  label: 'First Name',
                  placeholder: 'First Name'
                }
              },
              {
                className: 'col-md-5',
                type: 'input',
                key: 'lastName',
                templateOptions: {
                  label: 'Last Name',
                  placeholder: 'Last Name'
                },
                expressionProperties: {  //optional
                  'templateOptions.disabled': '!model.firstName'
                }
              },
            ],
          },
          {template:'<br><h3 class="ui dividing header"><a class="ui red circular label">3 </a> Input Text</h3>'},
          {
            type: 'input',
            key: 'firstName',
            templateOptions: {
              required: true,
              label: 'First Name',
              placeholder: 'First Name'
            },
          },
          {
            type: 'input',
            key: 'lastName',
            templateOptions: {
              label: 'Last Name',
              placeholder: 'Last Name'
            },
            expressionProperties: {  //optional
              'templateOptions.disabled': '!model.firstName'
            }
          },
          {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">4 </a> Input Text (type=email,number,password)</h3>'},
          {
            type: 'input',
            key: 'email',
            templateOptions: {
              label: 'Email Address',
              placeholder: 'Email Address',
              type:'email'
            }
          },
          {
            type: 'input',
            key: 'age',
            templateOptions: {
              label: 'Age',
              placeholder: 'Age',
              type:'number'
            }
          },
          {
            type: 'input',
            key: 'password',
            templateOptions: {
              label: 'Pasword',
              placeholder: 'Enter your Password',
              type:'password'
            }
          },
          {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">5a </a> Select (combobox)</h3>'},
          {
            type: 'select',
            key: 'city',
            templateOptions: {
              label: 'City',
              placeholder: 'Please Select City',
              valueProp: 'value',
              labelProp: 'name',
              options:[
                        {"name":"Alabama","value":"AL"},
                        {"name": "Alaska","value": "AK"},
                        {"name": "American Samoa","value": "AS"},
                        {"name": "Arizona", "value": "AZ"},
                        {"name": "Arkansas","value": "AR"},
                        {"name": "California","value": "CA"}
                      ]
            }
          },
          {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">5b </a> Select (using select2)</h3>'},
          {
              type: 'ui-select-single-select2',
              key: 'city',
              templateOptions: {
                                  // optionsAttr: 'bs-options',
                                  // ngOptions: 'option[to.valueProp] as option in to.options | filter: $select.search',
                                  label: 'City',
                                  valueProp: 'value',
                                  labelProp: 'name',
                                  placeholder: 'Please Select City',
                                  options: [
                                              {"name":"Alabama","value":"AL"},
                                              {"name": "Alaska","value": "AK"},
                                              {"name": "American Samoa","value": "AS"},
                                              {"name": "Arizona", "value": "AZ"},
                                              {"name": "Arkansas","value": "AR"},
                                              {"name": "California","value": "CA"}
                                           ]
              }
          },


          {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">6 </a> Radio</h3>'},
          {
            type: 'radio',
            key: 'city',
            templateOptions: {
              label: 'City',
              placeholder: 'Please Select City',
              //type: 'radio',
              valueProp: 'value',
              labelProp: 'name',
              options:[
                        {"name":"Alabama","value":"AL"},
                        {"name": "Alaska","value": "AK"},
                        {"name": "American Samoa","value": "AS"},
                        {"name": "Arizona", "value": "AZ"},
                      ]
            }
          },

          {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">7 </a> Multi Select</h3>'},
          {
              type: 'ui-select-multiple-select2',
              key: 'cities',
              templateOptions: {
                                  // optionsAttr: 'bs-options',
                                  // ngOptions: 'option[to.valueProp] as option in to.options | filter: $select.search',
                                  label: 'Cities',
                                  valueProp: 'value',
                                  labelProp: 'name',
                                  placeholder: 'Please Select Cities',
                                  options: [
                                              {"name":"Alabama","value":"AL"},
                                              {"name": "Alaska","value": "AK"},
                                              {"name": "American Samoa","value": "AS"},
                                              {"name": "Arizona", "value": "AZ"},
                                              {"name": "Arkansas","value": "AR"},
                                              {"name": "California","value": "CA"}
                                           ]
              }
          },

          {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">8 </a> Date Picker</h3>'},
          {
            type: 'date-picker',
            key: 'date',
            templateOptions: {
              label: 'Date',
              placeholder: 'Pls Choose Date',
              datepickerOptions: {
                                    format: 'dd/MM/yyyy'
                                 }
            }
          },


          {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">9 </a> TextArea (memo)</h3>'},
          {
            type: 'textarea',
            key: 'description',
            templateOptions: {
              required : true,
              label: 'Description',
              placeholder: 'Description',
              rows:2  //optional
            }
          },
          {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">10 </a> Checkbox (regular,slider,toggle)</h3>'},
          {
            type: 'checkbox',
            key: 'agree',
            templateOptions: {
              required : true,
              label: 'I agree to the terms and conditions'
            }
          },
          {
            type: 'checkbox',
            key: 'agree',
            templateOptions: {
              required : true,
              label: 'I agree to the terms and conditions',
              type: 'slider'
            }
          },
          {
            type: 'checkbox',
            key: 'agree',
            templateOptions: {
              required : true,
              label: 'I agree to the terms and conditions',
              type: 'toggle'
            }
          },
          // {template:'</br><h3 class="ui dividing header"><a class="ui red circular label">11 </a> Tab</h3>'},
          // {
          //   type:'tab',
          //   //className:'ui top attached tabular menu',
          //   templateOptions: {
          //     tabs: [
          //                 {
          //                   'name':'tab1',
          //                   'title':'Tab 1',
          //                   'fields': [
          //                               {
          //                                 type: 'input',
          //                                 key: 'firstName',
          //                                 templateOptions: {
          //                                   required:true,
          //                                   label: 'First Name',
          //                                   placeholder: 'First Name',
          //                                   //width: 'three'  //optional
          //                                 }
          //                               },
          //                               {
          //                                 type: 'input',
          //                                 key: 'lastName',
          //                                 templateOptions: {
          //                                   label: 'Last Name',
          //                                   placeholder: 'Last Name',
          //                                   //width: 'four', //optional
          //                                   minlength: 5,    //optional
          //                                   maxlength: 10    //optional
          //                                 },
          //                                 expressionProperties: {
          //                                   'templateOptions.disabled': '!model.firstName'
          //                                 }
          //                               },
          //                              ]
          //                },
          //                {
          //                  'name':'tab2',
          //                  'title':'Tab 2',
          //                  'fields':[
          //                               {
          //                                 type: 'input',
          //                                 key: 'firstName',
          //                                 templateOptions: {
          //                                   required:true,
          //                                   label: 'First Name',
          //                                   placeholder: 'First Name',
          //                                   //width: 'three'  //optional
          //                                 }
          //                               },
          //                           ]
          //                },
          //                {
          //                  'name':'tab3',
          //                  'title':'Tab 3',
          //                  'fields':[]
          //                }
          //             ]
          //   }
          // },
    ];

    // function definition
    function submit() {
      alert(JSON.stringify(vm.model), null, 2);
    }

  });