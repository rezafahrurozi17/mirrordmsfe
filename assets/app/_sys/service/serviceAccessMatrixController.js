angular.module('app')
  .controller('ServiceAccessMatrixController', function($scope, $http, $sce,$filter,$timeout,$window,CurrentUser, Service, Role, ServiceRights, OrgChart, Auth) {

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
                    $scope.getOrgData();
                    $scope.getRoleData();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    // $scope.mUserEmpty={
    //                     id:null,
    //                     name:null,
    //                     email:null,
    //                     password:null,
    //                     phone:null,
    //                     address:null
    //                   };
    $scope.orgData = []; //OrgChart Collection
    $scope.ctlOrg = {}; //OrgChart Tree Controller
    $scope.selOrg = null;  //Selected Org

    $scope.roleData = []; //Role Collection
    $scope.selRole = null; //Selected Role

    $scope.sm_showServiceList=false;
    $scope.sm_showRemoveService=false;

    $scope.mService = null; //Service Model
    $scope.serviceData = []; //Service Collection
    $scope.selService = null //Selected Service

    $scope.$on('tabChange', function(){
        resizeLayout();
    });
    $timeout(function () {
        // Set height initially
        resizeLayout();
    },0);
    var resizeLayout=function() {
        $timeout(function(){
          var main= $("#header").outerHeight()
                      + $("#page-footer").outerHeight()
                      + $(".nav.nav-tabs").outerHeight()
                      + $(".bs-navbar").outerHeight();
          $("#layoutContainer_sam").height($(window).height()-main-10); //- $("#footerDiv").height() - $("#menuDiv").height()
        },0);
    };
    angular.element($window).bind('resize', resizeLayout);
    $scope.$on('$destroy', function() {
        angular.element($window).unbind('resize', resizeLayout);
    });

    // ---------------------------------------------------------------------------------
    // --------------------------------- Role  -----------------------------------------
    // ---------------------------------------------------------------------------------
    $scope.getRoleData = function(){
      Role.getData().then(function(result) {
          $scope.roleData = result.data;
          console.log("roleData=>",result.data);
          $scope.ctlOrg.expand_all();
    });

    $scope.selectRole = function(role){
          console.log("sel role->",role);
          $scope.selRole = role;
          $scope.getServiceRightsbyOrgRole(role);
    }
}
    // ---------------------------------------------------------------------------------
    // --------------------------- Organization Chart  ---------------------------------
    // ---------------------------------------------------------------------------------

    //----------------------------------
    // Backend Operation
    //----------------------------------
    // $scope.getOrgData = function(){
    //     console.log('getorgdata');
    //     OrgChart.getData().then(function(result) {
    //       $scope.orgChartData = result.data;
    //       console.log("orgChartData all=>",result.data);
    //     });
    // }
    $scope.getOrgData = function(){
        OrgChart.getDataByRoleUser($scope.user).then(function(result) {
          $scope.orgData = result.data;
          console.log("orgData=>",result.data);
          $scope.ctlOrg.expand_all();
        });
    }
    $scope.showSelOrg = function(org){
      console.log('selected org:',org);
        $scope.selOrg = org;
        if($scope.selRole){
          $scope.getServiceRightsbyOrgRole($scope.selRole);
        }
    }
//------------------------------------------------------
// ------------------ Service Rights List --------------
//------------------------------------------------------

    //----------------------------------
    // Button Action
    //----------------------------------
    $scope.actAssign = function(){
        $scope.getServiceData();
        $scope.sm_showServiceList = true;
    }
    $scope.actEdit = function(){
        // $scope.getServiceData();
        // $scope.sm_showServiceList = true;
    }
    $scope.actRemove = function(){
        var selectedRow = $scope.gridApiServiceRights.selection.getSelectedRows();
        console.log("selectRow doRemove", selectedRow);
        $scope.mService = angular.copy(selectedRow[0]);
        $scope.sm_showRemoveService=true;
    }

    //----------------------------------
    // Backend Operation Service Rights
    //----------------------------------

    $scope.getServiceRightsbyOrgRole = function(role){
        console.log("selected org=>",$scope.selOrg);
        console.log("selected role=>",role);

        if($scope.selOrg){
          ServiceRights.getServiceRightsbyOrgRole($scope.selOrg.id, role.id).then(function(result) {
              $scope.gridServiceRights.data = result.data;
              console.log("ServiceRightsData=>",JSON.stringify(result.data));
          });
        }else{
          $scope.gridServiceRights.data=[];
        }
    }
    $scope.doAssign = function(){
        $scope.sm_showServiceList = false;
        var arrSelected = [];
        var selectedRow = $scope.gridApiService.selection.getSelectedRows();
        console.log("selectedRow:",selectedRow);
        for (var i = selectedRow.length - 1; i >= 0; i--) {
          arrSelected.push(selectedRow[i].id);
        };

        console.log("selectedRole=>",$scope.selRole);
        console.log("org=>",$scope.selOrg.id);
        console.log("di chek=>",arrSelected);

        ServiceRights.assign($scope.selOrg.id,$scope.selRole.id, arrSelected).then(function(result) {
          // $scope.gridService.data = result.data;
          // console.log("serviceData=>",JSON.stringify(result.data));
          $scope.getServiceRightsbyOrgRole($scope.selRole);
        });
    }
    $scope.doRemove = function(){
        $scope.sm_showRemoveService=false;
        var arrSelectedId = [];
        var arrSelectedServiceId = [];
        var selectedRow = $scope.gridApiServiceRights.selection.getSelectedRows();
        for (var i = selectedRow.length - 1; i >= 0; i--) {
          arrSelectedId.push(selectedRow[i].id);
          arrSelectedServiceId.push(selectedRow[i].service_id);
        };

        ServiceRights.remove(arrSelectedId, arrSelectedServiceId).then(function(result) {
          $scope.getServiceRightsbyOrgRole($scope.selRole);
        });
    }
    //----------------------------------
    // Backend Operation Service List
    //----------------------------------

    $scope.getServiceData = function(){
        Service.getData().then(function(result) {
            $scope.gridService.data = result.data;
            $scope.serviceData = result.data;
            console.log("serviceData=>",JSON.stringify(result.data));
            $scope.getAvailableService();
        });
    }
    $scope.getAvailableService = function() {
        $scope.serviceIdList = [];
        for (var i = $scope.gridServiceRights.data.length - 1; i >= 0; i--) {
          console.log($scope.gridServiceRights.data[i].service_id);
          var searchValue = {'id': $scope.gridServiceRights.data[i].service_id};
          var index = _.findIndex( $scope.gridService.data, searchValue);
          console.log("result=>",index);
          $scope.gridService.data.splice(index, 1);
        }
    }

    //------------------------------------------------------
    // ------------------- Grids--- ------------------------
    //------------------------------------------------------

    //grid service list
    $scope.getTableHeightService = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 40; // your header height
           var filterHeight = 40; // your filter height
           var pageSize = $scope.gridService.paginationPageSize;
           if($scope.gridService.paginationPageSize > $scope.gridService.data.length){
                pageSize = $scope.gridService.data.length;
           }
           if(pageSize<4){
              pageSize=3;
           }
           return {
              height: (pageSize * rowHeight + headerHeight)+42 + "px"
           };
    };

    $scope.gridService = {
            enableSorting: true,
            enableRowSelection: true,
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5,10,25,50,75,100],
            paginationPageSize: 10,

            columnDefs: [
              { name:'id', field: 'id',width:'7%' },
              { name:'service name',  field: 'service_name'},
              { name:'service url', field: 'service_url'}
            ]
    };

    $scope.gridService.onRegisterApi = function(gridApi) {
        // set gridApi on scope
        $scope.gridApiService = gridApi;

        $scope.gridApiService.selection.on.rowSelectionChanged($scope,
                function(row) {
                    console.log('selected row service list=>',row);
                });
            if($scope.gridApiService.selection.selectRow){
              $scope.gridApiService.selection.selectRow($scope.gridService.data[0]);
            }
        }

    $scope.selectAll = function() {
        $scope.gridApiService.selection.selectAllRows();
    };

    $scope.clearAll = function() {
        $scope.gridApiService.selection.clearSelectedRows();
    };

    //-----------------------------------

    $scope.getTableHeight = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 40; // your header height
           var filterHeight = 40; // your filter height
           var pageSize = $scope.gridServiceRights.paginationPageSize;
           if($scope.gridServiceRights.paginationPageSize > $scope.gridServiceRights.data.length){
                pageSize = $scope.gridServiceRights.data.length;
           }
           if(pageSize<4){
              pageSize=3;
           }
           return {
              height: (pageSize * rowHeight + headerHeight)+42 + "px"
           };
    };

    $scope.gridServiceRights = {
            enableSorting: true,
            enableRowSelection: true,
            enableFullRowSelection: true,
            enableRowHeaderSelection: true,
            multiSelect: true,
            paginationPageSizes: [5,10,25,50,75,100],
            paginationPageSize: 10,


            columnDefs: [
              { name:'id', field: 'id',width:'7%' },
              { name:'service name',  field: 'service_name'},
              { name:'service URL', field: 'service_url'}
            ]
    };

    $scope.gridServiceRights.onRegisterApi = function(gridApi) {
        // set gridApi on scope
        $scope.gridApiServiceRights = gridApi;

        $scope.gridApiServiceRights.selection.on.rowSelectionChanged($scope,
                function(row) {
                    $scope.selectedRow = row.entity;
                    console.log('selected row=>',row.entity);
                });
            if($scope.gridApiServiceRights.selection.selectRow){
              $scope.gridApiServiceRights.selection.selectRow($scope.gridServiceRights.data[0]);
            }
    };

});