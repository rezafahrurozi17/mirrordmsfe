angular.module('app')
  .factory('ServiceRights', function($http) {
    return {
      getServiceRightsbyServiceId: function(serviceId) {
        return $http.post('/sys/serviceRights/getServiceRightsbyServiceId',{serviceid:serviceId});
      },
      getServiceRightsbyOrgRole: function(orgId,roleId) {
        return $http.post('/sys/serviceRights/getServiceRightsbyOrgRole',{orgid:orgId,roleid:roleId});
      },
      assign: function(orgId,roleId, serviceIds) {
        return $http.post('/sys/serviceRights/assign',{orgid:orgId,roleid:roleId, serviceid: serviceIds});
      },
      remove: function(Ids, serviceIds) {
        return $http.post('/sys/serviceRights/remove',{id:Ids, serviceid: serviceIds});
      }
    }
  });
