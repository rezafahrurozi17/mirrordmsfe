angular.module('app')
    .controller('ServiceListController', function($scope, $http, CurrentUser, Appl,ServiceList) {

    //----------------------------------
    // Start-Up
    //----------------------------------

    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mService = null; //Model

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        ServiceList.getData().then(function(res){
            $scope.grid.data=res.data.Result;
            $scope.loading=false;
        });

    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        enableRowHeaderSelection: true,
        multiSelect: true,
        enableSelectAll:true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100],
        paginationPageSize: 15,

        columnDefs: [
            { name:'id',    field:'Id', width:'7%' },
            { name:'method', field:'Method' },
            { name:'controller name',  field: 'ControllerName' },
            { name:'action name',  field: 'ActionName' },
            { name:'Enabled',  field: 'StatusCode' },


        ]
    };
    //----------------------------------
    // Form Fields Setup
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'name',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Service Name',
    //                                         placeholder: 'Service Name',
    //                                         required: true,
    //                                         minlength:4,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'url',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Service URL',
    //                                         placeholder: 'Service URL',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    //                 {
    //                     type: 'ui-select-single-select2',
    //                     key: 'menuid',
    //                     templateOptions: {
    //                                         optionsAttr: 'bs-options',
    //                                         ngOptions: 'option[to.valueProp] as option in to.options | filter: $select.search',
    //                                         label: 'Menu Title',
    //                                         valueProp: 'id',
    //                                         labelProp: 'name',
    //                                         placeholder: 'Select Menu Title',
    //                                         required: true,
    //                                         options: [],
    //                                         icon: 'fa fa-user',
    //                                         refresh: refreshMenu,
    //                                         refreshDelay: 0
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'rights_byte',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Rights',
    //                                         placeholder: 'Rights',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    //                 {
    //                     type: 'ui-select-single-select2',
    //                     key: 'appid',
    //                     templateOptions: {
    //                                         optionsAttr: 'bs-options',
    //                                         ngOptions: 'option[to.valueProp] as option in to.options | filter: $select.search',
    //                                         label: 'Application Title',
    //                                         valueProp: 'id',
    //                                         labelProp: 'name',
    //                                         placeholder: 'Select Application',
    //                                         required: true,
    //                                         options: [],
    //                                         refresh: refreshAppl,
    //                                         refreshDelay: 0
    //                     }
    //                 }
    // ];

    // function refreshAppl(appl, field) {
    //   var promise = Appl.getData();
    //   return promise.then(function(result) {
    //     console.log('appl=>', JSON.stringify(result.data));
    //     field.templateOptions.options = result.data;
    //   });
    // }
    // function refreshMenu(appl, field) {
    //   var promise = Appl.getData();
    //   return promise.then(function(result) {
    //     console.log('appl=>', JSON.stringify(result.data));
    //     field.templateOptions.options = result.data;
    //   });
    // }

});
