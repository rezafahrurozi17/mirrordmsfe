angular.module('app')
  .factory('AppResource', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var r=$http.get('/api/fwdms/AppResource');
        console.log('r=>',r);
        return r;
      },
      create: function(service) {
        return $http.post('/api/fwdms/AppResource', [{
                                                Name: service.Name,
                                                Description: service.Description,
                                                StatusCode: service.StatusCode
                                              }]);
      },
      update: function(service){
        return $http.put('/api/fwdms/AppResource', [{
                                                Id: service.Id,
                                                Name: service.Name,
                                                Description: service.Description,
                                                StatusCode: service.StatusCode
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fwdms/AppResource',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });