angular.module('app')
  .factory('ServiceList', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var r=$http.get('/api/fwdms/Service');
        console.log('r=>',r);
        return r;
      },
      create: function(service) {
        return $http.post('/api/fwdms/Service', [{
                                                Method: service.Method,
                                                ControllerName: service.ControllerName,
                                                ActionName: service.ActionName,
                                                StatusCode: service.StatusCode
                                              }]);
      },
      update: function(service){
        return $http.put('/api/fwdms/Service', [{
                                                Id: service.Id,
                                                Method: service.Method,
                                                ControllerName: service.ControllerName,
                                                ActionName: service.ActionName,
                                                StatusCode: service.StatusCode
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fwdms/Service',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });