angular.module('myversion', []).provider('appversion', function () {
    // u should change this if u wanna build the latest
    this.versi = '1.0.26.2.0';
    // this.versi = new Date()
    // this.versi = this.versi.getTime();
    this.$get = function() {
        var versi = this.versi;
        return {
            getVersi: function() {
                return versi
            }
        }
    };

    this.setName = function(name) {
        this.name = name;
    };
});
