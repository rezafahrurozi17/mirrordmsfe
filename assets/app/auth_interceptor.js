angular.module('LoginModule')
    .factory('AuthInterceptor', function($rootScope, $q, $injector, LocalService) {
        var canceller = $q.defer();
        return {
            request: function(config) {
                //console.log('intercept request config:',config);
                var usePort = true;
                var port = '';
                if (LocalService.get('Svc_IP')) {
                    var svc_IP = LocalService.get('Svc_IP');
                } else {
                    var svc_IP = 'https://dms.toyota.astra.co.id';

                }
                // ---------------------------------------------
                var token;
                if (LocalService.get('BSLocalData')) {
                    token = angular.fromJson(LocalService.get('BSLocalData')).access_token;
                }
                if (token) {
                    config.headers.Authorization = 'Bearer ' + token;
                }
                var xurl = config.url.split('/');
                if (xurl[1] == 'token' || xurl[2] == 'Logout') {
                    config.url = svc_IP + config.url;
                } else if (xurl[1] == 'api') {
                    //kalo publish ke sit atau test, jangan lupa dimatikan yang dibawah ini
                    // switch (xurl[2]) {
                    // case 'fw':
                    // port = ':38001';
                    // break;
                    // case 'param':
                    // port = ':38002';
                    // break;s
                    // case 'ds':
                    // port = ':38003';
                    // break;
                    // case 'rpt':
                    // port = ':38004';
                    // break;
                    // case 'as':
                    // port = ':38007';
                    // break;
                    // case 'sales':
                    // port = ':38006';
                    // break;
                    // case 'fe':
                    // port = ':38008';
                    // break;
                    // case 'crm':
                    // port = ':38005';
                    // break;
                    // case 'ct':
                    // port = ':38010';
                    // break;
                    // }
                    config.url = svc_IP + (usePort ? port : '') + config.url;
                    //config.url = svc_IP + config.url;
                    //config.headers['Content-Type'] = 'application/json';
                }
                var tmpSession = sessionStorage.getItem('BSSessionData');
                var tmpKeySession = null;
                var tmpKeyLocalService = null;
                if(tmpSession !== null && tmpSession !== 'null'){
                    tmpKeySession = JSON.parse(angular.fromJson(tmpSession).LoginData);
                    tmpKeyLocalService = JSON.parse(angular.fromJson(LocalService.get('BSLocalData')).LoginData)
                    if(tmpKeySession.OrgId !== tmpKeyLocalService.OrgId || tmpKeySession.UserName !== tmpKeyLocalService.UserName ){
                        var response ='invalidSession';
                        // config.cache = {
                        //     get: function() {
                        //         return null;
                        //     }
                        // };
                        canceller.resolve(response);
                    }
                }else{
                    sessionStorage.setItem('BSSessionData', LocalService.get('BSLocalData'))
                    return config;
                }
                config.timeout = canceller.promise;
                console.log('config ===>', config);
                return $q.when(config);
            },
            responseError: function(response) {
                console.log("factory responseError=>", response, response.config.timeout.$$state.value);
                if (response.status <= 0) { // Request timeout
                    if(response.config.timeout.$$state.value === 'invalidSession' && $rootScope.ngDialog.getOpenDialogs().length <= 1 ){
                        $rootScope.ngDialog.openConfirm({ 
                            template: '\<div class="dialog-contents">\
                                            <div class="swal2-icon swal2-warning swal2-icon-show" style="display: flex;justify-content:center;"><div class="swal2-icon-content" style="">!</div></div>\
                                            <h2 style="text-align:center">Sesi login anda sudah berbeda, maka sesi ini akan dihentikan.</h2>\
                                            <h5 style="text-align:center">Halaman ini akan direfresh secara otomatis.<br> Tekan tombol <strong>OK</strong> untuk melanjutkan</h5>\
                                            <div class="ngdialog-buttons" style="display:flex;justify-content:center; margin-top:10px;"> <button type="button" class="ngdialog-button ngdialog-button-primary" style="margin:0;" ng-click="confirm(1)">OK</button></div>\
                                        </div>',
                            plain: true,
                            showClose: false,
                            closeByEscape : false,
                            closeByNavigation: false,
                            closeByDocument: false,
                            id:'sessionMsg'
                        }).then(function (success) {
                            window.location = '/';
                            sessionStorage.removeItem('BSSessionData');
                        }, function (error) {
                            // Error logic here
                        });
                    }else if ($rootScope.ngDialog.getOpenDialogs().length==0) {
                                            
                        $rootScope.ngDialog.open({ 
                            template: '_sys/auth/dialog_err_default.html',
                            controller: ['$scope', '$timeout', function($scope) {
                                $scope.status = "Error Koneksi";
                                $scope.message = "Mohon cek koneksi anda / aplikasi sedang dalam maintenance. Silakan coba beberapa saat lagi, Terima kasih";
                                $scope.debugMode = false;
                            }] 
                        });
                            
                    }
                    
                } else if (response.status == 401 && response.data.Message == "jwt expired") { // token expired
                    LocalService.unset('auth_token');
                    window.location = '/sys/auth/logout';
                } else if (response.status == 400) {
                    $rootScope.ngDialog.open({
                        template: '_sys/auth/dialog_err_default.html',
                        controller: ['$scope', '$timeout', function($scope) {
                            console.log("response=>", response);
                            // var msg=(msg==undefined?"":response.Message);
                            // var code=(response.data==""?"":" ("+response.data.code+")");
                            $scope.status = response.status + " (" + response.statusText + ")";
                            var msg = JSON.parse(response.data.Message);
                            console.log("msg,config=>", msg, response.config);
                            $scope.message = msg.Response.ResponseMessage;
                            $scope.sql = msg['Object'];
                            $scope.data = JSON.stringify(response.config.data);
                            $scope.url = response.config.url;
                            $scope.method = response.config.method;
                            $scope.debugMode = $rootScope.DEBUG_MODE.enabled;
                        }],
                    });
                } else if (response.status == 303) {
                    $rootScope.ngDialog.open({
                        template: '_sys/auth/dialog_err_default.html',
                        controller: ['$scope', '$timeout', function($scope) {
                            console.log("response=>", response);
                            // var msg=(msg==undefined?"":response.Message);
                            // var code=(response.data==""?"":" ("+response.data.code+")");
                            $scope.status = response.status + " (" + response.statusText + ")";
                            var msg = JSON.parse(response.data.Message);
                            console.log("msg,config=>", msg, response.config);
                            $scope.message = msg.Response.ResponseMessage;
                            $scope.sql = msg['Object'];
                            $scope.data = JSON.stringify(response.config.data);
                            $scope.url = response.config.url;
                            $scope.method = response.config.method;
                            $scope.debugMode = false;
                        }],
                    });
                } else {
                    try {
                        $rootScope.ngDialog.open({
                            template: '_sys/auth/dialog_err_default.html',
                            controller: ['$scope', '$timeout', function($scope) {
                                //console.log("respon=>",response);
                                var msg = (response.data.message == undefined ? "" : response.data.Message);
                                var code = (response.data == "" ? "" : " (" + response.data.code + ")");
                                $scope.status = response.status + " (" + response.statusText + ")";
                                $scope.message = response.data.Message;
                                $scope.data = JSON.stringify(response.config.data);
                                if ($scope.data == undefined)
                                {
                                    $scope.data = response.data.ExceptionMessage;
                                    if ($scope.data == undefined){ 
                                        $scope.data = new TextDecoder().decode(response.data);
                                    }
                                }
                                $scope.url = response.config.url;
                                $scope.method = response.config.method;
                                $scope.debugMode = $rootScope.DEBUG_MODE.enabled;
                            }],
                        });
                    } catch (err) {

                    } finally {
                        console.log("ResponseError=>", response);
                        if (response.status === 401) {
                            if (LocalService.get('BSLocalData')) {
                                // window.location = '/sys/auth/logout';
                            } else {
                                LocalService.unset('BSLocalData');
                            }
                            //$injector.get('$state').go('login');
                        }
                    }
                }
                return $q.reject(response);
            },
        }
    })
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });