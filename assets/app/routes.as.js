//"use strict";

//angular.module('app', ['ui.router','ngResource']);

angular.module('app')
    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function($locationProvider, $stateProvider, $urlRouterProvider) {

        $stateProvider
        // -------------------------------- //
        // Application Module - After Sales (services) //
        // -------------------------------- //


            .state('app.saMainMenu', {
                //url: '/kb',
                data: {
                    title: 'saMainMenu',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "saMainMenu@app": {
                        templateUrl: 'app/services/saMainMenu/saMainMenu.html',
                        controller: 'saMainMenuController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            }).state('app.ascalendar', {
                //url: '/kb',
                data: {
                    title: 'Working Calendar',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "ascalendar@app": {
                        templateUrl: 'app/services/calendar/calendar.html',
                        controller: 'WorkingCalendarController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    fullcalendar: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('ui.calendar');
                    }],

                },
                params: {
                    tab: null
                },
            })
            .state('app.attendance', {
                //url: '/kb',
                data: {
                    title: 'Absensi Teknisi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "attendance@app": {
                        templateUrl: 'app/services/attendance/attendance.html',
                        controller: 'AttendanceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: {
                    tab: null
                },
            })
            .state('app.attendanceplan', {
                //url: '/kb',
                data: {
                    title: 'Attendance Plan',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "attendanceplan@app": {
                        templateUrl: 'app/services/attendanceplan/attendanceplan.html',
                        controller: 'AttendancePlanController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: {
                    tab: null
                },
            })
            //udah ada di stevian
            /*.state('app.parts', {
              //url: '/kb',
              data: {
                title: 'Master Parts',
                access: 1
              },
              deepStateRedirect: true,
              sticky: true,
              views: {
                "parts@app": {
                  templateUrl: 'app/parts/mstparts/parts.html',
                  controller: 'PartsController'
                }
              },
              resolve: {
                sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                  return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                  return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }]

              },
              params: {
                tab: null
              },
            })*/
            .state('app.partsclass', {
                //url: '/kb',
                data: {
                    title: 'Parts Category',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "partsclass@app": {
                        templateUrl: 'app/parts/category/category.html',
                        controller: 'PartsCategoryController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_parts');
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.gmaster', {
                //url: '/kb',
                data: {
                    title: 'General Master',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "gmaster@app": {
                        templateUrl: 'app/services/gmaster/gmaster.html',
                        controller: 'GeneralMasterController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.gate', {
                //url: '/kb',
                data: {
                    title: 'Gate In/Out',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "gate@app": {
                        templateUrl: 'app/services/gate/gate.html',
                        controller: 'GateController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_gate')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    barcode: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('barcode')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.generalparameter', {
                //url: '/kb',
                data: {
                    title: 'General Parameter',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "generalparameter@app": {
                        templateUrl: 'app/services/workshopSettings/generalParameter/generalParameter.html',
                        controller: 'GeneralParameterController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.DMSCataloge', {
                //url: '/kb',
                data: {
                    title: 'DMS Katalog',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "DMSCataloge@app": {
                        templateUrl: 'app/services/helpsupport/katalog.html',
                        controller: 'KatalogController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: {
                    tab: null
                },
            })
            .state('app.crmAccesMatrix', {
                data: {
                    title: 'Hak Akses CRM',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "crmAccesMatrix@app": {
                        templateUrl: 'app/crm/accesMatrix/crmAccessMatrix.html',
                        // templateUrl: 'app/crm/customerData/cusdata.html',
                        controller: 'CrmAccessMatrixController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_crm');
                    }]
                },
                params: {
                    tab: null
                },
            })
            .state('app.crmDataBasic', {
                data: {
                    title: 'Data Basic',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "crmDataBasic@app": {
                        templateUrl: 'app/crm/dataBasic/crmDataBasic.html',
                        // templateUrl: 'app/crm/customerData/cusdata.html',
                        controller: 'CrmDataBasicController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_crm');
                    }]
                },
                params: {
                    tab: null
                },
            })
            .state('app.cusdata', {
                //url: '/kb',
                data: {
                    title: 'Customer Data',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "cusdata@app": {
                        templateUrl: 'app/crm/customerData/custData.html',
                        controller: 'CustDataController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_crm');
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.vehicleData', {
                data: {
                    title: 'Vehicle Data',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "vehicleData@app": {
                        templateUrl: 'app/crm/vehicleData/vehicleData.html',
                        // templateUrl: 'app/crm/customerData/cusdata.html',
                        controller: 'vehicleDataController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_crm');
                    }]
                },
                params: {
                    tab: null
                },
            })
            .state('app.stall', {
                //url: '/kb',
                data: {
                    title: 'Stall',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stall@app": {
                        templateUrl: 'app/services/workshopSettings/stall/stall.html',
                        controller: 'StallController'
                            // templateUrl: 'app/crm/customerData/cusdata.html',
                            // controller: 'CusDataController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.groupbp', {
                //url: '/kb',
                data: {
                    title: 'Group BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "groupbp@app": {
                        templateUrl: 'app/master/groupBP/groupBP.html',
                        controller: 'GroupBPController'
                            // templateUrl: 'app/crm/customerData/cusdata.html',
                            // controller: 'CusDataController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master');
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.groupgr', {
                //url: '/kb',
                data: {
                    title: 'Group GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "groupgr@app": {
                        templateUrl: 'app/master/groupGR/groupGR.html',
                        controller: 'GroupGRController'
                            // templateUrl: 'app/crm/customerData/cusdata.html',
                            // controller: 'CusDataController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master');
                    }]

                },
                params: {
                    tab: null
                },
            })
            // .state('app.groupgr', {
                // //url: '/kb',
                // data: {
                    // title: 'Group GR',
                    // access: 1
                // },
                // deepStateRedirect: true,
                // sticky: true,
                // views: {
                    // "groupgr@app": {
                        // templateUrl: 'app/master/groupGR/groupGR.html',
                        // controller: 'GroupGRController'
                            // // templateUrl: 'app/crm/customerData/cusdata.html',
                            // // controller: 'CusDataController'
                    // }
                // },
                // resolve: {
                    // sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        // return $ocLazyLoad.load('sysapp');
                    // }],
                    // AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        // return $ocLazyLoad.load('app');
                    // }]

                // },
                // params: {
                    // tab: null
                // },
            // })

        // crm //
        // .state('app.cusdata', {
        //     //url: '/kb',
        //     data: {
        //         title: 'Customer Data',
        //         access: 1
        //     },
        //     deepStateRedirect: true,
        //     sticky: true,
        //     views: {
        //         "cusdata@app": {
        //             templateUrl: 'app/crm/customerData/cusdata.html',
        //             controller: 'StallController'
        //         }
        //     },
        //     resolve: {
        //                sysapp: ['$ocLazyLoad',function($ocLazyLoad){
        //                  return $ocLazyLoad.load('sysapp');
        //                }],
        //                AppModule: ['$ocLazyLoad',function($ocLazyLoad){
        //                  return $ocLazyLoad.load('app_services');
        //                }]

        //     },
        //     params: { tab : null },
        // })
        // crm //
        .state('app.vehiclefare', {
                //url: '/kb',
                data: {
                    title: 'Vehicle Fare',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "vehiclefare@app": {
                        templateUrl: 'app/services/workshopSettings/vehicleFare/vehicleFare.html',
                        controller: 'vehicleFareController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.mrslist', {
                //url: '/kb',
                data: {
                    title: 'MRS list',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "mrslist@app": {
                        templateUrl: 'app/services/mrs/mrsList/mrsList.html',
                        controller: 'mrsListController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.costumerrequest', {
                //url: '/kb',
                data: {
                    title: 'Customer Request',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "costumerrequest@app": {
                        templateUrl: 'app/services/mrs/customerRequest/customerRequest.html',
                        controller: 'customerRequestController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.staffmanagement', {
                //url: '/kb',
                data: {
                    title: 'Staff Management',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "staffmanagement@app": {
                        templateUrl: 'app/services/staffManagement/staffManagement.html',
                        controller: 'staffManagementController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.workingshift', {
                //url: '/kb',
                data: {
                    title: 'Working Shift',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "workingshift@app": {
                        templateUrl: 'app/services/workshopSettings/workingShift/workingShift.html',
                        controller: 'workingShiftController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.appointmentgr', {
                //url: '/kb',
                data: {
                    title: 'Appointment Service GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "appointmentgr@app": {
                        templateUrl: 'app/services/appointment/appointmentServiceGr/appointmentGrService.html',
                        controller: 'appointmentGrServiceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.appointmentbp', {
                //url: '/kb',
                data: {
                    title: 'Appointment Service BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "appointmentbp@app": {
                        templateUrl: 'app/services/appointment/appointmentServiceBp/appointmentBpService.html',
                        controller: 'appointmentBpServiceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('boards');
                    }],
                    summernote: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('summernote');
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider');
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.followupgr', {
                //url: '/kb',
                data: {
                    title: 'Follow UP Appointment GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "followupgr@app": {
                        templateUrl: 'app/services/appointment/followupAppointmentGr/followupAppointmentGr.html',
                        controller: 'followupAppointmentGrController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]


                },
                params: {
                    tab: null
                },
            })

        .state('app.followupbp', {
                //url: '/kb',
                data: {
                    title: 'Follow UP Appointment BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "followupbp@app": {
                        templateUrl: 'app/services/appointment/followupAppointmentBp/followupAppointmentBp.html',
                        controller: 'followupAppointmentBpController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('boards');
                    }],
                    summernote: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('summernote');
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider');
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.queueCategory', {
                //url: '/kb',
                data: {
                    title: 'Queue Category',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "queueCategory@app": {
                        templateUrl: 'app/services/queueCategory/queueCategory.html',
                        controller: 'QueueCategoryController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.towing', {
                //url: '/kb',
                data: {
                    title: 'Towing',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "towing@app": {
                        templateUrl: 'app/services/reception/towing/towing.html',
                        controller: 'TowingController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_gate_towing')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            // ============ Backup ====
            .state('app.opllist', {
                //url: '/kb',
                data: {
                    title: 'Opl List',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "opllist@app": {
                        templateUrl: 'app/services/reception/oplList/oplList.html',
                        controller: 'oplListController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.oplteknisi', {
                //url: '/kb',
                data: {
                    title: 'Opl Teknisi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "oplteknisi@app": {
                        templateUrl: 'app/services/reception/oplTeknisi/oplTeknisi.html',
                        controller: 'oplTeknisiController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.wacmaintaince', {
                //url: '/kb',
                data: {
                    title: 'Wac Maintain',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "wacmaintaince@app": {
                        templateUrl: 'app/services/reception/wacMaintain/wacMaintain.html',
                        controller: 'WacMaintainController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.towingFollowUp', {
                //url: '/kb',
                data: {
                    title: 'Towing Follow Up',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "towingFollowUp@app": {
                        templateUrl: 'app/services/reception/towingFollowup/towingFollowup.html',
                        controller: 'TowingFollowUpController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.preDiagnose', {
                //url: '/kb',
                data: {
                    title: 'Pre Diagnose',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "preDiagnose@app": {
                        templateUrl: 'app/services/preDiagnose/preDiagnose.html',
                        controller: 'PreDiagnoseController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.wobp', {
                data: {
                    title: 'WO (BP)',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "wobp@app": {
                        templateUrl: 'app/services/reception/wo_bp/mainmenu.html',
                        controller: 'WOBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_reception_wobp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    summernote: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('summernote')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    lib: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('lib')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],

                },
                params: {
                    tab: null
                },
            })
            .state('app.wosatelitebp', {
                data: {
                    title: 'WO Satellite BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "wosatelitebp@app": {
                        templateUrl: 'app/services/reception/wo_bp/mainmenu.html',
                        controller: 'WOBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_reception_wobp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    summernote: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('summernote')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    lib: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('lib')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],

                },
                params: {
                    tab: null
                },
            })
            .state('app.wo', {
                //url: '/kb',
                data: {
                    title: 'WO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "wo@app": {
                        templateUrl: 'app/services/reception/wo/mainmenu.html',
                        controller: 'WOController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_reception_wogr')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    asbBoards: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('asbBoards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});;
                    }],
                    lib: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('lib')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});;
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.queueTaking', {
                //url: '/kb',
                data: {
                    title: 'Queue Taking',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "queueTaking@app": {
                        templateUrl: 'app/services/reception/queueTaking/queueTaking.html',
                        controller: 'queueTakingController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            /*//sudah tidak di pakai boleh di hapus
            .state('app.queueSetting', {
              //url: '/kb',
              data: {
                title: 'Queue Setting',
                access: 1
              },
              deepStateRedirect: true,
              sticky: true,
              views: {
                "queueSetting@app": {
                  templateUrl: 'app/services/reception/queueSetting/queueSetting.html',
                  controller: 'queueSettingController'
                }
              },
              resolve: {
                sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                  return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
                AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                  return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }]

              },
              params: {
                tab: null
              },
            })*/
            .state('app.jobDispatch', {
                //url: '/kb',
                data: {
                    title: 'Job Dispatch',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "jobDispatch@app": {
                        templateUrl: 'app/services/production/jobDispatch/jobDispatch.html',
                        controller: 'JobDispatchController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.attendanceBP', {
                //url: '/kb',
                data: {
                    title: 'Attendance (BP)',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "attendance@app": {
                        templateUrl: 'app/services/production/attendance/attendance.html',
                        controller: 'AttendanceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.AttendanceGR', {
                //url: '/kb',
                data: {
                    title: 'Attendance (GR)',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "AttendanceGR@app": {
                        templateUrl: 'app/services/production/attendanceGR/attendanceGR.html',
                        controller: 'AttendanceGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })

        .state('app.repairProcessBP', {
                //url: '/kb',
                data: {
                    title: 'Repair Process (BP)',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "repairProcessBP@app": {
                        templateUrl: 'app/services/production/repairProcessBP/repairProcessBP.html',
                        controller: 'RepairProcessBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    summernote: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('summernote');
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider');
                    }],

                },
                params: {
                    tab: null
                },
            })
            .state('app.repairProcessGR', {
                //url: '/kb',
                data: {
                    title: 'Repair Process (GR)',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "repairProcessGR@app": {
                        templateUrl: 'app/services/production/repairProcessGR/repairProcessGR.html',
                        controller: 'RepairProcessGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider');
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.repairSupportActivity', {
                //url: '/kb',
                data: {
                    title: 'Repair Support Activity',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "repairSupportActivity@app": {
                        templateUrl: 'app/services/production/repairSupportActivity/repairSupportActivity.html',
                        controller: 'RepairSupportActivityController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.repairSupportActivityGR', {
                //url: '/kb',
                data: {
                    title: 'Repair Support Activity (GR)',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "repairSupportActivityGR@app": {
                        templateUrl: 'app/services/production/repairSupportActivityGR/repairSupportActivityGR.html',
                        controller: 'RepairSupportActivityGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.followupService', {
                //url: '/kb',
                data: {
                    title: 'Follow Up Service',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "followupService@app": {
                        templateUrl: 'app/services/psfu/followupService/followupService.html',
                        controller: 'followupServiceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.followuphistory', {
                //url: '/kb',
                data: {
                    title: 'Follow Up History',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "followuphistory@app": {
                        templateUrl: 'app/services/psfu/followupHistory/followupHistory.html',
                        controller: 'followupHistoryController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.estimasi', {
                //url: '/kb',
                data: {
                    title: 'Estimasi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "estimasi@app": {
                        templateUrl: 'app/services/estimasi/estimasi.html',
                        controller: 'estimasiController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.questionmanagement', {
                //url: '/kb',
                data: {
                    title: 'Question Management',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "questionmanagement@app": {
                        templateUrl: 'app/services/psfu/questionManagement/questionManagement.html',
                        controller: 'questionManagementController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.washing', {
                //url: '/kb',
                data: {
                    title: 'Washing',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "washing@app": {
                        templateUrl: 'app/services/delivery/washing/washing.html',
                        controller: 'WashingController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.technicalcompletion', {
                //url: '/kb',
                data: {
                    title: 'Technical Completion',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "technicalcompletion@app": {
                        templateUrl: 'app/services/delivery/teco/Technicalcompletion.html',
                        controller: 'TechnicalcompletionController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.callcustomer', {
                //url: '/kb',
                data: {
                    title: 'Call Customer',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "callcustomer@app": {
                        templateUrl: 'app/services/delivery/callCustomer/callCustomer.html',
                        controller: 'callCustomerController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.serviceexplanation', {
                //url: '/kb',
                data: {
                    title: 'Service Explanation',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "serviceexplanation@app": {
                        templateUrl: 'app/services/delivery/serviceExplanation/serviceExplanation.html',
                        controller: 'serviceExplanationController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.bilingreprint', {
                //url: '/kb',
                data: {
                    title: 'Reprint Billing',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "bilingreprint@app": {
                        templateUrl: 'app/services/delivery/billing/reprint_Billing.html',
                        controller: 'ReprintBillingController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.woHistoryGR', {
                //url: '/kb',
                data: {
                    title: 'WO List (GR)',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "woHistoryGR@app": {
                        templateUrl: 'app/services/reception/woHistoryGR/woHistoryGR.html',
                        controller: 'WoHistoryGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_reception_wolistgr')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    asbBoards: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('asbBoards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    lib: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('lib')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.bstkList', {
                //url: '/kb',
                data: {
                    title: 'BSTK List',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "bstkList@app": {
                        templateUrl: 'app/services/reception/bstkList/bstkList.html',
                        controller: 'BstkListController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_reception_bstklist')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    slider: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('slider')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    lib: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('lib')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.listPDSGR', {
                //url: '/kb',
                data: {
                    title: 'List PDS GR*',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "listPDSGR@app": {
                        templateUrl: 'app/services/reception/listPDSGR/listPDSGR.html',
                        controller: 'listPDSGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.woListBP', {
                //url: '/kb',
                data: {
                    title: 'WO List BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "woListBP@app": {
                        templateUrl: 'app/services/reception/woHistoryBP/woHistoryBP.html',
                        controller: 'WoHistoryBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_reception_wolistbp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    slider: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('slider')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    lib: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('lib')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.billing', {
                //url: '/kb',
                data: {
                    title: 'Washing',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "billing@app": {
                        templateUrl: 'app/services/delivery/billing/billing.html',
                        controller: 'billingController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.warranty', {
                //url: '/kb',
                data: {
                    title: 'Warranty',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "warranty@app": {
                        templateUrl: 'app/services/delivery/warranty/warranty.html',
                        controller: 'WarrantyController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_delivery_warranty')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: {
                    tab: null
                },
            })
            // ------------------------------------------------ REPORT ------------------------------------------------
            .state('app.completenessData', {
                // url: '/org',
                data: {
                    title: 'Completeness Data',
                    access: 0,
                    roleId:1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "completenessData@app": {
                        templateUrl: 'app/services/report/CRM/CompletenessData/CompletenessData.html',
                        controller: 'CompletenessDataController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null},
            })
            .state('app.customerAccountCreated', {
                // url: '/org',
                data: {
                    title: 'Customer Account Created',
                    access: 0,
                    roleId:1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerAccountCreated@app": {
                        templateUrl: 'app/services/report/CRM/CustomerAccountCreated/CustomerAccountCreated.html',
                        controller: 'CustomerAccountCreatedController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null},
            })
            .state('app.psfuBP', {
                // url: '/org',
                data: {
                    title: 'psfuBP',
                    access: 0,
                    roleId:1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "psfuBP@app": {
                        templateUrl: 'app/services/report/BP/PSFUBP/psfuTemplate.html',
                        controller: 'PsfuBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch'},
            })
            .state('app.psfuBPHO', {
                // url: '/org',
                data: {
                    title: 'psfuBP',
                    access: 0,
                    roleId:1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "psfuBPHO@app": {
                        templateUrl: 'app/services/report/BP/PSFUBP/psfuTemplate.html',
                        controller: 'PsfuBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'HO'},
            })
            // done
            .state('app.KPILaporanBulananBengkelBP', {
                // url: '/org',
                data: {
                    title: 'KPI Laporan Bulanan Bengkel BP',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "KPILaporanBulananBengkelBP@app": {
                        templateUrl: 'app/services/report/BP/KPILaporanBulananBengkelBP/KPILaporanBulananBengkelBP.html',
                        controller: 'ReportKPIBulananBengkelBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })    
            
            //tambahan CR4 after sales by Galang

            .state('app.partsinfoestimasi', {
                //url: '/kb',
                data: {
                    title: 'Parts Info Estimasi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "partsinfoestimasi@app": {
                        templateUrl: 'app/services/partsInfoEstimasi/partsInfoEstimasi.html',
                        controller: 'partsInfoEstimasiController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],

                },
                params: {
                    tab: null
                },
            })


            .state('app.estimasibp', {
                //url: '/kb',
                data: {
                    title: 'Estimasi BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "estimasibp@app": {
                        templateUrl: 'app/services/estimasiBP/estimasiBP.html',
                        controller: 'estimasiBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_estimasiBP')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],

                },
                params: {
                    tab: null
                },
            })

            


            .state('app.reportOPL', {
                //url: '/kb',
                data: {
                    title: 'Report OPL Internal & External',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportOPL@app": {
                        templateUrl: 'app/services/report/GR/reportOPL/reportOPL.html',
                        controller: 'reportOPLController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],

                },
                params: {
                    tab: null
                },
            })


            .state('app.reportOPLTeknisi', {
                //url: '/kb',
                data: {
                    title: 'Report OPL Teknisi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportOPLTeknisi@app": {
                        templateUrl: 'app/services/report/BP/reportOPLTeknisi/reportOPLTeknisi.html',
                        controller: 'reportOPLTeknisiController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],

                },
                params: {
                    tab: null
                },
            })

            .state('app.reportPembelianPartsBahan', {
                //url: '/kb',
                data: {
                    title: 'Report Pembelian Parts & Bahan',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPembelianPartsBahan@app": {
                        templateUrl: 'app/services/report/BP/reportPembelianPartsBahan/reportPembelianPartsBahan.html',
                        controller: 'reportPembelianPartsBahanController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],

                },
                params: {
                    tab: null
                },
            })


            //tambahan CR4 after sales by Galang

            .state('app.reportAppointmentBP', {
                // url: '/org',
                data: {
                    title: 'reportAppointmentBP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportAppointmentBP@app": {
                        templateUrl: 'app/services/report/BP/reportAppointmentBP/reportAppointmentBP.html',
                        controller: 'ReportAppointmentBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch' },
            })
            .state('app.reportAppointmentBPHO', {
                // url: '/org',
                data: {
                    title: 'reportAppointmentBPHO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportAppointmentBPHO@app": {
                        templateUrl: 'app/services/report/BP/reportAppointmentBP/reportAppointmentBP.html',
                        controller: 'ReportAppointmentBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO' },
            })
            .state('app.reportLeadTimeBP', {
                // url: '/org',
                data: {
                    title: 'reportLeadTimeBP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportLeadTimeBP@app": {
                        templateUrl: 'app/services/report/BP/reportLeadTimeBP/reportLeadTimeBP.html',
                        controller: 'ReportLeadTimeBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch'},
            })
            .state('app.reportLeadTimeBPHO', {
                // url: '/org',
                data: {
                    title: 'reportLeadTimeBPHO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportLeadTimeBPHO@app": {
                        templateUrl: 'app/services/report/BP/reportLeadTimeBP/reportLeadTimeBP.html',
                        controller: 'ReportLeadTimeBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO' },
            })
            .state('app.reportPengMaterialBahanBP', {
                // url: '/org',
                data: {
                    title: 'reportPengMaterialBahanBP',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPengMaterialBahanBP@app": {
                        templateUrl: 'app/services/report/BP/reportPengMaterialBahan/reportPengMaterialBahan.html',
                        controller: 'ReportPengMaterialBahanController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            .state('app.reportProductivity', {
                // url: '/org',
                data: {
                    title: 'reportProductivity',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportProductivity@app": {
                        templateUrl: 'app/services/report/BP/reportProductivity/reportProductivity.html',
                        controller: 'ReportProductivityBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch' },
            })
            .state('app.reportProductivityHO', {
                // url: '/org',
                data: {
                    title: 'reportProductivityHO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportProductivityHO@app": {
                        templateUrl: 'app/services/report/BP/reportProductivity/reportProductivity.html',
                        controller: 'ReportProductivityBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO' },
            })
            .state('app.reportQualityBP', {
                // url: '/org',
                data: {
                    title: 'reportQualityBP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportQualityBP@app": {
                        templateUrl: 'app/services/report/BP/reportQualityBP/reportQualityBP.html',
                        controller: 'ReportQualityBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            .state('app.reportSAProductivityBP', {
                // url: '/org',
                data: {
                    title: 'reportSAProductivityBPBranch',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportSAProductivityBP@app": {
                        templateUrl: 'app/services/report/BP/reportSAProductivityBP/reportSAProductivityBP.html',
                        controller: 'ReportSAProductivityBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch' },
            })
            .state('app.reportSAProductivityBPHO', {
                // url: '/org',
                data: {
                    title: 'reportSAProductivityBPHO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportSAProductivityBPHO@app": {
                        templateUrl: 'app/services/report/BP/reportSAProductivityBP/reportSAProductivityBP.html',
                        controller: 'ReportSAProductivityBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO' },
            })
            .state('app.reportServiceUnitbyModelBP', {
                // url: '/org',
                data: {
                    title: 'reportServiceUnitbyModelBP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportServiceUnitbyModelBP@app": {
                        templateUrl: 'app/services/report/BP/reportServiceUnitbyModelBP/reportServiceUnitbyModelBP.html',
                        controller: 'ReportServiceUnitbyModelBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            .state('app.revenueBranch_BP', {
                // url: '/org',
                data: {
                    title: 'revenueBranch_BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "revenueBranch_BP@app": {
                        templateUrl: 'app/services/report/BP/revenueBP/revenueTemplate.html',
                        controller: 'RevenueBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, kategori: 'Body and Paint', role:'Branch' },
            })
            .state('app.revenueHO_BP', {
                // url: '/org',
                data: {
                    title: 'revenueHO_BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "revenueHO_BP@app": {
                        templateUrl: 'app/services/report/BP/revenueBP/revenueTemplate.html',
                        controller: 'RevenueBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, kategori: 'Body and Paint', role:'HO' },
            })
            .state('app.reportAdditionalSpk', {
                // url: '/org',
                data: {
                    title: 'Report Additional SPK',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportAdditionalSpk@app": {
                        templateUrl: 'app/services/report/BP/reportAdditionalSpk/reportAdditionalSpk.html',
                        controller: 'ReportAdditionalSpkController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            // done
            .state('app.reportCustomerInBP', {
                // url: '/org',
                data: {
                    title: 'Report Customer In BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportCustomerInBP@app": {
                        templateUrl: 'app/services/report/BP/reportCustomerInBP/reportCustomerInBP.html',
                        controller: 'ReportCustomerInBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch'},
            })  
            .state('app.reportCustomerInBPHO', {
                // url: '/org',
                data: {
                    title: 'Report Customer In BP HO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportCustomerInBPHO@app": {
                        templateUrl: 'app/services/report/BP/reportCustomerInBP/reportCustomerInBP.html',
                        controller: 'ReportCustomerInBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'HO'},
            })            
            // done
            .state('app.workshopCombinedAchievementBP', {
                // url: '/org',
                data: {
                    title: 'Report Workshop Combined Achievement BP',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "workshopCombinedAchievementBP@app": {
                        templateUrl: 'app/services/report/BP/workshopCombinedAchievementBP/workshopBP.html',
                        controller: 'WorkshopBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            .state('app.WIPReportBP', {
                // url: '/org',
                data: {
                    title: 'WIPReportBP',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "WIPReportBP@app": {
                        templateUrl: 'app/services/report/WIPReport/WIPReport.html',
                        controller: 'WIPReportController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role :'BP' },
            })
            .state('app.WIPReportGR', {
                // url: '/org',
                data: {
                    title: 'WIPReportGR',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "WIPReportGR@app": {
                        templateUrl: 'app/services/report/WIPReport/WIPReport.html',
                        controller: 'WIPReportController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role :'GR' },
            })            
            .state('app.psfuGRBranch', {
                // url: '/org',
                data: {
                    title: 'psfuGRBranch',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "psfuGRBranch@app": {
                        templateUrl: 'app/services/report/GR/PSFUGR/psfuTemplate.html',
                        controller: 'PsfuGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch'},
            })
            .state('app.psfuGRHO', {
                // url: '/org',
                data: {
                    title: 'psfuGRHO',
                    access: 0,
                    roleId:0
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "psfuGRHO@app": {
                        templateUrl: 'app/services/report/GR/PSFUGR/psfuTemplate.html',
                        controller: 'PsfuGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO'},
            })
            .state('app.reception', {
                // url: '/org',
                data: {
                    title: 'reception',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reception@app": {
                        templateUrl: 'app/services/report/GR/reception/reception.html',
                        controller: 'ReceptionController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            // done
            .state('app.KPILaporanBulananBengkelGR', {
                // url: '/org',
                data: {
                    title: 'KPILaporanBulananBengkelGR',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "KPILaporanBulananBengkelGR@app": {
                        templateUrl: 'app/services/report/GR/KPILaporanBulananBengkelGR/KPILaporanBulananBengkelGR.html',
                        controller: 'ReportKPIBulananBengkelGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            .state('app.reportAppointmentGR', {
                // url: '/org',
                data: {
                    title: 'Report Appointment GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportAppointmentGR@app": {
                        templateUrl: 'app/services/report/GR/reportAppointmentGR/reportAppointmentGR.html',
                        controller: 'ReportAppointmentGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch' },
            })
            .state('app.reportAppointmentGRHO', {
                // url: '/org',
                data: {
                    title: 'Report Appointment GR HO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportAppointmentGRHO@app": {
                        templateUrl: 'app/services/report/GR/reportAppointmentGR/reportAppointmentGR.html',
                        controller: 'ReportAppointmentGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO' },
            })
            // done
            .state('app.reportCustomerFieldActionGR', {
                // url: '/org',
                data: {
                    title: 'Report Customer Field Action GR',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportCustomerFieldActionGR@app": {
                        templateUrl: 'app/services/report/GR/reportCustomerFieldActionGR/reportCustomerFieldActionGR.html',
                        controller: 'ReportCustomerFieldActionGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            // done
            .state('app.reportCustomerInGR', {
                // url: '/org',
                data: {
                    title: 'Report Customer In GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportCustomerInGR@app": {
                        templateUrl: 'app/services/report/GR/reportCustomerInGR/reportCustomerInGR.html',
                        controller: 'ReportCustomerInGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch'},
            })
            .state('app.reportCustomerInGRHO', {
                // url: '/org',
                data: {
                    title: 'Report Customer In GR HO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportCustomerInGRHO@app": {
                        templateUrl: 'app/services/report/GR/reportCustomerInGR/reportCustomerInGR.html',
                        controller: 'ReportCustomerInGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO' },
            })
            // done
            .state('app.reportLeadTimeGR', {
                // url: '/org',
                data: {
                    title: 'Report Lead Time GR',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportLeadTimeGR@app": {
                        templateUrl: 'app/services/report/GR/reportLeadTimeGR/reportLeadTimeGR.html',
                        controller: 'ReportLeadTimeGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch' },
            })
            .state('app.reportLeadTimeGRHO', {
                // url: '/org',
                data: {
                    title: 'Report Lead Time GR HO',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportLeadTimeGRHO@app": {
                        templateUrl: 'app/services/report/GR/reportLeadTimeGR/reportLeadTimeGR.html',
                        controller: 'ReportLeadTimeGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO' },
            })
            // done
            .state('app.reportMRSGR', {
                // url: '/org',
                data: {
                    title: 'Report MRS GR',
                    access: 1,
                    // roleId: 2,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportMRSGR@app": {
                        templateUrl: 'app/services/report/GR/reportMRS/reportMRS.html',
                        controller: 'ReportMRSGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch' },
            })
             .state('app.reportMRSGRHO', {
                // url: '/org',
                data: {
                    title: 'Report MRS GR HO',
                    access: 1,
                    // roleId: 2,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportMRSGRHO@app": {
                        templateUrl: 'app/services/report/GR/reportMRS/reportMRS.html',
                        controller: 'ReportMRSGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'HO'},
            })
            .state('app.reportPengMaterialBahan', {
                // url: '/org',
                data: {
                    title: 'reportPengMaterialBahan',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPengMaterialBahan@app": {
                        templateUrl: 'app/services/report/GR/reportPengMaterialBahan/reportPengMaterialBahan.html',
                        controller: 'ReportPengMaterialBahanGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            .state('app.reportProductivityStallHO', {
                // url: '/org',
                data: {
                    title: 'reportProductivityStallHO',
                    access: 0,
                    roleID: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportProductivityStallHO@app": {
                        templateUrl: 'app/services/report/GR/reportProductivityStall/reportProductivityStall.html',
                        controller: 'ReportGRProductivityStallController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'HO'},
            })
            .state('app.reportProductivityStall', {
                // url: '/org',
                data: {
                    title: 'reportProductivityStall',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportProductivityStall@app": {
                        templateUrl: 'app/services/report/GR/reportProductivityStall/reportProductivityStall.html',
                        controller: 'ReportGRProductivityStallController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch' },
            })
            .state('app.reportSAProductivityGRHO', {
                // url: '/org',
                data: {
                    title: 'reportSAProductivityGRHO',
                    access: 0,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportSAProductivityGRHO@app": {
                        templateUrl: 'app/services/report/GR/reportSAProductivityGR/reportSAProductivityGR.html',
                        controller: 'ReportSAProductivityGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO' },
            })
            .state('app.reportSAProductivityGRBranch', {
                // url: '/org',
                data: {
                    title: 'reportSAProductivityGRBranch',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportSAProductivityGRBranch@app": {
                        templateUrl: 'app/services/report/GR/reportSAProductivityGR/reportSAProductivityGR.html',
                        controller: 'ReportSAProductivityGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch'},
            })
            // done
            .state('app.reportServiceUnitbyModelGR', {
                // url: '/org',
                data: {
                    title: 'Report Service Unit by Model GR',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportServiceUnitbyModelGR@app": {
                        templateUrl: 'app/services/report/GR/reportServiceUnitbyModelGR/reportServiceUnitbyModelGR.html',
                        controller: 'ReportServiceUnitbyModelGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            // done
            .state('app.revenueGR', {
                // url: '/org',
                data: {
                    title: 'Report Revenue GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "revenueGR@app": {
                        templateUrl: 'app/services/report/GR/revenueGR/revenueTemplate.html',
                        controller: 'RevenueGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch',kategori: 'General Repair'},
            })
            .state('app.revenueGRHO', {
                // url: '/org',
                data: {
                    title: 'Report Revenue GR HO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "revenueGRHO@app": {
                        templateUrl: 'app/services/report/GR/revenueGR/revenueTemplate.html',
                        controller: 'RevenueGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO',kategori: 'General Repair' },
            })
            // done
            .state('app.TechnicianProductivityGR', {
                // url: '/org',
                data: {
                    title: 'Report Technician Productivity GR',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "TechnicianProductivityGR@app": {
                        templateUrl: 'app/services/report/GR/TechnicianProductivityGR/TechnicianProductivityGRBranch.html',
                        controller: 'TechnicianProductivityGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role : 'Branch'},
            })
            .state('app.TechnicianProductivityGRHO', {
                // url: '/org',
                data: {
                    title: 'Report Technician Productivity GR HO',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "TechnicianProductivityGRHO@app": {
                        templateUrl: 'app/services/report/GR/TechnicianProductivityGR/TechnicianProductivityGRHO.html',
                        controller: 'TechnicianProductivityGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role : 'HO'},
            })
            // done
            .state('app.workshopCombinedAchievementGR', {
                // url: '/org',
                data: {
                    title: 'Report Workshop Combined Achievement GR',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "workshopCombinedAchievementGR@app": {
                        templateUrl: 'app/services/report/GR/workshopCombinedAchievementGR/workshopGR.html',
                        controller: 'WorkshopGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null },
            })
            .state('app.reportInventoryPositionGR', {
                // url: '/org',
                data: {
                    title: 'reportInventoryPosition',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportInventoryPositionGR@app": {
                        templateUrl: 'app/services/report/Parts/reportInventoryPosition/reportInventoryPosition.html',
                        controller: 'ReportInventoryPositionController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'GR'},
            })
            .state('app.reportInventoryPositionBP', {
                // url: '/org',
                data: {
                    title: 'reportInventoryPosition',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportInventoryPositionBP@app": {
                        templateUrl: 'app/services/report/Parts/reportInventoryPosition/reportInventoryPosition.html',
                        controller: 'ReportInventoryPositionController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'BP'},
            })
            .state('app.reportPartsClaimGR', {
                // url: '/org',
                data: {
                    title: 'reportPartsClaim',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsClaimGR@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsClaim/reportPartsClaim.html',
                        controller: 'ReportPartsClaimController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori : 'GR'},
            })
            .state('app.reportPartsClaimBP', {
                // url: '/org',
                data: {
                    title: 'reportPartsClaim',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsClaimBP@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsClaim/reportPartsClaim.html',
                        controller: 'ReportPartsClaimController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori : 'BP'},
            })
            // done
            .state('app.reportPartsDisposalGR', {
                // url: '/org',
                data: {
                    title: 'Report Parts Disposal',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsDisposalGR@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsDisposal/reportPartsDisposal.html',
                        controller: 'ReportPartsDisposalController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'GR'},
            })
            .state('app.reportPartsDisposalBP', {
                // url: '/org',
                data: {
                    title: 'Report Parts Disposal',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsDisposalBP@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsDisposal/reportPartsDisposal.html',
                        controller: 'ReportPartsDisposalController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'BP'},
            })
            .state('app.reportPartsPerformanceGR', {
                // url: '/org',
                data: {
                    title: 'reportPartsPerformance',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsPerformanceGR@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsPerformance/reportPartsPerformance.html',
                        controller: 'ReportPartsPerformanceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch', kategori:'GR'},
            })
            .state('app.reportPartsPerformanceBP', {
                // url: '/org',
                data: {
                    title: 'reportPartsPerformance',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsPerformanceBP@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsPerformance/reportPartsPerformance.html',
                        controller: 'ReportPartsPerformanceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'Branch', kategori:'BP'},
            })
            .state('app.reportPartsPerformanceHOGR', {
                // url: '/org',
                data: {
                    title: 'reportPartsPerformanceHO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsPerformanceHOGR@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsPerformance/reportPartsPerformance.html',
                        controller: 'ReportPartsPerformanceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'HO',kategori:'GR'},
            })
            .state('app.reportPartsPerformanceHOBP', {
                // url: '/org',
                data: {
                    title: 'reportPartsPerformanceHO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsPerformanceHOBP@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsPerformance/reportPartsPerformance.html',
                        controller: 'ReportPartsPerformanceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'HO',kategori:'BP'},
            })
            // .state('app.reportPartsPerformanceWO', {
            //     // url: '/org',
            //     data: {
            //         title: 'reportPartsPerformanceWO',
            //         access: 0,
            //         roleID: 2,
            //     },
            //     deepStateRedirect: true,
            //     sticky: true,
            //     views: {
            //         "reportPartsPerformanceWOHO@app": {
            //             templateUrl: 'app/services/report/Parts/reportPartsPerformance/reportPartsPerformance.html',
            //             controller: 'ReportPartsPerformanceController'
            //         }
            //     },
            //     resolve: {
            //         sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
            //             return $ocLazyLoad.load('sysapp');
            //         }],
            //     },
            //     params: { tab: null },
            // })
            // .state('app.reportPartsPerformanceWOBranch', {
            //     // url: '/org',
            //     data: {
            //         title: 'reportPartsPerformanceWOBranch',
            //         access: 0,
            //         roleID: 1,
            //     },
            //     deepStateRedirect: true,
            //     sticky: true,
            //     views: {
            //         "reportPartsPerformanceWOBranch@app": {
            //             templateUrl: 'app/services/report/Parts/reportPartsPerformance/reportPartsPerformance.html',
            //             controller: 'ReportPartsPerformanceController'
            //         }
            //     },
            //     resolve: {
            //         sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
            //             return $ocLazyLoad.load('sysapp');
            //         }],
            //     },
            //     params: { tab: null },
            // })
            // .state('app.reportPartsPerformanceInventory', {
            //     // url: '/org',
            //     data: {
            //         title: 'reportPartsPerformanceInventory',
            //         access: 0,
            //         roleID: 2,
            //     },
            //     deepStateRedirect: true,
            //     sticky: true,
            //     views: {
            //         "reportPartsPerformanceInventoryHO@app": {
            //             templateUrl: 'app/services/report/Parts/reportPartsPerformance/reportPartsPerformance.html',
            //             controller: 'ReportPartsPerformanceController'
            //         }
            //     },
            //     resolve: {
            //         sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
            //             return $ocLazyLoad.load('sysapp');
            //         }],
            //     },
            //     params: { tab: null },
            // })
            // .state('app.reportPartsPerformanceInventoryBranch', {
            //     // url: '/org',
            //     data: {
            //         title: 'reportPartsPerformanceInventoryBranch',
            //         access: 0,
            //         roleID: 1,
            //     },
            //     deepStateRedirect: true,
            //     sticky: true,
            //     views: {
            //         "reportPartsPerformanceInventoryBranch@app": {
            //             templateUrl: 'app/services/report/Parts/reportPartsPerformance/reportPartsPerformance.html',
            //             controller: 'ReportPartsPerformanceController'
            //         }
            //     },
            //     resolve: {
            //         sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
            //             return $ocLazyLoad.load('sysapp');
            //         }],
            //     },
            //     params: { tab: null },
            // })
            .state('app.reportPartsReturnGR', {
                // url: '/org',
                data: {
                    title: 'reportPartsReturn',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsReturnGR@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsReturn/reportPartsReturn.html',
                        controller: 'ReportPartsReturnController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'GR'},
            })
            .state('app.reportPartsReturnBP', {
                // url: '/org',
                data: {
                    title: 'reportPartsReturn',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsReturnBP@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsReturn/reportPartsReturn.html',
                        controller: 'ReportPartsReturnController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'BP'},
            })
            // done
            .state('app.reportPartsSalesGR', {
                // url: '/org',
                data: {
                    title: 'Report Parts Sales',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsSalesGR@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsSales/reportPartsSales.html',
                        controller: 'ReportPartsSalesController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch', kategori:'GR' },
            })
            .state('app.reportPartsSalesBP', {
                // url: '/org',
                data: {
                    title: 'Report Parts Sales',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsSalesBP@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsSales/reportPartsSales.html',
                        controller: 'ReportPartsSalesController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch', kategori:'BP' },
            })
             .state('app.reportPartsSalesHOGR', {
                // url: '/org',
                data: {
                    title: 'Report Parts Sales HO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsSalesHOGR@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsSales/reportPartsSales.html',
                        controller: 'ReportPartsSalesController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'HO',kategori:'GR'},
            })
             .state('app.reportPartsSalesHOBP', {
                // url: '/org',
                data: {
                    title: 'Report Parts Sales HO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsSalesHOBP@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsSales/reportPartsSales.html',
                        controller: 'ReportPartsSalesController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null , role:'HO',kategori:'BP'},
            })
            // done
            .state('app.reportPartsStockAdjGR', {
                // url: '/org',
                data: {
                    title: 'Report Parts Stock Adj',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsStockAdjGR@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsStockAdj/reportPartsStockAdj.html',
                        controller: 'ReportPartsStockAdjController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'GR'},
            })
             .state('app.reportPartsStockAdjBP', {
                // url: '/org',
                data: {
                    title: 'Report Parts Stock Adj',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reportPartsStockAdjBP@app": {
                        templateUrl: 'app/services/report/Parts/reportPartsStockAdj/reportPartsStockAdj.html',
                        controller: 'ReportPartsStockAdjController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'BP'},
            })
            // done
            .state('app.stockMovementGR', {
                // url: '/org',
                data: {
                    title: 'Report Stock Movement',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockMovementGR@app": {
                        templateUrl: 'app/services/report/Parts/stockMovement/stockMovement.html',
                        controller: 'StockMovementController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'GR'},
            })
            .state('app.stockMovementBP', {
                // url: '/org',
                data: {
                    title: 'Report Stock Movement',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockMovementBP@app": {
                        templateUrl: 'app/services/report/Parts/stockMovement/stockMovement.html',
                        controller: 'StockMovementController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori:'BP'},
            })
            // done
            .state('app.stockOpnameGR', {
                // url: '/org',
                data: {
                    title: 'Report Stock Opname',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockOpnameGR@app": {
                        templateUrl: 'app/services/report/Parts/stockOpname/stockOpname.html',
                        controller: 'StockOpnameController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori : 'GR'},
            })
            .state('app.stockOpnameBP', {
                // url: '/org',
                data: {
                    title: 'Report Stock Opname',
                    access: 1,
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockOpnameBP@app": {
                        templateUrl: 'app/services/report/Parts/stockOpname/stockOpname.html',
                        controller: 'StockOpnameController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null ,kategori : 'BP'},
            })
            // done
            .state('app.stockReportGR', {
                // url: '/org',
                data: {
                    title: 'Stock Report',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockReportGR@app": {
                        templateUrl: 'app/services/report/Parts/stock/stockTemplate.html',
                        controller: 'StockController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch', kategori:'GR' },
            })
            .state('app.stockReportBP', {
                // url: '/org',
                data: {
                    title: 'Stock Report',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockReportBP@app": {
                        templateUrl: 'app/services/report/Parts/stock/stockTemplate.html',
                        controller: 'StockController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'Branch', kategori:'BP' },
            })
            .state('app.stockReportHOGR', {
                // url: '/org',
                data: {
                    title: 'Stock Report HO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockReportHOGR@app": {
                        templateUrl: 'app/services/report/Parts/stock/stockTemplate.html',
                        controller: 'StockController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO', kategori:'GR' },
            })
            .state('app.stockReportHOBP', {
                // url: '/org',
                data: {
                    title: 'Stock Report HO',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockReportHOBP@app": {
                        templateUrl: 'app/services/report/Parts/stock/stockTemplate.html',
                        controller: 'StockController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_services')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]
                },
                params: { tab: null, role:'HO', kategori:'BP' },
            })
            // .state('app.CompletenessData', {
            //     // url: '/org',
            //     data: {
            //         title: 'CompletenessData',
            //         access: 0,
            //     },
            //     deepStateRedirect: true,
            //     sticky: true,
            //     views: {
            //         "CompletenessData@app": {
            //             templateUrl: 'app/services/report/CRM/CompletenessData/CompletenessData.html',
            //             controller: 'CompletenessDataController'
            //         }
            //     },
            //     resolve: {
            //         sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
            //             return $ocLazyLoad.load('sysapp');
            //         }],
            //     },
            //     params: { tab: null },
            // })       
            // .state('app.CustomerAccountCreated', {
            //     // url: '/org',
            //     data: {
            //         title: 'CustomerAccountCreated',
            //         access: 0,
            //     },
            //     deepStateRedirect: true,
            //     sticky: true,
            //     views: {
            //         "CustomerAccountCreated@app": {
            //             templateUrl: 'app/services/report/CRM/CustomerAccountCreated/CustomerAccountCreated.html',
            //             controller: 'CustomerAccountCreatedController'
            //         }
            //     },
            //     resolve: {
            //         sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
            //             return $ocLazyLoad.load('sysapp');
            //         }],
            //     },
            //     params: { tab: null },
            // })
            // ------------------------------------------------ REPORT ------------------------------------------------
        
        function errorPopUp(err, ngDialog, cfpLoadingBar){
            if (ngDialog.getOpenDialogs().length==0) {                        
                ngDialog.open({ 
                    template: '_sys/auth/dialog_err_default.html',
                    controller: ['$scope', '$timeout', function($scope) {
                        $scope.status = "Error Koneksi";
                        $scope.message = "Mohon cek koneksi anda / aplikasi sedang dalam maintenance. Silakan coba beberapa saat lagi, Terima kasih";
                        $scope.debugMode = false;
                    }] 
                });
                cfpLoadingBar.complete();
            }
        }

        if (window.history && window.history.pushState) {
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        };
        $urlRouterProvider.deferIntercept();
    }]);
