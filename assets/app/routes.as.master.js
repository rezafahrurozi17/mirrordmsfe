//"use strict";

//angular.module('app', ['ui.router','ngResource']);

angular.module('app')
    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function($locationProvider, $stateProvider, $urlRouterProvider) {

        $stateProvider
        // -------------------------------- //
        // Application Module - After Sales (services) //
        // -------------------------------- //
            .state('app.whshelf', {
                //url: '/kb',
                data: {
                    title: 'whshelf',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "whshelf@app": {
                        templateUrl: 'app/master/warehouseShelf/warehouseShelf.html',
                        controller: 'WarehouseShelfController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.tasklistgr', {
                //url: '/kb',
                data: {
                    title: 'Task List GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "tasklistgr@app": {
                        templateUrl: 'app/master/AfterSales/taskListGR/taskListGR.html',
                        controller: 'TaskListGRController'
                            // templateUrl: 'app/services/reception/queueTaking/queueTaking.html',
                            //       controller: 'queueTakingController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.counters', {
                //url: '/kb',
                data: {
                    title: 'counters',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "counters@app": {
                        templateUrl: 'app/services/counters/counters.html',
                        controller: 'CountersController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.countergr', {
                //url: '/kb',
                data: {
                    title: 'Counter GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "countergr@app": {
                        templateUrl: 'app/master/AfterSales/CounterGR/masterCounterGR.html',
                        controller: 'MasterCounterGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.counterbp', {
                //url: '/kb',
                data: {
                    title: 'Counter BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "counterbp@app": {
                        templateUrl: 'app/master/AfterSales/CounterBP/masterCounterBP.html',
                        controller: 'MasterCounterBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.asuransirekanan', {
                //url: '/kb',
                data: {
                    title: 'Asuransi Rekanan',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "asuransirekanan@app": {
                        templateUrl: 'app/master/AfterSales/asuransiRekanan/asuransiRekanan.html',
                        controller: 'AsuransiRekananController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.approvaldiscount', {
                //url: '/kb',
                data: {
                    title: 'Master Approval Discount',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "approvaldiscount@app": {
                        templateUrl: 'app/master/AfterSales/masterApprovalDiscount/masterApprovalDiscount.html',
                        controller: 'MasterApprovalDiscountController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.pertanyaanfir', {
                //url: '/kb',
                data: {
                    title: 'Pertanyaan FIR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "pertanyaanfir@app": {
                        templateUrl: 'app/master/AfterSales/penyerahanFIR/penyerahanFIR.html',
                        controller: 'PenyerahanFIRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.jawabanfir', {
                //url: '/kb',
                data: {
                    title: 'Jawaban FIR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "jawabanfir@app": {
                        templateUrl: 'app/master/AfterSales/jawabanFIR/jawabanFIR.html',
                        controller: 'JawabanFIRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.mastervendor', {
                //url: '/kb',
                data: {
                    title: 'Master Vendor',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "mastervendor@app": {
                        templateUrl: 'app/master/AfterSales/vendor/masterVendor.html',
                        controller: 'MasterVendorController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.masterpemeriksaangr', {
                //url: '/kb',
                data: {
                    title: 'Pemeriksaan Penyerahan GR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "masterpemeriksaangr@app": {
                        templateUrl: 'app/master/AfterSales/pemeriksaanGR/pemeriksaanGR.html',
                        controller: 'PemeriksaanGRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })

        .state('app.masterpemeriksaanbp', {
            //url: '/kb',
            data: {
                title: 'Pemeriksaan Penyerahan BP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterpemeriksaanbp@app": {
                    templateUrl: 'app/master/AfterSales/pemeriksaanBP/pemeriksaanBP.html',
                    controller: 'PemeriksaanBPController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
                AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
            },
            params: {
                tab: null
            },
        })

        .state('app.masterstallbp', {
            //url: '/kb',
            data: {
                title: 'Stall BP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterstallbp@app": {
                    templateUrl: 'app/master/AfterSales/stallBP/stallBP.html',
                    controller: 'StallBPController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
                AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
            },
            params: {
                tab: null
            },
        })

        .state('app.masterbpcentersatellite', {
            //url: '/kb',
            data: {
                title: 'BP Center Satellite',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterbpcentersatellite@app": {
                    templateUrl: 'app/master/AfterSales/bpCenterSatelite/bpCenterSatelite.html',
                    controller: 'BPCenterSateliteController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
                AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
            },
            params: {
                tab: null
            },
        })

        .state('app.masterhargaretailparts', {
            //url: '/kb',
            data: {
                title: 'Harga Retail Parts',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterhargaretailparts@app": {
                    templateUrl: 'app/master/AfterSales/hargaRetailParts/hargaRetailParts.html',
                    controller: 'HargaRetailPartsController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
                AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
            },
            params: {
                tab: null
            },
        })

        .state('app.masterspecialcampaign', {
            //url: '/kb',
            data: {
                title: 'Spesial Campaign Discount',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterspecialcampaign@app": {
                    templateUrl: 'app/master/AfterSales/specialCampaign/specialCampaign.html',
                    controller: 'SpecialCampaignController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
                AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                    return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                }],
            },
            params: {
                tab: null
            },
        })

        .state('app.dpparts', {
                //url: '/kb',
                data: {
                    title: 'dpparts',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "dpparts@app": {
                        templateUrl: 'app/master/dpParts/dpParts.html',
                        controller: 'DpPartsController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.orderleadtime', {
                //url: '/kb',
                data: {
                    title: 'orderleadtime',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "orderleadtime@app": {
                        templateUrl: 'app/master/LTDelivery/ltDelivery.html',
                        controller: 'OrderLeadTimeController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.customerpks', {
                //url: '/kb',
                data: {
                    title: 'customerpks',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerpks@app": {
                        templateUrl: 'app/master/AfterSales/customerPKS/customerPKS.html',
                        controller: 'CustomerPKSController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.opl', {
                //url: '/kb',
                data: {
                    title: 'opl',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "opl@app": {
                        templateUrl: 'app/master/OPL/OPL.html',
                        controller: 'OPLController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.mtrlretaildisc', {
                //url: '/kb',
                data: {
                    title: 'mtrlretaildisc',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "mtrlretaildisc@app": {
                        templateUrl: 'app/master/mtrlRetailDisc/mtrlRetailDisc.html',
                        controller: 'MtrlRetailDiscController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.sccretaildisc', {
                //url: '/kb',
                data: {
                    title: 'sccretaildisc',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "sccretaildisc@app": {
                        templateUrl: 'app/master/SCCRetailDisc/SCCRetailDisc.html',
                        controller: 'SCCRetailDiscController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.parametericc', {
                //url: '/kb',
                data: {
                    title: 'parametericc',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "parametericc@app": {
                        templateUrl: 'app/master/settingParameterICC/settingParameterICC.html',
                        controller: 'SettingParameterICCController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.stockingpolicy', {
                //url: '/kb',
                data: {
                    title: 'stockingpolicy',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stockingpolicy@app": {
                        templateUrl: 'app/master/stockingPolicy/stockingPolicy.html',
                        controller: 'StockingPolicyController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.defect', {
                //url: '/kb',
                data: {
                    title: 'Defect',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "defect@app": {
                        templateUrl: 'app/master/defect/defect.html',
                        controller: 'DefectController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.material', {
                //url: '/kb',
                data: {
                    title: 'Material',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "material@app": {
                        templateUrl: 'app/master/material/material.html',
                        controller: 'MaterialController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.parts', {
                //url: '/kb',
                data: {
                    title: 'Parts',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "parts@app": {
                        templateUrl: 'app/master/parts/parts.html',
                        controller: 'PartsController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.stallmst', {
                //url: '/kb',
                data: {
                    title: 'Stall',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "stallmst@app": {
                        templateUrl: 'app/master/stall/stall.html',
                        controller: 'StallMstController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.retailParts', {
                //url: '/kb',
                data: {
                    title: 'Retail Parts',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "retailParts@app": {
                        templateUrl: 'app/master/retailParts/retailParts.html',
                        controller: 'RetailPartsController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.tipejadwalkerja', {
                data: {
                    title: 'Tipe Jadwal Kerja',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "tipejadwalkerja@app": {
                        templateUrl: 'app/master/workingScheduleType/workingScheduleType.html',
                        controller: 'WorkingScheduleTypeController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.calendar', {
                data: {
                    title: 'Master Calendar',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "calendar@app": {
                        templateUrl: 'app/master/workingCalender/workingCalender.html',
                        controller: 'WorkingCalenderController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.harilibur', {
                data: {
                    title: 'Hari Libur',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "harilibur@app": {
                        templateUrl: 'app/master/holiday/holiday.html',
                        controller: 'HolidayController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.jamkerja', {
                data: {
                    title: 'Jam Kerja',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "jamkerja@app": {
                        templateUrl: 'app/master/workHour/workHour.html',
                        controller: 'WorkHourController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: {
                    tab: null
                },
            })
            .state('app.mastertemplatemessage', {
                //url: '/kb',
                data: {
                    title: 'Template Message',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "mastertemplatemessage@app": {
                        templateUrl: 'app/master/AfterSales/masterTemplateMessage/masterTemplateMessage.html',
                        controller: 'MasterTemplateMessageController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.mastercustomerpks', {
                //url: '/kb',
                data: {
                    title: 'Customer PKS',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "mastercustomerpks@app": {
                        templateUrl: 'app/master/AfterSales/customerPKS/customerPKS.html',
                        controller: 'CustomerPKSController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }]

                },
                params: { tab: null },
            })
            .state('app.mastertasklistbp', {
                //url: '/kb',
                data: {
                    title: 'Task List BP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "mastertasklistbp@app": {
                        templateUrl: 'app/master/AfterSales/taskListBP/taskListBP.html',
                        controller: 'TaskListBPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            }).state('app.mastercustomerdiscount', {      
                //url: '/kb',
                data: {        
                    title: 'Customer Discount',
                    access: 1      
                },
                deepStateRedirect: true,
                sticky: true,
                views: {        
                    "mastercustomerdiscount@app": {
                        templateUrl: 'app/master/AfterSales/customerDiscount/customerDiscount.html',
                        controller: 'CustomerDiscountController'        
                    }      
                },
                resolve: {        
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {          
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});        
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {          
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});        
                    }],
                          
                },
                params: {        
                    tab: null      
                },
            })
            .state('app.masterareadealer', {
                //url: '/kb',
                data: {
                    title: 'Master Area Dealer',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "masterareadealer@app": {
                        templateUrl: 'app/master/masterAreaDealer/masterAreaDealer.html',
                        controller: 'MasterAreaDealerController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                },
                params: {
                    tab: null
                },
            })
            .state('app.masterwac', {      
                //url: '/kb',
                data: {        
                    title: 'Master WAC',
                    access: 1      
                },
                deepStateRedirect: true,
                sticky: true,
                views: {        
                    "masterwac@app": {
                        templateUrl: 'app/master/AfterSales/masterWAC/masterWAC.html',
                        controller: 'MasterWACController'        
                    }      
                },
                resolve: {        
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {          
                        return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});        
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {          
                        return $ocLazyLoad.load('app_master')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});        
                    }],
                          
                },
                params: {        
                    tab: null      
                },
            })


            //Tambahan routes aftersales CR4 by Galang
            .state('app.masterdiskon', {
                //url: '/kb',
                data: {
                    title: 'Master Diskon',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "masterdiskon@app": {
                        templateUrl: 'app/master/AfterSales/MasterDiscount/MasterDiscount.html',
                        controller: 'MasterDiscountController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],

                },
                params: {
                    tab: null
                },
            })

            .state('app.masterprr', {
                //url: '/kb',
                data: {
                    title: 'Master PRR',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "masterprr@app": {
                        templateUrl: 'app/master/AfterSales/MasterPRR/MasterPRR.html',
                        controller: 'MasterPRRController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('sysapp')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function ($ocLazyLoad, ngDialog, cfpLoadingBar) {
                        return $ocLazyLoad.load('app_master')
                            .then(function (res) {

                            }, function (err) { errorPopUp(err, ngDialog, cfpLoadingBar) });
                    }],

                },
                params: {
                    tab: null
                },
            })
            ////Tambahan routes aftersales CR4 by Galang


        function errorPopUp(err, ngDialog, cfpLoadingBar){
            if (ngDialog.getOpenDialogs().length==0) {                        
                ngDialog.open({ 
                    template: '_sys/auth/dialog_err_default.html',
                    controller: ['$scope', '$timeout', function($scope) {
                        $scope.status = "Error Koneksi";
                        $scope.message = "Mohon cek koneksi anda / aplikasi sedang dalam maintenance. Silakan coba beberapa saat lagi, Terima kasih";
                        $scope.debugMode = false;
                    }] 
                });
                cfpLoadingBar.complete();
            }
        }

        if (window.history && window.history.pushState) {
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        };
        $urlRouterProvider.deferIntercept();
    }]);
