angular.module('appConfig', ['ngToast', 'ngDialog', 'ngIdle', 'pascalprecht.translate', 'ngAnimate', 'oc.lazyLoad', 'angular-loading-bar', 'myversion'])
  .config(['$httpProvider', function ($httpProvider) {
    // ISO 8601 Date Pattern: YYYY-mm-ddThh:MM:ss
    var dateMatchPattern = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/;

    var convertDates = function (obj) {
      for (var key in obj) {
        if (!obj.hasOwnProperty(key)) continue;

        var value = obj[key];
        var typeofValue = typeof (value);

        if (typeofValue === 'object') {
          // If it is an object, check within the object for dates.
          convertDates(value);
        } else if (typeofValue === 'string') {
          if (dateMatchPattern.test(value)) {
            obj[key] = new Date(value);
          }
        }
      }
    }

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    // delete $httpProvider.defaults.headers.post['Content-type'];
    // $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    // $httpProvider.defaults.headers.common['Content-Type']= 'application/x-www-form-urlencoded';
    $httpProvider.defaults.headers.post["Content-Type"] = "application/json";
    //

    //Reset headers to avoid OPTIONS request (aka preflight)
    // $httpProvider.defaults.headers.common = {};
    // $httpProvider.defaults.headers.post = {};
    // $httpProvider.defaults.headers.put = {};
    // $httpProvider.defaults.headers.patch = {};


    $httpProvider.defaults.transformResponse.push(function (data) {
      if (typeof (data) === 'object') {
        convertDates(data);
      }

      return data;
    });
  }])

  .config(['ngToastProvider', function (ngToastProvider) {
    ngToastProvider.configure({
      animation: 'slide' // or 'fade',slide
    });
  }])

  .config(['ngDialogProvider', function (ngDialogProvider) {
    ngDialogProvider.setDefaults({
      className: 'ngdialog-theme-default',
      // plain: true,
      showClose: true,
      closeByDocument: true,
      closeByEscape: true
    });
  }])

  .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.latencyThreshold = 0;
  }])

  .config(['IdleProvider', 'KeepaliveProvider', function (IdleProvider, KeepaliveProvider) {
    // Permintaan After Sales BU saat UAT
    // IdleProvider.idle(60*60*24); // in seconds
    IdleProvider.idle(28800); // 8 hour new request for production
    IdleProvider.timeout(30); // in seconds
    KeepaliveProvider.interval(10); // in seconds
  }])

  // app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  //   cfpLoadingBarProvider.latencyThreshold = 500;
  // }])

  .config(['$translateProvider', function ($translateProvider) {
    // add translation table
    $translateProvider.translations('id', langId);
    $translateProvider.translations('en', langEn);
    $translateProvider.preferredLanguage('en');
  }])

  .config(['$animateProvider', function ($animateProvider) {
    $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
  }])

  .config(['$ocLazyLoadProvider', 'appversionProvider', function ($ocLazyLoadProvider, appversionProvider) {
    $ocLazyLoadProvider.config({
      events: true,
      serie: true,
      modules: [
        //-------------------------
        // LazyLoad JS Dependencies
        //-------------------------
        {
          name: 'barcode',
          files: [
            'module/barcode/quagga.min.js',
            // 'module/barcode/index.min.js',
            // 'module/barcode/scandit-engine-sdk.min.js',
            // 'module/barcode/scandit-engine-sdk.wasm',

          ]
        }, {
          name: 'slider',
          files: [
            'module/angular-slider/dist/rzslider.min.js',
            'module/angular-slider/dist/rzslider.css',
          ],
          serie: true,
        },
        {
          name: 'summernote',
          files: [
            'module/summernote/summernote.min.js',
            'module/summernote/summernote.css',
            'module/summernote/angular-summernote.min.js',
          ],
          serie: true,
        },
        {
          name: 'freewall',
          files: [
            'module/freewall/freewall.js',
          ]
        },
        {
          name: 'moment',
          files: [
            'module/moment/moment.min.js',
            'module/moment/angular-moment.min.js',
          ],
          serie: true,
        },
        {
          name: 'agGrid',
          files: [
            'module/ag-grid/ag-grid-enterprise.min.js',
            'module/ag-grid/ag-grid.directive.js',
          ],
          serie: true,
        },
        //-------------------------
        // LazyLoad Boards
        //-------------------------
        {
          name: 'boards',
          files: [
            '/css/boards.css',
            '/js/boards.js?v=' + appversionProvider.versi,
          ],
          serie: true,
        },
        //-------------------------
        // LazyLoad ASB Boards
        //-------------------------
        {
          name: 'asbBoards',
          files: [
            '/css/boards.css',
            '/js/asbBoards.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        //-------------------------
        // LazyLoad Library
        //-------------------------
        {
          name: 'lib',
          files: [
            '/templates/templates_lib.js?v='+appversionProvider.versi,
            '/js/lib.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        //-------------------------
        // LazyLoad Sounds
        //-------------------------
        {
          name: 'sounds',
          files: [
            '/js/sounds.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        //-------------------------
        // LazyLoad Notify
        //-------------------------
        {
          name: 'notify',
          files: [
            '/js/notify.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        //-------------------------
        // LazyLoad App Modules
        //-------------------------
        {
          name: 'sysdashboard',
          files: [
            '/css/sysdashboard.css',
            '/js/sysdashboard.js?v='+appversionProvider.versi,
            '/js/appdashboard.js?v='+appversionProvider.versi,
            '/templates/templates_sysdashboard.js?v='+appversionProvider.versi,
            '/templates/templates_appdashboard.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        {
          name: 'sysapp',
          files: [
            '/css/sysapp.css',
            '/js/sysappdep.js?v='+appversionProvider.versi,
            '/js/sysapp.js?v='+appversionProvider.versi,
            '/templates/templates_sysapp.js?v='+appversionProvider.versi
          ]
        },
        {
          name: 'app',
          files: [
            '/js/appdep.js?v='+appversionProvider.versi,
            // '/js/app.js?v='+appversionProvider.versi,
            '/templates/templates_app.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_gate',
          files: [
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/app_gate.js?v='+appversionProvider.versi,
            '/templates/templates_app_gate.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_gate_towing',
          files: [
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/app_gate_towing.js?v='+appversionProvider.versi,
            '/templates/templates_app_gate_towing.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_delivery_warranty',
          files: [
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/app_delivery_warranty.js?v='+appversionProvider.versi,
            '/templates/templates_app_delivery_warranty.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_reception_wogr',
          files: [
            // '/module/ag-grid/ag-grid-enterprise.min.js?v='+appversionProvider.versi,
            //                '/module/ag-grid/ag-grid.directive.js?v='+appversionProvider.versi,
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/app_reception_wogr.js?v='+appversionProvider.versi,
            '/templates/templates_app_reception_wogr.js?v='+appversionProvider.versi,
            '/js/signature_pad.js?v='+appversionProvider.versi,
            '/js/signature.js?v='+appversionProvider.versi,
          ],
          serie: true,

        },

        // CR4
        {
          name: 'app_reception_wobp',
          files: [

            // '/module/ag-grid/ag-grid-enterprise.min.js?v='+appversionProvider.versi,
            // '/module/ag-grid/ag-grid.directive.js?v='+appversionProvider.versi,
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/app_reception_wobp.js?v='+appversionProvider.versi,
            '/templates/templates_app_reception_wobp.js?v='+appversionProvider.versi,
            '/js/signature_pad.js?v='+appversionProvider.versi,
            '/js/signature.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        // END CR4
        {
          name: 'app_reception_wolistgr',
          files: [
            // '/module/ag-grid/ag-grid-enterprise.min.js?v='+appversionProvider.versi,
            //                '/module/ag-grid/ag-grid.directive.js?v='+appversionProvider.versi,									   
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/app_reception_wolistgr.js?v='+appversionProvider.versi,
            '/templates/templates_app_reception_wolistgr.js?v='+appversionProvider.versi,
            '/js/signature_pad.js?v='+appversionProvider.versi,
            '/js/signature.js?v='+appversionProvider.versi,
          ],
          serie: true,

        },
        {
          name: 'app_reception_wolistbp',
          files: [
            // '/module/ag-grid/ag-grid-enterprise.min.js?v='+appversionProvider.versi,
            // '/module/ag-grid/ag-grid.directive.js?v='+appversionProvider.versi,
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/app_reception_wolistbp.js?v='+appversionProvider.versi,
            '/templates/templates_app_reception_wolistbp.js?v='+appversionProvider.versi,
            '/js/signature_pad.js?v='+appversionProvider.versi,
            '/js/signature.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        {
          name: 'app_reception_bstklist',
          files: [
              '/js/appdep.js?v='+appversionProvider.versi,
              '/js/app_reception_bstklist.js?v='+appversionProvider.versi,
              '/templates/templates_app_reception_bstklist.js?v='+appversionProvider.versi,
              '/js/signature_pad.js?v='+appversionProvider.versi,
              '/js/signature.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        // CR4
        {
          name: 'app_estimasiBP',
          files: [
            // '/module/ag-grid/ag-grid-enterprise.min.js?v='+appversionProvider.versi,
            // '/module/ag-grid/ag-grid.directive.js?v='+appversionProvider.versi,
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/app_estimasiBP.js?v='+appversionProvider.versi,
            '/templates/templates_app_estimasiBP.js?v='+appversionProvider.versi,
            // '/js/signature_pad.js?v='+appversionProvider.versi,
            // '/js/signature.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        //end CR4
        {
          name: 'app_boards',
          files: [
            // '/js/app_boards.js?v='+appversionProvider.versi,
            '/js/app_services.js?v='+appversionProvider.versi,
            '/templates/templates_appboards.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_branchOrder',
          files: [
            '/js/app_branchOrder.js?v='+appversionProvider.versi,
            '/templates/templates_appbranchorder.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_crm',
          files: [
            // '/js/app_crm.js?v='+appversionProvider.versi,
            '/js/app_services.js?v='+appversionProvider.versi,
            '/templates/templates_appcrm.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_finance',
          files: [
            '/js/app_finance.js?v='+appversionProvider.versi,
            '/templates/templates_appfinance.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_master',
          files: [
            '/js/app_master.js?v='+appversionProvider.versi,
            //'/js/app_parts.js?v='+appversionProvider.versi,
            '/js/app_services.js?v='+appversionProvider.versi,
            '/templates/templates_appmaster.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_parameter',
          files: [
            //'/js/app_parameter.js?v='+appversionProvider.versi,
            '/js/app_services.js?v='+appversionProvider.versi,
            '/templates/templates_appparameter.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_parts',
          files: [
            //                     '/js/app_parts.js?v='+appversionProvider.versi,
            '/js/app_master.js?v='+appversionProvider.versi,
            '/js/app_services.js?v='+appversionProvider.versi,
            '/js/print_min.js?v='+appversionProvider.versi,
            '/templates/templates_appparts.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_report',
          files: [
            '/js/app_report.js?v='+appversionProvider.versi,
            '/templates/templates_appreport.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales',
          files: [
            '/js/app_sales.js?v='+appversionProvider.versi,
            '/templates/templates_appsales.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales1',
          files: [
            '/js/app_sales1.js?v='+appversionProvider.versi,
            '/templates/templates_appsales1.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales2',
          files: [
            '/js/app_sales2.js?v='+appversionProvider.versi,
            '/templates/templates_appsales2.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales3',
          files: [
            '/js/app_sales3.js?v='+appversionProvider.versi,
            '/templates/templates_appsales3.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales4',
          files: [
            '/js/app_sales4.js?v='+appversionProvider.versi,
            '/templates/templates_appsales4.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales5',
          files: [
            '/js/app_sales5.js?v='+appversionProvider.versi,
            '/templates/templates_appsales5.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales6',
          files: [
            '/js/app_sales6.js?v='+appversionProvider.versi,
            '/templates/templates_appsales6.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales7',
          files: [
            '/js/app_sales7.js?v='+appversionProvider.versi,
            '/templates/templates_appsales7.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales8',
          files: [
            '/js/app_sales8.js?v='+appversionProvider.versi,
            '/templates/templates_appsales8.js?v='+appversionProvider.versi,
          ]
        },
        {
          name: 'app_sales9',
          files: [
            '/js/app_sales9.js?v='+appversionProvider.versi,
            '/templates/templates_appsales9.js?v='+appversionProvider.versi,
          ]
        },
        //         {
        //                   name: 'app_services',
        //                   files: [
        //                       '/js/app_master.js?v='+appversionProvider.versi,
        //                       '/js/app_services.js?v='+appversionProvider.versi,
        //             '/templates/templates_appservices.js?v='+appversionProvider.versi,
        //                   ]
        //               },
        {
          name: 'ui.calendar',
          files: [
            'module/fullcalendar/calendar.js?v='+appversionProvider.versi,
            'module/fullcalendar/fullcalendar.min.css',
            'module/fullcalendar/fullcalendar.min.js?v='+appversionProvider.versi,
          ],
          serie: true,
        },
        {
          name: 'app_services',
          files: [
            // '/module/ag-grid/ag-grid-enterprise.min.js?v='+appversionProvider.versi,
            //                '/module/ag-grid/ag-grid.directive.js?v='+appversionProvider.versi,									   
            '/js/app_services.js?v='+appversionProvider.versi,
            '/templates/templates_appservices.js?v='+appversionProvider.versi,
            '/js/appdep.js?v='+appversionProvider.versi,
            '/js/signature_pad.js?v='+appversionProvider.versi,
            '/js/signature.js?v='+appversionProvider.versi,
            '/js/print_min.js?v='+appversionProvider.versi,
            '/js/app_master.js?v='+appversionProvider.versi,
          ],
          serie: true
        },
      ]
    });
  }]);
