angular.module('pricingEngineopd', []).directive('pricingEngineopd', function($q, $rootScope, $parse, $timeout, $templateCache, Pricing, bsNotify, $http) { //bsPrint -> bs-print

    return {
        template: function(element, attrs) { //attrs dipakai jika static atau untuk manipulasi template sebelum di-compile

            //#region Template
            var strTemplate =
                '<div class="BodyRows col-lg-12 col-xs-12"   ng-repeat="row in jsData.BodyRows " >\
                    <div ng-transclude="bodyCustom" ng-if="row.Cells[0].Value == \'+\'" /> \
                    <hr ng-if="row.IsLine" class="ColLine" />\
                    <div ng-if="!row.IsLine && row.Cells[0].Value != \'+\'"   ng-repeat="cell in row.Cells" class="{{cell.CSSClass}}" >\
                        <label ng-hide="cell.IsCode" class="cell.CSSClass" >{{cell.Value}}</label>\
                        <input ng-show="cell.IsCode" class="cell.CSSClass" type="text" ng-model="cell.Value"  />\
                 </div></div>';

            var strDebugTemplate = '</br>\
                    <div class="col-md-12 col-xs-12" style="1px dotted" ><br/><p>THIS IS DEBUG DIV</p>\
                        <div    ng-repeat="x in jsData.Cells " >\
                            <label style="min-width:100px" >{{x.Code}}  :  {{x.Value}}</label>\
                        </div>\
                        <label>scope.jsData.Codes["</label> <input type="text" ng-model="debugCode" /> <label>"] = </label>\
                        <input type="button" value="Show Value to Console" data-ng-click="ShowLogValue()"/> \
                    </div> ';

            var xTemplate =
                //'<div style="border:3px dashed">  ' +
                '<div>' +
                //'<input sty type="button" class="rbtn btn" value="Calculate" ng-click="GetDataFromFactory();"/>'+
                ' <input type="button" class="rbtn btn" value="Calculate All" data-ng-click="pricingTotal()"/></div> ';
            //'<div><input ng-show="true" type="text" class="form-control" ng-model="count" ng-change=""/> </div> ' +
            // '<div> </div> ' +
            // '</div>';

            return xTemplate;
            //#endregion Template

        },
        restrict: 'EA',
        replace: true,

        // "#ServicePrice#": "300000.00",
        // "{AdminFee}": "0",
        // "[SubTotalHarga]": "300000.00",
        // "[DPP]": "272727.272727",
        // "[PPN]": "27272.7272727",
        // "[SubTotalHarga2]": "300000.00",
        // "$QTY$": "3",
        // "[TotalHarga]": "900000.00"

        transclude: { 'bodyCustom': '?formBodyCustom' },
        scope: {
            branch: '@',
            debugCode: '@',
            listDocument: '=listDocument',
            serviceprice: '=servicePrice',
            dpp: '=dpp',
            ppn: '=ppn',
            adminFee: '=adminFee',
            subtotalharga: '=subTotalharga',
            subtotalharga2: '=subTotalharga2',
            totalharga: "=totalHarga",
            url_opd: '=urlPricing',
            index: '=index',
            harga: '=harga',
            GrandTotal: '=grandTotal'
        },
        link: function postLink(scope, element, attrs, formCtl) {

            //#region Constructor

            //#endregion Contructor

            // $rootScope.$on("pricingCalculate", function() {
            //     scope.GetDataFromFactory();
            // });
            //   var count = 0;  

            scope.count = 0;
            scope.tempcount = 0;
            scope.promiseArray = [];
            //#region Action

            // {
            // "Title": "Pricing Engine",
            // "HeaderInputs": {
            //                 "$VehicleTypeColorId$": "4609",
            //                 "$OutletId$": "280",
            //                 "$AssemblyYear$": "2018",
            //                 "$KodeTPajak$":"01",
            //                 "$BBNAdj$":"500000",
            //                 "$BBNSAdj$":"1000000",
            //                 "$TotalACC$":"2000000",
            //                 "$Diskon$":"500000",
            //                 "$MCommision$":"1",
            //                 "$DSubsidiDP$":"2",
            //                 "$DSubsidiRate$":"3"
            //                 }
            // }

            scope.allGrandTotal = function() {
                for (var i in scope.unit) {
                    for (var j in scope.unit[i].ListDetailUnit) {
                        scope.unit[i].ListDetailUnit[j].GrandTotalAll =
                            scope.unit[i].ListDetailUnit[j].GrandTotalAll +
                            scope.totalvat + scope.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc
                            //     scope.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll +
                            //     parseFloat(res.data.Codes["[TotalHarga]"]) +
                            //     scope.unit[i].ListDetailUnit[j].GrandTotalAcc
                    }

                }
            }


            scope.pricingTotal = function() {
                if (angular.isUndefined(scope.karoserivat) == true || scope.karoserivat == null) {
                    scope.karoserivat = 0.0;
                }

                scope.grandtotalarray = [];
                for (var i in scope.unit) {

                    for (var j in scope.unit[i].ListDetailUnit) {
                        scope.unit[i].ListDetailUnit[j].GrandTotalAcc = 0;
                        scope.unit[i].ListDetailUnit[j].GrandTotalAll = 0;
                        scope.unit[i].ListDetailUnit[j].jsData = {
                            Title: null,
                            HeaderInputs: {
                                $VehicleTypeColorId$: scope.unit[i].ListDetailUnit[j].VehicleTypeColorId,
                                $BBNAdj$: scope.bbnAdjust,
                                $BBNSAdj$: scope.bbnAdjustservice,
                                $Diskon$: scope.discount,
                                $DSubsidiDP$: scope.subsidiDp,
                                $DSubsidiRate$: scope.discountSubrate,
                                $MCommision$: scope.mediatorCommition,
                                $TotalACC$: 0.0,
                                $OutletId$: scope.unit[i].ListDetailUnit[j].OutletId,
                                $VehicleYear$: scope.unit[i].ProductionYear
                            }
                        }

                        for (var k in scope.unit[i].ListDetailUnit[j].ListAccessoriesPackage) {
                            scope.unit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ =
                                scope.unit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ + scope.unit[i].ListDetailUnit[j].ListAccessoriesPackage[k].TotalInclVAT;
                            scope.unit[i].ListDetailUnit[j].GrandTotalAcc =
                                scope.unit[i].ListDetailUnit[j].GrandTotalAcc + scope.unit[i].ListDetailUnit[j].ListAccessoriesPackage[k].TotalInclVAT;
                        }

                        for (var k in scope.unit[i].ListDetailUnit[j].ListAccessories) {
                            scope.unit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ =
                                scope.unit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ + scope.unit[i].ListDetailUnit[j].ListAccessories[k].TotalInclVAT;
                            scope.unit[i].ListDetailUnit[j].GrandTotalAcc =
                                scope.unit[i].ListDetailUnit[j].GrandTotalAcc + scope.unit[i].ListDetailUnit[j].ListAccessories[k].TotalInclVAT;
                        }

                        scope.unit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ =
                            scope.unit[i].ListDetailUnit[j].jsData.HeaderInputs.$TotalACC$ + scope.karoserivat;

                        scope.unit[i].ListDetailUnit[j].GrandTotalAcc = scope.unit[i].ListDetailUnit[j].GrandTotalAcc + scope.karoserivat;

                        Pricing.getData(scope.url_pricing, scope.unit[i].ListDetailUnit[j].jsData)
                            .then(
                                function(res) {  
                                    scope.dpp = parseFloat(res.data.Codes["[DPP]"]);  
                                    scope.ppn = parseFloat(res.data.Codes["[PPN]"]);  
                                    scope.bbn = parseFloat(res.data.Codes["[BBN]"])
                                    scope.totalbbn = parseFloat(res.data.Codes["[TotalBBN]"]);
                                    scope.pph22 = parseFloat(res.data.Codes["[PPH22]"]);
                                    scope.totalvat = parseFloat(res.data.Codes["[TotalHarga]"]);
                                    scope.vehicleprice = parseFloat(res.data.Codes["[HargaMobil]"]);
                                    scope.priceafterdiscount = parseFloat(res.data.Codes["[PriceAfterDiskon]"]);

                                    //scope.grandtotalarray.push({ GrandTotalAll: parseFloat(res.data.Codes["[GrandTotal]"]) })
                                    //scope.unit[i].ListDetailUnit[j].GrandTotalAll = parseFloat(res.data.Codes["[GrandTotal]"]);
                                    // scope.unit[i].ListDetailUnit[j].GrandTotalAll =
                                    //     scope.unit[i].ListDetailUnit[j].GrandTotalAll +
                                    //     parseFloat(res.data.Codes["[TotalHarga]"]) +
                                    //     scope.unit[i].ListDetailUnit[j].GrandTotalAcc
                                    //scope.jsData = res.data;
                                    //scope.url_pricing, scope.unit[i].ListDetailUnit[j].
                                    //scope.harga[scope.index] = {totalsatuan:0};
                                    //scope.harga[scope.index].totalsatuan = parseFloat(scope.jsData.Codes["[GrandTotal]"]);
                                    //scope.allGrandTotal();
                                    $rootScope.$emit("GrandTotalSPK", {});
                                    console.log("harag", res.data);
                                });

                    }

                }
                // }


                console.log("hgasjhdas", scope.unit, scope.grandtotalarray);
            }

            //#endregion Action
        }
    };
});