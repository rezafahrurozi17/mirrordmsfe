angular.module('percentFormating',[]).directive('percentFormating', function() {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
          modelCtrl.$parsers.push(function (inputValue) {
              if (inputValue == undefined) return '';
			  
			  if (inputValue.length == 2 && inputValue.charAt(0) == "0" && inputValue.charAt(1) != ",") 
			  {
				console.log("tes2");
				// zero at first character 
				inputValue = inputValue.replace("0", "");
				modelCtrl.$setViewValue(inputValue);
				modelCtrl.$render();
				return inputValue;
			  }
			  
              var transformedInput = inputValue.replace(/[^0-9\,]/g, ''); 
              if (transformedInput!=inputValue) {
				modelCtrl.$setViewValue(transformedInput);
				modelCtrl.$render();
              }
              return transformedInput;         
          });
        }
      };
});