angular.module('currencyFormating', []).directive('currencyFormating', ['$locale', '$filter', function($locale,$filter) {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, $element, $attrs, $ngModel) {
			
            // For input validation
            var isValid = function(val) {
                return angular.isNumber(val) && !isNaN(val);
            };
      
            // Helper for creating RegExp's
            var toRegExp = function(val) {
                var escaped = val.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                return new RegExp(escaped, 'g');
            };
      
            // Saved to your $scope/model
            var toModel = function(val) {
				
                // Locale currency support
                //var decimal = toRegExp($locale.NUMBER_FORMATS.DECIMAL_SEP);
                //var group = toRegExp($locale.NUMBER_FORMATS.GROUP_SEP);
                //var currency = toRegExp($locale.NUMBER_FORMATS.CURRENCY_SYM);
				
				var decimal = toRegExp(',');
                var group = toRegExp('.');
                var currency = toRegExp('Rp. ');

        
                // Strip currency related characters from string
                val = val.replace(decimal, ',').replace(group, '').replace(currency, 'Rp.').trim();
                
                return parseInt(val, 10);
            };
      
            // Displayed in the input to users
            var toView = function(val) {
                // debugger
				//console.log("test1", val);
				if(!isNaN(val) && val > 999)
				{
					var temp = (angular.copy($filter('currency')(val, '', 0))).replace(/,/g,".");
					//console.log("test2",temp);
					return temp;
				}
                else
				{
					if(angular.isNumber(val))
					{
                        console.log("test3 ajg lah", val);
						return $filter('currency')(val, '', 0);
					}
					else
					{
						// var temp = val.replace(/,/g,".");
						// console.log("test3 fak lah", temp);
						return val;
					}
				}
            };

            
            // Link to DOM
            $ngModel.$formatters.push(toView);
            $ngModel.$parsers.push(toModel);
            $ngModel.$validators.currency = isValid;
			$ngModel.$viewValue = toView($ngModel.$modelValue);
            $ngModel.$render();
      
            $element.on('keyup', function() {
              var tempMask = angular.copy($ngModel.$modelValue);
			  
			  
              $ngModel.$viewValue = toView(tempMask);
              $ngModel.$render();
            });
        }
    };
}]);
    