angular.module('priceFormat', []).directive('inputCurrency', ['$locale', '$filter', function($locale, $filter) {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, $element, $attrs, $ngModel) {
             console.log($locale)
            // For input validation
            var isValid = function(val) {
                return angular.isNumber(val) && !isNaN(val);
            };
            
      
            // Helper for creating RegExp's
            var toRegExp = function(val) {
                var escaped = val.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                return new RegExp(escaped, 'g');
            };
      
            // Saved to your $scope/model
            var toModel = function(val) {
                // if (val.length <= 2 && event.which == 48) {
                //     // zero at first character 
                //     val = val.replace("0", '')
                // }
                //console.log("val0", val);
                // Locale currency support
                var decimal = toRegExp($locale.NUMBER_FORMATS.DECIMAL_SEP);
                var group = toRegExp($locale.NUMBER_FORMATS.GROUP_SEP);
                var currency = toRegExp($locale.NUMBER_FORMATS.CURRENCY_SYM);

                // Strip currency related characters from string
                val = val.replace(decimal, '').replace(group, '').replace(toRegExp('Rp. '), '').trim();

                var tempMask = angular.copy(val);
                var lengthdigit = tempMask.length;

                var concatValue ="";
                //var first = tempMask.substring(0, 1);
                if (lengthdigit >= 3){
                    //tampung 4 di cutLastCurrency
                    var temp1 = angular.copy(val);
                    var temp2 = angular.copy(val);
                    var cutLastCurrency = temp1.slice(lengthdigit-1,lengthdigit);
                    //console.log("last", cutLastCurrency);
                    var getFirstCurrency = temp2.slice(0, lengthdigit-3);
                        //console.log("first", getFirstCurrency);
                    //}
                    //buang 3 terakhir
                    concatValue = getFirstCurrency+cutLastCurrency;
                }
                //console.log("val", concatValue);
                return parseInt(val, 10);
            };
      
            // Displayed in the input to users
            var toView = function(val) {
                return $filter('currency')(val, 'Rp. ', 0);
            };

            //console.log("model",$ngModel);
            
            // Link to DOM
            $ngModel.$formatters.push(toView);
            $ngModel.$parsers.push(toModel);
            $ngModel.$validators.currency = isValid;
      
            $element.on('keyup', function(event) {
                //console.log("modeltestvalue",$ngModel);
                // var value = angular.copy($(this).val());  
                // console.log("test", value);

                setTimeout(function(){
                    $ngModel.$viewValue = toView($ngModel.$modelValue);
                    //$ngModel.$viewValue.replace(',', '.').replace('.', ',');
                    $ngModel.$render(); 
                }, 0);

              
            });
        }
    };
}]);
    