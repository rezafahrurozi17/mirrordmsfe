angular.module('bsUploadsingle', []).directive('bsUploadsingle', function ($rootScope, $parse, $timeout, $templateCache, bsNotify) { //bsUpload -> bs-upload
    return {
        // template: function (element, attrs) {
        //     //var actionLabels = attrs.actionLabel
        //     var xTemplate = '<div class="ui modal loadingImg" style="height:200px; background-color:transparent; box-shadow: none !important">'+
        //                         '<div align="center" class="list-group-item" style="border: none !important;padding:10px 0px 10px 0px; background-color:transparent;">'+
        //                             '<font color="white"><i class="fa fa-spinner fa-spin fa-3x fa-lg"></i></br>'+
        //                             '<b> Loading Menu... </b> </font>'+
        //                         '</div>'+
        //                     '</div>';
        //    // return xTemplate;
        //   },
        restrict: 'A',
        transclude: true,
        replace: true,
        //require:'^form',
        scope: {
            branch: '@',
            namatable: '@',
            tablerefid: '@',
            id: '@?', //
            formattype: '=?',
            maxbyte: '@?',
            loading: '@?',
            documenttypeid: '@',
            accepted:'=?',
            type: '@',
            maxupload: '=', // jumlah upload 
            isImage: '=isImage', // 1 = image , 0 = document(filename ajah)
            uploadFiles: '=imgUpload'
        },
        link: function postLink(scope, element, attrs, formCtl) {
            scope.accepted ='';

            element.bind("change", function (e) {
                scope.file = (e.srcElement || e.target).files[0];
                var count = 0;
                var suport_name = "";
                var selected_format = [];

                //--refresh element by Id (bindding scope.id )
                angular.element(document.querySelector('#'+scope.id)).prop("value", "");

                //--split file name by '.';
                var listExt = scope.file.name.split(".");
                
                //--check double extension.
                if(listExt.length >= 3){
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nama file tidak boleh menggunakan ' . '",
                        type: 'warning'
                    });
                }else{
                    if (scope.formattype != undefined) {
                        count = 0;
                        for (var i in scope.formattype) {
                            if (scope.formattype[i] == 'pdf') {
                                selected_format.push({ name: 'pdf', type: 'application/pdf' });
                            } else if (scope.formattype[i] == 'png') {
                                selected_format.push({ name: 'png', type: 'image/png' });
                            } else if (scope.formattype[i] == 'jpeg') {
                                selected_format.push({ name: 'jpeg', type: 'image/jpeg' });
                            } else if (scope.formattype[i] == 'jpg') {
                                selected_format.push({ name: 'jpg', type: 'image/jpeg' });
                            } else {
    
                            }
                        }
    
                        for (var i in selected_format) {
                            if (selected_format[i].type == scope.file.type) {
                                count++;
                            }
                            suport_name = suport_name + ", " + selected_format[i].name;
                            scope.accepted = scope.accepted +","+selected_format[i].type;
                        }
    
                        //console.log("acceped", scope.accepted);
                        //console.log("support ext", suport_name);
                        //console.log("tipe sekarang", scope.file.type);                   
    
                        if (count < 1) {
                            bsNotify.show({
                                title: "Peringatan",
                                content: "File hanya diperbolehkan tipe" + suport_name + '.',
                                type: 'warning'
                            });
                        } else {
                            reader();
                        }
                    } else {
                        reader();
                    }
                }
                
                //console.log('formattype', scope.formattype);
                //console.log('size', scope.maxbyte);
                //var support_format = ['image/png','image/jpg','image/jpeg','image/img','application/pdf'];

                function reader() {
                    var tempsize = scope.maxbyte * 1000000;

                    //--check file size (binding scope.maxbyte *MB )  
                    if (scope.file.size > tempsize && scope.maxbyte != undefined) {
                        bsNotify.show({
                            title: "Gagal",
                            content: "Ukuran file maksimal " + scope.maxbyte + "MB.",
                            type: 'warning'
                        });
                    } else {
                        if (scope.loading != undefined || scope.loading != null || scope.loading != '') {
                            angular.element('.ui.modal.' + scope.loading).modal('show');
                        }
                        var reader = new FileReader();
                        reader.readAsDataURL(scope.file);
                        reader.onload = function (e) {                                     
                            var newImage = {
                                UpDocObj: reader.result,
                                Branch: scope.branch,
                                NameTable: scope.namatable,
                                TableRefId: scope.tablerefid,
                                DocumentTypeId: scope.documenttypeid,
                                FileName: scope.file.name,
                            };
                            scope.uploadFiles = newImage;

                            //callback disabled loading
                            setTimeout(function () {
                                onFilesLoaded();
                            }, 1000);             
                        }
                        reader.onprogress = function (data) {
                            if (data.lengthComputable) {
                                var progress = parseInt(((data.loaded / data.total) * 100), 10);
                            }
                        }
                    }
                }

                function onFilesLoaded() {
                    if (scope.loading != undefined || scope.loading != null || scope.loading != '') {
                        angular.element('.ui.modal.' + scope.loading).modal('hide');
                    }
                }
            })
        }
    };
});