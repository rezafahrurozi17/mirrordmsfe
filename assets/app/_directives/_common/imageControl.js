angular.module('imageControl', []).directive('imageControl', function($parse, $timeout, $templateCache, bsNotify) { //imageControl -> image-control

    return {
        template: function(element, attrs) { //attrs dipakai jika static atau untuk manipulasi template sebelum di-compile

            var FileNamePre = '';
            var xhmtlStyles = "<style type='text/css'>.thumb{border:1px dashed #cecece;width:100%;height:90px;max-width:90px;border-radius:4px;display:inline-block;margin:0 7px 10px 0;box-sizing:border-box;vertical-align:top;font-size:13px;line-height:17px;text-align:center;color:#6e6e6e;box-shadow:0 2px 5px 1px rgba(0,0,0,.026);background:rgba(255,255,255,.33);overflow:hidden;position:relative;cursor:pointer}" +
                ".thumb.thumb-upload{border-color:#8a8a8a}" +
                ".thumb.thumb-done{border-style:solid}" +
                ".thumb:last-child{margin-right:0}" +
                ".thumb img{width:100%}" +
                ".thumb-setting{background:#fff;height:28px;position:absolute;width:100%;bottom:0;border-radius:0 0 3px 3px;box-sizing:border-box;padding:5px 4px;border-top:1px solid #ebebeb}" +
                ".thumb-text{width:75%;text-align:left;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;float:left}" +
                ".thumb-delete{font-size:24px;font-weight:300;cursor:pointer;text-align:center;display:inline-block}" +
                ".thumb-upload input[type=file]{width:100%;height:100px;max-width:100px;position:absolute;cursor:pointer;opacity:0;left:0;top:0}.mt-5{margin-top:5px}.mt-20{margin-top:20px}</style>";

            //#region Template
            // <img class="image-preview" src="\"{{ typeof x === '+"object"+'  ? x.UpDocObj : '+""+' }}\"" alt="" ng-show="' + attrs.isImage + '"> \
            // <span class="fa fa-times"  data-toggle="tooltip" data-original-title="Hapus gambar"></span>\


            var xTemplate =
                '<div> \
            <div id="menuThumbnailImages" style="margin-top:10px;"   class="">\
            {{tabSlider.thumbnailWidth}}' +
                xhmtlStyles +
                '<div class="add-attachment-image" id="image-list" styles="overflow-y:none;overflow-x:auto;"> \
                <div class="thumb thumb-done" ng-show="uploadFiles.length" ng-repeat="x in uploadFiles " >\
                    <div class="file">\
                        <img class="image-preview" src="{{x.UpDocObj}}" alt="{{x.FileName}}" ng-show="' + attrs.isImage + '"> \
                        <p ng-hide="' + attrs.isImage + '">{{x.FileName}}</p> \
                        <div class="thumb-setting" id="{{x.FileName}}" role="button" data-ng-click="showThumbnail($index)">\
                            <span class="fa fa-eye"  data-toggle="tooltip" data-original-title="Lihat Gambar"></span>\
                        </div>\
                    </div>\
                </div> \
\
                <div class="ui modal modalThumbnail_Preview" role="dialog" style="width:500px; position: fixed; margin-top: -220px; left: 60%;">\
                    <div class="modal-header">\
                    </div>\
                    \
                    <div class="modal-body"> \
                        <img src="{{uploadFiles[ImageChoosen].UpDocObj}}" alt="{{uploadFiles[ImageChoosen].FileName}}" width="500">\
                    </div>\
                    \
                    <div class="modal-footer">\
                        <button ng-click="removeItem(ImageChoosen)" class="wbtn btn ng-binding ladda-button" style="float: left;">Hapus</button>\
                        <button ng-click="Tutup()" class="wbtn btn ng-binding ladda-button" style="float: right;">Kembali</button>\
                    </div>\
                </div>\
\
                <div class="thumb thumb-upload" ng-show="uploadFiles.length <' + attrs.maxUpload + '">\
                    <div class="mt-20">\
                        <div class="fa fa-camera"></div>\
                        <div class="mt-5">Pilih Gambar</div>\
                    </div>\
                    <div ng-transclude="formUpload" /> \
                    \
                </div>\
               </div> \
        </div>';
        

            //#endregion Template

            return xTemplate;

        },
        restrict: 'E',
        transclude: { 'formUpload': '?formImageControl' },
        replace: true,
        scope: {
            uploadFiles: '=imgUpload'
        },
        link: function postLink(scope, element, attrs, formCtl, $http) {

            scope.removeItem = function(index) {
                angular.element(".ui.modal.modalThumbnail_Preview").modal("hide");
                scope.uploadFiles.splice(index, 1);
            };

            scope.showThumbnail = function(index){
                scope.ShowModalThumbnail = true;
                scope.ImageChoosen = index
                angular.element(".ui.modal.modalThumbnail_Preview").modal("show");
            }

            scope.Tutup = function(){
                scope.ShowModalThumbnail = false;
                angular.element(".ui.modal.modalThumbnail_Preview").modal("hide");
            }


        }
    };
});