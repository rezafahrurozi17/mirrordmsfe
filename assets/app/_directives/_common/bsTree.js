angular.module('bsTransTree',[])
.factory('bsTransTree',function(){
	return{
		createTree: function(arr,xid,xpid){
			if(!xid) xid='id';if(!xpid) xpid='parentId';
			var res=[];
			for(var i=0;i<arr.length;i++){
				if(arr[i][xpid]==0){
					createTreeChild(arr,arr[i],xid,xpid);
					res.push(arr[i]);
				}
			}

			function createTreeChild(arr,pobj,xid,xpid){
				pobj.child=[];
				for(var i=0;i<arr.length;i++){
					if(arr[i][xpid]==pobj[xid]){ //got child
						pobj.child.push(arr[i]);
						var x=pobj.child.length - 1;
						createTreeChild(arr,pobj.child[x],xid,xpid);
					}
				}
			}
			return res;
		},
		translate: function(data,transformModel){
			// data = [{data}]
			// transformModel = {
			// 	desiredAttrName1 : [existingAttrName1],
			// 	desiredAttrName2 : [existingAttrName2],
			// 	childrens : {isChild:true, fld:[childrenAttrName]}
			// }
	    	var res = [];
	    	function replaceAttr(obj,key,values,level){
	    		obj[key]=obj[values[level-1]];
	        	return obj;
	    	}
	     	function getAvailAttr(obj,arrOfAttr){
	       		for(var i in arrOfAttr){
	        		if(arrOfAttr[i] in obj){
	           			break;
	         		}
	        	}
	      	}
	      	function getChild(obj,level){
	        	for(var attr in transformModel){
	          		if(transformModel[attr].isChild){
	            		obj[attr] = [];
	            		for(var chIdx in transformModel[attr].fld){
	              			if(transformModel[attr].fld[chIdx] in obj){
	                			for(var chLen in obj[transformModel[attr].fld[chIdx]]){
	                				obj[attr].push(getChild(obj[transformModel[attr].fld[chIdx]][chLen],level+1));
	                				var newObj = JSON.parse(JSON.stringify(obj));
	                				newObj.child = [];
	                				newObj[ transformModel[attr].fld[chIdx] ] = [];
	                				obj[attr][chLen].parent = JSON.parse(JSON.stringify(newObj));
	                			}
	                			break;
	              			}
	            		}
	          		}else{
	            		obj = replaceAttr(obj,attr,transformModel[attr],level);
	            		obj.level = level;
	          		}
	        	}
	        	return obj;
	      	}
	      	for(var i=0; i<data.length; i++){
	        	res.push(getChild(data[i],1));
	      	}
	      	return res;
		},
		createUIGridTreeData: function(treeData){
			var res = [];
			function construct(data,level){
				for(var idx in data){
					var newObj = data[idx];
					newObj.$$treeLevel = level;
					res.push(newObj);
					if(data[idx].child && data[idx].child.length>0) construct(data[idx].child,level+1);
				}
			}
			construct(treeData,0);
			return res;
		},
	}
});
