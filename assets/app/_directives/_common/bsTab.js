angular.module('bsTab',[])
.factory('bsTab',function(Tabs,MENU_TREE){
	return{
		add: function(sref){

					function deepFind(node,sref){
              var res=null;
              for(var i=0;i<node.length;i++){
                  if(node[i].Sref == sref){
                      res = node[i];
                      return res;
                      break;
                  }else{
                      if(node[i].Child.length>0 && node[i].Child!=null && res==null){
                              res = deepFind(node[i].Child,sref);
                              if(res) break;
                      }
                  }
              }
              return res;
          }

					var menutree=MENU_TREE.ITEMS;
					var tab=deepFind(menutree,sref);
					if(tab){
						var xtab = JSON.stringify(tab);
						var href=tab.Sref.split('.')[1];
						Tabs.add({sref:tab.Sref,view:href,title:tab.Name,icon:tab.Icon,active:false,removeable:true,item:xtab,menu:tab});
					}
				}
	}
});
