angular.module('bslistEx',[]).directive('bslistEx', function ($parse,$state,$injector,ngDialog,bsAlert) {
   return {
      template : function(element, attrs) {
         var stateData = $state.current.data;
         //console.log("stateData=>",stateData);
         // DEFAULT VALUE
         var listTitle = '';if(attrs.listTitle){ listTitle = attrs.listTitle; }
         var modalTitle = '';if(attrs.modalTitle){ modalTitle = attrs.modalTitle; }
         var modalTitleDetail = '';if(attrs.modalTitleDetail){ modalTitleDetail = attrs.modalTitleDetail; }
         var modalSize = '';if(attrs.modalSize){ modalSize = ' size="'+attrs.modalSize+'" '; }
         var modalSizeDetail = '';if(attrs.modalSizeDetail){ modalSizeDetail = ' size="'+attrs.modalSizeDetail+'" '; }

         if(!attrs.formTitle){attrs.formTitle = stateData.title;}
         if(!attrs.formName){ attrs.formName =  attrs.formTitle.replace(/ /g,''); }
         //console.log("formname=>",attrs.formName);
         attrs.formNameMaster = 'lm1_'+attrs.formName;
         attrs.formNameDetail = 'ld1_'+attrs.formName;
         if(attrs.childKey==undefined) attrs.childKey='child';

         if(attrs.showActionButton){
             attrs.showActionButton=(attrs.showActionButton=='on');
         }
         var xtemplate =
      '<div class="bs-listx">'+
         '<div class="skewed-bg2">'+
            '<div class="content">'+
               '<div class="title">'+
                  listTitle+' Ex'+
               '</div>'+
            '</div>'+
         '</div>'+
         '<form class="form-inline">'+
            '<div ng-if="!vm.disabled" class="row filtersearch" style="margin-right:0px;margin-left:0px;padding-bottom:0px;">'+
               '<div style="padding-right:0px;">'+
                  '<div class="input-group">'+
                     '<div class="input-group-btn" style="margin-right:5px;">'+
                        '<button class="ui-list-btn-select-all" ng-class="{selectAll:vm.selected.length>0}" ng-click="vm.toggleSelectAll()"'+
                        ' ng-if="vm.btnSettings.selectall.enable && vm.mdata.length"'+
                        ' uib-tooltip="{{vm.btnSettings.selectall.title}}" tooltip-placement="bottom-left"'+
                        '>'+'<i class="{{vm.btnSettings.selectall.icon}}"></i>'+'</button>'+

                        '<button skip-enable class="ui-list-btn-delete" ng-disabled="vm.selected.length==0" ng-click="vm.actListDelete()"'+
                              ' ng-if="vm.btnSettings.delete.enable && vm.mdata.length"'+
                              ' uib-tooltip="{{vm.btnSettings.delete.title}}" tooltip-placement="bottom"'+
                        '>'+'<i class="{{vm.btnSettings.delete.icon}}"></i></button>'+

                        '<button skip-enable class="ui-list-btn-approve" ng-disabled="vm.selected.length==0" ng-if="vm.btnSettings.approve.enable && vm.mdata.length"'+
                              ' uib-tooltip="{{vm.btnSettings.approve.title}}" tooltip-placement="bottom"'+
                        '>'+'<i class="{{vm.btnSettings.approve.icon}}"></i></button>'+

                        '<button skip-enable class="ui-list-btn-reject" ng-disabled="vm.selected.length==0" ng-if="vm.btnSettings.reject.enable && vm.mdata.length"'+
                              ' uib-tooltip="{{vm.btnSettings.reject.title}}" tooltip-placement="bottom"'+
                        '>'+'<i class="{{vm.btnSettings.reject.icon}}"></i></button>'+

                        '<button skip-enable class="ui-list-btn-review" ng-disabled="vm.selected.length==0" ng-if="vm.btnSettings.review.enable && vm.mdata.length"'+
                              ' uib-tooltip="{{vm.btnSettings.review.title}}" tooltip-placement="bottom"'+
                        '>'+'<i class="{{vm.btnSettings.review.icon}}"></i></button>'+

                        '<button skip-enable class="ui-list-btn-print" ng-disabled="vm.selected.length==0" ng-if="vm.btnSettings.print.enable && vm.mdata.length"'+
                              ' uib-tooltip="{{vm.btnSettings.print.title}}" tooltip-placement="bottom"'+
                        '>'+'<i class="{{vm.btnSettings.print.icon}}"></i></button>'+
                     '</div>'+
                  '</div>'+
               '</div>'+
               '<div style="float:right;height:2px;">'+
                  '<div class="btn-group" style="margin-top:-55px;">'+
                     '<button class="btn rbtn" onclick="blur()" ng-click="vm.actListNew()" ng-if="vm.btnSettings.new.enable">'+
                        '<i class="{{vm.btnSettings.new.icon}}"></i>&nbsp;{{vm.btnSettings.new.title}}'+
                     '</button>'+
                     '<div class="btn-group" style="display:flex">'+
                        '<div ng-repeat="btn in vm.customBtnSettings">'+
                           '<button class="btn rbtn" onclick="blur()" ng-click="btn.func()" ng-if="btn.enable">'+
                              '<i class="{{btn.icon}}"></i>&nbsp;{{btn.title}}'+
                           '</button>'+
                        '</div>'+
                     '</div>'+
                  '</div>'+
               '</div>'+
            '</div>'+
         '</form>'+

         '<div class="bs-list panel panel-default">'+
            '<div class="panel-body">'+
               '<div class="ui-grid-nodata" ng-if="!vm.mdata.length && !loading" ng-cloak>No data available</div>'+
               '<div class="ui-grid-nodata" ng-if="loading" ng-cloak>Loading data... <i class="fa fa-spinner fa-spin"></i></div>'+
               '<div  class="ui-list-view-striped" ui-list-view="lmItem in vm.mdata | filter:search" status="vm.status.expand" height="vm.listHeight">'+
                  // '{{lmItem.selected}}'+
                  '<div class="ui-list-content" ng-class="{selected:lmItem.selected}">'+
                     // 'selected:{{lmItem.selected}}'+
                     '<div style="padding: 10px 10px;">'+
                        '<div bs-transclude="parent" bs-transclude-slot="listMasterTemplate">'+
                        '</div>'+
                     '</div>'+
                     // 'childKey.length => {{lmItem.'+attrs.childKey+'.length}} {{vm.disabled}}'+
                     '<div class="ui-list-view-cell-menu" style=""'+
                           // ' ng-if="( lmItem.'+attrs.childKey+'.length>0 )'+
                           //        ' || ( (lmItem.'+attrs.childKey+'.length<=0) && !vm.disabled)  ">'+
                           ' ng-if="!vm.disabled">'+
                        '<div style="float:left;">'+
                           '<button class="ui-list-btn-expand" ng-click="vm.statusClicked(lmItem)" '+
                                  ' ng-class="{expand:(lmItem.expand==undefined?true:lmItem.expand)}" skip-disable>'+
                              '<i class="fa fa-caret-right" ng-show="lmItem.'+attrs.childKey+'.length>0"></i>'+
                           '</button>'+

                           '<button class="ui-list-btn-select" ng-click="vm.toggleSelected(lmItem)" ng-class="{selected:lmItem.selected}"'+
                              ' uib-tooltip="{{vm.btnSettings.select.title}}" tooltip-placement="bottom"'+
                              ' ng-if="vm.btnSettings.select.enable"'+
                           '>'+
                              '<i class="{{vm.btnSettings.select.icon}}"></i>'+
                           '</button>'+
                        '</div>'+
                        '<div style="float:right;">'+
                           '<div class="btn-group">'+
                              '<div class="btn-group">'+
                                 '<button class="ui-list-btn" ng-disabled="vm.selected.length>0"'+
                                        ' onclick="this.blur();" ng-click="vm.actListView(lmItem)"'+
                                        ' uib-tooltip="{{vm.btnSettings.view.title}}" tooltip-placement="bottom"'+
                                        ' ng-if="vm.btnSettings.view.enable"'+
                                 '>'+
                                    '<i class="{{vm.btnSettings.view.icon}}"></i>'+
                                 '</button>'+
                                 '<button class="ui-list-btn" ng-disabled="vm.selected.length>0"'+
                                        ' onclick="this.blur();" ng-click="vm.actListEdit(lmItem)"'+
                                        ' uib-tooltip="{{vm.btnSettings.edit.title}}" tooltip-placement="bottom"'+
                                        ' ng-if="vm.btnSettings.edit.enable"'+
                                 '>'+
                                    '<i class="{{vm.btnSettings.edit.icon}}"></i>'+
                                 '</button>'+
                              '</div>'+
                              //Custom Button
                              '<div class="btn-group">'+
                                 '<div ng-repeat="btn in vm.customBtnSettingsDetail">'+
                                    '<button class="ui-list-btn" ng-disabled="vm.selected.length>0"'+
                                           ' onclick="this.blur();" ng-click="btn.func(lmItem)"'+
                                           ' uib-tooltip="{{btn.title}}" tooltip-placement="bottom"'+
                                           ' ng-if="btn.enable"'+
                                    '>'+
                                       '<i class="{{btn.icon}}"></i>'+
                                    '</button>'+
                                 '</div>'+
                              '</div>'+
                           '</div>'+
                        '</div>'+
                     '</div>'+
                  '</div>'+

                  '<div class="bs-list panel">'+
                     '<div class="panel-body">'+
                        '<div ng-if="lmItem.'+attrs.childKey+'.length>0" class="ui-list-view-striped"'+
                              ' ui-list-view-child="ldItem in lmItem.'+attrs.childKey+' | filter:search"'+
                              ' status="lmItem.expand">'+
                           '<div style="padding: 10px 10px;">'+
                              '<div bs-transclude="parent" bs-transclude-slot="listDetailTemplate"></div>'+
                              // '<p class="address"><strong>{{ ldItem.address }}</strong></p>'+
                              // '<div class="city">{{ ldItem.city }}</div>'+
                           '</div>'+
                        '</div>'+
                     '</div>'+
                  '</div>'+

               '</div>'+
            '</div>'+
            '<div class="panel-footer" style="height:20px;padding:1px 7px;" ng-show="vm.selected.length>0">'+
               'Selected: {{vm.selected.length}}'+
            '</div>'+
         '</div>'+

         '<bsui-modal show="vm.show_modal.show" title="'+ modalTitle +'" data="vm.lmModel"'+modalSize+
                    ' on-save="vm.onListSave" on-cancel="vm.onListCancel"'+
                    ' allow-multiple="true" close-on-save="false"'+
                    ' button-settings="vm.modalBtnSettings" mode="vm.mMode"'+
                    ' form-name="lm1_'+attrs.formName+'"'+
                    ' isolate-form="true"'+
         '>'+
                  '<div ng-transclude="mdlMasterForm"></div>'+

                  '<div class="row filtersearch" style="margin-right:0px;margin-left:0px;padding-bottom:0px;" ng-if="vm.grid">'+
                     '<div style="float:left;margin-top:10px;">'+
                        '<div class="skewed-bg">'+
                           '<div class="content">'+
                              '<div class="title">'+
                                 modalTitleDetail+
                              '</div>'+
                           '</div>'+
                        '</div>'+
                     '</div>'+
                     '<div style="float:right;">'+
                        '<button class="btn rbtn" style="margin-bottom:5px;" onclick="blur()" ng-click="vm.actListNewDetail()">'+
                           '<i class="{{vm.btnSettings.new.icon}}"></i>&nbsp;{{vm.btnCaption.new}}'+
                        '</button>'+
                     '</div>'+
                  '</div>'+

                  '<div ui-grid="vm.grid" ui-grid-move-columns ui-grid-resize-columns ui-grid-selection'+
                     '  ui-grid-pagination ui-grid-auto-resize class="grid" ng-if="vm.grid">'+
                        '<div class="ui-grid-nodata" ng-if="!vm.grid.data.length && !vm.loading">No data available</div>'+
                        '<div class="ui-grid-nodata" ng-if="vm.loading">Loading Data... <i class="fa fa-spinner fa-spin"></i></div>'+
                  '</div>'+
         '</bsui-modal>'+

         '<bsui-modal show="vm.show_modal_detail.show" title="'+ modalTitleDetail +'" data="vm.ldModel"'+modalSize+
                    ' on-save="vm.onListSaveDetail" on-cancel="vm.onListCancelDetail" scrolling="false"'+
                    ' allow-multiple="true" close-on-save="false"'+
                    ' button-settings="vm.modalBtnSettings_detail" mode="vm.dMode"'+
                    ' form-name="ld1_'+attrs.formName+'"'+
                    ' isolate-form="true"'+
         '>'+
                  '<div ng-transclude="mdlDetailForm"></div>'+
         '</bsui-modal>'+
      '</div>';
      //console.log("template=>",xtemplate);
      return xtemplate;
      },

   restrict : 'E',
   replace: true,
   transclude: {
      'listMasterTemplate':'?bslistTemplate',
      'listDetailTemplate':'?bslistDetailTemplate',
      'mdlMasterForm':'?bslistModalForm',
      'mdlDetailForm':'?bslistModalDetailForm'
   },
   require:['^?form'],
   controllerAs: 'vm',
   bindToController: true,
   scope : {
      name:'@?',
      mId:'@?',
      dId:'@?',
      childKey:'@?',
      factoryName: '@',
      factoryNameDetail: '@?',
      mdata:'=data',
      onSave:'&?',
      onCancel:'&?',
      onValidateSave:'&?',    //not-applied
      onValidateCancel:'&?',  //not-applied
      onBeforeNew:'&',        //applied
      onBeforeEdit:'&',       //applied
      onBeforeDelete:'&?',    //applied
      onBeforeSave:'&?',      //applied
      onBeforeCancel: '&?',   //applied
      onAfterNew:'&',         //applied
      onAfterEdit:'&',        //applied
      onAfterDelete:'&?',     //applied
      onAfterSave:'&?',       //applied
      onAfterCancel:'&?',     //applied

      onAfterNewDetail:'&',   //applied
      backToForm:'=?',        //applied

      selected:'=?selectedRows',
      onSelectRows:'&?',
      grid:'=?gridDetail',
      lmModel:'=listModel',
      ldModel:'=listDetailModel',
      ngDisabled:'=?',
      listApi:'=?',
      buttonSettings:'=?',
      buttonSettingsDetail: '=?',
      buttonCaption:'=?',
      customButtonSettings:'=?',
      customButtonSettingsDetail:'=?',
      listHeight:'=?',
      modalSize:'@?',
      modalSizeDetail:'@?',
      onValidateBeforeNew:'&?',
      listAppScope: '=?'
   },
   controller: function ($attrs, $element, $compile, $parse, $scope, $q, $timeout,$injector ) {
      var vm = this;
      vm.search = {};
      vm.selected = [];
      vm.selectAll = false;
      vm.show_modal={show:false};
      vm.show_modal_detail={show:false};
      vm.status={expand:true};
      if(!$attrs.mId) vm.mId = 'id';
      if(!$attrs.dId) vm.dId = 'id';
      vm.loading = false;
      vm.lmMode = 'view';
      vm.ldMode = 'view';
      //vm.detailForm={};
      //vm.detailForm.ldModel = $scope.ldModel;
      vm.lastIdx = 0;
      vm.lastIdxDetail = 0;
      vm.gridSelectedRows=[];
      if(!vm.childKey){vm.childKey='child'};
      if(!vm.listHeight){vm.listHeight=400};
      //console.log("childKey=>",vm.childKey);
      //vm.btnSettings={new:true,edit:true,delete:true,approve:true,reject:true,print:true};
      // if(vm.ngDisabled){
      //    vm.disabled = vm.ngDisabled;
      // }else{
      //    vm.disabled = false;
      // }
      $timeout(function(){
         if(vm.ngDisabled==undefined){
            vm.ngDisabled=false;
            vm.disabled = false;
         }else{
            if(vm.ngDisabled){
               vm.disabled = true;
            }else{
               vm.disabled = false;
            }
         }
      },0);

      //REM: Update by JH
      $timeout(function(){
         // vm.mData.forEach( function(data){
         //    data[vm.childKey] = [];
         // });
         // vm.lmModel[vm.childKey] = [];
         //console.log('lmModel=>',vm.lmModel[vm.childKey]);
      });
      //REM: End Update by JH

      if(!vm.buttonCaption) vm.buttonCaption={};
      vm.btnCaption={
                        new:'Tambah',edit:'Ubah',delete:'Hapus',view:'Lihat',selectall:'Pilih Semua',select:'Pilih',
                        back:'Kembali',cancel:'Batal',ok:'OK',save:'Simpan',
                        approve:'Setuju',reject:'Tolak',review:'Review',print:'Cetak'
                     };
      angular.forEach(vm.btnCaption, function(v,k){
         vm.btnCaption[k] = vm.buttonCaption[k] || v;
      })

      if(!vm.buttonSettings) vm.buttonSettings={};
      vm.btnSettings={
                        new:{title:'Tambah',enable:true,func:null,icon:'fa fa-fw fa-plus'},
                        edit:{title:'Ubah',enable:true,func:null,icon:'fa fa-fw fa-pencil'},
                        delete:{title:'Hapus',enable:true,func:null,icon:'fa fa-fw fa-times'},
                        view:{title:'Lihat',enable:true,func:null,icon:'fa fa-fw fa-list-alt'},
                        approve:{title:'Setuju',enable:false,func:null,icon:'fa fa-fw fa-thumbs-up'},
                        reject:{title:'Tolak',enable:false,func:null,icon:'fa fa-fw fa-thumbs-down'},
                        review:{title:'Review',enable:false,func:null,icon:'fa fa-fw fa-eye'},
                        print:{title:'Cetak',enable:false,func:null,icon:'fa fa-fw fa-print'},
                        selectall:{title:'Pilih Semua',enable:true,func:null,icon:'fa fa-fw fa-check-square-o'},
                        select:{title:'Pilih',enable:true,func:null,icon:'fa fa-fw fa-check'}
                     };
      angular.forEach(vm.btnSettings, function(v,k){
         angular.forEach(vm.btnSettings[k], function(xv,xk){
            if(vm.buttonSettings[k]!=undefined && vm.buttonSettings[k][xk]!=undefined){
               vm.btnSettings[k][xk] = vm.buttonSettings[k][xk];
            }
         })
      })
      // console.log("btnSettings=>",vm.btnSettings);
      if(!vm.customButtonSettings){
         vm.customBtnSettings=[]
      }
      else{
         vm.customBtnSettings = vm.customButtonSettings;
      }
      if(!vm.customButtonSettingsDetail){
         vm.customBtnSettingsDetail=[]
      }
      else{
         vm.customBtnSettingsDetail = vm.customButtonSettingsDetail;
      }
      // console.log("customBtnSettings=>",vm.customButtonSettings);
      // console.log("customBtnSettingsDetail=>",vm.customButtonSettingsDetail);
      //--------------
         //FACTORY
         //console.log("factory=>",vm.factoryName);
         if(vm.factoryName){
            var factoryInstance = $element.injector().get(vm.factoryName);
            vm.factory =  factoryInstance;
         }
         if(vm.factoryNameDetail){
            var factoryInstanceDetail = $element.injector().get(vm.factoryNameDetail);
            vm.factoryDetail =  factoryInstanceDetail;
         }
      //----------------------------------------------
      vm.lmItem = {};
      //----------------------------------------------
      var setSelected = function(item,b){
         item.selected = b;
         if(b){
           vm.selected.push(item);
         }else{
           vm.selected.splice(vm.selected.length-1, 1);
         }
         //console.log("sel=>",vm.selected);
      }
      vm.toggleSelected=function(sel){
         setSelected(sel,!sel.selected);
         if($attrs.onSelectRows){
            vm.onSelectRows()(vm.selected);
         }
      }
      function safeDigest($scope) {
          if (!$scope.$root.$$phase) {
              $scope.$digest();
          }
      }
      vm.toggleSelectAll=function(){
         vm.selectAll = !vm.selectAll;
         vm.selected=[];
         angular.forEach(vm.mdata, function(item){
            setSelected(item,vm.selectAll);
         });
         if($attrs.onSelectRows){
            vm.onSelectRows()(vm.selected);
         }
      }
      vm.statusClicked=function(row){
         if(row.expand==undefined) row.expand=true;
         row.expand = !row.expand;
         //console.log("status=>",row.expand);
      }
      //--------------------------------------------------------
      vm.actListView = function(item){
         vm.lmModel = angular.copy(item);
         if(vm.grid){
            vm.grid.data = vm.lmModel[vm.childKey] || [];
         }
         vm.mMode="view";
         vm.modalBtnSettings=null;
         vm.show_modal.show =! vm.show_modal.show;
         //console.log("show_modal=>",vm.show_modal);
      }
      vm.actListNew = function(){
         if(!$attrs.onValidateBeforeNew){
            vm.lmModel = {};
            vm.lmModel[vm.childKey]=[];
            if(vm.grid){
               vm.grid.data = vm.lmModel[vm.childKey];
            }
            vm.mMode="new";
            vm.modalBtnSettings=null;
            // console.log("beforenew",vm.onBeforeNew);
            if($attrs.onBeforeNew){
               vm.onBeforeNew()();
            }
            vm.show_modal.show =! vm.show_modal.show;
            if($attrs.onAfterNew){
               vm.onAfterNew()();
            }
            //console.log("show_modal=>",vm.show_modal);
         }else{
            vm.onValidateBeforeNew()().then(function(res){
               console.log("onValidateBeforeNew res=>",res);
               if(res){
                  vm.lmModel = {};
                  vm.lmModel[vm.childKey]=[];
                  if(vm.grid){
                     vm.grid.data = vm.lmModel[vm.childKey];
                  }
                  vm.mMode="new";
                  vm.modalBtnSettings=null;
                  console.log("beforenew",vm.onBeforeNew);
                  if($attrs.onBeforeNew){
                     vm.onBeforeNew()();
                  }
                  vm.show_modal.show =! vm.show_modal.show;
                  if($attrs.onAfterNew){
                     vm.onAfterNew()();
                  }
                  console.log("show_modal=>",vm.show_modal);
               }
            });
         }
      }
      vm.actListEdit = function(item){
         // console.log("actListEdit=>",item);
         vm.lmModel = angular.copy(item);
         if($attrs.onBeforeEdit){
            vm.onBeforeEdit()(item);
         }
         if(vm.grid){
            if(vm.lmModel[vm.childKey]==undefined){
               vm.lmModel[vm.childKey] = [];
               vm.grid.data = vm.lmModel[vm.childKey];
            }else{
               vm.grid.data = vm.lmModel[vm.childKey];
            }
         }
         if(vm.lmModel[vm.mId]){
            vm.mMode="edit";
         }else{
            vm.mMode="newedited";
         }
         vm.modalBtnSettings=null;
         vm.show_modal.show =! vm.show_modal.show;
         if($attrs.onAfterEdit){
            vm.onAfterEdit()();
         }
         //console.log("show_modal=>",vm.show_modal);
      }
      vm.actListDelete = function(){
         //console.log("actListDelete",vm.selected);
         if(vm.selected.length>0){
            vm.confirm('Delete '+vm.selected.length+' item(s).',vm.doListDelete,null);
         }
      }
      //--------------------------------------------------------
      vm.onListSave = function(item,mode){
         if($attrs.confirmSave=="true"){
            vm.confirm('Save Data.',vm.doListSave,item,mode);
         }else{
            vm.doListSave(item,mode);
         }
      }
      vm.doListSave = function(item,mode){
         // console.log("save_modal=>",item,mode);
         var updateMode = (mode=='edit' || mode=='newedited');
         //var updateMode = item[vm.mId];
         //var mode=(updateMode?'update':'create');
         if($attrs.onBeforeSave){
            vm.onBeforeSave()(item,mode);
         }
         if(updateMode){
               if(vm.factory){
                  vm.factory.update(item,vm.name).then(
                                             function(result) {
                                                //find the master Id
                                                var key = {};
                                                key[vm.mId] = item[vm.mId];
                                                var match = _.find(vm.mdata, key);
                                                var xitem = angular.copy(item);
                                                if(match){
                                                   var index = _.indexOf(vm.mdata, _.find(vm.mdata, key));
                                                   vm.mdata.splice(index, 1, xitem);
                                                }
                                                //console.log("bslist save=>",vm.mdata);
                                                if($attrs.onAfterSave) vm.onAfterSave()(item,mode);
                                             },
                                             function(err){
                                                console.log('create err',err);
                                                vm.savingState=false;
                                             }
                  )
               }else{
                  //find the master Id
                  if(item[vm.mId]){
                     var key = {};
                     key[vm.mId] = item[vm.mId];
                     var match = _.find(vm.mdata, key);
                     var xitem = angular.copy(item);
                     if(match){
                        var index = _.indexOf(vm.mdata, _.find(vm.mdata, key));
                        vm.mdata.splice(index, 1, xitem);
                     }
                  }else{
                     if(mode="newedited"){
                        //console.log("newedited=>",item);
                        var key = {};
                        key['index'] = item['index'];
                        var match = _.find(vm.mdata, key);
                        var xitem = angular.copy(item);
                        if(match){
                           var index = _.indexOf(vm.mdata, _.find(vm.mdata, key));
                           vm.mdata.splice(index, 1, xitem);
                        }
                     }
                  }
                  if($attrs.onAfterSave) vm.onAfterSave()(item,mode);
               }
         }else{
               //new or newedited
               if(vm.factory){
                  vm.factory.create(item,vm.name).then(
                                       function(result) {
                                          //console.log('create result=>',result);
                                          vm.mdata.push(item);
                                          if($attrs.onAfterSave) vm.onAfterSave()(item,mode);
                                       },
                                       function(err){
                                           console.log('create err',err);
                                           vm.savingState=false;
                                       }
                  )
               }else{
                  if(mode='new'){
                     vm.lastIdx++;
                     item.index="$$" + vm.lastIdx;
                     vm.mdata.push(item);
                     if($attrs.onAfterSave) vm.onAfterSave()(item,mode);
                  }
                  // else if(mode='newedited'){
                  //   //find the row
                  //   var matches = false;
                  //   _.find(vm.mdata, function(row){
                  //          matches = _.isEqual(item,row);

                  //   });
                  // }
               }
         }
         vm.show_modal.show = false;
      }
      vm.onListCancel = function(item){
         //console.log("cancel_modal=>",item);
         if($attrs.onBeforeCancel){
            vm.onBeforeCancel()();
         }
         if($attrs.onAfterCancel){
            vm.onAfterCancel()();
         }
      }
      vm.doListDelete = function(){
         //console.log("doListDelete=>",vm.selected);
         var id=[];
         for(var i=0;i<vm.selected.length;i++){
            id.push(vm.selected[i][vm.mId]);
         }
         if($attrs.onBeforeDelete){
            vm.onBeforeDelete()(id);
         }
         if(vm.factory){
            vm.factory.delete(id,vm.name).then(
                    function(result) {
                                       var deleteds = _.remove(vm.mdata, function(row){
                                                               var matches = false;
                                                               for(var i=0;i<vm.selected.length;i++){
                                                                 if(_.find(vm.selected,row)){
                                                                   matches = true;
                                                                 }
                                                               }
                                                               return matches;
                                                      });
                                       vm.selected=[];
                                       if($attrs.onAfterDelete) vm.onAfterDelete()(id);
                     },
                     function(err){
                         console.log('delete err',err);
                     }
            )
         }else{
            var deleteds = _.remove(vm.mdata, function(row){
                                    var matches = false;
                                    for(var i=0;i<vm.selected.length;i++){
                                      if(_.find(vm.selected,row)){
                                        matches = true;
                                      }
                                    }
                                    return matches;
                           });
            vm.selected=[];
            if($attrs.onAfterDelete) vm.onAfterDelete()(id);
         }
      }
      //--------------------------------------------------------
      vm.confirm = function(prompt,cb,item,mode) {
         bsAlert.alert(
                  {
                    title: prompt+' Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                  },
                  //confirm
                  function(){
                     cb(item,mode)
                  },
                  //cancel
                  function(dismiss) {
                     // console.log("$attrs.backToForm : ",$attrs.backToForm);
                     if ($attrs.backToForm || $attrs.backToForm=="true") {
                        if (mode=='new') {
                           vm.actListNew();
                           vm.lmModel = angular.copy(item);
                        } else if (mode='edit') {
                           vm.actListEdit(item);
                        }
                     }

                     //bsAlert.success("OK","Bye");
                  }
         )
      }
      //--------------------------------------------------------
      setResetModel = function(obj,item){
         for (var member in obj) delete obj[member];
         if(item==null){
            // for (var member in obj) delete obj[member];
         }else{
            angular.forEach(item,function(v,k){
               if(angular.isObject(v)){
                  setResetModel(obj[k],item[k]);
               }else{
                  obj[k]=item[k];
               }
               // console.log(v,k);
            })
         }
      }
      //--------------------------------------------------------
      vm.actListViewDetail = function(item){
         //vm.ldModel = angular.copy(item);
         setResetModel(vm.ldModel,item);
         vm.dMode="view";
         vm.modalBtnSettings_detail=null;
         vm.show_modal_detail.show =! vm.show_modal_detail.show;
         //console.log("show_modal_detail=>",vm.show_modal_detail);
      }
      vm.actListNewDetail = function(){
         //vm.ldModel = {};
         setResetModel(vm.ldModel,null);
         vm.dMode="new";
         vm.modalBtnSettings_detail=null;
         vm.show_modal_detail.show =! vm.show_modal_detail.show;
         //console.log("show_modal_detail=>",vm.show_modal_detail);
         // added by sss on 2018-01-25
         if($attrs.onAfterNewDetail){
            vm.onAfterNewDetail()(vm.ldModel);
         }
         //
      }
      vm.actListEditDetail = function(item){
         setResetModel(vm.ldModel,item);
         //console.log("editDetail 1=>",item);
         //vm.ldModel = angular.copy(item);
         //console.log("editDetail 2=>",vm.ldModel);
         if(vm.ldModel[vm.dId]){
            vm.dMode="edit";
         }else{
            vm.dMode="newedited";
         }

         //vm.dMode="edit";
         vm.modalBtnSettings_detail=null;
         vm.show_modal_detail.show =! vm.show_modal_detail.show;
         //console.log("show_modal_detail=>",vm.show_modal_detail);
      }
      vm.actListDeleteDetail = function(){
         //console.log("doListDeleteDetail=>",vm.gridSelectedRows);
         if(vm.gridSelectedRows.length>0){
            vm.confirm('Delete '+vm.gridSelectedRows.length+' item(s).',vm.doListDeleteDetail,null);
         }
      }
      //--------------------------------------------------------
      vm.onListSaveDetail = function(item,mode){
         if($attrs.confirmSave=="true"){
            vm.confirm('Save Data.',vm.doListSaveDetail,item,mode);
         }else{
            vm.doListSaveDetail(item,mode);
         }
      }

      vm.doListSaveDetail = function(item,mode){
         // console.log("save_modal detail=>",item,mode);
         //var updateMode = item[vm.dId];
         var updateMode = (mode=='edit' || mode=='newedited');
         var mode=(updateMode?'update':'create');
         if(updateMode){
               if(vm.factoryDetail){
                     vm.factoryDetail.update(item,vm.name).then(
                                                function(result) {
                                                   //find the detail Id
                                                   var key = {};
                                                   key[vm.dId] = item[vm.dId];
                                                   var match = _.find(vm.grid.data, key);
                                                   var xitem = angular.copy(item);
                                                   if(match){
                                                      var index = _.indexOf(vm.grid.data, _.find(vm.grid.data, key));
                                                      vm.grid.data.splice(index, 1, xitem);
                                                   }
                                                },
                                                function(err){
                                                    console.log('create err',err);
                                                    vm.savingState=false;
                                                }
                     )
               }else{
                     //find the detail Id
                     if(item[vm.dId]){
                        var key = {};
                        key[vm.dId] = item[vm.dId];
                        var match = _.find(vm.grid.data, key);
                        var xitem = angular.copy(item);
                        if(match){
                           var index = _.indexOf(vm.grid.data, _.find(vm.grid.data, key));
                           vm.grid.data.splice(index, 1, xitem);
                        }
                     }else{
                        if(mode="newedited"){
                           //console.log("newedited=>",item);
                           var key = {};
                           key['index'] = item['index'];
                           var match = _.find(vm.grid.data, key);
                           var xitem = angular.copy(item);
                           if(match){
                              var index = _.indexOf(vm.grid.data, _.find(vm.grid.data, key));
                              vm.grid.data.splice(index, 1, xitem);
                           }
                        }
                     }
               }
         }else{
               if(vm.factoryDetail){
                     vm.factoryDetail.create(item,vm.name).then(
                                          function(result) {
                                             //console.log('create result=>',result);
                                             // vm.grid.data.push(item);
                                             var temp = angular.copy(item);
                                             vm.grid.data.push(temp);
                                          },
                                          function(err){
                                              console.log('create err',err);
                                              vm.savingState=false;
                                          }
                     )
               }else{
                     vm.lastIdxDetail++;
                     item.index="$$" + vm.lastIdxDetail;
                     // vm.grid.data.push(item);
                     var temp = angular.copy(item);
                     vm.grid.data.push(temp);
               }
         }
         vm.show_modal_detail.show = false;
      }
      vm.onListCancelDetail = function(item){
         //console.log("cancel_modal_detail=>",item);
      }
      vm.doListDeleteDetail = function(){
         //console.log("doListDelete Detail=>",vm.gridSelectedRows);
         var id=[];
         for(var i=0;i<vm.gridSelectedRows.length;i++){
            id.push(vm.gridSelectedRows[i][vm.dId]);
         }
         if(vm.factoryDetail){
            vm.factoryDetail.delete(id,vm.name).then(
                        function(result) {
                                          var deleteds = _.remove(vm.grid.data, function(row){
                                                                  var matches = false;
                                                                  for(var i=0;i<vm.gridSelectedRows.length;i++){
                                                                    if(_.find(vm.gridSelectedRows,row)){
                                                                      matches = true;
                                                                    }
                                                                  }
                                                                  return matches;
                                                         });
                                          vm.gridSelectedRows=[];
                        },
                        function(err){
                            console.log('delete detail err',err);
                        }
            )
         }else{
                        var deleteds = _.remove(vm.grid.data, function(row){
                                                var matches = false;
                                                for(var i=0;i<vm.gridSelectedRows.length;i++){
                                                  if(_.find(vm.gridSelectedRows,row)){
                                                    matches = true;
                                                  }
                                                }
                                                return matches;
                                       });
                        vm.gridSelectedRows=[];
         }
      }
      //--------------------------------------------------------
      //Grid Setup
      if(vm.grid){
         vm.grid.enableRowSelection=true;
         vm.grid.enableFullRowSelection=false;
         vm.grid.enableRowHeaderSelection=true;
         vm.grid.enableSelectAll=true;
         vm.grid.multiSelect=true;

         vm.grid.enableGridMenu=true;
         vm.grid.enableColumnMenus=false;

         vm.grid.enableSorting=true;
         vm.grid.enableFiltering=true;

         vm.grid.minRowsToShow=8;

         vm.grid.appScopeProvider=this;
         //--------------------------------------------------------
         //Grid View Edit Button Column Template
          if(!$attrs.gridHideActionColumn) vm.gridHideActionColumn=false;
          var gridActionColWidth = 100;
          var btnActionEditTemplate =
                '<a href="" style="color:#777;" class="trlink"'+
                    ' uib-tooltip="View" tooltip-placement="bottom"'+
                    ' onclick="this.blur()"'+
                    ' ng-if="!grid.appScope.gridHideActionColumn && !row.groupHeader"'+
                    ' ng-click="grid.appScope.actListViewDetail(row.entity)">'+
                    '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>'+
                '</a>'+
                '<a href="" style="color:#777;" class="trlink"'+
                    ' uib-tooltip="Edit" tooltip-placement="bottom"'+
                    ' onclick="this.blur()"'+
                    ' ng-if="!grid.appScope.gridHideActionColumn && !vm.grid.appScope.hideEditButton'+
                           ' && !row.groupHeader && !grid.appScope.defHideEditButtonFunc(row)"'+
                    ' ng-click="grid.appScope.actListEditDetail(row.entity)">'+
                    '<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>'+
                '</a>'+
                '<a href="" style="color:#777;" class="trlink"'+
                    ' uib-tooltip="Delete" tooltip-placement="bottom"'+
                    ' onclick="this.blur()"'+
                    ' ng-if="!grid.appScope.gridHideActionColumn && !vm.grid.appScope.hideEditButton'+
                           ' && !row.groupHeader && !grid.appScope.defHideEditButtonFunc(row)"'+
                    ' ng-click="grid.appScope.actListDeleteDetail()">'+
                    '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>'+
                '</a>';
          vm.oriColDef = vm.grid.columnDefs;
          if($attrs.gridCustomActionButtonTemplate){
              if(vm.gridHideActionColumn){
                btnActionEditTemplate='';
                gridActionColWidth=0;
              }
              btnActionEditTemplate=btnActionEditTemplate+scope.$parent[$attrs.gridCustomActionButtonTemplate];
              var w=$attrs.gridCustomActionButtonColwidth;
              gridActionColWidth=gridActionColWidth+parseInt(w);
          }else{

          }
          // console.log(!attrs.gridInlineEdit && !scope.gridHideActionColumn);
          if(!$attrs.gridInlineEdit && (!vm.gridHideActionColumn || $attrs.gridCustomActionButtonTemplate)){
            vm.grid.columnDefs.push({ name:'action', allowCellFocus: false, width:gridActionColWidth, pinnedRight:true,
                                       enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                                       cellTemplate: btnActionEditTemplate
                                      });
          }
      }
      if($attrs.listApi){
         vm.listApi.addDetail = function(item,itemmaster){
                                 if(item && item !== false){
                                    // vm.grid.data.push(item);
                                    var temp = angular.copy(item);
                                    vm.grid.data.push(temp);
                                 }
                                 if(itemmaster){
                                    angular.forEach(itemmaster,function(v,k){
                                       vm.lmModel[k]=v;
                                    });
                                 }
                                };
         vm.listApi.clearDetail = function(item){
                                    vm.grid.data.length=0;
                                  };
         vm.listApi.clearSelected = function(){
            vm.selected=[];
         };
         vm.listApi.refreshListButton = function() {
            if(!vm.buttonCaption) vm.buttonCaption={};
            angular.forEach(vm.btnCaption, function(v,k){
               vm.btnCaption[k] = vm.buttonCaption[k] || v;
            })

            if(!vm.buttonSettings) vm.buttonSettings={};
            angular.forEach(vm.btnSettings, function(v,k){
               angular.forEach(vm.btnSettings[k], function(xv,xk){
                  if(vm.buttonSettings[k]!=undefined && vm.buttonSettings[k][xk]!=undefined){
                     vm.btnSettings[k][xk] = vm.buttonSettings[k][xk];
                  }
               })
            })
         };
         vm.listApi.consoleLog = function(p1,p2,p3){
          console.log("listApi.console p1=>",p1);
          console.log("listApi.console p2=>",p2);
          console.log("listApi.console p3=>",p3);
        }

      }
      //console.log("controller scope=>",$scope,vm);
      $scope.$watch(function() {
         return vm.ngDisabled;
      }, function(newv,oldv) {
            console.log("bslist ngDisabled watch=>",newv,oldv);
            // ctrl.disabled = newv;
            if(newv!=oldv){
               //console.log("xdate watch=>",newv,oldv);
               if(newv==undefined){
                  vm.disabled = false;
               }else{
                  if(newv){
                     vm.disabled = true;
                  }else{
                     vm.disabled = false;
                  }
               }
            }
      }, true);

   },
   link: function(scope,element,attrs,frmCtrl,ctrlx){
      var ctrl=scope.vm;
      ctrl.appScope=scope.$parent.$parent.$parent;

      // ctrl.listScope = scope.$parent;
      //ctrl.form = frmCtrl[0];
      //console.log("link scope=>",scope,ctrl);
      //console.log("link frmCtrl=>",frmCtrl[0]);
      //var mdlform = frmCtrl[0]["modal_lm1_"+attrs.formName];
      //resetForm(mdlform);
      //mdlform.name.$setValidity('required',true);
      //mdlform.email.$setValidity('required',true);
      //console.log("link frmCtrl modal=>",mdlform);
      //resetForm(ctrl.form["modal_ld1_"+attrs.formName][attrs.formNameDetail]);
      //--------------
         //FACTORY
         //scope.form = formCtrl[0];
         // var factoryInstance = element.injector().get(ctrl.factoryName);
         // ctrl.factory =  factoryInstance;
         // var factoryInstanceDetail = element.injector().get(ctrl.factoryNameDetail);
         // ctrl.factoryDetail =  factoryInstanceDetail;

      if(ctrl.grid){
         ctrl.grid.onRegisterApi = function(gridApi) {
                 // set gridApi on scope
                 scope.gridApi = gridApi;

                 scope.gridApi.selection.on.rowSelectionChanged(scope,function(row) {
                     //scope.selectedRows=null;
                     ctrl.gridSelectedRows = scope.gridApi.selection.getSelectedRows();
                      //console.log("detail selected=>",ctrl.gridSelectedRows);
                     // if(vm.onSelectRows){
                     //     vm.onSelectRows()(scope.selectedRows);
                     // }
                 });
                 scope.gridApi.selection.on.rowSelectionChangedBatch(scope,function(row) {
                     //scope.selectedRows=null;
                     ctrl.gridSelectedRows = scope.gridApi.selection.getSelectedRows();
                     //console.log("detail selected=>",ctrl.gridSelectedRows);
                     // if(vm.onSelectRows){
                     //     vm.onSelectRows()(vm.selectedRows);
                     // }
                 });
         }
      }

      // scope.$watch("vm.gridHideActionColumn",function(newv,oldv){
      //    if(newv!=oldv){
      //      if(newv){
      //          vm.grid.columnDefs.splice(vm.grid.columnDefs.length-1, 1);
      //          //scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
      //      }else{
      //          vm.grid.columnDefs.push({ name:'action', allowCellFocus: false, width:gridActionColWidth, pinnedRight:true,
      //                                     enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
      //                                     cellTemplate: btnActionEditTemplate
      //                                    });
      //      }
      //    }
      // })

      // scope.$watch(function() {
      //    return ctrl.ngDisabled;
      // }, function(newv,oldv) {
      //       console.log("bslist ngDisabled watch=>",newv,oldv);
      //       // ctrl.disabled = newv;
      //       if(newv!=oldv){
      //          //console.log("xdate watch=>",newv,oldv);
      //          if(newv==undefined){
      //             ctrl.disabled = false;
      //          }else{
      //             if(newv){
      //                ctrl.disabled = true;
      //             }else{
      //                ctrl.disabled = false;
      //             }
      //          }
      //       }
      // }, true);
      // $scope.$watchGroup(['vm.xdate','vm.ngDisabled'],function(newv,oldv,scope) {
      //    // console.log("xdate watch=>",newv[0],oldv[0],scope);
      //    // console.log("ngDisabled watch=>",newv[1],oldv[1],scope);
      //    if(newv[0] != oldv[0]){
      //      //console.log("xdate watch=>",newv,oldv);
      //      var typeofValue = typeof (newv[0]);
      //      if (typeofValue === 'string'){
      //          vm.xdate=new Date(newv[0]);
      //      }
      //    }
      //    // console.log("------------------------------------------");
      //        // console.log("watch change element=>",$element);
      //        if(newv[1]){
      //          // $timeout(function(){
      //            // console.log("------------");
      //            // var elx = $element.children()[0];
      //            // var el = angular.element(elx);
      //            // console.log("ngDisabled , el[0]=>",vm.ngDisabled,el[0].name,el);
      //            // el.attr('disabled','disabled');
      //            vm.disableInput=true;
      //          // })
      //        }else{
      //          // $timeout(function(){
      //            vm.disableInput=false;
      //          // })
      //        }

      // }, true);

   }
  }
});
