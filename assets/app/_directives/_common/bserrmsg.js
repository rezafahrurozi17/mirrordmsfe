angular.module('bserrmsg',[]).directive('bserrmsg', function ($parse,$timeout) {
  // 
    return {
      template:'<div>'+
                // '<pre>{{field.$invalid}}</pre>'+
                '<em ng-messages="field.$error"'+
                     ' ng-show="field.$touched && field.$invalid" class="help-block" style="color:#b94a48">'+
                    '<div ng-messages-include="_sys/message/error_msg.html"></div>'+
                '</em>'+
                '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      require:'^form',
      scope:{},
      link: function postLink(scope, element, attrs,formCtl) {
        $timeout(function(){
            scope.form = formCtl;
            scope.field = scope.form[attrs.field];
            // console.log('field:',scope.form,scope.field,attrs.field);
        },0);
      }
    };
  });