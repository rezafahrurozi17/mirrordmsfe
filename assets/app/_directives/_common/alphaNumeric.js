angular.module('alphaNumeric',[]).directive('alphaNumeric', function() {

    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {type: '@?mask'},
        link: function(scope, element, attrs, modelCtrl) {
          modelCtrl.$parsers.push(function (inputValue) {
              if (inputValue == undefined) return '' 
              if(scope.type == 'NPWP'){
                var transformedInput = inputValue.replace(/[^0-9\-\.\s]/g, ''); 
              } else if(scope.type == 'SIUP'){
                var transformedInput = inputValue.replace(/[^0-9a-zA-Z\-\/\.\s]/g, '');
              } else if(scope.type == 'BPKB'){
                var transformedInput = inputValue.replace(/[^0-9a-zA-Z\-\/\s]/g, '');
              } else if(scope.type == 'Instansi'){
                var transformedInput = inputValue.replace(/[^0-9a-zA-Z\-\.\s]/g, '');
              } else if(scope.type == 'RTRW'){
                var transformedInput = inputValue.replace(/[^0-9\/\s]/g, '');
              } else if(scope.type == 'TDP'){
                var transformedInput = inputValue.replace(/[^0-9a-zA-Z\/\s]/g, '');
              } else if(scope.type == 'Huruf'){
                var transformedInput = inputValue.replace(/[^a-zA-Z\ \.\s]/g, '');
              } else if(scope.type == 'Pekerjaan'){
                var transformedInput = inputValue.replace(/[^a-zA-Z\ \.\-\(\)\/\s]/g, '');
              } else if(scope.type == 'Name'){
                var transformedInput = inputValue.replace(/[^a-zA-Z\ \.\,\s]/g, '');
              }else{
                var transformedInput = inputValue.replace(/[^0-9a-zA-Z\s]/g, '');
              }
               
              if (transformedInput!=inputValue) {
                 modelCtrl.$setViewValue(transformedInput);
                 modelCtrl.$render();
              }      
              return transformedInput;
                       
          });
          element.on('keyup', function (event) {
            var value = $(this).val();
            if (value.length == 1 && event.which == 32) {
                // space at first character 
                value = value.replace(" ", '');
                $(this).val(value);
            }
        });
        }
      };
});