angular.module('bsRights', [])
  .factory('bsRights', function(Tabs, MENU_TREE) {
    return {
      get: function(sref) {
        function deepFind(node, sref) {
          var res = null;
          for (var i = 0; i < node.length; i++) {
            if (node[i].Sref == sref) {
              res = node[i];
              return res;
              break;
            } else {
              if (node[i].Child.length > 0 && node[i].Child != null && res == null) {
                res = deepFind(node[i].Child, sref);
                if (res) break;
              }
            }
          }
          return res;
        }

        var rb_obj = {
          view: false,
          new: false,
          edit: false,
          delete: false,
          approve: false,
          reject: false,
          review: false,
          print: false,
          custom1: false,
          custom2: false,
          custom3: false,
          custom4: false,
          custom5: false,
          custom6: false,
          custom7: false,
          custom8: false
        };

        var menutree = MENU_TREE.ITEMS;
        var tab = deepFind(menutree, sref);
        if (tab) {
          var be = tab.ByteEnable;
          var ba = tab.ByteAvailable;
          var eff = be & ba;
          var i = 0;
          //-----------------------
          angular.forEach(rb_obj, function(v, k) {
            if ((eff & Math.pow(2, i)) == Math.pow(2, i)) {
              rb_obj[k] = true;
            }
            i++;
          })
        }
        return angular.copy(rb_obj);
      }
    }
  });
