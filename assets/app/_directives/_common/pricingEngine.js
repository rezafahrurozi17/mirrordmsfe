angular.module('pricingEngine', []).directive('pricingEngine', function($parse, $timeout, $templateCache, Pricing, bsNotify, $http) { //bsPrint -> bs-print

    return {
        template: function(element, attrs) { //attrs dipakai jika static atau untuk manipulasi template sebelum di-compile

            //#region Template
            var strTemplate =
                '<div class="BodyRows col-lg-12 col-xs-12"   ng-repeat="row in jsData.BodyRows " >\
                <div ng-transclude="bodyCustom" ng-if="row.Cells[0].Value == \'+\'" /> \
                <hr ng-if="row.IsLine" class="ColLine" />\
                <div ng-if="!row.IsLine && row.Cells[0].Value != \'+\'"   ng-repeat="cell in row.Cells" class="{{cell.CSSClass}}" >\
                    <label ng-hide="cell.IsCode" class="cell.CSSClass" >{{cell.Value}}</label>\
                    <input style="text-align:right" input-currency number-only onkeypress="return event.charCode >= 48 && event.charCode <= 57" ng-show="cell.IsCode" class="cell.CSSClass" type="text" ng-model="cell.Value"  />\
             </div></div>';

            var strDebugTemplate = '</br>\
                <div class="col-md-12 col-xs-12" style="1px dotted" ><br/><p>THIS IS DEBUG DIV</p>\
                    <div    ng-repeat="x in jsData.Cells " >\
                        <label style="min-width:100px" >{{x.Code}}  :  {{x.Value}}</label>\
                    </div>\
                    <label>scope.jsData.Codes["</label> <input type="text" ng-model="debugCode" /> <label>"] = </label>\
                    <input type="button" value="Show Value to Console" data-ng-click="ShowLogValue()"/> \
                </div> ';

            var xTemplate =
                // '<div  style="border:3px dashed">  ' +
				'<div>  ' +
                strTemplate +
                '<br/> <div ng-hide="disableButton == true" ><input ng-disabled="validCalculate" type="button" class="rbtn btn"  value="Kalkulasi" data-ng-click="GetDataFromFactory()"/> </div> ' +
                //  strDebugTemplate +
                '</div>';

            return xTemplate;
            //#endregion Template

        },
        restrict: 'EA',
        replace: true,

        transclude: { 'bodyCustom': '?formBodyCustom' },
        scope: {
            branch: '@',
            jsData: '=',
            validCalculate: '=',
            isCalculateOPD: '=isCalculateopd',
            disableButton: '=disableButton',
            debugCode: '@',
            url_pricing: '=urlPricing',
            jumlahData: '='
        },
        link: function postLink(scope, element, attrs, formCtl) {

            if (typeof scope.disableButton == "undefined") { scope.disableButton = false; }
            if (typeof scope.validCalculate == "undefined") { scope.validCalculate = false; }
            //#region Constructor

            //#endregion Contructor



            //#region Action
            scope.GetDataFromFactory = function() {
                // console.log("SendingData");
                // console.log("url pricing", scope.url_pricing);
                // console.log("js Data Directive", scope.jsData);


                Pricing.getData(scope.url_pricing, scope.jsData)
                    .then(
                        function(res) {
                            //console.log("Testing2");
                            //console.log(res);
                            // console.log('jumlah data', scope.jumlahData);
                            scope.jsData = res.data;
                            scope.isCalculateOPD = true;
                            console.log("Testing2",scope.isCalculateOPD);
                            //DUMMY TEST TANPA WS 
                            /*
                            scope.jsData = [
                            {
                            "Label": "Disc count ",
                            "Value": "10.000.000",
                            "Variable": "$Disc$",
                            "Type": "$",
                            "Sort": 2,
                            "Disabled":"0",
                            },
                            {
                            "Label": "Total ",
                            "Value": "9.000.000",
                            "Variable": "#Total#",
                            "Type": "#",
                            "Sort": 1,
                            "Disabled" : "1",
                            }
                
                            ];*/

                        },
                        function(err) { console.log('Error upload'); }
                    );
            }

            scope.ShowLogValue = function() {
                console.log(scope.jsData);
                //console.log(scope.jsData.Codes); 
                console.log(" scope.jsData.Codes[\"" + scope.debugCode +
                    "\"] = " +
                    scope.jsData.Codes[scope.debugCode]);
            }
            console.log('inside controller');
            console.log(scope);
            //#endregion Action





        }
    };
});