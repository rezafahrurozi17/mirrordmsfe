angular.module('bslistview',[]).directive('bslistview', function ($parse,$state) {
    return {
        template : function(element, attrs) {
                    var stateData = $state.current.data;
                    // DEFAULT VALUE
                    if(!attrs.formTitle){ attrs.formTitle = stateData.title; }
                    if(!attrs.formName){ attrs.formName =  attrs.formTitle.replace(/ /g,''); }
                    if(!attrs.itemTitle){ attrs.title='title'};
                    if(!attrs.itemDesc){ attrs.desc='description'};    
                    if(attrs.showActionButton){
                        attrs.showActionButton=(attrs.showActionButton=='on');
                    }                
                    var xtemplate =
                        '<div class="ui segment">'+
                            '<sm-dimmer show="vm.dimmerVisible" sm-dimmer-bind="{transition: \'vertical flip\'}">'+
                                '<div class="content">'+
                                    '<div class="row">'+
                                        '<fieldset style="border:3px solid #e2e2e2;padding:20px">'+
                                            '<form name="'+attrs.formName+'" ng-submit="vm.save()" novalidate>'+
                                                '<div ng-transclude="dimFormDiv"></div>'+
                                                '<div class="row">'+
                                                    '<button type="button" class="btn btn-default" ng-click="vm.cancel();">{{vm.listDetailMode == "view" ? "KEMBALI" : "BATAL"}}</button>'+
                                                    '<button type="submit" ng-show="vm.listDetailMode == \'edit\'" ng-disabled="addressForm.$invalid" class="btn btn-default">SIMPAN</button>'+
                                                '</div>'+
                                            '</form>'+
                                        '</fieldset>'+
                                    '</div>'+
                                '</div>'+
                            '</sm-dimmer>'+
                            '<div class="contact-list panel panel-default">'+
                                '<div class="panel-body">'+
                                    '<div class="contact ui-list-view-striped" ui-list-view="list in vm.data | filter:search">'+
                                        '<div style="z-index:1;position:absolute;display:block;width:100%;height:100%; background-color:rgba(124, 177, 208, 0.28);opacity:0.7;" ng-show="list.selected && '+attrs.showActionButton+'">'+
                                            '<div style="float:right;height:100%;margin-right:10px;text-align:center;padding:15px 10px; display:-webkit-box;-webkit-box-align:center;-webkit-box-pack:center;">'+
                                                '<div style="width:50% 0;">'+
                                                    '<a class="ui-list-menu4" ng-class="{uiListBtnMore:vm.showMore}" style="" ng-show="vm.selected.length>0" ng-click="vm.moreClicked()" onclick="this.off(\'mouseleave\');" tabindex="-1">'+
                                                        '<i class="fa fa-caret-left"></i>'+
                                                    '</a>'+
                                                    // '<a class="ui-list-menu2" style="" ng-show="vm.showMore" ng-click="vm.menuClicked(list)">'+
                                                    //     '<i class="fa fa-print"></i>'+
                                                    // '</a>'+
                                                    // '<a class="ui-list-menu2" style="" ng-show="vm.showMore" ng-click="vm.menuClicked(list)">'+
                                                    //     '<i class="fa fa-thumbs-down"></i>'+
                                                    // '</a>'+
                                                    // '<a class="ui-list-menu2" style="" ng-show="vm.showMore" ng-click="vm.menuClicked(list)">'+
                                                    //     '<i class="fa fa-thumbs-up"></i>'+
                                                    // '</a>'+
                                                    '<a class="ui-list-menu2" ng-repeat="option in vm.optionList" style=""'+ 
                                                        'ng-show="vm.showMore"'+ 
                                                        'ng-click="option.func(list)">'+
                                                        '<i class="fa {{option.icon}}"></i>'+
                                                    '</a>'+
                                                    (attrs.hideDeleteButton ? '' :
                                                    '<a class="ui-list-menu2" style="" ng-show="vm.selected.length>0" ng-click="vm.actListDelete()">'+
                                                        '<i class="fa fa-times"></i>'+
                                                    '</a>'
                                                    )+
                                                    (attrs.hideEditButton ? '' :
                                                    '<a class="ui-list-menu2" style="" ng-show="vm.selected.length<2" ng-click="vm.actListEdit(list)">'+
                                                        '<i class="fa fa-pencil"></i>'+
                                                    '</a>'
                                                    )+
                                                    '<a class="ui-list-menu2" style="" ng-show="vm.selected.length<2" ng-click="vm.actListView(list)">'+
                                                        '<i class="fa fa-list-alt"></i>'+
                                                    '</a>'+                                                    
                                                    '<a class="ui-list-menu3" style="" ng-click="vm.menuClicked(list)">'+
                                                        '<i class="fa fa-check" style="color:green;"></i>'+
                                                    '</a>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+

                                        '<div style="float:right;height:100%;margin-right:10px;text-align:center;padding:20px 10px;display:-webkit-box;-webkit-box-align:center;-webkit-box-pack:center;" ng-show="'+attrs.showActionButton+'">'+
                                            '<div style="margin:50% 0;">'+
                                                '<a class="ui-list-menu" style="" ng-hide="list.selected" ng-click="vm.menuClicked(list)">'+
                                                    '<i class="fa fa-ellipsis-v"></i>'+
                                                '</a>'+
                                            '</div>'+
                                        '</div>'+

                                        '<div style="padding: 15px 10px;">'+
                                            (attrs.template ? attrs.template :
                                                '<p class="list"><strong>{{ list.'+attrs.itemTitle+' }}</strong></p>'+
                                                '<div class="list">{{ list.'+attrs.itemDesc+' }}</div>'
                                            )+
                                            // '<div ng-transclude="listDiv"></div>'+
                                            '<div ng-include="'+attrs.templateUrl+'"></div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="panel-footer">'+
                                    'Selected: {{vm.selected.length}}'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                    return xtemplate;
        },
        restrict : 'E',
        replace: true,
        transclude: {
            'dimFormDiv':'?bsformDimmer',
            'listDiv':'?bslistTemp'
        },
        controllerAs: 'vm',
        bindToController: true,
        scope : {
            data:'=',
            optionList:'=?',
            mData:'=model',
            doCustomSave: '&?',
            doCustomCancel: '&?',
            // itemTemplate: '@'
            // selected: '=',
        },
        controller: function ($attrs, $element, $compile, $parse, $scope, $q, $timeout ) {
            const vm = this;
            console.log("scope=>",$scope.vm); 

            // if(attrs.optionList){
            //     vm.customActFunc = $scope.$parent.$eval(attrs.optionList);
            // }

            vm.showMore=false;
            vm.selected = [];
            vm.listDetailMode = 'view';
            vm.dimmerVisible = false;

            vm.menuClicked=function(sel){
                vm.showMore=false;
                sel.selected = !sel.selected;
                console.log(sel.selected);        
                if(sel.selected){
                    vm.selected.push(sel);              
                }else{
                    vm.selected.splice(vm.selected.length-1, 1);            
                }
                console.log("sel=>",vm.selected);        
            }  
            vm.moreClicked = function(sel){
                vm.showMore=!vm.showMore;
            }     
            var prepareData = function(sel, mode){   
                vm.mData = angular.copy(sel);
                vm.listDetailMode = mode;
                vm.dimmerVisible = true;
            }
            // ---- action ----
            vm.cancel = function(){
                vm.dimmerVisible=false;
                if($attrs.doCustomCancel){
                    vm.doCustomCancel()();
                }
            }
            vm.save = function(){          
                vm.dimmerVisible=false;      
                if($attrs.doCustomSave){
                    vm.doCustomSave()();
                    vm.mData = {};
                }
            }
            // 
            // ---- view ----
            vm.actListView = function(sel){
                vm.editList = true;                
                prepareData(sel,'view');
            }
            //
            // ---- edit ----
            vm.actListEdit = function(sel){
                vm.editList = false;
                prepareData(sel,'edit');
            }
            //  
            // ---- delete ----
            vm.actListDelete = function(){
                // $scope.del = sp;
                // ngDialog.openConfirm ({
                //   template:'<fieldset style="border:3px solid #e2e2e2;padding:20px;text-align:center;">\
                //             <div class="row">Peringatan</div>\
                //             <div class="row">Anda yakin ingin menghapus {{selected.length}} data?</div>\
                //             <div class="row"></div>\
                //             <div class="ngdialog-buttons">\
                //               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                //               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deleteThisRow(del)">Ya</button>\
                //             </div>\
                //           </fieldset>'
                //          ,
                //   plain: true,
                //   scope: $scope
                // });
                // $scope.deleteThisRow(sp);
            }   
        }
  }
});