angular.module('numberOnly', []).directive('numberOnly', function () {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });

            element.on('keyup', function (event) {
                var value = $(this).val();
                if (value.length == 1 && event.which == 32) {
                    // space at first character 
                    value = value.replace(" ", '');
                    $(this).val(value);
                }
            });
        }
    };
});