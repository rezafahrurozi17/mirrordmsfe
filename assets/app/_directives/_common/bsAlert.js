(function (root, factory) {
    "use strict";

    /*global define*/
    if (typeof define === 'function' && define.amd) {
        define(['angular', 'sweetalert'], factory);  // AMD
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('angular'), require('sweetalert')); // Node
    } else {
        factory(root.angular, root.swal);					// Browser
    }

}(this, function (angular, swal) {
    "use strict";

    angular.module('bsAlert', [])
        .factory('bsAlert', ['$rootScope', 'SweetAlertConfig', function ($rootScope, SweetAlertConfig) {
            //public methods
            var self = {

                alert: function (arg1, arg2, arg3) {

                    //merge with default config
                    var arg1 = angular.extend(SweetAlertConfig,arg1);
                    //console.log("arg1=>",arg1,SweetAlertConfig);
                    // $rootScope.$evalAsync(function () {
                    //     if (typeof(arg2) === 'function') {
                    //         swal(arg1, function (isConfirm) {
                    //             $rootScope.$evalAsync(function () {
                    //                 arg2(isConfirm);
                    //             });
                    //         }, arg3);
                    //     } else {
                    //         swal(arg1, arg2, arg3);
                    //     }
                    // });
                    $rootScope.$evalAsync(function () {
                        if (typeof(arg2) === 'function') {
                            swal(arg1).then( function (isConfirm) {
                                                $rootScope.$evalAsync(function () {
                                                    arg2(isConfirm);
                                                });
                                             },
                                             arg3
                            );
                        } else {
                            swal(arg1).then(arg2,arg3);
                        }
                    });
                },
                success: function (title, message) {
                    $rootScope.$evalAsync(function () {
                        swal(title, message, 'success');
                    });
                },
                error: function (title, message) {
                    $rootScope.$evalAsync(function () {
                        swal(title, message, 'error');
                    });
                },
                warning: function (title, message) {
                    $rootScope.$evalAsync(function () {
                        swal(title, message, 'warning');
                    });
                },
                info: function (title, message) {
                    $rootScope.$evalAsync(function () {
                        swal(title, message, 'info');
                    });
                },
                question: function (title, message) {
                    $rootScope.$evalAsync(function () {
                        swal(title, message, 'question');
                    });
                },
                // showInputError: function (message) {
                //     $rootScope.$evalAsync(function () {
                //         swal.showInputError(message);
                //     });
                // },
                close: function () {
                    $rootScope.$evalAsync(function () {
                        swal.close();
                    });
                }
            };

            return self;
        }]).constant('SweetAlertConfig', {
        title: '',
        text: '',
        type: null,
        allowOutsideClick: false,
        showConfirmButton: true,
        showCancelButton: false,
        confirmButtonText: 'Setuju',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Kembali',
        imageUrl: null,
        //imageSize: null,
        //timer: null,
        //customClass: '',
        //html: false,
        animation: true,
        allowEscapeKey: true,
        //inputType: 'text',
        //inputPlaceholder: '',
        //inputValue: '',
        showLoaderOnConfirm: false
    });
}));