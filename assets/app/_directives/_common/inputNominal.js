angular.module('inputNominal',[]).directive('inputNominal', function() {
    return {  
        restrict: 'A',  
        link: function (scope, elm, attrs, ctrl) {  
            elm.on('keyup', function (event) {  
                var $input = $(this);  
                var value = $input.val();  
                value = value.toString().replace(/\D/g, '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'); 
                if (this.value.length == 1 && event.which == 48) {
                    // zero at first character 
                    value = value.replace("0", '')
                }
                $input.val(value); 
                if (event.which == 64 || event.which == 16) {  
                    // numbers  
                    return false;  
                } if ([8, 13, 27, 37, 38, 39, 40, 110].indexOf(event.which) > -1) {  
                    // backspace, enter, escape, arrows  
                    return true;  
                } else if (event.which >= 48 && event.which <= 57) {  
                    // numbers  
                    return true;  
                } else if (event.which >= 96 && event.which <= 105) {  
                    // numpad number  
                    return true;  
                } else if ([46, 110, 190].indexOf(event.which) > -1) {  
                    // dot and numpad dot  
                    return true;  
                } else {  
                    event.preventDefault();  
                    return false;  
                } 
            });  
        }  
    }  
    // return {
    //     restrict: 'A',
    //     // require: 'ngModel',
    //     link: function(scope, element, attrs, modelCtrl) {
    //       modelCtrl.$parsers.push(function (inputValue) {
    //           if (inputValue == undefined) return '' 
    //           var transformedInput = inputValue.toString().replace(/\D/g, '').toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'); 
    //           if (transformedInput!=inputValue) {
    //              modelCtrl.$setViewValue(transformedInput);
    //              modelCtrl.$render();
    //           }      
    //           return transformedInput;         
    //       });
    //     }
    //   }
});