angular.module('bsFormMobile', ['ui.bootstrap']).directive('bsformMobile', function($rootScope, $parse, $timeout, $templateCache, bsNotify) { //bsformModbile -> bsform-Mobile

    return {
        template: function(element, attrs) { //attrs dipakai jika static atau untuk manipulasi template sebelum di-compile

            //region Attribute

            var rowTemplate = $templateCache.get(attrs.rowTemplateUrl);
            var listTitle = attrs.listTitle;
            var infoTotalData = attrs.infoTotaldata;

            //endregionAttribute
            var xStyle = "<style type='text/css'> \
            .td,.td1,.th,.th1{text-align:left}.card{background:#fff;border-radius:2px;position:relative}.card-2{box-shadow:0 3px 3px rgba(0,0,0,.1),0 2px 8px rgba(0,0,0,.1)}.card-1{box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);transition:all .3s cubic-bezier(.25,.8,.25,1)}.card-1:hover{box-shadow:0 14px 28px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.22)}.row{margin:0}@media only screen and (max-width:430px){.iconFilter{margin-left:-4px}}@media only screen and (max-width:500px){.btnUp{margin:-28px 0px 0 0!important}.iconFilter{margin-left:-4px}#desktopderectivemobile{display:none!important}#mobilederectivemobile{display:block!important}}@media (min-width:501px) and (max-width:600px){.btnUp{margin:-36px 0px 0 0!important}#desktopderectivemobile{display:none!important}#mobilederectivemobile{display:block!important}}@media (min-width:601px){.btnUp{margin:-33px 0 0 0!important}#mobilederectivemobile{display:none!important}}.box-hide-setup,.box-show-setup{-webkit-transition:all linear .3s;-moz-transition:all linear .3s;-ms-transition:all linear .3s;-o-transition:all linear .3s;transition:all linear .3s}.input-group.input-group-unstyled input.form-control{-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px}.input-group-unstyled .input-group-addon{border-radius:4px;border:0;background-color:transparent}.adv-srch,.adv-srch-on{border:1px solid #e2e2e2}.adv-srch{padding:10px;margin-bottom:0;background-color:rgba(213,51,55,.02)}.adv-srch-on{padding-top:4px!important;padding:6px;margin:0 3px 0 0;margin-bottom:-5px!important;border-bottom-style:none!important;background-color:#FEFBFB;border-top-left-radius:3px;border-top-right-radius:3px}.colorsrch{color:red}.modalpadding{border-radius:3px;padding:0!important}.noppadingFormGroup{margin-bottom:0!important}.ScrollStyle{max-height:600px;overflow-y:scroll}.item-modal{border:none;padding-bottom:15px;border-bottom:1px solid #ddd!important;margin:10px 30px!important}.mobileAddBtn{padding:0 0 0 1!important;font-size:15px}.mobileFilterBtn{padding:0 0 0 1!important;font-size:13px;width:100%;float:right}.nopadding,.nopaddingmargin{padding:0!important}.filterMarg{margin-bottom:5px}.mobilesearchBtn{text-decoration:none}.vertical-alignment-helper{display:table;height:100%;width:100%;pointer-events:none}.vertical-align-center{display:table-cell;vertical-align:middle;pointer-events:none}.modal-content{width:inherit;height:inherit;margin:0 auto;pointer-events:all}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0)}100%{-webkit-transform:rotate(360deg)}}@keyframes spin{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.nopaddingmargin{margin:0!important}.ktk{border-style:solid;border-bottom:none!important;border-width:1px;border-color:#ddd}.atasPadding{padding-top:7px}.atasMargin{margin-top:7px}i .fa .fa-pencil{margin-top:-5px}.table1{font-family:arial,sans-serif;border-collapse:collapse;background-color:#ddd;width:100%}.td1,.th1{padding:8px}.tr:nth-child(even){background-color:#fff}.form-input{margin-bottom:5px}</style>"

            // #e8eaed
            //region Modal
            var xModal =
                '<div class="ui modal {{modal_name}}" style="background:none">\
                            <div ng-form name="contactForm"> \
									<div class="vertical-alignment-helper"> \
										<div class="modal-dialog vertical-align-center" style="padding: 10px 10px 10px 10px"> \
											<div class="modal-content" style="border-bottom-width: 0px; width:100%; height:100%"> \
												<div class="modal-body modalpadding"> \
													<ul class="list-group" style="padding:20px 0 0px 0"> \
														<a ng-if="(modalLihat==\'true\')?false:true" ng-click="viewSelectedItem(selected_data)"> \
															<li class="list-group-item item-modal"> \
                                                                <span><i class="fa fa-fw fa-lg fa-list-alt" style="margin-right: 15px"></i></span> \
                                                                    Lihat Detail \
                                                            </li> \
														</a>\
														<a ng-if="(modalubah==\'true\')?false:true" ng-click="updateSelectedItem(selected_data)"> \
															<li class="list-group-item item-modal"> \
                                                                <span><i class="fa fa-fw fa-lg fa-pencil" style="margin-right: 15px"></i></span> \
                                                                    Ubah Data \
                                                            </li> \
														</a> \
                                                        <div ng-transclude="modal" /> \
														<a ng-if="(modalhapus==\'true\')?false:true" ng-click="openModalKonfirmasi(3)"> \
															<li class="list-group-item item-modal" style="border-bottom: none !important"> \
                                                                <span><i class="fa fa-fw fa-lg fa-times" style="margin-right: 15px"></i></span> \
                                                                    Hapus Data \
                                                            </li> \
														</a> \
													</ul> \
												</div> \
											</div> \
										</div> \
									</div> \
							</div>\
					 	</div>';

            //endregion Modal

            var xModalKonfirmasi =
                '<div class="ui modal {{modal_name}}konfirmasi"> \
                    <div class="modalDefault-dialog"> \
                        <div class="modal-header"> \
                            <button type="button" class="close" data-dismiss="modal">&times;</button> \
                            <h4 class="modal-title">{{confirmation}} Data</h4> \
                        </div> \
                        <div class="modal-body"> \
                            Apakah anda yakin dengan pilihan ini ? \
                        </div> \
                        <div class="modal-footer"> \
                            <p align="Right"> \
                                <button class="rbtn btn" style="float: right" ng-click="konfirmasiok(statusModal)">OK</button> \
                                <button class="rbtn btn" style="float: right" ng-click="konfirmasicancel()">Cancel</button> \
                            </p> \
                        </div> \
                    </div> \
                </div>';

            //region Template
            var xTemplate =
            '<div style=min-height:100vh>' +
                xStyle +
                '<div class="row btnUp" style="margin: 0 0 0 0;"> \
                    <div ng-if="buttonGrouping == true" id="addMode" style="float:right;" ng-show="!isEditingMode"> \
                        <div id="desktopderectivemobile" class="btn-group "> \
                            <button id="addBtn" ng-if="((buttonTambah==\'enable\')?true:false) && ((add_custombutton==\'enable\')?true:false)" class="rbtn btn" tooltip-placement="bottom" tooltip-trigger="focus" tooltip-animation="true" uib-tooltip="Tambah" style="float: right; margin-right: 0px !important" ng-click="addNewItemCustom();" > \
                                <i class="fa fa-plus"></i> \
                            </button> \
                            <button id="addBtn" ng-if="((buttonTambah==\'enable\')?true:false) && ((add_custombutton==\'disable\')?true:false)" class="rbtn btn" style="float: right; margin-right: 0px !important" ng-click="addNewItem();" > \
                                <i class="fa fa-plus"></i> \
                            </button> \
                            <button class="ubtn btn mobilesearchBtn" style="float: right; padding-bottom: 5px" ng-click="refresh()" onclick="this.blur();" tooltip-placement="bottom" tooltip-trigger="focus" tooltip-animation="true" uib-tooltip="Refresh"  tabindex="0"> \
                                <i class="fa fa-fw fa-refresh" style="font-size:1.4em;"></i> \
                            </button> \
                            <button ng-class="{\'adv-srch-on\' : toggle}" ng-show="(enable_advsearch==\'true\')?true:false;" class="ubtn btn mobilesearchBtn" style="float: right; height: 34px;" ng-click="toggle = !toggle; advSearchOpened();" onclick="this.blur()" tooltip-placement="bottom" tooltip-trigger="focus" tooltip-animation="false" uib-tooltip="Search Panel" tabindex="0"> \
                                <i ng-class="{\'colorsrch\' : toggle}" class="fa fa-fw fa-search" style="font-size:1.4em"></i> \
                            </button> \
                            <span ng-transclude="headerButton" /> \
                        </div> \
                        \
                        <div id="mobilederectivemobile" class="btn-group" uib-dropdown dropdown-append-to-body> \
                        <button id="btn-append-to-body" style="background-color: transparent !important" class="ubtn btn mobileAddBtn dropdown-toggle" style="float: right;" type="button" uib-dropdown-toggle > \
                            <i class="glyphicon glyphicon-option-vertical" style="font-size:1em;"></i> \
                        </button> \
                        <ul class="dropdown-menu" role="menu" aria-labelledby="btn-append-to-body" style="-webkit-box-shadow:0 6px 12px rgba(0,0,0,.175); left:auto !important; right: 3px !important; border-radius: 2px; margin:0px 0 0 -97px; min-width:140px" uib-dropdown-menu> \
                                <li role="menuitem" ng-if="((buttonTambah==\'enable\')?true:false) && ((add_custombutton==\'disable\')?true:false)"><a href="#" style="padding: 3px 10px 3px 10px !important" ng-click="addNewItem();"><i class="fa fa-fw fa-plus-circle" style="margin-right: 5px"></i>Tambah</a></li> \
                                <li role="menuitem" ng-if="((buttonTambah==\'enable\')?true:false) && ((add_custombutton==\'enable\')?true:false)"><a href="#" style="padding: 3px 10px 3px 10px !important" ng-click="addNewItemCustom();"><i class="fa fa-fw fa-plus-circle" style="margin-right: 5px"></i>Tambah</a></li> \
                                <li class="divider" ng-if="(buttonTambah==\'enable\')?true:false" style="background-color:#ddd !important"></li> \
                                <li role="menuitem"><a style="padding: 3px 10px 3px 10px !important" href="#" ng-click="refresh()"><i class="fa fa-fw fa-refresh" style="margin-right: 5px"></i>Refresh</a></li> \
                                <li class="divider" ng-show="(enable_advsearch==\'true\')?true:false;" style="background-color:#ddd !important"></li> \
                                <li role="menuitem" ng-class="{\'adv-srch-on\' : toggle}" ng-show="(enable_advsearch==\'true\')?true:false;"><a  style="padding: 3px 10px 3px 10px !important" href="#" ng-click="toggle = !toggle; advSearchOpened();"><i class="fa fa-fw fa-search" style="margin-right: 5px"></i>Search</a></li> \
                                <div ng-transclude="headerButtonlist" /> \
                        </ul> \
                        </div> \
                    </div> \
                    \
                    <div ng-if="buttonGrouping != true" id="addMode" style="float:right;" ng-show="!isEditingMode"> \
                        <div class="btn-group "> \
                            <button id="addBtn" ng-if="((buttonTambah==\'enable\')?true:false) && ((add_custombutton==\'enable\')?true:false)" class="rbtn btn" tooltip-placement="bottom" tooltip-trigger="focus" tooltip-animation="true" uib-tooltip="Tambah" style="float: right; margin-right: 0px !important" ng-click="addNewItemCustom();" > \
                            <i class="fa fa-plus"></i> \
                            </button> \
                            <button id="addBtn" ng-if="((buttonTambah==\'enable\')?true:false) && ((add_custombutton==\'disable\')?true:false)" class="rbtn btn" tooltip-placement="bottom" tooltip-trigger="focus" tooltip-animation="true" uib-tooltip="Tambah" style="float: right; margin-right: 0px !important" ng-click="addNewItem();" > \
                                <i class="fa fa-plus"></i> \
                            </button> \
                            <button class="ubtn btn mobilesearchBtn" style="float: right; padding-bottom: 5px" ng-click="refresh()" onclick="this.blur();" tooltip-placement="bottom" tooltip-trigger="focus" tooltip-animation="true" uib-tooltip="Refresh"  tabindex="0"> \
                                <i class="fa fa-fw fa-refresh" style="font-size:1.4em;"></i> \
                            </button> \
                            <button ng-class="{\'adv-srch-on\' : toggle}" ng-show="(enable_advsearch==\'true\')?true:false;" class="ubtn btn mobilesearchBtn" style="float: right; height: 34px;" ng-click="toggle = !toggle; advSearchOpened()" onclick="this.blur()" tooltip-placement="bottom" tooltip-trigger="focus" tooltip-animation="false" uib-tooltip="Search Panel" tabindex="0"> \
                                <i ng-class="{\'colorsrch\' : toggle}" class="fa fa-fw fa-search" style="font-size:1.4em"></i> \
                            </button> \
                            <span ng-transclude="headerButton" /> \
                        </div> \
                    </div> \
                </div> \
                <div ng-if="buttonGrouping == true" id="mobilederectivemobile" class="row" style="height:30px;background:linear-gradient(transparent 0%, rgba(0,0,0,0.06));margin: -30px -8px 5px -8px;"> </div> \
                    \
                <div ng-show="advSearch" style="margin-bottom:5px" class="adv-srch" ng-show="!isEditingMode"> \
                        <div><div ng-transclude="formAdvSearch"/></div> \
                        <div class="row" style="margin:0px 0px 0 0"> \
                            <div class="" style="right:20px;"> \
                                <div class="pull-right"> \
                                    <div class="btn-group" style=""> \
                                        <button class="btn wbtn" style="padding:4px; margin-right:0px !important" ng-click="advancedSearch()" onclick="this.blur()" tooltip-placement="bottom" tooltip-trigger="mouseenter" tooltip-animation="false" uib-tooltip="Search" tabindex="0"> \
                                            <i class="large icons"><i class="database icon"></i><i class="inverted corner filter icon"></i> \
                                            </i>Search \
                                        </button>\
                                    </div>\
                                </div>\
                            </div> \
                        </div> \
                </div> \
                \
                <div ng-show="isEditingMode"> \
                    <form name="formPanel" novalidate> \
                            <div ng-transclude="formData" /> \
                    </form> \
                </div> \
                \
                <div ng-if="(add_custombutton==\'disable\')?true:false" class="row inputForm" style="margin: 0 0 0 0"> \
                    <div id="editMode" style="float:right;" ng-show="isEditingMode"> \
                        <button id="saveBtn" ng-show="!isViewData" ng-disabled="formPanel.$invalid" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="openModalKonfirmasi(1)">Simpan </button> \
                        <button id="cancelBtn" class="rbtn btn ng-binding ladda-button" style="float: right;" ng-click="cancelEditing();">Batal </button> \
                    </div> \
                </div> \
                \
                <div ng-if="disable_filter==false"; id="filterData" class="row" style="margin:0; margin-top:5px;" ng-show="!isEditingMode"> \
                    <div id="comboBoxFilter" style="float:left; width:12%;"> \
                        <div class="form-group filterMarg"> \
                            <select style="background:linear-gradient(transparent 40%, rgba(0,0,0,0.08)); border-radius:2px 0 0 2px !important; padding-left:3px !important; padding-right:0 !important" ng-model="pages.pageselected" required class="form-control" ng-options="item.page as item.page for item in filterPage" placeholder="Filter"> \
                            </select> \
                        </div> \
                    </div> \
                    <div id="comboBoxFilter" style="float:left; width:26%; font-size:12px !important;padding-right:0 !important"> \
                        <div class="form-group filterMarg"> \
                            <select style="background:linear-gradient(transparent 40%, rgba(0,0,0,0.08)); padding-left:3px !important; padding-right:0px !important " ng-change="tsts()" ng-model="modelfilter.selectedFilter" class="form-control" ng-options="item.value as item.name for item in filter_data.defaultFilter" placeholder="Filter"> \
                                <option style="color:#777" label="Filter" value="" disabled="true">Filter</option> \
                            </select> \
                        </div> \
                    </div> \
                    <div id="inputFilter" style="float:left; width:52%;"> \
                        <div class="form-group filterMarg" > \
                            <input type="text" required ng-model="modelfilter.textFilter" ng-change="clear(modelfilter.textFilter)" class="form-control" name="ModelKendaraan" placeholder="Search" ng-maxlength="100"> \
                        </div> \
                    </div> \
                    <div id="btnFilter" style="float:left; width:10%;"> \
                        <button style=" margin-right: 0px!important; ; border-radius:0 2px 2px 0 !important; borde" class="rbtn btn mobileFilterBtn" tooltip-placement="bottom" tooltip-trigger="focus" tooltip-animation="true" uib-tooltip="Filter" ng-click="filterSearch()"> \
                        <i class="fa fa-filter iconFilter"></i> \
                        </button>\
                    </div> \
                </div> \
                \
                    <div> \
                        <div ng-transclude="contentButtonlist" /> \
                    </div> \
                \
                <div id="ListData" ng-show="!isEditingMode"> \
                    <div ng-if="((toggle_switch==\'false\'||toggle_switch==false)?true:false) && ((button_tambah == true)?true:false);" style="margin-top:1px;" uib-dropdown dropdown-append-to-body> \
                        <button id="btn-append-to-body" class="btn btn-default" type="button" style="width: 90%; text-align:left; background-color:#888b91; color:white; margin-bottom: 5px" aria-haspopup="true" aria-expanded="false" uib-dropdown-toggle>\
                        <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp ' + listTitle + ' <span style="float:right"> ' + ((infoTotalData == "true") ? "( {{now}}/{{total}} )" : "") + ' <\span> \
                        </button> \
                        <span ng-transclude="tambah"> \
                        </span> \
                        \
                    </div> \
                    <div ng-if="((toggle_switch==\'false\'||toggle_switch==false)?true:false)  && ((button_tambah != true)?true:false);" style="margin-top:1px;" uib-dropdown dropdown-append-to-body> \
                        <button id="btn-append-to-body" class="btn btn-default" type="button" style="width: 100%; text-align:left; background-color:#888b91; color:white; margin-bottom: 5px" aria-haspopup="true" aria-expanded="false" uib-dropdown-toggle>\
                        <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp ' + listTitle + ' <span style="float:right"> ' + ((infoTotalData == "true") ? "( {{now}}/{{total}} )" : "") + ' <\span> \
                        </button> \
                        \
                    </div> \
                    <div ng-if="(toggle_switch==\'true\')?true:false;" style="margin-top:1px;" uib-dropdown dropdown-append-to-body > \
                        <button id="btn-append-to-body" ng-click="openList()"; refresh()" class="btn btn-default" type="button" style="width: 100%; text-align:left; background-color:#888b91; margin-bottom: 5px; color:white;" aria-haspopup="true" aria-expanded="false" uib-dropdown-toggle>\
                        <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp ' + listTitle + ' <span style="float:right"> ' + ((infoTotalData == "true") ? "( {{now}}/{{total}} )&nbsp&nbsp" : "") + ' <\span> \
                            <span id="Show_Hide_Prospect_Informasi_Kendaraan_Lain_PanelIcon" ng-show="(toggle_switch==\'true\')?true:false;" style="float:right; margin-right:3px;"><i ng-class="{\'fa fa-caret-up\' : isListShow, \'fa fa-caret-down\' : !isListShow }" aria-hidden="true"></i></span>\
                        </button> \
                    </div> \
                    <div id="PDS" class="formContent" ng-show="isListShow" style="margin-top: 0px !important"> \
                        <div class="listData ScrollStyle" style="padding-top: 0px !important;"> \
                            <div class="list-group" id="dropdownContent" aria-labelledby="dropdownMenu1"> \
                                <div align="center" ng-if="isEmpty == true" class="list-group-item" style="height:50px; padding:10px 0px 10px 0px; background-color:#e8eaed;"> \
                                        <b> Tidak ada data. </b> \
                                </div> \
                                <div ng-if="bind_data.data.length >= 1" ng-repeat="item in bind_data.data" class="list-group-item card card-2" style="padding: 10px 0px 10px 10px; margin: 0px 2px 10px 0px !important; background-color:white;"> \
                                ' + rowTemplate + ' </div> \
                                \
                                <div color="" align="center" ng-if="loading == true " class="list-group-item" style="border: none !important;padding:10px 0px 10px 0px; color:"> \
                                        <i class="fa fa-spinner fa-spin fa-2x"></i>    </br>\
                                        <b> Loading ... </b> \
                                </div> \
                                \
                                <div ng-if="bind_data.data.length < total" ng-hide="loading == true" ng-click="loadMore()" class="list-group-item" style="border: none !important"> \
                                        <div align="center" style="font-size: 20px"> \
                                                <i ng-class="{\'fa fa-spinner fa-spin\' : loadingMore, \'fa fa-angle-double-down\' : !loadingMore }" aria-hidden="true"></i> \
                                        </div> \
                                </div> \
                            </div> \
                        </div> \
                    </div> \
                    \
                    <div> \
                        <div class="row" style="margin: 0 0 0 0; margin-top:10px;"> \
                            <div id="customButton" style=="float:right;" > \
                                <div ng-transclude="footerButton"/> \
                            </div> \
                        </div> \
                    </div> \
                </div> ' + xModal + ' \
                ' + xModalKonfirmasi + ' \
            </div>';
            //endregion Template
            return xTemplate;

        },
        restrict: 'E',
        transclude: { 'tambah': '?bsButtonTambah', 'headerButton': '?bsHeaderbutton', 'headerButtonlist': '?bsHeaderbuttonlist', 'contentButtonlist': '?bsContentbutton', 'formData': '?bsformMobileformdata', 'formAdvSearch': '?bsformMobileadvancedsearch', 'footerButton': '?bsformMobilefooterbutton', 'modal': '?bsformMobilemodal' },
        replace: true,
        //require:'^form',

        //     <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="btn-append-to-body" style="-webkit-box-shadow:0 6px 12px rgba(0,0,0,.175); border-radius: 4px; margin:-5px 0 0 0"> \
        //     <li role="menuitem"><a href="#" ng-click="test != test">Urutkan Berdasarkan</a></li> \
        //         <div ng-show="test"> \
        //          tsest\
        //         </div> \
        //     <li role="menuitem"><a href="#">Under Construction 3</a></li> \
        //     <li class="divider"></li> \
        //     <li role="menuitem"><a href="#">Under Construction 4</a></li> \
        // </ul> \

        scope: {

            // Must Defined !!!!
            form_name: '@formName', // Nama Form (Must be unique)
            factory_name: '@factoryName', // Nama Factory (Must be Unique)
            selected_data: '=selectedData',
            updated_data: '=updatedData',
            filter_data: '=filterData',
            bind_data: '=bindData',
            Role: '=roleUser',

            // Enable / Disable Component
            enable_advsearch: '@enableAdvancedsearch', // true / false
            disable_filter: '@disableFilter',
            toggle_switch: '@enableListtoggle', // true / false
            button_tambah: '=?buttonTambah',
            buttonGrouping: '@?',

            modalLihat: '@customModallihat',
            modalhapus: '@customModalhapus',
            modalubah: '@customModalubah',
            buttonTambah: '@?enableAddbutton',
            // Used Services
            service_enable: '=serviceEnable', // true / false
            //loading: '=loading',
            total: '=?',
            now: '=?',
            isEmpty: '=?',
            // Custom Services
            add_custombutton: '@?addCustombutton',
            addCustomfunction:'&?',
			afterRefresh:'&?',
            get_custom: '@getCustom', // enable/disable
            get_customtotal: '=getCustomtotal',
            get_datafunction: '&getDatafunction', // Function Get Name

            save_custom: '@saveCustom', // enable/disable
            save_datafunction: '&saveDatafunction', // Function Save Name

            update_custom: '@updateCustom', // enable/disable
            update_datafunction: '&updateDatafunction', // Function Update Name

            delete_custom: '@deleteCustom', // enable/disable
            delete_datafunction: '&deleteDatafunction' // Function Delete Name
        },
        link: function postLink(scope, element, attrs, formCtl) {
            //console.log("bind filter", scope.filter_data);
            //#region Constructor

            //initialize Default
            scope.formState = "Add";
            scope.advSearch = false;
            scope.showModal = false;
            scope.isListShow = true;
            scope.isEditingMode = false;
            scope.AdvSearchStatus = false;
            scope.loadMoreStatus = 0;
            scope.okStatus = 0;
            //scope.selectedFilter = null;
            //scope.textFilter = null;
            scope.modelfilter = { selectedFilter: null, textFilter: null };
            var add = angular.copy(scope.add_custombutton);
            console.log("addcustom",add);
            //initialize Directive
            if (typeof scope.buttonGrouping == "undefined" || scope.buttonGrouping == null || scope.buttonGrouping == "") { scope.buttonGrouping = false; }
            if (typeof scope.add_custombutton == "undefined" || scope.add_custombutton == null || scope.add_custombutton == "") { scope.add_custombutton = "disable"; }            
            if (typeof scope.enable_advsearch == "undefined") { scope.enable_advsearch = false; }
            if (typeof scope.disable_filter == "undefined") { scope.disable_filter = false; }
            if (typeof scope.modalLihat == "undefined") { scope.modalLihat = false; }
            if (typeof scope.modalubah == "undefined") { scope.modalubah = false; }
            if (typeof scope.modalhapus == "undefined") { scope.modalhapus = false; }
            if (typeof scope.buttonTambah == "undefined") { scope.buttonTambah = "enable"; }
            if (typeof scope.service_enable == "undefined") { scope.service_enable = false; }
            if (typeof scope.get_custom == "undefined") { scope.get_custom = "disable"; }
            if (typeof scope.save_custom == "undefined") { scope.save_custom = "disable"; }
            if (typeof scope.update_custom == "undefined") { scope.update_custom = "disable"; }
            if (typeof scope.delete_custom == "undefined") { scope.delete_custom = "disable"; }
            if (typeof scope.toggle_switch == "undefined") { scope.toggle_switch = false; }
            if (typeof scope.button_tambah == "undefined") { scope.button_tambah = false; }
            //if(typeof scope.Role == "undefined") { scope.Role = ""; }
            //console.log("toogle",scope.toggle_switch);
            console.log("addcustom",scope.add_custombutton);
            //initialize Array Data
            scope.listData = [];
            scope.selected_data = {};
            scope.updated_data = [];
            scope.bind_data = {
                data: [],
                filter: []
            };


            //initialize pages
            scope.startGet = 1;
            scope.filterPage = [{ page: 10 }, { page: 25 }, { page: 50 }]; // Page Size List
            scope.pages = { pageselected: scope.filterPage[0].page };
            scope.limit = scope.pages.pageselected;
            scope.limitcopy = angular.copy(scope.pages.pageselected);
            var more = 0,
                startData = 0,
                pageSize = 10; // List per Page


            //initialize Parameter (String)
            scope.filterParamAdvTemp = "";
            scope.filterParamTemp = "";
            scope.filterParamSearchTemp = "";
            var filterParam = "/?start=" + scope.startGet + "&limit=" + scope.limit;
            var start = "/?start=" + scope.startGet,
                limit = "&limit=" + scope.pages.pageselected + "",
                filterParam = "" + start + "" + limit;


            //initialize Modal
            scope.modal_name = scope.form_name;


            //get Factory
            var factoryInstance = element.injector().get(scope.factory_name);
            scope.factory = factoryInstance;

            //First State
            initialize();
            //#endregion Constructor




            //#region Action

            scope.tsts = function() {
                console.log("testfilterselect", scope.modelfilter.selectedFilter);
            };

            scope.clear = function (data){
                if(data == null || data == "" || data == undefined){ scope.modelfilter.selectedFilter = null };
            }

            scope.addNewItemCustom = function(){
                scope.addCustomfunction();
            };
            //action btn "Refresh"
            scope.refresh = function() {
                scope.reset();
                initialize();
            };

            scope.advSearchOpened = function (){
                scope.advSearch = !scope.advSearch;
            }

            $rootScope.$on("RefreshDirective", function() {
                scope.refresh();
            });

            //action "Reset"
            scope.reset = function() {
                scope.formState = "Add";
                scope.startGet = 1;
                // scope.pages = { pageselected: scope.filterPage[0].page };
                // scope.pages = { pageselected : scope.pages.pageselected };
                scope.limit = angular.copy(scope.pages.pageselected);
                scope.filterParamAdvTemp = "";
                scope.modelfilter.selectedFilter = null;
                scope.modelfilter.textFilter = null;
                scope.bind_data.data = [];
                scope.bind_data.filter = [];
                scope.AdvSearchStatus = false;
                scope.isEmpty = false;
                scope.okStatus = 0;
                scope.loadMoreStatus = 0;
                scope.falseToggle();
                filterParam = "/?start=" + scope.startGet + "&limit=" + scope.limit;
            }

            //action btn "Tambah"
            scope.addNewItem = function() {
                scope.isEditingMode = true;
                scope.falseToggle();
                //  angular.forEach(scope.selected_data, function(value, key) {
                //         //console.log("test1", value, key);
                //         scope.selected_data[key] = "";
                //     });
            }

            //action btn "Batal"
            scope.cancelEditing = function() {
                scope.isEditingMode = false;
                scope.isViewData = false;

            }

            //action btn "Open Modal"
            scope.openModal = function(data) {
                //console.log("open modal data",data);
                //kondisi cek update atau gak
                scope.selected_data = angular.copy(data);
                //console.log("selected_data modal", scope.selected_data);
                scope.updated_data = data;
                //console.log("selected_data modal", scope.updated_data);
                angular.element('.ui.modal.' + scope.modal_name).modal('refresh').modal('show');
                //scope.modal_name=true;
            }

            scope.viewSelectedItem = function(data) {
                scope.isViewData = true;
                scope.isEditingMode = true;
                scope.falseToggle();
                angular.forEach(scope.selected_data, function(value, key) {
                    //console.log("test1", value, key);
                    scope.selected_data[key] = data[key];
                });
                console.log("isViewData", scope.isViewData);
                console.log("test", data);
                angular.element('.ui.modal.' + scope.modal_name).modal('hide');
                scope.modal_name = false;
            }

            scope.falseToggle = function() {
                scope.advSearch = false;
                scope.toggle = false;
            }

            scope.openList = function() {
                scope.isListShow = !scope.isListShow;
            }

            scope.cancelEditing = function() {
                scope.isEditingMode = false;
                scope.isViewData = false;
                angular.forEach(scope.selected_data, function(value, key) {
                    //console.log("test1", value, key);
                    scope.selected_data[key] = "";
                });
            }

            scope.updateSelectedItem = function(data) {
                scope.isEditingMode = true;
                scope.formState = "Update";
                angular.element('.ui.modal.' + scope.modal_name).modal('hide');
                scope.modal_name = false;
                scope.falseToggle();
            }

            function CloseEditing() {
                scope.isEditingMode = false;
                scope.isViewData = false;
                scope.isListShow = true;
                scope.formState = "Add";
            }

            scope.openModalKonfirmasi = function(status) {
                angular.element('.ui.modal.' + scope.modal_name + "konfirmasi").modal('refresh').modal('show');
                scope.statusModal = status;
                angular.element('.ui.modal.' + scope.modal_name).modal("hide");
                scope.modal_name = false;
            }

            scope.konfirmasiok = function(status) {
                //console.log("kesini ok");
                if (status == 1) {
                    scope.saveItem(scope.selected_data);
                } else if (status == 2) {

                } else if (status == 3) {
                    scope.deleteSelectedItem(scope.updated_data);
                }
                //scope.okStatus = 1;
                angular.element('.ui.modal.' + scope.modal_name + "konfirmasi").modal("hide");
                //return scope.okStatus;
            }

            scope.konfirmasicancel = function() {
                    //console.log("kesini cancel");
                    scope.okStatus = 0;
                    angular.element('.ui.modal.' + scope.modal_name + "konfirmasi").modal("hide");
                    return scope.okStatus;
                }
                //#endregion Action





            //#region Functional

            //GET
            function initialize() {
                console.log("kesini");
                if (scope.service_enable == false) { // not using service;
                    startData = 0;
                    var dataFactory = scope.factory.getData();
                    scope.listData = dataFactory.Result;
                    scope.total = scope.listData.length;

                    for (var i = startData; i < pageSize; i++) {
                        scope.bind_data.data.push(scope.listData[i]);
                        startData++;
                    }

                    more = scope.bind_data.data.length + pageSize;

                } else {
                    scope.loading = true;
                    if (scope.get_custom == "enable") {
                        if (scope.get_datafunction) {
                            scope.get_datafunction()();
                        } else {
                            console.log("Error get_datafunction");
                        }
                        scope.loading = false;
                        more = scope.bind_data.data.length + pageSize;
                        scope.total = scope.get_customtotal;
                        scope.now = scope.bind_data.data.length;
                        if (scope.now == 0) {
                            scope.isEmpty = true;
                        } else {
                            scope.isEmpty = false;
                        }
                        // return res.data;
                       
                    } else {
                        // console.log("filterparam", filterParam);
                        scope.loading = true;
                        scope.factory.getData(filterParam)
                            .then(
                                function(res) {
                                    //   console.log("result", res.data.Result);
                                    scope.loading = false;
                                    scope.bind_data.data = res.data.Result;
                                    if(scope.factory_name == 'KonfirmasiWaktuDanAlasanPengirimanFactory'){
                                        scope.total = scope.pages.pageselected;
                                    }else{
                                        scope.total = res.data.Total;
                                    }
                                    
                                    // setTimeout(function(){
                                    //     scope.loading = false;
                                    // },0);
                                    scope.now = scope.bind_data.data.length;
                                    if (scope.now == 0) {
                                        scope.isEmpty = true;
                                    } else {
                                        scope.isEmpty = false;
                                    }
									
									if(scope.afterRefresh!=undefined)
									{
										scope.afterRefresh();
									}
									
                                    return res.data;
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                    }
                }
            }

            //POST - PUT
            scope.saveItem = function(data) {
                scope.startGet = 1;
                //console.log("Service Enable",scope.service_enable);
                if (scope.formState == "Add") {
                    //POST 
                    if (scope.service_enable == false) {
                        scope.listData.push(data);
                        scope.total = scope.listData.length;
                        CloseEditing();
                    } else {
                        if (scope.save_custom == "enable") {
                            if (scope.save_datafunction) {
                                scope.save_datafunction(function() {
                                    initialize();
                                })(data);
                                CloseEditing();
                            } else {
                                console.log("Error save_datafunction");
                            }
                        } else {
                            //  console.log("post data", data);
                            scope.factory.create(data)
                                .then(
                                    function(res) {
                                        initialize();
                                        CloseEditing();
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                        }
                    }
                } else {
                    //PUT
                    if (scope.service_enable == false) {
                        angular.forEach(scope.updated_data, function(value, key) {
                            //console.log("test1", value, key);
                            scope.updated_data[key] = data[key];
                        });
                        CloseEditing();
                    } else {
                        if (scope.update_custom == "enable") {
                            if (scope.update_datafunction) {
                                scope.update_datafunction(function() {
                                    initialize();
                                })(data);
                                CloseEditing();

                            } else {
                                console.log("Error save_datafunction");
                            }
                        } else {
                            //  console.log("put data", data);
                            scope.factory.update(data)
                                .then(
                                    function(res) {
                                        initialize();
                                        CloseEditing();
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                        }
                    }
                }

            }

            //DELETE
            scope.deleteSelectedItem = function(data) {
                // angular.element('#'+scope.modal_name).modal("hide");

                // openModalKonfirmasi();
                //console.log("status sekarang", scope.okStatus);

                //if (scope.okStatus == 1) {
                if (scope.service_enable == false) {
                    var index = scope.bind_data.data.indexOf(data);
                    scope.bind_data.data.splice(index, 1);
                    //scope.Refresh();
                    angular.element('.ui.modal.' + scope.modal_name).modal("hide");
                } else {
                    if (scope.delete_custom == "enable") {
                        if (scope.delete_datafunction) {
                            scope.delete_datafunction()(data);
                            initialize();
                            angular.element('.ui.modal.' + scope.modal_name).modal("hide");
                        } else {
                            console.log("Error delete_datafunction");
                        }
                    } else {
                        //  console.log("delete data", data);
                        scope.factory.delete(data)
                            .then(
                                function(res) {
                                    scope.total = scope.total - 1;
                                    initialize();
                                    angular.element('.ui.modal.' + scope.modal_name).modal("hide");
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                    }
                }
            }


            scope.advancedSearch = function() {
                scope.loading = true;
                scope.bind_data.data = [];
                scope.filterParamAdvTemp = "";
                scope.modelfilter.selectedFilter = "";
                scope.modelfilter.textFilter = "";
                scope.startGet = 1;
                scope.AdvSearchStatus = true;
                scope.loadMoreStatus = 1;
                for (var i = 0; i < scope.filter_data.advancedFilter.length; i++) {

                    var temp = scope.filter_data.advancedFilter[i];
                    var tempdata = scope.bind_data.filter[temp.value];

                    if (temp.typeMandatory == 1) {
                        if (tempdata == null || tempdata == "") {
                            // ada error message;
                        } else {
                            scope.filterParamAdvTemp += "&" + temp.value + "=" + tempdata;
                        }
                    } else {
                        if (tempdata == null || tempdata == "") {} else {
                            scope.filterParamAdvTemp += "&" + temp.value + "=" + tempdata;
                        }
                    }
                }

                //console.log("bind_data filter", scope.bind_data);
                var filterParamAdv = filterParam + scope.filterParamAdvTemp;
                // console.log("param advance serach", filterParamAdv);
                //Service Process;
                scope.factory.getData(filterParamAdv)
                    .then(
                        function(res) {
                            scope.loading = false;
                            // console.log("result", res.data.Result);
                            scope.bind_data.data = res.data.Result;
                            scope.total = res.data.Total;
							scope.now = scope.bind_data.data.length;
                            // setTimeout(function(){
                            //     scope.loading = false;
                            // },0);
                            if (scope.total <= 0) {
                                scope.isEmpty = true;
                            } else {
                                scope.isEmpty = false;
                            }
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
            }


            scope.filterSearch = function() {
                //  console.log("1", scope.modelfilter.selectedFilter);
                //  console.log("2", scope.modelfilter.textFilter);

                if ((scope.modelfilter.selectedFilter == null || scope.modelfilter.selectedFilter == undefined || scope.modelfilter.selectedFilter == '') ||
                    (scope.modelfilter.textFilter == null || scope.modelfilter.textFilter == undefined || scope.modelfilter.textFilter == '')) {
                    scope.loading = false;
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Harap pilih kategori dan masukkan filter.",
                        type: 'warning',
                        timeout: 2000,
                    });
                } else {
                    scope.loading = true;
                    scope.startGet = 1;
                    scope.limit = scope.pages.pageselected;
                    scope.loadMoreStatus = 2;
                    scope.bind_data.data = [];
                    var limit = "&limit=" + scope.limit + "";
                    var filter = "";

                    if (scope.AdvSearchStatus == true) {
                        filter = "/?start=" + scope.startGet + "" + limit + "" + scope.filterParamAdvTemp + "&filterData=" + scope.modelfilter.selectedFilter + "|" + scope.modelfilter.textFilter;
                    } else if (scope.AdvSearchStatus == false) {
                        scope.filterParamSearchTemp = "";
                        for (var i = 0; i < scope.filter_data.defaultFilter.length; i++) {

                            var temp = scope.filter_data.advancedFilter[i];
                            var tempdata = scope.bind_data.filter[temp.value];

                            if (temp.typeMandatory == 1) {
                                if (typeof tempdata == 'undefined' || tempdata == null || tempdata == "") {
                                    tempdata = " ";
                                } else {
                                    tempdata = tempdata;
                                }
                                scope.filterParamSearchTemp += "&" + temp.value + "=" + tempdata;

                            } else {
                                if (typeof tempdata == 'undefined' || tempdata == null || tempdata == "") {
                                    tempdata = " ";
                                } else {
                                    tempdata = tempdata;
                                }
                                scope.filterParamSearchTemp += "&" + temp.value + "=" + tempdata;

                            }
                        }
                        if (scope.enable_advsearch == true) {
                            //   console.log("Param serach", scope.filterParamSearchTemp);
                            filter = "/?start=" + scope.startGet + "" + limit + "" + scope.filterParamSearchTemp + "&filterData=" + scope.modelfilter.selectedFilter + "|" + scope.modelfilter.textFilter;
                        } else {
                            filter = "/?start=" + scope.startGet + "" + limit + "&filterData=" + scope.modelfilter.selectedFilter + "|" + scope.modelfilter.textFilter;
                        }
                    }

                    //   console.log("Param search: ", filter);
                    scope.factory.getData(filter)
                        .then(
                            function(res) {
                               scope.loading = false;
                                //   console.log("result", res.data.Result);
                                scope.bind_data.data = res.data.Result;
                                scope.total = res.data.Total;
                                // setTimeout(function(){
                                //     scope.loading = false;
                                // },0);
                                if (scope.total <= 0) {
                                    scope.isEmpty = true;
                                    scope.now = 0;
                                } else {
                                    scope.isEmpty = false;
                                }
								
								//tadi ini function ga ada ditambah 23 mei jem 11 lewat 38
								if(scope.afterRefresh!=undefined)
								{
									scope.afterRefresh();
								}
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                }
            }

            scope.loadMore = function() {
                scope.loadingMore = true;
                if (scope.service_enable == false) {
                    if (more > scope.total) {
                        more = scope.total;
                    } else {
                        more = more;
                    }
                    for (var i = startData; i < more; i++) {
                        scope.bind_data.data.push(scope.listData[i]);
                        startData++;
                    }
                    more = scope.bind_data.data.length + pageSize;

                } else {
                    if (scope.bind_data.data.length < scope.total) {
                        // scope.startGet += scope.pages.pageselected;
                        scope.startGet = scope.now + 1;
                        var startNow = scope.startGet;
                        var start = "/?start=" + startNow + "";
                        var limit = "&limit=" + scope.pages.pageselected + "";
                        var paramtemp = "" + start + "" + limit;
                        var param = "";

                        if (scope.loadMoreStatus == 1) {
                            param = "" + start + "" + limit + "" + scope.filterParamAdvTemp;
                        } else if (scope.loadMoreStatus == 2) {
                            param = "" + start + "" + limit + "" + scope.filterParamSearchTemp + "&filterData=" + scope.modelfilter.selectedFilter + "|" + scope.modelfilter.textFilter;
                        } else {
                            param = paramtemp;
                        }

                        //   console.log("loadMore", param);
                        scope.factory.getData(param)
                            .then(
                                function(res) {
                                    scope.loadingMore = false;
                                    var tempData = res.data.Result;
                                    for (var i = 0; i < tempData.length; i++) {
                                        scope.bind_data.data.push(tempData[i]);
                                    }
                                    
                                    scope.total = res.data.Total;
                                    scope.now = scope.bind_data.data.length;
                                    if (scope.total <= 0) {
                                        scope.isEmpty = true;
                                    } else {
                                        scope.isEmpty = false;
                                    }
                                    // setTimeout(function(){
                                    //     scope.loadingMore = false;
                                    // },0);
                                    // param = "";
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                        //param = "";

                    } else {
                        console.log("All Data Loaded");
                    }


                }
            }

            //#endregion Functional

        }
    };
});