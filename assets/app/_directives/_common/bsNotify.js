angular.module('bsNotify',[])
.factory('bsNotify',function(){
	return{
		show: function(msg){
					var xmsg={};
					if(msg.timeout) {xmsg.timeout = msg.timeout};
					if(msg.number) {xmsg.number = msg.number};
					if(!msg.size) {xmsg.size = 'small'} else {xmsg.size = msg.size};
					xmsg.title = msg.title;
					xmsg.content = msg.content;
					if(msg.color) {xmsg.color = msg.color} else {xmsg.color = "#296191"};
					if(!msg.type) {xmsg.type='info'} else {xmsg.type= msg.type};
					//----------
					if(xmsg.type=='info'){
						xmsg.color="#296191";
						msg.icon="fa fa-bell swing animated";
					}else if(xmsg.type=='success'){
						xmsg.color="#739E73";
						msg.icon="fa fa-thumbs-up bounce animated";
					}
					else if(xmsg.type=='warning'){
						xmsg.color="#C79121";
						msg.icon="fa fa-exclamation-circle swing animated";
					}
					else if(xmsg.type=='danger'){
						xmsg.color="#C46A69";
						msg.icon="fa fa-warning shake animated";
					};

					if(xmsg.size=='small'){
							if(!msg.timeout) {xmsg.timeout = 4000};
							xmsg.iconSmall = msg.icon;
		     				$.smallBox(xmsg);
		     		}else{
		     				xmsg.icon = msg.icon;
							$.bigBox(xmsg);
		    		}
				}
	}
});
