angular.module('ASPricingEngine',[])
.factory('ASPricingEngine',function($http){
	

	return{
		getPPN: function(isNPWP,PPNType){
			var res = $http.get('/api/as/PPNMaster?isNPWP=' + isNPWP + '&PPNType=' + PPNType);
			console.log('res getPPN PricingEngine >', res);
			return res;
		},
		calculate: function(data){
			var pmInputPrice = 0	//harga semua yg include PPN
			var pmDiscount = 0
			var pmQty = 0
			var pnTipe = 0
			var pnPPNType = 5 //default untuk aftersales 3
			var pnIsNPWP = 1  //0=NonNPWP, 1=NPWP
			var PPNPercentage = 10

			/*
			pnTipe:
			1 	 Harga Retail Sebelum Diskon Include PPN				
			2 	 Harga Retail Sebelum Diskon Exclude PPN				
			3 	 Harga Retail Setelah Diskon Include PPN				
			4 	 Harga Retail Setelah Diskon Exclude PPN				
			11 	 Diskon Satuan Include PPN				
			12 	 Diskon Satuan Exclude PPN				
			23 	 Harga SubTotal Include PPN				
			24 	 Harga SubTotal Exclude PPN				
			25 	 Harga GrandTotal Include PPN				
			26 	 Harga GrandTotal Exclude PPN				
			31 	 PPN Satuan Sebelum Diskon				
			32 	 PPN Satuan Setelah Diskon				
			33 	 PPN GrandTotal	
			101 	 Harga Retail Sebelum Diskon Include PPN
			102 	 Harga Retail Sebelum Diskon Exclude PPN
			103 	 Harga Retail Setelah Diskon Include PPN
			104 	 Harga Retail Setelah Diskon Exclude PPN
			111 	 Diskon Include PPN
			112 	 Diskon Exclude PPN
			121 	 Harga Total Include PPN
			122 	 Harga Total Exclude PPN
			123 	 Harga SubTotal Include PPN
			124 	 Harga SubTotal Exclude PPN
			131 	 PPN Sebelum Diskon
			132 	 PPN Setelah Diskon		
			
			pnPPNType:
				1	PPN - Unit
				2	PPN - Aksesoris
				3	PPN - Service
				4	PPN - Parts
				5	PPN - General
			*/

			pmInputPrice = data.InputPrice
			pmDiscount = data.Discount
			pmQty = data.Qty
			pnTipe = data.Tipe
			pnPPNType = data.PPNType
			pnIsNPWP = data.IsNPWP
			PPNPercentage = data.PPNPercentage

			if (PPNPercentage === null || PPNPercentage === undefined){
				console.log('nah loh kurang param PPNPrecentage')
				PPNPercentage = 10; // default in aja biar aman
			}

			var mResult = 0
			var dPPNPercentage = 0
			var dPPN = 0
			var mHargaRetailSebelumDiskonIncludePPN = 0
			var mHargaRetailSebelumDiskonExcludePPN = 0
			var mHargaRetailSetelahDiskonIncludePPN = 0
			var mHargaRetailSetelahDiskonExcludePPN = 0
			var mDiskonSatuanIncludePPN	= 0
			var mDiskonSatuanExcludePPN	= 0
			var mHargaSubTotalIncludePPN = 0
			var mHargaSubTotalExcludePPN = 0
			var mHargaGrandTotalIncludePPN = 0
			var mHargaGrandTotalExcludePPN = 0
			var mPPNSatuanSebelumDiskon = 0
			var mPPNSatuanSetelahDiskon = 0
			var mPPNGrandTotal = 0
			var mHargaRetailSebelumDiskonIncludePPNQty = 0
			var mHargaRetailSebelumDiskonExcludePPNQty = 0
			var mHargaRetailSetelahDiskonIncludePPNQty = 0
			var mHargaRetailSetelahDiskonExcludePPNQty = 0
			var mDiskonIncludePPN = 0
			var mDiskonExcludePPN = 0
			var mHargaTotalIncludePPNQty = 0
			var mHargaTotalExcludePPNQty = 0
			var mHargaSubTotalIncludePPNQty = 0
			var mHargaSubTotalExcludePPNQty = 0
			var mPPNSebelumDiskonQty = 0
			var mPPNSetelahDiskonQty = 0

			dPPNPercentage = PPNPercentage 

			dPPN = 1 + (dPPNPercentage / 100)

			if (pnTipe == 23 || pnTipe == 24 || pnTipe == 123 || pnTipe == 124){

				// ======== subtotal =======================
				
				mHargaSubTotalExcludePPN = Math.round(((pmInputPrice * 1000000) / (dPPN * 1000000)))
				if (pnTipe == 24){
					// console.log('mHargaSubTotalExcludePPN', mHargaSubTotalExcludePPN)
					return mHargaSubTotalExcludePPN
				}

				mHargaSubTotalIncludePPN = mHargaSubTotalExcludePPN + (mHargaSubTotalExcludePPN * dPPNPercentage/100)
				if (pnTipe == 23){
					return mHargaSubTotalIncludePPN
				}

				mHargaSubTotalExcludePPNQty = Math.round(mHargaSubTotalExcludePPN * pmQty)
				if (pnTipe == 124){
					return mHargaSubTotalExcludePPNQty
				}

				mHargaSubTotalIncludePPNQty = Math.round(mHargaSubTotalIncludePPN * pmQty)
				if (pnTipe == 123){
					return mHargaSubTotalIncludePPNQty
				}

			} else if (pnTipe == 25 || pnTipe == 26 || pnTipe == 33) {

				// ======== grandtotal =======================

				mHargaGrandTotalExcludePPN = Math.round(((pmInputPrice * 1000000) / (dPPN * 1000000)))
				if (pnTipe == 26){
					return mHargaGrandTotalExcludePPN
				}

				mPPNGrandTotal = Math.floor(mHargaGrandTotalExcludePPN * (dPPNPercentage / 100))
				if (pnTipe == 33){
					return mPPNGrandTotal
				}

				mHargaGrandTotalIncludePPN = mHargaGrandTotalExcludePPN + mPPNGrandTotal
				if (pnTipe == 25){
					return mHargaGrandTotalIncludePPN
				}

			} else {


				mHargaRetailSebelumDiskonExcludePPN = Math.round(((pmInputPrice * 1000000) / (dPPN * 1000000)))
				if (pnTipe == 2){
					return mHargaRetailSebelumDiskonExcludePPN
				}

				mHargaRetailSebelumDiskonExcludePPNQty = mHargaRetailSebelumDiskonExcludePPN * pmQty
				if (pnTipe == 102){
					return mHargaRetailSebelumDiskonExcludePPNQty
				}

				mPPNSatuanSebelumDiskon = mHargaRetailSebelumDiskonExcludePPN * (dPPNPercentage / 100)
				if (pnTipe == 31){
					return mPPNSatuanSebelumDiskon
				}

				mPPNSebelumDiskonQty = mPPNSatuanSebelumDiskon * pmQty
				if (pnTipe == 131){
					return mPPNSebelumDiskonQty
				}

				mHargaRetailSebelumDiskonIncludePPN = mHargaRetailSebelumDiskonExcludePPN + mPPNSatuanSebelumDiskon
				if (pnTipe == 1){
					return mHargaRetailSebelumDiskonIncludePPN
				}

				mHargaRetailSebelumDiskonIncludePPNQty = mHargaRetailSebelumDiskonIncludePPN * pmQty
				if (pnTipe == 101){
					return mHargaRetailSebelumDiskonIncludePPNQty
				}

				mDiskonSatuanExcludePPN = Math.round(mHargaRetailSebelumDiskonExcludePPN * pmDiscount / 100)
				if (pnTipe == 12){
					return mDiskonSatuanExcludePPN
				}

				mDiskonExcludePPN = mDiskonSatuanExcludePPN * pmQty
				if (pnTipe == 112){
					return mDiskonExcludePPN
				}

				mDiskonSatuanIncludePPN = Math.round(mHargaRetailSebelumDiskonExcludePPN * pmDiscount / 100) + (Math.round(mHargaRetailSebelumDiskonExcludePPN * pmDiscount / 100) * (dPPNPercentage / 100))
				if (pnTipe == 11){
					return mDiskonSatuanIncludePPN
				}

				mDiskonIncludePPN = mDiskonSatuanIncludePPN * pmQty
				if (pnTipe == 111){
					return mDiskonIncludePPN
				}

				mHargaRetailSetelahDiskonExcludePPN = mHargaRetailSebelumDiskonExcludePPN - mDiskonSatuanExcludePPN
				if (pnTipe == 4){
					return mHargaRetailSetelahDiskonExcludePPN
				}

				mHargaRetailSetelahDiskonExcludePPNQty = mHargaRetailSetelahDiskonExcludePPN * pmQty
				if (pnTipe == 104){
					return mHargaRetailSetelahDiskonExcludePPNQty
				}

				mPPNSatuanSetelahDiskon = mHargaRetailSetelahDiskonExcludePPN * (dPPNPercentage / 100)
				if (pnTipe == 32){
					return mPPNSatuanSetelahDiskon
				}

				mPPNSetelahDiskonQty = mPPNSatuanSetelahDiskon * pmQty
				if (pnTipe == 132) {
					return mPPNSetelahDiskonQty
				}

				mHargaRetailSetelahDiskonIncludePPN = mHargaRetailSetelahDiskonExcludePPN + mPPNSatuanSetelahDiskon
				if (pnTipe == 3){
					return mHargaRetailSetelahDiskonIncludePPN
				}

				mHargaRetailSetelahDiskonIncludePPNQty = mHargaRetailSetelahDiskonIncludePPN * pmQty
				if (pnTipe == 103){
					return mHargaRetailSetelahDiskonIncludePPNQty
				}

				mHargaTotalIncludePPNQty = mHargaRetailSetelahDiskonIncludePPN * pmQty
				if (pnTipe == 121){
					return mHargaTotalIncludePPNQty
				}

				mHargaTotalExcludePPNQty = mHargaRetailSetelahDiskonExcludePPN * pmQty
				if (pnTipe == 122){
					return mHargaTotalExcludePPNQty
				}

			}

		},

		

		// calculatex: function(data){

		// 	// 1. total harga tanpa PPN
		// 	// 2. total PPN nya (kalkulasi dari jumlah total harga tanpa PPN di kali PPN)
		// 	// 3. total harga diskon (besar total diskon nya saja, dalam Rp)

		// 	if (data.harga === undefined || data.harga === null){
		// 		data.harga = 0
		// 	}
		// 	if (data.qty === undefined || data.qty === null){
		// 		data.qty = 1
		// 	}

		// 	if (data.param === 1){
		// 		var res = 0;
		// 		res = Math.round(data.harga / 1.1) * data.qty

		// 		return res;

		// 	} else if (data.param === 2){
		// 		var res = 0;
		// 		res = Math.floor(data.harga * 0.1) * data.qty

		// 		return res;
				
		// 	} else if (data.param === 3){
		// 		var res = 0;
		// 		res = Math.round( Math.round(data.harga / 1.1) * (data.discount/100) ) * data.qty

		// 		return res;
		// 	}
		
				
		// },
	}
});
