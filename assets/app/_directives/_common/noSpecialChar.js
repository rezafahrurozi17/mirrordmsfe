angular.module('noSpecialChar',[]).directive('noSpecialChar', function() {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function (scope, element, attrs, modelCtrl) {
			modelCtrl.$parsers.push(function (inputValue) {
				if (inputValue == null)
					return ''
				cleanInputValue = inputValue.replace(/([^a-zA-Z0-9,.])*/gi, '');
				if (cleanInputValue != inputValue) {
					modelCtrl.$setViewValue(cleanInputValue);
					modelCtrl.$render();
				}
				return cleanInputValue;
			});
		}
	}
});