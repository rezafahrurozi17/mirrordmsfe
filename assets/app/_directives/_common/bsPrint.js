angular.module('bsPrint', []).directive('bsPrint', function($parse, $timeout, $templateCache, PrintRpt, bsNotify, $http) { //bsPrint -> bs-print

return {
    template: function(element, attrs) { //attrs dipakai jika static atau untuk manipulasi template sebelum di-compile

    //#region Template
        var xTemplate =
        '<div> \
            <div ng-transclude="buttonPrint" ng-click="PrintFunction();"/> \
        </div>';

    //#endregion Template

    return xTemplate;

    },
    restrict: 'E',
    transclude: { 'buttonPrint': '?bsbuttonPrint'},
    replace: true,
    
    scope: {
        cetakan_url: '@cetakanUrl'
    },
    link: function postLink(scope, element, attrs, formCtl) {
       
    //#region Constructor
   
    //#endregion Contructor

    //#region Action
    scope.PrintFunction = function(){
        var pdfFile = null;

        PrintRpt.print(scope.cetakan_url).success(function (res) {
            var file = new Blob([res], {type: 'application/pdf'});
            var fileURL = URL.createObjectURL(file);
            
            console.log("pdf", fileURL);
            //$scope.content = $sce.trustAsResourceUrl(fileURL);
            pdfFile = fileURL;

            if(pdfFile != null)
                printJS(pdfFile);
            else
                console.log("error cetakan", pdfFile);  
        })
        .error(function (res) {
            console.log("error cetakan", pdfFile);  
        });
    }
    //#endregion Action





        }
    };
});