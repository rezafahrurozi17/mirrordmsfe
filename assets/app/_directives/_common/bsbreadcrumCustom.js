angular.module('bsbreadcrumbCustom', []).directive('bsbreadcrumbCustom', function ($parse, MENU_TREE, $timeout, $state, $stateParams) {
  //
  return {
    template: function (element, attrs) {
      //var actionLabels = attrs.actionLabel
      var xTemplate = '<div class="ui breadcrumb" style="font-size:13px;margin-top:-54px;padding-left:10px;background-color:transparent;">' +
        '<span class="bsbreadcrumb-path" ng-repeat="b in breadcrumb">' +
        '<a style="opacity:0;" class="section">{{b}}</a>' +
        '<i class="fa fa-angle-right divider"' +
        ' style="font-size: 1.2em;color: #777;opacity: 0;padding: 3px;">' +
        '</i>' +
        '</span>' +
        '<span><div style="opacity: 0" class="active section">{{formTitle}}</div></span>' +
        '<span ng-if="actionLink">' +
        '&nbsp;&nbsp;<i class="fa fa-angle-right divider"' +
        ' style="font-size: 1.2em;color: #777;opacity: 0;padding: 3px;">' +
        '</i>' +
        '<div style="opacity: 0" class="active section">{{actionLink}}</div>' +
        '</span>' +
        '<span>' +
        '&nbsp;&nbsp;<i class="fa fa-angle-right divider"' +
        'style="font-size: 1.2em;color: #777;padding: 3px;">' +
        '</i>' +
        '<div class="active section">{{actionLabel}}</div>' +
        '</span>' +
        '</div>';
      return xTemplate;
    },
    restrict: 'EA',
    transclude: true,
    replace: true,
    // require:'^form',
    // controllerAs: 'vm',
    //indToController: true,
    scope: {
      actionLabel: '=?',
      menuTree: '=?',
      actionType: '=?',
      actionLink: '=?',
      tab: '=?'
    },
    link: function postLink(scope, element, attrs, formCtl) {
      const vm = this;
      console.log("vm=", vm);

      setTimeout(function () {
        scope.actionLabels = scope.actionLabel;
        console.log("()&(&(", scope.actionLabel);
      }, 200);

      //console.log(MENU_TREE.ITEMS);
      var menutree = MENU_TREE.ITEMS;
      
      var tabsCust = null;

      tabsCust = angular.copy(scope.tab);

      if (!vm.tab) {
        vm.tab = angular.copy(scope.tab);
      }

      console.log("tab=", tabsCust);
      scope.formTitle = tabsCust.title;
      // console.log("vm.formTitle=>",$scope.formTitle);
      scope.icon = vm.tab.Icon;
      // console.log("icon=>",$scope.icon);
      var tab = null;
      if (tabsCust.item) {
        // console.log("vm.tab=",vm.tab);
        tab = JSON.parse(tabsCust.item);
      }
      //console.log ("tab=>",tab);
      var p = tab;
      scope.breadcrumb = [];
      //scope.breadcrumb.push(tab.title);
      if (p) {
        while (p.Parent != null) {
          scope.breadcrumb.splice(0, 0, p.Parent.Name);
          // console.log("menutree=>",menutree," id=>",p.parent.id);
          var x = deepFind(menutree, p.Parent.Id);
          this.actionLabels = scope.actionLabel;
          //console.log("x=>",x);
          if (x != null) { p = x } else { break };
        }
      }
      // console.log("breadcrumb=>",vm.breadcrumb);

      this.setActionType = function (actType) {
        // console.log("actType=>",actType);
        scope.actionType = actType;
      }
      this.getActiveMenu = function () {
        // console.log("actType=>",actType);
        //$scope.actionType=actType;
        return vm.tab.item;
      }
      this.getActiveTab = function () {
        // console.log("actType=>",actType);
        //$scope.actionType=actType;
        return vm.tab;
      }
      this.setActionLink = function (actLink) {
        // console.log("actType=>",actType);
        scope.actionLink = actLink;
      }
      //
      function deepFind(node, id) {
        var res = null;
        for (var i = 0; i < node.length; i++) {
          if (node[i].Id == id) {
            res = node[i];
            return res;
            break;
          } else {
            if (node[i].Child.length > 0 && node[i].Child != null && res == null) {
              res = deepFind(node[i].Child, id);
              if (res) break;
            }
          }
        }
        return res;
      }
    }
  };
});
//Backup
                // '<div'+
                // '<div class="bsbreadcrumb">'+
                // '<i class="{{vm.icon}} fa-2x" style="color:#4183C4;margin-top:0px;"></i>'+
                // '<div class="ui breadcrumb" style="font-size:13px;margin-top:-5px;padding-left:5px;background-color:transparent;">'+
                //   '<span class="bsbreadcrumb-path" ng-repeat="b in breadcrumb">'+
                //     '<a class="section">{{b}}</a>'+
                //     '<i class="fa fa-angle-right divider"'+
                //       ' style="font-size: 1.2em;color: #777;opacity: 1.0;padding: 3px;">'+
                //     '</i>'+
                //   '</span>'+
                //   '<span><div class="active section">{{formTitle}}</div></span>'+
                //   '<span ng-if="vm.actionType">'+
                //     '<i class="fa fa-angle-right divider"'+
                //       ' style="font-size: 1.2em;color: #777;opacity: 1.0;padding: 3px;">'+
                //     '</i>'+
                //     '<div class="active section">{{vm.actionType}}</div>'+
                //   '</span>'+
                // '</div>'+
                // '</div>'+
                // '<ng-transclude>'+
                // '</div>',