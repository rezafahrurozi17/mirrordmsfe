angular.module('onKeydown',[])
  .directive('onKeydown', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {

                scope.$apply(function (){
                    scope.$eval(attrs.onKeydown);
                });
                event.preventDefault();

        });
    }
  });
