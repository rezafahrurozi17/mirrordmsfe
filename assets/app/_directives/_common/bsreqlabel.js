angular.module('bsreqlabel',[])
.directive('bsreqlabel', function ($timeout) {
    return {
      restrict: 'EA',
      transclude: true,
      replace:true,
      template:'<label class="control-label">'+
                '<ng-transclude></ng-transclude>'+
                '</label>',
      scope:true,
      link: function postLink(scope, element,attrs,ctrl,transclude) {
              transclude(scope,function(clone) {
                var html=element.find('ng-transclude');
                var label=html[0].innerText;
                html[0].innerHTML = label + '<span style="color:#b94a48;"> *</span>';
              });
      }
    };
  });