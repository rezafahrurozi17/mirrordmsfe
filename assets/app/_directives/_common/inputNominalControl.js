angular.module('inputNominalControl', ['ui.bootstrap']).directive('inputnominalControl', function( $parse, $timeout) {
    return {
        template: function(element, attrs) { 
            var itemName = attrs.name;
            var itemId = attrs.id;
			var itemHolder = attrs.holder;
            
			var xTemplate = 
			'<div><input type="text" class="form-control" id="'+itemId+'" name="'+itemName+'" placeholder="'+itemHolder+'" ng-model="ngModels" ng-maxlength="50" maxlength="50"></input></div>';

            return xTemplate;
        },
        restrict: 'E',
        replace: true,
        scope: {
            output: '=?',
            name: '@?',
            ngModels:'=?',
            id:'@?',
			holder:'@?',
        },
        link: function postLink(scope, element, attrs, formCtl) {
			
			scope.output = 0;
			
            console.log("id sekarang", scope.ngModels);
            //console.log('id dan name', scope.id, scope.name);

            element.on('change',function (){
                scope.output = angular.copy(scope.ngModels).replace('.','');
				//console.log("masuk 2",scope.output);
            });
			
			element.on('keyup', function (event) {
                var value = scope.ngModels;
				//console.log("masuk 1", input);
                value = value.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.'); 
                if (value.length == 1 && event.which == 48) {
                    // zero at first character 
                    value = value.replace("0", '')
                }
				//console.log("value", value);
                scope.ngModels = value;
				
                if (event.which == 64 || event.which == 16) {  
                    // numbers  
                    return false;  
                } if ([8, 13, 27, 37, 38, 39, 40, 110].indexOf(event.which) > -1) {  
                    // backspace, enter, escape, arrows  
                    return true;  
                } else if (event.which >= 48 && event.which <= 57) {  
                    // numbers  
                    return true;  
                } else if (event.which >= 96 && event.which <= 105) {  
                    // numpad number  
                    return true;  
                } else if ([46, 110, 190].indexOf(event.which) > -1) {  
                    // dot and numpad dot  
                    return true;  
                } else {  
                    event.preventDefault();  
                    return false;  
                } 
            });  


        }
    };
});