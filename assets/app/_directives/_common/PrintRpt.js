angular.module('PrintRpt',[])
  .factory('PrintRpt', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      print: function(url) {
        var res=$http.get('/api/'+url, {responseType: 'arraybuffer'});
        return res;
      },

    }
  });