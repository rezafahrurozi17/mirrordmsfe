(function() {
  var showErrorsModule;

  showErrorsModule = angular.module('ui.bootstrap.showErrors', []);

  showErrorsModule.directive('showErrors', [
    '$timeout', 'showErrorsConfig', '$interpolate', function($timeout, showErrorsConfig, $interpolate) {
      var getShowSuccess, getTrigger, linkFn;
      getTrigger = function(options) {
        var trigger;
        trigger = showErrorsConfig.trigger;
        if (options && (options.trigger != null)) {
          trigger = options.trigger;
        }
        return trigger;
      };
      getShowSuccess = function(options) {
        var showSuccess;
        showSuccess = showErrorsConfig.showSuccess;
        if (options && (options.showSuccess != null)) {
          showSuccess = options.showSuccess;
        }
        return showSuccess;
      };
      linkFn = function(scope, el, attrs, formCtrl) {
        var blurred, inputEl, inputName, inputNgEl, options, showSuccess, toggleClasses, trigger;
        blurred = false;
        options = scope.$eval(attrs.showErrors);
        showSuccess = getShowSuccess(options);
        trigger = getTrigger(options);
        inputEl = el[0].querySelector('.form-control[name],input[name],select[name],password[name],email[name],datetime[name],'+
                                      'datetime-local[name],date[name],month[name],time[name],week[name],number[name],'+
                                      'url[name],search[name],tel[name],color[name],textarea[name],ui-select[name],bsselect[name],bsselect-tree[name],angucomplete-alt[name]');
        inputNgEl = angular.element(inputEl);
        // console.log("inputNgEl=>",inputNgEl);
        inputName = $interpolate(inputNgEl.attr('name') || '')(scope);
        // console.log("input=>",inputName);
        if (!inputName) {
          throw "show-errors element has no child input elements with a 'name' attribute or a 'form-control' class";
        }
        inputNgEl[0].onblur = function(){
            // console.log('onblur=>');
        }
        inputNgEl.bind(trigger, function() {
          blurred = true;
          // console.log("formCtrl=>",formCtrl[inputName]);
          return toggleClasses(formCtrl[inputName].$invalid && formCtrl[inputName].$touched);
        });
        scope.$watch(function() {
          return formCtrl[inputName] && formCtrl[inputName].$invalid && formCtrl[inputName].$touched;
        }, function(invalid) {
          if (!blurred) {
            return;
          }
          //console.log("blurred=>",invalid);
          return toggleClasses(invalid);
        });
        scope.$on('show-errors-check-validity', function() {
          return toggleClasses(formCtrl[inputName].$invalid && formCtrl[inputName].$touched);
        });
        scope.$on('show-errors-reset', function() {
          return $timeout(function() {
            el.removeClass('has-error');
            el.removeClass('has-success');
            return blurred = false;
          }, 0, false);
        });
        return toggleClasses = function(invalid) {
          el.toggleClass('has-error', invalid);
          if (showSuccess) {
            return el.toggleClass('has-success', !invalid);
          }
        };
      };
      return {
        restrict: 'A',
        require: '^form',
        compile: function(elem, attrs) {
          //console.log("showErrors elem=>",elem);
          if (attrs['showErrors'].indexOf('skipFormGroupCheck') === -1) {
            if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
              throw "show-errors element does not have the 'form-group' or 'input-group' class";
            }
          }
          return linkFn;
        }
      };
    }
  ]);

  showErrorsModule.provider('showErrorsConfig', function() {
    var _showSuccess, _trigger;
    _showSuccess = false;
    _trigger = 'blur';
    this.showSuccess = function(showSuccess) {
      return _showSuccess = showSuccess;
    };
    this.trigger = function(trigger) {
      return _trigger = trigger;
    };
    this.$get = function() {
      return {
        showSuccess: _showSuccess,
        trigger: _trigger
      };
    };
  });

}).call(this);
