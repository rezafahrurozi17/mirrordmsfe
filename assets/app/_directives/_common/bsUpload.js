angular.module('bsUpload', []).directive('bsUpload', function($parse, $timeout, $templateCache, bsNotify) { //bsUpload -> bs-upload

    return {

        restrict: 'A',
        transclude: true,
        replace: true,
        //require:'^form',

        scope: {
            branch: '@',
            namatable: '@',
            tablerefid: '@',
            documenttypeid: '@',
            type: '@',
            maxbyte: '=',
            maxupload: '=', // jumlah upload 
            isImage: '=isImage', // 1 = image , 0 = document(filename ajah)
            uploadFiles: '=imgUpload'
        },
        link: function postLink(scope, element, attrs, formCtl) {
            if (typeof scope.maxbyte == "undefined") { scope.maxbyte = null; };

            element.bind("change", function(e) {                
                scope.file = (e.srcElement || e.target).files[0];  
                var tempsize = scope.maxbyte * 1000000;            
                console.log(scope.file);    
                if (scope.file.size > tempsize && scope.maxbyte != null) {
                    bsNotify.show({
                        title: "Error Message",
                        content: "Ukuran file maksimal " + scope.maxbyte + "MB .",
                        type: 'warning'
                    });
                } else {
                    var reader = new FileReader();                
                    reader.onload = function(e) {                      
                        console.log("NamaFile yang diambil");                    
                        console.log(scope.file.name);                    
                        console.log(scope);                    
                        var newImage = {
                            UpDocObj: reader.result,
                                                    Branch: scope.branch,
                                                    NameTable: scope.namatable,
                                                    TableRefId: scope.tablerefid,
                                                    DocumentTypeId: scope.documenttypeid,
                                                    FileName: scope.file.name,
                                                
                        };                    
                        scope.uploadFiles.push(newImage);                
                    }                
                    reader.readAsDataURL(scope.file);  
                }                      
            })

        }
    };
});