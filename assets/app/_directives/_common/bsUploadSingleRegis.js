angular.module('bsUploadsingleregis', []).directive('bsUploadsingleregis', function($rootScope, $parse, $timeout, $templateCache, bsNotify) { //bsUpload -> bs-upload

    return {

        restrict: 'A',
        transclude: true,
        replace: true,
        //require:'^form',

        scope: {
            branch: '@',
            namatable: '@',
            tablerefid: '@',
            documenttypeid: '@',
            type: '@',
            maxupload: '=', // jumlah upload 
            isImage: '=isImage', // 1 = image , 0 = document(filename ajah)
            uploadFiles: '=imgUpload'
        },
        link: function postLink(scope, element, attrs, formCtl) {
            element.bind("change", function(e) {                
                scope.file = (e.srcElement || e.target).files[0];                
                console.log(scope.file);                
                var reader = new FileReader();                
                reader.onload = function(e) {                      
                    console.log("NamaFile yang diambil");                    
                    console.log(scope.file.name);                    
                    console.log(scope);                    
                    var newImage = {                        
                        UpDocObj: btoa(reader.result),
                                                
                        Branch: scope.branch,
                                                
                        NameTable: scope.namatable,
                                                
                        TableRefId: scope.tablerefid,
                                                
                        DocumentTypeId: scope.documenttypeid,
                                                
                        FileName: scope.file.name,
                                            
                    };                    
                    scope.uploadFiles = newImage;
                    console.log("asdasd", scope.uploadFile);                
                }                
                reader.readAsDataURL(scope.file);            
            })

        }
    };
});