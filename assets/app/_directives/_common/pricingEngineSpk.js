angular.module('pricingEnginespk', []).directive('pricingEnginespk', function($q, $rootScope, $parse, $timeout, $templateCache, Pricing, bsNotify, $http) { //bsPrint -> bs-print

    return {
        template: function(element, attrs) { //attrs dipakai jika static atau untuk manipulasi template sebelum di-compile

            //#region Template
            // var strTemplate =
            //     '<div class="BodyRows col-lg-12 col-xs-12"   ng-repeat="row in jsData.BodyRows " >\
            //     <div ng-transclude="bodyCustom" ng-if="row.Cells[0].Value == \'+\'" /> \
            //     <hr ng-if="row.IsLine" class="ColLine" />\
            //     <div ng-if="!row.IsLine && row.Cells[0].Value != \'+\'"   ng-repeat="cell in row.Cells" class="{{cell.CSSClass}}" >\
            //         <label ng-hide="cell.IsCode" class="cell.CSSClass" >{{cell.Value}}</label>\
            //         <input ng-show="cell.IsCode" class="cell.CSSClass" type="text" ng-model="cell.Value"  />\
            //  </div></div>';

            // var strDebugTemplate = '</br>\
            //     <div class="col-md-12 col-xs-12" style="1px dotted" ><br/><p>THIS IS DEBUG DIV</p>\
            //         <div    ng-repeat="x in jsData.Cells " >\
            //             <label style="min-width:100px" >{{x.Code}}  :  {{x.Value}}</label>\
            //         </div>\
            //         <label>scope.jsData.Codes["</label> <input type="text" ng-model="debugCode" /> <label>"] = </label>\
            //         <input type="button" value="Show Value to Console" data-ng-click="ShowLogValue()"/> \
            //     </div> ';

            var xTemplate =
                //'<div style="border:3px dashed">  ' +
                '<div>' +
                //'<input sty type="button" class="rbtn btn" value="Calculate" ng-click="GetDataFromFactory();"/>'+
                ' <button type="button" class="rbtn btn" value="" data-ng-click="calculate()"><i class=" fa fa-calculator"></i><span>&nbsp&nbspKalkulasi</span></button></div> ';
            //'<div><input ng-show="true" type="text" class="form-control" ng-model="count" ng-change=""/> </div> ' +
            // '<div> </div> ' +
            // '</div>';

            return xTemplate;
            //#endregion Template

        },
        restrict: 'EA',
        replace: true,

        transclude: { 'bodyCustom': '?formBodyCustom' },
        scope: {
            branch: '@',
            debugCode: '@',
            onoftheroad: '=onoftheRoad',
            bookingFee: '=bookingFee',
            kalkulasiulang: '=isCalculate',
            tdpDp: '=tdpDp',
            discount: '=discount',
            subsidiDp: '=subsidiDp',
            discountSubrate: '=discountSubrate',
            bbnAdjust: '=bbnAdjust',
            bbnAdjustservice: '=bbnAdjustservice',
            mediatorCommition: '=mediatorCommition',
            karoseriprice: '=karoseriPrice',
            vehicleprice: '=vehiclePrice',
            dpp: "=dpp",
            ppn: "=ppn",
            totalbbn: "=totalBbn",
            bbn: "=bbn",
            pph22: "=pph22",
            totalvat: "=totalVat",
            priceafterdiscount: "=priceAfterdiscount",
            karoserivat: "=karoseriVat",
            karoserippn: "=karoseriPpn",
            url_pricing: '=urlPricing',
            url_Acc: '=urlAccessories',
            url_AccP: '=urlAccessoriespackage',
            unit: '=listUnit',
            ppnbit: '=ppnBit',
            ppnbnbit: '=ppnbnBit',
            index: '=index',
            harga: '=harga',
            grandtotal: '=grandTotal',
            jumlahunit: '=qty',
            totalacc: '=totalAcc',
            npwp: '=npwp',
            customerid: '=customerid',
            prospectcode: '=prospectcode',
            prospectid: '=prospectid'
        },
        link: function postLink(scope, element, attrs, formCtl) {

            console.log("kesini ga engine");

            $rootScope.$on("pricingCalculateSPK", function() {
                scope.pricingTotal();
            });
            //   var count = 0;  

            scope.count = 0;
            scope.tempcount = 0;
            scope.promiseArray = [];
            //scope.grandtotal = 0;
            //#region Action

            // {
            // "Title": "Pricing Engine",
            // "HeaderInputs": {
            //                 "$VehicleTypeColorId$": "4609",
            //                 "$OutletId$": "280",
            //                 "$AssemblyYear$": "2018",
            //                 "$KodeTPajak$":"01",
            //                 "$BBNAdj$":"500000",
            //                 "$BBNSAdj$":"1000000",
            //                 "$TotalACC$":"2000000",
            //                 "$Diskon$":"500000",
            //                 "$MCommision$":"1",
            //                 "$DSubsidiDP$":"2",
            //                 "$DSubsidiRate$":"3"
            //                 }
            // }

            scope.allGrandTotal = function() {
                for (var i in scope.unit) {
                    for (var j in scope.unit[i].ListDetailUnit) {
                        scope.unit[i].ListDetailUnit[j].GrandTotalAll =
                            scope.unit[i].ListDetailUnit[j].GrandTotalAll +
                            scope.totalvat + scope.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAcc
                            //     scope.ListInfoUnit[i].ListDetailUnit[j].GrandTotalAll +
                            //     parseFloat(res.data.Codes["[TotalHarga]"]) +
                            //     scope.unit[i].ListDetailUnit[j].GrandTotalAcc
                    }

                }
            }

            scope.trigerGrandTotal = function() {
                setTimeout(function() { $rootScope.$emit("GrandTotalSPK", {}) }, 3000);
            }

            scope.calculate = function (){
                scope.pricingTotal();
            }

           function panggil(callback){

           }

            scope.pricingTotal = function() {
                //console.log("Scope nya1", scope);
                scope.jsData = {};
                scope.jsData.HeaderInputs= {};
                //console.log("Scope nya2", scope);
                if (angular.isUndefined(scope.karoserivat) == true || scope.karoserivat == null) {
                    scope.karoserivat = 0.0;
                }
                
                scope.grandtotal = 0;
                var tempunit = angular.copy(scope.unit)

                for (var i in scope.unit) {
                    scope.unit[i].GrandTotalUnit = 0;
                    for (var j in scope.unit[i].ListDetailUnit) {
                        scope.unit[i].ListDetailUnit[j].GrandTotalAcc = 0;
                        scope.unit[i].ListDetailUnit[j].GrandTotalAll = 0;
                    }
                }

                scope.grandtotalarray = [];
                console.log("Scope nya3", scope);
                scope.jsData = {
                    Title: null,
                    HeaderInputs: {
                        $VehicleTypeColorId$: scope.unit[0].ListDetailUnit[0].VehicleTypeColorId,
                        $OnOffTheRoadId$ : scope.onoftheroad,
                        $BBNAdj$: scope.bbnAdjust,
                        $BBNSAdj$: scope.bbnAdjustservice,
                        $Diskon$: scope.discount,
                        $DSubsidiDP$: scope.subsidiDp,
                        $DSubsidiRate$: scope.discountSubrate,
                        $MCommision$: scope.mediatorCommition,
                        $FreePPnBMBit$: scope.ppnbnbit == true ? 1 : 0 ,
                        $FreePPNBit$: scope.ppnbit == true ? 1 : 0,
                        $TotalACC$: 0.0,
                        $OutletId$: scope.unit[0].ListDetailUnit[0].OutletId,
                        $VehicleYear$: scope.unit[0].ProductionYear,
                        $NPWP$: scope.npwp == true ? 1 : 0,
                        $CustomerId$: scope.customerid,
                        $ProspectCode$: scope.prospectcode,
                        $ProspectId$: scope.prospectid
                    }
                }

                //console.log("kesini")
                scope.jumlahunit = 0;
                for (var i in scope.unit) {
                    scope.unit[i].GrandTotalUnit = 0;
                    for (var j in scope.unit[i].ListDetailUnit) {
                       scope.jumlahunit ++;

                        for (var k in scope.unit[i].ListDetailUnit[j].ListAccessoriesPackage) {
                            scope.jsData.HeaderInputs.$TotalACC$ =
                                scope.jsData.HeaderInputs.$TotalACC$ + 
                                scope.unit[i].ListDetailUnit[j].ListAccessoriesPackage[k].TotalInclVAT;

                            scope.unit[i].ListDetailUnit[j].GrandTotalAcc =
                                scope.unit[i].ListDetailUnit[j].GrandTotalAcc + 
                                scope.unit[i].ListDetailUnit[j].ListAccessoriesPackage[k].TotalInclVAT;
                        }

                        for (var k in scope.unit[i].ListDetailUnit[j].ListAccessories) {
                            scope.jsData.HeaderInputs.$TotalACC$ =
                                scope.jsData.HeaderInputs.$TotalACC$ + 
                                scope.unit[i].ListDetailUnit[j].ListAccessories[k].TotalInclVAT;
                            
                            scope.unit[i].ListDetailUnit[j].GrandTotalAcc =
                                scope.unit[i].ListDetailUnit[j].GrandTotalAcc + 
                                scope.unit[i].ListDetailUnit[j].ListAccessories[k].TotalInclVAT;
                        }

                        scope.jsData.HeaderInputs.$TotalACC$ =
                            scope.jsData.HeaderInputs.$TotalACC$ + scope.karoserivat;

                        scope.unit[i].ListDetailUnit[j].GrandTotalAcc = scope.unit[i].ListDetailUnit[j].GrandTotalAcc + scope.karoserivat;

                    }

                    
                }

                scope.totalacc = scope.jsData.HeaderInputs.$TotalACC$;
                // }
                console.log("testunit",scope.url_pricing);
                Pricing.getData(scope.url_pricing, scope.jsData)
                .then(
                    function(res) {  

                        scope.dpp = parseFloat(res.data.Codes["[DPP]"]);  
                        scope.ppn = parseFloat(res.data.Codes["[PPN]"]);  
                        scope.bbn = parseFloat(res.data.Codes["[BBN]"])
                        scope.totalbbn = parseFloat(res.data.Codes["[TotalBBN]"]);
                        scope.pph22 = parseFloat(res.data.Codes["[PPH22]"]);
                        scope.totalvat = parseFloat(res.data.Codes["[TotalHarga]"]);
                        scope.vehicleprice = parseFloat(res.data.Codes["[HargaMobil]"]);
                        scope.priceafterdiscount = parseFloat(res.data.Codes["[PriceAfterDiskon]"]);
                       // scope.grandtotal = parseFloat(res.data.Codes["[TotalHarga]"]) * jumlahunit + scope.jsData.$TotalACC$;

                        //scope.grandtotalarray.push({ GrandTotalAll: parseFloat(res.data.Codes["[GrandTotal]"]) })
                        //scope.unit[i].ListDetailUnit[j].GrandTotalAll = parseFloat(res.data.Codes["[GrandTotal]"]);
                        // scope.unit[i].ListDetailUnit[j].GrandTotalAll =
                        //     scope.unit[i].ListDetailUnit[j].GrandTotalAll +
                        //     parseFloat(res.data.Codes["[TotalHarga]"]) +
                        //     scope.unit[i].ListDetailUnit[j].GrandTotalAcc
                        //scope.jsData = res.data;
                        //scope.url_pricing, scope.unit[i].ListDetailUnit[j].
                        //scope.harga[scope.index] = {totalsatuan:0};
                        //scope.harga[scope.index].totalsatuan = parseFloat(scope.jsData.Codes["[GrandTotal]"]);
                        //scope.allGrandTotal();
                        //for (var i in $scope.selected_data.ListInfoUnit) {
                        // $scope.selected_data.ListInfoUnit[i].GrandTotalUnit = 0;
                        //for (var j in $scope.selected_data.ListInfoUnit[i].ListDetailUnit) {
                        // angular.merge({}, $scope.selected_data.ListInfoUnit[i].ListDetailUnit[j], $scope.grandTotalUnit);

                        //scope.unit[i].ListDetailUnit[j].GrandTotalAll = scope.totalvat + scope.unit[i].ListDetailUnit[j].GrandTotalAcc;

                        //scope.unit[i].GrandTotalUnit = scope.unit[i].GrandTotalUnit + scope.unit[i].ListDetailUnit[j].GrandTotalAll;
                        //}
                        // scope.GrandTotal = scope.GrandTotal + scope.unit[i].GrandTotalUnit;
                        //}
                        //console.log("harag", res.data);
                        //scope.pricingTotal();

                        //$rootScope.$emit("GrandTotalSPK", {});
                        scope.trigerGrandTotal();
                      

                        scope.kalkulasiulang = false;

                        if (scope.bbn == null || scope.bbn < 1) {
                            bsNotify.show({
                                title: "Peringatan",
                                content: "Nilai BBN 0, hubungi Admin",
                                type: 'warning'
                            });
                        }
                        //console.log("direcRecalc", scope.recalculate);
                        // $timeout(scope.trigerGrandTotal(), 5000);
                    });



                //console.log("hgasjhdas", scope.unit, scope.grandtotalarray);
            }

            //#endregion Action
        }
    };
});