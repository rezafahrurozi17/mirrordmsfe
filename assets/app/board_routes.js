angular.module('app')

.directive('onSizeChanged', ['$window', function ($window) {
    return {
        restrict: 'A',
        scope: {
            onSizeChanged: '&'
        },
        link: function (scope, $element, attr) {
            var element = $element[0];

            cacheElementSize(scope, element);
            $window.addEventListener('resize', onWindowResize);

            function cacheElementSize(scope, element) {
                scope.cachedElementWidth = element.offsetWidth;
                scope.cachedElementHeight = element.offsetHeight;
            }

            function onWindowResize() {
                var isSizeChanged = scope.cachedElementWidth != element.offsetWidth || scope.cachedElementHeight != element.offsetHeight;
                if (isSizeChanged) {
                    var expression = scope.onSizeChanged();
                    expression();
                }
            };
        }
    }
}])

    /*.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
        events:true,
        serie:true,
        modules: [
            //-------------------------
            // LazyLoad JS Dependencies
            //-------------------------
            {
                name: 'boards',
                files: [
                        'module/summernote/summernote.min.js',
                        'module/summernote/summernote.css',
                        'module/summernote/angular-summernote.min.js',
                ],
                serie:true,
            },            
        ]
        });
    }])
    */
    .config(['$locationProvider','$stateProvider','$urlRouterProvider', function($locationProvider,$stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.irregularity', {
            data: {
                title: 'Irregularity Board GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "irregularity@app": {
                    templateUrl: 'app/boards/irregularity/irregularity.html',
                    controller: 'IrregularityController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })

        .state('app.jpcbgr', {
            data: {
                title: 'JPCB GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jpcbgr@app": {
                    templateUrl: 'app/boards/jpcbgr/jpcbGr.html',
                    controller: 'JpcbGrController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.jpcbgrbck', {
            data: {
                title: 'JPCB GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jpcbgrbck@app": {
                    templateUrl: 'app/boards/jpcbgr/jpcbGrx.html',
                    controller: 'JpcbGrControllerx'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.irregularitybp', {
            data: {
                title: 'Irregularity Board BP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "irregularitybp@app": {
                    templateUrl: 'app/boards/irregularitybp/irregularitybp.html',
                    controller: 'IrregularityBpController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })

        .state('app.partsorderboard', {
            data: {
                title: 'Parts Order Board',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "partsorderboard@app": {
                    templateUrl: 'app/boards/partsorder/partsorder.html',
                    controller: 'PartsOrderController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })

        .state('app.partssupplyboard', {
            data: {
                title: 'Parts Supply Board',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "partssupplyboard@app": {
                    templateUrl: 'app/boards/partssupply/partssupply.html',
                    controller: 'PartsSupplyController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })

        .state('app.jpcbbp', {
            data: {
                title: 'JPCB BP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jpcbbp@app": {
                    templateUrl: 'app/boards/jpcbbp/jpcbbp.html',
                    controller: 'JpcbBpController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    slider: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('slider');
                    }]
            },
            params: { tab : null },
        })

        .state('app.sacapacityboard', {
            data: {
                title: 'SA Capacity Boardd',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "sacapacityboard@app": {
                    templateUrl: 'app/boards/sacapacity/sacapacity.html',
                    controller: 'SaCapacityController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })

        .state('app.satelitemonboard', {
            data: {
                title: 'Satelite Monitoring Board',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "satelitemonboard@app": {
                    templateUrl: 'app/boards/satelitemon/satelitemon.html',
                    controller: 'SateliteMonController_New'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })

        .state('app.asbmaintest', {
            data: {
                title: 'Appointment Schedule Board',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "asbmaintest@app": {
                    templateUrl: 'app/boards/asb/asbmaintest.html',
                    controller: 'AsbMainTestController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    // notif1:  function($http){
                    //     // $http returns a promise for the url data
                    //     return $http({method: 'GET', url: '/Scripts/jquery.signalR-2.1.2.js'});
                    // },
                    // notif2:  function($http){
                    //     // $http returns a promise for the url data
                    //     return $http({method: 'GET', url: '/signalr/hubs'});
                    // },
                    // notif1: ['$ocLazyLoad',function($ocLazyLoad){
                    //      return $ocLazyLoad.load('notify');
                    // }],
                    // notif2: ['$ocLazyLoad',function($ocLazyLoad){
                    //      return $ocLazyLoad.load('/signalr/hubs');
                    // }],
            },
            params: { tab : null },
        })

        .state('app.jpcbgrview', {
            data: {
                title: 'JPCB GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jpcbgrview@app": {
                    templateUrl: 'app/boards/jpcbgrview/jpcbgrview.html',
                    controller: 'JpcbGrViewController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })
        .state('app.jpcbviewbp', {
            data: {
                title: 'JPCB VIEW BP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jpcbviewbp@app": {
                    templateUrl: 'app/boards/jpcbbpview/jpcbbpview.html',
                    controller: 'JpcbBpViewController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.jscb', {
            data: {
                title: 'JPCB GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jscb@app": {
                    templateUrl: 'app/boards/jscb/jscb.html',
                    controller: 'JscbController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.soundtest', {
            data: {
                title: 'Sound Test',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "soundtest@app": {
                    templateUrl: 'app/boards/queue/soundtest.html',
                    controller: 'SoundTestController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    sounds: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sounds')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.cstatusgr', {
            data: {
                title: 'Customer Status GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "cstatusgr@app": {
                    templateUrl: 'app/boards/cstatus/cstatusgr.html',
                    controller: 'CustStatusGrController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.cstatusbp', {
            data: {
                title: 'Customer Status BP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "cstatusbp@app": {
                    templateUrl: 'app/boards/cstatus/cstatusbp.html',
                    controller: 'CustStatusBpController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.custqueuegr', {
            data: {
                title: 'Queue Board GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "custqueuegr@app": {
                    templateUrl: 'app/boards/queue/queuegr.html',
                    controller: 'QueueGrController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    sounds: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sounds')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.custqueuebp', {
            data: {
                title: 'Queue Board Bp',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "custqueuebp@app": {
                    templateUrl: 'app/boards/queue/queuebp.html',
                    controller: 'QueueBpController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    sounds: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sounds')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.wolistboardgr', {
            data: {
                title: 'WO List Board GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "wolistboardgr@app": {
                    templateUrl: 'app/boards/wolist/wolistgr.html',
                    controller: 'WoListGrController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.wolistboardbp', {
            data: {
                title: 'WO List Board GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "wolistboardbp@app": {
                    templateUrl: 'app/boards/wolist/wolistbp.html',
                    controller: 'WoListBpController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        .state('app.tabgr', {
            data: {
                title: 'WO List Board GR',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "tabgr@app": {
                    templateUrl: 'app/boards/tab/tabgr.html',
                    controller: 'TabController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('sysapp')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    AppModule: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('app_boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
                    boards: ['$ocLazyLoad', 'ngDialog', 'cfpLoadingBar', function($ocLazyLoad, ngDialog, cfpLoadingBar) {
                         return $ocLazyLoad.load('boards')
                        .then(function(res){

                        },function(err){errorPopUp(err, ngDialog, cfpLoadingBar)});
                    }],
            },
            params: { tab : null },
        })        

        function errorPopUp(err, ngDialog, cfpLoadingBar){
            if (ngDialog.getOpenDialogs().length==0) {                        
                ngDialog.open({ 
                    template: '_sys/auth/dialog_err_default.html',
                    controller: ['$scope', '$timeout', function($scope) {
                        $scope.status = "Error Koneksi";
                        $scope.message = "Mohon cek koneksi anda / aplikasi sedang dalam maintenance. Silakan coba beberapa saat lagi, Terima kasih";
                        $scope.debugMode = false;
                    }] 
                });
                cfpLoadingBar.complete();
            }
        }


  }])
