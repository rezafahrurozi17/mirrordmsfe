angular.module('app')
    .controller('AsbMainTestController', function($scope, $http, CurrentUser,ngDialog, Parameter,$timeout, $window, $filter, $state, AsbEstimasiFactory, AsbGrFactory, AsbProduksiFactory, $rootScope) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        console.log("Jquery Version", jQuery.fn.jquery);
        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd-MM-yyyy',
            // disableWeekend: 1
        };

        $scope.boardName = 'asb_gr_test';

        $scope.mdata = {}
        // $scope.mdata.PrediagStartTime = new Date('2018-01-25 15:00');
        var vxNewDate = new Date();
        var vTanggal = $filter('date')(vxNewDate, 'yyyy-MM-dd');
        $scope.mdata.PrediagStartTime = new Date(vTanggal+' 15:00');
        $scope.mdata.PrediagStallId = 0;
        $scope.mdata.JobStartTime = new Date(vTanggal+' 15:00:00');
        $scope.mdata.JobEndTime = new Date(vTanggal+' 16:00:00');
        $scope.mdata.JobStallId = 0;
        //$scope.mdata.JobStallId = 0;
        $scope.mdata.durasi = 5;



        $scope.user = CurrentUser.user();

        var vmsgname = 'joblist';
        var voutletid = $scope.user.OrgId;
        var vgroupname = voutletid+'.'+vmsgname;
        var vappname = vmsgname;
        try {
            $rootScope.ChatProxy.invoke('joinRoom', vgroupname);
        } catch (exc){
            console.log(exc);
            // $scope.showMsg = {message: 'Tidak dapat terhubung dengan SignalR server.', title: 'Koneksi SignalR gagal.', closeText: 'Close', exception: exc}
            // ngDialog.openConfirm ({
            //     width: 700,
            //     template: 'app/boards/factories/showMessage.html',
            //     plain: false,
            //     scope: $scope,
            // })
            
        }

        $scope.$on('SR-'+vappname, function(e, data){
            console.log("notif-data", data);
            var vparams = data.Message.split('.');

            var vJobId = 0;
            if (typeof vparams[0] !== 'undefined'){
                var vJobId = vparams[0];
            }

            var vStatus = 0;
            if (typeof vparams[1] !== 'undefined'){
                var vStatus = vparams[1];
            }


            var jobid = vparams[0];
            var vcounter = vparams[1];
            var vJobList = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "20"];
            var xStatus = vStatus+'';

            if (vJobList.indexOf(xStatus)>=0){
                $scope.asb.reloadBoard();
            }
            
        });
                
        
        $scope.asb = {currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza", durasi: 3}
        //$scope.asb.currentDate.setHours($scope.asb.currentDate.getHours()+1);
        console.log("$scope.asb.currentDate", $scope.asb.currentDate);
        console.log("$scope.dateOptions", $scope.dateOptions);
        $scope.asb.startDate = new Date();
        $scope.asb.endDate = new Date();
        $scope.asb.endDate.setDate($scope.asb.endDate.getDate()+7);
        $scope.asb.TPS = {id: '62', name: 'TPS'};
        $scope.asb.Light = {id: '63', name: 'LIGHT'};
        $scope.asb.BpCategory = $scope.asb.TPS;
        var allBoardHeight = 500;

        $scope.ClickNewChip= function(){
            $scope.asb.NewChips = 1;
        }

        $scope.$watch('asb.showMode', function(newValue, oldValue) {
            if (newValue !== oldValue) {
                console.log('Changed! '+newValue);
                $scope.asb.reloadBoard();
            }
        });
        $scope.$on('$viewContentLoaded', function() {
            $scope.asb.showAsbEstimasi = function(){
                this.showMode = 'estimasi';
            }
            $scope.asb.showAsbProduksi = function(){
                this.showMode = 'produksi';
            }
            $scope.asb.showAsbMain = function(){
                this.showMode = 'main';
            }
            $scope.asb.SaveData = function(){
                if (AsbGrFactory.checkChipFinished($scope.boardName)){
                    var splitChip = AsbGrFactory.getChipData($scope.boardName);
                    console.log('Split Chip data.', splitChip);
                } else {
                    alert ('Masih ada sisa jam belum diplot.')
                }
            }
            $scope.asb.showAsbGr = function(){
                this.showMode = 'asbgr';
            }
            $scope.asb.reloadBoard = function(){
                $timeout( function(){
                    var item = $scope.asb.startTime;
                    var vtime = new Date();
                    var vtime
                    var tmphour
                    var tmpMinute
                    if (item !== undefined && item !== null) {
                        vtime = item;
                        tmphour = item.getHours();
                        tmpMinute = item.getMinutes();
                    } else {
                        vtime = new Date();
                        tmphour = vtime.getHours();
                        tmpMinute = vtime.getMinutes();
                    }
        
                    vtime.setHours(tmphour);
                    vtime.setMinutes(tmpMinute);
                    vtime.setSeconds(0);

                    if ($scope.asb.BpCategoryId=='62'){
                        $scope.asb.BpCategoryName = 'TPS'
                    } else {
                        $scope.asb.BpCategoryName = 'Light'
                    }
                    $scope.asb.PrediagStartTime = $scope.mdata.PrediagStartTime;
                    $scope.asb.PrediagStallId = $scope.mdata.PrediagStallId;
                    $scope.asb.JobStartTime = $scope.mdata.JobStartTime;
                    $scope.asb.JobEndTime = $scope.mdata.JobEndTime;
                    $scope.asb.JobStallId = $scope.mdata.JobStallId;
                    // ============================================
                    var vobj = {
                        mode: $scope.asb.showMode,
                        preDiagnose: {FrequencyIncident: 1},
                        currentDate: $scope.asb.currentDate,
                        nopol: $scope.asb.nopol,
                        tipe: $scope.asb.tipe,
                        durasi: $scope.mdata.durasi,

                        asbStatus: $scope.asb,
                        //jobId: 1268,
                        // stallId: 5,
                        startTime: vtime,
                        // prediagnoseStallId: 12,
                        // PreDiagnoseScheduledTime: '2018-01-24T00:00:00',
                        // width: 4,
                        prediagnoseOnly: 0,
                        //visible: false,
                    }
                    var currentItem = {
                        mode: $scope.asb.showMode,
                        currentDate:  $scope.asb.currentDate,
                        nopol: $scope.asb.nopol,
                        tipe: $scope.asb.tipe,
                        durasi: $scope.mdata.durasi,
                        startTime: vtime,
                        saId: 0,
                        asbStatus: $scope.asb,
                        stallId: 0,
                        // JobId: $scope.mData.JobId,
                        /*stallId :1,
                        startDate :,
                        endDate:,
                        durasi:,*/
                    }
                    if ($scope.asb.showMode=='estimasi'){
                        showBoardEstimasi(currentItem);
                    }
                    if ($scope.asb.showMode=='produksi'){
                        showBoardProduksi(vobj);
                    }
                    if ($scope.asb.showMode=='asbgr'){
                        showBoardAsbGr(vobj);
                    }
                }, 10);
            }
        });

        // var notifyId = false;
        // var testNotify = function(){
        //     JsNotify.registerGroup('655.new_job_item');
        //     JsNotify.registerGroup('655.update_job_item');
        //     notifyId = JsNotify.registerNotify(function(groupName, notifName, dataId, status, message){
        //         console.log('***XNotification 2***');
        //         console.log('group name', groupName);
        //         console.log('notification name', notifName);
        //         console.log('dataId', dataId);
        //         console.log('status', status);
        //         console.log('message', message);
        //     });


        // }

        // testNotify();

        // $scope.$on('$destroy', function() {
        //     JsNotify.unregisterNotify(notifyId);
        // });        
        
        $scope.asb.FullScreen = function(){
            // alert('full screen');
            var vobj = document.getElementById('asb_estimasi_view');
            vobj.style.cssText = 'position: absolute; left: 0; top: 0; width: 100%; height: 100%;';
        }
        

        angular.element($window).bind('resize', function () {
            AsbEstimasiFactory.resizeBoard();
            // AsbProduksiFactory.resizeBoard();
            
            // jpcbStopage.autoResizeStage();
            // jpcbStopage.redraw();
        });

        // display board
        var showBoardEstimasi = function(vitem){
            AsbEstimasiFactory.showBoard({container: 'asb_estimasi_view', currentChip: vitem});

        }

        var showBoardProduksi = function(vitem){
            console.log("asb produksi", vitem);
            //AsbGrFactory.showBoard({container: 'asb_produksi_view', currentChip: vitem});
                        //             vargStall = {}
                        // vargStall.BpCategoryId = $scope.asb.BpCategoryId;
                        // vargStall.BpCategoryName = $scope.asb.BpCategoryName();

                        // vobj.startDate = $scope.asb.startDate;
                        // vobj.endDate = $scope.asb.endDate;
            AsbProduksiFactory.showBoard({container: 'asb_produksi_view', 
                currentChip: vitem, 
                argChips: {startDate: $scope.asb.startDate, endDate: $scope.asb.endDate, BpCategory: $scope.asb.BpCategory},
                argStall: {BpCategoryId: $scope.asb.BpCategory.id, BpCategoryName: $scope.asb.BpCategory.name},
                argHeader: {startDate: $scope.asb.startDate, endDate: $scope.asb.endDate, BpCategory: $scope.asb.BpCategory},
            });
            // AsbProduksiService.showBoard({container: 'asb_produksi_view'});

            // var vcontainer = 'asb_produksi_view';
        }

        var plotDataASB = function(data){

            $scope.mdata.PrediagStartTime = new Date(data.PrediagScheduledTime);
            $scope.mdata.PrediagStallId = data.PrediagStallId;

            $scope.mdata.JobStartTime = new Date(data.startTime);
            $scope.mdata.JobEndTime = new Date(data.endTime);
            $scope.mdata.JobStallId = data.stallId;
    

            // $scope.asb.PrediagStartTime = new Date('2018-01-25 15:00');
            // $scope.asb.PrediagStallId = 0;

            // $scope.asb.startTime = data.startTime;
            // $scope.asb.endTime = data.endTime;

            // $scope.asb.PrediagStartTime = new Date('2018-01-25 15:00');
            // $scope.asb.PrediagStallId = 0;
            // $scope.asb.JobStartTime = new Date('2018-01-25 14:00:00');
            // $scope.asb.JobEndTime = new Date('2018-01-25 15:00:00');
            // $scope.asb.JobStallId = 0;
    
        
        }
        var showBoardAsbGr = function(vitem){
            AsbGrFactory.showBoard({boardName: $scope.boardName, container: 'test_asb_asbgr_view', currentChip: vitem, onPlot: plotDataASB});

        }

        
    })