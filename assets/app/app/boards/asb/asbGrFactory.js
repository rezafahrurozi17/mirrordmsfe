angular.module('app')
    .factory('AsbGrFactory', function($rootScope, $http, CurrentUser, $filter, $timeout, jpcbFactory, JpcbGrViewFactory, ngDialog) {
        var currentUser = CurrentUser.user;
        var woCategorySelected = '';
        var tmpDurasi = 0;
        var tmpCloseTime = '';
        var tmpStartTime = ''
        var tmpChip ={};
        // console.log(currentUser);
        return {
            minuteDelay: 10*60+10,
            //minuteDelay: 0,
            onPlot: function(){
                // do nothing
            },
            getDataJobChipInfo: function(data){
                return tmpChip;
            },
            checkTime:function(tmpLastDate,tmpFirstDate){
                var hoursDate
                var minutesDate
                var dateOne
                var dateTwo
                var firstHourDate
                var endHourForStall
                var finalHourStall
                if(tmpLastDate !== undefined && tmpLastDate !== null && tmpFirstDate !== undefined && tmpFirstDate !== null){
                    endHourForStall = angular.copy(tmpCloseTime);
                    endHourForStall = endHourForStall.split(":");
                    finalHourStall = parseInt(endHourForStall[0]) + 1;
                    hoursDate = tmpLastDate.getHours();
                    firstHourDate = tmpFirstDate.getHours();
                    minutesDate = tmpLastDate.getMinutes();
                    dateOne = this.changeFormatDateStrip(tmpLastDate);
                    dateTwo = this.changeFormatDateStrip(tmpFirstDate);
                    if(dateOne == dateTwo){
                        if(hoursDate >= finalHourStall){
                            return true
                        }else if(hoursDate == finalHourStall && minutesDate > 0){
                            return true
                        }else if(hoursDate == finalHourStall && firstHourDate == 8){
                            return true
                        }else{
                            return false
                        }
                    }else{
                        return true
                    }
                }
            },
            isEmpty:function(obj){
                for(var key in obj) {
                    if(obj.hasOwnProperty(key))
                        return false;
                }
                return true;
            },
            boardName: '',
            showBoard: function(config){
                console.log('config blalala', config)
                console.log('config blalala cari jenis pekerjaan', config.currentChip.woCategory);
                woCategorySelected = config.currentChip.woCategory;
                tmpChip = angular.copy(config.currentChip.asbStatus);
                if(config.currentChip.longEnd !== undefined && config.currentChip.longEnd !== null){
                    tmpCloseTime = config.currentChip.longEnd;
                }
                console.log('config blalala tmpCloseTime===>', tmpCloseTime)
                if(config.currentChip.longStart !== undefined && config.currentChip.longStart !== null){
                    tmpStartTime = config.currentChip.longStart;
                }
                var vcontainer = config.container;
                var vname = 'asbgr';
                if (typeof config.boardName !== 'undefined'){
                    vname = config.boardName;
                }
                this.boardName = vname;
                var vobj = document.getElementById(vcontainer);
                if(vobj){
                    var vwidth = vobj.clientWidth;
                    console.log(vwidth);
                    if (vwidth<=0) {
                        return;
                    }
                } else {
                    return;
                }
                if (typeof config.onPlot=='function'){
                    this.onPlot = config.onPlot;
                }

                if (typeof config.currentChip.asbStatus ==='undefined'){
                    config.currentChip.asbStatus = {}
                }

                if (typeof config.currentChip.asbStatus.NewChips === 'undefined'){
                    
                    config.currentChip.asbStatus.NewChips = 1;
                    console.log("INI DUDE -1",config.currentChip.asbStatus.NewChips);
                }
                console.log("tmpCloseTime",tmpCloseTime);
                console.log("tmpStartTime",tmpStartTime);
                console.log("INI DUDE 0",config.currentChip.asbStatus.NewChips);
                if (config.currentChip.asbStatus.NewChips == 1){
                    config.currentChip.asbStatus.NewChips = 0;
                    config.currentChip.asbStatus.JobChipInfo = {}
                    config.currentChip.asbStatus.JobChipInfo.SplitChips = {}
                    config.currentChip.asbStatus.JobChipInfo.AllSplit = []
                    config.currentChip.asbStatus.JobChipInfo.JobStallId = config.currentChip.asbStatus.JobStallId;
                    config.currentChip.asbStatus.JobChipInfo.JobStartTime = config.currentChip.asbStatus.JobStartTime;
                    config.currentChip.asbStatus.JobChipInfo.JobEndTime = config.currentChip.asbStatus.JobEndTime;
                    config.currentChip.asbStatus.JobChipInfo.PrediagStallId = config.currentChip.asbStatus.PrediagStallId;
                    config.currentChip.asbStatus.JobChipInfo.PrediagStartTime = config.currentChip.asbStatus.PrediagStartTime;

                    if (config.currentChip.asbStatus.JobStallId){
                        var vxdate = $filter('date')(config.currentChip.asbStatus.JobStartTime, 'yyyy-MM-dd');
                        var xtstart = $filter('date')(config.currentChip.asbStatus.JobStartTime, 'HH:mm');
                        var vtstart = JsBoard.Common.getMinute(xtstart);
                        var vminLast = JsBoard.Common.getMinute(this.getLastHour());
                        xdurasi = (vminLast - vtstart)/60;
                        if (xdurasi>config.currentChip.durasi){
                            xdurasi = config.currentChip.durasi;
                        }
                        var vres = JsBoard.Common.getTimeString(xdurasi);
                        var vstartTime = new Date(config.currentChip.asbStatus.JobStartTime);
                        var vendTime = new Date(config.currentChip.asbStatus.JobStartTime);
                        var vdurmin = xdurasi*60;
                        vendTime.setMinutes(vendTime.getMinutes()+vdurmin);

                        config.currentChip.asbStatus.JobChipInfo.SplitChips[vxdate] = {
                            stallId: config.currentChip.asbStatus.JobStallId,
                            startTime: $filter('date')(vstartTime, 'HH:mm'),
                            endTime: $filter('date')(vendTime, 'HH:mm'),
                            durasi: xdurasi
                        }

                        // ======== ini hrs nya buat di wo list biar chip carry over yg di hari lain jg ke plot sesuai data tarikan be =========== start
                        if (config.currentChip.asbStatus.SplitChips.length > 1){
                            config.currentChip.asbStatus.JobChipInfo.SplitChips = {} // ini agak ragu apa pengaruh ga kl di kosongin dl, tp cb dl aja 7mei2021
                            config.currentChip.asbStatus.JobChipInfo.AllSplit = []
                            var pembandingDatasplit = 0; // untuk bandingin data split setelah ada edit / tambah job di hari tersebut, krn mungkin beda start / end

                            for (var i=0; i<config.currentChip.asbStatus.SplitChips.length; i++){
                                if (config.currentChip.asbStatus.SplitChips[i].tmpDate == vxdate && config.currentChip.asbStatus.SplitChips[i].isSplitActive == 1){
                                    if (pembandingDatasplit == 0){ // untuk ambil jobsplitid terbesar di hari itu, krn jobsplitid terbesar kemungkinan adalah chip terakhir
                                        pembandingDatasplit = config.currentChip.asbStatus.SplitChips[i].JobSplitId
                                    } else {
                                        if (config.currentChip.asbStatus.SplitChips[i].JobSplitId > pembandingDatasplit){
                                            pembandingDatasplit = config.currentChip.asbStatus.SplitChips[i].JobSplitId
                                        }
                                    }
                                }
                            }


                            for (var i=0; i<config.currentChip.asbStatus.SplitChips.length; i++){
                                // if (config.currentChip.asbStatus.SplitChips[i].tmpDate != vxdate){
                                    var tglCarryOver = config.currentChip.asbStatus.SplitChips[i].tmpDate

                                    if (config.currentChip.asbStatus.SplitChips[i].JobSplitId == pembandingDatasplit){
                                        config.currentChip.asbStatus.SplitChips[i].stallId = config.currentChip.asbStatus.JobChipInfo.JobStallId
                                        config.currentChip.asbStatus.SplitChips[i].startTime = $filter('date')(config.currentChip.asbStatus.JobChipInfo.JobStartTime, 'HH:mm:ss');
                                        config.currentChip.asbStatus.SplitChips[i].endTime = $filter('date')(config.currentChip.asbStatus.JobChipInfo.JobEndTime, 'HH:mm:ss');
                                    }

                                    var xtstartC = config.currentChip.asbStatus.SplitChips[i].startTime
                                    var xtendC = config.currentChip.asbStatus.SplitChips[i].endTime
                                    var vtstartC = JsBoard.Common.getMinute(xtstartC);
                                    var vtendC = JsBoard.Common.getMinute(xtendC);
                                    // var vminLastC = JsBoard.Common.getMinute(this.getLastHour());
                                    var xdurasiC = (vtendC - vtstartC)/60;
                                    // if (xdurasiC>config.currentChip.durasi){
                                    //     xdurasiC = config.currentChip.durasi;
                                    // }

                                    

                                    config.currentChip.asbStatus.JobChipInfo.SplitChips[tglCarryOver] = {
                                        JobSplitId: config.currentChip.asbStatus.SplitChips[i].JobSplitId,
                                        stallId: config.currentChip.asbStatus.SplitChips[i].stallId,
                                        startTime: config.currentChip.asbStatus.SplitChips[i].startTime,
                                        endTime: config.currentChip.asbStatus.SplitChips[i].endTime,
                                        durasi: xdurasiC,
                                        tmpDate: tglCarryOver,
                                        isSplitActive: config.currentChip.asbStatus.SplitChips[i].isSplitActive,
                                        IsInJobStopageGR: config.currentChip.asbStatus.SplitChips[i].IsInJobStopageGR,
                                    }
                                    config.currentChip.asbStatus.JobChipInfo.AllSplit.push(config.currentChip.asbStatus.JobChipInfo.SplitChips[tglCarryOver])

                                // }
                            }
                            
                        }

                        // ======== ini hrs nya buat di wo list biar chip carry over yg di hari lain jg ke plot sesuai data tarikan be =========== end
                        console.log('start Chip Info', config.currentChip.asbStatus.JobChipInfo);
                    }
                } else if (config.currentChip.asbStatus.NewChips == 2){

                
                } else {
                    // console.log("config.currentChip.asbStatus.JobChipInfo ======>",config.currentChip.asbStatus.JobChipInfo);
                    if(config.currentChip.asbStatus.JobChipInfo == undefined){
                        config.currentChip.asbStatus.NewChips = 0;
                        config.currentChip.asbStatus.JobChipInfo = {}
                        config.currentChip.asbStatus.JobChipInfo.AllSplit = []
                        config.currentChip.asbStatus.JobChipInfo.JobStallId = config.currentChip.asbStatus.JobStallId;
                        config.currentChip.asbStatus.JobChipInfo.JobStartTime = config.currentChip.asbStatus.JobStartTime;
                        config.currentChip.asbStatus.JobChipInfo.JobEndTime = config.currentChip.asbStatus.JobEndTime;
                        config.currentChip.asbStatus.JobChipInfo.PrediagStallId = config.currentChip.asbStatus.PrediagStallId;
                        config.currentChip.asbStatus.JobChipInfo.PrediagStartTime = config.currentChip.asbStatus.PrediagStartTime;
                        if(config.currentChip.asbStatus.JobChipInfo.SplitChips == undefined){
                            config.currentChip.asbStatus.JobChipInfo.SplitChips = {};
                            // ====================
                            for(var i in config.currentChip.asbStatus.SplitChips){
                                var isData = config.currentChip.asbStatus.SplitChips[i];
                                var thisNewFirstDate = new Date(isData.tmpDate +" "+isData.startTime);
                                var thisNewLastDate = new Date(isData.tmpDate +" "+isData.endTime);
                                var vxdate = $filter('date')(thisNewFirstDate, 'yyyy-MM-dd');
                                xdurasi = (thisNewLastDate - thisNewFirstDate)/(3600 * 1000);
                                // if (xdurasi>config.currentChip.durasi){
                                //     xdurasi = config.currentChip.durasi;
                                // }
                                console.log("isDAta",isData);
                                console.log("xdurasi",xdurasi);
                                config.currentChip.asbStatus.JobChipInfo.SplitChips[isData.tmpDate] = {
                                    stallId: isData.stallId,
                                    startTime: isData.startTime,
                                    endTime: isData.endTime,
                                    durasi: xdurasi,
                                    JobSplitId:isData.JobSplitId,
                                    isFI:isData.isFI?isData.isFI:0
                                }
                            }
                            console.log("confignya dong nyet ======>",config.currentChip.asbStatus.JobChipInfo.SplitChips);
                            // ====================
           
                        }
                    }
                    // config.currentChip.asbStatus.JobChipInfo = {}
    
                    // config.currentChip.asbStatus.JobChipInfo.JobStallId = config.currentChip.asbStatus.JobStallId;
                    // config.currentChip.asbStatus.JobChipInfo.JobStartTime = config.currentChip.asbStatus.JobStartTime;
                    // config.currentChip.asbStatus.JobChipInfo.JobEndTime = config.currentChip.asbStatus.JobEndTime;
                    // config.currentChip.asbStatus.JobChipInfo.PrediagStallId = config.currentChip.asbStatus.PrediagStallId;
                    // config.currentChip.asbStatus.JobChipInfo.PrediagStartTime = config.currentChip.asbStatus.PrediagStartTime;
                    config.currentChip.asbStatus.JobStallId = config.currentChip.asbStatus.JobChipInfo.JobStallId;
                    config.currentChip.asbStatus.JobStartTime = config.currentChip.asbStatus.JobChipInfo.JobStartTime;
                    config.currentChip.asbStatus.JobEndTime = config.currentChip.asbStatus.JobChipInfo.JobEndTime;
                    config.currentChip.asbStatus.PrediagStallId = config.currentChip.asbStatus.JobChipInfo.PrediagStallId;
                    config.currentChip.asbStatus.PrediagStartTime = config.currentChip.asbStatus.JobChipInfo.PrediagStartTime;
                    // config.currentChip.asbStatus.SplitChips = config.currentChip.asbStatus.JobChipInfo.SplitChips;

                }

                config.currentChip.asbStatus.JobChipInfo.durasi = config.currentChip.durasi;

                console.log("ASB Status - onShowBoard", config.currentChip.asbStatus);
                if ((typeof config.currentChip.asbStatus.JobStallId ==='undefined') || (config.currentChip.asbStatus.JobStallId === null)){
                    config.currentChip.asbStatus.JobStallId = 0;
                }
                if (config.currentChip.asbStatus.JobStallId==0){
                    config.currentChip.asbStatus.JobStartTime = null;
                    config.currentChip.asbStatus.JobEndTime = null;
                    tmpDurasi=0;
                }

                if ((typeof config.currentChip.asbStatus.PrediagStallId ==='undefined') || (config.currentChip.asbStatus.PrediagStallId === null)){
                    config.currentChip.asbStatus.PrediagStallId = 0;
                }

                if ((config.currentChip.asbStatus.PrediagStallId == 0) ){
                    config.currentChip.asbStatus.PrediagStartTime = null;
                    config.currentChip.asbStatus.PrediagEndTime = null;
                }

                var vdurasi = 1;
                if (typeof config.currentChip.durasi !=='undefined'){
                    vdurasi = config.currentChip.durasi;
                }
                config.currentChip.width = (vdurasi/60)*this.incrementMinute;

                var vrow = -2;
                // if (typeof config.currentChip.stallId !=='undefined'){
                //     if (config.currentChip.stallId>0){
                //         vrow = config.currentChip.stallId - 1 ;
                //     }
                // }
                config.currentChip.row = vrow;

                var vhourStart = JsBoard.Common.getMinute(this.startHour)/60;
                var vcol = 0;
                // if (typeof config.currentChip.startTime !=='undefined'){
                //     vstartTime = config.currentChip.startTime;
                //     vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                // }
                // config.currentChip.col = vcol;

                // if (config.currentChip.asbStatus.PrediagStallId>0){
                //     var vcol = 0;
                //     if (typeof config.currentChip.startTime !=='undefined'){
                //         vstartTime = config.currentChip.asbStatus.PrediagStartTime;
                //         vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                //     }
                //     config.currentChip.colPrediag = vcol;
                // } else {
                //     config.currentChip.colPrediag = config.currentChip.col+1;
                // }

                if (config.currentChip.asbStatus.JobStallId!=0){
                    vstartTime = config.currentChip.asbStatus.JobStartTime;
                    vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                }
                config.currentChip.col = vcol;

                if (config.currentChip.asbStatus.PrediagStallId>0){
                    var vcol = 0;
                    if (typeof config.currentChip.startTime !=='undefined'){
                        vstartTime = config.currentChip.asbStatus.PrediagStartTime;
                        vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                    }
                    config.currentChip.colPrediag = vcol;
                } else {
                    config.currentChip.colPrediag = config.currentChip.col+1;
                }
                ///

                jpcbFactory.createBoard(vname, {
                    board: {
                        chipGroup: 'asb_gr',
                        columnWidth: 120,
                        rowHeight: 80,
                        footerChipName: 'footer',
                        snapDivider: 4,
                    },
                    chip: {
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute
                    },
                    standardDraggable: false
                });
                var me = this;
                jpcbFactory.setHeader(vname, {
                    headerHeight: 40,
                    onGetHeader: function(arg, vfunc){
                        return me.onGetHeader(arg, vfunc);
                    }
                });
                jpcbFactory.setStall(vname, {
                    firstColumnWidth: 120,
                    onGetStall: function(arg, vfunc){
                        return me.onGetStall(arg, vfunc);
                    }
                });
                var vxConfig = jpcbFactory.boardConfigList[vname];
                vxConfig.JobChipInfo = config.currentChip.asbStatus.JobChipInfo;

                jpcbFactory.setOnGetChips(vname, function(arg, vfunc){
                    return me.onGetChips(arg, vfunc);
                });

                jpcbFactory.setOnChipLoaded(vname, function(arg, vfunc){
                    return me.onChipLoaded(arg, vfunc);
                });

                jpcbFactory.setSelectionArea(vname, {tinggiArea: 110});

                config.currentChip.boardName = vname;
                jpcbFactory.setArgChips(vname, config.currentChip);
                jpcbFactory.setArgStall(vname, config.currentChip);

                // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

                var me = this;
                jpcbFactory.setOnCreateChips(vname, function(vchip){
                    if (!vchip.data.isInfo){
                        vchip.on('click', me.onChipClick);
                    }
                    if (vchip.data.width<0.25){
                        vchip.data.isDraggable = false;
                    } else {
                        vchip.data.isDraggable = true;
                    }

                    if (vchip.data.selected){
                        if (vchip.data.width<0.25){
                            vchip.draggable(false);
                        } else {
                            vchip.draggable(true);
                            // vchip.draggable(false);
                            if (vchip.data.intStatus >= 5 && vchip.data.intStatus <= 8 && vchip.data.row != -2){
                                vchip.draggable(false)
                            }
                        }

                    }
                    if (vchip.data.isInfo){
                        vchip.data.isDraggable = false;
                        // vchip.draggable(false);
                    }
                });


                var the_config = config;
                jpcbFactory.setOnDragDrop(vname, function(vchip){
                    console.log("the_config.currentChip.asbStatus",the_config.currentChip,vchip);

                    // menentukan startDateTime, endDateTime, dan stallid
                    var curDate = new Date (config.currentChip.currentDate);
                    var curDate2 = new Date (config.currentChip.currentDate);
                    var vdurasi = 1;
                    // if (typeof config.currentChip.durasi !=='undefined'){
                    //     vdurasi = config.currentChip.durasi;
                    // }
                    if (vchip.data.isPrediagnose){
                        vdurasi = 1;
                    } else {
                        vdurasi = vchip.data.normal;
                    }
                    var vmin = me.incrementMinute * vchip.data['col'];
                    var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                    var vres = JsBoard.Common.getTimeString(vminhour);
                    var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+vres);
                    //startTime.setMinutes(startTime.getMinutes()+vmin);
                    //startTime.setMinutes(vminhour);

                    var emin = vminhour + vdurasi * 60;
                    var x24 = 24*60;
                    if (emin>x24){
                        var dmin = Math.floor(emin/x24);
                        console.log("emin=====>",dmin);
                        curDate.setDate(curDate.getDate()+dmin);
                        var emin = emin % x24;
                    }
                    if(vchip.data.width2 !== undefined){
                        emin = emin + 60;
                        console.log("emiinnnnnn",emin);
                    }
                    var eres = JsBoard.Common.getTimeString(emin);
                    console.log("eres=====>",eres, emin);
                    
                    var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                    //endTime.setMinutes(endTime.getMinutes()+emin);
                    
                    var vStallId = 0;
                    if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                        && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                        && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                        vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                    }

                    // hasil akhir di variabel startTime, endTime, dan vStallId
                    //

                    if (vchip.data.isPrediagnose){
                        the_config.currentChip.asbStatus.PrediagStallId = vStallId;
                        the_config.currentChip.asbStatus.PrediagStartTime = startTime;
                        the_config.currentChip.asbStatus.PrediagEndTime = endTime;
                    } else {
                        the_config.currentChip.asbStatus.JobStallId = vStallId;
                        the_config.currentChip.asbStatus.JobStartTime = startTime;
                        the_config.currentChip.asbStatus.JobEndTime = endTime;
                    }

                    // cek status
                    var pstatus;
                    // 0: tak ada yang diplot, 1: job item saja, 2: prediagnose saja, 3: job & prediagnose
                    if (the_config.currentChip.durasi<=0){
                        //if (angular.equals({}, the_config.currentChip.preDiagnose)){
                        // if (!(the_config.currentChip.preDiagnose.FrequencyIncident || the_config.currentChip.preDiagnose.Mil ||
                        //     the_config.currentChip.preDiagnose.ConditionIndication || the_config.currentChip.preDiagnose.MachineCondition)){
                        if (!me.isDataPrediagnose(the_config.currentChip.preDiagnose)){
                            pstatus = 0;
                        } else {
                            pstatus = 2;
                        }
                    } else {
                        if (!me.isDataPrediagnose(the_config.currentChip.preDiagnose)){
                            pstatus = 1;
                        } else {
                            pstatus = 3;
                        }
                    }

                    // if (the_config.currentChip.asbStatus.JobStallId==0){

                    //     if (the_config.currentChip.asbStatus.PrediagStallId==0){
                    //         pstatus = 0;
                    //     } else {
                    //         pstatus = 2;
                    //     }
                    // } else {
                    //     if (the_config.currentChip.asbStatus.PrediagStallId==0){
                    //         pstatus = 1;
                    //     } else {
                    //         pstatus = 3;
                    //     }
                    // }

                    vchip.data.StatusPreDiagnose = pstatus;
                    if (pstatus==0){
                        // tidak ada yang diplot
                        vchip.data.startTime = null;
                        vchip.data.endTime = null;
                        vchip.data.stallId = 0;
                        vchip.data.PrediagStallId = null;
                        vchip.data.PrediagScheduledTime = null;
                    } else if (pstatus==1){
                        // yang diplot job item
                        vchip.data.startTime = the_config.currentChip.asbStatus.JobStartTime;
                        vchip.data.endTime = the_config.currentChip.asbStatus.JobEndTime;
                        vchip.data.stallId = the_config.currentChip.asbStatus.JobStallId;
                        vchip.data.PrediagStallId = null;
                        vchip.data.PrediagScheduledTime = null;
                    } else if (pstatus==2){
                        vchip.data.startTime = the_config.currentChip.asbStatus.PrediagStartTime;
                        vchip.data.endTime = the_config.currentChip.asbStatus.PrediagEndTime;
                        vchip.data.stallId = the_config.currentChip.asbStatus.PrediagStallId;
                        vchip.data.PrediagStallId = the_config.currentChip.asbStatus.PrediagStallId;
                        vchip.data.PrediagScheduledTime = the_config.currentChip.asbStatus.PrediagStartTime;
                    } else {
                        // pstatus=3
                        vchip.data.startTime = the_config.currentChip.asbStatus.JobStartTime;
                        vchip.data.endTime = the_config.currentChip.asbStatus.JobEndTime;
                        vchip.data.stallId = the_config.currentChip.asbStatus.JobStallId;
                        vchip.data.PrediagStallId = the_config.currentChip.asbStatus.PrediagStallId;
                        vchip.data.PrediagScheduledTime = the_config.currentChip.asbStatus.PrediagStartTime;
                    }

                    var vxConfig = jpcbFactory.boardConfigList[vname];
                    if (vchip.data.isPrediagnose){
                        config.currentChip.asbStatus.JobChipInfo.PrediagStallId = vStallId;
                        config.currentChip.asbStatus.JobChipInfo.PrediagStartTime = startTime;
                        config.currentChip.asbStatus.JobChipInfo.PrediagEndTime = endTime;
                    } else {
                        config.currentChip.asbStatus.JobChipInfo.JobStallId = vStallId;
                        config.currentChip.asbStatus.JobChipInfo.JobStartTime = startTime;
                        config.currentChip.asbStatus.JobChipInfo.JobEndTime = endTime;
                    }
                    // ===== Commented =====
                    // if (!vchip.data.isPrediagnose){
                    //     // if (vchip.data.row==-2){
                    //     //     // vxConfig.JobChipInfo.data.row = -3;
                    //     // } else {
                    //     //     //me.calculateCarryOver();

                    //         var xinfo = me.getSplitInfo(vxConfig);
                    //         var vxTgl = $filter('date')(config.currentChip.currentDate, 'yyyy-MM-dd');
                    //         if (vStallId>0){
                    //             if(vxConfig.JobChipInfo.SplitChips[vxTgl] == undefined){
                    //                vxConfig.JobChipInfo.SplitChips[vxTgl] = {} 
                    //             }
                    //             // vxConfig.JobChipInfo.SplitChips[vxTgl] = {}
                    //             vxConfig.JobChipInfo.SplitChips[vxTgl].stallId = vStallId;
                    //             vxConfig.JobChipInfo.SplitChips[vxTgl].startTime = $filter('date')(startTime, 'HH:mm');
                    //             if(the_config.currentChip.asbStatus.JobChipInfo.SplitChips[vxTgl].JobSplitId !== undefined){
                    //                 vxConfig.JobChipInfo.SplitChips[vxTgl].JobSplitId = the_config.currentChip.asbStatus.JobChipInfo.SplitChips[vxTgl].JobSplitId
                    //             }
                    //             if (endTime<=startTime){
                    //                 //endTime = new Date($filter('date')(endTime, 'yyyy-MM-dd HH:mm:ss'));
                    //                 //endTime.setHours(endTime.getHours()+1);
                    //                 endTime.setTime(startTime.getTime());
                    //                 endTime.setHours(endTime.getHours()+1);
                    //             }
                    //             vxEndTime = $filter('date')(endTime, 'yyyy-MM-dd');
                    //             if (vxEndTime>vxTgl){
                    //                 var vLastHours = me.getLastHour();
                    //                 vxConfig.JobChipInfo.SplitChips[vxTgl].endTime = vLastHours;
                    //             } else {
                    //                 var vEndTime = $filter('date')(endTime, 'HH:mm');
                    //                 var vLastHours = me.getLastHour();
                    //                 if (vEndTime>vLastHours){
                    //                     vxConfig.JobChipInfo.SplitChips[vxTgl].endTime = vLastHours;
                    //                 } else {
                    //                     vxConfig.JobChipInfo.SplitChips[vxTgl].endTime = vEndTime;
                    //                 }
                    //             }
                    //             var xStart = JsBoard.Common.getMinute(vxConfig.JobChipInfo.SplitChips[vxTgl].startTime);
                    //             var xEnd = JsBoard.Common.getMinute(vxConfig.JobChipInfo.SplitChips[vxTgl].endTime);
                    //             console.log("(xEnd-xStart)/60 ===>",(xEnd-xStart)/60);
                    //             vxConfig.JobChipInfo.SplitChips[vxTgl].durasi = (xEnd-xStart)/60;

                    //         } else {
                    //             delete vxConfig.JobChipInfo.SplitChips[vxTgl];
                    //             var vinfo = me.getSplitInfo(vxConfig);
                    //             vchip.data.width = vinfo.sisaDurasi;
                    //         }
                    //         // me.updatePlottingInfo(vxConfig, vxConfig.JobChipInfo.data);

                    //         // var vEndTime = $filter('date')(vchip.data.endTime, 'HH:mm');
                    //         // var vLastHours = me.getLastHour();
                    //         // if (vEndTime>vLastHours){
                    //         //     vxConfig.JobChipInfo.data.row = -2;
                    //         // } else {
                    //         //     vxConfig.JobChipInfo.data.row = -3;
                    //         // }
                    //     // }
                    // }

                    console.log('onPlot', me.onPlot);

                    if (typeof me.onPlot=='function'){
                        delete vchip.data.start1;
                        delete vchip.data.width1;
                        delete vchip.data.start2;
                        delete vchip.data.width2;
                        delete vchip.data.start3;
                        delete vchip.data.width3;
                        // if (vchip.data.row<0){
                        //     vchip.data.stallId = 0;
                        //     //delete vchip.data.breakWidth;
                        // }
                        me.updateChipBreak(vchip.data);
                        //================================
                        if (!vchip.data.isPrediagnose){
                                var vxTgl = $filter('date')(config.currentChip.currentDate, 'yyyy-MM-dd');
                                if (vStallId>0){
                                    if(vxConfig.JobChipInfo.SplitChips[vxTgl] == undefined){
                                       vxConfig.JobChipInfo.SplitChips[vxTgl] = {} 
                                    }
                                    vxConfig.JobChipInfo.SplitChips[vxTgl].stallId = vStallId;
                                    vxConfig.JobChipInfo.SplitChips[vxTgl].startTime = $filter('date')(startTime, 'HH:mm');
                                    console.log("BABIIII --===>",the_config.currentChip.asbStatus.JobChipInfo.SplitChips[vxTgl],the_config.currentChip.asbStatus);
                                    
                                    for(var idx in the_config.currentChip.asbStatus.SplitChips){
                                        if(the_config.currentChip.asbStatus.SplitChips[idx].isFI !== undefined && the_config.currentChip.asbStatus.SplitChips[idx].isFI == 0 ){
                                            vxConfig.JobChipInfo.SplitChips[vxTgl].JobSplitId = the_config.currentChip.asbStatus.SplitChips[idx].JobSplitId;
                                        }
                                    }
                                    // if(the_config.currentChip.asbStatus.JobChipInfo.SplitChips[vxTgl].JobSplitId !== undefined){
                                    //     vxConfig.JobChipInfo.SplitChips[vxTgl].JobSplitId = the_config.currentChip.asbStatus.JobChipInfo.SplitChips[vxTgl].JobSplitId
                                    // }
                                    if (endTime<=startTime){
                                        endTime.setTime(startTime.getTime());
                                        endTime.setHours(endTime.getHours()+1);
                                    }
                                    vxEndTime = $filter('date')(endTime, 'yyyy-MM-dd');
                                    console.log("vxEndTime>vxTgl",vxEndTime,vxTgl);
                                    if (vxEndTime>vxTgl){
                                        var vLastHours = me.getLastHour();
                                        console.log("vxEndTime>vxTgl",vLastHours);
                                        vxConfig.JobChipInfo.SplitChips[vxTgl].endTime = vLastHours;
                                    } else {
                                        var vEndTime = $filter('date')(endTime, 'HH:mm');
                                        var vLastHours = me.getLastHour();
                                        if (vEndTime>vLastHours){
                                            vxConfig.JobChipInfo.SplitChips[vxTgl].endTime = vLastHours;
                                        } else {
                                            vxConfig.JobChipInfo.SplitChips[vxTgl].endTime = vEndTime;
                                        }
                                    }
                                    var xStart = JsBoard.Common.getMinute(vxConfig.JobChipInfo.SplitChips[vxTgl].startTime);
                                    var xEnd = JsBoard.Common.getMinute(vxConfig.JobChipInfo.SplitChips[vxTgl].endTime);
                                    var totDurasi = (xEnd-xStart)/60;
                                    // Commentted
                                    var xItem = me.checkTime(vchip.data.endTime,vchip.data.startTime);
                                    // var xItem = true;
                                    console.log("xItem ====>",xItem,vchip.data.endTime,vchip.data.width2)
                                    if(vchip.data.width2 !== undefined && xItem ){
                                        totDurasi -= 1;
                                    }
                                    vxConfig.JobChipInfo.SplitChips[vxTgl].durasi = totDurasi;
                                    // ======== NEW CODE ==
                                    var vinfo = me.getSplitInfo(vxConfig);
                                    console.log("INI DONG NYET ===>", vinfo.sisaDurasi, emin, vxConfig.JobChipInfo.SplitChips[vxTgl]);
                                    if(vinfo.sisaDurasi == 0 && (emin == 0 || emin == 1440) ){
                                        vxConfig.JobChipInfo.SplitChips[vxTgl].endTime = "23:59";
                                    }
                                    //===============================
    
                                } else {
                                    delete vxConfig.JobChipInfo.SplitChips[vxTgl];
                                    var vinfo = me.getSplitInfo(vxConfig,undefined,vchip.data);
                                    console.log("infooooo,",vinfo);
                                    // if(me.isEmpty(vxConfig.JobChipInfo.SplitChips  )){
                                        vchip.data.normal = vinfo.sisaDurasi
                                    // }
                                    vchip.data.width = vinfo.sisaDurasi;
                                }
                        }
                        // ===============================
                        var vmin = me.incrementMinute * vchip.data['col'];
                        var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                        var emin = vminhour + vdurasi * 60;
                        var x24 = 24*60;
                        if(vchip.data.width2 !== undefined){
                            emin = emin + 60;
                            console.log("eminnn MASUK==>",emin);
                        }
                        if (emin>=x24){
                            var dmin = Math.floor(emin/x24);
                            curDate.setDate(curDate.getDate()+dmin);
                            var emin = emin % x24;
                            console.log("eminnn dalam if==>",emin);
                        }
                        console.log("eminnn==>",emin);
                        var eres = JsBoard.Common.getTimeString(emin);
                        console.log("eres===",eres);
                        // ===== Jika Dealer 24 Jam ====
                        var vinfo = me.getSplitInfo(vxConfig);
                        console.log('vinfooo',vinfo);
                        if(vinfo.sisaDurasi == 0 && (emin == 0 || emin == 1440) ){
                            eres = "23:59";
                            curDate = curDate2;

                        }
                        //===============================
                        var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                        console.log("endTime =====>",endTime);
                        vchip.data.endTime = endTime
                        config.currentChip.asbStatus.JobChipInfo.JobEndTime = endTime;
                        // ===============================
                        me.updatePlottingInfo(vxConfig, vxConfig.JobChipInfo.data, vchip);
                        var vboard = jpcbFactory.getBoard(me.boardName);
                        me.onPlot(vchip.data);
                        vboard.chipItems[vchip.data.dataIndex] = vchip.data;
                        if (vboard.board.selectbox){
                            vboard.board.selectbox.destroyChildren();
                        }
                        vboard.redrawChips();
                    }


                    // vchip.data.startTime = startTime;
                    // vchip.data.endTime = endTime;
                    // vchip.data.stallId = vStallId;
                    //





                    // if (vchip.data.isPrediagnose){
                    //     var curDate = config.currentChip.currentDate;
                    //     var vdurasi = 1;
                    //     if (typeof config.currentChip.durasi !=='undefined'){
                    //         vdurasi = config.currentChip.durasi;
                    //     }

                    //     var vmin = me.incrementMinute * vchip.data['col'];
                    //     var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                    //     var vres = JsBoard.Common.getTimeString(vminhour);
                    //     var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+vres);
                    //     //startTime.setMinutes(startTime.getMinutes()+vmin);
                    //     //startTime.setMinutes(vminhour);

                    //     var emin = vminhour + vdurasi * 60;
                    //     var eres = JsBoard.Common.getTimeString(emin);
                    //     var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                    //     //endTime.setMinutes(endTime.getMinutes()+emin);

                    //     vchip.data.startTime = startTime;
                    //     vchip.data.endTime = endTime;
                    //     var vStallId = 0;
                    //     if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                    //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                    //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                    //         vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                    //     }

                    //     the_config.currentChip.PrediagStallId = vStallId;


                    //     vchip.data.stallId = vStallId;
                    //     if (vStallId===0){
                    //         vchip.data.startTime = null;
                    //         vchip.data.endTime = null;
                    //     }
                    //     console.log("on plot data", vchip.data);
                    //     the_config.currentChip.PrediagStallId = vStallId;
                    //     var pstatus = 0;
                    //     if (the_config.currentChip.JobStallId){
                    //         if (the_config.currentChip.PrediagStallId) {
                    //             pstatus = 3
                    //         } else {
                    //             pstatus = 1;
                    //         }
                    //     } else {
                    //         if (the_config.currentChip.PrediagStallId) {
                    //             pstatus = 2
                    //         }
                    //     }
                    //     vchip.data.StatusPreDiagnose = pstatus;
                    //     vchip.data.PrediagStallId = vStallId;
                    //     vchip.data.PrediagScheduledTime = vchip.data.startTime;
                    //     if (typeof me.onPlot=='function'){
                    //         me.onPlot(vchip.data);
                    //     }
                    //     console.log("on drag-drop vchip", vchip);
                    // } else {

                    //     var curDate = config.currentChip.currentDate;
                    //     var vdurasi = 1;
                    //     if (typeof config.currentChip.durasi !=='undefined'){
                    //         vdurasi = config.currentChip.durasi;
                    //     }

                    //     var vmin = me.incrementMinute * vchip.data['col'];
                    //     var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                    //     var vres = JsBoard.Common.getTimeString(vminhour);
                    //     var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+vres);
                    //     //startTime.setMinutes(startTime.getMinutes()+vmin);
                    //     //startTime.setMinutes(vminhour);

                    //     var emin = vminhour + vdurasi * 60;
                    //     var eres = JsBoard.Common.getTimeString(emin);
                    //     var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                    //     //endTime.setMinutes(endTime.getMinutes()+emin);

                    //     vchip.data.startTime = startTime;
                    //     vchip.data.endTime = endTime;
                    //     var vStallId = 0;
                    //     if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                    //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                    //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                    //         vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                    //     }
                    //     vchip.data.stallId = vStallId;
                    //     if (vStallId===0){
                    //         vchip.data.startTime = null;
                    //         vchip.data.endTime = null;
                    //     }
                    //     console.log("on plot data", vchip.data);
                    //     the_config.currentChip.JobStallId = vStallId;
                    //     var pstatus = 0;
                    //     if (the_config.currentChip.JobStallId){
                    //         if (the_config.currentChip.PrediagStallId) {
                    //             pstatus = 3
                    //         } else {
                    //             pstatus = 1;
                    //         }
                    //     } else {
                    //         if (the_config.currentChip.PrediagStallId) {
                    //             pstatus = 2
                    //         }
                    //     }
                    //     vchip.data.StatusPreDiagnose = pstatus;
                    //     if (typeof me.onPlot=='function'){
                    //         me.onPlot(vchip.data);
                    //     }
                    //     console.log("on drag-drop vchip", vchip);
                    // }


                });


                // jpcbFactory.setOnCreateChips(vname, function(vchip){
                //     vchip.on('click', me.onChipClick);
                // });

                jpcbFactory.setOnBoardReady(vname, function(vconfig){
                    console.log('board ready', vconfig);
                    vconfig.theBoard.layout.board.checkDragDrop = function(vchip, vcol, vrow){
                        // var vbreakstart = me.timeToColumn('12:00:00');
                        // var vbreakend = me.timeToColumn('13:00:00');
                        var vcolstart = vcol;
                        var vcolend = vcol+vchip.data.width;
                        var vbreak = me.getBreakTime();
                        console.log(vbreak);
                        for (var i=0; i<vbreak.length; i++){
                            vbreakstart = vbreak[i].start;
                            vbreakend = vbreak[i].end;
                            var vjamstart = JpcbGrViewFactory.columnToTime(vcol);
                            var splitJam = vjamstart.split(':');
                            if (splitJam[0] == '12'){
                                return null; // ini kalau dia di taro di jam 12 - 13. ga boleh di jam istirahat
                            }
                            if (me.isOverlapped3(vcolstart, vcolstart, vbreakstart, vbreakend)){
                                return false;
                            }
                        }


                        var vlast = me.getLastHour()+':00';
                        var vlastCol = me.timeToColumn(vlast);
                        if (vcol>=vlastCol){
                            return false;
                        }

                        if (vchip.data.isPrediagnose){
                            if (vconfig){
                                if ((typeof vconfig.theBoard.layout.infoRows[vrow]!=='undefined') && (vconfig.theBoard.layout.infoRows[vrow].data.type!=4)){
                                    return false;
                                }

                            }
                        } else {
                            if (vconfig){
                                if ((typeof vconfig.theBoard.layout.infoRows[vrow]!=='undefined') && (vconfig.theBoard.layout.infoRows[vrow].data.type==4)){
                                    return false;
                                }

                            }

                        }
                        if (typeof vconfig.theBoard.layout.cekMenumpuk == 'function'){
                            var xBreak = 0;
                            if (vchip.data.breakWidth){
                                vchip.attrs.minBreakWidth = vchip.data.breakWidth*vconfig.board.columnWidth;
                            } else {
                                vchip.attrs.minBreakWidth = 0;
                            }
                            var vbreaks = me.getBreakChipTime(vcol, vchip.data.width);
                            if (vbreaks.breakWidth){
                                vchip.attrs.breakWidth = vbreaks.breakWidth*vconfig.board.columnWidth;
                                vx = vconfig.theBoard.layout.col2x(vcol);
                                vy = vconfig.theBoard.layout.row2y(vrow);
                                if (vconfig.theBoard.layout.cekMenumpuk(vchip, vx, vy)){
                                    return false;
                                }
                            }

                        }
                        return true;
                    }

                    //me.updateTimeLine(vname);
                    var vrnd = me.getIntRandom();
                    me.timerCode = vrnd;
                    me.timerLoop(vname, vrnd);


                });

                jpcbFactory.showBoard(vname, vcontainer)
                // $timeout(function(){
                //     jpcbFactory.showBoard(vname, vcontainer)

                // }, 100);

            },

            dialogOpen: false,
            onChipClick: function(vchip){
                var xme = this;
                console.log('data', vchip.currentTarget.data);
                $rootScope.jpcbMsg = vchip.currentTarget.data;
                $rootScope.$on('ngDialog.closing', function () {
                    xme.dialogOpen = false;
                });
                if (!xme.dialogOpen){
                    xme.dialogOpen = true;
                    ngDialog.openConfirm ({
                        width: 400,
                        template: '<div class="panel">'+
                        '    <div class="panel-heading">'+
                        '        <h3 class="panel-title">{{jpcbMsg.nopol}} / {{jpcbMsg.tipe}}</h3></div>'+
                        '    <div class="panel-body">'+
                        '	<table>'+
                        '	  <tr>'+
                        '		<td>Jam Alokasi &nbsp;</td>'+
                        '		<td>{{jpcbMsg.PlanStart}} - {{jpcbMsg.PlanFinish}}</td>'+
                        '	  </tr>'+
                        '	  <tr>'+
                        '		<td>Pekerjaan</td>'+
                        '		<td>{{jpcbMsg.pekerjaan}}</td>'+
                        '	  </tr>'+
                        '	</table>'+
                        '    </div>'+
                        '    <br/>'+
                        '	<button type="button " class="ngdialog-button ngdialog-button-secondary " ng-click="closeThisDialog(0) ">OK</button>'+
                        '</div>',
                        plain: true,
                        scope: $rootScope,
                    })
                }


            },

            startHour: '08:00',
            // endHour: '17:00',
            // endHour: '22:00',
            endHour: tmpCloseTime,
            breakHour: '12:00',
            incrementMinute: 60,

            getFirstHour: function(){
                return this.startHour;
            },
            getLastHour: function(){
                // var vendmin = JsBoard.Common.getMinute(this.endHour);
                var vendmin = JsBoard.Common.getMinute(tmpCloseTime);
                vendmin = vendmin+60;
                return JsBoard.Common.getTimeString(vendmin);
            },

            timerCode: 0,
            getIntRandom: function(){
                return Math.floor(Math.random() * 100000);
            },
            timerLoop: function(vname, vcode){
                var vme = this;
                var vdate = $filter('date')(new Date(), 'HH:mm:ss');
                vme.updateTimeLine(vname);
                console.log('timerLoop', vdate, vcode);
                if (vme.timerCode==vcode){
                    $timeout( function(){
                        var vscode = vcode;
                        vme.timerLoop(vname, vscode);
                    }, 60000);
                }

            },
            getCurrentTime: function(){
                return jpcbFactory.getCurrentTime();
            },
            isProblem: function(vitem){
                return jpcbFactory.isJobProblem(vitem);
            },
            timeToColumn: function(vtime){
                var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vmin = JsBoard.Common.getMinute(vtime)-vstartmin;
                var vresult = vmin/this.incrementMinute;

                return vresult;
            },
            onGetHeader: function(arg, onFinish){
                varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
                console.log("this.endHour",tmpCloseTime);
                var vWorkHour = JsBoard.Common.generateDayHours({
                    startHour: this.startHour,
                    endHour: tmpCloseTime,
                    increment: this.incrementMinute,
                });
                var result = [];
                for(var idx in vWorkHour){
                    var vitem = {title: vWorkHour[idx]}
                    if (vWorkHour[idx]==this.breakHour){
                        vitem.colInfo = {isBreak: true}
                    }
                    result.push(vitem)

                }
                console.log("header", result);
                $timeout(function(){
                    onFinish(result);
                }, 100);

            },
            // onGetHeader: function(arg){
            //     varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
            //     var vWorkHour = JsBoard.Common.generateDayHours({
            //         startHour: this.startHour,
            //         endHour: this.endHour,
            //         increment: this.incrementMinute,
            //     });
            //     var result = [];
            //     for(var idx in vWorkHour){
            //         var vitem = {title: vWorkHour[idx]}
            //         if (vWorkHour[idx]==this.breakHour){
            //             vitem.colInfo = {isBreak: true}
            //         }
            //         result.push(vitem)

            //     }
            //     console.log("header", result);
            //     return result;
            // },
            onGetStall: function(arg, onFinish){
                // $http.get('/api/as/Stall').
                // var vparent = '0';
                var vparent = '1';
                if (arg.prediagnoseOnly){
                    vparent = '4';
                }
                $http.get('/api/as/Boards/Stall/'+vparent).then(function(res){
                    var xres = res.data.Result;
                    var result = [];
                    var vstart = '';
                    var vend = '';
                    var me = this;
                    for(var i=0;  i<xres.length; i++){
                        var vitem = xres[i];
                        // === NEW CODE Before GO LIVE
                        if (!vitem.DefaultOpenTime) {
                            vitem.DefaultOpenTime = '08:00';
                        }
                        if (typeof vitem.DefaultOpenTime !== 'undefined') {

                            //cek defaultopen time menit nya 00 ga? kl ga 00 set jd 00 biar header nya menit nya 00
                            var cekDefaultOpenTime = vitem.DefaultOpenTime.split(':');
                            if (cekDefaultOpenTime[1].toString() != '00'){
                                vitem.DefaultOpenTime = cekDefaultOpenTime[0] + ':' + '00' + ':' + '00'
                            }

                            // if (vitem.DefaultOpenTime){
                            if (vstart === '') {
                                vstart = vitem.DefaultOpenTime;
                            } else {
                                if (vstart > vitem.DefaultOpenTime) {
                                    vstart = vitem.DefaultOpenTime;
                                }
                            }
                            // }
                        }
                        if (vitem.DefaultCloseTime == undefined) {
                            vitem.DefaultCloseTime = '17:00';
                        }
                        if (typeof vitem.DefaultCloseTime !== 'undefined') {
                            if (vitem.DefaultCloseTime >= '23:59') {
                                vitem.DefaultCloseTime = '24:00';
                            }
                            if (vend === '') {
                                vend = vitem.DefaultCloseTime;
                            } else {
                                if (vend < vitem.DefaultCloseTime) {
                                    vend = vitem.DefaultCloseTime;
                                }
                            }
                        }
                        // =================
                        var vtech = '';
                            if (vitem.TeknisiName != null && vitem.TeknisiName != undefined && vitem.TeknisiName != '') {
                                vtech = vtech + vitem.TeknisiName;
                            }
                            if (vitem.Teknisi2Name != null && vitem.Teknisi2Name != undefined && vitem.Teknisi2Name != '') {
                                vtech = vtech + ', ' + vitem.Teknisi2Name;
                            }
                            if (vitem.Teknisi3Name != null && vitem.Teknisi3Name != undefined && vitem.Teknisi3Name != '') {
                                vtech = vtech + ', ' + vitem.Teknisi3Name;
                            }
                        result.push({
                            title: vitem.Name,
                            status: 'normal',
                            stallId: vitem.StallId,
                            type: vitem.Type,
                            startHour: vitem.DefaultOpenTime,
                            endHour: vitem.DefaultCloseTime,
                            // person:vitem.TeknisiName,
                            person: vtech,
                            // person1:'PErsonCUY1',
                            // person2:'PErsonCUY2',
                            // person3:'PErsonCUY3'
                        });
                    }
                    // =================
                    if (vstart === '') {
                        vstart = '08:00';
                    }
                    if (vend === '') {
                        vend = '17:00';
                    }
                    var vxmin = JsBoard.Common.getMinute(vend);
                    vend = JsBoard.Common.getTimeString(vxmin);
                    // me.startHour = vstart;
                    this.startHour = vstart;
                    this.endHour = vend;
                    tmpCloseTime = vend;
                    // =================
                    onFinish(result);
                })

                // var vres = [{title: 'Diagnose', status: 'normal', person: 'Joko'},
                //     {title: 'EM 1', status: 'normal', person: 'Ton'},
                //     {title: 'EM 2', status: 'normal', person: 'And'},
                //     {title: 'ST 1', status: 'normal', person: 'Rul'},
                //     {title: 'ST 2', status: 'normal', person: 'Fir'},
                //     {title: 'S3 3', status: 'normal', person: 'Ron'},
                // ]
                // $timeout(function(){
                //     onFinish(vres);
                // }, 100);
            },
            // onGetStall: function(arg){
            //     return[{title: 'Diagnose', status: 'normal', person: 'Joko'},
            //         {title: 'EM 1', status: 'normal', person: 'Ton'},
            //         {title: 'EM 2', status: 'normal', person: 'And'},
            //         {title: 'ST 1', status: 'normal', person: 'Rul'},
            //         {title: 'ST 2', status: 'normal', person: 'Fir'},
            //         {title: 'S3 3', status: 'normal', person: 'Ron'},
            //     ]
            // },
            findStallIndex: function(vStallId, vList){
                for (var i=0; i<vList.length; i++){
                    var vitem = vList[i];
                    if ((typeof vitem.stallId!=='undefined') && (vitem.stallId==vStallId)){
                        return i;
                    }
                }
                return -1;
            },
            isOverlapped: function(x1, x2, y1, y2){
                return (x1 < y2) && (y1 < x2)
            },
            isOverlapped2: function(x1, x2, y1, y2){
                return (x1 <= y2) && (y1 < x2)
            },
            isOverlapped3: function(x1, x2, y1, y2){
                return (x1 < y2) && (y1 <= x2)
            },
            getSplitInfo: function(vjpcb, vdata, vvchip){
                var result = {splitCount: 0, sisaDurasi: 0, totalDurasi: 0}
                if (vjpcb.JobChipInfo){
                    console.log('updatePlottingInfo: JobChipInfo', vjpcb.JobChipInfo);
                    console.log('getSplitInfo:', vjpcb);
                    var vSplitCount = 0;
                    // if (vjpcb.JobChipInfo.SplitChips){
                    //     vSplitCount = vjpcb.JobChipInfo.SplitChips.length;
                    // }
                    var vStart = '';
                    var vEnd = '';
                    var vidx = '';
                    var vdurasi = vjpcb.JobChipInfo.durasi;
                    var vtot = 0;
                    vMin = '';
                    vMax = '';
                    if (vjpcb.JobChipInfo.AllSplit.length == 0){
                        for (var vidx in vjpcb.JobChipInfo.SplitChips){
                            var vspchip = vjpcb.JobChipInfo.SplitChips[vidx];
                            vtot = vtot+vspchip.durasi;
                            vSplitCount = vSplitCount+1;
                            if (vMin==''){
                                vMin = vidx;
                            } else {
                                if (vMin>vidx){
                                    vMin = vidx;
                                }
                            }
                            if (vMax==''){
                                vMax = vidx;
                            } else {
                                if (vMax<vidx){
                                    vMax = vidx;
                                }
                            }
                        }
                    } else {
                        for (var d=0; d<vjpcb.JobChipInfo.AllSplit.length; d++){
                            jbstarttime = $filter('date')(vjpcb.JobChipInfo.JobStartTime, 'yyyy-MM-dd')
                            // if (vjpcb.JobChipInfo.AllSplit[d].tmpDate == jbstarttime){ // kaya nya ga perlu di patok hari, karena cari total panjang chip
                                vtot = vtot + vjpcb.JobChipInfo.AllSplit[d].durasi
                            // }
                            if (vMin==''){
                                vMin = vjpcb.JobChipInfo.AllSplit[d].tmpDate;
                            } else {
                                if (vMin>vjpcb.JobChipInfo.AllSplit[d].tmpDate){
                                    vMin = vjpcb.JobChipInfo.AllSplit[d].tmpDate;
                                }
                            }
                            if (vMax==''){
                                vMax = vjpcb.JobChipInfo.AllSplit[d].tmpDate;
                            } else {
                                if (vMax<vjpcb.JobChipInfo.AllSplit[d].tmpDate){
                                    vMax = vjpcb.JobChipInfo.AllSplit[d].tmpDate;
                                }
                            }
    
                        }
                    }
                    
                    
                    result.dateMin = vMin;
                    result.dateMax = vMax;
                    console.log("KENTEL",vdurasi,vtot);
                    // if (vjpcb.JobChipInfo.durasi){
                    //     if (vSplitCount==0){
                    //         vSplitCount = 1;
                    //     }
                    // } else {
                    //     vSplitCount = 0;
                    // }
                    // =========================
                    if(vdata !== undefined){
                        var xItem = this.checkTime(vdata.endTime,vdata.startTime);
                        var countSplit = 0;
                        console.log("masuk si   niii === ",xItem);
                        if(vdata.width2 !== undefined && vdata.width2 !== null && xItem ){
                            tmpDurasi = angular.copy(vdurasi);
                            // if(vSplitCount > 1){
                            //     tmpDurasi = tmpDurasi + vSplitCount;   
                            // }else{
                            //     tmpDurasi = tmpDurasi + 1 ;
                            // }
                            console.log("=============>",tmpDurasi,vdurasi,vtot);
                            // vtot = vtot+1;
                            if((tmpDurasi-vtot) <= 0){
                                result.sisaDurasi = 0;  
                            }else{
                                result.sisaDurasi = (tmpDurasi-vtot)
                            }
                        }else{
                            result.sisaDurasi = (vdurasi-vtot)
                        }
                    }else{
                        if(tmpDurasi > 0){
                            if((tmpDurasi-vtot) <= 0){
                                result.sisaDurasi = 0;
                                
                            }else{
                                result.sisaDurasi = (tmpDurasi-vtot)
                                console.log("malah keseini");
                            }
                        }else{
                            if((vdurasi-vtot) <= 0){
                                result.sisaDurasi = 0;  
                            }else{
                                result.sisaDurasi = (vdurasi-vtot)
                            }
                        }
                    }
                    // ======================
                    // if((vdurasi-vtot) <= 0){
                    //     result.sisaDurasi = 0;  
                    // }else{
                    //     result.sisaDurasi = (vdurasi-vtot)
                    // }
                    // =======================
                    // result.sisaDurasi = (vdurasi-vtot);
                    result.chipDurasi = vtot;
                    result.totalDurasi = vdurasi;
                    result.splitCount = vSplitCount;
                    if (vjpcb.JobChipInfo.AllSplit.length > 0){
                        result.splitCount = vjpcb.JobChipInfo.AllSplit.length;
                    }
                    console.log("resulttt dibalikiiinnn ===>",result);
                    return result;
                }
            },
            changeFormatDateStrip : function(item) {
                // console.log("itemmmm",item);
                if(item == undefined || item == null || item == ""){
                    return "";
                }
                var tmpItemDate = angular.copy(item);
                tmpItemDate = new Date(tmpItemDate);
                var finalDate = '';
                var yyyy = tmpItemDate.getFullYear().toString();
                var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpItemDate.getDate().toString();
                return finalDate += (dd[1] ? dd : "0" + dd[0])+'/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
            },
            changeFormatDatetoStrip : function(item) {
                // console.log("itemmmm changeFormatDatetoStrip",item);
                if(item == undefined || item == null || item == ""){
                    return "";
                }
                var tmpItemDate = angular.copy(item);
                tmpItemDate = new Date(tmpItemDate);
                // console.log("itemmmm changeFormatDatetoStrip",tmpItemDate)
                var finalDate = '';
                var yyyy = tmpItemDate.getFullYear().toString();
                var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpItemDate.getDate().toString();
                return finalDate += yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            },
            updatePlottingInfo: function(vjpcb, vitem, vdata){
                console.log("=====================,",vdata);
                if(vdata !== undefined &&  vdata !== null){
                    var vresult = this.getSplitInfo(vjpcb,vdata.data);
                    console.log("updateplotting info keseini", vresult);
                }else{
                    var vresult = this.getSplitInfo(vjpcb);
                }
                vitem.message1 = '';
                vitem.message2 = '';
                vitem.message3 = '';

                vitem.message1 = 'Jumlah Chip: '+vresult.splitCount;
                vitem.message2 = 'Sisa durasi (jam): '+(vresult.sisaDurasi)+' / total '+vresult.totalDurasi;
                if (vresult.splitCount>1){
                    vitem.message3 = 'Date: '+this.changeFormatDateStrip(vresult.dateMin)+' sd. '+this.changeFormatDateStrip(vresult.dateMax);
                } else {
                    vitem.message3 = 'Date: '+this.changeFormatDateStrip(vresult.dateMin);
                }
            },
            getInfoIsSplit: function(vboardName){
                var vxConfig = jpcbFactory.boardConfigList[vboardName];
                var vchipInfo = this.getSplitInfo(vxConfig);
                return vchipInfo;
            },
            getChipData: function(vboardName){
                var vxConfig = jpcbFactory.boardConfigList[vboardName];
                console.log("vxConfig.JobChipInfo", vxConfig.JobChipInfo);
                var vresult = '';
                var vchipInfo = this.getSplitInfo(vxConfig);
                console.log("vchipInfo",vchipInfo);
                if (vchipInfo.sisaDurasi==0){
                    var vlist = []
                    if (vxConfig.JobChipInfo.AllSplit.length == 0){
                        for (var vidx in vxConfig.JobChipInfo.SplitChips){
                            //var vchipItem = [vidx];
                            vlist.push(vidx);
                        }
                        if (vlist.length>0){
                            vlist.sort();
                            'PlanDate;TimeStart;TimeEnd;StallId;IsActive'
                            for (var i=0; i<vlist.length; i++){
                                vidx = vlist[i];
                                var vitem = vxConfig.JobChipInfo.SplitChips[vidx];
                                console.log("lolll",vitem);
                                if(vitem.JobSplitId == undefined){
                                    vitem.JobSplitId = 0;
                                }
                                var vrow = vidx+'|'+vitem.startTime+'|'+vitem.endTime+'|'+vitem.stallId+'|'+'1'+'|'+vitem.JobSplitId;
                                if(vresult!==''){
                                    vresult = vresult+';';
                                }
                                vresult = vresult+vrow;
                            }
    
                        }
                    } else {
                        for (var d=0; d<vxConfig.JobChipInfo.AllSplit.length; d++){
                            if (vxConfig.JobChipInfo.AllSplit[d].JobSplitId == undefined){
                                vxConfig.JobChipInfo.AllSplit[d].JobSplitId = 0
                            }
                            if (vxConfig.JobChipInfo.AllSplit[d].isSplitActive === null || vxConfig.JobChipInfo.AllSplit[d].isSplitActive === undefined || vxConfig.JobChipInfo.AllSplit[d].isSplitActive === ''){
                                vxConfig.JobChipInfo.AllSplit[d].isSplitActive = '1'
                            }
                            var itm = vxConfig.JobChipInfo.AllSplit[d]
                            var vrow = itm.tmpDate+'|'+itm.startTime+'|'+itm.endTime+'|'+itm.stallId+'|'+itm.isSplitActive+'|'+itm.JobSplitId;
                            if(vresult!==''){
                                vresult = vresult+';';
                            }
                            vresult = vresult+vrow;

                        }

                    }
                    


                }
                console.log('GetChipData result', vresult);
                return vresult;
            },
            checkChipFinished: function(vboardName){
                var vxConfig = jpcbFactory.boardConfigList[vboardName];
                console.log("vxConfig.JobChipInfo", vxConfig.JobChipInfo);
                var vinfo = this.getSplitInfo(vxConfig);
                if (vinfo.sisaDurasi==0){
                    return 1;
                }
                return 0;
            },
            onChipLoaded: function(vjpcb){
                // loaded ASB
                // console.log('load chip boi', vjpcb.allChips)
                var xresult = []
                for (var i=0; i<vjpcb.allChips.length; i++){
                    var vitem = vjpcb.allChips[i];
                    vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                    var vvisible = true;
                    if (vidx>=0){
                        vitem.row = vidx;
                    } else {
                        if (vitem.selected || vitem.isInfo){
                            vitem.stallId = 0;
                        }
                        vvisible = false;
                    }
                    if (vitem.stallId==0 && vitem.selected){
                        vvisible = true;
                    }
                    if (vitem.stallId==0 && vitem.isInfo){
                        vvisible = true; //vitem.visible;
                        vitem.row = vitem.infoRow;
                        this.updatePlottingInfo(vjpcb, vitem);
                    }
                    //vitem.normal = vitem.width;
                    vitem.ori_width = vitem.width;
                    var vext = 0;
                    if (typeof vitem.ext!=='undefined'){
                        vext = vitem.ext;
                    }
                    var xend = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vitem.PlanFinish)+vext*60)+':00';
                    if (this.isOverlapped(vitem.PlanStart, xend, '12:00:00', '13:00:00')){
                        // vitem.width = vitem.width+1;
                    }
                    if (vvisible){
                        xresult.push(vitem);
                    }
                }
                vjpcb.allChips = xresult;
            },
            isDataPrediagnose: function(vpred){
                if (typeof vpred == 'undefined'){
                    return false;
                }
                var vList = ['FrequencyIncident', 'Mil', 'ConditionIndication', 'MachineCondition', 'FirstIndication',
                    'PositionShiftLever', 'IsNeedTest'];
                for (var i=0; i<vList.length; i++){
                    var vitem = vList[i];
                    if (vpred[vitem]){
                        return true;
                    }
                }
                return false;
            },
            isCarryOver: function(vArg, vJobStallId){
                if (vJobStallId>0){
                    return true;
                }
                return false;
            },
            timeToColumn2: function(vtime) {
                var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vmin = JsBoard.Common.getMinute(vtime) - vstartmin;
                var vresult = vmin / this.incrementMinute;

                return vresult;
            },
            //JobChipInfo: {data: {}},
            onGetChips: function(arg, onFinish){
                var vArg = {nopol: 'B 1000 AAA', tipe: 'Avanza'}
                vArg = JsBoard.Common.extend(vArg, arg);

                var vboardName = vArg.boardName;
                var vxConfig = jpcbFactory.boardConfigList[vboardName];

                console.log("ASB Status - onGetChips", vArg.asbStatus);
                var tglAwal = vArg.currentDate;
                var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
                tglAkhir.setDate(tglAkhir.getDate());
                // var vurl = '/api/as/Boards/Appointment/1/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/'+$filter('date')(tglAkhir, 'yyyy-MM-dd');
                var vMinMaxStatus = '/0/17';
                var vurl = '/api/as/Boards/jpcb/bydate/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+vMinMaxStatus;
                var me = this;
                $http.get(vurl).then(function(res){
                    // console.log('getchip boi', res)
                    // res.data = xTempData2;
                    var result = [];
                    var vJobStallId = vArg.stallId;
                    if ((typeof vArg.asbStatus !=='undefined') && (typeof vArg.asbStatus.JobStallId !=='undefined') ){
                        vJobStallId = vArg.asbStatus.JobStallId
                    }
                    var vcurdate = $filter('date')(vArg.currentDate, 'yyyy-MM-dd');

                    var vPrediagStallId;
                    if ((typeof vArg.asbStatus !=='undefined') && (typeof vArg.asbStatus.JobStallId !=='undefined') ){
                        vPrediagStallId = vArg.asbStatus.PrediagStallId;
                    }

                    var vInfoRow = -2;
                    // if (me.isCarryOver(vArg, vJobStallId)){
                    //     vInfoRow = -2;
                    // }
                    var vesdata = {chip: 'estimasi2', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: -1,
                        width: 1.5, selected: 0, isInfo: 1, infoRow: vInfoRow,
                        stallId: 0, xxstallId: vArg.stallId, normal: vArg.width,
                        daydiff: 0, status: 'appointment', statusicon: [],
                        title: 'Plotting Info', message: 'Carry Over 0 hours',
                        visible: false, isDraggable: false};
                    vesdata.width1 = vesdata.width;
                    //vxConfig.JobChipInfo = {data: {}}
                    vxConfig.JobChipInfo.data = vesdata;
                    // me.JobChipInfo.data = vesdata;
                    result.push(vesdata);



                    if (!(typeof vArg.visible !== 'undefined' && !vArg.visible) && (vArg.durasi>0)) {
                        // var vjobdate = $filter('date')(vArg.asbStatus.JobStartTime, 'yyyy-MM-dd');
                        // if (vcurdate==vjobdate || vJobStallId==0){
                        //     var vesdata = {chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1,
                        //     stallId: vJobStallId, xxstallId: vArg.stallId, normal: vArg.width,
                        //     daydiff: 0, status: 'appointment', statusicon: ['customer_gray']};
                        //     vesdata.width1 = vesdata.width;
                        //     if(vesdata.stallId>0){
                        //         me.updateChipBreak(vesdata);
                        //     }
                        //     result.push(vesdata);
                        // }

                        var vdurasi = vArg.durasi;
                        // var vjobdate = $filter('date')(vArg.asbStatus.JobStartTime, 'yyyy-MM-dd');
                        var vjobdate = $filter('date')(vArg.currentDate, 'yyyy-MM-dd');
                        var vsplitInfo = me.getSplitInfo(vxConfig);
                        var vJobSelect = false;
                        var vrow = 0;
                        if (typeof vxConfig.JobChipInfo.SplitChips[vjobdate] !== 'undefined'){
                            // if(vxConfig.JobChipInfo.SplitChips[vjobdate].isFI == 0){
                            //     vJobStallId = vxConfig.JobChipInfo.SplitChips[vjobdate].stallId;
                            //     vdurasi = vxConfig.JobChipInfo.SplitChips[vjobdate].durasi;
                            //     vcol = me.timeToColumn(vxConfig.JobChipInfo.SplitChips[vjobdate].startTime);
                            //     vJobSelect = true;    
                            // }
                            if (vxConfig.JobChipInfo.SplitChips[vjobdate].IsInJobStopageGR != 1){
                                console.log(' INI DUDE 1 ======>', vxConfig.JobChipInfo.SplitChips);
                                vJobStallId = vxConfig.JobChipInfo.SplitChips[vjobdate].stallId;
                                vdurasi = vxConfig.JobChipInfo.SplitChips[vjobdate].durasi;
                                vcol = me.timeToColumn(vxConfig.JobChipInfo.SplitChips[vjobdate].startTime);
                                vJobSelect = true;  
                            } else {
                                // ini sama aj kaya atas nya, cm mau di cb kl hasil split di taro di stopage biar dia naik ke atas aja maka na stall id di buat 0 dan row nya minus
                                console.log(' INI DUDE 1 ======>', vxConfig.JobChipInfo.SplitChips);
                                vJobStallId = 0
                                vrow = -2;
                                vdurasi = vxConfig.JobChipInfo.SplitChips[vjobdate].durasi;
                                vcol = me.timeToColumn(vxConfig.JobChipInfo.SplitChips[vjobdate].startTime);
                                vJobSelect = true;  
                            }
                            

                        } else {
                            vJobStallId = 0;
                            vdurasi = vsplitInfo.sisaDurasi;
                            vcol = 0;
                            vrow = -2;
                            if (vdurasi>=0.25){
                                vJobSelect = true;
                            }
                        }
                        if (vJobSelect){

                            // //  ============= untuk case carry over, cek data tgl board nya dengan data tarikan dr be. jika ada set stallidnya coba ======= start
                            // var tglBoardnya = $filter('date')(vArg.currentDate, 'yyyy-MM-dd');
                            // var tglAwalChipnya = $filter('date')(vArg.asbStatus.JobStartTime, 'yyyy-MM-dd');

                            // if (tglBoardnya != tglAwalChipnya){
                            //     // harus nya ini masuk carry over krn uda beda hari
                            //     for (var i=0; i<vArg.asbStatus.SplitChips.length; i++){
                            //         if (tglBoardnya == vArg.asbStatus.SplitChips[i].tmpDate){
                            //             vJobStallId = vArg.asbStatus.SplitChips[i].stallId
                            //             vcol = me.timeToColumn(vArg.asbStatus.SplitChips[i].startTime);
                            //         }
                            //     }
                            // }
                            // //  ============= untuk case carry over, cek data tgl board nya dengan data tarikan dr be. jika ada set stallidnya coba ======= end
                            var vesdata = {chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe,
                            //row: vArg.row, col: vArg.col, width: vArg.width,
                            pekerjaan: woCategorySelected,
                            row: vrow, col: vcol, width: vdurasi,
                            selected: 1,
                            intStatus: vArg.asbStatus.intStatus,
                            stallId: vJobStallId, xxstallId: vArg.stallId, normal: vdurasi,
                            daydiff: 0, status: 'appointment', statusicon: ['customer_gray']};
                            vesdata.width1 = vesdata.width;
                            if(vesdata.stallId>0){
                                me.updateChipBreak(vesdata);
                            }
                            result.push(vesdata);
                        }


                    }
                    // if ((typeof vArg.preDiagnose !=='undefined') &&
                    //     // (vArg.preDiagnose.FrequencyIncident || vArg.preDiagnose.Mil || vArg.preDiagnose.ConditionIndication || vArg.preDiagnose.MachineCondition)
                    //     //!angular.equals({}, vArg.preDiagnose) && (typeof vArg.preDiagnose!== 'undefined')
                    //     me.isDataPrediagnose(vArg.preDiagnose)
                    //     ){
                    if (me.isDataPrediagnose(vArg.preDiagnose)){
                        var vjobdate = $filter('date')(vArg.asbStatus.PrediagStartTime, 'yyyy-MM-dd');
                        if (vcurdate==vjobdate || vPrediagStallId==0){
                            var vpddata = {chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.colPrediag, width: 1, selected: 1,
                                intStatus: vArg.asbStatus.intStatus,
                                pdText: 'PD', normal: 1,
                                stallId: vArg.asbStatus.PrediagStallId, isPrediagnose: 1,
                                // pekerjaan: woCategorySelected,
                                daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}
                            vpddata.width1 = vpddata.width;
                            if (vpddata.stallId>0){
                                me.updateChipBreak(vpddata);
                            }

                            result.push(vpddata);
                        }
                    }
                    if (vArg.isFinalInspect == 1){
                        var vFIdata = {chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: 0, width: vArg.durasi, selected: 1,
                                intStatus: vArg.asbStatus.intStatus,
                                pdText: 'FI', normal: 1,
                                stallId: 0,
                                // pekerjaan: woCategorySelected,
                                daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}
                                vFIdata.width1 = vFIdata.width;
                            if (vFIdata.stallId>0){
                                me.updateChipBreak(vFIdata);
                            }

                            result.push(vFIdata);
                    }
                    var xres = res.data.Result;
                    var checkJobId = false;
                    if (typeof vArg.jobId!=='undefined'){
                        checkJobId = true;
                    }
                    for(var i=0;  i<xres.length; i++){
                        var vitem = xres[i];
                        var vpush = true;
                        var isPrdiagnoseCarryover = false;
                        if (checkJobId){
                            if (vArg.jobId==vitem.JobId){
                                vpush = false;
                                // vpush = true;

                            }
                        }
                        if (vitem.Status==1 || vitem.Status==2){
                            vpush = false;
                        }
                        if (typeof vitem.IsInStopPageGr !== 'undefined'){
                            if (vitem.IsInStopPageGr==1){
                                vpush = false;
                            }
                        }
                        var vstatus = 'appointment';
                        var vicon = 'customer';
                        if (vstatus=='appointment'){
                            vicon = 'customer_gray';
                        }
                        var vcolumn = me.timeToColumn(vitem.PlanStart);
                        // NEW CODE FOR SYNC WITH JPCB
                        if(vitem.TimeFinish !== null){
                            var dateNow = me.changeFormatDateStrip(arg.currentDate);
                            diffDate = me.changeFormatDateStrip(vitem.PlanDateStart)
                            // console.log('diffdate', diffDate)
                            // console.log('dateNow', dateNow)
                            // console.log('vitem', vitem);
                            if(diffDate == dateNow ){
                                var TempPlanFinish = vitem.PlanFinish.split(':');
                                var TempTimeFinish = vitem.TimeFinish.split(':');
                                console.log("kadieu siaa",vitem.PoliceNumber,"=====",vitem.JumlahActualRate, me.incrementMinute)
                                vitem.PlanDateStart = vitem.PlanDateStart;
                                vitem.PlanDateFinish = vitem.PlanDateFinish;
                                vitem.PlanStart = vitem.PlanStart;
                                vitem.PlanFinish = vitem.PlanFinish;
                                vitem.Extension = vitem.Extension;
                                if (parseInt(TempTimeFinish[0]) < parseInt(TempPlanFinish[0])){
                                        vitem.PlanFinish = vitem.TimeFinish;
                                        var planStart = me.timeToColumn2(vitem.PlanStart);
                                        var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
                                        console.log('planStart =====', planStart)
                                        console.log('actualFinish ======', actualFinish)
                                        vitem.JumlahActualRate = actualFinish - planStart;
                                        vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                        if (vitem.JumlahActualRate <= 0){
                                            vitem.JumlahActualRate = 0.25;
                                        }
                                } else {
                                    if (parseInt(TempTimeFinish[1]) < parseInt(TempPlanFinish[1]) && parseInt(TempTimeFinish[0]) <= parseInt(TempPlanFinish[0])){
                                            vitem.PlanFinish = vitem.TimeFinish;
                                            var planStart = me.timeToColumn2(vitem.PlanStart);
                                            var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
                                            console.log('planStart =====', planStart)
                                            console.log('actualFinish ======', actualFinish)
                                            vitem.JumlahActualRate = actualFinish - planStart;
                                            vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                            if (vitem.JumlahActualRate <= 0){
                                                vitem.JumlahActualRate = 0.25;
                                            }
                                    }
                                }
                                var cekAwal = vitem.PlanStart.split(':');
                                var cekAkhir = vitem.PlanFinish.split(':');
                                if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                                        // vitem.JumlahActualRate = vitem.JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                                    if (vitem.isSplitActive == 0){
                                            vitem.JumlahActualRate = vitem.JumlahActualRate -1; 
                                    } else {
                                        vitem.JumlahActualRate = vitem.JumlahActualRate-1;
                                    }
                                }
                            }
                        }
                        // ======================
                        if(vitem.JobSplitChip != null && vitem.JobSplitChip != undefined && vitem.JobSplitChip.length > 0){
                            var dateNow = me.changeFormatDateStrip(arg.currentDate);
                            var diffDate = '';
                            var sortByProperty = function (property) {
                                return function (x, y) {
                                    return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
                                };
                            };
                            // vitem.JobSplitChip = vitem.JobSplitChip.sort(sortByProperty('PlanStart')); // komen dl blm tau knp di sort (30 juli 2021) by billy. uncomment jika ngaco

                            // if (adaSplit == 0){
                            //     for(var z in vitem.JobSplitChip){
                            //         diffDate = me.changeFormatDateStrip(vitem.JobSplitChip[z].PlanDateStart)
                            //         console.log('ini coco2', vitem.PoliceNumber)
                            //         if(diffDate == dateNow ){
                            //             adaSplit++;
                            //         }
                            //     }
                            // }
                           
                            // console.log('ada split', adaSplit)
                            console.log('ada split nopol', vitem.PoliceNumber, vitem.JobSplitChip)
                            for(var z = 0; z < vitem.JobSplitChip.length ; z++){
                                diffDate = me.changeFormatDateStrip(vitem.JobSplitChip[z].PlanDateStart)
                                // console.log('diffdate', diffDate)
                                // console.log('dateNow', dateNow)
                                // console.log('vitem', vitem);
                                    if(diffDate == dateNow ){
                                        JobSplitId = vitem.JobSplitChip[z].JobSplitId;
                                        vitem.StallId = vitem.JobSplitChip[z].StallId;
                                        vitem.PlanDateStart = vitem.JobSplitChip[z].PlanDateStart;
                                        vitem.PlanDateFinish = vitem.JobSplitChip[z].PlanDateFinish;
                                        vitem.PlanStart = vitem.JobSplitChip[z].PlanStart;
                                        vitem.PlanFinish = vitem.JobSplitChip[z].PlanFinish;
                                        vitem.Extension = vitem.JobSplitChip[z].Extension;
                                        if (vitem.isCarryOver == 0){
                                            vitem.IsInStopPageGr = vitem.JobSplitChip[z].IsInJobStopageGR;
                                        }
                                        var planStart = me.timeToColumn2(vitem.PlanStart);
                                        var actualFinish =  me.timeToColumn2(vitem.PlanFinish);
                                        if (vitem.JobSplitChip[z].TimeFinish !== null && vitem.JobSplitChip[z].TimeFinish !== undefined && vitem.JobSplitChip[z].TimeFinish !== ''){
                                            var datefinishString = $filter('date')(vitem.JobSplitChip[z].ActualDateFinish, 'yyyy-MM-dd');
                                            var plandatefinishString = $filter('date')(vitem.JobSplitChip[z].PlanDateFinish, 'yyyy-MM-dd');
                                            if (plandatefinishString == datefinishString){
                                                var timefinishX = vitem.JobSplitChip[z].TimeFinish.split(':')
                                                var planefinishX = vitem.JobSplitChip[z].PlanFinish.split(':')
                                                var xTf = new Date(2021,1,1,timefinishX[0],timefinishX[1])
                                                var xPf =new Date(2021,1,1,planefinishX[0],planefinishX[1]) 
                                                if (xTf.getTime() < xPf.getTime()){
                                                    actualFinish = me.timeToColumn2(vitem.JobSplitChip[z].TimeFinish);
                                                }
                                            }
                                        }
                                        vitem.JumlahActualRate = actualFinish - planStart;
                                        // vitem.JumlahActualRate = Math.ceil(Math.abs(vitem.JumlahActualRate)*4)/4;
                                        vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                        if (vitem.JumlahActualRate <= 0){
                                            vitem.JumlahActualRate = 0.25;
                                        }
                                        vcolumn = me.timeToColumn(vitem.PlanStart);

                                        vitem.isSplitActive = vitem.JobSplitChip[z].isSplitActive;

                                        var cekAwal = vitem.JobSplitChip[z].PlanStart.split(':');
                                        var cekAkhir = vitem.JobSplitChip[z].PlanFinish.split(':');
                                        if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                                                // vitem.JumlahActualRate = vitem.JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                                            if (vitem.JobSplitChip[z].isSplitActive == 0){
                                                    vitem.JumlahActualRate = vitem.JumlahActualRate -1; 
                                            } else {
                                                vitem.JumlahActualRate = vitem.JumlahActualRate-1;
                                            }
                                        }
                                        isPrdiagnoseCarryover = true;
                                        //    posisiLastSplitId = vitem.JobSplitChip[z].JobSplitId;
                                        //  NEW CODE
                                        var vdata = {
                                            chip: 'estimasi',
                                            nopol: vitem.PoliceNumber,
                                            tipe: vitem.ModelType,
                                            stallId: vitem.StallId,
                                            row: 0, col: vcolumn,
                                            normal: vitem.JumlahActualRate * 60 / me.incrementMinute,
                                            ext: vitem.Extension * 60 / me.incrementMinute,
                                            pekerjaan: vitem.JenisPekerjaan,
                                            // pekerjaanChipSelected : woCategorySelected,
                                            PlanDateStart: vitem.PlanDateStart,
                                            PlanDateFinish: vitem.PlanDateFinish,
                                            PlanStart: vitem.PlanStart,
                                            PlanFinish: vitem.PlanFinish,
                                            width: 1,
                                            daydiff: 0,
                                            status: vstatus,
                                            Status: vitem.Status,
                                            PreDiagnoseScheduledTime: vitem.PreDiagnoseScheduledTime,
                                            PreDiagStallId: vitem.PreDiagnoseStallId,
                                            statusicon: [vicon],
                                            TimeStart: vitem.TimeStart,
                                            TimeFinish: vitem.TimeFinish,
                                        }
                
                                        vdata.width = vdata.normal+vdata.ext;
                                        if (vdata.width<=0){
                                            vdata.width = 0.5;
                                        }
                                        if (me.isProblem(vdata)){
                                            vdata.status = 'block';
                                        } else {
                                            if (vdata.ext>0){
                                                vdata.status = 'extension';
                                            }
                                        }
                                        var tmpCheckDate = me.changeFormatDatetoStrip(vitem.JobSplitChip[z].PlanDateStart)
                                        // console.log("banjir boi ===>", tmpChip.JobChipInfo.SplitChips[tmpCheckDate],tmpCheckDate   );
                                        // debugger;
                                        if(vitem.JobSplitChip[z].isFI == 0 && vArg.jobId == vitem.JobId){
                                            vpush = false;
                                            if (vitem.JobSplitChip[z].TimeFinish !== null && vitem.JobSplitChip[z].TimeFinish !== undefined && vitem.JobSplitChip[z].TimeFinish !== ''){
                                                // kl uda clock off si chip yg selecteed nya biar jd pendek jg, karena kl baru clockoff dia blm punya isfi
                                                for (var k=0; k<result.length; k++){
                                                    if (vdata.nopol == result[k].nopol && result[k].chip == 'estimasi' && result[k].selected == 1 && vdata.PreDiagStallId === 0){
                                                        result[k].stallId = vdata.stallId
                                                        result[k].col = vdata.col
                                                        result[k].normal = vdata.normal
                                                        result[k].ext = vdata.ext
                                                        // result[k].status = vdata.status
                                                        // result[k].Status = vdata.Status
                                                        // result[k].statusicon = vdata.statusicon
                                                        result[k].width = vdata.width
                                                        result[k].width1 = vdata.width
                
                                                    }
                                                } 
                                            }
                                        }else if(vitem.JobSplitChip[z].isFI == null && vitem.JobSplitChip[z].isSplitActive == 1  && vArg.jobId == vitem.JobId){
                                            vpush = false;

                                            if (vitem.JobSplitChip[z].TimeFinish !== null && vitem.JobSplitChip[z].TimeFinish !== undefined && vitem.JobSplitChip[z].TimeFinish !== ''){
                                                // kl uda clock off si chip yg selecteed nya biar jd pendek jg, karena kl baru clockoff dia blm punya isfi
                                                for (var k=0; k<result.length; k++){
                                                    if (vdata.nopol == result[k].nopol && result[k].chip == 'estimasi' && result[k].selected == 1 && vdata.PreDiagStallId === 0){
                                                        result[k].stallId = vdata.stallId
                                                        result[k].col = vdata.col
                                                        result[k].normal = vdata.normal
                                                        result[k].ext = vdata.ext
                                                        // result[k].status = vdata.status
                                                        // result[k].Status = vdata.Status
                                                        // result[k].statusicon = vdata.statusicon
                                                        result[k].width = vdata.width
                                                        result[k].width1 = vdata.width
                
                                                    }
                                                } 
                                            } else {
                                                // ini kl di pake nanti pas tambah job, panajng nya ga akan nambah, tp jd ngikut ke tarikan data trs
                                                // for (var k=0; k<result.length; k++){
                                                //     if (vdata.nopol == result[k].nopol && result[k].chip == 'estimasi' && result[k].selected == 1 && vdata.PreDiagStallId === 0){
                                                //         result[k].stallId = vdata.stallId
                                                //         result[k].col = vdata.col
                                                //         result[k].normal = vdata.normal
                                                //         result[k].ext = vdata.ext
                                                //         // result[k].status = vdata.status
                                                //         // result[k].Status = vdata.Status
                                                //         // result[k].statusicon = vdata.statusicon
                                                //         result[k].width = vdata.width
                                                //         result[k].width1 = vdata.width
                                                //         result[k].row = -2 // biar naik ke atas, coba dl aja
                
                                                //     }
                                                // } 
                                            }
                                        }else{
                                            vpush = true;
                                        }
                                        // if(z == (vitem.JobSplitChip.length - 1)){
                                        //     vpush = false
                                        // }else{
                                        //     vpush = true
                                        // }
                                        // tmpChip.asbStatus.JobChipInfo.SplitChips
                                        //vdata.status = 'block';
                                        
                                        if (vpush){
                                            me.updateChipBreak(vdata);
                                            // if (vdata.breakWidth){
                                            //     vdata.width = vdata.width+vdata.breakWidth;
                                            //     vdata.width1 = vdata.break1start;
                                            //     vdata.start2 = vdata.break1start+vdata.break1width;
                                            //     vdata.width2 = vdata.width-vdata.start2;
                                            // } else {
                                            //     vdata.width1 = vdata.width;
                                            // }
                                            // console.log('vchipData', vdata);
                
                                            // if (!vArg.prediagnoseOnly){
                                                result.push(vdata);
                                            // }
                                            
                                            if ((vitem.StatusPreDiagnose==3 || vitem.StatusPreDiagnose==0) && isPrdiagnoseCarryover == false){
                                                var vpreDate = $filter('date')(vtgl1, 'yyyy-MM-dd');
                                                // if(me.changeFormatDateStrip(vitem.PreDiagnoseScheduledTime) == me.changeFormatDateStrip(vArg.tglAwal)){
                                                if(me.changeFormatDateStrip(vitem.PreDiagnoseScheduledTime) == me.changeFormatDateStrip(vArg.currentDate)){                                
                                                    if ((vpreDate!=='0001-01-01') && (vitem.PreDiagnoseStallId>0)){
                                                        var xdata = angular.copy(vdata);
                                                        xdata.breakWidth = 0;
                                                        xdata.width = 1;
                                                        xdata.width1 = 0;
                                                        xdata.width2 = 0;
                                                        xdata.Extension = 0;
                                                        xdata.normal = 1;
                                                        var vtgl1 = new Date(vitem.PreDiagnoseScheduledTime);
                                                        var vtgl2 = new Date(vitem.PreDiagnoseScheduledTime);
                                                        vtgl2.setTime(vtgl2.getTime() + (1*60*60*1000));
                
                                                        xdata.PlanStart = $filter('date')(vtgl1, 'HH:mm:ss');
                                                        xdata.PlanFinish = $filter('date')(vtgl2, 'HH:mm:ss');
                                                        xdata.stallId = vitem.PreDiagnoseStallId;
                                                        xdata.col = me.timeToColumn(xdata.PlanStart);
                                                        me.updateChipBreak(xdata);
                                                        result.push(xdata);
                                                    }
                                                }
                                            }
                                        }
                                    }
                            }
                            
                       }else{
                            // ======================
                            var vdata = {
                                chip: 'estimasi',
                                nopol: vitem.PoliceNumber,
                                tipe: vitem.ModelType,
                                stallId: vitem.StallId,
                                row: 0, col: vcolumn,
                                normal: vitem.JumlahActualRate * 60 / me.incrementMinute,
                                ext: vitem.Extension * 60 / me.incrementMinute,
                                pekerjaan: vitem.JenisPekerjaan,
                                // pekerjaanChipSelected : woCategorySelected,
                                PlanDateStart: vitem.PlanDateStart,
                                PlanDateFinish: vitem.PlanDateFinish,
                                PlanStart: vitem.PlanStart,
                                PlanFinish: vitem.PlanFinish,
                                width: 1,
                                daydiff: 0,
                                status: vstatus,
                                Status: vitem.Status,
                                PreDiagnoseScheduledTime: vitem.PreDiagnoseScheduledTime,
                                PreDiagStallId: vitem.PreDiagnoseStallId,
                                statusicon: [vicon],
                                TimeStart: vitem.TimeStart,
                                TimeFinish: vitem.TimeFinish,
                            }

                            vdata.width = vdata.normal+vdata.ext;
                            if (vdata.width<=0){
                                vdata.width = 0.5;
                            }
                            if (me.isProblem(vdata)){
                                vdata.status = 'block';
                            } else {
                                if (vdata.ext>0){
                                    vdata.status = 'extension';
                                }
                            }
                            //vdata.status = 'block';

                            if (vpush){
                                me.updateChipBreak(vdata);
                                // if (vdata.breakWidth){
                                //     vdata.width = vdata.width+vdata.breakWidth;
                                //     vdata.width1 = vdata.break1start;
                                //     vdata.start2 = vdata.break1start+vdata.break1width;
                                //     vdata.width2 = vdata.width-vdata.start2;
                                // } else {
                                //     vdata.width1 = vdata.width;
                                // }
                                // console.log('vchipData', vdata);

                                // if (!vArg.prediagnoseOnly){
                                    result.push(vdata);
                                // }
                                
                                if ((vitem.StatusPreDiagnose==3 || vitem.StatusPreDiagnose==0) && isPrdiagnoseCarryover == false){
                                    var vpreDate = $filter('date')(vtgl1, 'yyyy-MM-dd');
                                    // if(me.changeFormatDateStrip(vitem.PreDiagnoseScheduledTime) == me.changeFormatDateStrip(vArg.tglAwal)){
                                    if(me.changeFormatDateStrip(vitem.PreDiagnoseScheduledTime) == me.changeFormatDateStrip(vArg.currentDate)){                                
                                        if ((vpreDate!=='0001-01-01') && (vitem.PreDiagnoseStallId>0)){
                                            var xdata = angular.copy(vdata);
                                            xdata.breakWidth = 0;
                                            xdata.width = 1;
                                            xdata.width1 = 0;
                                            xdata.width2 = 0;
                                            xdata.Extension = 0;
                                            xdata.normal = 1;
                                            var vtgl1 = new Date(vitem.PreDiagnoseScheduledTime);
                                            var vtgl2 = new Date(vitem.PreDiagnoseScheduledTime);
                                            vtgl2.setTime(vtgl2.getTime() + (1*60*60*1000));

                                            xdata.PlanStart = $filter('date')(vtgl1, 'HH:mm:ss');
                                            xdata.PlanFinish = $filter('date')(vtgl2, 'HH:mm:ss');
                                            xdata.stallId = vitem.PreDiagnoseStallId;
                                            xdata.col = me.timeToColumn(xdata.PlanStart);
                                            me.updateChipBreak(xdata);
                                            result.push(xdata);
                                        }
                                    }
                                }
                            } else {
                                for (var k=0; k<result.length; k++){
                                    if (vdata.nopol == result[k].nopol && result[k].chip == 'estimasi' && result[k].selected == 1 && vdata.PreDiagStallId === 0 && (vdata.Status >= 8 && vdata.Status != 22)){
                                        result[k].stallId = vdata.stallId
                                        result[k].col = vdata.col
                                        result[k].normal = vdata.normal
                                        result[k].ext = vdata.ext
                                        result[k].status = vdata.status
                                        result[k].Status = vdata.Status
                                        result[k].statusicon = vdata.statusicon
                                        result[k].width = vdata.width
                                        result[k].width1 = vdata.width

                                    }
                                }
                            }
                       }
                        
                    }

                    // 

                    // try re-order chip, supaya chip yg selected ada di atas ============================= kl di pake ploting info nya jd ga bs update kl pas plot nya
                    // var SortedData = angular.copy(result);
                    // SortedData = SortedData.sort(function(a, b){
                    //     var A=a.col, B=b.col
                    //     return A-B //sort by date ascending
                    // })
                    // result = SortedData
                    // try re-order chip, supaya chip yg selected ada di atas =============================

                    
                    onFinish(result);
                })


                // var vres = [];
                // var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
                // vArg = JsBoard.Common.extend(vArg, arg);
                // var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                // ]
                // var today = new Date();
                // if (arg.currentDate){
                //     var vdiff = this.dayDiff(today, arg.currentDate);
                //     for (var i=0; i<vitems.length; i++){
                //         if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
                //             vres.push(vitems[i]);
                //         }
                //     }
                // }

                // $timeout(function(){
                //     onFinish(vres);
                // }, 100);
            },

            // onGetChips: function(arg){
            //     var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
            //     vArg = JsBoard.Common.extend(vArg, arg);
            //     var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     ]
            //     var today = new Date();
            //     if (arg.currentDate){
            //         var vdiff = this.dayDiff(today, arg.currentDate);
            //         var vres = []
            //         for (var i=0; i<vitems.length; i++){
            //             if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
            //                 vres.push(vitems[i]);
            //             }
            //         }
            //         return vres;
            //     }
            //     return []
            // },
            dayDiff: function(date1, date2){
                return Math.round((date2 - date1) / (1000*60*60*24));
            },

            getBreakTime: function(){
                var vstartMin = JsBoard.Common.getMinute(this.breakHour);
                var vendMin = vstartMin+60;
                var vBreakEnd = JsBoard.Common.getTimeString(vendMin);
                var vstart = this.timeToColumn(this.breakHour);
                var vend = this.timeToColumn(vBreakEnd);
                return [{
                    start: vstart,
                    end: vend,
                    width: vend-vstart,
                }]
            },
            getBreakChipTime: function(vstart, vlength){
                var vbreaks = this.getBreakTime();
                var vbreakCount = 0;
                var vtotalBreak = 0;
                var vresult = {}
                for(var i=0; i<vbreaks.length; i++){
                    vbreak = vbreaks[i];
                    if (this.isOverlapped(vstart, vstart+vlength, vbreak.start, vbreak.end)){
                        if (vstart>vbreak.start){
                            return 'start-inside-break';
                        }
                        vbreakCount = vbreakCount+1;
                        vresult['break'+vbreakCount+'start'] = vbreak.start-vstart;
                        vresult['break'+vbreakCount+'width'] = vbreak.width;
                        vtotalBreak = vtotalBreak+vbreak.width;
                    }
                }
                if (vbreakCount==0){
                    return 'no-break';
                } else {
                    vresult['breakWidth'] = vtotalBreak;
                }
                return vresult;

            },
            updateChipBreak: function(vchipData){
                if (vchipData.breakWidth){
                    vchipData.width = vchipData.width-vchipData.breakWidth;
                }
                var vbreaks = 'no-break';
                if (vchipData.isPrediagnose){
                    if (vchipData.PrediagStallId!=0){
                        vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width);
                    }
                } else {
                    if (vchipData.stallId!=0){
                        vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width);
                    }

                }

                var deleteList = ['break1start', 'break1width', 'break2start', 'break2width', 'break3start', 'break3width','break4start', 'break4width','breakWidth'];
                for (var i=0; i<deleteList.length; i++){
                    var idx = deleteList[i];
                    delete vchipData[idx];
                }
                if (typeof vbreaks=='object'){
                    for (var idx in vbreaks){
                        vchipData[idx] = vbreaks[idx];
                    }
                }

                if (vchipData.breakWidth){
                    vchipData.width = vchipData.width+vchipData.breakWidth;
                    vchipData.width1 = vchipData.break1start;
                    vchipData.start2 = vchipData.break1start+vchipData.break1width;
                    vchipData.width2 = vchipData.width-vchipData.start2;
                } else {
                    vchipData.width1 = vchipData.width;
                }
                // console.log('vchipData', vdata);

                // console.log('vbreaks', vbreaks);
                //////////
            },

            updateTimeLine: function(boardname){
                // var boardname = this.boardName;
                var vDate = this.getCurrentTime();
                // console.log("current Time#####", vDate.getTime());
                var currentHour = $filter('date')(vDate, 'HH:mm');
                var currentDate = $filter('date')(vDate, 'yyyy-MM-dd');

                // console.log("currentHour", currentHour);
                //var startHour = this.
                //var minutPerCol

                // asbGrFactoryTest

                var vboard = jpcbFactory.getBoard(boardname);
                if (vboard){
                    var xdate = vboard.config.argChips.currentDate;
                    var boardDate = $filter('date')(xdate, 'yyyy-MM-dd');
                    if (boardDate > currentDate){
                        currentHour = this.getFirstHour();
                    } else if (boardDate < currentDate){
                        currentHour = this.getLastHour();
                    }
                }
                jpcbFactory.updateTimeLine(boardname, currentHour, this.startHour, this.incrementMinute);

                //this.updateChipStatus(boardname);

                //this.checkChipStatus(boardname);

                // var vboard = jpcbFactory.getBoard(boardname);
                // if (vboard){
                //     vboard.redrawChips();
                // }

                //jpcbFactory.updateStandardDraggable(boardname);
                jpcbFactory.redrawBoard(boardname);
            },


        }
        // perchipanduniawi 11 agt 2021 : uda bs index chip yg di kanan itu di atas kl numpuk, chip di wo list ga bs di pindah kl lg clock on dan blm clock off,
    });
