angular.module('app')
.factory('AsbEstimasiFactory', function($http, CurrentUser, $filter, $timeout, jpcbFactory, JpcbGrViewFactory) {
    var currentUser = CurrentUser.user;
    var cekstallJPLUS = 0;
    var tempActiveJobId = null;
    // console.log(currentUser);
    
    return {
        minuteDelay: 10*60+10,
        //minuteDelay: 0,
        onPlot: function(){
            // do nothing
        },
        showBoard: function(config){
            console.log('config coy', config)
            tempActiveJobId = config.currentChip.nopol;
            if(config.currentChip.stallId ==0){
                cekstallJPLUS = 1;
            } else {
                cekstallJPLUS = 0;
            }
            var vcontainer = config.container;
            var vname = 'asbgr';
            if (typeof config.boardName !== 'undefined'){
                vname = config.boardName;
            }
            this.boardName = vname;
            var vobj = document.getElementById(vcontainer);
            if(vobj){
                var vwidth = vobj.clientWidth;
                console.log(vwidth);
                if (vwidth<=0) {
                    return;
                }
            } else {
                return;
            }
            if (typeof config.onPlot=='function'){
                this.onPlot = config.onPlot;
            }

            if (typeof config.currentChip.asbStatus ==='undefined'){
                config.currentChip.asbStatus = {}
            }

            if (typeof config.currentChip.asbStatus.JobStallId ==='undefined'){
                config.currentChip.asbStatus.JobStallId = 0;
                config.currentChip.asbStatus.PrediagStartTime = null;
                config.currentChip.asbStatus.PrediagEndTime = null;
            }

            if (typeof config.currentChip.asbStatus.PrediagStallId ==='undefined'){
                config.currentChip.asbStatus.PrediagStallId = 0;
                config.currentChip.asbStatus.JobStartTime = null;
                config.currentChip.asbStatus.JobEndTime = null;
            }

            var vdurasi = 1;
            if (typeof config.currentChip.durasi !=='undefined'){
                vdurasi = config.currentChip.durasi;
            }
            config.currentChip.width = (vdurasi/60)*this.incrementMinute;

            var vrow = -2;
            // if (typeof config.currentChip.stallId !=='undefined'){
            //     if (config.currentChip.stallId>0){
            //         vrow = config.currentChip.stallId - 1 ;
            //     }
            // }
            config.currentChip.row = vrow;

            var vhourStart = JsBoard.Common.getMinute(this.startHour)/60;
            var vcol = 0;
            if (typeof config.currentChip.startTime !=='undefined'){
                vstartTime = config.currentChip.startTime;
                vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
            }
            config.currentChip.col = vcol;

            if (config.currentChip.asbStatus.PrediagStallId>0){
                var vcol = 0;
                if (typeof config.currentChip.startTime !=='undefined'){
                    vstartTime = config.currentChip.asbStatus.PrediagStartTime;
                    vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                }
                config.currentChip.colPrediag = vcol;
            } else {
                config.currentChip.colPrediag = config.currentChip.col+1;
            }

            
            jpcbFactory.createBoard(vname, {
                board: {
                    chipGroup: 'asb_estimasi',
                    columnWidth: 120,
                    rowHeight: 80,
                    footerChipName: 'footer',
                    snapDivider: 4,
                    firstColumnTitle: 'SA Name',                    
                },
                chip: {
                    startHour: this.startHour,
                    incrementMinute: this.incrementMinute
                },
                standardDraggable: false
            });
            var me = this;
            jpcbFactory.setHeader(vname, {
                headerHeight: 40,
                onGetHeader: function(arg, vfunc){
                    return me.onGetHeader(arg, vfunc);
                }
            });
            jpcbFactory.setStall(vname, {
                firstColumnWidth: 120,
                onGetStall: function(arg, vfunc){
                    return me.onGetStall(arg, vfunc);
                }
            });
            var vxConfig = jpcbFactory.boardConfigList[vname];
            vxConfig.JobChipInfo = config.currentChip.asbStatus.JobChipInfo;
            jpcbFactory.setOnGetChips(vname, function(arg, vfunc){
                return me.onGetChips(arg, vfunc);
            });

            jpcbFactory.setOnChipLoaded(vname, function(arg, vfunc){
                return me.onChipLoaded(arg, vfunc);
            });
            
            jpcbFactory.setSelectionArea(vname, {tinggiArea: 110});

            jpcbFactory.setArgChips(vname, config.currentChip);
            jpcbFactory.setArgStall(vname, config.currentChip);
            // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

            jpcbFactory.setOnCreateChips(vname, function(vchip){
                if (vchip.data.width<0.25){
                    vchip.data.isDraggable = false;
                } else {
                    vchip.data.isDraggable = true;
                }
                
                if (vchip.data.selected){
                    if (vchip.data.width<0.25){
                        vchip.draggable(false);
                    } else {
                        vchip.draggable(true);
                    }
                    
                }
                if (vchip.data.isInfo){
                    vchip.data.isDraggable = false;
                    // vchip.draggable(false);
                }
            });


            var me = this;
            var the_config = config;
            jpcbFactory.setOnDragDrop(vname, function(vchip){

                // menentukan startDateTime, endDateTime, dan stallid
                var curDate = config.currentChip.currentDate;
                var vdurasi = 1;
                if (typeof config.currentChip.durasi !=='undefined'){
                    vdurasi = config.currentChip.durasi;
                }
                if (vchip.data.isPrediagnose){
                    vdurasi = 1;
                }

                var vmin = me.incrementMinute * vchip.data['col'];
                var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                var vres = JsBoard.Common.getTimeString(vminhour);
                var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+vres);
                //startTime.setMinutes(startTime.getMinutes()+vmin);
                //startTime.setMinutes(vminhour);

                var emin = vminhour + vdurasi * 60;
                var eres = JsBoard.Common.getTimeString(emin);
                var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                //endTime.setMinutes(endTime.getMinutes()+emin);

                var vStallId = 0;
                if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                    && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                    && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                    vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                }

                // hasil akhir di variabel startTime, endTime, dan vStallId
                //

                if (vchip.data.isPrediagnose){
                    the_config.currentChip.asbStatus.PrediagStallId = vStallId;
                    the_config.currentChip.asbStatus.PrediagStartTime = startTime;
                    the_config.currentChip.asbStatus.PrediagEndTime = endTime;
                } else {
                    the_config.currentChip.asbStatus.JobStallId = vStallId;
                    the_config.currentChip.asbStatus.JobStartTime = startTime;
                    the_config.currentChip.asbStatus.JobEndTime = endTime;
                }

                // cek status
                var pstatus;
                if (the_config.currentChip.asbStatus.JobStallId==0){
                    if (the_config.currentChip.asbStatus.PrediagStallId==0){
                        pstatus = 0;
                    } else {
                        pstatus = 2;
                    }
                } else {
                    if (the_config.currentChip.asbStatus.PrediagStallId==0){
                        pstatus = 1;
                    } else {
                        pstatus = 3;
                    }
                }

                vchip.data.StatusPreDiagnose = pstatus;
                if (pstatus==0){
                    // tidak ada yang diplot
                    vchip.data.startTime = null;
                    vchip.data.endTime = null;
                    vchip.data.stallId = 0;
                    vchip.data.PrediagStallId = null;
                    vchip.data.PrediagScheduledTime = null;
                } else if (pstatus==1){
                    // yang diplot job item
                    vchip.data.startTime = the_config.currentChip.asbStatus.JobStartTime;
                    vchip.data.endTime = the_config.currentChip.asbStatus.JobEndTime;
                    vchip.data.stallId = the_config.currentChip.asbStatus.JobStallId;
                    vchip.data.PrediagStallId = null;
                    vchip.data.PrediagScheduledTime = null;
                } else if (pstatus==2){
                    vchip.data.startTime = the_config.currentChip.asbStatus.PrediagStartTime;
                    vchip.data.endTime = the_config.currentChip.asbStatus.PrediagEndTime;
                    vchip.data.stallId = the_config.currentChip.asbStatus.PrediagStallId;
                    vchip.data.PrediagStallId = the_config.currentChip.asbStatus.PrediagStallId;
                    vchip.data.PrediagScheduledTime = the_config.currentChip.asbStatus.PrediagStartTime;
                } else {
                    // pstatus=3
                    vchip.data.startTime = the_config.currentChip.asbStatus.JobStartTime;
                    vchip.data.endTime = the_config.currentChip.asbStatus.JobEndTime;
                    vchip.data.stallId = the_config.currentChip.asbStatus.JobStallId;
                    vchip.data.PrediagStallId = the_config.currentChip.asbStatus.PrediagStallId;
                    vchip.data.PrediagScheduledTime = the_config.currentChip.asbStatus.PrediagStartTime;
                }

                if (typeof me.onPlot=='function'){
                    delete vchip.data.start1;
                    delete vchip.data.width1;
                    delete vchip.data.start2;
                    delete vchip.data.width2;
                    delete vchip.data.start3;
                    delete vchip.data.width3;
                    me.updateChipBreak(vchip.data);
                    var vboard = jpcbFactory.getBoard(me.boardName);

                    me.onPlot(vchip.data);
                    vboard.chipItems[vchip.data.dataIndex] = vchip.data;
                    if (vboard.board.selectbox){
                        vboard.board.selectbox.destroyChildren();
                    }
                    vboard.redrawChips();
                }
                

                // vchip.data.startTime = startTime;
                // vchip.data.endTime = endTime;
                // vchip.data.stallId = vStallId;
                //


                


                // if (vchip.data.isPrediagnose){
                //     var curDate = config.currentChip.currentDate;
                //     var vdurasi = 1;
                //     if (typeof config.currentChip.durasi !=='undefined'){
                //         vdurasi = config.currentChip.durasi;
                //     }

                //     var vmin = me.incrementMinute * vchip.data['col'];
                //     var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                //     var vres = JsBoard.Common.getTimeString(vminhour);
                //     var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+vres);
                //     //startTime.setMinutes(startTime.getMinutes()+vmin);
                //     //startTime.setMinutes(vminhour);

                //     var emin = vminhour + vdurasi * 60;
                //     var eres = JsBoard.Common.getTimeString(emin);
                //     var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                //     //endTime.setMinutes(endTime.getMinutes()+emin);

                //     vchip.data.startTime = startTime;
                //     vchip.data.endTime = endTime;
                //     var vStallId = 0;
                //     if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                //         vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                //     }

                //     the_config.currentChip.PrediagStallId = vStallId;


                //     vchip.data.stallId = vStallId;
                //     if (vStallId===0){
                //         vchip.data.startTime = null;
                //         vchip.data.endTime = null;
                //     }
                //     console.log("on plot data", vchip.data);
                //     the_config.currentChip.PrediagStallId = vStallId;
                //     var pstatus = 0;
                //     if (the_config.currentChip.JobStallId){
                //         if (the_config.currentChip.PrediagStallId) {
                //             pstatus = 3
                //         } else {
                //             pstatus = 1;
                //         }
                //     } else {
                //         if (the_config.currentChip.PrediagStallId) {
                //             pstatus = 2
                //         }                            
                //     }
                //     vchip.data.StatusPreDiagnose = pstatus;
                //     vchip.data.PrediagStallId = vStallId;
                //     vchip.data.PrediagScheduledTime = vchip.data.startTime;
                //     if (typeof me.onPlot=='function'){           
                //         me.onPlot(vchip.data);
                //     }
                //     console.log("on drag-drop vchip", vchip);
                // } else {

                //     var curDate = config.currentChip.currentDate;
                //     var vdurasi = 1;
                //     if (typeof config.currentChip.durasi !=='undefined'){
                //         vdurasi = config.currentChip.durasi;
                //     }

                //     var vmin = me.incrementMinute * vchip.data['col'];
                //     var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                //     var vres = JsBoard.Common.getTimeString(vminhour);
                //     var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+vres);
                //     //startTime.setMinutes(startTime.getMinutes()+vmin);
                //     //startTime.setMinutes(vminhour);

                //     var emin = vminhour + vdurasi * 60;
                //     var eres = JsBoard.Common.getTimeString(emin);
                //     var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                //     //endTime.setMinutes(endTime.getMinutes()+emin);

                //     vchip.data.startTime = startTime;
                //     vchip.data.endTime = endTime;
                //     var vStallId = 0;
                //     if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                //         vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                //     }
                //     vchip.data.stallId = vStallId;
                //     if (vStallId===0){
                //         vchip.data.startTime = null;
                //         vchip.data.endTime = null;
                //     }
                //     console.log("on plot data", vchip.data);
                //     the_config.currentChip.JobStallId = vStallId;
                //     var pstatus = 0;
                //     if (the_config.currentChip.JobStallId){
                //         if (the_config.currentChip.PrediagStallId) {
                //             pstatus = 3
                //         } else {
                //             pstatus = 1;
                //         }
                //     } else {
                //         if (the_config.currentChip.PrediagStallId) {
                //             pstatus = 2
                //         }                            
                //     }
                //     vchip.data.StatusPreDiagnose = pstatus;
                //     if (typeof me.onPlot=='function'){           
                //         me.onPlot(vchip.data);
                //     }
                //     console.log("on drag-drop vchip", vchip);
                // }


            });


            jpcbFactory.setOnBoardReady(vname, function(vconfig){
                console.log('board ready', vconfig);
                vconfig.theBoard.layout.board.checkDragDrop = function(vchip, vcol, vrow){
                    // var vbreakstart = me.timeToColumn('12:00:00');
                    // var vbreakend = me.timeToColumn('13:00:00');
                    var vcolstart = vcol;
                    var vcolend = vcol+vchip.data.width;
                    var vbreak = me.getBreakTime();
                    console.log(vbreak);
                    for (var i=0; i<vbreak.length; i++){
                        vbreakstart = vbreak[i].start;
                        vbreakend = vbreak[i].end;
                        var vjamstart = JpcbGrViewFactory.columnToTime(vcol);
                        var splitJam = vjamstart.split(':');
                        if (splitJam[0] == '12'){
                            return null; // ini kalau dia di taro di jam 12 - 13. ga boleh di jam istirahat
                        }
                        if (me.isOverlapped3(vcolstart, vcolstart, vbreakstart, vbreakend)){
                            return false;
                        }
                    }


                    var vlast = me.getLastHour()+':00';
                    var vlastCol = me.timeToColumn(vlast);
                    if (vcol>=vlastCol){
                        return false;
                    }

                    if (vchip.data.isPrediagnose){
                        if (vconfig){
                            if ((typeof vconfig.theBoard.layout.infoRows[vrow]!=='undefined') && (vconfig.theBoard.layout.infoRows[vrow].data.type!=4)){
                                return false;
                            }

                        }
                    } else {
                        if (vconfig){
                            if ((typeof vconfig.theBoard.layout.infoRows[vrow]!=='undefined') && (vconfig.theBoard.layout.infoRows[vrow].data.type==4)){
                                return false;
                            }

                        }

                    }
                    if (typeof vconfig.theBoard.layout.cekMenumpuk == 'function'){
                        var xBreak = 0;
                        if (vchip.data.breakWidth){
                            vchip.attrs.minBreakWidth = vchip.data.breakWidth*vconfig.board.columnWidth;
                        } else {
                            vchip.attrs.minBreakWidth = 0;
                        }
                        var vbreaks = me.getBreakChipTime(vcol, vchip.data.width);
                        if (vbreaks.breakWidth){
                            vchip.attrs.breakWidth = vbreaks.breakWidth*vconfig.board.columnWidth;
                            vx = vconfig.theBoard.layout.col2x(vcol);
                            vy = vconfig.theBoard.layout.row2y(vrow);
                            if (vconfig.theBoard.layout.cekMenumpuk(vchip, vx, vy)){
                                return false;
                            }
                        }

                    }
                    return true;
                }

                //me.updateTimeLine(vname);
                var vrnd = me.getIntRandom();
                me.timerCode = vrnd;
                me.timerLoop(vname, vrnd);


            });



            jpcbFactory.showBoard(vname, vcontainer)
            // $timeout(function(){
            //     jpcbFactory.showBoard(vname, vcontainer)

            // }, 100);

        },
        startHour: '08:00',
        endHour: '17:00',
        breakHour: '12:00',
        incrementMinute: 60,

        getCurrentTime: function(){
            return jpcbFactory.getCurrentTime();
        },
        isProblem: function(vitem){
            return jpcbFactory.isJobProblem(vitem);
        },
        timeToColumn: function(vtime){
            var vstartmin = JsBoard.Common.getMinute(this.startHour);
            var vmin = JsBoard.Common.getMinute(vtime)-vstartmin;
            var vresult = vmin/this.incrementMinute;
            
            return vresult;
        },
        onGetHeader: function(arg, onFinish){
            varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
            var vWorkHour = JsBoard.Common.generateDayHours({
                startHour: this.startHour,
                endHour: this.endHour,
                increment: this.incrementMinute,
            });
            var result = [];
            for(var idx in vWorkHour){
                var vitem = {title: vWorkHour[idx]}
                if (vWorkHour[idx]==this.breakHour){
                    vitem.colInfo = {isBreak: true}
                }
                result.push(vitem)

            }
            console.log("header", result);
            $timeout(function(){
                onFinish(result);
            }, 100);
            
        },
        // onGetHeader: function(arg){
        //     varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
        //     var vWorkHour = JsBoard.Common.generateDayHours({
        //         startHour: this.startHour,
        //         endHour: this.endHour,
        //         increment: this.incrementMinute,
        //     });
        //     var result = [];
        //     for(var idx in vWorkHour){
        //         var vitem = {title: vWorkHour[idx]}
        //         if (vWorkHour[idx]==this.breakHour){
        //             vitem.colInfo = {isBreak: true}
        //         }
        //         result.push(vitem)

        //     }
        //     console.log("header", result);
        //     return result;
        // },
        onGetStall: function(arg, onFinish){
            // $http.get('/api/as/Stall').
            //var vparent = '0';
            $http.get('/api/as/Boards/jpcb/employee/-/SABP').then(function(res){
                var xres = res.data.Result;
                var result = [];
                for(var i=0;  i<xres.length; i++){
                    var vitem = xres[i];
                    result.push({
                        title: vitem.EmployeeName,
                        status: 'normal',
                        stallId: vitem.EmployeeId
                    });
                }
                onFinish(result);
            })

            // var vres = [{title: 'Diagnose', status: 'normal', person: 'Joko'},
            //     {title: 'EM 1', status: 'normal', person: 'Ton'},
            //     {title: 'EM 2', status: 'normal', person: 'And'},
            //     {title: 'ST 1', status: 'normal', person: 'Rul'},
            //     {title: 'ST 2', status: 'normal', person: 'Fir'},
            //     {title: 'S3 3', status: 'normal', person: 'Ron'},
            // ]
            // $timeout(function(){
            //     onFinish(vres);
            // }, 100);
        },
        // onGetStall: function(arg){
        //     return[{title: 'Diagnose', status: 'normal', person: 'Joko'},
        //         {title: 'EM 1', status: 'normal', person: 'Ton'},
        //         {title: 'EM 2', status: 'normal', person: 'And'},
        //         {title: 'ST 1', status: 'normal', person: 'Rul'},
        //         {title: 'ST 2', status: 'normal', person: 'Fir'},
        //         {title: 'S3 3', status: 'normal', person: 'Ron'},
        //     ]
        // },
        findStallIndex: function(vStallId, vList){
            for (var i=0; i<vList.length; i++){
                var vitem = vList[i];
                if ((typeof vitem.stallId!=='undefined') && (vitem.stallId==vStallId)){
                    return i;
                }
            }
            return -1;
        },
        onChipLoaded: function(vjpcb){
            for (var i=0; i<vjpcb.allChips.length; i++){
                var vitem = vjpcb.allChips[i];
                vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                if (vidx>=0){
                    vitem.row = vidx;
                }                
                console.log('mama kau', vjpcb)
                if (vitem.nopol == tempActiveJobId){
                    if (cekstallJPLUS == 1){
                        vjpcb.allChips[i].row = -2;
                        vjpcb.allChips[i].stallId = 0;
                    }
                }
                
            }

        },
        onGetChips: function(arg, onFinish){
            var vArg = {nopol: 'B 1000 AAAXX', tipe: 'Avanza'}
            vArg = JsBoard.Common.extend(vArg, arg);
            
            var tglAwal = arg.currentDate;
            var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
            tglAkhir.setDate(tglAkhir.getDate());
            // var vurl = '/api/as/Boards/Appointment/1/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/'+$filter('date')(tglAkhir, 'yyyy-MM-dd');
            // var vMinMaxStatus = '/0/17';
            var vurl = '/api/as/Boards/BP/AsbEstimasi/'+$filter('date')(tglAwal, 'yyyy-MM-dd');
            var me = this;
            $http.get(vurl).then(function(res){
                var result = [];
                var vJobStallId = vArg.stallId;
                if ((typeof vArg.asbStatus !=='undefined') && (typeof vArg.asbStatus.JobStallId !=='undefined') ){
                    var vJobStallId = vArg.asbStatus.JobStallId
                }
                if (!(typeof vArg.visible !== 'undefined' && !vArg.visible)) {
                    result = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, 
                        stallId: vJobStallId, xxstallId: vArg.stallId,
                        daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}];
                }
                if ((typeof vArg.preDiagnose !=='undefined') && 
                    (vArg.preDiagnose.FrequencyIncident || vArg.preDiagnose.Mil || vArg.preDiagnose.ConditionIndication || vArg.preDiagnose.MachineCondition)){
                    result.push({chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.colPrediag, width: 1, selected: 1, 
                        pdText: 'PD',
                        stallId: vArg.asbStatus.PrediagStallId, isPrediagnose: 1,
                        daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}
                    );
                }
                var xres = res.data.Result;
                var checkJobId = false;
                if (typeof vArg.jobId!=='undefined'){
                    checkJobId = true;
                }
                for(var i=0;  i<xres.length; i++){
                    var vitem = xres[i];
                    var vpush = true;
                    if (checkJobId){
                        if (vArg.jobId==vitem.JobId){
                            vpush = false;
                        }
                    }
                    if (vitem.Status==1 || vitem.Status==2){
                        vpush = false;
                    }
                    var vstatus = 'appointment';
                    var vicon = 'customer';
                    if (vstatus=='appointment'){
                        vicon = 'customer_gray';
                    }
                    var vcolumn = me.timeToColumn(vitem.PlanStart);

                    if (vitem.PoliceNumber == tempActiveJobId){
                        if (cekstallJPLUS == 1){
                            vitem.UserIdSa = 0;
                        }
                    }

                    var vdata = {
                        chip: 'estimasi', 
                        nopol: vitem.PoliceNumber, 
                        tipe: vitem.ModelType, 
                        stallId: vitem.UserIdSa,
                        row: 0, col: vcolumn, 
                        normal: 1,
                        ext: 0,
                        PlanDateStart: vitem.AppointmentDate,
                        PlanDateFinish: vitem.PlanDateFinish,
                        PlanStart: vitem.AppointmentTime,
                        PlanFinish: vitem.PlanFinish,
                        width: 1, 
                        daydiff: 0, 
                        status: vstatus,
                        Status: vitem.Status,
                        statusicon: [vicon]
                    }
                    vdata.width = vdata.normal+vdata.ext;
                    if (vdata.width<=0){
                        vdata.width = 0.5;
                    }
                    if (me.isProblem(vdata)){
                        vdata.status = 'block';
                    } else {
                        if (vdata.ext>0){
                            vdata.status = 'extension';
                        }
                    }
                    //vdata.status = 'block';
                    
                    if (vpush){
                        me.updateChipBreak(vdata);
                        result.push(vdata);

                        if (vitem.StatusPreDiagnose==3){
                            var xdata = angular.copy(vdata);
                            xdata.width = 1;
                            var vtgl1 = new Date(vitem.PreDiagnoseScheduledTime);
                            var vtgl2 = new Date(vitem.PreDiagnoseScheduledTime);
                            vtgl2.setTime(vtgl2.getTime() + (1*60*60*1000));
                            
                            xdata.PlanStart = $filter('date')(vtgl1, 'HH:mm:ss');
                            xdata.PlanFinish = $filter('date')(vtgl2, 'HH:mm:ss');
                            xdata.stallId = vitem.PreDiagnoseStallId;
                            xdata.col = me.timeToColumn(xdata.PlanStart);
                            result.push(xdata);
                            me.updateChipBreak(xdata);
                        }
                    }
                }
                onFinish(result);
            })
            

            // var vres = [];
            // var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
            // vArg = JsBoard.Common.extend(vArg, arg);
            // var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //     {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //     {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //     {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //     {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //     {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //     {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
            //     {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            // ]
            // var today = new Date();
            // if (arg.currentDate){
            //     var vdiff = this.dayDiff(today, arg.currentDate);
            //     for (var i=0; i<vitems.length; i++){
            //         if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
            //             vres.push(vitems[i]);
            //         }
            //     }
            // }

            // $timeout(function(){
            //     onFinish(vres);
            // }, 100);
        },

        // onGetChips: function(arg){
        //     var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
        //     vArg = JsBoard.Common.extend(vArg, arg);
        //     var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
        //         {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
        //         {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
        //         {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
        //         {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
        //         {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
        //         {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
        //         {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
        //         {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
        //         {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
        //     ]
        //     var today = new Date();
        //     if (arg.currentDate){
        //         var vdiff = this.dayDiff(today, arg.currentDate);
        //         var vres = []
        //         for (var i=0; i<vitems.length; i++){
        //             if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
        //                 vres.push(vitems[i]);
        //             }
        //         }
        //         return vres;
        //     }
        //     return []
        // },
        dayDiff: function(date1, date2){
            return Math.round((date2 - date1) / (1000*60*60*24));
        },
        getBreakTime: function(){
            var vstartMin = JsBoard.Common.getMinute(this.breakHour);
            var vendMin = vstartMin+60;
            var vBreakEnd = JsBoard.Common.getTimeString(vendMin);
            var vstart = this.timeToColumn(this.breakHour);
            var vend = this.timeToColumn(vBreakEnd);
            return [{
                start: vstart,
                end: vend,
                width: vend-vstart,
            }]
        },
        getBreakChipTime: function(vstart, vlength){
                var vbreaks = this.getBreakTime();
                var vbreakCount = 0;
                var vtotalBreak = 0;
                var vresult = {}
                for(var i=0; i<vbreaks.length; i++){
                    vbreak = vbreaks[i];
                    if (this.isOverlapped(vstart, vstart+vlength, vbreak.start, vbreak.end)){
                        if (vstart>vbreak.start){
                            return 'start-inside-break';
                        }
                        vbreakCount = vbreakCount+1;
                        vresult['break'+vbreakCount+'start'] = vbreak.start-vstart;
                        vresult['break'+vbreakCount+'width'] = vbreak.width;
                        vtotalBreak = vtotalBreak+vbreak.width;
                    }
                }
                if (vbreakCount==0){
                    return 'no-break';
                } else {
                    vresult['breakWidth'] = vtotalBreak;
                }
                return vresult;

            },
        updateChipBreak: function(vchipData){
            if (vchipData.breakWidth){
                vchipData.width = vchipData.width-vchipData.breakWidth;
            }
            var vbreaks = 'no-break';
            if (vchipData.isPrediagnose){
                if (vchipData.PrediagStallId!=0){
                    vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width);
                }
            } else {
                if (vchipData.stallId!=0){
                    vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width);
                }

            }

            var deleteList = ['break1start', 'break1width', 'break2start', 'break2width', 'break3start', 'break3width','break4start', 'break4width','breakWidth'];
            for (var i=0; i<deleteList.length; i++){
                var idx = deleteList[i];
                delete vchipData[idx];
            }
            if (typeof vbreaks=='object'){
                for (var idx in vbreaks){
                    vchipData[idx] = vbreaks[idx];
                }
            }

            if (vchipData.breakWidth){
                vchipData.width = vchipData.width+vchipData.breakWidth;
                vchipData.width1 = vchipData.break1start;
                vchipData.start2 = vchipData.break1start+vchipData.break1width;
                vchipData.width2 = vchipData.width-vchipData.start2;
            } else {
                vchipData.width1 = vchipData.width;
            }
            // console.log('vchipData', vdata);

            // console.log('vbreaks', vbreaks);
            //////////
        },
        isOverlapped: function(x1, x2, y1, y2){
            return (x1 < y2) && (y1 < x2)
        },
        isOverlapped2: function(x1, x2, y1, y2){
            return (x1 <= y2) && (y1 < x2)
        },
        isOverlapped3: function(x1, x2, y1, y2){
            return (x1 < y2) && (y1 <= x2)
        },


        getFirstHour: function(){
            return this.startHour;
        },
        getLastHour: function(){
            var vendmin = JsBoard.Common.getMinute(this.endHour);
            vendmin = vendmin+60;
            return JsBoard.Common.getTimeString(vendmin);
        },
        getIntRandom: function(){
            return Math.floor(Math.random() * 100000);
        },
        timerLoop: function(vname, vcode){
            var vme = this;
            var vdate = $filter('date')(new Date(), 'HH:mm:ss');
            vme.updateTimeLine(vname);
            console.log('timerLoop', vdate, vcode);
            if (vme.timerCode==vcode){
                $timeout( function(){
                    var vscode = vcode;
                    vme.timerLoop(vname, vscode);
                }, 60000);
            }

        },

        updateTimeLine: function(boardname){
            // var boardname = this.boardName;
            var vDate = this.getCurrentTime();
            // console.log("current Time#####", vDate.getTime());
            var currentHour = $filter('date')(vDate, 'HH:mm');
            var currentDate = $filter('date')(vDate, 'yyyy-MM-dd');

            // console.log("currentHour", currentHour);
            //var startHour = this.
            //var minutPerCol

            // asbGrFactoryTest

            var vboard = jpcbFactory.getBoard(boardname);
            if (vboard){
                var xdate = vboard.config.argChips.currentDate;
                var boardDate = $filter('date')(xdate, 'yyyy-MM-dd');
                if (boardDate > currentDate){
                    currentHour = this.getFirstHour();
                } else if (boardDate < currentDate){
                    currentHour = this.getLastHour();
                }
            }
            jpcbFactory.updateTimeLine(boardname, currentHour, this.startHour, this.incrementMinute);

            //this.updateChipStatus(boardname);

            //this.checkChipStatus(boardname);

            // var vboard = jpcbFactory.getBoard(boardname);
            // if (vboard){
            //     vboard.redrawChips();
            // }

            //jpcbFactory.updateStandardDraggable(boardname);
            jpcbFactory.redrawBoard(boardname);
        },


        
    }
});
