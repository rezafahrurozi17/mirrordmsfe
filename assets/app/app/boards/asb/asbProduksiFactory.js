angular.module('app')
    .factory('AsbProduksiFactory', function($http, CurrentUser, $filter, $timeout, jpcbFactory) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            minuteDelay: 10*60+10,
            //minuteDelay: 0,
            onPlot: function(){
                // do nothing
            },
            showBoard: function(config){
                var vcontainer = config.container;
                var vname = 'asbgr';
                if (typeof config.boardName !== 'undefined'){
                    vname = config.boardName;
                }
                var vobj = document.getElementById(vcontainer);
                if(vobj){
                    var vwidth = vobj.clientWidth;
                    console.log(vwidth);
                    if (vwidth<=0) {
                        return;
                    }
                } else {
                    return;
                }
                if (typeof config.onPlot=='function'){
                    this.onPlot = config.onPlot;
                }

                var vdurasi = 1;
                if (typeof config.currentChip.durasi !=='undefined'){
                    vdurasi = config.currentChip.durasi;
                }
                config.currentChip.width = 7; //(vdurasi/60)*this.incrementMinute;

                var vrow = -2;
                // if (typeof config.currentChip.stallId !=='undefined'){
                //     if (config.currentChip.stallId>0){
                //         vrow = config.currentChip.stallId - 1 ;
                //     }
                // }
                config.currentChip.row = vrow;

                var vhourStart = JsBoard.Common.getMinute(this.startHour)/60;
                var vcol = 0;
                if (typeof config.currentChip.startTime !=='undefined'){
                    vstartTime = config.currentChip.startTime;
                    vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                }
                config.currentChip.col = vcol;
                var voptions = {
                    board: {
                        chipGroup: 'asb_produksi',
                        columnWidth: 24,
                        rowHeight: 90,
                        footerChipName: 'footer',
                        snapDivider: 1,
                        firstColumnWidth: 80,
                        firstHeaderChipName: 'first_header',
                        fixedHeaderChipName: 'fixed_header',
                        headerChipName: 'header',
                        firstColumnTitle: '',
                    },
                    chip: {
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute,
                        itemCount: 5,
                    },
                    argStall: {
                        BpCategoryId: '0',
                        BpCategoryName: '...'
                    }
                }

                var xvoptions = voptions;
                if (typeof config.options !== 'undefined'){
                    xvoptions = JsBoard.Common.extend(voptions, config.options);
                    if (typeof config.options.board !== 'undefined'){
                        xvoptions.board = JsBoard.Common.extend(voptions.board, config.options.board);
                    }
                    if (typeof config.options.argStall !== 'undefined'){
                        xvoptions.argStall = JsBoard.Common.extend(voptions.argStall, config.options.argStall);
                    }
                }
                jpcbFactory.createBoard(vname, xvoptions);
                var me = this;
                jpcbFactory.setHeader(vname, {
                    headerHeight: 40,
                    onGetHeader: function(arg, vfunc){
                        return me.onGetHeader(arg, vfunc);
                    }
                });
                jpcbFactory.setStall(vname, {
                    //firstColumnWidth: 120,
                    onGetStall: function(arg, vfunc){
                        return me.onGetStall(arg, vfunc);
                    }
                });

                jpcbFactory.setOnGetChips(vname, function(arg, vfunc){
                    return me.onGetChips(arg, vfunc);
                });

                jpcbFactory.setOnChipLoaded(vname, function(arg, vfunc){
                    return me.onChipLoaded(arg, vfunc);
                });
                
                jpcbFactory.setSelectionArea(vname, {
                    tinggiArea: 115,
                    tinggiHeader: 32,
                    tinggiSubHeader: 32,

                });

                jpcbFactory.setArgChips(vname, config.argChips);
                jpcbFactory.setArgStall(vname, config.argStall);
                jpcbFactory.setArgHeader(vname, config.argHeader);

                // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

                jpcbFactory.setOnCreateChips(vname, function(vchip){
                    if (vchip.data.selected){
                        vchip.draggable(true);
                    }
                });


                var me = this;
                var the_config = config;
                jpcbFactory.setOnDragDrop(vname, function(vchip){
                    var curDate = config.currentChip.currentDate;
                    var vdurasi = 1;
                    if (typeof config.currentChip.durasi !=='undefined'){
                        vdurasi = config.currentChip.durasi;
                    }

                    var vmin = me.incrementMinute * vchip.data['col'];
                    var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                    var vres = JsBoard.Common.getTimeString(vminhour);
                    var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+vres);
                    //startTime.setMinutes(startTime.getMinutes()+vmin);
                    //startTime.setMinutes(vminhour);

                    var emin = vminhour + vdurasi * 60;
                    var eres = JsBoard.Common.getTimeString(emin);
                    var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                    //endTime.setMinutes(endTime.getMinutes()+emin);

                    vchip.data.startTime = startTime;
                    vchip.data.endTime = endTime;
                    var vStallId = 0;
                    if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                        && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                        && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                        vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                    }
                    vchip.data.stallId = vStallId;
                    console.log("on plot data", vchip.data);

                    if (typeof me.onPlot=='function'){           

                        me.onPlot(vchip.data);
                    }
                    console.log("on drag-drop vchip", vchip);
                });

                jpcbFactory.showBoard(vname, vcontainer)
                // $timeout(function(){
                //     jpcbFactory.showBoard(vname, vcontainer)

                // }, 100);

            },
            headerIndex: {},
            headerData: [],
            startDate: false,
            startHour: '08:00',
            endHour: '17:00',
            breakHour: '12:00',
            incrementMinute: 60,

            getCurrentTime: function(){
                return jpcbFactory.getCurrentTime();
            },
            isProblem: function(vitem){
                return jpcbFactory.isJobProblem(vitem);
            },
            timeToColumn: function(vtipe, vdate, vtime){
                if (vtipe=='tps'){
                    var vres = false;
                    var vindex = $filter('date')(vdate, 'yyyy-MM-dd.');
                    vindex = vindex+vtime.substring(0, 2)+':00';
                    var vmin = vtime.substring(3, 2);
                    for (var i=0; i<this.headerData.length; i++){
                        var vitem = this.headerData[i];
                        if (vindex==vitem.dayIndex){
                            vres = i;
                            break;
                        } else {
                            if ((vindex<vitem.dayIndex)&&i==0){
                                break;
                            } else if (vindex<vitem.dayIndex){
                                vres = i;
                                break;
                            }
                        }
                    }
                    if (vmin!=='00'){
                        var vxmin = JsBoard.Common.getMinute('00:'+vmin);
                        vres = vres+vxmin/60;
                    }
                    return vres;
                }
                return 0;
                // var vstartmin = JsBoard.Common.getMinute(this.startHour);
                // var vmin = JsBoard.Common.getMinute(vtime)-vstartmin;
                // var vresult = vmin/this.incrementMinute;
                
                // return vresult;
            },
            onGetHeader: function(arg, onFinish){
                // varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
                // var vWorkHour = JsBoard.Common.generateDayHours({
                //     startHour: this.startHour,
                //     endHour: this.endHour,
                //     increment: this.incrementMinute,
                // });
                // var result = [];
                // for(var idx in vWorkHour){
                //     var vitem = {title: vWorkHour[idx]}
                //     if (vWorkHour[idx]==this.breakHour){
                //         vitem.colInfo = {isBreak: true}
                //     }
                //     result.push(vitem)

                // }
                // console.log("header", result);
                // $timeout(function(){
                //     onFinish(result);
                // }, 100);

                var one_day=1000*60*60*24;
                var difference_ms = arg.endDate.getTime() - arg.startDate.getTime();
                var daydiff = Math.round(difference_ms/one_day);
                if (daydiff<=2){
                    daydiff = 2;
                }

                result = this.getHeader('tps', arg.startDate, daydiff);
                this.headerData = result;
                $timeout(function(){
                    onFinish(result);
                }, 10);
                
            },

            getHeader: function(vtype, vparam1, vparam2){

                //return [{text: '1'}, {text: '2'}, {text: '3'}, {text: '4'}]
                // type: 'hourly'; param1: start date, param2: date count
                // type: 'daily'; param1: start date, param2: date count
                // type: 'weekly'; param1: start date, param2: date count
                if (vtype.toLowerCase()=='tps'){
                    if (typeof vparam1 == 'undefined'){
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined'){
                        vparam2 = 7;
                    }
                    var vres = this.getHourlyList(vparam1, vparam2);
                    console.log("hourly list", vres);
                    return vres;
                } else if (vtype.toLowerCase()=='light'){
                    if (typeof vparam1 == 'undefined'){
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined'){
                        vparam2 = 7;
                    }
                    var vres = this.getDailyList(vparam1, vparam2);
                    console.log("daily list", vres);
                    return vres;
                } else if (vtype.toLowerCase()=='medium'){
                    if (typeof vparam1 == 'undefined'){
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined'){
                        vparam2 = 14;
                    }
                    var vres = this.getDailyList(vparam1, vparam2);
                    console.log("daily list", vres);
                    return vres;
                } else if (vtype.toLowerCase()=='heavy'){
                    if (typeof vparam1 == 'undefined'){
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined'){
                        vparam2 = 30;
                    }
                    var vres = this.getDailyList(vparam1, vparam2);
                    console.log("daily list", vres);
                    return vres;
                }
            }, 
            getWorkHour: function(){
                return JsBoard.Common.generateDayHours({
                    startHour: '08:00',
                    endHour: '17:00',
                    increment: 60,
                })
            },
            getHourlyList: function(startDate, vcount){
                var breakHour = '12:00';
                var result = []
                var vidx = 0;
                var vWorkHour = this.getWorkHour();
                var item = false
                var vDate = new Date(startDate);
                var currentDate = '******';
                for (var i=0; i<vcount; i++){
                    var newDay = true;
                    var subCount = vWorkHour.length;
                    currentDate = $filter('date')(vDate, 'dd MMMM yyyy');
                    var dindex = $filter('date')(vDate, 'yyyy-MM-dd.');
                    var vindex = '';
                    for(var idx in vWorkHour){
                        var vpush = true;
                        vindex = dindex+vWorkHour[idx].substring(0, 5);
                        if (newDay){
                            item = {newDay: newDay, parentText: currentDate, subCount: subCount, text: vWorkHour[idx].substring(0, 2)}
                        } else {
                            item = {newDay: newDay, text: vWorkHour[idx].substring(0, 2)}
                        }
                        item.dayIndex = vindex;
                        if (vWorkHour[idx]==breakHour){
                            item.colInfo = {isBreak: true}
                            vpush = false;
                        }
                        if (vpush){
                            result.push(item);
                        }
                        var newDay = false;
                    }
                    vDate.setDate(vDate.getDate()+1);
                }
                return result;
            },

            // onGetHeader: function(arg){
            //     varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
            //     var vWorkHour = JsBoard.Common.generateDayHours({
            //         startHour: this.startHour,
            //         endHour: this.endHour,
            //         increment: this.incrementMinute,
            //     });
            //     var result = [];
            //     for(var idx in vWorkHour){
            //         var vitem = {title: vWorkHour[idx]}
            //         if (vWorkHour[idx]==this.breakHour){
            //             vitem.colInfo = {isBreak: true}
            //         }
            //         result.push(vitem)

            //     }
            //     console.log("header", result);
            //     return result;
            // },
            onGetStall: function(arg, onFinish){
                // // $http.get('/api/as/Stall').
                // var vparent = '0';
                // $http.get('/api/as/Boards/Stall/'+vparent).then(function(res){
                //     var xres = res.data.Result;
                //     var result = [];
                //     for(var i=0;  i<xres.length; i++){
                //         var vitem = xres[i];
                //         result.push({
                //             title: vitem.Name,
                //             status: 'normal',
                //             stallId: vitem.StallId
                //         });
                //     }
                //     onFinish(result);
                // })
                var vcount = 7;
                var vres = []
                var vstallText = arg.BpCategoryName;
                if (typeof vstallText == 'undefined'){
                    vstallText = '';
                }
                for (var i=0; i<vcount; i++){
                    var xstall = {Stallid: i, Name: 'Stall-'+i}
                    if (i==vcount-1){

                        var chcount = vstallText.length;
                        var vstr = '';
                        for(var j=0; j<vstallText.length; j++){
                            vstr = vstr+vstallText[j]+"\r\n";
                        }
                        xstall.visible = true;
                        xstall.itemCount = vcount;
                        xstall.textCount = chcount;
                        xstall.title = vstr;
                    } else {
                        xstall.visible = false;
                    }
                    vres.push(xstall);
                }
                $timeout(function(){
                    onFinish(vres);
                }, 100);
            },
            // onGetStall: function(arg){
            //     return[{title: 'Diagnose', status: 'normal', person: 'Joko'},
            //         {title: 'EM 1', status: 'normal', person: 'Ton'},
            //         {title: 'EM 2', status: 'normal', person: 'And'},
            //         {title: 'ST 1', status: 'normal', person: 'Rul'},
            //         {title: 'ST 2', status: 'normal', person: 'Fir'},
            //         {title: 'S3 3', status: 'normal', person: 'Ron'},
            //     ]
            // },
            findStallIndex: function(vStallId, vList){
                for (var i=0; i<vList.length; i++){
                    var vitem = vList[i];
                    if ((typeof vitem.stallId!=='undefined') && (vitem.stallId==vStallId)){
                        return i;
                    }
                }
                return -1;
            },
            onChipLoaded: function(vjpcb){
                for (var i=0; i<vjpcb.allChips.length; i++){
                    var vitem = vjpcb.allChips[i];
                    vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                    if (vidx>=0){
                        vitem.row = vidx;
                    }                    
                    
                }
            },
            isOverlap: function(vcol1, vrow1, vlen1, vcol2, vrow2, vlen2){
                if (vrow1==vrow2){
                    vcawal = vcol2;
                    vcakhir = vcol2+vlen2;
                    if( ((vcawal>=vcol1) && (vcawal<=vcol1+vlen1)) || ((vcakhir>=vcol1) && (vcakhir<=vcol1+vlen1)) ){
                        return true;
                    }
                }
                return false;
            },
            findStallRow: function(vlist, vchip, vrcount){                
                
                for (var i=0; i<vrcount; i++){
                    var overlap = false;
                    for (var j=0; j<vlist.length; j++){
                        var vtest = vlist[j];
                        if (this.isOverlap(vtest.col, vtest.row, vtest.width, vchip.col, i, vchip.width)){
                            overlap = true;
                            break;
                        }
                    }
                    if (!overlap){
                        return i;
                    }
                }
                return -1;
            },
            onGetChips: function(arg, onFinish){
                console.log('arg-onGetChips', arg);
                var vArg = {nopol: 'B 1000 AAA', tipe: 'Avanza', row: -2, col: 0, width: 7, stallId: 0}
                vArg = JsBoard.Common.extend(vArg, arg);
                
                // var tglAwal = arg.currentDate;
                // var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
                // tglAkhir.setDate(tglAkhir.getDate());
                // var vurl = '/api/as/Boards/Appointment/1/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/'+$filter('date')(tglAkhir, 'yyyy-MM-dd');
                // var vMinMaxStatus = '/0/17';
                // var vurl = '/api/as/Boards/jpcb/bydate/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+vMinMaxStatus;

                var vtgl1 = $filter('date')(arg.startDate, 'yyyy-MM-dd');
                var vtgl2 = $filter('date')(arg.endDate, 'yyyy-MM-dd');
                // var vurl = '/api/as/Boards/BP/AppointmentList/'+arg.BpCategory.id+'/'+vtgl1+'/'+vtgl2;
                var vurl = '/api/as/Boards/BP/GetBPJobListNoJobId/'+arg.BpCategory.id+'/'+vtgl1+'/'+vtgl2+'/'+0;
                var me = this;
                $http.get(vurl).then(function(res){
                    var result = [];
                    if (!(typeof vArg.visible !== 'undefined' && !vArg.visible)) {
                        result = [{chip: 'produksi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: 7, xwidth: vArg.width, selected: 1, 
                            stallId: vArg.stallId,
                            daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}];
                    }
                    var xres = res.data.Result;
                    var checkJobId = false;
                    if (typeof vArg.jobId!=='undefined'){
                        checkJobId = true;
                    }
                    //var vList = []
                    var vrow = 7;
                    var vcol = 7*8;
                    //var caData = CADataFactory.create({});
                    for(var i=0;  i<xres.length; i++){
                        var vitem = xres[i];
                        var vcolumn = me.timeToColumn('tps', vitem.AppointmentDate, vitem.AppointmentTime);
                        //var vstallid = CADataFactory.findStall(caData, vitem.AppointmentDate, vitem.AppointmentTime, 7);
                        vstallid = 1;
                        var vdata = {
                            chip: 'produksi', 
                            nopol: vitem.PoliceNumber, 
                            tipe: vitem.ModelType, 
                            stallId: vstallid,
                            row: 0, col: vcolumn, 
                            // PlanDateStart: vitem.PlanDateStart,
                            // PlanDateFinish: vitem.PlanDateFinish,
                            // PlanStart: vitem.PlanStart,
                            // PlanFinish: vitem.PlanFinish,
                            width: 7, 
                        }
                        vdata.row = me.findStallRow(result, vdata, vrow);
                        if (vdata.row>=0){
                            result.push(vdata);
                        }
                        
                    }
                    onFinish(result);
                })
                

                // var vres = [];
                // var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
                // vArg = JsBoard.Common.extend(vArg, arg);
                // var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                // ]
                // var today = new Date();
                // if (arg.currentDate){
                //     var vdiff = this.dayDiff(today, arg.currentDate);
                //     for (var i=0; i<vitems.length; i++){
                //         if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
                //             vres.push(vitems[i]);
                //         }
                //     }
                // }

                // $timeout(function(){
                //     onFinish(vres);
                // }, 100);
            },

            // onGetChips: function(arg){
            //     var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
            //     vArg = JsBoard.Common.extend(vArg, arg);
            //     var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     ]
            //     var today = new Date();
            //     if (arg.currentDate){
            //         var vdiff = this.dayDiff(today, arg.currentDate);
            //         var vres = []
            //         for (var i=0; i<vitems.length; i++){
            //             if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
            //                 vres.push(vitems[i]);
            //             }
            //         }
            //         return vres;
            //     }
            //     return []
            // },
            dayDiff: function(date1, date2){
                return Math.round((date2 - date1) / (1000*60*60*24));
            },


            
        }
    });

/*
angular.module('app')
    .factory('AsbProduksiFactory', function($http, CurrentUser, $filter, jpcbFactory) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            showBoard: function(config){
                var vcontainer = config.container;
                var vname = 'estimasi';
                jpcbFactory.createBoard(vname, {
                    board: {
                        chipGroup: 'asb_produksi',
                        columnWidth: 24,
                        rowHeight: 90,
                        snapDivider: 1,
                    },
                    chip: {
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute
                    },
                    listeners: {
                        chip:{
                            click: function(){
                                alert('test');
                            }
                        }
                    }
                });
                var me = this;
                jpcbFactory.setHeader(vname, {
                    headerHeight: 40,
                    onGetHeader: function(arg){
                        return me.onGetHeader(arg);
                    }
                });
                jpcbFactory.setStall(vname, {
                    firstColumnWidth: 60,
                    onGetStall: function(arg){
                        return me.onGetStall(arg);
                    }
                });

                jpcbFactory.setOnGetChips(vname, function(arg){
                    return me.onGetChips(arg);
                });

                jpcbFactory.setSelectionArea(vname, {
                    tinggiArea: 110,
                    tinggiHeader: 30,
                    tinggiSubHeader: 24,

                });

                jpcbFactory.setArgChips(vname, config.currentChip);
                jpcbFactory.setArgHeader(vname, config.currentChip);
                jpcbFactory.setCheckDragDrop(vname, function(vchip, xcol, vrow){
                    var vtoday = new Date();
                    vcontainer = vchip.container;
                    if (vcontainer.layout.infoCols){
                        vcol = xcol+1;
                        console.log("vcontainer.layout.infoCols", vcontainer.layout.infoCols);
                        console.log("vcol", vcol);
                        if (vcontainer.layout.infoCols[vcol] && vcontainer.layout.infoCols[vcol].data && vcontainer.layout.infoCols[vcol].data.tanggal){
                            console.log("vcontainer.layout.infoCols", vcontainer.layout.infoCols[vcol]);
                            if (vcontainer.layout.infoCols[vcol].data.tanggal.getDate() < vtoday.getDate()){
                                return false;
                            }
                        }
                    }
                    return true;
                });

                jpcbFactory.showBoard(vname, vcontainer)

            },
            startHour: '08:00',
            endHour: '17:00',
            breakHour: '12:00',
            incrementMinute: 60,
            listJam: ['8', '9', '10', '11', '13', '14', '15', '16'],
            onGetHeader: function(arg){
                console.log('arg header', arg);

                varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 7}, arg);
                if ((varg.startDate) && (varg.endDate)){
                    varg.dateCount = this.dayDiff(varg.startDate, varg.endDate)+1;
                }
                var vresult = []
                var vListJam = this.listJam;

                if (varg.startDate){
                    var vdate = new Date(varg.startDate);
                } else {
                    var vdate = new Date();
                }
                
                for(var i=0; i<varg.dateCount; i++){
                    var vshowParent = true;
                    for (var j=0; j<vListJam.length; j++){
                        var vdayname = JsBoard.Common.localDayName($filter('date')(vdate, 'EEEE'));
                        var vtgl = vdayname+', '+$filter('date')(vdate, 'dd/MM/yyyy');
                        vdata = {title: vListJam[j]+'', parentText: vtgl, itemCount: vListJam.length, showParent: vshowParent, tanggal: new Date(vdate)}
                        vresult.push(vdata);
                        vshowParent = false;
                    }
                    vdate.setDate(vdate.getDate()+1);
                    
                }
                console.log("vresult", vresult);
                return vresult;

                // var vWorkHour = JsBoard.Common.generateDayHours({
                //     startHour: this.startHour,
                //     endHour: this.endHour,
                //     increment: this.incrementMinute,
                // });
                // var result = [];
                // for(var idx in vWorkHour){
                //     var vitem = {title: vWorkHour[idx]}
                //     if (vWorkHour[idx]==this.breakHour){
                //         vitem.colInfo = {isBreak: true}
                //     }
                //     result.push(vitem)

                // }
                // console.log("header", result);
                // return result;
            },
            onGetStall: function(arg){
                return[
                    {title: 'Surveyor 2', status: 'normal', person: 'Ton', visible: false},
                    {title: 'Surveyor 3', status: 'normal', person: 'And', visible: false},
                    {title: 'SA 1', status: 'normal', person: 'Rul', visible: false},
                    {title: 'SA 2', status: 'normal', person: 'Fir', visible: false},
                    {title: 'SA 3', status: 'normal', person: 'Ron', visible: false},
                    {title: 'SA 4', status: 'normal', person: 'Bud', visible: false},
                    {title: "T\r\nP\r\nS\r\n", status: 'normal', person: 'Dod', visible: true, itemCount: 7},
                ]
            },
            onGetChips: function(arg){
                var vwidth = 7;
                var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
                vArg = JsBoard.Common.extend(vArg, arg);
                var vitems = [{chip: 'produksi', nopol: vArg.nopol, tipe: vArg.tipe, row: -2, col: 0, width: vwidth, selected: 1, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'MAN'},
                    {chip: 'produksi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'MIN'},
                    {chip: 'produksi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 1, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'DOD'},
                    {chip: 'produksi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 9, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'JOK'},
                    {chip: 'produksi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 2, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'DON'},
                    {chip: 'produksi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 10, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'LUD'},
                    {chip: 'produksi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 3, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'FAN'},
                    {chip: 'produksi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 4, col: 4, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'SUD'},
                    {chip: 'produksi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 4, col: 12, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'SBA'},
                    {chip: 'produksi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 4, col: 20, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'HPO'},
                ]
                // return vitems;
                var today = new Date();
                if (arg.startDate){
                    var vshift = this.dayDiff(arg.startDate, today);
                    var vres = []
                    for (var i=0; i<vitems.length; i++){
                        if (vitems[i].row>=0){
                            vcol = vitems[i].col+vshift*this.listJam.length;
                            if (vcol>=0){
                                vitems[i].col = vcol;
                                vres.push(vitems[i]);
                            }
                        } else {
                            vres.push(vitems[i]);
                        }
                    }
                    return vres;
                }
                return []
            },
            dayDiff: function(date1, date2){
                return Math.round((date2 - date1) / (1000*60*60*24));
            }

            
        }
    });
*/