angular.module('app')
.controller('PartsOrderController', function($scope, $http, CurrentUser, Parameter, ngDialog, $timeout, $window, $filter, Tabs, Idle) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    // $scope.$on('$viewContentLoaded', function() {
      $scope.$on('$viewContentLoaded', function() {
        $scope.countdown();
        $scope.heightHeader = $('#AdvSearch').height();
        Idle.unwatch();
        console.log('==== value of idle ==', Idle)
      });
      $scope.$on('$destroy', function(){
        Idle.watch();
      });
        $scope.heightHeader=null;
        var arrBoard = []
        var allBoardHeight = 540;
        var stopped;
        var pause = false;
        $scope.loading=true;
        $scope.gridData=[];
        $scope.waktu_sekarang = 'xxxx';
        $scope.loading = [true, true, true, true]

        $scope.count = {}
        $scope.clock = "loading clock..."; // initialise the time variable
        $scope.tickInterval = 1000 //ms
        $scope.counter = 30;
        $scope.counter = 10 //300;
        $scope.hourminute = '';

        $scope.porderboard = {}

        // Initialize ====

        var showData = 4;
        var countersBoard = {
          waiting_part_no_info:{
            StartCountingData:0,
            Data:[],
            Counts:0,
            BagiCountingData:0,
            ModulusCountingData:0,
            ClearData:true
          },
          waiting_order:{
            StartCountingData:0,
            Data:[],
            Counts:0,
            BagiCountingData:0,
            ModulusCountingData:0,
            ClearData:true
          },
          status_order_parts:{
            StartCountingData:0,
            Data:[],
            Counts:0,
            BagiCountingData:0,
            ModulusCountingData:0,
            ClearData:true
          },
          status_rpp_claim:{
            StartCountingData:0,
            Data:[],
            Counts:0,
            BagiCountingData:0,
            ModulusCountingData:0,
            ClearData:true
          }
        }

        // Auto Refresh
        $scope.countdown = function() {
          if (Tabs.isExist('app.partsorderboard')) {
            stopped = $timeout(function() {
              //console.log($scope.counter);
              if (!pause) {
                $scope.counter--;
                if($scope.counter == 0)
                {
                  console.log("====> refresh data ...");
                  //[START] refresh data

                  loadAllData();
                  $scope.porderboard.doFilter();
                  $scope.porderboard.resetFilter();

                  // [END] refresh data

                  $scope.counter = 30;
                  $scope.counter = 10//300;
                }
                $scope.countdown();
              }
            }, 1000);
          }
        };

        $scope.porderboard.doFilter= function(){
          console.log(this);
          vidx = this.category_filter;
          var vopt = {
            fieldname: this.field_filter,
            value: this.text_filter
          }
          for (var i=1; i<=arrBoard.length; i++){
            if ((i==vidx) || (vidx==0)){
              dataFiltering(arrBoard[i-1], vopt)
            } else {
              dataFiltering(arrBoard[i-1], {fieldname: '', value: ''})
            }
          }
        }
        $scope.porderboard.resetFilter= function(){
          console.log("reset-filter", this);
          this.category_filter = "0";
          this.field_filter = "all";
          this.text_filter = '';
          this.doFilter();
        }
        $scope.porderboard.resetFilter();

        var filterItem = function(vitem, vfield, vtext){
          if (vtext===''){
            return true;
          } else {
            if ((typeof vitem[vfield] !== 'undefined') && (vitem[vfield] !== null)){
              return vitem[vfield].search(new RegExp(vtext, "i"))>=0
              // if (vitem[vfield] === vtext) {
              //   return true;
              // }
            }
            return false;
          }
        }

        var filterAllField = function(vitem, vtext){
          if (vtext===''){
            return true;
          } else {
            for(vfield in vitem){
              if (typeof vitem[vfield] !== 'undefined'){
                var vstr = vitem[vfield]+'';
                var vresult = vstr.search(new RegExp(vtext, "i"))>=0
                if (vresult){
                  return true;
                }
                // if (vitem[vfield] === vtext) {
                //   return true;
                // }
              }
            }
            return false;
          }
        }

        var filterList = {
          'all': function(vdata, vtext){
            return filterAllField(vdata, vtext);
          },
          'nopol': function(vdata, vtext){
            return filterItem(vdata, 'nopol', vtext);
          },
          'appointment': function(vdata, vtext){
            return (vdata['TipeRequest']=='B') && filterItem(vdata, 'docno', vtext);
          },
          'wo': function(vdata, vtext){
            return (vdata['TipeRequest']=='W') && filterItem(vdata, 'docno', vtext);
          },
          'so': function(vdata, vtext){
            return (vdata['TipeRequest']=='S') && filterItem(vdata, 'docno', vtext);
          },
          'rpp': function(vdata, vtext){
            return (vdata['statusname']=='rpp') && filterItem(vdata, 'no_gr', vtext);
          },
          'claim': function(vdata, vtext){
            return (vdata['statusname']=='claim') && filterItem(vdata, 'no_gr', vtext);
          }
        }

        var firstTime = true;

        var filterData = function(vdata, vopt){
          if (vopt.fieldname===''){
            result = vdata;
          } else {
            result = [];
            for(var i=0; i<vdata.length; i++){
              if (typeof filterList[vopt.fieldname] !=='undefined'){
                if (filterList[vopt.fieldname](vdata[i], vopt.value)){
                  result.push(vdata[i]);
                }
              }
            }
          }
          return result;
        }
        var dataFiltering = function(vitem, voption){
          vdata = filterData(vitem.originalData, voption);
          vboard = vitem.board;
          console.log("vdata.length",vitem,voption);
          vboard.createChips(vdata);
          vboard.autoResizeStage();
          var xcounter = 0;
          vboard.redraw();
          $scope.count[vitem.boardName] = vdata.length;
        }


        var loadAllData = function(){

          for (var i=0; i<arrBoard.length; i++){
              var vboard = arrBoard[i];
              if(vboard.dataUrl){
                  $http({
                      method: 'GET',
                      url: vboard.dataUrl,
                      index: i,
                      board: vboard,
                  })
                    .then(function(resp){
                        var vboard = resp.config.board;
                        var tmpVboard = angular.copy(resp);
                        // ======================
                        var tmpData = [];
                        countersBoard[vboard.boardName].Data = [];
                        // countersBoard[vitem.boardName].Counts = vdata.length;
                        if (countersBoard[vboard.boardName].ClearData == true){
                            countersBoard[vboard.boardName].Counts = tmpVboard.data.Result.length;
                            countersBoard[vboard.boardName].ModulusCountingData = parseInt(countersBoard[vboard.boardName].Counts % showData);
                            countersBoard[vboard.boardName].BagiCountingData = parseInt(countersBoard[vboard.boardName].Counts / showData);
                            // if (countersBoard[vboard.boardName].ModulusCountingData != 0){
                            //   countersBoard[vboard.boardName].BagiCountingData = countersBoard[vboard.boardName].BagiCountingData + 1;
                            // }
                        }

                        

                        if(countersBoard[vboard.boardName].StartCountingData < countersBoard[vboard.boardName].Counts ){
                          countersBoard[vboard.boardName].ClearData = false;

                          // ======== if just follow the showdata
                          // var tmpLength = 0;
                          // if(countersBoard[vboard.boardName].StartCountingData + showData > tmpVboard.data.Result.length ){
                          //   tmpLength = countersBoard[vboard.boardName].StartCountingData + countersBoard[vboard.boardName].ModulusCountingData;
                          // }else{
                          //   tmpLength = countersBoard[vboard.boardName].StartCountingData + showData ;
                          // }
                          // ===================================

                          // for(var i = countersBoard[vboard.boardName].StartCountingData; i < tmpLength; i++){
                          //   countersBoard[vboard.boardName].Data.push(tmpVboard.data.Result[i]);
                          // }

                          var max = tmpVboard.data.Result.length;
                          if ((countersBoard[vboard.boardName].StartCountingData+showData) > tmpVboard.data.Result.length){
                            max = tmpVboard.data.Result.length;
                          } else {
                            max = countersBoard[vboard.boardName].StartCountingData+showData
                          }
                          

                          for(var i = countersBoard[vboard.boardName].StartCountingData; i < max; i++){
                            countersBoard[vboard.boardName].Data.push(tmpVboard.data.Result[i]);
                          }
                          // ===================================
                          countersBoard[vboard.boardName].StartCountingData = countersBoard[vboard.boardName].StartCountingData + showData;
                        }else{
                          countersBoard[vboard.boardName].StartCountingData = 0;
                          countersBoard[vboard.boardName].BagiCountingData = 0;
                          countersBoard[vboard.boardName].ClearData = true;

                          var max = tmpVboard.data.Result.length;
                          if ((countersBoard[vboard.boardName].StartCountingData+showData) > tmpVboard.data.Result.length){
                            max = tmpVboard.data.Result.length;
                          } else {
                            max = countersBoard[vboard.boardName].StartCountingData+showData
                          }

                          for(var i = countersBoard[vboard.boardName].StartCountingData; i < max; i++){
                            countersBoard[vboard.boardName].Data.push(tmpVboard.data.Result[i]);
                          }
                          countersBoard[vboard.boardName].StartCountingData = countersBoard[vboard.boardName].StartCountingData + showData;

                        }

                        // if(countersBoard[vboard.boardName].BagiCountingData != 0){
                        //   countersBoard[vboard.boardName].BagiCountingData = countersBoard[vboard.boardName].BagiCountingData - 1;
                        //   countersBoard[vboard.boardName].ClearData = false;
                        //   countersBoard[vboard.boardName].StartCountingData = countersBoard[vboard.boardName].StartCountingData + showData;
                        // }else{
                        //   countersBoard[vboard.boardName].StartCountingData = 0;
                        //   countersBoard[vboard.boardName].BagiCountingData = 0;
                        //   countersBoard[vboard.boardName].ClearData = true;
                        // }
                        
                        console.log("countersBoard ===========>",countersBoard);
                        resp.config.board.originalData = [];
                        resp.data.Result = [];
                        resp.config.board.originalData = countersBoard[vboard.boardName].Data;
                        resp.data.Result = countersBoard[vboard.boardName].Data;
                      // ======================
                        console.log('resp ===>',resp, resp.config.board.originalData);
                        if (vboard.processData){
                            resp.config.board.originalData = [];
                            vboard.originalData = vboard.processData(resp);
                            $scope.porderboard.doFilter();
                            $scope.loading[resp.config.index] = false;
                        }
                  });

                  // $http.get(vboard.dataUrl, function(resp){
                  //     if (vboard.processData){
                  //         vboard.boardData = vboard.processData(resp);
                  //         $scope.porderboard.doFilter();
                  //     }
                  // });
              } else {
                  var vindex = i;
                  $timeout(function(){
                      //var vboard = resp.config.board;
                      vboard.originalData = []
                      $scope.porderboard.doFilter();
                      $scope.loading[vindex] = false;
                  },400)
              }
              //arrBoard[i].loadData(arrBoard[i]);
          }

          // console.log(this);
          // vidx = this.category_filter;
          // var vopt = {
          //   fieldname: this.field_filter,
          //   value: this.text_filter
          // }
          // for (var i=1; i<=arrBoard.length; i++){
          //   if ((i==vidx) || (vidx==0)){
          //     dataFiltering(arrBoard[i-1], vopt)
          //   } else {
          //     dataFiltering(arrBoard[i-1], {fieldname: '', value: ''})
          //   }
          // }
        }

        var registerBoard = function(bConfig){
          var vboard = new JsBoard.Container(bConfig.boardConfig);
          var vitem = {
            board: vboard,
            originalData: bConfig.boardData,
            incField: bConfig.incField,
            incMode: bConfig.incMode, //inc or dec
            boardName: bConfig.boardName,
            dataUrl: bConfig.dataUrl,
            processData: bConfig.processData,
          }
          arrBoard.push(vitem);
          var voption = {
            fieldname: '',
            value: ''
          }
          // dataFiltering(vitem, voption);
        }

        var updateAutoResize = function(){
            for (var i=0; i<arrBoard.length; i++){
              vbr = arrBoard[i];
              vbr.board.autoResizeStage();
              vbr.board.redraw();
              //vbr.redrawChips();
            }
        }

        var velement = document.getElementById('parts-order-boards-panel');
        var erd = elementResizeDetectorMaker();
        erd.listenTo(velement, function(element) {
          updateAutoResize();
        });


        var incrementTime = function(varrdata, vfield){
          for(var i=0; i<varrdata.length; i++){
            var vdata = varrdata[i];
            var vmin = JsBoard.Common.getMinute(vdata[vfield]);
            var vinc = 1;
            var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
            vdata[vfield] = vsmin;
          }
        }

        var decrementTime = function(varrdata, vfield){
          for(var i=0; i<varrdata.length; i++){
            var vdata = varrdata[i];
            var vmin = JsBoard.Common.getMinute(vdata[vfield]);
            var vinc = -1;
            var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
            vdata[vfield] = vsmin;
          }
        }

        var simulateWaitPartNoDate = function(vdata){
          var vxdif = 2;
          for (var i=0; i<vdata.length; i++){
            var today = new Date();
            var xdate1 = new Date();
            xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
            var xdate2 = new Date();
            xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
            var vtime = vdata[i].jam;
            if((vdata[i].daydif+vxdif)==0){
              var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
              vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
            }
            vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
            vdata[i].tgl_diperlukan = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
            var vdays = Math.round((today-xdate2)/(24*60*60*1000));
            var xdays;
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
            vdata[i].daywait = xdays;
            vdata[i].dday = vdays;

          }
          console.log('**** vdata ****', vdata);
        }

        // berdasarkan nilai terkecil, satuan jam
        // harus terurut berdasarkan day terkecil, kemudian jam terkecil
        var orderDayColorRange = [
          {day: -1, color: '#fff'}, // day <-2
          {day: 0, color: '#0e0'},  // day <0 (yesterday)
          {day: 1, color: 'hour_color', default: '#ec0'}, // day <1 (today) --> check hour color, if hour color not defined, use default
          {day: 9999, color: '#900'}, // day<9999 (other day), use this color
        ]
        var orderDayColorRange2 = [
          {day: -1, color: '#0e0'}, // day <-2
          {day: 0, color: '#0e0'},  // day <0 (yesterday)
          {day: 1, color: 'hour_color', default: '#ec0'}, // day <1 (today) --> check hour color, if hour color not defined, use default
          {day: 9999, color: '#900'}, // day<9999 (other day), use this color
        ]
        var orderHourColorRange = [
          {hour: -1, color: '#ec0'}, // hour <-1 use this color
          {hour: 24, color: '#e00'}, // hour<9999 (other day), use this color
        ]
        var orderDayTextColorRange = [
          {day: -1, color: '#000'}, // day <-2
          {day: 9999, color: '#fff'}, // day<9999 (other day), use this color
        ]
        var orderDayTextColorRange2 = [
          {day: -1, color: '#fff'}, // day <-2
          {day: 0, color: '#fff'},  // day <0 (yesterday)
          {day: 1, color: '#fff'}, // day <1 (today)
          {day: 9999, color: '#fff'}, // day<9999 (other day), use this color
        ]

        var simulateWaitOrderDate = function(vdata){
          var vxdif = 2;
          for (var i=0; i<vdata.length; i++){
            var today = new Date();
            var xdate1 = new Date();
            xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
            var xdate2 = new Date();
            xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
            var vtime = vdata[i].jam;
            if((vdata[i].daydif+vxdif)==0){
              var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
              vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
            }
            vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
            vdata[i].tgl_diperlukan = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
            var vdays = Math.round((today-xdate2)/(24*60*60*1000));
            var xdays;
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
            vdata[i].daywait = xdays;
            vdata[i].dday = vdays;
            vdata[i].dayRange = orderDayColorRange;
            vdata[i].hourRange = orderHourColorRange;
            vdata[i].textDayRange = orderDayTextColorRange;
            vdata[i].textHourRange = [];
          }
          console.log('**** vdata ****', vdata);
        }

        var simulateStatusOrderDate = function(vdata){
          var vxdif = 2;
          for (var i=0; i<vdata.length; i++){
            var today = new Date();
            var xdate1 = new Date();
            xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
            var xdate2 = new Date();
            xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
            var vtime = vdata[i].jam;
            if((vdata[i].daydif+vxdif)==0){
              var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
              vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
            }
            vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
            vdata[i].tgl_diperlukan = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
            vdata[i].tgl_eta = $filter('date')(xdate1, 'dd/MM/yyyy')+' '+vtime;
            var vdays = Math.round((today-xdate2)/(24*60*60*1000));
            var xdays;
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
            vdata[i].daywait = xdays;
            vdata[i].dday = vdays;
            vdata[i].dayRange = orderDayColorRange;
            vdata[i].hourRange = orderHourColorRange;
            vdata[i].textDayRange = orderDayTextColorRange;
            vdata[i].textHourRange = [];
          }
          console.log('**** vdata ****', vdata);
        }

        var simulateStatusRppClaimDate = function(vdata){
          var vxdif = 2;
          for (var i=0; i<vdata.length; i++){
            var today = new Date();
            var xdate1 = new Date();
            xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
            var xdate2 = new Date();
            xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
            var vtime = vdata[i].jam;
            if((vdata[i].daydif+vxdif)==0){
              var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
              vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
            }
            vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
            vdata[i].tgl_gr = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
            vdata[i].tgl_eta = $filter('date')(xdate1, 'dd/MM/yyyy')+' '+vtime;
            var vdays = Math.round((today-xdate2)/(24*60*60*1000));
            var xdays;
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
            vdata[i].daywait = xdays;
            vdata[i].dday = vdays;
            vdata[i].dayRange = orderDayColorRange;
            vdata[i].hourRange = orderHourColorRange;
            vdata[i].textDayRange = orderDayTextColorRange;
            vdata[i].textHourRange = [];
          }
          console.log('**** vdata ****', vdata);
        }

        var simulateDeliveryDate = function(vdata){
          for (var i=0; i<vdata.length; i++){
            var today = new Date();
            var xdate1 = new Date();
            xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
            var xdate2 = new Date();
            xdate2.setDate(xdate2.getDate()+vdata[i].daydif+3);

            var vtime = vdata[i].start;
            if(vdata[i].daydif==0){
              var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
              vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
            }
            vdata[i].tgl_notif = $filter('date')(xdate1, 'dd/MM/yyyy')+' '+vtime;
            vdata[i].tgl_janji_serah = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vdata[i].start;
            var vdays = Math.round((today-xdate1)/(24*60*60*1000));
            var xdays;
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
            vdata[i].daywait = xdays;
            vdata[i].dday = vdays;

          }
          console.log('**** vdata ****', vdata);
        }


        var onTimer = function(){
            var vhmin= $filter('date')($scope.clock, 'hh:mm');
            // if ($scope.hourminute!==vhmin){
            //     $scope.hourminute = vhmin;
            //     if (!firstTime){
            //         for(var i=0; i<arrBoard.length; i++){
            //             vitem = arrBoard[i];
            //             // if (vitem.incMode=='inc'){
            //             //     incrementTime(vitem.board.chipItems, vitem.incField);
            //             // } else if (vitem.incMode=='dec'){
            //             //     decrementTime(vitem.board.chipItems, vitem.incField);
            //             // }
            //             if(vitem.updateTime){
            //                 vitem.updateTime(vitem.originalData);
            //             }
            //             vitem.board.redrawChips();
            //         }
            //     } else {
            //         firstTime = false;
            //     }
            // }
        }

        var tick = function () {
            $scope.clock = Date.now() // get the current time
            onTimer();
            $timeout(tick, $scope.tickInterval); // reset the timer
        }

        // Start the timer
        $timeout(tick, $scope.tickInterval);

        // start timeout for create board
        // menggunakan timeout agar dijalankan setelah screen benar-benar ter-load.
        $timeout(function() {
          //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
          // **** waiting for Parts No Info **** //
          //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            board:{
              container: 'parts-waiting_part_no_info',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'parts_order',
              optimizeVisibility: 1,
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }

          }

          // var boardData = [
          //   {chip: 'waiting_part_no_info', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 666 PLN', jam: '10:00', type: 'AVANZA',
          //     tgl_kirim: '09/Jan/2017', time_diperlukan: '11/Jan/2017 10:00', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3, cust_type: 'B', timewait: '00:00',
          //     no_rangka: 'JSHZ89172SDEADAS99', docno: 'WO/2931045892', doctype: 'WO'
          //   },
          //   {chip: 'waiting_part_no_info', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 PLN', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
          //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W',
          //     no_rangka: 'BCDW89172SDEADAS01', docno: 'AP/312998876', doctype: 'AP'
          //   },
          //   {chip: 'waiting_part_no_info', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 777 PLN', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B',
          //     no_rangka: 'KJSK89172SDEADAS02', docno: 'AP/298373333', doctype: 'AP'
          //   },
          //   {chip: 'waiting_part_no_info', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8718 PLN', jam: '09:00', type: 'AVANZA', timewait: '03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S',
          //     no_rangka: 'WDFW89172SDEADAS03', docno: 'SO/721045892', doctype: 'SO'
          //   },
          //   {chip: 'waiting_part_no_info', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9819 PLN', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B',
          //     no_rangka: 'JHDY89172SDEADAS04', docno: 'WO/999005892', doctype: 'WO'
          //   },
          // ]
          //simulateWaitPartNoDate(boardData);

          var checkWaitPartNoCounter = function(vitem){
              var vwait = '';
              var vCurrDate = new Date();
              //var sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd')+'T'+vtime;;
              var vitemDate = new Date (vitem.sDate);
              //vitemDate.setHours(vitem.NeededTime);
              //var vxdelta = vCurrDate - vitem.NeededDate;
              var vxdelta = vCurrDate - vitemDate;
              var vday = 1000*60*60*24;
              var vhour = 1000*60*60;
              var vdelta = (vxdelta/vday);
              var xdelta = vxdelta/vhour;
              if (Math.abs(xdelta)>23){
                  if (Math.abs(vdelta)<100000){
                      if (vdelta>0){
                          vwait = 'H+'+Math.floor(vdelta);
                      } else {
                          vwait = 'H'+Math.ceil(vdelta);
                      }
                      if (vwait == 'H0'){
                          vwait = 'H-1';
                      }
                  }
              } else {
                  if (vitem.sNeededTime!==""){
                      if (Math.abs(xdelta)>1){
                          if (xdelta>0){
                              vwait = 'J+'+Math.floor(xdelta);
                          } else {
                              vwait = 'J'+Math.ceil(xdelta);
                          }
                      } else {
                          vwait = 'J';
                      }
                  } else {
                      vwait = 'H';
                  }
              }
              if (vwait===''){
                  vitem.tgl_diperlukan = '';
              }
              return vwait;

          }

          var processPartNoInfo = function(resp){
            if (resp.data && resp.data.Result){
              var vdata = resp.data.Result;
              $scope.waiting_part_no_infoCount = resp.data.Total;
              console.log('jumlah ooooooooooooooo', $scope.waiting_part_no_infoCount)
              for(var i=0; i<vdata.length; i++){
                  //vdata[i].chip = 'waiting_part_no_info';
                  vitem = vdata[i];
                  vitem.chip = 'waiting_part_no_info';
                  vitem.nopol = vitem.LicensePlate;
                  vitem.no_rangka = vitem.VIN;
                  var vtime = '';
                  var sDate = '';
                  if (vitem.sNeededTime!==""){
                      vtime = $filter('date')(vitem.NeededTime, 'HH:mm');
                      vtime = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vtime));
                      sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd')+'T'+vtime;
                  } else {
                      sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd');
                  }
                  vitem.sDate = sDate;
                  vitem.tgl_diperlukan = $filter('date')(vitem.NeededDate, 'dd/MM/yyyy')+' '+vtime;
                  //vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
                  vitem.sa_name = vitem.RequestBy;
                  vitem.docno = vitem.RefMRNo;
                  vitem.cust_type = vitem.TipeRequest;
                  


                  vitem.timewait = checkWaitPartNoCounter(vitem);


                  // vitem.pekerjaan = vitem.JenisPekerjaan;
                  // vitem.category = vitem.JenisPekerjaan;
                  // vitem.type = vitem.ModelType;
                  // var vmili = vNow - vitem.GateInPushTime;
                  // var vminutes = vmili/(1000 * 60);
                  // vitem.timewait = JsBoard.Common.getTimeString(vminutes);
                  // vitem.deltaTime = vNow - vitem.GateInPushTime;
                  vitem.statusicon = [];
                  //updateIcons(vitem)
              }
              return vdata;
            }
            return []
          }

          var updateTimePartNoInfo = function(vdata){
              for(var i=0; i<vdata.length; i++){
                  vitem = vdata[i];
                  vitem.timewait = checkWaitPartNoCounter(vitem);
              }
          }

          registerBoard({
            boardConfig: boardConfig,
            boardData: [],
            dataUrl: '/api/as/Boards/partboard/1',
            processData: processPartNoInfo,
            updateTime: updateTimePartNoInfo,
            incField: 'xtimewait',
            incMode: 'inc',
            boardName: 'waiting_part_no_info'
          })


          //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
          // **** waiting for Order **** //
          //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            board:{
              container: 'parts-waiting_order',
              iconFolder: 'images/boards/parts',
              chipGroup: 'parts_order',
              optimizeVisibility: 1,
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }

          }

          // var boardData = [
          //   {chip: 'waiting_order', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 6661 BCD', jam: '10:00', type: 'AVANZA',
          //     tgl_kirim: '09/Jan/2017', time_diperlukan: '11/Jan/2017 10:00', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3, cust_type: 'B', timewait: '00:00',
          //     no_rangka: 'JSHZ89172SDEADAX03', docno: 'WO/2931045888', doctype: 'WO'
          //   },
          //   {chip: 'waiting_order', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 BCD', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
          //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W',
          //     no_rangka: 'BCDW89172SDEADAX04', docno: 'AP/3129988899', doctype: 'AP'
          //   },
          //   {chip: 'waiting_order', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 7788 BCD', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B',
          //     no_rangka: 'KJSK89172SDEADAX05', docno: 'AP/298373890', need_dp: 1, doctype: 'AP'
          //   },
          //   {chip: 'waiting_order', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9918 BCD', jam: '09:00', type: 'AVANZA', timewait: '03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S',
          //     no_rangka: 'WDFW89172SDEADAX06', docno: 'SO/721045991', doctype: 'SO'
          //   },
          //   {chip: 'waiting_order', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B',
          //     statusicon: ['dp', 'dp'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          // ]
          // simulateWaitOrderDate(boardData);

          var checkWaitPartOrderCounter = function(vitem){
              var vwait = '';
              var vCurrDate = new Date();
              vCurrDate.setHours(0,0,0,0);
              //var sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd')+'T'+vtime;;
              var vitemDate = new Date (vitem.sDate);
              vitemDate.setHours(0,0,0,0);
              //vitemDate.setHours(vitem.NeededTime);
              //var vxdelta = vCurrDate - vitem.NeededDate;             
              var vxdelta = vCurrDate - vitemDate;
              var vday = 1000*60*60*24;
              var vhour = 1000*60*60;
              var vdelta = vxdelta/vday;
              console.log("Vitem.vday A1", vday);
              console.log("Vitem.vCurrDate A1", vCurrDate);
              console.log("vitemDate A1", vitemDate);
              console.log("Vitem.sDtate A1", vitem.sDate);
              console.log("Vitem.vxdelta A1", vxdelta);
              console.log("Vitem.vdelta A1", vdelta);

              var xdelta = vxdelta/vhour;
              console.log("Vitem.xdelta A2", xdelta);
              if (Math.abs(xdelta)>23){
                  if (Math.abs(vdelta)<100000){
                      if (vdelta>0){
                          vwait = 'H+'+Math.floor(vdelta);
                      } else {
                          vwait = 'H'+Math.ceil(vdelta);
                      }
                      if (vwait == 'H0'){
                          vwait = 'H-1';
                      }
                  }
              } else {
                  if (vitem.sNeededTime!==""){
                      if (Math.abs(xdelta)>1){
                          if (xdelta>0){
                              vwait = 'J+'+Math.floor(xdelta);
                          } else {
                              vwait = 'J'+Math.ceil(xdelta);
                          }
                      } else {
                          vwait = 'J';
                      }
                  } else {
                      vwait = 'H';
                  }
              }
              console.log("Vitem.vwait A1", vwait);

            //   if (Math.abs(vdelta)>1){
            //       if (Math.abs(vdelta)<100000){
            //           if (vdelta>0){
            //               vwait = 'H+'+Math.floor(vdelta);
            //           } else {
            //               vwait = 'H'+Math.floor(vdelta);
            //           }
            //       }
            //   } else {
            //       if (vitem.sNeededTime!==""){
            //           var xdelta = vxdelta/vhour;
            //           if (Math.abs(xdelta)>1){
            //               if (xdelta>0){
            //                   vwait = 'J+'+Math.floor(xdelta);
            //               } else {
            //                   vwait = 'J'+Math.floor(xdelta);
            //               }
            //           } else {
            //               vwait = 'J';
            //           }
            //       } else {
            //           vwait = 'H';
            //       }
            //   }
              if (vwait===''){
                  vitem.tgl_diperlukan = '';
              }
              return vwait;

          }

          var processPartOrderInfo = function(resp){
            var vImgDataCt = 0;
            var vImgMaxDataCt = 10000;
            if (resp.data && resp.data.Result){
                var vdata = resp.data.Result;
                $scope.waiting_orderCount = resp.data.Total;                              
                for(var i=0; i<vdata.length; i++){
                    //vdata[i].chip = 'waiting_part_no_info';
                    vitem = vdata[i];
                    vitem.statusicon = []; 
                    vitem.chip = 'waiting_order';
                    vitem.nopol = vitem.LicensePlate;
                    vitem.no_rangka = vitem.VIN;
                    var vtime = '';
                    var sDate = '';
                    if (vitem.sNeededTime!==""){
                        vtime = $filter('date')(vitem.NeededTime, 'HH:mm');
                        vtime = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vtime));
                        sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd')+'T'+vtime;
                    } else {
                        sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd');
                    }
                    vitem.sDate = sDate;
                    vitem.tgl_diperlukan = $filter('date')(vitem.NeededDate, 'dd/MM/yyyy')+' '+vtime;
                    //vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
                    vitem.sa_name = vitem.RequestBy;
                    vitem.docno = vitem.RefMRNo;
                    vitem.cust_type = vitem.TipeRequest;


                    vitem.daywait = checkWaitPartOrderCounter(vitem);


                    // vitem.pekerjaan = vitem.JenisPekerjaan;
                    // vitem.category = vitem.JenisPekerjaan;
                    // vitem.type = vitem.ModelType;
                    // var vmili = vNow - vitem.GateInPushTime;
                    // var vminutes = vmili/(1000 * 60);
                    // vitem.timewait = JsBoard.Common.getTimeString(vminutes);
                    // vitem.deltaTime = vNow - vitem.GateInPushTime;
                    var vImgDataCt = 0;
                    var vImgMaxDataCt = 10000;                     
                    if (vImgDataCt<vImgMaxDataCt){
                        if (vitem.DownPayment>0){
                            vitem.statusicon.push('dp');
                            vitem.need_dp = 1;
                        }
                        vImgDataCt = vImgDataCt+1;
                    }
                    //updateIcons(vitem)
                    var vCurrDate = new Date();
                    var vitemDate = new Date (vitem.sDate);
                    var vxdelta = vCurrDate - vitemDate;
                    var vday = 1000*60*60*24;

                    //vdata[i].daywait = vxdelta/vday;
                    vdata[i].dday = vxdelta/vday;

                    vitem.dayRange = orderDayColorRange;
                    vitem.hourRange = orderHourColorRange;
                    vitem.textDayRange = orderDayTextColorRange;
                    vitem.textHourRange = [];

                    if(vitem.IsTyre == 1){
                      vitem.statusicon.push('tyre')
                    }

                }
                return vdata;
            }
            return []
          }

          var updateTimePartOrderInfo = function(vdata){
              for(var i=0; i<vdata.length; i++){
                  vitem = vdata[i];
                  vitem.daywait = checkWaitPartOrderCounter(vitem);
              }
          }


          registerBoard({
            boardConfig: boardConfig,
            boardData: [],
            dataUrl: '/api/as/Boards/partboard/3',
            processData: processPartOrderInfo,
            updateTime: updateTimePartOrderInfo,
            incField: 'xtimewait',
            incMode: 'inc',
            boardName: 'waiting_order'
          })


          //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
          // **** Status Order Parts **** //
          //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

          // var showDetailEta = function(vchip){
          //   $scope.currentChip = vchip.data;
          //   ngDialog.openConfirm ({
          //     width: 1000,
          //     template: 'app/boards/partsorder/detailEta.html',
          //     plain: false,
          //     controller: 'PartsOrderController',
          //     scope: $scope
          //   });
          // }

          var showDetailEta = function (vchip) {
            $scope.currentChip = vchip.data;
            console.log('kuy ah', vchip.data)
            $http.post('/api/as/Boards/SpecialOrderBoardBP/', [
              {
                PoNo: $scope.currentChip.RefPONo,
                PurchaseOrderNumber: $scope.currentChip.PurchaseOrderNo
              }
            ]).then(function (res) {
              var newData = res.data.Result;
              console.log('sql', newData );
              $scope.dataDetailEta = []
              $scope.dataDetailEta = newData;
              console.log('dataDetailEta', $scope.dataDetailEta);
              ngDialog.openConfirm({
                width: 1000,
                template: 'app/boards/partsorder/detailEta.html',
                plain: false,
                // controller: 'PartsOrderController',
                scope: $scope
              });
            })
    
          }
    
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            board:{
              container: 'parts-status_order_parts',
              iconFolder: 'images/boards/parts',
              chipGroup: 'parts_order',
              optimizeVisibility: 1,
              onChipCreated: function(vchip){
                console.log(vchip);
                if (typeof vchip.shapes !== 'undefined'){
                  if (typeof vchip.shapes.eta_link !== 'undefined'){
                    vchip.shapes.eta_link.on('mouseup', function(evt){
                      document.body.style.cursor = 'default';
                      evt.evt.preventDefault();
                      var vchip = this.parent;
                      showDetailEta(this.parent);
                      if ((typeof vchip.data !== 'undefined') && (typeof vchip.data.statusicon !=='undefined')){
                            // showDetailEta(this.parent);
                          // if (vchip.data.statusicon.indexOf('eta')>=0){
                          //   showDetailEta(this.parent);
                          // }
                      }
                      //alert("click eta");
                    });

                    vchip.shapes.eta_link.on('mouseover', function(evt) {
                        document.body.style.cursor = 'pointer';
                    });
                    vchip.shapes.eta_link.on('mouseout', function(evt) {
                        document.body.style.cursor = 'default';
                    });

                  }
                }
                //alert('chip created');
                //asdf();
              }
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }

          }

          // var boardData = [
          //   {chip: 'status_order_parts', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 6661 BCD', jam: '10:00', type: 'AVANZA', vendor: 'SPLD',
          //     tgl_kirim: '09/Jan/2017', time_diperlukan: '11/Jan/2017 10:00', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3, cust_type: 'B', timewait: '00:00',
          //     statusicon: ['open', 'late', 'eta', 'bo'],
          //     no_rangka: 'JSHZ89172SDEADAX03', docno: 'WO/2931045888', doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 BCD', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
          //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W', vendor: 'SPLD',
          //     statusicon: ['open', 'late'],
          //     no_rangka: 'BCDW89172SDEADAX04', docno: 'AP/312998889', doctype: 'AP'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 7788 BCD', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B', vendor: 'SPLD',
          //     no_rangka: 'KJSK89172SDEADAX05', docno: 'AP/298373890', need_dp: 1, doctype: 'AP'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9918 BCD', jam: '09:00', type: 'AVANZA', timewait: '03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S', vendor: 'SPLD',
          //     statusicon: ['open', 'eta'],
          //     no_rangka: 'WDFW89172SDEADAX06', docno: 'SO/721045991', doctype: 'SO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          // ]
          // simulateStatusOrderDate(boardData);

          var checkPartStatusOrderCounter = function(vitem){
              var vwait = '';
              var vCurrDate = new Date();
              //var sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd')+'T'+vtime;;
              var vitemDate = new Date (vitem.sDate);
              //vitemDate.setHours(vitem.NeededTime);
              //var vxdelta = vCurrDate - vitem.NeededDate;
              var vxdelta = vCurrDate - vitemDate;
              var vday = 1000*60*60*24;
              var vhour = 1000*60*60;
              var vdelta = vxdelta/vday;

              var xdelta = vxdelta/vhour;
              if (Math.abs(xdelta)>23){
                  if (Math.abs(vdelta)<100000){
                      if (vdelta>0){
                          vwait = 'H+'+Math.floor(vdelta);
                      } else {
                          vwait = 'H'+Math.ceil(vdelta);
                      }
                      if (vwait == 'H0'){
                          vwait = 'H-1';
                      }
                  }
              } else {
                  if (vitem.sNeededTime!==""){
                      if (Math.abs(xdelta)>1){
                          if (xdelta>0){
                              vwait = 'J+'+Math.floor(xdelta);
                          } else {
                              vwait = 'J'+Math.ceil(xdelta);
                          }
                      } else {
                          vwait = 'J';
                      }
                  } else {
                      vwait = 'H';
                  }
              }


            //   if (Math.abs(vdelta)>1){
            //       if (Math.abs(vdelta)<100000){
            //           if (vdelta>0){
            //               vwait = 'H+'+Math.floor(vdelta);
            //           } else {
            //               vwait = 'H'+Math.floor(vdelta);
            //           }
            //       }
            //   } else {
            //       if (vitem.sNeededTime!==""){
            //           var xdelta = vxdelta/vhour;
            //           if (Math.abs(xdelta)>1){
            //               if (xdelta>0){
            //                   vwait = 'J+'+Math.floor(xdelta);
            //               } else {
            //                   vwait = 'J'+Math.floor(xdelta);
            //               }
            //           } else {
            //               vwait = 'J';
            //           }
            //       } else {
            //           vwait = 'H';
            //       }
            //   }
              if (vwait===''){
                  vitem.tgl_diperlukan = '';
              }
              return vwait;

          }


          var processPartStatusOrderInfo = function(resp){
              var vImgDataCt = 0;
              var vImgMaxDataCt = 10000;
              if (resp.data && resp.data.Result){
                var vCurrDate = new Date();
                var vdata = resp.data.Result;
                $scope.status_order_partsCount = resp.data.Total;
                for(var i=0; i<vdata.length; i++){
                    //vdata[i].chip = 'waiting_part_no_info';
                    vitem = vdata[i];
                    vitem.chip = 'status_order_parts';
                    vitem.nopol = vitem.LicensePlate;
                    vitem.no_rangka = vitem.VIN;
                    var vtime = '';
                    var sDate = '';
                    if (vitem.sNeededTime!==""){
                        vtime = $filter('date')(vitem.NeededTime, 'HH:mm');
                        vtime = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vtime));
                        sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd')+'T'+vtime;
                    } else {
                        sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd');
                    }
                    vitem.sDate = sDate;
                    vitem.tgl_diperlukan = $filter('date')(vitem.NeededDate, 'dd/MM/yyyy')+' '+vtime;
                    //vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
                    vitem.sa_name = vitem.RequestBy;
                    vitem.docno = vitem.RefMRNo;
                    vitem.cust_type = vitem.TipeRequest;
                    vitem.tgl_eta = '';
                    var xeta = $filter('date')(vitem.ETA, 'yyyy-MM-dd HH:mm')
                    var xeta2 = $filter('date')(vitem.ETA, 'dd/MM/yyyy HH:mm')
                    if (xeta.substring(0, 7)!='0001-01'){
                        vitem.tgl_eta = xeta2;
                    }

                    vitem.daywait = checkPartStatusOrderCounter(vitem);


                    // vitem.pekerjaan = vitem.JenisPekerjaan; 
                    // vitem.category = vitem.JenisPekerjaan;
                    // vitem.type = vitem.ModelType;
                    // var vmili = vNow - vitem.GateInPushTime;
                    // var vminutes = vmili/(1000 * 60);
                    // vitem.timewait = JsBoard.Common.getTimeString(vminutes);
                    // vitem.deltaTime = vNow - vitem.GateInPushTime;
                    vitem.statusicon = [];

                    if (vImgDataCt<vImgMaxDataCt){
                        if(vitem.PurchaseOrderStatusId==1){
                            vitem.statusicon.push('draft')
                        } else if(vitem.PurchaseOrderStatusId==2){
                            vitem.statusicon.push('reqapproval')
                        } else if(vitem.PurchaseOrderStatusId==3){
                            vitem.statusicon.push('open')
                        } else if(vitem.PurchaseOrderStatusId==4){
                            vitem.statusicon.push('delivery')
                        }
                        if (vitem.ETAChange>0){
                            vitem.statusicon.push('eta');
                        }

                        if (vitem.NeededDateTime<vitem.ETA){
                            vitem.statusicon.push('late');
                        }

                        if (vitem.BackOrderStatus){
                            vitem.statusicon.push('bo');
                        }
                        vImgDataCt = vImgDataCt+1;
                    }

                    // if (vitem.DownPayment>0){
                    //     //vitem.statusicon.push('');
                    //     vitem.need_dp = 1;
                    // }
                    //updateIcons(vitem)
                    var vitemDate = new Date (vitem.sDate);
                    var vxdelta = vCurrDate - vitemDate;
                    var vday = 1000*60*60*24;

                    //vdata[i].daywait = vxdelta/vday;
                    vdata[i].dday = vxdelta/vday;

                    vitem.vendor = vitem.VendorName;
                    vitem.dayRange = orderDayColorRange;
                    vitem.hourRange = orderHourColorRange;
                    vitem.textDayRange = orderDayTextColorRange;
                    vitem.textHourRange = [];

                    if(vitem.IsTyre == 1){
                      vitem.statusicon.push('tyre')
                    }

                }
                return vdata;
            }
            return []
          }

          var updateTimePartStatusOrderInfo = function(vdata){
              for(var i=0; i<vdata.length; i++){
                  vitem = vdata[i];
                  vitem.daywait = checkPartStatusOrderCounter(vitem);
              }
          }



          registerBoard({
            boardConfig: boardConfig,
            boardData: [],
            dataUrl: '/api/as/Boards/partboard/4',
            processData: processPartStatusOrderInfo,
            updateTime: updateTimePartStatusOrderInfo,
            incField: 'xtimewait',
            incMode: 'inc',
            boardName: 'status_order_parts'
          })

          //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
          // **** Status RPP & Parts Claim **** //
          //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            board:{
              container: 'parts-status_rpp_claim',
              iconFolder: 'images/boards/parts',
              chipGroup: 'parts_order',
              optimizeVisibility: 1,
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }

          }

          // var boardData = [
          //   {chip: 'status_rpp_claim', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 6661 ABN', jam: '10:00', type: 'AVANZA', vendor: 'SPLD',
          //     tgl_kirim: '09/Jan/2017', time_diperlukan: '11/Jan/2017 10:00', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3, cust_type: 'B', timewait: '00:00',
          //     statusicon: ['open', 'late', 'eta'],
          //     statusname: 'rpp', no_gr: 'A1312345',
          //     no_rangka: 'JSHZ89172SDEADAX03', docno: 'WO/2931045888', doctype: 'WO'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 ABN', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
          //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W', vendor: 'SPLD',
          //     statusicon: ['open', 'late'],
          //     statusname: 'claim', no_gr: 'B191029123',
          //     no_rangka: 'BCDW89172SDEADAX04', docno: 'AP/312998889', doctype: 'AP'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 7788 ABN', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B', vendor: 'SPLD',
          //     statusname: 'rpp', no_gr: 'A998173123',
          //     no_rangka: 'KJSK89172SDEADAX05', docno: 'AP/298373890', need_dp: 1, doctype: 'AP'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9918 ABN', jam: '09:00', type: 'AVANZA', timewait: '03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S', vendor: 'SPLD',
          //     statusicon: ['open'], no_gr: 'V991031234',
          //     statusname: 'rpp',
          //     no_rangka: 'WDFW89172SDEADAX06', docno: 'SO/721045991', doctype: 'SO'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 ABN', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'], no_gr: 'K00944123222',
          //     statusname: 'claim',
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 ABN', jam: '09:20', type: 'AVANZA', timewait: '04:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'], no_gr: 'K00888123222',
          //     statusname: 'claim',
          //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', need_dp: 1, doctype: 'WO'
          //   },
          // ]
          // simulateStatusRppClaimDate(boardData);


          var checkPartRPPClaimCounter = function(vitem){
              var vwait = '';
              var vCurrDate = new Date();
              //var sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd')+'T'+vtime;;
              var vitemDate = new Date (vitem.sDate);
              //vitemDate.setHours(vitem.NeededTime);
              //var vxdelta = vCurrDate - vitem.NeededDate;
              var vxdelta = vCurrDate - vitemDate;
              var vday = 1000*60*60*24;
              var vhour = 1000*60*60;
              var vdelta = vxdelta/vday;
              if (Math.abs(vdelta)>1){
                  if (Math.abs(vdelta)<100000){
                      if (vdelta>0){
                          vwait = 'H+'+Math.floor(vdelta);
                      } else {
                          vwait = 'H'+Math.floor(vdelta);
                      }
                  }
              } else {
                  if (vitem.sNeededTime!==""){
                      var xdelta = vxdelta/vhour;
                      if (Math.abs(xdelta)>1){
                          if (xdelta>0){
                              vwait = 'J+'+Math.floor(xdelta);
                          } else {
                              vwait = 'J'+Math.floor(xdelta);
                          }
                      } else {
                          vwait = 'J';
                      }
                  } else {
                      vwait = 'H';
                  }
              }
              if (vwait===''){
                  vitem.tgl_diperlukan = '';
              }
              return vwait;

          }

          var processRPPClaimOrderInfo = function(resp){
            if (resp.data && resp.data.Result){
              var vCurrDate = new Date();
              var vdata = resp.data.Result;
              $scope.status_rpp_claimCount = resp.data.Total;
              for(var i=0; i<vdata.length; i++){
                  //vdata[i].chip = 'waiting_part_no_info';
                  vitem = vdata[i];
                  vitem.chip = 'status_rpp_claim';
                  vitem.nopol = vitem.LicensePlate;
                  vitem.no_rangka = vitem.VIN;
                  var vtime = '';
                  var sDate = '';
                  if (vitem.sNeededTime!==""){
                      vtime = $filter('date')(vitem.NeededTime, 'HH:mm');
                      vtime = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vtime));
                      sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd')+'T'+vtime;
                  } else {
                      sDate = $filter('date')(vitem.NeededDate, 'yyyy-MM-dd');
                  }
                  vitem.sDate = sDate;
                  vitem.tgl_diperlukan = $filter('date')(vitem.NeededDate, 'dd/MM/yyyy')+' '+vtime;
                  //vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
                  vitem.sa_name = vitem.RequestBy;
                  vitem.vendor = vitem.VendorName;
                  vitem.docno = vitem.RefPONo;
                  vitem.no_gr= vitem.GoodReceiptNo;
                  vitem.date_gr= $filter('date')(vitem.GoodReceiptDate, 'dd/MM/yyyy HH:mm');
                  vitem.cust_type = vitem.TipeRequest;
                  vitem.tgl_eta = '';
                  var xeta = $filter('date')(vitem.ETA, 'yyyy-MM-dd HH:mm')
                  var xeta2 = $filter('date')(vitem.ETA, 'dd/MM/yyyy HH:mm')
                  if (xeta.substring(0, 7)!='0001-01'){
                      vitem.tgl_eta = xeta2;
                  }

                  vitem.daywait = checkPartRPPClaimCounter(vitem);


                  // vitem.pekerjaan = vitem.JenisPekerjaan;
                  // vitem.category = vitem.JenisPekerjaan;
                  // vitem.type = vitem.ModelType;
                  // var vmili = vNow - vitem.GateInPushTime;
                  // var vminutes = vmili/(1000 * 60);
                  // vitem.timewait = JsBoard.Common.getTimeString(vminutes);
                  // vitem.deltaTime = vNow - vitem.GateInPushTime;
                  vitem.statusicon = [];
                  if(vitem.ClaimType=='rpp'){
                    vitem.statusname = 'rpp';
                  } else {
                    vitem.statusname = 'claim';
                  }
                //   if(vitem.PurchaseOrderStatusId==1){
                //       vitem.statusicon.push('draft')
                //   } else if(vitem.PurchaseOrderStatusId==2){
                //       vitem.statusicon.push('reqapproval')
                //   } else if(vitem.PurchaseOrderStatusId==3){
                //       vitem.statusicon.push('open')
                //   }
                  if (vitem.ETAChange>0){
                      vitem.statusicon.push('eta');
                  }

                  if (vitem.NeededDateTime<vitem.ETA){
                      vitem.statusicon.push('late');
                  }

                  if (vitem.BackOrderStatus){
                      vitem.statusicon.push('bo');
                  }

                  // if (vitem.DownPayment>0){
                  //     //vitem.statusicon.push('');
                  //     vitem.need_dp = 1;
                  // }
                  //updateIcons(vitem)
                  var vitemDate = new Date (vitem.sDate);
                  var vxdelta = vCurrDate - vitemDate;
                  var vday = 1000*60*60*24;

                  //vdata[i].daywait = vxdelta/vday;
                  vdata[i].dday = vxdelta/vday;

                  vitem.dayRange = orderDayColorRange2;
                  vitem.hourRange = orderHourColorRange;
                  vitem.textDayRange = orderDayTextColorRange2;
                  vitem.textHourRange = [];



              }
              return vdata;
            }
            return []
          }

          var updateTimePartRPPClaimInfo = function(vdata){
              for(var i=0; i<vdata.length; i++){
                  vitem = vdata[i];
                  vitem.daywait = checkPartRPPClaimCounter(vitem);
              }
          }



          registerBoard({
            boardConfig: boardConfig,
            boardData: [],
            dataUrl: '/api/as/Boards/partboard/10',
            processData: processRPPClaimOrderInfo,
            updateTime: updateTimePartRPPClaimInfo,
            incField: 'timewait',
            incMode: 'inc',
            boardName: 'status_rpp_claim'
          })


          // ------- //

          $scope.porderboard.resetFilter();
          loadAllData();
        },100); // end of timeout for create board


    // });

    var adsearch = 0;
    $scope.showOptions = function() {
      if (adsearch == 0){
        $('#AdvSearch').attr('hidden', 'hidden');
        adsearch = 1;
      } else {
        $('#AdvSearch').removeAttr('hidden', 'hidden');
        adsearch = 0;
      }
    }


});




//resolusi tv 1366 x 768
