angular.module('app')
.controller('IrregularityController', function($scope, $http, CurrentUser, Parameter,$timeout, $window, $filter, irregularFactory, Tabs, Idle) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    var allBoardHeight = 550;
    var onTimeRangeMinute = 30;
    var stopped;
    var pause = false;
    var finishGetData = false
    $scope.count_reception = 0;
    $scope.count_production = 0;
    $scope.count_inspect = 0;
    $scope.count_teco = 0;
    $scope.count_notif = 0;
    $scope.count_delivery = 0;
    $scope.clock = "loading clock..."; // initialise the time variable
    $scope.tickInterval = 1000; //ms
    // $scope.counter = 30; //ms
    $scope.counter = 60//300; //timer nya
    $scope.hourminute = '';
    // misalnya onTimeRangeMinute = 30
    // Time Datang - Time Appointment >= -30 dan <= 0 ==> ontime
    // Appointment 08:30 --> bila datang 08:05 => ontime; datang 08:25 tidak ontime; 8:35 tidak ontime

    var nameList = []
    var onPanelSizeChanged = function(element) {
        for (var i=0; i<nameList.length; i++){
            var vboard = irregularFactory.getBoard(nameList[i]);
            if(vboard){
                vboard.autoResizeStage();
            }
        }
    }
    var velement = document.getElementById('irregular-gr-main-panel');
    var erd = elementResizeDetectorMaker();
    erd.listenTo(velement, onPanelSizeChanged);
    

        var firstTime = true;
        var onTimer = function(){
            var vhmin= $filter('date')($scope.clock, 'hh:mm');
            if ($scope.hourminute!==vhmin){
                $scope.hourminute = vhmin;
                if (!firstTime){
                    for (var i=0; i<nameList.length; i++){
                        // irregularFactory.showBoard(nameList[i]);       // baris ini di komen karena kalau board nya di antepin lama, nanti refresh data nya ga sesuai pas di detik 0.
                        //var vboard = irregularFactory.getBoard(nameList[i]);
                        //if(vboard){
                        //    vboard.autoResizeStage();
                        //}
                    }
                    //irregularFactory.showBoard('');
                    /*for(var i=0; i<arrBoard.length; i++){
                        vitem = arrBoard[i];
                        // if (vitem.incMode=='inc'){
                        //     incrementTime(vitem.board.chipItems, vitem.incField);
                        // } else if (vitem.incMode=='dec'){
                        //     decrementTime(vitem.board.chipItems, vitem.incField);
                        // }
                        if(vitem.updateTime){
                            vitem.updateTime(vitem.originalData);
                        }
                        vitem.board.redrawChips();
                    }*/
                } else {
                    firstTime = false;
                }
            }
        }

        var tick = function () {
            $scope.clock = Date.now() // get the current time
            onTimer();
            $timeout(tick, $scope.tickInterval); // reset the timer
        }

        // Start the timer
        $timeout(tick, $scope.tickInterval);
    
    $scope.$on('$viewContentLoaded', function() {
        $scope.LoadAllData();
        
        // Auto refresh
        $scope.countDown();
        Idle.unwatch();
        console.log('==== value of idle ==', Idle)
    });
    
    $scope.$on('$destroy', function(){
      Idle.watch();
    });

    var getDateWait = function(data) {
      var timeStart = new Date().getTime();
      var timeEnd = new Date(data).getTime();
      // ===============================
      var hourDiff = timeStart - timeEnd; //in ms
      var secDiff = hourDiff / 1000; //in s
      var minDiff = hourDiff / 60 / 1000; //in minutes
      var hDiff = hourDiff / 3600 / 1000; //in hours
      var humanReadable = {};
      humanReadable.hours = Math.floor(hDiff);
      humanReadable.minutes = Math.floor(minDiff - 60 * humanReadable.hours);
      var gabung = (humanReadable.hours < 10 ? '0' + humanReadable.hours : humanReadable.hours) + ':' + (humanReadable.minutes < 10 ? '0' + humanReadable.minutes : humanReadable.minutes);

      return gabung
  }

    var showData = 6;

    var MulaiDataINSURANCE= 0;
    var BagiBoardINSURANCE = 0;
    var CountBoardINSURANCE = 0;
    var ModulusINSURANCE = 0;
    var dataHabisINSURANCE = true;

    var MulaiDataRECEPTION = 0;
    var BagiBoardRECEPTION = 0;
    var CountBoardRECEPTION = 0;
    var ModulusRECEPTION = 0;
    var dataHabisRECEPTION = true;

    var MulaiDataPRODUCTION = 0;
    var BagiBoardPRODUCTION = 0;
    var CountBoardPRODUCTION = 0;
    var ModulusPRODUCTION = 0;
    var dataHabisPRODUCTION = true;

    var MulaiDataFI = 0;
    var BagiBoardFI = 0;
    var CountBoardFI = 0;
    var ModulusFI = 0;
    var dataHabisFI = true;

    var MulaiDataTECO = 0;
    var BagiBoardTECO = 0;
    var CountBoardTECO = 0;
    var ModulusTECO = 0;
    var dataHabisTECO = true;

    var MulaiDataNOTIF = 0;
    var BagiBoardNOTIF = 0;
    var CountBoardNOTIF = 0;
    var ModulusNOTIF = 0;
    var dataHabisNOTIF = true;

    var MulaiDataDELIVERY = 0;
    var BagiBoardDELIVERY = 0;
    var CountBoardDELIVERY = 0;
    var ModulusDELIVERY = 0;
    var dataHabisDELIVERY = true;

    $scope.LoadAllData = function()
        {
            var refCounter = 0;
        var pushRefCounter = function(){
            refCounter = refCounter+1;
        }
        var pullRefCounter = function(onExecute){
            refCounter = refCounter-1;

            if (refCounter<=0){
              $timeout(onExecute(), 100);
            }
        }

        arrRECEPTION = [];
        arrPRODUCTION = [];
        arrFI = [];
        arrTECO = [];
        arrNOTIF = [];
        arrDELIVERY = [];


        // Irregularity Waiting for Reception
        var boardConfig = {
            type: 'kanban',
            // chipUrl: '/api/as/Boards/irregular/wfr', // di ganti karena ga sama jumlah nya dengan yang di queuelist
            chipUrl: '/api/as/queue/QueueList/1',
            // Add Code
            boardType:'WFR',
            board:{
              container: 'irggr-waiting_reception',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'irregular_gr',
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              height: 80,
              width: 280,
              gap: 2
            },
            argChips: {
              currentDate: new Date()
            }
        }

        var updateIcons = function(vitem){
            if (vitem.OnTimeDuration>=-onTimeRangeMinute && vitem.OnTimeDuration<0){
                vitem.statusicon.push('ontime')
            }
        }
        var boardName = 'waiting_reception';
        nameList.push(boardName);
        irregularFactory.createBoard(boardName, boardConfig);
        irregularFactory.setOnChipLoaded(boardName, function(vdata){
            var vNow = new Date();
            for(var i=0; i<vdata.length; i++){
              vitem = vdata[i];
              vitem.chip = 'waiting_reception';


            //  ========================= loopingan yg pas pake url wfr, sebelum di ganti ke queuelist ========== start
            //   vitem.nopol = vitem.nopol;
            //   vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
            //   //vitem.pekerjaan = vitem.JenisPekerjaan;
            //   vitem.category = vitem.category;
            //   //vitem.type = vitem.ModelType;
            // //   var vmili = vNow - vitem.GateInPushTime;
            //   var vmili = Math.abs(vNow - vitem.GateInPushTime);
            //   var vminutes = vmili/(1000 * 60);
            //   vitem.timewait = JsBoard.Common.getTimeString(vminutes);
            //   vitem.deltaTime = vNow - vitem.GateInPushTime;
            //   vitem.statusicon = [];
            //   if (vitem.ontime){
            //       vitem.statusicon.push('ontime');
            //   }
            //   updateIcons(vitem)
            //  ========================= loopingan yg pas pake url wfr, sebelum di ganti ke queuelist ========== end



              // ======== ini loopingan data setelah di ganti url wfr nya ========================= start
              vitem.nopol = vitem.LicensePlate;
              vitem.start = $filter('date')(vitem.ArrivalTime, 'HH:mm');
              // if (vitem.QueueCategory.QCategoryId == 1 || vitem.QueueCategory.QCategoryId == 2){
              //   vitem.category = 'B';
              // } else if (vitem.QueueCategory.QCategoryId == 3 || vitem.QueueCategory.QCategoryId == 4){
              //   vitem.category = 'EM';
              // } else if (vitem.QueueCategory.QCategoryId == 5){
              //   vitem.category = 'W';
              // } else {
              //   vitem.category = 'O';
              // }
              if (vitem.QueueCategory.Name == 'Booking On Time' || vitem.QueueCategory.Name == 'Booking'){
                vitem.category = 'B';
              } else if (vitem.QueueCategory.Name == 'EM On Time' || vitem.QueueCategory.Name == 'EM'){
                vitem.category = 'EM';
              } else if (vitem.QueueCategory.Name == 'Walk In'){
                vitem.category = 'W';
              } else {
                vitem.category = 'O';
              }
              var vmili = Math.abs(vNow - vitem.ArrivalTime);
              var vminutes = vmili/(1000 * 60);
              // vitem.timewait = JsBoard.Common.getTimeString(vminutes);
              vitem.timewait = getDateWait(vitem.ArrivalTime);
              vitem.deltaTime = vNow - vitem.ArrivalTime;
              vitem.statusicon = [];

              if (vitem.category == 'B'){
                if (vitem.Job != null){
                  var thn = vitem.Job.AppointmentDate.getFullYear()
                  var bln = vitem.Job.AppointmentDate.getMonth()
                  var tgl = vitem.Job.AppointmentDate.getDate()
                  var jm = vitem.Job.AppointmentTime.split(':')[0]
                  var mnt = vitem.Job.AppointmentTime.split(':')[1]
                  var cekWktB = new Date(thn,bln,tgl,jm,mnt,0)
                  console.log('jm komp',vitem.ArrivalTime.getTime())
                  console.log('jm b',cekWktB.getTime())
                  if (vitem.ArrivalTime.getTime() < cekWktB.getTime()){
                    vitem.statusicon.push('ontime');
                  } else {
                    vitem.statusicon.push('offtime');
                  }
                }
              }
              // if (vitem.ontime){
              //     vitem.statusicon.push('ontime');
              // }
              updateIcons(vitem)
              // ======== ini loopingan data setelah di ganti url wfr nya ========================= end

              arrRECEPTION.push(vitem);
            }
            // console.log("***** vdata *****", vdata);

            // vdata.push ({chip: 'waiting_reception', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 666 NB', start: '08:00', type: 'AVANZA', 
            //   pekerjaan: 'EM', teknisi: 'AND', timewait: '00:11', statusicon: ['ontime'], category: 'EM'});            

            $scope.count_reception = vdata.length;

            if (dataHabisRECEPTION == true)
            {
                CountBoardRECEPTION = arrRECEPTION.length;
                ModulusRECEPTION = parseInt(CountBoardRECEPTION % showData);
                BagiBoardRECEPTION = parseInt(CountBoardRECEPTION / showData);
                if (ModulusRECEPTION != 0)
                {
                    BagiBoardRECEPTION = BagiBoardRECEPTION + 1;
                }
            }

            var boardData = [];
            var max = $scope.count_reception;
            if ((MulaiDataRECEPTION+showData) > $scope.count_reception){
              max = $scope.count_reception;
            } else {
              max = MulaiDataRECEPTION+showData
            }
            for(var i= MulaiDataRECEPTION;  i< max; i++){
              var vboard = arrRECEPTION[i];
              boardData.push(vboard);
            }

          BagiBoardRECEPTION = BagiBoardRECEPTION - 1;

          if (BagiBoardRECEPTION != 0)
            {
                dataHabisRECEPTION = false;
                MulaiDataRECEPTION = MulaiDataRECEPTION + showData;
            }
            else
            {
                MulaiDataRECEPTION = 0;
                BagiBoardRECEPTION = 0;
                dataHabisRECEPTION = true;
            }

            return boardData;
            // return vdata;
        });
        pushRefCounter();
        irregularFactory.setOnBoardCreated(boardName, function(vboard){
            pullRefCounter(function(){
                onPanelSizeChanged(null);
            })
        });

        irregularFactory.showBoard(boardName);

        // Irregularity Waiting for Production
        var boardConfig = {
            type: 'kanban',
            chipUrl: '/api/as/Boards/Irregular/gr/wpr',
            // Add Code
            boardType:'WFP',
            board:{
                container: 'irggr-waiting_production',
                iconFolder: 'images/boards/iregular',
                chipGroup: 'irregular_gr',
            },
            stage: {
                maxHeight: allBoardHeight,
                minHeight: allBoardHeight,
            },
            chip: {
                height: 120,
                width: 280,
                gap: 2
            },
            layout: {
            },
            argChips: {
              currentDate: new Date()
            }
        }
        var boardName = 'waiting_production';
        nameList.push(boardName);
        var updateIconsWaitProc = function(vitem){
            //console.log("vitem", vitem);
            if (vitem.Status === 4){
                vitem.statusicon.push('assignment')
            }
        }
        irregularFactory.createBoard(boardName, boardConfig);
        irregularFactory.setOnChipLoaded(boardName, function(vdata){

            var vNow = new Date();
            for(var i=0; i<vdata.length; i++){
              vitem = vdata[i];
              vitem.chip = 'waiting_production';
              vitem.nopol = vitem.PoliceNumber;
              // vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
              vitem.workrelease = $filter('date')(vitem.JobDate, 'dd/MMM/yyyy HH:mm'); // WO Release
              vitem.workstart = $filter('date')(vitem.PlanDateTimeStart, 'dd/MMM/yyyy HH:mm');
              vitem.pekerjaan = vitem.JenisPekerjaan;
              vitem.category = vitem.JenisPekerjaan;
              vitem.sa_name = vitem.NameSa;
              vitem.type = vitem.ModelType;
            //   var vmili = vitem.PlanDateTimeStart - vNow;
              // var vmili = Math.abs(vitem.PlanDateTimeStart - vNow);
              // var vmilis = (vitem.PlanDateTimeStart - vNow);
              var vmili = Math.abs(vNow - vitem.PlanDateTimeStart);
              var vmilis = (vNow - vitem.PlanDateTimeStart);
              var vminutes = vmili/(1000 * 60);
              vitem.timestart = JsBoard.Common.getTimeString(vminutes);

              vitem.timestart_minus = angular.copy(vitem.timestart)
              if (vmilis < 0){
                vitem.timestart_minus = '-'+vitem.timestart_minus
              }

              vitem.deltaTime = vitem.PlanDateTimeStart - vNow;
              vitem.statusicon = [];
              vitem.wotype = vitem.WoCategory;
              updateIconsWaitProc(vitem)

              arrPRODUCTION.push(vitem)
            }
            // vdata.push({chip: 'waiting_production', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 1234 NB', start: '08:00', type: 'AVANZA', 
            //   wotype: 'SBI', sa_name: 'AND', workrelease: '12/Jan/2017 07:05', workstart: '12/Jan/2017 08:03', timestart: '00:04', statusicon: [], category: 'EM'});
          
            $scope.count_production = vdata.length;

            if (dataHabisPRODUCTION == true)
            {
                CountBoardPRODUCTION = arrPRODUCTION.length;
                ModulusPRODUCTION = parseInt(CountBoardPRODUCTION % showData);
                BagiBoardPRODUCTION = parseInt(CountBoardPRODUCTION / showData);
                if (ModulusPRODUCTION != 0)
                {
                    BagiBoardPRODUCTION = BagiBoardPRODUCTION + 1;
                }
            }

            var boardData = [];
            var max = $scope.count_production;
            if ((MulaiDataPRODUCTION+showData) > $scope.count_production){
              max = $scope.count_production;
            } else {
              max = MulaiDataPRODUCTION+showData
            }
            for(var i= MulaiDataPRODUCTION;  i< max; i++){
              var vboard = arrPRODUCTION[i];
              boardData.push(vboard);
            }

          BagiBoardPRODUCTION = BagiBoardPRODUCTION - 1;

          if (BagiBoardPRODUCTION != 0)
            {
                dataHabisPRODUCTION = false;
                MulaiDataPRODUCTION = MulaiDataPRODUCTION + showData;
            }
            else
            {
                MulaiDataPRODUCTION = 0;
                BagiBoardPRODUCTION = 0;
                dataHabisPRODUCTION = true;
            }

            return boardData;
            // console.log("***data chip***", vdata);
            // return vdata;
        });
        pushRefCounter();
        irregularFactory.setOnBoardCreated(boardName, function(vboard){
            pullRefCounter(function(){
                onPanelSizeChanged(null);
            })
        });


        irregularFactory.showBoard(boardName);


        // Irregularity Waiting Inspection
        var boardConfig = {
            type: 'kanban',
            chipUrl: '/api/as/Boards/Irregular/gr/wfi',
            // Add Code
            boardType:'WFFI',
            board:{
              container: 'irggr-waiting_inspection',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'irregular_gr',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              // height: 100,
              height: 120,
              width: 280,
              gap: 2
            },
            layout: {
            },
            argChips: {
              currentDate: new Date()
            }
        }
        var boardName = 'waiting_inspection';
        nameList.push(boardName);
        var updateIconsWaitFI = function(vitem){
            // console.log("vitem FI bos", vitem);
            // if (vitem.Teknisi===null){
            //     vitem.statusicon.push('assignment')
            // }
        }
        irregularFactory.createBoard(boardName, boardConfig);
        irregularFactory.setOnChipLoaded(boardName, function(vdata){
          console.log('vdata FI bos', vdata)
            var vNow = new Date();
            for(var i=0; i<vdata.length; i++){
              vitem = vdata[i];
              vitem.chip = 'waiting_inspection';
              vitem.nopol = vitem.PoliceNumber;
              vitem.InitialForeman = 'ASD';
              // vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
              vitem.workrelease = $filter('date')(vitem.PlanDateTimeStart, 'dd/MMM/yyyy HH:mm'); 
              // vitem.workstart = $filter('date')(vitem.PlanDateTimeStart, 'dd/MMM/yyyy HH:mm');
              vitem.pekerjaan = vitem.JenisPekerjaan;
              vitem.category = vitem.JenisPekerjaan;
              vitem.sa_name = vitem.NameSa;
              vitem.type = vitem.ModelType;
            //   var vmili = vitem.PlanDateTimeStart - vNow;
              var vmili = Math.abs(vitem.PlanDateTimeStart - vNow);
              var vminutes = vmili/(1000 * 60);
              vitem.timewait = JsBoard.Common.getTimeString(vminutes);
              vitem.deltaTime = vitem.PlanDateTimeStart - vNow;
              vitem.statusicon = [];
              vitem.wotype = vitem.WoCategory;
              updateIconsWaitFI(vitem)

              arrFI.push(vitem)
            }

            // vdata.push({chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
            //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'});
          
            // vdata = [
            //     {chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
            //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'},
            //     {chip: 'waiting_inspection', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 907 MO', start: '09:20', type: 'AVANZA', 
            //       wotype: 'SBM', sa_name: 'BUD', foreman: 'MIN', statusicon: [], timewait: '00:15', category: 'B'},
            //     {chip: 'waiting_inspection', row: 1, col: 2.5, timestatus: 1, width: 1, nopol: 'B 6467 AA', start: '09:30', type: 'AVANZA', 
            //       wotype: 'XYZ', sa_name: 'ANI', foreman: 'DON', icons: [], timewait: '00:08', category: 'W'},
            //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3888 CD', start: '10:00', type: 'AVANZA', 
            //       wotype: 'GR', sa_name: 'BUD', foreman: 'FIN', icons: ['watch', 'arrow1'], timewait: '00:04', category: 'O'},
            //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9888 HN', start: '10:05', type: 'AVANZA', 
            //       wotype: 'GR', sa_name: 'BUD', foreman: 'JOK', icons: ['watch', 'arrow1'], timewait: '00:03', category: 'O'},
            // ]
            $scope.count_inspect = vdata.length;

            if (dataHabisFI == true)
            {
                CountBoardFI = arrFI.length;
                ModulusFI = parseInt(CountBoardFI % showData);
                BagiBoardFI = parseInt(CountBoardFI / showData);
                if (ModulusFI != 0)
                {
                    BagiBoardFI = BagiBoardFI + 1;
                }
            }

            var boardData = [];
            var max = $scope.count_inspect;
            if ((MulaiDataFI+showData) > $scope.count_inspect){
              max = $scope.count_inspect;
            } else {
              max = MulaiDataFI+showData
            }
            for(var i= MulaiDataFI;  i< max; i++){
              var vboard = arrFI[i];
              boardData.push(vboard);
            }

          BagiBoardFI = BagiBoardFI - 1;

          if (BagiBoardFI != 0)
            {
                dataHabisFI = false;
                MulaiDataFI = MulaiDataFI + showData;
            }
            else
            {
                MulaiDataFI = 0;
                BagiBoardFI = 0;
                dataHabisFI = true;
            }

            return boardData;

            // console.log("***data chip***", vdata);
            // return vdata;
        });
        pushRefCounter();
        irregularFactory.setOnBoardCreated(boardName, function(vboard){
            pullRefCounter(function(){
                onPanelSizeChanged(null);
            })
        });


        irregularFactory.showBoard(boardName);


        // // Irregularity Waiting Teco
        // var boardConfig = {
        //     type: 'kanban',
        //     chipUrl: '/api/as/Boards/Irregular/gr/wfi',
        //     board:{
        //       container: 'irggr-waiting_inspection',
        //       iconFolder: 'images/boards/iregular',
        //       chipGroup: 'irregular_gr',
        //     },
        //     stage: {
        //       maxHeight: allBoardHeight,
        //       minHeight: allBoardHeight,
        //     },
        //     chip: {
        //       height: 100,
        //       width: 280,
        //       gap: 2
        //     },
        //     layout: {
        //     },
        //     argChips: {
        //       currentDate: new Date()
        //     }
        // }
        // var boardName = 'waiting_inspection';
        // nameList.push(boardName);
        // var updateIconsWaitFI = function(vitem){
        //     //console.log("vitem", vitem);
        //     // if (vitem.Teknisi===null){
        //     //     vitem.statusicon.push('assignment')
        //     // }
        // }
        // irregularFactory.createBoard(boardName, boardConfig);
        // irregularFactory.setOnChipLoaded(boardName, function(vdata){

        //     var vNow = new Date();
        //     for(var i=0; i<vdata.length; i++){
        //       vitem = vdata[i];
        //       vitem.chip = 'waiting_inspection';
        //       vitem.nopol = vitem.PoliceNumber;
        //       // vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
        //       vitem.workrelease = $filter('date')(vitem.JobDate, 'dd/MMM/yyyy HH:mm'); // WO Release
        //       vitem.workstart = $filter('date')(vitem.PlanDateTimeStart, 'dd/MMM/yyyy HH:mm');
        //       vitem.pekerjaan = vitem.JenisPekerjaan;
        //       vitem.category = vitem.JenisPekerjaan;
        //       vitem.sa_name = vitem.NameSa;
        //       vitem.type = vitem.ModelType;
        //       var vmili = vitem.PlanDateTimeStart - vNow;
        //       var vminutes = vmili/(1000 * 60);
        //       vitem.timewait = JsBoard.Common.getTimeString(vminutes);
        //       vitem.deltaTime = vitem.PlanDateTimeStart - vNow;
        //       vitem.statusicon = [];
        //       vitem.wotype = vitem.WoCategory;
        //       updateIconsWaitFI(vitem)
        //     }

        //     vdata.push({chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
        //           wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'});
          
        //     // vdata = [
        //     //     {chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
        //     //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'},
        //     //     {chip: 'waiting_inspection', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 907 MO', start: '09:20', type: 'AVANZA', 
        //     //       wotype: 'SBM', sa_name: 'BUD', foreman: 'MIN', statusicon: [], timewait: '00:15', category: 'B'},
        //     //     {chip: 'waiting_inspection', row: 1, col: 2.5, timestatus: 1, width: 1, nopol: 'B 6467 AA', start: '09:30', type: 'AVANZA', 
        //     //       wotype: 'XYZ', sa_name: 'ANI', foreman: 'DON', icons: [], timewait: '00:08', category: 'W'},
        //     //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3888 CD', start: '10:00', type: 'AVANZA', 
        //     //       wotype: 'GR', sa_name: 'BUD', foreman: 'FIN', icons: ['watch', 'arrow1'], timewait: '00:04', category: 'O'},
        //     //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9888 HN', start: '10:05', type: 'AVANZA', 
        //     //       wotype: 'GR', sa_name: 'BUD', foreman: 'JOK', icons: ['watch', 'arrow1'], timewait: '00:03', category: 'O'},
        //     // ]
        //     $scope.count_inspect = vdata.length;
        //     console.log("***data chip***", vdata);
        //     return vdata;
        // });
        // pushRefCounter();
        // irregularFactory.setOnBoardCreated(boardName, function(vboard){
        //     pullRefCounter(function(){
        //         onPanelSizeChanged(null);
        //     })
        // });


        // irregularFactory.showBoard(boardName);



        ////

        // Irregularity Waiting Inspection
        var boardConfig = {
            type: 'kanban',
            chipUrl: '/api/as/Boards/Irregular/gr/wteco',
            // /api/as/Boards/Irregular/gr/wteco
            board:{
              container: 'irggr-waiting_teco',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'irregular_gr',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              // height: 100,
              height: 120,
              width: 280,
              gap: 2
            },
            layout: {
            },
            argChips: {
              currentDate: new Date()
            }
        }

        var boardName = 'waiting_teco';
        nameList.push(boardName);
        var updateIconsWaitTeco = function(vitem){
            // console.log("vitem", vitem);
            // if (vitem.Teknisi===null){
            //     vitem.statusicon.push('assignment')
            // }
        }
        irregularFactory.createBoard(boardName, boardConfig);
        irregularFactory.setOnChipLoaded(boardName, function(vdata){

            var vNow = new Date();
            for(var i=0; i<vdata.length; i++){
              vitem = vdata[i];
              vitem.chip = 'waiting_teco';
              vitem.nopol = vitem.PoliceNumber;
              // vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
              vitem.workrelease = $filter('date')(vitem.FIClockOff, 'dd/MMM/yyyy HH:mm'); 
              // vitem.workstart = $filter('date')(vitem.PlanDateTimeStart, 'dd/MMM/yyyy HH:mm');
              vitem.pekerjaan = vitem.JenisPekerjaan;
              vitem.category = vitem.JenisPekerjaan;
              vitem.sa_name = vitem.NameSa;
              vitem.type = vitem.ModelType;
            //   var vmili = vitem.PlanDateTimeStart - vNow;
              var vmili = Math.abs(vNow - vitem.FIClockOff);
              var vminutes = vmili/(1000 * 60);
              vitem.timewait = JsBoard.Common.getTimeString(vminutes);
              vitem.deltaTime = vitem.FIClockOff - vNow;
              vitem.statusicon = [];
              vitem.wotype = vitem.WoCategory;
              updateIconsWaitTeco(vitem)

              arrTECO.push(vitem)
            }

            // vdata.push({chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
            //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'});
          
            // vdata = [
            //     {chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
            //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'},
            //     {chip: 'waiting_inspection', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 907 MO', start: '09:20', type: 'AVANZA', 
            //       wotype: 'SBM', sa_name: 'BUD', foreman: 'MIN', statusicon: [], timewait: '00:15', category: 'B'},
            //     {chip: 'waiting_inspection', row: 1, col: 2.5, timestatus: 1, width: 1, nopol: 'B 6467 AA', start: '09:30', type: 'AVANZA', 
            //       wotype: 'XYZ', sa_name: 'ANI', foreman: 'DON', icons: [], timewait: '00:08', category: 'W'},
            //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3888 CD', start: '10:00', type: 'AVANZA', 
            //       wotype: 'GR', sa_name: 'BUD', foreman: 'FIN', icons: ['watch', 'arrow1'], timewait: '00:04', category: 'O'},
            //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9888 HN', start: '10:05', type: 'AVANZA', 
            //       wotype: 'GR', sa_name: 'BUD', foreman: 'JOK', icons: ['watch', 'arrow1'], timewait: '00:03', category: 'O'},
            // ]
            $scope.count_teco = vdata.length;

            if (dataHabisTECO == true)
            {
                CountBoardTECO = arrTECO.length;
                ModulusTECO = parseInt(CountBoardTECO % showData);
                BagiBoardTECO = parseInt(CountBoardTECO / showData);
                if (ModulusTECO != 0)
                {
                    BagiBoardTECO = BagiBoardTECO + 1;
                }
            }

            var boardData = [];
            var max = $scope.count_teco;
            if ((MulaiDataTECO+showData) > $scope.count_teco){
              max = $scope.count_teco;
            } else {
              max = MulaiDataTECO+showData
            }
            for(var i= MulaiDataTECO;  i< max; i++){
              var vboard = arrTECO[i];
              boardData.push(vboard);
            }

          BagiBoardTECO = BagiBoardTECO - 1;

          if (BagiBoardTECO != 0)
            {
                dataHabisTECO = false;
                MulaiDataTECO = MulaiDataTECO + showData;
            }
            else
            {
                MulaiDataTECO = 0;
                BagiBoardTECO = 0;
                dataHabisTECO = true;
            }

            return boardData;
            // console.log("***data chip***", vdata);
            // return vdata;
        });
        pushRefCounter();
        irregularFactory.setOnBoardCreated(boardName, function(vboard){
            pullRefCounter(function(){
                onPanelSizeChanged(null);
            })
        });


        irregularFactory.showBoard(boardName);


        ///
        var boardConfig = {
            type: 'kanban',
            chipUrl: '/api/as/Boards/Irregular/gr/wnotif',
            // /api/as/Boards/Irregular/gr/wnotif
            board:{
              container: 'irggr-waiting_notif',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'irregular_gr',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              // height: 100,
              height: 120,
              width: 280,
              gap: 2
            },
            layout: {
            },
            argChips: {
              currentDate: new Date()
            }
        }

        
        var boardName = 'waiting_notif';
        nameList.push(boardName);
        var updateIconsWaitNotif = function(vitem){
            // console.log("vitem", vitem);
            // vitem.statusicon.push('billing')
            if (vitem.IsAllPulledBilling != 1){
                vitem.statusicon.push('billing')
            }

            if (vitem.IsWashingNotDone == 1){
                vitem.statusicon.push('car')
            }
        }
        irregularFactory.createBoard(boardName, boardConfig);
            irregularFactory.setOnChipLoaded(boardName, function(vdata){

                var vNow = new Date();
                for(var i=0; i<vdata.length; i++){
                vitem = vdata[i];
                vitem.chip = 'waiting_notif';
                vitem.nopol = vitem.PoliceNumber;
                // vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
                vitem.workrelease = $filter('date')(vitem.JanjiPenyerahan, 'dd/MMM/yyyy HH:mm'); 
                // vitem.workstart = $filter('date')(vitem.PlanDateTimeStart, 'dd/MMM/yyyy HH:mm');
                vitem.pekerjaan = vitem.JenisPekerjaan;
                vitem.category = vitem.JenisPekerjaan;
                vitem.sa_name = vitem.NameSa;
                vitem.type = vitem.ModelType;
                // var vmili = vitem.PlanDateTimeStart - vNow;
                var vmili = Math.abs(vNow - vitem.JanjiPenyerahan);
                var vmilis = (vNow - vitem.JanjiPenyerahan);

                var vminutes = vmili/(1000 * 60);
                vitem.timewait = JsBoard.Common.getTimeString(vminutes);

                if (vmilis < 0){
                  vitem.timewait = '-'+vitem.timewait
                }

                vitem.deltaTime = vitem.JanjiPenyerahan - vNow;
                vitem.statusicon = [];
                vitem.wotype = vitem.WoCategory;
                updateIconsWaitNotif(vitem)

                arrNOTIF.push(vitem);
                }

                // vdata.push({chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
                //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'});
            
                // vdata = [
                //     {chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
                //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'},
                //     {chip: 'waiting_inspection', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 907 MO', start: '09:20', type: 'AVANZA', 
                //       wotype: 'SBM', sa_name: 'BUD', foreman: 'MIN', statusicon: [], timewait: '00:15', category: 'B'},
                //     {chip: 'waiting_inspection', row: 1, col: 2.5, timestatus: 1, width: 1, nopol: 'B 6467 AA', start: '09:30', type: 'AVANZA', 
                //       wotype: 'XYZ', sa_name: 'ANI', foreman: 'DON', icons: [], timewait: '00:08', category: 'W'},
                //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3888 CD', start: '10:00', type: 'AVANZA', 
                //       wotype: 'GR', sa_name: 'BUD', foreman: 'FIN', icons: ['watch', 'arrow1'], timewait: '00:04', category: 'O'},
                //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9888 HN', start: '10:05', type: 'AVANZA', 
                //       wotype: 'GR', sa_name: 'BUD', foreman: 'JOK', icons: ['watch', 'arrow1'], timewait: '00:03', category: 'O'},
                // ]
                $scope.count_notif = vdata.length;

                if (dataHabisNOTIF == true)
                {
                    CountBoardNOTIF = arrNOTIF.length;
                    ModulusNOTIF = parseInt(CountBoardNOTIF % showData);
                    BagiBoardNOTIF = parseInt(CountBoardNOTIF / showData);
                    if (ModulusNOTIF != 0)
                    {
                        BagiBoardNOTIF = BagiBoardNOTIF + 1;
                    }
                }

                var boardData = [];
                var max = $scope.count_notif;
                if ((MulaiDataNOTIF+showData) > $scope.count_notif){
                  max = $scope.count_notif;
                } else {
                  max = MulaiDataNOTIF+showData
                }
                for(var i= MulaiDataNOTIF;  i< max; i++){
                  var vboard = arrNOTIF[i];
                  boardData.push(vboard);
                }

              BagiBoardNOTIF = BagiBoardNOTIF - 1;

              if (BagiBoardNOTIF != 0)
                {
                    dataHabisNOTIF = false;
                    MulaiDataNOTIF = MulaiDataNOTIF + showData;
                }
                else
                {
                    MulaiDataNOTIF = 0;
                    BagiBoardNOTIF = 0;
                    dataHabisNOTIF = true;
                }

                return boardData;
                // console.log("***data chip***", vdata);
                // return vdata;
            });
        pushRefCounter();
        irregularFactory.setOnBoardCreated(boardName, function(vboard){
            pullRefCounter(function(){
                onPanelSizeChanged(null);
            })
        });


        irregularFactory.showBoard(boardName);
        

        ///
        var boardConfig = {
            type: 'kanban',
            chipUrl: '/api/as/Boards/Irregular/gr/wdelivery',
            // /api/as/Boards/Irregular/gr/wdelivery
            board:{
              container: 'irggr-waiting_delivery',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'irregular_gr',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              // height: 100,
              height: 120,
              width: 280,
              gap: 2
            },
            layout: {
            },
            argChips: {
              currentDate: new Date()
            }
        }

        
        var boardName = 'waiting_delivery';
        nameList.push(boardName);
        var updateIconsWaitDeliv = function(vitem){
            // console.log("vitem", vitem);
            // if (vitem.Teknisi===null){
            //     vitem.statusicon.push('assignment')
            // }
        }
        irregularFactory.createBoard(boardName, boardConfig);
        irregularFactory.setOnChipLoaded(boardName, function(vdata){

            var vNow = new Date();
            for(var i=0; i<vdata.length; i++){
              vitem = vdata[i];
              vitem.chip = 'waiting_delivery';
              vitem.nopol = vitem.PoliceNumber;
              // vitem.start = $filter('date')(vitem.GateInPushTime, 'HH:mm');
              vitem.workrelease = $filter('date')(vitem.CustCallTime, 'dd/MMM/yyyy HH:mm'); 
              // vitem.workstart = $filter('date')(vitem.PlanDateTimeStart, 'dd/MMM/yyyy HH:mm');
              vitem.pekerjaan = vitem.JenisPekerjaan;
              vitem.category = vitem.JenisPekerjaan;
              vitem.sa_name = vitem.NameSa;
              vitem.type = vitem.ModelType;
            //   var vmili = vitem.PlanDateTimeStart - vNow;
              // var vmili = Math.abs(vNow - vitem.CustCallTime);
              // var vminutes = vmili/(1000 * 60);

              var vminutes = 0
            

              if (vitem.LamaHariCalltoSE == 0){
                vminutes = vitem.LamaWaktuCalltoSE
                vitem.timewait = JsBoard.Common.getTimeString(vminutes);
              } else {
                vitem.timewait = 'H+' + vitem.LamaHariCalltoSE
              }
              // vitem.timewait = JsBoard.Common.getTimeString(vminutes);
              vitem.deltaTime = vNow - vitem.CustCallTime;
              vitem.statusicon = [];
              vitem.wotype = vitem.WoCategory;
              updateIconsWaitDeliv(vitem)

              arrDELIVERY.push(vitem)
            }

            // vdata.push({chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
            //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'});
          
            // vdata = [
            //     {chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
            //       wotype: 'EM', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'},
            //     {chip: 'waiting_inspection', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 907 MO', start: '09:20', type: 'AVANZA', 
            //       wotype: 'SBM', sa_name: 'BUD', foreman: 'MIN', statusicon: [], timewait: '00:15', category: 'B'},
            //     {chip: 'waiting_inspection', row: 1, col: 2.5, timestatus: 1, width: 1, nopol: 'B 6467 AA', start: '09:30', type: 'AVANZA', 
            //       wotype: 'XYZ', sa_name: 'ANI', foreman: 'DON', icons: [], timewait: '00:08', category: 'W'},
            //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3888 CD', start: '10:00', type: 'AVANZA', 
            //       wotype: 'GR', sa_name: 'BUD', foreman: 'FIN', icons: ['watch', 'arrow1'], timewait: '00:04', category: 'O'},
            //     {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9888 HN', start: '10:05', type: 'AVANZA', 
            //       wotype: 'GR', sa_name: 'BUD', foreman: 'JOK', icons: ['watch', 'arrow1'], timewait: '00:03', category: 'O'},
            // ]
            $scope.count_delivery = vdata.length;

            if (dataHabisDELIVERY == true)
            {
                CountBoardDELIVERY = arrDELIVERY.length;
                ModulusDELIVERY = parseInt(CountBoardDELIVERY % showData);
                BagiBoardDELIVERY = parseInt(CountBoardDELIVERY / showData);
                if (ModulusDELIVERY != 0)
                {
                    BagiBoardDELIVERY = BagiBoardDELIVERY + 1;
                }
            }

            var boardData = [];
            var max = $scope.count_delivery;
            if ((MulaiDataDELIVERY+showData) > $scope.count_delivery){
              max = $scope.count_delivery;
            } else {
              max = MulaiDataDELIVERY+showData
            }
            for(var i= MulaiDataDELIVERY;  i< max; i++){
              var vboard = arrDELIVERY[i];
              boardData.push(vboard);
            }

          BagiBoardDELIVERY = BagiBoardDELIVERY - 1;

          if (BagiBoardDELIVERY != 0)
            {
                dataHabisDELIVERY = false;
                MulaiDataDELIVERY = MulaiDataDELIVERY + showData;
            }
            else
            {
                MulaiDataDELIVERY = 0;
                BagiBoardDELIVERY = 0;
                dataHabisDELIVERY = true;
            }

            return boardData;
            console.log("***data chip***", vdata);
            return vdata;
        });
        pushRefCounter();
        irregularFactory.setOnBoardCreated(boardName, function(vboard){
            pullRefCounter(function(){
                onPanelSizeChanged(null);
            })
        });

        irregularFactory.showBoard(boardName);
            finishGetData = true;
        }

    // Auto Refresh
    $scope.countDown = function() {
        if (Tabs.isExist('app.irregularity')) 
        {
            stopped = $timeout(function() {
                //console.log($scope.counter);
                if (!pause) 
                {
                    $scope.counter--;
                    if($scope.counter == 0)
                    {
                        console.log("====> refresh data ...");
                        //[START] refresh data
                        // $scope.LoadAllData();
                        // ==== ADDED CODE ====
                        var boardsName = irregularFactory.onGetDataBoardName();
                        var boardsConfig = irregularFactory.onGetDataConfig();
                        var tmpFunc = function(){
                          var count = 0
                          console.log("Boardsnameeeeee===>",boardsName);
                          for(var i in boardsName){
                            if(boardsConfig[boardsName[i]].statusGetData == 1){
                              count++
                            }
                          }
                          console.log("Boardsnameeeeee===>",count);
                          if(count == boardsName.length){
                            console.log("GET DATA");
                            // $scope.counter = 30;
                            $scope.counter = 60 //300; // timer nya
                            return true
                          }else{
                            tmpFunc()
                            $scope.counter = 0;
                            return false
                          }
                        }
                        tmpFunc();
                        $scope.LoadAllData();
                        console.log("onGetDataConfig====>",irregularFactory.onGetDataConfig());
                        
                        // [END] refresh data
                        // if(tmpFunc()){
                        //   $scope.counter = 30;
                        // }else{
                        //   tmpFunc();
                        // }
                        
                    }
                    if(finishGetData)
                    {
                        $scope.countDown();
                    }
                }
            }, 1000);
        }
      };
});

//resolusi tv 1366 x 768
