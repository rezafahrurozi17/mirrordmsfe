angular.module('app')
.controller('WoListGrController', function($scope, $http, CurrentUser, Parameter, ngDialog, $window, $timeout, $filter) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    var theChips;
    var queueBoard;
    $scope.wolist = {filterTeknisi: '', filterWo: ''}
    $scope.testing = "testing";


    var showBoard = function(){
        var boardConfig = {
            type: 'grid',
            board:{
                container: 'wo-list-board-gr',
                chipGroup: 'wolist', //<---- group dari chip
                iconFolder: 'images/boards/jpcb',
                headerHeight: 50,
                onChipCreated: function(vchip){
                }
            },
            stage: {
                height: 470,
                width: 960,
            },
            chip: {
                showPageIndex: 0,
                numberWidth: 24,
            }
        }

        queueBoard = new JsBoard.Container(boardConfig);
        var vDate = new Date();
        var vjam = $filter('date')(vDate, 'hh:mm');
        var vtgl = $filter('date')(vDate, 'dd/MM/yyyy');


        theChips = {
            headers: [
                {chip:'header', lokasi: 'header', height: 50},
            ],
            columnHeaders: [
                {chip: 'columnHeader', lokasi: 'columnHeader', width: 10, title: 'Menunggu Pengerjaan'},
                {chip: 'columnHeader', lokasi: 'columnHeader', width: 10, title: 'Dalam Pengerjaan'},
                {chip: 'columnHeader', lokasi: 'columnHeader', width: 10, title: 'Menunggu FI'},
            ],
            columns: [
                {chip: 'blank', lokasi: 'column', width: 10, name: 'menunggu_pengerjaan'},
                {chip: 'blank', lokasi: 'column', width: 10, name: 'dalam_pengerjaan'},
                {chip: 'blank', lokasi: 'column', width: 10, name: 'menunggu_fi'},
            ],
            rows: [
                {chip: 'none', lokasi: 'row', cellHeight: 104},
            ],
            cells: [
                // {chip:'cellChip', lokasi: 'cell', col: 1, row: 1, nopol: 'D 1245 AAA', jamdatang: '10:45', janjiserah: '06/04/2017 14:00', pekerjaan: 'GR', saname: 'ANDI', status: 'normal', tipe: 'Avanza', waktupengerjaan: '00:15:00'},
                // {chip:'cellChip', lokasi: 'cell', col: 2, row: 1, nopol: 'D 3445 AAB', jamdatang: '09:30', janjiserah: '06/04/2017 13:00', pekerjaan: 'EM', saname: 'ANTI', status: 'problem', tipe: 'Avanza', waktupengerjaan: '00:15:00'},
                // {chip:'cellChip', lokasi: 'cell', col: 2, row: 1, nopol: 'D 5645 AAC', jamdatang: '09:10', janjiserah: '06/04/2017 12:00', pekerjaan: 'EM', saname: 'ALDI', status: 'paused', tipe: 'Avanza', waktupengerjaan: '00:15:00'},
                // {chip:'cellChip', lokasi: 'cell', col: 3, row: 1, nopol: 'D 7845 AAD', jamdatang: '09:00', janjiserah: '06/04/2017 11:00', pekerjaan: 'EM', saname: 'ARDI', status: 'normal', tipe: 'Avanza', waktupengerjaan: '00:25:00'},
                // {chip:'cellChip', lokasi: 'cell', col: 3, row: 1, nopol: 'D 9045 AAE', jamdatang: '08:00', janjiserah: '06/04/2017 11:00', pekerjaan: 'EM', saname: 'ADRI', status: 'normal', tipe: 'Avanza', waktupengerjaan: '00:20:00'},
            ],
            columnFooters: [
            ],
            footers: [
                {chip:'footer', lokasi: 'footer', height: 30, text1: 'Normal', text2: 'Problem', text3: 'Paused'},
            ],
        }

        queueBoard.createChips(theChips, 'objarray'); //<--- untuk menampilkan chip grid
            // runningChip = queueBoard.Chips[2];
            // headerChip = queueBoard.Chips[0];
        queueBoard.redraw();              //<--- harus di-redraw

    }


    var refreshAllChips = function(){
        // queueBoard.createChips(theChips, 'objarray');
        // queueBoard.redraw();
    }

    var pullByCoord = function(row, col){
        var vresult = false;
        var idx = -1;
        var vlist = theChips.nopol;
        for(var i=0; i<vlist.length; i++){
            if (vlist[i].row==row && vlist[i].col==col){
                vresult = vlist[i];
                idx = i;
            } if (vlist[i].row>row && vlist[i].col==col){
                vlist[i].row = vlist[i].row-1;
            }
        }
        if (idx>=0){
            theChips.nopol.splice(idx, 1);
        }
        return vresult;
    }

    var callVehicle = function(vnopol, ctrId){
        var vcounter = theChips.counter[ctrId];
        listCall.push({
            nopol: vnopol,
            counter: vcounter.counterName,
            counterId: ctrId,
        });

        theChips.general[5].text = listCallStr(listCall);

        var vstr = '@'+vnopol+'#'+(ctrId+1);
        JsSound.playString(vstr);

    }
    
    var recallVehicle = function(idx){
        var vitems = listCall.splice(idx, 1);
        var vitem = vitems[0];
        listCall.push({
            nopol: vitem.nopol,
            counter: vitem.counter,
            counterId: vitem.counterId,
        });

        theChips.general[5].text = listCallStr(listCall);

        var vstr = '@'+vitem.nopol+'#'+(vitem.counterId+1);
        JsSound.playString(vstr);

    }
    

    // $scope.$on('$viewContentLoaded', function() {

        $scope.loading=true;
        $scope.gridData=[];

        $timeout(showBoard, 10);





    // });


    $scope.$on('$destroy', function() {
    })

    if (typeof JsBoard.Common.statusResize ==='undefined'){
        JsBoard.Common.statusResize = true;
    }

    $('.testing-div').each(function(x){
        console.log("div", this);
    })

    var velement = document.getElementById('base-wo-list-board-gr');

    var erd = elementResizeDetectorMaker();
    erd.listenTo(velement, function(element) {
        if(queueBoard){
            queueBoard.autoResizeStage();
        }
        var width = element.offsetWidth;
        var height = element.offsetHeight;
        console.log("Size: " + width + "x" + height);
    });
    
});
