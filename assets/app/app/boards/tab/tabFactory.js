angular.module('app')
    .factory('TabGrFactory', function($rootScope, $http, CurrentUser, $filter, $timeout, jpcbFactory, ngDialog) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            onPlot: function(){
                // do nothing
            },
            // ===== CODE Added
            checkTime:function(vdata){
                var planStart = this.timeToColumn(vdata.PlanStart);
                var actualFinish =  this.timeToColumn(vdata.PlanFinish);
                vdata.JumlahActualRate = actualFinish - planStart;
                vdata.JumlahActualRate = Math.ceil(Math.abs(vdata.JumlahActualRate)*4)/4;

                var cekAwal = vdata.PlanStart.split(':');
                var cekAkhir = vdata.PlanFinish.split(':');
                if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                        // vdata.JumlahActualRate = vdata.JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                    // vdata.JumlahActualRate = vdata.JumlahActualRate+1;
                }
                return vdata.JumlahActualRate * 60 / this.incrementMinute;
            },
            getBreakTime: function(){
                var vstartMin = JsBoard.Common.getMinute(this.breakHour);
                var vendMin = vstartMin+60;
                var vBreakEnd = JsBoard.Common.getTimeString(vendMin);
                var vstart = this.timeToColumn(this.breakHour);
                var vend = this.timeToColumn(vBreakEnd);
                return [{
                    start: vstart,
                    end: vend,
                    width: vend-vstart,
                }]
            },
            isOverlapped: function(x1, x2, y1, y2){
                return (x1 < y2) && (y1 < x2)
            },
            getBreakChipTime: function(vstart, vlength){
                var vbreaks = this.getBreakTime();
                var vbreakCount = 0;
                var vtotalBreak = 0;
                var vresult = {}
                for(var i=0; i<vbreaks.length; i++){
                    vbreak = vbreaks[i];
                    if (this.isOverlapped(vstart, vstart+vlength, vbreak.start, vbreak.end)){
                        if (vstart>vbreak.start){
                            return 'start-inside-break';
                        }
                        vbreakCount = vbreakCount+1;
                        vresult['break'+vbreakCount+'start'] = vbreak.start-vstart;
                        vresult['break'+vbreakCount+'width'] = vbreak.width;
                        vtotalBreak = vtotalBreak+vbreak.width;
                    }
                }
                if (vbreakCount==0){
                    return 'no-break';
                } else {
                    vresult['breakWidth'] = vtotalBreak;
                }
                return vresult;

            },
            updateChipBreak: function(vchipData){
                if (vchipData.breakWidth){
                    vchipData.width = vchipData.width-vchipData.breakWidth;
                }
                var vbreaks = 'no-break';
                if (vchipData.isPrediagnose){
                    if (vchipData.PrediagStallId!=0){
                        vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width);
                    }
                } else {
                    if (vchipData.stallId!=0){
                        vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width);
                    }

                }

                var deleteList = ['break1start', 'break1width', 'break2start', 'break2width', 'break3start', 'break3width','break4start', 'break4width','breakWidth'];
                for (var i=0; i<deleteList.length; i++){
                    var idx = deleteList[i];
                    delete vchipData[idx];
                }
                if (typeof vbreaks=='object'){
                    for (var idx in vbreaks){
                        vchipData[idx] = vbreaks[idx];
                    }
                }
                console.log("===============>",vchipData);
                if (vchipData.breakWidth){
                    vchipData.width = vchipData.width+vchipData.breakWidth;
                    vchipData.width1 = vchipData.break1start;
                    vchipData.start2 = vchipData.break1start+vchipData.break1width;
                    vchipData.width2 = vchipData.width-vchipData.start2;
                } else {
                    vchipData.width1 = vchipData.width;
                }
                // console.log('vchipData', vdata);

                // console.log('vbreaks', vbreaks);
                //////////
            },
            // ==================================
            chipLoaded: false,
            boardName: 'taboard',
            showBoard: function(config){
                this.chipLoaded = false;
                var vcontainer = config.container;
                var vname = this.boardName;
                if (typeof config.boardName !== 'undefined'){
                    vname = config.boardName;
                } else {
                    config.boardName = vname;
                }
                config.test = 'xx1234567';
                var vscrollhpos = jpcbFactory.getScrollablePos(vname);

                var velement = document.getElementById(vcontainer);
                var velement = document.body;

                // var erd = elementResizeDetectorMaker();
                // erd.listenTo(velement, function(element) {
                //     console.log('erd.....');
                //     if (typeof $rootScope.tabDoRefresh !== 'undefined'){
                //         $timeout( function(){
                //             $rootScope.tabDoRefresh();
                //         }, 500);
                //     }
                // });
                            
                var vobj = document.getElementById(vcontainer);
                if(vobj){
                    var vwidth = vobj.clientWidth;
                    console.log(vwidth);
                    if (vwidth<=0) {
                        return;
                    }
                } else {
                    return;
                }
                var boardContainerWidth = vwidth;
                if (typeof config.onPlot=='function'){
                    this.onPlot = config.onPlot;
                }

                var vdurasi = 1;
                if (typeof config.currentChip.durasi !=='undefined'){
                    vdurasi = config.currentChip.durasi;
                }
                config.currentChip.width = (vdurasi/60)*this.incrementMinute;
                config.currentChip.boardName = vname;

                var vrow = -2;
                if (typeof config.currentChip.stallId !=='undefined'){
                    if (config.currentChip.stallId>0){
                        vrow = config.currentChip.stallId - 1 ;
                    }
                }
                config.currentChip.row = vrow;

                var vhourStart = JsBoard.Common.getMinute(this.startHour)/60;
                var vcol = 0;
                if (typeof config.currentChip.startTime !=='undefined'){
                    vstartTime = config.currentChip.startTime;
                    vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                }
                config.currentChip.col = vcol;

                jpcbFactory.createBoard(vname, {
                    boardName: vname,
                    containerWidth: boardContainerWidth,
                    board: {
                        chipGroup: 'asb_gr',
                        columnWidth: 120,
                        rowHeight: 80,
                        footerChipName: 'tab_footer',
                        stopageChipTop: 90,
                        scrollableLeft: vscrollhpos,
                        checkDragDrop: function(vchip){
                            return false;
                        },
                        wrapStopage: 1
                    },
                    chip: {
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute
                    }
                });
                var me = this;
                jpcbFactory.setHeader(vname, {
                    headerHeight: 40,
                    onGetHeader: function(arg, vfunc){
                        return me.onGetHeader(arg, vfunc);
                    }
                });
                jpcbFactory.setStall(vname, {
                    firstColumnWidth: 0,
                    onGetStall: function(arg, vfunc){
                        return me.onGetStall(arg, vfunc);
                    }
                });

                jpcbFactory.setOnGetChips(vname, function(arg, vfunc){
                    return me.onGetChips(arg, vfunc);
                });

                jpcbFactory.setOnChipLoaded(vname, function(arg, vfunc){
                    return me.onChipLoaded(arg, vfunc);
                });
                
                jpcbFactory.setSelectionArea(vname, {tinggiArea: 4, fixedHeaderChipName: 'blank'});

                jpcbFactory.setStopageArea(vname, {tinggiArea: 160});
                

                jpcbFactory.setArgChips(vname, config.currentChip);

                // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

                jpcbFactory.setOnCreateChips(vname, function(vchip){
                    if (vchip.data.selected){
                        vchip.draggable(true);
                    }
                });

                jpcbFactory.setOnBoardReady(vname, function(vjpcb){
                    return me.onBoardReady(vjpcb);
                });
                
                var me = this;
                var noshowUrl = '/api/as/Jobs/SetNoShow';
                var the_config = config;
                jpcbFactory.setOnDragDrop(vname, function(vchip){
                    var vnopol = vchip.data.nopol;
                    var vjobid = vchip.data.JobId;
                    me.showMessage('Apakah Nopol '+vnopol+' akan dijadikan No Show?',
                        function(){
                            $http.put(noshowUrl, [vjobid])
                        },
                        function(){
                            var vlayout = vchip.container.layout;
                            var vstarty = vlayout.row2y(vchip.dragStartPos.row);
                            var vstartx = vlayout.col2x(vchip.dragStartPos.col);

                            vchip.moveTo(vchip.container.board.chips);
                            vchip.x(vstartx);
                            vchip.y(vstarty);
                            vchip.data.col = vchip.dragStartPos.col;
                            vchip.data.row = vchip.dragStartPos.row;
                            vchip.container.updateChip(vchip);
                            vchip.container.redraw();

                        }
                    );

                    // // var vstarty = vchip.dragStartPos.y;
                    // // var vstartx = vchip.dragStartPos.x;
                    // var vlayout = vchip.container.layout;
                    // var vstarty = vlayout.row2y(vchip.dragStartPos.row);
                    // var vstartx = vlayout.col2x(vchip.dragStartPos.col);

                    // vchip.moveTo(vchip.container.board.chips);
                    // vchip.x(vstartx);
                    // vchip.y(vstarty);
                    // vchip.data.col = vchip.dragStartPos.col;
                    // vchip.data.row = vchip.dragStartPos.row;
                    // vchip.container.updateChip(vchip);

                    // vchip.container.redrawBoard();

                    // var curDate = config.currentChip.currentDate;
                    // var vdurasi = 1;
                    // if (typeof config.currentChip.durasi !=='undefined'){
                    //     vdurasi = config.currentChip.durasi;
                    // }

                    // var vmin = me.incrementMinute * vchip.data['col'];
                    // var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                    // var vres = JsBoard.Common.getTimeString(vminhour);
                    // var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
                    // startTime.setMinutes(startTime.getMinutes()+vmin);

                    // var emin = vmin + vdurasi * 60;
                    // var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
                    // endTime.setMinutes(endTime.getMinutes()+emin);

                    // vchip.data.startTime = startTime;
                    // vchip.data.endTime = endTime;
                    // var vStallId = 0;
                    // if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                    //     && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                    //     && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                    //     vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                    // }
                    // vchip.data.stallId = vStallId;

                    // if (typeof me.onPlot=='function'){           

                    //     me.onPlot(vchip.data);
                    // }
                    console.log("on drag-drop vchip", vchip);
                });

                jpcbFactory.showBoard(vname, vcontainer)
                // $timeout(function(){
                //     jpcbFactory.showBoard(vname, vcontainer)

                // }, 100);

            },
            startHour: '08:00',
            endHour: '17:00',
            breakHour: '12:00',
            noShowTolerance: 15, // minutes
            incrementMinute: 60,
            doSetNoShow: function(vjobid){
                var noshowUrl = '/api/as/Jobs/SetNoShow';
                var vname = this.boardName;
                $http.put(noshowUrl, [vjobid]).then(function(response) {
                    //jpcbFactory.redrawBoard(vname);
                    if (typeof $rootScope.tabDoRefresh !== 'undefined'){
                        $timeout( function(){
                            $rootScope.tabDoRefresh();
                        }, 2000);
                    }
                });

            },
            timeToColumn: function(vtime){
                
                var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vmin = JsBoard.Common.getMinute(vtime)-vstartmin;
                var vresult = vmin/this.incrementMinute;
                
                return vresult;
            },
            onBoardReady: function(vjpcb){
                // var vboard = vjpcb.theBoard;
                // if (typeof vboard!=='undefined' && typeof vboard.layout !== 'undefined' 
                //     ){
                //     var vHeight = vboard.layout.calcStopageHeight(vboard.chipItems);
                //     if (vHeight>0){
                //         jpcbFactory.setStopageArea(vjpcb.boardName, {tinggiArea: vHeight});
                //     }
                // }

                jpcbFactory.setStopageArea(vjpcb.boardName, {tinggiArea: 500});
                this.updateTimeLine(vjpcb.boardName);
                this.initRefreshTime();
            },

            refreshPeriodeMs: 10000,
            nextRefreshTime: false,
            refreshReady: false,
            initRefreshTime: function(){
                if (!this.refreshReady){
                    this.refreshReady = true;
                    this.nextRefreshTime = new Date();
                    this.nextRefreshTime.setTime(this.nextRefreshTime.getTime()+this.refreshPeriodeMs);
                    this.refreshTick();
                }
            },
            // waitRefresh: function(){
            //     this.refreshReady = false;
            // },
            startRefresh: function(){
                this.refreshReady = true;
            },
            refreshTick: function(){
                var vme = this;
                $timeout( function(){
                    vme.executeRefresh();
                    vme.refreshTick();
                }, 500);
            },
            executeRefresh: function(){
                var vme = this;
                if (vme.refreshReady){
                    var currentTime = new Date();
                    if (vme.nextRefreshTime.getTime()<currentTime.getTime()){
                        console.log(currentTime.getTime(), " do Refresh");
                        if (typeof $rootScope.tabDoRefresh !== 'undefined'){
                            $rootScope.tabDoRefresh();
                        }
                        vme.nextRefreshTime.setTime(vme.nextRefreshTime.getTime()+vme.refreshPeriodeMs);
                    }
                    delete currentTime;
                }
            },

            getCurrentMinute: function(){
                var vDate = this.getCurrentTime();
                var currentHour = $filter('date')(vDate, 'HH:mm');
                var vcurrmin = JsBoard.Common.getMinute(currentHour);
                return vcurrmin;
            },
            checkAppointmentStatus: function(vtime, vstatus){
                
                // vtime: time-nya data
                // currentHour: jam/minute saat ini
                if ((vstatus==1) || (vstatus==2)){
                    return 'no_show';
                }
                var vDate = this.getCurrentTime();
                console.log("current Time#####", vDate.getTime());
                var currentHour = $filter('date')(vDate, 'HH:mm');

                var vcurrmin = JsBoard.Common.getMinute(currentHour);
                var vmin = JsBoard.Common.getMinute(vtime);
                //if ((vmin<vcurrmin) && (vstatus==0)){
                if (vstatus==0){
                    return 'no_show';
                }
                // console.log("***vstatus***", vstatus);
                return 'appointment';

            },
            onGetHeader: function(arg, onFinish){
                varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
                var vWorkHour = JsBoard.Common.generateDayHours({
                    startHour: this.startHour,
                    endHour: this.endHour,
                    increment: this.incrementMinute,
                });
                var result = [];
                for(var idx in vWorkHour){
                    var vitem = {title: vWorkHour[idx]}
                    if (vWorkHour[idx]==this.breakHour){
                        vitem.colInfo = {isBreak: true}
                    }
                    result.push(vitem)

                }
                console.log("header", result);
                $timeout(function(){
                    onFinish(result);
                }, 100);
                
            },
            onGetStall: function(arg, onFinish){
                // $http.get('/api/as/Stall').
                // $http.get('/api/as/Boards/Stall/0').then(function(res){
                $http.get('/api/as/Boards/Stall/1').then(function(res){
                    var xres = res.data.Result;
                    var result = [];
                    for(var i=0;  i<xres.length; i++){
                        var vitem = xres[i];
                        result.push({
                            title: vitem.Name,
                            status: 'normal',
                            stallId: vitem.StallId
                        });
                    }
                    onFinish(result);
                })

            },
            findStallIndex: function(vStallId, vList){
                for (var i=0; i<vList.length; i++){
                    var vitem = vList[i];
                    if ((typeof vitem.stallId!=='undefined') && (vitem.stallId==vStallId)){
                        return i;
                    }
                }
                return -1;
            },
            onChipLoaded: function(vjpcb){
                var vresult = []
                for (var i=0; i<vjpcb.allChips.length; i++){
                    var vitem = vjpcb.allChips[i];
                    vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                    var vpush = true;

                    if ((vitem.itemStatus==1) || (vitem.itemStatus==2)){
                        vitem.row = -1;
                    } else {
                        if (vidx>=0){
                            vitem.row = vidx;
                        } else {
                            vpush = false;
                        }
                    }
                    if (vpush){
                        vresult.push(vitem);
                    }
                    
                }
                vjpcb.allChips = vresult;
                this.chipLoaded = true;
            },
            calcStopageHeight: function(vitems, vMaxWidth){
                var vtop = 0;
                var vHorzGap = 20;
                var vVertGap = 10;
                var vColWidth = 120;
                var vRowHeight = 90;
                var vleft = vHorzGap;
                var vDelta = 3;
                var vYDelta = 120;
                
                for (var i=0; i<vitems.length; i++){
                    vitem = vitems[i];
                    if (vitem.stallId==-1){
                        var vwidth = vitem.width * vColWidth;
                        if ((vleft + vwidth)>(vMaxWidth-vDelta)){
                            vleft = vHorzGap;
                            vtop = vtop+vRowHeight + vVertGap;
                        }
                        vleft = vleft+vwidth;    
                    }
                }
                if (vleft>vHorzGap){
                    vtop = vtop+vRowHeight + vVertGap;
                }
                return vtop+vYDelta;
            },
            onGetChips: function(arg, onFinish){
                var vArg = {nopol: 'B 1000 AAA', tipe: 'Avanza'}
                vArg = JsBoard.Common.extend(vArg, arg);
                
                var tglAwal = arg.currentDate;
                var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
                tglAkhir.setDate(tglAkhir.getDate());
                //var vurl = '/api/as/Boards/Appointment/1/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/'+$filter('date')(tglAkhir, 'yyyy-MM-dd');
                var vurl = '/api/as/Boards/appointment/'+$filter('date')(tglAwal, 'yyyy-MM-dd');
                var me = this;
                $http.get(vurl).then(function(res){
                    // var result = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, 
                    //     daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}];
                    var result = [];
                    var xres = res.data.Result;
                    for(var i=0;  i<xres.length; i++){
                        var vitem = xres[i];
                        var vstatus = 'no_show';
                        
                        // var vstatus = me.checkAppointmentStatus(vitem.AppointmentTime);
                        // var vTime = vitem.PlanStart;

                        var vTime = vitem.AppointmentTime;
                        if (vitem.Status>3){
                            var vApDate = $filter('date')(vitem.AppointmentDate, 'yyyy-MM-dd');
                            var vPlDate = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd');

                            if (vApDate==vPlDate){
                                vitem.AppointmentTime = vitem.PlanStart;
                                vTime = vitem.AppointmentTime;
                            } else {
                                vitem.StallId = 0;
                            }
                        }
                        var vcurmin = me.getCurrentMinute();
                        var vchpmin = JsBoard.Common.getMinute(vTime);
                        var vdelta = me.noShowTolerance;
                        //(vapptime+vdelta<vcurrmin){
                        if ((vitem.Status==3) && (vchpmin+vdelta<vcurmin)){
                            //vstall = -1;
                            vitem.Status = 2;
                        }

                        var vstall = vitem.StallId;
                        if((vitem.Status==1) || (vitem.Status==2)){
                            // console.log("**vitem**", vitem);
                            // vstatus = 'no_show';
                            vstall = -1;
                        }
                        // console.log("**vitem**", vitem);
                        vstatus = me.checkAppointmentStatus(vTime, vitem.Status);
                        var vicon = 'customer';
                        if (vstatus=='appointment'){
                            vicon = 'customer_gray';
                        }
                        var vcolumn = me.timeToColumn(vTime);
                        //vstatus = 'no_show';
                        // ===================
                        if(vstall == -1){
                            vitem.width = 1;
                        }else{
                            vitem.width = me.checkTime(vitem);
                        }
                        vitem.col = vcolumn;
                        vitem.stallId = vstall;
                        vitem.row = 0;
                        vitem.daydiff = 0;
                        vitem.status = 'no_show';
                        vitem.statusicon = [vicon];
                        vitem.AppointmentTime = vTime;
                        vitem.nopol = vitem.PoliceNumber;
                        vitem.itemStatus = vitem.Status;
                        vitem.tipe = vitem.ModelType;
                        vitem.chip = 'tab';

                        me.updateChipBreak(vitem);
                        if(vstall == -1){
                            vitem.width = 1;
                            vitem.normal = 1;
                            vitem.width1 = 1;
                            delete vitem.width2;
                            delete vitem.start2;
                            // delete vitem.width1;
                        }
                        // ===================
                        if (vitem.Status!==1){
                            // result.push({
                            //     chip: 'tab', 
                            //     // AppointmentTime: vitem.AppointmentTime,
                            //     JobId: vitem.JobId,
                            //     AppointmentTime: vTime,
                            //     nopol: vitem.PoliceNumber, 
                            //     itemStatus: vitem.Status,
                            //     tipe: vitem.ModelType, 
                            //     stallId: vstall,
                            //     row: 0, col: vcolumn, 
                            //     width: me.checkTime(vitem), // get Real Duration
                            //     // width: 1, 
                            //     daydiff: 0, 
                            //     status: 'no_show', 
                            //     statusicon: [vicon]
                            // });
                            result.push(vitem);
                        }
                    }
                    var vCountNoShow = 0;
                    var vCountAll = 0;
                    for (var i=0; i<result.length; i++){
                        var resitem = result[i];
                        if (resitem.stallId==-1){
                            vCountNoShow = vCountNoShow+1;
                        }
                        vCountAll = vCountAll+1;
                    }
                    if (typeof arg.infoTab !== 'undefined'){
                        arg.infoTab.allAppointment = vCountAll;
                        arg.infoTab.noShowAppointment = vCountNoShow;
                        var vDate = new Date();
                        var xdate = $filter('date')(vDate, 'yyyy-MM-dd');
                        var ydate = $filter('date')(arg.today, 'yyyy-MM-dd');
                        if (xdate!==ydate){
                            arg.today = vDate;
                        }
                    }

                    var vbconfig = jpcbFactory.boardConfigList[vArg.boardName];
                    var vobj = document.getElementById(vbconfig.board.container);
                    var vwidth = 0;
                    if(vobj){
                        vwidth = vobj.clientWidth;
                        console.log(vwidth);
                        if (vwidth<=0) {
                        }
                    }
                    if (typeof vbconfig.containerWidth){
                        var vlayout = new JsBoard.Layouts['jpcb'](vbconfig.layout);
                        var vHeight = me.calcStopageHeight(result, vbconfig.containerWidth);
                        jpcbFactory.setStopageArea(vArg.boardName, {tinggiArea: vHeight});    
                    }
                        
                    // if (typeof vboard!=='undefined' && typeof vboard.layout !== 'undefined' 
                    //     // && typeof vboard.layout.board !== 'undefined'
                    //     // && typeof vboard.layout.stopageWidth !== 'undefined'
                    //     // && typeof vboard.layout.board.stopageHorizontalGap !== 'undefined'
                    //     // && typeof vboard.layout.board.stopageVerticalGap !== 'undefined'
                    //     // && typeof vboard.layout.board.columnWidth !== 'undefined'
                    //     ){
                    //     var vHeight = vboard.layout.calcStopageHeight(result);
                    //     if (vHeight>0){
                    //         jpcbFactory.setStopageArea(vArg.boardName, {tinggiArea: vHeight});
                    //     }
    
                    //     // var vleft = vboard.layout.board.stopageHorizontalGap;
                    //     // //var vtop = vboard.layout.board.stopageChipTop;
                    //     // var vtop = 0;
                    //     // for(var i=0;  i<result.length; i++){
                    //     //     var vItem = result[i];
                    //     //     var xwidth = vItem.width * vboard.layout.board.columnWidth;
                    //     //     if (vleft+xwidth>vboard.layout.stopageWidth){
                    //     //         vtop = vtop+vboard.layout.board.stopageVerticalGap+vboard.layout.board.rowHeight;
                    //     //     }
    
                    //     // }
        


                    //     // var vsHeight = vboard.layout.stopageMaxHeight;
                    //     // jpcbFactory.setStopageArea(vArg.boardName, {tinggiArea: vsHeight});
                    // }
                    // if (me.xxcounter>1){
                    //     jpcbFactory.setStopageArea(vArg.boardName, {tinggiArea: 400});
                    // } else {
                    //     me.xxcounter = me.xxcounter+1;
                    // }

                    var compare = function(a,b) {
                        vidx1 = 9999;
                        vidx2 = 9999;
                        if (a.AppointmentTime){
                            vidx1 = a.AppointmentTime;
                        }
                        if (b.AppointmentTime){
                            vidx2 = b.AppointmentTime;
                        }
                        if (vidx1 < vidx2)
                          return -1;
                        if (vidx1 > vidx2)
                          return 1;
                        return 0;
                    }
                    
                    result.sort(compare);

                    onFinish(result);
                })
                

            },
            xxcounter: 0,
            getCurrentTime: function(){
                return jpcbFactory.getCurrentTime();
            },
            updateChipStatus: function(){
                var boardname = this.boardName;
                var me = this;
                var vboard = jpcbFactory.getBoard(boardname);
                jpcbFactory.processChips(boardname, function(vchip){
                    return true;
                }, function(vchip){
                    // vchip.data.status = 'appointment';
                    // vboard.updateChip(vchip);
                    // //var vitem = vchip.data;
                    // //vitem.status=='appointment';
                    // return;
                    var vitem = vchip.data;
                    var vstatus = me.checkAppointmentStatus(vitem.AppointmentTime, vitem.itemStatus);

                    var vDate = me.getCurrentTime();
                    var currentHour = $filter('date')(vDate, 'HH:mm');
                    vapptime = JsBoard.Common.getMinute(vitem.AppointmentTime);
                    var vdelta = me.noShowTolerance;
                    var vcurrmin = JsBoard.Common.getMinute(currentHour);
                    if ((vstatus=='no_show') && (vitem.stallId!==-1)){
                        // console.log(vitem);
                        // console.log('appointment time', vapptime);
                        // console.log('current time', vcurrmin);
                        if (vapptime+vdelta<vcurrmin){
                            //vstatus = 'do_no_show';
                            vitem.stallId = -1;
                            me.doSetNoShow(vitem.JobId);
                        }
                    }

                    if (vitem.status !== vstatus){
                        vitem.status = vstatus;
                        vboard.updateChip(vchip);
                    }
                    if (vitem.status=='no_show'){
                        vchip.draggable(true);
                    }

                });

            },
            updateTimeLine: function(boardname){
                //var boardname = this.boardName;
                var vDate = this.getCurrentTime();
                console.log("current Time#####", vDate.getTime());
                var currentHour = $filter('date')(vDate, 'HH:mm');
                
                console.log("****currentHour", currentHour);
                //var startHour = this.
                //var minutPerCol

                jpcbFactory.updateTimeLine(boardname, currentHour, this.startHour, this.incrementMinute);

                this.updateChipStatus();

                jpcbFactory.redrawBoard(boardname);
            },

            showMessage: function(vmessage, onOk, onCancel){
                $rootScope.tab = {message: vmessage, title: 'Konfirmasi No Show', okText: 'Ya', cancelText: 'Tidak'}

                ngDialog.openConfirm ({
                    width: 700,
                    template: 'app/boards/tab/tabMessage.html',
                    plain: false,
                    scope: $rootScope,
                })
                .then(function(value) {
                    if (onOk){
                        return onOk(value);
                    }
                }, function(reason) {
                    if (onCancel){
                        return onCancel(reason);
                    }
                });                

            },
            // onGetChips: function(arg){
            //     var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
            //     vArg = JsBoard.Common.extend(vArg, arg);
            //     var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     ]
            //     var today = new Date();
            //     if (arg.currentDate){
            //         var vdiff = this.dayDiff(today, arg.currentDate);
            //         var vres = []
            //         for (var i=0; i<vitems.length; i++){
            //             if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
            //                 vres.push(vitems[i]);
            //             }
            //         }
            //         return vres;
            //     }
            //     return []
            // },
            dayDiff: function(date1, date2){
                return Math.round((date2 - date1) / (1000*60*60*24));
            }

            
        }
    });
