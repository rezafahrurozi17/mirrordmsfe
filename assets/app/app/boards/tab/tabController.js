angular.module('app')
    .controller('TabController', function($rootScope,$q, $scope, $http, CurrentUser, Parameter,$timeout, $window, $filter, $state, TabGrFactory, ngDialog, Idle) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $rootScope.ChatProxy
        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd-MM-yyyy',
            // disableWeekend: 1
        };
        $scope.asb = {currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza"}
        //$scope.asb.currentDate.setHours($scope.asb.currentDate.getHours()+1);
        console.log("$scope.asb.currentDate", $scope.asb.currentDate);
        console.log("$scope.dateOptions", $scope.dateOptions);
        $scope.asb.startDate = new Date();
        $scope.asb.endDate = new Date();
        $scope.asb.endDate.setDate($scope.asb.endDate.getDate()+7);
        var allBoardHeight = 500;

        $scope.$watch('asb.showMode', function(newValue, oldValue) {
            if (newValue !== oldValue) {
                console.log('Changed! '+newValue);
                $scope.asb.reloadBoard();
            }
        });

    $scope.user = CurrentUser.user();
    // console.log("CurrentUser.user()", CurrentUser.user());

    var vmsgname = 'joblist';
    var voutletid = $scope.user.OrgId;
    var vgroupname = voutletid+'.'+vmsgname;
    var vappname = vmsgname;
    // try {
    //     $rootScope.ChatProxy.invoke('joinRoom', vgroupname);
    // } catch (exc){
    //     $scope.showMsg = {message: 'Tidak dapat terhubung dengan SignalR server.', title: 'Koneksi SignalR gagal.', closeText: 'Close', exception: exc}
    //     ngDialog.openConfirm ({
    //         width: 700,
    //         template: 'app/boards/factories/showMessage.html',
    //         plain: false,
    //         scope: $scope,
    //     })
        
    // }

    // var refreshReady = false;
    // var refreshPeriodeMs = 10000;
    // var refreshLongPeriodeMs = 10000;
    // var currentTime;
    // var nextRefreshTime = new Date();
    // nextRefreshTime.setTime(nextRefreshTime.getTime()+refreshPeriodeMs);
    // var timerRefresh = function(){
    //     if (refreshReady){
    //         currentTime = new Date();
    //         if (nextRefreshTime.getTime()<currentTime.getTime()){
    //             console.log(currentTime.getTime(), " do Refresh");
    //             $rootScope.tabDoRefresh();
    //             nextRefreshTime.setTime(nextRefreshTime.getTime()+refreshPeriodeMs);
    //         }
    //         delete currentTime;
    //     }
    // }

    // var secondTick = function(){
    //     $timeout( function(){
    //         timerRefresh();
    //         secondTick();
    //     }, 500);        
    // }
    // secondTick();

    $rootScope.tabDoRefresh = function(){
            var vobj = {
                mode: $scope.asb.showMode,
                currentDate: $scope.asb.currentDate,
                // nopol: $scope.asb.nopol,
                // tipe: $scope.asb.tipe
            }
            showBoardTab(vobj);            
        
    }
    // $scope.$on('SR-'+vappname, function(e, data){
    //     console.log("notif-data", data);
    //     var vparams = data.Message.split('.');

    //     var vJobId = 0;
    //     if (typeof vparams[0] !== 'undefined'){
    //         var vJobId = vparams[0];
    //     }

    //     var vStatus = 0;
    //     if (typeof vparams[1] !== 'undefined'){
    //         var vStatus = vparams[1];
    //     }


    //     var jobid = vparams[0];
    //     var vcounter = vparams[1];
    //     var vJobList = ["0", "1", "2", "3", "4", "18", "20"];
    //     var xStatus = vStatus+'';

    //     if (vJobList.indexOf(xStatus)>=0){
    //         var vobj = {
    //             mode: $scope.asb.showMode,
    //             currentDate: $scope.asb.currentDate,
    //             // nopol: $scope.asb.nopol,
    //             // tipe: $scope.asb.tipe
    //         }
    //         showBoardTab(vobj);            
    //     }
        
    // });
    

        var timeActive = true;
        var minuteTick = function(){
            if (!timeActive){
                console.log('time Active');
                return;
            }
            if (TabGrFactory.chipLoaded){
                TabGrFactory.updateTimeLine();
                $timeout( function(){
                    minuteTick();
                }, 60000);
            } else {
                $timeout( function(){
                    minuteTick();
                }, 500);
            }
        }

        // var minuteTick = function(){
        //     if (!timeActive){
        //         console.log('time Active');
        //         return;
        //     }
        //     TabGrFactory.updateTimeLine();
        //     $timeout( function(){
        //         minuteTick();
        //     }, 60000);
        // }
        $scope.getData = function(){
            return $q.resolve()
        }
        $scope.$on('$viewContentLoaded', function() {
            //$scope.asb.reloadBoard = function(){
                $timeout( function(){
                    var vobj = {
                        mode: $scope.asb.showMode,
                        currentDate: $scope.asb.currentDate,
                        // nopol: $scope.asb.nopol,
                        // tipe: $scope.asb.tipe
                    }
                    showBoardTab(vobj);

                    // $timeout( function(){
                    //     minuteTick();
                    // }, 100);
                    minuteTick();
                    
                }, 100);
            //}
            Idle.unwatch();
            console.log('==== value of idle ==', Idle)
        });

        $scope.$on('$destroy', function() {
            timeActive = false;
            Idle.watch()
        });


        // $scope.asb.FullScreen = function(){
        //     // alert('full screen');
        //     var vobj = document.getElementById('asb_estimasi_view');
        //     vobj.style.cssText = 'position: absolute; left: 0; top: 0; width: 100%; height: 100%;';
        // }
        

        angular.element($window).bind('resize', function () {
            //AsbEstimasiFactory.resizeBoard();
            // AsbProduksiFactory.resizeBoard();
            
            // jpcbStopage.autoResizeStage();
            // jpcbStopage.redraw();
        });

        var showBoardTab = function(vitem){
            TabGrFactory.showBoard({container: 'tab-board-gr', currentChip: vitem});

        }

        
    })