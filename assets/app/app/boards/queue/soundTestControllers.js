angular.module('app')
.controller('SoundTestController', function($scope, $http, CurrentUser, Parameter, ngDialog, $window, $timeout, $filter) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {

        $scope.loading=true;
        $scope.gridData=[];

        $scope.soundTest = {}
        var strAngka = '1234567890';
        $scope.soundTest.angkaList = strAngka.split('');
        var strAbjad = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $scope.soundTest.abjadList = strAbjad.split('');

        $scope.soundTest.counterList = [
            {value: '1', caption: 'Konter 1'},
            {value: '2', caption: 'Konter 2'},
            {value: '3', caption: 'Konter 3'},
            {value: '4', caption: 'Konter 4'},
            {value: '5', caption: 'Konter 5'},
            {value: '6', caption: 'Konter 6'},
            {value: '7', caption: 'Konter 7'},
            {value: '8', caption: 'Konter 8'},
        ]

        $scope.soundTest.nopol = 'D 1234 ABC';
        $scope.soundTest.konter = '1';

        $scope.soundTest.playSprite = function(vsprite){
            JsSound.play(vsprite);
        }

        $scope.soundTest.panggil = function(){
            if ($scope.soundTest.nopol){
                var vstr = '@'+$scope.soundTest.nopol+'#'+$scope.soundTest.konter;
                JsSound.playString(vstr);
            }
        }

        $scope.soundTest.playString = function(vstr){
            
            JsSound.playString(vstr);
        }

    });

    
});
