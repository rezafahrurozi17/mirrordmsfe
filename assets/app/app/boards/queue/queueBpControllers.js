angular.module('app')
.controller('QueueBpController', function($rootScope, $scope, $http, CurrentUser, Parameter, ngDialog, $window, $timeout, $filter, Idle) {

    $scope.$on('$viewContentLoaded', function() {
        queueBoard = new JsBoard.Container(boardConfig);
        queueBoard.createChips(theChips, 'objarray');
        queueBoard.redraw();
        Idle.unwatch();
        console.log('==== value of idle ==', Idle)
      });
      $scope.$on('$destroy', function(){
        Idle.watch();
      });

    var queueBoard;
    var vDate = new Date();
    var vjam = $filter('date')(vDate, 'hh:mm');
    var vtgl = $filter('date')(vDate, 'EEE, dd/MM/yyyy');
    var be = '';
    var runningChip = false;
    var soundidx = 0;

    theChips = {
        general: [
            {chip: 'header', lokasi: 'header', title: 'SELAMAT DATANG DI BENGKEL TOYOTA', test: 'Percobaan', atas: 20, jam: vjam, tanggal: vtgl}, //<--- definisikan chip dengan nama header
            {chip: 'subheader', lokasi: 'subheader', text: ''},
            {chip: 'runningtext', lokasi: 'runningtext', text: 'Selamat datang di bengkel resmi Toyota. Silahkan melakukan pendaftaran di bagian registrasi.', counter: 0, deltaJarak: 20},
            {chip: 'counterheader', lokasi: 'counterheader', title: 'COUNTER'},
            {chip: 'counterbg', lokasi: 'counterbg'},
            //{chip: 'lastcall2', lokasi: 'lastcall2'},
            {chip: 'lastcall', lokasi: 'lastcall', text: 'LAST CALL' && be, counter2: 0, deltaJarak2: 20}
        ],
        rows: [ // Jumlah baris
            {chip: 'row', lokasi: 'row', text: ''},
            {chip: 'row', lokasi: 'row', text: ''},
            {chip: 'row', lokasi: 'row', text: ''},
            {chip: 'row', lokasi: 'row', text: ''},
            {chip: 'row', lokasi: 'row', text: ''},
            {chip: 'row', lokasi: 'row', text: ''},
            {chip: 'row', lokasi: 'row', text: ''},
            {chip: 'row', lokasi: 'row', text: ''},
        ],
        column: [ // kolom
            {chip: 'columnbp', lokasi: 'columntitle', title: 'PENERIMAAN', width: 30},
            {chip: 'columnbp2', lokasi: 'column2', title: 'Booking', width: 10, name: 'penerimaan_booking', titleHeight: 24},
            {chip: 'columnbp2', lokasi: 'column2', title: 'Walk In', width: 10, name: 'penerimaan_walkin', titleHeight: 24},
            {chip: 'columnbp2', lokasi: 'column2', title: 'Other', width: 10, name: 'penerimaan_other', titleHeight: 24},
            {chip: 'columnbp', lokasi: 'column', title: 'PENGAMBILAN', width: 10, name: 'penyerahan'},
        ],
        counter: [
            // {chip: 'chip', lokasi: 'counter', counterName: 'Counter 1', nopol: 'B 1234 ACD', person: 'SA Andi'},
            // {chip: 'chip', lokasi: 'counter', counterName: 'Counter 2', nopol: 'B 3456 ADB', person: 'SA Budi'},
        ],
        nopol: [
            // {chip:'nopol', lokasi: 'nopol', nopol: 'D 1245 DSA', col: 1, row: 1},
            // {chip:'nopol', lokasi: 'nopol', nopol: 'D 1246 DSB', col: 1, row: 2},
        ]
    };

    var boardConfig = {
        type: 'queue',
        board:{
            container: 'customer-queue-bp',
            chipGroup: 'queue', //<---- group dari chip
            iconFolder: 'images/boards/status',
            subHeaderHeight: 32,
            rowHeight: 25,
            headerHeight: 50,
            onChipCreated: function(vchip){
                if (vchip.data.chip == 'header'){
                    headerChip = vchip;
                }
                if (vchip.data.chip == 'runningtext'){
                    runningChip = vchip;
                }
                if (vchip.data.chip == 'lastcall'){
                    runningChip2 = vchip;
                }
            }
        },
        stage: {
            height: 470,
            width: 960,
        },
        chip: {
            showPageIndex: 0,
            numberWidth: 24,
        }
    };

    var velement = document.getElementById('base-customer-queue-bp');

    var erd = elementResizeDetectorMaker();
    erd.listenTo(velement, function(element) {
        if(queueBoard){
            queueBoard.autoResizeStage();
        }
        var width = element.offsetWidth;
        var height = element.offsetHeight;
    });

    var updateTimeBoard = function() {
        dateNow = new Date();
        vjam = $filter('date')(dateNow, 'HH:mm:ss');
        vtgl = $filter('date')(dateNow, 'EEE, dd/MM/yyyy');
        theChips.general[0].jam = vjam;
        theChips.general[0].tanggal = vtgl;
        queueBoard.redrawChips();
        $timeout(updateTimeBoard, 1000);
    };

    var updateRunningText = function(){
        var vchipdata = theChips.general[2];
        vjarak = vchipdata.deltaJarak;
        vchipdata.counter = vchipdata.counter+1;

        var xchip = runningChip.shapes.text;
        var vwidth = xchip.width();

        var vleft = vwidth-(vchipdata.counter*vjarak)+vjarak;
        if (vleft<-vwidth){
            vleft = vwidth+vjarak;
            vchipdata.counter = 0;
        }

        vchipdata.left = vleft;
        xchip.x(vleft);
        queueBoard.redrawChips();
        $timeout(updateRunningText, 500);
    };

    var queueMaxRow = 8;
    var LoadCounter = 0;

    // var showData = 8;
    // var MulaiData = 0;
    // var BagiBoard = 0;
    // var CountBoard = 0;
    // var Modulus = 0;
    // var dataHabis = true;

    var showData = 8;

    var MulaiDataBooking = 0;
    var BagiBoardBooking = 0;
    var CountBoardBooking = 0;
    var ModulusBooking = 0;
    var dataHabisBooking = true;

    var MulaiDataWalkIn = 0;
    var BagiBoardWalkIn = 0;
    var CountBoardWalkIn = 0;
    var ModulusWalkIn = 0;
    var dataHabisWalkIn = true;

    var MulaiDataOthers = 0;
    var BagiBoardOthers = 0;
    var CountBoardOthers = 0;
    var ModulusOthers = 0;
    var dataHabisOthers = true;

    var MulaiDataBPTake = 0;
    var BagiBoardBPTake = 0;
    var CountBoardBPTake = 0;
    var ModulusBPTake = 0;
    var dataHabisBPTake = true;

    var getBoardBPData = function() {
      var vurl = '/api/as/queue/boarddata/0';
      $http.get(vurl).then(function(res) {
          var xres = res.data;
          console.log('ah ssh', xres);
          // var boardresult = [];
          var boardresultFinal = [];
          var counterlist = [];
          var queuecounter = [];
          var countno = 1;

          // if (dataHabis == true)
          // {
          //     CountBoard = xres.board.length;
          //     Modulus = parseInt(CountBoard % showData);
          //     BagiBoard = parseInt(CountBoard / showData);
          //     if (Modulus != 0)
          //     {
          //         BagiBoard = BagiBoard + 1;
          //     }
          // }
          // console.log("Count Board", CountBoard, Modulus, BagiBoard, MulaiData);

          // for (var i= MulaiData;  i < xres.board.length; i++) {
          //     var vboard = xres.board[i];
          //     var vcat = vboard.QCategoryId;
          //     var vcatname = vboard.QCategoryName;
          //     if ((typeof vcat == 'undefined') || (!vcat)) {
          //         vcat = '';
          //     }

          //     if (vboard.isReception == 0) {
          //         boardresult.push({
          //             code: vcat,
          //             name: vcatname,
          //             nopol: vboard.LicensePlate,
          //             Id: vboard.Id,
          //             col: 0,
          //             row: 0
          //         });
          //         countno++;
          //     }
          // }

          // BagiBoard = BagiBoard - 1;
          // console.log("BagiBoard", BagiBoard);

          // if (BagiBoard != 0)
          // {
          //     dataHabis = false;
          //     MulaiData = MulaiData + showData;
          // }
          // else
          // {
          //     MulaiData = 0;
          //     BagiBoard = 0;
          //     dataHabis = true;
          // }
          // console.log("MulaiData", MulaiData);

          //=======try to loop each col========
          var arrBooking = [];
          var arrWalkIn = [];
          var arrOthers = [];
          var arrBPTake = [];
          for (var i=0; i < xres.board.length; i++) {
            switch (xres.board[i].QCategoryName.toUpperCase())
            {
                case "BOOKING":
                case "BOOKING ON TIME":
                    arrBooking.push(xres.board[i]);
                    break;
                case "WALK IN":
                    arrWalkIn.push(xres.board[i]);
                    break;
                case "OTHERS":
                    arrOthers.push(xres.board[i]);
                    break;
                case "WI BP TAKE":
                    if (xres.board[i].isSatellite == 1){
                        if (xres.board[i].IsTakeBP == 1) {
                            arrBPTake.push(xres.board[i]);
                        } else if (xres.board[i].IsTakeBP == 0) {
                            arrOthers.push(xres.board[i]);
                        }
                        // arrOthers.push(xres.board[i]);
                    } else {
                        arrBPTake.push(xres.board[i]);
                    }
                    break;
            }
          }

          // loop on BOOK=======================================
          if (arrBooking.length != 0) {
            // var showData = 8;
            // var MulaiData = 0;
            // var BagiBoard = 0;
            // var CountBoard = 0;
            // var Modulus = 0;
            // var dataHabis = true;

            var boardresult = [];

            if (dataHabisBooking == true)
            {
                CountBoardBooking = arrBooking.length;
                ModulusBooking = parseInt(CountBoardBooking % showData);
                BagiBoardBooking = parseInt(CountBoardBooking / showData);
                if (ModulusBooking != 0)
                {
                    BagiBoardBooking = BagiBoardBooking + 1;
                }
            }

            for(var i= MulaiDataBooking;  i< arrBooking.length; i++){
                var vboard = arrBooking[i];
                var vcat = vboard.QCategoryId;
                var vcatname = vboard.QCategoryName;
                if ((typeof vcat == 'undefined') || (!vcat)){
                    vcat = '';
                }

                // comment dulu karna sudah dihandle di backend
                // if (vboard.isReception == 0) {
                //     boardresult.push({
                //         code: vcat,
                //         name: vcatname,
                //         nopol: vboard.LicensePlate,
                //         Id: vboard.Id,
                //         col: 0,
                //         row: 0
                //     });
                // }
                
                var nu = '';
                if ((i+1) < 10){
                    nu = '0'+(i+1)+'.'
                } else {
                    nu = (i+1)+'.'
                }

                boardresult.push({
                    code: vcat,
                    name: vcatname,
                    nopol: vboard.LicensePlate,
                    Id: vboard.Id,
                    col: 0,
                    row: 0,
                    nourut: nu,
                });
            }

            BagiBoardBooking = BagiBoardBooking - 1;
            console.log('asem asin Book', arrBooking);
            console.log("BagiBoardBooking", BagiBoardBooking);

            if (BagiBoardBooking != 0)
            {
                dataHabisBooking = false;
                MulaiDataBooking = MulaiDataBooking + showData;
            }
            else
            {
                MulaiDataBooking = 0;
                BagiBoardBooking = 0;
                dataHabisBooking = true;
            }
            console.log("MulaiDataBooking", MulaiDataBooking);

            for(var i=0; i<boardresult.length; i++){
                boardresultFinal.push(boardresult[i]);
            }
            // updateQueueList(boardresult);

          }

          // loop on Walk IN =======================================
          if (arrWalkIn.length != 0) {
            // var showData = 8;
            // var MulaiData = 0;
            // var BagiBoard = 0;
            // var CountBoard = 0;
            // var Modulus = 0;
            // var dataHabis = true;

            var boardresult = [];

            if (dataHabisWalkIn == true)
            {
                CountBoardWalkIn = arrWalkIn.length;
                ModulusWalkIn = parseInt(CountBoardWalkIn % showData);
                BagiBoardWalkIn = parseInt(CountBoardWalkIn / showData);
                if (ModulusWalkIn != 0)
                {
                    BagiBoardWalkIn = BagiBoardWalkIn + 1;
                }
            }

            for(var i= MulaiDataWalkIn;  i< arrWalkIn.length; i++){
                var vboard = arrWalkIn[i];
                var vcat = vboard.QCategoryId;
                var vcatname = vboard.QCategoryName;
                if ((typeof vcat == 'undefined') || (!vcat)){
                    vcat = '';
                }

                // comment dulu karna sudah dihandle di backend
                // if (vboard.isReception == 0) {
                //     boardresult.push({
                //         code: vcat,
                //         name: vcatname,
                //         nopol: vboard.LicensePlate,
                //         Id: vboard.Id,
                //         col: 0,
                //         row: 0
                //     });
                // }

                var nu = '';
                if ((i+1) < 10){
                    nu = '0'+(i+1)+'.'
                } else {
                    nu = (i+1)+'.'
                }
                boardresult.push({
                    code: vcat,
                    name: vcatname,
                    nopol: vboard.LicensePlate,
                    Id: vboard.Id,
                    col: 0,
                    row: 0,
                    nourut: nu,
                });
            }

            BagiBoardWalkIn = BagiBoardWalkIn - 1;
            console.log('asem asin WALK IN', arrWalkIn);
            console.log("BagiBoardWalkIn", BagiBoardWalkIn);

            if (BagiBoardWalkIn != 0)
            {
                dataHabisWalkIn = false;
                MulaiDataWalkIn = MulaiDataWalkIn + showData;
            }
            else
            {
                MulaiDataWalkIn = 0;
                BagiBoardWalkIn = 0;
                dataHabisWalkIn = true;
            }
            console.log("MulaiDataWalkIn", MulaiDataWalkIn);

            for(var i=0; i<boardresult.length; i++){
                boardresultFinal.push(boardresult[i]);
            }
            // updateQueueList(boardresult);
        }


        // loop on Others =======================================
        if (arrOthers.length != 0) {
            // var showData = 8;
            // var MulaiData = 0;
            // var BagiBoard = 0;
            // var CountBoard = 0;
            // var Modulus = 0;
            // var dataHabis = true;

            var boardresult = [];

            if (dataHabisOthers == true)
            {
                CountBoardOthers = arrOthers.length;
                ModulusOthers = parseInt(CountBoardOthers % showData);
                BagiBoardOthers = parseInt(CountBoardOthers / showData);
                if (ModulusOthers != 0)
                {
                    BagiBoardOthers = BagiBoardOthers + 1;
                }
            }

            for(var i= MulaiDataOthers;  i< arrOthers.length; i++){
                var vboard = arrOthers[i];
                var vcat = vboard.QCategoryId;
                var vcatname = vboard.QCategoryName;
                if ((typeof vcat == 'undefined') || (!vcat)){
                    vcat = '';
                }

                // comment dulu karna sudah dihandle di backend
                // if (vboard.isReception == 0) {
                //     boardresult.push({
                //         code: vcat,
                //         name: vcatname,
                //         nopol: vboard.LicensePlate,
                //         Id: vboard.Id,
                //         col: 0,
                //         row: 0
                //     });
                // }

                var nu = '';
                if ((i+1) < 10){
                    nu = '0'+(i+1)+'.'
                } else {
                    nu = (i+1)+'.'
                }
                boardresult.push({
                    code: vcat,
                    name: vcatname,
                    nopol: vboard.LicensePlate,
                    Id: vboard.Id,
                    col: 0,
                    row: 0,
                    isSatellite: vboard.isSatellite,
                    // nourut: i+1,
                    nourut: nu,
                });
            }

            BagiBoardOthers = BagiBoardOthers - 1;
            console.log('asem asin OTHERS', arrOthers);
            console.log("BagiBoardOthers", BagiBoardOthers);

            if (BagiBoardOthers != 0)
            {
                dataHabisOthers = false;
                MulaiDataOthers = MulaiDataOthers + showData;
            }
            else
            {
                MulaiDataOthers = 0;
                BagiBoardOthers = 0;
                dataHabisOthers = true;
            }
            console.log("MulaiDataOthers", MulaiDataOthers);

            for(var i=0; i<boardresult.length; i++){
                boardresultFinal.push(boardresult[i]);
            }
            // updateQueueList(boardresult);

          }

          // loop on WI BP Take =======================================
        if (arrBPTake.length != 0) {
          // var showData = 8;
          // var MulaiData = 0;
          // var BagiBoard = 0;
          // var CountBoard = 0;
          // var Modulus = 0;
          // var dataHabis = true;

          var boardresult = [];

          if (dataHabisBPTake == true)
          {
              CountBoardBPTake = arrBPTake.length;
              ModulusBPTake = parseInt(CountBoardBPTake % showData);
              BagiBoardBPTake = parseInt(CountBoardBPTake / showData);
              if (ModulusBPTake != 0)
              {
                  BagiBoardBPTake = BagiBoardBPTake + 1;
              }
          }

          for(var i= MulaiDataBPTake;  i< arrBPTake.length; i++){
              var vboard = arrBPTake[i];
              var vcat = vboard.QCategoryId;
              var vcatname = vboard.QCategoryName;
              if ((typeof vcat == 'undefined') || (!vcat)){
                  vcat = '';
              }

            // comment dulu karna sudah dihandle di backend
            //   if (vboard.isReception == 0) {
            //       boardresult.push({
            //           code: vcat,
            //           name: vcatname,
            //           nopol: vboard.LicensePlate,
            //           Id: vboard.Id,
            //           col: 0,
            //           row: 0
            //       });
            //   }

            var nu = '';
            if ((i+1) < 10){
                nu = '0'+(i+1)+'.'
            } else {
                nu = (i+1)+'.'
            }
            boardresult.push({
                code: vcat,
                name: vcatname,
                nopol: vboard.LicensePlate,
                Id: vboard.Id,
                col: 0,
                row: 0,
                nourut: nu,
            });
          }

          BagiBoardBPTake = BagiBoardBPTake - 1;
          console.log('asem asin BP Take', arrBPTake);
          console.log("BagiBoardBPTake", BagiBoardBPTake);

          if (BagiBoardBPTake != 0)
          {
              dataHabisBPTake = false;
              MulaiDataBPTake = MulaiDataBPTake + showData;
          }
          else
          {
              MulaiDataBPTake = 0;
              BagiBoardBPTake = 0;
              dataHabisBPTake = true;
          }
          console.log("MulaiDataBPTake", MulaiDataBPTake);

          for(var i=0; i<boardresult.length; i++){
              boardresultFinal.push(boardresult[i]);
          }
          // updateQueueList(boardresult);

        }

        //=======end try to loop each col========

        for (var i=0;  i<xres.listCounter.length; i++) {
            var vcounter = xres.listCounter[i];
            console.log("xres.listCounter[i]",xres.listCounter[i]);
            console.log("Percobaan2",vcounter);
            counterlist.push({
                chip: 'chip', lokasi: 'counter', nopol: '',
                person: '',
                counterId: vcounter.CounterId,
                counterCode: vcounter.CounterCode,
                counterName: vcounter.CounterName,
                isCounterCall: true
            });
        }

        for (var i=0;  i<xres.queueCounter.length; i++) {
            var vcounter = xres.queueCounter[i];
            console.log("xres.queueCounter[i]",xres.queueCounter[i]);
            for (var j=0; j<counterlist.length; j++){
                if (counterlist[j].counterId == vcounter.CounterId){
                    console.log("xres.queueCounter[i]",counterlist[j]);
                    counterlist[j].id = vcounter.Id;
                    counterlist[j].nopol = vcounter.LicensePlate;
                    counterlist[j].person = vcounter.SAName;
                    counterlist[j].isCounterCall = vcounter.isCounterCall;
                }
            }
        }

        //updateQueueList(boardresult);
        updateQueueList(boardresultFinal);
        updateCounterList(counterlist);
        queueBoard.createChips(theChips, 'objarray');
        queueBoard.redraw();
        $timeout(getBoardBPData, 3000);
      }).catch(function (data) {
          console.log('$http.get [error]: ', data);
          $timeout(getBoardBPData, 3000);
      });
    };


    var updateQueueList = function(vdata){
        var vresult = [];
        var itemList = {};
        var rowMapping = {};

        for (var i=0; i < vdata.length; i++) {
            var vitem = vdata[i];
            if (typeof vitem.code !== 'undefined') {

                switch (vitem.name.toUpperCase())
                {
                    case "BOOKING":
                    case "BOOKING ON TIME":
                        vitem.col = 0;
                        break;
                    case "WALK IN":
                        vitem.col = 1;
                        break;
                    case "OTHERS":
                        vitem.col = 2;
                        break;
                    case "WI BP TAKE":
                        vitem.col = 3;
                        if (vitem.isSatellite == 1){
                            vitem.col = 2;
                            vitem.isSatellite = 1;
                        }
                        break;
                }

                if (typeof rowMapping[vitem.col] == 'undefined'){
                    rowMapping[vitem.col] = 0;
                }
                vitem.row = rowMapping[vitem.col];
                rowMapping[vitem.col] = rowMapping[vitem.col] + 1;
                vresult.push({
                    chip:'nopol', lokasi: 'nopol',
                    nopol: vitem.nopol, col: vitem.col, row: vitem.row,
                    //nourut: (vitem.row+1) + '.',
                    code: vitem.code,
                    isSatellite: vitem.isSatellite,
                    nourut: vitem.nourut,
                });
            }
        }

        var xresult = [];
        for (var i=0; i<vresult.length; i++) {
            var vitem = vresult[i];
            var vcount = rowMapping[vitem.col];
            var vMaxPage = Math.ceil(vcount/queueMaxRow);
            var vPage = Math.floor(vitem.row/queueMaxRow);
            var vRow = vitem.row % queueMaxRow;
            vitem.row = vRow+1;
            vitem.maxPage = vMaxPage;
            vitem.visible = true;
            if (vMaxPage > 1) {
                var xpage = LoadCounter%vMaxPage;
                if (vPage !== xpage){
                    vitem.visible = false;
                }
            }
            if (vitem.visible) {
                xresult.push(vitem);
            }
        }

        theChips.nopol = xresult;
    };

    var updateCounterList = function(vdata){
        theChips.counter = vdata;
    };

    var playSound = function() {
        var isEmptyNopol = false;
        var playidx = 12000;
        theChips.general[5].text = '';
        if (theChips.counter.length > 0) {
            var vcounter = theChips.counter[soundidx];
            if (vcounter.nopol.length > 0) {
                var counterName = vcounter.counterName.replace('Counter ', '');
                var vstr = '@' + vcounter.nopol + '#' + (counterName);
                theChips.general[5].text = 'Pemilik Kendaraan ' + vcounter.nopol + ' Harap Datang ke Counter ' + counterName;
                
                // Line code ini hanya berlaku jika di panggil ulang
                if (vcounter.numberOfCalls == -1 && vcounter.isCounterCall != false) {                    
                    // Call JSSound
                    $http.post('/api/as/queue/countercall/' +  vcounter.id).then(function(res){
                        JsSound.playString(vstr);
                    }).catch(function (data) {
                        console.log('$http.post countercall[error]: ', data);
                    });
                }

                if (vcounter.numberOfCalls != -1 && vcounter.isCounterCall != false) {
                     // Call JSSound
                    $http.post('/api/as/queue/countercall/' +  vcounter.id).then(function(res){
                        JsSound.playString(vstr);
                    }).catch(function (data) {
                        console.log('$http.post countercall[error]: ', data);
                    });
                }
            }
            else
            {
                isEmptyNopol = true;
                playidx = 100;
            }
        }

        soundidx += 1;
        if (soundidx >= theChips.counter.length) {
            soundidx = 0;
        }

        $timeout(playSound, playidx);
    };

    $timeout(playSound, 1000);
    $timeout(getBoardBPData, 3000);
    $timeout(updateTimeBoard, 1000);
    $timeout(updateRunningText, 500);

    if (typeof JsBoard.Common.statusResize === 'undefined') {
        JsBoard.Common.statusResize = true;
    }
});
