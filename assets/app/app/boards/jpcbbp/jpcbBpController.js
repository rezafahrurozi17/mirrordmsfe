angular.module('app')
    .controller('JpcbBpController', function($scope, $http, CurrentUser, Parameter, JpcbBpFactory, ngDialog, $window, $timeout, $filter,
        WO, bsNotify, WoHistoryGR, oplList, FollowUpGrService, AppointmentGrService, AsbGrFactory, RepairSupportActivityGR, RepairProcessGR,
        CMaster, $q, WOBP, Role, bsAlert, GeneralMaster, CAppPrintServiceHistory, PrintRpt, Idle) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        //=====hasil copyan di bawah ini======
        $scope.user = CurrentUser.user();
        $scope.mData = {};
        $scope.mData.US = [];
        $scope.mData.Signature = null;
        var tmpPrediagnose = {}; //Model
        $scope.mDataDetail = [];
        $scope.gridOriginal = [];
        $scope.gridDeleted = [];
        $scope.mDataCustomer = [];
        $scope.tmpAdditionalTaskId = null;
        $scope.IsAppointment = 0;
        $scope.dEstimate = false;
        $scope.mData.OutsideCleaness = 0;
        $scope.mData.CompletenessVehicle = 0;
        $scope.mData.InsideCleaness = 0;
        $scope.mData.UsedPart = 0;
        $scope.mData.DriverCarpetPosition = 0;
        $scope.diskonData = [];
        $scope.tmpDataOPL = [];
        $scope.tmpDataOPLFinal = [];
        $scope.isTAM = 1;

        $scope.GroupListTemp = [];
        // $scope.diskonData = [{
        //     MasterId: -1,
        //     Name: "Tidak Ada Diskon",
        // }, {
        //     MasterId: 0,
        //     Name: "Custom Diskon"
        // }]
        $scope.diskonData = [{
            MasterId: -1,
            Name: "Tidak Ada Diskon",
        }, {
            MasterId: 0,
            Name: "Custom Diskon"
        }]
        $scope.isCustomTask = false;
        $scope.isCustomPart = false;
        // $scope.mDataCrm = null;
        $scope.filter = {};
        $scope.xRole = {
            selected: []
        };
        $scope.JobIdforCloseWO = null;
        $scope.formApi = {};
        $scope.IsWash = {};
        $scope.isWaiting = {};
        $scope.UsedPart = {};
        $scope.PermissionPartChange = {};
        $scope.mDataDM = [];
        $scope.mDataCrm = {};
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        $scope.FlatRateAvailable = false;
        $scope.PriceAvailable = false;
        $scope.isCustomDiskon = false;
        $scope.JobRequest = [];
        $scope.formApi = {};
        $scope.JobComplaint = [];
        var currentDate = new Date();
        $scope.statusWO = true;
        $scope.minDateASB = new Date(currentDate);
        // $scope.customActionButtonSettingsDetail = [{

        //         actionType: 'back', //Use 'Back Action' of bsForm
        //         title: 'Kembali',
        //         //icon: 'fa fa-fw fa-edit',
        //     }
        //     ,
        //     {

        //         actionType: 'back', //Use 'Back Action' of bsForm
        //         title: 'Balik Bos',
        //         //icon: 'fa fa-fw fa-edit',
        //     }

        // ];
        // added by sss on 2018-02-22
        $scope.actButtonCaption = {
                cancel: 'Kembali'
            }
            //


            $scope.startlongDay = '';
            $scope.endlongDay = '';


        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpAppointmentDate.getDate().toString();
            finalDate = (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        }
        $scope.filterCounter = false;
        var gridData = [];
        // $scope.filter = [];
        $scope.Param = null;
        $scope.paramDiskon = null;
        $scope.CatWO = null;
        $scope.dataParam = [{
            Id: 3,
            Name: 'No. Polisi',
            width: 50
        }, {
            Id: 2,
            Name: 'No. WO',
            width: 150
        }, {
            Id: 1,
            Name: 'Tanggal',
            width: 50
        }];
        $scope.dataDiskon = [{
            Id: 1,
            Name: 'Data Tasks',
            width: 50
        }, {
            Id: 2,
            Name: 'Data Parts',
            width: 50
        }];
        var gridTemp = [];
        $scope.listApi = {};
        $scope.listApiJob = {};
        $scope.listApiOpl = {};
        $scope.listApiAddress = {};
        $scope.listRequestSelectedRows = [];
        $scope.listComplaintSelectedRows = [];
        $scope.listJoblistSelectedRows = [];
        $scope.modalMode = 'new';
        $scope.totalWork = 0;
        $scope.totalWorkDiscounted = 0;
        $scope.totalMaterialDiscounted = 0;
        $scope.totalDp = 0;
        $scope.totalDpDefault = 0;
        $scope.totalMaterial = 0;
        $scope.sumWorkOpl = 0;
        $scope.sumWorkOplDiscounted = 0;
        $scope.totalAll = 0;
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModelOpl = {};
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.isReject = false;
        $scope.isRejectedWarranty = false;
        $scope.asb = {
            currentDate: new Date(),
            showMode: 'main',
            nopol: "D 1234 ABC",
            tipe: "Avanza"
        }
        $scope.asb.startDate = new Date();
        $scope.asb.endDate = new Date();
        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
        var Parts = [];
        $scope.ComplaintCategory = [{
                ComplaintCatg: 'Body'
            },
            {
                ComplaintCatg: 'Body Electrical'
            },
            {
                ComplaintCatg: 'Brake'
            },
            {
                ComplaintCatg: 'Drive Train'
            },
            {
                ComplaintCatg: 'Engine'
            },
            {
                ComplaintCatg: 'Heater System & AC'
            },
            {
                ComplaintCatg: 'Restraint'
            },
            {
                ComplaintCatg: 'Steering'
            },
            {
                ComplaintCatg: 'Suspension and Axle'
            },
            {
                ComplaintCatg: 'Transmission'
            },
        ];
        $scope.buttonSettingModal = {
            save: {
                text: 'Service Explanation'
            }
        };
        // $scope.conditionData = [{ Id: 1, Name: "Ok" }, { Id: 2, Name: "Not Ok" }];
        $scope.conditionData = [{
            Id: 0,
            Name: "Not Ok"
        }, {
            Id: 1,
            Name: "Ok"
        }];
        $scope.typeMData = [{
            Id: 1,
            Name: 1
        }, {
            Id: 2,
            Name: 2
        }, {
            Id: 3,
            Name: 3
        }];
        $scope.JobRequest = [];
        $scope.oplData = [];
        $scope.JobComplaint = [];
        $scope.listView = {
            new: {
                enable: false
            },
            view: {
                enable: false
            },
            delete: {
                enable: false
            },
            select: {
                enable: false
            },
            selectall: {
                enable: false
            },
            edit: {
                enable: false
            },
        }
        $scope.listBilling = {
            new: {
                enable: false
            },
            view: {
                enable: true
            },
            delete: {
                enable: false
            },
            select: {
                enable: false
            },
            selectall: {
                enable: false
            },
            edit: {
                enable: true
            },
        }
        $scope.listControl = {
            new: {
                enable: true
            },
            view: {
                enable: true
            },
            delete: {
                enable: true
            },
            select: {
                enable: true
            },
            selectall: {
                enable: true
            },
            edit: {
                enable: true
            },
        }
        $scope.listCustomer = {
            new: {
                enable: false
            },
            view: {
                enable: false
            },
            delete: {
                enable: false
            },
            select: {
                enable: false
            },
            selectall: {
                enable: false
            },
            edit: {
                enable: false
            },
        }
        $scope.buttonSummaryData = {
            new: {
                enable: false
            },
            view: {
                enable: false
            },
            delete: {
                enable: false
            },
            select: {
                enable: false
            },
            selectall: {
                enable: false
            },
            edit: {
                enable: false
            },
        };
        $scope.paymentTypeData = [{
            Id: 1,
            Name: 'Tunai'
        }, {
            Id: 2,
            Name: 'Kredit'
        }, {
            Id: 3,
            Name: 'PKS'
        }];

        var dateFilter2 = 'date:"dd/MM/yyyy"';
        var approvalForDP = [{
                name: 'id',
                field: 'JobId',
                width: '7%',
                visible: false
            },
            {
                name: 'Vehicid',
                field: 'VehicleId',
                width: '8%',
                visible: false
            },
            {
                name: 'Tanggal',
                field: 'JobDate',
                width: '10%',
                cellFilter: dateFilter2
            },
            {
                name: 'No Wo',
                field: 'WoNo',
                width: '20%',
                displayName: 'No. WO'
            },
            {
                name: 'No Polisi',
                field: 'LicensePlate',
                width: '10%',
                displayName: 'No. Polisi'
            },
            {
                name: 'Customer',
                field: 'NamaKons',
                width: '20%',
                displayName: 'Pelanggan'
            },
            {
                name: 'SA',
                field: 'namasa',
                width: '10%',
                displayName: 'SA'
            },
            {
                name: 'Status',
                field: 'StatusDescriptionAm',
                width: '15%'
            }
        ];
        var approvalForPrint = [{
                name: 'Tanggal',
                field: 'LastModifiedDate',
                cellFilter: dateFilter2,
                width: '10%'
            }, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            {
                name: 'Permintaan',
                field: 'RequestorRoleId',
                width: '20%'
            },
            {
                name: 'No Polisi',
                field: 'LicensePlate',
                width: '10%',
                displayName: 'No. Polisi'
            },
            {
                name: 'VIN',
                field: 'VIN',
                width: '10%',
                displayName: 'VIN'
            },
            {
                name: 'Type',
                field: 'VIN',
                width: '20%',
                displayName: 'Type'
            }
        ];
        var approvalForDiskon = [{
                name: 'Tanggal',
                field: 'RequestDate',
                cellFilter: dateFilter2,
                width: '10%'
            }, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            {
                name: 'Permintaan',
                field: 'RequestorRoleId'
            },
            {
                name: 'Nama Part',
                field: 'JobPart.PartsName'
            },
            {
                name: 'Harga Normal',
                field: 'NormalPrice'
            },
            {
                name: 'Diskon',
                field: 'Discount'
            },
            {
                name: 'Harga Diskon',
                field: 'DiscountedPrice'
            },
        ];
        var approvalForJobReduction = [{
                name: 'Tanggal',
                field: 'LastModifiedDate',
                cellFilter: dateFilter2,
                width: '10%'
            }, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            {
                name: 'Nama Pekerjaan',
                field: 'JobTask.TaskName'
            },
            {
                name: 'No Polisi',
                field: 'Job.PoliceNumber',
                displayName: 'No. Polisi'
            },
            {
                name: 'No WO',
                field: 'Job.WoNo',
                displayName: 'No. WO'
            },
            {
                name: 'Request Date',
                field: 'RequestDate'
            }
        ];
        var showDataKabeng = [{
                Id: 1,
                Name: "Approval untuk Pengurangan DP"
            },
            {
                Id: 2,
                Name: "Approval untuk Print Service History"
            },
            {
                Id: 3,
                Name: "Approval untuk Diskon"
            },
            {
                Id: 4,
                Name: "Approval untuk Pengurangan Job"
            }
        ];
        var showData = [{
                Id: 1,
                Name: "Data Default SA"
            },
            {
                Id: 2,
                Name: "Data Semua SA"
            }
        ];
        $scope.option = showDataKabeng;
        $scope.option2 = showData;
        $scope.listCustomerIdx = $scope.listView;
        $scope.ButtonList = $scope.listControl;
        $scope.isRelease = false;
        $scope.isBilling = false;
        $scope.isKabeng = false;
        $scope.listButtonSettings = {
            new: {
                enable: true,
                icon: "fa fa-fw fa-car"
            }
        };
        $scope.listButtonSettingsOPL = {
            new: {
                enable: true,
                icon: "fa fa-fw fa-car"
            },
            delete: {
                enable: false
            }
        };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                console.log("customButton", row);
            }
        };
        $scope.vm = null;
        $scope.show_modal = {
            show: false,
            showApproval: false
        };
        $scope.show_modal = {
            showApprovalPrint: false,
            showApprovalDp: false,
            showApprovalDiskon: false,
            showApprovalJobReduction: false
        }
        $scope.modalModeAproval = 'new';
        $scope.modalMode = 'view';
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.noResults = true;
        $scope.disPng = true;
        $scope.disPnj = true;
        $scope.hiddenUserList = true;
        $scope.CVehicUId = 0;
        $scope.getCustIdBySearch = false;
        $scope.disVehic = true;
        $scope.hideVehic = true;
        $scope.Mlocation = [];
        $scope.CustomerAddress = [];
        $scope.mode = 'view';
        $scope.modeV = 'view';
        $scope.materialCategory = [{
            MaterialTypeId: 1,
            Name: "Spare Parts"
        }, {
            MaterialTypeId: 2,
            Name: "Bahan"
        }];
        $scope.VehicGas = [{
            VehicleGasTypeId: 1,
            VehicleGasTypeName: 'Bensin'
        }, {
            VehicleGasTypeId: 2,
            VehicleGasTypeName: 'Diesel'
        }, {
            VehicleGasTypeId: 3,
            VehicleGasTypeName: 'Hybrid'
        }];
        var dateFormat = 'dd/MM/yyyy';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateOptionsWithHour = {
            startingDay: 1,
            format: 'dd/MM/yyyy HH:mm:ss'
        };
        $scope.editenable = true;
        var dateFormat = 'dd/MM/yyyy';
        $scope.dateFollowupOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        var minDate = new Date();
        minDate.setDate(minDate.getDate() + 1);
        var minDate2 = new Date();
        // minDate2.setDate(minDate2.getDate() - 1);
        $scope.dateOptionsOPL = {
            startingDay: 1,
            format: dateFormat,
            minDate: minDate2
        }
        var dataCounter;
        $scope.minimal = minDate;
        $scope.dateFollowupOptions.minDate = $scope.minDate;
        $scope.user = CurrentUser.user();
        $scope.slider = {
            value: 0,
            options: {
                floor: 0,
                ceil: 100,
                showSelectionBar: true,
                translate: function(value) {
                    return value + '%';
                }
            }
        };
        //===================================END COPYan============


        var tmpPrediagnose = {};
        $scope.loading = true;
        $scope.gridData = [];
        $scope.DataChip = [];
        var startTime = '09:10';
        var startHour = '08:00';
        var currentTime = startTime;
        var isBoardLoaded = false;
        $scope.JpcbBpData = {
            screen_step: 1
        }
        $scope.JpcbBpData.startDate = new Date();
        $scope.JpcbBpData.showProdBoard = false;
        $scope.JpcbBpData.currentGroup = false;

        $scope.JpcbBpData.cuarrentDate = new Date();
        var boardChips = false;
        //boardChips = JpcbBpDataFactory.getData(false);
        var selectedChipData = {}
            //JpcbBpDataFactory.chipStatistic(boardChips, $scope.JpcbBpData);


        var vlong1 = 'col-xs-9';
        var vlong2 = 'col-xs-12';

        var boardName = 'jpcb-bp-board';

        $scope.safeApply = function(fn) {
            try {
                var phase = this.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {
                    if (fn && (typeof(fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }

            } catch (excp) {
                try {
                    this.$apply();
                } catch (exx) {
                    //
                }
            }
        };

        // $scope.updateScreenStep = function(step){
        //     $scope.JpcbBpData.screen_step = step;
        //     $scope.$apply();
        // }

        //$scope.updateScreenStep(4);

        var secondCounter = 0;
        var currentTime;
        $scope.clock = '';
        //$scope.clock = currentTime+':'+pad(secondCounter, 2);
        var pad = function(num, size) {
            var s = "000000000" + num;
            return s.substr(s.length - size);
        }

        var onTimer = function() {
            //$scope.clock = currentTime+':'+pad(secondCounter, 2);
            if (secondCounter >= 59) {
                secondCounter = 0;
                onMinuteTimer();
            } else {
                secondCounter = secondCounter + 1;
            }
        }
        var onMinuteTimer = function() {
            var vmin = JsBoard.Common.getMinute(currentTime);
            var vinc = 1;
            currentTime = JsBoard.Common.getTimeString(vmin + vinc);

            // var vxtime = convertHourToColumn(currentTime);
            // jpcbProduction.layout.updateTimeLine(jpcbProduction.board, vxtime);
            // jpcbProduction.draggableChips(checkChipDraggable);
            // updateDelayedChip();
            // updateBlockedRow();
            // jpcbProduction.redraw();
        }


        var tick = function() {
            //$scope.clock = Date.now() // get the current time
            onTimer();
            $timeout(tick, 100); // reset the timer
        }
        $timeout(tick, 100);


        $scope.showChipInfo = function(vmode) {
            if (vmode) {
                $scope.wcolumn = vlong1;
                $scope.ncolumn = 'col-xs-3';
                $scope.ncolvisible = 1;
            } else {
                $scope.wcolumn = vlong2;
                $scope.ncolvisible = 0;
            }
        }

        // click buttons BP
        $scope.additionalJob = function() {
            var xboard = JpcbBpFactory.getBoard(boardName);
            xboard.blinkChips(function(vchip) {
                if (vchip.data.additionalJob == 1) {
                    return true;
                }
                return false;
            }, {
                strokeWidth: 4
            });
        }
        $scope.pausedJob = function() {
            var xboard = JpcbBpFactory.getBoard(boardName);
            xboard.blinkChips(function(vchip) {
                if (vchip.data.intStatus == 6) {
                    return true;
                }
                return false;
            }, {
                strokeWidth: 4
            });
        }
        $scope.waitingFI = function() {
            var xboard = JpcbBpFactory.getBoard(boardName);
            xboard.blinkChips(function(vchip) {
                if (vchip.data.waiting_fi == 1) {
                    return true;
                }
                return false;
            }, {
                strokeWidth: 4
            });

        }
        $scope.delayedJob = function() {
            var xboard = JpcbBpFactory.getBoard(boardName);
            xboard.blinkChips(function(vchip) {
                var vlayout = vchip.container.layout;
                var vdata = vchip.data;
                if ((vdata.status !== 'done')) {
                    var vNow = new Date();
                    if (typeof vdata.endDate !== 'undefined') {
                        var xdate = $filter('date')(vdata.endDate, 'yyyy-MM-dd HH:mm:ss');
                        var xnow = $filter('date')(vNow, 'yyyy-MM-dd HH:mm:ss');
                        if (xnow > xdate) {
                            return true;
                        }
                    }
                }
                return false;
            }, {
                strokeWidth: 4
            });
        }


        var checkEditableItem = function(vchipdata, vitem) {
            if (vitem.chipType == vchipdata.ActiveChip) {
                var xstatus = vchipdata.Status + '';
                // var enableList = ["4", "6"];
                var enableList = ["1", "2", "3", "4", "5", "6", "0"];
                if (enableList.indexOf(xstatus) >= 0) {
                    return true;
                }
                // var enableList = ["4", "6", "0"];
                var enableList = ["1", "2", "3", "4", "5", "6", "0"];
                var xstatus = vitem.intStatus + '';
                if (enableList.indexOf(xstatus) >= 0) {
                    return true;
                }

            } else {
                // var enableList = ["4", "6", "0"];
                var enableList = ["1", "2", "3", "4", "5", "6", "0"];
                var xstatus = vitem.intStatus + '';
                if (enableList.indexOf(xstatus) >= 0) {
                    return true;
                }

            }

            return false;

            //var enableList = ["0", "3", "4", "6", "7", "8", "9", "10"]
            // var enableList = ["4", "6"]
            // var xstatus = vitem.intStatus+'';
            // if (enableList.indexOf(xstatus)>=0){
            //     return true;
            // }
            // return false;
        }


        var displayChipData = function(vJobId, vdata) {

            var actualDateTime = new Date();
            var chipStartDateTime = vdata.PlanDateTimeStart;
            $scope.disableSplitButton = false;
            $scope.belomPloting = true;
            $scope.sedangPause = false;
            if (chipStartDateTime == null || chipStartDateTime == undefined){
                $scope.disableSplitButton = true;
            } else {
                if (actualDateTime.getTime() < chipStartDateTime.getTime()){
                    $scope.disableSplitButton = true;
                }
            }

            if (vdata.stallId == null || vdata.stallId == undefined || vdata.stallId == '' || vdata.stallId <= 0){
                $scope.belomPloting = true;
            } else {
                $scope.belomPloting = false;
            }

            if (vdata.intStatus == 2 && vdata.isDispatched == 1){
                $scope.sedangPause = true;
            }
            


            var xboard = JpcbBpFactory.getBoard(boardName);
            var vcheck = false;
            if ((typeof detailJobData[vJobId] !== 'undefined') && vcheck) {
                $scope.displayedChip = detailJobData[vJobId];
                $scope.currentChip = vdata;
                $scope.safeApply();
            } else {
                $scope.displayedChip = {
                    JobId: vJobId
                }
                $scope.currentChip = vdata;
                console.log('current CHIP BOS', $scope.currentChip)

                // $scope.showSplit = true;

                $scope.showSplit = false;
                console.log('status chip', $scope.currentChip.intStatus)
                if ($scope.currentChip.intStatus == 2) {
                    $scope.showSplit = true;
                } else {
                    $scope.showSplit = false;
                }
                var vconfig = {
                    customData: {
                        isSplitable: vdata.isSplitable
                    }
                }
                JpcbBpFactory.CheckStartAllocation(vJobId).then(function(resul){

                    var vurl = '/api/as/Jobs/' + vJobId;
                    $http.get(vurl, vconfig).then(function(resp) {
                        if (typeof resp.data.Result[0] !== 'undefined') {
                            var vresult = resp.data.Result[0];
                            $scope.DataChip = resp.data.Result[0];
                            console.log('dataChip >>', $scope.DataChip);


                            var PosisiChip = 0; //cek posisi oldchip == 0
                            var DurasiChip = 0;
                            for (var i = 0; i < $scope.DataChip.JobTaskBp.length; i++) {
                                if ($scope.DataChip.JobTaskBp[i].isOldChip == 0) {
                                    PosisiChip = i;
                                }
                            }

                            if ($scope.currentChip.chipType == 1) {
                                DurasiChip = $scope.DataChip.JobTaskBp[PosisiChip].BodyEstimationMinute;
                            } else if ($scope.currentChip.chipType == 2) {
                                DurasiChip = $scope.DataChip.JobTaskBp[PosisiChip].PutyEstimationMinute;
                            } else if ($scope.currentChip.chipType == 3) {
                                DurasiChip = $scope.DataChip.JobTaskBp[PosisiChip].SurfacerEstimationMinute;
                            } else if ($scope.currentChip.chipType == 4) {
                                DurasiChip = $scope.DataChip.JobTaskBp[PosisiChip].SprayingEstimationMinute;
                            } else if ($scope.currentChip.chipType == 5) {
                                DurasiChip = $scope.DataChip.JobTaskBp[PosisiChip].PolishingEstimationMinute;
                            } else if ($scope.currentChip.chipType == 6) {
                                DurasiChip = $scope.DataChip.JobTaskBp[PosisiChip].ReassemblyEstimationMinute;
                            } else if ($scope.currentChip.chipType == 7) {
                                DurasiChip = $scope.DataChip.JobTaskBp[PosisiChip].FIEstimationMinute;
                            }

                            // var vNamaSa = '';
                            // if (typeof vresult.UserSa == 'object'){
                            //     if (typeof vresult.UserSa.EmployeeName !== 'undefined'){
                            //         vNamaSa = vresult.UserSa.EmployeeName;
                            //     }
                            // }
                            // vresult.NamaSa = vNamaSa;
                            var vstallid = vdata.stallId;
                            vresult.StallName = '';
                            if ((typeof xboard !== 'undefined') && (typeof xboard.config !== 'undefined') && (typeof xboard.config.argStall !== 'undefined') &&
                                (typeof xboard.config.argStall.stallVisible !== 'undefined') && (typeof xboard.config.argStall.stallVisible[vstallid] !== 'undefined') &&
                                (typeof xboard.config.argStall.stallVisible[vstallid].title !== 'undefined')) {
                                vresult.StallName = xboard.config.argStall.stallVisible[vstallid].title;
                            }
                            console.log(vresult.PlanStart)
                            var jamStartAllocation = '';
                            if (vresult.PlanStart == null || vresult.PlanStart == undefined) {
                                jamStartAllocation = ''
                            } else {
                                jamStartAllocation = vresult.PlanStart
                            }
                            // vresult.StartAllocation = $filter('date')(vresult.PlanDateStart, 'dd MMM yyyy') + ' ' + vresult.PlanStart;
                            vresult.StartAllocation = $filter('date')(vresult.PlanDateStart, 'dd MMM yyyy') + ' ' + jamStartAllocation;

                            var vdeliv = vresult.FixedDeliveryTime1;
                            if (vresult.FixedDeliveryTime2) {
                                vdeliv = vresult.FixedDeliveryTime2;
                            }
                            vresult.DeliveryTime = $filter('date')(vdeliv, 'dd MMM yyyy HH:mm');
                            vresult.isWOBase = vresult.isWOBase + '';
                            vresult.editable = checkEditableItem(vresult, vdata);
                            vresult.FmtWoCreatedDate = $filter('date')(vresult.WoCreatedDate, 'dd MMM yyyy hh:mm');
                            var vIsSplitable = 0;
                            if ((typeof resp.config.customData !== 'undefined') && (typeof resp.config.customData.isSplitable !== 'undefined')) {
                                vIsSplitable = resp.config.customData.isSplitable;
                            }
                            vresult.chipType = vdata.chipType;
                            vresult.Extension = vdata.ext / 60.0;
                            if (typeof vresult['JobId'] !== 'undefined') {
                                var xJobId = vresult['JobId'];
                                detailJobData[xJobId] = vresult;
                                if (typeof $scope.displayedChip == 'undefined' || typeof $scope.displayedChip.JobId == 'undefined') {
                                    $scope.displayedChip = detailJobData[vJobId];
                                    $scope.displayedChip.isSplitable = vIsSplitable;
                                    $scope.displayedChip.Durasi = DurasiChip;
                                    $scope.safeApply();
                                } else {
                                    if ($scope.displayedChip.JobId == xJobId) {
                                        $scope.displayedChip = detailJobData[vJobId];
                                        $scope.displayedChip.isSplitable = vIsSplitable;
                                        $scope.displayedChip.Durasi = DurasiChip;
                                        $scope.safeApply();
                                    }
                                }
                            }
                        }
                    });

                })
                
            }
        }

        $scope.dispatchChip = function(job, dispatched) {
            console.log("Job", job);
            console.log("dispatched", dispatched);
            console.log('buat nyari jobtaskBPID', $scope.currentChip);
            console.log('group list', $scope.GroupListTemp)


            $scope.backdrop = true;
            setTimeout(function() {
                $scope.backdrop = false;
            }, 25000);

            var dtBoard = JpcbBpFactory.getBoard('jpcb-bp-board')
            var ExtendDateFinish = JpcbBpFactory.columnToTime('jpcb-bp-board', ($scope.currentChip.col + $scope.currentChip.width));

            // validasi cek kl extend melebihi hari yg tampil di board  ================================================================================ start
            // karena jika extend, dan panjang ext lewat dr jam istirahat, maka dr be sudah di calculate tambah sejumlah jam istirahat
            var dataharijam = dtBoard.layout.board.headerItems;
            var tambahanjamIstirahat = 0
            var awalLoop = Math.floor($scope.currentChip.col)
            for (var x=awalLoop; x<dataharijam.length; x++){
                if (dataharijam[x].colInfo !== undefined){
                    if (dataharijam[x].colInfo.isBreak === true){
                        tambahanjamIstirahat++;
                    }
                }
            }
            
            var planwidth = 0;
            for (var c=0; c<job.JobTaskBp.length; c++){
                if (job.JobTaskBp[c].JobTaskBpId == $scope.currentChip.jobtaskbpid) {
                    if ($scope.currentChip.chipType == 1){
                        planwidth = job.JobTaskBp[c].BodyEstimationMinute
                    } else if ($scope.currentChip.chipType == 2){
                        planwidth = job.JobTaskBp[c].PutyEstimationMinute
                    } else if ($scope.currentChip.chipType == 3){
                        planwidth = job.JobTaskBp[c].SurfacerEstimationMinute
                    } else if ($scope.currentChip.chipType == 4){
                        planwidth = job.JobTaskBp[c].SprayingEstimationMinute
                    } else if ($scope.currentChip.chipType == 5){
                        planwidth = job.JobTaskBp[c].PolishingEstimationMinute
                    } else if ($scope.currentChip.chipType == 6){
                        planwidth = job.JobTaskBp[c].ReassemblyEstimationMinute
                    } else if ($scope.currentChip.chipType == 7){
                        planwidth = job.JobTaskBp[c].FIEstimationMinute
                    }
                }
            }
            planwidth = parseFloat((planwidth/60))


            // var validationExt = $scope.currentChip.col + $scope.currentChip.width + parseInt(job.Extension) + tambahanjamIstirahat; // ini width nya kl uda ada extend trs mau di tambah lg extendnya jd ngaco, karena width di sini uda ada extend nya dan jam breaknya
            var validationExt = $scope.currentChip.col + planwidth + parseFloat(job.Extension) + tambahanjamIstirahat; 
            

            validationExt = Math.floor(validationExt) // di math.floor krn liat dr function column to time
            if (dtBoard.layout.board.headerItems[validationExt] === undefined){
                $scope.backdrop = false;
                $timeout(
                    bsAlert.alert({
                        title: "Waktu extend melebihi jumlah hari yang tampil pada board.",
                        text: "Lakukan Perubahan Tanggal Tampilan Board Terlebih Dahulu.",
                        type: "warning",
                        showCancelButton: false
                    })
                , 100);
                
                return ;
            }


            // validasi cek kl extend melebihi hari yg tampil di board  ================================================================================ end

            console.log('jam akhir chip', ExtendDateFinish)

            JpcbBpFactory.getTodayAbsensi().then(function(result2) {
                if (result2.data.Result.length == 0 && dispatched == 1) {
                    $scope.backdrop = false;
                    $timeout(
                        // showMessageWarn('Chip tidak dapat di ploting pada stall tersebut. Pastikan absensi Teknisi sudah tersedia untuk hari ini pada stall tersebut.')
                        bsAlert.alert({
                            title: "Chip tidak dapat di ploting pada stall tersebut.",
                            text: "Pastikan absensi Teknisi sudah tersedia untuk hari ini pada stall tersebut.",
                            type: "warning",
                            showCancelButton: false
                        })
                    , 100);
                } else {
                    JpcbBpFactory.CekIsChipOnPlot(job.JobId, $scope.currentChip.jobtaskbpid).then(function(rezus) {

                        if (rezus.data == 666 && dispatched == 1) {
                            $scope.backdrop = false;
                            $timeout(showMessageWarn('Tidak dapat melakukan dispatch karena posisi chip berubah, silahkan refresh'), 100);
                        } else {
        
                            JpcbBpFactory.cekRawatJalanGetout(job.JobId).then(function(resu) {
                                if (resu.data == 1){
                                    //boleh dispatch
                
                                    var vurl2 = '/api/as/Jobs/' + job.JobId;
                                    var ini_JobTaskBp = [];
                
                                    $http.get(vurl2).then(function(resp) {
                                        if (typeof resp.data.Result[0] !== 'undefined') {
                                            ini_JobTaskBp = resp.data.Result[0].JobTaskBp;
                
                                            var ForemanId = null;
                                            angular.forEach($scope.GroupListTemp, function(val) {
                                                if (val.Id == $scope.currentChip.ChipGroupId) {
                                                    ForemanId = val.ForemanId;
                                                }
                                            })
                                            console.log('foreman id di Group ini', ForemanId)
                
                                            var NewItemChip = {};
                                            var posisiNew = 0;
                                            var chipTypeForDispatch = 1;
                                            var chipTypeForUpdate = job.chipType;
                                            if (dispatched == 1) {
                                                job.JobTaskBp = ini_JobTaskBp; // biar update data na, biar bs cek isoldchip terbaru dan isdispatched terbaru. soal nya user jarang refresh
                                                for (var i = 0; i < job.JobTaskBp.length; i++) {
                                                    if (job.JobTaskBp[i].isOldChip == 0) {
                                                        posisiNew = i;
                                                    }
                                                }
                
                                                NewItemChip = job.JobTaskBp[posisiNew];
                
                                                if ((NewItemChip.BodyEstimationMinute > 0 || (NewItemChip.BodyEstimationMinute == 0 && NewItemChip.Chip1ExtensionMinute > 0 )) && NewItemChip['Chip' + 1 + 'Status'] != 6) {
                                                    chipTypeForDispatch = 1;
                                                } else if ((NewItemChip.PutyEstimationMinute > 0 || (NewItemChip.PutyEstimationMinute == 0 && NewItemChip.Chip2ExtensionMinute > 0 )) && NewItemChip['Chip' + 2 + 'Status'] != 6) {
                                                    chipTypeForDispatch = 2;
                                                } else if ((NewItemChip.SurfacerEstimationMinute > 0 || (NewItemChip.SurfacerEstimationMinute == 0 && NewItemChip.Chip3ExtensionMinute > 0 )) && NewItemChip['Chip' + 3 + 'Status'] != 6) {
                                                    chipTypeForDispatch = 3;
                                                } else if ((NewItemChip.SprayingEstimationMinute > 0 || (NewItemChip.SprayingEstimationMinute == 0 && NewItemChip.Chip4ExtensionMinute > 0 )) && NewItemChip['Chip' + 4 + 'Status'] != 6) {
                                                    chipTypeForDispatch = 4;
                                                } else if ((NewItemChip.PolishingEstimationMinute > 0 || (NewItemChip.PolishingEstimationMinute == 0 && NewItemChip.Chip5ExtensionMinute > 0 )) && NewItemChip['Chip' + 5 + 'Status'] != 6) {
                                                    chipTypeForDispatch = 5;
                                                } else if ((NewItemChip.ReassemblyEstimationMinute > 0 || (NewItemChip.ReassemblyEstimationMinute == 0 && NewItemChip.Chip6ExtensionMinute > 0 )) && NewItemChip['Chip' + 6 + 'Status'] != 6) {
                                                    chipTypeForDispatch = 6;
                                                } else if ((NewItemChip.FIEstimationMinute > 0 || (NewItemChip.FIEstimationMinute == 0 && NewItemChip.Chip7ExtensionMinute > 0 )) && NewItemChip['Chip' + 7 + 'Status'] != 6) {
                                                    chipTypeForDispatch = 7;
                                                }
                
                                                if (NewItemChip.isDispatched == 1){
                                                    $scope.backdrop = false;
                                                    $timeout(showMessageWarn('Tidak dapat melakukan dispatch ulang, karena status chip sudah dispatch'), 100);
                                                    $scope.showChipInfo(false);
                                                    return
                                                }
                
                                            } else {
                                                chipTypeForDispatch = chipTypeForUpdate;
                                            }
                                            console.log('bangsooo', chipTypeForDispatch);
                
                                            var vext = 0;
                                            if (job.Extension) {
                                                vext = parseFloat(job.Extension);
                                            }
                                            var vdata = {
                                                JobId: job.JobId,
                                                ChipType: chipTypeForDispatch,
                                                TimeExtension: vext,
                                                Dispatched: dispatched,
                                                JobTaskBpId: $scope.currentChip.jobtaskbpid,
                                                ForemanId: ForemanId,
                                                ExtensionTimeFinish: $filter('date')(ExtendDateFinish, 'yyyy-MM-dd HH:mm')
                                            };
                                            // debugger;
                                            var url = '/api/as/Boards/BP/updateChipData';
                                            $http.put(url, [vdata], {
                                                headers: {
                                                    'Content-Type': 'application/json'
                                                }
                                            }).then(function(resp) {
                                                console.log(resp);
                                                var vmsg = "Data sent.";
                                                if ((typeof resp.data !== 'undefined')) {
                                                    var vmsg = resp.data;
                                                }
                                                if (vext == 0 || dispatched == 1){
                                                    $scope.backdrop = false;
                                                    $timeout(showMessage(vmsg), 100);
                                                    $scope.showChipInfo(false);
                                                }
                                                $timeout(function() {
                                                    $scope.backdrop = false;
                                                    showBoardJPCB();
                                                    
                                                }, 500);
                
                                            }).then(function(aa){
                                                if (dispatched == 0){
                                                    var extend_getTime = null;
                                                    var wktNow = new Date();
                
                                                    $timeout(function() {
                                                        var tesData = JpcbBpFactory.getDataChip();
                                                        console.log('ini tes tes tes aaaaaaaa', tesData)
                                                        $timeout(function() {
                
                                                        var ExtendDateFinish2 = null;
                                                        for(var i=0; i<tesData.length;i++){
                                                            if (tesData[i].chipType == chipTypeForDispatch && tesData[i].jobId == job.JobId && tesData[i].ext > 0 && tesData[i].isOldChip == 0){
                                                                ExtendDateFinish2 = JpcbBpFactory.columnToTime('jpcb-bp-board', (tesData[i].col + tesData[i].width));
                                                                extend_getTime = ExtendDateFinish2.getTime();
                                                            }
                                                        }
                
                                                        var vdata2 = {
                                                            JobId: job.JobId,
                                                            ChipType: chipTypeForDispatch,
                                                            TimeExtension: vext,
                                                            Dispatched: dispatched,
                                                            JobTaskBpId: $scope.currentChip.jobtaskbpid,
                                                            ForemanId: ForemanId,
                                                            ExtensionTimeFinish: $filter('date')(ExtendDateFinish2, 'yyyy-MM-dd HH:mm')
                                                        };
                                                        $http.put(url, [vdata2], {
                                                            headers: {
                                                                'Content-Type': 'application/json'
                                                            }
                                                        }).then(function(respon) {
                                                            console.log('borokokok',respon)
                                                            if (extend_getTime != null){
                                                                // var aa = wktNow.getTime()
                                                                // console.log(aa)
                                                                if (extend_getTime <= wktNow.getTime()){
                                                                    // kl akhir dari extend lbh kecil dr wkt sekarang ga blh katanya
                                                                    $timeout(showMessageWarn('Waktu extend tidak boleh kurang dari waktu sekarang. Silahkan tambah extend'), 100);
                                                                    $scope.backdrop = false;
                                                                } else {
                                                                    $timeout(showMessage(respon.data), 100);
                                                                    $scope.backdrop = false;
                                                                    $scope.showChipInfo(false);
                                                                }
                                                            } else {
                                                                $timeout(showMessage(respon.data), 100);
                                                                $scope.backdrop = false;
                                                                $scope.showChipInfo(false);
                                                            }
                                                            
                                                            // $timeout(showMessage(respon.data), 100);
                                                            // $timeout(showMessage('respon.data'), 100);
                                                        }, function (err) {
                                                            $scope.backdrop = false;
                                                        });
                                                        }, 1500);
                
                                                    }, 3000);
                                                    
                                                }
                                                
                                            });
                
                                        }
                                    }, function (err) {
                                        $scope.backdrop = false;
                                    })
                
                                    
                                } else {
                                    bsNotify.show({
                                        title: "Dispatch Chip Tidak Bisa",
                                        content: "Kendaraan rawat jalan belum keluar dari bengkel",
                                        type: 'danger'
                                    });
                                    $scope.backdrop = false;
                                }
                
                            }, function (err){
                                $scope.backdrop = false;
                            });
                            
        
                        }
        
                    })

                }
            })

            


            

            
        }


        $scope.BpCategory = {}
        $scope.BpWoList = []
        $scope.BpGroupList = []

        var loadMainData = function(config, onFinish) {
            var loadedStatus = [false, false, false];
            var updateLoadedData = function() {
                if (loadedStatus[0] && loadedStatus[1] && loadedStatus[2]) {
                    onFinish();
                }
            }

            $http.get(config.urlCategoryBp).then(function(res) {
                var xres = res.data.Result;
                var result = {};
                for (var i = 0; i < xres.length; i++) {
                    var vitem = xres[i];
                    result[vitem.MasterId] = {
                        id: vitem.MasterId,
                        name: vitem.Name,
                        type: vitem.Name
                    }
                }
                // result['all'] = {
                //     id: 0,
                //     name: 'All',
                //     type: ''
                // }
                // yg all dimatiin dl permintaan pa ian 21Mei2019
                $scope.BpCategory = result;
                console.log("$scope.BpCategory result", $scope.BpCategory);
                loadedStatus[0] = true;
                updateLoadedData();
            });

            // {chip: 'jpcb', row: 0, col: 0, timestatus: 1, width: 1, normal: 1, ext: 0,
            //     nopol: 'B 7654 FDA', tglcetakwo: '12/01/17',
            //     start: '08:00', normalDuration: '00:30', extDuration: '00:15',
            //     jamcetakwo: '08:00', jamjanjiserah: '09:00',
            //     type: 'AVANZA',
            //     pekerjaan: 'EM', teknisi: 'WAN',
            //     status: 'done',
            //     wotype: 'TPS', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'
            //   }

            $http.get(config.urlWoList).then(function(res) {
                var xres = res.data.Result;
                var result = [];
                var idx = 0;
                for (var i = 0; i < xres.length; i++) {
                    var vitem = xres[i];
                    var vdate = $filter('date')(vitem.PlanDateFinish, 'dd/MM/yyyy')
                    var vtime = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vitem.PlanFinish));
                    var vstartdate = $filter('date')(vitem.PlanDateStart, 'dd/MM/yyyy')
                    var vstarttime = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vitem.PlanStart));
                    var vappdate = $filter('date')(vitem.AppointmentDate, 'dd/MM/yyyy')
                    var vapptime = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vitem.AppointmentTime));
                    if (vitem.Status == 0) {
                        if (vapptime !== '00:00') {
                            vwaktu = vappdate + ' ' + vapptime;
                        } else {
                            vwaktu = vappdate;
                        }
                    } else {
                        if (vstarttime !== '00:00') {
                            vwaktu = vstartdate + ' ' + vstarttime;
                        } else {
                            vwaktu = vstartdate;
                        }
                    }

                    result.push({
                        jobId: vitem.JobId,
                        chip: 'jpcb',
                        row: 0,
                        col: idx,
                        width: 1,
                        normal: 1,
                        ext: 0,
                        nopol: vitem.PoliceNumber,
                        tipe: vitem.ModelType,
                        tgljanjiserah: vdate,
                        jamjanjiserah: vtime,
                        tglapp: vappdate,
                        jamapp: vapptime,
                        tgldatang: vstartdate,
                        jamdatang: vstarttime,
                        //waktudtg: vwaktu,
                        waktudtg: vitem.PushTimeIn ? $filter('date')(vitem.PushTimeIn, 'dd/MM/yyyy') : new Date (),
                        wotype: vitem.CategoryBP,
                        isDispatched: vitem.isDispatched,
                        filterDateStart: vitem.DatePlanStart ? $scope.changeFormatDate(vitem.DatePlanStart) : null,
                        //color_idx: idx,
                        color_idx: JpcbBpFactory.getIndexById(vitem.JobId),
                        MasterId: vitem.JobCatgForBPId,
                        chipsGroupId: vitem.ChipsGroupId
                    });
                    idx = idx + 1;
                };


                console.log("result bokek", result);

                // $scope.BpWoList = _.uniq(result, 'jobId');
                var listJobId = [];
                var newBpWoList = result.filter(function(a) {
                    if (listJobId.indexOf(a.jobId) === -1) {
                        listJobId.push(a.jobId);
                        return true;
                    } else {
                        return false;
                    };
                });
                $scope.BpWoList = newBpWoList;
                // console.log("listJobId", listJobId);
                // console.log("newBpWoList", newBpWoList);
                console.log(" $scope.BpWoList ", $scope.BpWoList);
                $scope.countAllocatedNumber = $scope.countAllocated(res.data.Result);

                $scope.countAllocatedNumber = 0;
                for (var i=0; i<$scope.BpWoList.length; i++){
                    if ($scope.BpWoList[i].isDispatched == 0){
                        $scope.countAllocatedNumber++;
                    }
                }

                loadedStatus[1] = true;
                updateLoadedData();
            });
            $http.get(config.urlGroupList).then(function(res) {
                $scope.GroupListTemp = res.data.Result;
                var xres = res.data.Result;
                var result = [];
                var idx = 0;
                for (var i = 0; i < xres.length; i++) {
                    var vitem = xres[i];
                    result.push({
                        GroupBPId: vitem.Id,
                        name: vitem.GroupName,
                        type: vitem.BpCategory,
                        count: vitem.TaskCount,
                        BpCategoryId: vitem.BpCategoryId,
                    });
                    idx = idx + 1;
                }
                $scope.BpGroupList = result;
                loadedStatus[2] = true;
                updateLoadedData();
            });


        }

        var afterMainDataLoaded = function() {
            for (var idx in $scope.BpCategory) {
                var vitem = $scope.BpCategory[idx];
                vitem.count = 0;
            }
            console.log('woi bapuk', $scope.BpWoList)
            for (var i = 0; i < $scope.BpWoList.length; i++) {
                if ($scope.BpWoList[i].isDispatched == 0){
                    var vitem = $scope.BpWoList[i];
                    var idx = vitem.MasterId;
                    if (typeof $scope.BpCategory[idx] !== 'undefined') {
                        $scope.BpCategory[idx].count = $scope.BpCategory[idx].count + 1;
                    } else {
                        console.log('ada aja undefined', i)
                        console.log('ada aja undefined', idx)
                        console.log('ada aja undefined', $scope.BpCategory[idx])
                    }
                }
            }

            // $scope.BpCategory['all'].count = $scope.BpWoList.length; yg all dimatiin dl permintaan pa ian 21Mei2019

            $scope.filterDataByType(null);
        }

        vconfig = {
            urlCategoryBp: '/api/as/Boards/MGlobal/1012',
            urlWoList: '/api/as/Boards/BP/WoList',
            urlGroupList: '/api/as/Boards/BP/GroupList/0'
        }
        loadMainData(vconfig, afterMainDataLoaded);

        $scope.refreshData = function(){
            vconfig = {
                urlCategoryBp: '/api/as/Boards/MGlobal/1012',
                urlWoList: '/api/as/Boards/BP/WoList',
                urlGroupList: '/api/as/Boards/BP/GroupList/0'
            }
            loadMainData(vconfig, afterMainDataLoaded);
        }

        var getSelBoardTitle = function(vcount) {
            result = []
            for (var i = 0; i < vcount; i++) {
                result.push({
                    text: i
                });
            }
            return result;
        }
        var boardConfig = {
            type: 'jpcb',
            board: {
                // scroll: 'none',
                rowHeight: 100,
                columnWidth: 180,
                headerType: 'simple',
                firstColumnWidth: 0,
                footerHeight: 0,
                headerHeight: 22,
                // headerItems: getSelBoardTitle(boardChips.length),
                showVerticalLine: false,
                container: 'jpcbbp-viewcontainer',
                iconFolder: 'images/boards/jpcb',
                firstColumnItems: [{
                    title: "",
                    items: ['', '', '', '', ''],
                    status: 'normal'
                }],
                chipGroup: 'selectboard',
                footerChipName: 'jpcb_footer',
                firstHeaderChipName: 'jpcb_header',
                headerChipName: 'jpcb_header',
            },
            margin: {
                left: 25
            },
            layout: {
                chipHeight: 600,
                chipGap: 2
            }
        }
        $scope.countAllocatedNumber = 0;
        $scope.countAllocated = function(data) {
            var counting = 0;
            _.map(data, function(val) {
                counting = counting + val.CountIsAllocated;
                console.log("countAllocated", counting);
            });
            console.log("countAllocated", counting);
            return counting;
        }
        $scope.UnselectChip = function() {
            vboard = theBoard;
            vboard.processChips(function(xchip) {
                    return true;
                },
                function(xchip) {
                    if (xchip.data.selected) {
                        xchip.data.selected = false
                        vboard.updateChip(xchip);
                    }
                }, false);
            vboard.redraw();
        }

        var selectChip = function(vchip) {
            vboard = vchip.container;
            vboard.processChips(function(xchip) {
                    return true;
                },
                function(xchip) {
                    if (xchip.data.selected) {
                        xchip.data.selected = false
                        vboard.updateChip(xchip);
                    }
                }, false);
            vchip.data.selected = true;
            vchip.moveToTop();
            selectedChipData = vchip.data;
            $scope.selectedChip = selectedChipData;
            if (vchip.shapes.widecontrol && vchip.shapes.highlight) {
                updateDragInfo(vchip.shapes.widecontrol, vchip.shapes.highlight);
            }
            vboard.updateChip(vchip);
            vboard.redraw();
        }

        var setOnClick = function(vboard, vEvent) {
            vboard.processChips(function(vchip) {
                    return true;
                },
                function(vchip) {
                    vchip.on('mousedown', function(evt) {
                        //alert('click');
                        selectChip(this);
                    });
                    vchip.setListening(true);
                }, false);
        }

        var theBoard = false;
        var showBoard = function(vchips) {
            boardConfig.board.headerItems = getSelBoardTitle(vchips.length),
                theBoard = new JsBoard.Container(boardConfig);
            theBoard.createChips(vchips);
            theBoard.redraw();
            setOnClick(theBoard);

            theBoard.autoResizeStage();
            theBoard.redraw();

        }

        $scope.filterType = '';
        $scope.filterDataByType = function(vfilter) {
            console.log('viltervilter boi', vfilter);
            JpcbBpFactory.getTypeHeader(vfilter);
            var vchips;
            if (vfilter === null) {
                vfilter = $scope.filterType;
            } else {
                $scope.filterType = vfilter;
            }
            var vchips = []
            xfilter = vfilter.toLowerCase();
            var vidx = 0;
            for (var i = 0; i < $scope.BpWoList.length; i++) {
                var vitem = $scope.BpWoList[i];
                if ((!vfilter) || (vitem.wotype !== null)) {
                    if ((!vfilter) || (vitem.wotype.toLowerCase() == xfilter)) {
                        vchips.push(vitem);
                        vitem.col = vidx;
                        vidx = vidx + 1;
                    }
                }
            }
            showBoard(vchips);
        }


        var selectGroup = function(vtype) {
            var result = []
            console.log('selectGroup vtype', vtype);
            console.log('selectGroup $scope.BpWoList', $scope.BpWoList);
            // for (var i = 0; i < $scope.BpGroupList.length; i++) {
            //     vitem = $scope.BpGroupList[i];
            //     if ((vitem.type == vtype) || (vtype === '')) {
            //         result.push(vitem);
            //     }
            // }
            _.map($scope.BpGroupList, function(val) {
                // if ((val.type == vtype) || (vtype === '')) {
                // val.Count = 0;
                val.count = 0;
                result.push(val);
                // }
            });

            var newArray = _.filter($scope.BpWoList, function(a) {
                return a.MasterId == vtype;
            });

            var array1 = 0;
            _.map(result, function(val) {
                _.map(newArray, function(a) {
                    if (a.chipsGroupId == val.GroupBPId) {
                        val.count = val.count + 1;
                    }
                })
            });
            console.log('selectGroup result', result);
            console.log('selectGroup newArray', newArray);
            return result;
        };

        $scope.nextSelectChip = function() {
            $scope.idgroup = 0;
            $scope.JpcbBpData.showProdBoard = false;
            var vSelected = false;
            theBoard.processChips(function(xchip) {
                    // console.log('xchip result', xchip);
                    if (xchip.data.selected) {
                        return true;
                    }
                },
                function(xchip) {
                    vSelected = xchip;
                }, false);
            var goNext = false;
            var vWoType = '';
            // if (!vSelected){
            //     //alert('Chip harus dipilih terlebih dahulu');
            //     //showMessage('Chip harus dipilih terlebih dahulu');
            //     if ($scope.filterType){
            //         goNext = true;
            //         vWoType = $scope.filterType;
            //     } else {
            //         showMessage('Pilih Chip atau Kategori BP (TPS, Light, Medium Heavy) terlebih dahulu.')
            //     }
            // } else {
            //     goNext = true;
            //     vWoType = vSelected.data.wotype;
            // }
            // console.log('$scope.filterType result', $scope.filterType);
            console.log('vSelected result', vSelected);
            JpcbBpFactory.getTypeHeader(vSelected.data.wotype);
            // console.log(' $scope.selectedChip result',  $scope.selectedChip);
            goNext = true;
            if (goNext) {
                vWoType = vSelected.data.MasterId;
                console.log('$scope.JpcbBpData result', $scope.JpcbBpData);
                $scope.JpcbBpData.currentWoType = vWoType;
                $scope.JpcbBpData.teamList = selectGroup($scope.JpcbBpData.currentWoType);
                $scope.JpcbBpData.screen_step = 2;

                // ====================
                if (vSelected.data.filterDateStart !== null) {
                    console.log("$scope.JpcbBpData =======>", $scope.JpcbBpData);
                    $scope.JpcbBpData.select = vSelected.data.chipsGroupId;
                    $scope.JpcbBpData.cuarrentDate = $scope.changeFormatDate(vSelected.data.filterDateStart);
                    // $scope.showProductionBoard = function(team, vdate) {
                    _.map($scope.JpcbBpData.teamList, function(val) {
                            if (val.GroupBPId == vSelected.data.chipsGroupId) {
                                $scope.JpcbBpData.currentGroup = val;
                            }
                        })
                        // console.log("==================> CSS",$('.radioCustomer label input[type=radio]').attr('ariaChecked'));
                        // $scope.JpcbBpData.currentGroup = vSelected.data.chipsGroupId;
                    $scope.JpcbBpData.showProdBoard = true;

                    //sini bill ah
                    JpcbBpFactory.getListStall($scope.JpcbBpData.currentGroup.GroupBPId).then(function(res){
                        console.log('masuk lagu1')
                        var xres = res.data.Result;
                        var vstart = '';
                        var vend = '';
                        for(var i=0;  i<xres.length; i++){
                            var vitem = xres[i];
                            // === NEW CODE Before GO LIVE
                            if (!vitem.DefaultOpenTime) {
                                vitem.DefaultOpenTime = '08:00';
                            }
                            if (typeof vitem.DefaultOpenTime !== 'undefined') {
        
                                //cek defaultopen time menit nya 00 ga? kl ga 00 set jd 00 biar header nya menit nya 00
                                var cekDefaultOpenTime = vitem.DefaultOpenTime.split(':');
                                if (cekDefaultOpenTime[1].toString() != '00'){
                                    vitem.DefaultOpenTime = cekDefaultOpenTime[0] + ':' + '00' + ':' + '00'
                                }
        
                                // if (vitem.DefaultOpenTime){
                                if (vstart === '') {
                                    vstart = vitem.DefaultOpenTime;
                                } else {
                                    if (vstart > vitem.DefaultOpenTime) {
                                        vstart = vitem.DefaultOpenTime;
                                    }
                                }
                                // }
                            }
                            if (vitem.DefaultCloseTime == undefined) {
                                vitem.DefaultCloseTime = '17:00';
                            }
                            if (typeof vitem.DefaultCloseTime !== 'undefined') {
                                if (vitem.DefaultCloseTime >= '23:59') {
                                    vitem.DefaultCloseTime = '24:00';
                                }
                                if (vend === '') {
                                    vend = vitem.DefaultCloseTime;
                                } else {
                                    if (vend < vitem.DefaultCloseTime) {
                                        vend = vitem.DefaultCloseTime;
                                    }
                                }
                            }
                        }
                        if (vstart === '') {
                            vstart = '08:00';
                        }
                        if (vend === '') {
                            vend = '17:00';
                        }
                        var vxmin = JsBoard.Common.getMinute(vend);
                        vend = JsBoard.Common.getTimeString(vxmin);
                        // me.startHour = vstart;
                        $scope.startlongDay = vstart;
                        $scope.endlongDay = vend;
        
                        $timeout(function() {
                        console.log('masuk lagu2')
                            showBoardJPCB();
                        }, 100);
                    });

                    // $timeout(function() {
                    //     showBoardJPCB();
                    // }, 100);


                    // }
                }

            };
        }

        $scope.backSelectChip = function() {
            $scope.JpcbBpData.showProdBoard = false;
            $scope.JpcbBpData.screen_step = 1;
            $scope.JpcbBpData.cuarrentDate = $scope.changeFormatDate(new Date());
            $scope.JpcbBpData.select = null;
            $scope.showChipInfo(false);

        }

        var showMessage = function(message) {
            // ngDialog.openConfirm({
            //     width: 400,
            //     template: message,
            //     plain: true,
            //     controller: 'JpcbBpController',
            //     scope: $scope
            // });
            bsAlert.alert({
                title: message,
                text: "",
                type: "success"
            });
        }

        var showMessageWarn = function(message) {
            // ngDialog.openConfirm({
            //     width: 400,
            //     template: message,
            //     plain: true,
            //     controller: 'JpcbBpController',
            //     scope: $scope
            // });
            bsAlert.alert({
                title: message,
                text: "",
                type: "warning"
            });
        }


        $scope.showProductionBoard = function(team, vdate) {
            console.log('team', team);
            $scope.JpcbBpData.currentGroup = team;
            $scope.JpcbBpData.showProdBoard = true;
            // $scope.checkStall();
            JpcbBpFactory.getListStall($scope.JpcbBpData.currentGroup.GroupBPId).then(function(res){
                console.log('masuk lagu1')
                var xres = res.data.Result;
                var vstart = '';
                var vend = '';
                for(var i=0;  i<xres.length; i++){
                    var vitem = xres[i];
                    // === NEW CODE Before GO LIVE
                    if (!vitem.DefaultOpenTime) {
                        vitem.DefaultOpenTime = '08:00';
                    }
                    if (typeof vitem.DefaultOpenTime !== 'undefined') {

                        //cek defaultopen time menit nya 00 ga? kl ga 00 set jd 00 biar header nya menit nya 00
                        var cekDefaultOpenTime = vitem.DefaultOpenTime.split(':');
                        if (cekDefaultOpenTime[1].toString() != '00'){
                            vitem.DefaultOpenTime = cekDefaultOpenTime[0] + ':' + '00' + ':' + '00'
                        }

                        // if (vitem.DefaultOpenTime){
                        if (vstart === '') {
                            vstart = vitem.DefaultOpenTime;
                        } else {
                            if (vstart > vitem.DefaultOpenTime) {
                                vstart = vitem.DefaultOpenTime;
                            }
                        }
                        // }
                    }
                    if (vitem.DefaultCloseTime == undefined) {
                        vitem.DefaultCloseTime = '17:00';
                    }
                    if (typeof vitem.DefaultCloseTime !== 'undefined') {
                        if (vitem.DefaultCloseTime >= '23:59') {
                            vitem.DefaultCloseTime = '24:00';
                        }
                        if (vend === '') {
                            vend = vitem.DefaultCloseTime;
                        } else {
                            if (vend < vitem.DefaultCloseTime) {
                                vend = vitem.DefaultCloseTime;
                            }
                        }
                    }
                }
                if (vstart === '') {
                    vstart = '08:00';
                }
                if (vend === '') {
                    vend = '17:00';
                }
                var vxmin = JsBoard.Common.getMinute(vend);
                vend = JsBoard.Common.getTimeString(vxmin);
                // me.startHour = vstart;
                $scope.startlongDay = vstart;
                $scope.endlongDay = vend;

                $timeout(function() {
                console.log('masuk lagu2')
                    showBoardJPCB();
                }, 100);
            });

        }

        $scope.showProductionBoardByDate = function() {
                if ($scope.JpcbBpData.showProdBoard) {
                    $scope.JpcbBpData.showProdBoard = true;
                    $timeout(function() {
                        showBoardJPCB();
                    }, 100);
                }

            }
            //showMessage('testing');



        // Show board


        var showBoardJPCB = function() {
            if ($scope.JpcbBpData.currentGroup) {
                detailJobData = {}
                var vscrollhpos = JpcbBpFactory.getScrollablePos(boardName);
                var vJobId = null;
                if ($scope.selectedChip) {
                    vJobId = $scope.selectedChip.jobId;
                }
                var vitem = {
                    // mode: $scope.jpcbgrview.showMode,
                    boardName: boardName,
                    currentDate: $scope.JpcbBpData.cuarrentDate,
                    //stallUrl: '/api/as/Boards/Stall/2',
                    stallUrl: '/api/as/Boards/BP/Stall/' + $scope.JpcbBpData.currentGroup.GroupBPId,
                    groupId: $scope.JpcbBpData.currentGroup.GroupBPId,
                    //chipUrl: '/api/as/Boards/jpcb/bydate/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/0/17',
                    chipUrl: '/api/as/Boards/BP/WoList',
                    // nopol: $scope.jpcbgrview.nopol,
                    // tipe: $scope.jpcbgrview.tipe
                    JobId: vJobId,
                    longStart: $scope.startlongDay,
                    longEnd:$scope.endlongDay,
                    team: $scope.JpcbBpData.currentGroup,

                }

                JpcbBpFactory.CheckGroupBP($scope.selectedChip.jobId).then(function(ress) {
                    console.log("cek ada di group mana", ress.data);
                    $scope.idgroup = ress.data
                });

                JpcbBpFactory.selectedChip($scope.selectedChip);
                console.log("showBoardJPCB");
                JpcbBpFactory.showBoard(boardName, {
                    container: 'jpcbbp-prod-container',
                    incrementMinute: 60,
                    currentChip: vitem,
                    options: {
                        standardDraggable: true,
                        draggableStatus: ["0", "2"],
                        stopageAreaHeight: 200,
                        //testing12345: 1000,
                        onJpcbReady: function(vjpcb) {
                            console.log("*****board ready", vjpcb);
                            $scope.isLoading = false;
                            $scope.countNoShow = vjpcb.argChips.countNoShow;
                            $scope.countBooking = vjpcb.argChips.countBooking;

                            // bangsulllllllll
                            vjpcb.theBoard.layout.board.checkDragDrop = function(vchip, vcol, vrow) {
                                console.log("dragdrop JPCB");

                                if (vrow >= 0) {
                                    vlayout = vjpcb.theBoard.layout;
                                    if (typeof vlayout.infoRows[vrow] != 'undefined') {
                                        // var vStallInfo = vlayout.infoRows[vrow];
                                        // var vEnd = vStallInfo.data.endHour;
                                        var vjamstart = JpcbBpFactory.columnToTime('jpcb-bp-board', vcol);
                                        vjamstart = $filter('date')(vjamstart, 'HH:mm:ss');
                                        // if (vjamstart >= vEnd) {
                                        //     return false;
                                        // }
                                        var splitJam = vjamstart.split(':');
                                        if (splitJam[0] == '12'){
                                            return null; // ini kalau dia di taro di jam 12 - 13. ga boleh di jam istirahat
                                        }

                                        //if (vchip.data.Final)

                                    }
                                } else {
                                    return false;
                                }

                                return true;
                            };
                            // alert('ready');
                        },
                        board: {
                            scrollableLeft: vscrollhpos,
                            footerChipName: 'footer_stopage',
                            firstColumnChipName: 'jpcb_stall',
                            firstHeaderChipName: 'jpcb_first_header',
                            fixedHeaderChipName: 'jpcb_fixed_header',
                        },
                        chip: {
                            stallTechWidth: 60,
                            incrementMinute: 60,
                            columnWidth: 120,
                            teamName: $scope.JpcbBpData.currentGroup.name,
                        }
                    },
                    onChipClick: function(vchip) {

                        console.log('on chip click!!', vchip);
                        console.log('masukbos 2 kali')

                        if ($scope.ProsesPloting == true){
                            bsAlert.alert({
                                title: "Mohon Tunggu Proses Ploting Chip Selesai & Pastikan Semua Chip Sudah Ter-Plot",
                                text: "",
                                type: "warning",
                                showCancelButton: false
                            },function() {
            
                            })
                            return false
                        }

                        $scope.showChipInfo(true);

                        displayChipData(vchip.currentTarget.data.jobId, vchip.currentTarget.data);

                        //$scope.displayedChip = vchip.currentTarget.data;
                        //$scope.$apply();
                    },
                    onPlot: function(vchip) {
                        $scope.ProsesPloting = true;
                        setTimeout(function() {
                            $scope.ProsesPloting = false
                        }, 10000);
                        $scope.showChipInfo(false)
                        var plotUrl = '/api/as/Boards/BP/plotBoardJPCB?UnPlot=0';
                        console.log("on plot. vchip: ", vchip);
                        if (vchip.dragStartPos) {
                            if (vchip.data.row == vchip.dragStartPos.row && vchip.data.col == vchip.dragStartPos.col) {
                                // sama
                                console.log("sama");
                            } else {
                                var xDateStart = JpcbBpFactory.columnToTime(boardName, vchip.data.col);
                                var xDateEnd = JpcbBpFactory.columnToTime(boardName, vchip.data.col + vchip.data.width);
                                var vStallId = 0;
                                vlayout = vchip.container.layout;
                                if (vchip.data.row >= 0) {
                                    if (typeof vlayout.infoRows[vchip.data.row]) {
                                        vStallId = vlayout.infoRows[vchip.data.row].data.stallId;
                                    };
                                } else if (vchip.data.row == -2) {
                                    vStallId = -2;
                                };

                                if (vStallId == -2) {
                                    plotUrl = '/api/as/Boards/BP/plotBoardJPCB?UnPlot=1';
                                    console.log('ploturl baru kalau drag ke outstanding');
                                };
                                var vDateStart = $filter('date')(xDateStart, 'yyyy-MM-dd HH:mm:ss');
                                var vDateEnd = $filter('date')(xDateEnd, 'yyyy-MM-dd HH:mm:ss');
                                var startCurDate = $filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd');
                                var vobj = {
                                    "DateStart": vDateStart,
                                    "DateFinish": vDateEnd,
                                    "StallId": vStallId,
                                    "JobId": vchip.data.jobId,
                                    "ChipType": vchip.data.chipType,
                                    "GroupId": this.currentChip.groupId,
                                    "JobTaskBpId": vchip.data.jobtaskbpid,
                                    "StartCalendarDate": startCurDate
                                };
                                console.log("vobj", vobj);

                                $http.put(plotUrl, [vobj]).then(
                                    function(res) {
                                        console.log('xres1', res);

                                        // ------------- notif ga bs plot ke atas kl di browser lain chip na uda di dispatch -------- start
                                        // 666#Sudah Pernah Dispatch
                                        // 100#JPCB Plot Success
                                        if (res.data != null && res.data != undefined) {
                                            var cek_res = res.data.split('#')
                                            if (cek_res[0] == 666) {
                                                $timeout(showMessageWarn('Tidak dapat memindahkan chip karena chip sudah dispatch, silahkan refresh'), 100);

                                                // showBoardJPCB();
                                                // return;
                                            }
                                        }

                                        // ------------- notif ga bs plot ke atas kl di browser lain chip na uda di dispatch -------- end

                                        showBoardJPCB();

                                        var vurl = '/api/as/Jobs/' + vchip.data.jobId;
                                        $http.get(vurl).then(function(respx) {
                                            if (typeof respx.data.Result[0] !== 'undefined') {
                                                var hasil = respx.data.Result[0];
                                                // $scope.selectedChip.tgljanjiserah = hasil.FixedDeliveryTime2.getDate() + '/' + (hasil.FixedDeliveryTime2.getMonth()+1) + '/' + hasil.FixedDeliveryTime2.getFullYear();
                                                // $scope.selectedChip.jamjanjiserah = hasil.FixedDeliveryTime2.getHours() + ':' + hasil.FixedDeliveryTime2.getMinutes();
                                                $scope.selectedChip.tgljanjiserah = $filter('date')(hasil.FixedDeliveryTime2, 'dd/MM/yyyy');
                                                $scope.selectedChip.jamjanjiserah = $filter('date')(hasil.FixedDeliveryTime2, 'HH:mm');
                                            }
                                        });
                                    },
                                    function(res) {
                                        console.log('xres2', res);
                                        // var vlayout = vchip.container.layout;
                                        // var vstarty = vlayout.row2y(vchip.dragStartPos.row);
                                        // var vstartx = vlayout.col2x(vchip.dragStartPos.col);

                                        // vchip.moveTo(vchip.container.board.chips);
                                        // vchip.x(vstartx);
                                        // vchip.y(vstarty);
                                        // vchip.data.col = vchip.dragStartPos.col;
                                        // vchip.data.row = vchip.dragStartPos.row;
                                        // vchip.container.updateChip(vchip);
                                        // vchip.container.redraw();

                                    }
                                );


                                // // tidak sama
                                // console.log("tidak sama");
                                // //vchip.data;
                                // //var vinc = JpcbBpFactory.incrementMinute;
                                // var vdate = new Date(this.currentChip.currentDate);
                                // var vjamstart = JpcbBpFactory.columnToTime(vchip.data.col);
                                // console.log("vjamstart", vjamstart);
                                // var vjamend = JpcbBpFactory.columnToTime(vchip.data.col+vchip.data.width);
                                // console.log("vjamend", vjamend);
                                // console.log("vdate", vdate);
                                // var vDateStart = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd')+'T'+vjamstart+':00';
                                // var vDateEnd = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd')+'T'+vjamend+':00';
                                // var vStallId = 0;
                                // vlayout = vchip.container.layout;
                                // if (vchip.data.row>=0){
                                //     if (typeof vlayout.infoRows[vchip.data.row]){
                                //         vStallId = vlayout.infoRows[vchip.data.row].data.stallId;
                                //     }
                                // }
                                // var vobj = {
                                //     "DateStart": vDateStart,
                                //     "DateFinish": vDateEnd,
                                //     "StallId":vStallId,
                                //     "JobId"  :vchip.data.jobId,
                                // }
                                // console.log("vobj", vobj);
                                // $http.put(plotUrl, [vobj]).then(
                                //     function(res){
                                //         // console.log(error)
                                //     },
                                //     function(res){
                                //         var vlayout = vchip.container.layout;
                                //         var vstarty = vlayout.row2y(vchip.dragStartPos.row);
                                //         var vstartx = vlayout.col2x(vchip.dragStartPos.col);

                                //         vchip.moveTo(vchip.container.board.chips);
                                //         vchip.x(vstartx);
                                //         vchip.y(vstarty);
                                //         vchip.data.col = vchip.dragStartPos.col;
                                //         vchip.data.row = vchip.dragStartPos.row;
                                //         vchip.container.updateChip(vchip);
                                //         vchip.container.redraw();

                                //     }
                                // );

                            }
                        }

                    }
                });
            }
        }


        var velement = document.getElementById('jpcbbp-board-panel');
        var erd = elementResizeDetectorMaker();
        erd.listenTo(velement, function(element) {
            // $scope.filterDataByType(null);
            // if (theBoard){
            //     //theBoard.autoResizeStage();
            //     //theBoard.redraw();
            // }
            // //showBoardJPCB();

            if (!isBoardLoaded) {
                isBoardLoaded = true;
                showBoardJPCB();
                // minuteTick();
                // untuk menampilkan timeline
                // $timeout( function(){
                //     minuteTick();
                // }, 1000);
            }
            vboard = JpcbBpFactory.getBoard(boardName);
            if (vboard) {
                vboard.autoResizeStage();
                vboard.redraw();
            }
        });


        $scope.ShowWO = function() {
            $scope.showChipInfo(false);
            $scope.JpcbBpData.screen_step = 3;
            $scope.JpcbBpData.showProdBoard = false;

            //=============Binding Data WO=====================
            $scope.onShowDetail($scope.DataChip, 'view');
        }

        $scope.BackFromWO = function() {
            $scope.showChipInfo(true);
            $scope.JpcbBpData.screen_step = 2;
            $scope.JpcbBpData.showProdBoard = true;
        }


        $scope.SplitChip = function(job) {
            var plotUrl = '/api/as/Boards/BP/SplitChipJPCBBP';

            console.log('apa ini', job);
            console.log('chip nih boii', $scope.currentChip)
            var boardNameX = 'jpcb-bp-board'
            var xDateStart = JpcbBpFactory.columnToTime(boardNameX, $scope.currentChip.col);
            var xDateEnd = JpcbBpFactory.columnToTime(boardNameX, $scope.currentChip.col + $scope.currentChip.width);

            var getTimeAwal = angular.copy(xDateStart.getTime())
            var getTimeAkhir = angular.copy(xDateEnd.getTime())



            var vDateStart = $filter('date')(xDateStart, 'yyyy-MM-dd HH:mm:ss');
            var vDateEnd = $filter('date')(xDateEnd, 'yyyy-MM-dd HH:mm:ss');
            var SplitTime = new Date();
            var vSplitTime = $filter('date')(SplitTime, 'yyyy-MM-dd HH:mm:ss');
            console.log('xdatestart', xDateStart);

            var getTimeNow = angular.copy(SplitTime.getTime())

            if (getTimeNow >= getTimeAkhir){
                bsAlert.alert({
                    title: "Split harus di dalam range chip. Silahkan Extend dahulu jika chip sudah terlewati",
                    text: "",
                    type: "warning",
                    showCancelButton: false
                },function() {

                })
                return false
            } else if (getTimeNow <= getTimeAwal) {
                bsAlert.alert({
                    title: "Split harus di dalam range chip.",
                    text: "",
                    type: "warning",
                    showCancelButton: false
                },function() {

                })
                return false
            }

            var vobj = {
                "DateStart": vDateStart,
                "DateFinish": vDateEnd,
                "SplitTime": vSplitTime,
                "StallId": $scope.currentChip.stallId,
                "JobId": $scope.currentChip.jobId,
                "ChipType": $scope.currentChip.chipType,
                "GroupId": $scope.currentChip.ChipGroupId,
                "JobTaskBpId": $scope.currentChip.jobtaskbpid
            };
            console.log("vobj", vobj);


            $http.put(plotUrl, [vobj]).then(
                function(res) {
                    console.log('Res SPlit', res);
                    showBoardJPCB();
                    $scope.showChipInfo(false);
                }
            );

        }






















        //===full from wo history gr===
        $scope.$on('$viewContentLoaded', function() {
            // $scope.loading = true;
            $scope.gridData = [];
            $scope.loading = false;
            $scope.getAllOPL();
            $scope.getVehicleMnT();
            $scope.OutletBP();
            $scope.getAddressList();
            $scope.getEmployeeId();
            // ===========
            $scope.enableType = true;
            $scope.enableColor = true;
            $scope.CheckApprove();
            $scope.filter.ShowData = 1;
            $scope.Param = 3;
            Idle.unwatch();
            console.log('==== value of idle ==', Idle)
        });
        $scope.$on('$destroy', function(){
            Idle.watch();
          });

        $scope.boardName = 'asbGrWOlist';
        $scope.EmployeeId = null;
        $scope.currUser = CurrentUser.user();
        $scope.getEmployeeId = function() {
            WoHistoryGR.getEmployeeId($scope.currUser.UserName).then(function(resu) {
                console.log('resu getemployeeid', resu.data);
                $scope.EmployeeId = resu.data[0].UserId;
            });
        };
        console.log('currentuser >>', CurrentUser.user());
        // $scope.currUser.RoleId = 1234;
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = {};
        $scope.mData.US = [];
        $scope.mData.Signature = null;
        var tmpPrediagnose = {}; //Model
        $scope.mDataDetail = [];
        $scope.gridOriginal = [];
        $scope.gridDeleted = [];
        $scope.mDataCustomer = [];
        $scope.tmpAdditionalTaskId = null;
        $scope.IsAppointment = 0;
        $scope.dEstimate = false;
        $scope.mData.OutsideCleaness = 0;
        $scope.mData.CompletenessVehicle = 0;
        $scope.mData.InsideCleaness = 0;
        $scope.mData.UsedPart = 0;
        $scope.mData.DriverCarpetPosition = 0;
        $scope.diskonData = [];
        $scope.tmpDataOPL = [];
        $scope.tmpDataOPLFinal = [];
        $scope.isTAM = 1;
        // $scope.diskonData = [{
        //     MasterId: -1,
        //     Name: "Tidak Ada Diskon",
        // }, {
        //     MasterId: 0,
        //     Name: "Custom Diskon"
        // }]
        $scope.diskonData = [{
            MasterId: -1,
            Name: "Tidak Ada Diskon",
        }, {
            MasterId: 0,
            Name: "Custom Diskon"
        }]
        $scope.isCustomTask = false;
        $scope.isCustomPart = false;
        // $scope.mDataCrm = null;
        $scope.filter = {};
        $scope.xRole = {
            selected: []
        };
        $scope.JobIdforCloseWO = null;
        $scope.formApi = {};
        $scope.IsWash = {};
        $scope.isWaiting = {};
        $scope.UsedPart = {};
        $scope.PermissionPartChange = {};
        $scope.mDataDM = [];
        $scope.mDataCrm = {};
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        $scope.FlatRateAvailable = false;
        $scope.PriceAvailable = false;
        $scope.isCustomDiskon = false;
        $scope.JobRequest = [];
        $scope.formApi = {};
        $scope.JobComplaint = [];
        var currentDate = new Date();
        $scope.statusWO = true;
        $scope.minDateASB = new Date(currentDate);
        // $scope.customActionButtonSettingsDetail = [{

        //         actionType: 'back', //Use 'Back Action' of bsForm
        //         title: 'Kembali',
        //         //icon: 'fa fa-fw fa-edit',
        //     }
        //     ,
        //     {

        //         actionType: 'back', //Use 'Back Action' of bsForm
        //         title: 'Balik Bos',
        //         //icon: 'fa fa-fw fa-edit',
        //     }

        // ];
        // added by sss on 2018-02-22
        $scope.actButtonCaption = {
                cancel: 'Kembali'
            }
            //
        $scope.filterCounter = false;
        var gridData = [];
        // $scope.filter = [];
        $scope.Param = null;
        $scope.paramDiskon = null;
        $scope.CatWO = null;
        $scope.dataParam = [{
            Id: 3,
            Name: 'No. Polisi',
            width: 50
        }, {
            Id: 2,
            Name: 'No. WO',
            width: 150
        }, {
            Id: 1,
            Name: 'Tanggal',
            width: 50
        }];
        $scope.dataDiskon = [{
            Id: 1,
            Name: 'Data Tasks',
            width: 50
        }, {
            Id: 2,
            Name: 'Data Parts',
            width: 50
        }];
        var gridTemp = [];
        $scope.listApi = {};
        $scope.listApiJob = {};
        $scope.listApiOpl = {};
        $scope.listApiAddress = {};
        $scope.listRequestSelectedRows = [];
        $scope.listComplaintSelectedRows = [];
        $scope.listJoblistSelectedRows = [];
        $scope.modalMode = 'new';
        $scope.totalWork = 0;
        $scope.totalWorkDiscounted = 0;
        $scope.totalMaterialDiscounted = 0;
        $scope.totalDp = 0;
        $scope.totalDpDefault = 0;
        $scope.totalMaterial = 0;
        $scope.sumWorkOpl = 0;
        $scope.sumWorkOplDiscounted = 0;
        $scope.totalAll = 0;
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModelOpl = {};
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.isReject = false;
        $scope.isRejectedWarranty = false;
        $scope.asb = {
            currentDate: new Date(),
            showMode: 'main',
            nopol: "D 1234 ABC",
            tipe: "Avanza"
        }
        $scope.asb.startDate = new Date();
        $scope.asb.endDate = new Date();
        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
        var Parts = [];
        $scope.ComplaintCategory = [{
                ComplaintCatg: 'Body'
            },
            {
                ComplaintCatg: 'Body Electrical'
            },
            {
                ComplaintCatg: 'Brake'
            },
            {
                ComplaintCatg: 'Drive Train'
            },
            {
                ComplaintCatg: 'Engine'
            },
            {
                ComplaintCatg: 'Heater System & AC'
            },
            {
                ComplaintCatg: 'Restraint'
            },
            {
                ComplaintCatg: 'Steering'
            },
            {
                ComplaintCatg: 'Suspension and Axle'
            },
            {
                ComplaintCatg: 'Transmission'
            },
        ];
        $scope.buttonSettingModal = {
            save: {
                text: 'Service Explanation'
            }
        };
        // $scope.conditionData = [{ Id: 1, Name: "Ok" }, { Id: 2, Name: "Not Ok" }];
        $scope.conditionData = [{
            Id: 0,
            Name: "Not Ok"
        }, {
            Id: 1,
            Name: "Ok"
        }];
        $scope.typeMData = [{
            Id: 1,
            Name: 1
        }, {
            Id: 2,
            Name: 2
        }, {
            Id: 3,
            Name: 3
        }];
        $scope.JobRequest = [];
        $scope.oplData = [];
        $scope.JobComplaint = [];
        $scope.listView = {
            new: {
                enable: false
            },
            view: {
                enable: false
            },
            delete: {
                enable: false
            },
            select: {
                enable: false
            },
            selectall: {
                enable: false
            },
            edit: {
                enable: false
            },
        }
        $scope.listBilling = {
            new: {
                enable: false
            },
            view: {
                enable: true
            },
            delete: {
                enable: false
            },
            select: {
                enable: false
            },
            selectall: {
                enable: false
            },
            edit: {
                enable: true
            },
        }
        $scope.listControl = {
            new: {
                enable: true
            },
            view: {
                enable: true
            },
            delete: {
                enable: true
            },
            select: {
                enable: true
            },
            selectall: {
                enable: true
            },
            edit: {
                enable: true
            },
        }
        $scope.listCustomer = {
            new: {
                enable: false
            },
            view: {
                enable: false
            },
            delete: {
                enable: false
            },
            select: {
                enable: false
            },
            selectall: {
                enable: false
            },
            edit: {
                enable: false
            },
        }
        $scope.buttonSummaryData = {
            new: {
                enable: false
            },
            view: {
                enable: false
            },
            delete: {
                enable: false
            },
            select: {
                enable: false
            },
            selectall: {
                enable: false
            },
            edit: {
                enable: false
            },
        };
        $scope.paymentTypeData = [{
            Id: 1,
            Name: 'Tunai'
        }, {
            Id: 2,
            Name: 'Kredit'
        }, {
            Id: 3,
            Name: 'PKS'
        }];

        var dateFilter2 = 'date:"dd/MM/yyyy"';
        var approvalForDP = [{
                name: 'id',
                field: 'JobId',
                width: '7%',
                visible: false
            },
            {
                name: 'Vehicid',
                field: 'VehicleId',
                width: '8%',
                visible: false
            },
            {
                name: 'Tanggal',
                field: 'JobDate',
                width: '10%',
                cellFilter: dateFilter2
            },
            {
                name: 'No Wo',
                field: 'WoNo',
                width: '20%',
                displayName: 'No. WO'
            },
            {
                name: 'No Polisi',
                field: 'LicensePlate',
                width: '10%',
                displayName: 'No. Polisi'
            },
            {
                name: 'Customer',
                field: 'NamaKons',
                width: '20%',
                displayName: 'Pelanggan'
            },
            {
                name: 'SA',
                field: 'namasa',
                width: '10%',
                displayName: 'SA'
            },
            {
                name: 'Status',
                field: 'StatusDescriptionAm',
                width: '15%'
            }
        ];
        var approvalForPrint = [{
                name: 'Tanggal',
                field: 'LastModifiedDate',
                cellFilter: dateFilter2,
                width: '10%'
            }, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            {
                name: 'Permintaan',
                field: 'RequestorRoleId',
                width: '20%'
            },
            {
                name: 'No Polisi',
                field: 'LicensePlate',
                width: '10%',
                displayName: 'No. Polisi'
            },
            {
                name: 'VIN',
                field: 'VIN',
                width: '10%',
                displayName: 'VIN'
            },
            {
                name: 'Type',
                field: 'VIN',
                width: '20%',
                displayName: 'Type'
            }
        ];
        var approvalForDiskon = [{
                name: 'Tanggal',
                field: 'RequestDate',
                cellFilter: dateFilter2,
                width: '10%'
            }, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            {
                name: 'Permintaan',
                field: 'RequestorRoleId'
            },
            {
                name: 'Nama Part',
                field: 'JobPart.PartsName'
            },
            {
                name: 'Harga Normal',
                field: 'NormalPrice'
            },
            {
                name: 'Diskon',
                field: 'Discount'
            },
            {
                name: 'Harga Diskon',
                field: 'DiscountedPrice'
            },
        ];
        var approvalForJobReduction = [{
                name: 'Tanggal',
                field: 'LastModifiedDate',
                cellFilter: dateFilter2,
                width: '10%'
            }, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            {
                name: 'Nama Pekerjaan',
                field: 'JobTask.TaskName'
            },
            {
                name: 'No Polisi',
                field: 'Job.PoliceNumber',
                displayName: 'No. Polisi'
            },
            {
                name: 'No WO',
                field: 'Job.WoNo',
                displayName: 'No. WO'
            },
            {
                name: 'Request Date',
                field: 'RequestDate'
            }
        ];
        var showDataKabeng = [{
                Id: 1,
                Name: "Approval untuk Pengurangan DP"
            },
            {
                Id: 2,
                Name: "Approval untuk Print Service History"
            },
            {
                Id: 3,
                Name: "Approval untuk Diskon"
            },
            {
                Id: 4,
                Name: "Approval untuk Pengurangan Job"
            }
        ];
        var showData = [{
                Id: 1,
                Name: "Data Default SA"
            },
            {
                Id: 2,
                Name: "Data Semua SA"
            }
        ];
        $scope.option = showDataKabeng;
        $scope.option2 = showData;
        $scope.listCustomerIdx = $scope.listView;
        $scope.ButtonList = $scope.listControl;
        $scope.isRelease = false;
        $scope.isBilling = false;
        $scope.isKabeng = false;
        $scope.listButtonSettings = {
            new: {
                enable: true,
                icon: "fa fa-fw fa-car"
            }
        };
        $scope.listButtonSettingsOPL = {
            new: {
                enable: true,
                icon: "fa fa-fw fa-car"
            },
            delete: {
                enable: false
            }
        };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                console.log("customButton", row);
            }
        };
        $scope.vm = null;
        $scope.show_modal = {
            show: false,
            showApproval: false
        };
        $scope.show_modal = {
            showApprovalPrint: false,
            showApprovalDp: false,
            showApprovalDiskon: false,
            showApprovalJobReduction: false
        }
        $scope.modalModeAproval = 'new';
        $scope.modalMode = 'view';
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.noResults = true;
        $scope.disPng = true;
        $scope.disPnj = true;
        $scope.hiddenUserList = true;
        $scope.CVehicUId = 0;
        $scope.getCustIdBySearch = false;
        $scope.disVehic = true;
        $scope.hideVehic = true;
        $scope.Mlocation = [];
        $scope.CustomerAddress = [];
        $scope.mode = 'view';
        $scope.modeV = 'view';
        $scope.materialCategory = [{
            MaterialTypeId: 1,
            Name: "Spare Parts"
        }, {
            MaterialTypeId: 2,
            Name: "Bahan"
        }];
        $scope.VehicGas = [{
            VehicleGasTypeId: 1,
            VehicleGasTypeName: 'Bensin'
        }, {
            VehicleGasTypeId: 2,
            VehicleGasTypeName: 'Diesel'
        }, {
            VehicleGasTypeId: 3,
            VehicleGasTypeName: 'Hybrid'
        }];
        var dateFormat = 'dd/MM/yyyy';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateOptionsWithHour = {
            startingDay: 1,
            format: 'dd/MM/yyyy HH:mm:ss'
        };
        $scope.editenable = true;
        var dateFormat = 'dd/MM/yyyy';
        $scope.dateFollowupOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        var minDate = new Date();
        minDate.setDate(minDate.getDate() + 1);
        var minDate2 = new Date();
        // minDate2.setDate(minDate2.getDate() - 1);
        $scope.dateOptionsOPL = {
            startingDay: 1,
            format: dateFormat,
            minDate: minDate2
        }
        var dataCounter;
        $scope.minimal = minDate;
        $scope.dateFollowupOptions.minDate = $scope.minDate;
        $scope.user = CurrentUser.user();
        $scope.slider = {
            value: 0,
            options: {
                floor: 0,
                ceil: 100,
                showSelectionBar: true,
                translate: function(value) {
                    return value + '%';
                }
            }
        };

        $scope.refreshSlider = function() {
            $timeout(function() {
                $scope.$broadcast('rzSliderForceRender');
            });
        };
        $scope.select = function(item, param) {
            $scope.showw = item;
            $scope.grid.data = [];
            if (param == 1) {
                if (item == 1) {
                    // test = beforeBilling;
                    $scope.grid.columnDefs = approvalForDP;
                    $scope.changingFilterButton();
                    $scope.showButton1 = true;
                } else if (item == 2) {
                    // test = afterBilling;
                    $scope.grid.columnDefs = approvalForPrint;
                    $scope.changingFilterButton();
                    $scope.showButton1 = false;
                } else if (item == 3) {
                    // test = afterBilling;
                    $scope.grid.columnDefs = approvalForDiskon;
                    $scope.changingFilterButton();
                    $scope.showButton1 = false;
                } else if (item == 4) {
                    $scope.grid.columnDefs = approvalForJobReduction;
                    $scope.changingFilterButton();
                    $scope.showButton1 = false;
                } else {
                    $scope.grid.columnDefs = [];
                }
            } else {
                $scope.filter.NoPolisi = null;
                $scope.filter.KodeWO = null;
                $scope.filter.DateFrom = null;
                $scope.filter.DateTo = null;
                $scope.grid.columnDefs = approvalForDP;
            }
            console.log("inisiisi", item);
            // // =======Adding Custom Filter==============
            // $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
            // // $scope.gridApiAppointment = {};
            // var x = -1;
            // for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
            //     if ($scope.grid.columnDefs[i].visible == undefined && $scope.grid.columnDefs[i].name !== 'Action') {
            //         x++;
            //         $scope.gridCols.push($scope.grid.columnDefs[i]);
            //         $scope.gridCols[x].idx = i;
            //     }
            //     console.log("$scope.gridCols", $scope.gridCols);
            //     console.log("$scope.grid.columnDefs[i]", $scope.grid.columnDefs[i]);
            // }
            // $scope.filterBtnLabel = $scope.gridCols[0].name;
            //
        };
        $scope.changingFilterButton = function() {
                var tmpGridCols = [];
                var x = -1;
                for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
                    if ($scope.grid.columnDefs[i].visible == undefined) {
                        x++;
                        tmpGridCols.push($scope.grid.columnDefs[i]);
                        tmpGridCols[x].idx = i;
                    }
                }
                $scope.formApi.changeDropdown(tmpGridCols);
            }
            // ====================Added For BOARD======
        var showBoardAsbGr = function(item) {
            console.log("item item", item);
            var vtime
            var tmphour
            var tmpMinute
            if (item !== undefined && item !== null) {
                vtime = item;
                tmphour = item.getHours();
                tmpMinute = item.getMinutes();
            } else {
                vtime = new Date();
                tmphour = vtime.getHours();
                tmpMinute = vtime.getMinutes();
            }

            vtime.setHours(tmphour);
            vtime.setMinutes(tmpMinute);
            vtime.setSeconds(0);
            $scope.currentItem = {
                mode: 'main',
                currentDate: $scope.asb.startDate,
                nopol: $scope.mData.PoliceNumber,
                tipe: $scope.mData.ModelType,
                durasi: $scope.tmpActRate,
                stallId: $scope.stallAsb,
                startTime: vtime,
                visible: 1,
                jobId: $scope.mData.JobId,
                preDiagnose: tmpPrediagnose,
                asbStatus: $scope.asb,
                longStart: $scope.startlongDay,
                longEnd:$scope.endlongDay,
                team: $scope.JpcbBpData.currentGroup
                /*stallId :1,
                startDate :,
                endDate:,
                durasi:,*/
            }
            console.log('=============Showboard==============')
            console.log("actRe", $scope.tmpActRate);
            console.log("harusnya tanggal", $scope.asb.startDate);
            console.log("ini vtime", vtime);
            console.log("$scope.stallAsb", $scope.stallAsb);
            console.log("$scope.currentDate", $scope.currentDate);
            console.log("$scope.mData.JobId", $scope.mData.JobId);
            console.log('=============/Showboard/==============')
            $timeout(function() {
                AsbGrFactory.showBoard({
                    boardName: $scope.boardName,
                    container: 'plottingStall',
                    currentChip: $scope.currentItem,
                    onPlot: getDateEstimasi
                });

            }, 100);

        }
        $scope.searchNoPol = function(param) {
            if (param === null) {
                return false;
            } else {
                param = param.toUpperCase();
            }
            var vchips = [];
            var vidx = 0;
            // _.map($scope.BpWoList,function(val){
            //     console.log("param  == val.PoliceNumber",param,val.nopol);
            //     if(param == val.nopol){
            //         vchips.push(val);
            //         val.col = vidx;
            //         vidx = vidx+1;
            //     }
            // });
            for (var i = 0; i < $scope.BpWoList.length; i++) {
                // for(key in $scope.BpWoList[i]) {
                if ($scope.BpWoList[i]['nopol'].indexOf(param) != -1) {
                    vchips.push($scope.BpWoList[i]);
                    $scope.BpWoList[i].col = vidx;
                    vidx = vidx + 1;
                }
                // }
            }
            var tmpVchips = _.uniq(vchips, 'jobId');
            console.log("ini vchipsnya", tmpVchips);
            showBoard(tmpVchips);
        }
        $scope.estimateAsb = function() {
            if (gridTemp.length > 0) {
                var totalW = 0;
                for (var i = 0; i < gridTemp.length; i++) {
                    if (gridTemp[i].ActualRate !== undefined) {
                        totalW += gridTemp[i].ActualRate;
                    }
                }
                $scope.visibleAsb = 1;
                $scope.tmpActRate = totalW;
                $scope.mData.EstimateHour = totalW;
            } else {
                $scope.tmpActRate = 1;
                $scope.visibleAsb = 0;
                $scope.mData.EstimateHour = 1;
            }
            $scope.checkAsb = true;
            $scope.asb.startDate = "";
            currentDate = new Date();
            // currentDate.setDate(currentDate.getDate() + 1);
            $scope.minDateASB = new Date(currentDate);
            // $scope.currentDate = new Date(currentDate);
            // $scope.asb.startDate = new Date(currentDate);
            console.log('appointment date', $scope.mData.AppointmentDate);
            if ($scope.mData.PlanDateStart !== undefined && $scope.mData.PlanDateStart !== null) {
                // var tmpDate = new Date($scope.mData.AppointmentDate);
                // tmpDate.setDate(tmpDate.getDate() + 1);
                // tmpDate.setHours(0, 0, 0);
                // var tmpMonth = "";
                // var tmpTime = $scope.mData.AppointmentTime;
                // if (tmpDate.getMonth() < 10) {
                //     tmpMonth = "0" + (tmpDate.getMonth() + 1);
                // } else {
                //     tmpMonth = tmpDate.getMonth();
                // }
                // // $scope.asb.startDate = $scope.mData.AppointmentDate;
                // var ini = tmpDate.getFullYear() + "/" + tmpMonth + "/" + tmpDate.getDate() + " " + tmpTime;
                // var msec = Date.parse(tmpDate.getFullYear(), "/", tmpDate.getMonth(), "/", tmpDate.getDate());
                // var d = new Date(ini);
                // $scope.asb.startDate = d;
                // $scope.currentDate = d;
                // console.log("dateBro", d, msec);
                // console.log("date yg diinginkan", ini);
                console.log("Estimate  ASB $scope.mData.AppointmentDate", $scope.mData.AppointmentDate);
                var tmpDate = $scope.mData.PlanDateStart;
                var tmpTime = $scope.mData.AppointmentTime;
                var finalDate
                var yyyy = tmpDate.getFullYear().toString();
                var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = tmpDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpTime;
                var d = new Date(finalDate);
                $scope.asb.startDate = d;
                $scope.currentDate = d;
            } else {
                $scope.asb.startDate = new Date(currentDate);
                $scope.currentDate = new Date(currentDate);
                $scope.mData.AppointmentDate = new Date(currentDate);
            }
            var timeToDateStart = $scope.asb.startDate;
            var timeToDateFinish = $scope.asb.startDate;
            var timePlanStart = $scope.mData.PlanStart;
            var timePlanFinish = $scope.mData.PlanFinish;
            if (($scope.mData.PlanStart !== null && $scope.mData.PlanStart !== undefined) && $scope.mData.PlanFinish !== null && $scope.mData.PlanFinish !== undefined) {
                timePlanStart = timePlanStart.split(":");
                timePlanFinish = timePlanFinish.split(":");
                timeToDateStart = new Date(timeToDateStart.setHours(timePlanStart[0], timePlanStart[1], timePlanStart[2]));
                timeToDateFinish = new Date(timeToDateFinish.setHours(timePlanFinish[0], timePlanFinish[1], timePlanFinish[2]));
            } else {
                timeToDateStart = $scope.mData.PlanStart;
                timeToDateFinish = $scope.mData.PlanFinish;
            }
            // ====================================
            $scope.asb.JobStallId = $scope.mData.StallId;
            $scope.asb.JobStartTime = timeToDateStart;
            $scope.asb.JobEndTime = timeToDateFinish;
            if (tmpPrediagnose.PrediagStallId !== undefined) {
                $scope.asb.PrediagStallId = tmpPrediagnose.PrediagStallId;
            } else {
                $scope.asb.PrediagStallId = $scope.mData.PrediagStallId
            }
            $scope.asb.preDiagnose = tmpPrediagnose;
            if (tmpPrediagnose.ScheduledTime !== undefined && tmpPrediagnose.ScheduledTime !== null) {
                $scope.asb.PrediagStartTime = tmpPrediagnose.ScheduledTime;
            } else {
                $scope.asb.PrediagStartTime = $scope.mData.PrediagScheduledTime;
            }

            // $scope.asb.startDate = $scope.mData.AppointmentDate;
            $scope.mData.FullDate = $scope.asb.startDate;
            $scope.stallAsb = $scope.mData.StallId;
            console.log("curDate", currentDate);
            console.log("$scope.asb", $scope.asb);
            console.log("$scope.asb.startDate", $scope.asb.startDate, tmpPrediagnose);
            showBoardAsbGr($scope.mData.FullDate);

        };
        $scope.ApproveData = {
            VIN: null,
            LicensePlate: null,
            ApprovalCategoryId: 40,
            ApproverId: null,
            RequesterId: null,
            ApproverRoleId: null,
            RequestorRoleId: null,
            StatusApprovalId: null,
            CategoryServices: null,
            RequestReason: null,
            RequestDate: null,
            RejectReason: null,
            ApproveRejectDate: null
        };
        $scope.DataSelect = {};
        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
<a href="#" ng-hide="row.entity.Status == 18 || grid.appScope.currUser.RoleId == 1128" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
<a href="#" ng-hide="row.entity.Status == 18 || grid.appScope.currUser.RoleId == 1128 || (row.entity.Status >= 8 && row.entity.Status <= 10)" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
<a href="#" ng-click="grid.appScope.$parent.actPrintWo(row.entity)" uib-tooltip="Print WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
</div>';
        $scope.CheckApprove = function() {
            if ($scope.currUser.RoleId == 1128) {

                // CAppPrintServiceHistory.getPrintHistoryRequest(1).then(function(result) {
                //   console.log("getPrintHistoryRequest=>",result.data.Result);
                //   $scope.grid.data = result.data.Result;

                //   // console.log("getPrintHistoryRequest=>",result.data);
                // });
                $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
        <a href="#" ng-hide="true" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-hide="true" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-click="grid.appScope.$parent.actPrintWo(row.entity)" uib-tooltip="Print WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        </div>';
                $scope.isKabeng = true;
            } else {
                $scope.isKabeng = false;
                $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
        <a href="#" ng-hide="row.entity.Status == 18 " ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-hide="row.entity.Status == 18 || (row.entity.Status >= 8 && row.entity.Status <= 10 )" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-click="grid.appScope.$parent.actPrintWo(row.entity)" uib-tooltip="Print WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        </div>';
            }

        };
        $scope.actPrintWo = function(data) {
            console.log('JobId cetakWO', data);
            // var data = $scope.mData;
            $scope.printWoHistoryGr = 'as/PrintReceptionGrWO/' + data.JobId + '/' + $scope.user.OrgId + '/1';
            WoHistoryGR.postCountingPrint(data.JobId).then(function(resu) {
                console.log('resu postCountingPrint', resu.data);
            });
            $scope.cetakan($scope.printWoHistoryGr);
        }
        $scope.cetakan = function(data) {
            var pdfFile = null;
            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], {
                    type: 'application/pdf'
                });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };
        $scope.doSaveApprovalJobReduction = function(item) {
            console.log("doSaveApprovalJobReduction itemsave", item);
            WoHistoryGR.updateApprovalJobReduction(item).then(function(resu) {
                bsNotify.show({
                    title: "Approval",
                    content: "Job Reduction Has Been Successfully Approved.",
                    type: 'success'
                });
                $scope.show_modal = {
                    show: false
                };
                $scope.getData();
            });

        };
        $scope.doSaveApprovalDp = function(item) {
            console.log("itemsave", item);
            WoHistoryGR.updateApprovalDp(item).then(
                function(res) {
                    bsNotify.show({
                        title: "Approval",
                        content: "Sales Order has been successfully approved.",
                        type: 'success'
                    });
                    $scope.show_modal = {
                        show: false
                    };
                    $scope.getData();
                },
                function(err) {

                }
            );

        };
        $scope.doSaveApprovalDiskon = function(item) {
            console.log("itemsave", item);
            // for task
            if ($scope.paramDiskon == 1) {
                WoHistoryGR.updateApprovalDiskonTasks(item).then(
                    function(res) {
                        bsNotify.show({
                            title: "Approval",
                            content: "Discount for Task has been successfully approved.",
                            type: 'success'
                        });
                        $scope.show_modal = {
                            show: false
                        };
                        $scope.getData();
                    },
                    function(err) {

                    }
                );
            } else {
                WoHistoryGR.updateApprovalDiskonParts(item).then(
                    function(res) {
                        bsNotify.show({
                            title: "Approval",
                            content: "Discount for Parts has been successfully approved.",
                            type: 'success'
                        });
                        $scope.show_modal = {
                            show: false
                        };
                        $scope.getData();
                    },
                    function(err) {

                    }
                );
            }

        }
        $scope.doBack = function() {

        }
        $scope.bulkApprove = function(item) {
            console.log("itemmmmmmmm bulkApprove", item);
            var tmpRow = {};
            tmpRow = angular.copy(item[0]);
            if ($scope.filter.ShowDataKabeng == 2) {
                console.log("select Approve==>", $scope.selectedRows);
                var tmpData = {
                    OutletId: tmpRow.OutletId,
                    TransactionId: tmpRow.TransactionId,
                    VIN: tmpRow.VIN,
                    LicensePlate: tmpRow.LicensePlate,
                    StatusApprovalId: tmpRow.StatusApprovalId
                }
                CAppPrintServiceHistory.updatePrintHistoryRequest(tmpData).then(
                    function(res) {
                        console.log("sukses start transfer==>", res);
                        $scope.getData();
                    }
                );
            } else if ($scope.filter.ShowDataKabeng == 3) {
                $scope.show_modal.showApprovalDiskon = true;
                $scope.isReject = false;
                if (tmpRow.StatusApprovalId == 3) {
                    tmpRow.NewStatusApprovalId = 1;
                    $scope.modal_model = angular.copy(tmpRow);
                } else if (tmpRow.StatusApprovalId == 2) {
                    bsAlert.warning("Request Sudah di Reject", "");
                } else {
                    bsAlert.warning("Request Sudah di Setujui", "");
                }
            } else if ($scope.filter.ShowDataKabeng == 4) {
                $scope.show_modal.showApprovalJobReduction = true;
                $scope.isReject = false;
                tmpRow.NewStatusApprovalId = 1;
                $scope.modal_model = angular.copy(tmpRow);
            } else {
                $scope.show_modal.showApprovalDp = true;
                $scope.isReject = false;
                if (tmpRow.StatusApprovalId == 3) {
                    tmpRow.NewStatusApprovalId = 1;
                    $scope.modal_model = angular.copy(tmpRow);
                } else if (tmpRow.StatusApprovalId == 2) {
                    bsAlert.warning("Request Sudah di Reject", "");
                } else {
                    bsAlert.warning("Request Sudah di Setujui", "");
                }
            }
        };
        $scope.bulkReject = function(item) {
            console.log("itemmmmmmm bulkReject", item);
            var tmpRow = {};
            tmpRow = angular.copy(item[0]);
            if ($scope.filter.ShowDataKabeng == 2) {
                console.log("select Reject==>", $scope.selectedRows);
                // console.log("api=>",$scope.formApi);
                $scope.show_modal.showApprovalPrint = true;
                CAppPrintServiceHistory.updatePrintHistoryRequest(1).then(
                    function(res) {
                        console.log("sukses start transfer==>", res);
                        $scope.getData();
                    }
                );
            } else if ($scope.filter.ShowDataKabeng == 3) {
                $scope.show_modal.showApprovalDiskon = true;
                $scope.isReject = true;
                if (tmpRow.StatusApprovalId == 3) {
                    tmpRow.NewStatusApprovalId = 2;
                    $scope.modal_model = angular.copy(tmpRow);
                } else if (tmpRow.StatusApprovalId == 2) {
                    bsAlert.warning("Request Sudah di Reject", "");
                } else {
                    bsAlert.warning("Request Sudah di Setujui", "");
                }
            } else if ($scope.filter.ShowDataKabeng == 4) {
                $scope.show_modal.showApprovalJobReduction = true;
                $scope.isReject = true;
                tmpRow.NewStatusApprovalId = 2;
                $scope.modal_model = angular.copy(tmpRow);
            } else {
                $scope.show_modal.showApprovalDp = true;
                $scope.isReject = true;
                if (tmpRow.StatusApprovalId == 3) {
                    tmpRow.NewStatusApprovalId = 2;
                    $scope.modal_model = angular.copy(tmpRow);
                } else if (tmpRow.StatusApprovalId == 2) {
                    bsAlert.warning("Request Sudah di Reject", "");
                } else {
                    bsAlert.warning("Request Sudah di Setujui", "");
                }
            }
        };
        var dateTimee = '';
        var RequestDateTime = '';
        // $scope.DataSelect={};
        $scope.bulkPrint = function() {
            var currentdate = new Date();
            dateTimee = currentdate.getFullYear() + "-" +
                (currentdate.getMonth() + 1) + "-" +
                currentdate.getDate() +
                " @ " +
                currentdate.getHours() + ":" +
                currentdate.getMinutes() + ":" +
                currentdate.getSeconds();
            RequestDateTime = currentdate.getFullYear() + "-" +
                (currentdate.getMonth() + 1) + "-" +
                currentdate.getDate();

            console.log("select Cetak==>", $scope.selectedRows);
            $scope.DataSelect = angular.copy($scope.selectedRows[0]);
            $scope.DataSelect.dateTimee = dateTimee;
            $scope.DataSelect.RequestDateTime = RequestDateTime;
            $scope.DataSelect.RequestReason = null;
            console.log("DataSelect before", $scope.DataSelect);

            if ($scope.DataSelect.StatusApproval == 1) {
                bsAlert.alert({
                        title: "Apakah Bapak IAN yakin seyakin yakinnya ?",
                        text: "Ingin Cetak History Kendaraan " + $scope.DataSelect.LicensePlate,
                        type: "warning",
                        showCancelButton: true
                    },
                    function() {
                        // $scope.cancelAppointment(item);
                        // $scope.signature.dataUrl = data.dataUrl;
                        console.log('AAAA');
                        $scope.cetakanService = 'as/PrintReceptionServiceHistory/' + +$scope.DataSelect.VehicleId + '/' + +$scope.DataSelect.OutletId + '/' + +$scope.DataSelect.isGr;
                        // return $q.resolve($scope.cetakanOPL);
                        $scope.cetakan($scope.cetakanService);
                        // $scope.disableReset = true;
                        // console.log('$scope.disableReset A', $scope.disableReset);
                        // $scope.setSignature($scope.signature.dataUrl);


                    },
                    function() {
                        // $scope.signature.dataUrl = EMPTY_IMAGE;
                        // $scope.disableReset = false;
                        console.log('BBBB');
                        // console.log('$scope.disableReset B', $scope.disableReset);
                        // $scope.clear();
                    }
                );

            } else {
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/services/reception/woHistoryGR/dialog_ReqServiceHistory.html\'\"></div>',
                    plain: true,
                    scope: $scope
                });
            }



            // CAppPrintServiceHistory.updatePrintHistoryRequest(1).then(
            //     function(res){
            //         console.log("sukses start transfer==>",res);
            //         $scope.getData();
            //     }
            // );
        }
        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], {
                    type: 'application/pdf'
                });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        $scope.kirimApproval = function(xcd) {
            console.log("====>", xcd);
            console.log("DataSelect==>", $scope.DataSelect);

            $scope.ApproveData = {
                VIN: $scope.DataSelect.VIN,
                LicensePlate: $scope.DataSelect.LicensePlate,
                ApprovalCategoryId: 40,
                RequesterId: null,
                RequestorRoleId: null,
                StatusApprovalId: 1,
                CategoryServices: 1,
                RequestReason: $scope.DataSelect.RequestReason,
                RequestDate: $scope.DataSelect.RequestDateTime
            };
            var cv = [$scope.ApproveData];

            console.log("Approvedata", $scope.ApproveData);
            $scope.ngDialog.close();
            // $scope.show_modal.showApproval=true;
            CAppPrintServiceHistory.insertRequestPrintHistory(cv).then(
                function(res) {
                    console.log("sukses start transfer==>", res);
                    $scope.getData();
                }
            );
        }
        $scope.ApproveData = {};
        $scope.mPrintServiceHistory = [];
        $scope.buttonSettingModalApproval = {
            save: {
                text: 'Reject'
            }
        };
        $scope.onReject = function() {
            console.log("===>", $scope.mPrintServiceHistory);
            $scope.show_modal.showApproval = false;
            bsNotify.show({
                title: "Approval",
                content: "Sales Order has been successfully approved.",
                type: 'success'
            });
        }

        $scope.reloadBoard = function(item) {
            var tmpMinDateASB = new Date();
            tmpMinDateASB.setDate(tmpMinDateASB.getDate() - 1);
            tmpMinDateASB.setHours(0, 0, 0);
            if (item < tmpMinDateASB.getTime()) {
                console.log("ini nih bossss", tmpMinDateASB);
                console.log("item ini nih", item);
            } else {
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                $scope.mData.AppointmentDate = null;
                $scope.mData.AppointmentTime = null;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.TimeTarget = null;
                $scope.mData.PlanDateFinish = null;
                $scope.asb.JobStallId = null;
                $scope.asb.JobStartTime = null;
                $scope.asb.JobEndTime = null;
                console.log("dari ng-change", item);
                console.log("$scope.mData", $scope.mData);
                var vtime = item;
                var tmphour
                var tmpMinute
                if ($scope.mData.FullDate !== undefined) {
                    console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                    var tmpTime = $scope.mData.FullDate;
                    tmphour = tmpTime.getHours();
                    tmpMinute = tmpTime.getMinutes();
                } else {
                    tmphour = 9;
                    tmpMinute = 0;
                }
                vtime.setHours(tmphour);
                vtime.setMinutes(tmpMinute);
                vtime.setSeconds(0);
                $scope.currentItem = {
                    mode: 'main',
                    currentDate: $scope.asb.startDate,
                    nopol: $scope.mData.PoliceNumber,
                    tipe: $scope.mDataDetail.VehicleModelName,
                    // tipe: "",
                    durasi: $scope.tmpActRate,
                    stallId: 0,
                    startTime: vtime,
                    visible: $scope.visibleAsb,
                    jobId: $scope.mData.JobId,
                    preDiagnose: tmpPrediagnose,
                    longStart: $scope.startlongDay,
                    longEnd:$scope.endlongDay,
                    team: $scope.JpcbBpData.currentGroup
                    // JobId: $scope.mData.JobId,
                    /*stallId :1,
                    startDate :,
                    endDate:,
                    durasi:,*/
                }
                console.log("actRe", $scope.tmpActRate);
                console.log("harusnya tanggal", $scope.asb.startDate);
                console.log("ini vtime", vtime);
                console.log("stallId", $scope.currentItem.stallId);
                $timeout(function() {
                    AsbGrFactory.showBoard({
                        boardName: $scope.boardName,
                        container: 'asb_asbgr_viewFollow',
                        currentChip: $scope.currentItem,
                        onPlot: getDateEstimasi
                    });

                }, 100);
            }
        }
        var getDateEstimasi = function(data) {
            console.log('onplot data...', data);
            if ((data.startTime === null) || (data.endTime === null)) {
                // jika startTime/EndTime null berarti belum ada plot, Data tanggal awal/akhir serta Tanggal Appointment harus dikosongkan maka harus dikosongkan
                console.log("startTime gk ada");
                $scope.mData.StallId = 0;
                $scope.mData.FullDate = null;
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                //$scope.mData.AppointmentDate = null;
                delete $scope.mData.AppointmentDate;
                //$scope.mData.AppointmentTime = null;
                delete $scope.mData.AppointmentTime;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.TimeTarget = null;
                $scope.mData.PlanDateFinish = null;
                $scope.mData.StatusPreDiagnose = 0;
                $scope.mData.JobTaksBpId =
                    delete $scope.mData.PrediagScheduledTime;
                delete $scope.mData.PrediagStallId;

                // delete tmpPrediagnose.ScheduledTime;
                // delete tmpPrediagnose.StallId;
            } else {
                console.log("plotting stall");
                var dt = new Date(data.startTime);
                var dtt = new Date(data.endTime);
                console.log("date", dt);
                dt.setSeconds(0);
                dtt.setSeconds(0);
                var timeStart = dt.toTimeString();
                var timeFinish = dtt.toTimeString();
                timeStart = timeStart.split(' ');
                timeFinish = timeFinish.split(' ');
                // ===============================
                var yearFirst = dt.getFullYear();
                var monthFirst = dt.getMonth() + 1;
                var dayFirst = dt.getDate();
                var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                // ===============================
                var yearFinish = dtt.getFullYear();
                var monthFinish = dtt.getMonth() + 1;
                var dayFinish = dtt.getDate();
                var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
                // ===============================
                // if(timeStart[0] !== null && timeFinish[0] !== null){
                $scope.mData.StallId = data.stallId;
                $scope.mData.FullDate = dt;
                $scope.mData.PlanStart = timeStart[0];
                $scope.mData.PlanFinish = timeFinish[0];
                $scope.mData.PlanDateStart = firstDate;
                $scope.mData.AppointmentDate = firstDate;
                $scope.mData.AppointmentTime = timeStart[0];
                $scope.mData.TargetDateAppointment = FinishDate;
                $scope.mData.TimeTarget = timeFinish[0];
                $scope.mData.PlanDateFinish = FinishDate;
                $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                $scope.mData.PrediagStallId = data.PrediagStallId;
                //-------//
                // var start = dataWo.PlanDateStart.toISOString().substring(0, 10);
                // var finish = dataWo.PlanDateFinish.toISOString().substring(0, 10);
                // console.log('start', start);
                // console.log('finish', finish);
                // var ScheduledStart = firstDate + ' ' + timeStart[0];
                // var ScheduledFinish = FinishDate + ' ' + dataWo.PlanFinish;
                // console.log('ScheduledStart', ScheduledStart);
                // console.log('ScheduledStart', ScheduledStart);
                $scope.mData.firstDate = new Date(data.startTime);
                $scope.mData.lastDate = new Date(data.endTime);
                var lastdate = angular.copy($scope.mData.lastDate);
                console.log('mData.firstDate estimateasb', $scope.mData.firstDate);
                console.log('mData.lastDate estimateasb', $scope.mData.lastDate);
                $scope.mData.EstimateDate = lastdate.setHours(lastdate.getHours() + 1);
                // tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                // tmpPrediagnose.StallId = data.PrediagStallId;
            }

            // }

            // }
        };

        // =========================================
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.selectFilter = function(item) {
            console.log('item >>', item);
            if (item != null) {
                $scope.Param = item.Id;
                if ($scope.Param == 1) {
                    $scope.filter.NoPolisi = "";
                    $scope.filter.KodeWO = "";
                } else if ($scope.Param == 2) {
                    $scope.filter.NoPolisi = "";
                    $scope.filter.DateFrom = undefined;
                    $scope.filter.DateTo = undefined;
                } else if ($scope.Param == 3) {
                    $scope.filter.KodeWO = "";
                    $scope.filter.DateFrom = undefined;
                    $scope.filter.DateTo = undefined;
                }
            }

        };
        $scope.selectDiskon = function(item) {
            if (item !== null) {
                $scope.paramDiskon = item.Id;
            }
            $scope.grid.data = [];
        }
        $scope.custTypeId = null;
        $scope.showFieldInst = false;
        $scope.showFieldPersonal = false;
        $scope.enableType = true;
        $scope.enableColor = true;
        $scope.selectedModel = function(item) {
            console.log('selectedModel >>>', item);
            console.log(' $scope.mDataCrm.Vehicle', $scope.mDataCrm.Vehicle);
            $scope.mDataCrm.Vehicle.Model = item;
            console.log("$scope.mDataCrm.Vehicle.Model", $scope.mDataCrm.Vehicle.Model);
            $scope.VehicT = [];
            $scope.mData.VehicleModelName = item.VehicleModelName;
            $scope.mDataCrm.Vehicle.Type = null;
            // var dVehic = item.MVehicleType;
            var dVehic = _.uniq(item.MVehicleType, 'KatashikiCode');
            _.map(dVehic, function(a) {
                a.NewDescription = a.Description + ' - ' + a.KatashikiCode;
                // a.NewDescription = a.Description + ' - ' + a.KatashikiCode + ' - ' + a.SuffixCode;
            });
            console.log('dVehic >>>', dVehic);
            $scope.VehicT = dVehic;
            $scope.enableType = false;
        };
        $scope.selectedType = function(item, param) {
            console.log('selectedType >>>', item, param);
            $scope.mDataCrm.Vehicle.Color = null;
            $scope.mDataCrm.Vehicle.Type = item;
            console.log("$scope.mDataCrm.Vehicle.Type", $scope.mDataCrm.Vehicle.Type);
            $scope.VehicColor = [];
            var arrCol = [];
            console.log("$scope.Color", $scope.Color);
            _.map($scope.Color, function(val) {
                if (val.VehicleTypeId == item.VehicleTypeId) {
                    console.log("val.VehicleTypeId == item.VehicleTypeId", val.VehicleTypeId, item.VehicleTypeId);
                    console.log("val.VehicleTypeId == item.VehicleTypeId", val);
                    arrCol = val.ListColor;
                };
            });
            if (param == 1) {
                _.map(arrCol, function(a) {
                    // if(a.ColorId == item.)
                });
            };
            console.log("$scope.arrCol", arrCol);
            // var objColor = {
            //     ColorId: 9999,
            //     ColorName: "Lainnya",
            //     ColorCode: "OTH"
            // };
            // arrCol.push(objColor);
            $scope.enableColor = false;
            $scope.VehicColor = arrCol;
            console.log('$scope.mDataCrm.Vehicle selectedType >>>', $scope.mDataCrm.Vehicle);
            $scope.mDataCrm.Vehicle.KatashikiCode = item.KatashikiCode;
            AppointmentGrService.getDataTaskListByKatashiki(item.KatashikiCode).then(function(restask) {
                taskList = restask.data.Result;
                var tmpTask = {};
                if (taskList.length > 0) {
                    for (var i = 0; i < taskList.length; i++) {
                        if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                            $scope.PriceAvailable = true;
                        } else {
                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                        }
                        $scope.tmpServiceRate = tmpTask;
                    }
                } else {
                    $scope.tmpServiceRate = null;
                };
            });
        };
        $scope.selectedColor = function(item) {
            console.log('selectedColor >>>', item);
            $scope.mDataCrm.Vehicle.Color = item;
            console.log("$scope.mDataCrm.Vehicle.Color", $scope.mDataCrm.Vehicle.Color);
            $scope.mDataCrm.Vehicle.ColorCode = item.ColorCode;
            if (item.ColorId == 9999) {
                $scope.isCustomColor = true;
            };
            console.log('$scope.mDataCrm >>>', $scope.mDataCrm);
        };
        $scope.onValidateSave = function(data) {
            if ($scope.dataPKSFinal.length == 1) {
                console.log("jumlah total", $scope.totalWork + $scope.totalMaterial);
                var totalAkhir = $scope.totalWork + $scope.totalMaterial;

                WO.updatePKS(policeNuberForPDS, totalAkhir).then(function(res) {
                    var dataUpdatePKS = res.data.Result;
                    console.log("dataupdatePKS", dataUpdatePKS);

                    if (dataUpdatePKS[0].CustomerPKSTop <= 0 || dataUpdatePKS[0].CustomerPKSTop == null) {

                        if (dataUpdatePKS[0].AvailableBalances > totalAkhir || dataUpdatePKS[0].OnPeriode !== 0) {
                            console.log("gak bisa releasewo nih");
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Sisa Saldo PKS Kurang Atau Sudah Lewat Tanggal"
                                    // ,
                                    // content: error.join('<br>'),
                                    // number: error.length
                            });
                        } else {
                            console.log("Bisa Releasewo");

                            // $scope.mData.JobTask = gridTemp;
                            console.log("mData", $scope.mData);
                            var tmpGrid = [];
                            $scope.AppointmentNo = null;
                            tmpGrid = angular.copy($scope.gridWork);
                            var JobTaskId = null;
                            // if (gridTemp.length > 0) {
                            //     for (var i = 0; i < tmpGrid.length; i++) {
                            //         tmpGrid[i].JobParts = [];
                            //         angular.forEach(tmpGrid[i].child, function(v, k) {
                            //             tmpGrid[i].JobParts[k] = v;
                            //             delete tmpGrid[i].JobParts[k].ETA;
                            //             delete tmpGrid[i].JobParts[k].PercentDP;
                            //             delete tmpGrid[i].child;
                            //         })
                            //         delete tmpGrid[i].PaidBy;

                            //     };
                            // };
                            // console.log('gridtemp faisal', gridTemp);
                            _.map(tmpGrid, function(val) {
                                delete val.PaidBy;
                                val.JobId = $scope.mData.JobId;
                                val.JobParts = val.child;
                                _.map(val.child, function(val2) {
                                    delete val2.ETA;
                                    delete val2.Dp;
                                    val2.JobId = $scope.mData.JobId;
                                    val2.Price = val2.RetailPrice;
                                    val2.DPRequest = val2.DownPayment;
                                    if (val2.JobPartsId == undefined || val2.JobPartsId == null) {
                                        val2.JobPartsId = 0;
                                    }
                                    if (val2.PaidById == 2277 || val2.PaidById == 30 || val2.PaidById == 31) {
                                        val2.minimalDp = 0;
                                    }
                                    delete val2.ETA;
                                    delete val2.PaidBy;
                                    // delete val2.Dp;
                                })
                                delete val.child;
                            });
                            if (data.Km !== undefined && data.Km !== null) {
                                var tmpKm = angular.copy(data.Km);
                                var backupKm = angular.copy(data.Km);
                                if (tmpKm.includes(",")) {
                                    tmpKm = tmpKm.split(',');
                                    if (tmpKm.length > 1) {
                                        tmpKm = tmpKm.join('');
                                    } else {
                                        tmpKm = tmpKm[0];
                                    }
                                }
                                tmpKm = parseInt(tmpKm);
                                data.KmNormal = tmpKm;
                            }
                            console.log('gridtemp amik', tmpGrid);
                            if ($scope.copyOplData.length != $scope.oplData.length) {
                                _.map($scope.oplData, function(o) {
                                    if (o.JobId == null || o.JobId === undefined) {
                                        o.JobId = data.JobId;
                                        o.JobTaskOplId = 0;
                                        // o.TargetFinishDate = data.RequiredDate;
                                    }
                                })
                            } else {
                                _.map($scope.oplData, function(o) {
                                    // o.TargetFinishDate = data.RequiredDate;
                                })
                            };
                            data.JobTask = tmpGrid;

                            // =============================
                            $scope.tmpDataOPLDeleted = [];
                            $scope.tmpDataOPLFinal = [];
                            // if($scope.oplData.length > 0){
                            for (var i in $scope.tmpDataOPL) {
                                if ($scope.tmpDataOPL[i].JobTaskOplId !== undefined) {
                                    if ($scope.tmpDataOPL[i].Status !== 4) {
                                        if (_.findIndex($scope.oplData, {
                                                JobTaskOplId: $scope.tmpDataOPL[i].JobTaskOplId
                                            }) < 0) {
                                            $scope.tmpDataOPL[i].Status = 4;
                                            $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                            $scope.tmpDataOPLDeleted.push($scope.tmpDataOPL[i]);
                                        } else {
                                            if ($scope.oplData.length !== $scope.tmpDataOPL.length) {
                                                if ($scope.tmpDataOPL[i].JobTaskOplId == undefined) {
                                                    $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                                } else {
                                                    $scope.tmpDataOPLFinal.push($scope.oplData[i]);
                                                }
                                            } else {
                                                $scope.tmpDataOPLFinal.push($scope.oplData[i]);
                                            }
                                        }
                                    } else {
                                        $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                    }
                                } else {
                                    $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                }
                            }
                            // $scope.updateOPL($scope.tmpDataOPLDeleted);
                            // }
                            console.log("$scope.tmpDataOPLDeleted", $scope.tmpDataOPLDeleted);
                            console.log("$scope.oplData", $scope.oplData);
                            console.log("$scope.tmpDataOPL", $scope.tmpDataOPL);
                            console.log("$scope.tmpDataOPLFinal", $scope.tmpDataOPLFinal);
                            // =============================
                            for (var i in $scope.tmpDataOPLFinal) {
                                if ($scope.tmpDataOPLFinal[i].TargetFinishDate !== undefined) {
                                    $scope.tmpDataOPLFinal[i].TargetFinishDate = $scope.changeFormatDate($scope.tmpDataOPLFinal[i].TargetFinishDate);
                                }
                                if ($scope.tmpDataOPLFinal[i].CreateOplDate !== undefined) {
                                    $scope.tmpDataOPLFinal[i].CreateOplDate = $scope.changeFormatDate($scope.tmpDataOPLFinal[i].CreateOplDate);
                                }
                            }
                            // =============================
                            // data.JobTask = $scope.gridWork;
                            data.WoCategoryId = $scope.CatWO;
                            data.OPL = $scope.tmpDataOPLFinal;
                            var tmpData = angular.copy($scope.mData);
                            var tmpAppointmentDate = tmpData.AppointmentDate;
                            tmpAppointmentDate = new Date(tmpAppointmentDate);
                            var m = tmpAppointmentDate.getMonth() + 1;
                            var day = tmpAppointmentDate.getDate();
                            var year = tmpAppointmentDate.getFullYear();
                            var n = year + "/" + m + "/" + day;
                            data.AppointmentDate = n;
                            console.log('data on save', data);
                            console.log('data on JobList', $scope.gridWork);
                            $scope.CatWO = null;
                            if ($scope.isRejectedWarranty) {
                                $scope.requestApprovalForWarranty(tmpGrid);
                            }
                            $scope.saveApprovalReductionTask(data);
                            $scope.saveApprovalDiscount(data, data.JobId, tmpGrid);

                            // return true
                        }
                    } else {

                        if (dataUpdatePKS[0].OnPeriode == 0) {
                            console.log("gak bisa releasewo nih karena tanggal");

                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Sisa Saldo PKS Kurang Atau Sudah Lewat Tanggal"
                                    // ,
                                    // content: error.join('<br>'),
                                    // number: error.length
                            });
                        } else {
                            console.log("Bisa Releasewo");

                            // $scope.mData.JobTask = gridTemp;
                            // console.log("mData", $scope.mData);
                            var tmpGrid = [];
                            $scope.AppointmentNo = null;
                            tmpGrid = angular.copy($scope.gridWork);
                            var JobTaskId = null;
                            // if (gridTemp.length > 0) {
                            //     for (var i = 0; i < tmpGrid.length; i++) {
                            //         tmpGrid[i].JobParts = [];
                            //         angular.forEach(tmpGrid[i].child, function(v, k) {
                            //             tmpGrid[i].JobParts[k] = v;
                            //             delete tmpGrid[i].JobParts[k].ETA;
                            //             delete tmpGrid[i].JobParts[k].PercentDP;
                            //             delete tmpGrid[i].child;
                            //         })
                            //         delete tmpGrid[i].PaidBy;

                            //     };
                            // };
                            // console.log('gridtemp faisal', gridTemp);

                            _.map(tmpGrid, function(val) {
                                delete val.PaidBy;
                                val.JobId = $scope.mData.JobId;
                                val.JobParts = val.child;
                                _.map(val.child, function(val2) {
                                    delete val2.ETA;
                                    delete val2.Dp;
                                    val2.JobId = $scope.mData.JobId;
                                    val2.Price = val2.RetailPrice;
                                    val2.DPRequest = val2.DownPayment;
                                    if (val2.JobPartsId == undefined || val2.JobPartsId == null) {
                                        val2.JobPartsId = 0;
                                    }
                                    if (val2.PaidById == 2277 || val2.PaidById == 30 || val2.PaidById == 31) {
                                        val2.minimalDp = 0;
                                    }
                                    delete val2.ETA;
                                    delete val2.PaidBy;
                                    // delete val2.Dp;
                                })
                                delete val.child;
                            });
                            if (data.Km !== undefined && data.Km !== null) {
                                var tmpKm = angular.copy(data.Km);
                                var backupKm = angular.copy(data.Km);
                                if (tmpKm.includes(",")) {
                                    tmpKm = tmpKm.split(',');
                                    if (tmpKm.length > 1) {
                                        tmpKm = tmpKm.join('');
                                    } else {
                                        tmpKm = tmpKm[0];
                                    }
                                }
                                tmpKm = parseInt(tmpKm);
                                data.KmNormal = tmpKm;
                            }
                            console.log('gridtemp amik', tmpGrid);
                            if ($scope.copyOplData.length != $scope.oplData.length) {
                                _.map($scope.oplData, function(o) {
                                    if (o.JobId == null || o.JobId === undefined) {
                                        o.JobId = data.JobId;
                                        o.JobTaskOplId = 0;
                                        // o.TargetFinishDate = data.RequiredDate;
                                    }
                                })
                            } else {
                                _.map($scope.oplData, function(o) {
                                    // o.TargetFinishDate = data.RequiredDate;
                                })
                            };
                            $scope.tmpDataOPLDeleted = [];
                            $scope.tmpDataOPLFinal = [];
                            // if($scope.oplData.length > 0){
                            for (var i in $scope.tmpDataOPL) {
                                if ($scope.tmpDataOPL[i].JobTaskOplId !== undefined) {
                                    if ($scope.tmpDataOPL[i].Status !== 4) {
                                        if (_.findIndex($scope.oplData, {
                                                JobTaskOplId: $scope.tmpDataOPL[i].JobTaskOplId
                                            }) < 0) {
                                            $scope.tmpDataOPL[i].Status = 4;
                                            $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                            $scope.tmpDataOPLDeleted.push($scope.tmpDataOPL[i]);
                                        } else {
                                            if ($scope.oplData.length !== $scope.tmpDataOPL.length) {
                                                if ($scope.tmpDataOPL[i].JobTaskOplId == undefined) {
                                                    $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                                } else {
                                                    $scope.tmpDataOPLFinal.push($scope.oplData[i]);
                                                }
                                            } else {
                                                $scope.tmpDataOPLFinal.push($scope.oplData[i]);
                                            }
                                        }
                                    } else {
                                        $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                    }
                                } else {
                                    $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                }
                            }
                            // $scope.updateOPL($scope.tmpDataOPLDeleted);
                            // }
                            console.log("$scope.tmpDataOPLDeleted", $scope.tmpDataOPLDeleted);
                            console.log("$scope.oplData", $scope.oplData);
                            console.log("$scope.tmpDataOPL", $scope.tmpDataOPL);
                            console.log("$scope.tmpDataOPLFinal", $scope.tmpDataOPLFinal);
                            // =============================
                            for (var i in $scope.tmpDataOPLFinal) {
                                if ($scope.tmpDataOPLFinal[i].TargetFinishDate !== undefined) {
                                    $scope.tmpDataOPLFinal[i].TargetFinishDate = $scope.changeFormatDate($scope.tmpDataOPLFinal[i].TargetFinishDate);
                                }
                                if ($scope.tmpDataOPLFinal[i].CreateOplDate !== undefined) {
                                    $scope.tmpDataOPLFinal[i].CreateOplDate = $scope.changeFormatDate($scope.tmpDataOPLFinal[i].CreateOplDate);
                                }
                            }
                            // =======================================
                            data.JobTask = tmpGrid; // data.WoCategoryId = $scope.CatWO;
                            data.OPL = $scope.tmpDataOPLFinal;
                            var tmpData = angular.copy($scope.mData);
                            var tmpAppointmentDate = tmpData.AppointmentDate;
                            tmpAppointmentDate = new Date(tmpAppointmentDate);
                            var m = tmpAppointmentDate.getMonth() + 1;
                            var day = tmpAppointmentDate.getDate();
                            var year = tmpAppointmentDate.getFullYear();
                            var n = year + "/" + m + "/" + day;
                            data.AppointmentDate = n;
                            data.WoCreatedDate = $scope.changeFormatDate(data.WoCreatedDate);
                            console.log('data on save', data);
                            console.log('data on JobList', $scope.gridWork);
                            $scope.CatWO = null;
                            if ($scope.isRejectedWarranty) {
                                $scope.requestApprovalForWarranty(tmpGrid);
                            }
                            $scope.saveApprovalDiscount(data, data.JobId, tmpGrid);
                            return true
                        }
                    }
                });

            } else {
                // $scope.mData.JobTask = gridTemp;
                console.log("mData ====> onValidateSave", $scope.mData);
                console.log("$scope.gridWork ====> onValidateSave", $scope.gridWork);
                var tmpGrid = [];
                $scope.AppointmentNo = null;
                tmpGrid = angular.copy($scope.gridWork);
                var JobTaskId = null;
                if (tmpGrid.length > 0) {
                    for (var i = 0; i < tmpGrid.length; i++) {
                        tmpGrid[i].JobParts = [];
                        angular.forEach(tmpGrid[i].child, function(v, k) {
                            tmpGrid[i].JobParts[k] = v;
                            tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                            tmpGrid[i].JobParts[k].DPRequest = tmpGrid[i].JobParts[k].DownPayment;
                            if (tmpGrid[i].JobParts[k].JobPartsId == undefined || tmpGrid[i].JobParts[k].JobPartsId == null) {
                                tmpGrid[i].JobParts[k].JobPartsId = 0;
                            }
                            if (tmpGrid[i].JobParts[k].PaidById == 2277 || tmpGrid[i].JobParts[k].PaidById == 30 || tmpGrid[i].JobParts[k].PaidById == 31) {
                                tmpGrid[i].JobParts[k].minimalDp = 0;
                            }
                            delete tmpGrid[i].JobParts[k].ETA;
                            delete tmpGrid[i].JobParts[k].PaidBy;
                            // delete tmpGrid[i].JobParts[k].Dp;
                            delete tmpGrid[i].child;
                        });
                        if (($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) && $scope.mData.JobId > 0) {
                            tmpGrid[i].JobId = $scope.mData.JobId;
                        } else {
                            tmpGrid[i].JobId = 0;
                        }
                        delete tmpGrid[i].PaidBy;
                        delete tmpGrid[i].tmpTaskId;

                    }
                };
                console.log('gridtemp faisal', gridTemp);

                // _.map(tmpGrid, function(val) {
                //     delete val.PaidBy;
                //     _.map(val.child, function(val2) {
                //         delete val2.ETA;
                //         delete val2.Dp;
                //     })
                // });
                console.log('gridtemp amik', tmpGrid);
                if ($scope.copyOplData.length != $scope.oplData.length) {
                    _.map($scope.oplData, function(o) {
                        if (o.JobId == null || o.JobId === undefined) {
                            o.JobId = data.JobId;
                            o.JobTaskOplId = 0;
                            // o.TargetFinishDate = data.RequiredDate;
                        }
                    })
                } else {
                    _.map($scope.oplData, function(o) {
                        // o.TargetFinishDate = data.RequiredDate;
                    })
                };
                if (data.Km !== undefined && data.Km !== null) {
                    var tmpKm = angular.copy(data.Km);
                    var backupKm = angular.copy(data.Km);
                    if (tmpKm.includes(",")) {
                        tmpKm = tmpKm.split(',');
                        if (tmpKm.length > 1) {
                            tmpKm = tmpKm.join('');
                        } else {
                            tmpKm = tmpKm[0];
                        }
                    }
                    tmpKm = parseInt(tmpKm);
                    data.KmNormal = tmpKm;
                }
                $scope.tmpDataOPLDeleted = [];
                $scope.tmpDataOPLFinal = [];

                // if($scope.oplData.length > 0){
                for (var i in $scope.tmpDataOPL) {
                    if ($scope.tmpDataOPL[i].JobTaskOplId !== undefined) {
                        if ($scope.tmpDataOPL[i].Status !== 4) {
                            if (_.findIndex($scope.oplData, {
                                    JobTaskOplId: $scope.tmpDataOPL[i].JobTaskOplId
                                }) < 0) {
                                $scope.tmpDataOPL[i].Status = 4;
                                $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                $scope.tmpDataOPLDeleted.push($scope.tmpDataOPL[i]);
                            } else {
                                if ($scope.oplData.length !== $scope.tmpDataOPL.length) {
                                    if ($scope.tmpDataOPL[i].JobTaskOplId == undefined) {
                                        $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                                    } else {
                                        $scope.tmpDataOPLFinal.push($scope.oplData[i]);
                                    }
                                } else {
                                    $scope.tmpDataOPLFinal.push($scope.oplData[i]);
                                }
                            }
                        } else {
                            $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                        }
                    } else {
                        $scope.tmpDataOPLFinal.push($scope.tmpDataOPL[i]);
                    }
                }
                // $scope.updateOPL($scope.tmpDataOPLDeleted);
                // }
                console.log("$scope.tmpDataOPLDeleted", $scope.tmpDataOPLDeleted);
                console.log("$scope.oplData", $scope.oplData);
                console.log("$scope.tmpDataOPL", $scope.tmpDataOPL);
                console.log("$scope.tmpDataOPLFinal", $scope.tmpDataOPLFinal);
                // =============================
                for (var i in $scope.tmpDataOPLFinal) {
                    if ($scope.tmpDataOPLFinal[i].TargetFinishDate !== undefined) {
                        $scope.tmpDataOPLFinal[i].TargetFinishDate = $scope.changeFormatDate($scope.tmpDataOPLFinal[i].TargetFinishDate);
                    }
                    if ($scope.tmpDataOPLFinal[i].CreateOplDate !== undefined) {
                        $scope.tmpDataOPLFinal[i].CreateOplDate = $scope.changeFormatDate($scope.tmpDataOPLFinal[i].CreateOplDate);
                    }
                }
                // ==============================
                data.JobTask = tmpGrid;
                data.WoCategoryId = $scope.CatWO;
                data.OPL = $scope.tmpDataOPLFinal;
                var tmpData = angular.copy($scope.mData);
                var tmpAppointmentDate = tmpData.AppointmentDate;
                tmpAppointmentDate = new Date(tmpAppointmentDate);
                var m = tmpAppointmentDate.getMonth() + 1;
                var day = tmpAppointmentDate.getDate();
                var year = tmpAppointmentDate.getFullYear();
                var n = year + "/" + m + "/" + day;
                data.AppointmentDate = n;
                data.WoCreatedDate = $scope.changeFormatDate(data.WoCreatedDate);
                console.log('data on save', data);
                console.log('data on JobList', $scope.gridWork);
                $scope.CatWO = null;
                if ($scope.isRejectedWarranty) {
                    $scope.requestApprovalForWarranty(tmpGrid);
                }
                $scope.saveApprovalDiscount(data, data.JobId, tmpGrid);
                // return true;
            }
        };
        $scope.requestApprovalForWarranty = function(data) {
            console.log("data", data);
            var tmpDataApprovalTECO = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].catName == "TWC" || data[i].catName == "PWC") {
                    tmpDataApprovalTECO.push({
                        NoClaim: "57812",
                        JobTaskId: data[i].JobTaskId,
                        ApprovalCategoryId: 43,
                        ApproverRoleId: 1128,
                        RequestorRoleId: $scope.user.RoleId,
                        StatusApprovalId: 3,
                        CategoryServices: data[i].JobTypeId, // kategori pekerjaan
                        RequestReason: "Request Ulang",
                        RequestDate: $scope.changeFormatDate(new Date()), // tanggal sekarang
                        // "RejectReason": null
                        // ,
                        // "ApproveRejectDate": null
                    });
                }
            }
            console.log("tmpDataApprovalTECO", tmpDataApprovalTECO);
            WO.createApprovalWarranty(tmpDataApprovalTECO).then(
                function(res) {
                    console.log("sukses start transfer==>", res);
                }
            );
        };
        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = tmpAppointmentDate.getDate().toString();
            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        };
        $scope.updateOPL = function(data) {
            var dataFinal = [];
            // console.log("$scope.updateOPL",data);
            for (var i in data) {
                dataFinal.push(data[i].JobTaskOplId);
            }
            console.log("$scope.updateOPL", dataFinal);
            oplList.updateStatusCancelOPLMore(dataFinal).then(function(res) {
                return true
            });
        }

        $scope.onBeforeCancel = function(data) {
            console.log('data on onBeforeCancel', data);
            $scope.CatWO = null;
            console.log('data $scope.CatWO', $scope.CatWO);

        };
        $scope.selectedGas = function(item) {
            console.log('VehicGas >>>', item);
            // $scope.VehicGasTypeId = item.VehicleGasTypeId;
            $scope.mDataCrm.Vehicle.GasTypeId = item.VehicleGasTypeId;
            $scope.enableType = false;
        };
        $scope.getVehicleMnT = function() {
            return $q.resolve(
                WOBP.getVehicleMList().then(function(resuM) {
                    // console.log('getVehicleMList', resuM.data.Result);
                    $scope.modelData = resuM.data.Result;
                    $scope.VehicM = resuM.data.Result;
                }),
                WO.getVehicleTypeColor().then(function(resu) {
                    // console.log('getVehicleTypeColor', resu.data.Result);
                    $scope.Color = resu.data.Result;
                })
            );
            // WOBP.getVehicleTList().then(function(resuT) {
            //     console.log('getVehicleTList', resuT.data.Result);
            //     $scope.VehicT = resuT.data.Result;
            // })
        };
        $scope.saveApprovalDiscount = function(data, jobId, tmpGrid) {
            var tmpDataTask = angular.copy(tmpGrid);
            console.log("tmpDataTask=========>tmpDataTask", tmpDataTask);
            // console.log("tmpDataPart=========>tmpDataPart",tmpDataPart);

            WO.postApprovalDiscountTask(jobId, tmpDataTask).then(function(res) {
                console.log("postApprovalDiscountTask", res.data);
                return true
            });
            WoHistoryGR.update(data).then(function(res) {

            });
            $scope.formApi.setMode('grid');

        };
        var policeNuberForPDS = {};
        $scope.saveApprovalReductionTask = function(data) {
            WoHistoryGR.postJobReduction(data.JobId, $scope.gridDeleted).then(function(res) {
                console.log("saveApprovalReductionTask", res.data);
                return true
            });
        }
        $scope.onShowDetail = function(row, mode) {
            console.log('row onShowDetail >>>', row, mode);
            $scope.isOpenWac = false;
            $scope.accordionWac.isOpen = false;
            $scope.clickWACcount = 0;
            // $scope.CatWO = null;
            // if (mode == 'edit') {
            $scope.mData = {};
            $scope.mDataDetail = {};
            $scope.mDataCrm = {};
            $scope.mDataCustomer = {};
            $scope.mDataCrm.Vehicle = {};
            $scope.IsWash = {};
            $scope.isWaiting = {};
            $scope.UsedPart = {};
            $scope.PermissionPartChange = {};
            // $scope.mDataCrm.Vehicle.LicensePlate = null;
            $scope.gridDeleted = [];
            gridTemp = [];
            $scope.JobComplaint = [];
            $scope.JobRequest = [];
            $scope.gridWork = [];
            $scope.gridWork = gridTemp;
            $scope.totalWork = 0;
            $scope.totalWorkDiscounted = 0;
            $scope.totalMaterialDiscounted = 0;
            $scope.totalMaterial = 0;
            $scope.tmpServiceRate = null;
            $scope.sumWorkOpl = 0;
            $scope.sumWorkOplDiscounted = 0;
            $scope.totalOplDiscount = 0;
            $scope.totalWorkOpl = 0;
            WoHistoryGR.getWObyJobId(row.JobId).then(function(resu) {
                console.log('resu <<<>>>', resu.data.Result[0]);
                var data = resu.data.Result[0];
                policeNuberForPDS = data.PoliceNumber;
                console.log("data.AppointmentDate", new Date(data.AppointmentDate));
                console.log("data.FixedDeliveryTime1", new Date(data.FixedDeliveryTime1));
                console.log("data.FixedDeliveryTime2", new Date(data.FixedDeliveryTime2));
                console.log("data.PlanDateStart", new Date(data.PlanDateStart));
                console.log("data.PlanDateFinish", new Date(data.PlanDateFinish));
                WO.getDataPKS(policeNuberForPDS).then(function(res) {
                    // tmpPrediagnose = res.data;
                    $scope.dataPKSFinal = res.data.Result;
                    console.log("$scope.dataPKSFinal", $scope.dataPKSFinal.length);
                    if ($scope.dataPKSFinal.length == 0) {
                        $scope.show_modal.show = false;
                    } else {
                        $scope.show_modal.show = true;
                    }
                    if ($scope.dataPKSFinal.length == 0) {
                        AppointmentGrService.getWoCategory().then(function(res) {
                            var data = res.data.Result;

                            var evens = _.remove(data, function(n) {
                                return n.Name !== 'PKS';
                            });
                            $scope.woCategory = angular.copy(evens);
                            console.log("ini a bukan PKS", evens);
                        });
                    } else {
                        // FLagPKS = 0;
                        // $scope.woCategory = [];
                        //     $scope.woCategory = $scope.woCategory;
                        AppointmentGrService.getWoCategory().then(function(res) {
                            $scope.woCategory = res.data.Result;
                            // $scope.dataCatWO = angular.copy($scope.woCategory);
                            console.log("ini a  PKS", $scope.woCategory);

                            // console.log('$scope.woCategory', $scope.woCategory);
                        });
                    }
                    // console.log("resss Prediagnosenya bosque", res.data);
                });

                console.log("police Number For PDS", policeNuberForPDS);
                $scope.setCPPK(data, 1);
                $scope.JobIdforCloseWO = data.JobId;
                $scope.mData = data;
                // console.log("$scope.UsedPart = resu.data.Result[0].UsedPart;", resu.data.Result[0].UsedPart);
                // $scope.UsedPart = resu.data.Result[0].UsedPart;
                // $scope.IsWaiting = resu.data.Result[0].IsWaiting;
                // $scope.IsWash = resu.data.Result[0].IsWash;
                // $scope.PermissionPartChange = resu.data.Result[0].PermissionPartChange;
                $scope.mData.PaymentType = resu.data.Result[0].PaymentMethod;
                $scope.mData.Km = String($scope.mData.Km);
                $scope.mData.Km = $scope.mData.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                $scope.mData.CodeTransaction = resu.data.Result[0].invoiceType;
                console.log("$scope.mData", $scope.mData);

                $scope.dataForBslist(data);
                $scope.dataForPrediagnose(data);
                $scope.editData(data);
                $scope.estimateAsb();
                $scope.dataForOPL(data);
                // $scope.dataForWAC(data);
                var start = $scope.changeFormatDate(data.PlanDateStart);
                var finish = $scope.changeFormatDate(data.PlanDateFinish);
                console.log("data.PlanDateStart.toISOString().substring(0, 10);", start);
                console.log("data.PlanDateStart.toISOString().substring(0, 10);", finish);
                var ScheduledStart = start + ' ' + data.PlanStart;
                var ScheduledFinish = finish + ' ' + data.PlanFinish;
                $scope.mData.firstDate = new Date(ScheduledStart);
                // $scope.mData.lastDate = new Date(ScheduledFinish);
                var lastdate = new Date(ScheduledFinish);
                // console.log('mData.firstDate', $scope.mData.firstDate);
                // console.log('mData.lastDate', $scope.mData.lastDate);
                $scope.mData.EstimateDate = lastdate.setHours(lastdate.getHours() + 1);
                $scope.mData.lastDate = $scope.mData.EstimateDate;
                $scope.mData.AdjusmentDate = angular.copy($scope.mData.EstimateDate);
                // console.log('mData.EstimateDate', $scope.mData.EstimateDate);
                // $scope.mData.Estimate = resu.data.Result[0].FixedDeliveryTime1;
                // $scope.mData.Adjusment = resu.data.Result[0].FixedDeliveryTime2;

                if ($scope.mData.Adjusment !== null) {
                    $scope.mData.customerRequest = 2;
                } else {
                    $scope.mData.customerRequest = 0;
                }

                var TotalLamaPekerjaan = 0;
                var TotalPartsDP = 0;

                for (var i = 0; i < resu.data.Result.length; i++) {
                    for (var a = 0; a < resu.data.Result[i].JobTask.length; a++) {
                        TotalLamaPekerjaan += resu.data.Result[i].JobTask[a].ActualRate;
                        for (var l = 0; l < resu.data.Result[i].JobTask[a].JobParts.length; l++) {
                            TotalPartsDP += resu.data.Result[i].JobTask[a].JobParts[l].DownPayment;
                        }
                    }
                }

                $scope.mData.LamaPekerjaan = TotalLamaPekerjaan;
                $scope.mData.ActualRate = data.LamaPekerjaan;
                console.log("$scope.mData.ActualRate", $scope.mData.ActualRate);
                $scope.mData.TotalDP = TotalPartsDP;
                var objSearch = {};
                objSearch.flag = 1;
                objSearch.filterValue = data.PoliceNumber;
                objSearch.TID = null;
                $scope.getCustomerVehicleList(objSearch);

            });
            if (row.VehicleId !== null) {
                AppointmentGrService.getDataVehicle(row.VehicleId).then(function(ress) {
                    console.log("dataDetail", ress.data.Result);
                    var dataDetail = ress.data.Result;
                    $scope.mDataDetail = dataDetail;
                    $scope.mDataCustomer.owner = $scope.mDataDetail[0].CustomerName;
                    $scope.mDataCustomer.address = $scope.mDataDetail[0].Address;
                    $scope.mDataCustomer.model = $scope.mDataDetail[0].VehicleModelName;
                    // if (dataDetail.length > 0) {
                    //     for (var i in dataDetail) {
                    //         dataDetail[i].VillageData = {};
                    //         dataDetail[i].DistrictData = {};
                    //         dataDetail[i].CityRegencyData = {};
                    //         dataDetail[i].ProvinceData = {};
                    //         dataDetail[i].VillageData.VillageName = dataDetail[i].VillageName;
                    //         dataDetail[i].VillageData.PostalCode = dataDetail[i].PostalCode;
                    //         dataDetail[i].DistrictData.DistrictName = dataDetail[i].DistrictName;
                    //         dataDetail[i].CityRegencyData.CityRegencyName = dataDetail[i].CityRegencyName;
                    //         dataDetail[i].ProvinceData.ProvinceName = dataDetail[i].ProvinceName;
                    //         // $scope.CustomerAddress = dataDetail[i];
                    //     }
                    //     $scope.CustomerAddress = dataDetail;
                    // }
                    WoHistoryGR.getDiscountMGroupCustomer(dataDetail[0]).then(function(res) {
                        console.log("getDiscountMGroupCustomer", res.data.Result);
                    });
                    console.log("data pelanggan nih", $scope.mDataDetail, $scope.CustomerAddress);
                });
            };
            if (row.Status == 4) {
                $scope.isRelease = false;
                $scope.PriceAvailable = false;
                $scope.statusWO = true;
                $scope.isBilling = false;
            } else if (row.Status > 4 && row.Status < 8) {
                $scope.isRelease = true;
                $scope.FlatRateAvailable = false;
                $scope.PriceAvailable = true
                $scope.statusWO = true;
                $scope.isBilling = false;
            } else if (row.Status > 10 && row.Status < 13) {
                $scope.statusWO = true;
                $scope.isRelease = true;
                $scope.FlatRateAvailable = true;
                $scope.PriceAvailable = false;
                $scope.isBilling = false;
            } else if (row.Status == 14) {
                $scope.isRelease = true;
                $scope.FlatRateAvailable = true;
                $scope.PriceAvailable = false;
                $scope.statusWO = true;
                $scope.isBilling = false;
                WO.checkApprovalWarranty($scope.mData).then(function(res) {
                    console.log("checkApprovalWarranty", res.data);
                    $scope.isRejectedWarranty = res.data;
                });
            } else if (row.Status == 15) {
                $scope.isRelease = true;
                $scope.FlatRateAvailable = true;
                $scope.PriceAvailable = false;
                $scope.statusWO = true;
                $scope.isBilling = false;
            } else if (row.Status == 11) {
                $scope.statusWO = false;
                $scope.isBilling = true;
                $scope.isRelease = true;
                $scope.FlatRateAvailable = false;
                $scope.PriceAvailable = true
                $scope.ButtonList = $scope.listBilling;
            };
            $('#CPPKInfo').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            $('#CustIndex').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            $('#RiwayatServis').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            $('#OrderPekerjaan').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            $('#OPL').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            $('#WAC').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            $('#JobSuggest').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            $('#Summary').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            // }
        };

        // $scope.PDS = function(WoCatId){
        //     pdsFlag = WoCatId;
        //     if(WoCatId == 2208){
        //         console.log("its PDS");
        //         console.log("ini PoliceNumber yang dikirim", policeNuberForPDS);

        //         WO.getDataPKS(policeNuberForPDS).then(function(res) {
        //             // tmpPrediagnose = res.data;
        //             $scope.dataPKS = res.data.Result;
        //             // console.log("resss Prediagnosenya bosque", res.data);
        //         });
        //         $scope.show_modal.show = true;
        //     }else{
        //         console.log("not PDS");
        //     }
        // }

        $scope.getCustomerVehicleList = function(data) {
            console.log('getCustomerVehicleList before >>>', data);
            WOBP.getCustomerVehicleList(data).then(function(resu) {
                var data = resu.data.Result[0];
                if (data == undefined) {
                    console.log("loooolllllll");
                } else {
                    console.log('getCustomerVehicleList >>>', data);
                    var customer = data.CustomerList.CustomerListPersonal[0];
                    var institusi = data.CustomerList.CustomerListInstitution[0];
                    var address = data.CustomerList.CustomerAddress;
                    var objC_Personal = {};
                    var objC_Institusi = {};
                    var checkAddres = function() {
                        if (address.length != 0) {
                            var tempAddressData = [];
                            _.map(address, function(val) {
                                console.log('adddersss <><>', val);
                                if (val.StatusCode == 1) {
                                    var obj = {};
                                    obj.CustomerId = val.CustomerId;
                                    obj.CustomerAddressId = val.CustomerAddressId;
                                    obj.Address = val.Address;
                                    obj.AddressCategoryId = val.AddressCategoryId;
                                    obj.AddressCategory = val.AddressCategory;
                                    obj.MainAddress = val.MainAddress;
                                    obj.Phone1 = val.Phone1;
                                    obj.Phone2 = val.Phone2;
                                    obj.RT = val.RT;
                                    obj.RW = val.RW;
                                    obj.StatusCode = val.StatusCode;
                                    if (val.MVillage != null || val.MVillage !== undefined) {
                                        obj.ProvinceId = val.MVillage.MDistrict.MCityRegency.MProvince.ProvinceId;
                                        obj.CityRegencyId = val.MVillage.MDistrict.MCityRegency.CityRegencyId;
                                        obj.VillageId = val.MVillage.VillageId;
                                        obj.DistrictId = val.MVillage.MDistrict.DistrictId;
                                        obj.CityRegencyData = val.MVillage.MDistrict.MCityRegency;
                                        obj.DistrictData = val.MVillage.MDistrict;
                                        obj.ProvinceData = val.MVillage.MDistrict.MCityRegency.MProvince;
                                        obj.VillageData = val.MVillage;
                                        obj.AddressCategory = val.AddressCategory;
                                    };
                                    // obj.CustomerId =
                                    tempAddressData.push(obj);
                                    console.log('tempAddressData <><>', obj);
                                };
                            });
                            return tempAddressData;
                        } else {
                            return [];
                        }
                    };

                    $scope.custTypeId = data.CustomerList.CustomerTypeId;
                    $scope.customerId = data.CustomerList.CustomerId;
                    $scope.CustomerVehicleId = data.CustomerVehicleId;
                    $scope.vehicleId = data.VehicleId;
                    if (data.CustomerList.CustomerTypeId == 3) {
                        $scope.showFieldPersonal = true;
                        $scope.showFieldInst = false;
                        $scope.personalId = customer.PersonalId;
                        objC_Personal.BirthDate = customer.BirthDate;
                        objC_Personal.KTPKITAS = customer.KTPKITAS;
                        objC_Personal.HandPhone1 = customer.Handphone1;
                        objC_Personal.HandPhone2 = customer.Handphone2;
                        objC_Personal.Name = customer.CustomerName;
                        objC_Personal.FrontTitle = customer.FrontTitle;
                        objC_Personal.EndTitle = customer.EndTitle;
                        objC_Personal.ToyotaId = data.CustomerList.ToyotaId;
                        objC_Personal.Npwp = data.CustomerList.Npwp;
                        objC_Personal.CustomerId = data.CustomerList.CustomerId;
                        objC_Personal.FleetId = data.CustomerList.FleetId;
                        objC_Personal.AFCOIdFlag = data.CustomerList.Afco;
                        objC_Personal.ToyotaIDFlag = null;
                        // objC_Personal.Address = checkAddres();
                        objC_Personal.CustomerVehicleId = data.CustomerVehicleId;
                        $scope.CustomerAddress = checkAddres();
                        $scope.mDataCrm.Customer = objC_Personal;
                        objC_Institusi = {};
                    } else {
                        $scope.showFieldInst = true;
                        $scope.showFieldPersonal = false;
                        $scope.institusiId = institusi.InstitutionId;
                        objC_Institusi.ToyotaId = data.CustomerList.ToyotaId;
                        objC_Institusi.CustomerTypeId = data.CustomerList.CustomerTypeId;
                        objC_Institusi.Name = institusi.Name;
                        objC_Institusi.PICName = institusi.PICName;
                        objC_Institusi.PICDepartment = institusi.PICDepartment;
                        objC_Institusi.PICHp = institusi.PICHp;
                        objC_Institusi.PICKTPKITAS = institusi.PICKTPKITAS;
                        objC_Institusi.PICEmail = institusi.PICEmail;
                        // objC_Institusi.Address = checkAddres();
                        objC_Institusi.CustomerVehicleId = data.CustomerVehicleId;
                        objC_Institusi.AFCOIdFlag = data.CustomerList.Afco;
                        $scope.CustomerAddress = checkAddres();
                        $scope.mDataCrm.Customer = objC_Institusi;
                        objC_Personal = {};
                    };

                    //Vehicle
                    if (data.VehicleList) {
                        var vehicle = data.VehicleList;
                        console.log("Vehicle Listnya", vehicle);
                        var objV = {};
                        objV.VehicleId = vehicle.VehicleId;
                        objV.isNonTAM = vehicle.isNonTAM;
                        objV.LicensePlate = vehicle.LicensePlate;
                        objV.GasTypeId = vehicle.GasTypeId;
                        objV.VIN = vehicle.VIN;
                        objV.ModelCode = vehicle.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;
                        objV.AssemblyYear = vehicle.AssemblyYear;
                        // objV.Color = vehicle.MVehicleTypeColor.MColor.ColorName;
                        // objV.ColorCode = vehicle.MVehicleTypeColor.MColor.ColorCode;
                        objV.KatashikiCode = vehicle.MVehicleTypeColor.MVehicleType.KatashikiCode;
                        objV.DECDate = vehicle.DECDate;
                        objV.EngineNo = vehicle.EngineNo;
                        objV.SPK = vehicle.SPK ? vehicle.SPK : 'No Data Available YET';
                        objV.Insurance = vehicle.Insurance ? vehicle.Insurance : 'No Data Available YET';
                        objV.STNKName = vehicle.STNKName;
                        objV.STNKAddress = vehicle.STNKAddress;
                        objV.STNKDate = vehicle.STNKDate;
                        objV.Model = vehicle.MVehicleTypeColor.MVehicleType.MVehicleModel;
                        // objV.Type = vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId;
                        var testingType = null;
                        var testingColor = null;
                        _.map($scope.VehicM, function(val) {
                            if (val.VehicleModelId == objV.Model.VehicleModelId) {
                                $scope.selectedModel(val);
                                _.map(val.MVehicleType, function(val2) {
                                    console.log('val2', val2);
                                    if (val2.VehicleTypeId == vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId) {
                                        testingType = val2;
                                        objV.Type = testingType;
                                        // objV.Type = testingType.VehicleTypeId;
                                        $scope.selectedType(val2, 1);
                                        _.map($scope.VehicColor, function(val3) {
                                            console.log('val3', val3);
                                            if (val3.ColorId == vehicle.MVehicleTypeColor.ColorId) {
                                                testingColor = val3;
                                                objV.Color = testingColor;
                                                // objV.Color = testingColor.ColorId;
                                                objV.ColorCode = testingColor.ColorCode;
                                                $scope.selectedColor(testingColor);
                                            };
                                        });
                                    };
                                });
                            };
                        });
                        // WO.getVehicleUser(vehicle.VehicleId).then(function(resu) {
                        //     console.log('list pengguna >>', resu.data.Result);
                        // });
                        // $scope.getUserList(vehicle.VehicleId, 4);
                        $scope.getUserList(data.CustomerVehicleId, 4);

                        console.log("Penting!!!!!!!!!!!!!!!!!!!!!!!!!!!Uncch jarang di cocol", objV);
                        $scope.mDataCrm.Vehicle = objV;
                        objV = {};
                        var objUS = {};
                        var arr1 = [];
                        var arr2 = [];
                        var arrusVUid = [];
                        var tempArr = [];
                        if (data.VehicleUser.length > 1) {
                            _.map(data.CustomerVehicle.VehicleUser, function(e, k) {
                                arr1.push(e.Name);
                                arr2.push(e.Phone);
                                arrusVUid.push(e.VehicleUserId);
                                tempArr.push({});
                            });
                            $scope.mData.US.name = arr1;
                            $scope.mData.US.phoneNumber1 = arr2;
                            $scope.mData.US.VehicleUserId = arrusVUid;
                            $scope.userForm = tempArr;
                        };
                    } else {
                        $scope.mDataCrm.Vehicle = {};
                        $scope.VehicT = [];
                        $scope.enableType = true;
                    };
                }
            });
        };

        $scope.editData = function(data) {
            $scope.JobRequest = data.JobRequest;
            // $scope.oplData = data.JobTaskOpl;
            $scope.JobComplaint = data.JobComplaint;
            $scope.CatWO = data.WoCategoryId;
            $scope.copyOplData = angular.copy($scope.oplData);
            console.log('resu <<<000>>>', $scope.CatWO, data.WoCategoryId);
        };

        $scope.dataForOPL = function(data) {
            $scope.tmpDataOPL = [];
            $scope.tmpDataOPLFinal = [];
            $scope.oplData = [];
            $scope.tmpDataOPLDeleted = [];
            _.map(data.JobTaskOpl, function(val, idx) {
                console.log('data.JobTaskOpl vall >>', val, idx);
                // val.RequiredDate = val.TargetFinishDate;
                // val.OplName = val.OPL;
                if (val.Status == 0) {
                    val.xStatus = "Open";
                } else if (val.Status == 1) {
                    val.xStatus = "On Progress";
                } else if (val.Status == 2) {
                    val.xStatus = "Completed";
                } else if (val.Status == 3) {
                    val.xStatus = "Billing";
                } else if (val.Status == 4) {
                    val.xStatus = "Cancel";
                    console.log('data masuk ke 4 3', val);
                };
                val.index = "$$" + idx;
                val.OPLName = val.OPL.OPLWorkName;
                val.VendorName = val.OPL.Vendor.Name;
                // val.
            });
            for (var i in data.JobTaskOpl) {
                if (data.JobTaskOpl[i].Status !== 4) {
                    $scope.oplData.push(data.JobTaskOpl[i]);
                }
            }
            // $scope.oplData = data.JobTaskOpl;
            $scope.tmpDataOPL = angular.copy(data.JobTaskOpl);
            $scope.tmpDataOPLFinal = [];

            if ($scope.oplData.length > 0) {

                var totalWOPL = 0;
                for (var i = 0; i < $scope.oplData.length; i++) {
                    if ($scope.oplData[i].Price !== undefined) {
                        totalWOPL += $scope.oplData[i].Price;
                    }
                };
                $scope.sumWorkOpl = totalWOPL;

                var totalDiscountedPrice = 0;
                for (var i = 0; i < $scope.oplData.length; i++) {
                    if ($scope.oplData[i].DiscountedPriceOPL !== undefined) {
                        totalDiscountedPrice += $scope.oplData[i].DiscountedPriceOPL;
                        console.log("ga masuk else opl");
                    } else {
                        totalDiscountedPrice = 0;
                        console.log("masuk else opl");
                    }
                    console.log("$scope.oplData[i].DiscountedPriceOPL", $scope.oplData[i].DiscountedPriceOPL);
                };
                console.log("totalDiscountedPrice", totalDiscountedPrice);
                $scope.sumWorkOplDiscounted = totalDiscountedPrice;
                console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
                console.log('$scope.oplData >>>', $scope.oplData);
            }

        };
        var typeParam = "";
        $scope.getData = function() {
            // console.log("$scope.formGridApi",$scope.formGridApi);
            console.log("filter", $scope.filter, $scope.Param, $scope.paramDiskon);
            if ($scope.isKabeng == true) {
                if ($scope.filter.ShowDataKabeng == 1 && $scope.Param == 3 && ($scope.filter.NoPolisi == null)) {
                    console.log('masuk sini 3');
                    $scope.showAlert();
                } else if ($scope.filter.ShowDataKabeng == 1 && $scope.Param == 2 && ($scope.filter.KodeWO == null)) {
                    console.log('masuk sini 2');
                    $scope.showAlert();
                } else if ($scope.filter.ShowDataKabeng == 1 && $scope.Param == 1 && ($scope.filter.DateFrom == undefined || $scope.filter.DateTo == undefined)) {
                    console.log('masuk sini 1');
                    $scope.showAlert();
                } else {
                    console.log("$scope.formGridApi.grid", $scope.formGridApi.grid);
                    if ($scope.filter.ShowDataKabeng == 2) {
                        CAppPrintServiceHistory.getPrintHistoryRequest(1).then(function(result) {
                            console.log("getPrintHistoryRequest=>", result.data.Result);
                            $scope.grid.data = result.data.Result;
                            var tmpGridCols = [];
                            var x = -1;
                            for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
                                if ($scope.grid.columnDefs[i].visible == undefined) {
                                    x++;
                                    tmpGridCols.push($scope.grid.columnDefs[i]);
                                    tmpGridCols[x].idx = i;
                                }
                            }
                            $scope.formApi.changeDropdown(tmpGridCols);
                            // console.log("getPrintHistoryRequest=>",result.data);
                        });
                    } else if ($scope.filter.ShowDataKabeng == 3 && $scope.filter.tipeApprovalDiskon == 1) {
                        WoHistoryGR.getAllApprovalJobTask().then(function(res) {
                            console.log("getAllApprovalJobTask=>", res.data.Result);
                            $scope.grid.data = res.data.Result;
                            var tmpGridCols = [];
                            var x = -1;
                            for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
                                if ($scope.grid.columnDefs[i].visible == undefined) {
                                    x++;
                                    tmpGridCols.push($scope.grid.columnDefs[i]);
                                    tmpGridCols[x].idx = i;
                                }
                            }
                            $scope.formApi.changeDropdown(tmpGridCols);
                        });
                    } else if ($scope.filter.ShowDataKabeng == 3 && $scope.filter.tipeApprovalDiskon == 2) {
                        WoHistoryGR.getAllApprovalJobPart().then(function(res) {
                            console.log("getAllApprovalJobPart=>", res.data.Result);
                            $scope.grid.data = res.data.Result;
                            var tmpGridCols = [];
                            var x = -1;
                            for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
                                if ($scope.grid.columnDefs[i].visible == undefined) {
                                    x++;
                                    tmpGridCols.push($scope.grid.columnDefs[i]);
                                    tmpGridCols[x].idx = i;
                                }
                            }
                            $scope.formApi.changeDropdown(tmpGridCols);
                        });
                    } else if ($scope.filter.ShowDataKabeng == 4) {
                        WoHistoryGR.getAllApprovalJobReductionGR().then(function(resu) {
                            console.log("getAllApprovalJobReductionGR=>", resu.data.Result);
                            $scope.grid.data = resu.data.Result;
                            var tmpGridCols = [];
                            var x = -1;
                            for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
                                if ($scope.grid.columnDefs[i].visible == undefined) {
                                    x++;
                                    tmpGridCols.push($scope.grid.columnDefs[i]);
                                    tmpGridCols[x].idx = i;
                                }
                            }
                            $scope.formApi.changeDropdown(tmpGridCols);
                        });
                    } else {
                        $scope.setData($scope.Param, $scope.filter);
                    }
                };
            } else {
                if ($scope.Param == 3 && ($scope.filter.NoPolisi == null)) {
                    console.log('masuk sini 3');
                    $scope.showAlert();
                } else if ($scope.Param == 2 && ($scope.filter.KodeWO == null)) {
                    console.log('masuk sini 2');
                    $scope.showAlert();
                } else if ($scope.Param == 1 && ($scope.filter.DateFrom == undefined || $scope.filter.DateTo == undefined)) {
                    console.log('masuk sini 1');
                    $scope.showAlert();
                } else {
                    $scope.setData($scope.Param, $scope.filter);
                }
            }
        };

        $scope.doCloseWO = function() {
            var id = $scope.JobIdforCloseWO;
            console.log('id close id', id);
            WoHistoryGR.closeWO(id).then(function(resu) {
                console.log('resu close wo', resu.data);
                $scope.formApi.setMode('grid');
                $scope.getData();
            })
        };

        // ==================================
        $scope.showAlert = function() {
            $scope.grid.data = [];
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Mohon Input Filter",
            });
        };

        $scope.setData = function(type, data, param) {
            console.log('type.data.param', type, data, param);
            var endDate = null;
            var noWO = null;
            var noPolice = null;
            var typeData = null;
            var datafilter = angular.copy(data);
            if (type == 1) {
                var dateFrom = new Date(datafilter.DateFrom);
                dateFrom.setSeconds(0);
                var yearFrom = dateFrom.getFullYear();
                var monthFrom = ('0' + (dateFrom.getMonth() + 1)).slice(-2);
                var dayFrom = ('0' + dateFrom.getDate()).slice(-2);
                startDate = yearFrom + '-' + monthFrom + '-' + dayFrom;

                var dateTo = new Date(datafilter.DateTo);
                dateTo.setSeconds(0);
                var yearTo = dateTo.getFullYear();
                var monthTo = ('0' + (dateTo.getMonth() + 1)).slice(-2);
                var dayTo = ('0' + dateTo.getDate()).slice(-2);
                endDate = yearTo + '-' + monthTo + '-' + dayTo;

                noWO = '-';
                noPolice = '-';
            } else if (type == 2) {
                startDate = '2017-01-01';
                endDate = '2099-01-31';
                noWO = datafilter.KodeWO;
                noPolice = '-';
            } else if (type == 3) {
                startDate = '2017-01-01';
                endDate = '2099-01-31';
                noWO = '-';
                noPolice = datafilter.NoPolisi;
            };
            console.log("filterno", noWO);
            if (noWO.includes("*")) {
                typeParam = 1;
            } else {
                typeParam = 0;
            }
            if (noPolice.includes("*")) {
                typeParam = 1;
            } else {
                typeParam = 0;
            }
            typeSearch = 0;
            WoHistoryGR.getWoListGr(type, startDate, endDate, noWO, noPolice, typeParam).then(function(resu) {
                $scope.loading = false;
                console.log('resu wo list gr >>', resu.data);
                console.log('resu user >>', $scope.currUser);
                var data = resu.data.Result;
                var arr = [];
                if (data.length != 0) {
                    // _.map(data, function(e) {
                    //     console.log('ee >', e);
                    //     if ($scope.currUser.OrgId == e.OutletId) {
                    //         var obj = {};
                    //         obj.Customer = e.NamaKons;
                    //         obj.NoWo = e.WoNo;
                    //         obj.JobId = e.JobId;
                    //         obj.VehicleId = e.VehicleId;
                    //         obj.JobDate = e.JobDate;
                    //         obj.PoliceNumber = e.LicensePlate;
                    //         obj.SA = e.namasa;
                    //         obj.Status = e.status;
                    //         obj.KatashikiCode = e.KatashikiCode;
                    //     } else {
                    //         obj.Customer = e.NamaKons;
                    //         obj.NoWo = e.WoNo;
                    //         obj.JobId = e.JobId;
                    //         obj.VehicleId = e.VehicleId;
                    //         obj.JobDate = e.JobDate;
                    //         obj.PoliceNumber = e.LicensePlate;
                    //         obj.SA = '-';
                    //         obj.Status = e.status;
                    //         obj.KatashikiCode = e.KatashikiCode;
                    //     }

                    //     arr.push(obj);
                    // });
                    console.log('data.showdata', datafilter.ShowData);
                    if (datafilter.ShowData == 1) {
                        // typeData
                        var newData = _.remove(data, function(a) {
                            // console.log('aaaa>>>', a);
                            if (a.UserIdSa == $scope.EmployeeId) {
                                return a;
                            };
                        });
                        console.log("Default SA newdata", newData);
                        console.log("Default SA data", data);
                        if ($scope.currUser.RoleId == 1128 || $scope.currUser.RoleId == 1021) {
                            $scope.grid.data = data;
                        } else {
                            $scope.grid.data = newData;
                        }
                    } else {
                        // typeData
                        $scope.grid.data = data;
                        console.log("All SA")
                    };
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'warning',
                        title: "Data Tidak Ditemukan",
                    });
                    $scope.grid.data = [];
                }

            });
        };

        $scope.onChangeCP = function(item) {
            console.log('selected CP', item);
            var arr = [];
            if (item != null) {
                _.map($scope.cpList, function(e) {
                    if (e.Name == item.Name) {
                        arr.push(e);
                    }
                });
                // console.log('arrCP', arr);
                $scope.mDataDM.CP = arr[0];
            }
        };

        $scope.pkList = [{
            Id: 1,
            Name: 'Adi Daya'
        }, {
            Id: 2,
            Name: 'Abi Daya'
        }, {
            Id: 4,
            Name: 'Ari Daya'
        }];

        $scope.onChangePK = function(item) {
            console.log('selected PJ', item);
            var arr = [];
            _.map($scope.pkList, function(e) {
                if (e.Name == item.Name) {
                    arr.push(e);
                }
            });
            $scope.mDataDM.PK = arr[0];
        };

        $scope.savemode = function() {
            $scope.hiddenRow = true;
            $scope.disVehic = true;
        };
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() {
                $scope.$broadcast('show-errors-check-validity');
            });
        };

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        };


        //----------------------------------
        // Approval
        //----------------------------------
        $scope.gridClickApprove = function(row, idx) {
            console.log(idx, "click==>", row);
            bsNotify.show({
                title: "Approval",
                content: "Sales Order has been successfully approved.",
                type: 'success'
            });
            // $scope.grid.
            // $scope.mAction.show=true;


        }
        $scope.gridClickReject = function(row, idx) {
            console.log(idx, "click==>", row);
            $scope.mPrintServiceHistory = angular.copy(row);
            $scope.show_modal.showApproval = true;

            // $scope.grid.


        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        var dateFilter = 'date:"dd/MM/yyyy"';
        var btnApproveTemplate = '<button class="btn btn-danger"' +
            // ' onclick="this.blur()"'+
            ' ng-click="grid.appScope.$parent.gridClickReject(row.entity,grid.renderContainers.body.visibleRowCache.indexOf(row))">' +

            'Reject' +
            '</button>';
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: approvalForDP
        };
        $scope.filterBtnChange = function(col) {
            console.log("col", col);
            $scope.filterBtnLabel = col.name;
            $scope.filterColIdx = col.idx + 1;
        };
        // if ($scope.currUser.RoleId == 1128){
        //     $scope.grid = {
        //         enableGridMenu: true,
        //         enableSorting: true,
        //          enableRowSelection: true,
        //          multiSelect : true,
        //         // enableFullRowSelection: true,
        //         // enableRowHeaderSelection: false,
        //         // enableFiltering:true,
        //         // enableSelectAll : false,
        //     // grid.appScope.
        //         columnDefs: [
        //             {name: 'Tanggal',field: 'LastModifiedDate', cellFilter: dateFilter, width: '10%'}, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
        //             {name: 'Permintaan',field: 'RequestorRoleId', width: '20%'},
        //             {name: 'No Polisi',field: 'LicensePlate', width: '10%'},
        //             {name: 'VIN',field: 'VIN', width: '10%'},
        //             {name: 'Type',field: 'VIN', width: '20%'},
        //             // {name: 'Kategori',field: 'key'},
        //             { name:'Action', allowCellFocus: false, width:'30%', pinnedRight:true,
        //                                        enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
        //                                        cellTemplate: btnApproveTemplate
        //               },

        //         ],
        //         onRegisterApi : function(gridApi) {
        //             // set gridApi on $scope
        //             $scope.gridApi = gridApi;

        //             $scope.gridApi.selection.on.rowSelectionChanged($scope,function(row) {
        //                   $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
        //                   // console.log("selected=>",$scope.selectedRows);
        //                   if($scope.onSelectRows){
        //                       $scope.onSelectRows($scope.selectedRows);
        //                   }
        //             });
        //             $scope.gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
        //                   $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
        //                   if($scope.onSelectRows){
        //                       $scope.onSelectRows($scope.selectedRows);
        //                   }
        //             });
        //         }
        //       }
        // }else{
        //     $scope.grid = {
        //         enableSorting: true,
        //         enableRowSelection: true,
        //         multiSelect: true,
        //         enableSelectAll: true,
        //         //showTreeExpandNoChildren: true,
        //         // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        //         // paginationPageSize: 15,
        //         columnDefs: [
        //             { name: 'id', field: 'JobId', width: '7%', visible: false },
        //             { name: 'Vehicid', field: 'VehicleId', width: '7%', visible: false },
        //             { name: 'Tanggal', field: 'JobDate', cellFilter: dateFilter },
        //             { name: 'No Wo', field: 'NoWo' },
        //             { name: 'No Polisi', field: 'PoliceNumber' },
        //             { name: 'Customer', field: 'Customer' },
        //             { name: 'SA', field: 'SA' },
        //             { name: 'Status', field: 'Status' }
        //         ]
        //     };
        // }

        // ============Signature=================
        $scope.disableBPCenter = false;
        // $scope.user = CurrentUser.user();
        $scope.OutletBP = function() {
            console.log('user >>', $scope.user);
            // WO.OutletBPCenter($scope.user.OrgId).then(function(resu) {
            //     console.log('resu user outletbp>>>', resu.data);
            //     var data = resu.data.Result;
            //     if (data.length == 0) {
            //         $scope.disableBPCenter = true;
            //         console.log('harusna disable wac bp center');
            //     } else {
            //         $scope.disableBPCenter = false;
            //         console.log('harusna enable wac bp center');
            //     }
            // })
        };
        $scope.coorX = null;
        $scope.coorY = null;
        $scope.drawData = [];
        $scope.showPopup = false;
        $scope.openPopup = function(e) {
            ////
            $scope.showPopup = true;
            var left = e.offsetX;
            var top = e.offsetY;
            $scope.coorX = angular.copy(left) + 'px';
            $scope.coorY = angular.copy(top) + 'px';
            console.log('event X', $scope.coorX);
            console.log('event Y', $scope.coorY);
            $scope.popoverStyle = {
                'position': 'absolute',
                'background': '#fff',
                'border': '1px solid #999',
                'padding': '10px',
                'width': 'auto',
                'box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
                'left': $scope.coorX,
                'top': $scope.coorY
            };
            $scope.drawData = [{
                x: $scope.coorX,
                y: $scope.coorY
            }];
            console.log('scope.drawData', $scope.drawData);
        };

        $scope.closePopover = function(param) {
            $scope.paramClosePopover = param;
            $scope.showPopup = false;
            console.log('scope.paramClosePopover', $scope.paramClosePopover);
        };

        $scope.boundingBox = {
            width: 700,
            height: 300
        };

        $scope.signature = {
            dataUrl: null
        };
        var EMPTY_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC';
        $scope.disableReset = false;
        $scope.open = function(data) {
            bsAlert.alert({
                    title: "Apakah anda yakin signature telah benar ?",
                    text: "Action tidak dapat di ubah setelah Sign",
                    type: "warning",
                    showCancelButton: true
                },
                function() {
                    // $scope.cancelAppointment(item);
                    $scope.signature.dataUrl = data.dataUrl;
                    console.log('AAAA');
                    $scope.disableReset = true;
                    console.log('$scope.disableReset A', $scope.disableReset);
                    $scope.setSignature($scope.signature.dataUrl);

                },
                function() {
                    $scope.signature.dataUrl = EMPTY_IMAGE;
                    $scope.disableReset = false;
                    console.log('BBBB');
                    console.log('$scope.disableReset B', $scope.disableReset);
                    $scope.clear();
                }
            );
            console.log("sign", $scope.signature.dataUrl);
        };

        $scope.setSignature = function(item) {
            console.log("data Signature", item);
            $scope.mData.Signature = $scope.signature.dataUrl;

        };
        // ==================Info Pelanggan======================
        $scope.setCPPK = function(data, param) {
            var objPK = {};
            var objCP = {};
            var arrPK = [];
            var arrCP = [];

            objPK.Name = data.DecisionMaker;
            objPK.HP1 = data.PhoneDecisionMaker1;
            objPK.HP2 = data.PhoneDecisionMaker2;
            // console.log('objPK >', objPK);
            arrPK.push(objPK);
            console.log('arrPK >', arrPK);

            objCP.Name = data.ContactPerson;
            objCP.HP1 = data.PhoneContactPerson1;
            objCP.HP2 = data.PhoneContactPerson2;
            // console.log('objCP >', objCP);
            arrCP.push(objCP);
            console.log('arrCP >', arrCP);

            // console.log('$scope.dataCP >', $scope.dataCP);
            // if ($scope.dataCP.length > 0) {
            //     console.log('CP MASUK KE SINI!!');
            //     $scope.pkList = angular.copy($scope.dataCP);
            //     $scope.dataCP.push(objCP);
            //     $scope.cpList = $scope.dataCP;
            // } else {
            // if (param == 1) {
            //     $scope.getCPPK(data.VehicleId, 1, arrCP, arrPK);
            // } else {
            // $scope.cpList = arrCP;
            $scope.mDataDM.CP = objCP;
            // $scope.pkList = arrPK;
            $scope.mDataDM.PK = objPK;
            // }
            // console.log('CP MASUK SINI!!', 'PK', arrPK, 'CP', arrCP);
            // console.log('CP MASUK SINI!! datadariCRM', datadariCRM);

            // };
            // $scope.getUserList(data.)
        };

        $scope.getCPPK = function(vehicleId, flag, arrCP, arrPK) {
            WO.getListVehicleCPPK(vehicleId).then(function(resu) {
                console.log('resu cppk', resu.data);
                if (flag == 1) {
                    console.log('return 1 cppk', resu.data.Result);
                    console.log('return 1 arrCP', arrCP);
                    console.log('return 1 arrPK', arrPK);
                    // return resu.data.Result;
                    var data = resu.data.Result;
                    var newData = _.union(arrCP, arrPK, data, 'Name');
                    console.log('return 1 newData', newData);
                    $scope.cpList = newData;
                    $scope.pkList = newData;
                } else {
                    console.log('return undefined cppk', resu.data.Result);
                    _.map(resu.data.Result, function(a) {
                        o.HP1 = o.Handphone1;
                        o.HP2 = o.Handphone2;
                    });
                    $scope.cpList = resu.data.Result;
                    $scope.pkList = resu.data.Result;
                }
            });
        };


        $scope.searchCustomer = function(flag, value) {
            $scope.getCustIdBySearch = true;
            $scope.mDataCrm = [];
            console.log('value search', value);
            console.log('flag search', flag);
            if (value.filterValue == "") {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon input filter terlebih dahulu",
                });
            } else {
                if (flag == 1) {
                    WOBP.getCustomerVehicleList(value).then(function(resu) {
                        console.log('search cust >>>', resu);
                        console.log('resu.data customer>', resu.data.Result[0]);
                        var data = resu.data.Result[0];
                        if (data === undefined) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Data Tidak Ditemukan",
                            });
                        } else {
                            console.log('getCustomerVehicleList >>>', data);
                            var customer = data.CustomerList.CustomerListPersonal[0];
                            var institusi = data.CustomerList.CustomerListInstitution[0];
                            var address = data.CustomerList.CustomerAddress;
                            var objC_Personal = {};
                            var objC_Institusi = {};
                            var checkAddres = function() {
                                if (address.length != 0) {
                                    var tempAddressData = [];
                                    _.map(address, function(val) {
                                        console.log('adddersss <><>', val);
                                        if (val.StatusCode == 1) {
                                            var obj = {};
                                            obj.CustomerId = val.CustomerId;
                                            obj.CustomerAddressId = val.CustomerAddressId;
                                            obj.Address = val.Address;
                                            obj.AddressCategoryId = val.AddressCategoryId;
                                            obj.AddressCategory = val.AddressCategory;
                                            obj.MainAddress = val.MainAddress;
                                            obj.Phone1 = val.Phone1;
                                            obj.Phone2 = val.Phone2;
                                            obj.RT = val.RT;
                                            obj.RW = val.RW;
                                            obj.StatusCode = val.StatusCode;
                                            if (val.MVillage != null || val.MVillage !== undefined) {
                                                obj.ProvinceId = val.MVillage.MDistrict.MCityRegency.MProvince.ProvinceId;
                                                obj.CityRegencyId = val.MVillage.MDistrict.MCityRegency.CityRegencyId;
                                                obj.VillageId = val.MVillage.VillageId;
                                                obj.DistrictId = val.MVillage.MDistrict.DistrictId;
                                                obj.CityRegencyData = val.MVillage.MDistrict.MCityRegency;
                                                obj.DistrictData = val.MVillage.MDistrict;
                                                obj.ProvinceData = val.MVillage.MDistrict.MCityRegency.MProvince;
                                                obj.VillageData = val.MVillage;
                                                obj.AddressCategory = val.AddressCategory;
                                            };
                                            // obj.CustomerId =
                                            tempAddressData.push(obj);
                                            console.log('tempAddressData <><>', obj);
                                        };
                                    });
                                    return tempAddressData;
                                } else {
                                    return [];
                                }
                            };

                            $scope.custTypeId = data.CustomerList.CustomerTypeId;
                            $scope.customerId = data.CustomerList.CustomerId;
                            $scope.CustomerVehicleId = data.CustomerVehicleId;
                            $scope.vehicleId = data.VehicleId;
                            if (data.CustomerList.CustomerTypeId == 3) {
                                $scope.showFieldPersonal = true;
                                $scope.showFieldInst = false;
                                $scope.personalId = customer.PersonalId;
                                objC_Personal.BirthDate = customer.BirthDate;
                                objC_Personal.KTPKITAS = customer.KTPKITAS;
                                objC_Personal.HandPhone1 = customer.Handphone1;
                                objC_Personal.HandPhone2 = customer.Handphone2;
                                objC_Personal.Name = customer.CustomerName;
                                objC_Personal.FrontTitle = customer.FrontTitle;
                                objC_Personal.EndTitle = customer.EndTitle;
                                objC_Personal.ToyotaId = data.CustomerList.ToyotaId;
                                objC_Personal.Npwp = data.CustomerList.Npwp;
                                objC_Personal.CustomerId = data.CustomerList.CustomerId;
                                objC_Personal.FleetId = data.CustomerList.FleetId;
                                objC_Personal.AFCOIdFlag = data.CustomerList.Afco;
                                objC_Personal.ToyotaIDFlag = null;
                                // objC_Personal.Address = checkAddres();
                                objC_Personal.CustomerVehicleId = data.CustomerVehicleId;
                                $scope.CustomerAddress = checkAddres();
                                $scope.mDataCrm.Customer = objC_Personal;
                                objC_Institusi = {};

                            } else {
                                $scope.showFieldInst = true;
                                $scope.showFieldPersonal = false;
                                $scope.institusiId = institusi.InstitutionId;
                                objC_Institusi.ToyotaId = data.CustomerList.ToyotaId;
                                objC_Institusi.CustomerTypeId = data.CustomerList.CustomerTypeId;
                                objC_Institusi.Name = institusi.Name;
                                objC_Institusi.PICName = institusi.PICName;
                                objC_Institusi.PICDepartment = institusi.PICDepartment;
                                objC_Institusi.PICHp = institusi.PICHp;
                                objC_Institusi.PICKTPKITAS = institusi.PICKTPKITAS;
                                objC_Institusi.PICEmail = institusi.PICEmail;
                                // objC_Institusi.Address = checkAddres();
                                objC_Institusi.CustomerVehicleId = data.CustomerVehicleId;
                                objC_Institusi.AFCOIdFlag = data.CustomerList.Afco;
                                $scope.CustomerAddress = checkAddres();
                                $scope.mDataCrm.Customer = objC_Institusi;
                                objC_Personal = {};
                            };

                            //Vehicle
                            if (data.VehicleList) {
                                var vehicle = data.VehicleList;
                                var objV = {};
                                objV.VehicleId = vehicle.VehicleId;
                                objV.LicensePlate = vehicle.LicensePlate;
                                objV.GasTypeId = vehicle.GasTypeId;
                                objV.VIN = vehicle.VIN;
                                objV.ModelCode = vehicle.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;
                                objV.AssemblyYear = vehicle.AssemblyYear;
                                // objV.Color = vehicle.MVehicleTypeColor.MColor.ColorName;
                                // objV.ColorCode = vehicle.MVehicleTypeColor.MColor.ColorCode;
                                objV.KatashikiCode = vehicle.MVehicleTypeColor.MVehicleType.KatashikiCode;
                                objV.DECDate = vehicle.DECDate;
                                objV.EngineNo = vehicle.EngineNo;
                                objV.SPK = vehicle.SPK ? vehicle.SPK : 'No Data Available YET';
                                objV.Insurance = vehicle.Insurance ? vehicle.Insurance : 'No Data Available YET';
                                objV.STNKName = vehicle.STNKName;
                                objV.STNKAddress = vehicle.STNKAddress;
                                objV.STNKDate = vehicle.STNKDate;
                                objV.Model = vehicle.MVehicleTypeColor.MVehicleType.MVehicleModel;
                                // objV.Type = vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId;
                                var testingType = null;
                                var testingColor = null;
                                _.map($scope.VehicM, function(val) {
                                    if (val.VehicleModelId == objV.Model.VehicleModelId) {
                                        $scope.selectedModel(val);
                                        _.map(val.MVehicleType, function(val2) {
                                            console.log('val2', val2);
                                            if (val2.VehicleTypeId == vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId) {
                                                testingType = val2;
                                                objV.Type = testingType;
                                                // objV.Type = testingType.VehicleTypeId;
                                                $scope.selectedType(val2, 1);
                                                _.map($scope.VehicColor, function(val3) {
                                                    console.log('val3', val3);
                                                    if (val3.ColorId == vehicle.MVehicleTypeColor.ColorId) {
                                                        testingColor = val3;
                                                        objV.Color = testingColor;
                                                        // objV.Color = testingColor.ColorId;
                                                        objV.ColorCode = testingColor.ColorCode;
                                                        $scope.selectedColor(testingColor);
                                                    };
                                                });
                                            };
                                        });
                                    };
                                });
                                // WO.getVehicleUser(vehicle.VehicleId).then(function(resu) {
                                //     console.log('list pengguna >>', resu.data.Result);
                                // });
                                $scope.getUserList(vehicle.VehicleId, 4);
                                $scope.mDataCrm.Vehicle = objV;
                                objV = {};
                                var objUS = {};
                                var arr1 = [];
                                var arr2 = [];
                                var arrusVUid = [];
                                var tempArr = [];
                                if (data.VehicleUser.length > 1) {
                                    _.map(data.CustomerVehicle.VehicleUser, function(e, k) {
                                        arr1.push(e.Name);
                                        arr2.push(e.Phone);
                                        arrusVUid.push(e.VehicleUserId);
                                        tempArr.push({});
                                    });
                                    $scope.mData.US.name = arr1;
                                    $scope.mData.US.phoneNumber1 = arr2;
                                    $scope.mData.US.VehicleUserId = arrusVUid;
                                    $scope.userForm = tempArr;
                                };
                            } else {
                                $scope.mDataCrm.Vehicle = {};
                                $scope.VehicT = [];
                                $scope.enableType = true;
                            };
                        }
                    });
                }
            }
            console.log("$scope.CustomerAddress", $scope.CustomerAddress);
        };
        $scope.getUserList = function(data, param) {
            console.log('$get user list ><>', data, param);

            WO.getVehicleUser(data).then(function(resu) {
                console.log('resu >>', resu.data.Result);
                var dataResu = resu.data.Result;

                $scope.mData.US = [];
                if (dataResu.length != 0) {

                    var arr = [];
                    // var arr1 = [];
                    // var arr2 = [];
                    // var arr3 = [];
                    // var arr4 = [];
                    _.map(dataResu, function(val) {
                        var obj = {};
                        obj.customerVehicleId = data;
                        obj.VehicleId = data;
                        obj.name = val.Name;
                        obj.VehicleUserId = val.VehicleUserId;
                        obj.Relationship = val.RelationId;
                        obj.phoneNumber1 = val.Phone1;
                        obj.phoneNumber2 = val.Phone2;
                        arr.push(obj);

                        // objCP.Name = val.Name;
                        // objCP.HP1 = val.Phone1;
                        // objCP.HP2 = val.Phone2;
                        // arrCP.push(objCP);
                    });
                    _.sortBy(dataResu, ['LastModifiedDate']).reverse();
                    // console.log("arr1", arr1);
                    // console.log("arr4", arr4);
                    // console.log("$scope.mData.US",$scope.mData.US);
                    $scope.mData.US.name = arr[0].name;
                    $scope.mData.US.phoneNumber1 = arr[0].phoneNumber1;
                    $scope.mData.US.phoneNumber2 = arr[0].phoneNumber2;
                    $scope.mData.US.Relationship = arr[0].Relationship;
                    // $scope.pngList = data;
                    $scope.CVehicUId = data;
                    console.log('$scope.mData.US', $scope.mData.US);
                    $scope.pngList = arr;
                    $scope.hiddenUserList = false;
                    console.log("arr", arr);
                } else {
                    $scope.hiddenUserList = true;
                    $scope.CVehicUId = 0;
                }
                console.log('$scope.CVehicUId >>>000<<<<', $scope.CVehicUId);
            });
        };
        $scope.filterSearch = {
            flag: "",
            TID: "",
            filterValue: "",
            choice: false
        };
        $scope.filterField = [{
                key: "Nopol",
                desc: "Nomor Polisi",
                flag: 1,
                value: ""
            },
            {
                key: "NoRank",
                desc: "Nomor Rangka",
                flag: 2,
                value: ""
            },
            {
                key: "NoMesin",
                desc: "Nomor Mesin",
                flag: 3,
                value: ""
            },
            {
                key: "Phone",
                desc: "Nomor Handphone",
                flag: 4,
                value: ""
            },
            {
                key: "NoKTP",
                desc: "Nomor KTP/KITAS",
                flag: 5,
                value: ""
            },
            {
                key: "TTL",
                desc: "Tanggal Lahir",
                flag: 6,
                value: ""
            }
        ];
        $scope.filterFieldChange = function(item) {
            $scope.filterSearch.flag = item.flag;
            $scope.filterSearch.filterValue = "";
            $scope.filterFieldSelected = item.desc;

        };
        $timeout(function() {
            $scope.filterFieldChange($scope.filterField[0]);
        }, 0);

        $scope.filterSearchV = {
            flag: "",
            TID: "",
            filterValue: "",
            choice: false
        };
        $scope.filterFieldV = [{
                key: "Nopol",
                desc: "Nomor Polisi",
                flag: 1,
                value: ""
            },
            {
                key: "NoRank",
                desc: "Nomor Rangka",
                flag: 2,
                value: ""
            }
        ];

        $scope.filterFieldChangeV = function(item) {
            $scope.filterSearchV.flag = item.flag;
            $scope.filterSearchV.filterValue = "";
            $scope.filterFieldSelectedV = item.desc;
        };

        $timeout(function() {
            $scope.filterFieldChangeV($scope.filterField[0]);
        }, 0);
        $scope.clearEditCustomer = function() {
            $scope.filterSearch.filterValue = "";
            $scope.showFieldInst = false;
            $scope.showFieldPersonal = false;
        };

        $scope.saveCustomerCrm = function(param, data, address) {
            console.log('param >>>000<<<<', param);
            console.log('data >>>000<<<<', data);
            console.log('address >>>000<<<<', address);
            if (data.Vehicle.VehicleId) {
                $scope.getUserList(data.Vehicle.VehicleId, 1);
            } else {
                //Kalau Tidak ada CustomerVehicleId
            };
            $scope.getCustIdBySearch = false;
            var date = new Date(data.Customer.BirthDate);
            date.setSeconds(0);
            var yearFirst = date.getFullYear();
            var monthFirst = date.getMonth() + 1;
            var dayFirst = date.getDate();
            var bDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
            data.Customer.BirthDate = bDate;
            data.Customer.newBirthDate = bDate;
            console.log('bDate', bDate);
            console.log('data >>>000<<<< 2', data);

            // var dummyToyataId = Math.floor((Math.random() * 99999999) + 1);

            if (param == 2) {
                // data.Customer.ToyotaId = dummyToyataId;
                WOBP.createNewCustomerList(data).then(function(resu) {
                    console.log('resu customer post', resu);
                    var customerId = resu.data[0].CustomerId;
                    _.map(address, function(val) {
                        val.CustomerId = customerId;
                    });
                    if (data.Customer.CustomerTypeId == 3) {
                        WOBP.createNewCustomerListPersonal(customerId, data).then(function(resu2) {
                            console.log('resu customer post', resu2.data);
                            WOBP.createNewCustomerListAddress(customerId, address).then(function(resu3) {
                                console.log('resu.data create address', resu3.data);
                                $scope.clearEditCustomer();
                            });
                        });
                    } else {
                        console.log("DISINI SIMPAN ADDRESS & INSTITUSI");
                        //SIMPAN NEW CUSTOMER LIST INSTITUSI
                    }
                });
            } else if (param == 1) {
                console.log("DISINI customerId, custTypeId", $scope.customerId, $scope.custTypeId);
                WOBP.updateCustomerList($scope.customerId, $scope.custTypeId, data).then(function(resu) {
                    // console.log('resu.data update', resu.data);
                    console.log('$scope.custTypeId', $scope.custTypeId);
                    if ($scope.custTypeId == 3) {
                        WOBP.updateCustomerListPersonal($scope.personalId, $scope.customerId, data).then(function(resu2) {
                            // console.log('resu.data update personal', resu.data);
                            WOBP.updateCustomerListAddress($scope.custAddressId, $scope.customerId, address).then(function(resu3) {
                                console.log('resu.data update address', resu3.data);
                                $scope.clearEditCustomer();
                            });
                        });
                    } else {
                        console.log("DISINI UPDATE ADDRESS & INSTITUSI");
                    }
                });
            }
        };
        $scope.changeOwner = function() {
            $scope.mDataCrm.Customer = [];
            if ($scope.action == 2) {
                $scope.mode = 'form';
            } else {
                $scope.showFieldInst = false;
                $scope.showFieldPersonal = false;
                $scope.filterSearch.filterValue = "";
                $scope.mode = 'search';
            };
        };
        $scope.changeVehic = function(param) {
            console.log('paramV', param);
            $scope.mDataCrm.Vehicle = [];
            if (param == 2) {
                console.log('masuk sini vehic 2');
                $scope.hideVehic = true;
                $scope.modeV = 'form';
                $scope.mDataVehicle = [];
            } else {
                $scope.hideVehic = false;
                $scope.modeV = 'search';
            }
        };
        $scope.searchV = function() {
            console.log('actionV', $scope.actionV);
        };
        $scope.search = function() {
            $scope.mode = 'search';
            $scope.listCustomerIdx = $scope.listControl;
        };
        $scope.backSearch = function() {
            $scope.mode = 'view';
            $scope.listCustomerIdx = $scope.listView;
        };
        $scope.getAddressList = function() {
            // return $q.resolve(
            //     CMaster.GetCAddressCategory().then(function(res) {
            //         console.log("List GetCAddressCategory====>", res.data.Result);
            //         $scope.categoryContact = res.data.Result;
            //     }),
            //     WOBP.getMLocationProvince().then(function(res) {
            //         console.log("List Province====>", res.data.Result);
            //         $scope.provinsiData = res.data.Result;
            //     })
            // );
        };

        $scope.selectProvince = function(row) {
            console.log('row selectProvince', row);
            WOBP.getMLocationCityRegency(row.ProvinceId).then(function(resu) {
                $scope.kabupatenData = resu.data.Result;
            });
            $scope.selectedProvince = angular.copy(row);
        };
        $scope.selectRegency = function(row) {
            console.log('row selectRegency', row);
            WOBP.getMLocationKecamatan(row.CityRegencyId).then(function(resu) {
                $scope.kecamatanData = resu.data.Result;
            });
            // $scope.kecamatanData = row.MDistrict;
            $scope.selectedRegency = angular.copy(row);
        };
        $scope.selectDistrict = function(row) {
            console.log('row selectDistrict', row);
            WOBP.getMLocationKelurahan(row.DistrictId).then(function(resu) {
                $scope.kelurahanData = resu.data.Result;
            });
            $scope.selectedDistrict = angular.copy(row);
        };
        $scope.selectVillage = function(row) {
            console.log('row selectVillage', row);
            $scope.selecedtVillage = angular.copy(row);
            $scope.lmModelAddress.PostalCode = row.PostalCode ? row.PostalCode : '-';
        }
        $scope.onBeforeSaveAddress = function(row) {
            console.log('masuk sini onbeforesaveaddress', row);
            var AddressCategory = {};
            if ($scope.getCustIdBySearch) {
                row.CustomerId = $scope.CustomerId;
            } else {
                row.CustomerId = 0;
            };
            _.map($scope.categoryContact, function(val) {
                if (val.AddressCategoryId == row.AddressCategoryId) {
                    AddressCategory = val;
                }
            });
            row.AddressCategory = AddressCategory;
            row.ProvinceData = $scope.selectedProvince;
            row.CityRegencyData = $scope.selectedRegency;
            row.DistrictData = $scope.selectedDistrict;
            row.VillageData = $scope.selecedtVillage;
            console.log('masuk sini onbeforesaveaddress NEWDATA', row);
        };

        $scope.onBeforeEditAddress = function(row) {};

        $scope.onShowDetailAddress = function(row) {
            console.log('masuk sini onShowDetailAddress', row);
        }

        $scope.onBeforeDeleteAddress = function(row) {
            console.log('masuk sini onBeforeDeleteAddress', row);
        };
        $scope.searchVehicle = function(flag, value) {
            console.log('searchVehicle', flag, value);
            var plat = '-';
            var vin = '-';
            if (value.flag == 1) {
                plat = value.filterValue;
                vin = '-';
            } else if (value.flag == 2) {
                vin = value.filterValue;
                plat = '-';
            };

            WOBP.getVehicleList(vin, plat).then(function(resu) {
                console.log('resu search vehic', resu.data.Result[0]);
                var data = resu.data.Result[0];
                var obj = {};
                obj.VehicleId = data.VehicleId;
                obj.LicensePlate = data.LicensePlate;
                obj.GasTypeId = data.GasTypeId;
                obj.VIN = data.VIN;
                obj.ModelCode = data.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;
                obj.AssemblyYear = data.AssemblyYear;
                // obj.Color = data.MVehicleTypeColor.MColor.ColorName;
                // obj.ColorId = data.MVehicleTypeColor.VehicleTypeColorId;
                // obj.ColorCode = data.MVehicleTypeColor.MColor.ColorCode;
                obj.KatashikiCode = data.MVehicleTypeColor.MVehicleType.KatashikiCode;
                obj.DECDate = data.DECDate;
                obj.EngineNo = data.EngineNo;
                obj.SPK = data.SPK ? data.SPK : 'No Data Available';
                obj.Insurance = data.Insurance ? data.Insurance : 'No Data Available';
                obj.Model = data.MVehicleTypeColor.MVehicleType.VehicleModelId;
                obj.STNKName = data.STNKName;
                obj.STNKAddress = data.STNKAddress;
                obj.STNKDate = data.STNKDate;

                var testingType = null;
                var testingColor = null;
                _.map($scope.VehicM, function(val) {
                    if (val.VehicleModelId == objV.Model.VehicleModelId) {
                        $scope.selectedModel(val);
                        _.map(val.MVehicleType, function(val2) {
                            console.log('val2', val2);
                            if (val2.VehicleTypeId == vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId) {
                                testingType = val2;
                                objV.Type = testingType;
                                // objV.Type = testingType.VehicleTypeId;
                                $scope.selectedType(val2, 1);
                                _.map($scope.VehicColor, function(val3) {
                                    console.log('val3', val3);
                                    if (val3.ColorId == vehicle.MVehicleTypeColor.ColorId) {
                                        testingColor = val3;
                                        objV.Color = testingColor;
                                        // objV.Color = testingColor.ColorId;
                                        objV.ColorCode = testingColor.ColorCode;
                                        $scope.selectedColor(testingColor);
                                    };
                                });
                            };
                        });
                    };
                });
                // WO.getVehicleUser(vehicle.VehicleId).then(function(resu) {
                //     console.log('list pengguna >>', resu.data.Result);
                // });
                $scope.getUserList(vehicle.VehicleId, 4);
                // obj.Type = data.MVehicleTypeColor.MVehicleType.VehicleTypeId;
                $scope.mDataCrm.Vehicle = obj;
                $scope.hideVehic = true;
                console.log('objobj', obj);
                console.log('$scope.mDataCrm', $scope.mDataCrm);
            });

        };
        $scope.editVehic = function() {
            $scope.disVehic = false;
            $scope.hideVehic = false;
        };
        $scope.saveVehic = function(param, data) {
            $scope.disVehic = true;
            $scope.hideVehic = true;
            console.log('data saveVehic >>><<<<', data);
            console.log('param saveVehic >>><<<<', param);
            var date = new Date(data.Vehicle.DECDate);
            date.setSeconds(0);
            var yearFirst = date.getFullYear();
            var monthFirst = date.getMonth() + 1;
            var dayFirst = date.getDate();
            var DECDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
            data.Vehicle.DECDate = DECDate;
            // $scope.modeV = 'view';
            if (param == 2) {
                WOBP.createNewVehicleList(data).then(function(resu) {
                    console.log('$resu.resu createNewVehicleList', resu.data);
                    $scope.mDataCrm.Vehicle.VehicleId = resu.data;
                    $scope.newVehicleId = resu.data;
                });
            } else {
                //Update Data Kendaraan
                WOBP.updateVehicleList(data).then(function(resu) {
                    console.log('$resu.resu updateVehicleList', resu.data);
                });
            };
        };
        $scope.cancelSearchV = function() {
            $scope.disVehic = true;
            $scope.hideVehic = true;
        };
        $scope.editPnj = function() {
            $scope.disPnj = false;
        };
        $scope.savePnj = function() {
            $scope.disPnj = true;
            console.log('$scope.mDataVehicle', $scope.mDataVehicle);
        };
        $scope.onChangePng = function(item) {
            $scope.mData.US = item;
        };
        $scope.editPng = function() {
            $scope.disPng = false;
        };
        $scope.cancelPng = function() {
            $scope.disPng = true;
            $scope.mData.US = [];
        };
        $scope.savePng = function(data, CVehicUId, crm) {
            var tempArrSavePng = [];
            var tempArrSavePngNoId = [];
            console.log('$scope.mData.US data >>', data, CVehicUId, crm);
            if (data.VehicleId == undefined) {
                if (CVehicUId == 0) {
                    bsAlert.alert({
                            title: "Customer Vehicle ID Tidak Ditemukan !!",
                            text: "Silahkan Cek Data kembali !",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    );
                } else {
                    $scope.disPng = true;
                    console.log('createNewUserList data 1>>', data, CVehicUId);
                    WOBP.createNewUserList(data, CVehicUId).then(function(resu) {
                        console.log('createNewUserList resu >>', resu.data);
                        $scope.getUserList(CVehicUId, 2);
                    });
                }
            } else {
                $scope.disPng = true;
                if (data.vehicleUserId != undefined) {
                    console.log('updateUserList data >>', data, CVehicUId);
                    WOBP.updateUserList(data, CVehicUId).then(function(resu) {
                        console.log('updateUserList resu>>', resu.data);
                        $scope.getUserList(CVehicUId, 3);
                    });
                } else {
                    console.log('createNewUserList data 2>>', data, CVehicUId);
                }
            }
        };
        //========================JOB LIST=============================
        $scope.closeWO = function(item) {
            bsAlert.alert({
                    title: "Apakah anda yakin untuk Close WO?",
                    text: item.WoNo,
                    type: "warning",
                    showCancelButton: true
                },
                function() {
                    $scope.doCloseWOHistory(item);
                },
                function() {

                }
            )
        }
        $scope.doCloseWOHistory = function(item) {
            console.log("$scope.doCloseWOHistory==========>", item);
            WoHistoryGR.closeWO(item.JobId).then(function(resu) {
                console.log('resu close wo', resu.data);
                $scope.formApi.setMode('grid');
                $scope.getData();
            })
        }
        AppointmentGrService.getPayment().then(function(res) {
            $scope.paymentData = res.data.Result;
        });
        AppointmentGrService.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result;
        });
        AppointmentGrService.getTaskCategory().then(function(res) {
            $scope.taskCategory = res.data.Result;
        });
        AppointmentGrService.getWoCategory().then(function(res) {
            $scope.woCategory = res.data.Result;
            $scope.dataCatWO = angular.copy($scope.woCategory);

            console.log('$scope.woCategory', $scope.woCategory);
        });
        // WOBP.getRelationship().then(function(resu) {
        //     $scope.dataRelationship = resu.data.Result;
        //     console.log("$scope.dataRelationship", $scope.dataRelationship);
        // });
        // WO.getTransactionCode().then(function(resu) {
        //     $scope.codeTransaction = resu.data;
        // });
        // $scope.listButtonFalse = {
        //     new: {
        //         enable: false
        //     }
        // };
        // ==================================================
        $scope.addDetail = function(data, item, itemPrice, row) {
            console.log('clearDetail', $scope.listApiJob.clearDetail());
            $scope.listApiJob.clearDetail();
            row.FlatRate = data.FlatRate;
            row.catName = $scope.tmpCatg.Name;
            row.ActualRate = data.FlatRate;
            row.tmpTaskId = null;
            // row.cusType=$scope.tmpCus.Name
            if ($scope.tmpPaidById !== null) {
                row.PaidById = $scope.tmpPaidById;
                row.paidName = $scope.tmpPaidName;
            }
            console.log("itemPrice", itemPrice);
            if (itemPrice.length > 0) {
                for (var i = 0; i < itemPrice.length; i++) {
                    if (itemPrice[i].ServiceRate !== undefined) {
                        var harganya = itemPrice[i].ServiceRate;
                        var harganyaa = data.FlatRate;
                        var summary = harganya * harganyaa;
                        var summaryy = parseInt(summary);
                        row.Summary = Math.round(summaryy);
                        // row.Fare = itemPrice[i].AdjustmentServiceRate;
                        row.Fare = itemPrice[i].ServiceRate;
                        $scope.PriceAvailable = true;
                    } else {
                        row.Fare = itemPrice[i].ServiceRate;
                        row.Summary = row.Fare;
                    }
                }
            } else {
                $scope.PriceAvailable = false;
                row.Fare = "";
                row.Summary = "";
            }
            row.TaskId = data.TaskId;
            row.isOPB = data.isOPB ? data.isOPB : 0;
            var tmp = {}
                // $scope.listApiJob.addDetail(tmp,row)
            if (item.length > 0) {
                for (var i = 0; i < item.length; i++) {
                    // var tmpItem = item[i];
                    // $scope.getAvailablePartsService(item[i]);
                    // if ($scope.tmpPaidById !== null) {
                    //     item[i].PaidById = $scope.tmpPaidById;
                    //     item[i].paidName = $scope.tmpPaidName;
                    // }
                    // item[i].subTotal = item[i].Qty * item[i].RetailPrice;
                    // // console.log("item[i].subTotal",item[i].subTotal);
                    // tmp = item[i]
                    // $scope.listApiJob.addDetail(tmp, row);
                    $scope.getAvailablePartsService(item[i], row);
                }
            } else {
                $scope.listApiJob.addDetail(false, row);
            }
        };
        $scope.addDetailOPL = function(data, row) {
            console.log("row OPL", row);
            console.log("ini datanya OPL", data);
            console.log("ini $scope.listApi", $scope.listApi);
            // $scope.listApi.clearDetail();
            row.VendorName = data.VendorName;
            row.OplId = data.Id;
            $scope.listApi.addDetail("", row);

        };
        $scope.onListSelectRows = function(rows) {
            console.log("form controller=>", rows);
        };

        $scope.gridPartsDetail = {
            columnDefs: [{
                    name: "No. Material",
                    field: "PartsCode",
                    width: '15%'
                },
                {
                    name: "Nama Material",
                    field: "PartsName",
                    width: '20%'
                },
                {
                    name: "Qty",
                    field: "Qty",
                    width: '6%'
                },
                {
                    name: "Satuan",
                    field: "satuanName",
                    width: '8%'
                },
                // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
                {
                    name: "Ketersediaan",
                    field: "Availbility",
                    width: '12%'
                },
                {
                    name: "Tipe",
                    field: "Type",
                    width: '6%'
                },
                {
                    name: "ETA",
                    displayName: "ETA",
                    field: "ETA",
                    cellFilter: dateFilter,
                    width: '11%'
                },
                {
                    name: "Harga",
                    field: "RetailPrice",
                    width: '10%'
                },
                {
                    name: "Discount",
                    field: "Discount",
                    width: '10%'
                },
                {
                    name: "SubTotal",
                    field: "subTotal",
                    width: '10%'
                },
                {
                    name: "Nilai Dp",
                    displayName: "Nilai DP",
                    field: "DownPayment",
                    width: '10%'
                },
                {
                    name: "Status DP",
                    displayName: "Status DP",
                    field: "StatusDp",
                    cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>',
                    width: '7%'
                },
                {
                    name: "Pembayaran",
                    field: "PaidBy",
                    width: '11%'
                },
                {
                    name: "OPB",
                    displayName: "OPB",
                    field: "isOPB",
                    width: '7%',
                    cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>'
                }
            ]
        };
        $scope.gridSumPartsDetail = {
            columnDefs: [{
                    name: "No. Material",
                    field: "PartsCode",
                    width: '15%'
                },
                {
                    name: "Nama Material",
                    field: "PartsName",
                    width: '20%'
                },
                {
                    name: "Qty",
                    field: "Qty",
                    width: '6%'
                },
                {
                    name: "Satuan",
                    field: "satuanName",
                    width: '8%'
                },
                // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
                {
                    name: "Ketersediaan",
                    field: "Availbility",
                    width: '12%'
                },
                {
                    name: "Tipe",
                    field: "Type",
                    width: '6%'
                },
                {
                    name: "ETA",
                    displayName: "ETA",
                    field: "ETA",
                    cellFilter: dateFilter,
                    width: '11%'
                },
                {
                    name: "Harga",
                    field: "RetailPrice",
                    width: '10%'
                },
                {
                    name: "Discount",
                    field: "Discount",
                    width: '10%'
                },
                {
                    name: "SubTotal",
                    field: "subTotal",
                    width: '10%'
                },
                {
                    name: "Nilai Dp",
                    displayName: "Nilai DP",
                    field: "DownPayment",
                    width: '10%'
                },
                {
                    name: "Status DP",
                    displayName: "Status DP",
                    field: "StatusDp",
                    cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>',
                    width: '7%'
                },
                {
                    name: "Pembayaran",
                    field: "PaidBy",
                    width: '11%'
                },
                {
                    name: "OPB",
                    displayName: "OPB",
                    field: "isOPB",
                    width: '7%',
                    cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>'
                }
            ]
        };
        $scope.gridWork = gridTemp;
        $scope.sendWork = function(key, data) {
            var taskandParts = [];
            var taskList = {};
            AppointmentGrService.getDataPartsByTaskId(data.TaskId).then(function(res) {
                console.log("resabis ambil parts", res.data.Result);
                taskandParts = res.data.Result;
                AppointmentGrService.getDataTaskListByKatashiki($scope.mData.KatashikiCode).then(function(restask) {
                    taskList = restask.data.Result;
                    $scope.addDetail(data, taskandParts, taskList, $scope.lmModel)
                });
            });
        }
        $scope.sendParts = function(key, data) {
                var tmpMaterialId = angular.copy($scope.ldModel.MaterialTypeId);
                console.log("a $scope.ldModel.Parts", $scope.ldModel);
                $scope.ldModel = data;
                var tmp = {};
                tmp = data;
                $scope.ldModel.PartsName = data.PartsName;
                $scope.ldModel.MaterialTypeId = tmpMaterialId;
                $scope.ldModel.SatuanId = data.UomId;
                $scope.ldModel.satuanName = data.Name;
                // $scope.listApi.addDetail(tmp);
                console.log("key Parts", key);
                console.log("data Parts", data);
                console.log("b $scope.ldModel.Parts", $scope.ldModel);
            }
            // $scope.sendOpe = function(key, data) {
            //     console.log("key Parts", key);
            //     console.log("data Parts", data);
            //     console.log("b $scope.ldModel.Parts", $scope.ldModel);
            // }
        $scope.sendOPL = function(key, data) {
            $scope.addDetailOPL(data, $scope.lmModel);
        };
        $scope.allOPL = [];
        $scope.getAllOPL = function() {
            // WoHistoryGR.getAllOPL().then(function(resu) {
            //     $scope.allOPL = resu.data.Result;
            //     console.log('$scope.allOPL', $scope.allOPL);
            // });
            // RepairSupportActivityGR.getT1().then(function(res) {
            //     $scope.TFirst = res.data.Result;
            //     //console.log("T1", $scope.T1);
            // });
        };
        //----------------------------------
        // Type Ahead
        //----------------------------------
        // $scope.getWork = function(key) {
        //     var Katashiki = $scope.mData.KatashikiCode;
        //     var catg = $scope.tmpCatg.MasterId;
        //     console.log("$scope.mData.KatashikiCode", Katashiki);
        //     console.log("$scope.lmModel.JobType", catg);
        //     // if (Katashiki != null && catg != null) { //Belum Get Data
        //     var res = AppointmentGrService.getDataTask(key, Katashiki, catg);
        //     return res;
        //     // }
        // };
        // $scope.getOPL = function(key) {
        //     // var Katashiki = $scope.mData.KatashikiCode;
        //     // var catg = $scope.tmpCatg.MasterId;
        //     console.log("key key", key);
        //     var search = key;
        //     var results = _.filter($scope.allOPL, function(item) {
        //         return item.OPLWorkName.indexOf(search) > -1;
        //     });
        //     console.log(results);
        //     return results;
        //     // console.log("$scope.lmModel.JobType", catg);
        //     // if (Katashiki != null && catg != null) { //Belum Get Data
        //     // var res = AppointmentGrService.getDataTask(key, Katashiki, catg);
        //     // return res;
        //     // }
        // };
        // $scope.onSelectOPL = function($item, $model, $label) {
        //     console.log('$item', $item);
        //     console.log('$model', $model);
        //     console.log('$label', $label);
        //     $scope.sendOPL($label, $item);
        // };
        $scope.getWork = function(key) {
            if ($scope.mDataCrm.Vehicle.KatashikiCode != null) {
                var Katashiki = $scope.mDataCrm.Vehicle.KatashikiCode;
                var catg = $scope.tmpCatg.MasterId;
                console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
                console.log("$scope.lmModel.JobType", catg);
                if (Katashiki != null && catg != null) {
                    var ress = AppointmentGrService.getDataTask(key, Katashiki, catg).then(function(resTask) {
                        return resTask.data.Result;
                    });
                    console.log("ress", ress);
                    return ress;
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Cari / Input Mobil Terlebih Dahulu",
                });
            };
        };
        $scope.getParts = function(key) {
            var kategori = $scope.ldModel.MaterialTypeId;
            if (kategori !== undefined) {
                var ress = AppointmentGrService.getDataParts(key, kategori).then(function(resparts) {
                    return resparts.data.Result;
                });
                console.log("ress", ress);
                return ress
            }
            // }
        };
        $scope.getTaskOpe = function(key) {
            if ($scope.mDataCrm.Vehicle.KatashikiCode != null) {
                console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
                console.log("$scope.lmModel.JobType", catg);
                var Katashiki = $scope.mDataCrm.Vehicle.KatashikiCode;
                var catg = $scope.tmpCatg.MasterId;
                if (Katashiki != null && (catg != null || $scope.lmModel.JobTypeId !== null)) {
                    var ress = WoHistoryGR.getDataTaskOpe(key, Katashiki, catg).then(function(resTask) {
                        return resTask.data.Result;
                    });
                    console.log("ress", ress);
                    return ress;
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Cari / Input Mobil Terlebih Dahulu",
                });
            };
        };
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        $scope.noResults = true;
        // var idx = gridTemp.length;
        $scope.onSelectWork = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            if ($item.FlatRate != null) {
                console.log("FlatRate", $item.FlatRate);
                $scope.FlatRateAvailable = true;
            } else {
                $scope.FlatRateAvailable = false;
            }
            $scope.sendWork($label, $item);
            console.log("modelnya", $scope.lmModel);
            // console.log('materialArray',materialArray);
        }
        $scope.onSelectParts = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectParts=>", $item);
            console.log("onSelectParts=>", $model);
            console.log("onSelectParts=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            if ($item.PartsName !== null) {
                $scope.partsAvailable = true;
            } else {
                $scope.partsAvailable = false;
            }
            $scope.sendParts($label, $item);
            console.log("modelnya", $item);
            // console.log('materialArray',materialArray);
        }
        $scope.onSelectOpeNo = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectParts=>", $item);
            console.log("onSelectParts=>", $model);
            console.log("onSelectParts=>", $label);
            $scope.tmpAdditionalTaskId = $item.TaskId;
            // console.log('materialArray',materialArray);
        }
        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
            $scope.listApiJob.clearDetail();
            var row = $scope.lmModel;
            row.FlatRate = "";
            row.Fare = "";
            row.Summary = "";
            row.TaskId = null;
            row.tmpTaskId = null;
            row.catName = $scope.tmpCatg.Name;
            // row.cusType =$scope.tmpCus.Name;
            $scope.listApiJob.addDetail(false, row);
            console.log("uunnnnchhh", $scope.lmModel)
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = true;
        };
        $scope.onNoPartResult = function() {
            console.log("onGotResult=>");
            $scope.partsAvailable = false;
            $scope.ldModel.PartsName = null;
            $scope.ldModel.RetailPrice = null;
            $scope.ldModel.priceDp = null;
            $scope.ldModel.DownPayment = null;
            $scope.ldModel.ETA = null;
            $scope.ldModel.SatuanId = null;
            $scope.ldModel.Availbility = null;
            $scope.ldModel.satuanName = null;
            $scope.ldModel.PartsId = null;
            $scope.ldModel.PartId = null;
            $scope.ldModel.TaskId = null;
            // $scope.partsAvailable = true;
            $scope.disableDP = true;
            // $scope.ldModel.paidName = null;
        };
        $scope.onNoOpeResult = function() {
            $scope.tmpAdditionalTaskId = null;
        }
        $scope.onGotResult = function(data) {
            console.log("onGotResult=>", data);
            // $scope.partsAvailable = true;
            $scope.disableDP = false;
            $scope.PriceAvailable = true;
        }
        $scope.selected = {};
        $scope.selectTypeWork = function(data) {
            console.log("selectTypeWork", data);
            $scope.tmpCatg = data;
            var tmpCatgName = data.Name
            if (tmpCatgName == "TWC" || tmpCatgName == "PWC") {
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Waranty") {
                        $scope.tmpPaidById = val.MasterId;
                        $scope.tmpPaidName = val.Name;
                        $scope.lmModel.JobTypeId = data.MasterId;
                        $scope.lmModel.PaidById = val.MasterId;
                        // $scope.listApi.addDetail(false, $scope.lmModel);
                    }
                })
            } else {
                $scope.lmModel.JobTypeId = data.MasterId
                $scope.tmpPaidById = null;
                $scope.lmModel.PaidById = null;
                // $scope.listApi.addDetail(false, $scope.lmModel);
            }
            console.log("gridTemp", gridTemp);
            console.log("gridWork", $scope.lmModel);
        };
        $scope.selectTypeCust = function(data) {
            console.log("selectTypeCust", data);
            $scope.tmpCus = data;
            $scope.lmModel.PaidBy = data.Name;
        }
        $scope.selectTypeUnitParts = function(data) {
            console.log("selectTypeUnitParts", data);
            $scope.tmpUnit = data;
            $scope.ldModel.satuanName = data.Name;
            console.log("selectTypeUnitParts", $scope.ldModel);
        }
        $scope.selectTypePaidParts = function(data) {
            console.log("selectTypePaidParts", data);
            $scope.tmpPaidName = data;
            $scope.ldModel.PaidBy = data.Name;
            console.log("selectTypePaidParts", $scope.ldModel);
            if (data.Name == "Internal" || data.Name == "Warranty" || data.Name == " Free Service") {
                $scope.ldModel.DownPayment = 0;
                $scope.disableDP = true;
            } else {
                $scope.checkPrice($scope.ldModel.RetailPrice);
                $scope.disableDP = false;
            };
        };
        $scope.selectTypeDiskonTask = function(data, item) {
            console.log("data", data);
            // var tmpItem = angular.copy(item);
            if (data.MasterId == 0 && data !== undefined) {
                $scope.isCustomTask = true;
            } else if (data.MasterId == -1 && data !== undefined) {
                var tmpModel = angular.copy(data);
                console.log("var tmpModel = angular.copy($scope.lmModel);", tmpModel);
                // $scope.lmModel.Discount = 0;
                $scope.isCustomTask = false;
                tmpModel.Discount = 0;
                $scope.listApiJob.addDetail(false, tmpModel);
            } else if (data.MasterId == 2 && data !== undefined) {
                var tmpModel = angular.copy(data);
                console.log("var tmpModel = angular.copy($scope.lmModel);", tmpModel);
                _.map($scope.tmpDiskonListData.MProfileCustomerGroupTR, function(val) {
                    val.Name = val.MCustomerGroup.GroupCustomerName;
                    val.Id = val.MCustomerGroup.GroupCustomerId;
                });
                $scope.isCustomTask = false;
                $scope.diskonListData = $scope.tmpDiskonListData.MProfileCustomerGroupTR;
            } else if (data.MasterId == 1 && data !== undefined) {
                var tmpModel = angular.copy(data);
                console.log("var tmpModel = angular.copy($scope.lmModel);", tmpModel);
                // $scope.lmModel.Discount = 0;
                _.map($scope.tmpDiskonListCampaign, function(val) {
                    val.Name = val.CampaignDiscountName;
                    val.Id = val.CampaignDiscountId;
                });
                $scope.isCustomTask = false;
                // tmpModel.Discount = 0;
                $scope.diskonListData = $scope.tmpDiskonListCampaign;
            } else {
                $scope.isCustomTask = false;
            };
        };
        $scope.selectTypeListDiskonTask = function(data, param) {
            var tmpModel = angular.copy($scope.lmModel);
            var tmpData = angular.copy($scope.tmpLastParts);
            console.log("var tmpModel = angular.copy($scope.lmModel);", tmpModel);
            if (param == 2) {
                // =====================
                tmpModel.Discount = data.MCustomerGroup.DiscountJasa;
                if (tmpData.length > 0) {
                    $scope.listApiJob.clearDetail();
                    for (var i in tmpData) {
                        tmpData[i].typeDiskon = 2;
                        tmpData[i].Discount = data.MCustomerGroup.DiscountParts;
                        tmpData[i].listTypeDiskon = data.MCustomerGroup.GroupCustomerId;
                        $scope.listApiJob.addDetail(tmpData[i], tmpModel);
                    }
                } else {
                    $scope.listApiJob.addDetail(false, tmpModel);
                }
                // ===========================
            } else if (param == 1) {
                console.log("pilihCampaign berarti");
                var resultDiscountTask = _.find($scope.tmpDiskonListCampaignTasks, {
                    "TaskId": $scope.tmpDiskonTaskId
                });
                // var resultDiscountPart = _.intersectionWith($scope.tmpDiskonListCampaignParts, tmpData, _.isEqual);
                // console.log("intersection result : ",resultDiscountPart);
                if (resultDiscountTask !== undefined) {
                    tmpModel.Discount = resultDiscountTask.Discount;
                } else {
                    tmpModel.Discount = 0;
                    console.log("Discount", resultDiscountTask);
                }
                if (tmpData.length > 0) {
                    $scope.listApiJob.clearDetail();
                    for (var i in $scope.tmpDiskonListCampaignParts) {
                        for (var j in tmpData) {
                            if ($scope.tmpDiskonListCampaignParts[i].PartsId === tmpData[j].PartsId) {
                                tmpData[j].typeDiskon = 1;
                                tmpData[j].listTypeDiskon = $scope.tmpDiskonListCampaign[0].CampaignDiscountId;
                                tmpData[j].Discount = $scope.tmpDiskonListCampaignParts[i].Discount;
                            } else {
                                tmpData[j].Discount = 0;
                            }
                            $scope.listApiJob.addDetail(tmpData[j], tmpModel);
                        }
                    }
                } else {
                    $scope.listApiJob.addDetail(false, tmpModel);
                }
            }
            // ============================
        };
        $scope.selectTypeDiskonParts = function(data) {
            console.log("data", data);
            if (data.MasterId == 0 && data !== undefined) {
                $scope.isCustomPart = true;
            } else if (data.MasterId == -1 && data !== undefined) {
                $scope.ldModel.Discount = 0;
            } else {
                $scope.isCustomPart = false;
            }
        }
        $scope.onListSave = function(item) {
            console.log("save_modal=>", item);
            // if(item[$scope.id]){ //Update
            //   //find the master Id
            //   var key = {};
            //   key[$scope.mId] = item[$scope.mId];
            //   var match = _.find($scope.contacts, key);
            //   var xitem = angular.copy(item);
            //   if(match){
            //       var index = _.indexOf($scope.contacts, _.find($scope.contacts, key));
            //       $scope.contacts.splice(index, 1, xitem);
            //   }
            // }else{
            //   $scope.contacts.push(item);
            // }
        }
        $scope.onListCancel = function(item) {
            console.log("cancel_modal=>", item);
        }
        $scope.onShowModal = function(mode, data) {
            console.log("====", mode);
            console.log("=====", data);
        }
        $scope.onBeforeSave = function(data, mode) {
            console.log("onBeforeSave data", data);
            console.log("onBeforeSave mode", mode);

            var tmpData = angular.copy(data);
            var tmpGridWork = $scope.gridWork;
            var lengthGrid = tmpGridWork.length - 1;
            if ($scope.tmpCus !== undefined) {
                tmpData.PaidBy = $scope.tmpCus.Name;
            }
            if (tmpData.isOpe == 1) {
                tmpData.AdditionalTaskId = $scope.tmpAdditionalTaskId;
            } else {
                tmpData.AdditionalTaskId = null;
            }

            // tmpData.PaidBy = $scope.tmpCus.Name;
            if (mode == 'new') {
                if (tmpData.tmpTaskId == null) {
                    if (tmpGridWork.length > 0) {
                        tmpData.tmpTaskId = tmpGridWork[lengthGrid].tmpTaskId + 1;
                    } else {
                        tmpData.tmpTaskId = 1;
                    }
                }
                if (data.child.length > 0) {
                    var tmpDataChild = angular.copy(data.child);
                    // for (var i = 0; i < tmpDataChild.length; i++) {
                    //     delete data.child[i];
                    // }
                    $scope.listApiJob.addDetail(tmpDataChild, tmpData);
                } else {
                    $scope.listApiJob.addDetail(false, tmpData);
                };
            } else {
                $scope.listApiJob.addDetail(false, tmpData);
            }
        }
        $scope.onAfterSave = function(data, mode) {
            console.log("onAfterSave data", data);
            console.log("onAfterSave mode", mode);
            console.log("ini sih wkwwk", $scope.taskCategory);
            $scope.estimateAsb();
            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Summary !== undefined) {
                    if (gridTemp[i].Summary == "") {
                        gridTemp[i].Summary = gridTemp[i].Fare;
                    }
                    totalW += gridTemp[i].Summary;
                }
            };
            $scope.totalWork = totalW;
            var totalM = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].subTotal !== undefined) {
                            totalM += gridTemp[i].child[j].subTotal;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].subTotal);
                        }
                    }
                }
            }
            $scope.totalMaterial = totalM;
            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== undefined) {
                            totalDp += gridTemp[i].child[j].DownPayment;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;

            var totalWD = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].DiscountedPrice !== undefined) {
                    totalWD += gridTemp[i].DiscountedPrice;
                }
            };
            $scope.totalWorkDiscounted = totalWD;
            var totalMD = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DiscountedPrice !== undefined) {
                            totalMD += gridTemp[i].child[j].DiscountedPrice;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DiscountedPrice);
                        }
                    }
                }
            };
            $scope.totalMaterialDiscounted = totalMD;
            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== undefined) {
                            totalDp += gridTemp[i].child[j].DownPayment;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DownPayment);
                        }
                    }
                }
            };
            $scope.totalDp = totalDp;
            var totalDefaultDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += gridTemp[i].child[j].minimalDp;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].minimalDp);
                        }
                    }
                }
            };
            $scope.totalDpDefault = totalDefaultDp;
            console.log("gridTemp", gridTemp);
            console.log("gridWork", $scope.gridWork);
        };
        $scope.onAfterDelete = function(data) {
            for (var i = 0; i < data.length; i++) {
                $scope.gridDeleted.push({
                    JobTaskId: data[i]
                });
            }
            console.log("$scope.gridDeleted", $scope.gridDeleted);
            $scope.estimateAsb();
            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Summary !== undefined) {
                    if (gridTemp[i].Summary == "") {
                        gridTemp[i].Summary = gridTemp[i].Fare;
                    }
                    totalW += gridTemp[i].Summary;
                }
            };
            $scope.totalWork = totalW;
            // ============
            var totalWD = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].DiscountedPrice !== undefined) {
                    totalWD += gridTemp[i].DiscountedPrice;
                }
            };
            $scope.totalWorkDiscounted = totalWD;
            var totalMD = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DiscountedPrice !== undefined) {
                            totalMD += gridTemp[i].child[j].DiscountedPrice;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DiscountedPrice);
                        }
                    }
                }
            };
            $scope.totalMaterialDiscounted = totalMD;
            // ============
            var totalM = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].subTotal !== undefined) {
                            totalM += gridTemp[i].child[j].subTotal;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].subTotal);
                        }
                    }
                }
            }
            $scope.totalMaterial = totalM;
            // ============
            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== undefined) {
                            totalDp += gridTemp[i].child[j].DownPayment;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
        };
        // $scope.onSelectUser = function(item, model, label){
        //     console.log("onSelectUser=>",item,model,label);
        // }
        //====================
        $scope.checkPrice = function(data) {
            console.log("RetailPrice nya ===>", data);
            console.log("$scope.ldModel.Qty nya ==>", $scope.ldModel.Qty);
            var sum = 0;
            var sumDp = 0;
            if (data !== undefined && data !== null) {
                if ($scope.ldModel.Qty !== null) {
                    var qty = $scope.ldModel.Qty;
                    var tmpDp = $scope.ldModel.nightmareIjal;
                    if (qty !== null) {
                        sum = Math.round(qty * data);
                        sumDp = Math.round(qty * tmpDp);
                        $scope.ldModel.subTotal = sum;
                        $scope.ldModel.DownPayment = sumDp;
                        $scope.ldModel.DPRequest = sumDp;
                        $scope.ldModel.minimalDp = sumDp;
                        $scope.ldModel.Discount = 0;

                    } else {
                        $scope.ldModel.subTotal = 0;
                        $scope.ldModel.Discount = 0;
                        $scope.ldModel.DownPayment = Math.round(tmpDp);
                        $scope.ldModel.DPRequest = Math.round(tmpDp);
                        $scope.ldModel.minimalDp = Math.round(tmpDp);
                    }
                }
            } else {
                $scope.ldModel.subTotal = 0;
                $scope.ldModel.Discount = 0;
                $scope.ldModel.DownPayment = 0;
                $scope.ldModel.DPRequest = 0;
                $scope.ldModel.minimalDp = 0;
            }
        };
        $scope.onAfterSaveOpl = function() {
            var totalWOPL = 0;
            for (var i = 0; i < $scope.oplData.length; i++) {
                if ($scope.oplData[i].Price !== undefined) {
                    totalWOPL += $scope.oplData[i].Price;
                }
            };
            $scope.sumWorkOpl = totalWOPL;

            var totalDiscountedPrice = 0;
            for (var i = 0; i < $scope.oplData.length; i++) {
                if ($scope.oplData[i].DiscountedPriceOPL !== undefined) {
                    totalDiscountedPrice += $scope.oplData[i].DiscountedPriceOPL;
                }
            };
            $scope.sumWorkOplDiscounted = totalDiscountedPrice;
            console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
        };
        // ===================================================
        $scope.isNew = false;
        $scope.onBeforeNewOPL = function() {
            $scope.isNew = true;
        }
        $scope.onBeforeEditOPL = function() {
                $scope.isNew = false;
            }
            // ============Added Code=============================
        $scope.onBeforeNew = function() {
            $scope.isRelease = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false;
            $scope.partsAvailable = false
            $scope.isCustomDiskon = false;

            $scope.getGrouCustomerDiscount();
            // console.log("$scope.mDataCrm.Customer", $scope.mDataCrm.Customer);
            console.log('onBeforeNew $scope.mData', $scope.mData);
            console.log('onBeforeNew $scope.mDataCrm', $scope.mDataCrm);
            var tmpDate = new Date();
            var finalDate
            var yyyy = tmpDate.getFullYear().toString();
            var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = tmpDate.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            if ($scope.mData.Km !== undefined) {
                $scope.mDataCrm.Vehicle.Type.VehicleTypeId = 4693;
                WoHistoryGR.getDiscountCampaignSecond(finalDate, $scope.mData, $scope.mDataCrm, 0).then(function(res) {
                    var tmpDiskonCampaign = res.data.Result;
                    console.log("WoHistoryGR.getDiscountCampaign", res.data);
                    if (tmpDiskonCampaign.length > 0) {
                        $scope.tmpDiskonListCampaign = tmpDiskonCampaign;
                        $scope.tmpDiskonListCampaignTasks = [];
                        $scope.tmpDiskonListCampaignParts = [];
                        for (var i in $scope.tmpDiskonListCampaign[0].MCampaignDiscount_Type_TR) {
                            if ($scope.tmpDiskonListCampaign[0].MCampaignDiscount_Type_TR[i].VehicleTypeId == $scope.mDataCrm.Vehicle.Type.VehicleTypeId) {
                                if ($scope.tmpDiskonListCampaign[0].MCampaignDiscount_Type_TR[i].MCampaignDiscountType_TaskList_TR.length > 0) {
                                    for (var t in $scope.tmpDiskonListCampaign[0].MCampaignDiscount_Type_TR[i].MCampaignDiscountType_TaskList_TR) {
                                        $scope.tmpDiskonListCampaignTasks.push($scope.tmpDiskonListCampaign[0].MCampaignDiscount_Type_TR[i].MCampaignDiscountType_TaskList_TR[t]);
                                    }
                                }
                                if ($scope.tmpDiskonListCampaign[0].MCampaignDiscount_Type_TR[i].MCampaignDiscountType_Part_TR.length > 0) {
                                    for (var p in $scope.tmpDiskonListCampaign[0].MCampaignDiscount_Type_TR[i].MCampaignDiscountType_Part_TR) {
                                        $scope.tmpDiskonListCampaignParts.push($scope.tmpDiskonListCampaign[0].MCampaignDiscount_Type_TR[i].MCampaignDiscountType_Part_TR[p]);
                                    }
                                }
                            }
                        }
                        if ($scope.diskonData.length < 4) {
                            $scope.diskonData.push({
                                MasterId: 1,
                                Name: "Campaign Diskon"
                            });
                        }
                        console.log("onBeforeNew $scope.diskonData", $scope.diskonData);
                    }

                });
            }
            var tmpTask = {}
            tmpTask.isOpe = 0;
            $scope.listApiJob.addDetail(false, tmpTask);
        }

        $scope.getGrouCustomerDiscount = function() {
            console.log("$scope.mDataCrm.Customer", $scope.mDataCrm.Customer);
            WO.getDiscountMGroupCustomer($scope.mDataCrm.Customer).then(function(res) {
                var tmpdataDiskon = res.data.Result;
                if (tmpdataDiskon.length > 0) {
                    $scope.tmpDiskonListData = tmpdataDiskon[0];
                    $scope.diskonData.push({
                        MasterId: 2,
                        Name: "Group Customer Diskon"
                    });
                }
            });
        };

        $scope.onAfterCancel = function() {
            $scope.isRelease = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false;
            $scope.partsAvailable = false;
            $scope.diskonData = [];
            console.log("$scope.diskonData.length", $scope.diskonData.length);
            // if($scope.diskonData.length = 0){

            $scope.diskonData = [{
                MasterId: -1,
                Name: "Tidak Ada Diskon",
            }, {
                MasterId: 0,
                Name: "Custom Diskon"
            }];
            // }

        }

        $scope.PriceIsTam = false;
        $scope.onBeforeEdit = function(row) {
            console.log("$scope.mData.Status=====================>", $scope.mData.Status, row);
            if ($scope.mData.Status == 4) {
                $scope.isRelease = false;
                $scope.PriceAvailable = false;
                $scope.partsAvailable = true;
                if ($scope.isTAM == 1) {
                    $scope.PriceIsTam = true;
                } else {
                    $scope.PriceIsTam = false;
                }
                console.log("$scope.PriceIsTam", $scope.PriceIsTam);
            } else if ($scope.mData.Status > 4 && $scope.mData.Status < 8) {
                $scope.isRelease = true;
                $scope.FlatRateAvailable = false;
                $scope.PriceAvailable = true;
                // $scope.partsAvailable = false;
                $scope.partsAvailable = true;
            } else if ($scope.mData.Status == 11) {
                $scope.isRelease = true;
                $scope.FlatRateAvailable = true;
                $scope.PriceAvailable = true;
                $scope.partsAvailable = true;
            }
            // ===========================
            if (row.IsCustomDiscount == 0) {
                $scope.isCustomDiskon = true;
                $scope.isCustomTask = false;
            } else {
                $scope.isCustomDiskon = true;
                $scope.isCustomTask = true;
            }
        };

        function getImgAndPoints(mstId) {
            console.log("mstId", mstId);
            switch (mstId) {
                case 2221:
                    $scope.imageSel = "../images/wacImages/Type A.png";
                    break;
                case 2222:
                    $scope.imageSel = "../images/wacImages/Type B.png";
                    break;
                case 2223:
                    $scope.imageSel = "../images/wacImages/Type C.png";
                    break;
                case 2224:
                    $scope.imageSel = "../images/wacImages/Type D.png";
                    break;
                case 2225:
                    $scope.imageSel = "../images/wacImages/Type E.png";
                    break;
                case 2226:
                    $scope.imageSel = "../images/wacImages/Type F.png";
                    break;
                case 2227:
                    $scope.imageSel = "../images/wacImages/Type G.png";
            }
            var temp = _.find($scope.imageWACArr, function(o) {
                return o.TypeId == mstId;
            });
            console.log("temp", temp);

            return JSON.parse(temp.Points);
        };

        var dotsData = {
            2221: [],
            2222: [],
            2223: [],
            2224: [],
            2225: [],
            2226: [],
            2227: []
        };

        $scope.accordionWac = {
            isOpen: false
        };
        $scope.clickWACcount = 0;
        $scope.dataForWAC = function() {
            if ($scope.clickWACcount == 0) {
                RepairProcessGR.getDataWac($scope.mData.JobId).then(
                    function(res) {
                        var dataWac = res.data;
                        $scope.finalDataWac = dataWac;

                        console.log("Data WAC", dataWac);
                        $scope.mData.OtherDescription = dataWac.Other;
                        dataWac.MoneyAmount = String(dataWac.MoneyAmount);
                        $scope.mData.moneyAmount = dataWac.MoneyAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        // $scope.mData.moneyAmount = dataWac.MoneyAmount;
                        $scope.mData.BanSerep = dataWac.isSpareTire;
                        $scope.mData.CdorKaset = dataWac.isCD;
                        $scope.mData.Payung = dataWac.isUmbrella;
                        $scope.mData.Dongkrak = dataWac.isJack;
                        $scope.mData.STNK = dataWac.isVehicleRegistrationNumber;
                        $scope.mData.P3k = dataWac.isP3k;
                        $scope.mData.KunciSteer = dataWac.isSteerLock;
                        $scope.mData.ToolSet = dataWac.isToolSet;
                        $scope.mData.ClipKarpet = dataWac.isCarpetClip;
                        $scope.mData.BukuService = dataWac.isServiceBook;
                        $scope.mData.AlarmCondition = dataWac.isAlarmConditionOk;
                        $scope.mData.AcCondition = dataWac.isAcOk;
                        $scope.mData.PowerWindow = dataWac.isPowerWindowOk;
                        $scope.mData.CarType = dataWac.isPowerWindowOk;
                        // $scope.slider.options = 100;
                        $scope.slider.value = dataWac.Fuel;
                        // $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                RepairProcessGR.getProbPoints($scope.mData.JobId, null).then(
                    function(res) {
                        console.log("RepairProcessGR.getProbPoints", res.data);
                        mstId = angular.copy(res.data.Result[0].TypeId);
                        console.log("ini MstId", mstId);
                        $scope.filter.vType = mstId;
                        console.log("probPoints", res.data.Result);
                        // $scope.chooseVType(mstId);
                        $scope.areasArray = getImgAndPoints(mstId);
                        console.log("$scope.areasArray", $scope.areasArray);
                        if (res.data.Result.length > 0) {
                            for (var i in res.data.Result) {
                                var getTemp = JSON.parse(res.data.Result[i]["Points"]);
                                for (var j in getTemp) {
                                    var xtemp = angular.copy(getTemp[j]);
                                    delete xtemp['status'];
                                    console.log("xtemp : ", xtemp);
                                    if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                                        console.log("find..", xtemp);
                                        var idx = _.findIndex($scope.areasArray, xtemp);
                                        xtemp.cssClass = "choosen-area";
                                        $scope.areasArray[idx] = xtemp;

                                        dotsData[mstId].push({
                                            "ItemId": xtemp.areaid,
                                            "ItemName": xtemp.name,
                                            "Points": xtemp,
                                            "Status": getTemp[j].status
                                        });
                                    }
                                }
                            }
                            // setDataGrid(dotsData[mstId]);
                        }
                    },
                    function(err) {
                        $scope.areasArray = getImgAndPoints(mstId);
                    }
                );
                $scope.clickWACcount++;
            } else {
                console.log("di click mulu, rese but sih lu");
            }

        }
        $scope.discountedPrice = null;
        $scope.normalPrice = null;
        $scope.summaryy = null;
        // ===========================
        // Allow Pattern
        // ===========================
        $scope.allowPattern = function(event, type, item) {
            if (event.charCode == 13) {
                $scope.getData();
            }

            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]|[/]|[-]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            }
            console.log("event", event);
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                console.log("allowPattern jin2", item);
                event.preventDefault();
                return false;
            }
        };
        $scope.getAvailablePartsServiceManual = function(item, x, y) {
            console.log("getAvailablePartsServiceManual", item);
            console.log("x : ", x);
            console.log("y : ", y);

            if (x > item.length - 1) {
                return item;
            }
            if (item[x].isDeleted !== 1) {
                if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                    console.log("punyaaaa part data lengkap", x);
                    var itemTemp = item[x].JobParts[y];
                    AppointmentGrService.getAvailableParts(itemTemp.PartsId).then(function(res) {
                        var tmpParts = res.data.Result[0];
                        if (tmpParts !== null && tmpParts !== undefined) {
                            // console.log("have data RetailPrice",item,tmpRes);
                            item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                            item[x].JobParts[y].priceDp = tmpParts.PriceDP;
                            item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                            if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                item[x].JobParts[y].typeDiskon = -1;
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                item[x].JobParts[y].typeDiskon = 0;
                            }
                            if (tmpParts.isAvailable == 0) {
                                item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                    item[x].JobParts[y].ETA = "";
                                } else {
                                    item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                }
                                item[x].JobParts[y].Type = 3;
                            } else {
                                item[x].JobParts[y].Availbility = "Tersedia";
                            }
                        } else {
                            console.log("haven't data RetailPrice");
                            item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                            item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                item[x].JobParts[y].typeDiskon = -1;
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                item[x].JobParts[y].typeDiskon = 0;
                            }
                        }
                        if (item[x].JobParts[y].PartsId !== null) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].PaidBy != undefined && item[x].JobParts[y].PaidBy != null) {
                            item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                        }

                        Parts.push(item[x].JobParts[y]);
                        $scope.sumAllPrice();
                        // return item;

                        if (x <= (item.length - 1)) {
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    var summ = item[x].FlatRate;
                                    var summaryy = sum * summ;
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(summaryy);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = {
                                        Name: ""
                                    };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = {
                                        Name: ""
                                    }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].typeDiskon = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].typeDiskon = 0;
                                    // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                    item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);
                                }
                                gridTemp.push({
                                    ActualRate: item[x].ActualRate,
                                    AdditionalTaskId: item[x].AdditionalTaskId,
                                    isOpe: item[x].isOpe,
                                    Discount: item[x].Discount,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    DiscountedPrice: item[x].DiscountedPrice,
                                    typeDiskon: item[x].typeDiskon,
                                    NormalPrice: normalPrice,
                                    catName: item[x].JobType.Name,
                                    Fare: item[x].Fare,
                                    IsCustomDiscount: item[x].IsCustomDiscount,
                                    Summary: summaryy,
                                    PaidById: item[x].PaidById,
                                    JobId: item[x].JobId,
                                    TaskId: item[x].TaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    PaidBy: item[x].PaidBy.Name,
                                    index: "$$" + x,
                                    child: Parts
                                });
                                $scope.sumAllPrice();
                                Parts.splice();
                                Parts = [];
                                console.log("if 1 nilai x", x);
                                $scope.getAvailablePartsServiceManual(item, x + 1, 0);

                            } else {
                                $scope.getAvailablePartsServiceManual(item, x, y + 1);
                            }
                        }
                    });
                } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                    console.log("punyaaaa part data yg gak punya part id", x);
                    item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                    item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                    item[x].JobParts[y].DownPayment = item[x].JobParts[y].DownPayment * item[x].JobParts[y].Qty;
                    if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                        item[x].JobParts[y].typeDiskon = -1;
                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                    } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                        item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                        item[x].JobParts[y].typeDiskon = 0;
                    }
                    tmp = item[x].JobParts[y];
                    Parts.push(item[x].JobParts[y]);
                    $scope.sumAllPrice();

                    if (x <= (item.length - 1)) {
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                var summ = item[x].FlatRate;
                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy
                                summaryy = parseInt(summaryy);
                            }
                            if (item[x].JobType == null) {
                                item[x].JobType = {
                                    Name: ""
                                };
                            }
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = {
                                    Name: ""
                                }
                            }
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            }
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].typeDiskon = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].typeDiskon = 0;
                                // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);
                            }

                            gridTemp.push({
                                ActualRate: item[x].ActualRate,
                                AdditionalTaskId: item[x].AdditionalTaskId,
                                isOpe: item[x].isOpe,
                                Discount: item[x].Discount,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                DiscountedPrice: item[x].DiscountedPrice,
                                typeDiskon: item[x].typeDiskon,
                                NormalPrice: normalPrice,
                                catName: item[x].JobType.Name,
                                Fare: item[x].Fare,
                                IsCustomDiscount: item[x].IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: item[x].PaidById,
                                JobId: item[x].JobId,
                                TaskId: item[x].TaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                PaidBy: item[x].PaidBy.Name,
                                index: "$$" + x,
                                child: Parts
                            });
                            $scope.sumAllPrice();
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                            console.log("if 2 nilai x", x);
                        } else {
                            $scope.getAvailablePartsServiceManual(item, x, y + 1);
                        }
                    }
                } else if (item[x].JobParts.length == 0) {
                    console.log("gak punya parts", x);
                    if (x <= (item.length - 1)) {
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                var summ = item[x].FlatRate;
                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy
                                summaryy = parseInt(summaryy);
                            }
                            if (item[x].JobType == null) {
                                item[x].JobType = {
                                    Name: ""
                                };
                            }
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = {
                                    Name: ""
                                }
                            }
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            }
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].typeDiskon = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].typeDiskon = 0;
                                // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);

                            }
                            gridTemp.push({
                                ActualRate: item[x].ActualRate,
                                AdditionalTaskId: item[x].AdditionalTaskId,
                                isOpe: item[x].isOpe,
                                Discount: item[x].Discount,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                DiscountedPrice: item[x].DiscountedPrice,
                                typeDiskon: item[x].typeDiskon,
                                NormalPrice: normalPrice,
                                catName: item[x].JobType.Name,
                                Fare: item[x].Fare,
                                IsCustomDiscount: item[x].IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: item[x].PaidById,
                                JobId: item[x].JobId,
                                TaskId: item[x].TaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                PaidBy: item[x].PaidBy.Name,
                                index: "$$" + x,
                                child: Parts
                            });
                            $scope.sumAllPrice();
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                        } else {
                            $scope.getAvailablePartsServiceManual(item, x, y + 1);
                        }
                    }
                    // return item;
                }

                if (x > item.length - 1) {
                    return item;
                }

                // gridTemp.push({
                //     ActualRate: item[x].ActualRate,
                //     AdditionalTaskId:item[x].AdditionalTaskId,
                //     isOpe:item[x].isOpe,
                //     Discount: item[x].Discount,
                //     JobTaskId: item[x].JobTaskId,
                //     JobTypeId: item[x].JobTypeId,
                //     DiscountedPrice:$scope.discountedPrice,
                //     typeDiskon: item[x].typeDiskon,
                //     NormalPrice:$scope.normalPrice,
                //     catName: item[x].JobType.Name,
                //     Fare: item[x].Fare,
                //     IsCustomDiscount: item[x].IsCustomDiscount,
                //     Summary: $scope.summaryy,
                //     PaidById: item[x].PaidById,
                //     JobId: item[x].JobId,
                //     TaskId: item[x].TaskId,
                //     TaskName: item[x].TaskName,
                //     FlatRate: item[x].FlatRate,
                //     ProcessId: item[x].ProcessId,
                //     TFirst1: item[x].TFirst1,
                //     PaidBy: item[x].PaidBy.Name,
                //     child: Parts
                // });
                // Parts.splice();
                // console.log("tmpJob : ",item);
            }
        };
        // ===========Backup=====================
        var Parts = [];
        $scope.dataForBslist = function(data) {
            gridTemp = []
            Parts.splice();
            Parts = [];
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                for (var i = 0; i < tmpJob.length; i++) {
                    console.log("tmpJob", tmpJob);
                    //
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        console.log("tmpJob[i].JobParts[j]", tmpJob[i].JobParts[j]);
                        if (tmpJob[i].JobParts[j].PaidBy !== null) {
                            tmpJob[i].JobParts[j].paidName = tmpJob[i].JobParts[j].PaidBy.Name;
                            delete tmpJob[i].JobParts[j].PaidBy;
                        }
                        if (tmpJob[i].JobParts[j].Satuan !== null) {
                            tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
                            delete tmpJob[i].JobParts[j].Satuan;
                        }
                        tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                        // $scope.getAvailablePartsServiceManual(tmpJob[i].JobParts[j]);

                        // Parts.push(tmpJob[i].JobParts[j]);
                    }

                    // if(tmpJob[i].JobType == null){
                    //     tmpJob[i].JobType = {Name:""};
                    // }
                    // if(tmpJob[i].PaidBy == null){
                    //     tmpJob[i].PaidBy = {Name:""}
                    // }
                    // if(tmpJob[i].AdditionalTaskId == null){
                    //     tmpJob[i].isOpe = 0;
                    // }else{
                    //     tmpJob[i].isOpe = 1;
                    // }
                    // if(tmpJob[i].IsCustomDiscount == 0 && tmpJob[i].Discount == 0 ){
                    //     tmpJob[i].typeDiskon = -1;
                    //     tmpJob[i].DiscountedPrice =  normalPrice;
                    // }else if(tmpJob[i].IsCustomDiscount == 1 && tmpJob[i].Discount !== 0){
                    //     tmpJob[i].typeDiskon = 0;
                    //     tmpJob[i].DiscountedPrice =  (normalPrice * tmpJob[i].Discount)/100;

                    // }
                    // gridTemp.push({
                    //     ActualRate: tmpJob[i].ActualRate,
                    //     AdditionalTaskId:tmpJob[i].AdditionalTaskId,
                    //     isOpe:tmpJob[i].isOpe,
                    //     Discount: tmpJob[i].Discount,
                    //     JobTaskId: tmpJob[i].JobTaskId,
                    //     JobTypeId: tmpJob[i].JobTypeId,
                    //     DiscountedPrice:discountedPrice,
                    //     typeDiskon: tmpJob[i].typeDiskon,
                    //     NormalPrice:normalPrice,
                    //     catName: tmpJob[i].JobType.Name,
                    //     Fare: tmpJob[i].Fare,
                    //     IsCustomDiscount: tmpJob[i].IsCustomDiscount,
                    //     Summary: summaryy,
                    //     PaidById: tmpJob[i].PaidById,
                    //     JobId: tmpJob[i].JobId,
                    //     TaskId: tmpJob[i].TaskId,
                    //     TaskName: tmpJob[i].TaskName,
                    //     FlatRate: tmpJob[i].FlatRate,
                    //     ProcessId: tmpJob[i].ProcessId,
                    //     TFirst1: tmpJob[i].TFirst1,
                    //     PaidBy: tmpJob[i].PaidBy.Name,
                    //     child: Parts
                    // });
                }
                var realJob = [];
                for (var i = 0; i < tmpJob.length; i++) {
                    if (tmpJob[i].isDeleted == 0) {
                        realJob.push(data.JobTask[i]);
                    }
                }
                // added by sss on 2017-11-03
                $scope.getAvailablePartsServiceManual(realJob, 0, 0);
                //
                $scope.gridWork = gridTemp;
                $scope.gridOriginal = gridTemp;
                // $scope.onAfterSave();
                // console.log("totalw", $scope.totalWork);
            }
            // $scope.sumAllPrice();
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
        };
        $scope.dataForPrediagnose = function(data) {
            FollowUpGrService.getPreDiagnoseByJobId(data.JobId).then(function(res) {
                tmpPrediagnose = res.data;
                console.log("resss Prediagnosenya bosque", res.data);
            });
        };
        // ========================
        $scope.modalModepreDiagnose = 'new';
        $scope.CountShiftPrediagnose = function() {
            $scope.dataPrediagnose.PositionShiftLever = ($scope.dataPrediagnose.PosisiShiftP == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftP) + ($scope.dataPrediagnose.PosisiShiftN == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftN) + ($scope.dataPrediagnose.PosisiShiftD == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftD) + ($scope.dataPrediagnose.PosisiShiftS == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftS) + ($scope.dataPrediagnose.PosisiShiftR == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftR) + ($scope.dataPrediagnose.PosisiShiftOne == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftOne) + ($scope.dataPrediagnose.PosisiShiftTwo == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftTwo) + ($scope.dataPrediagnose.PosisiShiftThree == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftThree) + ($scope.dataPrediagnose.PosisiShiftFour == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftFour) + ($scope.dataPrediagnose.PosisiShiftLainnya == undefined ? 0 : $scope.dataPrediagnose.PosisiShiftLainnya);
        }

        $scope.preDiagnose = function() {
            $scope.showPrediagnose = true;
            $scope.dataPrediagnose = angular.copy(tmpPrediagnose);
            console.log("tmpPrediagnose", tmpPrediagnose);

            //untuk bagian Posisi Shift Lever
            var co = ["PosisiShiftP", "PosisiShiftN", "PosisiShiftD", "PosisiShiftS", "PosisiShiftR", "PosisiShiftOne", "PosisiShiftTwo", "PosisiShiftThree", "PosisiShiftFour", "PosisiShiftLainnya"];

            var total = $scope.dataPrediagnose.PositionShiftLever;
            console.log("total", total);
            var arrBin = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512];
            for (var i in arrBin) {
                if (total & arrBin[i]) {
                    var xc = co[i];
                    $scope.dataPrediagnose[xc] = arrBin[i];

                    console.log("scope.dataPrediagnose[xc]", $scope.dataPrediagnose[xc], "arrBin[i]", arrBin[i]);


                    // console.log(co[i]+' : nyala');
                }
            }
            //akhir bagian posisi shift lever
        };
        $scope.cancelPreDiagnose = function() {
            $scope.showPrediagnose = false;
        };
        $scope.savePreDiagnose = function(data, mode) {
            console.log("savePreDiagnose mode", mode);
            console.log("savePreDiagnose data", data);
            tmpPrediagnose = data;
            $scope.showPrediagnose = false;
            $scope.estimateAsb();
        };
        // ========================
        $scope.sumAllPrice = function() {
            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Summary !== undefined) {
                    if (gridTemp[i].Summary == "") {
                        gridTemp[i].Summary = gridTemp[i].Fare;
                    }
                    totalW += gridTemp[i].Summary;
                }
            }
            $scope.totalWork = totalW;
            var totalM = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].subTotal !== undefined) {
                            totalM += gridTemp[i].child[j].subTotal;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].subTotal);
                        }
                    }
                }
            }
            $scope.totalMaterial = totalM;
            // ========================
            var totalWD = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].DiscountedPrice !== undefined) {
                    totalWD += gridTemp[i].DiscountedPrice;
                }
            };
            $scope.totalWorkDiscounted = totalWD;
            var totalMD = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DiscountedPrice !== undefined) {
                            totalMD += gridTemp[i].child[j].DiscountedPrice;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DiscountedPrice);
                        }
                    }
                }
            };
            $scope.totalMaterialDiscounted = totalMD;
            // ========================
            // var totalM = 0;
            // for (var i = 0; i < Parts.length; i++) {
            //     if (Parts[i].subTotal !== undefined) {
            //         totalM += Parts[i].subTotal;
            //         console.log("gridTemp[i].child.Price", Parts[i].subTotal);
            //     }
            // }
            // $scope.totalMaterial = totalM;
            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== undefined) {
                            totalDp += gridTemp[i].child[j].DownPayment;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
            $scope.estimateAsb();
            console.log("gridTemp", gridTemp);
            console.log("gridWork", $scope.gridWork);
        };
        $scope.checkAvailabilityParts = function(data) {
            console.log("data data data Parts", data);
            if (data.PartsId !== undefined) {
                AppointmentGrService.getAvailableParts(data.PartsId).then(function(res) {
                    console.log("Ressss abis availability", res.data);
                    var tmpAvailability = res.data.Result[0];
                    $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice;
                    $scope.ldModel.minimalDp = Math.round(tmpAvailability.PriceDP);
                    $scope.ldModel.nightmareIjal = Math.round(tmpAvailability.PriceDP);
                    $scope.ldModel.DPRequest = Math.round(tmpAvailability.PriceDP);
                    $scope.ldModel.DownPayment = Math.round(tmpAvailability.PriceDP);
                    if ($scope.ldModel.Qty !== undefined && $scope.ldModel.Qty !== null) {
                        $scope.checkPrice(tmpAvailability.RetailPrice);
                    }
                    if (tmpAvailability.isAvailable == 1) {
                        $scope.DisTipeOrder = true;
                    } else {
                        $scope.DisTipeOrder = false;
                    }
                    if (tmpAvailability.isAvailable == 0) {
                        $scope.ldModel.Availbility = "Tidak Tersedia";
                        if (tmpAvailability.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                            $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                        } else {
                            $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                        }
                        $scope.ldModel.Type = 3;
                    } else {
                        $scope.ldModel.Availbility = "Tersedia";
                    }
                });
            }
        };

        //    function allowPattern untuk no polisi
        $scope.allowPattern = function(event, type, item) {
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
            };
            console.log("event", event);
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            };
        };
        $scope.givePattern = function(item, event) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.mData.Km = $scope.mData.Km.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            }
        };
        $scope.givePatternMoney = function(item, event) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.mData.moneyAmount = $scope.mData.moneyAmount.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            }
        };
        $scope.getAvailablePartsService = function(item, row) {
            if (item.PartsId !== null) {
                AppointmentGrService.getAvailableParts(item.PartsId).then(function(res) {
                    var tmpRes = res.data.Result[0];
                    console.log("tmpRes", tmpRes);
                    tmpParts = tmpRes;
                    if (tmpParts !== null) {
                        console.log("have data RetailPrice");
                        item.RetailPrice = tmpParts.RetailPrice;
                        item.subTotal = item.Qty * tmpParts.RetailPrice;
                        item.DownPayment = tmpParts.PriceDP * item.Qty;
                        if (tmpParts.isAvailable == 0) {
                            item.Availbility = "Tidak Tersedia";
                            if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                item.ETA = "";
                            } else {
                                item.ETA = tmpParts.DefaultETA;
                            }

                            item.Type = 3;
                        } else {
                            item.Availbility = "Tersedia";
                        }
                    } else {
                        console.log("haven't data RetailPrice");
                    }
                    if ($scope.tmpPaidById !== null) {
                        item.PaidById = $scope.tmpPaidById;
                        item.paidName = $scope.tmpPaidName;
                    }
                    tmp = item;
                    $scope.listApiJob.addDetail(tmp, row);
                    return item;
                });
            } else {
                if ($scope.tmpPaidById !== null) {
                    item.PaidById = $scope.tmpPaidById;
                    item.paidName = $scope.tmpPaidName;
                }
                item.RetailPrice = item.Price;
                item.subTotal = item.Qty * item.Price;
                item.DownPayment = item.DownPayment * item.Qty;
                tmp = item;
                $scope.listApiJob.addDetail(tmp, row);
                return item;
            }
        }

        $scope.cancelWo = function(item) {
            console.log("item", item);
            bsAlert.alert({
                    title: "Apakah Anda yakin akan melakukan batal WO?",
                    text: "",
                    type: "question",
                    showCancelButton: true
                },
                function() {
                    if (item.JobId > 0) {
                        WO.updateStatusForWO(item.JobId, 20).then(function(res) {
                            $scope.pageMode = 'view'
                            $scope.formApi.setMode('grid');
                            $scope.getData();
                        });
                    } else {
                        WO.CancelWOWalkin(item).then(function(res) {
                            $scope.pageMode = 'view'
                            $scope.formApi.setMode('grid');
                            $scope.getData();
                        });
                    }
                },
                function() {}
            )
        };

        // ===============================WAC===========================================
        /// FROM BG STEV
        $scope.user = CurrentUser.user();
        $scope.mTrialCanvas = null; //Model
        $scope.cTrialCanvas = null; //Collection
        $scope.xTrialCanvas = {};
        $scope.xTrialCanvas.selected = [];

        $scope.inputMap = false;
        $scope.formApi = {};
        $scope.areaApi = {};
        $scope.filter = {
            GroupId: null,
            ItemId: null,
            AttendDate: null
        };

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.areasArray = [];
        $scope.show_modal = {
            wac: false
        };
        $scope.groupWACData = [];
        $scope.jobWACData = [];
        $scope.imageSel = null;
        // $scope.imageWACArr = [
        //     { imgId: 1117, img: "../images/wacImages/Type A.png" },
        //     { imgId: 1118, img: "../images/wacImages/Type B.png" },
        //     { imgId: 1119, img: "../images/wacImages/Type C.png" },
        //     { imgId: 1121, img: "../images/wacImages/Type D.png" },
        //     { imgId: 1122, img: "../images/wacImages/Type E.png" },
        //     { imgId: 1123, img: "../images/wacImages/Type F.png" },
        //     { imgId: 1124, img: "../images/wacImages/Type G.png" },
        // ];
        // WO.getAllTypePoints().then(
        //     function(res) {
        //         // console.log(res);
        //         $scope.imageWACArr = res.data.Result;
        //         $scope.loading = false;
        //         return res.data;
        //     },
        //     function(err) {
        //         console.log("err=>", err);
        //     }
        // );

        GeneralMaster.getData(2030).then(
            function(res) {
                $scope.VTypeData = res.data.Result;
                // console.log("$scope.UomData : ",$scope.UomData);
                return res.data;
            }
        );
        $scope.selectItemWAC = function(selected) {
            WO.getWACBPbyItemId(selected.ItemId).then(
                function(res) {
                    console.log(res.data.Result);
                    $scope.jobWACData = res.data.Result;
                },
                function(err) {
                    console.log("err=>", err);
                }
            )
        };

        $scope.chooseVType = function(selected) {
            switch (selected.MasterId) {
                /*
                case 2157:
                    $scope.imageSel = "../images/wacImages/Type A.png";
                    $scope.areasArray = JSON.parse('[{"areaid":0,"x":430,"y":87,"z":0,"height":20,"width":29,"name":"Menu Options","cssClass":"","id":37,"description":"Menu"},{"areaid":1,"x":215,"y":15,"z":0,"height":36,"width":39,"name":"AngularJS Main Logo","cssClass":"","id":35,"description":"AngularJS Main Logo"},{"areaid":2,"x":131,"y":185,"z":0,"height":24,"width":26,"name":"no name","cssClass":""},{"areaid":3,"x":128,"y":215,"z":0,"height":18,"width":28,"name":"no name","cssClass":""},{"areaid":4,"x":266,"y":251,"z":0,"height":27,"width":29,"name":"no name","cssClass":""},{"areaid":5,"x":84,"y":228,"z":0,"height":18,"width":39,"name":"no name","cssClass":""},{"areaid":6,"x":298,"y":325,"z":0,"height":22,"width":30,"name":"no name","cssClass":""},{"areaid":7,"x":513,"y":176,"z":0,"height":20,"width":44,"name":"no name","cssClass":""},{"areaid":8,"x":85,"y":144,"z":0,"height":19,"width":39,"name":"no name","cssClass":""},{"areaid":9,"x":412,"y":13,"z":0,"height":39,"width":35,"name":"no name","cssClass":""},{"areaid":10,"x":241,"y":60,"z":0,"height":24,"width":40,"name":"no name","cssClass":""},{"areaid":11,"x":358,"y":325,"z":0,"height":23,"width":32,"name":"no name","cssClass":""},{"areaid":12,"x":557,"y":208,"z":0,"height":25,"width":41,"name":"no name","cssClass":""},{"areaid":13,"x":37,"y":236,"z":0,"height":18,"width":36,"name":"no name","cssClass":""},{"areaid":14,"x":307,"y":88,"z":0,"height":24,"width":28,"name":"no name","cssClass":""},{"areaid":15,"x":370,"y":89,"z":0,"height":20,"width":27,"name":"no name","cssClass":""},{"areaid":16,"x":301,"y":51,"z":0,"height":22,"width":28,"name":"no name","cssClass":""},{"areaid":17,"x":362,"y":53,"z":0,"height":21,"width":28,"name":"no name","cssClass":""},{"areaid":18,"x":329,"y":20,"z":0,"height":17,"width":47,"name":"no name","cssClass":""},{"areaid":19,"x":353,"y":180,"z":0,"height":36,"width":40,"name":"no name","cssClass":""},{"areaid":20,"x":266,"y":124,"z":0,"height":27,"width":28,"name":"no name","cssClass":""},{"areaid":21,"x":355,"y":141,"z":0,"height":24,"width":50,"name":"no name","cssClass":""},{"areaid":22,"x":445,"y":59,"z":0,"height":20,"width":32,"name":"no name","cssClass":""},{"areaid":23,"x":599,"y":177,"z":0,"height":21,"width":41,"name":"no name","cssClass":""},{"areaid":24,"x":564,"y":181,"z":0,"height":19,"width":28,"name":"no name","cssClass":""},{"areaid":25,"x":561,"y":145,"z":0,"height":20,"width":35,"name":"no name","cssClass":""},{"areaid":26,"x":376,"y":227,"z":0,"height":17,"width":40,"name":"no name","cssClass":""},{"areaid":27,"x":466,"y":182,"z":0,"height":32,"width":30,"name":"no name","cssClass":""},{"areaid":28,"x":213,"y":355,"z":0,"height":36,"width":42,"name":"no name","cssClass":""},{"areaid":29,"x":407,"y":352,"z":0,"height":38,"width":44,"name":"no name","cssClass":""},{"areaid":30,"x":367,"y":294,"z":0,"height":20,"width":29,"name":"no name","cssClass":""},{"areaid":31,"x":309,"y":292,"z":0,"height":24,"width":29,"name":"no name","cssClass":""},{"areaid":32,"x":427,"y":297,"z":0,"height":21,"width":30,"name":"no name","cssClass":""},{"areaid":33,"x":313,"y":365,"z":0,"height":19,"width":52,"name":"no name","cssClass":""},{"areaid":34,"x":443,"y":324,"z":0,"height":19,"width":31,"name":"no name","cssClass":""},{"areaid":35,"x":245,"y":323,"z":0,"height":23,"width":37,"name":"no name","cssClass":""},{"areaid":36,"x":17,"y":215,"z":0,"height":20,"width":39,"name":"no name","cssClass":""},{"areaid":37,"x":81,"y":211,"z":0,"height":15,"width":33,"name":"no name","cssClass":""},{"areaid":38,"x":80,"y":195,"z":0,"height":15,"width":33,"name":"no name","cssClass":""},{"areaid":39,"x":31,"y":194,"z":0,"height":13,"width":29,"name":"no name","cssClass":""},{"areaid":40,"x":93,"y":180,"z":0,"height":14,"width":33,"name":"no name","cssClass":""},{"areaid":41,"x":90,"y":165,"z":0,"height":14,"width":32,"name":"no name","cssClass":""},{"areaid":42,"x":34,"y":163,"z":100,"height":15,"width":35,"name":"no name","cssClass":""},{"areaid":43,"x":20,"y":179,"z":0,"height":15,"width":31,"name":"no name","cssClass":""}]');
                    break;
                case 2158:
                    $scope.imageSel = "../images/wacImages/Type B.png";
                    $scope.areasArray = JSON.parse('[{"areaid":45,"x":223,"y":7,"z":0,"height":36,"width":39,"name":"AngularJS Main Logo","cssClass":"","id":35,"description":"AngularJS Main Logo"},{"areaid":46,"x":120,"y":201,"z":100,"height":24,"width":26,"name":"no name","cssClass":""},{"areaid":47,"x":71,"y":206,"z":0,"height":14,"width":30,"name":"no name","cssClass":""},{"areaid":48,"x":254,"y":261,"z":0,"height":27,"width":29,"name":"no name","cssClass":""},{"areaid":49,"x":67,"y":236,"z":0,"height":18,"width":39,"name":"no name","cssClass":""},{"areaid":50,"x":303,"y":341,"z":0,"height":22,"width":30,"name":"no name","cssClass":""},{"areaid":51,"x":509,"y":186,"z":0,"height":20,"width":44,"name":"no name","cssClass":""},{"areaid":52,"x":69,"y":167,"z":0,"height":19,"width":39,"name":"no name","cssClass":""},{"areaid":53,"x":420,"y":7,"z":0,"height":39,"width":35,"name":"no name","cssClass":""},{"areaid":54,"x":245,"y":50,"z":0,"height":24,"width":40,"name":"no name","cssClass":""},{"areaid":55,"x":380,"y":336,"z":0,"height":23,"width":32,"name":"no name","cssClass":""},{"areaid":56,"x":557,"y":208,"z":0,"height":25,"width":41,"name":"no name","cssClass":""},{"areaid":58,"x":312,"y":79,"z":0,"height":24,"width":28,"name":"no name","cssClass":""},{"areaid":59,"x":372,"y":85,"z":0,"height":20,"width":27,"name":"no name","cssClass":""},{"areaid":60,"x":297,"y":38,"z":0,"height":22,"width":28,"name":"no name","cssClass":""},{"areaid":61,"x":369,"y":41,"z":0,"height":21,"width":28,"name":"no name","cssClass":""},{"areaid":62,"x":332,"y":11,"z":0,"height":17,"width":47,"name":"no name","cssClass":""},{"areaid":63,"x":344,"y":190,"z":0,"height":36,"width":40,"name":"no name","cssClass":""},{"areaid":64,"x":258,"y":125,"z":0,"height":27,"width":28,"name":"no name","cssClass":""},{"areaid":65,"x":324,"y":154,"z":0,"height":24,"width":50,"name":"no name","cssClass":""},{"areaid":66,"x":431,"y":59,"z":0,"height":20,"width":32,"name":"no name","cssClass":""},{"areaid":67,"x":607,"y":184,"z":0,"height":21,"width":41,"name":"no name","cssClass":""},{"areaid":68,"x":564,"y":176,"z":0,"height":19,"width":28,"name":"no name","cssClass":""},{"areaid":69,"x":561,"y":151,"z":0,"height":20,"width":35,"name":"no name","cssClass":""},{"areaid":70,"x":323,"y":240,"z":0,"height":20,"width":52,"name":"no name","cssClass":""},{"areaid":71,"x":466,"y":182,"z":0,"height":32,"width":30,"name":"no name","cssClass":""},{"areaid":72,"x":232,"y":360,"z":0,"height":36,"width":42,"name":"no name","cssClass":""},{"areaid":73,"x":423,"y":358,"z":0,"height":38,"width":44,"name":"no name","cssClass":""},{"areaid":74,"x":380,"y":297,"z":0,"height":20,"width":29,"name":"no name","cssClass":""},{"areaid":75,"x":318,"y":297,"z":0,"height":24,"width":29,"name":"no name","cssClass":""},{"areaid":77,"x":333,"y":372,"z":0,"height":19,"width":52,"name":"no name","cssClass":""},{"areaid":78,"x":440,"y":323,"z":0,"height":19,"width":31,"name":"no name","cssClass":""},{"areaid":79,"x":245,"y":323,"z":0,"height":23,"width":37,"name":"no name","cssClass":""},{"areaid":80,"x":17,"y":234,"z":0,"height":20,"width":39,"name":"no name","cssClass":""},{"areaid":81,"x":68,"y":220,"z":0,"height":15,"width":33,"name":"no name","cssClass":""},{"areaid":82,"x":68,"y":191,"z":0,"height":15,"width":33,"name":"no name","cssClass":""},{"areaid":83,"x":17,"y":201,"z":0,"height":24,"width":33,"name":"no name","cssClass":""}]');
                    break;
                case 2159:
                    $scope.imageSel = "../images/wacImages/Type C.png";
                    break;
                case 2160:
                    $scope.imageSel = "../images/wacImages/Type D.png";
                    break;
                case 2161:
                    $scope.imageSel = "../images/wacImages/Type E.png";
                    break;
                case 2162:
                    $scope.imageSel = "../images/wacImages/Type F.png";
                    break;
                case 2163:
                    $scope.imageSel = "../images/wacImages/Type G.png";
                */

                case 2221:
                    $scope.imageSel = "../images/wacImages/Type A.png";
                    $scope.areasArray = [{
                            "x": 430,
                            "y": 87,
                            "z": 0,
                            "height": 20,
                            "width": 29,
                            "name": "Menu Options",
                            "cssClass": "choosen-area"
                        },
                        {
                            "areaid": 1,
                            "x": 215,
                            "y": 15,
                            "z": 0,
                            "height": 36,
                            "width": 39,
                            "name": "AngularJS Main Logo",
                            "cssClass": ""
                        },
                        {
                            "areaid": 2,
                            "x": 131,
                            "y": 185,
                            "z": 0,
                            "height": 24,
                            "width": 26,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 3,
                            "x": 128,
                            "y": 215,
                            "z": 0,
                            "height": 18,
                            "width": 28,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 4,
                            "x": 266,
                            "y": 251,
                            "z": 0,
                            "height": 27,
                            "width": 29,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 5,
                            "x": 84,
                            "y": 228,
                            "z": 0,
                            "height": 18,
                            "width": 39,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 6,
                            "x": 298,
                            "y": 325,
                            "z": 0,
                            "height": 22,
                            "width": 30,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 7,
                            "x": 513,
                            "y": 176,
                            "z": 0,
                            "height": 20,
                            "width": 44,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 8,
                            "x": 85,
                            "y": 144,
                            "z": 0,
                            "height": 19,
                            "width": 39,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 9,
                            "x": 412,
                            "y": 13,
                            "z": 0,
                            "height": 39,
                            "width": 35,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 10,
                            "x": 241,
                            "y": 60,
                            "z": 0,
                            "height": 24,
                            "width": 40,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 11,
                            "x": 358,
                            "y": 325,
                            "z": 0,
                            "height": 23,
                            "width": 32,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 12,
                            "x": 557,
                            "y": 208,
                            "z": 0,
                            "height": 25,
                            "width": 41,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 13,
                            "x": 37,
                            "y": 236,
                            "z": 0,
                            "height": 18,
                            "width": 36,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 14,
                            "x": 307,
                            "y": 88,
                            "z": 0,
                            "height": 24,
                            "width": 28,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 15,
                            "x": 370,
                            "y": 89,
                            "z": 0,
                            "height": 20,
                            "width": 27,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 16,
                            "x": 301,
                            "y": 51,
                            "z": 0,
                            "height": 22,
                            "width": 28,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 17,
                            "x": 362,
                            "y": 53,
                            "z": 0,
                            "height": 21,
                            "width": 28,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 18,
                            "x": 329,
                            "y": 20,
                            "z": 0,
                            "height": 17,
                            "width": 47,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 19,
                            "x": 353,
                            "y": 180,
                            "z": 0,
                            "height": 36,
                            "width": 40,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 20,
                            "x": 266,
                            "y": 124,
                            "z": 0,
                            "height": 27,
                            "width": 28,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 21,
                            "x": 355,
                            "y": 141,
                            "z": 0,
                            "height": 24,
                            "width": 50,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 22,
                            "x": 445,
                            "y": 59,
                            "z": 0,
                            "height": 20,
                            "width": 32,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 23,
                            "x": 599,
                            "y": 177,
                            "z": 0,
                            "height": 21,
                            "width": 41,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 24,
                            "x": 564,
                            "y": 181,
                            "z": 0,
                            "height": 19,
                            "width": 28,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 25,
                            "x": 561,
                            "y": 145,
                            "z": 0,
                            "height": 20,
                            "width": 35,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 26,
                            "x": 376,
                            "y": 227,
                            "z": 0,
                            "height": 17,
                            "width": 40,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 27,
                            "x": 466,
                            "y": 182,
                            "z": 0,
                            "height": 32,
                            "width": 30,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 28,
                            "x": 213,
                            "y": 355,
                            "z": 0,
                            "height": 36,
                            "width": 42,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 29,
                            "x": 407,
                            "y": 352,
                            "z": 0,
                            "height": 38,
                            "width": 44,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 30,
                            "x": 367,
                            "y": 294,
                            "z": 0,
                            "height": 20,
                            "width": 29,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 31,
                            "x": 309,
                            "y": 292,
                            "z": 0,
                            "height": 24,
                            "width": 29,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 32,
                            "x": 427,
                            "y": 297,
                            "z": 0,
                            "height": 21,
                            "width": 30,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 33,
                            "x": 313,
                            "y": 365,
                            "z": 0,
                            "height": 19,
                            "width": 52,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 34,
                            "x": 443,
                            "y": 324,
                            "z": 0,
                            "height": 19,
                            "width": 31,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 35,
                            "x": 245,
                            "y": 323,
                            "z": 0,
                            "height": 23,
                            "width": 37,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 36,
                            "x": 17,
                            "y": 215,
                            "z": 0,
                            "height": 20,
                            "width": 39,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 37,
                            "x": 81,
                            "y": 211,
                            "z": 0,
                            "height": 15,
                            "width": 33,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 38,
                            "x": 80,
                            "y": 195,
                            "z": 0,
                            "height": 15,
                            "width": 33,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 39,
                            "x": 31,
                            "y": 194,
                            "z": 0,
                            "height": 13,
                            "width": 29,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 40,
                            "x": 93,
                            "y": 180,
                            "z": 0,
                            "height": 14,
                            "width": 33,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 41,
                            "x": 90,
                            "y": 165,
                            "z": 0,
                            "height": 14,
                            "width": 32,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 42,
                            "x": 34,
                            "y": 163,
                            "z": 100,
                            "height": 15,
                            "width": 35,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "areaid": 43,
                            "x": 20,
                            "y": 179,
                            "z": 0,
                            "height": 15,
                            "width": 31,
                            "name": "no name",
                            "cssClass": ""
                        }
                    ];
                    break;
                case 2222:
                    $scope.imageSel = "../images/wacImages/Type B.png";
                    $scope.areasArray = [{
                            "x": 223,
                            "y": 7,
                            "z": 0,
                            "height": 36,
                            "width": 39,
                            "name": "Velg Depan Kanan",
                            "cssClass": ""
                        },
                        {
                            "x": 120,
                            "y": 201,
                            "z": 100,
                            "height": 24,
                            "width": 26,
                            "name": "Head Lamp Kiri",
                            "cssClass": ""
                        },
                        {
                            "x": 71,
                            "y": 206,
                            "z": 0,
                            "height": 14,
                            "width": 30,
                            "name": "Griell Radiator",
                            "cssClass": ""
                        },
                        {
                            "x": 254,
                            "y": 261,
                            "z": 0,
                            "height": 27,
                            "width": 29,
                            "name": "Spion Kiri",
                            "cssClass": ""
                        },
                        {
                            "x": 67,
                            "y": 236,
                            "z": 0,
                            "height": 18,
                            "width": 39,
                            "name": "Lower Griell",
                            "cssClass": ""
                        },
                        {
                            "x": 303,
                            "y": 341,
                            "z": 0,
                            "height": 22,
                            "width": 30,
                            "name": "Pintu Depan Kiri",
                            "cssClass": ""
                        },
                        {
                            "x": 509,
                            "y": 186,
                            "z": 0,
                            "height": 20,
                            "width": 44,
                            "name": "Stop Lamp Kiri",
                            "cssClass": ""
                        },
                        {
                            "x": 69,
                            "y": 167,
                            "z": 0,
                            "height": 19,
                            "width": 39,
                            "name": "Front Windshield Glass",
                            "cssClass": ""
                        },
                        {
                            "x": 420,
                            "y": 7,
                            "z": 0,
                            "height": 39,
                            "width": 35,
                            "name": "Velg Belakang Kanan",
                            "cssClass": ""
                        },
                        {
                            "x": 245,
                            "y": 50,
                            "z": 0,
                            "height": 24,
                            "width": 40,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 380,
                            "y": 336,
                            "z": 0,
                            "height": 23,
                            "width": 32,
                            "name": "Pintu Belakang Kiri",
                            "cssClass": ""
                        },
                        {
                            "x": 557,
                            "y": 208,
                            "z": 0,
                            "height": 25,
                            "width": 41,
                            "name": "Bumper Belakang",
                            "cssClass": ""
                        },
                        {
                            "x": 312,
                            "y": 79,
                            "z": 0,
                            "height": 24,
                            "width": 28,
                            "name": "Kaca Pintu Depan Kanan",
                            "cssClass": ""
                        },
                        {
                            "x": 372,
                            "y": 85,
                            "z": 0,
                            "height": 20,
                            "width": 27,
                            "name": "kaca Pintu Belakang Kanan",
                            "cssClass": ""
                        },
                        {
                            "x": 297,
                            "y": 38,
                            "z": 0,
                            "height": 22,
                            "width": 28,
                            "name": "Pintu Depan Kanan",
                            "cssClass": ""
                        },
                        {
                            "x": 369,
                            "y": 41,
                            "z": 0,
                            "height": 21,
                            "width": 28,
                            "name": "Pintu Belakang Kanan",
                            "cssClass": ""
                        },
                        {
                            "x": 332,
                            "y": 11,
                            "z": 0,
                            "height": 17,
                            "width": 47,
                            "name": "Stop Lamp Kanan",
                            "cssClass": ""
                        },
                        {
                            "x": 344,
                            "y": 190,
                            "z": 0,
                            "height": 36,
                            "width": 40,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 258,
                            "y": 125,
                            "z": 0,
                            "height": 27,
                            "width": 28,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 324,
                            "y": 154,
                            "z": 0,
                            "height": 24,
                            "width": 50,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 431,
                            "y": 59,
                            "z": 0,
                            "height": 20,
                            "width": 32,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 607,
                            "y": 184,
                            "z": 0,
                            "height": 21,
                            "width": 41,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 564,
                            "y": 176,
                            "z": 0,
                            "height": 19,
                            "width": 28,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 561,
                            "y": 151,
                            "z": 0,
                            "height": 20,
                            "width": 35,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 323,
                            "y": 240,
                            "z": 0,
                            "height": 20,
                            "width": 52,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 466,
                            "y": 182,
                            "z": 0,
                            "height": 32,
                            "width": 30,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 232,
                            "y": 360,
                            "z": 0,
                            "height": 36,
                            "width": 42,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 423,
                            "y": 358,
                            "z": 0,
                            "height": 38,
                            "width": 44,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 380,
                            "y": 297,
                            "z": 0,
                            "height": 20,
                            "width": 29,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 318,
                            "y": 297,
                            "z": 0,
                            "height": 24,
                            "width": 29,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 333,
                            "y": 372,
                            "z": 0,
                            "height": 19,
                            "width": 52,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 440,
                            "y": 323,
                            "z": 0,
                            "height": 19,
                            "width": 31,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 245,
                            "y": 323,
                            "z": 0,
                            "height": 23,
                            "width": 37,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 17,
                            "y": 234,
                            "z": 0,
                            "height": 20,
                            "width": 39,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 68,
                            "y": 220,
                            "z": 0,
                            "height": 15,
                            "width": 33,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 68,
                            "y": 191,
                            "z": 0,
                            "height": 15,
                            "width": 33,
                            "name": "no name",
                            "cssClass": ""
                        },
                        {
                            "x": 17,
                            "y": 201,
                            "z": 0,
                            "height": 24,
                            "width": 33,
                            "name": "no name",
                            "cssClass": ""
                        }
                    ];
                    break;
                case 2223:
                    $scope.imageSel = "../images/wacImages/Type C.png";
                    $scope.areasArray = [{
                        "x": 227,
                        "y": 15,
                        "z": 0,
                        "height": 29,
                        "width": 33,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 82,
                        "x": 399,
                        "y": 15,
                        "z": 0,
                        "height": 29,
                        "width": 37,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 83,
                        "x": 261,
                        "y": 186,
                        "z": 0,
                        "height": 26,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 84,
                        "x": 280,
                        "y": 229,
                        "z": 0,
                        "height": 25,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 85,
                        "x": 294,
                        "y": 150,
                        "z": 0,
                        "height": 27,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 86,
                        "x": 191,
                        "y": 193,
                        "z": 0,
                        "height": 25,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 87,
                        "x": 364,
                        "y": 119,
                        "z": 0,
                        "height": 26,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 88,
                        "x": 365,
                        "y": 257,
                        "z": 0,
                        "height": 28,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 89,
                        "x": 558,
                        "y": 183,
                        "z": 0,
                        "height": 25,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 90,
                        "x": 556,
                        "y": 220,
                        "z": 0,
                        "height": 24,
                        "width": 33,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 91,
                        "x": 612,
                        "y": 178,
                        "z": 0,
                        "height": 24,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 92,
                        "x": 505,
                        "y": 173,
                        "z": 0,
                        "height": 31,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 93,
                        "x": 562,
                        "y": 150,
                        "z": 0,
                        "height": 23,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 94,
                        "x": 323,
                        "y": 7,
                        "z": 0,
                        "height": 26,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 95,
                        "x": 283,
                        "y": 42,
                        "z": 0,
                        "height": 24,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 96,
                        "x": 361,
                        "y": 46,
                        "z": 0,
                        "height": 24,
                        "width": 23,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 97,
                        "x": 414,
                        "y": 56,
                        "z": 0,
                        "height": 25,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 98,
                        "x": 239,
                        "y": 60,
                        "z": 0,
                        "height": 24,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 99,
                        "x": 302,
                        "y": 81,
                        "z": 0,
                        "height": 22,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 100,
                        "x": 361,
                        "y": 87,
                        "z": 0,
                        "height": 23,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 101,
                        "x": 309,
                        "y": 306,
                        "z": 0,
                        "height": 21,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 102,
                        "x": 364,
                        "y": 304,
                        "z": 0,
                        "height": 22,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 103,
                        "x": 299,
                        "y": 340,
                        "z": 0,
                        "height": 24,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 104,
                        "x": 364,
                        "y": 340,
                        "z": 0,
                        "height": 26,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 105,
                        "x": 335,
                        "y": 371,
                        "z": 0,
                        "height": 24,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 106,
                        "x": 231,
                        "y": 365,
                        "z": 0,
                        "height": 28,
                        "width": 33,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 107,
                        "x": 405,
                        "y": 363,
                        "z": 0,
                        "height": 32,
                        "width": 32,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 108,
                        "x": 249,
                        "y": 328,
                        "z": 0,
                        "height": 25,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 109,
                        "x": 418,
                        "y": 326,
                        "z": 0,
                        "height": 26,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 110,
                        "x": 70,
                        "y": 223,
                        "z": 0,
                        "height": 24,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 111,
                        "x": 24,
                        "y": 221,
                        "z": 0,
                        "height": 24,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 112,
                        "x": 124,
                        "y": 186,
                        "z": 0,
                        "height": 25,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 113,
                        "x": 19,
                        "y": 186,
                        "z": 0,
                        "height": 24,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 114,
                        "x": 70,
                        "y": 191,
                        "z": 0,
                        "height": 23,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 115,
                        "x": 371,
                        "y": 189,
                        "z": 0,
                        "height": 22,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 116,
                        "x": 435,
                        "y": 189,
                        "z": 100,
                        "height": 25,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }];
                    break;
                case 2224:
                    $scope.imageSel = "../images/wacImages/Type D.png";
                    $scope.areasArray = [{
                        "x": 67,
                        "y": 154,
                        "z": 0,
                        "height": 21,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 37,
                        "x": 72,
                        "y": 186,
                        "z": 0,
                        "height": 20,
                        "width": 23,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 38,
                        "x": 68,
                        "y": 223,
                        "z": 0,
                        "height": 20,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 39,
                        "x": 28,
                        "y": 217,
                        "z": 0,
                        "height": 20,
                        "width": 19,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 40,
                        "x": 109,
                        "y": 216,
                        "z": 0,
                        "height": 22,
                        "width": 22,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 41,
                        "x": 432,
                        "y": 49,
                        "z": 0,
                        "height": 24,
                        "width": 22,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 42,
                        "x": 460,
                        "y": 7,
                        "z": 0,
                        "height": 23,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 43,
                        "x": 440,
                        "y": 328,
                        "z": 0,
                        "height": 25,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 44,
                        "x": 466,
                        "y": 371,
                        "z": 0,
                        "height": 26,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 45,
                        "x": 223,
                        "y": 153,
                        "z": 0,
                        "height": 20,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 46,
                        "x": 225,
                        "y": 188,
                        "z": 0,
                        "height": 25,
                        "width": 23,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 47,
                        "x": 226,
                        "y": 235,
                        "z": 0,
                        "height": 20,
                        "width": 23,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 48,
                        "x": 166,
                        "y": 124,
                        "z": 0,
                        "height": 23,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 49,
                        "x": 163,
                        "y": 259,
                        "z": 0,
                        "height": 24,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 50,
                        "x": 509,
                        "y": 204,
                        "z": 0,
                        "height": 25,
                        "width": 38,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 51,
                        "x": 607,
                        "y": 206,
                        "z": 100,
                        "height": 25,
                        "width": 31,
                        "name": "no name",
                        "cssClass": ""
                    }];
                    break;
                case 2225:
                    $scope.imageSel = "../images/wacImages/Type E.png";
                    $scope.areasArray = [{
                        "x": 244,
                        "y": 252,
                        "z": 0,
                        "height": 28,
                        "width": 31,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 118,
                        "x": 241,
                        "y": 127,
                        "z": 0,
                        "height": 29,
                        "width": 31,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 119,
                        "x": 288,
                        "y": 153,
                        "z": 0,
                        "height": 23,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 120,
                        "x": 290,
                        "y": 232,
                        "z": 0,
                        "height": 20,
                        "width": 32,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 121,
                        "x": 324,
                        "y": 189,
                        "z": 0,
                        "height": 25,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 122,
                        "x": 570,
                        "y": 151,
                        "z": 0,
                        "height": 23,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 123,
                        "x": 566,
                        "y": 219,
                        "z": 0,
                        "height": 23,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 124,
                        "x": 562,
                        "y": 185,
                        "z": 0,
                        "height": 23,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 125,
                        "x": 504,
                        "y": 190,
                        "z": 0,
                        "height": 27,
                        "width": 31,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 126,
                        "x": 623,
                        "y": 190,
                        "z": 0,
                        "height": 25,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 127,
                        "x": 392,
                        "y": 371,
                        "z": 0,
                        "height": 24,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 128,
                        "x": 198,
                        "y": 371,
                        "z": 0,
                        "height": 23,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 129,
                        "x": 303,
                        "y": 362,
                        "z": 0,
                        "height": 26,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 130,
                        "x": 256,
                        "y": 329,
                        "z": 0,
                        "height": 26,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 131,
                        "x": 340,
                        "y": 330,
                        "z": 0,
                        "height": 24,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 132,
                        "x": 285,
                        "y": 297,
                        "z": 0,
                        "height": 21,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 133,
                        "x": 340,
                        "y": 296,
                        "z": 0,
                        "height": 21,
                        "width": 22,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 134,
                        "x": 407,
                        "y": 322,
                        "z": 0,
                        "height": 22,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 135,
                        "x": 203,
                        "y": 320,
                        "z": 0,
                        "height": 26,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 136,
                        "x": 281,
                        "y": 86,
                        "z": 0,
                        "height": 21,
                        "width": 20,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 137,
                        "x": 334,
                        "y": 89,
                        "z": 0,
                        "height": 23,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 138,
                        "x": 265,
                        "y": 48,
                        "z": 0,
                        "height": 23,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 139,
                        "x": 333,
                        "y": 49,
                        "z": 0,
                        "height": 22,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 140,
                        "x": 305,
                        "y": 16,
                        "z": 0,
                        "height": 24,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 141,
                        "x": 395,
                        "y": 63,
                        "z": 0,
                        "height": 20,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 142,
                        "x": 391,
                        "y": 8,
                        "z": 0,
                        "height": 24,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 143,
                        "x": 192,
                        "y": 7,
                        "z": 0,
                        "height": 25,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 144,
                        "x": 215,
                        "y": 58,
                        "z": 0,
                        "height": 24,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 145,
                        "x": 170,
                        "y": 192,
                        "z": 0,
                        "height": 28,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 146,
                        "x": 235,
                        "y": 193,
                        "z": 0,
                        "height": 28,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 147,
                        "x": 17,
                        "y": 197,
                        "z": 0,
                        "height": 24,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 148,
                        "x": 106,
                        "y": 195,
                        "z": 0,
                        "height": 22,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 149,
                        "x": 59,
                        "y": 220,
                        "z": 0,
                        "height": 20,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 150,
                        "x": 61,
                        "y": 189,
                        "z": 100,
                        "height": 24,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }];
                    break;
                case 2226:
                    $scope.imageSel = "../images/wacImages/Type F.png";
                    $scope.areasArray = [{
                        "x": 226,
                        "y": 180,
                        "z": 0,
                        "height": 32,
                        "width": 31,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 53,
                        "x": 171,
                        "y": 181,
                        "z": 0,
                        "height": 28,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 54,
                        "x": 243,
                        "y": 121,
                        "z": 0,
                        "height": 31,
                        "width": 32,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 55,
                        "x": 245,
                        "y": 245,
                        "z": 0,
                        "height": 34,
                        "width": 34,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 56,
                        "x": 334,
                        "y": 181,
                        "z": 0,
                        "height": 28,
                        "width": 33,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 57,
                        "x": 314,
                        "y": 244,
                        "z": 0,
                        "height": 24,
                        "width": 32,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 58,
                        "x": 310,
                        "y": 128,
                        "z": 0,
                        "height": 28,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 59,
                        "x": 444,
                        "y": 181,
                        "z": 0,
                        "height": 28,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 60,
                        "x": 561,
                        "y": 154,
                        "z": 0,
                        "height": 24,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 61,
                        "x": 506,
                        "y": 184,
                        "z": 0,
                        "height": 27,
                        "width": 34,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 62,
                        "x": 609,
                        "y": 182,
                        "z": 0,
                        "height": 27,
                        "width": 35,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 63,
                        "x": 558,
                        "y": 223,
                        "z": 0,
                        "height": 29,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 64,
                        "x": 561,
                        "y": 189,
                        "z": 0,
                        "height": 23,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 65,
                        "x": 281,
                        "y": 304,
                        "z": 0,
                        "height": 23,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 66,
                        "x": 348,
                        "y": 301,
                        "z": 0,
                        "height": 27,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 67,
                        "x": 415,
                        "y": 295,
                        "z": 0,
                        "height": 25,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 68,
                        "x": 312,
                        "y": 376,
                        "z": 0,
                        "height": 22,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 69,
                        "x": 211,
                        "y": 365,
                        "z": 0,
                        "height": 30,
                        "width": 35,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 70,
                        "x": 388,
                        "y": 364,
                        "z": 0,
                        "height": 32,
                        "width": 35,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 71,
                        "x": 264,
                        "y": 340,
                        "z": 0,
                        "height": 24,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 72,
                        "x": 349,
                        "y": 339,
                        "z": 0,
                        "height": 24,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 73,
                        "x": 416,
                        "y": 329,
                        "z": 0,
                        "height": 25,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 74,
                        "x": 215,
                        "y": 332,
                        "z": 0,
                        "height": 23,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 75,
                        "x": 269,
                        "y": 31,
                        "z": 0,
                        "height": 27,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 76,
                        "x": 337,
                        "y": 28,
                        "z": 0,
                        "height": 28,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 77,
                        "x": 416,
                        "y": 47,
                        "z": 0,
                        "height": 24,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 78,
                        "x": 409,
                        "y": 84,
                        "z": 0,
                        "height": 21,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 79,
                        "x": 344,
                        "y": 83,
                        "z": 0,
                        "height": 22,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 80,
                        "x": 275,
                        "y": 79,
                        "z": 0,
                        "height": 21,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 81,
                        "x": 224,
                        "y": 44,
                        "z": 0,
                        "height": 24,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 82,
                        "x": 207,
                        "y": 7,
                        "z": 0,
                        "height": 27,
                        "width": 34,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 83,
                        "x": 387,
                        "y": 7,
                        "z": 0,
                        "height": 26,
                        "width": 34,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 84,
                        "x": 20,
                        "y": 193,
                        "z": 100,
                        "height": 27,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 85,
                        "x": 111,
                        "y": 193,
                        "z": 0,
                        "height": 27,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 86,
                        "x": 66,
                        "y": 238,
                        "z": 0,
                        "height": 24,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 87,
                        "x": 67,
                        "y": 194,
                        "z": 0,
                        "height": 26,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 88,
                        "x": 18,
                        "y": 234,
                        "z": 0,
                        "height": 24,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 89,
                        "x": 112,
                        "y": 224,
                        "z": 0,
                        "height": 20,
                        "width": 17,
                        "name": "no name",
                        "cssClass": ""
                    }];
                    break;
                case 2227:
                    $scope.imageSel = "../images/wacImages/Type G.png";
                    $scope.areasArray = [{
                        "x": 274,
                        "y": 124,
                        "z": 0,
                        "height": 33,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 91,
                        "x": 270,
                        "y": 250,
                        "z": 0,
                        "height": 33,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 92,
                        "x": 373,
                        "y": 188,
                        "z": 0,
                        "height": 27,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 93,
                        "x": 358,
                        "y": 243,
                        "z": 0,
                        "height": 26,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 94,
                        "x": 355,
                        "y": 139,
                        "z": 0,
                        "height": 24,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 95,
                        "x": 460,
                        "y": 191,
                        "z": 0,
                        "height": 24,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 96,
                        "x": 512,
                        "y": 175,
                        "z": 0,
                        "height": 24,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 97,
                        "x": 612,
                        "y": 173,
                        "z": 0,
                        "height": 30,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 98,
                        "x": 566,
                        "y": 226,
                        "z": 0,
                        "height": 25,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 99,
                        "x": 563,
                        "y": 185,
                        "z": 0,
                        "height": 23,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 100,
                        "x": 561,
                        "y": 145,
                        "z": 0,
                        "height": 25,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 101,
                        "x": 328,
                        "y": 371,
                        "z": 0,
                        "height": 27,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 102,
                        "x": 226,
                        "y": 366,
                        "z": 0,
                        "height": 28,
                        "width": 31,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 103,
                        "x": 409,
                        "y": 366,
                        "z": 0,
                        "height": 28,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 104,
                        "x": 432,
                        "y": 331,
                        "z": 0,
                        "height": 28,
                        "width": 29,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 105,
                        "x": 284,
                        "y": 334,
                        "z": 0,
                        "height": 28,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 106,
                        "x": 368,
                        "y": 335,
                        "z": 0,
                        "height": 27,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 107,
                        "x": 240,
                        "y": 321,
                        "z": 0,
                        "height": 28,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 108,
                        "x": 317,
                        "y": 301,
                        "z": 0,
                        "height": 23,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 109,
                        "x": 370,
                        "y": 303,
                        "z": 0,
                        "height": 23,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 110,
                        "x": 418,
                        "y": 301,
                        "z": 0,
                        "height": 20,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 111,
                        "x": 416,
                        "y": 85,
                        "z": 0,
                        "height": 22,
                        "width": 21,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 112,
                        "x": 363,
                        "y": 83,
                        "z": 0,
                        "height": 21,
                        "width": 23,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 113,
                        "x": 310,
                        "y": 81,
                        "z": 0,
                        "height": 23,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 114,
                        "x": 295,
                        "y": 37,
                        "z": 0,
                        "height": 24,
                        "width": 27,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 115,
                        "x": 357,
                        "y": 37,
                        "z": 0,
                        "height": 24,
                        "width": 23,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 116,
                        "x": 432,
                        "y": 41,
                        "z": 0,
                        "height": 28,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 117,
                        "x": 221,
                        "y": 7,
                        "z": 0,
                        "height": 31,
                        "width": 33,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 118,
                        "x": 401,
                        "y": 7,
                        "z": 0,
                        "height": 26,
                        "width": 31,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 119,
                        "x": 238,
                        "y": 59,
                        "z": 0,
                        "height": 28,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 120,
                        "x": 325,
                        "y": 3,
                        "z": 0,
                        "height": 24,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 121,
                        "x": 205,
                        "y": 185,
                        "z": 0,
                        "height": 27,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 122,
                        "x": 270,
                        "y": 184,
                        "z": 0,
                        "height": 28,
                        "width": 26,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 123,
                        "x": 23,
                        "y": 184,
                        "z": 0,
                        "height": 28,
                        "width": 30,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 124,
                        "x": 118,
                        "y": 184,
                        "z": 0,
                        "height": 27,
                        "width": 32,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 125,
                        "x": 77,
                        "y": 218,
                        "z": 0,
                        "height": 20,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 126,
                        "x": 27,
                        "y": 215,
                        "z": 0,
                        "height": 26,
                        "width": 24,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 127,
                        "x": 122,
                        "y": 212,
                        "z": 0,
                        "height": 22,
                        "width": 25,
                        "name": "no name",
                        "cssClass": ""
                    }, {
                        "areaid": 128,
                        "x": 76,
                        "y": 185,
                        "z": 100,
                        "height": 24,
                        "width": 28,
                        "name": "no name",
                        "cssClass": ""
                    }];
            };

            if ($scope.areasArray.length > 0) {
                console.log("inside if..");
                for (var i = 0; i < $scope.areasArray.length; i++) {
                    $scope.areasArray[i]["areaid"] = i;
                    // $scope.areasArray[i]["name"] = i;
                    // $scope.areasArray[i]["height"] = 20;
                    // $scope.areasArray[i]["width"] = 20;
                }
            };

            // $scope.doReload.rld($scope.areasArray);
            // console.log("inside func");
            // $scope.imageSel = selected.MasterId;
            // console.log($scope.imageSel);
        };

        $scope.onChangeAreas = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        }

        $scope.onAddArea = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        }

        $scope.onRemoveArea = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        }

        $scope.doClick = function(evt) {
            console.log(evt);
            // console.log("formApi : ",$scope.areaApi);
            // var temp = _.findIndex($scope.areasArray, evt);
            // console.log(temp);
            // if(temp > -1) {
            //     if ($scope.areasArray[temp].cssClass == "") {
            //         $scope.areasArray[temp].cssClass = "choosen-area";
            //     } else {
            //         $scope.areasArray[temp].cssClass = "";
            //     }
            // }

            // $scope.formApi.setMode('detail');

            $scope.gridWac.data.push({
                "ItemName": evt.name,
                Status: null
            });
        };
        $scope.gridWac = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'JobWACId',
                    field: 'JobWACId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'Nama Item',
                    field: 'ItemName'
                },
                {
                    name: 'Status Kerusakan',
                    field: 'Status',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'Id',
                    editDropdownOptionsArray: [{
                            'Id': 1,
                            'Name': 'Penyok'
                        },
                        {
                            'Id': 2,
                            'Name': 'Baret'
                        },
                        {
                            'Id': 3,
                            'Name': 'Hilang'
                        }
                    ],
                    cellFilter: "griddropdown:this"
                },
            ]
        };

        function eventKey(event) {
            console.log('eventkey <>><<<>>', event);
            return (event.ctrlKey || event.altKey ||
                (47 < event.keyCode && event.keyCode < 58 && event.shiftKey == false) ||
                (95 < event.keyCode && event.keyCode < 106) || (event.keyCode == 8) ||
                (event.keyCode == 9) || (event.keyCode > 34 && event.keyCode < 40) || (event.keyCode == 46))
        };
        $scope.eventKey = function(event) {
            console.log('eventkey <>><<<>>', event);
            return (event.ctrlKey || event.altKey ||
                (47 < event.keyCode && event.keyCode < 58 && event.shiftKey == false) ||
                (95 < event.keyCode && event.keyCode < 106) || (event.keyCode == 8) ||
                (event.keyCode == 9) || (event.keyCode > 34 && event.keyCode < 40) || (event.keyCode == 46))

        };

        $scope.urlCetakan = "D:\New folder\AppointmentGR20170821000000000.pdf";

        // ==============================
        $scope.QtyOPL = function(key, data) {
            var sum = 0;
            var row = {};
            row = data;
            console.log('QtyOPL', key);
            if (key != null || key !== undefined) {
                row.QtyPekerjaan = key;
                row.Price = data.NormalPrice * key;
                $scope.listApiOpl.addDetail(false, row);
            } else {
                row.Price = data.NormalPrice;
            };
        };

        $scope.onBeforeSaveOpl = function(data, mode) {
            console.log("mode", mode);
            var tmpNormalPriceOPL = data.Price;
            data.Price = tmpNormalPriceOPL;
            data.DiscountedPriceOPL = tmpNormalPriceOPL - ((tmpNormalPriceOPL * data.Discount) / 100);
            var tmpDiscountedOPL;
            data.OutletId = $scope.user.OrgId;
            data.IsGR = 1;
            if (mode == 'new') {
                $scope.tmpDataOPL.push(data);
            }
            $scope.listApiOpl.addDetail(false, data);
        };
        $scope.onAfterSaveOpl = function() {
            var totalWOPL = 0;
            for (var i = 0; i < $scope.oplData.length; i++) {
                if ($scope.oplData[i].Price !== undefined) {
                    totalWOPL += ($scope.oplData[i].Price);
                }
            };
            $scope.sumWorkOpl = totalWOPL;

            var totalDiscountedPrice = 0;
            for (var i = 0; i < $scope.oplData.length; i++) {
                if ($scope.oplData[i].DiscountedPriceOPL !== undefined) {
                    totalDiscountedPrice += $scope.oplData[i].DiscountedPriceOPL;
                }
            };
            $scope.sumWorkOplDiscounted = totalDiscountedPrice;
            console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
        };

        $scope.printOPL = function(data) {
            console.log('data print OPL >>', data);
        };
        // $scope.getOpl = function(key) {
        //     var ress = WO.GetOplList(key).then(function(resTask) {
        //         return resTask.data.Result;
        //     });
        //     console.log("ress", ress);
        //     _.map(ress.value, function(a) {
        //         // _.map(a.value, function(b) {
        //         if (a.isExternal == 1) {
        //             a.isExternalDesc = ' (External)';
        //             // a.VendorName = a.VendorName + a.isExternalDesc;
        //         } else {
        //             a.isExternalDesc = ' (Internal)';
        //             // a.VendorName = a.VendorName + a.isExternalDesc;
        //         };
        //         // });
        //     });
        //     console.log("ress 2", ress);
        //     return ress
        // };
        $scope.getOpl = function(key) {
            console.log('data CRM', $scope.mDataCrm);
            var ress = WO.GetOplList(key).then(function(resTask) {
                var data = resTask.data.Result;
                console.log("resTask resTask", data);
                _.map(data, function(a) {
                    // _.map(a.value, function(b) {
                    if (a.isExternal == 1) {
                        a.isExternalDesc = ' (External)';
                        // a.VendorName = a.VendorName + a.isExternalDesc;
                    } else {
                        a.isExternalDesc = ' (Internal)';
                        // a.VendorName = a.VendorName + a.isExternalDesc;
                    };
                    // });
                });
                console.log("data data", data);
                return data;
            });
            console.log("ress", ress);

            return ress;
        };
        $scope.onSelectWorkOPL = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);
            var tmpItem = $item;
            var row = {};
            if (tmpItem.Vendor !== undefined) {
                row.VendorName = tmpItem.Vendor.Name;
            };
            row.OPLName = tmpItem.OPLWorkName + ' -' + tmpItem.isExternalDesc;
            row.OPLId = tmpItem.Id;
            row.Price = tmpItem.Price;
            row.NormalPrice = tmpItem.Price;
            row.QtyPekerjaan = 1;
            $scope.listApiOpl.addDetail(false, row);
            // console.log('materialArray',materialArray);
        };
        $scope.onNoResultOpl = function() {
            var row = {};
            $scope.listApiOpl.addDetail(false, row);
            // $scope.listApiOpl.clearDetail();
        };

        // =====================================
        //===full from wo history gr==================================








        // ========================== from WO Controller ===============================
        // $scope.setCPPK = function(data, param) {
        //     var objPK = {};
        //     var objCP = {};
        //     var arrPK = [];
        //     var arrCP = [];

        //     objPK.Name = data.DecisionMaker;
        //     objPK.HP1 = data.PhoneDecisionMaker1;
        //     objPK.HP2 = data.PhoneDecisionMaker2;
        //     // console.log('objPK >', objPK);
        //     arrPK.push(objPK);
        //     console.log('arrPK >', arrPK);

        //     objCP.Name = data.ContactPerson;
        //     objCP.HP1 = data.PhoneContactPerson1;
        //     objCP.HP2 = data.PhoneContactPerson2;
        //     // console.log('objCP >', objCP);
        //     arrCP.push(objCP);
        //     console.log('arrCP >', arrCP);

        //     $scope.mDataDM.CP = objCP;

        //     $scope.mDataDM.PK = objPK;

        // };

        // var Parts = [];
        // $scope.dataForBslist = function(data) {
        //     gridTemp = []
        //     Parts.splice();
        //     Parts = [];
        //     if (data.JobTask.length > 0) {
        //         var tmpJob = data.JobTask;
        //         for (var i = 0; i < tmpJob.length; i++) {
        //             console.log("tmpJob", tmpJob);
        //             //
        //             for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
        //                 console.log("tmpJob[i].JobParts[j]", tmpJob[i].JobParts[j]);
        //                 if (tmpJob[i].JobParts[j].PaidBy !== null) {
        //                     tmpJob[i].JobParts[j].paidName = tmpJob[i].JobParts[j].PaidBy.Name;
        //                     delete tmpJob[i].JobParts[j].PaidBy;
        //                 }
        //                 if (tmpJob[i].JobParts[j].Satuan !== null) {
        //                     tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
        //                     delete tmpJob[i].JobParts[j].Satuan;
        //                 }
        //                 tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
        //                 // $scope.getAvailablePartsServiceManual(tmpJob[i].JobParts[j]);

        //                 // Parts.push(tmpJob[i].JobParts[j]);
        //             }


        //         }
        //         var realJob = [];
        //         for (var i = 0; i < tmpJob.length; i++) {
        //             if (tmpJob[i].isDeleted == 0) {
        //                 realJob.push(data.JobTask[i]);
        //             }
        //         }
        //         // added by sss on 2017-11-03
        //         $scope.getAvailablePartsServiceManual(realJob, 0, 0);
        //         //
        //         $scope.gridWork = gridTemp;
        //         $scope.gridOriginal = gridTemp;
        //         // $scope.onAfterSave();
        //         // console.log("totalw", $scope.totalWork);
        //     }
        //     // $scope.sumAllPrice();
        //     console.log("$scope.gridWork", $scope.gridWork);
        //     console.log("$scope.JobRequest", $scope.JobRequest);
        //     console.log("$scope.JobComplaint", $scope.JobComplaint);
        // };


        // $scope.dataForPrediagnose = function(data) {
        //     FollowUpGrService.getPreDiagnoseByJobId(data.JobId).then(function(res) {
        //         tmpPrediagnose = res.data;
        //         console.log("resss Prediagnosenya bosque", res.data);
        //     });
        // };

        // $scope.editData = function(data) {
        //     $scope.JobRequest = data.JobRequest;
        //     // $scope.oplData = data.JobTaskOpl;
        //     $scope.JobComplaint = data.JobComplaint;
        //     $scope.CatWO = data.WoCategoryId;
        //     $scope.copyOplData = angular.copy($scope.oplData);
        //     console.log('resu <<<000>>>', $scope.CatWO, data.WoCategoryId);
        // };


        // $scope.estimateAsb = function() {
        //     $scope.asb.startDate = new Date();
        //     console.log('masuk estimateasb')
        //     if (gridTemp.length > 0) {
        //         var totalW = 0;
        //         for (var i = 0; i < gridTemp.length; i++) {
        //             if (gridTemp[i].ActualRate !== undefined) {
        //                 totalW += gridTemp[i].ActualRate;
        //             }
        //         }
        //         $scope.visibleAsb = 1;
        //         $scope.tmpActRate = totalW;
        //         $scope.mData.EstimateHour = totalW;
        //     } else {
        //         $scope.tmpActRate = 1;
        //         $scope.visibleAsb = 0;
        //         $scope.mData.EstimateHour = 1;
        //     }
        //     $scope.checkAsb = true;
        //     $scope.asb.startDate = "";
        //     currentDate = new Date();
        //     // currentDate.setDate(currentDate.getDate() + 1);
        //     $scope.minDateASB = new Date(currentDate);
        //     // $scope.currentDate = new Date(currentDate);
        //     // $scope.asb.startDate = new Date(currentDate);
        //     console.log('appointment date', $scope.mData.AppointmentDate);
        //     if ($scope.mData.PlanDateStart !== undefined && $scope.mData.PlanDateStart !== null) {

        //         console.log("Estimate  ASB $scope.mData.AppointmentDate", $scope.mData.AppointmentDate);
        //         var tmpDate = $scope.mData.PlanDateStart;
        //         var tmpTime = $scope.mData.AppointmentTime;
        //         var finalDate
        //         var yyyy = tmpDate.getFullYear().toString();
        //         var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based
        //         var dd = tmpDate.getDate().toString();
        //         finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpTime;
        //         var d = new Date(finalDate);
        //         $scope.asb.startDate = d;
        //         $scope.currentDate = d;
        //     } else {
        //         $scope.asb.startDate = new Date(currentDate);
        //         $scope.currentDate = new Date(currentDate);
        //         $scope.mData.AppointmentDate = new Date(currentDate);
        //     }
        //     var timeToDateStart = $scope.asb.startDate;
        //     var timeToDateFinish = $scope.asb.startDate;
        //     var timePlanStart = $scope.mData.PlanStart;
        //     var timePlanFinish = $scope.mData.PlanFinish;
        //     if (($scope.mData.PlanStart !== null && $scope.mData.PlanStart !== undefined) && $scope.mData.PlanFinish !== null && $scope.mData.PlanFinish !== undefined) {
        //         timePlanStart = timePlanStart.split(":");
        //         timePlanFinish = timePlanFinish.split(":");
        //         timeToDateStart = new Date(timeToDateStart.setHours(timePlanStart[0], timePlanStart[1], timePlanStart[2]));
        //         timeToDateFinish = new Date(timeToDateFinish.setHours(timePlanFinish[0], timePlanFinish[1], timePlanFinish[2]));
        //     } else {
        //         timeToDateStart = $scope.mData.PlanStart;
        //         timeToDateFinish = $scope.mData.PlanFinish;
        //     }
        //     // ====================================
        //     $scope.asb.JobStallId = $scope.mData.StallId;
        //     $scope.asb.JobStartTime = timeToDateStart;
        //     $scope.asb.JobEndTime = timeToDateFinish;
        //     if (tmpPrediagnose.PrediagStallId !== undefined) {
        //         $scope.asb.PrediagStallId = tmpPrediagnose.PrediagStallId;
        //     } else {
        //         $scope.asb.PrediagStallId = $scope.mData.PrediagStallId
        //     }
        //     $scope.asb.preDiagnose = tmpPrediagnose;
        //     if (tmpPrediagnose.ScheduledTime !== undefined && tmpPrediagnose.ScheduledTime !== null) {
        //         $scope.asb.PrediagStartTime = tmpPrediagnose.ScheduledTime;
        //     } else {
        //         $scope.asb.PrediagStartTime = $scope.mData.PrediagScheduledTime;
        //     }

        //     // $scope.asb.startDate = $scope.mData.AppointmentDate;
        //     $scope.mData.FullDate = $scope.asb.startDate;
        //     $scope.stallAsb = $scope.mData.StallId;
        //     console.log("curDate", currentDate);
        //     console.log("$scope.asb", $scope.asb);
        //     console.log("$scope.asb.startDate", $scope.asb.startDate, tmpPrediagnose);
        //     showBoardAsbGr($scope.mData.FullDate);

        // };

        // $scope.dataForOPL = function(data) {
        //     $scope.tmpDataOPL = [];
        //     $scope.tmpDataOPLFinal = [];
        //     $scope.oplData = [];
        //     $scope.tmpDataOPLDeleted = [];
        //     _.map(data.JobTaskOpl, function(val, idx) {
        //         console.log('data.JobTaskOpl vall >>', val, idx);
        //         // val.RequiredDate = val.TargetFinishDate;
        //         // val.OplName = val.OPL;
        //         if (val.Status == 0) {
        //             val.xStatus = "Open";
        //         } else if (val.Status == 1) {
        //             val.xStatus = "On Progress";
        //         } else if (val.Status == 2) {
        //             val.xStatus = "Completed";
        //         } else if (val.Status == 3) {
        //             val.xStatus = "Billing";
        //         } else if (val.Status == 4) {
        //             val.xStatus = "Cancel";
        //             console.log('data masuk ke 4 3', val);
        //         };
        //         val.index = "$$" + idx;
        //         val.OPLName = val.OPL.OPLWorkName;
        //         val.VendorName = val.OPL.Vendor.Name;
        //         // val.
        //     });
        //     for (var i in data.JobTaskOpl) {
        //         if (data.JobTaskOpl[i].Status !== 4) {
        //             $scope.oplData.push(data.JobTaskOpl[i]);
        //         }
        //     }
        //     // $scope.oplData = data.JobTaskOpl;
        //     $scope.tmpDataOPL = angular.copy(data.JobTaskOpl);
        //     $scope.tmpDataOPLFinal = [];

        //     if ($scope.oplData.length > 0) {

        //         var totalWOPL = 0;
        //         for (var i = 0; i < $scope.oplData.length; i++) {
        //             if ($scope.oplData[i].Price !== undefined) {
        //                 totalWOPL += $scope.oplData[i].Price;
        //             }
        //         };
        //         $scope.sumWorkOpl = totalWOPL;

        //         var totalDiscountedPrice = 0;
        //         for (var i = 0; i < $scope.oplData.length; i++) {
        //             if ($scope.oplData[i].DiscountedPriceOPL !== undefined) {
        //                 totalDiscountedPrice += $scope.oplData[i].DiscountedPriceOPL;
        //                 console.log("ga masuk else opl");
        //             } else {
        //                 totalDiscountedPrice = 0;
        //                 console.log("masuk else opl");
        //             }
        //             console.log("$scope.oplData[i].DiscountedPriceOPL", $scope.oplData[i].DiscountedPriceOPL);
        //         };
        //         console.log("totalDiscountedPrice", totalDiscountedPrice);
        //         $scope.sumWorkOplDiscounted = totalDiscountedPrice;
        //         console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
        //         console.log('$scope.oplData >>>', $scope.oplData);
        //     }

        // };

        // $scope.changeFormatDate = function(item) {
        //     var tmpAppointmentDate = item;
        //     console.log("changeFormatDate item", item);
        //     tmpAppointmentDate = new Date(tmpAppointmentDate);
        //     var finalDate
        //     var yyyy = tmpAppointmentDate.getFullYear().toString();
        //     var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
        //     var dd = tmpAppointmentDate.getDate().toString();
        //     finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
        //     console.log("changeFormatDate finalDate", finalDate);
        //     return finalDate;
        // };

        // $scope.getCustomerVehicleList = function(data) {
        //     console.log('getCustomerVehicleList before >>>', data);
        //     WOBP.getCustomerVehicleList(data).then(function(resu) {
        //         var data = resu.data.Result[0];
        //         if (data == undefined) {
        //             console.log("loooolllllll");
        //         } else {
        //             console.log('getCustomerVehicleList >>>', data);
        //             var customer = data.CustomerList.CustomerListPersonal[0];
        //             var institusi = data.CustomerList.CustomerListInstitution[0];
        //             var address = data.CustomerList.CustomerAddress;
        //             var objC_Personal = {};
        //             var objC_Institusi = {};
        //             var checkAddres = function() {
        //                 if (address.length != 0) {
        //                     var tempAddressData = [];
        //                     _.map(address, function(val) {
        //                         console.log('adddersss <><>', val);
        //                         if (val.StatusCode == 1) {
        //                             var obj = {};
        //                             obj.CustomerId = val.CustomerId;
        //                             obj.CustomerAddressId = val.CustomerAddressId;
        //                             obj.Address = val.Address;
        //                             obj.AddressCategoryId = val.AddressCategoryId;
        //                             obj.AddressCategory = val.AddressCategory;
        //                             obj.MainAddress = val.MainAddress;
        //                             obj.Phone1 = val.Phone1;
        //                             obj.Phone2 = val.Phone2;
        //                             obj.RT = val.RT;
        //                             obj.RW = val.RW;
        //                             obj.StatusCode = val.StatusCode;
        //                             if (val.MVillage != null || val.MVillage !== undefined) {
        //                                 obj.ProvinceId = val.MVillage.MDistrict.MCityRegency.MProvince.ProvinceId;
        //                                 obj.CityRegencyId = val.MVillage.MDistrict.MCityRegency.CityRegencyId;
        //                                 obj.VillageId = val.MVillage.VillageId;
        //                                 obj.DistrictId = val.MVillage.MDistrict.DistrictId;
        //                                 obj.CityRegencyData = val.MVillage.MDistrict.MCityRegency;
        //                                 obj.DistrictData = val.MVillage.MDistrict;
        //                                 obj.ProvinceData = val.MVillage.MDistrict.MCityRegency.MProvince;
        //                                 obj.VillageData = val.MVillage;
        //                                 obj.AddressCategory = val.AddressCategory;
        //                             };
        //                             // obj.CustomerId =
        //                             tempAddressData.push(obj);
        //                             console.log('tempAddressData <><>', obj);
        //                         };
        //                     });
        //                     return tempAddressData;
        //                 } else {
        //                     return [];
        //                 }
        //             };

        //             $scope.custTypeId = data.CustomerList.CustomerTypeId;
        //             $scope.customerId = data.CustomerList.CustomerId;
        //             $scope.CustomerVehicleId = data.CustomerVehicleId;
        //             $scope.vehicleId = data.VehicleId;
        //             if (data.CustomerList.CustomerTypeId == 3) {
        //                 $scope.showFieldPersonal = true;
        //                 $scope.showFieldInst = false;
        //                 $scope.personalId = customer.PersonalId;
        //                 objC_Personal.BirthDate = customer.BirthDate;
        //                 objC_Personal.KTPKITAS = customer.KTPKITAS;
        //                 objC_Personal.HandPhone1 = customer.Handphone1;
        //                 objC_Personal.HandPhone2 = customer.Handphone2;
        //                 objC_Personal.Name = customer.CustomerName;
        //                 objC_Personal.FrontTitle = customer.FrontTitle;
        //                 objC_Personal.EndTitle = customer.EndTitle;
        //                 objC_Personal.ToyotaId = data.CustomerList.ToyotaId;
        //                 objC_Personal.Npwp = data.CustomerList.Npwp;
        //                 objC_Personal.CustomerId = data.CustomerList.CustomerId;
        //                 objC_Personal.FleetId = data.CustomerList.FleetId;
        //                 objC_Personal.AFCOIdFlag = data.CustomerList.Afco;
        //                 objC_Personal.ToyotaIDFlag = null;
        //                 // objC_Personal.Address = checkAddres();
        //                 objC_Personal.CustomerVehicleId = data.CustomerVehicleId;
        //                 $scope.CustomerAddress = checkAddres();
        //                 $scope.mDataCrm.Customer = objC_Personal;
        //                 objC_Institusi = {};
        //             } else {
        //                 $scope.showFieldInst = true;
        //                 $scope.showFieldPersonal = false;
        //                 $scope.institusiId = institusi.InstitutionId;
        //                 objC_Institusi.ToyotaId = data.CustomerList.ToyotaId;
        //                 objC_Institusi.CustomerTypeId = data.CustomerList.CustomerTypeId;
        //                 objC_Institusi.Name = institusi.Name;
        //                 objC_Institusi.PICName = institusi.PICName;
        //                 objC_Institusi.PICDepartment = institusi.PICDepartment;
        //                 objC_Institusi.PICHp = institusi.PICHp;
        //                 objC_Institusi.PICKTPKITAS = institusi.PICKTPKITAS;
        //                 objC_Institusi.PICEmail = institusi.PICEmail;
        //                 // objC_Institusi.Address = checkAddres();
        //                 objC_Institusi.CustomerVehicleId = data.CustomerVehicleId;
        //                 objC_Institusi.AFCOIdFlag = data.CustomerList.Afco;
        //                 $scope.CustomerAddress = checkAddres();
        //                 $scope.mDataCrm.Customer = objC_Institusi;
        //                 objC_Personal = {};
        //             };

        //             //Vehicle
        //             if (data.VehicleList) {
        //                 var vehicle = data.VehicleList;
        //                 console.log("Vehicle Listnya", vehicle);
        //                 var objV = {};
        //                 objV.VehicleId = vehicle.VehicleId;
        //                 objV.isNonTAM = vehicle.isNonTAM;
        //                 objV.LicensePlate = vehicle.LicensePlate;
        //                 objV.GasTypeId = vehicle.GasTypeId;
        //                 objV.VIN = vehicle.VIN;
        //                 objV.ModelCode = vehicle.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;
        //                 objV.AssemblyYear = vehicle.AssemblyYear;
        //                 // objV.Color = vehicle.MVehicleTypeColor.MColor.ColorName;
        //                 // objV.ColorCode = vehicle.MVehicleTypeColor.MColor.ColorCode;
        //                 objV.KatashikiCode = vehicle.MVehicleTypeColor.MVehicleType.KatashikiCode;
        //                 objV.DECDate = vehicle.DECDate;
        //                 objV.EngineNo = vehicle.EngineNo;
        //                 objV.SPK = vehicle.SPK ? vehicle.SPK : 'No Data Available YET';
        //                 objV.Insurance = vehicle.Insurance ? vehicle.Insurance : 'No Data Available YET';
        //                 objV.STNKName = vehicle.STNKName;
        //                 objV.STNKAddress = vehicle.STNKAddress;
        //                 objV.STNKDate = vehicle.STNKDate;
        //                 objV.Model = vehicle.MVehicleTypeColor.MVehicleType.MVehicleModel;
        //                 // objV.Type = vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId;
        //                 var testingType = null;
        //                 var testingColor = null;
        //                 _.map($scope.VehicM, function(val) {
        //                     if (val.VehicleModelId == objV.Model.VehicleModelId) {
        //                         $scope.selectedModel(val);
        //                         _.map(val.MVehicleType, function(val2) {
        //                             console.log('val2', val2);
        //                             if (val2.VehicleTypeId == vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId) {
        //                                 testingType = val2;
        //                                 objV.Type = testingType;
        //                                 // objV.Type = testingType.VehicleTypeId;
        //                                 $scope.selectedType(val2, 1);
        //                                 _.map($scope.VehicColor, function(val3) {
        //                                     console.log('val3', val3);
        //                                     if (val3.ColorId == vehicle.MVehicleTypeColor.ColorId) {
        //                                         testingColor = val3;
        //                                         objV.Color = testingColor;
        //                                         // objV.Color = testingColor.ColorId;
        //                                         objV.ColorCode = testingColor.ColorCode;
        //                                         $scope.selectedColor(testingColor);
        //                                     };
        //                                 });
        //                             };
        //                         });
        //                     };
        //                 });
        //                 // WO.getVehicleUser(vehicle.VehicleId).then(function(resu) {
        //                 //     console.log('list pengguna >>', resu.data.Result);
        //                 // });
        //                 // $scope.getUserList(vehicle.VehicleId, 4);
        //                 $scope.getUserList(data.CustomerVehicleId, 4);

        //                 console.log("Penting!!!!!!!!!!!!!!!!!!!!!!!!!!!Uncch jarang di cocol", objV);
        //                 $scope.mDataCrm.Vehicle = objV;
        //                 objV = {};
        //                 var objUS = {};
        //                 var arr1 = [];
        //                 var arr2 = [];
        //                 var arrusVUid = [];
        //                 var tempArr = [];
        //                 if (data.VehicleUser.length > 1) {
        //                     _.map(data.CustomerVehicle.VehicleUser, function(e, k) {
        //                         arr1.push(e.Name);
        //                         arr2.push(e.Phone);
        //                         arrusVUid.push(e.VehicleUserId);
        //                         tempArr.push({});
        //                     });
        //                     $scope.mData.US.name = arr1;
        //                     $scope.mData.US.phoneNumber1 = arr2;
        //                     $scope.mData.US.VehicleUserId = arrusVUid;
        //                     $scope.userForm = tempArr;
        //                 };
        //             } else {
        //                 $scope.mDataCrm.Vehicle = {};
        //                 $scope.VehicT = [];
        //                 $scope.enableType = true;
        //             };
        //         }
        //     });
        // };

        //  // ====================Added For BOARD======
        //  var showBoardAsbGr = function(item) {
        //     console.log("item item", item);
        //     var vtime
        //     var tmphour
        //     var tmpMinute
        //     if (item !== undefined && item !== null) {
        //         vtime = item;
        //         tmphour = item.getHours();
        //         tmpMinute = item.getMinutes();
        //     } else {
        //         vtime = new Date();
        //         tmphour = vtime.getHours();
        //         tmpMinute = vtime.getMinutes();
        //     }

        //     vtime.setHours(tmphour);
        //     vtime.setMinutes(tmpMinute);
        //     vtime.setSeconds(0);
        //     $scope.currentItem = {
        //         mode: 'main',
        //         currentDate: $scope.asb.startDate,
        //         nopol: $scope.mData.PoliceNumber,
        //         tipe: $scope.mData.ModelType,
        //         durasi: $scope.tmpActRate,
        //         stallId: $scope.stallAsb,
        //         startTime: vtime,
        //         visible: 1,
        //         jobId: $scope.mData.JobId,
        //         preDiagnose: tmpPrediagnose,
        //         asbStatus: $scope.asb,
        //         /*stallId :1,
        //         startDate :,
        //         endDate:,
        //         durasi:,*/
        //     }
        //     console.log('=============Showboard==============')
        //     console.log("actRe", $scope.tmpActRate);
        //     console.log("harusnya tanggal", $scope.asb.startDate);
        //     console.log("ini vtime", vtime);
        //     console.log("$scope.stallAsb", $scope.stallAsb);
        //     console.log("$scope.currentDate", $scope.currentDate);
        //     console.log("$scope.mData.JobId", $scope.mData.JobId);
        //     console.log('=============/Showboard/==============')
        //     $timeout(function() {
        //         AsbGrFactory.showBoard({ boardName: $scope.boardName, container: 'plottingStall', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

        //     }, 100);

        // }

        // $scope.selectedModel = function(item) {
        //     console.log('selectedModel >>>', item);
        //     console.log(' $scope.mDataCrm.Vehicle', $scope.mDataCrm.Vehicle);
        //     $scope.mDataCrm.Vehicle.Model = item;
        //     console.log("$scope.mDataCrm.Vehicle.Model", $scope.mDataCrm.Vehicle.Model);
        //     $scope.VehicT = [];
        //     $scope.mData.VehicleModelName = item.VehicleModelName;
        //     $scope.mDataCrm.Vehicle.Type = null;
        //     // var dVehic = item.MVehicleType;
        //     var dVehic = _.uniq(item.MVehicleType, 'KatashikiCode');
        //     _.map(dVehic, function(a) {
        //         a.NewDescription = a.Description + ' - ' + a.KatashikiCode;
        //         // a.NewDescription = a.Description + ' - ' + a.KatashikiCode + ' - ' + a.SuffixCode;
        //     });
        //     console.log('dVehic >>>', dVehic);
        //     $scope.VehicT = dVehic;
        //     $scope.enableType = false;
        // };


        // $scope.selectedType = function(item, param) {
        //     console.log('selectedType >>>', item, param);
        //     $scope.mDataCrm.Vehicle.Color = null;
        //     $scope.mDataCrm.Vehicle.Type = item;
        //     console.log("$scope.mDataCrm.Vehicle.Type", $scope.mDataCrm.Vehicle.Type);
        //     $scope.VehicColor = [];
        //     var arrCol = [];
        //     console.log("$scope.Color", $scope.Color);
        //     _.map($scope.Color, function(val) {
        //         if (val.VehicleTypeId == item.VehicleTypeId) {
        //             console.log("val.VehicleTypeId == item.VehicleTypeId", val.VehicleTypeId, item.VehicleTypeId);
        //             console.log("val.VehicleTypeId == item.VehicleTypeId", val);
        //             arrCol = val.ListColor;
        //         };
        //     });
        //     if (param == 1) {
        //         _.map(arrCol, function(a) {
        //             // if(a.ColorId == item.)
        //         });
        //     };
        //     console.log("$scope.arrCol", arrCol);
        //     // var objColor = {
        //     //     ColorId: 9999,
        //     //     ColorName: "Lainnya",
        //     //     ColorCode: "OTH"
        //     // };
        //     // arrCol.push(objColor);
        //     $scope.enableColor = false;
        //     $scope.VehicColor = arrCol;
        //     console.log('$scope.mDataCrm.Vehicle selectedType >>>', $scope.mDataCrm.Vehicle);
        //     $scope.mDataCrm.Vehicle.KatashikiCode = item.KatashikiCode;
        //     AppointmentGrService.getDataTaskListByKatashiki(item.KatashikiCode).then(function(restask) {
        //         taskList = restask.data.Result;
        //         var tmpTask = {};
        //         if (taskList.length > 0) {
        //             for (var i = 0; i < taskList.length; i++) {
        //                 if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
        //                     tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
        //                     tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
        //                     $scope.PriceAvailable = true;
        //                 } else {
        //                     tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
        //                     tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
        //                 }
        //                 $scope.tmpServiceRate = tmpTask;
        //             }
        //         } else {
        //             $scope.tmpServiceRate = null;
        //         };
        //     });
        // };


        // $scope.selectedColor = function(item) {
        //     console.log('selectedColor >>>', item);
        //     $scope.mDataCrm.Vehicle.Color = item;
        //     console.log("$scope.mDataCrm.Vehicle.Color", $scope.mDataCrm.Vehicle.Color);
        //     $scope.mDataCrm.Vehicle.ColorCode = item.ColorCode;
        //     if (item.ColorId == 9999) {
        //         $scope.isCustomColor = true;
        //     };
        //     console.log('$scope.mDataCrm >>>', $scope.mDataCrm);
        // };


        // $scope.getUserList = function(data, param) {
        //     console.log('$get user list ><>', data, param);

        //     WO.getVehicleUser(data).then(function(resu) {
        //         console.log('resu >>', resu.data.Result);
        //         var dataResu = resu.data.Result;

        //         $scope.mData.US = [];
        //         if (dataResu.length != 0) {

        //             var arr = [];
        //             // var arr1 = [];
        //             // var arr2 = [];
        //             // var arr3 = [];
        //             // var arr4 = [];
        //             _.map(dataResu, function(val) {
        //                 var obj = {};
        //                 obj.customerVehicleId = data;
        //                 obj.VehicleId = data;
        //                 obj.name = val.Name;
        //                 obj.VehicleUserId = val.VehicleUserId;
        //                 obj.Relationship = val.RelationId;
        //                 obj.phoneNumber1 = val.Phone1;
        //                 obj.phoneNumber2 = val.Phone2;
        //                 arr.push(obj);

        //                 // objCP.Name = val.Name;
        //                 // objCP.HP1 = val.Phone1;
        //                 // objCP.HP2 = val.Phone2;
        //                 // arrCP.push(objCP);
        //             });
        //             _.sortBy(dataResu, ['LastModifiedDate']).reverse();
        //             // console.log("arr1", arr1);
        //             // console.log("arr4", arr4);
        //             // console.log("$scope.mData.US",$scope.mData.US);
        //             $scope.mData.US.name = arr[0].name;
        //             $scope.mData.US.phoneNumber1 = arr[0].phoneNumber1;
        //             $scope.mData.US.phoneNumber2 = arr[0].phoneNumber2;
        //             $scope.mData.US.Relationship = arr[0].Relationship;
        //             // $scope.pngList = data;
        //             $scope.CVehicUId = data;
        //             console.log('$scope.mData.US', $scope.mData.US);
        //             $scope.pngList = arr;
        //             $scope.hiddenUserList = false;
        //             console.log("arr", arr);
        //         } else {
        //             $scope.hiddenUserList = true;
        //             $scope.CVehicUId = 0;
        //         }
        //         console.log('$scope.CVehicUId >>>000<<<<', $scope.CVehicUId);
        //     });
        // };

        // $scope.getAvailablePartsServiceManual = function(item, x, y) {
        //     console.log("getAvailablePartsServiceManual", item);
        //     console.log("x : ", x);
        //     console.log("y : ", y);

        //     if (x > item.length - 1) {
        //         return item;
        //     }
        //     if (item[x].isDeleted !== 1) {
        //         if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
        //             console.log("punyaaaa part data lengkap", x);
        //             var itemTemp = item[x].JobParts[y];
        //             AppointmentGrService.getAvailableParts(itemTemp.PartsId).then(function(res) {
        //                 var tmpParts = res.data.Result[0];
        //                 if (tmpParts !== null && tmpParts !== undefined) {
        //                     // console.log("have data RetailPrice",item,tmpRes);
        //                     item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
        //                     item[x].JobParts[y].priceDp = tmpParts.PriceDP;
        //                     item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
        //                     item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
        //                     // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
        //                     if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
        //                         item[x].JobParts[y].typeDiskon = -1;
        //                         item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
        //                     } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
        //                         item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
        //                         // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
        //                         item[x].JobParts[y].typeDiskon = 0;
        //                     }
        //                     if (tmpParts.isAvailable == 0) {
        //                         item[x].JobParts[y].Availbility = "Tidak Tersedia";
        //                         if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
        //                             item[x].JobParts[y].ETA = "";
        //                         } else {
        //                             item[x].JobParts[y].ETA = tmpParts.DefaultETA;
        //                         }
        //                         item[x].JobParts[y].Type = 3;
        //                     } else {
        //                         item[x].JobParts[y].Availbility = "Tersedia";
        //                     }
        //                 } else {
        //                     console.log("haven't data RetailPrice");
        //                     item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
        //                     item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
        //                     item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
        //                     if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
        //                         item[x].JobParts[y].typeDiskon = -1;
        //                         item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
        //                     } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
        //                         item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
        //                         // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
        //                         item[x].JobParts[y].typeDiskon = 0;
        //                     }
        //                 }
        //                 if (item[x].JobParts[y].PartsId !== null) {
        //                     item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
        //                     delete item[x].JobParts[y].Part;
        //                 }
        //                 if (item[x].JobParts[y].PaidBy != undefined && item[x].JobParts[y].PaidBy != null) {
        //                     item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
        //                 }

        //                 Parts.push(item[x].JobParts[y]);
        //                 $scope.sumAllPrice();
        //                 // return item;

        //                 if (x <= (item.length - 1)) {
        //                     if (y >= (item[x].JobParts.length - 1)) {
        //                         if (item[x].Fare !== undefined) {
        //                             var sum = item[x].Fare;
        //                             var summ = item[x].FlatRate;
        //                             var summaryy = sum * summ;
        //                             var discountedPrice = (summaryy * item[x].Discount) / 100;
        //                             var normalPrice = summaryy
        //                             summaryy = parseInt(summaryy);
        //                         }
        //                         if (item[x].JobType == null) {
        //                             item[x].JobType = { Name: "" };
        //                         }
        //                         if (item[x].PaidBy == null) {
        //                             item[x].PaidBy = { Name: "" }
        //                         }
        //                         if (item[x].AdditionalTaskId == null) {
        //                             item[x].isOpe = 0;
        //                         } else {
        //                             item[x].isOpe = 1;
        //                         }
        //                         if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
        //                             item[x].typeDiskon = -1;
        //                             item[x].DiscountedPrice = normalPrice;
        //                         } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
        //                             item[x].typeDiskon = 0;
        //                             // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
        //                             item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);
        //                         }
        //                         gridTemp.push({
        //                             ActualRate: item[x].ActualRate,
        //                             AdditionalTaskId: item[x].AdditionalTaskId,
        //                             isOpe: item[x].isOpe,
        //                             Discount: item[x].Discount,
        //                             JobTaskId: item[x].JobTaskId,
        //                             JobTypeId: item[x].JobTypeId,
        //                             DiscountedPrice: item[x].DiscountedPrice,
        //                             typeDiskon: item[x].typeDiskon,
        //                             NormalPrice: normalPrice,
        //                             catName: item[x].JobType.Name,
        //                             Fare: item[x].Fare,
        //                             IsCustomDiscount: item[x].IsCustomDiscount,
        //                             Summary: summaryy,
        //                             PaidById: item[x].PaidById,
        //                             JobId: item[x].JobId,
        //                             TaskId: item[x].TaskId,
        //                             TaskName: item[x].TaskName,
        //                             FlatRate: item[x].FlatRate,
        //                             ProcessId: item[x].ProcessId,
        //                             TFirst1: item[x].TFirst1,
        //                             PaidBy: item[x].PaidBy.Name,
        //                             index: "$$" + x,
        //                             child: Parts
        //                         });
        //                         $scope.sumAllPrice();
        //                         Parts.splice();
        //                         Parts = [];
        //                         console.log("if 1 nilai x", x);
        //                         $scope.getAvailablePartsServiceManual(item, x + 1, 0);

        //                     } else {
        //                         $scope.getAvailablePartsServiceManual(item, x, y + 1);
        //                     }
        //                 }
        //             });
        //         } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
        //             console.log("punyaaaa part data yg gak punya part id", x);
        //             item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
        //             item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
        //             item[x].JobParts[y].DownPayment = item[x].JobParts[y].DownPayment * item[x].JobParts[y].Qty;
        //             if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
        //                 item[x].JobParts[y].typeDiskon = -1;
        //                 item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
        //             } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
        //                 item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
        //                 item[x].JobParts[y].typeDiskon = 0;
        //             }
        //             tmp = item[x].JobParts[y];
        //             Parts.push(item[x].JobParts[y]);
        //             $scope.sumAllPrice();

        //             if (x <= (item.length - 1)) {
        //                 if (y >= (item[x].JobParts.length - 1)) {
        //                     if (item[x].Fare !== undefined) {
        //                         var sum = item[x].Fare;
        //                         var summ = item[x].FlatRate;
        //                         var summaryy = sum * summ;
        //                         var discountedPrice = (summaryy * item[x].Discount) / 100;
        //                         var normalPrice = summaryy
        //                         summaryy = parseInt(summaryy);
        //                     }
        //                     if (item[x].JobType == null) {
        //                         item[x].JobType = { Name: "" };
        //                     }
        //                     if (item[x].PaidBy == null) {
        //                         item[x].PaidBy = { Name: "" }
        //                     }
        //                     if (item[x].AdditionalTaskId == null) {
        //                         item[x].isOpe = 0;
        //                     } else {
        //                         item[x].isOpe = 1;
        //                     }
        //                     if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
        //                         item[x].typeDiskon = -1;
        //                         item[x].DiscountedPrice = normalPrice;
        //                     } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
        //                         item[x].typeDiskon = 0;
        //                         // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
        //                         item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);
        //                     }

        //                     gridTemp.push({
        //                         ActualRate: item[x].ActualRate,
        //                         AdditionalTaskId: item[x].AdditionalTaskId,
        //                         isOpe: item[x].isOpe,
        //                         Discount: item[x].Discount,
        //                         JobTaskId: item[x].JobTaskId,
        //                         JobTypeId: item[x].JobTypeId,
        //                         DiscountedPrice: item[x].DiscountedPrice,
        //                         typeDiskon: item[x].typeDiskon,
        //                         NormalPrice: normalPrice,
        //                         catName: item[x].JobType.Name,
        //                         Fare: item[x].Fare,
        //                         IsCustomDiscount: item[x].IsCustomDiscount,
        //                         Summary: summaryy,
        //                         PaidById: item[x].PaidById,
        //                         JobId: item[x].JobId,
        //                         TaskId: item[x].TaskId,
        //                         TaskName: item[x].TaskName,
        //                         FlatRate: item[x].FlatRate,
        //                         ProcessId: item[x].ProcessId,
        //                         TFirst1: item[x].TFirst1,
        //                         PaidBy: item[x].PaidBy.Name,
        //                         index: "$$" + x,
        //                         child: Parts
        //                     });
        //                     $scope.sumAllPrice();
        //                     Parts.splice();
        //                     Parts = [];
        //                     $scope.getAvailablePartsServiceManual(item, x + 1, 0);
        //                     console.log("if 2 nilai x", x);
        //                 } else {
        //                     $scope.getAvailablePartsServiceManual(item, x, y + 1);
        //                 }
        //             }
        //         } else if (item[x].JobParts.length == 0) {
        //             console.log("gak punya parts", x);
        //             if (x <= (item.length - 1)) {
        //                 if (y >= (item[x].JobParts.length - 1)) {
        //                     if (item[x].Fare !== undefined) {
        //                         var sum = item[x].Fare;
        //                         var summ = item[x].FlatRate;
        //                         var summaryy = sum * summ;
        //                         var discountedPrice = (summaryy * item[x].Discount) / 100;
        //                         var normalPrice = summaryy
        //                         summaryy = parseInt(summaryy);
        //                     }
        //                     if (item[x].JobType == null) {
        //                         item[x].JobType = { Name: "" };
        //                     }
        //                     if (item[x].PaidBy == null) {
        //                         item[x].PaidBy = { Name: "" }
        //                     }
        //                     if (item[x].AdditionalTaskId == null) {
        //                         item[x].isOpe = 0;
        //                     } else {
        //                         item[x].isOpe = 1;
        //                     }
        //                     if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
        //                         item[x].typeDiskon = -1;
        //                         item[x].DiscountedPrice = normalPrice;
        //                     } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
        //                         item[x].typeDiskon = 0;
        //                         // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
        //                         item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);

        //                     }
        //                     gridTemp.push({
        //                         ActualRate: item[x].ActualRate,
        //                         AdditionalTaskId: item[x].AdditionalTaskId,
        //                         isOpe: item[x].isOpe,
        //                         Discount: item[x].Discount,
        //                         JobTaskId: item[x].JobTaskId,
        //                         JobTypeId: item[x].JobTypeId,
        //                         DiscountedPrice: item[x].DiscountedPrice,
        //                         typeDiskon: item[x].typeDiskon,
        //                         NormalPrice: normalPrice,
        //                         catName: item[x].JobType.Name,
        //                         Fare: item[x].Fare,
        //                         IsCustomDiscount: item[x].IsCustomDiscount,
        //                         Summary: summaryy,
        //                         PaidById: item[x].PaidById,
        //                         JobId: item[x].JobId,
        //                         TaskId: item[x].TaskId,
        //                         TaskName: item[x].TaskName,
        //                         FlatRate: item[x].FlatRate,
        //                         ProcessId: item[x].ProcessId,
        //                         TFirst1: item[x].TFirst1,
        //                         PaidBy: item[x].PaidBy.Name,
        //                         index: "$$" + x,
        //                         child: Parts
        //                     });
        //                     $scope.sumAllPrice();
        //                     Parts.splice();
        //                     Parts = [];
        //                     $scope.getAvailablePartsServiceManual(item, x + 1, 0);
        //                 } else {
        //                     $scope.getAvailablePartsServiceManual(item, x, y + 1);
        //                 }
        //             }
        //             // return item;
        //         }

        //         if (x > item.length - 1) {
        //             return item;
        //         }

        //         // gridTemp.push({
        //         //     ActualRate: item[x].ActualRate,
        //         //     AdditionalTaskId:item[x].AdditionalTaskId,
        //         //     isOpe:item[x].isOpe,
        //         //     Discount: item[x].Discount,
        //         //     JobTaskId: item[x].JobTaskId,
        //         //     JobTypeId: item[x].JobTypeId,
        //         //     DiscountedPrice:$scope.discountedPrice,
        //         //     typeDiskon: item[x].typeDiskon,
        //         //     NormalPrice:$scope.normalPrice,
        //         //     catName: item[x].JobType.Name,
        //         //     Fare: item[x].Fare,
        //         //     IsCustomDiscount: item[x].IsCustomDiscount,
        //         //     Summary: $scope.summaryy,
        //         //     PaidById: item[x].PaidById,
        //         //     JobId: item[x].JobId,
        //         //     TaskId: item[x].TaskId,
        //         //     TaskName: item[x].TaskName,
        //         //     FlatRate: item[x].FlatRate,
        //         //     ProcessId: item[x].ProcessId,
        //         //     TFirst1: item[x].TFirst1,
        //         //     PaidBy: item[x].PaidBy.Name,
        //         //     child: Parts
        //         // });
        //         // Parts.splice();
        //         // console.log("tmpJob : ",item);
        //     }
        // };



        // var getDateEstimasi = function(data) {
        //     console.log('onplot data...', data);
        //     if ((data.startTime === null) || (data.endTime === null)) {
        //         // jika startTime/EndTime null berarti belum ada plot, Data tanggal awal/akhir serta Tanggal Appointment harus dikosongkan maka harus dikosongkan
        //         console.log("startTime gk ada");
        //         $scope.mData.StallId = 0;
        //         $scope.mData.FullDate = null;
        //         $scope.mData.PlanStart = null;
        //         $scope.mData.PlanFinish = null;
        //         $scope.mData.PlanDateStart = null;
        //         //$scope.mData.AppointmentDate = null;
        //         delete $scope.mData.AppointmentDate;
        //         //$scope.mData.AppointmentTime = null;
        //         delete $scope.mData.AppointmentTime;
        //         $scope.mData.TargetDateAppointment = null;
        //         $scope.mData.TimeTarget = null;
        //         $scope.mData.PlanDateFinish = null;
        //         $scope.mData.StatusPreDiagnose = 0;
        //         delete $scope.mData.PrediagScheduledTime;
        //         delete $scope.mData.PrediagStallId;

        //         // delete tmpPrediagnose.ScheduledTime;
        //         // delete tmpPrediagnose.StallId;
        //     } else {
        //         console.log("plotting stall");
        //         var dt = new Date(data.startTime);
        //         var dtt = new Date(data.endTime);
        //         console.log("date", dt);
        //         dt.setSeconds(0);
        //         dtt.setSeconds(0);
        //         var timeStart = dt.toTimeString();
        //         var timeFinish = dtt.toTimeString();
        //         timeStart = timeStart.split(' ');
        //         timeFinish = timeFinish.split(' ');
        //         // ===============================
        //         var yearFirst = dt.getFullYear();
        //         var monthFirst = dt.getMonth() + 1;
        //         var dayFirst = dt.getDate();
        //         var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
        //         // ===============================
        //         var yearFinish = dtt.getFullYear();
        //         var monthFinish = dtt.getMonth() + 1;
        //         var dayFinish = dtt.getDate();
        //         var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
        //         // ===============================
        //         // if(timeStart[0] !== null && timeFinish[0] !== null){
        //         $scope.mData.StallId = data.stallId;
        //         $scope.mData.FullDate = dt;
        //         $scope.mData.PlanStart = timeStart[0];
        //         $scope.mData.PlanFinish = timeFinish[0];
        //         $scope.mData.PlanDateStart = firstDate;
        //         $scope.mData.AppointmentDate = firstDate;
        //         $scope.mData.AppointmentTime = timeStart[0];
        //         $scope.mData.TargetDateAppointment = FinishDate;
        //         $scope.mData.TimeTarget = timeFinish[0];
        //         $scope.mData.PlanDateFinish = FinishDate;
        //         $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
        //         $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
        //         $scope.mData.PrediagStallId = data.PrediagStallId;
        //         //-------//
        //         // var start = dataWo.PlanDateStart.toISOString().substring(0, 10);
        //         // var finish = dataWo.PlanDateFinish.toISOString().substring(0, 10);
        //         // console.log('start', start);
        //         // console.log('finish', finish);
        //         // var ScheduledStart = firstDate + ' ' + timeStart[0];
        //         // var ScheduledFinish = FinishDate + ' ' + dataWo.PlanFinish;
        //         // console.log('ScheduledStart', ScheduledStart);
        //         // console.log('ScheduledStart', ScheduledStart);
        //         $scope.mData.firstDate = new Date(data.startTime);
        //         $scope.mData.lastDate = new Date(data.endTime);
        //         var lastdate = angular.copy($scope.mData.lastDate);
        //         console.log('mData.firstDate estimateasb', $scope.mData.firstDate);
        //         console.log('mData.lastDate estimateasb', $scope.mData.lastDate);
        //         $scope.mData.EstimateDate = lastdate.setHours(lastdate.getHours() + 1);
        //         // tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
        //         // tmpPrediagnose.StallId = data.PrediagStallId;
        //     }

        //     // }

        //     // }
        // };




        // $scope.sumAllPrice = function() {
        //     var totalW = 0;
        //     for (var i = 0; i < gridTemp.length; i++) {
        //         if (gridTemp[i].Summary !== undefined) {
        //             if (gridTemp[i].Summary == "") {
        //                 gridTemp[i].Summary = gridTemp[i].Fare;
        //             }
        //             totalW += gridTemp[i].Summary;
        //         }
        //     }
        //     $scope.totalWork = totalW;
        //     var totalM = 0;
        //     for (var i = 0; i < gridTemp.length; i++) {
        //         if (gridTemp[i].child !== undefined) {
        //             for (var j = 0; j < gridTemp[i].child.length; j++) {
        //                 if (gridTemp[i].child[j].subTotal !== undefined) {
        //                     totalM += gridTemp[i].child[j].subTotal;
        //                     console.log("gridTemp[i].child.Price", gridTemp[i].child[j].subTotal);
        //                 }
        //             }
        //         }
        //     }
        //     $scope.totalMaterial = totalM;
        //     // ========================
        //     var totalWD = 0;
        //     for (var i = 0; i < gridTemp.length; i++) {
        //         if (gridTemp[i].DiscountedPrice !== undefined) {
        //             totalWD += gridTemp[i].DiscountedPrice;
        //         }
        //     };
        //     $scope.totalWorkDiscounted = totalWD;
        //     var totalMD = 0;
        //     for (var i = 0; i < gridTemp.length; i++) {
        //         if (gridTemp[i].child !== undefined) {
        //             for (var j = 0; j < gridTemp[i].child.length; j++) {
        //                 if (gridTemp[i].child[j].DiscountedPrice !== undefined) {
        //                     totalMD += gridTemp[i].child[j].DiscountedPrice;
        //                     console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DiscountedPrice);
        //                 }
        //             }
        //         }
        //     };
        //     $scope.totalMaterialDiscounted = totalMD;
        //     // ========================
        //     // var totalM = 0;
        //     // for (var i = 0; i < Parts.length; i++) {
        //     //     if (Parts[i].subTotal !== undefined) {
        //     //         totalM += Parts[i].subTotal;
        //     //         console.log("gridTemp[i].child.Price", Parts[i].subTotal);
        //     //     }
        //     // }
        //     // $scope.totalMaterial = totalM;
        //     var totalDp = 0;
        //     for (var i = 0; i < gridTemp.length; i++) {
        //         if (gridTemp[i].child !== undefined) {
        //             for (var j = 0; j < gridTemp[i].child.length; j++) {
        //                 if (gridTemp[i].child[j].DownPayment !== undefined) {
        //                     totalDp += gridTemp[i].child[j].DownPayment;
        //                     console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DownPayment);
        //                 }
        //             }
        //         }
        //     }
        //     $scope.totalDp = totalDp;
        //     $scope.estimateAsb();
        //     console.log("gridTemp", gridTemp);
        //     console.log("gridWork", $scope.gridWork);
        // };


        // $scope.changeOwner = function() {
        //     $scope.mDataCrm.Customer = [];
        //     if ($scope.action == 2) {
        //         $scope.mode = 'form';
        //     } else {
        //         $scope.showFieldInst = false;
        //         $scope.showFieldPersonal = false;
        //         $scope.filterSearch.filterValue = "";
        //         $scope.mode = 'search';
        //     };
        // };

        // $scope.refreshSlider = function() {
        //     $timeout(function() {
        //         $scope.$broadcast('rzSliderForceRender');
        //     });
        // };


        // $scope.onAfterCancel = function() {
        //     $scope.isRelease = false;
        //     $scope.FlatRateAvailable = false;
        //     $scope.PriceAvailable = false;
        //     $scope.partsAvailable = false;
        //     $scope.diskonData = [];
        //     console.log("$scope.diskonData.length", $scope.diskonData.length);
        //     // if($scope.diskonData.length = 0){

        //     $scope.diskonData = [{
        //         MasterId: -1,
        //         Name: "Tidak Ada Diskon",
        //     }, {
        //         MasterId: 0,
        //         Name: "Custom Diskon"
        //     }];
        //     // }

        // }

        // $scope.PriceIsTam = false;
        // $scope.onBeforeEdit = function(row) {
        //     console.log("$scope.mData.Status=====================>", $scope.mData.Status, row);
        //     if ($scope.mData.Status == 4) {
        //         $scope.isRelease = false;
        //         $scope.PriceAvailable = false;
        //         $scope.partsAvailable = true;
        //         if ($scope.isTAM == 1) {
        //             $scope.PriceIsTam = true;
        //         } else {
        //             $scope.PriceIsTam = false;
        //         }
        //         console.log("$scope.PriceIsTam", $scope.PriceIsTam);
        //     } else if ($scope.mData.Status > 4 && $scope.mData.Status < 8) {
        //         $scope.isRelease = true;
        //         $scope.FlatRateAvailable = false;
        //         $scope.PriceAvailable = true;
        //         // $scope.partsAvailable = false;
        //         $scope.partsAvailable = true;
        //     } else if ($scope.mData.Status == 11) {
        //         $scope.isRelease = true;
        //         $scope.FlatRateAvailable = true;
        //         $scope.PriceAvailable = true;
        //         $scope.partsAvailable = true;
        //     }
        //     // ===========================
        //     if (row.IsCustomDiscount == 0) {
        //         $scope.isCustomDiskon = true;
        //         $scope.isCustomTask = false;
        //     } else {
        //         $scope.isCustomDiskon = true;
        //         $scope.isCustomTask = true;
        //     }
        // };

        // function getImgAndPoints(mstId) {
        //     console.log("mstId", mstId);
        //     switch (mstId) {
        //         case 2221:
        //             $scope.imageSel = "../images/wacImages/Type A.png";
        //             break;
        //         case 2222:
        //             $scope.imageSel = "../images/wacImages/Type B.png";
        //             break;
        //         case 2223:
        //             $scope.imageSel = "../images/wacImages/Type C.png";
        //             break;
        //         case 2224:
        //             $scope.imageSel = "../images/wacImages/Type D.png";
        //             break;
        //         case 2225:
        //             $scope.imageSel = "../images/wacImages/Type E.png";
        //             break;
        //         case 2226:
        //             $scope.imageSel = "../images/wacImages/Type F.png";
        //             break;
        //         case 2227:
        //             $scope.imageSel = "../images/wacImages/Type G.png";
        //     }
        //     var temp = _.find($scope.imageWACArr, function(o) {
        //         return o.TypeId == mstId;
        //     });
        //     console.log("temp", temp);

        //     return JSON.parse(temp.Points);
        // };

        // var dotsData = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };

        // $scope.accordionWac = {
        //     isOpen: false
        // };

        // $scope.clickWACcount = 0;
        // $scope.dataForWAC = function() {
        //     if ($scope.clickWACcount == 0) {
        //         RepairProcessGR.getDataWac($scope.mData.JobId).then(
        //             function(res) {
        //                 var dataWac = res.data;
        //                 $scope.finalDataWac = dataWac;

        //                 console.log("Data WAC", dataWac);
        //                 $scope.mData.OtherDescription = dataWac.Other;
        //                 dataWac.MoneyAmount = String(dataWac.MoneyAmount);
        //                 $scope.mData.moneyAmount = dataWac.MoneyAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        //                 // $scope.mData.moneyAmount = dataWac.MoneyAmount;
        //                 $scope.mData.BanSerep = dataWac.isSpareTire;
        //                 $scope.mData.CdorKaset = dataWac.isCD;
        //                 $scope.mData.Payung = dataWac.isUmbrella;
        //                 $scope.mData.Dongkrak = dataWac.isJack;
        //                 $scope.mData.STNK = dataWac.isVehicleRegistrationNumber;
        //                 $scope.mData.P3k = dataWac.isP3k;
        //                 $scope.mData.KunciSteer = dataWac.isSteerLock;
        //                 $scope.mData.ToolSet = dataWac.isToolSet;
        //                 $scope.mData.ClipKarpet = dataWac.isCarpetClip;
        //                 $scope.mData.BukuService = dataWac.isServiceBook;
        //                 $scope.mData.AlarmCondition = dataWac.isAlarmConditionOk;
        //                 $scope.mData.AcCondition = dataWac.isAcOk;
        //                 $scope.mData.PowerWindow = dataWac.isPowerWindowOk;
        //                 $scope.mData.CarType = dataWac.isPowerWindowOk;
        //                 // $scope.slider.options = 100;
        //                 $scope.slider.value = dataWac.Fuel;
        //                 // $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
        //             },
        //             function(err) {
        //                 console.log("err=>", err);
        //             }
        //         );
        //         RepairProcessGR.getProbPoints($scope.mData.JobId, null).then(
        //             function(res) {
        //                 console.log("RepairProcessGR.getProbPoints", res.data);
        //                 mstId = angular.copy(res.data.Result[0].TypeId);
        //                 console.log("ini MstId", mstId);
        //                 $scope.filter.vType = mstId;
        //                 console.log("probPoints", res.data.Result);
        //                 // $scope.chooseVType(mstId);
        //                 $scope.areasArray = getImgAndPoints(mstId);
        //                 console.log("$scope.areasArray", $scope.areasArray);
        //                 if (res.data.Result.length > 0) {
        //                     for (var i in res.data.Result) {
        //                         var getTemp = JSON.parse(res.data.Result[i]["Points"]);
        //                         for (var j in getTemp) {
        //                             var xtemp = angular.copy(getTemp[j]);
        //                             delete xtemp['status'];
        //                             console.log("xtemp : ", xtemp);
        //                             if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
        //                                 console.log("find..", xtemp);
        //                                 var idx = _.findIndex($scope.areasArray, xtemp);
        //                                 xtemp.cssClass = "choosen-area";
        //                                 $scope.areasArray[idx] = xtemp;

        //                                 dotsData[mstId].push({ "ItemId": xtemp.areaid, "ItemName": xtemp.name, "Points": xtemp, "Status": getTemp[j].status });
        //                             }
        //                         }
        //                     }
        //                     // setDataGrid(dotsData[mstId]);
        //                 }
        //             },
        //             function(err) {
        //                 $scope.areasArray = getImgAndPoints(mstId);
        //             }
        //         );
        //         $scope.clickWACcount++;
        //     } else {
        //         console.log("di click mulu, rese but sih lu");
        //     }

        // }
        // $scope.discountedPrice = null;
        // $scope.normalPrice = null;
        // $scope.summaryy = null;


        // // ===============================WAC===========================================
        // /// FROM BG STEV
        // $scope.user = CurrentUser.user();
        // $scope.mTrialCanvas = null; //Model
        // $scope.cTrialCanvas = null; //Collection
        // $scope.xTrialCanvas = {};
        // $scope.xTrialCanvas.selected = [];

        // $scope.inputMap = false;
        // $scope.formApi = {};
        // $scope.areaApi = {};
        // $scope.filter = { GroupId: null, ItemId: null, AttendDate: null };

        // //----------------------------------
        // // Get Data
        // //----------------------------------
        // $scope.areasArray = [];
        // $scope.show_modal = { wac: false };
        // $scope.groupWACData = [];
        // $scope.jobWACData = [];
        // $scope.imageSel = null;
        // // $scope.imageWACArr = [
        // //     { imgId: 1117, img: "../images/wacImages/Type A.png" },
        // //     { imgId: 1118, img: "../images/wacImages/Type B.png" },
        // //     { imgId: 1119, img: "../images/wacImages/Type C.png" },
        // //     { imgId: 1121, img: "../images/wacImages/Type D.png" },
        // //     { imgId: 1122, img: "../images/wacImages/Type E.png" },
        // //     { imgId: 1123, img: "../images/wacImages/Type F.png" },
        // //     { imgId: 1124, img: "../images/wacImages/Type G.png" },
        // // ];
        // WO.getAllTypePoints().then(
        //     function(res) {
        //         // console.log(res);
        //         $scope.imageWACArr = res.data.Result;
        //         $scope.loading = false;
        //         return res.data;
        //     },
        //     function(err) {
        //         console.log("err=>", err);
        //     }
        // );

        // GeneralMaster.getData(2030).then(
        //     function(res) {
        //         $scope.VTypeData = res.data.Result;
        //         // console.log("$scope.UomData : ",$scope.UomData);
        //         return res.data;
        //     }
        // );
        // $scope.selectItemWAC = function(selected) {
        //     WO.getWACBPbyItemId(selected.ItemId).then(
        //         function(res) {
        //             console.log(res.data.Result);
        //             $scope.jobWACData = res.data.Result;
        //         },
        //         function(err) {
        //             console.log("err=>", err);
        //         }
        //     )
        // };

        // $scope.chooseVType = function(selected) {
        //     switch (selected.MasterId) {
        //         /*
        //         case 2157:
        //             $scope.imageSel = "../images/wacImages/Type A.png";
        //             $scope.areasArray = JSON.parse('[{"areaid":0,"x":430,"y":87,"z":0,"height":20,"width":29,"name":"Menu Options","cssClass":"","id":37,"description":"Menu"},{"areaid":1,"x":215,"y":15,"z":0,"height":36,"width":39,"name":"AngularJS Main Logo","cssClass":"","id":35,"description":"AngularJS Main Logo"},{"areaid":2,"x":131,"y":185,"z":0,"height":24,"width":26,"name":"no name","cssClass":""},{"areaid":3,"x":128,"y":215,"z":0,"height":18,"width":28,"name":"no name","cssClass":""},{"areaid":4,"x":266,"y":251,"z":0,"height":27,"width":29,"name":"no name","cssClass":""},{"areaid":5,"x":84,"y":228,"z":0,"height":18,"width":39,"name":"no name","cssClass":""},{"areaid":6,"x":298,"y":325,"z":0,"height":22,"width":30,"name":"no name","cssClass":""},{"areaid":7,"x":513,"y":176,"z":0,"height":20,"width":44,"name":"no name","cssClass":""},{"areaid":8,"x":85,"y":144,"z":0,"height":19,"width":39,"name":"no name","cssClass":""},{"areaid":9,"x":412,"y":13,"z":0,"height":39,"width":35,"name":"no name","cssClass":""},{"areaid":10,"x":241,"y":60,"z":0,"height":24,"width":40,"name":"no name","cssClass":""},{"areaid":11,"x":358,"y":325,"z":0,"height":23,"width":32,"name":"no name","cssClass":""},{"areaid":12,"x":557,"y":208,"z":0,"height":25,"width":41,"name":"no name","cssClass":""},{"areaid":13,"x":37,"y":236,"z":0,"height":18,"width":36,"name":"no name","cssClass":""},{"areaid":14,"x":307,"y":88,"z":0,"height":24,"width":28,"name":"no name","cssClass":""},{"areaid":15,"x":370,"y":89,"z":0,"height":20,"width":27,"name":"no name","cssClass":""},{"areaid":16,"x":301,"y":51,"z":0,"height":22,"width":28,"name":"no name","cssClass":""},{"areaid":17,"x":362,"y":53,"z":0,"height":21,"width":28,"name":"no name","cssClass":""},{"areaid":18,"x":329,"y":20,"z":0,"height":17,"width":47,"name":"no name","cssClass":""},{"areaid":19,"x":353,"y":180,"z":0,"height":36,"width":40,"name":"no name","cssClass":""},{"areaid":20,"x":266,"y":124,"z":0,"height":27,"width":28,"name":"no name","cssClass":""},{"areaid":21,"x":355,"y":141,"z":0,"height":24,"width":50,"name":"no name","cssClass":""},{"areaid":22,"x":445,"y":59,"z":0,"height":20,"width":32,"name":"no name","cssClass":""},{"areaid":23,"x":599,"y":177,"z":0,"height":21,"width":41,"name":"no name","cssClass":""},{"areaid":24,"x":564,"y":181,"z":0,"height":19,"width":28,"name":"no name","cssClass":""},{"areaid":25,"x":561,"y":145,"z":0,"height":20,"width":35,"name":"no name","cssClass":""},{"areaid":26,"x":376,"y":227,"z":0,"height":17,"width":40,"name":"no name","cssClass":""},{"areaid":27,"x":466,"y":182,"z":0,"height":32,"width":30,"name":"no name","cssClass":""},{"areaid":28,"x":213,"y":355,"z":0,"height":36,"width":42,"name":"no name","cssClass":""},{"areaid":29,"x":407,"y":352,"z":0,"height":38,"width":44,"name":"no name","cssClass":""},{"areaid":30,"x":367,"y":294,"z":0,"height":20,"width":29,"name":"no name","cssClass":""},{"areaid":31,"x":309,"y":292,"z":0,"height":24,"width":29,"name":"no name","cssClass":""},{"areaid":32,"x":427,"y":297,"z":0,"height":21,"width":30,"name":"no name","cssClass":""},{"areaid":33,"x":313,"y":365,"z":0,"height":19,"width":52,"name":"no name","cssClass":""},{"areaid":34,"x":443,"y":324,"z":0,"height":19,"width":31,"name":"no name","cssClass":""},{"areaid":35,"x":245,"y":323,"z":0,"height":23,"width":37,"name":"no name","cssClass":""},{"areaid":36,"x":17,"y":215,"z":0,"height":20,"width":39,"name":"no name","cssClass":""},{"areaid":37,"x":81,"y":211,"z":0,"height":15,"width":33,"name":"no name","cssClass":""},{"areaid":38,"x":80,"y":195,"z":0,"height":15,"width":33,"name":"no name","cssClass":""},{"areaid":39,"x":31,"y":194,"z":0,"height":13,"width":29,"name":"no name","cssClass":""},{"areaid":40,"x":93,"y":180,"z":0,"height":14,"width":33,"name":"no name","cssClass":""},{"areaid":41,"x":90,"y":165,"z":0,"height":14,"width":32,"name":"no name","cssClass":""},{"areaid":42,"x":34,"y":163,"z":100,"height":15,"width":35,"name":"no name","cssClass":""},{"areaid":43,"x":20,"y":179,"z":0,"height":15,"width":31,"name":"no name","cssClass":""}]');
        //             break;
        //         case 2158:
        //             $scope.imageSel = "../images/wacImages/Type B.png";
        //             $scope.areasArray = JSON.parse('[{"areaid":45,"x":223,"y":7,"z":0,"height":36,"width":39,"name":"AngularJS Main Logo","cssClass":"","id":35,"description":"AngularJS Main Logo"},{"areaid":46,"x":120,"y":201,"z":100,"height":24,"width":26,"name":"no name","cssClass":""},{"areaid":47,"x":71,"y":206,"z":0,"height":14,"width":30,"name":"no name","cssClass":""},{"areaid":48,"x":254,"y":261,"z":0,"height":27,"width":29,"name":"no name","cssClass":""},{"areaid":49,"x":67,"y":236,"z":0,"height":18,"width":39,"name":"no name","cssClass":""},{"areaid":50,"x":303,"y":341,"z":0,"height":22,"width":30,"name":"no name","cssClass":""},{"areaid":51,"x":509,"y":186,"z":0,"height":20,"width":44,"name":"no name","cssClass":""},{"areaid":52,"x":69,"y":167,"z":0,"height":19,"width":39,"name":"no name","cssClass":""},{"areaid":53,"x":420,"y":7,"z":0,"height":39,"width":35,"name":"no name","cssClass":""},{"areaid":54,"x":245,"y":50,"z":0,"height":24,"width":40,"name":"no name","cssClass":""},{"areaid":55,"x":380,"y":336,"z":0,"height":23,"width":32,"name":"no name","cssClass":""},{"areaid":56,"x":557,"y":208,"z":0,"height":25,"width":41,"name":"no name","cssClass":""},{"areaid":58,"x":312,"y":79,"z":0,"height":24,"width":28,"name":"no name","cssClass":""},{"areaid":59,"x":372,"y":85,"z":0,"height":20,"width":27,"name":"no name","cssClass":""},{"areaid":60,"x":297,"y":38,"z":0,"height":22,"width":28,"name":"no name","cssClass":""},{"areaid":61,"x":369,"y":41,"z":0,"height":21,"width":28,"name":"no name","cssClass":""},{"areaid":62,"x":332,"y":11,"z":0,"height":17,"width":47,"name":"no name","cssClass":""},{"areaid":63,"x":344,"y":190,"z":0,"height":36,"width":40,"name":"no name","cssClass":""},{"areaid":64,"x":258,"y":125,"z":0,"height":27,"width":28,"name":"no name","cssClass":""},{"areaid":65,"x":324,"y":154,"z":0,"height":24,"width":50,"name":"no name","cssClass":""},{"areaid":66,"x":431,"y":59,"z":0,"height":20,"width":32,"name":"no name","cssClass":""},{"areaid":67,"x":607,"y":184,"z":0,"height":21,"width":41,"name":"no name","cssClass":""},{"areaid":68,"x":564,"y":176,"z":0,"height":19,"width":28,"name":"no name","cssClass":""},{"areaid":69,"x":561,"y":151,"z":0,"height":20,"width":35,"name":"no name","cssClass":""},{"areaid":70,"x":323,"y":240,"z":0,"height":20,"width":52,"name":"no name","cssClass":""},{"areaid":71,"x":466,"y":182,"z":0,"height":32,"width":30,"name":"no name","cssClass":""},{"areaid":72,"x":232,"y":360,"z":0,"height":36,"width":42,"name":"no name","cssClass":""},{"areaid":73,"x":423,"y":358,"z":0,"height":38,"width":44,"name":"no name","cssClass":""},{"areaid":74,"x":380,"y":297,"z":0,"height":20,"width":29,"name":"no name","cssClass":""},{"areaid":75,"x":318,"y":297,"z":0,"height":24,"width":29,"name":"no name","cssClass":""},{"areaid":77,"x":333,"y":372,"z":0,"height":19,"width":52,"name":"no name","cssClass":""},{"areaid":78,"x":440,"y":323,"z":0,"height":19,"width":31,"name":"no name","cssClass":""},{"areaid":79,"x":245,"y":323,"z":0,"height":23,"width":37,"name":"no name","cssClass":""},{"areaid":80,"x":17,"y":234,"z":0,"height":20,"width":39,"name":"no name","cssClass":""},{"areaid":81,"x":68,"y":220,"z":0,"height":15,"width":33,"name":"no name","cssClass":""},{"areaid":82,"x":68,"y":191,"z":0,"height":15,"width":33,"name":"no name","cssClass":""},{"areaid":83,"x":17,"y":201,"z":0,"height":24,"width":33,"name":"no name","cssClass":""}]');
        //             break;
        //         case 2159:
        //             $scope.imageSel = "../images/wacImages/Type C.png";
        //             break;
        //         case 2160:
        //             $scope.imageSel = "../images/wacImages/Type D.png";
        //             break;
        //         case 2161:
        //             $scope.imageSel = "../images/wacImages/Type E.png";
        //             break;
        //         case 2162:
        //             $scope.imageSel = "../images/wacImages/Type F.png";
        //             break;
        //         case 2163:
        //             $scope.imageSel = "../images/wacImages/Type G.png";
        //         */

        //         case 2221:
        //             $scope.imageSel = "../images/wacImages/Type A.png";
        //             $scope.areasArray = [
        //                 { "x": 430, "y": 87, "z": 0, "height": 20, "width": 29, "name": "Menu Options", "cssClass": "choosen-area" },
        //                 { "areaid": 1, "x": 215, "y": 15, "z": 0, "height": 36, "width": 39, "name": "AngularJS Main Logo", "cssClass": "" },
        //                 { "areaid": 2, "x": 131, "y": 185, "z": 0, "height": 24, "width": 26, "name": "no name", "cssClass": "" },
        //                 { "areaid": 3, "x": 128, "y": 215, "z": 0, "height": 18, "width": 28, "name": "no name", "cssClass": "" },
        //                 { "areaid": 4, "x": 266, "y": 251, "z": 0, "height": 27, "width": 29, "name": "no name", "cssClass": "" },
        //                 { "areaid": 5, "x": 84, "y": 228, "z": 0, "height": 18, "width": 39, "name": "no name", "cssClass": "" },
        //                 { "areaid": 6, "x": 298, "y": 325, "z": 0, "height": 22, "width": 30, "name": "no name", "cssClass": "" },
        //                 { "areaid": 7, "x": 513, "y": 176, "z": 0, "height": 20, "width": 44, "name": "no name", "cssClass": "" },
        //                 { "areaid": 8, "x": 85, "y": 144, "z": 0, "height": 19, "width": 39, "name": "no name", "cssClass": "" },
        //                 { "areaid": 9, "x": 412, "y": 13, "z": 0, "height": 39, "width": 35, "name": "no name", "cssClass": "" },
        //                 { "areaid": 10, "x": 241, "y": 60, "z": 0, "height": 24, "width": 40, "name": "no name", "cssClass": "" },
        //                 { "areaid": 11, "x": 358, "y": 325, "z": 0, "height": 23, "width": 32, "name": "no name", "cssClass": "" },
        //                 { "areaid": 12, "x": 557, "y": 208, "z": 0, "height": 25, "width": 41, "name": "no name", "cssClass": "" },
        //                 { "areaid": 13, "x": 37, "y": 236, "z": 0, "height": 18, "width": 36, "name": "no name", "cssClass": "" },
        //                 { "areaid": 14, "x": 307, "y": 88, "z": 0, "height": 24, "width": 28, "name": "no name", "cssClass": "" },
        //                 { "areaid": 15, "x": 370, "y": 89, "z": 0, "height": 20, "width": 27, "name": "no name", "cssClass": "" },
        //                 { "areaid": 16, "x": 301, "y": 51, "z": 0, "height": 22, "width": 28, "name": "no name", "cssClass": "" },
        //                 { "areaid": 17, "x": 362, "y": 53, "z": 0, "height": 21, "width": 28, "name": "no name", "cssClass": "" },
        //                 { "areaid": 18, "x": 329, "y": 20, "z": 0, "height": 17, "width": 47, "name": "no name", "cssClass": "" },
        //                 { "areaid": 19, "x": 353, "y": 180, "z": 0, "height": 36, "width": 40, "name": "no name", "cssClass": "" },
        //                 { "areaid": 20, "x": 266, "y": 124, "z": 0, "height": 27, "width": 28, "name": "no name", "cssClass": "" },
        //                 { "areaid": 21, "x": 355, "y": 141, "z": 0, "height": 24, "width": 50, "name": "no name", "cssClass": "" },
        //                 { "areaid": 22, "x": 445, "y": 59, "z": 0, "height": 20, "width": 32, "name": "no name", "cssClass": "" },
        //                 { "areaid": 23, "x": 599, "y": 177, "z": 0, "height": 21, "width": 41, "name": "no name", "cssClass": "" },
        //                 { "areaid": 24, "x": 564, "y": 181, "z": 0, "height": 19, "width": 28, "name": "no name", "cssClass": "" },
        //                 { "areaid": 25, "x": 561, "y": 145, "z": 0, "height": 20, "width": 35, "name": "no name", "cssClass": "" },
        //                 { "areaid": 26, "x": 376, "y": 227, "z": 0, "height": 17, "width": 40, "name": "no name", "cssClass": "" },
        //                 { "areaid": 27, "x": 466, "y": 182, "z": 0, "height": 32, "width": 30, "name": "no name", "cssClass": "" },
        //                 { "areaid": 28, "x": 213, "y": 355, "z": 0, "height": 36, "width": 42, "name": "no name", "cssClass": "" },
        //                 { "areaid": 29, "x": 407, "y": 352, "z": 0, "height": 38, "width": 44, "name": "no name", "cssClass": "" },
        //                 { "areaid": 30, "x": 367, "y": 294, "z": 0, "height": 20, "width": 29, "name": "no name", "cssClass": "" },
        //                 { "areaid": 31, "x": 309, "y": 292, "z": 0, "height": 24, "width": 29, "name": "no name", "cssClass": "" },
        //                 { "areaid": 32, "x": 427, "y": 297, "z": 0, "height": 21, "width": 30, "name": "no name", "cssClass": "" },
        //                 { "areaid": 33, "x": 313, "y": 365, "z": 0, "height": 19, "width": 52, "name": "no name", "cssClass": "" },
        //                 { "areaid": 34, "x": 443, "y": 324, "z": 0, "height": 19, "width": 31, "name": "no name", "cssClass": "" },
        //                 { "areaid": 35, "x": 245, "y": 323, "z": 0, "height": 23, "width": 37, "name": "no name", "cssClass": "" },
        //                 { "areaid": 36, "x": 17, "y": 215, "z": 0, "height": 20, "width": 39, "name": "no name", "cssClass": "" },
        //                 { "areaid": 37, "x": 81, "y": 211, "z": 0, "height": 15, "width": 33, "name": "no name", "cssClass": "" },
        //                 { "areaid": 38, "x": 80, "y": 195, "z": 0, "height": 15, "width": 33, "name": "no name", "cssClass": "" },
        //                 { "areaid": 39, "x": 31, "y": 194, "z": 0, "height": 13, "width": 29, "name": "no name", "cssClass": "" },
        //                 { "areaid": 40, "x": 93, "y": 180, "z": 0, "height": 14, "width": 33, "name": "no name", "cssClass": "" },
        //                 { "areaid": 41, "x": 90, "y": 165, "z": 0, "height": 14, "width": 32, "name": "no name", "cssClass": "" },
        //                 { "areaid": 42, "x": 34, "y": 163, "z": 100, "height": 15, "width": 35, "name": "no name", "cssClass": "" },
        //                 { "areaid": 43, "x": 20, "y": 179, "z": 0, "height": 15, "width": 31, "name": "no name", "cssClass": "" }
        //             ];
        //             break;
        //         case 2222:
        //             $scope.imageSel = "../images/wacImages/Type B.png";
        //             $scope.areasArray = [
        //                 { "x": 223, "y": 7, "z": 0, "height": 36, "width": 39, "name": "Velg Depan Kanan", "cssClass": "" },
        //                 { "x": 120, "y": 201, "z": 100, "height": 24, "width": 26, "name": "Head Lamp Kiri", "cssClass": "" },
        //                 { "x": 71, "y": 206, "z": 0, "height": 14, "width": 30, "name": "Griell Radiator", "cssClass": "" },
        //                 { "x": 254, "y": 261, "z": 0, "height": 27, "width": 29, "name": "Spion Kiri", "cssClass": "" },
        //                 { "x": 67, "y": 236, "z": 0, "height": 18, "width": 39, "name": "Lower Griell", "cssClass": "" },
        //                 { "x": 303, "y": 341, "z": 0, "height": 22, "width": 30, "name": "Pintu Depan Kiri", "cssClass": "" },
        //                 { "x": 509, "y": 186, "z": 0, "height": 20, "width": 44, "name": "Stop Lamp Kiri", "cssClass": "" },
        //                 { "x": 69, "y": 167, "z": 0, "height": 19, "width": 39, "name": "Front Windshield Glass", "cssClass": "" },
        //                 { "x": 420, "y": 7, "z": 0, "height": 39, "width": 35, "name": "Velg Belakang Kanan", "cssClass": "" },
        //                 { "x": 245, "y": 50, "z": 0, "height": 24, "width": 40, "name": "no name", "cssClass": "" },
        //                 { "x": 380, "y": 336, "z": 0, "height": 23, "width": 32, "name": "Pintu Belakang Kiri", "cssClass": "" },
        //                 { "x": 557, "y": 208, "z": 0, "height": 25, "width": 41, "name": "Bumper Belakang", "cssClass": "" },
        //                 { "x": 312, "y": 79, "z": 0, "height": 24, "width": 28, "name": "Kaca Pintu Depan Kanan", "cssClass": "" },
        //                 { "x": 372, "y": 85, "z": 0, "height": 20, "width": 27, "name": "kaca Pintu Belakang Kanan", "cssClass": "" },
        //                 { "x": 297, "y": 38, "z": 0, "height": 22, "width": 28, "name": "Pintu Depan Kanan", "cssClass": "" },
        //                 { "x": 369, "y": 41, "z": 0, "height": 21, "width": 28, "name": "Pintu Belakang Kanan", "cssClass": "" },
        //                 { "x": 332, "y": 11, "z": 0, "height": 17, "width": 47, "name": "Stop Lamp Kanan", "cssClass": "" },
        //                 { "x": 344, "y": 190, "z": 0, "height": 36, "width": 40, "name": "no name", "cssClass": "" },
        //                 { "x": 258, "y": 125, "z": 0, "height": 27, "width": 28, "name": "no name", "cssClass": "" },
        //                 { "x": 324, "y": 154, "z": 0, "height": 24, "width": 50, "name": "no name", "cssClass": "" },
        //                 { "x": 431, "y": 59, "z": 0, "height": 20, "width": 32, "name": "no name", "cssClass": "" },
        //                 { "x": 607, "y": 184, "z": 0, "height": 21, "width": 41, "name": "no name", "cssClass": "" },
        //                 { "x": 564, "y": 176, "z": 0, "height": 19, "width": 28, "name": "no name", "cssClass": "" },
        //                 { "x": 561, "y": 151, "z": 0, "height": 20, "width": 35, "name": "no name", "cssClass": "" },
        //                 { "x": 323, "y": 240, "z": 0, "height": 20, "width": 52, "name": "no name", "cssClass": "" },
        //                 { "x": 466, "y": 182, "z": 0, "height": 32, "width": 30, "name": "no name", "cssClass": "" },
        //                 { "x": 232, "y": 360, "z": 0, "height": 36, "width": 42, "name": "no name", "cssClass": "" },
        //                 { "x": 423, "y": 358, "z": 0, "height": 38, "width": 44, "name": "no name", "cssClass": "" },
        //                 { "x": 380, "y": 297, "z": 0, "height": 20, "width": 29, "name": "no name", "cssClass": "" },
        //                 { "x": 318, "y": 297, "z": 0, "height": 24, "width": 29, "name": "no name", "cssClass": "" },
        //                 { "x": 333, "y": 372, "z": 0, "height": 19, "width": 52, "name": "no name", "cssClass": "" },
        //                 { "x": 440, "y": 323, "z": 0, "height": 19, "width": 31, "name": "no name", "cssClass": "" },
        //                 { "x": 245, "y": 323, "z": 0, "height": 23, "width": 37, "name": "no name", "cssClass": "" },
        //                 { "x": 17, "y": 234, "z": 0, "height": 20, "width": 39, "name": "no name", "cssClass": "" },
        //                 { "x": 68, "y": 220, "z": 0, "height": 15, "width": 33, "name": "no name", "cssClass": "" },
        //                 { "x": 68, "y": 191, "z": 0, "height": 15, "width": 33, "name": "no name", "cssClass": "" },
        //                 { "x": 17, "y": 201, "z": 0, "height": 24, "width": 33, "name": "no name", "cssClass": "" }
        //             ];
        //             break;
        //         case 2223:
        //             $scope.imageSel = "../images/wacImages/Type C.png";
        //             $scope.areasArray = [{ "x": 227, "y": 15, "z": 0, "height": 29, "width": 33, "name": "no name", "cssClass": "" }, { "areaid": 82, "x": 399, "y": 15, "z": 0, "height": 29, "width": 37, "name": "no name", "cssClass": "" }, { "areaid": 83, "x": 261, "y": 186, "z": 0, "height": 26, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 84, "x": 280, "y": 229, "z": 0, "height": 25, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 85, "x": 294, "y": 150, "z": 0, "height": 27, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 86, "x": 191, "y": 193, "z": 0, "height": 25, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 87, "x": 364, "y": 119, "z": 0, "height": 26, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 88, "x": 365, "y": 257, "z": 0, "height": 28, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 89, "x": 558, "y": 183, "z": 0, "height": 25, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 90, "x": 556, "y": 220, "z": 0, "height": 24, "width": 33, "name": "no name", "cssClass": "" }, { "areaid": 91, "x": 612, "y": 178, "z": 0, "height": 24, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 92, "x": 505, "y": 173, "z": 0, "height": 31, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 93, "x": 562, "y": 150, "z": 0, "height": 23, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 94, "x": 323, "y": 7, "z": 0, "height": 26, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 95, "x": 283, "y": 42, "z": 0, "height": 24, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 96, "x": 361, "y": 46, "z": 0, "height": 24, "width": 23, "name": "no name", "cssClass": "" }, { "areaid": 97, "x": 414, "y": 56, "z": 0, "height": 25, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 98, "x": 239, "y": 60, "z": 0, "height": 24, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 99, "x": 302, "y": 81, "z": 0, "height": 22, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 100, "x": 361, "y": 87, "z": 0, "height": 23, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 101, "x": 309, "y": 306, "z": 0, "height": 21, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 102, "x": 364, "y": 304, "z": 0, "height": 22, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 103, "x": 299, "y": 340, "z": 0, "height": 24, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 104, "x": 364, "y": 340, "z": 0, "height": 26, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 105, "x": 335, "y": 371, "z": 0, "height": 24, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 106, "x": 231, "y": 365, "z": 0, "height": 28, "width": 33, "name": "no name", "cssClass": "" }, { "areaid": 107, "x": 405, "y": 363, "z": 0, "height": 32, "width": 32, "name": "no name", "cssClass": "" }, { "areaid": 108, "x": 249, "y": 328, "z": 0, "height": 25, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 109, "x": 418, "y": 326, "z": 0, "height": 26, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 110, "x": 70, "y": 223, "z": 0, "height": 24, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 111, "x": 24, "y": 221, "z": 0, "height": 24, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 112, "x": 124, "y": 186, "z": 0, "height": 25, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 113, "x": 19, "y": 186, "z": 0, "height": 24, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 114, "x": 70, "y": 191, "z": 0, "height": 23, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 115, "x": 371, "y": 189, "z": 0, "height": 22, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 116, "x": 435, "y": 189, "z": 100, "height": 25, "width": 25, "name": "no name", "cssClass": "" }];
        //             break;
        //         case 2224:
        //             $scope.imageSel = "../images/wacImages/Type D.png";
        //             $scope.areasArray = [{ "x": 67, "y": 154, "z": 0, "height": 21, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 37, "x": 72, "y": 186, "z": 0, "height": 20, "width": 23, "name": "no name", "cssClass": "" }, { "areaid": 38, "x": 68, "y": 223, "z": 0, "height": 20, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 39, "x": 28, "y": 217, "z": 0, "height": 20, "width": 19, "name": "no name", "cssClass": "" }, { "areaid": 40, "x": 109, "y": 216, "z": 0, "height": 22, "width": 22, "name": "no name", "cssClass": "" }, { "areaid": 41, "x": 432, "y": 49, "z": 0, "height": 24, "width": 22, "name": "no name", "cssClass": "" }, { "areaid": 42, "x": 460, "y": 7, "z": 0, "height": 23, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 43, "x": 440, "y": 328, "z": 0, "height": 25, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 44, "x": 466, "y": 371, "z": 0, "height": 26, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 45, "x": 223, "y": 153, "z": 0, "height": 20, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 46, "x": 225, "y": 188, "z": 0, "height": 25, "width": 23, "name": "no name", "cssClass": "" }, { "areaid": 47, "x": 226, "y": 235, "z": 0, "height": 20, "width": 23, "name": "no name", "cssClass": "" }, { "areaid": 48, "x": 166, "y": 124, "z": 0, "height": 23, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 49, "x": 163, "y": 259, "z": 0, "height": 24, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 50, "x": 509, "y": 204, "z": 0, "height": 25, "width": 38, "name": "no name", "cssClass": "" }, { "areaid": 51, "x": 607, "y": 206, "z": 100, "height": 25, "width": 31, "name": "no name", "cssClass": "" }];
        //             break;
        //         case 2225:
        //             $scope.imageSel = "../images/wacImages/Type E.png";
        //             $scope.areasArray = [{ "x": 244, "y": 252, "z": 0, "height": 28, "width": 31, "name": "no name", "cssClass": "" }, { "areaid": 118, "x": 241, "y": 127, "z": 0, "height": 29, "width": 31, "name": "no name", "cssClass": "" }, { "areaid": 119, "x": 288, "y": 153, "z": 0, "height": 23, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 120, "x": 290, "y": 232, "z": 0, "height": 20, "width": 32, "name": "no name", "cssClass": "" }, { "areaid": 121, "x": 324, "y": 189, "z": 0, "height": 25, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 122, "x": 570, "y": 151, "z": 0, "height": 23, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 123, "x": 566, "y": 219, "z": 0, "height": 23, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 124, "x": 562, "y": 185, "z": 0, "height": 23, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 125, "x": 504, "y": 190, "z": 0, "height": 27, "width": 31, "name": "no name", "cssClass": "" }, { "areaid": 126, "x": 623, "y": 190, "z": 0, "height": 25, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 127, "x": 392, "y": 371, "z": 0, "height": 24, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 128, "x": 198, "y": 371, "z": 0, "height": 23, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 129, "x": 303, "y": 362, "z": 0, "height": 26, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 130, "x": 256, "y": 329, "z": 0, "height": 26, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 131, "x": 340, "y": 330, "z": 0, "height": 24, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 132, "x": 285, "y": 297, "z": 0, "height": 21, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 133, "x": 340, "y": 296, "z": 0, "height": 21, "width": 22, "name": "no name", "cssClass": "" }, { "areaid": 134, "x": 407, "y": 322, "z": 0, "height": 22, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 135, "x": 203, "y": 320, "z": 0, "height": 26, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 136, "x": 281, "y": 86, "z": 0, "height": 21, "width": 20, "name": "no name", "cssClass": "" }, { "areaid": 137, "x": 334, "y": 89, "z": 0, "height": 23, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 138, "x": 265, "y": 48, "z": 0, "height": 23, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 139, "x": 333, "y": 49, "z": 0, "height": 22, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 140, "x": 305, "y": 16, "z": 0, "height": 24, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 141, "x": 395, "y": 63, "z": 0, "height": 20, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 142, "x": 391, "y": 8, "z": 0, "height": 24, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 143, "x": 192, "y": 7, "z": 0, "height": 25, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 144, "x": 215, "y": 58, "z": 0, "height": 24, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 145, "x": 170, "y": 192, "z": 0, "height": 28, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 146, "x": 235, "y": 193, "z": 0, "height": 28, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 147, "x": 17, "y": 197, "z": 0, "height": 24, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 148, "x": 106, "y": 195, "z": 0, "height": 22, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 149, "x": 59, "y": 220, "z": 0, "height": 20, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 150, "x": 61, "y": 189, "z": 100, "height": 24, "width": 26, "name": "no name", "cssClass": "" }];
        //             break;
        //         case 2226:
        //             $scope.imageSel = "../images/wacImages/Type F.png";
        //             $scope.areasArray = [{ "x": 226, "y": 180, "z": 0, "height": 32, "width": 31, "name": "no name", "cssClass": "" }, { "areaid": 53, "x": 171, "y": 181, "z": 0, "height": 28, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 54, "x": 243, "y": 121, "z": 0, "height": 31, "width": 32, "name": "no name", "cssClass": "" }, { "areaid": 55, "x": 245, "y": 245, "z": 0, "height": 34, "width": 34, "name": "no name", "cssClass": "" }, { "areaid": 56, "x": 334, "y": 181, "z": 0, "height": 28, "width": 33, "name": "no name", "cssClass": "" }, { "areaid": 57, "x": 314, "y": 244, "z": 0, "height": 24, "width": 32, "name": "no name", "cssClass": "" }, { "areaid": 58, "x": 310, "y": 128, "z": 0, "height": 28, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 59, "x": 444, "y": 181, "z": 0, "height": 28, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 60, "x": 561, "y": 154, "z": 0, "height": 24, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 61, "x": 506, "y": 184, "z": 0, "height": 27, "width": 34, "name": "no name", "cssClass": "" }, { "areaid": 62, "x": 609, "y": 182, "z": 0, "height": 27, "width": 35, "name": "no name", "cssClass": "" }, { "areaid": 63, "x": 558, "y": 223, "z": 0, "height": 29, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 64, "x": 561, "y": 189, "z": 0, "height": 23, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 65, "x": 281, "y": 304, "z": 0, "height": 23, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 66, "x": 348, "y": 301, "z": 0, "height": 27, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 67, "x": 415, "y": 295, "z": 0, "height": 25, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 68, "x": 312, "y": 376, "z": 0, "height": 22, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 69, "x": 211, "y": 365, "z": 0, "height": 30, "width": 35, "name": "no name", "cssClass": "" }, { "areaid": 70, "x": 388, "y": 364, "z": 0, "height": 32, "width": 35, "name": "no name", "cssClass": "" }, { "areaid": 71, "x": 264, "y": 340, "z": 0, "height": 24, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 72, "x": 349, "y": 339, "z": 0, "height": 24, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 73, "x": 416, "y": 329, "z": 0, "height": 25, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 74, "x": 215, "y": 332, "z": 0, "height": 23, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 75, "x": 269, "y": 31, "z": 0, "height": 27, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 76, "x": 337, "y": 28, "z": 0, "height": 28, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 77, "x": 416, "y": 47, "z": 0, "height": 24, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 78, "x": 409, "y": 84, "z": 0, "height": 21, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 79, "x": 344, "y": 83, "z": 0, "height": 22, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 80, "x": 275, "y": 79, "z": 0, "height": 21, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 81, "x": 224, "y": 44, "z": 0, "height": 24, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 82, "x": 207, "y": 7, "z": 0, "height": 27, "width": 34, "name": "no name", "cssClass": "" }, { "areaid": 83, "x": 387, "y": 7, "z": 0, "height": 26, "width": 34, "name": "no name", "cssClass": "" }, { "areaid": 84, "x": 20, "y": 193, "z": 100, "height": 27, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 85, "x": 111, "y": 193, "z": 0, "height": 27, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 86, "x": 66, "y": 238, "z": 0, "height": 24, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 87, "x": 67, "y": 194, "z": 0, "height": 26, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 88, "x": 18, "y": 234, "z": 0, "height": 24, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 89, "x": 112, "y": 224, "z": 0, "height": 20, "width": 17, "name": "no name", "cssClass": "" }];
        //             break;
        //         case 2227:
        //             $scope.imageSel = "../images/wacImages/Type G.png";
        //             $scope.areasArray = [{ "x": 274, "y": 124, "z": 0, "height": 33, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 91, "x": 270, "y": 250, "z": 0, "height": 33, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 92, "x": 373, "y": 188, "z": 0, "height": 27, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 93, "x": 358, "y": 243, "z": 0, "height": 26, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 94, "x": 355, "y": 139, "z": 0, "height": 24, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 95, "x": 460, "y": 191, "z": 0, "height": 24, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 96, "x": 512, "y": 175, "z": 0, "height": 24, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 97, "x": 612, "y": 173, "z": 0, "height": 30, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 98, "x": 566, "y": 226, "z": 0, "height": 25, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 99, "x": 563, "y": 185, "z": 0, "height": 23, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 100, "x": 561, "y": 145, "z": 0, "height": 25, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 101, "x": 328, "y": 371, "z": 0, "height": 27, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 102, "x": 226, "y": 366, "z": 0, "height": 28, "width": 31, "name": "no name", "cssClass": "" }, { "areaid": 103, "x": 409, "y": 366, "z": 0, "height": 28, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 104, "x": 432, "y": 331, "z": 0, "height": 28, "width": 29, "name": "no name", "cssClass": "" }, { "areaid": 105, "x": 284, "y": 334, "z": 0, "height": 28, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 106, "x": 368, "y": 335, "z": 0, "height": 27, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 107, "x": 240, "y": 321, "z": 0, "height": 28, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 108, "x": 317, "y": 301, "z": 0, "height": 23, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 109, "x": 370, "y": 303, "z": 0, "height": 23, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 110, "x": 418, "y": 301, "z": 0, "height": 20, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 111, "x": 416, "y": 85, "z": 0, "height": 22, "width": 21, "name": "no name", "cssClass": "" }, { "areaid": 112, "x": 363, "y": 83, "z": 0, "height": 21, "width": 23, "name": "no name", "cssClass": "" }, { "areaid": 113, "x": 310, "y": 81, "z": 0, "height": 23, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 114, "x": 295, "y": 37, "z": 0, "height": 24, "width": 27, "name": "no name", "cssClass": "" }, { "areaid": 115, "x": 357, "y": 37, "z": 0, "height": 24, "width": 23, "name": "no name", "cssClass": "" }, { "areaid": 116, "x": 432, "y": 41, "z": 0, "height": 28, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 117, "x": 221, "y": 7, "z": 0, "height": 31, "width": 33, "name": "no name", "cssClass": "" }, { "areaid": 118, "x": 401, "y": 7, "z": 0, "height": 26, "width": 31, "name": "no name", "cssClass": "" }, { "areaid": 119, "x": 238, "y": 59, "z": 0, "height": 28, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 120, "x": 325, "y": 3, "z": 0, "height": 24, "width": 28, "name": "no name", "cssClass": "" }, { "areaid": 121, "x": 205, "y": 185, "z": 0, "height": 27, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 122, "x": 270, "y": 184, "z": 0, "height": 28, "width": 26, "name": "no name", "cssClass": "" }, { "areaid": 123, "x": 23, "y": 184, "z": 0, "height": 28, "width": 30, "name": "no name", "cssClass": "" }, { "areaid": 124, "x": 118, "y": 184, "z": 0, "height": 27, "width": 32, "name": "no name", "cssClass": "" }, { "areaid": 125, "x": 77, "y": 218, "z": 0, "height": 20, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 126, "x": 27, "y": 215, "z": 0, "height": 26, "width": 24, "name": "no name", "cssClass": "" }, { "areaid": 127, "x": 122, "y": 212, "z": 0, "height": 22, "width": 25, "name": "no name", "cssClass": "" }, { "areaid": 128, "x": 76, "y": 185, "z": 100, "height": 24, "width": 28, "name": "no name", "cssClass": "" }];
        //     };

        //     if ($scope.areasArray.length > 0) {
        //         console.log("inside if..");
        //         for (var i = 0; i < $scope.areasArray.length; i++) {
        //             $scope.areasArray[i]["areaid"] = i;
        //             // $scope.areasArray[i]["name"] = i;
        //             // $scope.areasArray[i]["height"] = 20;
        //             // $scope.areasArray[i]["width"] = 20;
        //         }
        //     };

        //     // $scope.doReload.rld($scope.areasArray);
        //     // console.log("inside func");
        //     // $scope.imageSel = selected.MasterId;
        //     // console.log($scope.imageSel);
        // };

        // $scope.onChangeAreas = function(ev, boxId, areas, area) {
        //     console.log(areas);
        //     $scope.logDots = JSON.stringify(areas);
        //     $scope.$apply();
        // }

        // $scope.onAddArea = function(ev, boxId, areas, area) {
        //     console.log(areas);
        //     $scope.logDots = JSON.stringify(areas);
        //     $scope.$apply();
        // }

        // $scope.onRemoveArea = function(ev, boxId, areas, area) {
        //     console.log(areas);
        //     $scope.logDots = JSON.stringify(areas);
        //     $scope.$apply();
        // }

        // $scope.doClick = function(evt) {
        //     console.log(evt);
        //     // console.log("formApi : ",$scope.areaApi);
        //     // var temp = _.findIndex($scope.areasArray, evt);
        //     // console.log(temp);
        //     // if(temp > -1) {
        //     //     if ($scope.areasArray[temp].cssClass == "") {
        //     //         $scope.areasArray[temp].cssClass = "choosen-area";
        //     //     } else {
        //     //         $scope.areasArray[temp].cssClass = "";
        //     //     }
        //     // }

        //     // $scope.formApi.setMode('detail');

        //     $scope.gridWac.data.push({ "ItemName": evt.name, Status: null });
        // };
        // $scope.gridWac = {
        //     enableSorting: true,
        //     enableRowSelection: true,
        //     multiSelect: true,
        //     enableSelectAll: true,
        //     //showTreeExpandNoChildren: true,
        //     // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        //     // paginationPageSize: 15,
        //     columnDefs: [
        //         { name: 'JobWACId', field: 'JobWACId', width: '7%', visible: false },
        //         { name: 'Nama Item', field: 'ItemName' },
        //         {
        //             name: 'Status Kerusakan',
        //             field: 'Status',
        //             editableCellTemplate: 'ui-grid/dropdownEditor',
        //             editDropdownValueLabel: 'Name',
        //             editDropdownIdLabel: 'Id',
        //             editDropdownOptionsArray: [
        //                 { 'Id': 1, 'Name': 'Penyok' },
        //                 { 'Id': 2, 'Name': 'Baret' },
        //                 { 'Id': 3, 'Name': 'Hilang' }
        //             ],
        //             cellFilter: "griddropdown:this"
        //         },
        //     ]
        // };


        // AppointmentGrService.getWoCategory().then(function(res) {
        //     $scope.woCategory = res.data.Result;
        //     $scope.dataCatWO = angular.copy($scope.woCategory);

        //     console.log('$scope.woCategory', $scope.woCategory);
        // });

        //=====================from WO Controller============================================





        // $scope.$on('$viewContentLoaded', function() {
        //     var velement = document.getElementById('jpcb_gr_edit_panel');
        //     var erd = elementResizeDetectorMaker();
        //     erd.listenTo(velement, function(element) {
        //         if (!isBoardLoaded){
        //             isBoardLoaded = true;
        //             showBoardJPCB();
        //             minuteTick();
        //             // untuk menampilkan timeline
        //             // $timeout( function(){
        //             //     minuteTick();
        //             // }, 1000);
        //         }
        //         vboard = JpcbGrViewFactory.getBoard(boardName);
        //         if (vboard){
        //             vboard.autoResizeStage();
        //             vboard.redraw();
        //         }

        //     });

        // });



        /*
        var vColumnWidth = 125;
        var vContainerName = 'jpcbbp-prod-container';
        var vobj = document.getElementById(vContainerName);
        if(vobj){
            var vwidth = vobj.clientWidth-20;
            vMaxCount= Math.floor(vwidth/vColumnWidth);
        } else {
            vMaxCount = 5;
        }

        var vWorklist = JpcbBpDataFactory.getWorkList();
        var vWLCount = vWorklist.length;
        var vopt = {}
        var vxcount = Math.ceil(vWLCount/vMaxCount);
        var vfixedHeaderHeight = vxcount*80+20;
        // if (vWLCount<=5){
        //     var vfixedHeaderHeight = 90;
        // } else {
        //     var vfixedHeaderHeight = 180;
        // }
        var prodBoardConfig = {
          type: 'jpcb',
          board:{
            // scroll: 'none',
            rowHeight: 80,
            columnWidth: vColumnWidth,
            maxSelectCol: vMaxCount,
            headerType: 'simple',
            firstColumnWidth: 220,
            footerHeight: 240,
            headerHeight: vfixedHeaderHeight+80,
            headerFill: '#ddd',
            showVerticalLine: true,
            breakChipName: 'break',
            verticalLineTop: vfixedHeaderHeight+40,
            container: vContainerName,
            iconFolder: 'images/boards/jpcb',
            firstColumnItems: [],
            // firstColumnItems: [
            //   {title: "STALL 1", technician: 'WAN', status: 'normal'},
            //   {title: "STALL 2 Testing 123456 AAABBBCC CCDDD Bas", technician: 'BUD', status: 'normal'},
            //   {title: "STALL 3", technician: 'MAT', status: 'normal'},
            //   {title: "STALL 4", technician: 'WDO', status: 'normal'},
            //   {title: "STALL 5", technician: 'FIT', status: 'normal'},
            //   {title: "STALL 6", technician: 'SUB', status: 'normal'},
            // ],
            firstColumnTitle: 'Stall',
            chipGroup: 'jpcbbp',
            footerChipName: 'jpcb_footer',
            fixedHeaderChipName: 'jpcb_fixed_header',
            fixedHeaderHeight: vfixedHeaderHeight,
            firstHeaderChipName: 'jpcb_first_header',
            firstHeaderData: {title: 'TEAM', subtitle1: 'Stall', subtitle2: 'Tech'},
            headerChipName: 'jpcb_header',
            firstColumnChipName: 'jpcb_stall',
            firstColumnBaseChipName: 'jpcb_stall_base',
          },
          margin: {
            left: 25
          },
          layout: {
            chipHeight: 600,
            chipGap: 2
          },
          chip: {
              stallTechWidth: 60,
              titleTop: vfixedHeaderHeight,
              subtitleTop: vfixedHeaderHeight+40,
              teamName: '***',
          }
        }

        var updateDragInfo = function(vcontrol, vobject){
            vcontrol.dragBoundFunc(function(pos){
                var vx = pos.x;
                var vref = this.controlable.getAbsolutePosition().x+this.minWidth;

                if (vx<vref){
                    vx = vref;
                }
                return {
                    x: vx,
                    y: this.getAbsolutePosition().y
                }
            });
            vcontrol.controlable = vobject;
            vcontrol.minWidth = 10;
            vcontrol.on('dragmove', function(){
                this.controlable.setWidth(this.x() - this.controlable.x());
            })
            vcontrol.draggable(true);
        }

        // var selectChip = function(vchip){
        //     theBoard.processChips(function(xchip){
        //         return true;
        //     },
        //     function(xchip){
        //         xchip.data.selected = false
        //         theBoard.updateChip(xchip);
        //     }, false);
        //     vchip.data.selected = true;
        //     selectedChipData = vchip.data;
        //     theBoard.updateChip(vchip);
        //     theBoard.redraw();
        // }

        // var setOnClick = function(){
        //     theBoard.processChips(function(vchip){
        //         return true;
        //     },
        //     function(vchip){
        //         vchip.on('mousedown', function(evt){
        //             //alert('click');
        //             selectChip(this);
        //         });
        //         vchip.setListening(true);
        //     }, false);
        // }
*/
        /*
        var prodBoard = false;
        var showProdBoard = function(vchips){
            // vchips = []

            prodBoard = new JsBoard.Container(prodBoardConfig);
            prodBoard.createChips(vchips);
            // prodBoard.redraw();

            setOnClick(prodBoard);

            prodBoard.autoResizeStage();
            prodBoard.redraw();
        }

        $scope.showProductionBoard = function(team, vdate){
            $scope.JpcbBpData.showProdBoard = true;
            $timeout(function(){
                console.log("team", team);
                prodBoardConfig.chip.teamName = team.name;
                prodBoardConfig.board.headerItems = JpcbBpDataFactory.getHeader(team.type);
                prodBoardConfig.board.firstColumnItems = JpcbBpDataFactory.getStall(team.name);
                // vchips = JpcbBpDataFactory.getData();
                var vrange = JpcbBpDataFactory.getRangeByType(team.type, vdate);
                console.log("***vrange***", vrange);
                vchips = JpcbBpDataFactory.getChipData(team.name, vrange.start, vrange.end, selectedChipData);
                console.log("***vchips***", vchips);
                showProdBoard(vchips);
            }, 10);
        }

        // console.log('data:', JpcbBpDataFactory.getData(vfilter));
        $scope.nextSelectChip = function(){
            $scope.JpcbBpData.showProdBoard = false;
            var vSelected = false;
            theBoard.processChips(function(xchip){
                if (xchip.data.selected){
                    return true;
                }
            },
            function(xchip){
                vSelected = xchip;
            }, false);
            if (!vSelected){
                //alert('Chip harus dipilih terlebih dahulu');
                showMessage('Chip harus dipilih terlebih dahulu');
            } else {
                console.log(vSelected.data.wotype);
                $scope.JpcbBpData.currentWoType = vSelected.data.wotype;
                $scope.JpcbBpData.teamList = JpcbBpDataFactory.getGroup($scope.JpcbBpData.currentWoType);
                $scope.JpcbBpData.screen_step = 2;
            }
        }

        $scope.backSelectChip = function(){
            $scope.JpcbBpData.screen_step = 1;
        }
*/
        /*
                $timeout(showBoard(boardChips),0);
                $timeout(function(){
                    $scope.filterDataByType('') //testing....
                },100);

                $timeout(function(){
                    // selectChip();
                },5000);

                $timeout(function(){
                    // $scope.jpcbbp_screen_step = 2;
                },10000);

                */

    });