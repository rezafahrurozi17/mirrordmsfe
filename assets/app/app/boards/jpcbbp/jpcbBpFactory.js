angular.module('app')
    .factory('JpcbBpFactory', function($http, CurrentUser, $filter, $timeout, jpcbFactory) {
        var currentUser = CurrentUser.user;
        var tmpWorkNames = [{ id: 0, name: 'Stall Body' }, { id: 1, name: 'Stall Putty' }, { id: 2, name: 'Stall Surfacer' }, { id: 3, name: 'Stall Spraying' }, { id: 4, name: 'Stall Polishing' }, { id: 5, name: 'Stall Re-assembly' }, { id: 6, name: 'Stall Final Inspection' }]
        var tmpGetStall = [];
        var tmpSelectedChip = [];
        var vdataTemp = [];
        var vdataTemp2 = []; //buat cek menumpuk kalau di onPlot
        var vdataTemp3 = []; //buat cek menumpuk kalau di extend
        var vgroupidTemp = null;
        var argTemp = null;
        var onFinishTemp = null;
        var dataChipTempSatuGroup = [];
        var baseMerah = 0; //750 kalau mau merah semua;
        var jmlHariMerah = 0;

        var CopyVboard = null;
        var TypeHeader = null;
        var longStart = null;
        var longEnd = null;
        var team = null;

        // console.log(currentUser);
        return {
            // hourDelay: 2.2,
            // minuteDelay: 6*60+0,
            // minuteDelay: 0,
            // onPlot: function(){
            //     // do nothing
            // },
            //boardName: 'jpcbviewboard',
            headerItems: {},
            lastIndex: 990,
            arrIndex: {},
            getIndexById: function(vid) {
                if (typeof this.arrIndex[vid] !== 'undefined') {
                    return this.arrIndex[vid];
                } else {
                    this.arrIndex[vid] = this.lastIndex;
                    this.lastIndex = this.lastIndex + 1;
                    return this.arrIndex[vid];
                }
            },
            onPlot: function() {
                // do nothing
            },
            selectedChip: function(param) {
                tmpSelectedChip = angular.copy(param);
                return tmpSelectedChip;
            },
            checkStatuStall: function(datachip) {
                if (datachip.nopol === tmpSelectedChip.nopol) {
                    if (tmpGetStall.length > 0) {
                        var workname = _.find(tmpWorkNames, { 'id': (datachip.ChipType - 1) });
                        var result = _.find(tmpGetStall, { 'title': workname.name });
                        if (result !== undefined) {
                            return result.idx;
                        } else {
                            return null
                        }
                        // console.log("================>",workname);
                        // console.log("================>",tmpWorkNames);
                        // console.log("================>",datachip.ChipType);
                        // console.log("================>",workname.name);

                    }
                    // console.log('1 ====>',datachip.ChipType,tmpWorkNames);
                    // var result = _.find(tmpWorkNames, { 'id': (datachip.ChipType - 1) });
                    // console.log("workname result", result,tmpGetStall);
                    // if(tmpGetStall.length>0){
                    //     var tes = _.find(tmpGetStall, { 'idx': result.id });
                    //     console.log("workname anjir", tes)
                    //     return tes.idx
                    // }
                }
            },
            showBoard: function(vname, config) {
                console.log('cek sini long start', config)
                var tempLongStart = config.currentChip.longStart.split(':');
                this.longStart = tempLongStart[0]+':'+tempLongStart[1];
                this.longEnd = config.currentChip.longEnd;
                console.log('kiwi an', this.longStart, ' dan ', this.longEnd)
                if (typeof config.incrementMinute !== 'undefined') {
                    this.incrementMinute = config.incrementMinute;
                }
                if (!config.options) {
                    config.options = {}
                }
                var vcontainer = config.container;
                //var vname = this.boardName;
                var vobj = document.getElementById(vcontainer);
                if (vobj) {
                    var vwidth = vobj.clientWidth;
                    // console.log(vwidth);
                    console.log('known obj', vwidth);
                    if (vwidth <= 0) {
                        return;
                    }
                } else {
                    console.log('unknown obj');
                    return;
                }
                if (typeof config.onPlot == 'function') {
                    this.onPlot = config.onPlot;
                }

                var vdurasi = 1;
                if (typeof config.currentChip.durasi !== 'undefined') {
                    vdurasi = config.currentChip.durasi;
                }
                config.currentChip.width = (vdurasi / 60) * this.incrementMinute;

                var vrow = -2;
                if (typeof config.currentChip.stallId !== 'undefined') {
                    if (config.currentChip.stallId > 0) {
                        vrow = config.currentChip.stallId - 1;
                    }
                }
                config.currentChip.row = vrow;
                var vhourStart = JsBoard.Common.getMinute(this.startHour) / 60;
                var vcol = 0;
                if (typeof config.currentChip.startTime !== 'undefined') {
                    vstartTime = config.currentChip.startTime;
                    vcol = vstartTime.getHours() + vstartTime.getMinutes() / 60 - vhourStart;
                }
                config.currentChip.col = vcol;

                var voptions = {
                    boardName: vname,
                    board: {
                        chipGroup: 'jpcb_bp',
                        // columnWidth: 140, kenapa ini 140? ga tau.. kalau 120 nya salah balikin aja ke 140
                        columnWidth: 120,
                        rowHeight: 75,
                        footerChipName: 'footer',
                        headerChipName: 'jpcb_header',
                        firstColumnWidth: 220,
                        stopageChipTop: 90,
                        snapDivider: 4
                    },
                    chip: {
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute
                    }
                }
                xvoptions = JsBoard.Common.extend(voptions, config.options);
                xvoptions.board = JsBoard.Common.extend(voptions.board, config.options.board);
                jpcbFactory.createBoard(vname, xvoptions);
                var me = this;
                var vxConfig = jpcbFactory.boardConfigList[vname];
                console.log("vxconfig", vxConfig, config);
                vxConfig.JobChipInfo = config.currentChip;
                jpcbFactory.setHeader(vname, {
                    headerHeight: 40,
                    onGetHeader: function(arg, vfunc) {
                        return me.onGetHeader(arg, vfunc);
                    }
                });
                jpcbFactory.setStall(vname, {
                    //firstColumnWidth: 120,
                    onGetStall: function(arg, vfunc) {
                        return me.onGetStall(arg, vfunc);
                    }
                });

                jpcbFactory.setOnGetChips(vname, function(arg, vfunc) {
                    return me.onGetChips(arg, vfunc);
                });

                jpcbFactory.setOnChipLoaded(vname, function(vjpcb) {
                    return me.onChipLoaded(vjpcb);
                });

                jpcbFactory.setOnBoardReady(vname, function(vjpcb) {
                    return me.onBoardReady(vjpcb);
                });

                // jpcbFactory.setSelectionArea(vname, {tinggiArea: 4, fixedHeaderChipName: 'blank'});
                jpcbFactory.setSelectionArea(vname, {
                    tinggiArea: 110,
                    tinggiHeader: 40,
                    tinggiSubHeader: 40,
                    fixedHeaderChipName: 'blank',
                });


                if (config.options.stopageAreaHeight) {
                    jpcbFactory.setStopageArea(vname, { tinggiArea: config.options.stopageAreaHeight });
                }


                jpcbFactory.setArgChips(vname, config.currentChip);
                jpcbFactory.setArgStall(vname, config.currentChip);
                jpcbFactory.setArgHeader(vname, config.currentChip);

                if (config.onChipClick) {
                    jpcbFactory.setOnCreateChips(vname, function(vchip) {
                        vchip.on('click', config.onChipClick);
                        // vchip.on('dragmove', config.onChipMove);
                    });
                }
                
                // if (config.onChipMove) {
                //     jpcbFactory.setOnCreateChips(vname, function(vchip) {
                //         console.log('ada batu', vchip)
                //         // vchip.on('click', config.onChipClick);
                //         vchip.on('dragmove', config.onChipMove);
                //     });
                // }

                // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

                // if (config.standardDraggable){
                //     jpcbFactory.setOnCreateChips(vname, function(vchip){
                //         if (vchip.data.nopol=='B 7002 AA'){
                //             if (false){

                //             }
                //         }
                //         var vdrag = false;
                //         if ((typeof vchip.data !== 'undefined') && (typeof vchip.container.layout!=='undefined')){
                //             var vlayout = vchip.container.layout;
                //             if (vchip.data.col>=vlayout.timeLine){
                //                 vdrag = true;
                //             }
                //         }
                //         vchip.draggable(vdrag);

                //         // if (vchip.data.selected){
                //         //     vchip.draggable(true);
                //         // }
                //     });
                // }

                var me = this;
                var the_config = config;
                jpcbFactory.setOnDragDrop(vname, function(vchip) {
                    console.log("Naro Boyyyy CAPEEEEE", tmpWorkNames, vchip);
                    var curDate = config.currentChip.currentDate;
                    var vdurasi = 1;
                    if (typeof config.currentChip.durasi !== 'undefined') {
                        vdurasi = config.currentChip.durasi;
                    }

                    var vmin = me.incrementMinute * vchip.data['col'];
                    var vminhour = JsBoard.Common.getMinute(me.startHour) + vmin;
                    var vres = JsBoard.Common.getTimeString(vminhour);
                    var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
                    startTime.setMinutes(startTime.getMinutes() + vmin);

                    var emin = vmin + vdurasi * 60;
                    var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
                    endTime.setMinutes(endTime.getMinutes() + emin);

                    vchip.data.startTime = startTime;
                    vchip.data.endTime = endTime;
                    var vStallId = 0;
                    if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined') &&
                        (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined') &&
                        (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')) {
                        vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                    };

                    if ((vchip.data.row == -2) && (typeof vchip.container.layout.infoRows[vchip.data.row] == undefined)) {
                        vStallId = -2;
                    };
                    vchip.data.stallId = vStallId;
                    console.log('setOnDragDrop stall id', vchip.data);

                    // if (typeof the_config.onPlot == 'function') {
                    //     the_config.onPlot(vchip);
                    // };
                    // ================== TERUSAN ========
                    if (typeof me.onPlot == 'function') {
                        delete vchip.data.start1;
                        delete vchip.data.width1;
                        delete vchip.data.start2;
                        delete vchip.data.width2;
                        delete vchip.data.start3;
                        delete vchip.data.width3;
                        me.updateChipBreak(vchip.data);
                        var vboard = jpcbFactory.getBoard(vchip.container.name);

                        // me.onPlot(vchip.data);
                        vboard.chipItems[vchip.data.dataIndex] = vchip.data;

                        CopyVboard = vboard;

                        console.log("vboard plsssss", vboard, vboard.redrawChips);
                        if (vboard.board.selectbox) {
                            console.log("masuk sini gk?");
                            vboard.board.selectbox.destroyChildren();
                        }
                        console.log('ini vchip abis redraw', vchip);
                        console.log('ini the_config abis redraw', the_config);
                        console.log('ini vboard abis redraw', vboard);

                       

                        vboard.redrawChips();

                        // console.log('showboard exec');
                        jpcbFactory.showBoard(vname, vcontainer);
                        // console.log('onPlot exec');
                        the_config.onPlot(vchip);

                        console.log('vdataTemp sebelom timeout', vdataTemp);

                        dataChipTempSatuGroup = [];

                        setTimeout(function() {

                            //==========bisa ga nih buat update break=============
                            // me.onGetChips(argTemp, onFinishTemp);
                            // for (var i = 0; i < vdataTemp.length; i++) {
                            //     console.log('masuk loop');
                            //     if (vdataTemp[i].nopol == vchip.data.nopol) {
                            //         // dataChipTempSatuGroup.push(vdataTemp[i]);

                            //         if ((vdataTemp[i].start2 != undefined && vdataTemp[i].start2 != null) && vdataTemp[i].intStatus!=6 ) {
                            //             console.log('masuk if 2', i);
                            //             console.log('vdatatemp iii', vdataTemp[i]);
                            //             var plotUrl = '/api/as/Boards/BP/plotBoardJPCB?UnPlot=0&RePlot=1';
                            //             var xDateStart = me.columnToTime('jpcb-bp-board', vdataTemp[i].col);
                            //             var xDateEnd = me.columnToTime('jpcb-bp-board', vdataTemp[i].col + vdataTemp[i].width, 0.1);

                            //             var vDateStart = $filter('date')(xDateStart, 'yyyy-MM-dd HH:mm:ss');
                            //             var vDateEnd = $filter('date')(xDateEnd, 'yyyy-MM-dd HH:mm:ss');
                            //             var vobj = {
                            //                 "DateStart": vDateStart,
                            //                 "DateFinish": vDateEnd,
                            //                 "StallId": vdataTemp[i].stallId,
                            //                 "JobId": vdataTemp[i].jobId,
                            //                 "ChipType": vdataTemp[i].chipType,
                            //                 "GroupId": vgroupidTemp,
                            //                 "JobTaskBpId": vdataTemp[i].jobtaskbpid
                            //             };
                            //             console.log("vobj-fac", vobj);

                            //             $http.put(plotUrl, [vobj]).then(
                            //                 function(res) {
                            //                     console.log('xres1-fac', res);
                                                console.log('vname-fac', vname)
                            //                     console.log('vcontainer-fac', vcontainer)
                            //                     // jpcbFactory.showBoard(vname, vcontainer);
                            //                     // me.updateChipBreak(vdataTemp[i]);
                            //                     console.log('tes dulu')
                            //                     // jpcbFactory.redrawBoard(vname);
                            //                 }
                            //             );
                            //         }
                            //     }
                            // }
                            me.CekReplot(vdataTemp, vchip, vname, 0);
                            // me.CekReplot(vdataTemp, vchip, vname, vchip.data.chipType-1);

                            // setTimeout(function(){
                            //     jpcbFactory.showBoard(vname, vcontainer);
                            // },500);



                            //==========bisa ga nih buat update break=============


                            setTimeout(function() {
                                //=======================cek menumpuk setelah auto waterfall plotting========================

                                for (var i = 0; i < vdataTemp2.length; i++) {
                                    if (vdataTemp2[i].nopol == vchip.data.nopol) {
                                        dataChipTempSatuGroup.push(vdataTemp2[i]);
                                    }
                                }

                                var cek = angular.copy(dataChipTempSatuGroup);
                                console.log('dataChipTempSatuGroup cek', cek)
                                console.log('vdataTemp2 cek', vdataTemp2)
                                for (var i = 0; i < dataChipTempSatuGroup.length; i++) {
                                    var menumpuk = 0;

                                    console.log('masuk loop dataChipTempSatuGroup1');

                                    for (var j = 0; j < vdataTemp.length; j++) {
                                        if (dataChipTempSatuGroup[i].stallId > 0) {
                                            if ((vdataTemp[j].chipType == dataChipTempSatuGroup[i].chipType) && (vdataTemp[j].nopol != dataChipTempSatuGroup[i].nopol) && (vdataTemp[j].ChipGroupId == vgroupidTemp) &&
                                                (vdataTemp[j].stallId == dataChipTempSatuGroup[i].stallId) &&
                                                (vdataTemp[j].chipType >= vchip.data.chipType && dataChipTempSatuGroup[i].chipType >= vchip.data.chipType)) {
                                                console.log('masuk if tumpuk');
                                                var startActiveChip = dataChipTempSatuGroup[i].col;
                                                var endActiveChip = dataChipTempSatuGroup[i].col + dataChipTempSatuGroup[i].width;

                                                var startPlottedChip = vdataTemp[j].col;
                                                var endPlottedChip = vdataTemp[j].col + vdataTemp[j].width;

                                                



                                                //cek apakah plotted chip ada di tengah active chip
                                                if ((startActiveChip <= startPlottedChip) && (endActiveChip > startPlottedChip)) {
                                                    menumpuk = 1;
                                                    console.log('startActiveChip', startActiveChip)
                                                    console.log('endActiveChip', endActiveChip)
                                                    console.log('startPlottedChip', startPlottedChip)
                                                    console.log('endPlottedChip', endPlottedChip)
                                                    console.log('dataChipTempSatuGroup', dataChipTempSatuGroup[i])
                                                    console.log('vdataTemp', vdataTemp[j])
                                                }

                                                // cek apakah active chip ada di tengah plotted chip
                                                if ((startPlottedChip <= startActiveChip) && (endPlottedChip > startActiveChip)) {
                                                    menumpuk = 1;
                                                    console.log('startActiveChip', startActiveChip)
                                                    console.log('endActiveChip', endActiveChip)
                                                    console.log('startPlottedChip', startPlottedChip)
                                                    console.log('endPlottedChip', endPlottedChip)
                                                    console.log('dataChipTempSatuGroup', dataChipTempSatuGroup[i])
                                                    console.log('vdataTemp', vdataTemp[j])
                                                }
                                            }
                                        }

                                        if (menumpuk == 1) {
                                            break;
                                        }

                                    }

                                    if (menumpuk == 1) {
                                        console.log('menumpuk', menumpuk);
                                        console.log('dataChipTempSatuGroup xxx', dataChipTempSatuGroup[i]);
                                        var plotUrl = '/api/as/Boards/BP/plotBoardJPCB?UnPlot=1';
                                        var xDateStart = me.columnToTime('jpcb-bp-board', dataChipTempSatuGroup[i].col);
                                        var xDateEnd = me.columnToTime('jpcb-bp-board', dataChipTempSatuGroup[i].col + dataChipTempSatuGroup[i].width);
                                        var vDateStart = $filter('date')(xDateStart, 'yyyy-MM-dd HH:mm:ss');
                                        var vDateEnd = $filter('date')(xDateEnd, 'yyyy-MM-dd HH:mm:ss');
                                        var vobj = {
                                            "DateStart": vDateStart,
                                            "DateFinish": vDateEnd,
                                            "StallId": dataChipTempSatuGroup[i].stallId,
                                            "JobId": dataChipTempSatuGroup[i].jobId,
                                            "ChipType": dataChipTempSatuGroup[i].chipType,
                                            "GroupId": vgroupidTemp,
                                            "JobTaskBpId": dataChipTempSatuGroup[i].jobtaskbpid
                                        };
                                        console.log("vobj-fac", vobj);

                                        $http.put(plotUrl, [vobj]).then(
                                            function(res) {
                                                console.log('xres1-facX', res);
                                                console.log('vname-facX', vname)
                                                console.log('vcontainer-facX', vcontainer)
                                                    // vboard.redrawChips();
                                                jpcbFactory.showBoard(vname, vcontainer);
                                                // jpcbFactory.redrawBoard(vname);

                                            }
                                        );
                                        break;
                                    }

                                }
                                //=======================cek menumpuk setelah auto waterfall plotting========================
                            }, 7000);






                        }, 3000);

                    }

                });

                jpcbFactory.showBoard(vname, vcontainer)
                    // $timeout(function(){
                    //     jpcbFactory.showBoard(vname, vcontainer)

                // }, 100);

            },

            CekReplot: function(vdataTemp, vchip, vname, x){
                me = this;
                var DraggedChip = vchip.data.chipType;
                // for (var i = 0; i < vdataTemp.length; i++) {
                if(x < vdataTemp.length){
                    console.log('masuk loop doer', vdataTemp);
                    if (vdataTemp[x].nopol == vchip.data.nopol) {
                        if(vdataTemp[x].chipType >= DraggedChip){
                            console.log('cek nih', vdataTemp[x].chipType, ' cek ', DraggedChip)
                            console.log('cek nih2', vdataTemp[x])
                            // dataChipTempSatuGroup.push(vdataTemp[i]);
                            if ((vdataTemp[x].start2 != undefined && vdataTemp[x].start2 != null) && vdataTemp[x].intStatus!=6 ) {
                                console.log('masuk if 2', x);
                                console.log('vdatatemp iii', vdataTemp[x]);
                                var plotUrl = '/api/as/Boards/BP/plotBoardJPCB?UnPlot=0&RePlot=1';
                                var xDateStart = me.columnToTime('jpcb-bp-board', vdataTemp[x].col);
                                var xDateEnd = me.columnToTime('jpcb-bp-board', vdataTemp[x].col + vdataTemp[x].width);

                                var vDateStart = $filter('date')(xDateStart, 'yyyy-MM-dd HH:mm:ss');
                                var vDateEnd = $filter('date')(xDateEnd, 'yyyy-MM-dd HH:mm:ss');
                                var vobj = {
                                    "DateStart": vDateStart,
                                    "DateFinish": vDateEnd,
                                    "StallId": vdataTemp[x].stallId,
                                    "JobId": vdataTemp[x].jobId,
                                    "ChipType": vdataTemp[x].chipType,
                                    "GroupId": vgroupidTemp,
                                    "JobTaskBpId": vdataTemp[x].jobtaskbpid
                                };
                                console.log("vobj-fac", vobj);

                                $http.put(plotUrl, [vobj]).then(
                                    function(res) {
                                        console.log('xres1-fac', res);
                                        // console.log('vname-fac', vname)
                                        console.log('vcontainer-fac', vcontainer)
                                        me.CekReplot(vdataTemp, vchip, vname, 100000); // di set 100000 karena cm pengen sekali manggil replot nya
                                        // jpcbFactory.showBoard(vname, vcontainer);
                                        // me.updateChipBreak(vdataTemp[i]);
                                        console.log('tes dulu')
                                        // jpcbFactory.redrawBoard(vname);
                                    }
                                );
                            } else {
                                this.CekReplot(vdataTemp, vchip, vname, x+1);
                            }

                        } else {
                            this.CekReplot(vdataTemp, vchip, vname, x+1);
                        }
                        
                    } else {
                        this.CekReplot(vdataTemp, vchip, vname, x+1);
                    }

                } else {
                    jpcbFactory.showBoard(vname, vcontainer);
                }
                    
                // }

            },

            // startHour: '08:00',
            // endHour: '17:00',
            startHour: this.longStart,
            endHour: this.longEnd,
            // endHour: '22:00',
            breakHour: '12:00',
            incrementMinute: 60,
            jmlHariTps: 3, //tadi nya 3, di ubah jadi 1 karena diminta nampilin 1 hari aja
            jmlHariLight: 7, //tadi nya 7, diubah jadi 1 karena diminta pa ian
            jmlHariMedium: 30,
            jmlHariHeavy: 90,
            timeToColumn: function(vName, vDate, vtime) {
                // var vboard = jpcbFactory.getBoard(vName);
                // var vcolData = vboard.layout.board.headerItems;
                var vcolData = this.headerItems[vName];
                var vresult = 0;
                // if (vtime > '18:00') {
                //     vtime = '18:00';
                // }
                if (vtime > '23:00') {
                    vtime = '23:00';
                }
                console.log('ini aneh lagi', vcolData)
                for (var i = 0; i < vcolData.length; i++) {
                    var vitem = vcolData[i];
                    var xdate = $filter('date')(vitem.dateTime, 'yyyy-MM-dd');
                    if (xdate == vDate) {

                        // var vstartmin = JsBoard.Common.getMinute(this.startHour);
                        var vstartmin = JsBoard.Common.getMinute(this.longStart);
                        var vmin = JsBoard.Common.getMinute(vtime) - vstartmin;
                        if (vmin < 0) {
                            vmin = 0;
                        }
                        var vresult = i + vmin / this.incrementMinute;
                        return vresult;
                    }
                }


                return vresult;
            },
            columnToTime: function(vname, vcolumn, vdelta) {
                if (typeof vdelta == 'undefined') {
                    vdelta = 0;
                }
                var vboard = jpcbFactory.getBoard(vname);
                console.log('vboard nih', vboard);
                console.log('vname nih', vname);

                if (vboard == undefined){
                    vboard = CopyVboard;
                }

                var vcolData = vboard.layout.board.headerItems;
                var vcol = Math.floor(vcolumn - vdelta);
                var xcol = vcolumn - vcol;
                var vbase = null;
                vbase = new Date(vcolData[vcol].dateTime);
                var vmin = this.incrementMinute * xcol;
                if (vmin > 0) {
                    vmin = Math.ceil(vmin);
                    vbase.setMinutes(vbase.getMinutes() + vmin);
                }
                return vbase;




                // var vstartmin = JsBoard.Common.getMinute(this.startHour);
                // var vmin = vcolumn * this.incrementMinute + vstartmin;
                // var vresult = JsBoard.Common.getTimeString(vmin);

                // return vresult;
            },
            checkAppointmentStatus: function(vtime) {
                // vtime: time-nya data
                // currentHour: jam/minute saat ini
                var vDate = this.getCurrentTime();
                // console.log("current Time#####", vDate.getTime());
                var currentHour = $filter('date')(vDate, 'HH:mm');

                var vcurrmin = JsBoard.Common.getMinute(currentHour);
                var vmin = JsBoard.Common.getMinute(vtime);
                if (vmin < vcurrmin) {
                    return 'no_show';
                }
                return 'appointment';

            },
            onGetHeader: function(arg, onFinish) {
                // vtype = 'tps';
                vtype = TypeHeader;
                varg = JsBoard.Common.extend({ startDate: new Date(), dateCount: 1 }, arg);
                var vWorkHour = JsBoard.Common.generateDayHours({
                    startHour: this.startHour,
                    endHour: this.endHour,
                    increment: this.incrementMinute,
                });
                // var result = [];
                // for(var idx in vWorkHour){
                //     var vitem = {title: vWorkHour[idx]}
                //     if (vWorkHour[idx]==this.breakHour){
                //         vitem.colInfo = {isBreak: true}
                //     }
                //     result.push(vitem)

                // }
                // // console.log("header", result);
                var vDate = new Date($filter('date')(arg.currentDate, 'yyyy-MM-dd'));
                result = this.getHeader(vtype, vDate);
                this.headerItems[arg.boardName] = result;
                $timeout(function() {
                    onFinish(result);
                }, 10);

            },
            onGetStall: function(arg, onFinish) {
                // $http.get('/api/as/Stall').
                // var vurl = '/api/as/Boards/Stall/0';
                var vurl = arg.stallUrl;
                // var vrandom = Math.random()*10000+'';
                // if (vurl.indexOf('?')>=0){
                //     vurl = vurl+'&_xrnd='+vrandom;
                // } else {
                //     vurl = vurl+'?_xrnd='+vrandom;
                // }
                $http.get(vurl, { arg: arg }).then(function(res) {
                    if (typeof arg.stallVisible == 'undefined') {
                        arg.stallVisible = {}
                    }
                    tmpGetStall = [];
                    var xres = res.data.Result;
                    var result = [];
                    // sini bill
                    // console.log('asow', JsBoard.Common.getMinute("00:00:00") - JsBoard.Common.getMinute(this.startHour));
                    // console.log('asow', JsBoard.Common.getMinute("20:00:00") - JsBoard.Common.getMinute("00:00:00"));
                    var startBreak4Val = 0;
                    var durationBreak4Val = baseMerah * jmlHariMerah;
                    // var startBreak4Val = JsBoard.Common.getMinute("00:00:00") - JsBoard.Common.getMinute(this.startHour);
                    // var durationBreak4Val = JsBoard.Common.getMinute("17:00:00") - JsBoard.Common.getMinute("00:00:00");


                    for (var i = 0; i < xres.length; i++) {
                        var vitem = xres[i];
                        if (typeof arg.stallVisible[vitem.StallId] == 'undefined') {
                            arg.stallVisible[vitem.StallId] = { visible: true, title: vitem.Name };
                        }
                        if (arg.stallVisible[vitem.StallId].visible) {
                            var vtech = '';
                            if (vitem.Initial1) {
                                vtech = vtech + vitem.Initial1;
                            }
                            if (vitem.Initial2) {
                                vtech = vtech + ', ' + vitem.Initial2;
                            }
                            if (vitem.Initial3) {
                                vtech = vtech + ', ' + vitem.Initial3;
                            }
                            result.push({
                                title: vitem.Name,
                                status: 'normal',
                                stallId: vitem.StallId,
                                // technician: 'X-'+vitem.StallId,
                                technician: vtech,
                                // startBreak4: 0,
                                // durationBreak4: 0,
                                startBreak4: startBreak4Val,
                                durationBreak4: durationBreak4Val,

                            });
                            tmpGetStall.push({
                                title: vitem.Name,
                                status: 'normal',
                                stallId: vitem.StallId,
                                idx: i,
                                // technician: 'X-'+vitem.StallId,
                                technician: vtech,
                                // startBreak4: 0,
                                // durationBreak4: 0,
                                startBreak4: startBreak4Val,
                                durationBreak4: durationBreak4Val,

                            });
                            console.log("tmpGetStall", tmpGetStall);


                        }
                    }
                    // onFinish(result);
                    onFinish(tmpGetStall);
                })

            },
            findStallIndex: function(vStallId, vList) {
                for (var i = 0; i < vList.length; i++) {
                    var vitem = vList[i];
                    if ((typeof vitem.stallId !== 'undefined') && (vitem.stallId == vStallId)) {
                        return i;
                    }
                }
                return -1;
            },
            reformatTime: function(vTime) {
                if (vTime) {
                    return JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vTime));
                }
                return '';
            },
            // isProblem: function(vitem){
            //     var vresult = false;
            //     var vstatus = vitem.Status;
            //     var vNow = this.getCurrentTime();
            //     var xStart = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd')+' '+vitem.PlanStart;
            //     var xFinish = $filter('date')(vitem.PlanDateFinish, 'yyyy-MM-dd')+' '+vitem.PlanFinish;
            //     var xNow = $filter('date')(vNow, 'yyyy-MM-dd HH:mm:ss');
            //     console.log("start", xStart);
            //     console.log("finish", xFinish);
            //     console.log("Now", xNow);
            //     // 5. Problem
            //     //    a. waktu awal < sekarang tapi status < 5 and status>=3
            //     if ((xStart<xNow) && (vstatus<5) && (vstatus>=3)){
            //         vresult = true;
            //     }
            //     //    b. ketika status belum done tapi jam akhir < jam sekarang
            //     if ((xFinish<xNow) && (vstatus<17) && (vstatus>=3)){
            //         vresult = 'irregular';
            //     }
            //     return vresult;

            // },
            checkStatus: function(vitem) {
                var vstatus = vitem.Status;
                var vresult = '';
                if (vstatus <= 2) {
                    vresult = 'belumdatang';
                } else if (vstatus == 3) {
                    vresult = 'sudahdatang';
                } else if (vstatus == 4) {
                    if (vitem.isAppointment) {
                        vresult = 'booking';
                    } else {
                        vresult = 'walkin';
                    }
                } else if ((vstatus >= 5) && (vstatus <= 16)) {
                    vresult = 'inprogress';
                } else if (vstatus >= 17) {
                    vresult = 'done';
                }

                // 5. Problem
                //    a. waktu awal < sekarang tapi status < 5 and status>=3

                // var vNow = this.getCurrentTime();
                // var xStart = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd')+' '+vitem.PlanStart;
                // var xFinish = $filter('date')(vitem.PlanDateFinish, 'yyyy-MM-dd')+' '+vitem.PlanFinish;
                // var xNow = $filter('date')(vNow, 'yyyy-MM-dd HH:mm:ss');
                // console.log("start", xStart);
                // console.log("finish", xFinish);
                // console.log("Now", xNow);
                // if ((xStart<xNow) && (vstatus<5) && (vstatus>=3)){
                //     vresult = 'irregular';
                // }

                // //    b. ketika status belum done tapi jam akhir < jam sekarang
                // if ((xFinish<xNow) && (vstatus<17) && (vstatus>=3)){
                //     vresult = 'irregular';
                // }

                if (jpcbFactory.isJobProblem(vitem)) {
                    vresult = 'irregular';
                }

                return vresult;
            },
            updateStatusIcons: function(vdata) {
                var vresult = [];
                console.log('vdata >>', vdata);
                // customer waiting
                // is waiting 0 waiting 1 drop off
                if (vdata.isWaiting) {
                    vresult.push('customer');
                }
                // is Waiting for Pats
                if (vdata.isWaitingPart) {
                    vresult.push('waitparts');
                }
                //------------------- kodingan lama---------------------------------
                // in progress
                // 5 = Clock ON *
                // 6 = Clock Pause
                // 7 = Clock resume
                // 8 = Clock Off *
                // 9 = Qc start *
                // 10 = Qc finish *

                // if ((vdata.intStatus == 5) || (vdata.intStatus == 7) || (vdata.intStatus == 9)) {
                //     vresult.push('inprogress');
                // }
                // if (vdata.intStatus == 6) {
                //     vresult.push('box');
                // }
                //------------------- kodingan lama---------------------------------

                //----ini baru------------------------------------------------------
                // 0 = belum mulai proses
                // 1 = sudah mulai clock on
                // 2 = clock pause
                // 3 = clock pause finish
                // 4 = clock off
                // 5 = clock on QC
                // 6 = clock off Qc
                if ((vdata.intStatus == 1) || (vdata.intStatus == 3) || (vdata.intStatus == 5)) {
                    vresult.push('inprogress');
                }
                // multiple task
                if (vdata.jmltask > 1) {
                    vresult.push('multitask');
                }
                //Carry OVER

                // Check Status Chip, is Paused ?
                if (vdata.intStatus == 2) {
                    vresult.push('box');
                }
                // Warranty
                if (vdata.wocategoryname == 'TWC' || vdata.wocategoryname == 'PWC') {
                    if (vdata.wocategoryname == 'TWC') {
                        vresult.push('TWC');
                    } else {
                        vresult.push('PWC');
                    }
                }

                // RTJ
                if (vdata.wocategoryname == 'RTJ') {
                    vresult.push('returnjob');
                }

                if(vdata.isCarryOver >= 1){
                    vresult.push('carryover')
                }

                vdata.statusicon = vresult;
            },
            onChipLoaded: function(vjpcb) {
                // vjpcb.allChips = []
                // return;
                var vresult = [];
                console.log('onchipLoaded chip oi', vjpcb.allChips);
                for (var i = 0; i < vjpcb.allChips.length; i++) {
                    var vitem = vjpcb.allChips[i];
                    if (vitem.stallId == -2) {
                        vitem.row = -2;
                        vresult.push(vitem);
                    } else if (vitem.stallId == -1) {
                        vitem.row = -1;
                        vresult.push(vitem);
                    } else {
                        vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                        if (vidx >= 0) {
                            vitem.row = vidx;
                            vresult.push(vitem);
                        }
                    }

                }
                vjpcb.allChips = vresult;
            },
            onBoardReady: function(vjpcb) {
                this.updateTimeLine(vjpcb.boardName);
                if (vjpcb.onJpcbReady) {
                    vjpcb.onJpcbReady(vjpcb);
                }
            },
            getDayCountByType: function(vtype) {
                //tps 3 hari, light 7hari, medium 30hari, heavy 90hari
                console.log('jumlah hari Board', vtype)
                if (vtype == 'light') {
                    jmlHariMerah = this.jmlHariLight;
                    return this.jmlHariLight;
                } else if (vtype == 'medium') {
                    jmlHariMerah = this.jmlHariMedium;
                    return this.jmlHariMedium;
                } else if (vtype == 'heavy') {
                    jmlHariMerah = this.jmlHariHeavy;
                    return this.jmlHariHeavy;
                } else if (vtype == 'tps'){
                    jmlHariMerah = this.jmlHariTps;
                    return this.jmlHariTps;
                } else {
                    jmlHariMerah = this.jmlHariTps;
                    return this.jmlHariTps;
                }
                
            },
            onGetChips: function(arg, onFinish) {
                console.log('arg', arg);
                argTemp = arg;
                onFinishTemp = onFinish;
                console.log('onFinish', onFinish);
                // var vtype = 'tps';
                var vtype = TypeHeader;
                var vcount = this.getDayCountByType(vtype);
                var vArg = { nopol: 'B 1000 AXA', tipe: 'Avanza' }
                vArg = JsBoard.Common.extend(vArg, arg);

                var tglAwal = arg.currentDate;
                var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
                tglAkhir.setDate(tglAkhir.getDate() + vcount - 1);
                var vjobid = vArg.JobId;
                if (!vjobid) {
                    vjobid = 0;
                }
                var vgroupid = vArg.groupId
                vgroupidTemp = vArg.groupId
                var vurl = '/api/as/Boards/BP/JobList/' + vjobid + '/' + vgroupid + '/' + $filter('date')(tglAwal, 'yyyy-MM-dd') + '/' + $filter('date')(tglAkhir, 'yyyy-MM-dd');

                var workNames = { 1: 'Body', 2: 'Putty', 3: 'Surfacer', 4: 'Spray', 5: 'Polish', 6: 'Reassembly', 7: 'Final Insp' }
                var me = this;
                var vboardName = arg.boardName;
                $http.get(vurl).then(function(res) {
                    var dataResCopy = angular.copy(res);
                    console.log('dataResCopy', dataResCopy);
                    var result = [];
                    var xres = res.data.Result;
                    var vNoShow = 0;
                    var vBooking = 0;
                    // var tmpSuggestStall =[];
                    vdataTemp = [];
                    vdataTemp2 = [];
                    vdataTemp3 = [];
                    for (var i = 0; i < xres.length; i++) {
                        var vitem = xres[i];
                        console.log('vitem >>', vitem);
                        var vstatus = 'no_show';
                        var vstatus = me.checkAppointmentStatus(vitem.PlanStart);
                        // var vicon = 'customer';
                        // if (vstatus=='appointment'){
                        //     vicon = 'customer_gray';
                        // }

                        if ((vitem.TimeDuration != vitem.ActualChipTimeDuration) && (vitem.ActualChipTimeDuration <= vitem.TimeDuration) && (vitem.Status == 6)){
                            if (vitem.ActualChipTimeDuration > 0){
                                vitem.TimeDuration = vitem.ActualChipTimeDuration
                            }
                        }

                        var vtime = vitem.PlanStart;
                        var vdate = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd');
                        var vEndDate = new Date(vitem.PlanDateStart);
                        var vTimeStart = JsBoard.Common.getMinute(vitem.PlanStart);
                        var vMinutes = (vitem.TimeDuration + vitem.Extension) * 60;
                        var vMinStart = JsBoard.Common.getMinute(me.startHour);
                        vEndDate.setMinutes(vEndDate.getMinutes() + vTimeStart + vMinutes);
                        // var vcolumn = me.timeToColumn(vboardName, vdate, vtime);
                        if ((vjobid == vitem.JobId) && vitem.PlanStart == '00:00:00') {
                            var vcolumn = 1;
                        } else {
                            // console.log('trap 1')
                            var vcolumn = me.timeToColumn(vboardName, vdate, vtime);
                        }

                        if (vitem.PoliceNumber) {
                            vitem.nopol = vitem.PoliceNumber;
                        } else {
                            vitem.nopol = '#' + vitem.JobId;
                        }
                        vstatus = me.checkStatus(vitem);
                        console.log("iiiiiii", i);
                        console.log("vitem i", vitem);
                        console.log("vboardName vdate vtime i", vboardName, vdate, vtime);

                        var vDontDrag = 0;
                        if (typeof vitem.PauseDraggable != 'undefined') {
                            if ((vitem.Status == 6) && (vitem.PauseDraggable == 0)) {
                                vDontDrag = 1;
                            }
                        }

                        if (typeof vitem.PauseDraggable != 'undefined') {
                            if ((vitem.Status == 5) && (vitem.PauseDraggable == 0)) {
                                vDontDrag = 1;
                            }
                        }

                        if(vitem.isDispatched == 1 && vitem.Status != 2){
                            vDontDrag = 1;

                        }

                        if(vitem.isDispatched == 1 && vitem.Status == 2){
                            vDontDrag = 1;

                        }

                        var vdata = {
                            chip: 'jpcb2',
                            ChipGroupId: vitem.ChipGroupId,
                            jobId: vitem.JobId,
                            jobtaskbpid: vitem.JobTaskBpId,
                            AppointmentTime: vitem.PlanStart,
                            jamcetakwo: me.reformatTime(vitem.PlanStart),
                            jamjanjiserah: $filter('date')(vitem.JanjiSerah, 'hh:mm'),
                            tgljanjiserah: $filter('date')(vitem.JanjiSerah, 'dd/MM/yyyy'),
                            nopol: vitem.nopol,
                            type: vitem.ModelType,
                            normalDuration: '01:00',
                            normal: vitem.TimeDuration * 60 / me.incrementMinute,
                            ext: vitem.Extension * 60 / me.incrementMinute,
                            endDate: vEndDate,
                            stallId: vitem.StallId,
                            row: 0,
                            col: vcolumn,
                            width: vitem.TimeDuration * 60 / me.incrementMinute,
                            color_idx: me.getIndexById(vitem.JobId),
                            daydiff: 0,
                            status: vstatus,
                            intStatus: vitem.Status,
                            dontDrag: vDontDrag,
                            teknisi: vitem.Teknisi,
                            pekerjaan: vitem.JenisPekerjaan,
                            jmltask: vitem.jmltask,
                            allocated: ((vitem.jmltask >= 1) ? 1 : 0),
                            chipType: vitem.ChipType,
                            workname: vitem.ChipType + ': ' + workNames[vitem.ChipType],
                            additionalJob: vitem.additionalJob,
                            tmpStall: me.checkStatuStall(vitem),
                            wocategoryname: vitem.WoCategoryName,
                            isCarryOver: vitem.isCarryOver,
                            isDispatched: vitem.isDispatched,
                            isOldChip: vitem.IsOldChip,
                            // tmpSuggestStall:me.checkStatuStall(),
                            statusicon: [],
                            PlanDateTimeStart: vitem.PlanDateTimeStart,
                            PlanDateTimeFinish: vitem.PlanDateTimeFinish,
                        };
                        console.log('vdata >>', vdata);
                        var isPush = true;
                        if (vitem.Status == 1) {
                            // isPush = false;
                            vNoShow = vNoShow + 1;
                        }
                        if (vitem.Status == 2) {
                            isPush = true;
                            // vdata.dontDrag = 0;
                        }
                        if ((vitem.isAppointment) && (vitem.Status >= 3)) {
                            vBooking = vBooking + 1;
                        }
                        if ((vitem.isActive) && (vitem.Status == 8)) {
                            vdata.waiting_fi = 1;
                        } else {
                            vdata.waiting_fi = 0;
                        }
                        me.updateStatusIcons(vdata);
                        // vdata.width = vdata.normal+vdata.ext;
                        // vdata.width = 1;

                        // if (vdata.width <= 0) { //AMK, karena mw ngilangin chip yg gk ada waktu pekerjaannya
                        //     vdata.width = 1;
                        // }

                        if (!vitem.isAllocated) {
                            vdata.row = -2;
                            vdata.stallId = -2;
                        } else {
                            console.log('kapan kesini nya?')
                            if (vdata.stallId == 0) {
                                console.log('kapan kesini nya1')
                                    //pake yg -1 jd ke jobstoppage
                                vdata.row = -1;
                                vdata.stallId = -1;
                                vdata.width = 1;
                                vdata.tgljanjiserah = ''
                                vdata.ext = 0;

                            } else if (vdata.stallId == -2) {
                                console.log('kapan kesini nya2')
                                vdata.row = -2;
                                vdata.stallId = -2;
                            }
                        }
                        if ((vdata.stallId !== 0) && isPush) {
                            if (vdata.width > 0) {
                                console.log("vdata=======>", vdata)
                                    // ============ TERUSAN ======
                                me.updateChipBreak(vdata);
                                result.push(vdata);
                                vdataTemp.push(vdata);
                                vdataTemp2.push(vdata);
                                vdataTemp3.push(vdata);
                                console.log('vdatatemp push', vdataTemp);
                                console.log('vdatatemp2 push', vdataTemp2);
                                console.log('vdatatemp3 push', vdataTemp3);

                                console.log('res ini apa', result);
                            }

                        }
                        // console.log("vdata", vdata);
                        // break;
                    }


                    //=============cobain cek menumpuk extend ==========
                    console.log("tmpGetStall extend", tmpGetStall);
                    var vdataTemp3Copy = angular.copy(vdataTemp3);
                    var menumpukExt = 0;
                    console.log('vdatatemp3 ext', vdataTemp3);
                    console.log('vdatatemp3Copy ext', vdataTemp3Copy);

                    var stallIdMerah = []

                    for (var i = 0; i < vdataTemp3.length; i++) {

                        for (var j = 0; j < vdataTemp3Copy.length; j++) {
                            if ((vdataTemp3[i].nopol != vdataTemp3Copy[j].nopol) &&
                                (vdataTemp3[i].chipType == vdataTemp3Copy[j].chipType) &&
                                (vdataTemp3[i].ChipGroupId == vdataTemp3Copy[j].ChipGroupId) &&
                                ((vdataTemp3[i].stallId > 0) && (vdataTemp3Copy[j].stallId > 0) && (vdataTemp3[i].stallId == vdataTemp3Copy[j].stallId)) &&
                                (vdataTemp3[i].ChipGroupId != null) && (vdataTemp3Copy[j].stallId != null)) {

                                var startActiveChip = vdataTemp3[i].col;
                                var endActiveChip = vdataTemp3[i].col + vdataTemp3[i].width;

                                var startPlottedChip = vdataTemp3Copy[j].col;
                                var endPlottedChip = vdataTemp3Copy[j].col + vdataTemp3Copy[j].width;

                                // console.log('startActiveChip', startActiveChip)
                                // console.log('endActiveChip', endActiveChip)
                                // console.log('startPlottedChip', startPlottedChip)
                                // console.log('endPlottedChip', endPlottedChip)
                                // console.log('masuk if tumpiuk', vdataTemp3[i]);



                                //cek apakah plotted chip ada di tengah active chip
                                if ((startActiveChip <= startPlottedChip) && (endActiveChip > startPlottedChip)) {
                                    // console.log('ini chip yg numpuk1', vdataTemp3[i]);
                                    // console.log('ini chip yg numpuk1', vdataTemp3Copy[j]);
                                    stallIdMerah.push(vdataTemp3[i].stallId);
                                    menumpukExt = 1;
                                }

                                // cek apakah active chip ada di tengah plotted chip
                                if ((startPlottedChip <= startActiveChip) && (endPlottedChip > startActiveChip)) {
                                    // console.log('ini chip yg numpuk2', vdataTemp3[i]);
                                    // console.log('ini chip yg numpuk2', vdataTemp3Copy[j]);
                                    stallIdMerah.push(vdataTemp3[i].stallId);
                                    menumpukExt = 1;
                                }
                            }
                        }
                        console.log('menumpukExt', menumpukExt);
                        console.log('stallIdMerah', stallIdMerah);



                    }
                    if (stallIdMerah.length > 0) {
                        for (var i = 0; i < tmpGetStall.length; i++) {
                            for (var j = 0; j < stallIdMerah.length; j++) {
                                if (tmpGetStall[i].stallId == stallIdMerah[j]) {
                                    tmpGetStall[i].durationBreak4 = 750 * jmlHariMerah;
                                }
                            }
                        }
                    } else {
                        for (var i = 0; i < tmpGetStall.length; i++) {
                            tmpGetStall[i].durationBreak4 = 0 * jmlHariMerah;
                        }
                    }
                    //=============cobain cek menumpuk extend ==========



                    // setTimeout(function() {
                        result.reverse();
                        console.log('result seuda reverse', result)
                        arg.countNoShow = vNoShow;
                        arg.countBooking = vBooking;

                        console.log("cek data resutl", result);
                           var tmpArray = result.sort(function(a,b){
                                return a.col - b.col;
                            })
                        console.log("cek data nih", tmpArray);
                        result = tmpArray;
                        onFinish(result);
                    // }, 2000);
                    
                })


            },

            onGetChipsYY: function(arg, onFinish) {
                // result = [];
                // result.push({
                //     chip : "jpcb2",
                //     row : -2,
                //     col : 1,
                //     width : 1,
                //     normal : 1,
                //     ext : 0,
                //     nopol : "B6101AD",
                //     tipe : "AVANZA",
                //     tgljanjiserah : "14/06/2017",
                //     jamjanjiserah : "08:00",
                //     wotype : "Medium",
                //     color_idx : 0,
                //     MasterId : 64,
                //     index : 1,
                //     stallId: -2,
                //     selected : false,
                //     workname: 'TEST',
                // });


                var vArg = { nopol: 'B 1000 AXA', tipe: 'Avanza' }
                vArg = JsBoard.Common.extend(vArg, arg);

                var tglAwal = arg.currentDate;
                var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
                tglAkhir.setDate(tglAkhir.getDate());
                // var vurl = arg.chipUrl;
                var vurl = '/api/as/Boards/jpcb/bydate/' + $filter('date')(tglAwal, 'yyyy-MM-dd') + '/0/17';
                var me = this;
                $http.get(vurl).then(function(res) {
                    // onFinish([]);
                    // return;
                    // var result = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1,
                    //     daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}];
                    var result = [];

                    onFinish(result);

                });

            },
            getCurrentTime: function() {
                return jpcbFactory.getCurrentTime();
                // var vDate = new Date();
                // //vDate.setHours(vDate.getHours()-this.hourDelay);
                // // vDate.setMinutes(vDate.getMinutes()-this.hourDelay*60);
                // vDate.setMinutes(vDate.getMinutes()-this.minuteDelay);
                // return vDate;
            },
            getScrollablePos: function(boardname) {
                return jpcbFactory.getScrollablePos(boardname);
            },
            updateChipStatus: function(boardname) {
                // var boardname = this.boardName;
                var me = this;
                var vboard = jpcbFactory.getBoard(boardname);
                jpcbFactory.processChips(boardname, function(vchip) {
                    return true;
                }, function(vchip) {
                    var vitem = vchip.data;
                    var vstatus = me.checkAppointmentStatus(vitem.AppointmentTime);
                    if (vitem.status !== vstatus) {
                        vitem.status = vstatus;
                        vboard.updateChip(vchip);
                    }
                    if (vitem.status == 'no_show') {
                        vchip.draggable(true);
                    }
                });

            },
            updateTimeLine: function(boardname) {
                var vDate = this.getCurrentTime();
                var tempDate = new Date();
                var tempEndDate = angular.copy(this.longEnd);
                var tempEndDate2 = tempEndDate.split(':')
                var maxVDate = new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate(), tempEndDate2[0], tempEndDate2[1], 0);
                if (vDate > maxVDate){
                    vDate = new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate(), parseInt(tempEndDate2[0])+1, tempEndDate2[1], 0);
                }
                var xDate = $filter('date')(vDate, 'yyyy-MM-dd');
                var xTime = $filter('date')(vDate, 'HH:mm:ss');
                console.log('trap 1', xTime);
                console.log('trap 111', this.longEnd)
                var vline = this.timeToColumn(boardname, xDate, xTime);
                var vboard = jpcbFactory.getBoard(boardname);
                vboard.layout.updateTimeLine(vboard.board, vline);
                jpcbFactory.updateStandardDraggable(boardname);

                jpcbFactory.redrawBoard(boardname);
            },
            xupdateTimeLine: function(boardname) {
                // var boardname = this.boardName;
                var vDate = this.getCurrentTime();
                // console.log("current Time#####", vDate.getTime());
                var currentHour = $filter('date')(vDate, 'HH:mm');

                // console.log("currentHour", currentHour);
                //var startHour = this.
                //var minutPerCol

                jpcbFactory.updateTimeLine(boardname, currentHour, this.startHour, this.incrementMinute);

                //this.updateChipStatus();

                jpcbFactory.updateStandardDraggable(boardname);

                jpcbFactory.redrawBoard(boardname);
            },
            getBoard: function(boardname) {
                return jpcbFactory.getBoard(boardname);
            },

            getHeader: function(vtype, vparam1, vparam2) {

                //return [{text: '1'}, {text: '2'}, {text: '3'}, {text: '4'}]
                // type: 'hourly'; param1: start date, param2: date count
                // type: 'daily'; param1: start date, param2: date count
                // type: 'weekly'; param1: start date, param2: date count
                if (vtype.toLowerCase() == 'tps') {
                    if (typeof vparam1 == 'undefined') {
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined') {
                        vparam2 = this.jmlHariTps;
                    }
                    var vres = this.getHourlyList(vparam1, vparam2);
                    return vres;
                } else if (vtype.toLowerCase() == 'light') {
                    if (typeof vparam1 == 'undefined') {
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined') {
                        vparam2 = this.jmlHariLight;
                    }
                    // var vres = this.getDailyList(vparam1, vparam2); ini getdailylist kalau bisa tau darimana, kayanya bisa bikin per hari bukan per jam board nya
                    var vres = this.getHourlyList(vparam1, vparam2);
                    console.log("daily list", vres);
                    return vres;
                } else if (vtype.toLowerCase() == 'medium') {
                    if (typeof vparam1 == 'undefined') {
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined') {
                        vparam2 = this.jmlHariMedium;
                    }
                    // var vres = this.getDailyList(vparam1, vparam2);
                    var vres = this.getHourlyList(vparam1, vparam2);
                    console.log("daily list", vres);
                    return vres;
                } else if (vtype.toLowerCase() == 'heavy') {
                    if (typeof vparam1 == 'undefined') {
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined') {
                        vparam2 = this.jmlHariHeavy;
                    }
                    // var vres = this.getDailyList(vparam1, vparam2);
                    var vres = this.getHourlyList(vparam1, vparam2);
                    console.log("daily list", vres);
                    return vres;
                } else {
                    // kata nya kl ga ada di list samain aja sama tps
                    if (typeof vparam1 == 'undefined') {
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined') {
                        vparam2 = this.jmlHariTps;
                    }
                    var vres = this.getHourlyList(vparam1, vparam2);
                    return vres;
                }
            },
            getWorkHour: function() {
                return JsBoard.Common.generateDayHours({
                    // startHour: '08:00',
                    // endHour: '17:00',
                    // endHour: '22:00',
                    // startHour: '08:00',
                    // endHour: '17:00',
                    startHour: this.longStart,
                    endHour: this.longEnd,
                    increment: 60,
                })
            },
            getHourlyList: function(startDate, vcount) {
                var breakHour = '12:00';
                var result = []
                var vidx = 0;
                var vWorkHour = this.getWorkHour();
                var item = false
                var vDate = startDate;
                var currentDate = '******';
                for (var i = 0; i < vcount; i++) { //sini looping header hari
                    var newDay = true;
                    var subCount = vWorkHour.length;
                    currentDate = $filter('date')(vDate, 'dd MMMM yyyy');
                    for (var idx in vWorkHour) {
                        var xDate = new Date($filter('date')(vDate, 'yyyy-MM-dd') + ' ' + vWorkHour[idx]);
                        if (newDay) {
                            item = { newDay: newDay, parentText: currentDate, subCount: subCount, text: vWorkHour[idx], dateTime: xDate }
                        } else {
                            item = { newDay: newDay, text: vWorkHour[idx], dateTime: xDate }
                        }
                        if (vWorkHour[idx] == breakHour) {
                            item.colInfo = { isBreak: true }
                        }
                        result.push(item);
                        var newDay = false;
                    }
                    console.log('getdate dari vdate', vDate.getDate());
                    vDate.setDate(vDate.getDate() + 1);
                }
                return result;
            },
            // onGetChips: function(arg){
            //     var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
            //     vArg = JsBoard.Common.extend(vArg, arg);
            //     var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     ]
            //     var today = new Date();
            //     if (arg.currentDate){
            //         var vdiff = this.dayDiff(today, arg.currentDate);
            //         var vres = []
            //         for (var i=0; i<vitems.length; i++){
            //             if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
            //                 vres.push(vitems[i]);
            //             }
            //         }
            //         return vres;
            //     }
            //     return []
            // },
            dayDiff: function(date1, date2) {
                return Math.round((date2 - date1) / (1000 * 60 * 60 * 24));
            },
            // ===================== TERUSAN ======
            timeToColumn2: function(vtime) {
                // var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vstartmin = JsBoard.Common.getMinute(this.longStart);
                var vmin = JsBoard.Common.getMinute(vtime) - vstartmin;
                var vresult = vmin / this.incrementMinute;

                return vresult;
            },
            getBreakTime: function() {
                var vstartMin = JsBoard.Common.getMinute(this.breakHour);
                var vendMin = vstartMin + 60;
                var vBreakEnd = JsBoard.Common.getTimeString(vendMin);
                var vstart = this.timeToColumn2(this.breakHour);
                var vend = this.timeToColumn2(vBreakEnd);

                var arrayBreak = [];
                // jeda antar istirahat = (break hour - start hour) + (end hour - break hour) + 1
                var JedaAntarBreak = 0;
                var LStart = this.longStart.split(':');
                var LEnd = this.longEnd.split(':');
                var XLStart = parseInt(LStart[0]);
                var XLEnd = parseInt(LEnd[0]);
                console.log('asem kuy',XLStart, ' ', XLEnd);
                for (var i = 0; i < jmlHariMerah; i++) {
                    var breakXX = {};
                    breakXX = {
                        start: vstart + (JedaAntarBreak * i),
                        end: vend + (JedaAntarBreak * i),
                        width: vend - vstart,
                    }
                    arrayBreak.push(breakXX);
                    // JedaAntarBreak = 10; // (12:00 - 08:00) + (17:00 - 12:00) + 1
                    JedaAntarBreak = (12-XLStart) + (XLEnd - 12) + 1;

                }
                console.log('daftar break bos', arrayBreak)
                return arrayBreak;
                // return [{
                //     start: vstart,
                //     end: vend,
                //     width: vend - vstart,
                // }]
            },
            getBreakChipTime: function(vstart, vlength) {
                console.log("getBreakChipTime", vstart, vlength);
                var vbreaks = this.getBreakTime();
                console.log("vbreaks", vbreaks);
                var vbreakCount = 0;
                var vtotalBreak = 0;
                var vresult = {}
                for (var i = 0; i < vbreaks.length; i++) {
                    vbreak = vbreaks[i];
                    //(x1, x2, y1, y2)
                    // (x1 <= y2) && (y1 < x2)
                    if (this.isOverlapped2(vstart, vstart + vlength, vbreak.start, vbreak.end)) {
                        console.log("===============>", vstart, vbreak.start);
                        if (vstart > vbreak.start) {
                            return 'start-inside-break';
                        }
                        vbreakCount = vbreakCount + 1;


                        if (vbreakCount == 1){
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vstart;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break1On'] = 1;
                        } else if (vbreakCount == 2) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break2On'] = 1;
                        } else if (vbreakCount == 3) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break3On'] = 1;
                        } else if (vbreakCount == 4) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break4On'] = 1;
                        } else if (vbreakCount == 5) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break5On'] = 1;
                        } else if (vbreakCount == 6) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break6On'] = 1;
                        } else if (vbreakCount == 7) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break7On'] = 1;
                        } else if (vbreakCount == 8) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break8On'] = 1;
                        } else if (vbreakCount == 9) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break9On'] = 1;
                        } else if (vbreakCount == 10) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break10On'] = 1;
                        } else if (vbreakCount == 11) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break11On'] = 1;
                        } else if (vbreakCount == 12) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break12On'] = 1;
                        } else if (vbreakCount == 13) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break13On'] = 1;
                        } else if (vbreakCount == 14) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break14On'] = 1;
                        } else if (vbreakCount == 15) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength +1;
                            vresult['break15On'] = 1;
                        }

                        vtotalBreak = vtotalBreak + vbreak.width;



                        // vresult['break' + vbreakCount + 'start'] = vbreak.start - vstart;
                        // vresult['break' + vbreakCount + 'width'] = vbreak.width;
                        // vtotalBreak = vtotalBreak + vbreak.width;
                        // vlength = vlength +1;
                        
                        
                    }
                }
                console.log('vtotalbreak', vtotalBreak)
                if (vbreakCount == 0) {
                    return 'no-break';
                } else {
                    vresult['breakWidth'] = vtotalBreak;
                }
                console.log('vresult coge', vresult);
                return vresult;

            },
            updateChipBreak: function(vchipData) {
                console.log('vchipdata anj', vchipData);
                if (vchipData.breakWidth) {
                    vchipData.width = vchipData.width - vchipData.breakWidth;
                }
                var vbreaks = 'no-break';
                // if (vchipData.stallId != 0) {
                if (vchipData.stallId > 0) {
                    vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width);
                }

                var deleteList = ['break1start', 'break1width', 'break2start', 'break2width', 'break3start', 'break3width', 'break4start', 'break4width', 'breakWidth'];
                for (var i = 0; i < deleteList.length; i++) {
                    var idx = deleteList[i];
                    delete vchipData[idx];
                }
                if (typeof vbreaks == 'object') {
                    for (var idx in vbreaks) {
                        vchipData[idx] = vbreaks[idx];
                    }
                }

                if (vchipData.breakWidth) {
                    // console.log('breakwidth per chip', vchipData.breakWidth, ' - ', vchipData.chipType)
                    //==================== start codingan benar untuk 1 chip transparan di break pertama saja ===============
                    // vchipData.width = vchipData.width + vchipData.breakWidth;
                    // vchipData.width1 = vchipData.break1start;
                    // vchipData.start2 = vchipData.break1start + vchipData.break1width;
                    // vchipData.width2 = vchipData.width - vchipData.start2;
                    //==================== end codingan benar untuk 1 chip transparan di break pertama saja ==================




                    // ============== start codingan buat multiple transparan ================================================
                    if (vbreaks.break15On == 1){
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                        vchipData.width8 = vchipData.break8start;
                        vchipData.start9 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width;
                        vchipData.width9 = vchipData.break9start;
                        vchipData.start10 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width;
                        vchipData.width10 = vchipData.break10start
                        vchipData.start11 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width;
                        vchipData.width11 = vchipData.break11start
                        vchipData.start12 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width;
                        vchipData.width12 = vchipData.break12start;
                        vchipData.start13 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width;
                        vchipData.width13 = vchipData.break13start;
                        vchipData.start14 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width + vchipData.break13start + vchipData.break13width;
                        vchipData.width14 = vchipData.break14start;
                        vchipData.start15 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width + vchipData.break13start + vchipData.break13width + vchipData.break14start + vchipData.break14width;

                        vchipData.width15 = vchipData.break15start;
                        vchipData.start16 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width + vchipData.break13start + vchipData.break13width + vchipData.break14start + vchipData.break14width + vchipData.break15start + vchipData.break15width;
                        vchipData.width16 = vchipData.width - vchipData.start16;

                    } else if (vbreaks.break14On == 1){
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                        vchipData.width8 = vchipData.break8start;
                        vchipData.start9 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width;
                        vchipData.width9 = vchipData.break9start;
                        vchipData.start10 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width;
                        vchipData.width10 = vchipData.break10start
                        vchipData.start11 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width;
                        vchipData.width11 = vchipData.break11start
                        vchipData.start12 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width;
                        vchipData.width12 = vchipData.break12start;
                        vchipData.start13 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width;
                        vchipData.width13 = vchipData.break13start;
                        vchipData.start14 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width + vchipData.break13start + vchipData.break13width;
                        
                        vchipData.width14 = vchipData.break14start;
                        vchipData.start15 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width + vchipData.break13start + vchipData.break13width + vchipData.break14start + vchipData.break14width;
                        vchipData.width15 = vchipData.width - vchipData.start15;

                    } else if (vbreaks.break13On == 1){
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                        vchipData.width8 = vchipData.break8start;
                        vchipData.start9 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width;
                        vchipData.width9 = vchipData.break9start;
                        vchipData.start10 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width;
                        vchipData.width10 = vchipData.break10start
                        vchipData.start11 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width;
                        vchipData.width11 = vchipData.break11start
                        vchipData.start12 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width;
                        vchipData.width12 = vchipData.break12start;
                        vchipData.start13 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width;
                        
                        vchipData.width13 = vchipData.break13start;
                        vchipData.start14 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width + vchipData.break13start + vchipData.break13width;
                        vchipData.width14 = vchipData.width - vchipData.start14;

                    } else if (vbreaks.break12On == 1){
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                        vchipData.width8 = vchipData.break8start;
                        vchipData.start9 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width;
                        vchipData.width9 = vchipData.break9start;
                        vchipData.start10 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width;
                        vchipData.width10 = vchipData.break10start
                        vchipData.start11 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width;
                        vchipData.width11 = vchipData.break11start
                        vchipData.start12 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width;

                        vchipData.width12 = vchipData.break12start;
                        vchipData.start13 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width + vchipData.break12start + vchipData.break12width;
                        vchipData.width13 = vchipData.width - vchipData.start13;

                    } else if (vbreaks.break11On == 1){
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                        vchipData.width8 = vchipData.break8start;
                        vchipData.start9 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width;
                        vchipData.width9 = vchipData.break9start;
                        vchipData.start10 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width;
                        vchipData.width10 = vchipData.break10start
                        vchipData.start11 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width;

                        vchipData.width11 = vchipData.break11start;
                        vchipData.start12 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width + vchipData.break11start + vchipData.break11width;
                        vchipData.width12 = vchipData.width - vchipData.start12;

                    } else if (vbreaks.break10On == 1){
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                        vchipData.width8 = vchipData.break8start;
                        vchipData.start9 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width;
                        vchipData.width9 = vchipData.break9start;
                        vchipData.start10 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width;

                        vchipData.width10 = vchipData.break10start;
                        vchipData.start11 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width + vchipData.break10start + vchipData.break10width;
                        vchipData.width11 = vchipData.width - vchipData.start11;

                    } else if (vbreaks.break9On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                        vchipData.width8 = vchipData.break8start;
                        vchipData.start9 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width;
                       
                        vchipData.width9 = vchipData.break9start;
                        vchipData.start10 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width + vchipData.break9start + vchipData.break9width;
                        vchipData.width10 = vchipData.width - vchipData.start10;
                        
                    } else if (vbreaks.break8On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                       
                        vchipData.width8 = vchipData.break8start;
                        vchipData.start9 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width + vchipData.break8start + vchipData.break8width;
                        vchipData.width9 = vchipData.width - vchipData.start9;
                        
                    } else if (vbreaks.break7On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        
                        vchipData.width7 = vchipData.break7start;
                        vchipData.start8 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width + vchipData.break7start + vchipData.break7width;
                        vchipData.width8 = vchipData.width - vchipData.start8;
                        
                    } else if (vbreaks.break6On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                     
                        vchipData.width6 = vchipData.break6start;
                        vchipData.start7 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width + vchipData.break6start + vchipData.break6width;
                        vchipData.width7 = vchipData.width - vchipData.start7;

                    } else if (vbreaks.break5On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        
                        vchipData.width5 = vchipData.break5start;
                        vchipData.start6 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width + vchipData.break5start + vchipData.break5width;
                        vchipData.width6 = vchipData.width - vchipData.start6;

                    } else if (vbreaks.break4On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        
                        vchipData.width4 = vchipData.break4start;
                        vchipData.start5 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width + vchipData.break4start + vchipData.break4width;
                        vchipData.width5 = vchipData.width - vchipData.start5;

                    } else if (vbreaks.break3On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                       
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.width - vchipData.start4;

                    } else if (vbreaks.break2On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;

                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.width - vchipData.start3;

                    } else if (vbreaks.break1On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth;
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.width - vchipData.start2;
                    }
                    // ============== start codingan buat multiple transparan ================================================



                } else {
                    vchipData.width1 = vchipData.width;
                }
                // console.log('vchipData', vdata);

                // console.log('vbreaks', vbreaks);
                //////////
            },
            isOverlapped: function(x1, x2, y1, y2) {
                return (x1 < y2) && (y1 < x2)
            },
            isOverlapped2: function(x1, x2, y1, y2) {
                return (x1 <= y2) && (y1 < x2)
            },
            isOverlapped3: function(x1, x2, y1, y2) {
                return (x1 < y2) && (y1 <= x2)
            },

            getTypeHeader: function(TypeHeaderParam){
                if (TypeHeaderParam == null || TypeHeaderParam == '' || TypeHeaderParam == undefined){
                    TypeHeader = 'tps';
                } else {
                    TypeHeader = TypeHeaderParam.toLowerCase();
                }
            },


            getListStall: function(param){
                var res = $http.get('/api/as/Boards/BP/Stall/'+param);
                return res
            },

            cekRawatJalanGetout : function (JobId){
                return $http.put('/api/as/Boards/BP/CheckIsRawatJalanGateOut/' + JobId);
            },
            CekIsChipOnPlot : function (JobId, JobTaskBpId){
                // api/as/Boards/BP/CekIsChipOnPlot
                // HTTPUT
                // [{JobId : 0, JobTaskBpId : 0}]
                return $http.put('/api/as/Boards/BP/CekIsChipOnPlot/',[{JobId : JobId, JobTaskBpId : JobTaskBpId}]);
            },

            getDataChip : function(){
                return vdataTemp
            },

            CheckGroupBP: function(JobId){
                var res = $http.get('/api/as/Boards/BP/CheckGroupBP/'+JobId);
                return res
            },
            CheckStartAllocation: function(JobId){
                // api/as/Boards/BP/CheckStartAllocation/{JobId}
                var res = $http.put('/api/as/Boards/BP/CheckStartAllocation/'+JobId);
                return res
            },
            getTodayAbsensi: function(JobId){
                // get semua teknisi seperti tampilan awal menu absensi teknisi, 1032 role ptm bp
                var today_x = new Date()
                var today_xx = $filter('date')(today_x, 'yyyy-MM-dd');
                var res = $http.get('/api/as/GetAttendanceList/' + today_xx + '/1032');
                return res
            },
            


        }
    });