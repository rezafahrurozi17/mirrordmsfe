angular.module('app')
  .factory('JpcbBpDataFactory', function($http, CurrentUser, $filter) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getData: function(vfilter) {
        var res = [
          {chip: 'jpcb', row: 0, col: 0, timestatus: 1, width: 1, normal: 1, ext: 0, 
            nopol: 'B 7654 FDA', tglcetakwo: '12/01/17',
            start: '08:00', normalDuration: '00:30', extDuration: '00:15',
            jamcetakwo: '08:00', jamjanjiserah: '09:00',
            type: 'AVANZA', 
            pekerjaan: 'EM', teknisi: 'WAN',
            status: 'done',
            wotype: 'TPS', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'
          },
          {chip: 'jpcb', row: 0, col: 1, timestatus: 1, width: 1, normal: 1, ext: 0, 
            nopol: 'B 5527 FDA', tglcetakwo: '12/01/17',
            start: '08:45', normalDuration: '01:00', extDuration: '00:15',
            jamcetakwo: '08:15', jamjanjiserah: '09:30',
            type: 'AVANZA', 
            pekerjaan: 'EM', teknisi: 'WAN',
            status: 'inprogress',
            wotype: 'TPS', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: ['multitask', 'inprogress'], category: 'EM',
            allocated: 1, additional: 1
          },
          {chip: 'jpcb', row: 0, col: 2, timestatus: 1, width: 1, normal: 1, ext: 0, 
            nopol: 'B 4324 FDA', tglcetakwo: '12/01/17',
            start: '10:30', normalDuration: '00:30',
            jamcetakwo: '09:00', jamjanjiserah: '10:30',
            type: 'AVANZA', 
            pekerjaan: 'EM', teknisi: 'WAN',
            status: 'sudahdatang',
            wotype: 'Light', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: ['customer'], category: 'EM',
            allocated: 1},
          {chip: 'jpcb', row: 0, col: 2, timestatus: 1, width: 1, normal: 1, ext: 0, 
            nopol: 'B 4312 FDA', tglcetakwo: '12/01/17',
            start: '10:30', normalDuration: '00:30',
            jamcetakwo: '09:00', jamjanjiserah: '10:30',
            type: 'AVANZA', 
            pekerjaan: 'EM', teknisi: 'WAN',
            status: 'sudahdatang',
            wotype: 'Medium', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: ['customer'], category: 'EM',
            allocated: 1},
          {chip: 'jpcb', row: 0, col: 2, timestatus: 1, width: 1, normal: 1, ext: 0, 
            nopol: 'B 4313 FDA', tglcetakwo: '12/01/17',
            start: '10:30', normalDuration: '00:30',
            jamcetakwo: '09:00', jamjanjiserah: '10:30',
            type: 'AVANZA', 
            pekerjaan: 'EM', teknisi: 'WAN',
            status: 'sudahdatang',
            wotype: 'Medium', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: ['customer'], category: 'EM',
            allocated: 1},
          {chip: 'jpcb', row: 0, col: 2, timestatus: 1, width: 1, normal: 1, ext: 0, 
            nopol: 'B 4314 FDA', tglcetakwo: '12/01/17',
            start: '10:30', normalDuration: '00:30',
            jamcetakwo: '09:00', jamjanjiserah: '10:30',
            type: 'AVANZA', 
            pekerjaan: 'EM', teknisi: 'WAN',
            status: 'sudahdatang',
            wotype: 'Light', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: ['customer'], category: 'EM',
            allocated: 1},
          {chip: 'jpcb', row: 0, col: 2, timestatus: 1, width: 1, normal: 1, ext: 0, 
            nopol: 'B 4315 FDA', tglcetakwo: '12/01/17',
            start: '10:30', normalDuration: '00:30',
            jamcetakwo: '09:00', jamjanjiserah: '10:30',
            type: 'AVANZA', 
            pekerjaan: 'Heavy', teknisi: 'WAN',
            status: 'sudahdatang',
            wotype: 'Light', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: ['customer'], category: 'EM',
            allocated: 1},
          {chip: 'jpcb', row: 0, col: 2, timestatus: 1, width: 1, normal: 1, ext: 0, 
            nopol: 'B 4316 FDA', tglcetakwo: '12/01/17',
            start: '10:30', normalDuration: '00:30',
            jamcetakwo: '09:00', jamjanjiserah: '10:30',
            type: 'AVANZA', 
            pekerjaan: 'EM', teknisi: 'WAN',
            status: 'sudahdatang',
            wotype: 'Heavy', sa_name: 'AND', foreman: 'MAN', timewait: '00:20', statusicon: ['customer'], category: 'EM',
            allocated: 1},
        ]
        for(var i=0; i<res.length; i++){
          res[i].color_idx=i;
        }
        if(vfilter){
          var xres = []
          for(var i=0; i<res.length; i++){
            if (vfilter(res[i])){
              xres.push(res[i]);
            }
          }
          for(var i=0; i<xres.length; i++){
            xres[i].col=i;
          }
          return xres;
        }
          for(var i=0; i<res.length; i++){
            res[i].col=i;
          }
        return res;
      },
      chipStatistic: function(chipData, target){
        target.count_tps = 0;
        target.count_light = 0;
        target.count_medium = 0;
        target.count_heavy = 0;
        target.count_all = 0;
        for(var i=0; i<chipData.length; i++){
          if (chipData[i].wotype=='TPS'){
            target.count_tps = target.count_tps+1;
          } else if (chipData[i].wotype=='Light'){
            target.count_light = target.count_light+1;
          } else if (chipData[i].wotype=='Medium'){
            target.count_medium = target.count_medium+1;
          } else if (chipData[i].wotype=='Heavy'){
            target.count_heavy = target.count_heavy+1;
          }
          target.count_all = target.count_all+1;
        }
      },

      getGroup: function(vtype){
        // console.log(vtype);
        var res = [{name: 'ABC', type: 'TPS', count: 4},{name: 'AVC', type: 'TPS', count: 4},{name: 'XDC', type: 'TPS', count: 4},{name: 'BBC', type: 'TPS', count: 4},{name: 'LDC', type: 'TPS', count: 4},
          {name: 'SBC', type: 'TPS', count: 2},{name: 'PVC', type: 'TPS', count: 5},{name: 'BCD', type: 'Light', count: 3},{name: 'ABD', type: 'Light', count: 2},{name: 'LBD', type: 'Light', count: 5},
          {name: 'XBD', type: 'Light', count: 5}, {name: 'CBD', type: 'Light', count: 4},{name: 'MVD', type: 'Light', count: 3},{name: 'DXD', type: 'Light', count: 3},{name: 'MPD', type: 'Light', count: 5},
          {name: 'CDD', type: 'Light', count: 6},{name: 'FDD', type: 'Light', count: 4}, {name: 'LSD', type: 'Light', count: 5},{name: 'SMD', type: 'Light', count: 0},
          // {name: 'DED', type: 'Light', count: 7},{name: 'FED', type: 'Light', count: 3}, {name: 'XSD', type: 'Light', count: 5},{name: 'SBD', type: 'Light', count: 0},{name: 'ZXD', type: 'Light', count: 5},{name: 'LCD', type: 'Light', count: 5},
          {name: 'CDE', type: 'Medium', count: 6},{name: 'FDE', type: 'Medium', count: 4}, {name: 'LSE', type: 'Medium', count: 5},{name: 'SME', type: 'Medium', count: 0},
          {name: 'DEF', type: 'Medium', count: 7},{name: 'FEF', type: 'Medium', count: 3}, {name: 'XSF', type: 'Medium', count: 5},{name: 'SBF', type: 'Medium', count: 0},{name: 'ZXF', type: 'Medium', count: 5},{name: 'LCF', type: 'Medium', count: 5},
          {name: 'GHI', type: 'Heavy', count: 4},{name: 'MDI', type: 'Heavy', count: 6},{name: 'VHI', type: 'Heavy', count: 4},{name: 'HVI', type: 'Heavy', count: 1},{name: 'LBI', type: 'Heavy', count: 5},
        ]

        var xres = []
        var xtype = vtype.toLowerCase();
        // console.log("xtype", xtype);
        for (idx in res){
          item = res[idx];
          // console.log(item);
          if (item.type.toLowerCase()==xtype){
            xres.push(item);
          }
        }
        return xres;
      },
      getStall: function(team){
        if (team=='ABC'){
          return [
            {title: "BODY", technician: 'FIRM', status: 'normal'},
            {title: "PUTTY", technician: 'WAWN', status: 'normal'},
            {title: "SURFACER", technician: 'DONI', status: 'normal'},
            {title: "SPRAYING & POLISHING", technician: 'JOKO', status: 'normal'},
            {title: "RE-ASSEMBLY", technician: 'BUDI', status: 'normal'},
            {title: "FINAL INSPECTION", technician: 'MAMT', status: 'normal'},
          ]
        } else if (team=='BCD'){
          return [
            {title: "BODY 1", technician: 'FAJR', status: 'normal'},
            {title: "BODY 2", technician: 'AGUS', status: 'normal'},
            {title: "PUTTY 1", technician: 'WWNA', status: 'normal'},
            {title: "PUTTY 2", technician: 'SUMD', status: 'normal'},
            {title: "SURFACER 1", technician: 'LUBS', status: 'normal'},
            {title: "SURFACER 2", technician: 'MORD', status: 'normal'},
            {title: "SPRAYING", technician: 'MUND', status: 'normal'},
            {title: "POLISHING", technician: 'DARM', status: 'normal'},
            {title: "RE-ASSEMBLY", technician: 'DAMR', status: 'normal'},
            {title: "FINAL INSPECTION", technician: 'MAMD', status: 'normal'},
          ]
        } else {
          return [{title: "STALL A", technician: '-', status: 'normal'}, {title: "STALL B", technician: '-', status: 'normal'}, {title: "STALL C", technician: '-', status: 'normal'},
            {title: "STALL D", technician: '-', status: 'normal'}]
        }
      },
      getTeamJobs: function(team){
        if (team=='ABC'){
          return []
        } else {
          return []
        }
      },
      getHeader: function(vtype, vparam1, vparam2){

        //return [{text: '1'}, {text: '2'}, {text: '3'}, {text: '4'}]
        // type: 'hourly'; param1: start date, param2: date count
        // type: 'daily'; param1: start date, param2: date count
        // type: 'weekly'; param1: start date, param2: date count
        if (vtype.toLowerCase()=='tps'){
          if (typeof vparam1 == 'undefined'){
            vparam1 = new Date();
          }
          if (typeof vparam2 == 'undefined'){
            vparam2 = 3;
          }
          var vres = this.getHourlyList(vparam1, vparam2);
          return vres;
        } else if (vtype.toLowerCase()=='light'){
          if (typeof vparam1 == 'undefined'){
            vparam1 = new Date();
          }
          if (typeof vparam2 == 'undefined'){
            vparam2 = 7;
          }
          var vres = this.getDailyList(vparam1, vparam2);
          console.log("daily list", vres);
          return vres;
        } else if (vtype.toLowerCase()=='medium'){
          if (typeof vparam1 == 'undefined'){
            vparam1 = new Date();
          }
          if (typeof vparam2 == 'undefined'){
            vparam2 = 14;
          }
          var vres = this.getDailyList(vparam1, vparam2);
          console.log("daily list", vres);
          return vres;
        } else if (vtype.toLowerCase()=='heavy'){
          if (typeof vparam1 == 'undefined'){
            vparam1 = new Date();
          }
          if (typeof vparam2 == 'undefined'){
            vparam2 = 30;
          }
          var vres = this.getDailyList(vparam1, vparam2);
          console.log("daily list", vres);
          return vres;
        }
      }, 
      getWorkHour: function(){
        return JsBoard.Common.generateDayHours({
            startHour: '08:00',
            endHour: '17:00',
            increment: 60,
        })
      },
      getHourlyList: function(startDate, vcount){
        var breakHour = '12:00';
        var result = []
        var vidx = 0;
        var vWorkHour = this.getWorkHour();
        var item = false
        var vDate = startDate;
        var currentDate = '******';
        for (var i=0; i<vcount; i++){
          var newDay = true;
          var subCount = vWorkHour.length;
          currentDate = $filter('date')(vDate, 'dd MMMM yyyy');
          for(var idx in vWorkHour){
            if (newDay){
              item = {newDay: newDay, parentText: currentDate, subCount: subCount, text: vWorkHour[idx]}
            } else {
              item = {newDay: newDay, text: vWorkHour[idx]}
            }
            if (vWorkHour[idx]==breakHour){
              item.colInfo = {isBreak: true}
            }
            result.push(item);
            var newDay = false;
          }
          vDate.setDate(vDate.getDate()+1);
        }
        return result;
      },
      getDailyList: function(vstartDate, vcount){
        var result = []
        var newMonth = true;
        var subCount = 1;
        var vDate = vstartDate;
        var vparent = $filter('date')(vDate, 'MMMM');
        var lastItem = false;
        for (var i=0; i<vcount; i++){
          var vtext = $filter('date')(vDate, 'dd');
          if(newMonth){
            item = {newDay: newMonth, parentText: vparent, subCount: subCount, text: vtext}
            if (lastItem){
              lastItem.subCount = subCount;
            }
            lastItem = item;
            subCount = 1;
          } else {
            item = {newDay: newMonth, text: vtext}
            subCount = subCount+1;
          }
          vDate.setDate(vDate.getDate()+1);
          result.push(item);
          var xparent = $filter('date')(vDate, 'MMMM');
          if (xparent!==vparent){
            newMonth = true;
            vparent = xparent;
          } else {
            newMonth = false;
          }
        }
        if (lastItem){
          lastItem.subCount = subCount;
        }
        return result;

      },
      getWeeklyList: function(startDate, vcount){
        return [{text: '1'}, {text: '2'}, {text: '3'}, {text: '4'}]

      },
      allChipData: {
        'ABC': [
          {chip: 'jpcb', nopol: "B 7801 BDA", type: "AVANZA", tglcetakwo: '12/01/17', jamcetakwo: '09:00', daynow: 0, color_idx: 20,
            items: [{jobtype: 'body', col: 0, row: 0, width: 1}, {jobtype: 'putty', col: 1, row: 1, width: 1}, {jobtype: 'body', col: 2, row: 2, width: 1}, 
              {jobtype: 'body', col: 3, row: 3, width: 1}, {jobtype: 'body', col: 5, row: 4, width: 0.5}]},
          {chip: 'jpcb', nopol: "B 7802 BDB", type: "AVANZA", tglcetakwo: '12/01/17', jamcetakwo: '10:00', daynow: 0,  color_idx: 21,
            items: [{jobtype: 'body', col: 1, row: 0, width: 1}, {jobtype: 'putty', col: 2, row: 1, width: 1}, {jobtype: 'body', col: 3, row: 2, width: 1}, 
              {jobtype: 'body', col: 5, row: 3, width: 1}, {jobtype: 'body', col: 6, row: 4, width: 1}]},
          {chip: 'jpcb', nopol: "B 7803 BDC", type: "AVANZA", tglcetakwo: '12/01/17', jamcetakwo: '10:30', daynow: 0,  color_idx: 22,
            items: [{jobtype: 'body', col: 2, row: 0, width: 1}, {jobtype: 'putty', col: 3, row: 1, width: 1}, {jobtype: 'body', col: 5, row: 2, width: 1}, 
              {jobtype: 'body', col: 6, row: 3, width: 1}, {jobtype: 'body', col: 7, row: 4, width: 1}]},
          {chip: 'jpcb', nopol: "B 7804 BDD", type: "AVANZA", tglcetakwo: '12/01/17', jamcetakwo: '09:00', daynow: 1,  color_idx: 23,
            items: [{jobtype: 'body', col: 0, row: 0, width: 1}, {jobtype: 'putty', col: 1, row: 1, width: 1}, {jobtype: 'body', col: 2, row: 2, width: 1}, 
              {jobtype: 'body', col: 3, row: 3, width: 1}, {jobtype: 'body', col: 5, row: 4, width: 1}]},
          {chip: 'jpcb', nopol: "B 7805 BDE", type: "AVANZA", tglcetakwo: '12/01/17', jamcetakwo: '10:00', daynow: 1,  color_idx: 24,
            items: [{jobtype: 'body', col: 1, row: 0, width: 1}, {jobtype: 'putty', col: 2, row: 1, width: 1}, {jobtype: 'body', col: 3, row: 2, width: 1}, 
              {jobtype: 'body', col: 5, row: 3, width: 1}, {jobtype: 'body', col: 6, row: 4, width: 1}]},
          {chip: 'jpcb', nopol: "B 7806 BDF", type: "AVANZA", tglcetakwo: '12/01/17', jamcetakwo: '10:30', daynow: 1,  color_idx: 25,
            items: [{jobtype: 'body', col: 2, row: 0, width: 1}, {jobtype: 'putty', col: 3, row: 1, width: 1}, {jobtype: 'body', col: 5, row: 2, width: 1}, 
              {jobtype: 'body', col: 6, row: 3, width: 1}, {jobtype: 'body', col: 7, row: 4, width: 1}]},
        ],
        'AVC': [
          {chip: 'jpcb', nopol: "B 7801 BDA", type: "AVANZA", tglcetakwo: '12/01/17', jamcetakwo: '09:00', daynow: 0, color_idx: 20,
            items: [{jobtype: 'body', col: 0, row: 0, width: 1}, {jobtype: 'putty', col: 1, row: 1, width: 1}, {jobtype: 'body', col: 2, row: 2, width: 1}, 
              {jobtype: 'body', col: 3, row: 3, width: 1}, {jobtype: 'body', col: 5, row: 3, width: 1.5}]},
        ]
      },
      getChipData: function(team, startDate, endDate, vtemplate){
        var vtoday = new Date();
        vresult = [];
        if (typeof this.allChipData[team] !== 'undefined'){
          var xdata = this.allChipData[team];
          for (var i=0; i<xdata.length; i++){
            var xdate = new Date();
            xdate.setDate(xdate.getDate()+xdata[i].daynow);
            xdata[i].workdate = xdate;
          }
          for (var i=0; i<xdata.length; i++){
            if (xdata[i].workdate>=startDate && xdata[i].workdate<=endDate){
              for(var j=0; j<xdata[i].items.length; j++){
                var xitem = JsBoard.Common.extend(xdata[i], xdata[i].items[j]);
                delete xitem.items;
                //xitem = JsBoard.Common.extend(null, xdata[i].items[i]);
                vresult.push(xitem);
              }
              //vresult.push(xdata[i]);
            }
          }
          if(vtemplate){
            var worklist = this.getWorkList();
            console.log("worklist", worklist);
            var xcol = 0;
            for (var i=0; i<worklist.length; i++){
              var vwork = worklist[i];
              var vnchip = JsBoard.Common.extend(vtemplate, {row: -2, workname: vwork, col: xcol, chip: 'jpcb2', selected: false});
              xcol++;
              vresult.push(vnchip);
            }
          }
          console.log("vresult", vresult);
        }
        // var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        return vresult;
      },
      getRangeByType: function(vtype, vstart){
        var vresult = {start: vstart}
        vcount = 0;
        if (vtype.toLowerCase()=='tps'){
          vcount = 3;
        } else if (vtype.toLowerCase()=='light'){
          vcount = 7;
        } else if (vtype.toLowerCase()=='medium'){
          vcount = 30;
        } else if (vtype.toLowerCase()=='heavy'){
          vcount = 30;
        }
        var vdate = new Date();
        vdate.setDate(vdate.getDate()+vcount);
        vresult.end = vdate;
        return vresult;
      },

      getWorkList: function(){
        var vresult = ['BODY', 'PUTTY', 'SURFACER', 'SPRAY', 'POLISH', 'REASSEMBLY', 'FINAL INSP']
        return vresult;
      }

    }
  });
