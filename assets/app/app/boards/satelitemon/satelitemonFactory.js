angular.module('app')
  .factory('SateliteMonitoring', function($http, CurrentUser, $filter) {
    return {
      allData: {
        ready_pickup: [
        ],
        wait_delivery: [
        ],
        ready_delivery: [
        ],
        wait_reception: [
        ],
      },
      daysBetween: function( date1, date2 ) {
        //Get 1 day in milliseconds
        var one_day=1000*60*60*24;
      
        // Convert both dates to milliseconds
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();
      
        // Calculate the difference in milliseconds
        var difference_ms = date2_ms - date1_ms;
          
        // Convert back to days and return
        return Math.round(difference_ms/one_day); 
      },
      process: {
        ready_pickup: function(vdata){
          var vtoday = new Date();
          for(var i=0; i<vdata.length; i++){
            var vitem = vdata[i];

            // theDay.setDate(vtoday.getDate()-vitem.daydiff);
            // vitem.tgl_masuk = $filter('date')(theDay, 'dd/MM/yyyy')+' '+vitem.jam_masuk;

            var theDay = new Date(vitem.startdate);
            vitem.daydiff = vtoday.getDate()-theDay.getDate();
            vitem.tgl_masuk = $filter('date')(theDay, 'dd/MM/yyyy')+' '+vitem.jam_masuk;

            if (vitem.daydiff==0){
              vitem.status = 'H';
            } else {
              vitem.status = 'H+'+vitem.daydiff;
            }

          }
        },
        wait_delivery: function(vdata){
          var vtoday = new Date();
          var theDay = new Date();
          for(var i=0; i<vdata.length; i++){
            var vitem = vdata[i];
            // theDay.setDate(vtoday.getDate()-vitem.daydiff);
            // vitem.tgl_masuk = $filter('date')(theDay, 'dd/MM/yyyy')+' '+vitem.jam_masuk;
            var theDay = new Date(vitem.startdate);
            vitem.daydiff = vtoday.getDate()-theDay.getDate();
            vitem.tgl_masuk = $filter('date')(theDay, 'dd/MM/yyyy')+' '+vitem.jam_masuk;

            //vitem.tgl_serah = $filter('date')(theDay, 'dd/MM/yyyy')+' '+vitem.jam_serah;

            if (vitem.daydiff==0){
              vitem.status = 'H';
            } else {
              vitem.status = 'H+'+vitem.daydiff;
            }

          }
        },
        ready_delivery: function(vdata){
          var vtoday = new Date();
          var theDay = new Date();
          for(var i=0; i<vdata.length; i++){
            var vitem = vdata[i];
            
            // theDay.setDate(vtoday.getDate()-vitem.daydiff);
            // vitem.tgl_masuk = $filter('date')(theDay, 'dd/MM/yyyy')+' '+vitem.jam_masuk;

            // vitem.tgl_serah = $filter('date')(vtoday, 'dd/MM/yyyy')+' '+vitem.jam_serah;
            var theDay = new Date(vitem.startdate);
            vitem.daydiff = vtoday.getDate()-theDay.getDate();
            vitem.tgl_masuk = $filter('date')(theDay, 'dd/MM/yyyy HH:mm');

            if (vitem.daydiff==0){
              vitem.status = 'H';
            } else {
              vitem.status = 'H+'+vitem.daydiff;
            }

          }
        },
        wait_reception: function(vdata){
          var vtoday = new Date();
          var theDay = new Date();
          for(var i=0; i<vdata.length; i++){
            var vitem = vdata[i];
            // theDay.setDate(vtoday.getDate()-vitem.daydiff);            
            // var vminute = vitem.timewait;
            // var vnow = $filter('date')(vtoday, 'HH:mm');
            // var mnow = JsBoard.Common.getMinute(vnow);
            // vitem.jam_masuk = JsBoard.Common.getTimeString(mnow-vminute);
            // vitem.tgl_masuk = $filter('date')(theDay, 'dd/MM/yyyy')+' '+vitem.jam_masuk;

            var theDay = new Date(vitem.startdate);
            vitem.daydiff = vtoday.getDate()-theDay.getDate();
            vitem.tgl_masuk = $filter('date')(theDay, 'dd/MM/yyyy HH:mm');
            var vDayMin = $filter('date')(theDay, 'HH:mm');
            var vNowMin = $filter('date')(vtoday, 'HH:mm');
            vminute = JsBoard.Common.getMinute(vNowMin)-JsBoard.Common.getMinute(vDayMin);

            if (vitem.daydiff==0){
              vitem.status = JsBoard.Common.getTimeString(vminute);
            } else {
              vitem.status = 'H+'+vitem.daydiff;
            }
          }
        },
      },
      getData: function(dataname) {
        var vdata = this.allData[dataname];
        this.process[dataname](vdata);
        // console.log("**vdata**", vdata);
        return vdata;
      },
    }
  });