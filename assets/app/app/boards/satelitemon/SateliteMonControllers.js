angular.module('app')
.controller('SateliteMonController', function($scope, $http, CurrentUser, Parameter, ngDialog, $timeout, $window, $filter, SateliteMonitoring, Idle) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        var arrBoard = []
        var allBoardHeight = 450;
        $scope.loading=true;
        $scope.gridData=[];
        $scope.waktu_sekarang = 'xxxx';

        $scope.smoncount = {}
        $scope.clock = "loading clock..."; // initialise the time variable
        $scope.tickInterval = 1000 //ms
        $scope.hourminute = '';

        $scope.porderboard = {}

        // $scope.porderboard.doFilter= function(){
        //   console.log(this);
        //   vidx = this.category_filter;
        //   var vopt = {
        //     fieldname: this.field_filter,
        //     value: this.text_filter
        //   }
        //   for (var i=1; i<=arrBoard.length; i++){
        //     if ((i==vidx) || (vidx==0)){
        //       dataFiltering(arrBoard[i-1], vopt)
        //     } else {
        //       dataFiltering(arrBoard[i-1], {fieldname: '', value: ''})
        //     }
        //   }
        // }
        // $scope.porderboard.resetFilter= function(){
        //   console.log("reset-filter", this);
        //   this.category_filter = "0";
        //   this.field_filter = "all";
        //   this.text_filter = '';
        //   this.doFilter();
        // }
        // $scope.porderboard.resetFilter();

        // var filterItem = function(vitem, vfield, vtext){
        //   if (vtext===''){
        //     return true;
        //   } else {
        //     if (typeof vitem[vfield] !== 'undefined'){
        //       return vitem[vfield].search(new RegExp(vtext, "i"))>=0
        //       // if (vitem[vfield] === vtext) {
        //       //   return true;
        //       // }
        //     }
        //     return false;
        //   }
        // }

        // var filterAllField = function(vitem, vtext){
        //   if (vtext===''){
        //     return true;
        //   } else {
        //     for(vfield in vitem){
        //       if (typeof vitem[vfield] !== 'undefined'){
        //         var vstr = vitem[vfield]+'';
        //         var vresult = vstr.search(new RegExp(vtext, "i"))>=0
        //         if (vresult){
        //           return true;
        //         }
        //         // if (vitem[vfield] === vtext) {
        //         //   return true;
        //         // }
        //       }
        //     }
        //     return false;
        //   }
        // }
        
        // var filterList = {
        //   'all': function(vdata, vtext){
        //     return filterAllField(vdata, vtext);
        //   },
        //   'nopol': function(vdata, vtext){
        //     return filterItem(vdata, 'nopol', vtext);
        //   },
        //   'appointment': function(vdata, vtext){
        //     return (vdata['doctype']=='AP') && filterItem(vdata, 'docno', vtext);
        //   },
        //   'wo': function(vdata, vtext){
        //     return (vdata['doctype']=='WO') && filterItem(vdata, 'docno', vtext);
        //   },
        //   'so': function(vdata, vtext){
        //     return (vdata['doctype']=='WO') && filterItem(vdata, 'docno', vtext);
        //   },
        //   'rpp': function(vdata, vtext){
        //     return (vdata['statusname']=='rpp') && filterItem(vdata, 'no_gr', vtext);
        //   },
        //   'claim': function(vdata, vtext){
        //     return (vdata['statusname']=='claim') && filterItem(vdata, 'no_gr', vtext);
        //   }
        // }

        var firstTime = true;

        // var filterData = function(vdata, vopt){
        //   if (vopt.fieldname===''){
        //     result = vdata;
        //   } else {
        //     result = [];
        //     for(var i=0; i<vdata.length; i++){
        //       if (typeof filterList[vopt.fieldname] !=='undefined'){
        //         if (filterList[vopt.fieldname](vdata[i], vopt.value)){
        //           result.push(vdata[i]);
        //         }
        //       }
        //     }
        //   }
        //   return result;
        // }

        // var dataFiltering = function(vitem, voption){
        //   vdata = filterData(vitem.originalData, voption);
        //   vboard = vitem.board;
        //   vboard.createChips(vdata);
        //   vboard.autoResizeStage();
        //   vboard.redraw();
        //   $scope.count[vitem.boardName] = vdata.length;
        // }

        var registerBoard = function(bConfig){
          var vboard = new JsBoard.Container(bConfig.boardConfig);
          var vitem = {
            board: vboard,
            originalData: bConfig.boardData,
            incField: bConfig.incField,
            incMode: bConfig.incMode, //inc or dec 
            boardName: bConfig.boardName
          }
          arrBoard.push(vitem);
          var voption = {
            fieldname: '',
            value: ''
          }

          vboard.createChips(bConfig.boardData);
          vboard.autoResizeStage();
          vboard.redraw();
          $scope.smoncount[vitem.boardName] = bConfig.boardData.length;
          
          // dataFiltering(vitem, voption);
        }

        // angular.element($window).bind('resize', function () {
        //     console.log("on resize", $window.innerWidth);
        //     for (var i=0; i<arrBoard.length; i++){
        //       vbr = arrBoard[i];
        //       vbr.board.autoResizeStage();
        //       vbr.board.redraw();
        //       //vbr.redrawChips();
        //     }


        // });

        var incrementTime = function(varrdata, vfield){
          for(var i=0; i<varrdata.length; i++){
            var vdata = varrdata[i];
            var vmin = JsBoard.Common.getMinute(vdata[vfield]);
            var vinc = 1;
            var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
            vdata[vfield] = vsmin;
          }
        }

        var decrementTime = function(varrdata, vfield){
          for(var i=0; i<varrdata.length; i++){
            var vdata = varrdata[i];
            var vmin = JsBoard.Common.getMinute(vdata[vfield]);
            var vinc = -1;
            var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
            vdata[vfield] = vsmin;
          }
        }

        // var simulateWaitPartNoDate = function(vdata){
        //   var vxdif = 2;
        //   for (var i=0; i<vdata.length; i++){
        //     var today = new Date();
        //     var xdate1 = new Date();
        //     xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
        //     var xdate2 = new Date();
        //     xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
        //     var vtime = vdata[i].jam;
        //     if((vdata[i].daydif+vxdif)==0){
        //       var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
        //       vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
        //     }
        //     vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
        //     vdata[i].tgl_diperlukan = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
        //     var vdays = Math.round((today-xdate2)/(24*60*60*1000));
        //     var xdays;
        //     var xdays = 'TODAY';
        //     if (vdays>0){
        //       xdays = 'H+'+vdays;
        //     } else if (vdays<0){
        //       xdays = 'H'+vdays;
        //     }
        //     vdata[i].daywait = xdays;
        //     vdata[i].dday = vdays;

        //   }
        //   console.log('**** vdata ****', vdata);
        // }
        
        // // berdasarkan nilai terkecil, satuan jam
        // // harus terurut berdasarkan day terkecil, kemudian jam terkecil
        // var orderDayColorRange = [
        //   {day: -1, color: '#fff'}, // day <-2
        //   {day: 0, color: '#0e0'},  // day <0 (yesterday)
        //   {day: 1, color: 'hour_color', default: '#ec0'}, // day <1 (today) --> check hour color, if hour color not defined, use default
        //   {day: 9999, color: '#900'}, // day<9999 (other day), use this color
        // ]
        // var orderHourColorRange = [
        //   {hour: -1, color: '#ec0'}, // hour <-1 use this color
        //   {hour: 24, color: '#e00'}, // hour<9999 (other day), use this color
        // ]
        // var orderDayTextColorRange = [
        //   {day: -1, color: '#000'}, // day <-2
        //   {day: 9999, color: '#fff'}, // day<9999 (other day), use this color
        // ]
        //     // if (vmin>30){
        //     //     return '#e00';
        //     // } else if (vmin>15){
        //     //     return '#ec0';
        //     // } else {
        //     //     return '#0e0';
        //     // }
        
        // var simulateWaitOrderDate = function(vdata){
        //   var vxdif = 2;
        //   for (var i=0; i<vdata.length; i++){
        //     var today = new Date();
        //     var xdate1 = new Date();
        //     xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
        //     var xdate2 = new Date();
        //     xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
        //     var vtime = vdata[i].jam;
        //     if((vdata[i].daydif+vxdif)==0){
        //       var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
        //       vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
        //     }
        //     vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
        //     vdata[i].tgl_diperlukan = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
        //     var vdays = Math.round((today-xdate2)/(24*60*60*1000));
        //     var xdays;
        //     var xdays = 'TODAY';
        //     if (vdays>0){
        //       xdays = 'H+'+vdays;
        //     } else if (vdays<0){
        //       xdays = 'H'+vdays;
        //     }
        //     vdata[i].daywait = xdays;
        //     vdata[i].dday = vdays;
        //     vdata[i].dayRange = orderDayColorRange;
        //     vdata[i].hourRange = orderHourColorRange;
        //     vdata[i].textDayRange = orderDayTextColorRange;
        //     vdata[i].textHourRange = [];
        //   }
        //   console.log('**** vdata ****', vdata);
        // }
        
        // var simulateStatusOrderDate = function(vdata){
        //   var vxdif = 2;
        //   for (var i=0; i<vdata.length; i++){
        //     var today = new Date();
        //     var xdate1 = new Date();
        //     xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
        //     var xdate2 = new Date();
        //     xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
        //     var vtime = vdata[i].jam;
        //     if((vdata[i].daydif+vxdif)==0){
        //       var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
        //       vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
        //     }
        //     vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
        //     vdata[i].tgl_diperlukan = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
        //     vdata[i].tgl_eta = $filter('date')(xdate1, 'dd/MM/yyyy')+' '+vtime;
        //     var vdays = Math.round((today-xdate2)/(24*60*60*1000));
        //     var xdays;
        //     var xdays = 'TODAY';
        //     if (vdays>0){
        //       xdays = 'H+'+vdays;
        //     } else if (vdays<0){
        //       xdays = 'H'+vdays;
        //     }
        //     vdata[i].daywait = xdays;
        //     vdata[i].dday = vdays;
        //     vdata[i].dayRange = orderDayColorRange;
        //     vdata[i].hourRange = orderHourColorRange;
        //     vdata[i].textDayRange = orderDayTextColorRange;
        //     vdata[i].textHourRange = [];
        //   }
        //   console.log('**** vdata ****', vdata);
        // }
        
        // var simulateStatusRppClaimDate = function(vdata){
        //   var vxdif = 2;
        //   for (var i=0; i<vdata.length; i++){
        //     var today = new Date();
        //     var xdate1 = new Date();
        //     xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
        //     var xdate2 = new Date();
        //     xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
        //     var vtime = vdata[i].jam;
        //     if((vdata[i].daydif+vxdif)==0){
        //       var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
        //       vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
        //     }
        //     vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
        //     vdata[i].tgl_gr = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
        //     vdata[i].tgl_eta = $filter('date')(xdate1, 'dd/MM/yyyy')+' '+vtime;
        //     var vdays = Math.round((today-xdate2)/(24*60*60*1000));
        //     var xdays;
        //     var xdays = 'TODAY';
        //     if (vdays>0){
        //       xdays = 'H+'+vdays;
        //     } else if (vdays<0){
        //       xdays = 'H'+vdays;
        //     }
        //     vdata[i].daywait = xdays;
        //     vdata[i].dday = vdays;
        //     vdata[i].dayRange = orderDayColorRange;
        //     vdata[i].hourRange = orderHourColorRange;
        //     vdata[i].textDayRange = orderDayTextColorRange;
        //     vdata[i].textHourRange = [];
        //   }
        //   console.log('**** vdata ****', vdata);
        // }
        
        // var simulateDeliveryDate = function(vdata){
        //   for (var i=0; i<vdata.length; i++){
        //     var today = new Date();
        //     var xdate1 = new Date();
        //     xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
        //     var xdate2 = new Date();
        //     xdate2.setDate(xdate2.getDate()+vdata[i].daydif+3);
            
        //     var vtime = vdata[i].start;
        //     if(vdata[i].daydif==0){
        //       var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
        //       vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
        //     }
        //     vdata[i].tgl_notif = $filter('date')(xdate1, 'dd/MM/yyyy')+' '+vtime;
        //     vdata[i].tgl_janji_serah = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vdata[i].start;
        //     var vdays = Math.round((today-xdate1)/(24*60*60*1000));
        //     var xdays;
        //     var xdays = 'TODAY';
        //     if (vdays>0){
        //       xdays = 'H+'+vdays;
        //     } else if (vdays<0){
        //       xdays = 'H'+vdays;
        //     }
        //     vdata[i].daywait = xdays;
        //     vdata[i].dday = vdays;

        //   }
        //   console.log('**** vdata ****', vdata);
        // }
        

        var onTimer = function(){
            var vhmin= $filter('date')($scope.clock, 'hh:mm');
            if ($scope.hourminute!==vhmin){
              $scope.hourminute = vhmin;
              if (!firstTime){
                for(var i=0; i<arrBoard.length; i++){
                  vitem = arrBoard[i];
                  if (vitem.incMode=='inc'){
                    incrementTime(vitem.board.chipItems, vitem.incField);
                  } else if (vitem.incMode=='dec'){
                    decrementTime(vitem.board.chipItems, vitem.incField);
                  }
                  vitem.board.redrawChips();
                }
              } else {
                firstTime = false;
              }
            }
        }

        var tick = function () {
            $scope.clock = Date.now() // get the current time
            onTimer();
            $timeout(tick, $scope.tickInterval); // reset the timer
        }

        // Start the timer
        $timeout(tick, $scope.tickInterval);

        // start timeout for create board
        // menggunakan timeout agar dijalankan setelah screen benar-benar ter-load.
        
        $timeout(function(){
          var vurl = '/api/as/Boards/BP/SatMon';
          $http.get(vurl).then(function(res){
            updateData(res);
            boardLoad();
          });
          }, 100);

        var updateData = function(res){
          SateliteMonitoring.allData = {
            ready_pickup: [],
            wait_delivery: [],
            ready_delivery: [],
            wait_reception: []
          }
          var vdata = res.data.Result;
          for(var i=0; i<vdata.length; i++){
            var vitem = vdata[i];
            if (vitem.BoardType=='sat_rpu'){
              var vnewdata = {chip: 'ready_pickup', branchname: vitem.SateliteName, nopol: vitem.PoliceNumber, tipe: vitem.ModelType, 
                startdate: $filter('date')(vitem.WaktuStart, 'yyyy-MM-dd'),
                jam_masuk: $filter('date')(vitem.WaktuStart, 'HH:mm'),
                sa_name: vitem.SaName,
                tgl_masuk: '01/01/2017', statusicon: [],
              }
              if (vitem.StatusId==2394){
                vnewdata.statusicon.push('pickup');
              }
              SateliteMonitoring.allData['ready_pickup'].push(vnewdata);
            } else if (vitem.BoardType=='sat_wdv'){
              var vnewdata = {chip: 'wait_delivery', branchname: vitem.SateliteName, nopol: vitem.PoliceNumber, tipe: vitem.ModelType, 
                startdate: $filter('date')(vitem.WaktuStart, 'yyyy-MM-dd'),
                jam_masuk: $filter('date')(vitem.WaktuStart, 'HH:mm'),
                tgl_serah: $filter('date')(vitem.JanjiSerah, 'dd/MM/yyyy HH:mm'),
                sa_name: vitem.SaName,
                tgl_masuk: '01/01/2017', statusicon: [],
              }
              SateliteMonitoring.allData['wait_delivery'].push(vnewdata);
            } else if (vitem.BoardType=='ctr_wfr'){
              var vnewdata = {chip: 'wait_reception', branchname: vitem.SateliteName, nopol: vitem.PoliceNumber, tipe: vitem.ModelType, 
                startdate: $filter('date')(vitem.WaktuStart, 'yyyy-MM-dd HH:mm:ss'),
                jam_masuk: $filter('date')(vitem.WaktuStart, 'HH:mm'),
                //tgl_serah: $filter('date')(vitem.JanjiSerah, 'dd/MM/yyyy HH:mm'),
                sa_name: vitem.SaName,
                tgl_masuk: '01/01/2017', statusicon: [],
              }
              SateliteMonitoring.allData['wait_reception'].push(vnewdata);
            } else if (vitem.BoardType=='ctr_rdv'){
              var vnewdata = {chip: 'ready_delivery', branchname: vitem.SateliteName, nopol: vitem.PoliceNumber, tipe: vitem.ModelType, 
                startdate: $filter('date')(vitem.WaktuStart, 'yyyy-MM-dd'),
                jam_masuk: $filter('date')(vitem.WaktuStart, 'HH:mm'),
                tgl_serah: $filter('date')(vitem.JanjiSerah, 'dd/MM/yyyy HH:mm'),
                sa_name: vitem.SaName,
                tgl_masuk: '01/01/2017', statusicon: [],
              }
              SateliteMonitoring.allData['ready_delivery'].push(vnewdata);
            }
          }

          // SateliteMonitoring.allData['ready_pickup'] = [{chip: 'ready_pickup', branchname: 'Pramuka', nopol: 'B 5601 BNC', tipe: 'AVANZA', daydiff: 0,
          //   startdate:'2017-12-23', tgl_masuk: '2017-12-23', statusicon: [], sa_name: 'ADI', jam_masuk: '08:16'
          // }]

          // {chip: 'wait_delivery', branchname: 'Pramuka', nopol: 'B 5601 VNC', tipe: 'AVANZA', daydiff: 0, 
          //   tgl_masuk: '01/01/2017', status: 'H+1', statusicon: [], sa_name: 'BUDI', jam_masuk: '08:16',
          //   tgl_serah: '01/01/2017', jam_serah: '09:00',
          // },          

          // {chip: 'ready_delivery', branchname: 'Pramuka', nopol: 'B 5801 FBC', tipe: 'AVANZA', daydiff: 0, 
          //   tgl_masuk: '01/01/2017', status: 'H+1', statusicon: [], sa_name: 'ADI', jam_masuk: '08:16',
          //   tgl_serah: '01/01/2017', jam_serah: '10:00',
          // },          
          
          // {chip: 'wait_reception', branchname: 'Pramuka', nopol: 'B 6801 LNC', tipe: 'AVANZA', daydiff: 0, 
          //   tgl_masuk: '01/01/2017', status: 'H+1', statusicon: [], sa_name: 'ADI', jam_masuk: '08:16', timewait: 29,
          // },          
          

        }
        var boardLoad = function() {
          //------------------------------------------//
          // **** ready to pickup **** //
          //------------------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 100,
              width: 320,
              gap: 2
            },
            board:{
              container: 'satmon-ready_for_pickup',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'satelitemon',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }

          }

          var boardData = SateliteMonitoring.getData('ready_pickup')
          // console.log('boardData-readypickup', boardData);

          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: '',
            boardName: 'ready_to_pickup'
          })

          //------------------------------------------//
          // **** waiting for delivery **** //
          //------------------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 120,
              width: 320,
              gap: 2
            },
            board:{
              container: 'satmon-waitingdelivery',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'satelitemon',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }

          }

          var boardData = SateliteMonitoring.getData('wait_delivery')
          // console.log('boardData-readypickup', boardData);

          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: '',
            boardName: 'waiting_for_delivery'
          })


          //------------------------------------------//
          // **** Ready for delivery Center **** //
          //------------------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 100,
              width: 320,
              gap: 2
            },
            board:{
              container: 'stmon-readydelivery-center',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'satelitemon',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }

          }

          var boardData = SateliteMonitoring.getData('ready_delivery')
          // console.log('boardData-readypickup', boardData);

          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: '',
            boardName: 'ready_for_delivery'
          })

          //------------------------------------------//
          // **** Waiting For Reception Center **** //
          //------------------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 100,
              width: 320,
              gap: 2
            },
            board:{
              container: 'stmon-waitingreception-center',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'satelitemon',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }

          }

          var boardData = SateliteMonitoring.getData('wait_reception')
          // console.log('boardData-readypickup', boardData);

          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'status',
            incMode: 'inc',
            boardName: 'wait_reception'
          })


          // //---------------------------------//
          // // **** waiting for Order **** //
          // //---------------------------------//
          // var boardConfig = {
          //   type: 'kanban',
          //   chip: {
          //     height: 105,
          //     width: 320,
          //     gap: 2
          //   },
          //   board:{
          //     container: 'parts-waiting_order',
          //     iconFolder: 'images/boards/parts',
          //     chipGroup: 'parts_order',
          //   },
          //   stage: {
          //     maxHeight: allBoardHeight,
          //     minHeight: allBoardHeight,
          //   },
          //   layout: {
          //     chipHeight: 80,
          //     chipGap: 2
          //   }

          // }

          // var boardData = [
          //   {chip: 'waiting_order', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 6661 BCD', jam: '10:00', type: 'AVANZA', 
          //     tgl_kirim: '09/Jan/2017', time_diperlukan: '11/Jan/2017 10:00', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3, cust_type: 'B', timewait: '00:00',
          //     no_rangka: 'JSHZ89172SDEADAX03', docno: 'WO/2931045888', doctype: 'WO'
          //   },
          //   {chip: 'waiting_order', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 BCD', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
          //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W',
          //     no_rangka: 'BCDW89172SDEADAX04', docno: 'AP/3129988899', doctype: 'AP'
          //   },
          //   {chip: 'waiting_order', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 7788 BCD', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B',
          //     no_rangka: 'KJSK89172SDEADAX05', docno: 'AP/298373890', need_dp: 1, doctype: 'AP'
          //   },
          //   {chip: 'waiting_order', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9918 BCD', jam: '09:00', type: 'AVANZA', timewait: '03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S',
          //     no_rangka: 'WDFW89172SDEADAX06', docno: 'SO/721045991', doctype: 'SO'
          //   },
          //   {chip: 'waiting_order', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B',
          //     statusicon: ['dp', 'dp'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          // ]
          // simulateWaitOrderDate(boardData);

          // registerBoard({
          //   boardConfig: boardConfig,
          //   boardData: boardData,
          //   incField: 'timewait',
          //   incMode: 'inc',
          //   boardName: 'waiting_order'
          // })


          // //------------------------------//
          // // **** Status Order Parts **** //
          // //------------------------------//

          // var showDetailEta = function(vchip){
          //   $scope.currentChip = vchip.data;
          //   ngDialog.openConfirm ({
          //     width: 1000,
          //     template: 'app/boards/partsorder/detailEta.html',
          //     plain: false,
          //     controller: 'PartsOrderController',
          //     scope: $scope
          //   });
          // }
          // var boardConfig = {
          //   type: 'kanban',
          //   chip: {
          //     height: 115,
          //     width: 320,
          //     gap: 2
          //   },
          //   stage: {
          //     maxHeight: allBoardHeight,
          //     minHeight: allBoardHeight,
          //   },
          //   board:{
          //     container: 'parts-status_order_parts',
          //     iconFolder: 'images/boards/parts',
          //     chipGroup: 'parts_order',
          //     onChipCreated: function(vchip){
          //       console.log(vchip);
          //       if (typeof vchip.shapes !== 'undefined'){
          //         if (typeof vchip.shapes.eta_link !== 'undefined'){
          //           vchip.shapes.eta_link.on('mouseup', function(evt){
          //             evt.evt.preventDefault();
          //             var vchip = this.parent;
          //             showDetailEta(this.parent);
          //             if ((typeof vchip.data !== 'undefined') && (typeof vchip.data.statusicon !=='undefined')){
          //                   // showDetailEta(this.parent);
          //                 // if (vchip.data.statusicon.indexOf('eta')>=0){
          //                 //   showDetailEta(this.parent);
          //                 // }
          //             }
          //             //alert("click eta");
          //           })
          //         }
          //       }
          //       //alert('chip created');
          //       //asdf();
          //     }
          //   },
          //   layout: {
          //     chipHeight: 80,
          //     chipGap: 2
          //   }

          // }

          // var boardData = [
          //   {chip: 'status_order_parts', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 6661 BCD', jam: '10:00', type: 'AVANZA', vendor: 'SPLD',
          //     tgl_kirim: '09/Jan/2017', time_diperlukan: '11/Jan/2017 10:00', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3, cust_type: 'B', timewait: '00:00',
          //     statusicon: ['open', 'late', 'eta', 'bo'],
          //     no_rangka: 'JSHZ89172SDEADAX03', docno: 'WO/2931045888', doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 BCD', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
          //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W', vendor: 'SPLD',
          //     statusicon: ['open', 'late'],
          //     no_rangka: 'BCDW89172SDEADAX04', docno: 'AP/312998889', doctype: 'AP'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 7788 BCD', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B', vendor: 'SPLD',
          //     no_rangka: 'KJSK89172SDEADAX05', docno: 'AP/298373890', need_dp: 1, doctype: 'AP'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9918 BCD', jam: '09:00', type: 'AVANZA', timewait: '03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S', vendor: 'SPLD',
          //     statusicon: ['open', 'eta'],
          //     no_rangka: 'WDFW89172SDEADAX06', docno: 'SO/721045991', doctype: 'SO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_order_parts', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 BCD', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'],
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          // ]
          // simulateStatusOrderDate(boardData);

          // registerBoard({
          //   boardConfig: boardConfig,
          //   boardData: boardData,
          //   incField: 'timewait',
          //   incMode: 'inc',
          //   boardName: 'status_order_parts'
          // })

          // //------------------------------------//
          // // **** Status RPP & Parts Claim **** //
          // //------------------------------------//
          // var boardConfig = {
          //   type: 'kanban',
          //   chip: {
          //     height: 115,
          //     width: 320,
          //     gap: 2
          //   },
          //   stage: {
          //     maxHeight: allBoardHeight,
          //     minHeight: allBoardHeight,
          //   },
          //   board:{
          //     container: 'parts-status_rpp_claim',
          //     iconFolder: 'images/boards/parts',
          //     chipGroup: 'parts_order',
          //   },
          //   layout: {
          //     chipHeight: 80,
          //     chipGap: 2
          //   }

          // }

          // var boardData = [
          //   {chip: 'status_rpp_claim', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 6661 ABN', jam: '10:00', type: 'AVANZA', vendor: 'SPLD',
          //     tgl_kirim: '09/Jan/2017', time_diperlukan: '11/Jan/2017 10:00', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3, cust_type: 'B', timewait: '00:00',
          //     statusicon: ['open', 'late', 'eta'],
          //     statusname: 'rpp', no_gr: 'A1312345',
          //     no_rangka: 'JSHZ89172SDEADAX03', docno: 'WO/2931045888', doctype: 'WO'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 ABN', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
          //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W', vendor: 'SPLD',
          //     statusicon: ['open', 'late'], 
          //     statusname: 'claim', no_gr: 'B191029123',
          //     no_rangka: 'BCDW89172SDEADAX04', docno: 'AP/312998889', doctype: 'AP'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 7788 ABN', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B', vendor: 'SPLD',
          //     statusname: 'rpp', no_gr: 'A998173123',
          //     no_rangka: 'KJSK89172SDEADAX05', docno: 'AP/298373890', need_dp: 1, doctype: 'AP'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9918 ABN', jam: '09:00', type: 'AVANZA', timewait: '03:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S', vendor: 'SPLD',
          //     statusicon: ['open'], no_gr: 'V991031234',
          //     statusname: 'rpp',
          //     no_rangka: 'WDFW89172SDEADAX06', docno: 'SO/721045991', doctype: 'SO'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 ABN', jam: '09:20', type: 'AVANZA', timewait: '02:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'], no_gr: 'K00944123222',
          //     statusname: 'claim',
          //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', need_dp: 1, doctype: 'WO'
          //   },
          //   {chip: 'status_rpp_claim', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 ABN', jam: '09:20', type: 'AVANZA', timewait: '04:00',
          //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
          //     statusicon: ['draft'], no_gr: 'K00888123222',
          //     statusname: 'claim',
          //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', need_dp: 1, doctype: 'WO'
          //   },
          // ]
          // simulateStatusRppClaimDate(boardData);

          // registerBoard({
          //   boardConfig: boardConfig,
          //   boardData: boardData,
          //   incField: 'timewait',
          //   incMode: 'inc',
          //   boardName: 'status_rpp_claim'
          // })



          // ------- //

        }


      Idle.unwatch();
      console.log('==== value of idle ==', Idle)
    });
    $scope.$on('$destroy', function(){
      Idle.watch();
    });



})

;
