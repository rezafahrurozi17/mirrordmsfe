angular.module('app')
.controller('SateliteMonController_New', function($scope, $http, CurrentUser, Parameter, ngDialog, $timeout, $window, $interval, $filter, SateliteMonitoring) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {

      $scope.LoadAllData();
      var tesH = $("#tesh").height();
      var tesw = $(window).height();
      console.log('ada')

    });

      
        var arrBoard = []
        // var allBoardHeight = 450; // 800
        var allBoardHeight = $(window).height() * 70 / 100;
        $scope.loading=true;
        $scope.gridData=[];
        $scope.waktu_sekarang = 'xxxx';

        $scope.smoncount = {}
        $scope.clock = "loading clock..."; // initialise the time variable
        $scope.tickInterval = 1000 //ms
        $scope.hourminute = '';
        var firstTime = true;

      

        var registerBoard = function(bConfig){
          var vboard = new JsBoard.Container(bConfig.boardConfig);
          vboard.createChips(bConfig.boardData);
          vboard.autoResizeStage();
          vboard.redraw();

          var vitem = {
            board: vboard,
            // originalData: bConfig.boardData,
            incField: bConfig.incField,
            incMode: bConfig.incMode, //inc or dec 
            // boardName: bConfig.boardName
          }
          arrBoard.push(vitem);
          var voption = {
            fieldname: '',
            value: ''
          }

          
          $scope.smoncount[vitem.boardName] = bConfig.boardData.length;
        }

        var showData = 2;

       
        var MulaiDataReadyForPickup = 0;
        var BagiBoardReadyForPickup = 0;
        var CountBoardReadyForPickup = 0;
        var ModulusReadyForPickup = 0;
        var dataHabisReadyForPickup = true;

        var MulaiDataWaitingForDelivery = 0;
        var BagiBoardWaitingForDelivery = 0;
        var CountBoardWaitingForDelivery = 0;
        var ModulusWaitingForDelivery = 0;
        var dataHabisWaitingForDelivery = true;

        var MulaiDataWaitingForReceptionCenter = 0;
        var BagiBoardWaitingForReceptionCenter = 0;
        var CountBoardWaitingForReceptionCenter = 0;
        var ModulusWaitingForReceptionCenter = 0;
        var dataHabisWaitingForReceptionCenter = true;

        var MulaiDataReadyForDelivery = 0;
        var BagiBoardReadyForDelivery = 0;
        var CountBoardReadyForDelivery = 0;
        var ModulusReadyForDelivery = 0;
        var dataHabisReadyForDelivery = true;

        
        $scope.LoadAllData = function(){

          arrBoard = [];

          arrReadyForPickup = [];
          arrWaitingForDelivery = [];
          arrWaitingForReceptionCenter = [];
          arrReadyForDelivery = [];

          var updateAutoResize = function(){
            for (var i=0; i<arrBoard.length; i++){
              vbr = arrBoard[i];
              vbr.board.autoResizeStage();
              vbr.board.redraw();
              //vbr.redrawChips();
            }
          }

          var velement = document.getElementById('sm-main-panel');
          var erd = elementResizeDetectorMaker();
          erd.listenTo(velement, function(element) {
            updateAutoResize();
          });

          var incrementTime = function(varrdata, vfield){
            for(var i=0; i<varrdata.length; i++){
              var vdata = varrdata[i];
              var vmin = JsBoard.Common.getMinute(vdata[vfield]);
              var vinc = 1;
              var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
              vdata[vfield] = vsmin;
            }
          }

          var decrementTime = function(varrdata, vfield){
            for(var i=0; i<varrdata.length; i++){
              var vdata = varrdata[i];
              var vmin = JsBoard.Common.getMinute(vdata[vfield]);
              var vinc = -1;
              var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
              vdata[vfield] = vsmin;
            }
          }

          var onTimer = function(){
            var vhmin= $filter('date')($scope.clock, 'hh:mm');
            if ($scope.hourminute!==vhmin){
              $scope.hourminute = vhmin;
              if (!firstTime){
                for(var i=0; i<arrBoard.length; i++){
                  vitem = arrBoard[i];
                  if (vitem.incMode=='inc'){
                    incrementTime(vitem.board.chipItems, vitem.incField);
                  } else if (vitem.incMode=='dec'){
                    decrementTime(vitem.board.chipItems, vitem.incField);
                  }
                  vitem.board.redrawChips();
                }
              } else {
                firstTime = false;
              }
            }
          }

          var tick = function () {
            $scope.clock = Date.now() // get the current time
            onTimer();
            $timeout(tick, $scope.tickInterval); // reset the timer
          }

          // Start the timer
          $timeout(tick, $scope.tickInterval);







          // // ========================================================================================= start ready for pick up

          // var FuncReadyForPickup = function(data){
          //   //---------------------------------//
          //   // **** Ready For PickUP **** //
          //   //---------------------------------//
          //   var boardConfig = {
          //     type: 'kanban',
          //     chip: {
          //       height: 115,
          //       width: 320,
          //       gap: 2
          //     },
          //     board:{
          //       container: 'satmon-ready_for_pickup',
          //       iconFolder: 'images/boards/iregular',
          //       chipGroup: 'satelitemon',
          //     },
          //     stage: {
          //       maxHeight: allBoardHeight,
          //       minHeight: allBoardHeight,
          //     },
          //     layout: {
          //       chipHeight: 80,
          //       chipGap: 2
          //     }
      
          //   }
      
          //   if (typeof data == 'undefined'){
          //     data = [];
          //   }
          //   boardData = data;
          //   registerBoard({
          //     boardConfig: boardConfig,
          //     boardData: boardData,
          //     incField: 'timewait',
          //     incMode: 'inc',
          //     boardName: 'ready_to_pickup'
          //   })
      
          // }
      
      
      
          // // var vurlReadyForPickup = '/api/as/Boards/BP/irregular/wfr';
          // var vurlReadyForPickup = '/api/as/Boards/BoardSatelliteBP/1';

          // $http.get(vurlReadyForPickup).then(function(res){
          //   var boardData = []
          //   // var boardData = res.data.Result;
          //   var vres = res.data.Result;
          //   for(var i=0; i<vres.length; i++){
          //     var vresitem = vres[i];
          //     if (vresitem.Status == 0){
          //       // var dtNow = new Date()
          //       // var dateOnly = new Date (dtNow.getFullYear(), dtNow.getMonth(), dtNow.getDate(),0,0,0)
          //       // var timeIn = new Date (vresitem.PushTimeIn.getFullYear(), vresitem.PushTimeIn.getMonth(), vresitem.PushTimeIn.getDate(),0,0,0)
              
          //       // var daysCount = (dateOnly.getTime() - timeIn.getTime()) / (1000 * 60 * 60 * 24)
          //       var statusHari = 'H'
          //       if (vresitem.LamaHariSatellite > 0){
          //         statusHari = 'H+' + vresitem.LamaHariSatellite
          //       }
              
          //       var vitem = {
          //         chip: 'ready_pickup', 
          //         branchname: vresitem.BranchName,
          //         nopol: vresitem.PoliceNumber,
          //         daydiff: vresitem.LamaHariSatellite,
          //         status: statusHari, 
          //         tipe: vresitem.ModelName + ' - ' + vresitem.ColorName, 
          //         tgl_masuk: $filter('date')(vresitem.PushTimeIn, 'dd/MM/yyyy HH:mm'), 
          //         sa_name: vresitem.Initial, 
          //         timewait: '', 
          //         statusicon: [], 
          //         category: '',
                  
          //       }
          //       // if (vresitem.isPrediagnose){
          //         vitem.statusicon.push('pickup');
          //       // }
          //       // boardData.push(vitem);
          //       arrReadyForPickup.push(vitem);
          //       $scope.jumlahReadyForPickup = arrReadyForPickup.length;

          //     }
              
          //   }
  
          //   if (dataHabisReadyForPickup == true)
          //   {
          //       CountBoardReadyForPickup = arrReadyForPickup.length;
          //       ModulusReadyForPickup = parseInt(CountBoardReadyForPickup % showData);
          //       BagiBoardReadyForPickup = parseInt(CountBoardReadyForPickup / showData);
          //       if (ModulusReadyForPickup != 0)
          //       {
          //           BagiBoardReadyForPickup = BagiBoardReadyForPickup + 1;
          //       }
          //   }
  
          //   for(var i= MulaiDataReadyForPickup;  i< arrReadyForPickup.length; i++){
          //     var vboard = arrReadyForPickup[i];
          //     boardData.push(vboard);
          //   }
  
          //   BagiBoardReadyForPickup = BagiBoardReadyForPickup - 1;
  
          //   if (BagiBoardReadyForPickup != 0)
          //   {
          //       dataHabisReadyForPickup = false;
          //       MulaiDataReadyForPickup = MulaiDataReadyForPickup + showData;
          //   }
          //   else
          //   {
          //       MulaiDataReadyForPickup = 0;
          //       BagiBoardReadyForPickup = 0;
          //       dataHabisReadyForPickup = true;
          //   }
  
          //   $timeout(function() {
          //     FuncReadyForPickup(boardData);
          //   }, 100);
      
          // });

          // // ========================================================================================= end ready for pick up




          // // ========================================================================================= start waiting for reception center

          // var FuncWaitingForReceptionCenter = function(data){
          //   //---------------------------------//
          //   // **** Waiting For Reception Center **** //
          //   //---------------------------------//
          //   var boardConfig = {
          //     type: 'kanban',
          //     chip: {
          //       height: 115,
          //       width: 320,
          //       gap: 2
          //     },
          //     board:{
          //       container: 'stmon-waitingreception-center',
          //       iconFolder: 'images/boards/iregular',
          //       chipGroup: 'satelitemon',
          //     },
          //     stage: {
          //       maxHeight: allBoardHeight,
          //       minHeight: allBoardHeight,
          //     },
          //     layout: {
          //       chipHeight: 80,
          //       chipGap: 2
          //     }
      
          //   }
      
          //   if (typeof data == 'undefined'){
          //     data = [];
          //   }
          //   boardData = data;
          //   registerBoard({
          //     boardConfig: boardConfig,
          //     boardData: boardData,
          //     incField: 'timewait',
          //     incMode: 'inc',
          //     boardName: 'wait_reception'
          //   })
      
          // }

          // var vurlWaitingForReceptionCenter = '/api/as/Boards/BoardSatelliteBP/1';

          // $http.get(vurlWaitingForReceptionCenter).then(function(res){
          //   var boardData = []
          //   // var boardData = res.data.Result;
          //   var vres = res.data.Result;
          //   for(var i=0; i<vres.length; i++){
          //     var vresitem = vres[i];
          //     if (vresitem.Status >= 4 && vresitem.Status <= 17){
          //       // var dtNow = new Date()
          //       // var dateOnly = new Date (dtNow.getFullYear(), dtNow.getMonth(), dtNow.getDate(),0,0,0)
          //       // var timeIn = new Date (vresitem.PushTimeIn.getFullYear(), vresitem.PushTimeIn.getMonth(), vresitem.PushTimeIn.getDate(),0,0,0)
              
          //       // var daysCount = (dateOnly.getTime() - timeIn.getTime()) / (1000 * 60 * 60 * 24)
          //       var statusHari = 'H'
          //       if (vresitem.LamaHariSatellite > 0){
          //         statusHari = 'H+' + vresitem.LamaHariSatellite
          //       }
              
          //       var vitem = {
          //         chip: 'wait_reception', 
          //         branchname: vresitem.BranchName,
          //         nopol: vresitem.PoliceNumber,
          //         daydiff: vresitem.LamaHariSatellite,
          //         status: statusHari, 
          //         tipe: vresitem.ModelName + ' - ' + vresitem.ColorName, 
          //         tgl_masuk: $filter('date')(vresitem.PushTimeIn, 'dd/MM/yyyy HH:mm'), 
          //         sa_name: vresitem.Initial, 
          //         timewait: '', 
          //         statusicon: [], 
          //         category: '',
                  
          //       }
          //       // if (vresitem.isPrediagnose){
          //         vitem.statusicon.push('pickup');
          //       // }
          //       // boardData.push(vitem);
          //       arrWaitingForReceptionCenter.push(vitem);
          //       $scope.jumlahWaitingForReceptionCenter = arrWaitingForReceptionCenter.length;

          //     }
              
          //   }
  
          //   if (dataHabisWaitingForReceptionCenter == true)
          //   {
          //       CountBoardWaitingForReceptionCenter = arrWaitingForReceptionCenter.length;
          //       ModulusWaitingForReceptionCenter = parseInt(CountBoardWaitingForReceptionCenter % showData);
          //       BagiBoardWaitingForReceptionCenter = parseInt(CountBoardWaitingForReceptionCenter / showData);
          //       if (ModulusWaitingForReceptionCenter != 0)
          //       {
          //           BagiBoardWaitingForReceptionCenter = BagiBoardWaitingForReceptionCenter + 1;
          //       }
          //   }
  
          //   for(var i= MulaiDataWaitingForReceptionCenter;  i< arrWaitingForReceptionCenter.length; i++){
          //     var vboard = arrWaitingForReceptionCenter[i];
          //     boardData.push(vboard);
          //   }
  
          //   BagiBoardWaitingForReceptionCenter = BagiBoardWaitingForReceptionCenter - 1;
  
          //   if (BagiBoardWaitingForReceptionCenter != 0)
          //   {
          //       dataHabisWaitingForReceptionCenter = false;
          //       MulaiDataWaitingForReceptionCenter = MulaiDataWaitingForReceptionCenter + showData;
          //   }
          //   else
          //   {
          //       MulaiDataWaitingForReceptionCenter = 0;
          //       BagiBoardWaitingForReceptionCenter = 0;
          //       dataHabisWaitingForReceptionCenter = true;
          //   }
  
          //   $timeout(function() {
          //     FuncWaitingForReceptionCenter(boardData);
          //   }, 100);
      
          // });
          // // ========================================================================================= end waiting for reception center

          // ========================================================================================= start ready for pick up & waiting for reception center (GABUNGAN)
          var FuncReadyForPickup = function(data){
            //---------------------------------//
            // **** Ready For PickUP **** //
            //---------------------------------//
            var boardConfig = {
              type: 'kanban',
              chip: {
                height: 135,
                width: 320,
                gap: 2
              },
              board:{
                container: 'satmon-ready_for_pickup',
                iconFolder: 'images/boards/iregular',
                chipGroup: 'satelitemon',
              },
              stage: {
                maxHeight: allBoardHeight,
                minHeight: allBoardHeight,
              },
              layout: {
                chipHeight: 80,
                chipGap: 2
              }
      
            }
      
            if (typeof data == 'undefined'){
              data = [];
            }
            boardData = data;
            registerBoard({
              boardConfig: boardConfig,
              boardData: boardData,
              incField: 'timewait',
              incMode: 'inc',
              boardName: 'ready_to_pickup'
            })
      
          }


          var FuncWaitingForReceptionCenter = function(data){
            //---------------------------------//
            // **** Waiting For Reception Center **** //
            //---------------------------------//
            var boardConfig = {
              type: 'kanban',
              chip: {
                height: 135,
                width: 320,
                gap: 2
              },
              board:{
                container: 'stmon-waitingreception-center',
                iconFolder: 'images/boards/iregular',
                chipGroup: 'satelitemon',
              },
              stage: {
                maxHeight: allBoardHeight,
                minHeight: allBoardHeight,
              },
              layout: {
                chipHeight: 80,
                chipGap: 2
              }
      
            }
      
            if (typeof data == 'undefined'){
              data = [];
            }
            boardData = data;
            registerBoard({
              boardConfig: boardConfig,
              boardData: boardData,
              incField: 'timewait',
              incMode: 'inc',
              boardName: 'wait_reception'
            })
      
          }

          var FuncReadyForDelivery = function(data){
            //---------------------------------//
            // **** Waiting For Delivery Center **** //
            //---------------------------------//
            var boardConfig = {
              type: 'kanban',
              chip: {
                height: 135,
                width: 320,
                gap: 2
              },
              board:{
                container: 'stmon-readydelivery-center',
                iconFolder: 'images/boards/iregular',
                chipGroup: 'satelitemon',
              },
              stage: {
                maxHeight: allBoardHeight,
                minHeight: allBoardHeight,
              },
              layout: {
                chipHeight: 80,
                chipGap: 2
              }
      
            }
      
            if (typeof data == 'undefined'){
              data = [];
            }
            boardData = data;
            registerBoard({
              boardConfig: boardConfig,
              boardData: boardData,
              incField: 'timewait',
              incMode: 'inc',
              boardName: 'ready_for_delivery'
            })
      
          }

          var FuncWaitingForDelivery = function(data){
            //---------------------------------//
            // **** Waiting For Delivery Center **** //
            //---------------------------------//
            var boardConfig = {
              type: 'kanban',
              chip: {
                height: 135,
                width: 320,
                gap: 2
              },
              board:{
                container: 'satmon-waitingdelivery',
                iconFolder: 'images/boards/iregular',
                chipGroup: 'satelitemon',
              },
              stage: {
                maxHeight: allBoardHeight,
                minHeight: allBoardHeight,
              },
              layout: {
                chipHeight: 80,
                chipGap: 2
              }
      
            }
      
            if (typeof data == 'undefined'){
              data = [];
            }
            boardData = data;
            registerBoard({
              boardConfig: boardConfig,
              boardData: boardData,
              incField: 'timewait',
              incMode: 'inc',
              boardName: 'waiting_for_delivery'
            })
      
          }


          var vurlReadyForPickupWaitingForReceptionCenter = '/api/as/Boards/BoardSatelliteBP/1';

          $http.get(vurlReadyForPickupWaitingForReceptionCenter).then(function(res){
            var boardDataReadyForPickup = []
            var boardDataWaitingForReceptionCenter = []
            var boardDataReadyForDelivery = []
            var boardDataWaitingForDelivery = []


            // var boardData = res.data.Result;
            var vres = res.data.Result;
            for(var i=0; i<vres.length; i++){
              var vresitem = vres[i];
              if (vresitem.ColorName === null || vresitem.ColorName === undefined){
                vresitem.ColorName = ''
              }
              // ColorNamex = vresitem.ColorName.split(' ')
              // if (ColorNamex.length > 3){
              //   vresitem.ColorName = ColorNamex[0] + ' ' + ColorNamex[1] + ' ' + ColorNamex[2]+'..'
              // } else {
              //   vresitem.ColorName = ColorNamex.join(' ');
              // }
              if (vresitem.ColorName.length > 21){
                vresitem.ColorName = vresitem.ColorName.substring(0,21)
                vresitem.ColorName = vresitem.ColorName + '..'
              }
              vresitem.ColorName = vresitem.ColorName.toUpperCase();

              if (vresitem.Status == 0 && vresitem.PushTimeInCenter === null){
               // ini kondisi untuk ready for pick up
                    var statusHari = 'H'
                    if (vresitem.LamaHariSatellite > 0){
                      statusHari = 'H+' + vresitem.LamaHariSatellite
                    }
                  
                    var vitem = {
                      chip: 'ready_pickup', 
                      branchname: vresitem.NamaSatellite,
                      nopol: vresitem.PoliceNumber,
                      daydiff: vresitem.LamaHariSatellite,
                      status: statusHari, 
                      tipe: vresitem.ModelName, 
                      warna: vresitem.ColorName,
                      tgl_masuk: $filter('date')(vresitem.PushTimeIn, 'dd/MM/yyyy HH:mm'), 
                      sa_name: vresitem.Initial, 
                      timewait: '', 
                      statusicon: [], 
                      category: '',
                      
                    }
                    if ((vresitem.PushTimeIn !== null && vresitem.PushTimeIn !== undefined && vresitem.PushTimeIn !== '') && vresitem.PushTimeInCenter === null && vresitem.PushTimeInSatelliteAfterCenter === null &&
                        (vresitem.PushTimeOut !== null && vresitem.PushTimeOut !== undefined && vresitem.PushTimeOut !== '') && vresitem.PushTimeOutCenter === null && vresitem.PushTimeOutSatelliteAfterCenter === null) 
                    {
                      vitem.statusicon.push('pickup');

                    }
                    arrReadyForPickup.push(vitem);
                    $scope.jumlahReadyForPickup = arrReadyForPickup.length;
                

              } else if ( ((vresitem.Status == 0 && vresitem.PushTimeInCenter !== null) || (vresitem.Status >= 3 && vresitem.Status <= 17)) && (vresitem.StatusGateOut != 1 && vresitem.PushTimeOutCenter == null)){
                // ini kondisi untuk waiting for reception
                  var hours = Math.floor(vresitem.LamaWaktuCenter / 60);          
                  var minutes = vresitem.LamaWaktuCenter % 60;
                  if (minutes < 10){
                    minutes = '0'+minutes;
                  }

                  if (vresitem.Status < 4){
                    vresitem.Initial = '';
                  }
              
                var vitem = {
                  chip: 'wait_reception', 
                  branchname: vresitem.NamaSatellite,
                  nopol: vresitem.PoliceNumber,
                  daydiff: vresitem.LamaWaktuCenter,
                  status: vresitem.LamaWaktuCenter, 
                  xstatus: hours+':'+minutes,
                  tipe: vresitem.ModelName, 
                  warna: vresitem.ColorName,
                  tgl_masuk: $filter('date')(vresitem.PushTimeInCenter, 'dd/MM/yyyy HH:mm'), 
                  sa_name: vresitem.Initial, 
                  timewait: '', 
                  statusicon: [], 
                  category: '',
                  
                }
                  //  vitem.statusicon.push('pickup');
                arrWaitingForReceptionCenter.push(vitem);
                $scope.jumlahWaitingForReceptionCenter = arrWaitingForReceptionCenter.length;


              } else if ( ((vresitem.PushTimeInCenter !== null && vresitem.StatusGateOut == 1) || (vresitem.Status >= 3 && vresitem.Status <= 17) || (vresitem.StatusGateOut != 1 && vresitem.PushTimeOutCenter != null)) && vresitem.PushTimeInSatelliteAfterCenter == null) {
                // ini kondisi Ready for delivery
                var statusHari = 'H'
                if (vresitem.LamaHariGateOutCenter > 0){
                  statusHari = 'H+' + vresitem.LamaHariGateOutCenter
                }
              
                var vitem = {
                  chip: 'ready_delivery', 
                  branchname: vresitem.NamaSatellite,
                  nopol: vresitem.PoliceNumber,
                  daydiff: vresitem.LamaHariGateOutCenter,
                  status: statusHari, 
                  xstatus: hours+':'+minutes,
                  tipe: vresitem.ModelName, 
                  warna: vresitem.ColorName,
                  tgl_masuk: $filter('date')(vresitem.JanjiPenyerahan, 'dd/MM/yyyy HH:mm'), 
                  tgl_serah: $filter('date')(vresitem.JanjiPenyerahan, 'dd/MM/yyyy HH:mm'), 
                  sa_name: vresitem.Initial, 
                  timewait: '', 
                  statusicon: [], 
                  category: '',
                  
                }

                if (vresitem.PushTimeInCenter != null && vresitem.PushTimeOutCenter != null){
                   vitem.statusicon.push('pickup');
                }
                arrReadyForDelivery.push(vitem);
                $scope.jumlahReadyForDelivery = arrReadyForDelivery.length;


              } else if ((vresitem.Status >= 3 && vresitem.Status < 17) && vresitem.PushTimeInSatelliteAfterCenter != null) {
                // ini kondisi waiting for delivery

                //     public DateTime? CustCallTime { get; set; }
                // public DateTime? ServiceExplanationDate { get; set; }
                // public long LamaWaktuCalltoSE { get; set; }
                // public int LamaHariCalltoSE { get; set; }
                var statusHari = 'H'
                var hours = 0       
                var minutes = 0
                
                if (vresitem.LamaHariCalltoSE > 0){
                  statusHari = 'H+' + vresitem.LamaHariCalltoSE 
                } else {
                  if (vresitem.LamaWaktuCalltoSE == null || vresitem.LamaWaktuCalltoSE == undefined){
                    vresitem.LamaWaktuCalltoSE = 0
                  }
                  hours = Math.floor(vresitem.LamaWaktuCalltoSE / 60);       
                  minutes = vresitem.LamaWaktuCalltoSE % 60;
                  if (minutes < 10){
                    minutes = '0'+minutes;
                  }
                  statusHari = hours+':'+minutes
                }

                if (vresitem.LamaHariGateOutCenter > 0){
                  statusHari = 'H+' + vresitem.LamaHariGateOutCenter
                }
              
                var vitem = {
                  chip: 'wait_delivery', 
                  branchname: vresitem.NamaSatellite,
                  nopol: vresitem.PoliceNumber,
                  daydiff: vresitem.LamaHariCalltoSE,
                  status: statusHari, 
                  // xstatus: hours+':'+minutes,
                  tipe: vresitem.ModelName, 
                  warna: vresitem.ColorName,
                  tgl_masuk: $filter('date')(vresitem.CustCallTime, 'dd/MM/yyyy HH:mm'), 
                  tgl_serah: $filter('date')(vresitem.JanjiPenyerahan, 'dd/MM/yyyy HH:mm'), 
                  sa_name: vresitem.Initial, 
                  timewait: '', 
                  statusicon: [], 
                  category: '',
                  
                }

                if (vresitem.PushTimeInCenter != null && vresitem.PushTimeOutCenter != null){
                   vitem.statusicon.push('pickup');
                }
                arrWaitingForDelivery.push(vitem);
                $scope.jumlahWaitingForDelivery = arrWaitingForDelivery.length;
              }
              
            }
  
            if (dataHabisReadyForPickup == true)
            {
                CountBoardReadyForPickup = arrReadyForPickup.length;
                ModulusReadyForPickup = parseInt(CountBoardReadyForPickup % showData);
                BagiBoardReadyForPickup = parseInt(CountBoardReadyForPickup / showData);
                if (ModulusReadyForPickup != 0)
                {
                    BagiBoardReadyForPickup = BagiBoardReadyForPickup + 1;
                }
            }
  
            for(var i= MulaiDataReadyForPickup;  i< arrReadyForPickup.length; i++){
              var vboard = arrReadyForPickup[i];
              boardDataReadyForPickup.push(vboard);
            }
  
            BagiBoardReadyForPickup = BagiBoardReadyForPickup - 1;
  
            if (BagiBoardReadyForPickup != 0)
            {
                dataHabisReadyForPickup = false;
                MulaiDataReadyForPickup = MulaiDataReadyForPickup + showData;
            }
            else
            {
                MulaiDataReadyForPickup = 0;
                BagiBoardReadyForPickup = 0;
                dataHabisReadyForPickup = true;
            }
  
            $timeout(function() {
              FuncReadyForPickup(boardDataReadyForPickup);
            }, 100);



            if (dataHabisWaitingForReceptionCenter == true)
            {
                CountBoardWaitingForReceptionCenter = arrWaitingForReceptionCenter.length;
                ModulusWaitingForReceptionCenter = parseInt(CountBoardWaitingForReceptionCenter % showData);
                BagiBoardWaitingForReceptionCenter = parseInt(CountBoardWaitingForReceptionCenter / showData);
                if (ModulusWaitingForReceptionCenter != 0)
                {
                    BagiBoardWaitingForReceptionCenter = BagiBoardWaitingForReceptionCenter + 1;
                }
            }
  
            for(var i= MulaiDataWaitingForReceptionCenter;  i< arrWaitingForReceptionCenter.length; i++){
              var vboard = arrWaitingForReceptionCenter[i];
              boardDataWaitingForReceptionCenter.push(vboard);
            }
  
            BagiBoardWaitingForReceptionCenter = BagiBoardWaitingForReceptionCenter - 1;
  
            if (BagiBoardWaitingForReceptionCenter != 0)
            {
                dataHabisWaitingForReceptionCenter = false;
                MulaiDataWaitingForReceptionCenter = MulaiDataWaitingForReceptionCenter + showData;
            }
            else
            {
                MulaiDataWaitingForReceptionCenter = 0;
                BagiBoardWaitingForReceptionCenter = 0;
                dataHabisWaitingForReceptionCenter = true;
            }
  
            $timeout(function() {
              FuncWaitingForReceptionCenter(boardDataWaitingForReceptionCenter);
            }, 100);



            //   ready for delivery
            if (dataHabisReadyForDelivery == true)
            {
                CountBoardReadyForDelivery = arrReadyForDelivery.length;
                ModulusReadyForDelivery = parseInt(CountBoardReadyForDelivery % showData);
                BagiBoardReadyForDelivery = parseInt(CountBoardReadyForDelivery / showData);
                if (ModulusReadyForDelivery != 0)
                {
                    BagiBoardReadyForDelivery = BagiBoardReadyForDelivery + 1;
                }
            }
  
            for(var i= MulaiDataReadyForDelivery;  i< arrReadyForDelivery.length; i++){
              var vboard = arrReadyForDelivery[i];
              boardDataReadyForDelivery.push(vboard);
            }
  
            BagiBoardReadyForDelivery = BagiBoardReadyForDelivery - 1;
  
            if (BagiBoardReadyForDelivery != 0)
            {
                dataHabisReadyForDelivery = false;
                MulaiDataReadyForDelivery = MulaiDataReadyForDelivery + showData;
            }
            else
            {
                MulaiDataReadyForDelivery = 0;
                BagiBoardReadyForDelivery = 0;
                dataHabisReadyForDelivery = true;
            }
  
            $timeout(function() {
              FuncReadyForDelivery(boardDataReadyForDelivery);
            }, 100);


            //   Waiting for delivery
            if (dataHabisWaitingForDelivery == true)
            {
                CountBoardWaitingForDelivery = arrWaitingForDelivery.length;
                ModulusWaitingForDelivery = parseInt(CountBoardWaitingForDelivery % showData);
                BagiBoardWaitingForDelivery = parseInt(CountBoardWaitingForDelivery / showData);
                if (ModulusWaitingForDelivery != 0)
                {
                    BagiBoardWaitingForDelivery = BagiBoardWaitingForDelivery + 1;
                }
            }
  
            for(var i= MulaiDataWaitingForDelivery;  i< arrWaitingForDelivery.length; i++){
              var vboard = arrWaitingForDelivery[i];
              boardDataWaitingForDelivery.push(vboard);
            }
  
            BagiBoardWaitingForDelivery = BagiBoardWaitingForDelivery - 1;
  
            if (BagiBoardWaitingForDelivery != 0)
            {
                dataHabisWaitingForDelivery = false;
                MulaiDataWaitingForDelivery = MulaiDataWaitingForDelivery + showData;
            }
            else
            {
                MulaiDataWaitingForDelivery = 0;
                BagiBoardWaitingForDelivery = 0;
                dataHabisWaitingForDelivery = true;
            }
  
            $timeout(function() {
              FuncWaitingForDelivery(boardDataWaitingForDelivery);
            }, 100);

      
          });



          // ========================================================================================= end ready for pick up & waiting for reception center (GABUNGAN)







      }  // end loadalldata

      $interval(function() {
        $scope.LoadAllData();
      },120000 );
     

      $scope.refreshData = function() {
        $scope.LoadAllData();
      }
        

        
        

        

        

        

        // // start timeout for create board
        // // menggunakan timeout agar dijalankan setelah screen benar-benar ter-load.
        
        // $timeout(function(){
        //   var vurl = '/api/as/Boards/BP/SatMon';
        //   $http.get(vurl).then(function(res){
        //     updateData(res);
        //     boardLoad();
        //   });
        //   }, 100);

        // var updateData = function(res){
        //   SateliteMonitoring.allData = {
        //     ready_pickup: [],
        //     wait_delivery: [],
        //     ready_delivery: [],
        //     wait_reception: []
        //   }
        //   var vdata = res.data.Result;
        //   for(var i=0; i<vdata.length; i++){
        //     var vitem = vdata[i];
        //     if (vitem.BoardType=='sat_rpu'){
        //       var vnewdata = {chip: 'ready_pickup', branchname: vitem.SateliteName, nopol: vitem.PoliceNumber, tipe: vitem.ModelType, 
        //         startdate: $filter('date')(vitem.WaktuStart, 'yyyy-MM-dd'),
        //         jam_masuk: $filter('date')(vitem.WaktuStart, 'HH:mm'),
        //         sa_name: vitem.SaName,
        //         tgl_masuk: '01/01/2017', statusicon: [],
        //       }
        //       if (vitem.StatusId==2394){
        //         vnewdata.statusicon.push('pickup');
        //       }
        //       SateliteMonitoring.allData['ready_pickup'].push(vnewdata);
        //     } else if (vitem.BoardType=='sat_wdv'){
        //       var vnewdata = {chip: 'wait_delivery', branchname: vitem.SateliteName, nopol: vitem.PoliceNumber, tipe: vitem.ModelType, 
        //         startdate: $filter('date')(vitem.WaktuStart, 'yyyy-MM-dd'),
        //         jam_masuk: $filter('date')(vitem.WaktuStart, 'HH:mm'),
        //         tgl_serah: $filter('date')(vitem.JanjiSerah, 'dd/MM/yyyy HH:mm'),
        //         sa_name: vitem.SaName,
        //         tgl_masuk: '01/01/2017', statusicon: [],
        //       }
        //       SateliteMonitoring.allData['wait_delivery'].push(vnewdata);
        //     } else if (vitem.BoardType=='ctr_wfr'){
        //       var vnewdata = {chip: 'wait_reception', branchname: vitem.SateliteName, nopol: vitem.PoliceNumber, tipe: vitem.ModelType, 
        //         startdate: $filter('date')(vitem.WaktuStart, 'yyyy-MM-dd HH:mm:ss'),
        //         jam_masuk: $filter('date')(vitem.WaktuStart, 'HH:mm'),
        //         //tgl_serah: $filter('date')(vitem.JanjiSerah, 'dd/MM/yyyy HH:mm'),
        //         sa_name: vitem.SaName,
        //         tgl_masuk: '01/01/2017', statusicon: [],
        //       }
        //       SateliteMonitoring.allData['wait_reception'].push(vnewdata);
        //     } else if (vitem.BoardType=='ctr_rdv'){
        //       var vnewdata = {chip: 'ready_delivery', branchname: vitem.SateliteName, nopol: vitem.PoliceNumber, tipe: vitem.ModelType, 
        //         startdate: $filter('date')(vitem.WaktuStart, 'yyyy-MM-dd'),
        //         jam_masuk: $filter('date')(vitem.WaktuStart, 'HH:mm'),
        //         tgl_serah: $filter('date')(vitem.JanjiSerah, 'dd/MM/yyyy HH:mm'),
        //         sa_name: vitem.SaName,
        //         tgl_masuk: '01/01/2017', statusicon: [],
        //       }
        //       SateliteMonitoring.allData['ready_delivery'].push(vnewdata);
        //     }
        //   }

        //   // SateliteMonitoring.allData['ready_pickup'] = [{chip: 'ready_pickup', branchname: 'Pramuka', nopol: 'B 5601 BNC', tipe: 'AVANZA', daydiff: 0,
        //   //   startdate:'2017-12-23', tgl_masuk: '2017-12-23', statusicon: [], sa_name: 'ADI', jam_masuk: '08:16'
        //   // }]

        //   // {chip: 'wait_delivery', branchname: 'Pramuka', nopol: 'B 5601 VNC', tipe: 'AVANZA', daydiff: 0, 
        //   //   tgl_masuk: '01/01/2017', status: 'H+1', statusicon: [], sa_name: 'BUDI', jam_masuk: '08:16',
        //   //   tgl_serah: '01/01/2017', jam_serah: '09:00',
        //   // },          

        //   // {chip: 'ready_delivery', branchname: 'Pramuka', nopol: 'B 5801 FBC', tipe: 'AVANZA', daydiff: 0, 
        //   //   tgl_masuk: '01/01/2017', status: 'H+1', statusicon: [], sa_name: 'ADI', jam_masuk: '08:16',
        //   //   tgl_serah: '01/01/2017', jam_serah: '10:00',
        //   // },          
          
        //   // {chip: 'wait_reception', branchname: 'Pramuka', nopol: 'B 6801 LNC', tipe: 'AVANZA', daydiff: 0, 
        //   //   tgl_masuk: '01/01/2017', status: 'H+1', statusicon: [], sa_name: 'ADI', jam_masuk: '08:16', timewait: 29,
        //   // },          
          

        // }
        // var boardLoad = function() {
        //   //------------------------------------------//
        //   // **** ready to pickup **** //
        //   //------------------------------------------//
        //   var boardConfig = {
        //     type: 'kanban',
        //     chip: {
        //       height: 100,
        //       width: 320,
        //       gap: 2
        //     },
        //     board:{
        //       container: 'satmon-ready_for_pickup',
        //       iconFolder: 'images/boards/iregular',
        //       chipGroup: 'satelitemon',
        //     },
        //     stage: {
        //       maxHeight: allBoardHeight,
        //       minHeight: allBoardHeight,
        //     },
        //     layout: {
        //       chipHeight: 80,
        //       chipGap: 2
        //     }

        //   }

        //   var boardData = SateliteMonitoring.getData('ready_pickup')
        //   // console.log('boardData-readypickup', boardData);

        //   registerBoard({
        //     boardConfig: boardConfig,
        //     boardData: boardData,
        //     incField: 'timewait',
        //     incMode: '',
        //     boardName: 'ready_to_pickup'
        //   })

        //   //------------------------------------------//
        //   // **** waiting for delivery **** //
        //   //------------------------------------------//
        //   var boardConfig = {
        //     type: 'kanban',
        //     chip: {
        //       height: 120,
        //       width: 320,
        //       gap: 2
        //     },
        //     board:{
        //       container: 'satmon-waitingdelivery',
        //       iconFolder: 'images/boards/iregular',
        //       chipGroup: 'satelitemon',
        //     },
        //     stage: {
        //       maxHeight: allBoardHeight,
        //       minHeight: allBoardHeight,
        //     },
        //     layout: {
        //       chipHeight: 80,
        //       chipGap: 2
        //     }

        //   }

        //   var boardData = SateliteMonitoring.getData('wait_delivery')
        //   // console.log('boardData-readypickup', boardData);

        //   registerBoard({
        //     boardConfig: boardConfig,
        //     boardData: boardData,
        //     incField: 'timewait',
        //     incMode: '',
        //     boardName: 'waiting_for_delivery'
        //   })


        //   //------------------------------------------//
        //   // **** Ready for delivery Center **** //
        //   //------------------------------------------//
        //   var boardConfig = {
        //     type: 'kanban',
        //     chip: {
        //       height: 100,
        //       width: 320,
        //       gap: 2
        //     },
        //     board:{
        //       container: 'stmon-readydelivery-center',
        //       iconFolder: 'images/boards/iregular',
        //       chipGroup: 'satelitemon',
        //     },
        //     stage: {
        //       maxHeight: allBoardHeight,
        //       minHeight: allBoardHeight,
        //     },
        //     layout: {
        //       chipHeight: 80,
        //       chipGap: 2
        //     }

        //   }

        //   var boardData = SateliteMonitoring.getData('ready_delivery')
        //   // console.log('boardData-readypickup', boardData);

        //   registerBoard({
        //     boardConfig: boardConfig,
        //     boardData: boardData,
        //     incField: 'timewait',
        //     incMode: '',
        //     boardName: 'ready_for_delivery'
        //   })

        //   //------------------------------------------//
        //   // **** Waiting For Reception Center **** //
        //   //------------------------------------------//
        //   var boardConfig = {
        //     type: 'kanban',
        //     chip: {
        //       height: 100,
        //       width: 320,
        //       gap: 2
        //     },
        //     board:{
        //       container: 'stmon-waitingreception-center',
        //       iconFolder: 'images/boards/iregular',
        //       chipGroup: 'satelitemon',
        //     },
        //     stage: {
        //       maxHeight: allBoardHeight,
        //       minHeight: allBoardHeight,
        //     },
        //     layout: {
        //       chipHeight: 80,
        //       chipGap: 2
        //     }

        //   }

        //   var boardData = SateliteMonitoring.getData('wait_reception')
        //   // console.log('boardData-readypickup', boardData);

        //   registerBoard({
        //     boardConfig: boardConfig,
        //     boardData: boardData,
        //     incField: 'status',
        //     incMode: 'inc',
        //     boardName: 'wait_reception'
        //   })


         

        // }





});
