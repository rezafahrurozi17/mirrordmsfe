angular.module('app')
    .factory('JpcbBpViewFactory', function($http,$q, CurrentUser, $filter, $timeout, jpcbFactory) {
       var currentUser = CurrentUser.user;
        var tmpWorkNames = [{ id: 0, name: 'Stall Body' }, { id: 1, name: 'Stall Putty' }, { id: 2, name: 'Stall Surfacer' }, { id: 3, name: 'Stall Spraying' }, { id: 4, name: 'Stall Polishing' }, { id: 5, name: 'Stall Re-assembly' }, { id: 6, name: 'Stall Final Inspection' }]
        var tmpGetStall = [];
        var tmpSelectedChip = [];
        var vdataTemp = [];
        var vdataTemp2 = []; //buat cek menumpuk kalau di onPlot
        var vdataTemp3 = []; //buat cek menumpuk kalau di extend
        var vgroupidTemp = null;
        var argTemp = null;
        var onFinishTemp = null;
        var dataChipTempSatuGroup = [];
        var baseMerah = 0; //750 kalau mau merah semua;
        var jmlHariMerah = 0;

        var CopyVboard = null;
        var TypeHeader = null;

        var longStart = null;
        var longEnd = null;
       // console.log(currentUser);
       return {
           // hourDelay: 2.2,
           // minuteDelay: 6*60+0,
           // minuteDelay: 0,
           // onPlot: function(){
           //     // do nothing
           // },
           //boardName: 'jpcbviewboard',
           headerItems: {},
           lastIndex: 990,
           arrIndex: {},
           getIndexById: function(vid){
               if (typeof this.arrIndex[vid] !== 'undefined'){
                   return this.arrIndex[vid];
               } else {
                   this.arrIndex[vid] = this.lastIndex;
                   this.lastIndex = this.lastIndex+1;
                   return this.arrIndex[vid];
               }
           },
           selectedChip: function(param) {
                tmpSelectedChip = angular.copy(param);
                return tmpSelectedChip;
            },
           sendChiptoFront: function(data){
            console.log("dataaaaaaaaaaaaaaaaa",data);
            var defer = $q.defer();
            var tmpData = [];
            if(data.length > 0){
              for(var i in data){
                tmpData.push(data[i]);
              }
            }
            // var tmpData = [{
            //   id:1,name:"tes"
            // }];
            defer.resolve(tmpData);
            console.log("defer",defer,tmpData)
            return defer.promise;
            
           },
           loadChipData: function(data){
             var res =  $http.get(data);
             return res
           },
           checkStatuStall: function(datachip) {
                if (datachip.nopol === tmpSelectedChip.nopol) {
                    if (tmpGetStall.length > 0) {
                        var workname = _.find(tmpWorkNames, { 'id': (datachip.ChipType - 1) });
                        var result = _.find(tmpGetStall, { 'title': workname.name });
                        if (result !== undefined) {
                            return result.idx;
                        } else {
                            return null
                        }
                        // console.log("================>",workname);
                        // console.log("================>",tmpWorkNames);
                        // console.log("================>",datachip.ChipType);
                        // console.log("================>",workname.name);

                    }
                    // console.log('1 ====>',datachip.ChipType,tmpWorkNames);
                    // var result = _.find(tmpWorkNames, { 'id': (datachip.ChipType - 1) });
                    // console.log("workname result", result,tmpGetStall);
                    // if(tmpGetStall.length>0){
                    //     var tes = _.find(tmpGetStall, { 'idx': result.id });
                    //     console.log("workname anjir", tes)
                    //     return tes.idx
                    // }
                }
            },
           showBoard: function(vname, config){
               console.log("vnameeee",vname);
               console.log("configgggg",config);
               console.log('cek sini long start', config)
                var tempLongStart = config.currentChip.longStart.split(':');
                this.longStart = tempLongStart[0]+':'+tempLongStart[1];
                this.longEnd = config.currentChip.longEnd;
               if (config.incrementMinute !== undefined ){
                   this.incrementMinute = config.incrementMinute;
               }
               if (!config.options){
                   config.options = {}
               }
               var vcontainer = config.container;
               //var vname = this.boardName;
               var vobj = document.getElementById(vcontainer);
               if(vobj){
                   var vwidth = vobj.clientWidth;
                   // console.log(vwidth);
                   console.log('known obj', vwidth);
                   if (vwidth<=0) {
                       return;
                   }
               } else {
                   console.log('unknown obj');
                   return;
               }
               if (typeof config.onPlot=='function'){
                   this.onPlot = config.onPlot;
               }

               var vdurasi = 1;
               if (typeof config.currentChip.durasi !=='undefined'){
                   vdurasi = config.currentChip.durasi;
               }
               config.currentChip.width = (vdurasi/60)*this.incrementMinute;

               var vrow = -2;
               if (typeof config.currentChip.stallId !=='undefined'){
                   if (config.currentChip.stallId>0){
                       vrow = config.currentChip.stallId - 1 ;
                   }
               }
               config.currentChip.row = vrow;

               var vhourStart = JsBoard.Common.getMinute(this.startHour)/60;
               var vcol = 0;
               if (typeof config.currentChip.startTime !=='undefined'){
                   vstartTime = config.currentChip.startTime;
                   vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
               }
               config.currentChip.col = vcol;
               
               var voptions = {
                   boardName: vname,
                   // board: {
                   //     chipGroup: 'jpcb_bp',
                   //     columnWidth: 140,
                   //     rowHeight: 75,
                   //     footerChipName: 'footer',
                   //     headerChipName: 'jpcb_header',
                   //     firstColumnWidth: 220,
                   //     stopageChipTop: 90,
                   // },
                   board: {
                       chipGroup: 'jpcb_bp',
                       columnWidth: 140,
                       rowHeight: 75,
                       footerChipName: 'footer',
                       headerChipName: 'jpcb_header',
                       firstColumnWidth: 150,
                       stopageChipTop: 90,
                       snapDivider: 4
                   },
                   chip: {
                       startHour: this.startHour,
                       incrementMinute: this.incrementMinute
                   }
               }
               xvoptions = JsBoard.Common.extend(voptions, config.options);
               xvoptions.board = JsBoard.Common.extend(voptions.board, config.options.board);
               jpcbFactory.createBoard(vname, xvoptions);
               var me = this;
               jpcbFactory.setHeader(vname, {
                   headerHeight: 40,
                   onGetHeader: function(arg, vfunc){
                       return me.onGetHeader(arg, vfunc);
                   }
               });
               jpcbFactory.setStall(vname, {
                   //firstColumnWidth: 120,
                   onGetStall: function(arg, vfunc){
                       return me.onGetStall(arg, vfunc);
                   }
               });

               jpcbFactory.setOnGetChips(vname, function(arg, vfunc){
                   return me.onGetChips(arg, vfunc);
               });

               jpcbFactory.setOnChipLoaded(vname, function(vjpcb){
                   return me.onChipLoaded(vjpcb);
               });
               
               jpcbFactory.setOnBoardReady(vname, function(vjpcb){
                   return me.onBoardReady(vjpcb);
               });
               
               // jpcbFactory.setSelectionArea(vname, {tinggiArea: 4, fixedHeaderChipName: 'blank'});
               jpcbFactory.setSelectionArea(vname, {
                   tinggiArea: 0,
                   tinggiHeader: 40,
                   tinggiSubHeader: 40,
                   fixedHeaderChipName: 'blank',
               });


               if (config.options.stopageAreaHeight){
                   jpcbFactory.setStopageArea(vname, {tinggiArea: config.options.stopageAreaHeight});
               }
               

               jpcbFactory.setArgChips(vname, config.currentChip);
               jpcbFactory.setArgStall(vname, config.currentChip);
               jpcbFactory.setArgHeader(vname, config.currentChip);

               if (config.onChipClick){
                   jpcbFactory.setOnCreateChips(vname, function(vchip){
                       vchip.on('click', config.onChipClick);
                   });
               }

               // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

               // if (config.standardDraggable){
               //     jpcbFactory.setOnCreateChips(vname, function(vchip){
               //         if (vchip.data.nopol=='B 7002 AA'){
               //             if (false){

               //             }
               //         }
               //         var vdrag = false;
               //         if ((typeof vchip.data !== 'undefined') && (typeof vchip.container.layout!=='undefined')){
               //             var vlayout = vchip.container.layout;
               //             if (vchip.data.col>=vlayout.timeLine){
               //                 vdrag = true;
               //             }
               //         }
               //         vchip.draggable(vdrag);
                       
               //         // if (vchip.data.selected){
               //         //     vchip.draggable(true);
               //         // }
               //     });
               // }

               var me = this;
               var the_config = config;
               // jpcbFactory.setOnDragDrop(vname, function(vchip){
               //     var curDate = config.currentChip.currentDate;
               //     var vdurasi = 1;
               //     if (typeof config.currentChip.durasi !=='undefined'){
               //         vdurasi = config.currentChip.durasi;
               //     }

               //     var vmin = me.incrementMinute * vchip.data['col'];
               //     var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
               //     var vres = JsBoard.Common.getTimeString(vminhour);
               //     var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
               //     startTime.setMinutes(startTime.getMinutes()+vmin);

               //     var emin = vmin + vdurasi * 60;
               //     var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
               //     endTime.setMinutes(endTime.getMinutes()+emin);

               //     vchip.data.startTime = startTime;
               //     vchip.data.endTime = endTime;
               //     var vStallId = 0;
               //     if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
               //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
               //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
               //         vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
               //     }
               //     vchip.data.stallId = vStallId;

               //     if (typeof the_config.onPlot=='function'){
               //         the_config.onPlot(vchip);
               //     }
               //     // if (typeof me.onPlot=='function'){           

               //     //     me.onPlot(vchip.data);
               //     // }
               //     // console.log("on drag-drop vchip", vchip);
               // });

               jpcbFactory.showBoard(vname, vcontainer)
               // $timeout(function(){
               //     jpcbFactory.showBoard(vname, vcontainer)


               // }, 100);

           },
           // startHour: '08:00',
            // endHour: '17:00',
           startHour: this.longStart,
           endHour: this.longEnd,
           breakHour: '',
           incrementMinute: 60,
        //    jmlHariTps: 1,
           jmlHariTps: 1,           
           jmlHariLight: 7,
           jmlHariMedium: 14,
           jmlHariHeavy: 30,
           timeToColumn: function(vName, vDate, vtime){
               // var vboard = jpcbFactory.getBoard(vName);
               // var vcolData = vboard.layout.board.headerItems;
               var vcolData = this.headerItems[vName];
               var vresult = 0;
               if (vtime>'18:00'){
                   vtime = '18:00';
               }
               console.log('vcoldata masa kosong', vcolData)
               for (var i=0; i<vcolData.length; i++){
                   var vitem = vcolData[i];
                   var xdate = $filter('date')(vitem.dateTime, 'yyyy-MM-dd');
                   if (xdate==vDate){
                       
                    //    var vstartmin = JsBoard.Common.getMinute(this.startHour);
                       var vstartmin = JsBoard.Common.getMinute(this.longStart);
                       var vmin = JsBoard.Common.getMinute(vtime)-vstartmin;
                       if (vmin<0){
                           vmin = 0;
                       }
                       var vresult = i+vmin/this.incrementMinute;
                       return vresult;
                   }
               }

               
               return vresult;
           },
           columnToTime: function(vname, vcolumn, vdelta){
               if (typeof vdelta =='undefined'){
                   vdelta = 0;
               }
               var vboard = jpcbFactory.getBoard(vname);
               console.log(vboard);
               var vcolData = vboard.layout.board.headerItems;
               var vcol = Math.floor(vcolumn-vdelta);
               var xcol = vcolumn-vcol;
               var vbase = null;
               vbase = new Date(vcolData[vcol].dateTime);
               var vmin = this.incrementMinute*xcol;
               if (vmin>0){
                   vbase.setMinutes(vbase.getMinutes()+vmin);
               }
               return vbase;




               // var vstartmin = JsBoard.Common.getMinute(this.startHour);
               // var vmin = vcolumn * this.incrementMinute + vstartmin;
               // var vresult = JsBoard.Common.getTimeString(vmin);

               // return vresult;
           },
           checkAppointmentStatus: function(vtime){
               // vtime: time-nya data
               // currentHour: jam/minute saat ini
               var vDate = this.getCurrentTime();
               // console.log("current Time#####", vDate.getTime());
               var currentHour = $filter('date')(vDate, 'HH:mm');

               var vcurrmin = JsBoard.Common.getMinute(currentHour);
               var vmin = JsBoard.Common.getMinute(vtime);
               if (vmin<vcurrmin){
                   return 'no_show';
               }
               return 'appointment';

           },
           onGetHeader: function(arg, onFinish){
               vtype = 'tps';
               varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
               var vWorkHour = JsBoard.Common.generateDayHours({
                   startHour: this.startHour,
                   endHour: this.endHour,
                   increment: this.incrementMinute,
               });
               // var result = [];
               // for(var idx in vWorkHour){
               //     var vitem = {title: vWorkHour[idx]}
               //     if (vWorkHour[idx]==this.breakHour){
               //         vitem.colInfo = {isBreak: true}
               //     }
               //     result.push(vitem)

               // }
               // // console.log("header", result);
               var vDate = new Date($filter('date')(arg.currentDate, 'yyyy-MM-dd'));
               result = this.getHeader(vtype, vDate);
               this.headerItems[arg.boardName] = result;
               $timeout(function(){
                   onFinish(result);
               }, 10);
               
           },
           onGetStall: function(arg, onFinish){
               // $http.get('/api/as/Stall').
               // var vurl = '/api/as/Boards/Stall/0';
               var vurl = arg.stallUrl;
               // var vrandom = Math.random()*10000+'';
               // if (vurl.indexOf('?')>=0){
               //     vurl = vurl+'&_xrnd='+vrandom;
               // } else {
               //     vurl = vurl+'?_xrnd='+vrandom;
               // }
               
               $http.get(vurl, {arg: arg}).then(function(res){
                   if (typeof arg.stallVisible=='undefined'){
                       arg.stallVisible = {}
                   }
                   tmpGetStall = [];
                   var xres = res.data.Result;
                   var result = [];

                    var startBreak4Val = 0;
                    var durationBreak4Val = baseMerah * jmlHariMerah;
                   for(var i=0;  i<xres.length; i++){
                       var vitem = xres[i];
                       if (typeof arg.stallVisible[vitem.StallId]=='undefined'){
                           arg.stallVisible[vitem.StallId] = {visible: true, title: vitem.Name};
                       }
                       if(arg.stallVisible[vitem.StallId].visible){
                           var vtech = '';
                           if (vitem.Initial1){
                               vtech = vtech+vitem.Initial1;
                           }
                           if (vitem.Initial2){
                               vtech = vtech+', '+vitem.Initial2;
                           }
                           if (vitem.Initial3){
                               vtech = vtech+', '+vitem.Initial3;
                           }
                        //    result.push({
                        //        title: vitem.Name,
                        //        status: 'normal',
                        //        stallId: vitem.StallId,
                        //        // technician: 'X-'+vitem.StallId,
                        //        technician: vtech,
                        //    });
                        result.push({
                                title: vitem.Name,
                                status: 'normal',
                                stallId: vitem.StallId,
                                // technician: 'X-'+vitem.StallId,
                                technician: vtech,
                                // startBreak4: 0,
                                // durationBreak4: 0,
                                startBreak4: startBreak4Val,
                                durationBreak4: durationBreak4Val,

                            });
                            tmpGetStall.push({
                                title: vitem.Name,
                                status: 'normal',
                                stallId: vitem.StallId,
                                idx: i,
                                // technician: 'X-'+vitem.StallId,
                                technician: vtech,
                                // startBreak4: 0,
                                // durationBreak4: 0,
                                startBreak4: startBreak4Val,
                                durationBreak4: durationBreak4Val,

                            });
                       }
                   }
                   onFinish(tmpGetStall);
               })

           },
           findStallIndex: function(vStallId, vList){
               for (var i=0; i<vList.length; i++){
                   var vitem = vList[i];
                   if ((typeof vitem.stallId!=='undefined') && (vitem.stallId==vStallId)){
                       return i;
                   }
               }
               return -1;
           },
           reformatTime: function(vTime){
               if (vTime){
                   return JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vTime));
               }
               return '';
           },
           checkStatus: function(vitem){
               var vstatus = vitem.Status;
               var vresult = '';
               if (vstatus<=2){
                   vresult = 'belumdatang';
               } else if (vstatus==3){
                   vresult = 'sudahdatang';
               } else if (vstatus == 4 || vstatus == 8 || vstatus == 11) {
                    if (vitem.isAppointment) {
                        vresult = 'booking';
                    } else {
                        vresult = 'walkin';
                    }
               }
                else if ((vstatus >= 5) || (vstatus <= 11)){
                   vresult = 'inprogress';
                    if (vstatus == 6) {
                        vresult = 'irregular';
                    }    
                } else if (vstatus >= 12 && vstatus != 22) {       // ditambah && != 22 karena job dispatch ada status nya 22
                    vresult = 'done';
                } else if (vstatus == 22) {                      // ditambah else if 22 karena kl job dispatch ga ngerubah warna garis chip.
                    if (vitem.isAppointment) {
                        vresult = 'booking';
                    } else {
                        vresult = 'walkin';
                    }
                }

               // 5. Problem
               //    a. waktu awal < sekarang tapi status < 5 and status>=3

               // var vNow = this.getCurrentTime();
               // var xStart = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd')+' '+vitem.PlanStart;
               // var xFinish = $filter('date')(vitem.PlanDateFinish, 'yyyy-MM-dd')+' '+vitem.PlanFinish;
               // var xNow = $filter('date')(vNow, 'yyyy-MM-dd HH:mm:ss');
               // console.log("start", xStart);
               // console.log("finish", xFinish);
               // console.log("Now", xNow);
               // if ((xStart<xNow) && (vstatus<5) && (vstatus>=3)){
               //     vresult = 'irregular';
               // }

               // //    b. ketika status belum done tapi jam akhir < jam sekarang
               // if ((xFinish<xNow) && (vstatus<17) && (vstatus>=3)){
               //     vresult = 'irregular';
               // }
               
               if (jpcbFactory.isJobProblem(vitem)){
                   vresult = 'irregular';
               }
               
               return vresult;
           },
           updateStatusIcons: function(vdata){
               var vresult = []
               // customer waiting
               // is waiting 0 waiting 1 drop off
               if (vdata.isWaiting) {
                   vresult.push('customer');
               }
               if (vdata.isWaitingPart) {
                    vresult.push('waitparts');
                }  
               // in progress
               // 5 = Clock ON *
               // 6 = Clock Pause 
               // 7 = Clock resume
               // 8 = Clock Off * 
               // 9 = Qc start *
               // 10 = Qc finish *
                // if ((vdata.intStatus==5) || (vdata.intStatus==7) || (vdata.intStatus==9)){
                //     vresult.push('inprogress');
                // }
                if ((vdata.intStatus == 1) || (vdata.intStatus == 3) || (vdata.intStatus == 5)) {
                    vresult.push('inprogress');
                }

                if (vdata.intStatus==6){
                    vresult.push('box');
                }
                // multiple task
                if (vdata.jmltask > 1) {
                    vresult.push('multitask');
                }

                // Check Status Chip, is Paused ?
                if (vdata.intStatus == 2) {
                    vresult.push('box');
                }


                // Warranty
                if (vdata.wocategoryname == 'TWC' || vdata.wocategoryname == 'PWC') {
                    if (vdata.wocategoryname == 'TWC') {
                        vresult.push('TWC');
                    } else {
                        vresult.push('PWC');
                    }
                }

                // RTJ
                if (vdata.wocategoryname == 'RTJ') {
                    vresult.push('returnjob');
                }

                if(vdata.isCarryOver == 1){
                    vresult.push('carryover')
                }


               // multiple task
               // if (vdata.jmltask>1){
               //     vresult.push('multitask');
               // }


               vdata.statusicon = vresult;
           },
           onChipLoaded: function(vjpcb){
               // vjpcb.allChips = []
               // return;
               var vresult = [];
               console.log('onchipLoaded chip oi', vjpcb.allChips);
               for (var i=0; i<vjpcb.allChips.length; i++){
                   var vitem = vjpcb.allChips[i];
                   if (vitem.stallId==-2){
                       vitem.row = -2;
                       vresult.push(vitem);
                   } else if (vitem.stallId==-1){
                       vitem.row = -1;
                       vresult.push(vitem);
                   } else {
                       vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                       if (vidx>=0){
                           vitem.row = vidx;
                           vresult.push(vitem);
                       }
                   }
                   
               }
               vjpcb.allChips = vresult;
           },
           onBoardReady: function(vjpcb){
               this.updateTimeLine(vjpcb.boardName);
               if (vjpcb.onJpcbReady){
                   vjpcb.onJpcbReady(vjpcb);
               }
           },
        //    getDayCountByType: function(vtype){
        //        if (vtype=='light'){
        //            return this.jmlHariMedium;
        //        } else if (vtype=='medium'){
        //            return this.jmlHariMedium;
        //        } else if (vtype=='light'){
        //            return this.jmlHariLight;
        //        }
        //        return this.jmlHariTps;
        //    },
        getDayCountByType: function(vtype) {
                //tps 3 hari, light 7hari, medium 30hari, heavy 90hari
                console.log('jumlah hari Board', vtype)
                if (vtype == 'light') {
                    jmlHariMerah = this.jmlHariLight;
                    return this.jmlHariLight;
                } else if (vtype == 'medium') {
                    jmlHariMerah = this.jmlHariMedium;
                    return this.jmlHariMedium;
                } else if (vtype == 'heavy') {
                    jmlHariMerah = this.jmlHariHeavy;
                    return this.jmlHariHeavy;
                } else if (vtype == 'tps'){
                    jmlHariMerah = this.jmlHariTps;
                    return this.jmlHariTps;
                } else {
                    jmlHariMerah = this.jmlHariTps;
                    return this.jmlHariTps;
                }
                
            },
           onGetChips: function(arg, onFinish){
               console.log('arg', arg);
                argTemp = arg;
                onFinishTemp = onFinish;
                console.log('onFinish', onFinish);
            //    ===============
               var vtype = 'tps';
               var vcount = this.getDayCountByType(vtype);
               var vArg = {nopol: 'B 1000 AXA', tipe: 'Avanza'}
               vArg = JsBoard.Common.extend(vArg, arg);
               
               var tglAwal = arg.currentDate;
               var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
               tglAkhir.setDate(tglAkhir.getDate()+vcount-1);
               var vjobid = vArg.JobId;
               if (!vjobid){
                   vjobid = 0;
               }
               var vgroupid = vArg.groupId
               // var vurl = '/api/as/Boards/BP/JobList/'+vjobid+'/'+vgroupid+'/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/'+$filter('date')(tglAkhir, 'yyyy-MM-dd');
               var vurl = arg.chipUrl;
               var workNames = {1: 'Body', 2: 'Putty', 3: 'Surfacer', 4: 'Spray', 5: 'Polish', 6: 'Reassembly', 7: 'Final Insp'}
               var me = this;
               var vboardName = arg.boardName;
               $http.get(vurl).then(function(res){
                   var result = [];
                   var xres = res.data.Result;
                   // me.sendChiptoFront(xres);
                   var vNoShow = 0;
                   var vBooking = 0;
                   for(var i=0;  i<xres.length; i++){
                       var vitem = xres[i];
                       var vstatus = 'no_show';
                       var vstatus = me.checkAppointmentStatus(vitem.PlanStart);
                       // var vicon = 'customer';
                       // if (vstatus=='appointment'){
                       //     vicon = 'customer_gray';
                       // }

                       if ((vitem.TimeDuration != vitem.ActualChipTimeDuration) && (vitem.ActualChipTimeDuration <= vitem.TimeDuration) && (vitem.Status == 6)){
                            if (vitem.ActualChipTimeDuration > 0){
                                vitem.TimeDuration = vitem.ActualChipTimeDuration
                            }
                        }

                       var vtime = vitem.PlanStart;
                       var vdate = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd');
                       var vEndDate = new Date(vitem.PlanDateStart);
                       var vTimeStart = JsBoard.Common.getMinute(vitem.PlanStart);
                       var vMinutes = (vitem.TimeDuration+vitem.Extension)*60;
                       var vMinStart = JsBoard.Common.getMinute(me.startHour);
                       vEndDate.setMinutes(vEndDate.getMinutes()+vTimeStart+vMinutes);
                       console.log('apakah kemari')
                       var vcolumn = me.timeToColumn(vboardName, vdate, vtime);
                       if (vitem.PoliceNumber){
                           vitem.nopol = vitem.PoliceNumber;
                       } else {
                           vitem.nopol = '#'+vitem.JobId;
                       }
                       vstatus = me.checkStatus(vitem);
                    //    var vdata = {
                    //        chip: 'jpcb2', 
                    //        jobId: vitem.JobId,
                    //        AppointmentTime: vitem.PlanStart,
                    //        jamcetakwo: me.reformatTime(vitem.PlanStart),
                    //        // jamjanjiserah: $filter('date')(vitem.JanjiSerah, 'hh:mm'),
                    //        jamjanjiserah: $filter('date')(vitem.PlanStart, 'hh:mm'),
                    //        tgljanjiserah: $filter('date')(vitem.PlanDateFinish, 'dd/MM/yyyy'),
                    //        nopol: vitem.nopol, 
                    //        type: vitem.ModelType,
                    //        dontDrag: 1, 
                    //        normalDuration: '01:00',
                    //        normal: vitem.TimeDuration * 60 / me.incrementMinute,
                    //        ext: vitem.Extension * 60 / me.incrementMinute,
                    //        endDate: vEndDate,
                    //        stallId: vitem.StallId,
                    //        row: 0, col: vcolumn, 
                    //        width: vitem.TimeDuration * 60/me.incrementMinute, 
                    //        color_idx : me.getIndexById(vitem.JobId),
                    //        daydiff: 0, 
                    //        status: vstatus,
                    //        intStatus: vitem.Status,
                    //        teknisi: vitem.Teknisi,
                    //        pekerjaan: vitem.JenisPekerjaan,
                    //        jmltask: vitem.jmltask,
                    //        allocated: ((vitem.jmltask>=1)?1:0),
                    //        chipType: vitem.ChipType,
                    //        workname: vitem.ChipType+': '+workNames[vitem.ChipType],
                    //        additionalJob: vitem.additionalJob,
                    //        statusicon: []
                    //    }
                    var vDontDrag = 0;
                        if (typeof vitem.PauseDraggable != 'undefined') {
                            if ((vitem.Status == 6) && (vitem.PauseDraggable == 0)) {
                                vDontDrag = 1;
                            }
                        }

                        if (typeof vitem.PauseDraggable != 'undefined') {
                            if ((vitem.Status == 5) && (vitem.PauseDraggable == 0)) {
                                vDontDrag = 1;
                            }
                        }

                        if(vitem.isDispatched == 1 && vitem.Status != 2){
                            vDontDrag = 1;

                        }
                    var vdata = {
                            chip: 'jpcb2',
                            ChipGroupId: vitem.ChipGroupId,
                            jobId: vitem.JobId,
                            jobtaskbpid: vitem.JobTaskBpId,
                            AppointmentTime: vitem.PlanStart,
                            jamcetakwo: me.reformatTime(vitem.PlanStart),
                            jamjanjiserah: $filter('date')(vitem.JanjiSerah, 'hh:mm'),
                            tgljanjiserah: $filter('date')(vitem.JanjiSerah, 'dd/MM/yyyy'),
                            nopol: vitem.nopol,
                            type: vitem.ModelType,
                            normalDuration: '01:00',
                            normal: vitem.TimeDuration * 60 / me.incrementMinute,
                            ext: vitem.Extension * 60 / me.incrementMinute,
                            endDate: vEndDate,
                            stallId: vitem.StallId,
                            row: 0,
                            col: vcolumn,
                            width: vitem.TimeDuration * 60 / me.incrementMinute,
                            color_idx: me.getIndexById(vitem.JobId),
                            daydiff: 0,
                            status: vstatus,
                            intStatus: vitem.Status,
                            dontDrag: vDontDrag,
                            teknisi: vitem.Teknisi,
                            pekerjaan: vitem.JenisPekerjaan,
                            jmltask: vitem.jmltask,
                            allocated: ((vitem.jmltask >= 1) ? 1 : 0),
                            chipType: vitem.ChipType,
                            workname: vitem.ChipType + ': ' + workNames[vitem.ChipType],
                            additionalJob: vitem.additionalJob,
                            tmpStall: me.checkStatuStall(vitem),
                            wocategoryname: vitem.WoCategoryName,
                            isCarryOver: vitem.isCarryOver,
                            isDispatched: vitem.isDispatched,
                            // tmpSuggestStall:me.checkStatuStall(),
                            statusicon: []
                        };

                        console.log('vdata sebelum checking', vdata)
                       var isPush = true;
                    //    if((vitem.Status==1) || (vitem.Status==2)){
                    //        isPush = false;
                    //        vNoShow = vNoShow+1;
                    //    }
                    //    if ((vitem.isAppointment) && (vitem.Status>=3)){
                    //        vBooking = vBooking+1;
                    //    }
                    //    if ((vitem.isActive) && (vitem.Status==8)){
                    //        vdata.waiting_fi = 1;
                    //    } else {
                    //        vdata.waiting_fi = 0;
                    //    }

                       if (vitem.Status == 1) {
                            // isPush = false;
                            vNoShow = vNoShow + 1;
                        }
                        if (vitem.Status == 2) {
                            isPush = true;
                            vdata.dontDrag = 0;
                        }
                        if ((vitem.isAppointment) && (vitem.Status >= 3)) {
                            vBooking = vBooking + 1;
                        }
                        if ((vitem.isActive) && (vitem.Status == 8)) {
                            vdata.waiting_fi = 1;
                        } else {
                            vdata.waiting_fi = 0;
                        }

                       me.updateStatusIcons(vdata);
                       // vdata.width = vdata.normal+vdata.ext;
                       // vdata.width = 1;
                       if (vdata.width<=0){
                           vdata.width = 1;
                       }
                       
                    //    if (!vitem.isAllocated){
                    //        vdata.row = -2;
                    //        vdata.stallId = -2;
                    //    } else {
                    //        if (vdata.stallId==0){
                    //            vdata.row = -1;
                    //            vdata.stallId = -1;
                    //        }
                    //    }
                    //    if ((vdata.stallId!==0) && isPush){
                    //        result.push(vdata);

                    //    }
                       // console.log("vdata", vdata);
                       // break;
                        //    ====================
                        if (!vitem.isAllocated) {
                            vdata.row = -2;
                            vdata.stallId = -2;
                        } else {
                            console.log('kapan kesini nya?')
                            if (vdata.stallId == 0) {
                                console.log('kapan kesini nya1')
                                    //pake yg -1 jd ke jobstoppage
                                vdata.row = -1;
                                vdata.stallId = -1;
                                vdata.width = 1;
                                vdata.tgljanjiserah = ''

                            } else if (vdata.stallId == -2) {
                                console.log('kapan kesini nya2')
                                vdata.row = -2;
                                vdata.stallId = -2;
                            }
                        }
                        if ((vdata.stallId !== 0) && isPush) {
                            if (vdata.width > 0) {
                                console.log("vdata=======>", vdata)
                                    // ============ TERUSAN ======
                                me.updateChipBreak(vdata);
                                result.push(vdata);
                                vdataTemp.push(vdata);
                                vdataTemp2.push(vdata);
                                vdataTemp3.push(vdata);
                                console.log('vdatatemp push', vdataTemp);
                                console.log('vdatatemp2 push', vdataTemp2);
                                console.log('vdatatemp3 push', vdataTemp3);

                                console.log('res ini apa', result);
                            }

                        }
                   }
                   //=============cobain cek menumpuk extend ==========
                   console.log("tmpGetStall extend", tmpGetStall);
                   var vdataTemp3Copy = angular.copy(vdataTemp3);
                   var menumpukExt = 0;
                   console.log('vdatatemp3 ext', vdataTemp3);
                   console.log('vdatatemp3Copy ext', vdataTemp3Copy);

                   var stallIdMerah = []

                   for (var i = 0; i < vdataTemp3.length; i++) {

                       for (var j = 0; j < vdataTemp3Copy.length; j++) {
                           if ((vdataTemp3[i].nopol != vdataTemp3Copy[j].nopol) &&
                               (vdataTemp3[i].chipType == vdataTemp3Copy[j].chipType) &&
                               (vdataTemp3[i].ChipGroupId == vdataTemp3Copy[j].ChipGroupId) &&
                               (vdataTemp3[i].stallId > 0) && (vdataTemp3Copy[j].stallId > 0) &&
                               (vdataTemp3[i].ChipGroupId != null) && (vdataTemp3Copy[j].stallId != null)) {

                               var startActiveChip = vdataTemp3[i].col;
                               var endActiveChip = vdataTemp3[i].col + vdataTemp3[i].width;

                               var startPlottedChip = vdataTemp3Copy[j].col;
                               var endPlottedChip = vdataTemp3Copy[j].col + vdataTemp3Copy[j].width;

                               // console.log('startActiveChip', startActiveChip)
                               // console.log('endActiveChip', endActiveChip)
                               // console.log('startPlottedChip', startPlottedChip)
                               // console.log('endPlottedChip', endPlottedChip)
                               // console.log('masuk if tumpiuk', vdataTemp3[i]);



                               //cek apakah plotted chip ada di tengah active chip
                               if ((startActiveChip <= startPlottedChip) && (endActiveChip > startPlottedChip)) {
                                   // console.log('ini chip yg numpuk1', vdataTemp3[i]);
                                   // console.log('ini chip yg numpuk1', vdataTemp3Copy[j]);
                                   stallIdMerah.push(vdataTemp3[i].stallId);
                                   menumpukExt = 1;
                               }

                               // cek apakah active chip ada di tengah plotted chip
                               if ((startPlottedChip <= startActiveChip) && (endPlottedChip > startActiveChip)) {
                                   // console.log('ini chip yg numpuk2', vdataTemp3[i]);
                                   // console.log('ini chip yg numpuk2', vdataTemp3Copy[j]);
                                   stallIdMerah.push(vdataTemp3[i].stallId);
                                   menumpukExt = 1;
                               }
                           }
                       }
                       console.log('menumpukExt', menumpukExt);
                       console.log('stallIdMerah', stallIdMerah);



                   }
                   if (stallIdMerah.length > 0) {
                       for (var i = 0; i < tmpGetStall.length; i++) {
                           for (var j = 0; j < stallIdMerah.length; j++) {
                               if (tmpGetStall[i].stallId == stallIdMerah[j]) {
                                   tmpGetStall[i].durationBreak4 = 750 * jmlHariMerah;
                               }
                           }
                       }
                   } else {
                       for (var i = 0; i < tmpGetStall.length; i++) {
                           tmpGetStall[i].durationBreak4 = 0 * jmlHariMerah;
                       }
                   }
                    //===================================
                   arg.countNoShow = vNoShow;
                   arg.countBooking = vBooking;
                   console.log("INI DATA CHIPNYA",result)
                   onFinish(result);
               })
               

           },

           onGetChipsYY: function(arg, onFinish){
               var vArg = {nopol: 'B 1000 AXA', tipe: 'Avanza'}
               vArg = JsBoard.Common.extend(vArg, arg);
               
               var tglAwal = arg.currentDate;
               var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
               tglAkhir.setDate(tglAkhir.getDate());
               // var vurl = arg.chipUrl;
               var vurl = '/api/as/Boards/jpcb/bydate/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/0/17';
               var me = this;
               $http.get(vurl).then(function(res){
                   // onFinish([]);
                   // return;
                   // var result = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, 
                   //     daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}];
                   var result = [];
                   
                   onFinish(result);

               });

           },
           getCurrentTime: function(){
               return jpcbFactory.getCurrentTime();
           },
           getScrollablePos: function(boardname){
               return jpcbFactory.getScrollablePos(boardname);
           },
           updateChipStatus: function(boardname){
               // var boardname = this.boardName;
               var me = this;
               var vboard = jpcbFactory.getBoard(boardname);
               jpcbFactory.processChips(boardname, function(vchip){
                   return true;
               }, function(vchip){
                   var vitem = vchip.data;
                   var vstatus = me.checkAppointmentStatus(vitem.AppointmentTime);
                   if (vitem.status !== vstatus){
                       vitem.status = vstatus;
                       vboard.updateChip(vchip);
                   }
                   if (vitem.status=='no_show'){
                       vchip.draggable(true);
                   }
               });

           },
           updateTimeLine: function(boardname){
            //    var vDate = this.getCurrentTime();
            //    var xDate = $filter('date')(vDate, 'yyyy-MM-dd');
            //    var xTime = $filter('date')(vDate, 'HH:mm:ss');
            //    console.log('ataukah kemari')
            //    var vline = this.timeToColumn(boardname, xDate,  xTime);
            //    var vboard = jpcbFactory.getBoard(boardname);
            //    vboard.layout.updateTimeLine(vboard.board, vline);
            //    jpcbFactory.updateStandardDraggable(boardname);
               
            //    jpcbFactory.redrawBoard(boardname);

                var vDate = this.getCurrentTime();
                var tempDate = new Date();
                var tempEndDate = angular.copy(this.longEnd);
                var tempEndDate2 = tempEndDate.split(':')
                var maxVDate = new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate(), tempEndDate2[0], tempEndDate2[1], 0);
                if (vDate > maxVDate){
                    vDate = new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate(), parseInt(tempEndDate2[0])+1, tempEndDate2[1], 0);
                }
                var xDate = $filter('date')(vDate, 'yyyy-MM-dd');
                var xTime = $filter('date')(vDate, 'HH:mm:ss');
                console.log('trap 1', xTime);
                console.log('trap 111', this.longEnd)
                var vline = this.timeToColumn(boardname, xDate, xTime);
                var vboard = jpcbFactory.getBoard(boardname);
                vboard.layout.updateTimeLine(vboard.board, vline);
                jpcbFactory.updateStandardDraggable(boardname);

                jpcbFactory.redrawBoard(boardname);
          },
           xupdateTimeLine: function(boardname){
               // var boardname = this.boardName;
               var vDate = this.getCurrentTime();
               // console.log("current Time#####", vDate.getTime());
               var currentHour = $filter('date')(vDate, 'HH:mm');
               
               // console.log("currentHour", currentHour);
               //var startHour = this.
               //var minutPerCol

               jpcbFactory.updateTimeLine(boardname, currentHour, this.startHour, this.incrementMinute);

               //this.updateChipStatus();
               
               jpcbFactory.updateStandardDraggable(boardname);
               
               jpcbFactory.redrawBoard(boardname);
           },
           getBoard: function(boardname){
               return jpcbFactory.getBoard(boardname);
           },

           getHeader: function(vtype, vparam1, vparam2){

               //return [{text: '1'}, {text: '2'}, {text: '3'}, {text: '4'}]
               // type: 'hourly'; param1: start date, param2: date count
               // type: 'daily'; param1: start date, param2: date count
               // type: 'weekly'; param1: start date, param2: date count
               if (vtype.toLowerCase()=='tps'){
                   if (typeof vparam1 == 'undefined'){
                       vparam1 = new Date();
                   }
                   if (typeof vparam2 == 'undefined'){
                       vparam2 = this.jmlHariTps;
                   }
                   var vres = this.getHourlyList(vparam1, vparam2);
                   return vres;
               } else if (vtype.toLowerCase()=='light'){
                   if (typeof vparam1 == 'undefined'){
                       vparam1 = new Date();
                   }
                   if (typeof vparam2 == 'undefined'){
                       vparam2 = this.jmlHariLight;
                   }
                   var vres = this.getDailyList(vparam1, vparam2);
                   console.log("daily list", vres);
                   return vres;
               } else if (vtype.toLowerCase()=='medium'){
                   if (typeof vparam1 == 'undefined'){
                       vparam1 = new Date();
                   }
                   if (typeof vparam2 == 'undefined'){
                       vparam2 = this.jmlHariMedium;
                   }
                   var vres = this.getDailyList(vparam1, vparam2);
                   console.log("daily list", vres);
                   return vres;
               } else if (vtype.toLowerCase()=='heavy'){
                   if (typeof vparam1 == 'undefined'){
                       vparam1 = new Date();
                   }
                   if (typeof vparam2 == 'undefined'){
                       vparam2 = this.jmlHariHeavy;
                   }
                   var vres = this.getDailyList(vparam1, vparam2);
                   console.log("daily list", vres);
                   return vres;
               }
           }, 
           getWorkHour: function(){
               return JsBoard.Common.generateDayHours({
                //    startHour: '08:00',
                //    endHour: '17:00',
                    startHour: this.longStart,
                    endHour: this.longEnd,
                   increment: 60,
               })
           },
           getHourlyList: function(startDate, vcount){
               var breakHour = '12:00';
               var result = []
               var vidx = 0;
               var vWorkHour = this.getWorkHour();
               var item = false
               var vDate = startDate;
               var currentDate = '******';
               for (var i=0; i<vcount; i++){
                   var newDay = true;
                   var subCount = vWorkHour.length;
                   currentDate = $filter('date')(vDate, 'dd MMMM yyyy');
                   for(var idx in vWorkHour){
                       var xDate = new Date($filter('date')(vDate, 'yyyy-MM-dd')+' '+vWorkHour[idx]);
                       if (newDay){
                           item = {newDay: newDay, parentText: currentDate, subCount: subCount, text: vWorkHour[idx], dateTime: xDate}
                       } else {
                           item = {newDay: newDay, text: vWorkHour[idx], dateTime: xDate}
                       }
                       if (vWorkHour[idx]==breakHour){
                           item.colInfo = {isBreak: true}
                       }
                       result.push(item);
                       var newDay = false;
                   }
                   vDate.setDate(vDate.getDate()+1);
               }
               return result;
           },
           dayDiff: function(date1, date2){
               return Math.round((date2 - date1) / (1000*60*60*24));
           },
           loadMainData: function(config, onFinish){
               var BpCategory = {}
               var BpGroupList = [] 
               var loadedStatus = [false, false];
               var updateLoadedData = function(){
                   if (loadedStatus[0] && loadedStatus[1]){
                       onFinish(BpCategory, BpGroupList);
                   }
               }
               
               $http.get(config.urlCategoryBp).then(function(res){
                   var xres = res.data.Result;
                //    var result = {};
                    var result = [];
                   for(var i=0;  i<xres.length; i++){
                       var vitem = xres[i];
                    //    result[vitem.MasterId] = {
                    //        id: vitem.MasterId,
                    //        name: vitem.Name,
                    //        type: vitem.Name
                    //    }
                         result.push({
                            id: vitem.MasterId,
                            name: vitem.Name,
                            type: vitem.Name
                        })
                   }
                   BpCategory = result;
                   loadedStatus[0] = true;
                   updateLoadedData();
               });
           
               $http.get(config.urlGroupList).then(function(res){
                   var xres = res.data.Result;
                   var result = [];
                   var idx = 0;
                   for(var i=0; i<xres.length; i++){
                       var vitem = xres[i];
                       result.push({
                           name: vitem.GroupName,
                           type: vitem.BpCategory,
                           count: vitem.TaskCount,
                           BpCategoryId: vitem.BpCategoryId,
                           Id:vitem.Id
                       });
                       idx = idx+1;
                   }
                   BpGroupList = result;
                   loadedStatus[1] = true;
                   updateLoadedData();
               });
               
               
           },
           timeToColumn2: function(vtime) {
                // var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vstartmin = JsBoard.Common.getMinute(this.longStart);
                var vmin = JsBoard.Common.getMinute(vtime) - vstartmin;
                var vresult = vmin / this.incrementMinute;

                return vresult;
            },
           getBreakTime: function() {
                var vstartMin = JsBoard.Common.getMinute(this.breakHour);
                var vendMin = vstartMin + 60;
                var vBreakEnd = JsBoard.Common.getTimeString(vendMin);
                var vstart = this.timeToColumn2(this.breakHour);
                var vend = this.timeToColumn2(vBreakEnd);

                var arrayBreak = [];
                // jeda antar istirahat = (break hour - start hour) + (end hour - break hour) + 1
                var JedaAntarBreak = 0;
                for (var i = 0; i < jmlHariMerah; i++) {
                    var breakXX = {};
                    breakXX = {
                        start: 4,
                        end: 5,
                        width: vend - vstart,
                    }
                    arrayBreak.push(breakXX);
                    JedaAntarBreak = 10; // (12:00 - 08:00) + (17:00 - 12:00) + 1

                }
                console.log('daftar break bos', arrayBreak)
                return arrayBreak;
                // return [{
                //     start: vstart,
                //     end: vend,
                //     width: vend - vstart,
                // }]
            },
           getBreakChipTime: function(vstart, vlength) {
                console.log("getBreakChipTime", vstart, vlength);
                var vbreaks = this.getBreakTime();
                console.log("vbreaks", vbreaks);
                var vbreakCount = 0;
                var vtotalBreak = 0;
                var vresult = {}
                for (var i = 0; i < vbreaks.length; i++) {
                    vbreak = vbreaks[i];
                    // (x1 <= y2) && (y1 < x2)
                    if (this.isOverlapped2(vstart, vstart + vlength, vbreak.start, vbreak.end)) {
                        console.log("===============>", vstart, vbreak.start);
                        if (vstart > vbreak.start) {
                            return 'start-inside-break';
                        }
                        vbreakCount = vbreakCount + 1;
                        vresult['break' + vbreakCount + 'start'] = vbreak.start - vstart;
                        vresult['break' + vbreakCount + 'width'] = vbreak.width;
                        vtotalBreak = vtotalBreak + vbreak.width;
                    }
                }
                if (vbreakCount == 0) {
                    return 'no-break';
                } else {
                    vresult['breakWidth'] = vtotalBreak;
                }
                return vresult;

            },
           updateChipBreak: function(vchipData) {
                console.log('vchipdata anj', vchipData);
                if (vchipData.breakWidth) {
                    vchipData.width = vchipData.width - vchipData.breakWidth;
                }
                var vbreaks = 'no-break';
                // if (vchipData.stallId != 0) {
                if (vchipData.stallId > 0) {
                    vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width);
                }

                var deleteList = ['break1start', 'break1width', 'break2start', 'break2width', 'break3start', 'break3width', 'break4start', 'break4width', 'breakWidth'];
                for (var i = 0; i < deleteList.length; i++) {
                    var idx = deleteList[i];
                    delete vchipData[idx];
                }
                if (typeof vbreaks == 'object') {
                    for (var idx in vbreaks) {
                        vchipData[idx] = vbreaks[idx];
                    }
                }

                if (vchipData.breakWidth) {
                    vchipData.width = vchipData.width + vchipData.breakWidth;
                    vchipData.width1 = vchipData.break1start;
                    vchipData.start2 = vchipData.break1start + vchipData.break1width;
                    vchipData.width2 = vchipData.width - vchipData.start2;
                } else {
                    vchipData.width1 = vchipData.width;
                }
                // console.log('vchipData', vdata);

                // console.log('vbreaks', vbreaks);
                //////////
            },
            isOverlapped: function(x1, x2, y1, y2) {
                return (x1 < y2) && (y1 < x2)
            },
            isOverlapped2: function(x1, x2, y1, y2) {
                return (x1 <= y2) && (y1 < x2)
            },
            isOverlapped3: function(x1, x2, y1, y2) {
                return (x1 < y2) && (y1 <= x2)
            },

            getTypeHeader: function(TypeHeaderParam){
                if (TypeHeaderParam == null || TypeHeaderParam == '' || TypeHeaderParam == undefined){
                    TypeHeader = 'tps';
                } else {
                    TypeHeader = TypeHeaderParam.toLowerCase();
                }
            }

           
       }
    });
