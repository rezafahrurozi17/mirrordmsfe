angular.module('app')
    .controller('JpcbBpViewController', function($rootScope,$interval,JpcbBpFactory,JpcbGrViewFactory, $scope, $http, CurrentUser, Parameter,$timeout, $window, $filter, $state, JpcbBpViewFactory, ngDialog, Idle) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        // ================
        $scope.JpcbBpData = {screen_step:1}
        $scope.JpcbBpData.startDate = new Date();
        $scope.JpcbBpData.showProdBoard = false;
        $scope.JpcbBpData.currentGroup = true;
        $scope.jpcbDataChip = [];
        $scope.teamSelected = {};
        $scope.bpCategorySelected = {};
        $scope.groupBpId = null;

        var vconfig = {
            urlCategoryBp: '/api/as/Boards/MGlobal/1012',
            urlGroupList: '/api/as/Boards/BP/GroupList/0',
            tpsLigthOnly: false
        }

        $scope.JpcbBpData.cuarrentDate = new Date();
        var boardName = 'jpcbbp-prod-container2';
        $scope.updateTeam = function(vteam){
            console.log('team', vteam);
            // this.team = vteam;
            $scope.teamSelected = vteam;
            if ($scope.bpCategorySelected !== null && $scope.teamSelected !== null){
                // this.reloadBoard();
                _.map($scope.BpGroupList,function(val){
                    val.visible = 0;
                })
                var tmpIdx = _.findIndex($scope.BpGroupList,{Id:$scope.teamSelected.Id})
                if(tmpIdx !== -1){
                    $scope.BpGroupList[tmpIdx].visible = 1;
                    $scope.groupBpId = $scope.BpGroupList[tmpIdx].Id;
                }
                $scope.counting = 0
                $scope.jpcbgrview.reloadBoard();
            }
        }
        // ================
        var showBoardJPCB = function(paramGroupId,paramCategoryId){
            if ($scope.JpcbBpData.currentGroup){
                detailJobData = {}
                var vscrollhpos = JpcbBpViewFactory.getScrollablePos(boardName);
                if(paramGroupId == null || paramGroupId == undefined){
                    paramGroupId =1;
                }
                if(paramCategoryId == null || paramCategoryId == undefined){
                    paramCategoryId = 1;
                }
                var vitem = {
                    // mode: $scope.jpcbgrview.showMode,
                    boardName: boardName,
                    currentDate: $scope.JpcbBpData.cuarrentDate,
                    //stallUrl: '/api/as/Boards/Stall/2',
                    // stallUrl: '/api/as/Boards/BP/Stall/'+1,
                    stallUrl: '/api/as/Boards/BP/Stall/'+ $scope.groupBpId,
                    // stallUrl: '/api/as/Boards/BP/Stall/'+$scope.JpcbBpData.currentGroup.GroupBPId,
                    groupId: paramGroupId,
                    //chipUrl: '/api/as/Boards/jpcb/bydate/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/0/17',
                    // chipUrl: '/api/as/Boards/BP/WoList',
                    // ==============
                    // chipUrl:'/api/as/Boards/jpcb/GetJpcbBPViewByTypeAndGroup/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/0/17/62/'+paramGroupId,
                    // ==============
                    chipUrl:'/api/as/Boards/BP/GetBPJobListNoJobId/'+paramGroupId+'/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/'+paramCategoryId,
                    // chipUrl:'/api/as/Boards/jpcb/GetJpcbBPViewByTypeAndGroup/{tanggal}/{stawal}/{stakhir}/{IdJobType}/{IdGroup}'
                    // nopol: $scope.jpcbgrview.nopol,
                    // tipe: $scope.jpcbgrview.tipe
                    JobId: 0,
                    longStart: $scope.startlongDay,
                    longEnd:$scope.endlongDay,
                }
                console.log("showBoardJPCB");
                JpcbBpViewFactory.showBoard(boardName, {
                    container: 'jpcbbp-prod-container2', 
                    incrementMinute: 60,
                    currentChip: vitem,
                    options: {
                        standardDraggable: true,
                        stopageAreaHeight: 200,
                        //testing12345: 1000,
                        onJpcbReady: function(vjpcb){
                            console.log("*****board ready", vjpcb);
                            $scope.isLoading = false;
                            $scope.countNoShow = vjpcb.argChips.countNoShow;
                            $scope.countBooking = vjpcb.argChips.countBooking;
                            // alert('ready');
                        },
                        board:{
                            scrollableLeft: vscrollhpos,
                            footerChipName: 'footer_stopage',
                            firstColumnChipName: 'jpcb_stall',
                            firstHeaderChipName: 'jpcb_first_header',
                            fixedHeaderChipName: 'jpcb_fixed_header',
                        },
                        chip: {
                            // stallTechWidth: 60,
                            teamName: $scope.JpcbBpData.currentGroup.name,
                        }
                    },
                    // onChipClick: function(vchip){
                        
                    //     console.log('on chip click!!', vchip);
                    //     // $scope.showChipInfo(true);

                    //     displayChipData(vchip.currentTarget.data.jobId, vchip.currentTarget.data);

                    //     //$scope.displayedChip = vchip.currentTarget.data;
                    //     //$scope.$apply();
                    // },
                });
            }
            // ===================================
            // var tmpHeightCanvas = $('#jpcbbp-prod-container2').height()
            // $('#labelForGroup').css('height',tmpHeightCanvas);
            // ==================================
        }
        
        var isBoardLoaded = false;
        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd-MM-yyyy',
            // disableWeekend: 1
        };
        
        // $scope.jpcbgrview = {currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza"}
        $scope.jpcbgrview = {currentDate: new Date()}
        //$scope.jpcbgrview.currentDate.setHours($scope.asb.currentDate.getHours()+1);
        // console.log("$scope.jpcbgrview.currentDate", $scope.jpcbgrview.currentDate);
        // console.log("$scope.dateOptions", $scope.dateOptions);
        $scope.jpcbgrview.startDate = new Date();

        $scope.jpcbgrview.stallVisible = {}
        $scope.jpcbgrview.tempStallVisible = {}
        $scope.BpCategory = {}
        $scope.BpGroupList = []
        $scope.jpcbgrview.testing = "TESTING";

        var vlong1 = 'col-xs-9';
        var vlong2 = 'col-xs-12';
        $scope.isLoading = true;
        $scope.wcolumn = vlong2;
        $scope.ncolumn = 'col-xs-3';
        $scope.ncolvisible = 0;
        

        // $scope.jpcbgrview.endDate = new Date();
        // $scope.jpcbgrview.endDate.setDate($scope.jpcbgrview.endDate.getDate()+7);
        // var allBoardHeight = 500;

        // $scope.$watch('jpcbgrview.showMode', function(newValue, oldValue) {
        //     if (newValue !== oldValue) {
        //         console.log('Changed! '+newValue);
        //         $scope.jpcbgrview.reloadBoard();
        //     }
        // });

        var scrollable_hpos = 0;


    $scope.user = CurrentUser.user();
    // console.log("CurrentUser.user()", CurrentUser.user());

    var vmsgname = 'joblist';
    var voutletid = $scope.user.OrgId;
    var vgroupname = voutletid+'.'+vmsgname;
    var vappname = vmsgname;
    try {
        $rootScope.ChatProxy.invoke('joinRoom', vgroupname);
    } catch (exc){
        console.log(exc);
        // $scope.showMsg = {message: 'Tidak dapat terhubung dengan SignalR server.', title: 'Koneksi SignalR gagal.', closeText: 'Close', exception: exc}
        // ngDialog.openConfirm ({
        //     width: 700,
        //     template: 'app/boards/factories/showMessage.html',
        //     plain: false,
        //     scope: $scope,
        // })
        
    }

    $scope.$on('SR-'+vappname, function(e, data){
        console.log("notif-data", data);
        console.log("Coba yeee", data.catitem.name)
        var vparams = data.Message.split('.');

        var vJobId = 0;
        if (typeof vparams[0] !== 'undefined'){
            var vJobId = vparams[0];
        }

        var vStatus = 0;
        if (typeof vparams[1] !== 'undefined'){
            var vStatus = vparams[1];
        }


        var jobid = vparams[0];
        var vcounter = vparams[1];
        var vJobList = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "20"];
        var xStatus = vStatus+'';

        if (vJobList.indexOf(xStatus)>=0){
            showBoardJPCB();
            // $scope.cekGroup(1);
            $scope.cekGroup(0,0);

        }
        
    });
    
    $scope.jpcbgrview.reloadBoard = function(){
        // me = this;
        // // (this.BpCategory && this.team)
        // var vitem = {
        //     // mode: $scope.jpcbgrview.showMode,
        //     boardName: boardName,
        //     currentDate: $scope.JpcbBpData.cuarrentDate,
        //     //stallUrl: '/api/as/Boards/Stall/2',
        //     // stallUrl: '/api/as/Boards/BP/Stall/'+1,
        //     stallUrl: '/api/as/Boards/BP/Stall/'+ $scope.groupBpId,

        //     // stallUrl: '/api/as/Boards/BP/Stall/'+$scope.JpcbBpData.currentGroup.GroupBPId,
        //     // groupId: this.BpCategory.id,
        //     groupId: $scope.teamSelected.Id,
        //     //chipUrl: '/api/as/Boards/jpcb/bydate/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/0/17',
        //     // chipUrl: '/api/as/Boards/BP/WoList',
        //     // ==============
        //     // chipUrl:'/api/as/Boards/jpcb/GetJpcbBPViewByTypeAndGroup/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/0/17/62/'+paramGroupId,
        //     // ==============
        //     chipUrl:'/api/as/Boards/BP/GetBPJobListNoJobId/'+$scope.teamSelected.Id+'/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/'+$scope.bpCategorySelected.id,
        //     // chipUrl:'/api/as/Boards/jpcb/GetJpcbBPViewByTypeAndGroup/{tanggal}/{stawal}/{stakhir}/{IdJobType}/{IdGroup}'
        //     // nopol: $scope.jpcbgrview.nopol,
        //     // tipe: $scope.jpcbgrview.tipe
        //     JobId: 0,
        // }
        // var vscrollhpos = JpcbBpViewFactory.getScrollablePos(boardName);
        //     $timeout(function(){
        //         me.changeOption();
        //         JpcbBpViewFactory.showBoard($scope.jpcbgrview,{
        //             container: 'jpcbbp-prod-container2', 
        //             incrementMinute: 60,
        //             currentChip: vitem,
        //             options: {
        //                 standardDraggable: true,
        //                 stopageAreaHeight: 200,
        //                 //testing12345: 1000,
        //                 onJpcbReady: function(vjpcb){
        //                     console.log("*****board ready", vjpcb);
        //                     $scope.isLoading = false;
        //                     $scope.countNoShow = vjpcb.argChips.countNoShow;
        //                     $scope.countBooking = vjpcb.argChips.countBooking;
        //                     // alert('ready');
        //                 },
        //                 board:{
        //                     scrollableLeft: vscrollhpos,
        //                     footerChipName: 'footer_stopage',
        //                     firstColumnChipName: 'jpcb_stall',
        //                     firstHeaderChipName: 'jpcb_first_header',
        //                     fixedHeaderChipName: 'jpcb_fixed_header',
        //                 },
        //                 chip: {
        //                     // stallTechWidth: 60,
        //                     teamName: $scope.JpcbBpData.currentGroup.name,
        //                 }
        //             }})
        //     }, 200);
        //     var keyTmpData = [];
        //     $scope.tmpDataTarget = [];
        //     JpcbBpViewFactory.loadChipData(vitem.chipUrl).then(function(res){
        //         if(res.data.Result.length > 0){

        //             var tmpjpcbDataChip = [];
        //             // var tmpjpcbDataChip = res.data.Result;
        //             console.log('res boko', res)
        //             for(var k in res.data.Result){
        //                 tmpjpcbDataChip.push(res.data.Result[k]);
        //             }
        //             console.log('tmpjpcbDataChip boko', tmpjpcbDataChip)

        //             if(tmpjpcbDataChip.length > 0){
        //                 $scope.jpcbDataChip = _.groupBy(tmpjpcbDataChip,'PoliceNumber');
        //                 keyTmpData = Object.keys($scope.jpcbDataChip);
        //                 for(var i in keyTmpData){
        //                     console.log('bor',$scope.jpcbDataChip);
        //                     console.log('bor',keyTmpData);
        //                     $scope.tmpDataTarget.push({
        //                         name:keyTmpData[i],
        //                         process:$scope.filterDataProcess($scope.jpcbDataChip[keyTmpData[i]])
        //                     })
        //                 }
        //             }
        //             $timeout(function(){
        //                 JpcbBpViewFactory.updateTimeLine(boardName);
        //             },500);
        //             console.log("$scope.jpcbDataChip",$scope.tmpDataTarget,$scope.jpcbDataChip,$scope.keyTmpData);
        //         }
        //     });

        JpcbBpFactory.getListStall($scope.groupBpId).then(function(res){
            console.log('masuk lagu1')
            var xres = res.data.Result;
            var vstart = '';
            var vend = '';
            for(var i=0;  i<xres.length; i++){
                var vitem = xres[i];
                // === NEW CODE Before GO LIVE
                if (!vitem.DefaultOpenTime) {
                    vitem.DefaultOpenTime = '08:00';
                }
                if (typeof vitem.DefaultOpenTime !== 'undefined') {

                    //cek defaultopen time menit nya 00 ga? kl ga 00 set jd 00 biar header nya menit nya 00
                    var cekDefaultOpenTime = vitem.DefaultOpenTime.split(':');
                    if (cekDefaultOpenTime[1].toString() != '00'){
                        vitem.DefaultOpenTime = cekDefaultOpenTime[0] + ':' + '00' + ':' + '00'
                    }

                    // if (vitem.DefaultOpenTime){
                    if (vstart === '') {
                        vstart = vitem.DefaultOpenTime;
                    } else {
                        if (vstart > vitem.DefaultOpenTime) {
                            vstart = vitem.DefaultOpenTime;
                        }
                    }
                    // }
                }
                if (vitem.DefaultCloseTime == undefined) {
                    vitem.DefaultCloseTime = '17:00';
                }
                if (typeof vitem.DefaultCloseTime !== 'undefined') {
                    if (vitem.DefaultCloseTime >= '23:59') {
                        vitem.DefaultCloseTime = '24:00';
                    }
                    if (vend === '') {
                        vend = vitem.DefaultCloseTime;
                    } else {
                        if (vend < vitem.DefaultCloseTime) {
                            vend = vitem.DefaultCloseTime;
                        }
                    }
                }
            }
            if (vstart === '') {
                vstart = '08:00';
            }
            if (vend === '') {
                vend = '17:00';
            }
            var vxmin = JsBoard.Common.getMinute(vend);
            vend = JsBoard.Common.getTimeString(vxmin);
            // me.startHour = vstart;
            $scope.startlongDay = vstart;
            $scope.endlongDay = vend;

            
        });

        showBoardJPCB($scope.teamSelected.Id,$scope.bpCategorySelected.id);
            // showBoardJPCB(1);
            var urlBoii ='/api/as/Boards/BP/GetBPJobListNoJobId/'+$scope.teamSelected.Id+'/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/'+$scope.bpCategorySelected.id;            
            var keyTmpData = [];
            $scope.tmpDataTarget = [];
            JpcbBpViewFactory.loadChipData(urlBoii).then(function(res){
                if(res.data.Result.length > 0){

                    var tmpjpcbDataChip = [];
                    // var tmpjpcbDataChip = res.data.Result;
                    console.log('res boko', res)
                    for(var k in res.data.Result){
                        if (res.data.Result[k].Status == 0 || res.data.Result[k].Status == 1){
                            tmpjpcbDataChip.push(res.data.Result[k]);
                        }
                    }
                    console.log('tmpjpcbDataChip boko', tmpjpcbDataChip)

                    if(tmpjpcbDataChip.length > 0){
                        $scope.jpcbDataChip = _.groupBy(tmpjpcbDataChip,'PoliceNumber');
                        keyTmpData = Object.keys($scope.jpcbDataChip);
                        for(var i in keyTmpData){
                            console.log('bor',$scope.jpcbDataChip);
                            console.log('bor',keyTmpData);
                            $scope.tmpDataTarget.push({
                                name:keyTmpData[i],
                                process:$scope.filterDataProcess($scope.jpcbDataChip[keyTmpData[i]])
                            })
                        }
                    }
                    
                    console.log("$scope.jpcbDataChip",$scope.tmpDataTarget,$scope.jpcbDataChip,$scope.keyTmpData);
                }
            });
       
    }

        // untuk menampilkan garis waktu saat ini (timeline)
        var timeActive = true;
        var minuteTick = function(){
            if (!timeActive){
                return;
            }
            JpcbBpViewFactory.updateTimeLine(boardName);
            $timeout( function(){
                minuteTick();
            }, 60000);
        }
        
        // $scope.$on('$viewContentLoaded', function() {
        //     //$scope.asb.reloadBoard = function(){
        //         $timeout( function(){
        //             var vobj = {
        //                 mode: $scope.jpcbgrview.showMode,
        //                 currentDate: $scope.jpcbgrview.currentDate,
        //                 nopol: $scope.jpcbgrview.nopol,
        //                 tipe: $scope.jpcbgrview.tipe
        //             }
        //             showBoardJPCB(vobj);

        //             $timeout( function(){
        //                 minuteTick();
        //             }, 100);
                    
        //         }, 100);
        //     //}
        // });

        $scope.$on('$viewContentLoaded', function() {


            //Menggunakan resize detector untuk mengatasi bug board tidak muncul setelah menu di-close dan diopen lagi.
            // elemen yang dimonitor adalah panel yang width-nya 100%
            var velement = document.getElementById('jpcb_gr_view_panel');
            var erd = elementResizeDetectorMaker();
            erd.listenTo(velement, function(element) {
                if (!isBoardLoaded){
                    isBoardLoaded = true;
                    JpcbBpViewFactory.loadMainData(vconfig, function(vBpCategory, vBpGroupList){
                        vBpCategory.push({id : 0, name : 'ALL' , type: 'ALL' })
                        console.log("vBpCategory ======>",vBpCategory);
                        // ====================
                        $scope.BpCategory = vBpCategory;
                        // =============
                        $scope.BpGroupList = vBpGroupList;
                        _.map($scope.BpGroupList, function(val) {
                            val.visible = 0;
                        });
                        $scope.BpGroupList[0].visible = 1;
                        var param = [];
                        angular.forEach(vBpCategory, function(cat){
                                 angular.forEach(vBpGroupList, function(group){
                                        param.push({idtype : cat.id, idgroup : group.name  })
                                 })
                        })
                        console.log("param", param);
                        console.log("BpCategory", vBpCategory);
                        console.log("BpGroupList", vBpGroupList);
                        $scope.groupBpId = vBpGroupList[0].Id; // di kasi [0] karena biara awal load ambil group yg awal

                        JpcbBpFactory.getListStall($scope.groupBpId).then(function(res){
                            console.log('masuk lagu1')
                            var xres = res.data.Result;
                            var vstart = '';
                            var vend = '';
                            for(var i=0;  i<xres.length; i++){
                                var vitem = xres[i];
                                // === NEW CODE Before GO LIVE
                                if (!vitem.DefaultOpenTime) {
                                    vitem.DefaultOpenTime = '08:00';
                                }
                                if (typeof vitem.DefaultOpenTime !== 'undefined') {
                
                                    //cek defaultopen time menit nya 00 ga? kl ga 00 set jd 00 biar header nya menit nya 00
                                    var cekDefaultOpenTime = vitem.DefaultOpenTime.split(':');
                                    if (cekDefaultOpenTime[1].toString() != '00'){
                                        vitem.DefaultOpenTime = cekDefaultOpenTime[0] + ':' + '00' + ':' + '00'
                                    }
                
                                    // if (vitem.DefaultOpenTime){
                                    if (vstart === '') {
                                        vstart = vitem.DefaultOpenTime;
                                    } else {
                                        if (vstart > vitem.DefaultOpenTime) {
                                            vstart = vitem.DefaultOpenTime;
                                        }
                                    }
                                    // }
                                }
                                if (vitem.DefaultCloseTime == undefined) {
                                    vitem.DefaultCloseTime = '17:00';
                                }
                                if (typeof vitem.DefaultCloseTime !== 'undefined') {
                                    if (vitem.DefaultCloseTime >= '23:59') {
                                        vitem.DefaultCloseTime = '24:00';
                                    }
                                    if (vend === '') {
                                        vend = vitem.DefaultCloseTime;
                                    } else {
                                        if (vend < vitem.DefaultCloseTime) {
                                            vend = vitem.DefaultCloseTime;
                                        }
                                    }
                                }
                            }
                            if (vstart === '') {
                                vstart = '08:00';
                            }
                            if (vend === '') {
                                vend = '17:00';
                            }
                            var vxmin = JsBoard.Common.getMinute(vend);
                            vend = JsBoard.Common.getTimeString(vxmin);
                            // me.startHour = vstart;
                            $scope.startlongDay = vstart;
                            $scope.endlongDay = vend;
                
                            showBoardJPCB();
                            // $scope.cekGroup(1);
                            $scope.cekGroup(0,0);                        
                            minuteTick();
                            
                        });

                        
                    });
                    // untuk menampilkan timeline
                    // $timeout( function(){
                    //     minuteTick();
                    // }, 1000);
                }
                vboard = JpcbBpViewFactory.getBoard(boardName);
                if (vboard){
                    vboard.autoResizeStage();
                    vboard.redraw();
                }

            });
            Idle.unwatch();
            console.log('==== value of idle ==', Idle)

        });


        $scope.$on('$destroy', function() {
            timeActive = false;
            Idle.watch();
        });


        var loadStallVisible = function(){
            for (var stallid in $scope.jpcbgrview.stallVisible){
                var vitem = $scope.jpcbgrview.stallVisible[stallid];
                $scope.jpcbgrview.tempStallVisible[stallid] = {visible: vitem.visible, title: vitem.title}                
            }
        }
        var updateStallVisible = function(){
            for (var stallid in $scope.jpcbgrview.tempStallVisible){
                var vitem = $scope.jpcbgrview.tempStallVisible[stallid];
                $scope.jpcbgrview.stallVisible[stallid] = {visible: vitem.visible, title: vitem.title}                
            }
        }

        $scope.showOptions = function(vmode){
            if (vmode){
                loadStallVisible();
                $scope.wcolumn = vlong1;
                $scope.ncolumn = 'col-xs-3';
                $scope.ncolvisible = 1;                
            } else {
                $scope.wcolumn = vlong2;
                $scope.ncolvisible = 0;
            }
        }
        $scope.jpcbgrview.updateCategory = function(vcategory){
            console.log('category', vcategory);
            $scope.bpCategorySelected = vcategory;
            if ($scope.bpCategorySelected !== null && $scope.teamSelected !== null){
                _.map($scope.BpGroupList,function(val){
                    val.visible = 0;
                })
                var tmpIdx = _.findIndex($scope.BpGroupList,{Id:$scope.teamSelected.Id})
                if(tmpIdx !== -1){
                    $scope.BpGroupList[tmpIdx].visible = 1;
                    $scope.groupBpId = $scope.BpGroupList[tmpIdx].Id;
                }
                $scope.counting = 0;
                $scope.jpcbgrview.reloadBoard();
            }
        }
        $scope.jpcbgrview.changeOption = function(){
            var vmode = $scope.bpCategorySelected.type.toLowerCase();
            var vcount = 7;
            if (vmode=='tps'){
                vcount = 7;
                $scope.jpcbgrview.columnWidth = 24;
            } else if (vmode=='light'){
                vcount = 7;
                $scope.jpcbgrview.columnWidth = 24;
            } else if (vmode=='medium'){
                vcount = 14;
                $scope.jpcbgrview.columnWidth = 120;
            } else if (vmode=='heavy'){
                vcount = 30;
                $scope.jpcbgrview.columnWidth = 40;
            }
            var vDate = new Date();
            vDate.setDate($scope.jpcbgrview.startDate.getDate()+vcount);
            $scope.jpcbgrview.endDate = vDate;
            // $scope.jscb.endDate.setDate($scope.jscb.startDate.getDate()+vcount);
        }
        // var vshowOptions = function(){
        //     ngDialog.openConfirm ({
        //       width: 400,
        //       template: 'app/boards/jpcbgrview/options.html',
        //       plain: false,
        //       showClose: false,
        //       controller: 'JpcbGrViewController',
        //       //scope: $scope
        //     });

        // }
        // $scope.showOptions = function(){
        //     loadStallVisible();
        //     this.jpcbgrview.displayOptions = 1;
        //     // var vboard = JpcbGrViewFactory.getBoard(boardName);
        //     // console.log(vboard);
        //     // this.testing = "TESTING!!!";
        //     //this.$apply();
        //     // $timeout(function(){
        //     //     vshowOptions();
        //     // })
        // }

        // $scope.asb.FullScreen = function(){
        //     // alert('full screen');
        //     var vobj = document.getElementById('asb_estimasi_view');
        //     vobj.style.cssText = 'position: absolute; left: 0; top: 0; width: 100%; height: 100%;';
        // }
        

        // angular.element($window).bind('resize', function () {
        //     //AsbEstimasiFactory.resizeBoard();
        //     // AsbProduksiFactory.resizeBoard();
             
        //     // jpcbStopage.autoResizeStage();
        //     // jpcbStopage.redraw();
        // });
        var vNow = $filter('date')(new Date(), 'yyyy-MM-dd');
        var urlTechnician = '/api/as/Boards/jpcb/employee/'+vNow+'/TGR';
        $scope.ListTechnician = [];
        $http.get(urlTechnician).then(function(resp){
            var vdata = resp.data.Result;
            $scope.countTechnician = vdata.length;
            for (var i=0; i<vdata.length; i++){
                var vitem = vdata[i];
                //var vname = getPeopleName(vitem);
                $scope.ListTechnician.push({id: vitem.EmployeeId, name: vitem.EmployeeName});
            }
        });
        $scope.TimeForClock = "";
        $scope.checkTime = function(i) {
            if (i < 10) { i = "0" + i }; // add zero in front of numbers < 10
            return i;
        }
        // $scope.counting = 1
        $scope.counting = 0
        $scope.countingParent = 0;
        $interval(function() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            h = $scope.checkTime(h);
            m = $scope.checkTime(m);
            s = $scope.checkTime(s);
            $scope.TimeForClock = h + ":" + m + ":" + s;
            $scope.counting++
            $scope.cekGroup($scope.counting,$scope.countingParent);

        }, 60000);
        var workNames = {1: 'Body', 2: 'Putty', 3: 'Surfacer', 4: 'Spray', 5: 'Polish', 6: 'Reassembly', 7: 'Final Insp'}
        var workNamesUndefined = {1: ''}
        $scope.filterDataProcess = function(data){
            console.log("filterDataProcess data",data);
            var tmpIdx = 0;
            var tmpLength = data.length;
            var filterDataProcess = _.find(data,function(val){
                return val.Status == 0 || val.Status == 1;
            }
                // { 'Status': 0 }
            );
            console.log("filterDataProcess",filterDataProcess);
            if (filterDataProcess == undefined){
                return workNamesUndefined[1]
            } else {
                return workNames[filterDataProcess.ChipType];
            }
            // for(var i in data){
            //     // if(data[i].Status == 0){
            //     //     if(tmpLength){

            //     //     }
            //     //     // var filterDataProcess = _.find(data, { 'Status': 0 });

            //     //     console.log("filterDataProcess",filterDataProcess);
            //     // }else{

            //     // }
            // }
            
            return
        }
        $scope.cekGroup = function(counting,countingParent){
            console.log("counting",counting,countingParent);
            _.map($scope.BpGroupList, function(val) {
                val.visible = 0;
            });
            if(counting == $scope.BpGroupList.length){
                // $scope.counting = 1;
                $scope.counting = 0;
                $scope.countingParent += 1;
                $scope.cekGroup(0,$scope.countingParent);
                return false
                // clearInterval($interval);
            }
            if(countingParent == $scope.BpCategory.length && counting == $scope.BpGroupList.length ){
                $scope.counting = 0;
                $scope.countingParent = 0;
                $scope.cekGroup(0,0);
            }
            console.log("$scope.counting",$scope.counting);
            console.log("$scope.BpGroupList",$scope.BpGroupList);
            console.log("$scope.BpCategory",$scope.BpCategory);
            $scope.BpGroupList[counting].visible = 1;
            $scope.teamSelected = $scope.BpGroupList[counting];
            // $scope.bpCategorySelected = $scope.BpCategory[countingParent];
            $scope.bpCategorySelected = $scope.BpCategory[_.findIndex($scope.BpCategory,{name:'ALL'})];

            $scope.groupBpId = $scope.BpGroupList[counting].Id;
            console.log('ini groupbpid', $scope.groupBpId);

            showBoardJPCB($scope.BpGroupList[counting].Id,$scope.bpCategorySelected.id);
            // showBoardJPCB(1);
            var urlBoii ='/api/as/Boards/BP/GetBPJobListNoJobId/'+$scope.BpGroupList[counting].Id+'/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/'+$filter('date')($scope.JpcbBpData.cuarrentDate, 'yyyy-MM-dd')+'/'+$scope.bpCategorySelected.id;
            var keyTmpData = [];
            $scope.tmpDataTarget = [];
            JpcbBpViewFactory.loadChipData(urlBoii).then(function(res){
                if(res.data.Result.length > 0){

                    var tmpjpcbDataChip = [];
                    // var tmpjpcbDataChip = res.data.Result;
                    console.log('res boko', res)
                    for(var k in res.data.Result){
                        if (res.data.Result[k].Status == 0 || res.data.Result[k].Status == 1){
                            tmpjpcbDataChip.push(res.data.Result[k]);
                        }
                    }
                    console.log('tmpjpcbDataChip boko', tmpjpcbDataChip)

                    if(tmpjpcbDataChip.length > 0){
                        $scope.jpcbDataChip = _.groupBy(tmpjpcbDataChip,'PoliceNumber');
                        keyTmpData = Object.keys($scope.jpcbDataChip);
                        for(var i in keyTmpData){
                            console.log('bor',$scope.jpcbDataChip);
                            console.log('bor',keyTmpData);
                            $scope.tmpDataTarget.push({
                                name:keyTmpData[i],
                                process:$scope.filterDataProcess($scope.jpcbDataChip[keyTmpData[i]])
                            })
                        }
                    }
                    
                    console.log("$scope.jpcbDataChip",$scope.tmpDataTarget,$scope.jpcbDataChip,$scope.keyTmpData);
                }
            });
            console.log("$scope.BpGroupList ===",$scope.BpGroupList[counting]);
        }
        
    })