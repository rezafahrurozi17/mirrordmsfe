angular.module('app')
    .factory('JscbFactory', function($http, CurrentUser, $filter, $timeout, jpcbFactory) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            minuteDelay: 10*60+10,
            //minuteDelay: 0,
            onPlot: function(){
                // do nothing
            },
            showBoard: function(config){
                var vcontainer = config.container;
                var vname = 'asbgr';
                if (typeof config.boardName !== 'undefined'){
                    vname = config.boardName;
                }
                var vobj = document.getElementById(vcontainer);
                if(vobj){
                    var vwidth = vobj.clientWidth;
                    console.log(vwidth);
                    if (vwidth<=0) {
                        return;
                    }
                } else {
                    return;
                }
                if (typeof config.onPlot=='function'){
                    this.onPlot = config.onPlot;
                }

                var vdurasi = 1;
                // if (typeof config.currentChip.durasi !=='undefined'){
                //     vdurasi = config.currentChip.durasi;
                // }
                // config.currentChip.width = 7; //(vdurasi/60)*this.incrementMinute;

                this.updateFooterAsb();
                var vrow = -2;
                // if (typeof config.currentChip.stallId !=='undefined'){
                //     if (config.currentChip.stallId>0){
                //         vrow = config.currentChip.stallId - 1 ;
                //     }
                // }
                // config.currentChip.row = vrow;

                // var vhourStart = JsBoard.Common.getMinute(this.startHour)/60;
                // var vcol = 0;
                // if (typeof config.currentChip.startTime !=='undefined'){
                //     vstartTime = config.currentChip.startTime;
                //     vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                // }
                // config.currentChip.col = vcol;
                var vColumnWidth = 24;
                if (typeof config.columnWidth!=='undefined'){
                    vColumnWidth = config.columnWidth;
                }
                var voptions = {
                    board: {
                        chipGroup: 'jscb',
                        columnWidth: vColumnWidth,
                        rowHeight: 90,
                        footerChipName: 'footer',
                        snapDivider: 1,
                        firstColumnWidth: 80,
                        firstHeaderChipName: 'first_header',
                        fixedHeaderChipName: 'fixed_header',
                        headerChipName: 'header',
                        firstColumnTitle: '',
                    },
                    chip: {
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute,
                        itemCount: 5,
                    },
                    argStall: {
                        BpCategoryId: '0',
                        BpCategoryName: '...'
                    }
                }

                var xvoptions = voptions;
                if (typeof config.options !== 'undefined'){
                    xvoptions = JsBoard.Common.extend(voptions, config.options);
                    if (typeof config.options.board !== 'undefined'){
                        xvoptions.board = JsBoard.Common.extend(voptions.board, config.options.board);
                    }
                    if (typeof config.options.argStall !== 'undefined'){
                        xvoptions.argStall = JsBoard.Common.extend(voptions.argStall, config.options.argStall);
                    }
                }
                jpcbFactory.createBoard(vname, xvoptions);
                var me = this;
                jpcbFactory.setHeader(vname, {
                    headerHeight: 40,
                    onGetHeader: function(arg, vfunc){
                        return me.onGetHeader(arg, vfunc);
                    }
                });
                jpcbFactory.setStall(vname, {
                    //firstColumnWidth: 120,
                    onGetStall: function(arg, vfunc){
                        return me.onGetStall(arg, vfunc);
                    }
                });

                jpcbFactory.setOnGetChips(vname, function(arg, vfunc){
                    return me.onGetChips(arg, vfunc);
                });

                jpcbFactory.setOnChipLoaded(vname, function(arg, vfunc){
                    return me.onChipLoaded(arg, vfunc);
                });
                
                jpcbFactory.setSelectionArea(vname, {
                    tinggiArea: 4,
                    tinggiHeader: 32,
                    tinggiSubHeader: 32,

                });

                // jpcbFactory.setSelectionArea(vname, {tinggiArea: 4, fixedHeaderChipName: 'blank'});

                jpcbFactory.setArgChips(vname, config);
                jpcbFactory.setArgStall(vname, config);
                jpcbFactory.setArgHeader(vname, config);
                // jpcbFactory.setArgChips(vname, config.argChips);
                // jpcbFactory.setArgStall(vname, config.argStall);
                // jpcbFactory.setArgHeader(vname, config.argHeader);

                // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

                jpcbFactory.setOnCreateChips(vname, function(vchip){
                    if (vchip.data.selected){
                        vchip.draggable(true);
                    }
                });


                var me = this;
                var the_config = config;
                // jpcbFactory.setOnDragDrop(vname, function(vchip){
                //     var curDate = config.currentChip.currentDate;
                //     var vdurasi = 1;
                //     if (typeof config.currentChip.durasi !=='undefined'){
                //         vdurasi = config.currentChip.durasi;
                //     }

                //     var vmin = me.incrementMinute * vchip.data['col'];
                //     var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                //     var vres = JsBoard.Common.getTimeString(vminhour);
                //     var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+vres);
                //     //startTime.setMinutes(startTime.getMinutes()+vmin);
                //     //startTime.setMinutes(vminhour);

                //     var emin = vminhour + vdurasi * 60;
                //     var eres = JsBoard.Common.getTimeString(emin);
                //     var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd')+' '+eres);
                //     //endTime.setMinutes(endTime.getMinutes()+emin);

                //     vchip.data.startTime = startTime;
                //     vchip.data.endTime = endTime;
                //     var vStallId = 0;
                //     if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                //         && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                //         vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                //     }
                //     vchip.data.stallId = vStallId;
                //     console.log("on plot data", vchip.data);

                //     if (typeof me.onPlot=='function'){           

                //         me.onPlot(vchip.data);
                //     }
                //     console.log("on drag-drop vchip", vchip);
                // });

                jpcbFactory.showBoard(vname, vcontainer)
                // $timeout(function(){
                //     jpcbFactory.showBoard(vname, vcontainer)

                // }, 100);

            },
            headerIndex: {},
            headerData: [],
            startDate: false,
            startHour: '08:00',
            endHour: '17:00',
            breakHour: '12:00',
            incrementMinute: 60,

            updateFooterAsb: function(){
                asbProduksiColorNames['line1'] = 'Body Repair';
                asbProduksiColorNames['line2'] = 'Putty Sanding';
                asbProduksiColorNames['line3'] = 'Surfacer';
                asbProduksiColorNames['line4'] = 'Spraying';
                asbProduksiColorNames['line5'] = 'Polish';
                asbProduksiColorNames['line6'] = 'Re-Assy';
                asbProduksiColorNames['line7'] = 'Final Inspection';
                // JsBoard.Chips.jscb.footer.items['line1'] = 'Body Repair';
                // JsBoard.Chips.jscb.footer.items['line2'] = 'Putty Sanding';
                // JsBoard.Chips.jscb.footer.items['line3'] = 'Surfacer';
                // JsBoard.Chips.jscb.footer.items['line4'] = 'Spraying';
                // JsBoard.Chips.jscb.footer.items['line5'] = 'Polish';
                // JsBoard.Chips.jscb.footer.items['line6'] = 'Re-Assy';
                // JsBoard.Chips.jscb.footer.items['line7'] = 'Final Inspection';
            },
            getCurrentTime: function(){
                return jpcbFactory.getCurrentTime();
            },
            isProblem: function(vitem){
                return jpcbFactory.isJobProblem(vitem);
            },
            loadMainData: function(config, onFinish){
                var BpCategory = {}
                var BpGroupList = [] 
                var loadedStatus = [false, false];
                var updateLoadedData = function(){
                    if (loadedStatus[0] && loadedStatus[1]){
                        onFinish(BpCategory, BpGroupList);
                    }
                }
                
                $http.get(config.urlCategoryBp).then(function(res){
                    var xres = res.data.Result;
                    var result = {};
                    for(var i=0;  i<xres.length; i++){
                        var vitem = xres[i];
                        result[vitem.MasterId] = {
                            id: vitem.MasterId,
                            name: vitem.Name,
                            type: vitem.Name
                        }
                    }
                    BpCategory = result;
                    loadedStatus[0] = true;
                    updateLoadedData();
                });
    
                $http.get(config.urlGroupList).then(function(res){
                    var xres = res.data.Result;
                    var result = [];
                    var idx = 0;
                    for(var i=0; i<xres.length; i++){
                        var vitem = xres[i];
                        result.push({
                            name: vitem.GroupName,
                            type: vitem.BpCategory,
                            count: vitem.TaskCount,
                            BpCategoryId: vitem.BpCategoryId,
                        });
                        idx = idx+1;
                    }
                    BpGroupList = result;
                    loadedStatus[1] = true;
                    updateLoadedData();
                });
                
                
            },
            timeToColumn: function(vtipe, vtime){
                if ((vtipe=='tps') || (vtipe=='light') ){
                    var vres = false;
                    var vindex = $filter('date')(vtime, 'yyyy-MM-dd.HH:00');
                    var vmin = $filter('date')(vtime, 'mm');
                    for (var i=0; i<this.headerData.length; i++){
                        var vitem = this.headerData[i];
                        if (vindex==vitem.dayIndex){
                            vres = i;
                            break;
                        } else {
                            if ((vindex<vitem.dayIndex)&&i==0){
                                break;
                            } else if (vindex<vitem.dayIndex){
                                vres = i;
                                break;
                            }
                        }
                    }
                    if ((vmin!=='00') && (vres!==false)){
                        var vxmin = JsBoard.Common.getMinute('00:'+vmin);
                        vres = vres+vxmin/60;
                    }
                    return vres;
                } else if ((vtipe=='medium') || (vtipe=='heavy')){
                    var vres = false;
                    var vindex = $filter('date')(vtime, 'yyyy-MM-dd');
                    //var vmin = $filter('date')(vtime, 'mm');
                    var vhour = $filter('date')(vtime, 'HH:mm');
                    for (var i=0; i<this.headerData.length; i++){
                        var vitem = this.headerData[i];
                        if (vindex==vitem.dayIndex){
                            vres = i;
                            break;
                        } else {
                            if ((vindex<vitem.dayIndex)&&i==0){
                                break;
                            } else if (vindex<vitem.dayIndex){
                                vres = i;
                                break;
                            }
                        }
                    }
                    if ((vhour!=='00:00') && (vres!==false)){
                        // var vxmin = JsBoard.Common.getMinute(vhour); // ini punya pa toto
                        // vres = vres+vxmin/(60*8); // ini punya pa toto

                        // new era begin ======================start
                        var waktu = vhour.split(':');
                        var penambahanJam = (1/10) * (parseInt(waktu[0])-8) // 1 adalah 1 kolom hari, 10 adalah jam kerja (pulang-masuk), 8 adalah jam masuk anggapannya nih semua masih hardcode
                        var penambahanMenit = (1/10) * (parseInt(waktu[1]) / 60) // 1 adalah 1 kolom hari, 10 adalah jam kerja (pulang-masuk), 60 menit dalam sejam ya biasa
                        // new era begin ======================end
                        vres = vres + penambahanJam + penambahanMenit

                    }
                    return vres;
                }
                
                return 0;
            },
            xtimeToColumn: function(vtipe, vdate, vtime){
                if (vtipe=='tps'){
                    var vres = false;
                    var vindex = $filter('date')(vdate, 'yyyy-MM-dd.');
                    vindex = vindex+vtime.substring(0, 2)+':00';
                    var vmin = vtime.substring(3, 2);
                    for (var i=0; i<this.headerData.length; i++){
                        var vitem = this.headerData[i];
                        if (vindex==vitem.dayIndex){
                            vres = i;
                            break;
                        } else {
                            if ((vindex<vitem.dayIndex)&&i==0){
                                break;
                            } else if (vindex<vitem.dayIndex){
                                vres = i;
                                break;
                            }
                        }
                    }
                    if (vmin!=='00'){
                        var vxmin = JsBoard.Common.getMinute('00:'+vmin);
                        vres = vres+vxmin/60;
                    }
                    return vres;
                }
                return 0;
                // var vstartmin = JsBoard.Common.getMinute(this.startHour);
                // var vmin = JsBoard.Common.getMinute(vtime)-vstartmin;
                // var vresult = vmin/this.incrementMinute;
                
                // return vresult;
            },
            durationToColumn: function(vtipe, vduration){
                // input jam
                if ((vtipe=='tps') || (vtipe=='light') || (vtipe=='medium') || (vtipe=='heavy')){
                    return vduration;
                }
                return 0;
            },
            onGetHeader: function(arg, onFinish){
                var vtype = arg.BpCategory.type.toLowerCase();
                var one_day=1000*60*60*24;
                var difference_ms = arg.endDate.getTime() - arg.startDate.getTime();
                var daydiff = Math.round(difference_ms/one_day);
                if (daydiff<=2){
                    daydiff = 2;
                }

                result = this.getHeader(vtype, arg.startDate, daydiff);
                this.headerData = result;
                $timeout(function(){
                    onFinish(result);
                }, 10);
                
            },

            getDailyList: function(vstartDate, vcount){
                var result = []
                var newMonth = true;
                var subCount = 1;
                var vDate = new Date(vstartDate);
                var vparent = $filter('date')(vDate, 'MMMM');
                var lastItem = false;
                for (var i=0; i<vcount; i++){
                    var vtext = $filter('date')(vDate, 'dd');
                    if(newMonth){
                        item = {newDay: newMonth, parentText: vparent, subCount: subCount, text: vtext}
                        if (lastItem){
                        lastItem.subCount = subCount;
                        }
                        lastItem = item;
                        subCount = 1;
                    } else {
                        item = {newDay: newMonth, text: vtext}
                        subCount = subCount+1;
                    }
                    var dindex = $filter('date')(vDate, 'yyyy-MM-dd');
                    item.dayIndex = dindex;
                    result.push(item);
                    vDate.setDate(vDate.getDate()+1);
                    var xparent = $filter('date')(vDate, 'MMMM');
                    if (xparent!==vparent){
                        newMonth = true;
                        vparent = xparent;
                    } else {
                        newMonth = false;
                    }
                }
                if (lastItem){
                  lastItem.subCount = subCount;
                }
                return result;
        
            },
            getWeeklyList: function(startDate, vcount){
                return [{text: '1'}, {text: '2'}, {text: '3'}, {text: '4'}]
    
            },
            getHeader: function(vtype, vparam1, vparam2){

                //return [{text: '1'}, {text: '2'}, {text: '3'}, {text: '4'}]
                // type: 'hourly'; param1: start date, param2: date count
                // type: 'daily'; param1: start date, param2: date count
                // type: 'weekly'; param1: start date, param2: date count
                if (vtype.toLowerCase()=='tps'){
                    if (typeof vparam1 == 'undefined'){
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined'){
                        vparam2 = 7;
                    }
                    var vres = this.getHourlyList(vparam1, vparam2);
                    console.log("hourly list", vres);
                    return vres;
                } else if (vtype.toLowerCase()=='light'){
                    if (typeof vparam1 == 'undefined'){
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined'){
                        vparam2 = 7;
                    }
                    var vres = this.getHourlyList(vparam1, vparam2);
                    console.log("hourly list", vres);
                    return vres;
                    // var vres = this.getDailyList(vparam1, vparam2);
                    // console.log("daily list", vres);
                    // return vres;
                } else if (vtype.toLowerCase()=='medium'){
                    if (typeof vparam1 == 'undefined'){
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined'){
                        vparam2 = 14;
                    }
                    var vres = this.getDailyList(vparam1, vparam2);
                    console.log("daily list", vres);
                    return vres;
                } else if (vtype.toLowerCase()=='heavy'){
                    if (typeof vparam1 == 'undefined'){
                        vparam1 = new Date();
                    }
                    if (typeof vparam2 == 'undefined'){
                        vparam2 = 30;
                    }
                    var vres = this.getDailyList(vparam1, vparam2);
                    console.log("daily list", vres);
                    return vres;
                }
            }, 
            getWorkHour: function(){
                return JsBoard.Common.generateDayHours({
                    startHour: '08:00',
                    endHour: '17:00',
                    increment: 60,
                })
            },
            dayNameMap: {
                'Sun': 'Minggu', 'Mon': "Senin", 'Tue': 'Selasa', 'Wed': 'Rabu',
                'Thu': 'Kamis', 'Fri': 'Jumat', 'Sat': 'Sabtu'
            },
            formatDisplayDate: function(vdate){
                var vDay = $filter('date')(vdate, 'EEE');
                var vHari = this.dayNameMap[vDay];
                var vresult = vHari+', '+$filter('date')(vdate, 'dd/MM/yy');
                return vresult;
            },
            getHourlyList: function(startDate, vcount){
                var breakHour = '12:00';
                var result = []
                var vidx = 0;
                var vWorkHour = this.getWorkHour();
                var item = false
                var vDate = new Date(startDate);
                var currentDate = '******';
                for (var i=0; i<vcount; i++){
                    var newDay = true;
                    var subCount = vWorkHour.length;
                    //currentDate = $filter('date')(vDate, 'dd MMMM yyyy');
                    currentDate = this.formatDisplayDate(vDate);
                    var dindex = $filter('date')(vDate, 'yyyy-MM-dd.');
                    var vindex = '';
                    for(var idx in vWorkHour){
                        var vpush = true;
                        vindex = dindex+vWorkHour[idx].substring(0, 5);
                        if (newDay){
                            item = {newDay: newDay, parentText: currentDate, subCount: subCount, text: vWorkHour[idx].substring(0, 2)}
                        } else {
                            item = {newDay: newDay, text: vWorkHour[idx].substring(0, 2)}
                        }
                        item.dayIndex = vindex;
                        if (vWorkHour[idx]==breakHour){
                            item.colInfo = {isBreak: true}
                            //vpush = false;
                        }
                        if (vpush){
                            result.push(item);
                        }
                        var newDay = false;
                    }
                    vDate.setDate(vDate.getDate()+1);
                }
                return result;
            },

            // onGetHeader: function(arg){
            //     varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
            //     var vWorkHour = JsBoard.Common.generateDayHours({
            //         startHour: this.startHour,
            //         endHour: this.endHour,
            //         increment: this.incrementMinute,
            //     });
            //     var result = [];
            //     for(var idx in vWorkHour){
            //         var vitem = {title: vWorkHour[idx]}
            //         if (vWorkHour[idx]==this.breakHour){
            //             vitem.colInfo = {isBreak: true}
            //         }
            //         result.push(vitem)

            //     }
            //     console.log("header", result);
            //     return result;
            // },
            onGetStall: function(arg, onFinish){
                // // $http.get('/api/as/Stall').
                // var vparent = '0';
                // $http.get('/api/as/Boards/Stall/'+vparent).then(function(res){
                //     var xres = res.data.Result;
                //     var result = [];
                //     for(var i=0;  i<xres.length; i++){
                //         var vitem = xres[i];
                //         result.push({
                //             title: vitem.Name,
                //             status: 'normal',
                //             stallId: vitem.StallId
                //         });
                //     }
                //     onFinish(result);
                // })
                // ======================== add 31 juli 2019 ==================== start
                var vtype = arg.BpCategory.type.toLowerCase();
                console.log('arg-onGetChips', arg);
                var vArg = {nopol: 'B 1000 AAA', tipe: 'Avanza', row: -2, col: 0, width: 7, stallId: 0}
                vArg = JsBoard.Common.extend(vArg, arg);

                var vtgl1 = $filter('date')(arg.startDate, 'yyyy-MM-dd');
                var vtgl2 = $filter('date')(arg.endDate, 'yyyy-MM-dd');
                //var vurl = '/api/as/Boards/BP/JobList/0/'+arg.BpCategory.id+'/'+vtgl1+'/'+vtgl2;
                var vurl = '/api/as/Boards/BP/JscbList/'+arg.BpCategory.id+'/'+vtgl1+'/'+vtgl2;
                var me = this;
                // $timeout(function(){
                    var jumlahRow = 30;
                    $http.get(vurl).then(function(res){
                        jumlahRow = res.data.Result.length;

                        var vcount = jumlahRow;
                        var vres = []
                        var vstallText = arg.BpCategory.name;
                        if (typeof vstallText == 'undefined'){
                            vstallText = '';
                        }
                        for (var i=0; i<vcount; i++){
                            var xstall = {Stallid: i, Name: 'Stall-'+i}
                            if (i==vcount-1){

                                var chcount = vstallText.length;
                                var vstr = '';
                                for(var j=0; j<vstallText.length; j++){
                                    vstr = vstr+vstallText[j]+"\r\n";
                                }
                                xstall.visible = true;
                                xstall.itemCount = vcount;
                                xstall.textCount = chcount;
                                xstall.title = vstr;
                            } else {
                                xstall.visible = false;
                            }
                            vres.push(xstall);
                        }
                        $timeout(function(){
                            onFinish(vres);
                        }, 100);
                    });
                // }, 200);

                // ======================== add 31 juli 2019 ==================== end

                // ======================== commented 31 juli 2019 ============== start
                // var vcount = jumlahRow-1;
                // var vres = []
                // var vstallText = arg.BpCategory.name;
                // if (typeof vstallText == 'undefined'){
                //     vstallText = '';
                // }
                // for (var i=0; i<vcount; i++){
                //     var xstall = {Stallid: i, Name: 'Stall-'+i}
                //     if (i==vcount-1){

                //         var chcount = vstallText.length;
                //         var vstr = '';
                //         for(var j=0; j<vstallText.length; j++){
                //             vstr = vstr+vstallText[j]+"\r\n";
                //         }
                //         xstall.visible = true;
                //         xstall.itemCount = vcount;
                //         xstall.textCount = chcount;
                //         xstall.title = vstr;
                //     } else {
                //         xstall.visible = false;
                //     }
                //     vres.push(xstall);
                // }
                // $timeout(function(){
                //     onFinish(vres);
                // }, 100);
                // ======================== commented 31 juli 2019 ============== end

            },
            // onGetStall: function(arg){
            //     return[{title: 'Diagnose', status: 'normal', person: 'Joko'},
            //         {title: 'EM 1', status: 'normal', person: 'Ton'},
            //         {title: 'EM 2', status: 'normal', person: 'And'},
            //         {title: 'ST 1', status: 'normal', person: 'Rul'},
            //         {title: 'ST 2', status: 'normal', person: 'Fir'},
            //         {title: 'S3 3', status: 'normal', person: 'Ron'},
            //     ]
            // },
            findStallIndex: function(vStallId, vList){
                for (var i=0; i<vList.length; i++){
                    var vitem = vList[i];
                    if ((typeof vitem.stallId!=='undefined') && (vitem.stallId==vStallId)){
                        return i;
                    }
                }
                return -1;
            },
            onChipLoaded: function(vjpcb){
                for (var i=0; i<vjpcb.allChips.length; i++){
                    var vitem = vjpcb.allChips[i];
                    vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                    if (vidx>=0){
                        vitem.row = vidx;
                    }                    
                    
                }
            },
            isOverlap: function(vcol1, vrow1, vlen1, vcol2, vrow2, vlen2){
                if (vrow1==vrow2){
                    vcawal = vcol2;
                    vcakhir = vcol2+vlen2;
                    if( ((vcawal>=vcol1) && (vcawal<=vcol1+vlen1)) || ((vcakhir>=vcol1) && (vcakhir<=vcol1+vlen1)) ){
                        return true;
                    }
                }
                return false;
            },
            findStallRow: function(vlist, vchip, vrcount){                
                
                for (var i=0; i<vrcount; i++){
                    var overlap = false;
                    for (var j=0; j<vlist.length; j++){
                        var vtest = vlist[j];
                        if (this.isOverlap(vtest.col, vtest.row, vtest.width, vchip.col, i, vchip.width)){
                            overlap = true;
                            break;
                        }
                    }
                    if (!overlap){
                        return i;
                    }
                }
                return -1;
            },
            updateChipByIndex: function(vtype, vleft, vidx, vdata, vitem){
                var vLeft1 = this.timeToColumn(vtype, vitem['Chip'+vidx+'PlanStart'])-vleft;
                var vVisible1 = 0;
                if (vitem['Chip'+vidx+'Visible']){
                    if (vLeft1>=0){
                        vVisible1 = 1;
                    }
                }
                var vWidth1 = this.durationToColumn(vtype, vitem['Chip'+vidx+'Duration']);

                vdata['Chip'+vidx+'Left'] = vLeft1;
                vdata['Chip'+vidx+'Visible'] = vVisible1;
                vdata['Chip'+vidx+'Width'] = vWidth1;
                vdata['chip'+vidx+'Start'] = vitem['Chip'+vidx+'PlanStart'];
                vdata['chip'+vidx+'Duration'] = vitem['Chip'+vidx+'Duration'];

                // ============================= awal dari semua perubahan ============================================
                if (vitem.cekKerjaanDone == vitem.cekjumlahkerjaan){
                    // ini set posisi awal dari kiri warnanya, awal posisi chip putih di anggap posisi 0
                    var cariPosisiKiriWarna = 0;
                                    
                    if (vLeft1 == 0){
                        vdata['Chip'+vidx+'Left'] = vLeft1
                    } else {
                        cariPosisiKiriWarna = vdata['Chip'+(vidx-1)+'Left'] + vdata['Chip'+(vidx-1)+'Width']
                        vdata['Chip'+vidx+'Left'] = cariPosisiKiriWarna
                    }

                    // ini cari lebar setiap warna
                    if (vitem['ActualChipTimeDuration'+vidx] != null && vitem['ActualChipTimeDuration'+vidx] != undefined){
                        if (vitem['ActualChipTimeDuration'+vidx] > 0){
                            vdata['Chip'+vidx+'Width'] =  vitem['ActualChipTimeDuration'+vidx];
                        } else {
                            vdata['Chip'+vidx+'Width'] =  vitem['Chip'+ vidx +'Duration'];
                        }
                    } else {
                        vdata['Chip'+vidx+'Width'] =  vitem['Chip'+ vidx +'Duration'];
                    }

                    // ini cari duration setiap warna (masih belom tau buat apa duration)
                    if (vitem['ActualChipTimeDuration'+vidx] != null && vitem['ActualChipTimeDuration'+vidx] != undefined){
                        if (vitem['ActualChipTimeDuration'+vidx] > 0){
                            vdata['Chip'+vidx+'Duration'] =  vitem['ActualChipTimeDuration'+vidx];
                        } else {
                            vdata['Chip'+vidx+'Duration'] =  vitem['Chip'+ vidx +'Duration'];
                        }
                    } else {
                        vdata['Chip'+vidx+'Duration'] =  vitem['Chip'+ vidx +'Duration'];
                    }

                    vdata.width = cariPosisiKiriWarna + vdata['Chip'+vidx+'Width']
                }
                
                // ============================= akhir dari semua perubahan ============================================


            },
            onGetChips: function(arg, onFinish){
                var vtype = arg.BpCategory.type.toLowerCase();
                console.log('arg-onGetChips', arg);
                var vArg = {nopol: 'B 1000 AAA', tipe: 'Avanza', row: -2, col: 0, width: 7, stallId: 0}
                vArg = JsBoard.Common.extend(vArg, arg);
                
                // var tglAwal = arg.currentDate;
                // var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
                // tglAkhir.setDate(tglAkhir.getDate());
                // var vurl = '/api/as/Boards/Appointment/1/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/'+$filter('date')(tglAkhir, 'yyyy-MM-dd');
                // var vMinMaxStatus = '/0/17';
                // var vurl = '/api/as/Boards/jpcb/bydate/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+vMinMaxStatus;

                var vtgl1 = $filter('date')(arg.startDate, 'yyyy-MM-dd');
                var vtgl2 = $filter('date')(arg.endDate, 'yyyy-MM-dd');
                //var vurl = '/api/as/Boards/BP/JobList/0/'+arg.BpCategory.id+'/'+vtgl1+'/'+vtgl2;
                var vurl = '/api/as/Boards/BP/JscbList/'+arg.BpCategory.id+'/'+vtgl1+'/'+vtgl2;
                var me = this;
                $timeout(function(){
                    $http.get(vurl).then(function(res){
                        var result = [];
                        // if (!(typeof vArg.visible !== 'undefined' && !vArg.visible)) {
                        //     result = [{chip: 'produksi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: 7, xwidth: vArg.width, selected: 1, 
                        //         stallId: vArg.stallId,
                        //         daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}];
                        // }
                        var xres = res.data.Result;
                        var xxxres = [{
                            "JobId" : 29,
                            "ChipType" : 1,
                            "PoliceNumber" : "B4101AB",
                            "ModelType" : "AVANZA",
                            "WoNo" : null,
                            "AppointmentDate" : "2017-12-17T00:00:00",
                            "AppointmentTime" : "08:00:00",
                            "PlanDateFinish" : "2017-12-17T00:00:00",
                            "PlanFinish" : "09:00:00",
                            "StallId" : 3,
                            "Status" : 10,
                            "DateDuration" : 0.04166666,
                            "TimeDuration" : 1.0,
                            "Extension" : 0.0,
                            "JanjiSerah" : "2017-12-18T12:00:00",
                            "isAllocated" : 1,
                            "PauseReasonId" : 0,
                            "PauseReason" : null
                        }];
                        var checkJobId = false;
                        if (typeof vArg.jobId!=='undefined'){
                            checkJobId = true;
                        }
                        //var vList = []
                        var vrow = 7;
                        var vcol = 7*8;
                        // var vtype = 'tps';
                        //var caData = CADataFactory.create({});

                        var cekKerjaanDone = 0;
                        var cekjumlahkerjaan = 0
                        for (var i=0; i<xres.length; i++){
                            if (xres[i].Chip1Duration > 0){
                                cekjumlahkerjaan++
                            }
                            if (xres[i].Chip2Duration > 0){
                                cekjumlahkerjaan++
                            }
                            if (xres[i].Chip3Duration > 0){
                                cekjumlahkerjaan++
                            }
                            if (xres[i].Chip4Duration > 0){
                                cekjumlahkerjaan++
                            }
                            if (xres[i].Chip5Duration > 0){
                                cekjumlahkerjaan++
                            }
                            if (xres[i].Chip6Duration > 0){
                                cekjumlahkerjaan++
                            }
                            if (xres[i].Chip7Duration > 0){
                                cekjumlahkerjaan++
                            }
                            xres[i].cekjumlahkerjaan = cekjumlahkerjaan

                            if (xres[i].ActualChipTimeDuration1 > 0){
                                cekKerjaanDone++
                            }
                            if (xres[i].ActualChipTimeDuration2 > 0){
                                cekKerjaanDone++
                            }
                            if (xres[i].ActualChipTimeDuration3 > 0){
                                cekKerjaanDone++
                            }
                            if (xres[i].ActualChipTimeDuration4 > 0){
                                cekKerjaanDone++
                            }
                            if (xres[i].ActualChipTimeDuration5 > 0){
                                cekKerjaanDone++
                            }
                            if (xres[i].ActualChipTimeDuration6 > 0){
                                cekKerjaanDone++
                            }
                            if (xres[i].ActualChipTimeDuration7 >= 0){ // chip 7 blm update actual jadi selalu 0
                                cekKerjaanDone++
                            }
                            xres[i].cekKerjaanDone = cekKerjaanDone
                        }

                        for(var i=0;  i<xres.length; i++){
                            var vitem = xres[i];
                            // var xtime = $filter('date')(vitem.DateTimeStart, 'HH:mm')
                            // var vcolumn = me.timeToColumn('tps', vitem.DateTimeStart, xtime);
                            //var vstallid = CADataFactory.findStall(caData, vitem.AppointmentDate, vitem.AppointmentTime, 7);

                            var vcol1 = me.timeToColumn(vtype, vitem.DateTimeStart);
                            var vcol2 = me.timeToColumn(vtype, vitem.DateTimeFinish);


                            
                            if (vcol2-vcol1>0.1){
                                vstallid = 1;
                                var vdata = {
                                    chip: 'produksi', 
                                    nopol: vitem.PoliceNumber, 
                                    tipe: vitem.ModelType, 
                                    stallId: vstallid,
                                    row: 0, col: vcol1, 
                                    // PlanDateStart: vitem.PlanDateStart,
                                    // PlanDateFinish: vitem.PlanDateFinish,
                                    // PlanStart: vitem.PlanStart,
                                    // PlanFinish: vitem.PlanFinish,
                                    width: vcol2-vcol1,
                                    start: vitem.DateTimeStart,
                                    finish: vitem.DateTimeFinish,
                                    MaxRight: vcol2,

                                }
                                for (var vidx=1; vidx<=7; vidx++){
                                    me.updateChipByIndex(vtype, vcol1, vidx, vdata, vitem);
                                }
                                
                                //vdata.row = me.findStallRow(result, vdata, vrow);
                                vdata.row = i;
                                if (vdata.row>=0){
                                    result.push(vdata);
                                }
                            }
                        }
                        console.log('chips', result);
                        onFinish(result);
                    });
                }, 200);

                // var vres = [];
                // var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
                // vArg = JsBoard.Common.extend(vArg, arg);
                // var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
                //     {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                //     {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
                // ]
                // var today = new Date();
                // if (arg.currentDate){
                //     var vdiff = this.dayDiff(today, arg.currentDate);
                //     for (var i=0; i<vitems.length; i++){
                //         if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
                //             vres.push(vitems[i]);
                //         }
                //     }
                // }

                // $timeout(function(){
                //     onFinish(vres);
                // }, 100);
            },

            // onGetChips: function(arg){
            //     var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
            //     vArg = JsBoard.Common.extend(vArg, arg);
            //     var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     ]
            //     var today = new Date();
            //     if (arg.currentDate){
            //         var vdiff = this.dayDiff(today, arg.currentDate);
            //         var vres = []
            //         for (var i=0; i<vitems.length; i++){
            //             if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
            //                 vres.push(vitems[i]);
            //             }
            //         }
            //         return vres;
            //     }
            //     return []
            // },
            dayDiff: function(date1, date2){
                return Math.round((date2 - date1) / (1000*60*60*24));
            },


            
        }
    });

/*
angular.module('app')
    .factory('AsbProduksiFactory', function($http, CurrentUser, $filter, jpcbFactory) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            showBoard: function(config){
                var vcontainer = config.container;
                var vname = 'estimasi';
                jpcbFactory.createBoard(vname, {
                    board: {
                        chipGroup: 'asb_produksi',
                        columnWidth: 24,
                        rowHeight: 90,
                        snapDivider: 1,
                    },
                    chip: {
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute
                    },
                    listeners: {
                        chip:{
                            click: function(){
                                alert('test');
                            }
                        }
                    }
                });
                var me = this;
                jpcbFactory.setHeader(vname, {
                    headerHeight: 40,
                    onGetHeader: function(arg){
                        return me.onGetHeader(arg);
                    }
                });
                jpcbFactory.setStall(vname, {
                    firstColumnWidth: 60,
                    onGetStall: function(arg){
                        return me.onGetStall(arg);
                    }
                });

                jpcbFactory.setOnGetChips(vname, function(arg){
                    return me.onGetChips(arg);
                });

                jpcbFactory.setSelectionArea(vname, {
                    tinggiArea: 110,
                    tinggiHeader: 30,
                    tinggiSubHeader: 24,

                });

                jpcbFactory.setArgChips(vname, config.currentChip);
                jpcbFactory.setArgHeader(vname, config.currentChip);
                jpcbFactory.setCheckDragDrop(vname, function(vchip, xcol, vrow){
                    var vtoday = new Date();
                    vcontainer = vchip.container;
                    if (vcontainer.layout.infoCols){
                        vcol = xcol+1;
                        console.log("vcontainer.layout.infoCols", vcontainer.layout.infoCols);
                        console.log("vcol", vcol);
                        if (vcontainer.layout.infoCols[vcol] && vcontainer.layout.infoCols[vcol].data && vcontainer.layout.infoCols[vcol].data.tanggal){
                            console.log("vcontainer.layout.infoCols", vcontainer.layout.infoCols[vcol]);
                            if (vcontainer.layout.infoCols[vcol].data.tanggal.getDate() < vtoday.getDate()){
                                return false;
                            }
                        }
                    }
                    return true;
                });

                jpcbFactory.showBoard(vname, vcontainer)

            },
            startHour: '08:00',
            endHour: '17:00',
            breakHour: '12:00',
            incrementMinute: 60,
            listJam: ['8', '9', '10', '11', '13', '14', '15', '16'],
            onGetHeader: function(arg){
                console.log('arg header', arg);

                varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 7}, arg);
                if ((varg.startDate) && (varg.endDate)){
                    varg.dateCount = this.dayDiff(varg.startDate, varg.endDate)+1;
                }
                var vresult = []
                var vListJam = this.listJam;

                if (varg.startDate){
                    var vdate = new Date(varg.startDate);
                } else {
                    var vdate = new Date();
                }
                
                for(var i=0; i<varg.dateCount; i++){
                    var vshowParent = true;
                    for (var j=0; j<vListJam.length; j++){
                        var vdayname = JsBoard.Common.localDayName($filter('date')(vdate, 'EEEE'));
                        var vtgl = vdayname+', '+$filter('date')(vdate, 'dd/MM/yyyy');
                        vdata = {title: vListJam[j]+'', parentText: vtgl, itemCount: vListJam.length, showParent: vshowParent, tanggal: new Date(vdate)}
                        vresult.push(vdata);
                        vshowParent = false;
                    }
                    vdate.setDate(vdate.getDate()+1);
                    
                }
                console.log("vresult", vresult);
                return vresult;

                // var vWorkHour = JsBoard.Common.generateDayHours({
                //     startHour: this.startHour,
                //     endHour: this.endHour,
                //     increment: this.incrementMinute,
                // });
                // var result = [];
                // for(var idx in vWorkHour){
                //     var vitem = {title: vWorkHour[idx]}
                //     if (vWorkHour[idx]==this.breakHour){
                //         vitem.colInfo = {isBreak: true}
                //     }
                //     result.push(vitem)

                // }
                // console.log("header", result);
                // return result;
            },
            onGetStall: function(arg){
                return[
                    {title: 'Surveyor 2', status: 'normal', person: 'Ton', visible: false},
                    {title: 'Surveyor 3', status: 'normal', person: 'And', visible: false},
                    {title: 'SA 1', status: 'normal', person: 'Rul', visible: false},
                    {title: 'SA 2', status: 'normal', person: 'Fir', visible: false},
                    {title: 'SA 3', status: 'normal', person: 'Ron', visible: false},
                    {title: 'SA 4', status: 'normal', person: 'Bud', visible: false},
                    {title: "T\r\nP\r\nS\r\n", status: 'normal', person: 'Dod', visible: true, itemCount: 7},
                ]
            },
            onGetChips: function(arg){
                var vwidth = 7;
                var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
                vArg = JsBoard.Common.extend(vArg, arg);
                var vitems = [{chip: 'produksi', nopol: vArg.nopol, tipe: vArg.tipe, row: -2, col: 0, width: vwidth, selected: 1, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'MAN'},
                    {chip: 'produksi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'MIN'},
                    {chip: 'produksi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 1, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'DOD'},
                    {chip: 'produksi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 9, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'JOK'},
                    {chip: 'produksi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 2, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'DON'},
                    {chip: 'produksi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 10, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'LUD'},
                    {chip: 'produksi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 3, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'FAN'},
                    {chip: 'produksi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 4, col: 4, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'SUD'},
                    {chip: 'produksi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 4, col: 12, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'SBA'},
                    {chip: 'produksi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 4, col: 20, width: vwidth, daydiff: 0, statusicon: ['customer_gray'], status: 'appoinment', sa_name: 'HPO'},
                ]
                // return vitems;
                var today = new Date();
                if (arg.startDate){
                    var vshift = this.dayDiff(arg.startDate, today);
                    var vres = []
                    for (var i=0; i<vitems.length; i++){
                        if (vitems[i].row>=0){
                            vcol = vitems[i].col+vshift*this.listJam.length;
                            if (vcol>=0){
                                vitems[i].col = vcol;
                                vres.push(vitems[i]);
                            }
                        } else {
                            vres.push(vitems[i]);
                        }
                    }
                    return vres;
                }
                return []
            },
            dayDiff: function(date1, date2){
                return Math.round((date2 - date1) / (1000*60*60*24));
            }

            
        }
    });
*/