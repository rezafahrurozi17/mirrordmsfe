angular.module('app')
    .controller('JscbController', function($scope, $http, CurrentUser, Parameter,$timeout, $window, $filter, $state, JscbFactory, AsbEstimasiFactory, AsbGrFactory, AsbProduksiFactory, Idle) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            Idle.unwatch();
            console.log('==== value of idle ==', Idle)
        });
        $scope.$on('$destroy', function(){
            Idle.watch();
        });
        $scope.jscb = {currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza"}
        $scope.jscb.startDate = new Date();
        $scope.jscb.endDate = new Date();
        $scope.jscb.endDate.setDate($scope.jscb.endDate.getDate()+7);
        $scope.jscb.teamList = []
        $scope.jscb.team = '';
        $scope.jscb.displayBoard = false;
        $scope.jscb.boardName = 'jscb-view';
        $scope.jscb.container = 'jscb_tps_view';
        $scope.BpCategory = {}
        $scope.BpGroupList = []
        var allBoardHeight = 500;

        $scope.jscb.updateCategory = function(vcategory){
            console.log('category', vcategory);
            this.BpCategory = vcategory;
            if (this.BpCategory && this.team){
                $scope.jscb.displayBoard = true;
                this.reloadBoard();
            }
        }

        $scope.jscb.updateTeam = function(vteam){
            console.log('team', vteam);
            this.team = vteam;
            if (this.BpCategory && this.team){
                $scope.jscb.displayBoard = true;
                this.reloadBoard();
            }
        }
        
        vconfig = {
            urlCategoryBp: '/api/as/Boards/MGlobal/1012',
            urlGroupList: '/api/as/Boards/BP/GroupList/0',
            tpsLigthOnly: false
        }

        JscbFactory.loadMainData(vconfig, function(vBpCategory, vBpGroupList){
            $scope.BpCategory = vBpCategory;
            $scope.BpGroupList = vBpGroupList;
            console.log("BpCategory", vBpCategory);
            console.log("BpGroupList", vBpGroupList);
        });

        var getTeamList = function(vcat){
            return $scope.BpGroupList;
            var result = []
            for(var i = 0; i<$scope.BpGroupList.length; i++){
                var vitem = $scope.BpGroupList[i];
                if (vitem.BpCategoryId==vcat){
                    var resitem = {name: vitem.GroupName}
                    result.push(vitem);
                }
            }
            return result;
        }

        $scope.jscb.changeOption = function(){
            var vmode = this.BpCategory.type.toLowerCase();
            var vcount = 7;
            if (vmode=='tps'){
                vcount = 7;
                $scope.jscb.columnWidth = 24;
            } else if (vmode=='light'){
                vcount = 7;
                $scope.jscb.columnWidth = 24;
            } else if (vmode=='medium'){
                vcount = 14;
                $scope.jscb.columnWidth = 120;
            } else if (vmode=='heavy'){
                vcount = 30;
                $scope.jscb.columnWidth = 40;
            }
            var vDate = new Date();
            vDate.setDate($scope.jscb.startDate.getDate()+vcount);
            $scope.jscb.endDate = vDate;
            // $scope.jscb.endDate.setDate($scope.jscb.startDate.getDate()+vcount);
        }

        $scope.jscb.reloadBoard = function(){
            me = this;
            $timeout(function(){
                me.changeOption();
                JscbFactory.showBoard($scope.jscb)
            }, 200);

        }


        
    })