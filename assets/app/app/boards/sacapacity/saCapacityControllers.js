angular.module('app')
.controller('SaCapacityController', function($rootScope,$q, $scope, $http, CurrentUser, Parameter,$timeout, $window, $filter, $state, SaCapacityFactory, ngDialog, Idle) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $rootScope.ChatProxy
    $scope.dateOptions = {
        startingDay: 1,
        format: 'dd-MM-yyyy',
        // disableWeekend: 1
    };
    $scope.sac = {currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza"}
    //$scope.sac.currentDate.setHours($scope.sac.currentDate.getHours()+1);
    console.log("$scope.sac.currentDate", $scope.sac.currentDate);
    console.log("$scope.dateOptions", $scope.dateOptions);
    $scope.sac.startDate = new Date();
    $scope.sac.endDate = new Date();
    $scope.sac.endDate.setDate($scope.sac.endDate.getDate()+7);
    var allBoardHeight = 500;

    $scope.$watch('sac.showMode', function(newValue, oldValue) {
        if (newValue !== oldValue) {
            console.log('Changed! '+newValue);
            $scope.sac.reloadBoard();
        }
    });





$scope.user = CurrentUser.user();
// console.log("CurrentUser.user()", CurrentUser.user());

var vmsgname = 'joblist';
var voutletid = $scope.user.OrgId;
var vgroupname = voutletid+'.'+vmsgname;
var vappname = vmsgname;
try {
    $rootScope.ChatProxy.invoke('joinRoom', vgroupname);
} catch (exc){
    // $scope.showMsg = {message: 'Tidak dapat terhubung dengan SignalR server.', title: 'Koneksi SignalR gagal.', closeText: 'Close', exception: exc}
    // ngDialog.openConfirm ({
    //     width: 700,
    //     template: 'app/boards/factories/showMessage.html',
    //     plain: false,
    //     scope: $scope,
    // })
    
}



// $rootScope.tabDoRefresh = function(){
//         var vobj = {
//             mode: $scope.sac.showMode,
//             currentDate: $scope.sac.currentDate,
//             startTime: $scope.sac.startDate,
//             endTime: $scope.sac.endDate,
//             // nopol: $scope.sac.nopol,
//             // tipe: $scope.sac.tipe
//         }
//         showBoardTab(vobj);            
    
// }

$scope.sac.doFilter = function(){
        var vobj = {
            mode: $scope.sac.showMode,
            currentDate: $scope.sac.currentDate,
            startTime: $scope.sac.startDate,
            endTime: $scope.sac.endDate,
            // nopol: $scope.sac.nopol,
            // tipe: $scope.sac.tipe
        }
        showBoardTab(vobj);
}


$scope.$on('SR-'+vappname, function(e, data){
    console.log("notif-data", data);
    var vparams = data.Message.split('.');

    var vJobId = 0;
    if (typeof vparams[0] !== 'undefined'){
        var vJobId = vparams[0];
    }

    var vStatus = 0;
    if (typeof vparams[1] !== 'undefined'){
        var vStatus = vparams[1];
    }


    var jobid = vparams[0];
    var vcounter = vparams[1];
    var vJobList = ["0", "1", "2", "3", "4", "18", "20"];
    var xStatus = vStatus+'';

    if (vJobList.indexOf(xStatus)>=0){
        var vobj = {
            mode: $scope.sac.showMode,
            currentDate: $scope.sac.currentDate,
            startTime: $scope.sac.startDate,
            endTime: $scope.sac.endDate,
            // nopol: $scope.sac.nopol,
            // tipe: $scope.sac.tipe
        }
        showBoardTab(vobj);            
    }
    
});


    var timeActive = true;
    var minuteTick = function(){
        if (!timeActive){
            console.log('time Active');
            return;
        }
        if (SaCapacityFactory.chipLoaded){
            SaCapacityFactory.updateTimeLine();
            $timeout( function(){
                minuteTick();
            }, 60000);
        } else {
            $timeout( function(){
                minuteTick();
            }, 500);
        }
    }

    // var minuteTick = function(){
    //     if (!timeActive){
    //         console.log('time Active');
    //         return;
    //     }
    //     SaCapacityFactory.updateTimeLine();
    //     $timeout( function(){
    //         minuteTick();
    //     }, 60000);
    // }
    $scope.getData = function(){
        return $q.resolve()
    }
    $scope.$on('$viewContentLoaded', function() {
        //$scope.sac.reloadBoard = function(){
            $timeout( function(){
                var vobj = {
                    mode: $scope.sac.showMode,
                    currentDate: $scope.sac.currentDate,
                    startTime: $scope.sac.startDate,
                    endTime: $scope.sac.endDate,
                            // nopol: $scope.sac.nopol,
                    // tipe: $scope.sac.tipe
                }
                showBoardTab(vobj);

                // $timeout( function(){
                //     minuteTick();
                // }, 100);
                minuteTick();
                
            }, 100);
        //}
        Idle.unwatch();
        console.log('==== value of idle ==', Idle)
      });

    $scope.$on('$destroy', function() {
        timeActive = false;
        Idle.watch();
    });


    
    var showBoardTab = function(vitem){
        SaCapacityFactory.showBoard({container: 'sacapacityboard', currentChip: vitem});

    }

    
})