angular.module('app')
    .factory('SaCapacityFactory', function($rootScope, $http, CurrentUser, $filter, $timeout, jpcbFactory, ngDialog) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            onPlot: function(){
                // do nothing
            },
            chipLoaded: false,
            boardName: 'taboard',
            rangeDayHeader: function(vstart, vend){
                var vresult = []
                var xmax = new Date();
                var xstrdate = $filter('date')(xmax, 'yyyy-MM-dd')+' 00:00:00 GMT+00';
                xmax.setDate(vstart.getDate()+30);
                var vfull;
                var xend = vend;
                if (vend>xmax){
                    xend = xmax;
                }
                var xstart = new Date(xstrdate);
                xstart.setDate(vstart.getDate());
                //var xstart = vstart;
                while (xstart<=xend){
                    var vresitem = {}
                    vday = $filter('date')(xstart, 'EEEE');
                    vfull = $filter('date')(xstart, 'yyyy-MM-dd');
                    vresitem['text'] = JsBoard.Common.localDayName(vday);
                    var xdate = $filter('date')(xstart, 'd');
                    vresitem['date'] = xdate;
                    vresitem['fullDate'] = vfull;
                    vresult.push(vresitem);
                    xstart.setDate(xstart.getDate()+1);
                }
                console.log('testing***', vresult);
                return vresult;
            },
                                
            showBoard: function(config){
                var vscrollhpos = jpcbFactory.getScrollablePos(this.boardName);
                this.chipLoaded = false;
                var vcontainer = config.container;
                var vname = this.boardName;
                var vobj = document.getElementById(vcontainer);
                if(vobj){
                    var vwidth = vobj.clientWidth;
                    console.log(vwidth);
                    if (vwidth<=0) {
                        return;
                    }
                } else {
                    return;
                }
                if (typeof config.onPlot=='function'){
                    this.onPlot = config.onPlot;
                }

                var vdurasi = 1;
                if (typeof config.currentChip.durasi !=='undefined'){
                    vdurasi = config.currentChip.durasi;
                }
                config.currentChip.width = (vdurasi/60)*this.incrementMinute;

                var vrow = -2;
                if (typeof config.currentChip.stallId !=='undefined'){
                    if (config.currentChip.stallId>0){
                        vrow = config.currentChip.stallId - 1 ;
                    }
                }
                config.currentChip.row = vrow;

                var vhourStart = JsBoard.Common.getMinute(this.startHour)/60;
                var vcol = 0;
                if (typeof config.currentChip.startTime !=='undefined'){
                    vstartTime = config.currentChip.startTime;
                    vcol = vstartTime.getHours()+vstartTime.getMinutes()/60-vhourStart;
                }
                config.currentChip.col = vcol;
                
                            
                jpcbFactory.createBoard(vname, {
                    board: {
                        chipGroup: 'sacapacity',

                        footerChipName: 'footer',
                        firstColumnChipName: 'stall',
                        firstColumnBaseChipName: 'stall_base',
                        firstHeaderChipName: 'first_header',
                        headerChipName: 'header',
                        headerType: 'simple',
                        headerFill: 'red',
                        footerHeight: 31,
                        
                        columnWidth: 120,
                        rowHeight: 55,
                        headerHeight: 80,
                        // stopageChipTop: 90,
                        scrollableLeft: vscrollhpos,
                        checkDragDrop: function(vchip){
                            return false;
                        }
                    },
                    chip: {
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute
                    }
                });
                var me = this;
                jpcbFactory.setHeader(vname, {
                    onGetHeader: function(arg, vfunc){
                        return me.onGetHeader(arg, vfunc);
                    }
                });

                jpcbFactory.setStall(vname, {
                    firstColumnWidth: 120,
                    onGetStall: function(arg, vfunc){
                        return me.onGetStall(arg, vfunc);
                    }
                });

                // jpcbFactory.setHeaderOnly(vname, {
                //     tinggiHeader: 30,
                //     tinggiSubHeader: 40,
                // });
                
                jpcbFactory.setOnGetChips(vname, function(arg, vfunc){
                    return me.onGetChips(arg, vfunc);
                });

                jpcbFactory.setOnChipLoaded(vname, function(arg, vfunc){
                    return me.onChipLoaded(arg, vfunc);
                });
                
                //jpcbFactory.setSelectionArea(vname, {tinggiArea: 4, fixedHeaderChipName: 'blank'});

                //jpcbFactory.setStopageArea(vname, {tinggiArea: 160});
                

                jpcbFactory.setArgChips(vname, config.currentChip);
                jpcbFactory.setArgHeader(vname, config.currentChip);

                // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

                jpcbFactory.setOnCreateChips(vname, function(vchip){
                    if (vchip.data.selected){
                        vchip.draggable(true);
                    }
                });

                jpcbFactory.setOnBoardReady(vname, function(vjpcb){
                    return me.onBoardReady(vjpcb);
                });
                
                var me = this;
                var noshowUrl = '/api/as/Jobs/SetNoShow';
                var the_config = config;
                jpcbFactory.setOnDragDrop(vname, function(vchip){
                    var vnopol = vchip.data.nopol;
                    var vjobid = vchip.data.JobId;
                    me.showMessage('Apakah Nopol '+vnopol+' akan dijadikan No Show?',
                        function(){
                            $http.put(noshowUrl, [vjobid])
                        },
                        function(){
                            var vlayout = vchip.container.layout;
                            var vstarty = vlayout.row2y(vchip.dragStartPos.row);
                            var vstartx = vlayout.col2x(vchip.dragStartPos.col);

                            vchip.moveTo(vchip.container.board.chips);
                            vchip.x(vstartx);
                            vchip.y(vstarty);
                            vchip.data.col = vchip.dragStartPos.col;
                            vchip.data.row = vchip.dragStartPos.row;
                            vchip.container.updateChip(vchip);
                            vchip.container.redraw();

                        }
                    );

                    // // var vstarty = vchip.dragStartPos.y;
                    // // var vstartx = vchip.dragStartPos.x;
                    // var vlayout = vchip.container.layout;
                    // var vstarty = vlayout.row2y(vchip.dragStartPos.row);
                    // var vstartx = vlayout.col2x(vchip.dragStartPos.col);

                    // vchip.moveTo(vchip.container.board.chips);
                    // vchip.x(vstartx);
                    // vchip.y(vstarty);
                    // vchip.data.col = vchip.dragStartPos.col;
                    // vchip.data.row = vchip.dragStartPos.row;
                    // vchip.container.updateChip(vchip);

                    // vchip.container.redrawBoard();

                    // var curDate = config.currentChip.currentDate;
                    // var vdurasi = 1;
                    // if (typeof config.currentChip.durasi !=='undefined'){
                    //     vdurasi = config.currentChip.durasi;
                    // }

                    // var vmin = me.incrementMinute * vchip.data['col'];
                    // var vminhour = JsBoard.Common.getMinute(me.startHour)+vmin;
                    // var vres = JsBoard.Common.getTimeString(vminhour);
                    // var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
                    // startTime.setMinutes(startTime.getMinutes()+vmin);

                    // var emin = vmin + vdurasi * 60;
                    // var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
                    // endTime.setMinutes(endTime.getMinutes()+emin);

                    // vchip.data.startTime = startTime;
                    // vchip.data.endTime = endTime;
                    // var vStallId = 0;
                    // if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                    //     && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                    //     && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')){
                    //     vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                    // }
                    // vchip.data.stallId = vStallId;

                    // if (typeof me.onPlot=='function'){           

                    //     me.onPlot(vchip.data);
                    // }
                    console.log("on drag-drop vchip", vchip);
                });

                jpcbFactory.showBoard(vname, vcontainer)
                // $timeout(function(){
                //     jpcbFactory.showBoard(vname, vcontainer)

                // }, 100);

            },
            startHour: '08:00',
            endHour: '17:00',
            breakHour: '12:00',
            noShowTolerance: 15, // minutes
            incrementMinute: 60,
            doSetNoShow: function(vjobid){
                var noshowUrl = '/api/as/Jobs/SetNoShow';
                var vname = this.boardName;
                $http.put(noshowUrl, [vjobid]).then(function(response) {
                    //jpcbFactory.redrawBoard(vname);
                    if (typeof $rootScope.tabDoRefresh !== 'undefined'){
                        $timeout( function(){
                            $rootScope.tabDoRefresh();
                        }, 2000);
                    }
                });

            },
            timeToColumn: function(vtime){
                
                var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vmin = JsBoard.Common.getMinute(vtime)-vstartmin;
                var vresult = vmin/this.incrementMinute;
                
                return vresult;
            },
            onBoardReady: function(vjpcb){
                this.updateTimeLine(vjpcb.boardName);
            },
            checkAppointmentStatus: function(vtime, vstatus){
                
                // vtime: time-nya data
                // currentHour: jam/minute saat ini
                if ((vstatus==1) || (vstatus==2)){
                    return 'no_show';
                }
                var vDate = this.getCurrentTime();
                console.log("current Time#####", vDate.getTime());
                var currentHour = $filter('date')(vDate, 'HH:mm');

                var vcurrmin = JsBoard.Common.getMinute(currentHour);
                var vmin = JsBoard.Common.getMinute(vtime);
                //if ((vmin<vcurrmin) && (vstatus==0)){
                if (vstatus==0){
                    return 'no_show';
                }
                // console.log("***vstatus***", vstatus);
                return 'appointment';

            },
            onGetHeader: function(arg, onFinish){
                // varg = JsBoard.Common.extend({startDate: new Date(), dateCount: 1}, arg);
                // var vWorkHour = JsBoard.Common.generateDayHours({
                //     startHour: this.startHour,
                //     endHour: this.endHour,
                //     increment: this.incrementMinute,
                // });
                // var result = [];
                // for(var idx in vWorkHour){
                //     var vitem = {title: vWorkHour[idx]}
                //     if (vWorkHour[idx]==this.breakHour){
                //         vitem.colInfo = {isBreak: true}
                //     }
                //     result.push(vitem)

                // }

                var xtgl = $filter('date')(arg.startTime, 'yyyy-MM-dd')+' 00:00:00 GMT+00';
                var vstart = new Date(xtgl);
                if(typeof arg.endTime=='undefined'){
                    var vend = new Date(xtgl);
                    vend.setDate(vend.getDate()+7);
                    
                } else {
                    var xtgl2 = $filter('date')(arg.endTime, 'yyyy-MM-dd')+' 00:00:00 GMT+00';
                    var vend = new Date(xtgl2);
                }
                
                

                result = this.rangeDayHeader(vstart, vend);

                this.listHeaderIdx = {}
                for(var i=0; i<result.length; i++){
                    var vitem = result[i];
                    this.listHeaderIdx[vitem.fullDate] = i;
                }
                
                console.log("header", result);
                $timeout(function(){
                    onFinish(result);
                }, 100);
                
            },

            getDayHours: function(vstart, vend){
                var vHours = JsBoard.Common.generateDayHours({
                    startHour: vstart,
                    endHour: vend,
                increment: 60});
                var vresult = []
                for (var i=0; i<vHours.length; i++){
                    vresult.push({title: vHours[i], status: 'normal', stallId: i});
                }
                return vresult;
            },

            listStallIdx: {},
            listHeaderIdx: {},
                
            onGetStall: function(arg, onFinish){
                var result = this.getDayHours(this.startHour, this.endHour);

                this.listStallIdx = {}
                for(var i=0; i<result.length; i++){
                    var vitem = result[i];
                    this.listStallIdx[vitem.title] = i;
                }
                onFinish(result);

                // // $http.get('/api/as/Stall').
                // $http.get('/api/as/Boards/Stall/0').then(function(res){
                //     var xres = res.data.Result;
                //     var result = [];
                //     for(var i=0;  i<xres.length; i++){
                //         var vitem = xres[i];
                //         result.push({
                //             title: vitem.Name,
                //             status: 'normal',
                //             stallId: vitem.StallId
                //         });
                //     }
                //     onFinish(result);
                // })

            },
            findStallIndex: function(vStallId, vList){
                for (var i=0; i<vList.length; i++){
                    var vitem = vList[i];
                    if ((typeof vitem.stallId!=='undefined') && (vitem.stallId==vStallId)){
                        return i;
                    }
                }
                return -1;
            },
            onChipLoaded: function(vjpcb){
                // var vresult = []
                // for (var i=0; i<vjpcb.allChips.length; i++){
                //     var vitem = vjpcb.allChips[i];
                //     vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                //     var vpush = true;

                //     if ((vitem.itemStatus==1) || (vitem.itemStatus==2)){
                //         vitem.row = -1;
                //     } else {
                //         if (vidx>=0){
                //             vitem.row = vidx;
                //         } else {
                //             vpush = false;
                //         }
                //     }
                //     if (vpush){
                //         vresult.push(vitem);
                //     }
                    
                // }
                // vjpcb.allChips = vresult;
                var vresult = [];
                for (var i=0; i<vjpcb.allChips.length; i++){
                    var vitem = vjpcb.allChips[i];
                    var vpush = true;
                    if (typeof this.listHeaderIdx[vitem.fullDate] !== 'undefined'){
                        vitem.col = this.listHeaderIdx[vitem.fullDate];
                    } else {
                        vpush = false;
                    }
                    // if (typeof this.listStallIdx[vitem.jam] !== 'undefined'){
                    //     vitem.row = this.listStallIdx[vitem.jam];
                    // } else {
                    //     vitem.row = 0;
                    // }
                    var vminstart = JsBoard.Common.getMinute(this.startHour)/60;
                    var vminend = JsBoard.Common.getMinute(this.endHour)/60;
                    var vminjam = JsBoard.Common.getMinute(vitem.jam)/60;
                    if ((vminjam>=vminstart) && (vminjam<=vminend)){
                        vitem.row = vminjam - vminstart;
                    } else {
                        vpush = false;
                    }

                    if (vpush){
                        vresult.push(vitem);
                    }

                }

                vjpcb.allChips = vresult;

                this.chipLoaded = true;
            },
            onGetChips: function(arg, onFinish){
                var vArg = {nopol: 'B 1000 AAA', tipe: 'Avanza'}
                vArg = JsBoard.Common.extend(vArg, arg);
                
                var tglAwal = arg.startTime;
                var tglAkhir = arg.endTime;
                //tglAkhir.setDate(tglAkhir.getDate());
                var vurl = '/api/as/Boards/BP/SaCapacity/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/'+$filter('date')(tglAkhir, 'yyyy-MM-dd');
                var me = this;
                $http.get(vurl).then(function(res){
                    // var result = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, 
                    //     daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}];
                    var result = [];
                    var xres = res.data.Result;
                    for(var i=0;  i<xres.length; i++){
                        var vitem = xres[i];
                        var vtgl = $filter('date')(vitem.DateStart, 'yyyy-MM-dd');
                        var vtglF = $filter('date')(vitem.DateStart, 'dd/MM/yy');
                        var vmin = JsBoard.Common.getMinute(vitem.TimeStart);
                        var vjam = JsBoard.Common.getTimeString(vmin);
    
                        result.push({
                            chip: 'sachip', 
                            row: 0,
                            col: 0, 
                            // timestatus: 1, 
                            width: 1, 
                            stallId: i,
                            fullDate: vtgl,
                            jam: vjam,
                            dispDate: vtglF,
                            // normal: 1, ext: 0, 
                            // day: 1,
                            nopol: vitem.PoliceNumber, 
                            // day: 0, 
                            status: vitem.ActionType,
                            type: vitem.ModelType, 
                            // pekerjaan: 'EM', teknisi: 'DON',
                            start: '08:00',
                            SAName: vitem.SAName
                        });



                        // var vstatus = 'no_show';
                        
                        // // var vstatus = me.checkAppointmentStatus(vitem.AppointmentTime);
                        // // var vTime = vitem.PlanStart;
                        // var vTime = vitem.AppointmentTime;
                        // var vstall = vitem.StallId;
                        // if((vitem.Status==1) || (vitem.Status==2)){
                        //     // console.log("**vitem**", vitem);
                        //     // vstatus = 'no_show';
                        //     vstall = -1;
                        // }
                        // console.log("**vitem**", vitem);
                        // vstatus = me.checkAppointmentStatus(vTime, vitem.Status);
                        // var vicon = 'customer';
                        // if (vstatus=='appointment'){
                        //     vicon = 'customer_gray';
                        // }
                        // var vcolumn = me.timeToColumn(vTime);
                        // //vstatus = 'no_show';
                        // if (vitem.Status!==1){
                        //     result.push({
                        //         chip: 'tab', 
                        //         // AppointmentTime: vitem.AppointmentTime,
                        //         JobId: vitem.JobId,
                        //         AppointmentTime: vTime,
                        //         nopol: vitem.PoliceNumber, 
                        //         itemStatus: vitem.Status,
                        //         tipe: vitem.ModelType, 
                        //         stallId: vstall,
                        //         row: 0, col: vcolumn, 
                        //         width: 1, 
                        //         daydiff: 0, 
                        //         status: 'no_show', 
                        //         statusicon: [vicon]
                        //     });
                        // }



                    }
                    onFinish(result);
                })
                

            },

            getCurrentTime: function(){
                return jpcbFactory.getCurrentTime();
            },
            updateChipStatus: function(){
                var boardname = this.boardName;
                var me = this;
                var vboard = jpcbFactory.getBoard(boardname);
                jpcbFactory.processChips(boardname, function(vchip){
                    return true;
                }, function(vchip){
                    // vchip.data.status = 'appointment';
                    // vboard.updateChip(vchip);
                    // //var vitem = vchip.data;
                    // //vitem.status=='appointment';
                    // return;
                    var vitem = vchip.data;
                    var vstatus = me.checkAppointmentStatus(vitem.AppointmentTime, vitem.itemStatus);

                    var vDate = me.getCurrentTime();
                    var currentHour = $filter('date')(vDate, 'HH:mm');
                    vapptime = JsBoard.Common.getMinute(vitem.AppointmentTime);
                    var vdelta = me.noShowTolerance;
                    var vcurrmin = JsBoard.Common.getMinute(currentHour);
                    if ((vstatus=='no_show') && (vitem.stallId!==-1)){
                        // console.log(vitem);
                        // console.log('appointment time', vapptime);
                        // console.log('current time', vcurrmin);
                        if (vapptime+vdelta<vcurrmin){
                            //vstatus = 'do_no_show';
                            vitem.stallId = -1;
                            me.doSetNoShow(vitem.JobId);
                        }
                    }

                    if (vitem.status !== vstatus){
                        vitem.status = vstatus;
                        vboard.updateChip(vchip);
                    }
                    if (vitem.status=='no_show'){
                        vchip.draggable(true);
                    }

                });

            },
            updateTimeLine: function(){
                // var boardname = this.boardName;
                // var vDate = this.getCurrentTime();
                // console.log("current Time#####", vDate.getTime());
                // var currentHour = $filter('date')(vDate, 'HH:mm');
                
                // console.log("****currentHour", currentHour);
                // //var startHour = this.
                // //var minutPerCol

                // jpcbFactory.updateTimeLine(boardname, currentHour, this.startHour, this.incrementMinute);

                // //this.updateChipStatus();

                // jpcbFactory.redrawBoard(boardname);
            },

            showMessage: function(vmessage, onOk, onCancel){
                $rootScope.tab = {message: vmessage, title: 'Konfirmasi No Show', okText: 'Ya', cancelText: 'Tidak'}

                ngDialog.openConfirm ({
                    width: 700,
                    template: 'app/boards/tab/tabMessage.html',
                    plain: false,
                    scope: $rootScope,
                })
                .then(function(value) {
                    if (onOk){
                        return onOk(value);
                    }
                }, function(reason) {
                    if (onCancel){
                        return onCancel(reason);
                    }
                });                

            },
            // onGetChips: function(arg){
            //     var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
            //     vArg = JsBoard.Common.extend(vArg, arg);
            //     var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     ]
            //     var today = new Date();
            //     if (arg.currentDate){
            //         var vdiff = this.dayDiff(today, arg.currentDate);
            //         var vres = []
            //         for (var i=0; i<vitems.length; i++){
            //             if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
            //                 vres.push(vitems[i]);
            //             }
            //         }
            //         return vres;
            //     }
            //     return []
            // },
            dayDiff: function(date1, date2){
                return Math.round((date2 - date1) / (1000*60*60*24));
            }

            
        }
    });
