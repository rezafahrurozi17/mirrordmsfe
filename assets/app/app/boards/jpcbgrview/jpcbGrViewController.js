angular.module('app')
    .controller('JpcbGrViewController', function($rootScope, $scope, $http, CurrentUser, $interval, Parameter, $timeout, $window, $filter, $state, JpcbGrViewFactory, ngDialog, Idle) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            Idle.unwatch();
            console.log('==== value of idle ==', Idle)
        });
        $scope.$on('$destroy', function(){
            Idle.watch();
        });
        var isBoardLoaded = false;
        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd-MM-yyyy',
            // disableWeekend: 1
        };
        var boardName = 'jpcbgrview';
        // $scope.jpcbgrview = {currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza"}
        $scope.jpcbgrview = { currentDate: new Date() }
            //$scope.jpcbgrview.currentDate.setHours($scope.asb.currentDate.getHours()+1);
            // console.log("$scope.jpcbgrview.currentDate", $scope.jpcbgrview.currentDate);
            // console.log("$scope.dateOptions", $scope.dateOptions);
        $scope.jpcbgrview.startDate = new Date();

        $scope.jpcbgrview.stallVisible = {}
        $scope.jpcbgrview.tempStallVisible = {}
        $scope.jpcbgrview.testing = "TESTING";

        var vlong1 = 'col-xs-9';
        var vlong2 = 'col-xs-12';
        $scope.isLoading = true;
        $scope.wcolumn = vlong2;
        $scope.ncolumn = 'col-xs-3';
        $scope.ncolvisible = 0;
        $scope.filter = [{
            Id:0,
            name:"Title Header",
            visible:true
        },{
            Id:1,
            name:"Job Stoppage",
            visible:true
        }
        ]
        $scope.heightForStoppage = 80;


        // $scope.jpcbgrview.endDate = new Date();
        // $scope.jpcbgrview.endDate.setDate($scope.jpcbgrview.endDate.getDate()+7);
        // var allBoardHeight = 500;

        // $scope.$watch('jpcbgrview.showMode', function(newValue, oldValue) {
        //     if (newValue !== oldValue) {
        //         console.log('Changed! '+newValue);
        //         $scope.jpcbgrview.reloadBoard();
        //     }
        // });

        var scrollable_hpos = 0;


        $scope.user = CurrentUser.user();
        // console.log("CurrentUser.user()", CurrentUser.user());

        var vmsgname = 'joblist';
        var voutletid = $scope.user.OrgId;
        var vgroupname = voutletid + '.' + vmsgname;
        var vappname = vmsgname;
        try {
            $rootScope.ChatProxy.invoke('joinRoom', vgroupname);
        } catch (exc) {
            console.log(exc);
            // $scope.showMsg = {message: 'Tidak dapat terhubung dengan SignalR server.', title: 'Koneksi SignalR gagal.', closeText: 'Close', exception: exc}
            // ngDialog.openConfirm ({
            //     width: 700,
            //     template: 'app/boards/factories/showMessage.html',
            //     plain: false,
            //     scope: $scope,
            // })

        }

        $scope.$on('SR-' + vappname, function(e, data) {
            console.log("notif-data", data);
            var vparams = data.Message.split('.');

            var vJobId = 0;
            if (typeof vparams[0] !== 'undefined') {
                var vJobId = vparams[0];
            }

            var vStatus = 0;
            if (typeof vparams[1] !== 'undefined') {
                var vStatus = vparams[1];
            }


            var jobid = vparams[0];
            var vcounter = vparams[1];
            var vJobList = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "20"];
            var xStatus = vStatus + '';

            if (vJobList.indexOf(xStatus) >= 0) {
                showBoardJPCB();
            }

        });

        $scope.jpcbgrview.reloadBoard = function() {
            showBoardJPCB();
        }

        

        // untuk menampilkan garis waktu saat ini (timeline)
        var timeActive = true;
        var minuteTick = function() {
            if (!timeActive) {
                return;
            }

            JpcbGrViewFactory.updateTimeLine(boardName);
            $timeout(function() {
                minuteTick();
            }, 60000);
        }

        var ReloadBoard30s = function() {
            if (!timeActive) {
                return;
            }

            // JpcbGrViewFactory.updateTimeLine(boardName);
            // showBoardJPCB();
            $timeout(function() {
                ReloadBoard30s();
            }, 45000);
        }

        // $scope.$on('$viewContentLoaded', function() {
        //     //$scope.asb.reloadBoard = function(){
        //         $timeout( function(){
        //             var vobj = {
        //                 mode: $scope.jpcbgrview.showMode,
        //                 currentDate: $scope.jpcbgrview.currentDate,
        //                 nopol: $scope.jpcbgrview.nopol,
        //                 tipe: $scope.jpcbgrview.tipe
        //             }
        //             showBoardJPCB(vobj);

        //             $timeout( function(){
        //                 minuteTick();
        //             }, 100);

        //         }, 100);
        //     //}
        // });

        $scope.$on('$viewContentLoaded', function() {


            //Menggunakan resize detector untuk mengatasi bug board tidak muncul setelah menu di-close dan diopen lagi.
            // elemen yang dimonitor adalah panel yang width-nya 100%
            var velement = document.getElementById('jpcb_gr_view_panel');
            var erd = elementResizeDetectorMaker();
            erd.listenTo(velement, function(element) {
                if (!isBoardLoaded) {
                    isBoardLoaded = true;
                    showBoardJPCB();
                    minuteTick();
                    ReloadBoard30s();
                    // untuk menampilkan timeline
                    // $timeout( function(){
                    //     minuteTick();
                    // }, 1000);
                }
                vboard = JpcbGrViewFactory.getBoard(boardName);
                if (vboard) {
                    vboard.autoResizeStage();
                    vboard.redraw();
                }

            });

        });


        $scope.$on('$destroy', function() {
            timeActive = false;
        });


        var loadStallVisible = function() {
            for (var stallid in $scope.jpcbgrview.stallVisible) {
                var vitem = $scope.jpcbgrview.stallVisible[stallid];
                $scope.jpcbgrview.tempStallVisible[stallid] = { visible: vitem.visible, title: vitem.title }
            }
        }
        var updateStallVisible = function() {
            for (var stallid in $scope.jpcbgrview.tempStallVisible) {
                var vitem = $scope.jpcbgrview.tempStallVisible[stallid];
                $scope.jpcbgrview.stallVisible[stallid] = { visible: vitem.visible, title: vitem.title }
            }
        }

        $scope.showOptions = function(vmode) {
            if (vmode) {
                loadStallVisible();
                $scope.wcolumn = vlong1;
                $scope.ncolumn = 'col-xs-3';
                $scope.ncolvisible = 1;
            } else {
                $scope.wcolumn = vlong2;
                $scope.ncolvisible = 0;
            }
        }

        $scope.saveOptions = function(param) {
            console.log("paramm===>",param);
            
            updateStallVisible();
            this.showOptions(false);
            if(param[0].visible == false){
                $('#jpcb_gr_view_panel .panel-heading').css('display','none');
            }else{
                $('#jpcb_gr_view_panel .panel-heading').css('display','block');
            }
            if(param[1].visible == false){    
                $scope.heightForStoppage = 0;
                showBoardJPCB(param[1].name);
            }else{
                $scope.heightForStoppage = 80;
                showBoardJPCB();
            }
        }

        // var vshowOptions = function(){
        //     ngDialog.openConfirm ({
        //       width: 400,
        //       template: 'app/boards/jpcbgrview/options.html',
        //       plain: false,
        //       showClose: false,
        //       controller: 'JpcbGrViewController',
        //       //scope: $scope
        //     });

        // }
        // $scope.showOptions = function(){
        //     loadStallVisible();
        //     this.jpcbgrview.displayOptions = 1;
        //     // var vboard = JpcbGrViewFactory.getBoard(boardName);
        //     // console.log(vboard);
        //     // this.testing = "TESTING!!!";
        //     //this.$apply();
        //     // $timeout(function(){
        //     //     vshowOptions();
        //     // })
        // }

        // $scope.asb.FullScreen = function(){
        //     // alert('full screen');
        //     var vobj = document.getElementById('asb_estimasi_view');
        //     vobj.style.cssText = 'position: absolute; left: 0; top: 0; width: 100%; height: 100%;';
        // }


        // angular.element($window).bind('resize', function () {
        //     //AsbEstimasiFactory.resizeBoard();
        //     // AsbProduksiFactory.resizeBoard();

        //     // jpcbStopage.autoResizeStage();
        //     // jpcbStopage.redraw();
        // });
        var vNow = $filter('date')(new Date(), 'yyyy-MM-dd');
        var urlTechnician = '/api/as/Boards/jpcb/employee/' + vNow + '/TGR';
        $scope.ListTechnician = [];
        $http.get(urlTechnician).then(function(resp) {
            $scope.ListTechnician = [];
            var vdata = resp.data.Result;
            console.log("vdata", vdata);
            var countTek = 0;
            // $scope.countTechnician = vdata.length;
            for (var i = 0; i < vdata.length; i++) {
                var vitem = vdata[i];
                // if(vdata[i].Attend == 1 && vdata[i].Available == 1){
                    countTek+=1;
                // }
                //var vname = getPeopleName(vitem);
                $scope.ListTechnician.push({ id: vitem.EmployeeId, name: vitem.EmployeeName });
            }
            $scope.countTechnician = countTek;
        });


        var showBoardJPCB = function(param) {
                var dialogOpen = false;
                var automationHeight = 200;
                if(param !== undefined){
                    automationHeight=0;
                }
                var vscrollhpos = JpcbGrViewFactory.getScrollablePos(boardName);
                console.log("vscrollhpos", vscrollhpos);
                var vitem = {
                    // mode: $scope.jpcbgrview.showMode,
                    currentDate: $scope.jpcbgrview.currentDate,
                    stallVisible: $scope.jpcbgrview.stallVisible,
                    stallUrl: '/api/as/Boards/Stall/1',
                    chipUrl: '/api/as/Boards/jpcb/bydate/' + $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd') + '/0/17',
                    // nopol: $scope.jpcbgrview.nopol,
                    // tipe: $scope.jpcbgrview.tipe
                }
                console.log("showBoardJPCB vitem", vitem);
                JpcbGrViewFactory.showBoard(boardName, {
                    container: 'jpcb_gr_view',
                    currentChip: vitem,
                    options: {
                        standardDraggable: true,
                        // draggableStatus: ["4", "6"],
                        draggableStatus: [],
                        // stopageAreaHeight: 200,
                        // stopageAreaHeight: automationHeight,     
                        stopageAreaHeight: $scope.heightForStoppage,
                        // stopageAreaHeight: 80,                        
                        //testing12345: 1000,
                        onJpcbReady: function(vjpcb) {
                            console.log("*****board ready", vjpcb);
                            $scope.isLoading = false;
                            $scope.isBoardLoading = false;

                            $scope.countNoShow = vjpcb.argChips.countNoShow;
                            $scope.countBookingA = vjpcb.argChips.countBookingA;
                            $scope.countBookingB = vjpcb.argChips.countBookingB;
                            $scope.countCarryOver = vjpcb.argChips.countCarryOver;
                            $scope.countInProgress = vjpcb.argChips.countInProgress;
                            $scope.countProductionCapacity = vjpcb.argChips.countProductionCapacity;

                            vjpcb.theBoard.layout.board.checkDragDrop = function(vchip, vcol, vrow) {
                                console.log("dragdrop JPCB");

                                if (vchip.data.row >= 0) {
                                    vlayout = vjpcb.theBoard.layout;
                                    if (typeof vlayout.infoRows[vchip.data.row] != 'undefined') {
                                        var vStallInfo = vlayout.infoRows[vchip.data.row];
                                        var vEnd = vStallInfo.data.endHour;
                                        var vjamstart = JpcbGrViewFactory.columnToTime(vcol);
                                        if (vjamstart >= vEnd) {
                                            return false;
                                        }
                                        //if (vchip.data.Final)

                                    }
                                } else {
                                    return false;
                                }

                                return true;
                            };

                            // alert('ready');
                        },
                        board: {
                            scrollableLeft: vscrollhpos,
                        },
                    },
                    onChipClick: function(vchip) {
                        console.log('data', vchip.currentTarget.data);
                        $scope.jpcbMsg = vchip.currentTarget.data;
                        if ($scope.jpcbMsg.PlanStart !== null) {
                            var tmpPlantStart = $scope.jpcbMsg.PlanStart;
                            tmpPlantStart = tmpPlantStart.split(":");
                            $scope.jpcbMsg.PlanStart = tmpPlantStart[0] + ":" + tmpPlantStart[1];
                        }
                        if ($scope.jpcbMsg.PlanFinish !== null) {
                            var tmpPlanFinish = $scope.jpcbMsg.PlanFinish;
                            tmpPlanFinish = tmpPlanFinish.split(":");
                            $scope.jpcbMsg.PlanFinish = tmpPlanFinish[0] + ":" + tmpPlanFinish[1];
                        }
                        $scope.$on('ngDialog.closing', function() {
                            dialogOpen = false;
                        });
                        if (!dialogOpen) {
                            dialogOpen = true;
                            ngDialog.openConfirm({
                                width: 400,
                                template: '<div class="panel">' +
                                    '    <div class="panel-heading">' +
                                    '        <h3 class="panel-title">{{jpcbMsg.nopol}} / {{jpcbMsg.type}}</h3></div>' +
                                    '    <div class="panel-body">' +
                                    '	<table>' +
                                    '	  <tr>' +
                                    '		<td>Jam Alokasi &nbsp;</td>' +
                                    '		<td>{{jpcbMsg.PlanStart}} - {{jpcbMsg.PlanFinish}}</td>' +
                                    '	  </tr>' +
                                    '	  <tr>' +
                                    '		<td>Pekerjaan</td>' +
                                    '		<td>{{jpcbMsg.pekerjaan}}</td>' +
                                    '	  </tr>' +
                                    '	</table>' +
                                    '    </div>' +
                                    '    <br/>' +
                                    '	<button type="button " class="ngdialog-button ngdialog-button-secondary " ng-click="closeThisDialog(0) ">OK</button>' +
                                    '</div>',
                                plain: true,
                                scope: $scope,
                            })
                        }


                    },
                });
            }
            // var showBoardTab = function(vitem){
            //     JpcbGrViewFactory.showBoard({container: 'jpcb_gr_view', currentChip: vitem});

        // }

        $interval(function() {
            showBoardJPCB();
        },20000 );


    })