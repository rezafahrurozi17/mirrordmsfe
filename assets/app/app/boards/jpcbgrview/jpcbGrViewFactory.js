angular.module('app')
    .factory('JpcbGrViewFactory', function ($http, CurrentUser, $filter, $timeout, jpcbFactory) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        var tmpDataChips = [];
        var tmpDataStall = [];
        var cekTimeline = 0;
        var listStallDispacthUlang = [];
        var chipExpiredBoard;
        // var tempVjpcb = {};
        // var tempVjpcbBoard = [];
        var tempOnGetChip = null;
        var ChipOvertime = 0;
        var TotalOvertime = 0;
        var repairProsesBP = 0;
        
        var CopyVNAME = null;
        var CopyVCONTAINER = null;
        var keywordNopol = null;
        var CopyTglAwal = null;
        var dataSemuaChip = null;
        var tmpDurasiStall = 0;

        return {
            // hourDelay: 2.2,
            // minuteDelay: 6*60+0,
            // minuteDelay: 0,
            // onPlot: function(){
            //     // do nothing
            // },
            //boardName: 'jpcbviewboard',
            showBoard: function (vname, config, chipExpired) {
                console.log('apakah ini bisa mendeteksi BP GR?', vname,config)
                if (vname == 'repairprocessbpboard') {
                    repairProsesBP = 1;
                }

                var topStoppage = 90;
                if (vname == 'jpcbgrview'){
                    console.log('masuk lah sini uuu')
                    topStoppage = 35;
                }
                chipExpiredBoard = chipExpired;
                console.log("chip Expired", chipExpiredBoard);
                if (!config.options) {
                    config.options = {}
                }
                var vcontainer = config.container;

                //copyvname copyvcontainer buat search stoppage
                CopyVNAME = vname;
                CopyVCONTAINER = vcontainer;


                //var vname = this.boardName;
                var vobj = document.getElementById(vcontainer);
                if (vobj) {
                    var vwidth = vobj.clientWidth;
                    // console.log(vwidth);
                    console.log('known obj', vwidth);
                    if (vwidth <= 0) {
                        return;
                    }
                } else {
                    console.log('unknown obj');
                    return;
                }
                if (typeof config.onPlot == 'function') {
                    this.onPlot = config.onPlot;
                }

                var vdurasi = 1;
                if (typeof config.currentChip.durasi !== 'undefined') {
                    vdurasi = config.currentChip.durasi;
                }
                config.currentChip.width = (vdurasi / 60) * this.incrementMinute;

                var vrow = -2;
                if (typeof config.currentChip.stallId !== 'undefined') {
                    if (config.currentChip.stallId > 0) {
                        vrow = config.currentChip.stallId - 1;
                    }
                }
                config.currentChip.row = vrow;

                var vhourStart = JsBoard.Common.getMinute(this.startHour) / 60;
                var vcol = 0;
                if (typeof config.currentChip.startTime !== 'undefined') {
                    vstartTime = config.currentChip.startTime;
                    vcol = vstartTime.getHours() + vstartTime.getMinutes() / 60 - vhourStart;
                }
                config.currentChip.col = vcol;

                var vColWidth = 120;
                var voptions = {
                    boardName: vname,
                    board: {
                        chipGroup: 'jpcbv',
                        columnWidth: vColWidth,
                        rowHeight: 75,
                        footerChipName: 'footer',
                        // stopageChipTop: 90,
                        stopageChipTop: topStoppage,
                        snapDivider: 4
                    },
                    chip: {
                        columnWidth: vColWidth,
                        startHour: this.startHour,
                        incrementMinute: this.incrementMinute
                    }
                }
                xvoptions = JsBoard.Common.extend(voptions, config.options);
                xvoptions.board = JsBoard.Common.extend(voptions.board, config.options.board);
                jpcbFactory.createBoard(vname, xvoptions);
                var me = this;
                jpcbFactory.setHeader(vname, {
                    headerHeight: 40,
                    onGetHeader: function (arg, vfunc) {
                        return me.onGetHeader(arg, vfunc);
                    }
                });
                jpcbFactory.setStall(vname, {
                    firstColumnWidth: 120,
                    onGetStall: function (arg, vfunc) {
                        return me.onGetStall(arg, vfunc);
                    }
                });

                jpcbFactory.setOnGetChips(vname, function (arg, vfunc) {
                    return me.onGetChips(arg, vfunc);
                });

                jpcbFactory.setOnChipLoaded(vname, function (vjpcb) {
                    return me.onChipLoaded(vjpcb);
                });

                jpcbFactory.setOnBoardReady(vname, function (vjpcb) {
                    return me.onBoardReady(vjpcb);
                });

                jpcbFactory.setSelectionArea(vname, { tinggiArea: 4, fixedHeaderChipName: 'blank' });

                if (config.options.stopageAreaHeight) {
                    jpcbFactory.setStopageArea(vname, { tinggiArea: config.options.stopageAreaHeight });
                    config.currentChip.showStoppage = 1;
                } else {
                    config.currentChip.showStoppage = 0;
                }


                jpcbFactory.setArgChips(vname, config.currentChip);
                jpcbFactory.setArgStall(vname, config.currentChip);

                if (config.onChipClick) {
                    jpcbFactory.setOnCreateChips(vname, function (vchip) {
                        vchip.on('click', config.onChipClick);
                    });
                }

                // jpcbFactory.setBlockRow(vname, [{row: 3, col: 0}]);

                // if (config.standardDraggable){
                //     jpcbFactory.setOnCreateChips(vname, function(vchip){
                //         if (vchip.data.nopol=='B 7002 AA'){
                //             if (false){

                //             }
                //         }
                //         var vdrag = false;
                //         if ((typeof vchip.data !== 'undefined') && (typeof vchip.container.layout!=='undefined')){
                //             var vlayout = vchip.container.layout;
                //             if (vchip.data.col>=vlayout.timeLine){
                //                 vdrag = true;
                //             }
                //         }
                //         vchip.draggable(vdrag);

                //         // if (vchip.data.selected){
                //         //     vchip.draggable(true);
                //         // }
                //     });
                // }

                var me = this;
                var the_config = config;
                var tempstallTracking = null;
                var tempVchipTracking = null;
                jpcbFactory.setOnDragDrop(vname, function (vchip) {
                    tempstallTracking = angular.copy(vchip.data.stallId);
                    tempVchipTracking = angular.copy(vchip);
                    console.log('ini tracking kampang', tempstallTracking);
                    var curDate = config.currentChip.currentDate;
                    var vdurasi = 1;
                    if (typeof config.currentChip.durasi !== 'undefined') {
                        vdurasi = config.currentChip.durasi;
                    }

                    var vmin = me.incrementMinute * vchip.data['col'];
                    var vminhour = JsBoard.Common.getMinute(me.startHour) + vmin;
                    var vres = JsBoard.Common.getTimeString(vminhour);
                    var startTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
                    startTime.setMinutes(startTime.getMinutes() + vmin);

                    var emin = vmin + vdurasi * 60;
                    var endTime = new Date($filter('date')(curDate, 'yyyy-MM-dd'));
                    endTime.setMinutes(endTime.getMinutes() + emin);

                    vchip.data.startTime = startTime;
                    vchip.data.endTime = endTime;
                    var vStallId = 0;
                    if ((typeof vchip.data.row !== 'undefined') && (typeof vchip.container.layout.infoRows[vchip.data.row] !== 'undefined')
                        && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data !== 'undefined')
                        && (typeof typeof vchip.container.layout.infoRows[vchip.data.row].data.stallId !== 'undefined')) {
                        vStallId = vchip.container.layout.infoRows[vchip.data.row].data.stallId;
                    }
                    vchip.data.stallId = vStallId;

                    var fromstoppage = 0;
                    if(tempVchipTracking.data.isInStopPageGr == 1){
                        //dari stoppage
                        console.log('Mindahin chip dari stoppage', tempVchipTracking);
                        console.log('tempOnGetChip', tempOnGetChip);
                        fromstoppage = 1;
                        for (var i=0; i < tempOnGetChip.length; i++){
                            if (tempVchipTracking.data.nopol == tempOnGetChip[i].PoliceNumber){

                                if (tempOnGetChip[i].JobSplitChip != null && tempOnGetChip[i].JobSplitChip != undefined && tempOnGetChip[i].JobSplitChip.length > 0){
                                // if (tempOnGetChip[i].JobSplitChip.length > 0){
                                    var dateNow = me.changeFormatDateStrip(CopyTglAwal);
                                    var diffDate = '';
                                    for(var z in tempOnGetChip[i].JobSplitChip){
                                        diffDate = me.changeFormatDateStrip(tempOnGetChip[i].JobSplitChip[z].PlanDateStart)
                                        if(diffDate == dateNow ){
                                            tempOnGetChip[i].StallId = tempOnGetChip[i].JobSplitChip[z].StallId;
                                            tempOnGetChip[i].PlanStart = tempOnGetChip[i].JobSplitChip[z].PlanStart;
                                            tempOnGetChip[i].PlanFinish = tempOnGetChip[i].JobSplitChip[z].PlanFinish;
                                            tempOnGetChip[i].PlanDateStart = tempOnGetChip[i].JobSplitChip[z].PlanDateStart;
                                            tempOnGetChip[i].PlanDateFinish = tempOnGetChip[i].JobSplitChip[z].PlanDateFinish;
                                            var planStart = me.timeToColumn2(tempOnGetChip[i].PlanStart);
                                            var actualFinish =  me.timeToColumn2(tempOnGetChip[i].PlanFinish);
                                            tempOnGetChip[i].JumlahActualRate = actualFinish - planStart;
                                            tempOnGetChip[i].JumlahActualRate = Math.ceil(Math.abs(tempOnGetChip[i].JumlahActualRate)*4)/4;
                                            tempOnGetChip[i].Extension = tempOnGetChip[i].JobSplitChip[z].Extension;

                                            var cekAwal = tempOnGetChip[i].JobSplitChip[z].PlanStart.split(':');
                                            var cekAkhir = tempOnGetChip[i].JobSplitChip[z].PlanFinish.split(':');
                                            if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                                                // tempOnGetChip[i].JumlahActualRate = tempOnGetChip[i].JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                                                // tempOnGetChip[i].JumlahActualRate = tempOnGetChip[i].JumlahActualRate + 1; //
                                                tempOnGetChip[i].JumlahActualRate = tempOnGetChip[i].JumlahActualRate; //
                                            }

                                        }
                                    }
                                    tempVchipTracking.data.ext = tempOnGetChip[i].Extension * 60 / me.incrementMinute; //di komen dl bikin pusing
                                    // tempVchipTracking.data.normal = tempOnGetChip[i].TimeDuration * 60 / me.incrementMinute; //kita cb ga pake timeduration tp pake AR actual rate, biar kl release di wo kena break, ga ngaco
                                    tempVchipTracking.data.normal = tempOnGetChip[i].JumlahActualRate * 60 / me.incrementMinute;
                                    tempVchipTracking.data.width = tempVchipTracking.data.normal + tempVchipTracking.data.ext;
                                    
                                } else {
                                    tempVchipTracking.data.ext = tempOnGetChip[i].Extension * 60 / me.incrementMinute;
                                    // tempVchipTracking.data.normal = tempOnGetChip[i].TimeDuration * 60 / me.incrementMinute; //kita cb ga pake timeduration tp pake AR actual rate, biar kl release di wo kena break, ga ngaco
                                    tempVchipTracking.data.normal = tempOnGetChip[i].JumlahActualRate * 60 / me.incrementMinute;
                                    tempVchipTracking.data.width = tempVchipTracking.data.normal + tempVchipTracking.data.ext;
                                }

                                // tempVchipTracking.data.ext = tempOnGetChip[i].Extension * 60 / me.incrementMinute;
                                // // tempVchipTracking.data.normal = tempOnGetChip[i].TimeDuration * 60 / me.incrementMinute; //kita cb ga pake timeduration tp pake AR actual rate, biar kl release di wo kena break, ga ngaco
                                // tempVchipTracking.data.normal = tempOnGetChip[i].JumlahActualRate * 60 / me.incrementMinute;
                                // tempVchipTracking.data.width = tempVchipTracking.data.normal + tempVchipTracking.data.ext;
                            }
                        }
                       
                    } else {
                        //dari stall
                        console.log('Mindahin chip dari stall', tempVchipTracking);
                        fromstoppage = 0;
                    }


                    console.log('end time si chip yang dipindah', vchip.data.endTime)
                    console.log('end work time', me.endHour)

                    if (typeof the_config.onPlot == 'function') {
                        console.log('kampang lagi', vchip);
                        // ==========ga ke pake ini kaya nya====================
                        // tempVjpcb.allChips = [];
                        // tempVjpcb.board = {};
                        // tempVjpcb.board.firstColumnItems = [];
                        // tempVjpcb.board.firstColumnItems = angular.copy(tempVjpcbBoard);
                        // tempVjpcb.allChips.push(vchip.data);
                        // console.log('kampang lagi2', tempVjpcb);

                        // me.onChipLoaded(tempVjpcb);
                        // ==============ga ke pake ini kaya nya====================

                        if (fromstoppage == 1){
                            the_config.onPlot(tempVchipTracking);

                        } else {
                            the_config.onPlot(vchip);
                        }
                    }
                    // if (typeof me.onPlot=='function'){

                    //     me.onPlot(vchip.data);
                    // }
                    // console.log("on drag-drop vchip", vchip);
                });

                jpcbFactory.showBoard(vname, vcontainer)
                // $timeout(function(){
                //     jpcbFactory.showBoard(vname, vcontainer)

                // }, 100);

            },
            startHour: '08:00',
            endHour: '17:00',
            breakHour: '12:00',
            // endHour: '17:00',
            // breakHour: '',
            incrementMinute: 60,
            noShowTolerance: 15, // minutes
            timeToColumn: function (vtime) {

                var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vmin = JsBoard.Common.getMinute(vtime) - vstartmin;
                var vresult = vmin / this.incrementMinute;

                return vresult;
            },
            columnToTime: function (vcolumn) {

                var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vmin = vcolumn * this.incrementMinute + vstartmin;
                var vresult = JsBoard.Common.getTimeString(vmin);

                return vresult;
            },
            checkAppointmentStatus: function (vtime) {
                // vtime: time-nya data
                // currentHour: jam/minute saat ini
                var vDate = this.getCurrentTime();
                var currentHour = $filter('date')(vDate, 'HH:mm');

                var vcurrmin = JsBoard.Common.getMinute(currentHour);
                var vmin = JsBoard.Common.getMinute(vtime);
                console.log("current Time#####", vcurrmin, vmin);

                if (vmin < vcurrmin) {
                    return 'no_show';
                }
                return 'appointment';

            },
            onGetHeader: function (arg, onFinish) {
                varg = JsBoard.Common.extend({ startDate: new Date(), dateCount: 1 }, arg);
                var vWorkHour = JsBoard.Common.generateDayHours({
                    startHour: this.startHour,
                    endHour: this.endHour,
                    increment: this.incrementMinute,
                });

                var result = [];
                for (var idx in vWorkHour) {
                    var vitem = { title: vWorkHour[idx] }
                    //var vtgl = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd')+' '+vitem.PlanStart;
                    //var vnow = $filter('date')(this.getCurrentTime(), 'yyyy-MM-dd HH:mm:ss');

                    if (vWorkHour[idx] == this.breakHour) {
                        vitem.colInfo = { isBreak: true }
                    }


                    result.push(vitem)

                }
                // console.log("header", result);
                $timeout(function () {
                    onFinish(result);
                }, 100);

            },
            onGetStall: function (arg, onFinish) {
                console.log("chip di getstall", chipExpiredBoard);
                var isExpired = false;
                // $http.get('/api/as/Stall').
                // var vurl = '/api/as/Boards/Stall/0';
                var vurl = arg.stallUrl;
                console.log('url buat get stall', vurl);
                // var vrandom = Math.random()*10000+'';
                // if (vurl.indexOf('?')>=0){
                //     vurl = vurl+'&_xrnd='+vrandom;
                // } else {
                //     vurl = vurl+'?_xrnd='+vrandom;
                // }

                console.log('apa ini BP? kalo BP 1 nih', repairProsesBP)
                var groupidFac = null; // dipake buat repair proses BP, buat flag chip nya ada di group mana
                if(repairProsesBP == 1){
                    groupidFac = arg.groupid;
                }

                var me = this;
                $http.get(vurl, { arg: arg }).then(function (res) {
                    var vstart = '';
                    var vend = '';
                    var merah = false;
                    console.log('daftar stall ' , res);
                    if (typeof arg.stallVisible=='undefined'){
                        arg.stallVisible = {}
                    }
                    var xres = res.data.Result;

                    if (repairProsesBP == 1){
                        console.log('ini nih BP', xres);
                        var finalstallRepairProsesBP = [];
                        for (var i=0; i< xres.length; i++){
                            if (xres[i].GroupId == groupidFac){
                                finalstallRepairProsesBP.push(xres[i]);
                            }
                        }
                        xres = finalstallRepairProsesBP;

                    } else {
                        xres = res.data.Result;
                    }

                    // tmpDataStall.push(xres);
                    console.log("stalll list", xres);
                    // var xres = xTempData1.Result;
                    var ChipIdExpired = chipExpiredBoard;
                    var result = [];
                    for (var i = 0; i < xres.length; i++) {
                        var vitem = xres[i];

                        if (!vitem.DefaultOpenTime) {
                            vitem.DefaultOpenTime = '08:00';
                        }
                        if (typeof vitem.DefaultOpenTime !== 'undefined') {

                            //cek defaultopen time menit nya 00 ga? kl ga 00 set jd 00 biar header nya menit nya 00
                            var cekDefaultOpenTime = vitem.DefaultOpenTime.split(':');
                            if (cekDefaultOpenTime[1].toString() != '00'){
                                vitem.DefaultOpenTime = cekDefaultOpenTime[0] + ':' + '00' + ':' + '00'
                            }

                            // if (vitem.DefaultOpenTime){
                            if (vstart === '') {
                                vstart = vitem.DefaultOpenTime;
                            } else {
                                if (vstart > vitem.DefaultOpenTime) {
                                    vstart = vitem.DefaultOpenTime;
                                }
                            }
                            // }
                        }
                        // if (!vitem.DefaultCloseTime) {
                        //     vitem.DefaultCloseTime = '17:00';
                        //     console.log('kerubah kok disini')
                        // }
                        // ================ AGAR SAMA Dengan ASB di WO
                        if (vitem.DefaultCloseTime == undefined) {
                            vitem.DefaultCloseTime = '17:00';
                        }

                        // comment disini kl end hour ngaco ===============
                        if (typeof vitem.DefaultCloseTime !== 'undefined') {
                            if (vitem.DefaultCloseTime >= '23:59') {
                                vitem.DefaultCloseTime = '24:00';
                            }
                            // if (vitem.DefaultCloseTime){
                            if (vend === '') {
                                vend = vitem.DefaultCloseTime;
                            } else {
                                if (vend < vitem.DefaultCloseTime) {
                                    vend = vitem.DefaultCloseTime;
                                }
                            }
                            // }
                        }
                        // end comment kl end hour ngaco ==============
                        if (typeof arg.stallVisible[vitem.StallId] == 'undefined') {
                            arg.stallVisible[vitem.StallId] = { visible: true, title: vitem.Name };
                        }
                        if (arg.stallVisible[vitem.StallId].visible) {
                            var vtech = '';
                            if (vitem.TeknisiName != null && vitem.TeknisiName != undefined && vitem.TeknisiName != '') {
                                vtech = vtech + vitem.TeknisiName;
                            }
                            if (vitem.Teknisi2Name != null && vitem.Teknisi2Name != undefined && vitem.Teknisi2Name != '') {
                                vtech = vtech + ', ' + vitem.Teknisi2Name;
                            }
                            if (vitem.Teknisi3Name != null && vitem.Teknisi3Name != undefined && vitem.Teknisi3Name != '') {
                                vtech = vtech + ', ' + vitem.Teknisi3Name;
                            }
                            result.push({
                                title: vitem.Name,
                                status: 'normal',
                                // person:vitem.TeknisiName,
                                person: vtech,
                                stallId: vitem.StallId,
                                startHour: vitem.DefaultOpenTime,
                                endHour: vitem.DefaultCloseTime,
                                startBreakTime1: vitem.StartBreakTime1,
                                endBreakTime1: vitem.EndBreakTime1,
                                startBreakTime2: vitem.StartBreakTime2,
                                endBreakTime2: vitem.EndBreakTime2,
                                startBreakTime3: vitem.StartBreakTime3,
                                endBreakTime3: vitem.EndBreakTime3,
                                type: vitem.Type,

                            });
                            tmpDataStall.push({
                                title: vitem.Name,
                                status: 'normal',
                                // person:vitem.TeknisiName,
                                person: vtech,
                                stallId: vitem.StallId,
                                startHour: vitem.DefaultOpenTime,
                                endHour: vitem.DefaultCloseTime,
                                startBreakTime1: vitem.StartBreakTime1,
                                endBreakTime1: vitem.EndBreakTime1,
                                startBreakTime2: vitem.StartBreakTime2,
                                endBreakTime2: vitem.EndBreakTime2,
                                startBreakTime3: vitem.StartBreakTime3,
                                endBreakTime3: vitem.EndBreakTime3,
                                type: vitem.Type,

                            })
                        }
                    }
                    //me.endHour = '19:00';
                    if (vstart === '') {
                        vstart = '08:00';
                    }
                    if (vend === '') {
                        vend = '17:00';
                    }
                    // vend = '20:00';
                    var vxmin = JsBoard.Common.getMinute(vend) + 60;
                    vend = JsBoard.Common.getTimeString(vxmin);
                    console.log('akakakak', JsBoard.Common.getTimeString(vxmin))

                    // if (vend > '23:30') {
                    //     vend = '23:30';
                    // }

                    // setting disini untuk overtime====
                    console.log('if chipoT', ChipOvertime);
                    if (ChipOvertime == 0){
                        me.startHour = vstart;
                        me.endHour = vend;
                        console.log('if endHour1', me.endHour);

                    } else {
                        me.startHour = vstart;
                        var endHourOverTime = 18 + TotalOvertime;
                        if (endHourOverTime > 24){
                            endHourOverTime = 24;
                        }
                        var cekVEnd = vend.split(':');
                        if (endHourOverTime <= parseInt(cekVEnd[0])){
                            endHourOverTime = vend;
                        } else {
                            endHourOverTime = endHourOverTime.toString();
                            me.endHour = endHourOverTime+':00';
                        }
                        
                        console.log('if endHour2', me.endHour);

                    }
                    
                    // setting disini kl overtime====

                    // me.endHour = '20:00';
                    // jam 17:00 itu ada batas nya col 10, kalo uda lbh dr col 10 lewat jam kerja (lembur)

                    var vIncrement = me.incrementMinute;
                    me.tmpDurasiStall = 0;
                    for (var i = 0; i < result.length; i++) {
                        isExpired = false;
                        var xitem = result[i];
                        me.tmpDurasiStall += me.timeToColumn2(xitem.endHour); 
                        console.log("xitem", xitem)
                        xitem.colEndWorkHour = me.timeToColumn2(xitem.endHour); 

                        // me.startHour = xitem.startHour;
                        // me.endHour = xitem.endHour;


                        if(ChipIdExpired != undefined)
                        {
                            angular.forEach(ChipIdExpired,function(index) {
                                if(xitem.stallId == index){
                                    console.log("index", index)
                                    isExpired = true;
                                }
                                // else{
                                //     isExpired = false;

                                // }
                            }, this);
                        }


                        // xitem.endHour = '17:00'; // ini di hardcode dl karena dr BE nya blm di set jam 5.. hapus aja kl uda di set dr BE
                        console.log('xitem.starthour', xitem.startHour)
                        console.log('me.starthour', me.startHour)
                        console.log('xitem.endHour', xitem.endHour)
                        console.log('me.endHour', me.endHour)
                        var vLeft = JsBoard.Common.getMinute(xitem.startHour) - JsBoard.Common.getMinute(me.startHour);
                        xitem['leftMargin'] = vLeft;
                        var vRight = JsBoard.Common.getMinute(me.endHour) - JsBoard.Common.getMinute(xitem.endHour);
                        // xitem['rightMargin'] = vRight; buat panjang abu2 dari kanan ke kiri, nilainya -
                        // xitem['rightMargin'] = vRight *-1 -60;
                        if (ChipOvertime == 0){
                            xitem['rightMargin'] = vRight;
                        } else {
                            xitem['rightMargin'] = (vRight *-1) - (60 * TotalOvertime);
                        }
                        var vRightPos = JsBoard.Common.getMinute(me.endHour) - JsBoard.Common.getMinute(me.startHour);
                        // xitem['rightMarginPos'] = vRightPos;
                        // xitem['rightMarginPos'] = vRightPos + 60;
                        if (ChipOvertime == 0){
                            xitem['rightMarginPos'] = vRightPos;
                        } else {
                            xitem['rightMarginPos'] = vRightPos + (60 * TotalOvertime);
                        }


                        console.log("vRightPos hapus",vRightPos, vRight);
                        console.log('vleft uy', vLeft);

                        xitem['startBreak1'] = 0;
                        xitem['durationBreak1'] = 0;
                        xitem['startBreak2'] = 0;
                        xitem['durationBreak2'] = 0;
                        xitem['startBreak3'] = 0;
                        xitem['durationBreak3'] = 0;
                        xitem['startBreak4'] = 0;
                        xitem['durationBreak4'] = 0;


                        if (xitem.startBreakTime1) {
                            xitem['startBreak1'] = JsBoard.Common.getMinute(xitem.startBreakTime1) - JsBoard.Common.getMinute(me.startHour);
                            xitem['durationBreak1'] = JsBoard.Common.getMinute(xitem.endBreakTime1) - JsBoard.Common.getMinute(xitem.startBreakTime1);
                        }

                        if (xitem.startBreakTime2) {
                            xitem['startBreak2'] = JsBoard.Common.getMinute(xitem.startBreakTime2) - JsBoard.Common.getMinute(me.startHour);
                            xitem['durationBreak2'] = JsBoard.Common.getMinute(xitem.endBreakTime2) - JsBoard.Common.getMinute(xitem.startBreakTime2);
                        }

                        if (xitem.startBreakTime3) {
                            xitem['startBreak3'] = JsBoard.Common.getMinute(xitem.startBreakTime3) - JsBoard.Common.getMinute(me.startHour);
                            xitem['durationBreak3'] = JsBoard.Common.getMinute(xitem.endBreakTime3) - JsBoard.Common.getMinute(xitem.startBreakTime3);
                        }

                        if(isExpired == true)
                        {
                             xitem['startBreak4'] = JsBoard.Common.getMinute("00:00:00") - JsBoard.Common.getMinute(me.startHour);
                             xitem['durationBreak4'] = JsBoard.Common.getMinute("20:00:00") - JsBoard.Common.getMinute("00:00:00");

                        }

                    }


                    onFinish(result);
                })

            },
            findStallIndex: function (vStallId, vList) {
                for (var i = 0; i < vList.length; i++) {
                    var vitem = vList[i];
                    if ((typeof vitem.stallId !== 'undefined') && (vitem.stallId == vStallId)) {
                        return i;
                    }
                }
                return -1;
            },
            reformatTime: function (vTime) {
                if (vTime) {
                    return JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vTime));
                }
                return '';
            },

            getStallCloseTime: function(stallId){
                var url = '/api/as/AfterSalesMasterStall/GetStallCloseTime/?stallId='+stallId;
                var res=$http.get(url);
                return res;
            },
            // isProblem: function(vitem){
            //     var vresult = false;
            //     var vstatus = vitem.Status;
            //     var vNow = this.getCurrentTime();
            //     var xStart = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd')+' '+vitem.PlanStart;
            //     var xFinish = $filter('date')(vitem.PlanDateFinish, 'yyyy-MM-dd')+' '+vitem.PlanFinish;
            //     var xNow = $filter('date')(vNow, 'yyyy-MM-dd HH:mm:ss');
            //     console.log("start", xStart);
            //     console.log("finish", xFinish);
            //     console.log("Now", xNow);
            //     // 5. Problem
            //     //    a. waktu awal < sekarang tapi status < 5 and status>=3
            //     if ((xStart<xNow) && (vstatus<5) && (vstatus>=3)){
            //         vresult = true;
            //     }
            //     //    b. ketika status belum done tapi jam akhir < jam sekarang
            //     if ((xFinish<xNow) && (vstatus<17) && (vstatus>=3)){
            //         vresult = 'irregular';
            //     }
            //     return vresult;

            // },
            checkStatus: function (vitem, vname) {
                console.log('kok BP ke sini', repairProsesBP);
                if (typeof vname == 'undefined') {
                    vname = 'Status';
                }
                var vstatus = vitem[vname];
                var vresult = '';
                if (vstatus <= 2) {
                    if(repairProsesBP == 1){
                        // vresult = 'sudahdatang' 
                        vresult = 'inprogress'; 
                    } else {
                        vresult = 'belumdatang';
                    }
                    // vresult = 'belumdatang';
                } else if (vstatus == 3) {
                    if(repairProsesBP == 1){
                        vresult = 'inprogress';
                    }
                    else {
                        vresult = 'sudahdatang';
                    }
                } else if (vstatus == 4 || vstatus == 8 || vstatus == 11) {
                    if (vitem.isAppointment) {
                        vresult = 'booking';
                    } else {
                        vresult = 'walkin';
                    }
                } else if (vstatus == 6 || vstatus == 25) {
                    vresult = 'irregular';
                } 
                // else if ((vstatus == 5) && (vstatus <= 16)) {
                //     vresult = 'inprogress';
                // }
                else if ((vstatus == 5) || (vstatus == 7)) {
                    vresult = 'inprogress';
                }
                 else if (vstatus >= 12 && vstatus != 22) {       // ditambah && != 22 karena job dispatch ada status nya 22
                    vresult = 'done';
                } else if (vstatus == 22) {                      // ditambah else if 22 karena kl job dispatch ga ngerubah warna garis chip.
                    if (vitem.isAppointment) {
                        vresult = 'booking';
                    } else {
                        vresult = 'walkin';
                    }
                }

                
                // 5. Problem
                //    a. waktu awal < sekarang tapi status < 5 and status>=3

                // var vNow = this.getCurrentTime();
                // var xStart = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd')+' '+vitem.PlanStart;
                // var xFinish = $filter('date')(vitem.PlanDateFinish, 'yyyy-MM-dd')+' '+vitem.PlanFinish;
                // var xNow = $filter('date')(vNow, 'yyyy-MM-dd HH:mm:ss');
                // console.log("start", xStart);
                // console.log("finish", xFinish);
                // console.log("Now", xNow);
                // if ((xStart<xNow) && (vstatus<5) && (vstatus>=3)){
                //     vresult = 'irregular';
                // }

                // //    b. ketika status belum done tapi jam akhir < jam sekarang
                // if ((xFinish<xNow) && (vstatus<17) && (vstatus>=3)){
                //     vresult = 'irregular';
                // }

                if (jpcbFactory.isJobProblem(vitem, vname)) {
                    vresult = 'irregular';
                }

                return vresult;
            },
            updateStatusIcons: function (vdata) {
                var vresult = []
                // customer waiting
                // is waiting 0 waiting 1 drop off
                console.log('vdata icon', vdata)
                if (vdata.isWaiting == 1) {
                    vresult.push('customer');
                }
                //GR nih keterangan na ini
                // in progress
                // 5 = Clock ON *
                // 6 = Clock Pause
                // 7 = Clock resume
                // 8 = Clock Off *
                // 9 = Qc start *
                // 10 = Qc finish *

                //BP nih keterangan nya ini
                // 0 = belum mulai proses
                // 1 = sudah mulai clock on
                // 2 = clock pause
                // 3 = clock pause finish
                // 4 = clock off
                // 5 = clock on QC
                // 6 = clock off Qc
                if(repairProsesBP == 1){
                    if ((vdata.intStatus == 1) || (vdata.intStatus == 3) || (vdata.intStatus == 5)) {
                        // console.log('masuk sini in prog', vdata)
                        // console.log('masuk sini in prog', vdata.intStatus)
                        vresult.push('inprogress');
                    }

                    // Check Status Chip, is Paused ?
                    if (vdata.intStatus == 2) {
                        vresult.push('box');
                    }
                    // Warranty
                    if (vdata.wocategoryname == 'TWC' || vdata.wocategoryname == 'PWC') {
                        if (vdata.wocategoryname == 'TWC') {
                            vresult.push('TWC');
                        } else {
                            vresult.push('PWC');
                        }
                    }
                } else {
                    if ((vdata.intStatus == 5) || (vdata.intStatus == 7) || (vdata.intStatus == 9)) {
                        vresult.push('inprogress');
                    }
                    if (vdata.intStatus == 6 || vdata.intStatus == 25) {
                        vresult.push('box');
                    }
                }
                
                // vresult.push('waitparts');
                if (vdata.isWaitingPart) {
                    vresult.push('waitparts');
                }
                // multiple task
                if (vdata.jmltask > 1) {
                    vresult.push('multitask');
                }

                // RTJ
                if (vdata.pekerjaan == 'RTJ') {
                    vresult.push('returnjob');
                }


                vdata.statusicon = vresult;
            },
            isOverlapped: function (x1, x2, y1, y2) {
                return (x1 < y2) && (y1 < x2)
            },
            onChipLoaded: function (vjpcb) {
                // vjpcb.allChips = []
                // return;

                //data allchips di sort karena di jobstoppage katanya mau tampil 7 data dan terurut dari data yang paling lama
                var SortedData = angular.copy(vjpcb.allChips);
                SortedData = SortedData.sort(function(a, b){
                    var dateA=new Date(a.PlanDateStart), dateB=new Date(b.PlanDateStart)
                    return dateA-dateB //sort by date ascending
                })
                vjpcb.allChips = SortedData;

                console.log('sorted kah', SortedData);
                console.log("ALL CHIPS", vjpcb);
                console.log('search ada ga nih',keywordNopol);

                // tempVjpcbBoard = angular.copy(vjpcb.board.firstColumnItems);
                // console.log("tempVjpcbBoard coeg", tempVjpcbBoard);


                var vresult = [];
                var vresultStoppage = [];
                var count = 0;

                var count7JobStoppageNormal = 0;
                var count7JobStoppageSearch = 0;
                for (var i=0; i<vjpcb.allChips.length; i++){
                    var vitem = vjpcb.allChips[i];

                    // if (vitem.nopol == 'B4242LOK'){
                    //     console.log('wah edan', vitem)
                    // }
                    if (vitem.isInStopPageGr == 0){
                        console.log('wah edanz', vitem)
                        
                        if (vitem.col + vitem.width + vitem.ext > 10){  // di hardcode 10 karena msh static dr FE jam pulang kerjanya
                            ChipOvertime = 1; 
                            //jika nilai nya 1 berarti ada chip yg overtime
                            if (((vitem.col + vitem.width) -10) > TotalOvertime){
                                console.log('adada', vitem.col, vitem.width, vitem.ext);
                                TotalOvertime = ((vitem.col + vitem.width) -10);
                                TotalOvertime = Math.ceil(TotalOvertime);
                                console.log('panjang overtime nih', TotalOvertime)
                            }
                        }
                    }

                    var itemPrediagnose = angular.copy(vitem);
                    if (vitem.isInStopPageGr==1){
                        //cek buat search stoppage
                        if (vitem.intStatus != 26){
                            vresultStoppage.push(vitem);


                            if (keywordNopol != null && keywordNopol != '' && keywordNopol != undefined){
                                if (vitem.nopol.toUpperCase().includes(keywordNopol)){
                                    count7JobStoppageSearch = count7JobStoppageSearch +1;
                                    vitem.row = -1;
                                    //vitem.row = -1;
                                    vitem.width = 1;
                                    vitem.normal = 1;
                                    vitem.ext = 0;
                                    if (count7JobStoppageSearch <= 7){
                                        vresult.push(vitem);
                                    }
                                }
                            } else {
                                count7JobStoppageNormal = count7JobStoppageNormal +1;
                                vitem.row = -1;
                                //vitem.row = -1;
                                vitem.width = 1;
                                vitem.normal = 1;
                                vitem.ext = 0;
                                console.log('data jobstoppgae', vitem)
                                if(count7JobStoppageNormal <= 7){
                                    vresult.push(vitem);
                                }
                            }
    
                            
    
                            // vitem.row = -1;
                            // //vitem.row = -1;
                            // vitem.width = 1;
                            // vitem.normal = 1;
                            // vitem.ext = 0;
                            // vresult.push(vitem);
                        }
                       

                    } else {
                      if ((itemPrediagnose.PreDiagnoseStallId != 0) || (itemPrediagnose.PreDiagnoseStallId != 'undefined')) {
                        vidx1 = this.findStallIndex(itemPrediagnose.PreDiagnoseStallId, vjpcb.board.firstColumnItems);
                        console.log('masuk diagnose' ,vidx1 );
                        if (vidx1>=0){
                            count++;
                            itemPrediagnose.row = vidx1;

                            var vcolumn = this.timeToColumn($filter('date')(itemPrediagnose.PrediagnoseScheduledTime, 'HH:mm:ss'));
                            itemPrediagnose.col = vcolumn;
                            //vitem.row = -1;
                            var vstall2 = vjpcb.board.firstColumnItems[vidx1];
                            console.log('stall preDiagnose', vstall2);
                            console.log('item diagnose', itemPrediagnose , count++);
                            var jobid = itemPrediagnose.jobId;
                            var nopolice = itemPrediagnose.nopol;
                            itemPrediagnose.jobId = jobid * -1;
                            itemPrediagnose.nopol =  nopolice + 'PD';
                            //var vstall
                            // console.log('bill sini itemPrediagnose.PrediagnoseScheduledTime', itemPrediagnose.PrediagnoseScheduledTime)
                            var prediagnoseStart = $filter('date')(itemPrediagnose.PrediagnoseScheduledTime, 'HH:mm');
                            // console.log('bill sini prediagnoseStart', prediagnoseStart)


                            var hourPrediagnoseStart = prediagnoseStart.split(':');
                            // console.log('bill sini', hourPrediagnoseStart)


                            var hourPrediagnoseEnd = parseInt(hourPrediagnoseStart[0])+1;
                            var xendPred = hourPrediagnoseEnd.toString()+":"+hourPrediagnoseStart[1]+":00";
                            // console.log('bill sini hourPrediagnoseEnd', hourPrediagnoseEnd)
                            // console.log('bill sini xendPred', xendPred)


                            if (itemPrediagnose.ext){
                                xendPred =  JsBoard.Common.getTimeString(JsBoard.Common.getMinute(xendPred)+itemPrediagnose.ext*60)+':00';
                            }
                            var vaddPred = 0;
                            if (vstall2.durationBreak1>0){
                                if (this.isOverlapped(prediagnoseStart, xendPred, vstall2.startBreakTime1, vstall2.endBreakTime1)){
                                    vaddPred = vaddPred+vstall2.durationBreak1;
                                    xendPred =  JsBoard.Common.getTimeString(JsBoard.Common.getMinute(xendPred)+vstall2.durationBreak1)+':00';
                                    this.breakHour1 = vstall2.startBreakTime1;
                                }
                            }


                            if (vstall2.durationBreak2>0){
                                if (this.isOverlapped(prediagnoseStart, xendPred, vstall2.startBreakTime2, vstall2.endBreakTime2)){
                                    vaddPred = vaddPred+vstall2.durationBreak2;
                                    xendPred =  JsBoard.Common.getTimeString(JsBoard.Common.getMinute(xendPred)+vstall2.durationBreak2)+':00';
                                    this.breakHour2 = vstall2.startBreakTime2;
                                }
                            }

                            if (vstall2.durationBreak3>0){
                                if (this.isOverlapped(prediagnoseStart, xendPred, vstall2.startBreakTime3, vstall2.endBreakTime3)){
                                    vaddPred = vaddPred+vstall2.durationBreak3;
                                    xendPred =  JsBoard.Common.getTimeString(JsBoard.Common.getMinute(xendPred)+vstall2.durationBreak3)+':00';
                                    this.breakHour3 = vstall2.startBreakTime3;
                                }
                            }
                            itemPrediagnose.breakAdd = vaddPred/this.incrementMinute;

                            // itemPrediagnose.width = itemPrediagnose.width+vaddPred/this.incrementMinute; //di comment karena pengen cm 1 jam pjng chip prediagnose nya
                            itemPrediagnose.width = 1;
                            
                            itemPrediagnose.jamcetakwo = prediagnoseStart; // ini dua biar jam yg tampil di chip PD sesuai sama jam plotingan chipnya
                            itemPrediagnose.jamjanjiserah = hourPrediagnoseEnd.toString()+":"+hourPrediagnoseStart[1]; // ini dua biar jam yg tampil di chip PD sesuai sama jam plotingan chipnya

                            // vitem.width = vitem.width+1;
                            this.updateChipBreak(itemPrediagnose, vstall2);

                            vresult.push(itemPrediagnose);
                        }

                      }
                        vidx = this.findStallIndex(vitem.stallId, vjpcb.board.firstColumnItems);
                        if (vidx >= 0) {
                            vitem.row = vidx;
                            //vitem.row = -1;
                            var vstall = vjpcb.board.firstColumnItems[vidx];
                            var vDate = (new Date(), 'HH:mm:ss');

                            var xend = vitem.PlanFinish;
                            if (vitem.ext) {
                                xend = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(xend) + vitem.ext * 60) + ':00';
                            }
                            var vadd = 0;
                            console.log('vstall oeg', vstall)
                            // breakHour
                            if (vstall.durationBreak1 > 0) {
                                if (this.isOverlapped(vitem.PlanStart, xend, vstall.startBreakTime1, vstall.endBreakTime1)) {
                                    vadd = vadd + vstall.durationBreak1;
                                    xend = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(xend) + vstall.durationBreak1) + ':00';
                                    this.breakHour1 = vstall.startBreakTime1;
                                    
                                }
                            }



                            if (vstall.durationBreak2 > 0) {
                                if (this.isOverlapped(vitem.PlanStart, xend, vstall.startBreakTime2, vstall.endBreakTime2)) {
                                    vadd = vadd + vstall.durationBreak2;
                                    xend = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(xend) + vstall.durationBreak2) + ':00';
                                    this.breakHour2 = vstall.startBreakTime2;
                                }
                            }

                            if (vstall.durationBreak3 > 0) {
                                if (this.isOverlapped(vitem.PlanStart, xend, vstall.startBreakTime3, vstall.endBreakTime3)) {
                                    vadd = vadd + vstall.durationBreak3;
                                    xend = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(xend) + vstall.durationBreak3) + ':00';
                                    this.breakHour3 = vstall.startBreakTime3;
                                }
                            }
                            vitem.breakAdd = vadd / this.incrementMinute;

                            // vitem.width = vitem.width + vadd / this.incrementMinute;
                            console.log('vadd oeg', vadd)
                            console.log('xend oeg', xend)
                            console.log('vitem.breakAdd oeg', vitem.breakAdd)
                            console.log('vitem.width oeg', vitem.width)

                            this.updateChipBreak(vitem, vstall);
                            console.log('vitem oeg', vitem)



                            // vitem.width = vitem.width+1;

                            vresult.push(vitem);
                        }

                    }


                }
                vjpcb.allChips = vresult;
                vjpcb.allChipsTemp = vresultStoppage; // di buat supaya bisa cari jumlah data real yang ada di jobstoppage, karean di stoppage mau nampilin cm 7 chip yang terlama aja
                dataSemuaChip = vjpcb;
            },
            getCurrentMinute: function () {
                var vDate = this.getCurrentTime();
                var currentHour = $filter('date')(vDate, 'HH:mm');
                var vcurrmin = JsBoard.Common.getMinute(currentHour);
                return vcurrmin;
            },
            changeFormatDateStrip : function(item) {
                console.log("itemmmm",item);
                if(item == undefined || item == null || item == ""){
                    return "";
                }
                var tmpItemDate = angular.copy(item);
                tmpItemDate = new Date(tmpItemDate);
                var finalDate = '';
                var yyyy = tmpItemDate.getFullYear().toString();
                var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpItemDate.getDate().toString();
                return finalDate += (dd[1] ? dd : "0" + dd[0])+'/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
            },
            onBoardReady: function (vjpcb) {
                this.updateTimeLine(vjpcb.boardName);
                if (vjpcb.onJpcbReady) {
                    vjpcb.onJpcbReady(vjpcb);
                }
            },
            xDataCounter: 0,

            addTask: function(Model, IdSA) {
                console.log("model", IdSA);
                return $http.post('/api/as/lappet', [{
                    // Message: Model.Message,
                    Message: 'Ploting Ulang Chipo ',
                    userIdSA: IdSA
                }]);
            },
            addTasksatu: function(Model, IdSA) {
                console.log("model", IdSA);
                return $http.post('/api/as/lappetTandingan', [{
                    // Message: Model.Message,
                    Message: 'Ploting Ulang Chipo ',
                    userIdSA: IdSA
                }]);
            },
            addTaskdua: function(Model, IdSA) {
                console.log("model", IdSA);
                return $http.post('/api/as/lappetTandinganDua', [{
                    // Message: Model.Message,
                    Message: 'Ploting Ulang Chipo ',
                    userIdSA: IdSA
                }]);
            },

            updateFlagForAddTask: function(Model) {
                console.log("data", Model);
                return $http.put('/api/as/setFlreqjob/' + Model, [{
                    Flreqjob: 1
                }]);

            },

            onGetChips: function (arg, onFinish) {
                console.log("arg ====>",arg);
                console.log("onFinish =====>",onFinish)
                var vArg = { nopol: 'B 1000 AAA', tipe: 'Avanza' }
                vArg = JsBoard.Common.extend(vArg, arg);

                var tglAwal = arg.currentDate;
                CopyTglAwal = tglAwal;
                var tglAkhir = new Date($filter('date')(tglAwal, 'yyyy-MM-dd'));
                tglAkhir.setDate(tglAkhir.getDate());
                // var vurl = '/api/as/Boards/jpcb/bydate/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/0/17';
                var vurl = arg.chipUrl;
                // var vrandom = Math.random()*10000+'';
                // if (vurl.indexOf('?')>=0){
                //     vurl = vurl+'&_xrnd='+vrandom;
                // } else {
                //     vurl = vurl+'?_xrnd='+vrandom;
                // }
                var me = this;
                var dateTest = $filter('date')(arg.currentDate, 'yyyy-MM-dd');
                me.xDataCounter = me.xDataCounter + 1;
                $http.get(vurl).then(function (res) {
                    console.log('---- xDataCounter ----', me.xDataCounter);
                    // onFinish([]);
                    // return;
                    // var result = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1,
                    //     daydiff: 0, status: 'appointment', statusicon: ['customer_gray']}];
                    var result = [];
                    var xres = res.data.Result;
                    if (repairProsesBP == 0){
                        xres = _.uniq(xres, 'JobId');
                    }
                    // var xres = xTempData2.Result;
                    console.log('xress', xres, dateTest);

                    tempOnGetChip = xres;
                    var cekEndOfCO = 0;

                    var vNoShow = 0;
                    var vBookingA = 0;
                    var vBookingB = 0;
                    var vInProgress = 0;
                    var vCarryOver = 0;
                    var vIsCarryOver = 0;
                    var vProductCapacity=0;
                    tmpDataChips = [];

                    var adaSplit = 0;
                    var posisiLastSplitId = 0;
                    var posisiI = null;

                    for (var h=0; h < xres.length; h++){
                        if (xres[h].isAppointment == 1){
                            var datePlantTest = $filter('date')(xres[h].PlanDateStart, 'yyyy-MM-dd');
                            if (xres[h].Status >= 4 && datePlantTest == dateTest) {
                                console.log('masuk if booking',datePlantTest,'==',dateTest);
                                  vBookingA = vBookingA + 1;
                              
                              }
                              if(datePlantTest == dateTest && xres[h].Status != 1){
                                  vBookingB = vBookingB + 1;
                              }
                        }
                    }

                    for (var i = 0; i < xres.length; i++) {
                        cekEndOfCO = 0;


                        if (adaSplit < 1 || posisiI != i){
                            adaSplit = 0;
                            posisiLastSplitId = 0;
                        }
                        posisiI = angular.copy(i);

                        var vitem = xres[i];
                        var statusAwal = angular.copy(vitem.Status);

                        if (vitem.isSplitActive == undefined || vitem.isSplitActive == null){
                            vitem.isSplitActive = 0;
                        }
                        tmpDataChips.push(xres[i]);
                        var vstatus = 'no_show';

                        // ============ Added new Code
                        var JobSplitId = 0;
                        if(repairProsesBP == 0){
                            // if (vitem.JobSplitChip != null){
                                if(vitem.JobSplitChip != null && vitem.JobSplitChip != undefined && vitem.JobSplitChip.length > 0){
                                    var dateNow = me.changeFormatDateStrip(arg.currentDate);
                                    var diffDate = '';

                                    if (adaSplit == 0){
                                        for(var z in vitem.JobSplitChip){
                                            diffDate = me.changeFormatDateStrip(vitem.JobSplitChip[z].PlanDateStart)
                                            console.log('ini coco2', vitem.PoliceNumber)
                                            if(diffDate == dateNow ){
                                                adaSplit++;
                                            }
                                        }
                                    }
                                   
                                    console.log('ada split', adaSplit)
                                    console.log('ada split nopol', vitem.PoliceNumber)
                                    for(var z in vitem.JobSplitChip){
                                        diffDate = me.changeFormatDateStrip(vitem.JobSplitChip[z].PlanDateStart)
                                        console.log('diffdate', diffDate)
                                        console.log('dateNow', dateNow)
                                        console.log('vitem', vitem)
                                        if(diffDate == dateNow && vitem.JobSplitChip[z].JobSplitId != posisiLastSplitId && vitem.JobSplitChip[z].JobSplitId > posisiLastSplitId){
                                           JobSplitId = vitem.JobSplitChip[z].JobSplitId;
                                           vitem.StallId = vitem.JobSplitChip[z].StallId;
                                           vitem.PlanDateStart = vitem.JobSplitChip[z].PlanDateStart;
                                           vitem.PlanDateFinish = vitem.JobSplitChip[z].PlanDateFinish;
                                           vitem.PlanStart = vitem.JobSplitChip[z].PlanStart;
                                           vitem.PlanFinish = vitem.JobSplitChip[z].PlanFinish;
                                           vitem.Extension = vitem.JobSplitChip[z].Extension;
                                           vitem.TimeFinish = vitem.JobSplitChip[z].TimeFinish
                                           vitem.TimeStart = vitem.JobSplitChip[z].TimeStart
                                           vitem.DateStart = vitem.JobSplitChip[z].ActualDateStart
                                           vitem.DateFinish = vitem.JobSplitChip[z].ActualDateFinish

                                           if (vitem.isCarryOver == 0){
                                            vitem.IsInStopPageGr = vitem.JobSplitChip[z].IsInJobStopageGR;
                                           }
                                           var planStart = me.timeToColumn2(vitem.PlanStart);
                                           var actualFinish =  me.timeToColumn2(vitem.PlanFinish);
                                           vitem.JumlahActualRate = actualFinish - planStart;
                                           vitem.JumlahActualRate = Math.ceil(Math.abs(vitem.JumlahActualRate)*4)/4;

                                           vitem.isSplitActive = vitem.JobSplitChip[z].isSplitActive;
       
                                           var cekAwal = vitem.JobSplitChip[z].PlanStart.split(':');
                                           var cekAkhir = vitem.JobSplitChip[z].PlanFinish.split(':');
                                           if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                                                // vitem.JumlahActualRate = vitem.JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                                               if (vitem.JobSplitChip[z].isSplitActive == 0){
                                                    vitem.JumlahActualRate = vitem.JumlahActualRate -1; 
                                               } else {
                                                if (vitem.StallId != 0 && vitem.StallId != null){
                                                    vitem.JumlahActualRate = vitem.JumlahActualRate-1;
                                                } else {
                                                    vitem.JumlahActualRate = vitem.JumlahActualRate;
                                                }
                                                // vitem.JumlahActualRate = vitem.JumlahActualRate-1;
                                                vitem.JumlahActualRate = vitem.JumlahActualRate;
                                               }
                                           }
                                           posisiLastSplitId = vitem.JobSplitChip[z].JobSplitId;
                                           break;
                                        } else {
                                            var cekDateAkhirCarryOverFinish = 0
                                            for (var j in vitem.JobSplitChip){
                                                if (arg.currentDate > vitem.JobSplitChip[j].PlanDateStart){
                                                    cekDateAkhirCarryOverFinish++;
                                                }
                                            }
                                            if(cekDateAkhirCarryOverFinish == vitem.JobSplitChip.length){
                                                vitem.JumlahActualRate = 0;
                                                cekEndOfCO = 1;
                                            }

                                            if (vitem.isCarryOver == 0 && vitem.JobSplitChip[z].IsInJobStopageGR == 1){
                                                JobSplitId = vitem.JobSplitChip[z].JobSplitId;
                                                vitem.StallId = vitem.JobSplitChip[z].StallId;
                                                vitem.PlanDateStart = vitem.JobSplitChip[z].PlanDateStart;
                                                vitem.PlanDateFinish = vitem.JobSplitChip[z].PlanDateFinish;
                                                vitem.PlanStart = vitem.JobSplitChip[z].PlanStart;
                                                vitem.PlanFinish = vitem.JobSplitChip[z].PlanFinish;
                                                vitem.Extension = vitem.JobSplitChip[z].Extension;
                                                if (vitem.isCarryOver == 0){
                                                    vitem.IsInStopPageGr = vitem.JobSplitChip[z].IsInJobStopageGR;
                                                }
                                                var planStart = me.timeToColumn2(vitem.PlanStart);
                                                var actualFinish =  me.timeToColumn2(vitem.PlanFinish);
                                                vitem.JumlahActualRate = actualFinish - planStart;
                                                vitem.JumlahActualRate = Math.ceil(Math.abs(vitem.JumlahActualRate)*4)/4;

                                                vitem.isSplitActive = vitem.JobSplitChip[z].isSplitActive;
            
                                                var cekAwal = vitem.JobSplitChip[z].PlanStart.split(':');
                                                var cekAkhir = vitem.JobSplitChip[z].PlanFinish.split(':');
                                                if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                                                        // vitem.JumlahActualRate = vitem.JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                                                    if (vitem.JobSplitChip[z].isSplitActive == 0){
                                                            vitem.JumlahActualRate = vitem.JumlahActualRate -1; 
                                                    } else {
                                                        if (vitem.StallId != 0 && vitem.StallId != null){
                                                            vitem.JumlahActualRate = vitem.JumlahActualRate-1;
                                                        } else {
                                                            vitem.JumlahActualRate = vitem.JumlahActualRate;
                                                        }
                                                    }
                                                }
                                                posisiLastSplitId = vitem.JobSplitChip[z].JobSplitId;
                                                break;
                                            }

                                        }
                                    }
                                    
                               }
                            // }
                            
                        }
                       
                        // ==========================


                        // ===== Tambahan ======
                        var tmpPlanDateStart = angular.copy(vitem.PlanDateStart);
                        var tmpPlanDateFinish = angular.copy(vitem.PlanDateFinish);
                        tmpPlanDateStart = new Date(tmpPlanDateStart).getTime();
                        tmpPlanDateFinish = new Date(tmpPlanDateFinish).getTime();
                        if( tmpPlanDateStart !== tmpPlanDateFinish){
                            var tmpTglAwal = angular.copy(tglAwal);
                            var tmpTglAkhir = angular.copy(vitem.PlanDateFinish);
                            tmpTglAwal = new Date(tmpTglAwal);
                            tmpTglAkhir = new Date(tmpTglAkhir);
                            tmpTglAwal.setHours(0);
                            tmpTglAwal.setMinutes(0);
                            tmpTglAwal.setSeconds(0);
                            tmpTglAwal.setMilliseconds(0);
                            console.log("dataaaa Beneerrr",tmpTglAkhir.getTime(), tmpTglAwal.getTime());
                            console.log("dataaaa Beneerrr",tmpTglAkhir, tmpTglAwal)
                            if(tmpTglAkhir.getTime() == tmpTglAwal.getTime()){
                                vitem.PlanDateStart = vitem.PlanDateFinish;
                                vitem.TimeDuration = (JsBoard.Common.getMinute(vitem.PlanFinish) - JsBoard.Common.getMinute('08:00'))/60;
                                vitem.PlanStart = '08:00:00'
                                vitem.IsInStopPageGr = 0;
                                console.log("vitem",vitem);
                            }
                        }
                        var vstatus = me.checkAppointmentStatus(vitem.PlanStart);
                        // var datePlantTest = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd');
                        // var vicon = 'customer';
                        // if (vstatus=='appointment'){
                        //     vicon = 'customer_gray';
                        // }
                        // var vcolumn = me.timeToColumn(vitem.PlanStart); ini di komen coba, mau di buletin jadi kelipattan 0.25 ke atas
                        var vcolumn = me.timeToColumn(vitem.PlanStart);
                        vcolumn = Math.ceil(Math.abs(vcolumn)*4)/4; //nah ini di tambahin buat jadi kelipatan 0.25   
                        
                        if (vitem.PoliceNumber) {
                            vitem.nopol = vitem.PoliceNumber;
                        } else {
                            vitem.nopol = '#' + vitem.JobId;
                        }
                        var vDontDrag = 0;
                        var vIsSplitable = 0;

                        if (typeof vitem.PauseDraggable != 'undefined') {
                            if ((vitem.Status == 6 || vitem.Status == 25) && (vitem.PauseDraggable == 0) && (vitem.isSplitActive != 0)) {
                                // vDontDrag = 1; tadi nya begini, tapi kl carry over split, hasil split nya ga bs pindah
                                vDontDrag = 0;
                            } else if ((vitem.Status == 6 || vitem.Status == 25) && (vitem.PauseDraggable == 0) && (vitem.isSplitActive == 0)){
                                vDontDrag = 1;
                            } else if ((vitem.Status == 6 || vitem.Status == 25) && (vitem.PauseDraggable == 1) && (vitem.IsInStopPageGr == 0)){
                                if (vitem.isSplitActive == 0){
                                    vDontDrag = 1;
                                } else if (vitem.isSplitActive == 1) {
                                    vDontDrag = 0;
                                } else {
                                    vDontDrag = 1;
                                }
                            } else if ((vitem.Status == 6 || vitem.Status == 25) && (vitem.PauseDraggable == 1) && (vitem.IsInStopPageGr == 1)){
                                vDontDrag = 0;
                            }
                        }

                        if (typeof vitem.PauseDraggable != 'undefined') {
                            if ((vitem.Status == 5) && (vitem.PauseDraggable == 0)) {
                                vDontDrag = 1;
                                // vitem.isSplitable = 1;
                            }
                        }

                        if (typeof vitem.PauseDraggable != 'undefined') {
                            if ((vitem.Status == 6 || vitem.Status == 25)) {
                                // vDontDrag = 1;
                                vitem.isSplitable = 1;
                            }
                        }

                        if (typeof vitem.isSplitable != 'undefined') {
                            vIsSplitable = vitem.isSplitable;
                        }
                        vstatus = me.checkStatus(vitem);
                        
                        if (repairProsesBP == 0){
                            if (vitem.nopol == "JAJA"){
                                console.log('ampun deh ampun11',posisiLastSplitId)
                            }
                            var alreadyClockoff = 0;
                            if (vitem.TimeFinish != null){
                                console.log('chip sudah clock off', vitem)
                                var TempPlanFinish = vitem.PlanFinish.split(':');
                                var TempTimeFinish = vitem.TimeFinish.split(':');
                                alreadyClockoff = 1;
                                
                                if (vitem.JobSplitChip != null && vitem.JobSplitChip != undefined && vitem.JobSplitChip.length > 0){
                                    
                                    var countingSudahFI = 0;
                                    for (var h in vitem.JobSplitChip){
                                        if (vitem.JobSplitChip[h].isFI == 1){
                                            countingSudahFI++;
                                        }
                                    }

                                    if (countingSudahFI >= 1){

                                        var dateNow = me.changeFormatDateStrip(arg.currentDate);
                                        var diffDate = '';
                                        for(var z=0; z<vitem.JobSplitChip.length; z++){
                                            if (posisiLastSplitId == vitem.JobSplitChip[z].JobSplitId){
                                                if (vitem.JobSplitChip[z].isFI == 0 || vitem.JobSplitChip[z].isFI == null){
                                                    if (vitem.JobSplitChip[z].TimeFinish == null){
                                                        vitem.isFI = vitem.JobSplitChip[z].isFI
                                                        vitem.PlanFinish = vitem.JobSplitChip[z].PlanFinish;
                                                    } else {
                                                        diffDate = me.changeFormatDateStrip(vitem.JobSplitChip[z].PlanDateStart)
                                                        console.log('diffdate', diffDate)
                                                        console.log('dateNow', dateNow)
                                                        console.log('vitem', vitem)
                                                        if(diffDate == dateNow ){
                                                            vitem.isFI = vitem.JobSplitChip[z].isFI
                                                        //    JobSplitId = vitem.JobSplitChip[z].JobSplitId;
                                                        //    vitem.StallId = vitem.JobSplitChip[z].StallId;
                                                        //    vitem.PlanStart = vitem.JobSplitChip[z].PlanStart; // ini isi nya jam 
                                                        //    vitem.PlanFinish = vitem.JobSplitChip[z].PlanFinish; // ini isinya jam jg
                                                        //    var planStart = me.timeToColumn2(vitem.PlanStart);
                                                        //    var actualFinish =  me.timeToColumn2(vitem.PlanFinish);
                                                        //    vitem.JumlahActualRate = actualFinish - planStart;
                                                        //    vitem.JumlahActualRate = Math.ceil(Math.abs(vitem.JumlahActualRate)*4)/4;
                    
                                                        //    var cekAwal = vitem.JobSplitChip[z].PlanStart.split(':');
                                                        //    var cekAkhir = vitem.JobSplitChip[z].PlanFinish.split(':');
            
                                                        console.log('date start clock', vitem.DateStart)
                                                        console.log('date finish clock', vitem.DateFinish)    
                                                        var realFinishClockDate = new Date(vitem.DateFinish);
                                                        var planFinishClockDate = new Date(vitem.JobSplitChip[z].PlanDateFinish);
                                                        console.log('realFinishClockDate',realFinishClockDate)
                                                        console.log('planFinishClockDate',planFinishClockDate)
            
                                                        if (realFinishClockDate.toString() == planFinishClockDate.toString()){
                                                            //di sini harus di cek kalau dia clock off sebelum plan nya, chip nya mendekkin
                                                            console.log('nih cek mendek')
                                                            if (parseInt(TempTimeFinish[0]) < parseInt(TempPlanFinish[0])){
                                                                    vitem.PlanFinish = vitem.TimeFinish;
                                                                    var planStart = me.timeToColumn2(vitem.JobSplitChip[z].PlanStart);
                                                                    var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
                                                                    console.log('planStart =====', planStart)
                                                                    console.log('actualFinish ======', actualFinish)
                                                                    vitem.JumlahActualRate = actualFinish - planStart;
                                                                    vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                                                    if (vitem.JumlahActualRate <= 0){
                                                                        vitem.JumlahActualRate = 0.25;
                                                                    }
                                                            } else {
                                                                if (parseInt(TempTimeFinish[1]) < parseInt(TempPlanFinish[1]) && parseInt(TempTimeFinish[0]) <= parseInt(TempPlanFinish[0])){
                                                                        vitem.PlanFinish = vitem.TimeFinish;
                                                                        var planStart = me.timeToColumn2(vitem.JobSplitChip[z].PlanStart);
                                                                        var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
                                                                        console.log('planStart =====', planStart)
                                                                        console.log('actualFinish ======', actualFinish)
                                                                        vitem.JumlahActualRate = actualFinish - planStart;
                                                                        vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                                                        if (vitem.JumlahActualRate <= 0){
                                                                            vitem.JumlahActualRate = 0.25;
                                                                        }
                                                                }
                                                            }
            
                                                        }
            
                                                        //    if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                                                        //        vitem.JumlahActualRate = vitem.JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                                                        //    }
                                                        } else {
                                                        
                                                        }
                                                    }
                                                    
                                                    break;
                                                } else {
                                                    diffDate = me.changeFormatDateStrip(vitem.JobSplitChip[z].PlanDateStart)
                                                    console.log('diffdate after FI', diffDate)
                                                    console.log('dateNow after FI', dateNow)
                                                    console.log('vitem after FI', vitem)
                                                    if(diffDate == dateNow ){
                                                        vitem.isFI = vitem.JobSplitChip[z].isFI
                                                        vitem.Status = 12
                                                        if (vitem.JobSplitChip[z].TimeFinish != null){
                                                            vitem.TimeFinish = angular.copy(vitem.JobSplitChip[z].TimeFinish)
                                                        }
                                                        TempTimeFinish = vitem.TimeFinish.split(':');
                                                    //    JobSplitId = vitem.JobSplitChip[z].JobSplitId;
                                                    //    vitem.StallId = vitem.JobSplitChip[z].StallId;
                                                    //    vitem.PlanStart = vitem.JobSplitChip[z].PlanStart; // ini isi nya jam 
                                                    //    vitem.PlanFinish = vitem.JobSplitChip[z].PlanFinish; // ini isinya jam jg
                                                    //    var planStart = me.timeToColumn2(vitem.PlanStart);
                                                    //    var actualFinish =  me.timeToColumn2(vitem.PlanFinish);
                                                    //    vitem.JumlahActualRate = actualFinish - planStart;
                                                    //    vitem.JumlahActualRate = Math.ceil(Math.abs(vitem.JumlahActualRate)*4)/4;
                
                                                    //    var cekAwal = vitem.JobSplitChip[z].PlanStart.split(':');
                                                    //    var cekAkhir = vitem.JobSplitChip[z].PlanFinish.split(':');
        
                                                    console.log('date start clock after FI', vitem.DateStart)
                                                    console.log('date finish clock after FI', vitem.DateFinish)    
                                                    var realFinishClockDate = new Date(vitem.DateFinish);
                                                    var planFinishClockDate = new Date(vitem.JobSplitChip[z].PlanDateFinish);
                                                    console.log('realFinishClockDate after FI',realFinishClockDate)
                                                    console.log('planFinishClockDate after FI',planFinishClockDate)
        
                                                    if (realFinishClockDate.toString() == planFinishClockDate.toString()){
                                                        //di sini harus di cek kalau dia clock off sebelum plan nya, chip nya mendekkin
                                                        console.log('nih cek mendek after FI')
                                                        if (parseInt(TempTimeFinish[0]) < parseInt(TempPlanFinish[0]) && vitem.JobSplitChip[z].isFI == 1){
                                                                vitem.PlanFinish = vitem.TimeFinish;
                                                                var planStart = me.timeToColumn2(vitem.JobSplitChip[z].PlanStart);
                                                                var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
                                                                console.log('planStart =====', planStart)
                                                                console.log('actualFinish ======', actualFinish)
                                                                vitem.JumlahActualRate = actualFinish - planStart;
                                                                vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                                                if (vitem.JumlahActualRate <= 0){
                                                                    vitem.JumlahActualRate = 0.25;
                                                                }
                                                        } else {
                                                            if (parseInt(TempTimeFinish[1]) < parseInt(TempPlanFinish[1]) && parseInt(TempTimeFinish[0]) <= parseInt(TempPlanFinish[0]) && vitem.JobSplitChip[z].isFI == 1){
                                                                    vitem.PlanFinish = vitem.TimeFinish;
                                                                    var planStart = me.timeToColumn2(vitem.JobSplitChip[z].PlanStart);
                                                                    var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
                                                                    console.log('planStart =====', planStart)
                                                                    console.log('actualFinish ======', actualFinish)
                                                                    vitem.JumlahActualRate = actualFinish - planStart;
                                                                    vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                                                    if (vitem.JumlahActualRate <= 0){
                                                                        vitem.JumlahActualRate = 0.25;
                                                                    }
                                                            }
                                                        }
        
                                                    }
        
                                                    //    if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                                                    //        vitem.JumlahActualRate = vitem.JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                                                    //    }
                                                    } else {
                                                    
                                                    }
                                                } 
                                            } else {

                                            }
                                            
                                            
                                        }
                                    } else {

                                        var dateNow = me.changeFormatDateStrip(arg.currentDate);
                                        var diffDate = '';
                                        for(var z in vitem.JobSplitChip){
                                            if (posisiLastSplitId == vitem.JobSplitChip[z].JobSplitId){
                                                diffDate = me.changeFormatDateStrip(vitem.JobSplitChip[z].PlanDateStart)
                                                console.log('diffdate', diffDate)
                                                console.log('dateNow', dateNow)
                                                console.log('vitem', vitem)
                                                if(diffDate == dateNow ){
                                                    vitem.isFI = vitem.JobSplitChip[z].isFI
                                                //    JobSplitId = vitem.JobSplitChip[z].JobSplitId;
                                                //    vitem.StallId = vitem.JobSplitChip[z].StallId;
                                                //    vitem.PlanStart = vitem.JobSplitChip[z].PlanStart; // ini isi nya jam 
                                                //    vitem.PlanFinish = vitem.JobSplitChip[z].PlanFinish; // ini isinya jam jg
                                                //    var planStart = me.timeToColumn2(vitem.PlanStart);
                                                //    var actualFinish =  me.timeToColumn2(vitem.PlanFinish);
                                                //    vitem.JumlahActualRate = actualFinish - planStart;
                                                //    vitem.JumlahActualRate = Math.ceil(Math.abs(vitem.JumlahActualRate)*4)/4;
            
                                                //    var cekAwal = vitem.JobSplitChip[z].PlanStart.split(':');
                                                //    var cekAkhir = vitem.JobSplitChip[z].PlanFinish.split(':');

                                                console.log('date start clock', vitem.DateStart)
                                                console.log('date finish clock', vitem.DateFinish)    
                                                var realFinishClockDate = new Date(vitem.DateFinish);
                                                var planFinishClockDate = new Date(vitem.JobSplitChip[z].PlanDateFinish);
                                                console.log('realFinishClockDate',realFinishClockDate)
                                                console.log('planFinishClockDate',planFinishClockDate)

                                                if (realFinishClockDate.toString() == planFinishClockDate.toString()){
                                                    //di sini harus di cek kalau dia clock off sebelum plan nya, chip nya mendekkin
                                                    console.log('nih cek mendek')
                                                    if (parseInt(TempTimeFinish[0]) < parseInt(TempPlanFinish[0])){
                                                            vitem.PlanFinish = vitem.TimeFinish;
                                                            var planStart = me.timeToColumn2(vitem.JobSplitChip[z].PlanStart);
                                                            var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
                                                            console.log('planStart =====', planStart)
                                                            console.log('actualFinish ======', actualFinish)
                                                            vitem.JumlahActualRate = actualFinish - planStart;
                                                            vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                                            if (vitem.JumlahActualRate <= 0){
                                                                vitem.JumlahActualRate = 0.25;
                                                            }
                                                    } else {
                                                        if (parseInt(TempTimeFinish[1]) < parseInt(TempPlanFinish[1]) && parseInt(TempTimeFinish[0]) <= parseInt(TempPlanFinish[0])){
                                                                vitem.PlanFinish = vitem.TimeFinish;
                                                                var planStart = me.timeToColumn2(vitem.JobSplitChip[z].PlanStart);
                                                                var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
                                                                console.log('planStart =====', planStart)
                                                                console.log('actualFinish ======', actualFinish)
                                                                vitem.JumlahActualRate = actualFinish - planStart;
                                                                vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                                                if (vitem.JumlahActualRate <= 0){
                                                                    vitem.JumlahActualRate = 0.25;
                                                                }
                                                        }
                                                    }

                                                }

                                                //    if(parseInt(cekAwal[0])<= 12 && cekAkhir[0] > 12){
                                                //        vitem.JumlahActualRate = vitem.JumlahActualRate -1; // di kurangin karena di updatechipbreak uda di tambah 1 jam buat break
                                                //    }
                                                } else {
                                                
                                                }
                                            }
                                            
                                        }
                                    }
                                    
                                    
                                } else {
                                    //cek jam nya dl actual < plan ga?
                                    if (parseInt(TempTimeFinish[0]) < parseInt(TempPlanFinish[0])){
                                        vitem.PlanFinish = vitem.TimeFinish;
                                        var planStart = me.timeToColumn2(vitem.PlanStart);
                                        var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
    
                                        console.log('planStart =====', planStart)
                                        console.log('actualFinish ======', actualFinish)
                                        vitem.JumlahActualRate = actualFinish - planStart;
                                        vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                        if (vitem.JumlahActualRate <= 0){
                                            vitem.JumlahActualRate = 0.25;
                                        }
    
                                        
                                    } else {
                                        if (parseInt(TempTimeFinish[1]) < parseInt(TempPlanFinish[1]) && parseInt(TempTimeFinish[0]) <= parseInt(TempPlanFinish[0])){
                                            vitem.PlanFinish = vitem.TimeFinish;
                                            var planStart = me.timeToColumn2(vitem.PlanStart);
                                            var actualFinish =  me.timeToColumn2(vitem.TimeFinish);
    
                                            console.log('planStart', planStart)
                                            console.log('actualFinish', actualFinish)
                                            var cekplanstart = vitem.PlanStart.split(':')
                                            var cektimefinish = vitem.TimeFinish.split(':')
                                            var adaIstirahat = 0;
                                            if (parseInt(cekplanstart[0])<= 12 && parseInt(cektimefinish[0]) >= 13){
                                                adaIstirahat = 1;
                                            }
                                            vitem.JumlahActualRate = actualFinish - planStart - adaIstirahat;
                                            vitem.JumlahActualRate = Math.ceil((vitem.JumlahActualRate)*4)/4;
                                            if (vitem.JumlahActualRate <= 0){
                                                vitem.JumlahActualRate = 0.25;
                                            }
                                        }
                                    }
                                }
                                
    
                            } else {
                                // ini kalau ada joblistsplitchip dan isfi nya ada yg uda keisi

                            }
                        }

                        //cek rawat jalan kl uda di split
                        if (vitem.Status == 25){
                            // cek kl uda di split 
                            if (vitem.JobSplitChip != null && vitem.JobSplitChip != undefined && vitem.JobSplitChip.length > 0){
                                if (vitem.isSplitActive == 0){
                                    vitem.Status = 12
                                    vIsSplitable = 0;
                                }
                            }
                        }
                       
                        

                        var vdata = {
                            chip: 'chip',
                            jobId: vitem.JobId,
                            AppointmentTime: vitem.PlanStart,
                            jamcetakwo: me.reformatTime(vitem.PlanStart),
                            jamjanjiserah: me.reformatTime(vitem.PlanFinish),
                            nopol: vitem.nopol,
                            type: vitem.ModelType,
                            normalDuration: '01:00',
                            // normal: vitem.TimeDuration * 60 / me.incrementMinute, // ini bikin chip agak aneh di break, kita cb ga pake timeduration tp pk AR
                            normal: vitem.JumlahActualRate * 60 / me.incrementMinute,
                            ext: vitem.Extension * 60 / me.incrementMinute,
                            stallId: vitem.StallId,
                            row: 0, 
                            col: vcolumn,
                            width: 0,
                            daydiff: 0,
                            PlanDateStart: vitem.PlanDateStart,
                            PlanStart: vitem.PlanStart,
                            PlanDateFinish: vitem.PlanDateFinish,
                            PlanFinish: vitem.PlanFinish,
                            isAppointment: vitem.isAppointment,
                            status: vstatus,
                            intStatus: vitem.Status,
                            dontDrag: vDontDrag,
                            teknisi: vitem.Teknisi,
                            pekerjaan: vitem.JenisPekerjaan,
                            jmltask: vitem.jmltask,
                            allocated: ((vitem.JmlTeknisi >= 1) ? 1 : 0),
                            isCarryOver: vIsCarryOver,
                            isWaitingPart: vitem.isWaitingPart,
                            isWaiting : vitem.isWaiting,
                            isInStopPageGr: vitem.IsInStopPageGr,
                            isSplitable: vIsSplitable,
                            PrediagnoseScheduledTime: vitem.PreDiagnoseScheduledTime,
                            PreDiagnoseStallId: vitem.PreDiagnoseStallId,
                            fl_reqjob: vitem.fl_reqjob,
                            JobSplitId: JobSplitId,
                            statusicon: ['customer'],
                            wBreak : 0,
                            isSplitActive: vitem.isSplitActive,
                            isFI: vitem.isFI,
                            TimeFinish : vitem.TimeFinish,
                            TimeStart : vitem.TimeStart,
                            PauseDate: vitem.PauseDate,
                            PauseTime: vitem.PauseTime,
                        }

                        if (repairProsesBP == 1){
                            if (vdata.ext > 0){
                                vdata.ext = vdata.ext / me.incrementMinute;
                                vdata.normal = (vitem.TimeDuration * 60 / me.incrementMinute) - vdata.ext; //dikurang ext karena dr belakang uda termasuk ext.. nanti ngaco di width nya kalau ga di kurang dl
                            } else {
                                vdata.normal = vitem.TimeDuration * 60 / me.incrementMinute;
                            }
                        }

                        // di ganti dl counting buat inprogress nya, cek dari intstatus nya lgsg jng dari vstatus
                        // if (vstatus == 'inprogress') {
                        //     vInProgress = vInProgress + 1;
                        // }
                        if ((vdata.intStatus == 5 || vdata.intStatus == 7 || vdata.intStatus == 9) && vdata.isInStopPageGr == 0) {
                            vInProgress = vInProgress + 1;
                        }
                        if (vIsCarryOver) {
                            vCarryOver = vCarryOver + 1;
                        }
                        var isPush = true;
                        if(repairProsesBP == 0){
                            // if ((vitem.Status == 1)) {
                            //     // isPush = true;
                            //     // vNoShow = vNoShow + 1;
                            // }
                        // } else {
                            if ((vitem.Status == 1) || (vitem.Status == 2)) {
                                isPush = false;
                                vNoShow = vNoShow + 1;
                            }
                        }
                        // if ((vitem.Status == 1) || (vitem.Status == 2)) {
                        //     isPush = false;
                        //     vNoShow = vNoShow + 1;
                        // }
                        if (vitem.isAppointment) {

                            if (vitem.Status == 3) {
                                var vTime = vdata.PlanStart;
                                var vcurmin = me.getCurrentMinute();
                                var vchpmin = JsBoard.Common.getMinute(vTime);
                                var vdelta = me.noShowTolerance;
                                //(vapptime+vdelta<vcurrmin){
                                // if (vchpmin + vdelta < vcurmin) {
                                //     //vstall = -1;
                                //     // vitem.Status = 2;
                                //     vdata.status = 'no_show';
                                //     vdata.isInStopPageGr = 1;
                                // }

                            }

                            // if (vitem.Status >= 4 && datePlantTest == dateTest) {
                            //   console.log('masuk if booking',datePlantTest,'==',dateTest);
                            //     vBookingA = vBookingA + 1;
                            
                            // }
                            // if(datePlantTest == dateTest){
                            //     vBookingB = vBookingB + 1;
                            // }

                        }
                        me.updateStatusIcons(vdata);
                        vdata.width = vdata.normal + vdata.ext;
                        if (vdata.width <= 0) {
                            if (cekEndOfCO == 1){
                                isPush = false;
                            } else {
                                vdata.width = 1;
                            }
                            // vdata.width = 1;
                        }

                        if ((vdata.intStatus == 6 || vdata.intStatus == 25)) {
                            vdata.paused = 1;
                        } else {
                            vdata.paused = 0;
                        }

                        if ((vdata.intStatus == 8)) {
                            vdata.waiting_fi = 1;
                        } else {
                            vdata.waiting_fi = 0;
                        }

                        if ((vdata.fl_reqjob == 1)) {
                            vdata.additional = 1;
                        } else {
                            vdata.additional = 0;
                        }


                        if (((vdata.stallId !== 0) || (vdata.isInStopPageGr == 1)) && isPush) {
                            //if (if (((vdata.stallId!==0) || ((vdata.isInStopPageGr==1) && (vArg.showStoppage) )) && isPush){)
                            if (vdata.isInStopPageGr == 1) {
                                if (vArg.showStoppage) {
                                    result.push(vdata);
                                    vitem.Status = angular.copy(statusAwal);
                                }
                            } else {
                                vProductCapacity = vdata.width + vProductCapacity;
                                result.push(vdata);
                                vitem.Status = angular.copy(statusAwal);

                            }


                        }

                        console.log("vdata", vdata);
                        // break;

                        if (adaSplit>1){
                            i--;
                            adaSplit--;
                        }
                        
                    }
                    arg.countNoShow = vNoShow;
                    arg.countBookingA = vBookingA;
                    arg.countBookingB = vBookingB;
                    arg.countInProgress = vInProgress;
                    arg.countCarryOver = vCarryOver;
                    arg.countWIP = result.length;
                    arg.countProductionCapacity = ((vProductCapacity/me.tmpDurasiStall)*100).toFixed(1);
                    // try re-order chip, supaya chip yg lebih kanan kolomnya ada di atas =============================
                    var SortedData2 = angular.copy(result);
                    SortedData2 = SortedData2.sort(function(a, b){
                        var A=a.col, B=b.col
                        return A-B //sort by date ascending
                    })
                    result = SortedData2
                    // try re-order chip, supaya chip yg lebih kanan kolomnya ada di atas =============================
                    onFinish(result);
                })


            },

            //undo sampe sini masih bener.. tapi break masih di jam 12

            isOverlapped2: function(x1, x2, y1, y2) {
                return (x1 < y2) && (y1 < x2)
            },

            timeToColumn2: function(vtime) {
                var vstartmin = JsBoard.Common.getMinute(this.startHour);
                var vmin = JsBoard.Common.getMinute(vtime) - vstartmin;
                var vresult = vmin / this.incrementMinute;

                return vresult;
            },

            getBreakTime: function(vstall) {
                console.log('break hour nih coy', this.breakHour);
                console.log('vstall break hour nih coy', vstall);
                var arrStartBreakHour = [];
                var arrEndBreakHour = [];
                var arrDurationBreakHour = [];
                arrStartBreakHour.push(vstall.startBreakTime1);
                arrStartBreakHour.push(vstall.startBreakTime2);
                arrStartBreakHour.push(vstall.startBreakTime3);

                arrEndBreakHour.push(vstall.endBreakTime1);
                arrEndBreakHour.push(vstall.endBreakTime2);
                arrEndBreakHour.push(vstall.endBreakTime3);

                arrDurationBreakHour.push(vstall.durationBreak1);
                arrDurationBreakHour.push(vstall.durationBreak2);
                arrDurationBreakHour.push(vstall.durationBreak3);

                console.log('nih jadwal istirahatnya per stall',arrStartBreakHour);

                var arrayBreakTime = [];

                for (var i=0; i<3; i++){
                    if (arrStartBreakHour[i] != null && arrStartBreakHour[i] != undefined) {
                        var objBreakTime = {};

                        var vstartMin = JsBoard.Common.getMinute(arrStartBreakHour[i]);
                        var vendMin = vstartMin + arrDurationBreakHour[i];
                        var vBreakEnd = JsBoard.Common.getTimeString(vendMin);
                        var vstart = this.timeToColumn2(arrStartBreakHour[i]);
                        var vend = this.timeToColumn2(vBreakEnd);
                        objBreakTime.start = vstart;
                        objBreakTime.end = vend;
                        objBreakTime.width = vend - vstart;
                        arrayBreakTime.push(objBreakTime);
                        
                    }
                }
                console.log('nih ah sialan', arrayBreakTime);
                // var vstartMin = JsBoard.Common.getMinute(this.breakHour);
                // var vendMin = vstartMin + 60;
                // var vBreakEnd = JsBoard.Common.getTimeString(vendMin);
                // var vstart = this.timeToColumn2(this.breakHour);
                // var vend = this.timeToColumn2(vBreakEnd);
                // objBreakTime.start = vstart;
                // objBreakTime.end = vend;
                // objBreakTime.width = vend - vstart;
                // arrayBreakTime.push(objBreakTime);

                // return [{
                //     start: vstart,
                //     end: vend,
                //     width: vend - vstart,
                // }]
                return arrayBreakTime;
            },

            getBreakChipTime: function(vstart, vlength, vstall) {
                console.log("getBreakChipTime", vstart, vlength, vstall);
                var vbreaks = this.getBreakTime(vstall);
                console.log("vbreaks", vbreaks);
                var vbreakCount = 0;
                var vtotalBreak = 0;
                var vresult = {}

                console.log('banyak looping', vbreaks.length);
                for (var i = 0; i < vbreaks.length; i++) {
                    vbreak = vbreaks[i];
                    // (x1 <= y2) && (y1 < x2)
                    console.log('logic', vstart, vstart + vlength, vbreak.start, vbreak.end)
                    console.log('result', this.isOverlapped2(vstart, vstart + vlength, vbreak.start, vbreak.end))
                    if (this.isOverlapped2(vstart, vstart + vlength, vbreak.start, vbreak.end)) {
                        console.log("===============>", vstart, vbreak.start);
                        if (vstart > vbreak.start) {
                            return 'start-inside-break';
                        }
                        vbreakCount = vbreakCount + 1;
                        if (vbreakCount == 1) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vstart;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength + vbreak.width;
                            vresult['break1On'] = 1;
                        } else if (vbreakCount == 2) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vlength = vlength + vbreak.width;
                            vresult['break2On'] = 1;
                        } else if (vbreakCount == 3) {
                            vresult['break' + vbreakCount + 'start'] = vbreak.start - vbreaks[i-1].end;
                            vresult['break' + vbreakCount + 'width'] = vbreak.width;
                            vresult['break3On'] = 1;
                        }
                      
                        vtotalBreak = vtotalBreak + vbreak.width;
                        console.log('vtotalBreak coeg', vtotalBreak);

                    }
                }
                if (vbreakCount == 0) {
                    return 'no-break';
                } else {
                    vresult['breakWidth'] = vtotalBreak;
                }
                console.log('vresult coeg', vresult);

                return vresult;

            },

            updateChipBreak: function(vchipData, vstall) {
                console.log('vchipdata anj', vchipData);
                if (vchipData.breakWidth) {
                    console.log('coy kurangin dl')
                    vchipData.width = vchipData.width - vchipData.breakWidth;
                }
                var vbreaks = 'no-break';
                if (vchipData.stallId != 0) {
                    
                    vbreaks = this.getBreakChipTime(vchipData.col, vchipData.width, vstall);
                }

                var deleteList = ['break1start', 'break1width', 'break2start', 'break2width', 'break3start', 'break3width', 'break4start', 'break4width', 'break5start', 'break5width', 'break6start', 'break6width', 'breakWidth'];
                for (var i = 0; i < deleteList.length; i++) {
                    var idx = deleteList[i];
                    delete vchipData[idx];
                }
                if (typeof vbreaks == 'object') {
                    for (var idx in vbreaks) {
                        vchipData[idx] = vbreaks[idx];
                    }
                }

                
                if (vchipData.breakWidth) {
                    console.log('vbreaks nih On kaga', vbreaks)
                    if(vbreaks.break3On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth; // ini di comment karena kl yg nabrak break jd nambah 1 jam..
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                       
                        vchipData.width3 = vchipData.break3start;
                        vchipData.start4 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width + vchipData.break3start + vchipData.break3width;
                        vchipData.width4 = vchipData.width - vchipData.start4;
                        vchipData.wBreak = vchipData.break1width + vchipData.break2width + vchipData.break3width;

                        // vchipData.width2 = vchipData.width - vchipData.start2;
                        // vchipData.width2 = vchipData.break2start;
                        // vchipData.start3 = vchipData.break2start + vchipData.break2width;
                        // vchipData.width3 = vchipData.break3start;
                    } else if (vbreaks.break2On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth; // ini di comment karena kl yg nabrak break jd nambah 1 jam..
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;

                        vchipData.width2 = vchipData.break2start;
                        vchipData.start3 = vchipData.break1start + vchipData.break1width + vchipData.break2start + vchipData.break2width;
                        vchipData.width3 = vchipData.width - vchipData.start3;
                        vchipData.wBreak = vchipData.break1width + vchipData.break2width;

                         // vchipData.width2 = vchipData.width - vchipData.start2;

                        // vchipData.width2 = vchipData.break2start;
                        // vchipData.start3 = vchipData.break2start + vchipData.break2width;

                    } else if (vbreaks.break1On == 1) {
                        vchipData.width = vchipData.width + vchipData.breakWidth; // ini di comment karena kl yg nabrak break jd nambah 1 jam..
                        vchipData.width1 = vchipData.break1start;
                        vchipData.start2 = vchipData.break1start + vchipData.break1width;
                        vchipData.width2 = vchipData.width - vchipData.start2;
                        vchipData.wBreak = vchipData.break1width;
                    }
                    
                } else {
                    vchipData.width1 = vchipData.width;
                }
                // console.log('vchipData', vdata);

                // console.log('vbreaks', vbreaks);
                //////////
            },

            getCurrentTime: function () {
                return jpcbFactory.getCurrentTime();
                // var vDate = new Date();
                // //vDate.setHours(vDate.getHours()-this.hourDelay);
                // // vDate.setMinutes(vDate.getMinutes()-this.hourDelay*60);
                // vDate.setMinutes(vDate.getMinutes()-this.minuteDelay);
                // return vDate;
            },
            getScrollablePos: function (boardname) {
                return jpcbFactory.getScrollablePos(boardname);
            },
            updateChipStatus: function (boardname) {
                // var boardname = this.boardName;
                var me = this;
                var vboard = jpcbFactory.getBoard(boardname);
                jpcbFactory.processChips(boardname, function (vchip) {
                    return true;
                }, function (vchip) {
                    var vitem = vchip.data;
                    var vstatus = me.checkAppointmentStatus(vitem.AppointmentTime);
                    if (vitem.status !== vstatus) {
                        vitem.status = vstatus;
                        vboard.updateChip(vchip);
                    }
                    if (vitem.status == 'no_show') {
                        vchip.draggable(true);
                    }
                });

            },

            updateTimeLine: function (boardname, onFinish) {
                cekTimeline = cekTimeline + 1;
                // var boardname = this.boardName;
                var vDate = this.getCurrentTime();
                // console.log("current Time#####", vDate.getTime());
                var currentHour = $filter('date')(vDate, 'HH:mm');
                var realDateFull = new Date();
                // console.log('vdate cek bos', CopyTglAwal)
                // console.log('currentHour cek bos', currentHour)
                // console.log('realDateFull cek bos', realDateFull)
                if(CopyTglAwal > realDateFull){
                    currentHour = "00:00";
                }
                // console.log("currentHour", currentHour);
                //var startHour = this.
                //var minutPerCol
                jpcbFactory.updateTimeLine(boardname, currentHour, this.startHour, this.incrementMinute);


                //cek jam  sekrang lebih besar dari jam plan finish chip jika lebih besar maka di blok
                //cuyyy
                //this.updateChipStatus(boardname);

                this.checkChipStatus(boardname);

                var vboard = jpcbFactory.getBoard(boardname);
                if (vboard) {
                    vboard.redrawChips();
                }

                jpcbFactory.updateStandardDraggable(boardname);
                jpcbFactory.redrawBoard(boardname);
            },

            statusFinalinspektion: function (jobid) {
                var res = $http.get('/api/as/JobInspections/job/', jobid);
                console.log("Hasil status Finalinspektion : ", res);
                return res;
            },
            checkChipStatus: function (boardname) {
                // test 1234
                var vboard = jpcbFactory.getBoard(boardname);
                if (vboard) {
                    for (var i = 0; i < vboard.chipItems.length; i++) {
                        var vitem = vboard.chipItems[i];
                        vitem.status = this.checkStatus(vitem, 'intStatus');
                        //vitem.status = 'irregular';
                    }

                }
                // if (typeof jpcbFactory.boardConfigList[boardname] !== 'undefined'){
                //     // vconfig = jpcbFactory.boardConfigList[boardname];
                //     // for(var i=0; i<vconfig.allChips.length; i++){
                //     //     var vitem = vconfig.allChips[i];
                //     //     vitem.status = this.checkStatus(vitem);
                //     //     //vitem.status = 'irregular';
                //     // }
                // }
                // var boardname = this.boardName;
                // var me = this;
                // var vboard = jpcbFactory.getBoard(boardname);
                // jpcbFactory.processChips(boardname, function(vchip){
                //     return true;
                // }, function(vchip){
                //     var vitem = vchip.data;
                //     //vitem.status = me.checkStatus(vitem, 'intStatus');
                //     //vitem.status = 'irregular';
                //     vchip.data.status = '';
                // });

            },
            getBoard: function (boardname) {
                return jpcbFactory.getBoard(boardname);
            },

            // onGetChips: function(arg){
            //     var vArg = {nopol: 'B 7100 ACA', tipe: 'Vios'}
            //     vArg = JsBoard.Common.extend(vArg, arg);
            //     var vitems = [{chip: 'estimasi', nopol: vArg.nopol, tipe: vArg.tipe, row: vArg.row, col: vArg.col, width: vArg.width, selected: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7150 BDA', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7151 CMY', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7152 KLM', tipe: 'Avanza', row: 1, col: 2.5, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7153 PKD', tipe: 'Avanza', row: 2, col: 0, width: 1, daydiff: 0, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7154 MND', tipe: 'Avanza', row: 2, col: 1, width: 1, daydiff: 0, status: 'extension', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7155 FSD', tipe: 'Avanza', row: 3, col: 0, width: 2, daydiff: 0, status: 'block', statusicon: ['customer']},
            //         {chip: 'estimasi', nopol: 'B 7156 UHJ', tipe: 'Avanza', row: 0, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7157 JLK', tipe: 'Avanza', row: 1, col: 0, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //         {chip: 'estimasi', nopol: 'B 7158 MDS', tipe: 'Avanza', row: 1, col: 2, width: 1, daydiff: 1, status: 'appointment', statusicon: ['customer_gray']},
            //     ]
            //     var today = new Date();
            //     if (arg.currentDate){
            //         var vdiff = this.dayDiff(today, arg.currentDate);
            //         var vres = []
            //         for (var i=0; i<vitems.length; i++){
            //             if ((vitems[i].daydiff==vdiff) ||(vitems[i]['row']<0)){
            //                 vres.push(vitems[i]);
            //             }
            //         }
            //         return vres;
            //     }
            //     return []
            // },
            dayDiff: function (date1, date2) {
                return Math.round((date2 - date1) / (1000 * 60 * 60 * 24));
            },

            // cobain cari di stoppage start here ===============================
            searchStoppage: function (keyword){
                keywordNopol = keyword;
                console.log('ini keword Nopol fac', keywordNopol);
                console.log('tes vname, vcontainer', CopyVNAME, ' noh-ah ', CopyVCONTAINER)
                jpcbFactory.showBoard(CopyVNAME, CopyVCONTAINER);
            },
            // cobain cari di stoppage end here ===============================

            //kirim allChips buat cek expired chip
            dataAllChip: function (){
                return dataSemuaChip
            },

            cekPlotKeDB: function(data){
                var res = $http.put('/api/as/ValidationApptoStall?flag=3', data);
                console.log("hasil cek ke DB : ", res);
                return res;
            },
            getStatWO: function (JobId, status) {
                return $http.get('/api/as/Jobs/GetStatusJob/' + JobId + '/' + status);
            },
            JPCBFindDate: function (PoliceNo) {
                return $http.get('/api/as/Boards/JPCBFindDate/' + PoliceNo);
            },

            
            CheckIsJobCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.get('/api/as/CheckIsJobCancelWO/'+jobId);
            },
            
        }
    });
