angular.module('app')
.controller('PartsSupplyController', function($scope, $http, CurrentUser,PartsCurrentUser, Parameter,$timeout,$interval, $window, $filter, Idle) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      // $scope.GetTotalwaiting_issuing();
      // $scope.GetTotalDataPrePicking();
      $scope.GetAutoRefreshBoard();
      // $scope.countdown();
      Idle.unwatch();
      console.log('==== value of idle ==', Idle)
    });
    $scope.$on('$destroy', function(){
      Idle.watch();
    });

        var arrBoard = []
        var allBoardHeight = 550;
        $scope.loading=true;
        $scope.gridData=[];
        $scope.waktu_sekarang = 'xxxx'; 
        $scope.loading = [true, true]
        $scope.DataFromLimit = [];
        $scope.counterFromData;

        $scope.count = {}
        $scope.clock = "loading clock..."; // initialise the time variable
        $scope.tickInterval = 1000 //ms
        $scope.hourminute = '';
        var firstTime = true;
        var limitCount = 0;
        $scope.porderboard = {}
        $scope.TotalDataPrePicking = 0;
        $scope.TotalDataIssuing = 0;
        var TimeAutoRefresh = 30000;

        var showData = 5;
        var countersBoard = {
          waiting_pre_picking:{
            StartCountingData:0,
            Data:[],
            Counts:0,
            BagiCountingData:0,
            ModulusCountingData:0,
            ClearData:true
          },
          waiting_issuing:{
            StartCountingData:0,
            Data:[],
            Counts:0,
            BagiCountingData:0,
            ModulusCountingData:0,
            ClearData:true
          },
        }
        
         // ===================== GET WAREHOUSE
        
         
         $scope.user = CurrentUser.user();
         $scope.GetWarehouseId = function(){
          PartsCurrentUser.getWarehouseIdi().then(function(res) {        
            for (var i=0; i < res.data.length; i++) {  
              $scope.WarehouseId = res.data[i].warehouseid;
            }       
            
          });
          }
           console.log("user",$scope.user);
           $scope.MaterialTypeId = 0;
           $scope.ServiceTypeId  = 0;
          switch ($scope.user.RoleId) {
            case 1135:
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 1;
                break;
            case 1122:
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 1;
                break;
            case 1123:
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 0;
                break;
            case 1124:
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 0;
                break;
            case 1125:
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 1;
                break;
          }
          $scope.WarehouseId = 0;
        //  =====TEST =====
        //   PartsCurrentUser.getWarehouseId($scope.ServiceTypeId, $scope.MaterialTypeId, $scope.user.OrgId).then(function(res) {
        //          var gridData = res.data;
        //          console.log(gridData[0]);
        //          $scope.WarehouseId = 0;
        //          $scope.OutletId = 0;
        //          $scope.WarehouseId = gridData[0].WarehouseId;
        //          console.log("Iniservise2a",$scope.WarehouseId)
        //          $scope.OutletId = gridData[0].OutletId;
        //          $scope.countdown();

        //      },
        //      function(err) {
        //          console.log("err=>", err);
        //      }
        //  );

        

         // ===================== GET WAREHOUSE SAMPAI SINI

         $scope.countdown = function() {
           
              stopped = $timeout(function() {
                    $scope.countdown();
                    $scope.counter--;
                    if($scope.counter == 0)
                    {
                      console.log("====> refresh data ...");

                      loadAllData();
                      $scope.porderboard.doFilter();
                      $scope.porderboard.resetFilter();
                      $scope.counter = $scope.counterFromData;
                    }
               }, 1000);

         }

         $scope.GetAutoRefreshBoard= function(){
               var url = '/api/as/Boards/partsupplyGetTimeRefresh/9002';
               // var res=$http.get(url);
                $http.get(url).then(function (res) {
                  if(res.data.length > 0){
                    TimeAutoRefresh = res.data[0].NumVal1;
                    $scope.counterFromData =  res.data[0].NumVal1;
                    $scope.counter = $scope.counterFromData;
                    
                    var TimeAuto = parseInt(TimeAutoRefresh) * 1000;
                    TimeAutoRefresh = TimeAuto;
                    console.log("TimeAutoRefresh res",TimeAutoRefresh, TimeAuto);
                  }else{
                    TimeAutoRefresh = 30;
                    $scope.counterFromData =  30;
                    $scope.counter = $scope.counterFromData;
                    
                    var TimeAuto = parseInt(TimeAutoRefresh) * 1000;
                    TimeAutoRefresh = TimeAuto;
                    console.log("TimeAutoRefresh res",TimeAutoRefresh, TimeAuto);
                  }
                  
                
               });
           }

         
 
           $scope.GetTotalwaiting_issuing= function(){
            $scope.GetWarehouseId();
              console.log("ini warehosenya", $scope.WarehouseId);
               //keterangan Parameter pertama (1) untuk menentukan pre picking atau atau (2) mengunggu issue
               //kedua (0) = jika (0) select all data jika (!= 0) hanya sesuai limit yg ditampilkan
               //ketiga (1) = mengambil data atau (!=0) mengambil jumlah data
               var url = '/api/as/Boards/partsupplyboard/0/0/1/'+ $scope.WarehouseId;
               // var res=$http.get(url);
               $http.get(url).then(function (res) {
                console.log("GetTotalwaiting_issuing res",res.data);
                $scope.TotalDataIssuing = res.data;
               });
           }
           $scope.GetTotalDataPrePicking= function(){
            $scope.GetWarehouseId();
            console.log("ini warehosenya", $scope.WarehouseId);
            //keterangan Parameter pertama (1) untuk menentukan pre picking atau atau (0) mengunggu issue
             //kedua (0) = jika (0) select all data jika (!= 0) hanya sesuai limit yg ditampilkan
             //ketiga (1) = mengambil data atau (!=0) mengambil jumlah data
             var url = '/api/as/Boards/partsupplyboard/1/0/1'+$scope.WarehouseId;
             // var res=$http.get(url);
             $http.get(url).then(function (res) {
               console.log("GetTotalDataPrePicking res",res.data);
               $scope.TotalDataPrePicking  = res.data
              });
           }


        $scope.porderboard.doFilter= function(){
          console.log(this);
          vidx = this.category_filter;
          var vopt = {
            fieldname: this.field_filter,
            value: this.text_filter
          }
          for (var i=1; i<=arrBoard.length; i++){
            if ((i==vidx) || (vidx==0)){
              dataFiltering(arrBoard[i-1], vopt)
            } else {
              dataFiltering(arrBoard[i-1], {fieldname: '', value: ''})
            }
          }
        }
        $scope.porderboard.resetFilter= function(){
          console.log("reset-filter", this);
          this.category_filter = "0";
          this.field_filter = "all";
          this.text_filter = '';
          this.doFilter();
        }
        $scope.porderboard.resetFilter();

        var filterItem = function(vitem, vfield, vtext){
          if (vtext===''){
            return true;
          } else {
            if ((typeof vitem[vfield] !== 'undefined') && (vitem[vfield] !== null)){
              return vitem[vfield].search(new RegExp(vtext, "i"))>=0
              // if (vitem[vfield] === vtext) {
              //   return true;
              // }
            }
            return false;
          }
        }

        var filterAllField = function(vitem, vtext){
          if (vtext===''){
            return true;
          } else {
            for(vfield in vitem){
              if (typeof vitem[vfield] !== 'undefined'){
                var vstr = vitem[vfield]+'';
                var vresult = vstr.search(new RegExp(vtext, "i"))>=0
                if (vresult){
                  return true;
                }
                // if (vitem[vfield] === vtext) {
                //   return true;
                // }
              }
            }
            return false;
          }
        }
        
        var filterList = {
          'all': function(vdata, vtext){
            return filterAllField(vdata, vtext);
          },
          'nopol': function(vdata, vtext){
            return filterItem(vdata, 'nopol', vtext);
          },
          'appointment': function(vdata, vtext){
            return (vdata['TipeRequest']=='B') && filterItem(vdata, 'docno', vtext);
          },
          'wo': function(vdata, vtext){
            return (vdata['TipeRequest']=='W') && filterItem(vdata, 'docno', vtext);
          },
          'so': function(vdata, vtext){
            return (vdata['TipeRequest']=='S') && filterItem(vdata, 'docno', vtext);
          }
        }



        var filterData = function(vdata, vopt){
          if (vopt.fieldname===''){
            result = vdata;
          } else {
            result = [];
            for(var i=0; i<vdata.length; i++){
              if (typeof filterList[vopt.fieldname] !=='undefined'){
                if (filterList[vopt.fieldname](vdata[i], vopt.value)){
                  result.push(vdata[i]);
                }
              }
            }
          }
          return result;
        }

        var dataFiltering = function(vitem, voption){
          console.log("data filtering", vitem, voption);
          vdata = filterData(vitem.originalData, voption);
          console.log("vdata ======>",vdata);
          vboard = vitem.board;
          vboard.createChips(vdata);
          vboard.autoResizeStage();
          vboard.redraw();
          $scope.count[vitem.boardName] = vdata.length;

        }
        
        
        // $interval(function(){
        //     console.log("TimeAutoRefresh res",TimeAutoRefresh);
          
        //    console.log("tessss", limitCount);
        //    loadAllData();
        // }, TimeAutoRefresh);
        $scope.jumlahIssuing = 0;
        $scope.jumlahPrePick = 0;

        var loadAllData = function(){
          
          for (var i=0; i<arrBoard.length; i++){
              var vboard = arrBoard[i];
              console.log("LoadAllData", vboard);
              if(vboard.dataUrl){
                  $http({
                      method: 'GET',
                      url: vboard.dataUrl,
                      index: i,
                      // tempIdx: vboard.tempIdx,
                      board: vboard,
                  })
                    .then(function(resp){
                      console.log('aduh ada ada aja',resp)
                      if (resp.config.board.boardName == "waiting_pre_picking"){
                        $scope.jumlahPrePick = resp.data.Total;
                      } else if (resp.config.board.boardName == "waiting_issuing"){
                        $scope.jumlahIssuing = resp.data.Total;
                      }
                        var vboard = resp.config.board;
                        var tmpVboard = angular.copy(resp);
                        // ======================
                        var tmpData = [];
                        countersBoard[vboard.boardName].Data = [];
                        // countersBoard[vitem.boardName].Counts = vdata.length;
                        if (countersBoard[vboard.boardName].ClearData == true){
                            countersBoard[vboard.boardName].Counts = tmpVboard.data.Result.length;
                            countersBoard[vboard.boardName].ModulusCountingData = parseInt(countersBoard[vboard.boardName].Counts % showData);
                            countersBoard[vboard.boardName].BagiCountingData = parseInt(countersBoard[vboard.boardName].Counts / showData);
                            // if (countersBoard[vboard.boardName].ModulusCountingData != 0){
                            //   countersBoard[vboard.boardName].BagiCountingData = countersBoard[vboard.boardName].BagiCountingData + 1;
                            // }
                        }
                        if(countersBoard[vboard.boardName].StartCountingData < countersBoard[vboard.boardName].Counts ){
                          countersBoard[vboard.boardName].ClearData = false;
                          for(var i = countersBoard[vboard.boardName].StartCountingData; i < tmpVboard.data.Result.length; i++){
                            countersBoard[vboard.boardName].Data.push(tmpVboard.data.Result[i]);
                          }
                          countersBoard[vboard.boardName].StartCountingData = countersBoard[vboard.boardName].StartCountingData + showData;
                        }else{
                          countersBoard[vboard.boardName].StartCountingData = 0;
                          countersBoard[vboard.boardName].BagiCountingData = 0;
                          countersBoard[vboard.boardName].ClearData = true;
                          for(var i = countersBoard[vboard.boardName].StartCountingData; i < tmpVboard.data.Result.length; i++){
                            countersBoard[vboard.boardName].Data.push(tmpVboard.data.Result[i]);
                          }
                        }

                        // if(countersBoard[vboard.boardName].BagiCountingData != 0){
                        //   countersBoard[vboard.boardName].BagiCountingData = countersBoard[vboard.boardName].BagiCountingData - 1;
                        //   countersBoard[vboard.boardName].ClearData = false;
                        //   countersBoard[vboard.boardName].StartCountingData = countersBoard[vboard.boardName].StartCountingData + showData;
                        // }else{
                        //   countersBoard[vboard.boardName].StartCountingData = 0;
                        //   countersBoard[vboard.boardName].BagiCountingData = 0;
                        //   countersBoard[vboard.boardName].ClearData = true;
                        // }
                        
                        console.log("countersBoard ===========>",countersBoard);
                        resp.config.board.originalData = [];
                        resp.data.Result = [];
                        resp.config.board.originalData = countersBoard[vboard.boardName].Data;
                        resp.data.Result = countersBoard[vboard.boardName].Data;
                        if (vboard.processData){
                            vboard.originalData = vboard.processData(resp);
                            $scope.porderboard.doFilter();
                            $scope.loading[resp.config.index] = false;
                            console.log("vboard DATA ====>",vboard);
                        }

                  });
                  
              } else {
                  var vindex = i;
                  $timeout(function(){
                      //var vboard = resp.config.board;
                      vboard.originalData = []
                      $scope.porderboard.doFilter();
                      $scope.loading[vindex] = false;
                  },400)
                  // var vindex = i;
                  // $timeout(function(mode, vpar){
                  //     vboard = vpar.board;
                  //     vboard.originalData = []
                  //     $scope.porderboard.doFilter();
                  //     $scope.loading[vpar.index] = false;
                      
                  // },400, 0, true, {test: 1, board: vboard, index: vindex})
              }
          } 


        }
        
        var count = 0;
        var CountGetData = function(vData)
        {
          var counter=0;
          var limit = vData.length;
          
          $interval(function(){
              if(counter <= limit)
              {
                $scope.LoopData(counter,vData)
                counter = counter + 5;
              }
              else{
                counter = 0;
              }
          }, 3000);

        }

        $scope.LoopData = function(counter, vData)
        {
           $scope.DataFromLimit = [];
          for(var i = counter ; i < (counter + 5) ; i++)
          {
            $scope.DataFromLimit.push(vData[i]);
          }
        }

        var registerBoard = function(bConfig){
          var vboard = new JsBoard.Container(bConfig.boardConfig);
          var vitem = {
            board: vboard,
            dataUrl: bConfig.dataUrl,
            processData: bConfig.processData,
            originalData: bConfig.boardData,
            incField: bConfig.incField,
            incMode: bConfig.incMode, //inc or dec 
            boardName: bConfig.boardName
          }
          console.log("arrBoard", vitem);
          arrBoard.push(vitem);
          var voption = {
            fieldname: '',
            value: ''
          }
          dataFiltering(vitem, voption);
        }

        // angular.element($window).bind('resize', function () {
        //     console.log("on resize", $window.innerWidth);
        //     for (var i=0; i<arrBoard.length; i++){
        //       vbr = arrBoard[i];
        //       vbr.board.autoResizeStage();
        //       vbr.board.redraw();
        //       //vbr.redrawChips();
        //     }
        // });

        var updateAutoResize = function(){
            for (var i=0; i<arrBoard.length; i++){
              vbr = arrBoard[i];
              vbr.board.autoResizeStage();
              vbr.board.redraw();
              //vbr.redrawChips();
            }          
        }

        var velement = document.getElementById('parts-order-supply-panel');
        var erd = elementResizeDetectorMaker();
        erd.listenTo(velement, function(element) {
          updateAutoResize();
        });
        
        

        var incrementTime = function(varrdata, vfield){
          for(var i=0; i<varrdata.length; i++){
            var vdata = varrdata[i];
            var vmin = JsBoard.Common.getMinute(vdata[vfield]);
            var vinc = 1;
            var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
            vdata[vfield] = vsmin;
          }
        }

        var decrementTime = function(varrdata, vfield){
          for(var i=0; i<varrdata.length; i++){
            var vdata = varrdata[i];
            var vmin = JsBoard.Common.getMinute(vdata[vfield]);
            var vinc = -1;
            var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
            vdata[vfield] = vsmin;
          }
        }

        var simulateWaitPartNoDate = function(vdata){
          var vxdif = 2;
          for (var i=0; i<vdata.length; i++){
            var today = new Date();
            var xdate1 = new Date();
            xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
            var xdate2 = new Date();
            xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
            var vtime = vdata[i].jam;
            if((vdata[i].daydif+vxdif)==0){
              var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
              vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
            }
            vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
            vdata[i].tgl_diperlukan = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
            var vdays = Math.round((today-xdate2)/(24*60*60*1000));
            var xdays;
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
            vdata[i].daywait = xdays;
            vdata[i].dday = vdays;

          }
          console.log('**** vdata ****', vdata);
          console.log('**** vdata data****', vdata[0]);
          
        }
        
        // berdasarkan nilai terkecil, satuan jam
        // harus terurut berdasarkan day terkecil, kemudian jam terkecil
        var orderDayColorRange = [
          {day: -1, color: '#fff'}, // day <-2
          {day: 0, color: '#0e0'},  // day <0 (yesterday)
          {day: 1, color: 'hour_color', default: '#ec0'}, // day <1 (today) --> check hour color, if hour color not defined, use default
          {day: 9999, color: '#900'}, // day<9999 (other day), use this color
        ]
        var orderHourColorRange = [
          {hour: -1, color: '#ec0'}, // hour <-1 use this color
          {hour: 24, color: '#e00'}, // hour<9999 (other day), use this color
        ]
        var orderDayTextColorRange = [
          {day: -1, color: '#000'}, // day <-2
          {day: 9999, color: '#fff'}, // day<9999 (other day), use this color
        ]
        var orderDayOnlyColorRange = [
          {day: 0, color: '#0e0'},  // day <0 (yesterday)
          {day: 9999, color: '#e00'}, // day<9999 (other day), use this color
        ]
        var orderDayOnlyTextColorRange = [
          {day: -1, color: '#fff'}, // day <-2
          {day: 9999, color: '#fff'}, // day<9999 (other day), use this color
        ]
            // if (vmin>30){
            //     return '#e00';
            // } else if (vmin>15){
            //     return '#ec0';
            // } else {
            //     return '#0e0';
            // }
        
        var simulatePrePickingDate = function(vdata){
          var vxdif = 2;
          for (var i=0; i<vdata.length; i++){
            var today = new Date();
            var xdate1 = new Date();
            xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
            var xdate2 = new Date();
            xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif);
            var vtime = vdata[i].jam;
            if((vdata[i].daydif+vxdif)==0){
              var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
              vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
            }
            vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
            vdata[i].time_booking = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
            var vdays = Math.round((today-xdate2)/(24*60*60*1000));
            var xdays;
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
            vdata[i].daywait = xdays;
            vdata[i].dday = vdays;
            vdata[i].dayRange = orderDayOnlyColorRange;
            vdata[i].textDayRange = orderDayOnlyTextColorRange;
            vdata[i].textHourRange = [];
          }
          console.log('**** vdata ****', vdata);
        }
                
        var simulateWaitIssuingDate = function(vdata){
          var vxdif = 2;
          for (var i=0; i<vdata.length; i++){
            var today = new Date();
            var xdate1 = new Date();
            xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
            var xdate2 = new Date();
            xdate2.setDate(xdate2.getDate()+vdata[i].daydif+vxdif-4);
            var vtime = vdata[i].jam;
            if((vdata[i].daydif+vxdif)==0){
              var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
              vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
            }
            vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
            vdata[i].time_request = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vtime;
            var vdays = Math.round((today-xdate2)/(24*60*60*1000));
            var xdays;
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
            vdata[i].daywait = xdays;
            vdata[i].dday = vdays;
            vdata[i].dayRange = orderDayOnlyColorRange;
            vdata[i].textDayRange = orderDayOnlyTextColorRange;
            vdata[i].textHourRange = [];
          }
          console.log('**** vdata ****', vdata);
        }
                
        var onTimer = function(){
            var vhmin= $filter('date')($scope.clock, 'hh:mm');
            // if ($scope.hourminute!==vhmin){
            //   $scope.hourminute = vhmin;
            //   if (!firstTime){
            //     for(var i=0; i<arrBoard.length; i++){
            //       vitem = arrBoard[i];
            //       if (vitem.incMode=='inc'){
            //         incrementTime(vitem.board.chipItems, vitem.incField);
            //       } else if (vitem.incMode=='dec'){
            //         decrementTime(vitem.board.chipItems, vitem.incField);
            //       }
            //       vitem.board.redrawChips();
            //     }
            //   } else {
            //     firstTime = false;
            //   }
            // }
        }

        var tick = function () {
            $scope.clock = Date.now() // get the current time
            onTimer();
            $timeout(tick, $scope.tickInterval); // reset the timer
        }

        // Start the timer
        $timeout(tick, $scope.tickInterval);

        // start timeout for create board
        // menggunakan timeout agar dijalankan setelah screen benar-benar ter-load.
        // PartsCurrentUser.getWarehouseId($scope.ServiceTypeId, $scope.MaterialTypeId, $scope.user.OrgId).then(function(res) {
          
          
        PartsCurrentUser.getWarehouseIdi().then(function(res) {        
            for (var i=0; i < res.data.length; i++) {  
              $scope.WarehouseId = res.data[i].warehouseid;
            }             
          console.log($scope.WarehouseId);
          var gridData = res.data;
          console.log(gridData[0]);
          //$scope.WarehouseId = 0;
          $scope.OutletId = 0;
          $scope.WarehouseId = $scope.WarehouseId;
          console.log("Iniservise2a",$scope.WarehouseId)
          $scope.OutletId = gridData[0].OutletId;
          $scope.countdown();
            $timeout(function() {
              //-----------------------------------//
              // **** waiting for Pre Picking **** //
              //-----------------------------------//
              var boardConfig = {
                type: 'kanban',
                chip: {
                  height: 65,
                  width: 360,
                  gap: 2
                },
                stage: {
                  maxHeight: allBoardHeight,
                  minHeight: allBoardHeight,
                },
                board:{
                  container: 'parts-waiting_pre_picking',
                  iconFolder: 'images/boards/parts',
                  chipGroup: 'parts_supply',
                  optimizeVisibility: 1,
                },
                layout: {
                  chipHeight: 65,
                  chipGap: 2
                }

              }

              // var xdate1 = vdata[i].date_kirim;
              // var xdate2 = vdata[i].datetime_booking;

              // var tmpBoardData1 = [
              //   { 
              //     nopol: 'B 6661 ABN', type: 'AVANZA',
              //     datetime_booking: new Date(),
              //     sa_name: 'MAMAN', 
              //     statusname: 'partial', no_gr: 'A1312345',
              //     no_rangka: 'JSHZ89172SDEADAX03', docno: 'AP/2931045888',

              //   },
              //   {chip: 'waiting_pre_picking', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 ABN', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
              //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W', vendor: 'SPLD',
              //     statusicon: ['open', 'late'], 
              //     statusname: 'claim', no_gr: 'B191029123',
              //     no_rangka: 'BCDW89172SDEADAX04', docno: 'AP/312998889', doctype: 'AP'
              //   },
              //   {chip: 'waiting_pre_picking', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 7788 ABN', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B', vendor: 'SPLD',
              //     statusname: 'noprepic', no_gr: 'A998173123',
              //     no_rangka: 'KJSK89172SDEADAX05', docno: 'AP/298373890', need_dp: 1, doctype: 'AP'
              //   },
              //   {chip: 'waiting_pre_picking', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9918 ABN', jam: '09:00', type: 'AVANZA', timewait: '03:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S', vendor: 'SPLD',
              //     statusicon: ['open'], no_gr: 'V991031234',
              //     statusname: 'partial',
              //     no_rangka: 'WDFW89172SDEADAX06', docno: 'AP/721045991', doctype: 'AP'
              //   },
              //   {chip: 'waiting_pre_picking', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 ABN', jam: '09:20', type: 'AVANZA', timewait: '02:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00944123222',
              //     statusname: 'partial',
              //     no_rangka: 'JHDY89172SDEADAX07', docno: 'AP/999005992', need_dp: 1, doctype: 'AP'
              //   },
              //   {chip: 'waiting_pre_picking', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 ABN', jam: '09:20', type: 'AVANZA', timewait: '04:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00888123222',
              //     statusname: 'noprepick',
              //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'AP/999999992', need_dp: 1, doctype: 'AP'
              //   },
              // ]
              // updatePrePickingDate(tmpBoardData1);

              var updatePrePickingDate = function(vdata){
              
                var vxdif = 2;
                for (var i=0; i<vdata.length; i++){
                  vdata[i].chip = 'waiting_pre_picking';
                  var today = new Date();
      
                  // var xdate1 = vdata[i].date_kirim;
                  var xdate2 = vdata[i].datetime_booking;
                  // vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
                  vdata[i].time_booking = $filter('date')(xdate2, 'dd/MM/yyyy HH:mm');
                  var vdays = Math.round((today-xdate2)/(24*60*60*1000));
                  var xdays;
                  var xdays = 'TODAY';
                  if (vdays>0){
                    xdays = 'H+'+vdays;
                  } else if (vdays<0){
                    xdays = 'H'+vdays;
                  }
                  vdata[i].daywait = xdays;
                  vdata[i].dday = vdays;
                  vdata[i].dayRange = orderDayOnlyColorRange;
                  vdata[i].textDayRange = orderDayOnlyTextColorRange;
                  vdata[i].textHourRange = [];
                }
                console.log('**** vdata ****', vdata);
                CountGetData(vdata);
                console.log('**** vdata count ****', vdata);
              
              }
                      
              var processPrePicking = function(vresp){           
                var vdata = []
                if (vresp.data && vresp.data.Result){
                  var xdata = vresp.data.Result;
                  for(var i=0; i<xdata.length; i++){
                    var xitem = xdata[i];
                    vdata.push({
                      nopol: xitem.LicensePlate,
                      type: xitem.ModelType,
                      datetime_booking: xitem.DateBooking,
                      sa_name: xitem.RequestBy, 
                      statusname: xitem.StatusName, 
                      no_rangka: xitem.VIN, 
                      docno: xitem.DocNo,
                      TipeRequest: xitem.TipeRequest,
                    })
                  }
                  // var vdata = tmpBoardData1;
                  updatePrePickingDate(vdata);
                  // CountGetData(vdata);
                  // $scope.LoopData(0,vdata)
                  console.log("get data count", vdata);
                  console.log("data loopp", $scope.DataFromLimit);          
                  
                }
                // return $scope.DataFromLimit;
                // return $scope.DataFromLimit
                return vdata;
              }
              $scope.GetWarehouseId();
              console.log("diatas registerboard", processPrePicking,limitCount);
              console.log("ini warehosenya",$scope.WarehouseId);
              registerBoard({
                boardConfig: boardConfig,
                boardData: [],
                // tempIdx: 1,
                processData: processPrePicking,
                dataUrl: '/api/as/Boards/partsupplyboard/1/' + limitCount+ '/0/'+$scope.WarehouseId,
                incField: 'timewait',
                incMode: 'inc',
                boardName: 'waiting_pre_picking'
              })

              //---------------------------------//
              // **** waiting for Issuing **** //
              //---------------------------------//

              var boardConfig = {
                type: 'kanban',
                chip: {
                  height: 65,
                  width: 360,
                  gap: 2
                },
                stage: {
                  maxHeight: allBoardHeight,
                  minHeight: allBoardHeight,
                },
                board:{
                  container: 'parts-waiting_issuing',
                  iconFolder: 'images/boards/parts',
                  chipGroup: 'parts_supply',
                  optimizeVisibility: 1,
                },
                layout: {
                  chipHeight: 65,
                  chipGap: 2
                }

              }

              // var tmpBoardData2 = [
              //   {
              //     nopol: 'B 6661 CBA', type: 'AVANZA',
              //     datetime_request: new Date(),
              //     sa_name: 'MAMAN', 
              //     prepicking: 1, no_gr: 'A1312345',
              //     no_rangka: 'JSHZ89172SDEADAX03', docno: 'WO/2931045888', 
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 CBA', jam: '08:30', type: 'AVANZA', timewait: '-00:05',
              //     tgl_kirim: '10/Jan/2017', time_diperlukan: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, cust_type: 'W', vendor: 'SPLD',
              //     statusicon: ['open', 'late'], 
              //     statusname: 'claim', no_gr: 'B191029123',
              //     no_rangka: 'BCDW89172SDEADAX04', docno: 'AP/312998889', doctype: 'WO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 7788 CBA', jam: '10:30', type: 'AVANZA', timewait: '-03:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -2, cust_type: 'B', vendor: 'SPLD',
              //     statusname: 'noprepic', no_gr: 'A998173123',
              //     no_rangka: 'KJSK89172SDEADAX05', docno: 'AP/298373890', prepicking: 1
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9918 CBA', jam: '09:00', type: 'AVANZA', timewait: '03:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: -1, cust_type: 'S', vendor: 'SPLD',
              //     statusicon: ['open'], no_gr: 'V991031234',
              //     statusname: 'partial',
              //     no_rangka: 'WDFW89172SDEADAX06', docno: 'SO/721045991', doctype: 'SO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9919 CBA', jam: '09:20', type: 'AVANZA', timewait: '02:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00944123222',
              //     statusname: 'partial',
              //     no_rangka: 'JHDY89172SDEADAX07', docno: 'WO/999005992', prepicking: 1, doctype: 'WO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 CBA', jam: '09:20', type: 'AVANZA', timewait: '04:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00888123222',
              //     statusname: 'noprepick',
              //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', prepicking: 1, doctype: 'WO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 CBA', jam: '09:20', type: 'AVANZA', timewait: '04:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00888123222',
              //     statusname: 'noprepick',
              //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', prepicking: 1, doctype: 'WO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 CBA', jam: '09:20', type: 'AVANZA', timewait: '04:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00888123222',
              //     statusname: 'noprepick',
              //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', prepicking: 1, doctype: 'WO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 CBA', jam: '09:20', type: 'AVANZA', timewait: '04:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00888123222',
              //     statusname: 'noprepick',
              //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', prepicking: 1, doctype: 'WO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 CBA', jam: '09:20', type: 'AVANZA', timewait: '04:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00888123222',
              //     statusname: 'noprepick',
              //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', prepicking: 1, doctype: 'WO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 CBA', jam: '09:20', type: 'AVANZA', timewait: '04:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00888123222',
              //     statusname: 'noprepick',
              //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', prepicking: 1, doctype: 'WO'
              //   },
              //   {chip: 'waiting_issuing', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8819 CBA', jam: '09:20', type: 'AVANZA', timewait: '04:00',
              //     tgl_kirim: '12/Jan/2017', time_diperlukan: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 1, cust_type: 'B', vendor: 'PT. ABC',
              //     statusicon: ['draft'], no_gr: 'K00888123222',
              //     statusname: 'noprepick',
              //     no_rangka: 'JHDY891UH7876EADAX07', docno: 'WO/999999992', prepicking: 1, doctype: 'WO'
              //   },
              // ]
              // simulateWaitIssuingDate(boardData);

              var updateWaitIssuingDate = function(vdata){
                var vxdif = 2;
                for (var i=0; i<vdata.length; i++){
                  vdata[i].chip = 'waiting_issuing';
                  var today = new Date();
                  // var xdate1 = vdata[i].date_kirim;
                  var xdate2 = vdata[i].datetime_request;
      
                  // vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
                  vdata[i].time_request = $filter('date')(xdate2, 'dd/MM/yyyy HH:mm');
                  var vdays = Math.round((today-xdate2)/(24*60*60*1000));
                  var xdays;
                  var xdays = 'TODAY';
                  if (vdays>0){
                    xdays = 'H+'+vdays;
                  } else if (vdays<0){
                    xdays = 'H'+vdays;
                  }
                  vdata[i].daywait = xdays;
                  vdata[i].dday = vdays;
                  vdata[i].dayRange = orderDayOnlyColorRange;
                  vdata[i].textDayRange = orderDayOnlyTextColorRange;
                  vdata[i].textHourRange = [];
                }
                console.log('**** vdata ****', vdata);
              }
                      
      
              var processWaitIssuing = function(vresp){
                $scope.GetWarehouseId();
                console.log("ini warehosenya",$scope.WarehouseId);
                var vdata = []
                if (vresp.data && vresp.data.Result){
                  var xdata = vresp.data.Result;
                  for(var i=0; i<xdata.length; i++){
                    var xitem = xdata[i];
                    var vprepick = 0;
                    if (xitem.StatusName=='prepicking' || xitem.StatusName=='partial') {
                      vprepick = 1;
                    }
                    vdata.push({
                      nopol: xitem.LicensePlate,
                      type: xitem.ModelType,
                      datetime_request: xitem.DateRequest,
                      sa_name: xitem.RequestBy, 
                      statusname: xitem.StatusName, 
                      no_rangka: xitem.VIN,
                      prepicking: vprepick,
                      docno: xitem.DocNo,
                      TipeRequest: xitem.TipeRequest,
                    })
                  }
                  // var vdata = tmpBoardData2;
                  updateWaitIssuingDate(vdata);
                }
                return vdata;
              }
              console.log("WOIIIIIIIIII",limitCount);
              if(limitCount == 0)
              {
                  $scope.GetAutoRefreshBoard();

              }
              limitCount = limitCount + 1;
              $scope.GetWarehouseId();
              console.log("ini warehosenya",$scope.WarehouseId);
              registerBoard({
                boardConfig: boardConfig,
                boardData: [],
                processData: processWaitIssuing,
                dataUrl: '/api/as/Boards/partsupplyboard/2/0/0/'+$scope.WarehouseId,
                incField: 'timewait',
                incMode: 'inc',
                boardName: 'waiting_issuing'
              })

              // ------- //
              $scope.porderboard.resetFilter();
              loadAllData();
            },100); // end of timeout for create board

          },
          function(err) {
              console.log("err=>", err);
          }
      );


    // });

    var adsearch = 1;
    $scope.showOptions = function() {
      if (adsearch == 0){
        $('#AdvSearch').attr('hidden', 'hidden');
        adsearch = 1;
      } else {
        $('#AdvSearch').removeAttr('hidden', 'hidden');
        adsearch = 0;
      }
    }



})

;
