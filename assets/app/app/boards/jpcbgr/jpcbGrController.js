angular.module('app')
    .controller('JpcbGrController', function($rootScope, $scope, $http, RepairProcessGR, CurrentUser,AppointmentGrService, jpcbFactory, Parameter, $timeout, $window, $filter, $state, JpcbGrViewFactory, ngDialog, bsAlert, bsNotify, Idle) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        var isBoardLoaded = false;
        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd-MM-yyyy',

            // disableWeekend: 1
        };
        var detailJobData = {}
        $scope.displayedChip = {}
        $scope.datachip1 = [];
        var listnopol = [];
        $scope.IdSA = 11577;
        var vlong1 = 'col-xs-8';
        var vlong2 = 'col-xs-12';
        $scope.isLoading = true;
        $scope.isBoardLoading = false;
        $scope.wcolumn = vlong2;
        $scope.ncolumn = 'col-xs-4';
        $scope.ncolvisible = 0;
        $scope.show_modal = {
            show:false
        }
        var boardName = 'jpcbgr_edit';
        // $scope.jpcbgrview = {currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza"}

        // $scope.jpcbgrview = {currentDate: JsBoard.Common.createLocalTime()}
        $scope.jpcbgrview = { currentDate: new Date() }
        $scope.jpcbgrview.xdate = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd HH:mm');

        //$scope.jpcbgrview.currentDate.setHours($scope.asb.currentDate.getHours()+1);
        // console.log("$scope.jpcbgrview.currentDate", $scope.jpcbgrview.currentDate);
        // console.log("$scope.dateOptions", $scope.dateOptions);
        $scope.jpcbgrview.startDate = new Date();
        // $scope.jpcbgrview.endDate = new Date();
        // $scope.jpcbgrview.endDate.setDate($scope.jpcbgrview.endDate.getDate()+7);
        // var allBoardHeight = 500;

        // $scope.$watch('jpcbgrview.showMode', function(newValue, oldValue) {
        //     if (newValue !== oldValue) {
        //         console.log('Changed! '+newValue);
        //         $scope.jpcbgrview.reloadBoard();
        //     }
        // });

        $scope.refreshData = function(){
            showBoardJPCB();
        }

        $scope.selectTechnician = function(item, data, index) {
            console.log("item ====>", item, data, $scope.ListTechnician);
            console.log('index tec per job', index)

            for (var i in $scope.displayedChip.JobTask) {
                if ($scope.displayedChip.JobTask[i].JobTaskId == data) {
                    _.map($scope.ListTechnician, function(val) {
                        if (val.id == item) {
                            for (var j in $scope.displayedChip.JobTask[i].JobTaskTechnician) {
                                $scope.displayedChip.JobTask[i].JobTaskTechnician[j].JTAdjustment = val.JTAdjustment;
                            }
                            console.log('hitammmm', val.JTAdjustment)
                        }
                    })
                }
            }

        }
        var tmpvNow = $filter('date')(new Date(), 'yyyy-MM-dd');
        var tmpurlTechnician = '/api/as/Boards/jpcb/employee/' + tmpvNow + '/TGR';
        $http.get(tmpurlTechnician).then(function(resp) {
            var vdata = resp.data.Result;
            console.log("vdata====>", vdata);
            var countTek = 0;
            for (var i = 0; i < vdata.length; i++) {
                var vitem = vdata[i];
                // if(vdata[i].Attend == 1 && vdata[i].Available == 1){
                    countTek+=1;
                // }
                //var vname = getPeopleName(vitem);
                // $scope.ListTechnician.push({ id: vitem.EmployeeId, name: vitem.EmployeeName });
            }
            $scope.countTechnician = countTek;
        });
        $scope.selectedForeman = function(item, bindingOn) {
            $scope.ListTechnician = [];
            var newListTechnician = [];

            console.log('data displayed temp', tempVDATA)
            console.log('jobid displayed temp', tempVJOBID)
            console.log('Binding data On?', bindingOn);
            console.log('selectedForeman', item);
            var urlGetGroup = '/api/as/GetGroupId/' + item;
            var vNow = $filter('date')(new Date(), 'yyyy-MM-dd');
            var urlTechnician = '/api/as/Boards/jpcb/employee/' + vNow + '/TGR';

            $http.get(urlGetGroup).then(function(res) {
                var ForemanGroupId = res.data.GroupId;
                console.log('selectedForeman ress>>', res.data);
                $http.get(urlTechnician).then(function(resp) {
                    var vdata = resp.data.Result;
                    $scope.countTechnician = vdata.length;
                    console.log('selectedForeman vdata>>', vdata);
                    console.log('selectedForeman $scope.countTechnician>>', $scope.countTechnician);
                    if ($scope.countTechnician > 0) {
                        angular.forEach(vdata, function(val) {
                            if (val.GroupId == ForemanGroupId) {
                                newListTechnician.push({ id: val.EmployeeId, name: val.EmployeeName, groupId: val.GroupId, JTAdjustment: val.JTAdjustment });
                            };
                        });

                        // if(tempVDATA.intStatus == 5 || tempVDATA.intStatus == 7 || tempVDATA.intStatus == 9 || tempVDATA.intStatus == 11){
                        //     $scope.ListTechnician = newListTechnician;
                        // } else {
                        angular.forEach(newListTechnician, function(data) {
                            var urlCheckTechnician = '/api/as/CheckTechClockOn/' + data.id;
                            // if(tempVDATA.intStatus == 5 || tempVDATA.intStatus == 7 || tempVDATA.intStatus == 9 || tempVDATA.intStatus == 11){
                            if (tempVDATA.intStatus > 4 && tempVDATA.intStatus != 22) {

                                console.log('$scope.displayedChip.JobTask', $scope.displayedChip.JobTask);
                                console.log('newListTechnician', newListTechnician);

                                angular.forEach($scope.displayedChip.JobTask, function(dataJob) {

                                    angular.forEach(dataJob.JobTaskTechnician, function(dataTechnician) {

                                        if (dataTechnician.MProfileTechnicianId == data.id) {
                                            if ($scope.ListTechnician.length == 0){
                                                $scope.ListTechnician.push(data);
                                            } else {
                                                var alrExist = 0
                                                for (var i=0; i<$scope.ListTechnician.length; i++){
                                                    if ($scope.ListTechnician[i].id == data.id){
                                                        alrExist++;
                                                    }
                                                }
                                                if (alrExist == 0){
                                                    $scope.ListTechnician.push(data);
                                                }
                                            }
                                        } else {
                                            $http.get(urlCheckTechnician).then(function(valx) {
                                                if (valx.data == 0) {
                                                    if ($scope.ListTechnician.length == 0){
                                                        $scope.ListTechnician.push(data);
                                                    } else {
                                                        var alrExist = 0
                                                        for (var i=0; i<$scope.ListTechnician.length; i++){
                                                            if ($scope.ListTechnician[i].id == data.id){
                                                                alrExist++;
                                                            }
                                                        }
                                                        if (alrExist == 0){
                                                            $scope.ListTechnician.push(data);
                                                        }
                                                    }
                                                };
                                            });
                                        }
                                    });
                                });

                                // $scope.ListTechnician.push(data);
                            } else {
                                $http.get(urlCheckTechnician).then(function(valx) {
                                    if (valx.data == 0) {
                                        $scope.ListTechnician.push(data);
                                    };
                                });
                            }
                        });
                        // }


                        // for (var i = 0; i < vdata.length; i++) {
                        //     var vitem = vdata[i];
                        //     if (vitem.GroupId == ForemanGroupId) {
                        //         var urlCheckTechnician = '/api/as/CheckTechClockOn/' + vitem.EmployeeId;
                        //         $http.get(urlCheckTechnician).then(function(val) {
                        //             if (val.data == 0) {
                        //                 $scope.ListTechnician.push({ id: vitem.EmployeeId, name: vitem.EmployeeName, groupId: vitem.GroupId });
                        //             };
                        //         });
                        //         // RepairProcessGR.checkAvailabilityForTechnician(vitem.EmployeeId).then(function(resu) {
                        //         //     if (resu.data == 0) {
                        //         //         $scope.ListTechnician.push({ id: vitem.EmployeeId, name: vitem.EmployeeName, groupId: vitem.GroupId });
                        //         //     }
                        //         // });
                        //         // $scope.ListTechnician.push({ id: vitem.EmployeeId, name: vitem.EmployeeName, groupId: vitem.GroupId });
                        //     };
                        // };
                        console.log('$scope.ListTechnician Ada', $scope.ListTechnician);
                    } else {
                        $scope.ListTechnician = [];
                        console.log('$scope.ListTechnician Tidak Ada', $scope.ListTechnician);
                    };
                });
            });
        };


        $scope.jpcbgrview.scNomorPolisi = '';
        $scope.jpcbgrview.searchInputEdit = function() {
            this.searchFound = '';
            $scope.jpcbgrview.searchFound = '';
            var xboard = JpcbGrViewFactory.getBoard(boardName);
            xboard.hideBlinkChips(function(vchip) {
                return true;
            });
        }
        $scope.jpcbgrview.checkFindNopol = function(vchip) {
            console.log('nopol apa lah', vchip);
            console.log('$scope.jpcbgrview.scNomorPolisi apa lah', $scope.jpcbgrview.scNomorPolisi);
            var vNopol = $scope.jpcbgrview.scNomorPolisi.toUpperCase();
            if (vchip.data.nopol == vNopol) {
                // if (vchip.nopol == vNopol) {
                //$scope.jpcbgrview.searchFound = 'Found';
                return true;
            }
            return false;
        }
        $scope.jpcbgrview.searchNomorPolisi = function() {
            $scope.showChipInfo(false)
            var xboard = JpcbGrViewFactory.getBoard(boardName);
            console.log('xboard doang', xboard);
            vcount = this.searchByFunctionChip(xboard.chips.children, this.checkFindNopol);
            console.log('vcount doang', vcount);
            if (vcount > 0) {
                if (vcount > 1) {
                    this.searchFound = 'Found ' + vcount + ' chips';
                    $scope.jpcbgrview.searchFound = 'Found ' + vcount + ' chips';
                } else {
                    this.searchFound = 'Found';
                    $scope.jpcbgrview.searchFound = 'Found ';

                }
                // console.log('ini ketemu', this.checkFindNopol);
                xboard.blinkChips(this.checkFindNopol, { strokeWidth: 4 },
                    300, 3
                );
            } else {
                // this.searchFound = 'Not Found';
                if ($scope.jpcbgrview.scNomorPolisi !== '' && $scope.jpcbgrview.scNomorPolisi !== null && $scope.jpcbgrview.scNomorPolisi !== undefined){
                    $scope.backdrop = true
                }
                JpcbGrViewFactory.JPCBFindDate($scope.jpcbgrview.scNomorPolisi).then(function(resz){
                    if (resz.data !== null){
                        $scope.jpcbgrview.currentDate = new Date(resz.data)
                        $scope.jpcbgrview.reloadBoard();
                        $timeout(function() {
                            $scope.backdrop = false
                        }, 2000);
                    } else {
                        this.searchFound = 'Not Found';
                        $scope.jpcbgrview.searchFound = 'Not Found';
                        $scope.backdrop = false
                    }

                },function(err) {
                    console.log("err=>", err);
                    $scope.backdrop = false
                })

            }
        }

        $scope.jpcbgrview.searchByFunctionChip = function(vchipList, vcheck) {
            var vcount = 0;
            console.log('vchiplist dodol', vchipList)
            console.log('vcheck dodol', vcheck)

            for (var i = 0; i < vchipList.length; i++) {
                var vchip = vchipList[i];
                var vflag = vcheck(vchip);
                if (vflag) {
                    vcount = vcount + 1;
                }
            }

            // for (var i=0; i< vchipList[0].container.chipItems.length; i++){
            //     var vchip = vchipList[0].container.chipItems[i];
            //     var vflag = vcheck(vchip);
            //     if (vflag) {
            //         vcount = vcount + 1;
            //     }
            // }

            return vcount;
        }


        // cobain cari di stoppage start here ===============================

        $scope.SearchStoppage = function() {
            var keyword = $scope.StoppageNomorPolisi.toUpperCase();
            JpcbGrViewFactory.searchStoppage(keyword);
            console.log('ini keyword plat nomer nya', keyword)
        }

        // cobain cari di stoppage end here ===============================





        // click buttons
        $scope.additionalJob = function() {
            var xboard = JpcbGrViewFactory.getBoard(boardName);
            xboard.blinkChips(function(vchip) {
                if (vchip.data.additional == 1) {
                    return true;
                }
                return false;
            }, { strokeWidth: 4 });
        }
        $scope.pausedJob = function() {
            var xboard = JpcbGrViewFactory.getBoard(boardName);
            xboard.blinkChips(function(vchip) {
                if (vchip.data.paused == 1) {
                    return true;
                }
                return false;
            }, { strokeWidth: 4 });
        }
        $scope.waitingFI = function() {
            var xboard = JpcbGrViewFactory.getBoard(boardName);
            xboard.blinkChips(function(vchip) {
                if (vchip.data.waiting_fi == 1) {
                    return true;
                }
                return false;
            }, { strokeWidth: 4 });

        }
        $scope.delayedJob = function() {
            var xboard = JpcbGrViewFactory.getBoard(boardName);
            xboard.blinkChips(function(vchip) {
                var vlayout = vchip.container.layout;
                vdata = vchip.data;
                var vadd = 0;
                if (typeof vdata.breakAdd !== 'undefined') {
                    vadd = vdata.breakAdd;;
                }
                var vendtime = vdata.col + vdata.normal + vdata.ext + vadd;
                if ((vchip.data.status !== 'done') && (vlayout.timeLine > vendtime)) {
                    return true;
                }
                return false;
            }, { strokeWidth: 4 });

        }


        // Lookup for Technician
        $scope.ListTechnician = [];
        $scope.ListForeman = [];

        var getPeopleName = function(vitem) {
            var vname = '';
            if (typeof vitem.People !== 'undefined') {
                if (typeof vitem.People.FirstName !== 'undefined') {
                    vname = vitem.People.FirstName;
                }
                if (typeof vitem.People.LastName !== 'undefined') {
                    if (vname !== '') {
                        vname = vname = vname + ' ';
                    }
                    vname = vname + vitem.People.LastName;
                }
            }
            return vname;
        }

        // var urlTechnician = '/api/as/EmployeeRoles/2';
        // $http.get(urlTechnician).then(function(resp){
        //     var vdata = resp.data;
        //     for (var i=0; i<vdata.length; i++){
        //         var vitem = vdata[i];
        //         var vname = getPeopleName(vitem);
        //         $scope.ListTechnician.push({id: vitem.EmployeeId, name: vname});
        //     }
        // });

        //di komen ama amik, karna pilih teknisi harus pilih foreman nya dulu
        // var vNow = $filter('date')(new Date(), 'yyyy-MM-dd');
        // var urlTechnician = '/api/as/Boards/jpcb/employee/' + vNow + '/TGR';
        // $http.get(urlTechnician).then(function(resp) {
        //     var vdata = resp.data.Result;
        //     $scope.countTechnician = vdata.length;
        //     for (var i = 0; i < vdata.length; i++) {
        //         var vitem = vdata[i];
        //         //var vname = getPeopleName(vitem);
        //         $scope.ListTechnician.push({ id: vitem.EmployeeId, name: vitem.EmployeeName, groupId: vitem.GroupId });
        //     }
        // });

        var vNow = $filter('date')(new Date(), 'yyyy-MM-dd');
        var urlForeman = '/api/as/Boards/jpcb/employee/' + vNow + '/FGR';
        $http.get(urlForeman).then(function(resp) {
            var vdata = resp.data.Result;
            for (var i = 0; i < vdata.length; i++) {
                var vitem = vdata[i];
                // var vname = getPeopleName(vitem);
                $scope.ListForeman.push({ id: vitem.EmployeeId, name: vitem.EmployeeName });
            }
        });
        //var urlTechnician = '/api/as/EmployeeRoles/1';
        //$http.get(urlTechnician);

        $scope.safeApply = function(fn) {
            try {
                var phase = this.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {
                    if (fn && (typeof(fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }

            } catch (excp) {
                try {
                    this.$apply();
                } catch (exx) {
                    //
                }
            }
        };

        $scope.updateTechRate = function(task) {
            var vrate = task.ActualRate;
            console.log('vrate', vrate);
            if (task.JobTaskTechnician.length > 0) {
                var xrate = vrate / task.JobTaskTechnician.length;
                xrate = Math.round(xrate * 1000) / 1000;
                for (var i = 0; i < task.JobTaskTechnician.length; i++) {
                    task.JobTaskTechnician[i].FlatRate = xrate;
                    console.log('FR kah?', task.JobTaskTechnician[i].FlatRate)
                }
            }
        }

        $scope.deleteItem = function(task, vitem) {
            var index = task.JobTaskTechnician.indexOf(vitem);
            task.JobTaskTechnician.splice(index, 1);
            this.updateTechRate(task);
        }

        $scope.newItemRow = function(task) {
            console.log('ini apa', task);
            console.log('apa ini lah', task.JobTaskTechnician);
            var vitem = { "FlatRate": 0, "MProfileTechnicianId": "" }
            task.JobTaskTechnician.push(vitem);
            this.updateTechRate(task);
        }

        var isJobLampau = function(job) {
            var vtgl = $filter('date')(job.PlanDateFinish, 'yyyy-MM-dd') + ' ' + job.PlanFinish;
            var vnow = $filter('date')(JpcbGrViewFactory.getCurrentTime(), 'yyyy-MM-dd HH:mm:ss');
            if (vtgl < vnow) {
                // return true; di komen karena pengen bisa di dispatch walau sudah lewat        
                return true;
            }
            return false;
        }

        var isJobLampau2 = function(job) {
            var vtgl = $filter('date')(job.PlanDateStart, 'yyyy-MM-dd') + ' ' + job.PlanStart;
            var vnow = $filter('date')(JpcbGrViewFactory.getCurrentTime(), 'yyyy-MM-dd HH:mm:ss');
            if (vnow >= vtgl) {
                return true;
            }
            return false;
        }
        $scope.showChipWO = function(job){
            console.log("jobbb===>",job);
            $scope.dataForBslist(job);
            $scope.show_modal = {
                show:true
            }
        }
        var Parts = [];
        $scope.dataForBslist = function(data){
            // gridTemp = [];
            $scope.gridWork = [];
            Parts.splice();
            Parts = [];
            $scope.showSubmitTeco = 0;
            $scope.showCompleteTeco = 0;
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                for (var i = 0; i < tmpJob.length; i++) {
                    if (tmpJob[i].JobType !== null && tmpJob[i].JobType !== undefined) {
                        if (tmpJob[i].JobType.Name == "TWC" || tmpJob[i].JobType.Name == "PWC") {
                            $scope.showSubmitTeco++;
                        } else {
                            $scope.showCompleteTeco++;
                        }
                    }
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        console.log("tmpJob[i].JobParts[j]", tmpJob[i].JobParts[j]);
                        if (tmpJob[i].JobParts[j].PaidBy !== null && tmpJob[i].JobParts[j].PaidBy !== undefined ) {
                            tmpJob[i].JobParts[j].PaidBy = tmpJob[i].JobParts[j].PaidBy.Name;
                        };
                        if (tmpJob[i].JobParts[j].Satuan !== null && tmpJob[i].JobParts[j].Satuan !== undefined) {
                            tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
                            delete tmpJob[i].JobParts[j].Satuan;
                        };
                        tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                    };
                };
                // added by sss on 2017-11-03
                $scope.getAvailablePartsServiceManual(tmpJob, 0, 0);
            };
        }
        $scope.getAvailablePartsServiceManual = function(item, x, y) {
            console.log("getAvailablePartsServiceManual", item);
            console.log("x : ", x);
            console.log("y : ", y);
            // $scope.mData.MProfileTechnicianId.EmployeeName = item.JobTaskTechnician.MProfileTechnicianId.EmployeeName;

            if (x > item.length - 1) {
                return item;
            }
            if (item[x].isDeleted !== 1) {
                console.log("item[x]", item[x]);

                if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                    var itemTemp = item[x].JobParts[y];
                    AppointmentGrService.getAvailableParts(itemTemp.PartsId, item[x].JobId).then(function(res) {
                        var tmpParts = res.data.Result[0];

                        if (tmpParts !== null && tmpParts !== undefined) {
                            // console.log("have data RetailPrice",item,tmpRes);
                            item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                            // item[x].JobParts[y].minimalDp = tmpParts.PriceDP;
                            item[x].JobParts[y].nightmareIjal = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;
                            item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                            if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                item[x].JobParts[y].DiscountTypeId = -1;
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                item[x].JobParts[y].DiscountTypeId = 0;
                            }
                            if (tmpParts.isAvailable == 0) {
                                item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                    item[x].JobParts[y].ETA = "";
                                } else {
                                    item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                }
                                item[x].JobParts[y].OrderType = 3;
                            } else {
                                item[x].JobParts[y].Availbility = "Tersedia";
                            }
                        } else {
                            console.log("haven't data RetailPrice");
                            item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                            item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
                            if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                item[x].JobParts[y].DiscountTypeId = -1;
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                item[x].JobParts[y].DiscountTypeId = 0;
                            }
                        }
                        if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].PaidBy != undefined && item[x].JobParts[y].PaidBy != null) {
                            item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy;
                        }
                        // if (item[x].JobParts[y].PaidBy !== null) {
                        //     item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                        //     // delete item[x].JobParts[y].PaidBy;
                        // };
                        if (item[x].JobParts[y].IsOPB !== null) {
                            item[x].JobParts[y].isOPB = item[x].JobParts[y].IsOPB;
                        }
                        if (item[x].JobParts[y].isDeleted == 0) {
                            Parts.push(item[x].JobParts[y]);
                        }
                        // return item;

                        if (x <= (item.length - 1)) {
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    var summ = item[x].FlatRate;
                                    var summaryy = sum * summ;
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                        // summaryy = parseInt(summaryy);
                                    summaryy = Math.round(summaryy);
                                    console.log("summaryy ==>", summaryy);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = { Name: "" }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].DiscountTypeId = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].DiscountTypeId = 0;
                                    // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                    item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);
                                }
                                $scope.gridWork.push({
                                    // gridTemp.push({
                                    ActualRate: item[x].ActualRate,
                                    AdditionalTaskId: item[x].AdditionalTaskId,
                                    isOpe: item[x].isOpe,
                                    Discount: item[x].Discount,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    DiscountedPrice: item[x].DiscountedPrice,
                                    DiscountTypeId: item[x].DiscountTypeId,
                                    NormalPrice: normalPrice,
                                    catName: item[x].JobType.Name,
                                    Fare: item[x].Fare,
                                    IsCustomDiscount: item[x].IsCustomDiscount,
                                    Summary: summaryy,
                                    PaidById: item[x].PaidById,
                                    JobId: item[x].JobId,
                                    TaskId: item[x].TaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    PaidBy: item[x].PaidBy.Name,
                                    index: "$$" + x,
                                    child: Parts
                                });
                                Parts.splice();
                                Parts = [];
                                console.log("if 1 nilai x", x);
                                $scope.getAvailablePartsServiceManual(item, x + 1, 0);

                            } else {
                                $scope.getAvailablePartsServiceManual(item, x, y + 1);
                            }
                        }
                    });
                } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                    item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                    item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                    item[x].JobParts[y].DownPayment = item[x].JobParts[y].DownPayment;
                    if (item[x].JobParts[y].PaidBy != undefined || item[x].JobParts[y].PaidBy != null) {
                        item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy;
                    }
                    if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                        item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                        delete item[x].JobParts[y].Part;
                    }
                    if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                        item[x].JobParts[y].DiscountTypeId = -1;
                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                    } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                        // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                        item[x].JobParts[y].DiscountTypeId = 0;
                    }
                    // if (item[x].JobParts[y].PaidBy !== null) {
                    //     item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                    //     // delete item[x].JobParts[y].PaidBy;
                    // };
                    tmp = item[x].JobParts[y];
                    if (item[x].JobParts[y].IsOPB !== null) {
                        item[x].JobParts[y].isOPB = item[x].JobParts[y].IsOPB;
                    }
                    if (item[x].JobParts[y].isDeleted == 0) {
                        Parts.push(item[x].JobParts[y]);
                    }
                    if (x <= (item.length - 1)) {
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                var summ = item[x].FlatRate;
                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy
                                    // summaryy = parseInt(summaryy);
                                summaryy = Math.round(summaryy);
                                console.log("summaryy ==>", summaryy);
                            }
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            }
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = { Name: "" }
                            }
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            }
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].DiscountTypeId = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].DiscountTypeId = 0;
                                // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);
                            }

                            // gridTemp.push({
                            $scope.gridWork.push({
                                ActualRate: item[x].ActualRate,
                                AdditionalTaskId: item[x].AdditionalTaskId,
                                isOpe: item[x].isOpe,
                                Discount: item[x].Discount,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                DiscountedPrice: item[x].DiscountedPrice,
                                DiscountTypeId: item[x].DiscountTypeId,
                                NormalPrice: normalPrice,
                                catName: item[x].JobType.Name,
                                Fare: item[x].Fare,
                                IsCustomDiscount: item[x].IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: item[x].PaidById,
                                JobId: item[x].JobId,
                                TaskId: item[x].TaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                PaidBy: item[x].PaidBy.Name,
                                index: "$$" + x,
                                child: Parts
                            });
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                            console.log("if 2 nilai x", x);
                        } else {
                            $scope.getAvailablePartsServiceManual(item, x, y + 1);
                        }
                    }
                } else if (item[x].JobParts.length == 0) {
                    if (x <= (item.length - 1)) {
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                var summ = item[x].FlatRate;
                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy
                                    // summaryy = parseInt(summaryy);
                                summaryy = Math.round(summaryy);
                                console.log("summaryy ==>", summaryy);
                            }
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            }
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = { Name: "" }
                            }
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            }
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].DiscountTypeId = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].DiscountTypeId = 0;
                                // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);

                            }
                            // gridTemp.push({
                            $scope.gridWork.push({
                                ActualRate: item[x].ActualRate,
                                AdditionalTaskId: item[x].AdditionalTaskId,
                                isOpe: item[x].isOpe,
                                Discount: item[x].Discount,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                DiscountedPrice: item[x].DiscountedPrice,
                                DiscountTypeId: item[x].DiscountTypeId,
                                NormalPrice: normalPrice,
                                catName: item[x].JobType.Name,
                                Fare: item[x].Fare,
                                IsCustomDiscount: item[x].IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: item[x].PaidById,
                                JobId: item[x].JobId,
                                TaskId: item[x].TaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                PaidBy: item[x].PaidBy.Name,
                                index: "$$" + x,
                                child: Parts
                            });
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                        } else {
                            $scope.getAvailablePartsServiceManual(item, x, y + 1);
                        };
                    };
                };

                if (x > item.length - 1) {
                    return item;
                };
            } else {
                $scope.getAvailablePartsServiceManual(item, x + 1, 0);
            }
        };
        $scope.validateJob = function(job) {
            console.log("jobb", job);
            try {
                if (!isJobLampau(job) && !isJobLampau2(job) && job.MProfileForemanId) {
                    // $scope.statusdispa = true;
                }
                if (isJobLampau(job)) {
                    // $scope.JbId = JobId;
                    $scope.modal_modelTask = [];
                    $scope.modalModeTask = 'new';
                    //$scope.doAddPekerjaan = function (Data) {

                    console.log("Ini data gueeeeee", job);
                    RepairProcessGR.addTask(job, $scope.IdSA).then(
                        function(res) {
                            console.log('Suksess simpan');

                            //  RepairProcessGR.updateFlagForAddTask($scope.JbId).then(
                            //      function (res) {
                            //          console.log('Suksess update');
                            //          $scope.show_modalTask = { show: false };
                            //          $scope.getDataDetail($scope.JbId);
                            //          ngDialog.closeAll();
                            //      },
                            //      function (err) {
                            //          console.log("err=>", err);
                            //      }
                            //  );

                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                    // throw ("Tidak bisa mendispatch job yang waktunya sudah berlalu.");
                }
                if (!job.MProfileForemanId) {
                    throw ("Foreman harus diisi.");
                }
                if (isJobLampau2(job)) {

                    // throw ("Sedang Dikerjakan data tidak dapat diubah");
                }

                if (job.JobTask.length === 0){
                    throw("Chip ini tidak memiliki pekerjaan, silahkan cek ke menu wo list.")
                }

                var cekdeletedjob = 0
                for (var x=0; x<job.JobTask.length; x++){
                    if (job.JobTask[x].isDeleted === 1){
                        cekdeletedjob++
                    }
                }

                if (cekdeletedjob === job.JobTask.length){
                    throw("Chip ini tidak memiliki pekerjaan, silahkan cek ke menu wo list.")
                }

                for (var i = 0; i < job.JobTask.length; i++) {
                    if (job.JobTask[i].isDeleted === 0){
                        this.validateTechnician(job.JobTask[i]);
                        // $scope.JbId = JobId;
                        $scope.modal_modelTask = [];
                        $scope.modalModeTask = 'new';
                        //$scope.doAddPekerjaan = function (Data) {
    
                        console.log("Ini data gueeeeee", job);
                        RepairProcessGR.addTask(job, $scope.IdSA).then(
                            function(res) {
                                console.log('Suksess simpan');
    
                                // RepairProcessGR.updateFlagForAddTask($scope.JbId).then(
                                //     function (res) {
                                //         console.log('Suksess update');
                                //         $scope.show_modalTask = { show: false };
                                //         $scope.getDataDetail($scope.JbId);
                                //         ngDialog.closeAll();
                                //     },
                                //     function (err) {
                                //         console.log("err=>", err);
                                //     }
                                // );
    
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        // }
                    }
                    
                }
            } catch (exception) {
                console.log(exception);
                console.log("typeof error", typeof exception);
                showMessageNotify(exception.toString());
                return false;
            }
            return true;
        }

        $scope.validateTechnician = function(task) {
            if (task.ActualRate === "" || task.ActualRate === null || task.ActualRate == 0) {
                throw ("Actual Rate tidak boleh kosong.");
            }
            var vcheckDup = {}
            for (var i = 0; i < task.JobTaskTechnician.length; i++) {
                // teknisi tidak boleh kosong
                var teknisi = task.JobTaskTechnician[i];
                if (!teknisi.MProfileTechnicianId) {
                    throw ("Teknisi harus diisi.");
                }

                // flat rate tidak boleh <=0
                if (teknisi.FlatRate <= 0) {
                    throw ("Flat Rate harus diisi > 0.");
                }

                // teknisi tidak boleh duplicate
                var vid = teknisi.MProfileTechnicianId;
                if (vcheckDup[vid]) {
                    throw ("Dalam satu pekerjaan, teknisi tidak boleh sama.");
                } else {
                    vcheckDup[vid] = 1;
                }
            }
            if (task.JobTaskTechnician.length === 0){
                throw ("Teknisi untuk masing masing pekerjaan harus diisi.");
            }
        }


        $scope.dispatchChip = function(job, isDispatch) {
            JpcbGrViewFactory.getStatWO(job.JobId, job.Status).then(function(result){
                if (result.data == 1){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                    });
                    return false
                } else {
                    JpcbGrViewFactory.CheckIsJobCancelWO(job.JobId).then(function(resultx){
                        if (resultx.data == 666){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                            });
                            return false
                        } else {
                            $scope.dispatchChip2(job, isDispatch)
                        }
                    })
                }
            })
        }

        $scope.dispatchChip2 = function(job, isDispatch) {
            var TesValidSAR = 0;
            for (var i=0; i<job.JobTask.length; i++){
                if (job.JobTask[i].ActualRate > 0){
                    var tesMod = job.JobTask[i].ActualRate % 0.25
                    if (tesMod != 0){
                        //tidak berhasil, kelipatan tidak 0.25
                        TesValidSAR++;
                    }
                } else {
                    TesValidSAR++;
                }
            }

            if (TesValidSAR == 0){
                //artinya keilapatan 0.25 semua
                //ini yang mau di edit
                if (this.validateJob(job)) {
                    console.log("gue mau nyoba", job);
                    console.log("Job", job);
                    var vext = 0;
                    var Overtime = 0;
                    if (job.AdditionalTimeAmount) {
                        vext = parseInt(job.AdditionalTimeAmount);
                    }
                    if (job.Overtime) {
                        Overtime = parseInt(job.Overtime);
                    }

                    if ($scope.jobsplitID == null || $scope.jobsplitID == undefined) {
                        $scope.jobsplitID = 0;
                    }

                    var vdata = {
                        JobId: job.JobId,
                        TimeExtension: vext,
                        ForemanId: job.MProfileForemanId,
                        IsWoBase: job.isWOBase,
                        status: 22,
                        Overtime: Overtime,
                        JobTas: [],
                        JobSplitId: $scope.jobsplitID
                    };
                    console.log("Mau tau isinya apa", vdata);
                    console.log("jobTask", job.JobTask);
                    for (var i = 0; i < job.JobTask.length; i++) {
                        var jtask = job.JobTask[i];
                        var vtask = {
                            JobTaskId: jtask.JobTaskId,
                            ActualRate: jtask.ActualRate,
                            JobTaskTechnicians: []
                        }
                        for (var j = 0; j < jtask.JobTaskTechnician.length; j++) {
                            vjtech = jtask.JobTaskTechnician[j];
                            vtech = {
                                MProfileTechnicianId: vjtech.MProfileTechnicianId,
                                FlatRate: vjtech.FlatRate
                            }
                            vtask.JobTaskTechnicians.push(vtech);
                        }
                        vdata.JobTas.push(vtask);
                    }
                    //console.log("vdata", vdata);
                    var urlDispatch = '/api/as/Jobs/PlotStallGR?isDispatch=1';
                    var urlUpdate = '/api/as/Jobs/PlotStallGR?isDispatch=0';
                    var url = '';
                    if (isDispatch == 1) {
                        url = urlDispatch;
                    } else {
                        url = urlUpdate;
                    }
                    // var url = '/api/as/Jobs/PlotStallGR';
                    $http.put(url, [vdata], { headers: { 'Content-Type': 'application/json' } }).then(function(resp) {
                        console.log(resp);
                        if (resp.data == 0){
                            // ga bisa dispatch, sar nya nabrak chip lain numpuk lah pokok nya
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Tidak dapat dispatch / update karena akan menumpuk chip yang lain",
                            });

                        } else {
                            var vmsg = "Data sent.";
                            if ((typeof resp.data !== 'undefined') && (typeof resp.data.ResponseMessage !== 'undefined')) {
                                var vmsg = resp.data.ResponseMessage;
                            }

                            //$timeout(showMessage(vmsg), 100);
                            $timeout(function() {
                                showMessage(vmsg);
                                showBoardJPCB();
                                displayChipData(tempVJOBID, tempVDATA);
                            }, 100);

                        }
                        
                    });

                }
            } else {
                //artinya ada yg ga kelipatan 0.25
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input SAR dengan Kelipatan 0.25",
                });
            }
            
        }

        var checkEditableItem = function(vitem) {
            //var enableList = ["0", "3", "4", "6", "7", "8", "9", "10"]
            var enableList = ["4", "5", "6", "7", "22", "25"] // ditambah status 22 karena foreman mau bisa di edit saat sudah dispatch namun blm clock on
            var xstatus = vitem.Status + '';
            if (enableList.indexOf(xstatus) >= 0) {
                return true;
            }
            return false;
        }

        var checkEditableItemDispatch = function(vitem) {
            //var enableList = ["0", "3", "4", "6", "7", "8", "9", "10"]
            // var enableList = ["4", "5", "6", "7"] di comment 30 oktber 2018, karena mau coba biar button dispatch cm bs di awal sekali doang (status 4)
            var enableList = ["4"]
            var xstatus = vitem.Status + '';
            if (enableList.indexOf(xstatus) >= 0) {
                return true;
            }
            return false;
        }

        var tempVDATA = {};
        var tempVJOBID = null;
        $scope.jobsplitID = null;
        var displayChipData = function(vJobId, vdata) {
            console.log("coba dulu ini kodingan gua", vJobId, vdata);
            $scope.jobsplitID = vdata.JobSplitId;
            tempVDATA = angular.copy(vdata);
            tempVJOBID = angular.copy(vJobId);
            $scope.chipLama = 0; 
            if (vdata.isSplitActive === 0 && (vdata.TimeFinish !== null && vdata.TimeFinish !== undefined) && vdata.JobSplitId != 0){
                $scope.chipLama = 1;
            }


            var tmpTinggi = $(window).height() / 1.5;
            var tmpHeader = $('#sideInfo .panel .panel-heading').height();
            var tmpFooter = $('#sideInfo .panel .panel-footer').height();
            var tmpPanelbody = tmpTinggi - (tmpHeader + tmpFooter);
            console.log("tmpTinggi ====>", tmpTinggi);
            console.log("tmpHeader ====>", tmpHeader);
            console.log("tmpPanelbody ====>", tmpPanelbody);
            console.log("tmpFooter ====>", tmpFooter);
            $('#sideInfo .panel .panel-body').css('height', (tmpPanelbody - tmpFooter) + 'px');

            var xboard = JpcbGrViewFactory.getBoard(boardName);
            // if (typeof detailJobData[vJobId] !== 'undefined'){
            //     $scope.displayedChip = detailJobData[vJobId];
            //     $timeout(function(){
            //         $scope.$apply();
            //         // $scope.safeApply();
            //     }, 200);
            // } else {
            $scope.displayedChip = { JobId: vJobId }
            var vconfig = { customData: { isSplitable: vdata.isSplitable } }
            var vurl = '/api/as/Jobs/' + vJobId;
            $http.get(vurl, vconfig).then(function(resp) {
                if (typeof resp.data.Result[0] !== 'undefined') {
                    var vresult = resp.data.Result[0];
                    var arrJobTaskVresult = [];
                    _.map(vresult.JobTask, function(val) {
                        if (val.isDeleted == 0) {
                            arrJobTaskVresult.push(val);
                        }
                    });

                    // ==== ambil teknisi yg isActive na 1 aja ==================== start
                    for (var i=0; i<vresult.JobTask.length; i++){
                        var teknisiaktif = [];
                        for (var j=0; j<vresult.JobTask[i].JobTaskTechnician.length; j++){
                            if (vresult.JobTask[i].JobTaskTechnician[j].isActive == 1){
                                teknisiaktif.push(vresult.JobTask[i].JobTaskTechnician[j])
                            }
                        }
                        vresult.JobTask[i].JobTaskTechnician = teknisiaktif
                    }
                    // ==== ambil teknisi yg isActive na 1 aja ==================== end

                    // var vNamaSa = '';
                    // if (typeof vresult.UserSa == 'object'){
                    //     if (typeof vresult.UserSa.EmployeeName !== 'undefined'){
                    //         vNamaSa = vresult.UserSa.EmployeeName;
                    //     }
                    // }
                    // vresult.NamaSa = vNamaSa;
                    console.log("vresult time", vresult);
                    var vstallid = vresult.StallId;
                    vresult.StallName = '';
                    if ((typeof xboard !== 'undefined') && (typeof xboard.config !== 'undefined') && (typeof xboard.config.argStall !== 'undefined') &&
                        (typeof xboard.config.argStall.stallVisible !== 'undefined') && (typeof xboard.config.argStall.stallVisible[vstallid] !== 'undefined') &&
                        (typeof xboard.config.argStall.stallVisible[vstallid].title !== 'undefined')) {
                        vresult.StallName = xboard.config.argStall.stallVisible[vstallid].title;
                    }
                    vresult.StartAllocation = $filter('date')(vresult.PlanDateStart, 'dd MMM yyyy HH:mm') /*+' '+vresult.PlanStart.substring(0, 5)*/ ;
                    vresult.DeliveryTime = $filter('date')(vresult.PlanDateFinish, 'dd MMM yyyy') + ' ' + vresult.PlanFinish.substring(0, 5);
                    console.log('datee', vresult.PlanDateStart, vresult.PlanStart);

                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();
                    today = yyyy + '-' + mm + '-' + dd;
                    var date = today;
                    var Stime = vresult.PlanStart;
                    var Etime = vresult.PlanFinish;

                    var EDate = new Date(date + " " + Etime);
                    var SDate = new Date(date + " " + Stime);

                    var selisihjam = (EDate.getHours() - SDate.getHours());
                    var selisihmenit = (EDate.getMinutes() - SDate.getMinutes());
                    var getdetik = EDate.getSeconds();
                    var jumlahselisihmenit = selisihmenit + 30;

                    var totaljam = SDate.getHours() + selisihjam;
                    var totalmenit = SDate.getMinutes() + jumlahselisihmenit;

                    if (totalmenit >= 60) {
                        totaljam = totaljam + 1;
                        totalmenit = jumlahselisihmenit - 60;
                    }

                    var Jamdeliv = totaljam + ':' + totalmenit;



                    vresult.DeliveryTime = Jamdeliv;
                    // vresult.PlanDateFinish = $filter('date')(combinedDeliveryTime, 'dd MMM yyyy HH:mm');
                    var dtDeliveryTime = $filter('date')(vresult.PlanDateFinish, 'dd MMM yyyy');
                    console.log("dtDeliveryTime", dtDeliveryTime);
                    console.log("vresult.DeliveryTime", vresult.DeliveryTime.toString());

                    var combinedDeliveryTime = new Date(dtDeliveryTime + ' ' + vresult.DeliveryTime.toString());
                    vresult.PlanDateFinish = $filter('date')(combinedDeliveryTime, 'dd MMM yyyy HH:mm');

                    console.log("Jam delivery", combinedDeliveryTime);

                    // if (vresult.FixedDeliveryTime2) {
                    //     vdeliv = vresult.FixedDeliveryTime2;
                    // }
                    //vresult.DeliveryTime = $filter('date')(vdeliv, 'hh:mm ');

                    vresult.isWOBase = vresult.isWOBase + '';
                    vresult.editable = checkEditableItem(vresult);
                    vresult.editableDispatch = checkEditableItemDispatch(vresult);
                    console.log('editable ga nih', vresult);

                    vresult.FmtWoCreatedDate = $filter('date')(vresult.WoCreatedDate, 'dd MMM yyyy HH:mm');
                    vresult.PlanStart = $filter('date')(vresult.PlanStart, 'HH:mm');
                    var dtPlanStart = $filter('date')(vresult.PlanDateStart, 'dd MMM yyyy');

                    var combinedPlanStart = new Date(dtPlanStart + ' ' + vresult.PlanStart);
                    console.log("combined", combinedPlanStart);

                    vresult.PlanDateStart = $filter('date')(combinedPlanStart, 'dd MMM yyyy HH:mm');

                    var vIsSplitable = 0;
                    if ((typeof resp.config.customData !== 'undefined') && (typeof resp.config.customData.isSplitable !== 'undefined')) {
                        vIsSplitable = resp.config.customData.isSplitable;
                    }
                    if (typeof vresult['JobId'] !== 'undefined') {
                        var xJobId = vresult['JobId'];
                        detailJobData[xJobId] = vresult;
                        detailJobData[xJobId].JobTask = arrJobTaskVresult; //masukkan jobtask yg aktif aja;
                        // console.log("edit chip", vresult.JobTask[0].JobTaskTechnician[0].Overtime, detailJobData[xJobId]);

                        if (typeof $scope.displayedChip == 'undefined' || typeof $scope.displayedChip.JobId == 'undefined') {
                            console.log('masuk ke atas')
                            $scope.displayedChip = detailJobData[vJobId];
                            $scope.displayedChip.isSplitable = vIsSplitable;
                            if ($scope.displayedChip.FixedDeliveryTime1 != $scope.displayedChip.FixedDeliveryTime2) {
                                $scope.displayedChip.DelivTime = $filter('date')($scope.displayedChip.FixedDeliveryTime2, 'dd MMM yyyy HH:mm');
                            } else {
                                $scope.displayedChip.DelivTime = $filter('date')($scope.displayedChip.FixedDeliveryTime1, 'dd MMM yyyy HH:mm');
                            }
                            $timeout(function() {
                                $scope.$apply();
                                //$scope.safeApply();
                            }, 200);
                        } else {
                            console.log('masuk ke bawah')
                            if ($scope.displayedChip.JobId == xJobId) {
                                $scope.displayedChip = detailJobData[vJobId];
                                if ($scope.displayedChip.FixedDeliveryTime1 != $scope.displayedChip.FixedDeliveryTime2) {
                                    $scope.displayedChip.DelivTime = $filter('date')($scope.displayedChip.FixedDeliveryTime2, 'dd MMM yyyy HH:mm');
                                } else {
                                    $scope.displayedChip.DelivTime = $filter('date')($scope.displayedChip.FixedDeliveryTime1, 'dd MMM yyyy HH:mm');
                                }
                                console.log("displayedChip", $scope.displayedChip);

                                $scope.displayedChip.isSplitable = vIsSplitable;
                                // 20181204, Begin, Overtime calculation, Tiyas
                                // $scope.displayedChip.Overtime = vresult.JobTask[0].JobTaskTechnician[0].Overtime;
                                // JpcbGrViewFactory.getStallCloseTime(vresult['StallId']).then(function(res){
                                //     $scope.xStallCloseTime = res.data;

                                //     $scope.displayedChip.Overtime = 0;
                                //     var vPlanDateStart = vresult['PlanDateStart'].toString().substring(0,11);
                                //     var vPlanDateFinish = vresult['PlanDateFinish'].toString().substring(0,11);

                                //     var vPlanStart = vresult['PlanStart'];
                                //     var vPlanFinish = vresult['PlanFinish'];
                                //     var vStallCloseTime = $scope.xStallCloseTime;

                                //     var vPlanFinishSplit = vPlanFinish.split(':');
                                //     var vPlanStartSplit = vPlanStart.split(':');
                                //     var vStallCloseTimeSplit = vStallCloseTime.split(':');

                                //     var FinishSecond = (+vPlanFinishSplit[0]) * 60 * 60 + (+vPlanFinishSplit[1]) * 60 + (+vPlanFinishSplit[2]); 
                                //     var StartSecond = (+vPlanStartSplit[0]) * 60 * 60 + (+vPlanStartSplit[1]) * 60 + (+vPlanStartSplit[2]); 
                                //     var CloseSecond = (+vStallCloseTimeSplit[0]) * 60 * 60 + (+vStallCloseTimeSplit[1]) * 60 + (+vStallCloseTimeSplit[2]); 

                                //     var hours = 0;
                                //     var minutes = 0;
                                //     if (vPlanStart > vStallCloseTime){
                                //         hours = Math.floor((FinishSecond-StartSecond) / 3600);
                                //         minutes = Math.floor(((FinishSecond-StartSecond) - (hours * 3600)) / 60);
                                //     } else {
                                //         hours = Math.floor((FinishSecond-CloseSecond) / 3600);
                                //         minutes = Math.floor(((FinishSecond-CloseSecond) - (hours * 3600)) / 60);
                                //     }

                                //     if (vPlanDateStart == vPlanDateFinish ){
                                //         if(hours < 0){
                                //             hours = 0;
                                //         }
                                //         $scope.displayedChip.Overtime = hours + ' jam ' + minutes + ' menit';
                                //     }        
                                // });   //DIMINTA DIKOSONGIN SAJA FIELD OVERTIMEnya

                                // End
                                if ($scope.displayedChip.MProfileForemanId != null || $scope.displayedChip.MProfileForemanId != undefined) {
                                    $scope.selectedForeman($scope.displayedChip.MProfileForemanId, 1)
                                }
                                $timeout(function() {
                                    $scope.$apply();
                                    //$scope.safeApply();
                                }, 200);
                            }
                        }
                    }
                }
            });
            // }
        }

        $scope.showChipInfo = function(vmode) {
            if (vmode) {
                $scope.wcolumn = vlong1;
                $scope.ncolumn = 'col-xs-4';
                $scope.ncolvisible = 1;
            } else {
                $scope.wcolumn = vlong2;
                $scope.ncolvisible = 0;
            }
        }

        //$scope.showChipInfo(false);

        $scope.user = CurrentUser.user();
        // console.log("CurrentUser.user()", CurrentUser.user());

        var vmsgname = 'joblist';
        var voutletid = $scope.user.OrgId;
        var vgroupname = voutletid + '.' + vmsgname;
        var vappname = vmsgname;
        try {
            $rootScope.ChatProxy.invoke('joinRoom', vgroupname);
        } catch (exc) {
            console.log(exc);
            // $scope.showMsg = { message: 'Tidak dapat terhubung dengan SignalR server.', title: 'Koneksi SignalR gagal.', closeText: 'Close', exception: exc }
            // ngDialog.openConfirm({
            //     width: 700,
            //     template: 'app/boards/factories/showMessage.html',
            //     plain: false,
            //     scope: $scope,
            // })

        }

        var showMessage = function(message) {
            // ngDialog.openConfirm({
            //     width: 400,
            //     template: message,
            //     plain: true,
            //     controller: 'JpcbGrController',
            //     scope: $scope
            // });
            bsAlert.alert({
                title: message,
                text: "",
                type: "success"
            });
        }

        var showMessageNotify = function(message) {
            // ngDialog.openConfirm({
            //     width: 400,
            //     template: message,
            //     plain: true,
            //     controller: 'JpcbGrController',
            //     scope: $scope
            // });
            bsNotify.show({
                size: 'big',
                title: message,
                // text: 'I will close in 2 seconds.',
                timeout: 2000,
                type: 'danger'
            });
        }

        var confirmMessage = function(vtitle, vmessage, onOk, onCancel) {
            $rootScope.tab = { message: vmessage, title: vtitle, okText: 'Ya', cancelText: 'Tidak' }

            ngDialog.openConfirm({
                    width: 700,
                    template: 'app/boards/tab/tabMessage.html',
                    plain: false,
                    scope: $rootScope,
                })
                .then(function(value) {
                    if (onOk) {
                        return onOk(value);
                    }
                }, function(reason) {
                    if (onCancel) {
                        return onCancel(reason);
                    }
                });

        }

        $scope.$on('SR-' + vappname, function(e, data) {
            console.log("notif-data", data);
            var vparams = data.Message.split('.');

            var vJobId = 0;
            if (typeof vparams[0] !== 'undefined') {
                var vJobId = vparams[0];
            }

            var vStatus = 0;
            if (typeof vparams[1] !== 'undefined') {
                var vStatus = vparams[1];
            }


            var jobid = vparams[0];
            var vcounter = vparams[1];
            var vJobList = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "20"];
            var xStatus = vStatus + '';

            if (vJobList.indexOf(xStatus) >= 0) {
                showBoardJPCB();
            }

        });

        $scope.jpcbgrview.reloadBoard = function() {
            console.log("tessssssss");
            showBoardJPCB();
            $scope.StoppageNomorPolisi = '';
            $scope.SearchStoppage();
            $scope.showChipInfo(false);
        }



        // untuk menampilkan garis waktu saat ini (timeline)
        var timeActive = true;
        var minuteTick = function() {
            if (!timeActive) {
                return;
            }
            //if ()
            JpcbGrViewFactory.updateTimeLine(boardName);
            $timeout(function() {
                minuteTick();
            }, 60000);
            console.log("dari controller");
            // jpcbFactory.setOnChipLoaded(boardName, function(vjpcb) {
            //     $scope.datachip1.push(vjpcb.allChips);
            //     console.log("dari controller", vjpcb);
            //     return JpcbGrViewFactory.onChipLoaded(vjpcb);
            // });

            $scope.datachip1 = JpcbGrViewFactory.dataAllChip();
            console.log("tessssssss view22222", $scope.datachip1);

            if ($scope.datachip1 != null){
                this.CekChipExpired($scope.datachip1);
            }
            // console.log("tessssssss view", $scope.datachip1);
        }

        // console.log(CurrentUser);
        // $rootScope.ChatProxy.invoke('joinRoom', '655.joblist');

        // $scope.$on('SR-joblist', function(e, data){
        //     console.log("*** notif-data joblist *** ", data);
        // });



        $scope.$on('$viewContentLoaded', function() {

            console.log("view Content Loaded");
            //Menggunakan resize detector untuk mengatasi bug board tidak muncul setelah menu di-close dan diopen lagi.
            // elemen yang dimonitor adalah panel yang width-nya 100%
            var velement = document.getElementById('jpcb_gr_edit_panel');
            var erd = elementResizeDetectorMaker();
            erd.listenTo(velement, function(element) {
                if (!isBoardLoaded) {
                    isBoardLoaded = true;
                    showBoardJPCB();
                    minuteTick();
                    // untuk menampilkan timeline
                    // $timeout( function(){
                    //     minuteTick();
                    // }, 1000);
                }
                vboard = JpcbGrViewFactory.getBoard(boardName);
                if (vboard) {
                    vboard.autoResizeStage();
                    vboard.redraw();
                }

            });
            Idle.unwatch();
            console.log('==== value of idle ==', Idle)

        });


        $scope.$on('$destroy', function() {
            timeActive = false;
            Idle.watch();
        });

        $scope.disableSplit = false;
        $scope.execSplitChip = function(vjobid, dataChip) {
            //alert('execChip');
            // function(vtitle, vmessage, onOk, onCancel)
            console.log('data si chip ',tempVDATA)
            console.log('nih buat split jobsplitID nya', $scope.jobsplitID)

            if (tempVDATA.ext == null || tempVDATA.ext == undefined){
                tempVDATA.ext = 0
            }
            var mntExt = tempVDATA.ext * 60;

            var thnPause = tempVDATA.PauseDate.getFullYear()
            var blnPause = tempVDATA.PauseDate.getMonth()
            var tglPause = tempVDATA.PauseDate.getDate()
            var jamPause = tempVDATA.PauseTime.split(':')[0]
            var mntPause = tempVDATA.PauseTime.split(':')[1]
            var dtkPause = tempVDATA.PauseTime.split(':')[2]

            var thnFinish = tempVDATA.PlanDateFinish.getFullYear()
            var blnFinish = tempVDATA.PlanDateFinish.getMonth()
            var tglFinish = tempVDATA.PlanDateFinish.getDate()
            var jamFinish = tempVDATA.PlanFinish.split(':')[0]
            var mntFinish = tempVDATA.PlanFinish.split(':')[1]
            var dtkFinish = tempVDATA.PlanFinish.split(':')[2]

            var thnStart = tempVDATA.PlanDateStart.getFullYear()
            var blnStart = tempVDATA.PlanDateStart.getMonth()
            var tglStart = tempVDATA.PlanDateStart.getDate()
            var jamStart = tempVDATA.PlanStart.split(':')[0]
            var mntStart = tempVDATA.PlanStart.split(':')[1]
            var dtkStart = tempVDATA.PlanStart.split(':')[2]

            var mntFinishwExt = parseInt(mntFinish) + parseInt(mntExt)
            var pauseDateTime = new Date(thnPause, blnPause, tglPause, jamPause, mntPause, dtkPause)
            var FinishDateTime = new Date(thnFinish, blnFinish, tglFinish, jamFinish, mntFinishwExt, dtkFinish)
            var StartDateTime = new Date(thnStart, blnStart, tglStart, jamStart, mntStart, dtkStart)

            if (pauseDateTime.getTime() > FinishDateTime.getTime()){
                bsAlert.warning('Tidak bisa split di luar range chip, harap extend')
                return;
            } else if ((FinishDateTime.getTime() - pauseDateTime.getTime()) < 900000) {
                // 900000 adalah 15 menit
                bsAlert.warning('Tidak bisa split karena sisa waktu chip kurang dari 15 menit, harap extend')
                return;
            } else if (pauseDateTime.getTime() < StartDateTime.getTime()){
                bsAlert.warning('Tidak bisa split karena waktu pause lebih kecil dari plan. Silahkan lanjutkan proses dan pause kembali di tengah jam plan')
                return;
            }


            if ($scope.jobsplitID == null || $scope.jobsplitID == undefined) {
                $scope.jobsplitID = 0;
            }

            confirmMessage('Split Chip', 'Apakah chip ini akan di-split agar bisa dipindahkan ?', function() {
                $scope.disableSplit = true;
                var url = '/api/as/Boards/jpcb/splitChipJPCB_GR/' + vjobid + '/' + $scope.jobsplitID;
                $scope.isBoardLoading = true;
                $http.put(url, '', { headers: { 'Content-Type': 'application/json' } }).then(function(resp) {
                    $scope.showChipInfo(false);
                    console.log(resp);
                    var vmsg = "Data sent.";
                    if ((typeof resp.data !== 'undefined') && (typeof resp.data.ResponseMessage !== 'undefined')) {
                        var vmsg = resp.data.ResponseMessage;
                    }
                    $timeout(showMessage(vmsg), 100);
                    $scope.isBoardLoading = false;
                    showBoardJPCB();
                    // $timeout(function() {
                    //     $scope.isBoardLoading = false;
                    //     showBoardJPCB();
                    // }, 500);
                    $timeout(function() {
                        $scope.disableSplit = false;
                    }, 45000);


                });

            }, function() {});
            $timeout(function() {
                $scope.disableSplit = false;
            }, 45000);
        }

        // $scope.asb.FullScreen = function(){
        //     // alert('full screen');
        //     var vobj = document.getElementById('asb_estimasi_view');
        //     vobj.style.cssText = 'position: absolute; left: 0; top: 0; width: 100%; height: 100%;';
        // }


        // angular.element($window).bind('resize', function () {
        //     //AsbEstimasiFactory.resizeBoard();
        //     // AsbProduksiFactory.resizeBoard();

        //     // jpcbStopage.autoResizeStage();
        //     // jpcbStopage.redraw();
        // });

        $scope.Split = function(displayedChip) {
            console.log('coeg', displayedChip);

        }


        CekChipExpired = function(datachip) {
            // var dtchip = datachip[0];
            var dtchip = datachip.allChips;
            var listNoPloiceExpired = [];
            var listStallId = [];
            var listStallMenumpuk = []; // buat cek kalau ada chip yang di tambahin job nya dan menumpuk ke chip di depannya, stall jadi merah

            if (dtchip != undefined) {

                for (var i = 0; i < dtchip.length; i++) {
                    var vitem = dtchip[i];
                    console.log("vitem cek expired", vitem);
                    if ($filter('date')(vitem.PlanDateStart, 'dd-MM-yyyy') === $filter('date')(new Date(), 'dd-MM-yyyy')) {
                        var vfinish = '';
                        if (vitem.ext > 0){
                            var totalExt = vitem.ext * 60;
                            var dateFinish = vitem.PlanDateFinish;
                            var jamFinish = vitem.PlanFinish.split(':');
                            var dateFinishWithExt = new Date (dateFinish.getFullYear(), dateFinish.getMonth(), dateFinish.getDate(), parseInt(jamFinish[0]), (parseInt(jamFinish[1]) + totalExt), 0);
                            var xJam = '00';
                            var xMin = '00';
                            console.log('aiiii', dateFinishWithExt.getHours())
                            console.log('aiiii', dateFinishWithExt.getHours().toString().length)
                            if (dateFinishWithExt.getHours().toString().length < 2){
                                xJam = '0'+ dateFinishWithExt.getHours();
                            } else {
                                xJam = '' + dateFinishWithExt.getHours();
                            }
                            if (dateFinishWithExt.getMinutes().toString().length < 2){
                                xMin = '0'+ dateFinishWithExt.getMinutes();
                            } else {
                                xMin = ''+ dateFinishWithExt.getMinutes();
                            }
                            vfinish = xJam +':'+ xMin + ':'+'00';
                        } else {
                            vfinish = $filter('date')(vitem.PlanFinish);
                        }
                        var vnow = $filter('date')(new Date(), 'HH:mm:ss');
                        var vdatenow = new Date().getDate() + '-' + new Date().getMonth() + '-' + new Date().getFullYear();
                        var vPlanDateFinish = vitem.PlanDateFinish.getDate() + '-' + vitem.PlanDateFinish.getMonth() + '-' + vitem.PlanDateFinish.getFullYear();
                        // if (vdatenow == vPlanDateFinish){
                        //     console.log('sama hari')
                        // } else {
                        //     console.log('beda')
                        // }
                        
                        // if (vnow >= vfinish && ((vitem.intStatus >= 5 && vitem.intStatus < 12) || vitem.intStatus == 22) && vitem.isSplitActive != 0 && vitem.isInStopPageGr != 1 && vdatenow == vPlanDateFinish) { // isSplitActive nya di taro ke dalem, bkin susah
                        if (vnow >= vfinish && ((vitem.intStatus >= 5 && vitem.intStatus < 12) || vitem.intStatus == 22) && vitem.isInStopPageGr != 1 && vdatenow == vPlanDateFinish) {
                            if (vitem.TimeFinish != null && vitem.TimeFinish != undefined){
                                // ga di apa apain sih, soal nya uda clock off.. hrs nya ga usah di merah2
                            } else {
                                if (vitem.JobSplitId == 0){
                                    listNoPloiceExpired.push(vitem.nopol);
                                    listStallId.push(vitem.stallId);
                                } else {
                                    if (vitem.isSplitActive != 0){
                                        listNoPloiceExpired.push(vitem.nopol);
                                        listStallId.push(vitem.stallId);
                                    }
                                }
                            }
                            
                            // listNoPloiceExpired.push(vitem.nopol);
                            // listStallId.push(vitem.stallId);

                            // var sFinalIns = JpcbGrViewFactory.statusFinalinspektion(vitem.jobId)

                            // console.log('udahh lewatt controller', sFinalIns);
                        }


                        for (var k=0; k<dtchip.length; k++){
                            var plotedchip = dtchip[k];
                            if (vitem.nopol != plotedchip.nopol && vitem.stallId == plotedchip.stallId && vitem.col < plotedchip.col){
                                var startvitem = vitem.col;
                                var endvitem = vitem.col + vitem.width;
                                var startplotedchip = plotedchip.col;
                                var endplotedchip = plotedchip.col + plotedchip.width;

                                if (endvitem > startplotedchip){
                                    listStallMenumpuk.push(vitem.stallId);
                                    listStallId.push(vitem.stallId);
                                    console.log('ini data menumpuk di gr', listStallMenumpuk);
                                }
                            }
                        }


                    }
                }
            }
            //kondisi apabila ada chip yang baru expired, maka akan di alert
            if ((listnopol.length != listNoPloiceExpired.length) && (listNoPloiceExpired.length > 0)) {
                alert("Chip No. Polisi " + listNoPloiceExpired.join(' , ') + " sudah melewati jam yang seharusnya, silahkan ploting ulang ");

                //seteleah di alert di 1501 maka berikan notifikasi pada SA 1201
                // $scope.JbId = JobId;
                $scope.modal_modelTask = [];
                $scope.modalModeTask = 'new';
                //$scope.doAddPekerjaan = function (Data) {

                console.log("Ini data gueeeeee", datachip);
                RepairProcessGR.addTask(datachip, $scope.IdSA).then(
                    function(res) {
                        console.log('Suksess simpan');

                        //  RepairProcessGR.updateFlagForAddTask($scope.JbId).then(
                        //      function (res) {
                        //          console.log('Suksess update');
                        //          $scope.show_modalTask = { show: false };
                        //          $scope.getDataDetail($scope.JbId);
                        //          ngDialog.closeAll();
                        //      },
                        //      function (err) {
                        //          console.log("err=>", err);
                        //      }
                        //  );

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            }

            console.log("length list", listnopol.length + " " + listNoPloiceExpired.length)
            listnopol = listNoPloiceExpired;
            showBoardJPCB(listStallId);

        }


        var showBoardJPCB = function(chipExpired) {
                if ($scope.isBoardLoading) {
                    return;
                }
                $scope.isBoardLoading = true;
                detailJobData = {}
                var vscrollhpos = JpcbGrViewFactory.getScrollablePos(boardName);
                console.log('vscrollhpos tjoi', vscrollhpos)
                var vitem = {
                    // mode: $scope.jpcbgrview.showMode,
                    currentDate: $scope.jpcbgrview.currentDate,
                    stallUrl: '/api/as/Boards/Stall/1',
                    chipUrl: '/api/as/Boards/jpcb/bydate/' + $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd') + '/0/17',
                    // nopol: $scope.jpcbgrview.nopol,
                    // tipe: $scope.jpcbgrview.tipe
                }
                console.log("showBoardJPCB");
                var xchipClickCheeck = false;

                JpcbGrViewFactory.showBoard(boardName, {
                    container: 'jpcb_gr_edit',
                    currentChip: vitem,
                    options: {
                        standardDraggable: true,
                        draggableStatus: ["4", "6", "22", "25"], // nambahin 25 pause rawatjalan 2april2020
                        // stopageAreaHeight: 200,
                        stopageAreaHeight: 140,
                        //testing12345: 1000,
                        onJpcbReady: function(vjpcb) {
                            console.log("*****board ready", vjpcb);
                            console.log("tessssssss view");

                            $scope.isLoading = false;
                            $scope.isBoardLoading = false;

                            $scope.countNoShow = vjpcb.argChips.countNoShow;
                            $scope.countBookingA = vjpcb.argChips.countBookingA;
                            $scope.countBookingB = vjpcb.argChips.countBookingB;
                            $scope.countCarryOver = vjpcb.argChips.countCarryOver;
                            $scope.countInProgress = vjpcb.argChips.countInProgress;
                            $scope.countWIP = vjpcb.argChips.countWIP;
                            $scope.countProductionCapacity = vjpcb.argChips.countProductionCapacity;

                            vjpcb.theBoard.layout.board.checkDragDrop = function(vchip, vcol, vrow) {
                                console.log("dragdrop JPCB");

                                if (vchip.data.row >= 0) {
                                    vlayout = vjpcb.theBoard.layout;
                                    if (typeof vlayout.infoRows[vchip.data.row] != 'undefined') {
                                        var vStallInfo = vlayout.infoRows[vchip.data.row];
                                        var vEnd = vStallInfo.data.endHour;
                                        var vjamstart = JpcbGrViewFactory.columnToTime(vcol);
                                        if (vjamstart >= vEnd) {
                                            return false;
                                        }
                                        var splitJam = vjamstart.split(':');
                                        if (splitJam[0] == '12'){
                                            return null; // ini kalau dia di taro di jam 12 - 13. ga boleh di jam istirahat
                                        }

                                        //if (vchip.data.Final)

                                    }
                                } else {
                                    return false;
                                }

                                return true;
                            };

                            // alert('ready');
                        },
                        board: {
                            scrollableLeft: vscrollhpos,
                            footerChipName: 'footer_stopage',
                            onStallItemCreated: function(vfront, vback) {
                                console.log("---Chip Created---", vfront, vback);
                                vfront.on('click', function(vevt) {
                                    // alert('click');
                                    console.log("---- click chip ----", this);
                                    showMenu();
                                })
                            },

                        }
                    },
                    onChipClick: function(vchip) {
                        if (!xchipClickCheeck) {
                            xchipClickCheeck = true;
                            console.log('on chip click!!');
                            console.log('on chip click value!!', vchip);
                            // di kasi validasi karena jobs tiba2 ada yang mengandung  ' - '
                            var tempjobid = null;
                            tempjobid = angular.copy(vchip.currentTarget.data.jobId);
                            tempjobid = tempjobid.toString();
                            if (tempjobid.includes('-')) {
                                console.log('masuk ada -', tempjobid.replace("-", ""));
                                tempjobid = tempjobid.replace("-", "");

                                console.log('tempjobid', parseInt(tempjobid));
                                vchip.currentTarget.data.jobId = parseInt(tempjobid);
                            }

                            console.log('on chip click!!', vchip.currentTarget.data.jobId);

                            $scope.showChipInfo(true);
                            displayChipData(vchip.currentTarget.data.jobId, vchip.currentTarget.data);
                            //$scope.displayedChip = vchip.currentTarget.data;
                            //$scope.$apply();
                            $timeout(function() {
                                xchipClickCheeck = false;
                            }, 200);
                        }
                    },
                    onPlot: function(vchip) {
                        // var plotUrl = '/api/as/jobs/plotBoardJPCB_GR';
                        var plotUrl = '/api/as/Boards/jpcb/plotBoardJPCB_GR';
                        console.log("on plot. vchip: ", vchip);
                        console.log('cek this', this.currentChip);
                        var ThisCurrentChip = this.currentChip;

                        if (vchip.data.stallId > 0 && vchip.data.stallId != null){
                            var tempJamTutup = 0;
                            var JamTutup = 0;
                            var SisaJamLebih = 0; //sisa jam yg lebih / lewat dari jam tutup
                            JpcbGrViewFactory.getStallCloseTime(vchip.data.stallId).then(function(res){
                                console.log('res jam tutup stall', res.data)
                                tempJamTutup = res.data.split(':');
                                JamTutup = parseInt(tempJamTutup[0]);
                                JamTutup = JamTutup + 1; //karena kalo liat di board nya emang ketambah 1. entah dari mana

                                if (vchip.dragStartPos) {
                                    var vChipJobId = vchip.data.jobId;
                                    if (vchip.data.row == vchip.dragStartPos.row && vchip.data.col == vchip.dragStartPos.col) {
                                        // sama
                                        //console.log("sama");
                                    } else {
                                        // tidak sama
        
                                        console.log("tidak sama");
                                        //vchip.data;
                                        //var vinc = JpcbGrViewFactory.incrementMinute;
                                        var vdate = new Date(ThisCurrentChip.currentDate);
                                        var vjamstart = JpcbGrViewFactory.columnToTime(vchip.data.col);
                                        console.log("vjamstart", vjamstart);
                                        var vjamend = JpcbGrViewFactory.columnToTime(vchip.data.col + vchip.data.normal + vchip.data.ext);
                                        console.log("vjamend", vjamend);
                                        console.log("vdate", vdate);

                                        // cek kena break buat nambah ke vdateend
                                        var cekjamstart = vjamstart.split(':');
                                        var cekjamend = vjamend.split(':');
                                        if (parseInt(cekjamstart[0]) < 12) {
                                            if (parseInt(cekjamend[0]) == 12 && parseInt(cekjamend[1]) > 0){
                                                vjamend = JpcbGrViewFactory.columnToTime(vchip.data.col + vchip.data.normal + vchip.data.ext + 1);
                                            } else if (parseInt(cekjamend[0]) == 12 && parseInt(cekjamend[1]) == 0){
                                                vjamend = JpcbGrViewFactory.columnToTime(vchip.data.col + vchip.data.normal + vchip.data.ext);
                                            } else if (parseInt(cekjamend[0]) > 12 && parseInt(cekjamend[1]) >= 0){
                                                vjamend = JpcbGrViewFactory.columnToTime(vchip.data.col + vchip.data.normal + vchip.data.ext + 1);
                                            }
                                        }
                                        
                                        // cek kena break buat nambah ke vdateend


                                        var tempJamEnd = vjamend.split(':');
                                        if (parseInt(tempJamEnd[0]) > JamTutup){
                                            vjamend = JamTutup.toString() + ':00';
                                            SisaJamLebih = parseInt(tempJamEnd[0] - JamTutup);
                                        }
                                        var vDateStart = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd') + 'T' + vjamstart + ':00';
                                        var vDateEnd = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd') + 'T' + vjamend + ':00';
                                        var vStallId = 0;
                                        vlayout = vchip.container.layout;
                                        if (vchip.data.row >= 0) {
                                            if (typeof vlayout.infoRows[vchip.data.row]) {
                                                vStallId = vlayout.infoRows[vchip.data.row].data.stallId;
                                            }
                                        }
        
                                        var xJobSplitId = vchip.data.JobSplitId;
                                        // if (xJobSplitId == 0){
                                        //     xJobSplitId = null;
                                        // } 

                                        var oPlanStart = vjamstart+ ':00'
                                        var oPlanFinish = vjamend+ ':00'
                                        var oStallId = vStallId
                                        var oPlanDateStart = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd')
                                        var oPlanDateFinish = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd')
                                        var oJobListSplitChip = [];
                                        var oIsInStopPageGr = 0;
                                        if (vStallId >= 0){
                                            oIsInStopPageGr = 0;
                                        } else {
                                            oIsInStopPageGr = 1;
                                        }
                                        if (xJobSplitId != 0){
                                            oJobListSplitChip = [{
                                                PlanStart : oPlanStart,
                                                PlanFinish : oPlanFinish,
                                                StallId : oStallId,
                                                PlanDateStart : oPlanDateStart,
                                                PlanDateFinish : oPlanDateFinish,
                                                isGr : 1,
                                                JobId : vchip.data.jobId,
                                                IsInStopPageGr : oIsInStopPageGr
                                            }]
                                        }
                                        var oDataCek = [{
                                                PlanStart : oPlanStart,
                                                PlanFinish : oPlanFinish,
                                                StallId : oStallId,
                                                PlanDateStart : oPlanDateStart,
                                                PlanDateFinish : oPlanDateFinish, 
                                                JobListSplitChip : oJobListSplitChip,
                                                isGr : 1,
                                                JobId : vchip.data.jobId,
                                                IsInStopPageGr : oIsInStopPageGr
                                        }]

                                        JpcbGrViewFactory.cekPlotKeDB(oDataCek).then(function(resul){
                                            if (resul.data == true){
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Sudah ada chip di area ini, silahkan refresh board",
                                                });
                                            } else {
                                                var vobj = {
                                                    "DateStart": vDateStart,
                                                    "DateFinish": vDateEnd,
                                                    "StallId": vStallId,
                                                    "JobId": vchip.data.jobId,
                                                    "JobSplitId": xJobSplitId,
                                                    "Sisa": SisaJamLebih
                                                }
                                                console.log("vobj", vobj);
                                                $scope.isBoardLoading = true;
                                                $http.put(plotUrl, [vobj]).then(
                                                    function(res) {
                                                        console.log(res);
                                                        $timeout(function() {
                                                            $scope.isBoardLoading = false;
                                                            var vfound = false;
                                                            var xboard = JpcbGrViewFactory.getBoard(boardName);
                                                            xboard.processChips(function(xchip) {
                                                                    if (vfound) {
                                                                        return false;
                                                                    }
                                                                    if (xchip.data.jobId == vChipJobId) {
                                                                        vfound = true;
                                                                        xfoundChip = xchip;
                                                                        return true;
                                                                    }
                                                                    return false;
                                                                },
                                                                function(xchip) {
                                                                    delete detailJobData[xchip.data.jobId];
                                                                    displayChipData(xchip.data.jobId, xchip.data);
                                                                }, false);
                                                            showBoardJPCB();
                                                        }, 500);
                
                                                    },
                                                    function(res) {
                                                        // var vlayout = vchip.container.layout;
                                                        // var vstarty = vlayout.row2y(vchip.dragStartPos.row);
                                                        // var vstartx = vlayout.col2x(vchip.dragStartPos.col);
                
                                                        // vchip.moveTo(vchip.container.board.chips);
                                                        // vchip.x(vstartx);
                                                        // vchip.y(vstarty);
                                                        // vchip.data.col = vchip.dragStartPos.col;
                                                        // vchip.data.row = vchip.dragStartPos.row;
                                                        // vchip.container.updateChip(vchip);
                                                        // vchip.container.redraw();
                                                        $scope.isBoardLoading = false;
                                                        $timeout(function() {
                                                            showBoardJPCB();
                                                        }, 500);
                
                
                
                                                    }
                                                );
                
                
                
                                                // [{
                                                //   "DateStart":"2017-05-08T11:30:00.000",
                                                //   "DateFinish":"2017-05-08T12:30:00.000",
                                                //   "StallId":2,
                                                //   "JobId"  :1151
                                                // }]
                                                // var vmenitStart = JsBoard.Common.
                                                // var vjamstart =
                                            }
                                        })
        
                                        
        
                                    }
                                }

                            });
                        } else {
                            if (vchip.dragStartPos) {
                                var vChipJobId = vchip.data.jobId;
                                if (vchip.data.row == vchip.dragStartPos.row && vchip.data.col == vchip.dragStartPos.col) {
                                    // sama
                                    //console.log("sama");
                                } else {
                                    // tidak sama
    
                                    console.log("tidak sama");
                                    //vchip.data;
                                    //var vinc = JpcbGrViewFactory.incrementMinute;
                                    var vdate = new Date(ThisCurrentChip.currentDate);
                                    var vjamstart = JpcbGrViewFactory.columnToTime(vchip.data.col);
                                    console.log("vjamstart", vjamstart);
                                    var vjamend = JpcbGrViewFactory.columnToTime(vchip.data.col + vchip.data.normal + vchip.data.ext);
                                    console.log("vjamend", vjamend);
                                    console.log("vdate", vdate);
                                    var vDateStart = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd') + 'T' + vjamstart + ':00';
                                    var vDateEnd = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd') + 'T' + vjamend + ':00';
                                    var vStallId = 0;
                                    vlayout = vchip.container.layout;
                                    if (vchip.data.row >= 0) {
                                        if (typeof vlayout.infoRows[vchip.data.row]) {
                                            vStallId = vlayout.infoRows[vchip.data.row].data.stallId;
                                        }
                                    }
    
                                    var xJobSplitId = vchip.data.JobSplitId;
                                    // if (xJobSplitId == 0){
                                    //     xJobSplitId = null;
                                    // } 

                                    var oPlanStart = vjamstart+ ':00'
                                        var oPlanFinish = vjamend+ ':00'
                                        var oStallId = vStallId
                                        var oPlanDateStart = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd')
                                        var oPlanDateFinish = $filter('date')($scope.jpcbgrview.currentDate, 'yyyy-MM-dd')
                                        var oJobListSplitChip = [];
                                        var oIsInStopPageGr = 0;
                                        if (vStallId >= 0){
                                            oIsInStopPageGr = 0;
                                        } else {
                                            oIsInStopPageGr = 1;
                                        }
                                        if (xJobSplitId != 0){
                                            oJobListSplitChip = [{
                                                PlanStart : oPlanStart,
                                                PlanFinish : oPlanFinish,
                                                StallId : oStallId,
                                                PlanDateStart : oPlanDateStart,
                                                PlanDateFinish : oPlanDateFinish,
                                                isGr : 1,
                                                JobId : vchip.data.jobId,
                                                IsInStopPageGr : oIsInStopPageGr
                                            }]
                                        }
                                        var oDataCek = [{
                                                PlanStart : oPlanStart,
                                                PlanFinish : oPlanFinish,
                                                StallId : oStallId,
                                                PlanDateStart : oPlanDateStart,
                                                PlanDateFinish : oPlanDateFinish, 
                                                JobListSplitChip : oJobListSplitChip,
                                                isGr : 1,
                                                JobId : vchip.data.jobId,
                                                IsInStopPageGr : oIsInStopPageGr
                                        }]

                                    JpcbGrViewFactory.cekPlotKeDB(oDataCek).then(function(resul){
                                        if (resul.data == true){
                                            bsNotify.show({
                                                size: 'big',
                                                type: 'danger',
                                                title: "Sudah ada chip di area ini, silahkan refresh board",
                                            });
                                        } else {

                                            var vobj = {
                                                "DateStart": vDateStart,
                                                "DateFinish": vDateEnd,
                                                "StallId": vStallId,
                                                "JobId": vchip.data.jobId,
                                                "JobSplitId": xJobSplitId,
                                                "Sisa": 0
                                            }
                                            console.log("vobj", vobj);
                                            $scope.isBoardLoading = true;
                                            $http.put(plotUrl, [vobj]).then(
                                                function(res) {
                                                    console.log(res);
                                                    $timeout(function() {
                                                        $scope.isBoardLoading = false;
                                                        var vfound = false;
                                                        var xboard = JpcbGrViewFactory.getBoard(boardName);
                                                        xboard.processChips(function(xchip) {
                                                                if (vfound) {
                                                                    return false;
                                                                }
                                                                if (xchip.data.jobId == vChipJobId) {
                                                                    vfound = true;
                                                                    xfoundChip = xchip;
                                                                    return true;
                                                                }
                                                                return false;
                                                            },
                                                            function(xchip) {
                                                                delete detailJobData[xchip.data.jobId];
                                                                displayChipData(xchip.data.jobId, xchip.data);
                                                            }, false);
                                                        showBoardJPCB();
                                                    }, 500);
            
                                                },
                                                function(res) {
                                                    // var vlayout = vchip.container.layout;
                                                    // var vstarty = vlayout.row2y(vchip.dragStartPos.row);
                                                    // var vstartx = vlayout.col2x(vchip.dragStartPos.col);
            
                                                    // vchip.moveTo(vchip.container.board.chips);
                                                    // vchip.x(vstartx);
                                                    // vchip.y(vstarty);
                                                    // vchip.data.col = vchip.dragStartPos.col;
                                                    // vchip.data.row = vchip.dragStartPos.row;
                                                    // vchip.container.updateChip(vchip);
                                                    // vchip.container.redraw();
                                                    $scope.isBoardLoading = false;
                                                    $timeout(function() {
                                                        showBoardJPCB();
                                                    }, 500);
            
            
            
                                                }
                                            );
            
        
            
                                            // [{
                                            //   "DateStart":"2017-05-08T11:30:00.000",
                                            //   "DateFinish":"2017-05-08T12:30:00.000",
                                            //   "StallId":2,
                                            //   "JobId"  :1151
                                            // }]
                                            // var vmenitStart = JsBoard.Common.
                                            // var vjamstart =

                                        }
                                    })
    
                                    
    
                                }
                            }

                        }

                    

                        
                        

                        

                    }
                }, chipExpired);
            }
            // var showBoardTab = function(vitem){
            //     JpcbGrViewFactory.showBoard({container: 'jpcb_gr_view', currentChip: vitem});

        // }


    })