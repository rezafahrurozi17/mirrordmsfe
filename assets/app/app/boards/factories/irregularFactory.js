angular.module('app')
    .factory('irregularFactory', function($http, CurrentUser, $filter, $timeout) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            boardConfigList: {},
            boardList: {},
            //minuteDelay: 12*60+10,
            minuteDelay: 0,
            tmpStatusWFR : 0,
            tmpStatusWFP : 0,
            tmpStatusWFFI : 0,
            tmpStatusWFTECO : 0,
            tmpStatusWFNotif : 0,
            tmpStatusWFD : 0,
            boardsName:[],
            getCurrentTime: function(){
                var vDate = new Date();
                vDate.setMinutes(vDate.getMinutes()-this.minuteDelay);
                return vDate;
            },
            setOnGetChips: function(boardname, evfunction){
                this.boardConfigList[boardname].onGetChips = evfunction;
            },
            setOnChipLoaded: function(boardname, evfunction){
                this.boardConfigList[boardname].onChipLoaded = evfunction;

            },
            setOnBoardCreated: function(boardname, evfunction){
                this.boardConfigList[boardname].onBoardCreated = evfunction;
            },
            createBoard: function(boardname, config){
                this.boardConfigList[boardname] = config;
                delete this.boardList[boardname];
                 // ==== Add Code
                if(this.boardsName.length > 6){
                    this.boardsName = [];
                }
                this.boardsName.push(boardname);

            },
            prepareData: function(boardname, onLoaded){
                var vconfig = this.boardConfigList[boardname];
                vconfig.dataStatus = 1;
                vconfig.onDataLoaded = onLoaded;
                var me = this;
                // load chip
                this.onGetChips(boardname, vconfig.argChips, function(vdata){
                    vconfig.allChips = vdata;
                    me.checkLoaded(boardname);
                });
            },
            onGetChips: function(boardname, varg, onFinish){
                var vconfig = this.boardConfigList[boardname];
                var tglAwal = varg.currentDate;
                //var vurl = '/api/as/Boards/jpcb/bydate/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/0/17';
                //var vurl = '/api/as/Boards/IrregularGR/wfr';
                var me = this;
                vconfig.statusGetData = 0;
                if (vconfig.chipUrl){
                    if (vconfig.chipUrl == '/api/as/queue/QueueList/1'){
                        $http.get(vconfig.chipUrl).then(function(res){
                            var datafilter = []; // buat nampung yang walk in dan other
                            var dateNow = new Date();
                            for (var j=0; j<res.data.length; j++){
                                // if (res.data[j].QueueCategory.Name == 'Walk In' || res.data[j].QueueCategory.Name == 'Others'){
                                    var waktuDatang = res.data[j].ArrivalTime.getTime();
                                    var waktuSkrg = dateNow.getTime();
                                    var selisih = (waktuSkrg - waktuDatang) / 60000 * 60 // buat itung lebih dari 10 menit ga
                                    // if (selisih > 600) {     
                                        datafilter.push(res.data[j]);
                                    // }
                                // }
                            }

                            var result = datafilter;
                            for(var i=0; i<result.length; i++){
                                result[i].currentTime = me.getCurrentTime();
                            }
                            vconfig.statusGetData = 1;
                            onFinish(result);
                        })
                    } else {
                        $http.get(vconfig.chipUrl).then(function(res){
                            var result = res.data.Result;
                            for(var i=0; i<result.length; i++){
                                result[i].currentTime = me.getCurrentTime();
                            }
                            vconfig.statusGetData = 1;
                            onFinish(result);
                        })
                    }

                } else {
                    $timeout(function(){
                        vconfig.statusGetData = 1;
                        var vdata = []
                        onFinish(vdata)
                    }, 1000);
                }
            },
            checkLoaded: function(boardname){
                var vconfig = this.boardConfigList[boardname];
                vconfig.dataStatus = vconfig.dataStatus-1;
                if (vconfig.dataStatus<=0){
                    vconfig.onDataLoaded();
                }
            },
            getBoard: function(boardname){
                var vresult = false;
                vresult = this.boardList[boardname];
                return vresult;
            },
            showBoard: function(boardname){
                var vconfig = this.boardConfigList[boardname];
                var me = this;
                this.prepareData(boardname, function(){
                    //var vconfig = this.boardConfigList[boardname];
                    if (typeof vconfig.onChipLoaded == 'function'){
                        vconfig.allChips = vconfig.onChipLoaded(vconfig.allChips);
                    }
                    vconfig.theBoard = me.doShowBoard(boardname);
                })
                console.log("boardConfigList ===>",this.boardConfigList);
            },
            doShowBoard: function(boardname){
                var vconfig = this.boardConfigList[boardname];
                var myBoard = new JsBoard.Container(vconfig);
                this.boardList[boardname] = myBoard;
                myBoard.createChips(vconfig.allChips);
                myBoard.redraw();
                if (vconfig.onBoardCreated){
                    vconfig.onBoardCreated(myBoard);
                }
            },
            onGetDataConfig: function(){
                return this.boardConfigList
            },
            onGetDataBoardName: function(){
                return this.boardsName
            }
        }
    });



