angular.module('app')
    .factory('jpcbFactory', function($http, CurrentUser, $filter) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            boardConfigList: {},
            boardList: {},
            // argChips: {},
            // argHeaders: {},
            // argStall: {},
            createBoard: function(boardname, config){
                var defconfig = this.getDefaultConfig();
                var vboard = JsBoard.Common.extend(defconfig, config);
                vboard.board = JsBoard.Common.extend(defconfig.board, config.board);
                vboard.margin = JsBoard.Common.extend(defconfig.margin, config.margin);
                vboard.layout = JsBoard.Common.extend(defconfig.board, config.layout);
                vboard.chips = JsBoard.Common.extend(defconfig.board, config.chips);
                vboard.onGetHeader = function(varg, vFunc){
                   vFunc([{title: 'Column 1'}, {title: 'Column 2'}, {title: 'Column 3'}, {title: 'Column 4'}, {title: 'Column 5'}]);
                }
                vboard.onGetStall = function(varg, vFunc){
                   vFunc([{title: 'Stall 1'}, {title: 'Stall 2'}, {title: 'Stall 3'}]);
                }
                vboard.onGetChips = function(varg, vFunc){
                   vFunc([]);
                }
                vboard.onGetSelectionChip = function(varg){
                   return []
                }
                vboard.onGetStopageChip = function(varg){
                   return []
                }
                vboard.blockList = [];
                delete this.boardConfigList[boardname];
                this.boardConfigList[boardname] = vboard;
            },
            //minuteDelay: 15*60+0,
            // minuteDelay: JsBoard.Common.minuteDelay,
            getCurrentTime: function(){
                var vDate = new Date();
                //vDate.setHours(vDate.getHours()-this.hourDelay);
                // vDate.setMinutes(vDate.getMinutes()-this.hourDelay*60);
                // vDate.setMinutes(vDate.getMinutes()-this.minuteDelay);
                vDate.setMinutes(vDate.getMinutes()-JsBoard.Common.minuteDelay);
                return vDate;
            },
            activateSelectArea: function(config){

            },
            activateJobStopage: function(config){

            },
            setArgChips: function(boardname, varg){
                this.boardConfigList[boardname].argChips = varg;
            },
            setArgHeader: function(boardname, varg){
                this.boardConfigList[boardname].argHeader = varg;
            },
            setArgStall: function(boardname, varg){
                this.boardConfigList[boardname].argStall = varg;
            },
            getDefaultConfig: function(){
                var vresult = {
                    type: 'jpcb',
                    board:{
                        rowHeight: 80,
                        headerHeight: 40,
                        columnWidth: 120,
                        breakChipName: 'break',
                        chipGroup: 'jpcbdefault',
                        container: 'jpcb_container',
                        iconFolder: 'images/boards/jpcb',
                        footerChipName: 'footer',
                        firstColumnChipName: 'stall',
                        firstColumnBaseChipName: 'stall_base',
                        firstColumnBaseChipNameFiller: 'stall_base_filler',
                        firstHeaderChipName: 'first_header',
                        headerChipName: 'header',
                        headerType: 'simple',
                        headerFill: '#ddd',
                        footerHeight: 31,
                        firstColumnTitle: 'Stall',
                    },
                    margin: {
                        left: 5
                    },
                    layout: {
                        chipHeight: 600,
                        chipGap: 2
                    },
                    chips: {},
                    listeners: {}
                }
                return vresult;
            },
            setOnGetHeader: function(boardname, evfunction){
                this.boardConfigList[boardname].onGetHeader = evfunction;
            },
            setOnGetStall: function(boardname, evfunction){
                this.boardConfigList[boardname].onGetStall = evfunction;
            },
            setOnGetChips: function(boardname, evfunction){
                this.boardConfigList[boardname].onGetChips = evfunction;
            },
            setOnChipLoaded: function(boardname, evfunction){
                this.boardConfigList[boardname].onChipLoaded = evfunction;
            },
            setOnBoardReady: function(boardname, evfunction){
                this.boardConfigList[boardname].onBoardReady = evfunction;
            },
            setHeader: function(boardname, config){
                var vconfig = this.boardConfigList[boardname];
                // list related to header
                var vlist = ['headerType', 'headerHeight', 'verticalLineTop', 'headerChipName', 'columnWidth'];
                for (var i=0; i<vlist.length; i++){
                    var vlitem = vlist[i];
                    if (typeof config[vlitem] !== 'undefined'){
                        vconfig.board[vlitem] = config[vlitem];
                    }
                }
                if (typeof config.onGetHeader !== 'undefined'){
                    vconfig.onGetHeader = config.onGetHeader;
                }
            },
            setStall: function(boardname, config){
                var vconfig = this.boardConfigList[boardname];
                // list related to header
                var vlist = ['firstHeaderChipName', 'firstHeaderData', 'firstColumnChipName', 'firstColumnBaseChipName', 'firstColumnWidth'];
                for (var i=0; i<vlist.length; i++){
                    var vlitem = vlist[i];
                    if (typeof config[vlitem] !== 'undefined'){
                        vconfig.board[vlitem] = config[vlitem];
                    }
                }
                if (typeof config.onGetStall !== 'undefined'){
                    vconfig.onGetStall = config.onGetStall;
                }
            },
            setSelectionArea: function(boardname, config){
                //Selection area adalah wilayah di atas header tempat chip awal yang belum ditempatkan
                // tinggi area, misalnya 200
                var vTinggiArea = 200;
                if (typeof config.tinggiArea !== 'undefined'){
                    vTinggiArea = config.tinggiArea;
                }
                
                // tinggi header, misalnya 40 
                var vTinggiHeader = 40;
                if (typeof config.tinggiHeader !== 'undefined'){
                    vTinggiHeader = config.tinggiHeader;
                }
                
                // tinggi subheader, misalnya 40
                var vTinggiSubHeader = 0;
                if (typeof config.tinggiSubHeader !== 'undefined'){
                    vTinggiSubHeader = config.tinggiSubHeader;
                }
                var vconfig = this.boardConfigList[boardname];
                vconfig.board.headerHeight = vTinggiArea+vTinggiHeader+vTinggiSubHeader;
                vconfig.board.fixedHeaderHeight = vTinggiArea;
                if (vTinggiSubHeader>0){
                    vconfig.board.verticalLineTop = vTinggiArea+vTinggiHeader;
                } else {
                    vconfig.board.verticalLineTop = vTinggiArea;
                }
                
                // titleTop akan digunakan oleh chip fix_header untuk mengatur posisi title-nya
                
                vconfig.chip.titleTop = vTinggiArea;
                // subTitleTop akan digunakan oleh chip fix_header untuk mengatur posisi subtitle-nya (bila ada)
                vconfig.chip.subtitleTop = vTinggiArea+vTinggiHeader;
                // default nama chip untuk selection area adalah fix_header
                // dalam chip tersebut harus ada item dengan nama selectarea
                vconfig.board.fixedHeaderChipName = 'fix_header';
                if (typeof config.fixedHeaderChipName !== 'undefined'){
                    vconfig.board.fixedHeaderChipName = config.fixedHeaderChipName;
                }
                
            },
            setStopageArea: function(boardname, config){
                //Stopage area adalah wilayah di bawan footer tempat ditaronya chip yang berhenti (stopage)
                // tinggi area, misalnya 200
                var vTinggiArea = 200;
                if (typeof config.tinggiArea !== 'undefined'){
                    vTinggiArea = config.tinggiArea;
                }
                
                // tinggi footer, misalnya 40 
                var vTinggiFooter = 40;
                if (typeof config.tinggiFooter !== 'undefined'){
                    vTinggiFooter = config.tinggiFooter;
                }
                
                // tinggi subheader, misalnya 40
                // var vTinggiSubHeader = 0;
                // if (typeof config.tinggiSubHeader !== 'undefined'){
                //     vTinggiSubHeader = config.tinggiSubHeader;
                // }
                var vconfig = this.boardConfigList[boardname];
                vconfig.board.footerHeight = vTinggiArea+vTinggiFooter;
                // vconfig.board.fixedHeaderHeight = vTinggiArea;
                // if (vTinggiSubHeader>0){
                //     vconfig.board.verticalLineTop = vTinggiArea+vTinggiHeader;
                // } else {
                //     vconfig.board.verticalLineTop = vTinggiArea;
                // }
                
                // // titleTop akan digunakan oleh chip fix_header untuk mengatur posisi title-nya
                
                // vconfig.chip.titleTop = vTinggiArea;
                // // subTitleTop akan digunakan oleh chip fix_header untuk mengatur posisi subtitle-nya (bila ada)
                // vconfig.chip.subtitleTop = vTinggiArea+vTinggiHeader;
                // // default nama chip untuk selection area adalah fix_header
                // // dalam chip tersebut harus ada item dengan nama selectarea
                // vconfig.board.fixedHeaderChipName = 'fix_header';
                // if (typeof config.fixedHeaderChipName !== 'undefined'){
                //     vconfig.board.fixedHeaderChipName = config.fixedHeaderChipName;
                // }
                
            },
            setHeaderOnly: function(boardname, config){
                //Selection area adalah wilayah di atas header tempat chip awal yang belum ditempatkan
                // tinggi area, misalnya 200
                var vTinggiArea = 0;
                // if (typeof config.tinggiArea !== 'undefined'){
                //     vTinggiArea = config.tinggiArea;
                // }
                
                // tinggi header, misalnya 40 
                var vTinggiHeader = 40;
                if (typeof config.tinggiHeader !== 'undefined'){
                    vTinggiHeader = config.tinggiHeader;
                }
                
                // tinggi subheader, misalnya 40
                var vTinggiSubHeader = 0;
                if (typeof config.tinggiSubHeader !== 'undefined'){
                    vTinggiSubHeader = config.tinggiSubHeader;
                }
                var vconfig = this.boardConfigList[boardname];
                vconfig.board.headerHeight = vTinggiArea+vTinggiHeader+vTinggiSubHeader;
                vconfig.board.fixedHeaderHeight = vTinggiArea;
                if (vTinggiSubHeader>0){
                    vconfig.board.verticalLineTop = vTinggiArea+vTinggiHeader;
                } else {
                    vconfig.board.verticalLineTop = vTinggiArea;
                }
                
                // titleTop akan digunakan oleh chip fix_header untuk mengatur posisi title-nya
                
                vconfig.chip.titleTop = vTinggiArea;
                // subTitleTop akan digunakan oleh chip fix_header untuk mengatur posisi subtitle-nya (bila ada)
                vconfig.chip.subtitleTop = vTinggiArea+vTinggiHeader;
                // default nama chip untuk selection area adalah fix_header
                // dalam chip tersebut harus ada item dengan nama selectarea
                
                // vconfig.board.fixedHeaderChipName = 'fix_header';
                // if (typeof config.fixedHeaderChipName !== 'undefined'){
                //     vconfig.board.fixedHeaderChipName = config.fixedHeaderChipName;
                // }
                
            },
            setBlockRow: function(boardName, blockArray){
                var vconfig = this.boardConfigList[boardName];
                vconfig.blockList = blockArray;
            },
            blockRows: function(vboard, vlist){
                if (vlist){
                    for (var i=0; i<vlist.length; i++){
                        var vitem = vlist[i];
                        vboard.layout.blockRow(vitem.row, vitem.col);
                    }
                }
            },
            setListener: function(boardName, boardObj, event, vfunc){
                var vconfig = this.boardConfigList[boardName];
                if (typeof vconfig.listeners[boardObj]=='undnefined'){
                    vconfig.listeners[boardObj] = {}
                }
                vconfig.listeners[boardObj][event] = vfunc;
            },
            setCheckDragDrop: function(boardName, vfunc){
                var vconfig = this.boardConfigList[boardName];
                vconfig.board.checkDragDrop = vfunc;
            },
            setOnDragDrop: function(boardName, vfunc){
                var vconfig = this.boardConfigList[boardName];
                vconfig.board.onDragDrop = vfunc;
            },
            // onDragDrop: function(vchip){
            //     console.log('drag-drop-chip', vchip);
            //     var vcont = vchip.container;
            //     var vname = vcont.name;
            //     var vowner = vcont.owner;
            //     var vconfig = this.boardConfigList[vname];
            //     if (typeof vconfig.board.onDragDrop=='function'){
            //         vconfig.board.onDragDrop.call(vowner, vchip);
            //     }
            // },
            setOnCreateChips: function(boardName, vfunc){
                var vconfig = this.boardConfigList[boardName];
                vconfig.board.onChipCreated = vfunc;
            },
            // isStallLoaded: 1,
            // isChipLoaded: 1,
            checkLoaded: function(boardName){
                // this.dataStatus = this.dataStatus-1;
                // console.log('Check loaded', this.dataStatus);
                // if (this.dataStatus<=0){
                //     this.onDataLoaded();
                // }
                var vconfig = this.boardConfigList[boardName];
                if ((vconfig.isStallLoaded==1) && (vconfig.isChipLoaded)){
                    this.onDataLoaded();
                }
            },
            prepareData: function(boardname, onLoaded){
                this.dataStatus = 2;
                this.onDataLoaded = onLoaded;
                var me = this;
                var vconfig = this.boardConfigList[boardname];
                vconfig.isStallLoaded = 0;
                vconfig.isChipLoaded = 0;
                // load stall
                vconfig.onGetStall(vconfig.argStall, function(vdata){
                    vconfig.board.firstColumnItems = vdata;
                    //me.checkLoaded(boardname);
                    // header
                    console.log('GetStall - loaded');
                    vconfig.onGetHeader(vconfig.argHeader, function(vdata){
                        vconfig.board.headerItems = vdata;
                        console.log('GetHeader - loaded');
                        
                        vconfig.isStallLoaded = 1;
                        me.checkLoaded(boardname);
                    });
                });
                // load chip
                vconfig.onGetChips(vconfig.argChips, function(vdata){
                    vconfig.allChips = vdata;
                    console.log('GetStall - loaded');
                    vconfig.isChipLoaded = 1;
                    me.checkLoaded(boardname);
                });



            },
            showBoard: function(boardname, divid, vowner){
                var vconfig = this.boardConfigList[boardname];
                this.prepareData(boardname, function(){
                    //var vconfig = this.boardConfigList[boardname];
                    if (typeof vconfig.onChipLoaded == 'function'){
                        vconfig.onChipLoaded(vconfig);
                    }
                    vconfig.theBoard = this.doShowBoard(boardname, divid, vowner);
                    if (typeof vconfig.onBoardReady == 'function'){
                        vconfig.onBoardReady(vconfig);
                    }
                })
            },
            doShowBoard: function(boardname, divid, vowner){
                var vconfig = this.boardConfigList[boardname];

                // vconfig.board.headerItems = vconfig.onGetHeader(vconfig.argHeader);
                // vconfig.board.firstColumnItems = vconfig.onGetStall(vconfig.argStall);

                vconfig.board.container = divid;
                var vboard = new JsBoard.Container(vconfig);
                if (typeof vowner !=='undefined'){
                    vboard.owner = vowner;
                } else {
                    vboard.owner = this;
                }
                vboard.name = boardname;
                vboard.onDragDrop = this.onDragDrop;
                if (typeof vconfig.board.onDragDrop=='function'){
                    vboard.onDragDrop = vconfig.board.onDragDrop;
                }
                //var vchips = vconfig.onGetChips(vconfig.argChips);
                if (typeof vconfig.board.scrollableLeft !=='undefined'){
                    if ((typeof vboard.board !== 'undefined') && (typeof vboard.board.scrollable !== 'undefined')){
                        vboard.board.scrollable.x(vconfig.board.scrollableLeft);
                    }
                }

                vchips = vconfig.allChips;
                vboard.createChips(vchips);
                vboard.redraw();
                this.blockRows(vboard, vconfig.blockList);
                vboard.autoResizeStage();
                vboard.redraw();       
                if(typeof this.boardList[boardname] !== 'undefined'){
                    if(typeof this.boardList[boardname].stage !== 'undefined'){
                        this.boardList[boardname].stage.destroy();
                    }
                }
                delete this.boardList[boardname];
                this.boardList[boardname] = vboard;         
                return vboard;
            },
            updateTimeLine: function(boardname, currentHour, startHour, minutPerCol){
                var vconfig = this.boardConfigList[boardname];
                if (vconfig && vconfig.theBoard){
                    var vstartmin = JsBoard.Common.getMinute(startHour);
                    var vmin = JsBoard.Common.getMinute(currentHour)-vstartmin;
                    var vresult = vmin/minutPerCol;
                    vconfig.theBoard.layout.updateTimeLine(vconfig.theBoard.board, vresult);
                    //vconfig.theBoard.redraw();
                }
            },
            getScrollablePos: function(boardname){
                var vboard = this.getBoard(boardname);
                console.log('akar masalah',vboard);
                if(vboard != undefined){
                    if ((typeof vboard.board !== 'undefined') && (typeof vboard.board.scrollable !== 'undefined')){
                        console.log('ini aneh', vboard)
                        vpos = vboard.board.scrollable.x();
                        //console.log("vpos", vpos);
                        return vpos;
                    }
                    //console.log("vboard", vboard);
                }
                
                return 0;
            },
            setScrollablePos: function(boardname, xpos){
                var vboard = this.getBoard(boardname);
                if ((typeof vboard.board !== 'undefined') && (typeof vboard.board.scrollable !== 'undefined')){
                    vboard.board.scrollable.x(xpos);
                    //console.log("vpos", vpos);
                }
                //console.log("vboard", vboard);
            },
            redrawBoard: function(boardname){
                var vconfig = this.boardConfigList[boardname];
                if (vconfig.theBoard){
                    vconfig.theBoard.redraw();
                }
            },
            processChips: function(boardname, check, execute){
                var vconfig = this.boardConfigList[boardname];
                if (vconfig.theBoard){
                    vconfig.theBoard.processChips(check, execute);
                }
            },
            getBoard: function(boardname){
                var vconfig = this.boardConfigList[boardname];
                // console.log('vconfig1 dudu', vconfig)
                if (typeof vconfig!=='undefined'){
                    // console.log('vconfig2 dudu', vconfig)
                    // if (vconfig.theBoard){
                        return vconfig.theBoard;
                    // } 
                }
                return false;
            },
            isJobProblem: function(vitem, vname){
                if (typeof vname=='undefined'){
                    vname = 'Status';
                }
                var vresult = false;
                var vstatus = vitem[vname];
                var vNow = this.getCurrentTime();
                var xStart = $filter('date')(vitem.PlanDateStart, 'yyyy-MM-dd')+' '+vitem.PlanStart;
                var xFinish = $filter('date')(vitem.PlanDateFinish, 'yyyy-MM-dd')+' '+vitem.PlanFinish;
                var xNow = $filter('date')(vNow, 'yyyy-MM-dd HH:mm:ss');

                // if (vitem.nopol == 'JAJA'){
                //     console.log('BLA nopol', vitem.nopol)
                //     console.log('BLA xstart', xStart)
                //     console.log('BLA xNow', xNow)
                //     console.log('BLA xFinish', xFinish)
                // }
               
                
                // ========== ini coding lama, mau di ganti kondisi na per tgl 29 juni 2019 =============================== start
                    // // 5. Problem
                    // //    a. waktu awal < sekarang tapi status < 5 and status>=3 (belum clock on)
                    // if ((xStart<xNow) && (vstatus<5) && (vstatus>=4)){
                    //     // if (vitem.nopol == 'TESTAL'){
                    //     //     console.log('masuk atas')
                    //     // }
                    //     vresult = true;
                    // }
                    // //    b. ketika status belum done tapi jam akhir < jam sekarang
                    // // if ((xFinish<xNow) && (vstatus<17) && (vstatus>=4)){
                    // if ((xFinish<xNow) && (vstatus<12) && (vstatus>=4)){
                    //     // if (vitem.nopol == 'TESTAL'){
                    //     //     console.log('masuk bawah')
                    //     // }
                    //     vresult = true;
                    // }
                // ========== ini coding lama, mau di ganti kondisi na per tgl 29 juni 2019 =============================== end
                
                //=========== coding baru nih 29 juni 2019 ======================================================= start
                if ((xFinish<xNow) && ((vstatus >= 5 && vstatus <12) || (vstatus == 22))){
                    // if (vitem.nopol == 'JAJA'){
                    //     console.log('masuk bawah')
                    // }
                    if (vitem.TimeFinish != null && vitem.TimeFinish != undefined){
                        vresult = false;
                    } else {
                        if (vitem.JobSplitId == 0){
                            vresult = true;
                        } else {
                            if (vitem.isSplitActive != 0){
                                vresult = true;
                            }
                        }
                    }
                    
                    // vresult = true;
                }
                //=========== coding baru nih 29 juni 2019 ======================================================= end


                return vresult;

            },
            updateStandardDraggable: function(boardname){
                var vconfig = this.boardConfigList[boardname];
                var vList = ["0", "3", "4", "6"];
                if (typeof vconfig.draggableStatus !== 'undefined'){
                    vList = vconfig.draggableStatus;
                }
                if (vconfig.standardDraggable){
                    this.processChips(boardname, function(vchip){
                        return true;
                    }, function(vchip){
                        var vdrag = false;
                        // if ((typeof vchip.data !== 'undefined') && (typeof vchip.container.layout!=='undefined')){
                        //     var vlayout = vchip.container.layout;
                        //     if (vchip.data.col>=vlayout.timeLine){
                        //         vdrag = true;
                        //     }
                        // }
                        if (vchip.data.nopol == "B 7011 FF"){
                            if (vchip.data.nopol = ""){

                            }
                        }
                        if ((typeof vchip.data !== 'undefined') && (typeof vchip.data.intStatus!=='undefined')){
                            var vstatus = vchip.data.intStatus.toString();
                            if (vList.indexOf(vstatus)>=0){
                                vdrag  = true;
                            }
                        }
                        if ((typeof vchip.data !== 'undefined') && (typeof vchip.data.dontDrag!=='undefined')){
                            if (vchip.data.dontDrag==1){
                                vdrag  = false;
                            }
                        }
                        
                        vchip.draggable(vdrag);
                    });
                }
            }
                // jpcbFactory.updateStandardDraggable(boardname);
                // if (config.standardDraggable){
                //     updateDraggableStatus(vjpcb.boardName);
                // }
            
            

        }
    });




            // onGetHeaderFunc: function(){
            //     return [{title: '1'}, {title: '2'}, {title: '3'}]
            // },
            // onGetStallFunc: function(){
            //     return [{title: 'Stall 1'}, {title: 'Stall 2'}, {title: 'Stall 3'}]
            // },
            // onGetChipsFunc: function(){
            //     return []
            // },
