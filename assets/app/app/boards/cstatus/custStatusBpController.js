angular.module('app')
.controller('CustStatusBpController', function($scope, $http, CurrentUser, Parameter, ngDialog, $window, $timeout, $filter, Idle) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        Idle.unwatch();
        console.log('==== value of idle ==', Idle)
    });
    $scope.$on('$destroy', function(){
        Idle.watch();
    });
    var theChips;
    var statusBoard;
    var headerChip = false;
    var runningChip = false;
    var pageCount = 1;
    var listSelesai = [];
    var jmlPerPageSelesai = 6;
    var jmlPageSelesai = 0;

    var selectListSelesai = function(vlist, vpage){
        var vresult = "";
        for (var i=0; i<vlist.length; i++){
            if ((i>=vpage*jmlPerPageSelesai) && (i<(vpage+1)*jmlPerPageSelesai)){
                //vresult.push(vlist[i]);
                if (vresult!==""){
                    vresult = vresult+' >> ';
                }
                vresult = vresult+vlist[i];
            }
        }
        return vresult;
    }

    var processChips = function(chipList){
        var vmaxpage = 5;
        var vcount = 0;
        var vpage = 0;
        var xcount = 0;
        for(var i=0; i<chipList.length; i++){
            vchip = chipList[i];
            if (vchip.lokasi=='listitem'){
                vchip.pageIndex = vpage;
                vchip.indexByPage = vcount;
                vcount = vcount+1;
                xcount = xcount+1;
                if (vcount>=vmaxpage){
                    vpage = vpage+1;
                    vcount = 0;
                }
            }
        }
        pageCount = Math.floor(xcount/vmaxpage);
    }


    var checkStatusItem = function(vstatus){
        if (vstatus==4 || vstatus == 22){
            result = 'wrecept'; // tunggu produksi
        } else if (vstatus>=5 && vstatus<=10){
            result = 'production'; // produksi
        // } else if (vstatus>=11 && vstatus<=12){
        } else if (vstatus == 11){
            result = 'finspection'
        // } else if (vstatus>=13 && vstatus<=15){
        } else if (vstatus >= 12 && vstatus <=14){
            result = 'dokumen'
        // } else if (vstatus>=16 && vstatus<=17){
        } else if (vstatus == 15){
            result = 'siapserah'
        } else {
            result = ''; // harus nya ini yg status nya 16 aja
        }
        return result;
    }
    var loadChips = function(){
        var tglAwal = new Date();
        var tglAwal = new Date();
        // var vurl = '/api/as/Boards/BP/status/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/4/17';
        var vurl = '/api/as/Boards/BP/status/'+$filter('date')(tglAwal, 'yyyy-MM-dd')+'/4/16';
        var me = this;
        $http.get(vurl).then(function(res){
            var xresult = res.data.Result;
            console.log(xresult);
            var result = []
            var finres = []
            for (var i=0; i<xresult.length; i++){
                vitem = xresult[i];
                var vdata = {
                    chip: 'chip', lokasi: 'listitem'
                }
                if (vitem.PoliceNumber){
                    vdata['nopol'] = vitem.PoliceNumber;
                } else {
                    vdata['nopol'] = '#'+vitem.JobId;
                }
                vdata.sa_name = vitem.NameSa;
                vdata.janjiselesai = $filter('date')(vitem.JanjiSelesai, 'dd/MM/yyyy HH:mm');
                vdata.datang = JsBoard.Common.getTimeString(JsBoard.Common.getMinute(vitem.PlanStart));
                vdata.status = checkStatusItem(vitem.Status);
                if (vdata.status){
                    result.push(vdata);
                } else {
                    finres.push(vdata.nopol);
                }
            }
            listSelesai = finres;
            theChips.general[1].text = selectListSelesai(listSelesai, theChips.general[1].counter);
            jmlPageSelesai = Math.ceil(listSelesai.length/jmlPerPageSelesai);
            theChips.listitem = result;
            processChips(theChips.listitem);
            statusBoard.createChips(theChips, 'objarray'); //<--- untuk menampilkan chip
            statusBoard.redraw();              //<--- harus di-redraw
            
        });

                // $http.get('/api/as/Boards/Stall/0').then(function(res){
                //     var xres = res.data.Result;
                //     var result = [];
                //     for(var i=0;  i<xres.length; i++){
                //         var vitem = xres[i];
                //         result.push({
                //             title: vitem.Name,
                //             status: 'normal',
                //             stallId: vitem.StallId
                //         });
                //     }
                //     onFinish(result);
                // })
    }


        // $timeout(function(){
        //     var vresult = [
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 6798 RRT', status: 'siapserah', datang: '08:00', janjiselesai: '10:10', 'sa_name': 'DRIT'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 4157 YYT', status: 'wrecept', datang: '09:00', janjiselesai: '10:30', 'sa_name': 'MIRA'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 457 TYP', status: 'wrecept', datang: '09:20', janjiselesai: '10:50', 'sa_name': 'ANDI'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 6677 THH', status: 'dokumen', datang: '10:30', janjiselesai: '11:05', 'sa_name': 'ANIY'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1457 JYP', status: 'production', datang: '11:00', janjiselesai: '11:50', 'sa_name': 'DRIT'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1401 JBA', status: 'dokumen', datang: '11:00', janjiselesai: '12:00', 'sa_name': 'DONI'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1402 JBB', status: 'production', datang: '11:00', janjiselesai: '12:10', 'sa_name': 'DONA'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1403 JBC', status: 'wrecept', datang: '11:00', janjiselesai: '12:30', 'sa_name': 'DONO'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1404 JBD', status: 'production', datang: '11:00', janjiselesai: '13:00', 'sa_name': 'DANU'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1405 JBE', status: 'wrecept', datang: '11:00', janjiselesai: '13:10', 'sa_name': 'DINA'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1406 JBF', status: 'production', datang: '11:00', janjiselesai: '13:30', 'sa_name': 'DENI'},
        //     ]
        //     var xlist = []
        //     var xselesai = []
        //     for (var i=0; i<vresult.length; i++){
        //         var vitem = vresult[i];
        //         if (vitem.Status>=18){
        //             xselesai.push(vitem.nopol);
        //         } else {
        //             xlistpush(vitem);
        //         }
        //     }
        // }, 300);
    

    var showBoard = function(){
        var boardConfig = {
            type: 'status',
            board:{
                container: 'customer-status-bp',
                chipGroup: 'status', //<---- group dari chip
                iconFolder: 'images/boards/status',
                headerHeight: 70,
                onChipCreated: function(vchip){
                    if (vchip.data.chip=='header'){
                        headerChip = vchip;
                    }
                    if (vchip.data.chip=='runningtext'){
                        runningChip = vchip;
                    }
                }
            },
            stage: {
                height: 492,
                width: 960,
            },
            chip: {
                showPageIndex: 0
            }
        }

        statusBoard = new JsBoard.Container(boardConfig);
        var vDate = new Date();
        var vjam = $filter('date')(vDate, 'hh:mm');
        var vtgl = $filter('date')(vDate, 'EEE, dd/MM/yyyy');
        listSelesai = [
            // 'B 1234 ABC', 'B 8763 ABD', 'B 8777 ABE', 'B 8788 ABF', 
            // 'B 1225 ABG', 'B 8743 ABH', 'B 8737 ABI', 'B 8388 ABJ', 
            // 'B 1246 ABK', 'B 8722 ABL', 'B 8277 ABM', 'B 8488 ABN'
            ]
        jmlPageSelesai = Math.ceil(listSelesai.length/jmlPerPageSelesai);
        

        theChips = {
            general: [
                {chip: 'header', lokasi: 'header', title: 'INFORMASI STATUS SERVICE', test: 'Percobaan', atas: 20, jam: vjam, tanggal: vtgl}, //<--- definisikan chip dengan nama header
                {chip: 'subheader', lokasi: 'subheader', caption: 'SELESAI', text: selectListSelesai(listSelesai, 0), counter: 0},
                {chip: 'runningtext', lokasi: 'runningtext', text: 'Selamat datang di bengkel resmi Toyota. Silahkan melakukan pendaftaran di bagian registrasi.', counter: 0, deltaJarak: 20},
                {chip: 'listbg', lokasi: 'listbg'},
                {chip: 'footer', lokasi: 'footer', items: ['TUNGGU PRODUKSI', 'PRODUKSI', 'FINAL INSPEKSI', 'DOKUMEN', 'SIAP DISERAHKAN']},
            ],
            listitem: [
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 6798 RRT', status: 'siapserah', datang: '08:00', janjiselesai: '10:10', 'sa_name': 'DRIT'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 4157 YYT', status: 'wrecept', datang: '09:00', janjiselesai: '10:30', 'sa_name': 'MIRA'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 457 TYP', status: 'wrecept', datang: '09:20', janjiselesai: '10:50', 'sa_name': 'ANDI'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 6677 THH', status: 'dokumen', datang: '10:30', janjiselesai: '11:05', 'sa_name': 'ANIY'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1457 JYP', status: 'production', datang: '11:00', janjiselesai: '11:50', 'sa_name': 'DRIT'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1401 JBA', status: 'dokumen', datang: '11:00', janjiselesai: '12:00', 'sa_name': 'DONI'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1402 JBB', status: 'production', datang: '11:00', janjiselesai: '12:10', 'sa_name': 'DONA'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1403 JBC', status: 'wrecept', datang: '11:00', janjiselesai: '12:30', 'sa_name': 'DONO'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1404 JBD', status: 'production', datang: '11:00', janjiselesai: '13:00', 'sa_name': 'DANU'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1405 JBE', status: 'wrecept', datang: '11:00', janjiselesai: '13:10', 'sa_name': 'DINA'},
                // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1406 JBF', status: 'production', datang: '11:00', janjiselesai: '13:30', 'sa_name': 'DENI'},
            ]
        }

        // theChips = [
        //     {chip: 'header', lokasi: 'header', title: 'INFORMASI STATUS SERVIS', test: 'Percobaan', atas: 20, jam: vjam, tanggal: vtgl}, //<--- definisikan chip dengan nama header
        //         {chip: 'subheader', lokasi: 'subheader', caption: 'SELESAI', text: selectListSelesai(listSelesai, 0), counter: 1},
        //         {chip: 'runningtext', lokasi: 'runningtext', text: 'Selamat datang di bengkel resmi Toyota. Silahkan melakukan pendaftaran di bagian registrasi.', counter: 0, deltaJarak: 20},
        //         {chip: 'listbg', lokasi: 'listbg'},
        //         {chip: 'footer', lokasi: 'footer', items: ['TUNGGU PRODUKSI', 'PRODUKSI', 'FINAL INSPEKSI', 'DOKUMEN', 'SIAP DISERAHKAN']},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 6798 RRT', status: 'siapserah', datang: '08:00', janjiselesai: '10:10', 'sa_name': 'DRIT'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 4157 YYT', status: 'wrecept', datang: '09:00', janjiselesai: '10:30', 'sa_name': 'MIRA'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 457 TYP', status: 'wrecept', datang: '09:20', janjiselesai: '10:50', 'sa_name': 'ANDI'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 6677 THH', status: 'dokumen', datang: '10:30', janjiselesai: '11:05', 'sa_name': 'ANIY'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 1457 JYP', status: 'production', datang: '11:00', janjiselesai: '11:50', 'sa_name': 'DRIT'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 1401 JBA', status: 'dokumen', datang: '11:00', janjiselesai: '12:00', 'sa_name': 'DONI'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 1402 JBB', status: 'production', datang: '11:00', janjiselesai: '12:10', 'sa_name': 'DONA'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 1403 JBC', status: 'wrecept', datang: '11:00', janjiselesai: '12:30', 'sa_name': 'DONO'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 1404 JBD', status: 'production', datang: '11:00', janjiselesai: '13:00', 'sa_name': 'DANU'},
        //         {chip: 'chip', lokasi: 'listitem', nopol: 'B 1405 JBE', status: 'wrecept', datang: '11:00', janjiselesai: '13:10', 'sa_name': 'DINA'},
        //         // {chip: 'chip', lokasi: 'listitem', nopol: 'B 1406 JBF', status: 'production', datang: '11:00', janjiselesai: '13:30', 'sa_name': 'DENI'},
        // ]

        processChips(theChips.listitem);
        statusBoard.createChips(theChips, 'objarray'); //<--- untuk menampilkan chip
            // runningChip = statusBoard.Chips[2];
            // headerChip = statusBoard.Chips[0];
        statusBoard.redraw();              //<--- harus di-redraw

    }

    var updateVideoPosition = function(){
        var vBaseTop = 40;
        var vRelativeTop = statusBoard.layout.board.headerHeight+statusBoard.layout.board.subHeaderHeight+2;
        var vScale = statusBoard.stage.scale();
        var vTop = vBaseTop+vRelativeTop*vScale.x;
        // var vTop = vBaseTop;
        var vLeft = 8;
        var vWidth = 718*vScale.x;
        var vHeight = 328*vScale.x;
        console.log("vTop", vTop);
        var vobj = document.getElementById('video-customer-status-bp');
        if (vobj){
            vobj.style.cssText = "display: block; top: "+vTop+"px; "+"left: "+vLeft+"px; "+"width: "+vWidth+"px; "+"height: "+vHeight+"px; ";
        }
        var vobj = document.getElementById('video-player-customer-status-bp');
        if (vobj){
            vobj.style.cssText = "width: "+vWidth+"px; "+"height: "+vHeight+"px; ";
        }
    }


    var playSelectedFile = function (event) {
        var file = this.files[0]
        var type = file.type
        //var videoNode = document.querySelector('video')
        var videoNode = document.getElementById('video-player-customer-status-bp');
        
        var canPlay = videoNode.canPlayType(type)
        if (canPlay === '') canPlay = 'no'
        var message = 'Can play type "' + type + '": ' + canPlay
        var isError = canPlay === 'no'
        //displayMessage(message, isError)
        //alert(message);

        if (isError) {
            alert(message);
        return
        }

        var fileURL = URL.createObjectURL(file)
        videoNode.src = fileURL
    }    

    var inputNode = document.getElementById('btn-video-customer-status-bp');
    inputNode.addEventListener('change', playSelectedFile, false)

    // $scope.$on('$viewContentLoaded', function() {

        $scope.loading=true;
        $scope.gridData=[];

        $timeout(showBoard, 10);

            // var vjarak = 20;
            // var onTimer = function(){
            //     vDate = new Date();
            //     // theChips[0].jam = $filter('date')(vDate, 'hh:mm:ss');
            //     // theChips[2].counter = theChips[2].counter+1;
            //     // statusBoard.redrawChips();
            //     headerChip.data['jam'] = $filter('date')(vDate, 'hh:mm:ss');
            //     runningChip.data['counter'] = runningChip.data['counter'] +1;
            //     var xchip = runningChip.shapes.text;
            //     var vwidth = xchip.width();
            //     var vleft = xchip.x();
            //     var vleft = vleft-vjarak;
            //     if (vleft<-vwidth){
            //         vleft = vwidth+vjarak;
            //     }
            //     xchip.x(vleft);
            //     statusBoard.updateChip(headerChip);
            //     //statusBoard.updateChip(runningChip);
            //     statusBoard.redraw();
            // }

            var updateAll = function(){
                vDate = new Date();
                // headerChip.data['jam'] = $filter('date')(vDate, 'hh:mm');
                theChips.general[0].jam = $filter('date')(vDate, 'hh:mm');
                theChips.general[0].tanggal = $filter('date')(vDate, 'EEE, dd/MM/yyyy');
                theChips.general[1].text = selectListSelesai(listSelesai, theChips.general[1].counter);
                theChips.general[1].counter = theChips.general[1].counter+1;
                if (theChips.general[1].counter>=jmlPageSelesai){
                    theChips.general[1].counter = 0;
                }
                
                // statusBoard.config.chip.showPageIndex = (statusBoard.config.chip.showPageIndex+1) % pageCount;
                if (pageCount > 0){
                    statusBoard.config.chip.showPageIndex = (statusBoard.config.chip.showPageIndex + 1) % pageCount;
                } else {
                    statusBoard.config.chip.showPageIndex = (statusBoard.config.chip.showPageIndex);
                }
                statusBoard.redrawChips();
                
            }

            var updateRunningText = function(){
                var vjarak = theChips.general[2].deltaJarak;
                theChips.general[2].counter = theChips.general[2].counter+1;
                var xchip = runningChip.shapes.text;
                var vwidth = xchip.width();

                // var vleft = xchip.x();
                // vleft = vleft-vjarak;

                var vleft = vwidth-(theChips.general[2].counter*vjarak)+vjarak;
                if (vleft<-vwidth){
                    vleft = vwidth+vjarak;
                    theChips.general[2].counter = 0;
                }
                theChips.general[2].left = vleft;
                xchip.x(vleft);
                statusBoard.redraw();
            }

            var vinterval_short = 500;
            var vinterval_long = 10000;

            var shortTick = function(){
                updateRunningText();
                $timeout(shortTick, vinterval_short);
            }
            $timeout(shortTick, vinterval_short);


            var longTick = function(){
                updateAll();
                $timeout(longTick, vinterval_long);
            }
            $timeout(longTick, vinterval_long);



            // $scope.tickInterval = 500;

            // var tick = function () {
            //     $scope.clock = Date.now() // get the current time
            //     onTimer();
            //     $timeout(tick, $scope.tickInterval); // reset the timer
            // }

            // // Start the timer
            // $timeout(tick, $scope.tickInterval);

            // var reloadInterval = 90000;
            var reloadInterval = 100000;
            var reloadTick = function(){
                loadChips();
                $timeout(reloadTick, reloadInterval);
            }
            $timeout(reloadTick, reloadInterval);
            



    // });

    // $scope.$watch(function () {
    //     var vobj = document.getElementById('base-customer-status-gr');
    //     if(vobj){
    //         return vobj.clientWidth;
    //     }
    // }, function(newVal, oldVal) {
    //         //console.log('Width changed');
    //     console.log("update screen size");
    //     updateScreenSize();
    // });

    // var updateScreenSize = function(){
    //     if (statusBoard){
    //         var vobj = document.getElementById('base-customer-status-gr');
    //         if(vobj){
    //             var screenWidth = vobj.clientWidth;
    //             var vwidth = statusBoard.stage.width;
    //             var vscale = screenWidth/vscale;
    //             statusBoard.stage.scale(vscale);
    //         }            
    //     }
    // }

    // $timeout(function(){
    //     var vobj = document.getElementById('cust-status-gr-main-panel');
    //     if(vobj){
    //         vobj.style.cssText = "width: 100%; height: 100%; position: absolute; top: 0; left: 0;";
    //     }
    // }, 5000);


    $timeout(function(){
        loadChips();
    }, 1000)


    

    if (typeof JsBoard.Common.statusResize ==='undefined'){
        JsBoard.Common.statusResize = true;
    }

    $('.testing-div').each(function(x){
        console.log("div", this);
    })

    var velement = document.getElementById('base-customer-status-bp');

    var erd = elementResizeDetectorMaker();
    erd.listenTo(velement, function(element) {
        if(statusBoard){
            statusBoard.autoResizeStage();
            updateVideoPosition();
        }
        var width = element.offsetWidth;
        var height = element.offsetHeight;
        console.log("Size: " + width + "x" + height);
    });
    
});
