angular.module('app')
.controller('IrregularityBpController', function($scope, $http, CurrentUser, Parameter,$timeout, $window, $interval, $filter, Idle) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.showBoard = 'atas';
      // $scope.waktuRefresh = 25000;
      $scope.LoadAllData();
      Idle.unwatch();
      console.log('==== value of idle ==', Idle)
    });
    $scope.$on('$destroy', function(){
      Idle.watch();
    });



    var arrBoard = []
    // var allBoardHeight = 215;
    var allBoardHeight = (215*2) + 50;
    $scope.loading=true;
    $scope.gridData=[];
    $scope.waktu_sekarang = 'xxxx';

    $scope.count = {}
    $scope.clock = "loading clock..."; // initialise the time variable
    $scope.tickInterval = 1000 //ms
    $scope.hourminute = '';
    var firstTime = true;

    var registerBoard = function(bConfig){
      var vboard = new JsBoard.Container(bConfig.boardConfig);
      vboard.createChips(bConfig.boardData);
      vboard.autoResizeStage();
      vboard.redraw();
      var vitem = {
        board: vboard,
        incField: bConfig.incField,
        incMode: bConfig.incMode, //inc or dec
      }
      arrBoard.push(vitem);
      $scope.count[bConfig.boardName] = bConfig.boardData.length;
    }

    // angular.element($window).bind('resize', function () {
    //     console.log("on resize", $window.innerWidth);
    //     for (var i=0; i<arrBoard.length; i++){
    //       vbr = arrBoard[i];
    //       vbr.board.autoResizeStage();
    //       vbr.board.redraw();
    //       //vbr.redrawChips();
    //     }


    // });

    var showData = 2;

    var MulaiDataINSURANCE= 0;
    var BagiBoardINSURANCE = 0;
    var CountBoardINSURANCE = 0;
    var ModulusINSURANCE = 0;
    var dataHabisINSURANCE = true;

    var MulaiDataRECEPTION= 0;
    var BagiBoardRECEPTION = 0;
    var CountBoardRECEPTION = 0;
    var ModulusRECEPTION = 0;
    var dataHabisRECEPTION = true;

    var MulaiDataDISPATCH= 0;
    var BagiBoardDISPATCH = 0;
    var CountBoardDISPATCH = 0;
    var ModulusDISPATCH = 0;
    var dataHabisDISPATCH = true;

    var MulaiDataPROD= 0;
    var BagiBoardPROD = 0;
    var CountBoardPROD = 0;
    var ModulusPROD = 0;
    var dataHabisPROD = true;

    var MulaiDataFI = 0;
    var BagiBoardFI = 0;
    var CountBoardFI = 0;
    var ModulusFI = 0;
    var dataHabisFI = true;

    var MulaiDataTECO= 0;
    var BagiBoardTECO = 0;
    var CountBoardTECO = 0;
    var ModulusTECO = 0;
    var dataHabisTECO = true;

    var MulaiDataNOTIF= 0;
    var BagiBoardNOTIF = 0;
    var CountBoardNOTIF = 0;
    var ModulusNOTIF = 0;  
    var dataHabisNOTIF = true;

    var MulaiDataDELIVERY= 0;
    var BagiBoardDELIVERY = 0;
    var CountBoardDELIVERY = 0;
    var ModulusDELIVERY = 0;
    var dataHabisDELIVERY = true;
    

    $scope.LoadAllData = function(){
      arrBoard = [];

      arrINSURANCE = [];
      arrRECEPTION = [];
      arrDISPATCH = [];
      arrPROD = [];

      arrFI = [];
      arrTECO = [];
      arrNOTIF = [];
      arrDELIVERY = [];



      var dayDifference = function(date1, date2){
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        return diffDays;
      }
  
      var updateAutoResize = function(){
          for (var i=0; i<arrBoard.length; i++){
            vbr = arrBoard[i];
            vbr.board.autoResizeStage();
            vbr.board.redraw();
            //vbr.redrawChips();
          }
      }
  
      var velement = document.getElementById('irregular-bp-main-panel');
      var erd = elementResizeDetectorMaker();
      erd.listenTo(velement, function(element) {
        updateAutoResize();
      });
  
      var incrementTime = function(varrdata, vfield){
        for(var i=0; i<varrdata.length; i++){
          var vdata = varrdata[i];
          var vmin = JsBoard.Common.getMinute(vdata[vfield]);
          var vinc = 1;
          var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
          vdata[vfield] = vsmin;
        }
      }
  
      var decrementTime = function(varrdata, vfield){
        for(var i=0; i<varrdata.length; i++){
          var vdata = varrdata[i];
          var vmin = JsBoard.Common.getMinute(vdata[vfield]);
          var vinc = -1;
          var vsmin = JsBoard.Common.getTimeString(vmin+vinc);
          vdata[vfield] = vsmin;
        }
      }
  
      var simulateInsuranceDate = function(vdata){
        for (var i=0; i<vdata.length; i++){
          var today = new Date();
          var xdate1 = new Date();
          xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
          var xdate2 = new Date();
          xdate2.setDate(xdate2.getDate()+vdata[i].daydif+2);
          vdata[i].tgl_kirim = $filter('date')(xdate1, 'dd/MM/yyyy');
          vdata[i].tgl_target_notif = $filter('date')(xdate2, 'dd/MM/yyyy');
          var vdays = Math.round((today-xdate2)/(24*60*60*1000));
          var xdays;
          var xdays = 'TODAY';
          if (vdays>0){
            xdays = 'H+'+vdays;
          } else if (vdays<0){
            xdays = 'H'+vdays;
          }
          vdata[i].daywait = xdays;
          vdata[i].dday = vdays;
  
        }
        console.log('**** vdata ****', vdata);
      }
      
      var simulateDeliveryDate = function(vdata){
        for (var i=0; i<vdata.length; i++){
          var today = new Date();
          var xdate1 = new Date();
          xdate1.setDate(xdate1.getDate()+vdata[i].daydif);
          var xdate2 = new Date();
          xdate2.setDate(xdate2.getDate()+vdata[i].daydif+3);
          
          var vtime = vdata[i].start;
          if(vdata[i].daydif==0){
            var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
            vtime = $filter('date')(today-(vmin*60*1000), 'HH:mm');
          }
          vdata[i].tgl_notif = $filter('date')(xdate1, 'dd/MM/yyyy')+' '+vtime;
          vdata[i].tgl_janji_serah = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vdata[i].start;
          var vdays = Math.round((today-xdate1)/(24*60*60*1000));
          var xdays;
          var xdays = 'TODAY';
          if (vdays>0){
            xdays = 'H+'+vdays;
          } else if (vdays<0){
            xdays = 'H'+vdays;
          }
          vdata[i].daywait = xdays;
          vdata[i].dday = vdays;
  
        }
        console.log('**** vdata ****', vdata);
      }
      
      var simulateReceptionDate = function(vdata){
        for (var i=0; i<vdata.length; i++){
          var today = new Date();
          var xdate1 = new Date();
          //var vjam = $filter('date')(today, 'hh:mm');
          var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
          var vWaktu = $filter('date')(today-(vmin*60*1000), 'HH:mm');
  
          vdata[i].tgl_masuk = $filter('date')(today, 'dd/MM/yyyy')+' '+vWaktu;
  
        }
        console.log('**** vdata ****', vdata);
      }
      
      // var simulateDispatchDate = function(vdata){
      //   for (var i=0; i<vdata.length; i++){
      //     var today = new Date();
      //     var xdate1 = new Date();
      //     //var vjam = $filter('date')(today, 'hh:mm');
      //     var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
      //     var vWaktu = $filter('date')(today-(vmin*60*1000), 'HH:mm');
  
      //     //vdata[i].workrelease = $filter('date')(today, 'dd/MM/yyyy')+' '+vWaktu;
      //     //vdata[i].janjiserah = $filter('date')(today-(vmin*60*1000)+(3*24*60*60*1000), 'dd/MM/yyyy HH:mm');
  
      //     // workrelease: '12/Jan/2017 07:05', janjiserah
  
      //   }
      //   console.log('**** vdata ****', vdata);
      // }
      
      // var simulateProductionDate = function(vdata){
      //   for (var i=0; i<vdata.length; i++){
      //     var today = new Date();
      //     var xdate1 = new Date();
      //     //var vjam = $filter('date')(today, 'hh:mm');
      //     var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
      //     var vWaktu = $filter('date')(today-(vmin*60*1000), 'HH:mm');
  
      //     // vdata[i].workrelease = $filter('date')(today, 'dd/MM/yyyy')+' '+vWaktu;
      //     // vdata[i].janjiserah = $filter('date')(today-(vmin*60*1000)+(3*24*60*60*1000), 'dd/MM/yyyy HH:mm');
  
      //     // workrelease: '12/Jan/2017 07:05', workstart: '12/Jan/2017 08:03',
      //   }
      //   console.log('**** vdata ****', vdata);
      // }
      
      // var simulateInspectionDate = function(vdata){
      //   for (var i=0; i<vdata.length; i++){
      //     var today = new Date();
      //     var xdate1 = new Date();
      //     //var vjam = $filter('date')(today, 'hh:mm');
      //     var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
      //     var vWaktu = $filter('date')(today-(vmin*60*1000), 'HH:mm');
  
      //     // vdata[i].clockofftime = $filter('date')(today, 'dd/MM/yyyy')+' '+vWaktu;
      //     // vdata[i].janjiserah = $filter('date')(today-(vmin*60*1000)+(2*24*60*60*1000), 'dd/MM/yyyy HH:mm');
  
      //     // workrelease: '12/Jan/2017 07:05', workstart: '12/Jan/2017 08:03',
      //   }
      //   console.log('**** vdata ****', vdata);
      // }
      
      var simulateTecoDate = function(vdata){
        for (var i=0; i<vdata.length; i++){
          var today = new Date();
          var xdate1 = new Date();
          //var vjam = $filter('date')(today, 'hh:mm');
          var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
          var vWaktu = $filter('date')(today-(vmin*60*1000), 'HH:mm');
  
          vdata[i].fi_finish = $filter('date')(today, 'dd/MM/yyyy')+' '+vWaktu;
          vdata[i].janjiserah = $filter('date')(today-(vmin*60*1000)+(1*24*60*60*1000), 'dd/MM/yyyy HH:mm');
  
          // workrelease: '12/Jan/2017 07:05', workstart: '12/Jan/2017 08:03',
        }
        console.log('**** vdata ****', vdata);
      }
      
      var simulateNotifDate = function(vdata){
        for (var i=0; i<vdata.length; i++){
          var today = new Date();
          var xdate1 = new Date();
          //var vjam = $filter('date')(today, 'hh:mm');
          var vmin = JsBoard.Common.getMinute(vdata[i].timewait);
          var vWaktu = $filter('date')(today-(vmin*60*1000), 'HH:mm');
  
          vdata[i].teco_finish = $filter('date')(today, 'dd/MM/yyyy')+' '+vWaktu;
          vdata[i].janjiserah = $filter('date')(today-(vmin*60*1000)+(1*24*60*60*1000), 'dd/MM/yyyy HH:mm');
  
          // workrelease: '12/Jan/2017 07:05', workstart: '12/Jan/2017 08:03',
        }
        console.log('**** vdata ****', vdata);
      }
      
  
  
      var onTimer = function(){
          var vhmin= $filter('date')($scope.clock, 'hh:mm');
          if ($scope.hourminute!==vhmin){
            $scope.hourminute = vhmin;
            if (!firstTime){
              for(var i=0; i<arrBoard.length; i++){
                vitem = arrBoard[i];
                if (vitem.incMode=='inc'){
                  incrementTime(vitem.board.chipItems, vitem.incField);
                } else if (vitem.incMode=='dec'){
                  decrementTime(vitem.board.chipItems, vitem.incField);
                }
                vitem.board.redrawChips();
              }
            } else {
              firstTime = false;
            }
          }
      }
  
      var tick = function () {
          $scope.clock = Date.now() // get the current time
          onTimer();
          $timeout(tick, $scope.tickInterval); // reset the timer
      }
  
      // Start the timer
      $timeout(tick, $scope.tickInterval);
  
      if ($scope.showBoard == 'atas'){
        // Waiting insurance approval
        var FuncWaitingInsuranceApproval = function(data){
    
          //------------------------------------------//
          // **** waiting for insurance approval **** //
          //------------------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            board:{
              container: 'irgbp-waiting_ins_approval',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'irregular_bp',
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }
    
          }
    
          if (typeof data == 'undefined'){
            data = [];
          }
          boardData = data;
          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: '',
            boardName: 'waiting_ins_approval'
          })
        }
    
        $timeout(function() {
          // var boardData = [
          //   {chip: 'waiting_ins_approval', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 666 PLN', start: '08:00', type: 'AVANZA', 
          //     tgl_kirim: '09/Jan/2017', tgl_target_notif: '11/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3
          //   },
          //   {chip: 'waiting_ins_approval', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 PLN', start: '08:00', type: 'AVANZA', 
          //     tgl_kirim: '10/Jan/2017', tgl_target_notif: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2
          //   },
          //   {chip: 'waiting_ins_approval', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 777 PLN', start: '08:30', type: 'AVANZA', 
          //     tgl_kirim: '12/Jan/2017', tgl_target_notif: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -1
          //   },
          //   {chip: 'waiting_ins_approval', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8718 PLN', start: '09:00', type: 'AVANZA', 
          //     tgl_kirim: '12/Jan/2017', tgl_target_notif: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: 0
          //   },
          //   {chip: 'waiting_ins_approval', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9819 PLN', start: '09:00', type: 'AVANZA', 
          //     tgl_kirim: '12/Jan/2017', tgl_target_notif: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0
          //   },
          // ]
          var boardData = []
          simulateInsuranceDate(boardData);
          $timeout(function() {
            FuncWaitingInsuranceApproval(boardData);
          }, 100);
    
        }, 100);
    
    
    
    
        var FuncWaitingForReception = function(data){
          //---------------------------------//
          // **** waiting for reception **** //
          //---------------------------------//
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 110,
              width: 320,
              gap: 2
            },
            board:{
              container: 'irgbp-waiting_reception',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'irregular_bp',
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }
    
          }
    
          if (typeof data == 'undefined'){
            data = [];
          }
          boardData = data;
          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: 'inc',
            boardName: 'waiting_reception'
          })
    
        }
    
        var getMinToday = function(vdate){
          var vtoday = new Date();
          var theDay = new Date(vdate);
          var vDayDiff = vtoday.getDate()-theDay.getDate();
          var vDayMin = $filter('date')(theDay, 'HH:mm');
          var vNowMin = $filter('date')(vtoday, 'HH:mm');
          vminute = JsBoard.Common.getMinute(vNowMin)-JsBoard.Common.getMinute(vDayMin);
          if (vDayDiff>0){
            vminute = vminute+vDayDiff*24*60;
          }
          return vminute;
    
        }
    
        var getMinToday2 = function(vdate){
          var vtoday = new Date();
          var theDay = new Date(vdate);
          var vDayDiff = vtoday.getDate()-theDay.getDate();
          var vDayMin = $filter('date')(theDay, 'HH:mm');
          var vNowMin = $filter('date')(vtoday, 'HH:mm');
          vminute = JsBoard.Common.getMinute(vNowMin)-JsBoard.Common.getMinute(vDayMin);
          if (vDayDiff){
            vminute = vminute+vDayDiff*24*60;
          }
          return vminute;
    
        }
    
        var getDayToday = function(vdate){
          var vtoday = new Date();
          var theDay = new Date(vdate);
          var vDayDiff = vtoday.getDate()-theDay.getDate();
          return vDayDiff;
        }
    
        
    
        var vurlwfr = '/api/as/Boards/BP/irregular/wfr';
        $http.get(vurlwfr).then(function(res){
          var boardData = [
            // {chip: 'waiting_reception', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 666 NB', start: '08:00', type: 'AVANZA', 
            //   tgl_masuk: '12/Jan/2017 07:05', sa_name: 'MAN', 
            //   pekerjaan: 'EM', teknisi: 'AND', timewait: '00:30', statusicon: ['diagnose'], category: 'EM'},
            // {chip: 'waiting_reception', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 MNO', start: '08:00', type: 'AVANZA', 
            //   tgl_masuk: '12/Jan/2017 07:05', sa_name: 'ANDI', 
            //   pekerjaan: 'EM', teknisi: 'BUD', statusicon: ['reception'], timewait: '00:28', category: 'B'},
            // {chip: 'waiting_reception', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 777 AA', start: '08:30', type: 'AVANZA', 
            //   tgl_masuk: '12/Jan/2017 07:05', sa_name: 'SINTA', 
            //   pekerjaan: 'EM', teknisi: 'BUD', statusicon: ['delivery'], timewait: '00:12', category: 'W'},
            // {chip: 'waiting_reception', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8718 CD', start: '09:00', type: 'AVANZA', 
            //   tgl_masuk: '12/Jan/2017 07:05', sa_name: 'JONO', 
            //   pekerjaan: 'GR', teknisi: 'BUD', icons: ['watch', 'arrow1'], timewait: '00:05', category: 'O'},
          ]
          // var boardData = res.data.Result;
          var vres = res.data.Result;
          $scope.jumlahWRECEPTION = res.data.Result.length;
          for(var i=0; i<vres.length; i++){
            var vresitem = vres[i];
            var vminute = getMinToday(vresitem.TimeStart);
            var vtimewait = JsBoard.Common.getTimeString(vminute);
            if (vresitem.isAppointment){
              vcategory = 'B';
            } else {
              vcategory = 'W';
            }
          
            var vitem = {chip: 'waiting_reception', nopol: vresitem.PoliceNumber, 
              start: '08:00', type: vresitem.ModelType, 
              tgl_masuk: $filter('date')(vresitem.TimeStart, 'dd/MM/yyyy HH:mm'), 
              sa_name: vresitem.Initial, 
              timewait: vtimewait, 
              statusicon: [], category: vcategory
            }
            if (vresitem.isPrediagnose){
              vitem.statusicon.push('diagnose');
            }
            // boardData.push(vitem);
            arrRECEPTION.push(vitem);
          }

          if (dataHabisRECEPTION == true)
          {
              CountBoardRECEPTION = arrRECEPTION.length;
              ModulusRECEPTION = parseInt(CountBoardRECEPTION % showData);
              BagiBoardRECEPTION = parseInt(CountBoardRECEPTION / showData);
              if (ModulusRECEPTION != 0)
              {
                  BagiBoardRECEPTION = BagiBoardRECEPTION + 1;
              }
          }

          for(var i= MulaiDataRECEPTION;  i< arrRECEPTION.length; i++){
            var vboard = arrRECEPTION[i];
            boardData.push(vboard);
        }

        BagiBoardRECEPTION = BagiBoardRECEPTION - 1;

        if (BagiBoardRECEPTION != 0)
          {
              dataHabisRECEPTION = false;
              MulaiDataRECEPTION = MulaiDataRECEPTION + showData;
          }
          else
          {
              MulaiDataRECEPTION = 0;
              BagiBoardRECEPTION = 0;
              dataHabisRECEPTION = true;
          }

          //simulateReceptionDate(boardData);
          $timeout(function() {
            FuncWaitingForReception(boardData);
          }, 100);
    
        });
    
        
    
        var FuncWaitingForDispatch = function(data){
          //---------------------------------//
          // **** waiting for dispatch **** //
          //---------------------------------//
          var boardConfig = {
            type: 'kanban',
            board:{
              container: 'irgbp-waiting_dispatch',
              chipGroup: 'irregular_bp',
              iconFolder: 'images/boards/iregular'
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            layout: {
            }
    
          }
    
          if (typeof data == 'undefined'){
            data = [];
          }
          boardData = data;
          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: 'inc',
            boardName: 'waiting_dispatch'
          })          
        }
    
        var vurlwfd = '/api/as/Boards/BP/irregular/wfd';
        $http.get(vurlwfd).then(function(res){
          var boardData = [
            // {chip: 'waiting_dispatch', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 1234 NB', start: '08:00', type: 'AVANZA', 
            //   wotype: 'TPS', sa_name: 'ANDI', workrelease: '12/Jan/2017 07:05', janjiserah: '12/Jan/2017 08:03', timewait: '00:20', statusicon: [], category: 'EM'},
            // {chip: 'waiting_dispatch', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 2345 MNO', start: '08:00', type: 'AVANZA', 
            //   wotype: 'TPS', sa_name: 'BUDI', statusicon: ['assignment'], workrelease: '12/Jan/2017 07:13', janjiserah: '12/Jan/2017 08:15', timewait: '00:15', category: 'B'},
            // {chip: 'waiting_dispatch', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 777 AA', start: '08:30', type: 'AVANZA', 
            //   wotype: 'Heavy', sa_name: 'ANI', icons: ['assignment'], workrelease: '12/Jan/2017 07:35', janjiserah: '12/Jan/2017 08:07', timewait: '00:05', category: 'W'},
            // {chip: 'waiting_dispatch', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3231 CD', start: '09:00', type: 'AVANZA', 
            //   wotype: 'Medium', sa_name: 'SINTA', icons: ['watch', 'arrow1'], workrelease: '12/Jan/2017 07:45', janjiserah: '12/Jan/2017 08:45', timewait: '00:04', category: 'O'},
            // {chip: 'waiting_dispatch', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 2423 CD', start: '09:00', type: 'AVANZA', 
            //   wotype: 'Light', sa_name: 'BUDI', icons: ['watch', 'arrow1'], workrelease: '12/Jan/2017 08:02', janjiserah: '12/Jan/2017 09:00', timewait: '00:02', category: 'O'},
          ]
          // var boardData = []
          var vres = res.data.Result;
          $scope.jumlahWDISPATCH = res.data.Result.length;
          for(var i=0; i<vres.length; i++){
            var vresitem = vres[i];
            var vminute = getMinToday(vresitem.TimeStart);
            var vtimewait = JsBoard.Common.getTimeString(vminute);
            //vdata[i].workrelease = $filter('date')(today, 'dd/MM/yyyy')+' '+vWaktu;
            //vdata[i].janjiserah = $filter('date')(today-(vmin*60*1000)+(3*24*60*60*1000), 'dd/MM/yyyy HH:mm');
    
            var vitem = {chip: 'waiting_dispatch', nopol: vresitem.PoliceNumber, 
              start: '08:00', type: vresitem.ModelType, 
              workrelease: $filter('date')(vresitem.TimeStart, 'dd/MM/yyyy HH:mm'), 
              janjiserah: $filter('date')(vresitem.JanjiSerah, 'dd/MM/yyyy HH:mm'), 
              wotype: vresitem.BpCategory,
              sa_name: vresitem.Initial, 
              timewait: vtimewait, 
              statusicon: [], category: ''
            }
            if (vresitem.isPrediagnose){
              vitem.statusicon.push('diagnose');
            }
            // boardData.push(vitem);
            arrDISPATCH.push(vitem);
          }

          if (dataHabisDISPATCH == true)
            {
                CountBoardDISPATCH = arrDISPATCH.length;
                ModulusDISPATCH = parseInt(CountBoardDISPATCH % showData);
                BagiBoardDISPATCH = parseInt(CountBoardDISPATCH / showData);
                if (ModulusDISPATCH != 0)
                {
                    BagiBoardDISPATCH = BagiBoardDISPATCH + 1;
                }
            }

            for(var i= MulaiDataDISPATCH;  i< arrDISPATCH.length; i++){
              var vboard = arrDISPATCH[i];
              boardData.push(vboard);
          }

          BagiBoardDISPATCH = BagiBoardDISPATCH - 1;

          if (BagiBoardDISPATCH != 0)
            {
                dataHabisDISPATCH = false;
                MulaiDataDISPATCH = MulaiDataDISPATCH + showData;
            }
            else
            {
                MulaiDataDISPATCH = 0;
                BagiBoardDISPATCH = 0;
                dataHabisDISPATCH = true;
            }
          
          // simulateDispatchDate(boardData);
          $timeout(function() {
            FuncWaitingForDispatch(boardData);
          }, 100);
    
        }, 100);
        
    
        var FuncWaitingForProduction = function(data){
          //----------------------------------//
          // **** waiting for production **** //
          //----------------------------------//
          var boardConfig = {
            type: 'kanban',
            board:{
              container: 'irgbp-waiting_production',
              chipGroup: 'irregular_bp',
              iconFolder: 'images/boards/iregular'
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            layout: {
            }
    
          }
    
          if (typeof data == 'undefined'){
            data = [];
          }
          boardData = data;
          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: 'inc',
            boardName: 'waiting_production'
          })
    
    
        }
    
    
        var vurlwfd = '/api/as/Boards/BP/irregular/wfp';
        $http.get(vurlwfd).then(function(res){
          var boardData = [
            // {chip: 'waiting_production', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 1234 NB', start: '08:00', type: 'AVANZA', 
            //   wotype: 'TPS', sa_name: 'AND', workrelease: '12/Jan/2017 07:05', workstart: '12/Jan/2017 08:03', 
            //   timewait: '00:32', statusicon: [], category: 'EM'},
            // {chip: 'waiting_production', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 2345 MNO', start: '08:00', type: 'AVANZA', grouphead: 'MAT',
            //   wotype: 'Light', sa_name: 'BUD', statusicon: ['assignment'], workrelease: '12/Jan/2017 07:13', workstart: '12/Jan/2017 08:15', 
            //   timewait: '00:29', category: 'B'},
            // {chip: 'waiting_production', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 777 AA', start: '08:30', type: 'AVANZA', grouphead: 'RUD',
            //   wotype: 'Medium', sa_name: 'ANI', icons: ['assignment'], workrelease: '12/Jan/2017 07:35', workstart: '12/Jan/2017 08:07', 
            //   timewait: '00:21', category: 'W'},
            // {chip: 'waiting_production', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3231 CD', start: '09:00', type: 'AVANZA', grouphead: 'DOD',
            //   wotype: 'Medium', sa_name: 'BUD', icons: ['watch', 'arrow1'], workrelease: '12/Jan/2017 07:45', workstart: '12/Jan/2017 08:45', 
            //   timewait: '00:10', category: 'O'},
            // {chip: 'waiting_production', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 2423 CD', start: '09:00', type: 'AVANZA', grouphead: 'JOK',
            //   wotype: 'Heavy', sa_name: 'BUD', icons: ['watch', 'arrow1'], workrelease: '12/Jan/2017 08:02', workstart: '12/Jan/2017 09:00', 
            //   timewait: '00:03', category: 'O'},
          ]
          var vres = res.data.Result;
          $scope.jumlahWPROD = res.data.Result.length;
          for(var i=0; i<vres.length; i++){
            var vresitem = vres[i];
            var vminute = getMinToday(vresitem.TimeStart);
            var vtimewait = JsBoard.Common.getTimeString(vminute);
    
            var vitem = {chip: 'waiting_production', nopol: vresitem.PoliceNumber, 
              start: '08:00', type: vresitem.ModelType, 
              workrelease: $filter('date')(vresitem.TimeStart, 'dd/MM/yyyy HH:mm'), 
              janjiserah: $filter('date')(vresitem.JanjiSerah, 'dd/MM/yyyy HH:mm'), 
              wotype: vresitem.BpCategory,
              sa_name: vresitem.Initial, 
              timewait: vtimewait, 
              statusicon: [], category: ''
            }
            if (vresitem.isPrediagnose){
              vitem.statusicon.push('diagnose');
            }
            // boardData.push(vitem);
            arrPROD.push(vitem);
          }

          if (dataHabisPROD == true)
          {
              CountBoardPROD = arrPROD.length;
              ModulusPROD = parseInt(CountBoardPROD % showData);
              BagiBoardPROD = parseInt(CountBoardPROD / showData);
              if (ModulusPROD != 0)
              {
                  BagiBoardPROD = BagiBoardPROD + 1;
              }
          }

          for(var i= MulaiDataPROD;  i< arrPROD.length; i++){
            var vboard = arrPROD[i];
            boardData.push(vboard);
        }

        BagiBoardPROD = BagiBoardPROD - 1;

        if (BagiBoardPROD != 0)
          {
              dataHabisPROD = false;
              MulaiDataPROD = MulaiDataPROD + showData;
          }
          else
          {
              MulaiDataPROD = 0;
              BagiBoardPROD = 0;
              dataHabisPROD = true;
          }

          // simulateProductionDate(boardData);
          $timeout(function() {
            FuncWaitingForProduction(boardData);
          }, 100);
    
        });
      }
  
      // ========================================================= bagi 2 coy ============================================================================

      if ($scope.showBoard == 'bawah'){
        var getMinToday = function(vdate){
          var vtoday = new Date();
          var theDay = new Date(vdate);
          var vDayDiff = vtoday.getDate()-theDay.getDate();
          var vDayMin = $filter('date')(theDay, 'HH:mm');
          var vNowMin = $filter('date')(vtoday, 'HH:mm');
          vminute = JsBoard.Common.getMinute(vNowMin)-JsBoard.Common.getMinute(vDayMin);
          if (vDayDiff>0){
            vminute = vminute+vDayDiff*24*60;
          }
          return vminute;
    
        }
    
        var getMinToday2 = function(vdate){
          var vtoday = new Date();
          var theDay = new Date(vdate);
          var vDayDiff = vtoday.getDate()-theDay.getDate();
          var vDayMin = $filter('date')(theDay, 'HH:mm');
          var vNowMin = $filter('date')(vtoday, 'HH:mm');
          vminute = JsBoard.Common.getMinute(vNowMin)-JsBoard.Common.getMinute(vDayMin);
          if (vDayDiff){
            vminute = vminute+vDayDiff*24*60;
          }
          return vminute;
    
        }
    
        var getDayToday = function(vdate){
          var vtoday = new Date();
          var theDay = new Date(vdate);
          var vDayDiff = vtoday.getDate()-theDay.getDate();
          return vDayDiff;
        }


        var FuncWaitingForFinalInspection = function(data){
          //----------------------------------------//
          // **** waiting for final inspection **** //
          //----------------------------------------//
          
          var boardConfig = {
            type: 'kanban',
            board:{
              container: 'irgbp-waiting_inspection',
              chipGroup: 'irregular_bp',
              iconFolder: 'images/boards/iregular'
    
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            layout: {
            }
    
          }
          
          if (typeof data == 'undefined'){
            data = [];
          }
          boardData = data;
          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: 'inc',
            boardName: 'waiting_final_inspection'
          })
    
        }
        
        var vurlwfd = '/api/as/Boards/BP/irregular/wfi';
        $http.get(vurlwfd).then(function(res){
          var boardData = [
            // {chip: 'waiting_inspection', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 NB', start: '08:00', type: 'AVANZA', 
            //   wotype: 'TPS', sa_name: 'AND', clockofftime: '12/Jan/2017 07:05', janjiserah: '12/Jan/2017 08:03', 
            //   foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'},
            // {chip: 'waiting_inspection', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 907 MO', start: '09:20', type: 'AVANZA', 
            //   wotype: 'Light', sa_name: 'BUD', clockofftime: '12/Jan/2017 07:13', janjiserah: '12/Jan/2017 08:15', 
            //   foreman: 'MIN', statusicon: [], timewait: '00:15', category: 'B'},
            // {chip: 'waiting_inspection', row: 1, col: 2.5, timestatus: 1, width: 1, nopol: 'B 6467 AA', start: '09:30', type: 'AVANZA', 
            //   wotype: 'Medium', sa_name: 'ANI', clockofftime: '12/Jan/2017 07:35', janjiserah: '12/Jan/2017 08:07', 
            //   foreman: 'DON', icons: [], timewait: '00:13', category: 'W'},
            // {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3888 CD', start: '10:00', type: 'AVANZA', 
            //   wotype: 'Medium', sa_name: 'BUD', clockofftime: '12/Jan/2017 07:45', janjiserah: '12/Jan/2017 08:45', 
            //   foreman: 'FIN', icons: ['watch', 'arrow1'], timewait: '00:04', category: 'O'},
            // {chip: 'waiting_inspection', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9888 HN', start: '10:05', type: 'AVANZA', 
            //   wotype: 'Heavy', sa_name: 'BUD', clockofftime: '12/Jan/2017 08:02', janjiserah: '12/Jan/2017 09:00', 
            //   foreman: 'JOK', icons: ['watch', 'arrow1'], timewait: '00:03', category: 'O'},
          ]
          var vres = res.data.Result;
          $scope.jumlahWFI = res.data.Result.length;
          for(var i=0; i<vres.length; i++){
            var vresitem = vres[i];
            var vminute = getMinToday(vresitem.TimeStart);
            var vtimewait = JsBoard.Common.getTimeString(vminute);
    
            var vitem = {chip: 'waiting_inspection', nopol: vresitem.PoliceNumber, 
              start: '08:00', type: vresitem.ModelType, 
              clockofftime: $filter('date')(vresitem.TimeStart, 'dd/MM/yyyy HH:mm'), 
              janjiserah: $filter('date')(vresitem.JanjiSerah, 'dd/MM/yyyy HH:mm'), 
              wotype: vresitem.BpCategory,
              foreman: vresitem.ForemanInitial,
              sa_name: vresitem.Initial, 
              timewait: vtimewait, 
              statusicon: [], category: ''
            }
            if (vresitem.isPrediagnose){
              vitem.statusicon.push('diagnose');
            }
            // boardData.push(vitem);
            arrFI.push(vitem);
          }
  
          if (dataHabisFI == true)
            {
                CountBoardFI = arrFI.length;
                ModulusFI = parseInt(CountBoardFI % showData);
                BagiBoardFI = parseInt(CountBoardFI / showData);
                if (ModulusFI != 0)
                {
                    BagiBoardFI = BagiBoardFI + 1;
                }
            }
  
            for(var i= MulaiDataFI;  i< arrFI.length; i++){
              var vboard = arrFI[i];
              boardData.push(vboard);
          }
  
          BagiBoardFI = BagiBoardFI - 1;
  
          if (BagiBoardFI != 0)
            {
                dataHabisFI = false;
                MulaiDataFI = MulaiDataFI + showData;
            }
            else
            {
                MulaiDataFI = 0;
                BagiBoardFI = 0;
                dataHabisFI = true;
            }
  
          // simulateInspectionDate(boardData);
          $timeout(function() {
            FuncWaitingForFinalInspection(boardData);
          }, 100);
    
        }, 100);
        
        var FuncWaitingForTeco = function(data){
          //----------------------------//
          // **** waiting for TECO **** //
          //----------------------------//
          var boardConfig = {
            type: 'kanban',
            board:{
              container: 'irgbp-waiting_teco',
              chipGroup: 'irregular_bp',
              iconFolder: 'images/boards/iregular'
    
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            layout: {
            }
    
          }
    
          if (typeof data == 'undefined'){
            data = [];
          }
          boardData = data;
          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: 'inc',
            boardName: 'waiting_teco'
          })
    
        }
    
        var vurlwfd = '/api/as/Boards/BP/irregular/wteco';
        $http.get(vurlwfd).then(function(res){
          var boardData = [
            // {chip: 'waiting_teco', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8765 BNS', start: '08:00', type: 'AVANZA', 
            //   wotype: 'TPS', sa_name: 'AND', fi_finish: '12/Jan/2017 07:05', janjiserah: '12/Jan/2017 08:03', 
            //   foreman: 'MAN', timewait: '00:20', statusicon: [], category: 'EM'},
            // {chip: 'waiting_teco', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 907 MNO', start: '09:20', type: 'AVANZA', 
            //   wotype: 'Light', sa_name: 'BUD', fi_finish: '12/Jan/2017 07:13', janjiserah: '12/Jan/2017 08:15', 
            //   foreman: 'MIN', statusicon: [], timewait: '00:15', category: 'B'},
            // {chip: 'waiting_teco', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 3888 CDD', start: '10:00', type: 'AVANZA', 
            //   wotype: 'Medium', sa_name: 'BUD', fi_finish: '12/Jan/2017 07:45', janjiserah: '12/Jan/2017 08:45', 
            //   foreman: 'FIN', icons: ['watch', 'arrow1'], timewait: '00:04', category: 'O'},
            // {chip: 'waiting_teco', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9888 BSS', start: '10:05', type: 'AVANZA', 
            //   wotype: 'Heavy', sa_name: 'BUD', fi_finish: '12/Jan/2017 08:02', janjiserah: '12/Jan/2017 09:00', 
            //   foreman: 'JOK', icons: ['watch', 'arrow1'], timewait: '00:03', category: 'O'},
          ]
          var vres = res.data.Result;
          $scope.jumlahWTECO = res.data.Result.length;
          for(var i=0; i<vres.length; i++){
            var vresitem = vres[i];
            var vminute = getMinToday(vresitem.TimeStart);
            var vtimewait = JsBoard.Common.getTimeString(vminute);
    
            var vitem = {chip: 'waiting_teco', nopol: vresitem.PoliceNumber, 
              start: '08:00', type: vresitem.ModelType, 
              fi_finish: $filter('date')(vresitem.TimeStart, 'dd/MM/yyyy HH:mm'), 
              janjiserah: $filter('date')(vresitem.JanjiSerah, 'dd/MM/yyyy HH:mm'), 
              wotype: vresitem.BpCategory,
              sa_name: vresitem.Initial, 
              timewait: vtimewait, 
              statusicon: [], category: ''
            }
            if (vresitem.isPrediagnose){
              vitem.statusicon.push('diagnose');
            }
            // boardData.push(vitem);
            arrTECO.push(vitem);
          }
  
          if (dataHabisTECO == true)
            {
                CountBoardTECO = arrTECO.length;
                ModulusTECO = parseInt(CountBoardTECO % showData);
                BagiBoardTECO = parseInt(CountBoardTECO / showData);
                if (ModulusTECO != 0)
                {
                    BagiBoardTECO = BagiBoardTECO + 1;
                }
            }
  
            for(var i= MulaiDataTECO;  i< arrTECO.length; i++){
              var vboard = arrTECO[i];
              boardData.push(vboard);
          }
  
          BagiBoardTECO = BagiBoardTECO - 1;
  
          if (BagiBoardTECO != 0)
            {
                dataHabisTECO = false;
                MulaiDataTECO = MulaiDataTECO + showData;
            }
            else
            {
                MulaiDataTECO = 0;
                BagiBoardTECO = 0;
                dataHabisTECO = true;
            }
  
          //simulateTecoDate(boardData);
          $timeout(function() {
            FuncWaitingForTeco(boardData);
          }, 100);
    
        });
        
    
        var FuncWaitingForNotification = function(data){
          //------------------------------------//
          // **** waiting for Notification **** //
          //------------------------------------//
    
          var boardConfig = {
            type: 'kanban',
            board:{
              container: 'irgbp-waiting_notif',
              chipGroup: 'irregular_bp',
              iconFolder: 'images/boards/iregular'
    
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            layout: {
            }
    
          }
    
          if (typeof data == 'undefined'){
            data = [];
          }
          boardData = data;
          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: 'inc',
            boardName: 'waiting_notification'
          })
    
    
    
        }
    
        var vurlwfd = '/api/as/Boards/BP/irregular/wfn';
        $http.get(vurlwfd).then(function(res){
          var boardData = [
            // {chip: 'waiting_notif', row: 4, col: 1, timestatus: 1, width: 1, ext: 0.25, nopol: 'B 8481 NB', start: '12/Jan/2017 09:00', type: 'AVANZA', 
            //   wotype: 'TPS', sa_name: 'AND', teco_finish: '12/Jan/2017 07:05', janjiserah: '12/Jan/2017 08:03', 
            //   timewait: '-00:02', statusicon: ['billing'], category: 'EM'},
            // {chip: 'waiting_notif', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 8451 MO', start: '12/Jan/2017 09:30', type: 'AVANZA', 
            //   wotype: 'Light', sa_name: 'BUD', teco_finish: '12/Jan/2017 07:13', janjiserah: '12/Jan/2017 08:15', 
            //   statusicon: ['billing', 'car'], timewait: '-00:06', category: 'B'},
            // {chip: 'waiting_notif', row: 1, col: 2.5, timestatus: 1, width: 1, nopol: 'B 8466 AA', start: '12/Jan/2017 09:40', type: 'AVANZA', 
            //   wotype: 'Medium', sa_name: 'BUD', teco_finish: '12/Jan/2017 07:45', janjiserah: '12/Jan/2017 08:45', 
            //   icons: [], timewait: '-00:11', category: 'W'},
            // {chip: 'waiting_notif', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8496 CD', start: '12/Jan/2017 10:00', type: 'AVANZA', 
            //   wotype: 'Heavy', sa_name: 'BUD', teco_finish: '12/Jan/2017 08:02', janjiserah: '12/Jan/2017 09:00', 
            //   icons: ['watch', 'arrow1'], timewait: '-00:16', category: 'O'},
          ]
          var vres = res.data.Result;
          $scope.jumlahWFN = res.data.Result.length;
          for(var i=0; i<vres.length; i++){
            var vresitem = vres[i];
            var vminute = getMinToday2(vresitem.JanjiSerah);
            var vtimewait = JsBoard.Common.getTimeString(vminute);
    
            var vitem = {chip: 'waiting_notif', nopol: vresitem.PoliceNumber, 
              start: '08:00', type: vresitem.ModelType, 
              teco_finish: $filter('date')(vresitem.TimeStart, 'dd/MM/yyyy HH:mm'), 
              janjiserah: $filter('date')(vresitem.JanjiSerah, 'dd/MM/yyyy HH:mm'), 
              wotype: vresitem.BpCategory,
              sa_name: vresitem.Initial, 
              timewait: vtimewait, 
              statusicon: [], category: ''
            }
            if (!vresitem.isBilling){
              vitem.statusicon.push('billing');
            }
            if (!vresitem.isWashed){
              vitem.statusicon.push('car');
            }
            // boardData.push(vitem);
            arrNOTIF.push(vitem);
          }
  
          if (dataHabisNOTIF == true)
            {
                CountBoardNOTIF = arrNOTIF.length;
                ModulusNOTIF = parseInt(CountBoardNOTIF % showData);
                BagiBoardNOTIF = parseInt(CountBoardNOTIF / showData);
                if (ModulusNOTIF != 0)
                {
                    BagiBoardNOTIF = BagiBoardNOTIF + 1;
                }
            }
  
            for(var i= MulaiDataNOTIF;  i< arrNOTIF.length; i++){
              var vboard = arrNOTIF[i];
              boardData.push(vboard);
          }
  
          BagiBoardNOTIF = BagiBoardNOTIF - 1;
  
          if (BagiBoardNOTIF != 0)
            {
                dataHabisNOTIF = false;
                MulaiDataNOTIF = MulaiDataNOTIF + showData;
            }
            else
            {
                MulaiDataNOTIF = 0;
                BagiBoardNOTIF = 0;
                dataHabisNOTIF = true;
            }
  
          // simulateNotifDate(boardData);
          $timeout(function() {
            FuncWaitingForNotification(boardData);
          }, 100);
    
        }, 100);
        
        
        var FuncWaitingForDelivery = function(data){
          //--------------------------------//
          // **** waiting for Delivery **** //
          //--------------------------------//
    
          var boardConfig = {
            type: 'kanban',
            chip: {
              height: 115,
              width: 320,
              gap: 2
            },
            stage: {
              maxHeight: allBoardHeight,
              minHeight: allBoardHeight,
            },
            board:{
              container: 'irgbp-waiting_delivery',
              iconFolder: 'images/boards/iregular',
              chipGroup: 'irregular_bp',
            },
            layout: {
              chipHeight: 80,
              chipGap: 2
            }
    
          }
    
          if (typeof data == 'undefined'){
            data = [];
          }
          boardData = data;
          registerBoard({
            boardConfig: boardConfig,
            boardData: boardData,
            incField: 'timewait',
            incMode: 'inc',
            boardName: 'waiting_delivery'
          })
    
    
        }
    
        var vurlwfd = '/api/as/Boards/BP/irregular/wdeliv';
        $http.get(vurlwfd).then(function(res){
          var boardData = [
            // {chip: 'waiting_delivery', row: 4, col: 1, timestatus: 2, width: 1, ext: 0.25, nopol: 'B 666 PLN', start: '08:15', type: 'AVANZA', 
            //   tgl_kirim: '09/Jan/2017', tgl_target_notif: '11/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: -3, timewait: '00:13'
            // },
            // {chip: 'waiting_delivery', row: 1, col: 1, timestatus: 1, width: 1, ext: 0.5, nopol: 'B 1234 PLN', start: '12:10', type: 'AVANZA', 
            //   tgl_kirim: '10/Jan/2017', tgl_target_notif: '11/Jan/2017', sa_name: 'JOKO', daywait: 'H+1', daydif: -2, timewait: '00:13'
            // },
            // {chip: 'waiting_delivery', row: 1, col: 2.5, timestatus: 0, width: 1, nopol: 'B 777 PLN', start: '09:30', type: 'AVANZA', 
            //   tgl_kirim: '12/Jan/2017', tgl_target_notif: '13/Jan/2017', sa_name: 'GANI', daywait: 'H+1', daydif: -1, timewait: '00:13'
            // },
            // {chip: 'waiting_delivery', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 8718 PLN', start: '09:00', type: 'AVANZA', 
            //   tgl_kirim: '12/Jan/2017', tgl_target_notif: '13/Jan/2017', sa_name: 'SINTA', daywait: 'H+1', daydif: 0, timewait: '02:10'
            // },
            // {chip: 'waiting_delivery', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9819 PLN', start: '09:00', type: 'AVANZA', 
            //   tgl_kirim: '12/Jan/2017', tgl_target_notif: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, timewait: '01:30'
            // },
            // {chip: 'waiting_delivery', row: 1, col: 3.5, timestatus: 0, width: 1, nopol: 'B 9822 PLN', start: '09:00', type: 'AVANZA', 
            //   tgl_kirim: '12/Jan/2017', tgl_target_notif: '13/Jan/2017', sa_name: 'MAMAN', daywait: 'H+1', daydif: 0, timewait: '00:13'
            // },
          ]
          var vres = res.data.Result;
          $scope.jumlahWDELIV = res.data.Result.length;
          for(var i=0; i<vres.length; i++){
            var vresitem = vres[i];
            var vminute = getMinToday(vresitem.TimeStart);
            var vtimewait = JsBoard.Common.getTimeString(vminute);
    
            var vdays = getDayToday(vresitem.TimeStart);
            var xdays = 'TODAY';
            if (vdays>0){
              xdays = 'H+'+vdays;
            } else if (vdays<0){
              xdays = 'H'+vdays;
            }
    
            var vitem = {chip: 'waiting_delivery', nopol: vresitem.PoliceNumber, 
              start: '08:00', type: vresitem.ModelType, 
              tgl_notif: $filter('date')(vresitem.TimeStart, 'dd/MM/yyyy HH:mm'), 
              tgl_janji_serah: $filter('date')(vresitem.JanjiSerah, 'dd/MM/yyyy HH:mm'), 
              wotype: vresitem.BpCategory,
              sa_name: vresitem.Initial, 
              daydif: vdays,
              timewait: vtimewait, 
              statusicon: [], 
              daywait: xdays, dday: vdays
            }
            
            // boardData.push(vitem);
            arrDELIVERY.push(vitem);
          }
  
  
          if (dataHabisDELIVERY == true)
            {
                CountBoardDELIVERY = arrDELIVERY.length;
                ModulusDELIVERY = parseInt(CountBoardDELIVERY % showData);
                BagiBoardDELIVERY = parseInt(CountBoardDELIVERY / showData);
                if (ModulusDELIVERY != 0)
                {
                    BagiBoardDELIVERY = BagiBoardDELIVERY + 1;
                }
            }
  
            for(var i= MulaiDataDELIVERY;  i< arrDELIVERY.length; i++){
              var vboard = arrDELIVERY[i];
              boardData.push(vboard);
          }
  
          BagiBoardDELIVERY = BagiBoardDELIVERY - 1;
  
          if (BagiBoardDELIVERY != 0)
            {
                dataHabisDELIVERY = false;
                MulaiDataDELIVERY = MulaiDataDELIVERY + showData;
            }
            else
            {
                MulaiDataDELIVERY = 0;
                BagiBoardDELIVERY = 0;
                dataHabisDELIVERY = true;
            }
  
          // simulateDeliveryDate(boardData);
          $timeout(function() {
            FuncWaitingForDelivery(boardData);
          }, 100);
    
        });

      }
      
  
      
      
      
      // vdata[i].daywait = xdays;
      // vdata[i].dday = vdays;
      // vdata[i].tgl_notif = $filter('date')(xdate1, 'dd/MM/yyyy')+' '+vtime;
      // vdata[i].tgl_janji_serah = $filter('date')(xdate2, 'dd/MM/yyyy')+' '+vdata[i].start;
  

    }

    $interval(function() {
      if($scope.showBoard == 'atas'){
        $scope.showBoard = 'bawah';
      } else {
        $scope.showBoard = 'atas';
      }
        $scope.LoadAllData();
    },150000 );
    // },25000 );
    // },$scope.waktuRefresh );

    $scope.refreshData = function() {
      if($scope.showBoard == 'atas'){
        $scope.showBoard = 'bawah';
      } else {
        $scope.showBoard = 'atas';
      }
        $scope.LoadAllData();
        // $scope.waktuRefresh = 25000;
    }

    





})

;
