angular.module('app')
  .factory('CAccessMatrix', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getDealOut: function() {
        // var res=$http.get('/api/fw/Organization/Tree?childOnly=1');
        var res=$http.get('/api/ct/GetCGroupDealer');
        console.log('outlet=>',res);

        //res.data.Result = null;
        return res;
      },

      getOutlet: function(data) {
        var res=$http.get('/api/ct/GetOutletByGID/'+data.GroupDealerId+'/');
        console.log('d=>',res);

        //res.data.Result = null;
        return res;
      },
      getOutletcrm: function(data) {
        var res=$http.get('/api/ct/GetOutletByGID/'+data.dealerId+'/');
        console.log('d=>',res);

        //res.data.Result = null;
        return res;
      },

      getData: function(data) {
        console.log("Acces Matrix=====>",data);
        var res=$http.get('/api/ct/GetCustomerAccessTab/'+data.OutletId+'/'+data.RoleId+'/');

        console.log('hasil=>',res);

        //res.data.Result = null;
        return res;
      },

      updateData: function(data) {

        console.log("data update",data);
        // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);

        return $http.put('/api/ct/PutCCustomerAccessTab', [data]);
      },

      insertData: function(data) {
        console.log("data Insert",data);
        // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);

        return $http.post('/api/ct/PostCCustomerAccessTab', [data]);
      }


    }
  });
