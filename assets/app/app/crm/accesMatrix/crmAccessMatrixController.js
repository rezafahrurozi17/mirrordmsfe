angular.module('app')
  .controller('CrmAccessMatrixController', function($scope, $state, $http, CurrentUser,  bsNotify, ngDialog, CAccessMatrix, Role, OrgChart, $timeout, Auth) {
    // $scope, $http, $sce,$filter,$timeout,$window,CurrentUser, Service, Role, ServiceRights, OrgChart, Auth

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
                    $scope.getOrgData();
                    $scope.getRoleData();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

//     //                   };
    $scope.orgData = []; //OrgChart Collection
    $scope.outletData= [];
    $scope.ctlOrg = {}; //OrgChart Tree Controller
    $scope.selOrg = null;  //Selected Org

    $scope.roleData = []; //Role Collection
    $scope.selRole = null; //Selected Role
    $scope.mAccesFilter = {};

    var tabFile = [{tab:"Informasi Data Pribadi", key:1, OpenState : false, edit: false},
                      {tab:"Informasi Pekerjaan", key:2, OpenState : false, edit: false},
                      {tab:"Informasi Keluarga", key:4, OpenState : false, edit: false},
                      {tab:"Informasi Kendaraan", key:8, OpenState : false, edit: false},
                      {tab:"Informasi Preferensi", key:16, OpenState : false, edit: false},
                      {tab:"Informasi Field Action", key:32, OpenState : false, edit: false},
                      {tab:"Informasi Riwayat Perubahan", key:64, OpenState : false, edit: false}];


//     // ---------------------------------------------------------------------------------
//     // --------------------------------- Role  -----------------------------------------
//     // ---------------------------------------------------------------------------------
    $scope.getRoleData = function(){
      Role.getData().then(function(result) {
          $scope.roleData = result.data.Result;
          console.log("roleData=>",result.data);
          // $scope.ctlOrg.expand_all();
    });

    $scope.selectRole = function(role){
          console.log("sel role->",role);
          $scope.selRole = role;
          // $scope.getServiceRightsbyOrgRole(role);
    }
    }
//     // ---------------------------------------------------------------------------------
//     // --------------------------- Organization Chart  ---------------------------------
//     // ---------------------------------------------------------------------------------

//     //----------------------------------
//     // Backend Operation
    var bin = [1,2,4,8,16,32,64];
    $scope.getData = function(){
      delete $scope.mAccesFilter["AccessId"];
      console.log("getDate",$scope.mAccesFilter);

      if ($scope.mAccesFilter.OutletId != undefined && $scope.mAccesFilter.RoleId != undefined) {
          CAccessMatrix.getData($scope.mAccesFilter).then(function(res){
            console.log("getResult",res.data.Result);
              if (res.data.Result.length == 0) {
                  var tot = 0;
                  var tit = 0;

              } else{
                  $scope.mAccesFilter.AccessId = res.data.Result[0].AccessId;
                  var tot = res.data.Result[0].RightByte;
                  var tit = res.data.Result[0].RightByteEdit;
              };

              for (var i = 0; i < bin.length; i++) {
                if(tot&bin[i]){
                  tabFile[i].OpenState = true;
                }else{
                  tabFile[i].OpenState = false;
                }
                // if(tit >= 1){
                if(tit&bin[i]){
                  tabFile[i].edit = true;
                }else{
                  tabFile[i].edit = false;
                }
              };

              $scope.grid.data = tabFile;
            })
      } else{
          alert("Role Atau Outlet Kosong");
      };

      // )


      // CAccessMatrix.getAkses($scope.mAccesFilter).then(function(result) {
      //     // $scope.orgData = result.data.Result;
      //     // console.log("orgData=>",result.data);
      //     // $scope.ctlOrg.expand_all();
      //   });

    }

    $scope.getOrgData = function(){
      console.log("user==>",$scope.user);
        CAccessMatrix.getDealOut($scope.user).then(function(result) {
          $scope.orgData = result.data.Result;
          console.log("orgData=>",result.data);
          // $scope.ctlOrg.expand_all();
        });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    $scope.saveAcces = function(){
      console.log("save==>",$scope.mAccesFilter);
      console.log("save==>",$scope.grid.data);
      var cKey = 0;
      var eKey = 0;
      for (var i =0; i < $scope.grid.data.length; i++) {
        if($scope.grid.data[i].OpenState){
          cKey = cKey + $scope.grid.data[i].key;
        }
        if ($scope.grid.data[i].edit) {
          eKey = eKey + $scope.grid.data[i].key;
        };
      };

      console.log("key==>",cKey);
      $scope.mAccesFilter.RightByte = cKey;
      $scope.mAccesFilter.RightByteEdit = eKey;


      console.log("key==>",$scope.mAccesFilter);
      if ($scope.mAccesFilter.AccessId != undefined) {
          // $scope.mAccesFilter.AccessId = $scope.grid.data.AccessId
          CAccessMatrix.updateData($scope.mAccesFilter).then(function(result){
            $scope.grid.data = [];

          })
      } else{
          CAccessMatrix.insertData($scope.mAccesFilter).then(function(result){
            $scope.grid.data = [];

          })
      };



    }
    $scope.selectDealer = function(xD){
      console.log("dealeeer==>",xD);
      // $scope.outletData = xD.Child;
      CAccessMatrix.getOutlet(xD).then(function(result) {
        $scope.outletData = result.data.Result;
        console.log("Dn==>",result.data.Result);
      })



    }
       var btnActionAccesTemplate = '<button class="ui icon inverted grey button"'+
                                    ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:green;margin:1px 1px 0px 2px"'+
                                    // ' onclick="this.blur()"'+
                                    ' ng-click="grid.appScope.$parent.gridClickAcces(row.entity,grid.renderContainers.body.visibleRowCache.indexOf(row))">'+
                                    '<i ng-class="'+
                                                      '{ \'fa fa-fw fa-lg fa-toggle-on\' : row.entity.OpenState,'+
                                                      '  \'fa fa-fw fa-lg fa-toggle-off\': !row.entity.OpenState,'+
                                                      '}'+
                                    '">'+
                                    '</i>'+
                                '</button>';
      var btnActionEditTemplate = '<button class="ui icon inverted grey button"'+
                                    ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:green;margin:1px 1px 0px 2px"'+
                                    // ' onclick="this.blur()"'+
                                    ' ng-click="grid.appScope.$parent.gridClickUbah(row.entity,grid.renderContainers.body.visibleRowCache.indexOf(row))">'+
                                    '<i ng-class="'+
                                                      '{ \'fa fa-fw fa-lg fa-toggle-on\' : row.entity.edit,'+
                                                      '  \'fa fa-fw fa-lg fa-toggle-off\': !row.entity.edit,'+
                                                      '}'+
                                    '">'+
                                    '</i>'+
                                '</button>';
      $scope.gridClickUbah = function(row,idx){
      console.log(idx,"click==>",row);
        // $scope.grid.

        // if ($scope.grid.data[idx].edit) {
        //     for (var i = 0; i < $scope.grid.data.length; i++) {
        //       $scope.grid.data[i].edit = false;
        //     };
        // } else{
        //     for (var i = 0; i < $scope.grid.data.length; i++) {
        //       $scope.grid.data[i].edit = true;
        //     };
        // };
        if ($scope.grid.data[idx].edit) {
            $scope.grid.data[idx].edit = false;
        } else{
            $scope.grid.data[idx].edit = true;
            $scope.grid.data[idx].OpenState = true;
        };

      }
      $scope.gridClickAcces = function(row,idx){
      console.log(idx,"click==>",row);
      // $scope.grid.
        if ($scope.grid.data[idx].OpenState) {
            $scope.grid.data[idx].OpenState = false;
            $scope.grid.data[idx].edit = false;
        } else{
            $scope.grid.data[idx].OpenState = true;
        };

      }

    $scope.grid = {
        enableGridMenu: true,
        enableSorting: true,
        // enableRowSelection: true,
        // multiSelect : false,
        // enableFullRowSelection: true,
        // enableRowHeaderSelection: false,
        // enableFiltering:true,
        // enableSelectAll : false,
    // grid.appScope.
        columnDefs: [
            {name: 'Jenis Informasi',field: 'tab'}, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            // {name: 'Kategori',field: 'key'},
            { name:'Akses', allowCellFocus: false, width:100, pinnedRight:true,
                                       enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                                       cellTemplate: btnActionAccesTemplate
              },
            { name:'Ubah', allowCellFocus: false, width:100, pinnedRight:true,
                                       enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                                       cellTemplate: btnActionEditTemplate
              },
             //, cellTemplate:'<a ng-click="grid.appScope.rangkaLink(row.entity)">{{row.entity.noRank}}</a>'
        // };{{row.entity}}
        ],
        onRegisterApi : function(gridApi) {
            // set gridApi on $scope
            $scope.gridApi = gridApi;

            $scope.gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                  $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                  // console.log("selected=>",$scope.selectedRows);
                  if($scope.onSelectRows){
                      $scope.onSelectRows($scope.selectedRows);
                  }
            });
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                  $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                  if($scope.onSelectRows){
                      $scope.onSelectRows($scope.selectedRows);
                  }
            });
        }
      }




});
