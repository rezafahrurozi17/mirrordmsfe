angular.module('app')
    .controller('vehicleDataController', function ($scope, $q, $http, CurrentUser, CMaster, vehicleData, bsNotify, bsAlert, VehicleHistory, $timeout) {

        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.loading = false;
            $scope.gridData = [];
            //$scope.getData(); /// langsung ke grid
            // $scope.getService(); // coba dulu nit
            // $scope.getdataUser(); // coba dulu nit
            // $scope.getdataVSaleInfo();
            // $scope.getdataCVehicle();
            // $scope.getdataService();
            //$scope.getListSpesifik();
            // $scope.getdataVModel();
            // $scope.getdataVType();

        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log('user', $scope.user)
        // $scope.user.OrgTypeId = 1128; // buat tes aja Role Kepala Bengkel GR
        $scope.mFilterData = { xcari: "", sservis: false, filterSearch: 0, xchoice: 1, xSearch: "", xcariId: 1 }; //Model
        $scope.mVehicleData = {}; //Model
        $scope.dataSalesHistory = {};

        $scope.mVehicleOwner = {};
        $scope.mVehicleTamInfo = {};
        // $scope.hsearch = false;
        $scope.hfilter = true;
        $scope.hdetail = false;
        $scope.hdetService = true;
        $scope.hidetab = false;
        // $scope.hkepemilikan = true;
        $scope.hservis = true;
        $scope.hupdSTNK = true;
        $scope.xenabled = true;
        $scope.xbatal = true;
        $scope.xsimpan = true;
        $scope.xubah = false;
        $scope.heditOwner = true;
        $scope.lmModel = {};
        $scope.mDetail = [];
        // $scope.dDetail = [];
        $scope.open1 = false;
        $scope.show = { xtab: false, advSearch: false, teknisi: false };

        $scope.fSearch = { vin: "", lp: "" };
        $scope.fSearchGroup = { vin: "", lp: "" };

        $scope.dataService = [];
        var det = new Date("October 13, 2014 11:13:00");
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,

            //disableWeekend: 1
        };
        $scope.dateOptionsEnd = {
            startingDay: 1,
            format: dateFormat,
            // min:
            //disableWeekend: 1
        };

        $scope.startDateChange = function (dt) {
            console.log("change->", dt);
            $scope.dateOptionsEnd.minDate = dt;



            // $scope.startHOReviewDateOptions.minDate = $scope.mPeriod.EditEndPeriod;
            // $scope.startHOReviewDateOptions.maxDate = $scope.mPeriod.EndPeriod;

        }

        $scope.actionButtonSettingsDetail = [
            {
                // enable:true,
                actionType: 'back', //Use 'Back Action' of bsForm
                title: 'Kembali',
                //icon: 'fa fa-fw fa-edit',
            }

        ];


        CMaster.GetCVehicleModel().then(function (res) {
            // console.log("List Vehicle Brand====>",res);
            $scope.modelVehicle = res.data.Result;
        })

        $scope.selectModel = function (selected) {
            $scope.selectedModel = selected;
            console.log("$scope.selectedModel", $scope.selectedModel);
            $scope.typeVehicle = $scope.selectedModel.MVehicleType;
        }

        // $scope.show.advSearch = true; 
        if ($scope.user.OrgTypeId == 1 || $scope.user.OrgId == 1000038) { $scope.show.advSearch = true; }
        else {console.log("aoa"); };
        

        $scope.listButtonSettingsGroupSearch = { new: { enable: false }, view: { enable: false }, edit: { enable: false }, delete: { enable: false } };
        $scope.listCustomButtonSettingsGroupSearch = [{
            enable: true,
            // title:"Jadikan Utama",
            icon: "fa fa-external-link",
            func: function (row) {
                console.log("customButton", row);
                $scope.ininovin = row.VIN;
                console.log("no vin apa a",  $scope.ininovin);
                // var vd={VehicleId:row.VehicleId,crm:1};
                // $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'view',vd,true,true);
                // $scope.onShowDetail(row,'view');
                $scope.formApi.setMode('detail');
                $scope.onShowDetail(row, 'view');
                // setEditMode(false);
            }
            
        }];
            
        $scope.listButtonSettingsVehicleService = { new: { enable: false }, edit: { enable: false }, delete: { enable: false } };
        $scope.listButtonSettingsFamily = { new: { enable: false }, edit: { enable: false }, delete: { enable: false } };
        $scope.listButtonSettingsPengguna = {   new: { enable: false }, view: { enable: false }, delete: { enable: false }, select: { enable: false }, selectall: { enable: false }, edit: { enable: false }};
        $scope.listCustomButtonSettingsVehicleService = [{
            enable: true,
            title: "Detail Service",
            icon: "fa fa-external-link",
            func: function (row) {
                console.log("customButton", row);
                $scope.ininovin = row.VIN;
                console.log("no vin apac",  $scope.ininovin);
                // var vd={VehicleId:row.VehicleId,crm:1};
                // $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'view',vd,true,true);
                // $scope.onShowDetail(row,'view');
                // $scope.formApi.setMode('detail');
                // $scope.onShowDetail(row,'view');
                // setEditMode(false);
                // $scope.detailService(row.JobId);
                $scope.hdetService = false;
                $scope.hdetail = true;

                // $scope.DServiceId = val;

                // $scope.mDetail = val;

                vehicleData.getHistoryDetail(row.JobId).then(
                    function (res) {
                        console.log("res HistoryDetail", res);
                        $scope.mDetail = res.data.Result[0];
                        // $scope.dDetail = res.data.Result[0].JobTask;
                        // console.log("dDetail",dDetail);
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );
            }
        }];
        $scope.listButtonSettingsVehicleOwner = {
            new: { enable: false },
            view: { enable: false },
            edit: { enable: false },
            delete: { enable: false },
            select: { enable: false }, 
            selectall: { enable: false }
        };
        $scope.listCustomButtonSettingsVehicleOwner = [{           
            enable: true,
            title: "History Service",
            icon: "fa fa-external-link",
            func: function (row) {
                console.log("customButton", row);
                $scope.ininovin = row.VIN;
                console.log("no vin apaas",  $scope.ininovin);
                // var vd={VehicleId:row.VehicleId,crm:1};
                // $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'view',vd,true,true);
                // $scope.onShowDetail(row,'view');
                // $scope.formApi.setMode('detail');
                // $scope.onShowDetail(row,'view');
                // setEditMode(false);

                $scope.gridDataInfoService = [];
                $scope.mFilterData.sservis = true;

                // $scope.hkepemilikan = false;
                $scope.hservis = false;
                $scope.hdetService = false;

                // console.log("dataService", dataService);
                // var a = $scope.dataService;
                // var vId = row.VehicleId;
                // var xSDate = formatDate(row.StartDate);
                // $scope.xEDate = row.EndDate;
                // console.log("SINI WOY1",$scope.xEDate);
                // if ($scope.xEDate === null) {
                //     $scope.xEDate = new Date();
                //     console.log("SINI WOY",$scope.xEDate);
                //     var dd70 = $scope.xEDate.getDate();
                //     var day70 = dd70 < 10 ? '0' + dd70 : dd70;
                //     var mm70 = $scope.xEDate.getMonth() + 1;
                //     var month70 = mm70 < 10 ? '0' + mm70 : mm70;
                //     var yyyy70 = $scope.xEDate.getFullYear();
                    
                //     $scope.xEDate = yyyy70 + "-" + month70 + "-" + day70;
                // }
                // else{
                //     console.log("SINI WOY",$scope.xEDate);
                // }

                // console.log("id", xSDate, "-", $scope.xEDate);

                // vehicleData.getHistory(xSDate,xEDate,vId).then(
                //     function(res){
                //         console.log("res History", res);
                //         $scope.gridVehicleService = res.data.Result;
                //         // $scope.gridDataInfoService = res.data.Result;
                //     },
                //     function(err){
                //         console.log("err=>",err);
                //     }
                // );
                // row.VIN = 'TATOBA602DV000296';
                // VehicleHistory.getData(2, row.VIN, xSDate, $scope.xEDate).then(
                //     function (res) {
                //         $scope.mDataVehicleHistory = res.data;

                //         console.log("mDataVehicleHistory=>", $scope.mDataVehicleHistory);
                //     },
                //     function (err) {
                //     }
                // );

                
            }
        }];
        // var setEditMode = function(editing){
        //     // console.log("editing==>",editing);
        //   $scope.actionButtonSettingsDetail[0].visible=!editing; //back
        //   $scope.actionButtonSettingsDetail[1].visible=editing;  //save
        //   $scope.actionButtonSettingsDetail[2].visible=editing;  //cancel
        //   $scope.actionButtonSettingsDetail[3].visible=!editing;
        //   // $scope.actionButtonSettingsDetail[4].visible=!editing;
        //   // $scope.actionButtonSettingsDetail[5].visible=!editing;
        //   $scope.show.disableOtherTab = editing;
        //   $scope.formApi.setFormReadOnly(!editing);
        // }

        $scope.AssemblyYearOptions = {
            autoclose: true,
            format: 'yyyy',
            viewMode: "years",
            minMode: "year"
        };
        // $scope.mVehicleData.xcari="";

        $scope.forceGetData = function () {
            console.log("Success ForceGetData");
        }
        //$scope.VehicleModelData = [{id:1,name:"Model1"},{id:2,name:"Model2"},{id:3,name:"Model3"}];
        $scope.MadeYearData = [{ id: 1, name: "2014" }, { id: 2, name: "2015" }, { id: 3, name: "2016" }];
        //$scope.VehicleTypeData = [{id:1,name:"type1"},{id:2,name:"type2"},{id:3,name:"type3"}];
        $scope.NSearchData = [{ id: 1, name: "No Rangka" }, { id: 2, name: "No Polisi" }];
        $scope.xperanPelanggan = [{ RoleId: 1, name: "Pembeli" }, { RoleId: 2, name: "Pemilik" }, { RoleId: 3, name: "Penanggung Jawab" }, { RoleId: 4, name: "Pengguna" }];
        $scope.xdataSearch = [{ id: 1, nametab: "Pencarian Spesifik" }, { id: 2, nametab: "Pencarian Kelompok Data" }];
        // $scope.refmarkers = [{ref:'abc'},{ref:'def'},{ref:'ghi'}];

        $scope.filterFieldChange = function (item) {
            console.log("filterFieldSelected item =>", item);
            $scope.filterFieldSelected = item.name;
            $scope.mFilterData.xcariId = item.id;
            console.log()
        }
        $timeout(function () {
            $scope.filterFieldChange($scope.NSearchData[0]);
        }, 0);




        $scope.actionButtonSettings = [
            {
                type: 'view',
                title: 'View',
                visible: false,
                func: function (formScope, row, param) {
                    console.log('action View', row);
                    if (row.crm != undefined) {
                        $scope.formApi.setMode('detail');
                        $scope.onShowDetail(row, 'view');
                    } else {
                        console.log("kosooooong");
                    };


                }
            },

        ];





        // $scope.detailActionButtonSettings = [
        //     {
        //       actionType: 'edit', //Use 'Edit Action' of bsForm
        //       title: 'Edit',
        //       icon: 'fa fa-fw fa-edit',
        //       color: 'rgb(213,51,55)'
        //     },

        // ];


        $scope.xdservis =
            [
                {
                    serviceId: 1,
                    toyotaId: "1234567890-1",
                    jenisservis: "Perbaikan Pintu Kanan",
                    lokasi: "Auto2000 Sunter Km 30.529",
                    jobsuggest: "Memperbaikin lecet pintu",
                    dateService: "26/06/2016 10:45",
                    SA: "Minhyung",
                    Teknisi: "Mark",
                    kategori: "BP"
                },

                // {
                //     serviceId: 2,
                //     toyotaId: "1234567890-1",
                //     jenisservis: "Servis Berkala 30000",
                //     lokasi : "Auto2000 Sunter Km 30.529",
                //     jobsuggest : "Ganti Kampas Rem Depan",
                //     dateService: "26/06/2016 10:45",
                //     SA : "Minhyung",
                //     Teknisi : "Mark",
                //     kategori: "GR"
                // },

                // {
                //     serviceId: 3,
                //     toyotaId: "1234567891-2",
                //     jenisservis: "Servis Berkala 20000",
                //     lokasi : "Auto2000 Sunter Km 30.529",
                //     jobsuggest : "Ganti Kampas Rem Belakang",
                //     dateService: "26/06/2016 10:45",
                //     SA : "Minhyung",
                //     Teknisi : "Mark",
                //     kategori: "GR"
                // },

                // {
                //     serviceId: 4,
                //     toyotaId: "1234567892-3",
                //     jenisservis: "Servis Berkala 10000",
                //     lokasi : "Auto2000 Sunter Km 30.529",
                //     jobsuggest : "Tambah angin ban belakang",
                //     dateService: "26/06/2016 10:45",
                //     SA : "Minhyung",
                //     Teknisi : "Mark",
                //     kategori: "GR"
                // }
            ];

        // $scope.dDetail = [
        //     {
        //         id: 1,
        //         serviceId: 1,
        //         jenisservis : "Perbaikan Pintu Kanan",
        //         materials : "Cat",
        //         Qty : 1,
        //         satuan : "Piece"
        //     },
        //     // {
        //     //     id: 2,
        //     //     serviceId: 2,
        //     //     jenisservis : "Servis Berkala 30.000",
        //     //     materials : "Oli Mesin",
        //     //     Qty : 1,
        //     //     satuan : "Piece"
        //     // },
        //     // {
        //     //     id: 3,
        //     //     serviceId: 3,
        //     //     jenisservis : "Servis Berkala 20.000",
        //     //     materials : "Oli Mesin",
        //     //     Qty : 1,
        //     //     satuan : "Piece"
        //     // },
        //     // {
        //     //     id: 4,
        //     //     serviceId: 4,
        //     //     jenisservis : "Servis Berkala 10.000",
        //     //     materials : "Oli Mesin",
        //     //     Qty : 1,
        //     //     satuan : "Piece"
        //     // },
        //     // {
        //     //     id: 5,
        //     //     serviceId: 4,
        //     //     jenisservis : "Servis Berkala 20.000",
        //     //     materials : "Ganti ban dalam",
        //     //     Qty : 1,
        //     //     satuan : "Piece"
        //     // }
        // ];








        // $scope.mySelections1 = [];

        $scope.updateOwner = function (mVehicleOwner) {
            console.log("tes aja");
            $scope.dimmerShow = false;
        }

        $scope.toggle = function (itemPos) {
            if ($scope.menuIsOpen === itemPos) {
                $scope.menuIsOpen = 0;
                $scope.mFilterData.filterSearch = 0;
            }
            else {
                $scope.menuIsOpen = itemPos;
                $scope.mFilterData.filterSearch = 1;
            }
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        $scope.teknisi = function (xte, cat) {
            console.log("teknisi==>", xte);
            // alert('hahahahahha');
            // $scope.training = [{name:"Belajar Tulis Menulis",tanggal:"2017-09-09"},{name:"Belajar Baca Coding",tanggal:"2017-09-09"}];
            // $scope.sertification = [{name:"Belajar Menulis",tanggal:"2017-09-09"},{name:"Belajar Coding",tanggal:"2017-09-09"}];
            if (cat == 1) {
                $scope.namaTeknisi = xte.EmployeeName;
                $scope.namaRole = 'Nama Teknisi ';
                var ktp = xte.KTPNo;

            } else if (cat == 2) {
                $scope.namaTeknisi = xte.ServiceAdvisor;
                $scope.namaRole = 'Nama Service Advisor  ';
                var ktp = xte.ServiceAdvisorKTP;
            } else if (cat == 3) {
                $scope.namaTeknisi = xte.Foreman;
                $scope.namaRole = 'Nama Foreman ';
                var ktp = xte.ForemanKTP;
            }
            //'Budi Suliastianto;'//

            vehicleData.getEmployeeCertifications(ktp).then(
                function (res) {
                    $scope.sertification = res.data.Result;
                    console.log("Training, Specification=>", res.data.Result);
                },
                function (err) {
                }
            );
            vehicleData.getEmployeeTrainings(ktp).then(
                function (res) {
                    $scope.training = res.data.Result;
                    console.log("Training, Specification=>", res.data.Result);
                },
                function (err) {
                }
            );
            $scope.show.teknisi = true;
        }

        $scope.onSelectRows = function (rows) {
            console.log("rows ooooooooo", rows);
            $scope.gridDataInfoService = [];
            $scope.mFilterData.sservis = true;

            // $scope.hkepemilikan = false;
            $scope.hservis = false;

            // console.log("dataService", dataService);
            // var a = $scope.dataService;
            var vId = rows[0].VehicleId;
            var xSDate = formatDate(rows[0].StartDate);
            var xEDate = formatDate(rows[0].EndDate);

            console.log("id", xSDate, "-", xEDate);

            vehicleData.getHistory(xSDate, xEDate, vId).then(
                function (res) {
                    // console.log("res History", res);
                    $scope.gridDataInfoService = res.data.Result;
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
            // for(var i=0; i<a.length; i++){
            // if(xStart < a[i].ServiceDate > xEnd){
            //      a[i].toyotaId = id;
            //      $scope.gridDataInfoService.push(a[i]);

            // }
            //var infoServiceObj = {ToyotaId: a[1].ToyotaId};
            // console.log("infoServiceObj", infoServiceObj);
            //$scope.gridDataInfoService.push(a[1]);
            // }
            console.log("$scope.gridDataInfoService", $scope.gridDataInfoService);

            // $scope.cobafilter = rows[0].toyotaId;
        }



        // $scope.getdataVModel = function() {
        //     vehicleData.getVModel().then(
        //         function(res){
        //             // console.log("res getService VehicleModelData", res);
        //             // $scope.gridDataHOwner = res;
        //             $scope.VehicleModelData = res.data.Result;
        //             // $scope.varTypeData = res.data.Result;
        //             // $scope.loading=false;
        //             // return res.data.Result;
        //         },
        //         function(err){
        //             console.log("err=>",err);
        //         }
        //     );
        // }

        // $scope.getdataVType = function() {
        //     vehicleData.getVType().then(
        //         function(res){
        //             // console.log("res getService VehicleTypeData", res);
        //             // $scope.gridDataHOwner = res;
        //             $scope.VehicleTypeData = res.data.Result;
        //             // $scope.varTypeData = res.data.Result;
        //             // $scope.loading=false;
        //             // return res.data.Result;
        //         },
        //         function(err){
        //             console.log("err=>",err);
        //         }
        //     );
        // }



        $scope.getData = function () {
            console.log($scope.mVehicleData, "mSearch--->", $scope.mFilterData);
            // var vin =""
            console.log("$scope.mVehicleData.xcari;", $scope.mFilterData.xcari);

            // untuk link view.....!!!



            if (!$scope.mFilterData.filterSearch && $scope.mFilterData.xcari != "") {
                var vin = "";
                var lp = "";
                if ($scope.mFilterData.xcariId == 1) {
                    vin = $scope.mFilterData.xcari;
                    lp = "-";
                } else {
                    vin = "-";
                    lp = $scope.mFilterData.xcari;
                }

                vehicleData.getListSpecific(vin, lp).then(
                    function (res) {
                        console.log("res getListSpecific", res);
                        console.log('user outlet id', $scope.user.OutletId)
                        // $scope.grid.data = res.data.Result;

                        // kata nya muncul nya sesuai outlet nya aja, jd kalau ada yg beda outletnya jng di tampilin yg ga sesuai outlet loginnya
                        $scope.filteringData = [];
                        for (var i=0; i<res.data.Result.length; i++){
                            // if (res.data.Result[i].OutletId == $scope.user.OutletId){
                                $scope.filteringData.push(res.data.Result[i]);
                            // }
                        }
                        $scope.gridGroupSearch = $scope.filteringData;

                        // $scope.gridGroupSearch = res.data.Result;
                        // $scope.varTypeData = res.data.Result;
                        // $scope.loading=false;
                        // return res.data.Result;
                        console.log('VIN ==>', $scope.gridGroupSearch[0].VIN)
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                )

                

                // console.log("sampai $scope.fSearch ",vin, " & ", lp);
                // $scope.getListSpesifik = function(vin,lp) {



                // }
            } else if ($scope.mFilterData.filterSearch) {

                var aYear = angular.copy($scope.mFilterData.AssemblyYear);
                var vTypeId = angular.copy($scope.mFilterData.VehicleTypeId);
                var vModelId = angular.copy($scope.mFilterData.vehicleModelId);
                var prodDate1 = angular.copy($scope.mFilterData.ProdDate1);
                var prodDate2 = angular.copy($scope.mFilterData.ProdDate2);

                //var prodDate22 = angular.copy();
                //console.log("prodDate22", prodDate22);

                if (aYear == null || aYear == undefined || aYear == "") {
                    aYear = "-";
                } else {
                    aYear = (aYear.getFullYear());
                }
                if (vTypeId == null || vTypeId == undefined || vTypeId == "") {
                    vTypeId = "-";
                }
                if (vModelId == null || vModelId == undefined || vModelId == "") {
                    vModelId = "-";
                }
                if (prodDate1 == null || prodDate1 == undefined || prodDate1 == "") {
                    prodDate1 = "-";
                } else {
                    prodDate1 = (prodDate1.getFullYear() + '-' + (prodDate1.getMonth() + 1) + '-' + prodDate1.getDate());
                }
                if (prodDate2 == null || prodDate2 == undefined || prodDate2 == "") {
                    prodDate2 = "-";
                } else {
                    prodDate2 = (prodDate2.getFullYear() + '-' + (prodDate2.getMonth() + 1) + '-' + prodDate2.getDate());
                }

                console.log("sampai", aYear);
                console.log("sampai", vTypeId);
                console.log("sampai", vModelId);
                console.log("sampai", prodDate1);
                console.log("sampai", prodDate2);

                if (vTypeId == "-" || vModelId == "-") {
                    bsAlert.alert({
                        title: "Model Dan Tipe Kendaraan Kosong",
                        text: "Harap isi terlebih dahulu",
                        type: "warning"
                        // },function(){

                        // },function(){

                    })
                } else {
                    vehicleData.getListGroup(aYear, vTypeId, vModelId, prodDate1, prodDate2).then(
                        function (res) {
                            console.log("res getListGroup", res);
                            $scope.gridGroupSearch = res.data.Result;
                            // $scope.grid.data = res.data.Result;
                        },
                        function (err) {
                            console.log("err=>", err);
                        }
                    );
                };



            } else {
                return $q.resolve(
                    vehicleData.getListSpecific(vin, lp).then(
                        function (res) {
                            console.log("res getBlaccck", res);

                            // $scope.gridGroupSearch=res.data.Result;

                        },
                        function (err) {
                            console.log("err=>", err);
                        }
                    )
                )
            }

            // if (rw.crm != undefined) {
            //     $scope.onShowDetail(rw);
            // };


        }

        

        var mthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agus", "September", "Oktober", "November", "Desember"];
        $scope.onShowDetail = function (row) {
            console.log("===================================== ON SHOW DETAIL VEHICLE", row, $scope.hupdSTNK);
            $scope.show.xtab = false;
            console.log("no vin apa",  row.VIN);
            
            

            vehicleData.getDataSalesHistoryKendaraan(row.VIN).then(function (res) {
                $scope.dataSalesHistory = res.data.Result[0];
                console.log('$scope.dataSalesHistory', $scope.dataSalesHistory);

                //DATE FORMAT DATA KENDARAAN ---------------------------------

                var dateClock1 = new Date($scope.dataSalesHistory.ProductionLineOffDate);
                var dd1 = dateClock1.getDate();
                var day1 = dd1 < 10 ? '0' + dd1 : dd1;
                var mm1 = dateClock1.getMonth() + 1;
                var month1 = mm1 < 10 ? '0' + mm1 : mm1;
                var yyyy1 = dateClock1.getFullYear();
                var dateFormat1 = day1 + "/" + month1 + "/" + yyyy1;

                var dateClock2 = new Date($scope.dataSalesHistory.WholeSalesDoDate);
                var dd2 = dateClock2.getDate();
                var day2 = dd2 < 10 ? '0' + dd2 : dd2;
                var mm2 = dateClock2.getMonth() + 1;
                var month2 = mm2 < 10 ? '0' + mm2 : mm2;
                var yyyy2 = dateClock2.getFullYear();
                var dateFormat2 = day2 + "/" + month2 + "/" + yyyy2;

                var dateClock3 = new Date($scope.dataSalesHistory.PDCGoodIssueDate);
                var dd3 = dateClock3.getDate();
                var day3 = dd3 < 10 ? '0' + dd3 : dd3;
                var mm3 = dateClock3.getMonth() + 1;
                var month3 = mm3 < 10 ? '0' + mm3 : mm3;
                var yyyy3 = dateClock3.getFullYear();
                var dateFormat3 = day3 + "/" + month3 + "/" + yyyy3;

                $scope.dataSalesHistory.ProductionLineOffDate = dateFormat1;
                $scope.dataSalesHistory.WholeSalesDoDate = dateFormat2;
                $scope.dataSalesHistory.PDCGoodIssueDate = dateFormat3;

                //DATE FORMAT DATA SERVIS ------------------------

                var dateClock4 = new Date($scope.dataSalesHistory.branchgoodreceivedate);
                var dd4 = dateClock4.getDate();
                var day4 = dd4 < 10 ? '0' + dd4 : dd4;
                var mm4 = dateClock4.getMonth() + 1;
                var month4 = mm4 < 10 ? '0' + mm4 : mm4;
                var yyyy4 = dateClock4.getFullYear();
                var dateFormat4 = day4 + "/" + month4 + "/" + yyyy4;

                var dateClock5 = new Date($scope.dataSalesHistory.jadwaltanggalpengiriman);
                var dd5 = dateClock5.getDate();
                var day5 = dd5 < 10 ? '0' + dd5 : dd5;
                var mm5 = dateClock5.getMonth() + 1;
                var month5 = mm5 < 10 ? '0' + mm5 : mm5;
                var yyyy5 = dateClock5.getFullYear();
                var dateFormat5 = day5 + "/" + month5 + "/" + yyyy5;

                var dateClock6 = new Date($scope.dataSalesHistory.aktualtanggalpengiriman);
                var dd6 = dateClock6.getDate();
                var day6 = dd6 < 10 ? '0' + dd6 : dd6;
                var mm6 = dateClock6.getMonth() + 1;
                var month6 = mm6 < 10 ? '0' + mm6 : mm6;
                var yyyy6 = dateClock6.getFullYear();
                var dateFormat6 = day6 + "/" + month6 + "/" + yyyy6;

                $scope.dataSalesHistory.branchgoodreceivedate = dateFormat4;
                $scope.dataSalesHistory.jadwaltanggalpengiriman = dateFormat5;
                $scope.dataSalesHistory.aktualtanggalpengiriman = dateFormat6;

                // ISSUE OR REPAIR

                var dateClock7 = new Date($scope.dataSalesHistory.ProblemDate);
                var dd7 = dateClock7.getDate();
                var day7 = dd7 < 10 ? '0' + dd7 : dd7;
                var mm7 = dateClock7.getMonth() + 1;
                var month7 = mm7 < 10 ? '0' + mm7 : mm7;
                var yyyy7 = dateClock7.getFullYear();
                var dateFormat7 = day7 + "/" + month7 + "/" + yyyy7;

                $scope.dataSalesHistory.ProblemDate = dateFormat7;
            });

            vehicleData.getListById(row.VehicleId).then(
                function (res) {
                    // console.log("res getListById", res);
                    $scope.mVehicleData = res.data.Result[0];
                    var d = new Date(res.data.Result[0].DECDate);

                    // var curr_date = d.getDate();
                    // var curr_month = d.getMonth() + 1; //Months are zero based
                    // var curr_year = d.getFullYear();
                    $scope.mVehicleData.DECDate = (d.getDate() + " " + mthNames[d.getMonth()] + " " + d.getFullYear());

                    if ($scope.mVehicleData.STNKDate == null || $scope.mVehicleData.STNKDate == undefined){
    
                        $scope.mVehicleData.STNKDate = null;
                    }else{
                        var dateClock7 = new Date($scope.mVehicleData.STNKDate);
                        var dd7 = dateClock7.getDate();
                        var day7 = dd7 < 10 ? '0' + dd7 : dd7;
                        var mm7 = dateClock7.getMonth() + 1;
                        var month7 = mm7 < 10 ? '0' + mm7 : mm7;
                        var yyyy7 = dateClock7.getFullYear();
                        var dateFormat7 = day7 + "/" + month7 + "/" + yyyy7;
    
                        $scope.mVehicleData.STNKDate = dateFormat7;
                    }

                   
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
            if (row.crm != undefined) {
                $scope.actionButtonSettingsDetail[0].visible = false;
                $scope.show.xtab = true;
                vehicleData.getUser(row.VehicleId).then(
                    function (res) {
                        // console.log("res user", res);
                        
                        for (var i=0; i<res.data.Result.length; i++) {
                            res.data.Result[i].StatusPengguna = 0;
                            if (res.data.Result[i].Name == null && res.data.Result[i].RoleCustomer == 'Pengguna' && res.data.Result[i].Status == 1){
                                res.data.Result[i].StatusPengguna = 1;
                            }
                            else if (res.data.Result[i].RoleCustomer == 'Pemilik'){
                                res.data.Result[i].StatusPengguna = 1;
                            }
                            else{
                                res.data.Result[i].StatusPengguna = 0;
                            }
                        }
                        $scope.gridCData.data = res.data.Result;
                        console.log("$scope.gridCData.data", $scope.gridCData.data);

                        
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );
            }

            var xSDate = '1900-01-01';
            
            $scope.xEDate = new Date();
            console.log("SINI WOY",$scope.xEDate);
            var dd70 = $scope.xEDate.getDate();
            var day70 = dd70 < 10 ? '0' + dd70 : dd70;
            var mm70 = $scope.xEDate.getMonth() + 1;
            var month70 = mm70 < 10 ? '0' + mm70 : mm70;
            var yyyy70 = $scope.xEDate.getFullYear();
            
            $scope.xEDate = yyyy70 + "-" + month70 + "-" + day70;


            VehicleHistory.getData(2, row.VIN, xSDate, $scope.xEDate).then(
                function (res) {
                    for(var i in res.data){
                        var tmpArray = [];
                        for(var j = 0; j<res.data[i].VehicleJobService.length; j++){
                            if(res.data[i].VehicleJobService[j].KTPNo == null || res.data[i].VehicleJobService[j].KTPNo == undefined){
                                res.data[i].VehicleJobService[j].KTPNo = ''
                            }
                            if(!(res.data[i].VehicleJobService[j].KTPNo.includes('/'))){
                                if(res.data[i].VehicleJobService[j].EmployeeName !== null){
                                    tmpArray.push(res.data[i].VehicleJobService[j].EmployeeName);
                                }
                            }
                            
                        }
                        tmpArray = $scope.getUnique(tmpArray);
                        res.data[i].namaTeknisi = tmpArray.join(', ');
                    }
                    $scope.mDataVehicleHistory = res.data;

                    console.log("mDataVehicleHistory=>", $scope.mDataVehicleHistory);
                },
                function (err) {
                }
            );

            vehicleData.getOwner(row.VehicleId).then(
                function (res) {
                    for (var i=0; i<res.data.Result.length; i++) {
                        res.data.Result[i].tanggal = "";
                        res.data.Result[i].tanggalconv = new Date (res.data.Result[i].EndDate);
                        
                        if(res.data.Result[i].EndDate == null ||  res.data.Result[i].EndDate == undefined ||  res.data.Result[i].EndDate == '0000-12-31T16:52:48.000Z'){
                            console.log("apaan nih",res.data.Result[i].EndDate);
                            res.data.Result[i].tanggal = "Saat Ini";
                        }
                        else{
                            res.data.Result[i].tanggal = res.data.Result[i].EndDate;
                        }
                    }
                    
                    $scope.gridVehicleOwner = res.data.Result;
                    console.log('$scope.gridVehicleOwner ',$scope.gridVehicleOwner);
                    // $scope.gridHOwner.data = res.data.Result;
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
            var salesInfo =
                [
                    {
                        information: "Branch Good Receive Date",
                        value: "",
                        key: "EstimateBranchReceivedDate"
                    },
                    {
                        information: "Name Saleforce",
                        value: "",
                        key: "OutletId"
                    },
                    {
                        information: "Tanggal Pengiriman yang dijanjikan",
                        value: "",
                        key: "ActualPDD"
                    },
                    {
                        information: "Tanggal Pengiriman Aktual",
                        value: "",
                        key: "SentDate"
                    },
                    {
                        information: "Asuransi yang digunakan",
                        value: "",
                        key: "InsuranceName"
                    }
                ];
            
                $scope.getUnique = function (array){
                    var uniqueArray = [];
                    
                    // Loop through array values
                    for(i=0; i < array.length; i++){
                        if(uniqueArray.indexOf(array[i]) === -1) {
                            uniqueArray.push(array[i]);
                        }
                    }
                    return uniqueArray;
                }

            // vehicleData.getVSaleInfo(row.VehicleId).then(
            //     function (res) {
            //         console.log("res getVSaleInfo", res);
            //         $scope.fsalesInfo = res.data.Result[0];
            //         var aD = new Date(res.data.Result[0].ActualPDD);
            //         var bD = new Date(res.data.Result[0].EstimateBranchReceivedDate);
            //         var cD = new Date(res.data.Result[0].SentDate);
            //         $scope.fsalesInfo.ActualPDD = (aD.getDate() + " " + mthNames[aD.getMonth()] + " " + aD.getFullYear());
            //         $scope.fsalesInfo.EstimateBranchReceivedDate = (bD.getDate() + " " + mthNames[bD.getMonth()] + " " + bD.getFullYear());
            //         $scope.fsalesInfo.SentDate = (cD.getDate() + " " + mthNames[cD.getMonth()] + " " + cD.getFullYear());

            //         // var uSaleInfo1 = formatDate(res.data.Result[0].EstimateBranchReceivedDate);
            //         // var uSaleInfo2 = formatDate(res.data.Result[0].ActualPDD);
            //         // var uSaleInfo3 = formatDate(res.data.Result[0].SentDate);
            //         // console.log("uSaleInfo", uSaleInfo);
            //         // res.data.Result[0].EstimateBranchReceivedDate = uSaleInfo1;
            //         // res.data.Result[0].ActualPDD = uSaleInfo2;
            //         // res.data.Result[0].SentDate = uSaleInfo3;

            //         // for (var i = 0; i < salesInfo.length; i++) {
            //         //     var cc = salesInfo[i].key;
            //         //     // console.log("====>",res.data.Result[0][cc]);
            //         //        salesInfo[i].value = res.data.Result[0][cc];
            //         //  }
            //         // $scope.gridInfoSales.data = salesInfo;

            //         // console.log("$scope.gridInfoSales =>", $scope.gridInfoSales);

            //         // console.log("salesInfo", salesInfo);
            //         // $scope.dataService = res.data.Result;
            //         // $scope.varTypeData = res.data.Result;
            //         // $scope.loading=false;
            //         // return res.data.Result;
            //     },
            //     function (err) {
            //         console.log("err=>", err);
            //     }
            // );

            //getVTamInfo mVehicleTamInfo
            vehicleData.getVTamInfo(row.VehicleId).then(
                function (res) {
                    console.log("res mVehicleTamInfo", res.data.Result[0]);
                    //$scope.gridHOwner.data = res.data.Result;
                    $scope.mVehicleTamInfo = res.data.Result[0];
                },
                function (err) {
                    console.log("err=>", err);
                }
            );

            $scope.hidetab = true;
            $scope.hdetail = false;

        }

        $scope.detailService = function (val) {
            console.log("detailService", val);
            $scope.hdetService = false;
            $scope.hdetail = true;

            // $scope.DServiceId = val;

            // $scope.mDetail = val;

            vehicleData.getHistoryDetail(val).then(
                function (res) {
                    console.log("res HistoryDetail", res);
                    $scope.mDetail = res.data.Result[0];
                    // $scope.dDetail = res.data.Result[0].JobTask;
                    // console.log("dDetail",dDetail);
                },
                function (err) {
                    console.log("err=>", err);
                }
            );


            // mDataVehicleHistory
        };

        // $scope.editOwner = function(val){
        //         // console.log("edit Owner", val);
        //         $scope.heditOwner = false;
        //         $scope.hdetail = true;
        //         $scope.dimmerShow=true;
        //         $scope.mVehicleOwner = val;

        // };

        $scope.updateSTNK = function () {
            console.log("sampai updateSTNK");
            $scope.hupdSTNK = false;
            $scope.xubah = true;
            $scope.xbatal = false;
            $scope.xsimpan = false;
            // $scope.hdetail = true;

            // $scope.DServiceId = val;

            // disini
        };

        $scope.batalSTNK = function () {
            console.log("sampai batalSTNK");
            $scope.hupdSTNK = true;
            $scope.xubah = false;
            $scope.xbatal = true;
            $scope.xsimpan = true;
        };

        $scope.simpanSTNK = function () {
            console.log("sampai simpanSTNK", $scope.mVehicleData);
            $scope.hupdSTNK = true;
            $scope.xubah = false;
            $scope.xbatal = true;
            $scope.xsimpan = true;
            var xUpdate = {};
            xUpdate.VehicleId = $scope.mVehicleData.VehicleId;
            xUpdate.STNKName = $scope.mVehicleData.STNKName;
            xUpdate.STNKAddress = $scope.mVehicleData.STNKAddress;
            xUpdate.STNKDate = $scope.mVehicleData.STNKDate;

            vehicleData.updateSTNK(xUpdate).then(
                function (res) {
                    console.log("succes==>", res);
                    // $scope.mVehicleData = res.data.Result[1];
                    // $scope.mVehicleData.mSearch = "1";
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
        };

        $scope.detBack = function () {
            $scope.hdetService = true;
            $scope.hdetail = false;
            $scope.heditOwner = true;
            $scope.dimmerShow = false;
        }

        $scope.filter = function () {

            $scope.hfilter = false;
            //$scope.getData();
            vehicleData.getData().then(
                function (res) {
                    $scope.mVehicleData = res.data.Result[1];
                    // $scope.mVehicleData.mSearch = "1";
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.gridCData = {
            // enableGridMenu: true,
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableFullRowSelection: true,
            enableRowHeaderSelection: false,
            enableFiltering: true,
            enableSelectAll: false,
            // data: 'xgridData',

            columnDefs: [
                // { name:'id', field: 'id',width:'15%', enableHiding : false, visible: false },
                // { name:'', field: 'xstat', width:'5%', cellTemplate:'<input type=\"checkbox\" ng-model=\"COL_FIELD\" ng-true-value=\'"Y"\' ng-false-value=\'"N"\'/>'},
                { name: '', field: 'Status', width: '3%', type: 'boolean', cellTemplate: '<div> <i ng-if="row.entity.Status == 1" class="glyphicon glyphicon-record" style="color:green; margin:8px;"></i> </div>' },  //'<input type="checkbox" ng-model="row.entity.xstat" disabled>'
                // { name:'', field: 'xstat', width:'3%', type: 'boolean', cellTemplate: cAddress},//<input type="checkbox" ng-model="row.entity.xstat" disabled>
                //<div ng-if=\"row.entity.xstat != false\"><input type="checkbox" ng-model="row.entity.xstat" disabled></div>
                //  fa fa-square-o
                { name: 'Toyota id', field: 'ToyotaVehicleId', enableHiding: false, visible: false },
                { name: 'Nama Pengguna', field: 'Name', enableHiding: false },
                // { name:'Kategori Kendaraan', field: 'PCategoryVehicle.VehicleCategoryName',  width:'17%', enableHiding : false},
                { name: 'Peran Pengguna', field: 'RoleCustomer', width: '17%', enableHiding: false },
                { name: 'No. Handphone', field: 'Phone', enableHiding: false },
                { name: 'Hubungan Pengguna', field: 'Relation', width: '17%', enableHiding: false },
                // { name: 'Ubah', width:95, pinnedRight:true, cellTemplate: '<button class="btn btn-info" ng-click="grid.appScope.editOwner(row.entity)" value="">Ubah</button>'}
            ]
        };

        $scope.gridInfoSales = {
            // enableGridMenu: true,
            enableSorting: true,
            enableRowSelection: true,
            enableFullRowSelection: true,
            enableRowHeaderSelection: false,
            enableFiltering: true,
            enableSelectAll: false,
            enablePaginationControls: false,
            showGridFooter: false,
            showColumnFooter: false,
            gridFooterHeight: 20,
            //data: 'gridDataInfoSales',

            columnDefs: [
                { name: 'id', field: 'SalesInfoId', width: '15%', enableHiding: false, visible: false },
                { name: 'Jenis Informasi', field: 'information', enableHiding: false },
                { name: 'Keterangan', field: 'value', enableHiding: false }
            ]
        };

        $scope.gridHOwner = {
            // enableGridMenu: true,
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            // enableFullRowSelection: true,
            // enableRowHeaderSelection: false,
            // selectionRowHeaderWidth: 35,
            enableFiltering: true,
            enableSelectAll: false,
            // showSelectionCheckbox: true,
            // selectedItems: $scope.mySelections1,
            //data: 'gridDataHOwner',

            columnDefs: [
                // { name: 'a', width:'5%', cellTemplate: '<input type:"checkbox" value:"grid.getCellValue(row,col)"> '},
                // cellTemplate: '<input type="checkbox" ng-model="row.entity.dude" ng-click="toggle(row.entity.name,row.entity.dude)">'
                // { name:'', field:'x', width:'3%', type: 'boolean',  cellTemplate: '<input ng-model="mVehicleData.sservis" ng-click="onSelectRows()" type="checkbox">'},
                { name: 'id', field: 'CustomerVehicleId', width: '15%', enableHiding: false, visible: false },
                { name: 'VehicleId', field: 'VehicleId', enableHiding: false, visible: false },
                { name: 'CurrentStatus', field: 'CurrentStatus', enableHiding: false, visible: false },
                { name: 'Toyota ID', field: 'ToyotaId', enableHiding: false, visible: false },
                { name: 'CustomerTypeId', field: 'CustomerTypeId', enableHiding: false, visible: false },
                { name: 'Name', field: 'Name', enableHiding: false },
                { name: 'Awal Kepemilikan', field: 'StartDate', type: 'date', cellFilter: 'date:\'d MMMM yyyy\'', enableHiding: false },
                { name: 'Akhir Kepemilikan', field: 'EndDate', type: 'date', cellFilter: 'date:\'d MMMM yyyy\'', enableHiding: false }
            ]
        };

        $scope.gridHOwner.onRegisterApi = function (gridApi) {
            // set gridApi on $scope
            $scope.gridApi = gridApi;

            $scope.gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                // console.log("selected=>",$scope.selectedRows);
                if ($scope.onSelectRows) {
                    $scope.onSelectRows($scope.selectedRows);
                }
            });
        }

        $scope.gridInfoService = {
            // enableGridMenu: true,
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableFullRowSelection: true,
            enableRowHeaderSelection: false,
            enableFiltering: true,
            enableSelectAll: false,
            data: 'gridDataInfoService',

            columnDefs: [
                { name: 'JobId', field: 'JobId', width: '15%', enableHiding: false, visible: false },
                { name: 'VehicleId', width: 120, field: 'VehicleId', visible: false },
                { name: 'Kategori', width: 100, field: 'Process.Name', enableHiding: false },
                { name: 'Tanggal Servis', width: 120, field: 'JobDate', type: 'date', cellFilter: 'date:\'d MMMM yyyy\'', enableHiding: false },
                { name: 'Jenis Servis', width: 150, field: 'JobType', enableHiding: false },
                { name: 'Lokasi Servis', width: 120, field: 'OutletId', enableHiding: false },
                { name: 'Service Advisor', width: 130, field: 'EmployeeSa', enableHiding: false },
                { name: 'Kilometer', width: 90, field: 'Km', enableHiding: false },
                { name: 'Job Suggest', width: 200, field: 'JobSuggest', enableHiding: false },
                { name: 'detail', width: 95, pinnedRight: true, cellTemplate: '<button ng-click="grid.appScope.detailService(row.entity.JobId)" value="">detail</button>' }
            ]
        };

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            // data: 'vehicleData',
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name:'id',    field:'id', width:'7%' },

                { name: 'Nomor Rangka', width: 170, field: 'VIN' },  // , cellTemplate:'<a ng-click="grid.appScope.btnNoRangka()">{{row.entity.NoRangka}}</a>'
                { name: 'Nomor Polisi', field: 'LicensePlate' },
                { name: 'Nomor Mesin', field: 'EngineNo', visible: false },
                { name: 'Model Kendaraan', field: 'MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelName' },
                { name: 'Tipe Kendaraan', field: 'MVehicleTypeColor.MVehicleType.Description' },
                { name: 'Tahun Rakit', field: 'AssemblyYear' },
                { name: 'Tanggal Produksi', field: 'ProdDate', type: 'date', cellFilter: 'date:\'d MMMM yyyy\'' },

                // { name:'open',  field: 'status',
                //   cellTemplate: '<label style="margin-top:5px;margin-left:5px;">'+
                //                     '<input type="checkbox"'+
                //                         ' class="checkbox style-0"'+
                //                         ' onclick="this.checked=!this.checked;"'+
                //                         ' ng-true-value="1"'+
                //                         ' ng-false-value="0"'+
                //                         ' ng-model="row.entity.status"'+
                //                         ' ng-checked="row.entity.status==1"'+
                //                         '/>'+
                //                     '<span></span>'+
                //                 '</label>'
                // },
                // { name:'desc',  field: 'desc' },
                // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
            ]
        };


        // $scope.gridKendaraan = {
        //     enableSorting: true,
        //     enableRowSelection: true,
        //     multiSelect: true,
        //     enableSelectAll: true,
        //     //showTreeExpandNoChildren: true,
        //     // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        //     // paginationPageSize: 15,
        //     columnDefs: [{
        //             name: 'Toyota ID',
        //             displayName: 'Nama Pengguna',
        //             field: 'Name',
        //             width: 200,
        //             // width: '7%'
        //         },
        //         {
        //             name: 'Peran Pengguna(tes)',
        //             field: 'RoleCustomer',
        //             width: 200,
        //             // width: '7%'
        //         },
        //         {
        //             name: 'No Telepon(tes)',
        //             field: 'VehicleId',
        //             width: 200,
        //             // width: '7%'
        //         },
        //         {
        //             name: 'Hubungan Pengguna (tes)',
        //             field: 'Relation',
        //             width: 200,
        //             // width: '9%'
        //         }
        //     ]
        // };
    });
