angular.module('app')
  .factory('vehicleData', function($http, CurrentUser, $q ) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      // getData: function() {
      //   var res=$http.get('/api/as/VehicleData');
      //   console.log('res1234567890=>',res);
      //   //res.data.Result = null;
      //   return res;
      // },
      getData: function() {
        // var res=$http.get('/api/fw/Role');

        var defer = $q.defer();
             defer.resolve(
            {
              "data":{ 
                "Result": [
                  {
                    "NoRangka": "MHFM1BA3JBK118253",
                    "NoPolisi": "B 1271 XYZ",
                    "NoMesin": "K3DD47192",
                    "VehicleModel": "Avanza",
                    "VehicleType": "All New 1.2 G A/T",
                    "FullModel": "F601RM-GMMFJJ10",
                    "ColorCode": "T040",
                    "Color": "Hitam",
                    "TahunRakit": 2015,
                    "KeyNumber": 68842,
                    "TipePerakitan": "CBU/CKD",
                    "NamaCabang": "TAM - Sunter",
                    "NamaSTNK": "Anita Wulandari",
                    "AlamatSTNK": "Jl. Veteran III Ciawi, Bogor",
                    "tanggalSTNK": "2016-08-30T13:42:33.34",
                    "tanggalDEC": "2016-08-30T13:42:33.34",
                    "statusGantiPemilik": 1,
                    "TechnicalSpecification": "tes TechnicalSpecification",
                    "ProductionLineOffDate": "2016-08-30T13:42:33.34",
                    "IssueOrRepairData": "Machine Issue",
                    "IssueOrRepairDate": "2016-08-30T13:42:33.34",
                    "wholesaleDODate": "2016-08-30T13:42:33.34",
                    "PDCGoodIssueDate": "2016-08-30T13:42:33.34",
                    "BranchOwner": "Auto 2000",
                    "swappingHistory": "tunas",
                    "tanggalProduksi":"2016-08-30T13:42:33.34",
                    "LastModifiedDate": "2016-08-30T13:42:33.34",
                    "LastModifiedUserId": -1
                  }
                ],

                "Result1": [
                  { "id": 1, 
                    "xstat": 1, 
                    "toyotaId": "1234567890-1", 
                    "name": "Anita", 
                    "xcategory":"Individu", 
                    "RoleId":"Pemilik", 
                    "phone":"0856993393",
                    "relation":""
                  }
                ],
                "Result2": [
                  { "VehicleUserId": 1, 
                    "ToyotaId": "1234567890-1", 
                    "CustomerListdotName": "Anita",   
                    "StartDate":"Januari 2016", 
                    "EndDate":"Saat Ini"
                  }
                ],
                "Result3": [
                  { "id": 1, 
                    "InformationName": "Branch Good Recieve Date", 
                    "Description": "12/01/2016"
                  }
                ],
                "Result4": [
                  { "serviceId": 1, 
                    "ToyotaId": "1234567890-1", 
                    "kategori": "BP", 
                    "dataeservice": "12/12/2016", 
                    "jenisservis":"Perbaikan Pintu Kanan", 
                    "lokasi": "Auto2000 Sunter", 
                    "advisor": "Mark", 
                    "km": "40.129", 
                    "jobsuggest":""
                  }                        
                ],

                      
                "Start": 0,
                "Limit": 10,
                "Total": 3
            }}
              );
              console.log('ressss=>',defer.promise);
             return defer.promise;
        //console.log('res=>',res);
        //res.data.Result = null;
        // return res;
      },

      getList: function() { // belom tau haha
        var res =  $http.get('/api/as/GetVehicleList/1');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc factory=>", res);
        return res;
      },

      getVSaleInfo: function(vehId) {
        var res =  $http.get('/api/ct/GetCDataPenjualan/'+vehId);
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc factory=>", res);
        return res;
      },

      getVTamInfo: function(vehId) {
        var res = $http.get('/api/ct/GetCInfoDataTAM/'+vehId);
        // var res =  $http.get('/api/as/GetVTamInfo/1');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc factory=>", res);
        return res;
      },

      // getCVehicle: function() {
      //   var res =  $http.get('/api/as/GetCVehicle/1');
      //   // var resUsr =  $http.get('/api/as/VehicleUser');
      //   console.log("resSvc factory=>", res);
      //   return res;
      // },

      getVModel: function() {
        var res =  $http.get('/api/ct/GetCVehicleModel/');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc Model factory=>", res);
        return res;
      },

      getVType: function() {
        var res =  $http.get('/api/ct/GetCVehicleType/');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc Type factory=>", res);
        return res;
      },

      getListSpecific: function(vin,lp) {
        console.log("Get Spesifik==>",vin,"==>",lp);
        var res =  $http.get('/api/ct/GetVehicleListSpecific/'+vin+'/'+lp+'/');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc List Spesifik=>", res);
        return res;
      },
      //40api/ct/GetVehicleListGroup/2016/13/40/-/-
      getListGroup: function(aYear,vTypeId,vModelId,prodDate1,prodDate2) {
        // console.log("Get Spesifik==>",vin,"==>",lp);
        var res =  $http.get('/api/ct/GetVehicleListGroup/'+aYear+'/'+vTypeId+'/'+vModelId+'/'+prodDate1+'/'+prodDate2+'/');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc List Spesifik=>", res);
        return res;
      },
      //http://localhost:38005/api/as/jobs/WoHistoryByVehicle/FirstDate/2017-04-25/LastDate/2017-04-30/vehicleId/1
      getHistory: function(xSDate,xEDate,vId) {
        var res =  $http.get('/api/ct/jobs/WoHistoryByVehicle/FirstDate/'+xSDate+'/LastDate/'+xEDate+'/vehicleId/'+vId+'/');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc List Spesifik=>", res);
        return res;
      },

      getHistoryDetail: function(jobId) {
        var res =  $http.get('/api/ct/Jobs/'+jobId);
        // console.log("resSvc HistoryDetail factory=>", res);
        return res;
      },

      getListById: function(vId) {
        var res =  $http.get('/api/ct/GetVehicleListById/'+vId+'/');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc List Spesifik=>", res);
        return res;
      },

      getOwner: function(vehId) {
        var res =  $http.get('/api/ct/GetListVehicleOwnerSP/'+vehId);
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc Owner factory=>", res);
        return res;
      },
      getUser: function(vehId) {
        var res =  $http.get('/api/ct/GetListVehicleUserSP/'+vehId+'/-');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc User factory=>", res);
        return res;
      },

      getEmployeeCertifications: function(ktp) {
        var res =  $http.get('/api/ct/GetEmployeeCertification/'+ktp+'/');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc User factory=>", res);
        return res;
      },

      getEmployeeTrainings: function(ktp) {
        var res =  $http.get('/api/ct/GetEmployeeTraining/'+ktp+'/');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc User factory=>", res);
        return res;
      },

      //api/ct/GetCInfoDataTAM/1

      getUser2: function() {
        // var res =  $http.get('/api/as/GetVehicleUser/1');
        
        // console.log("resSvc factory=>", res);
        // return res;
        var defer = $q.defer();
             defer.resolve(
            {
              "data":{ 
                "Result1": [
                  { "id": 1, 
                    "xstat": 1, 
                    "ToyotaId": "897986", 
                    "name": "Anita", 
                    "xcategory":"Individu", 
                    "RoleId":"Pemilik", 
                    "phone":"0856993393",
                    "relation":"Anak"
                  }
                ],

                      
                "Start": 0,
                "Limit": 10,
                "Total": 3
            }}
              );
              console.log('ressss=>',defer.promise);
             return defer.promise;
      },

      updateUser: function(vUser) {
        var res =  $http.get('/api/as/PutVehicleUser',[{
                                                        CategoryId: vUser.CategoryId,
                                                        RoleId: vUser.RoleId,
                                                        Phone: vUser.Phone
                                                      }]);
        // var resUsr =  $http.get('/api/as/VehicleUser');
        
        //return res;
      },

      getDataSalesHistoryKendaraan: function (vin) {
        var res = $http.get('/api/ct/GetCRM_SalesHistoryKendaraan?VIN=' + vin);
        return res
      },

      

      // getTamInfo: function(){
      //   var res = $http.get('/api/as/VehicleTamInfo');
      //   console.log("resTamInfo factory=>", res);
      //   return res;
      // }
      // create: function(role) {
      //   return $http.post('/api/fw/Role', [{
      //                                       AppId: 1,
      //                                       ParentId: 0,
      //                                       Name: role.Name,
      //                                       Description: role.Description}]);
      // },
      // update: function(role){
      //   return $http.put('/api/fw/Role', [{
      //                                       Id: role.Id,
      //                                       //pid: role.pid,
      //                                       Name: role.Name,
      //                                       Description: role.Description}]);
      // },
      // delete: function(id) {
      //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      // },
    }
  });
