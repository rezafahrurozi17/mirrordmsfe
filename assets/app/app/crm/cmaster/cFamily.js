angular.module('app')
  .factory('CFamily', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      
      getDataFamily: function() {
        var res=$http.get('/api/crm/GetCListFamily');
        
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      getFamilyRelation: function() {
        var res=$http.get('/api/crm/GetCFamilyRelation');
        
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      
      
    }
  });
