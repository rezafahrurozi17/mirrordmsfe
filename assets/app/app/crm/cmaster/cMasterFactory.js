angular.module('app')
  .factory('CMaster', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getGender: function() {
        var res=$http.get('/api/ct/GetGender');
        
        console.log('hasil=>',res);

        //res.data.Result = null;
        return res;
      },
      
      getReligion: function() {
        var res=$http.get('/api/ct/GetCCustomerReligion/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      getEthnic: function() {
        var res=$http.get('/api/ct/GetCustomerEthnic/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      }, 

      getCCategory: function() {
        var res=$http.get('/api/ct/GetCCustomerCategory/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      }, 

      getCStatusMarital: function() {
        var res=$http.get('/api/ct/GetCCustomerStatusMarital/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetCCustomerFleet: function() {
        var res=$http.get('/api/ct/GetCCustomerFleet');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetCLanguange: function() {
         var res=$http.get('/api/ct/GetCCustomerLanguage/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetCSectorBisnis: function() {
        var res=$http.get('/api/ct/GetCCustomerBusiness/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      }, 


      GetCPosition: function() {
        var res=$http.get('/api/ct/GetCCustomerPosition/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

        
      GetCGrossIncome: function() {
        var res=$http.get('/api/ct/GetCRangeGrossIncome');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      
      GetCAddressCategory: function() {
        var res=$http.get('/api/ct/GetCAddressCategory/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      GetCJobCategory: function() {
        var res=$http.get('/api/ct/GetJobCategory/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },


      


      GetCProvince: function() {
        var res=$http.get('/api/sales/MLocationProvince/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      
      GetCRegency: function(xdata) {
        var res=$http.get('/api/sales/MLocationCityRegency?start=1&limit=100&filterData=ProvinceId|'+xdata);
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetCDistrict: function(xdata) {
        var res=$http.get('/api/sales/MLocationKecamatan?start=1&limit=100&filterData=CityRegencyId|'+xdata);
    
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;

      },

      GetCVillage: function(xdata) {
        var res=$http.get('/api/sales/MLocationKelurahan?start=1&limit=100&filterData=DistrictId|'+xdata);
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      }, 
      GetLocation: function() {
        var res=$http.get('/api/ct/GetLocation/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      }, 

      //GetDepartment
      GetDepartment: function(){
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetDepartment/');
        return res;
      },


      getDealOut: function() {
        var res=$http.get('/api/fw/Organization/Tree?childOnly=1');
        
        console.log('hasil getDealOut=>',res);

        //res.data.Result = null;
        return res;
      },

      GetCFamilyRelation: function() {
        var res=$http.get('/api/ct/GetCFamilyRelation/');
          

        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      }, 
      //   {

      
      GetCVhehicleBrand: function() {
        var res=$http.get('/api/ct/GetCBrandVehicle/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      GetCVhehicleCategory: function() {
        var res=$http.get('/api/ct/GetCCategoryVehicle/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },




      GetCMasterPreference: function() {
        var res=$http.get('/api/ct/GetCMasterPreference/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      

      GetCMasterSocialMedia: function() {
        var res=$http.get('/api/ct/GetCMasterSocialMedia/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

	//vehicle
      GetCVehicleModel: function() {
        var res=$http.get('/api/ct/GetCVehicleModel/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      GetBasicDataOutlet: function() {
        var res=$http.get('/api/ct/GetCustomerBasicTab/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      }

      
      
    }
  });


// var res=[
      //           {
      //             "CustomerGenderId": 1,
      //             "CustomerGenderName": "Setengah Lelaki",
      //             "StatusCode": 0,
      //             "LastModifiedDate": "2017-04-06T22:04:55.34",
      //             "LastModifiedUserId": 1
      //           },
      //           {
      //             "CustomerGenderId": 2,
      //             "CustomerGenderName": "Perempuan",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2017-04-06T19:51:59.14",
      //             "LastModifiedUserId": 1
      //           }
      //         ]
      // var res={
      //         "Result": [
      //           {
      //             "CustomerReligionId": 1,
      //             "CustomerReligionName": "Islam",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2017-04-06T19:55:25.747",
      //             "LastModifiedUserId": 1
      //           },
      //           {
      //             "CustomerReligionId": 2,
      //             "CustomerReligionName": "Kristen",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2017-04-06T19:55:28.86",
      //             "LastModifiedUserId": 1
      //           },
      //           {
      //             "CustomerReligionId": 3,
      //             "CustomerReligionName": "Budha",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2017-04-06T19:55:31.02",
      //             "LastModifiedUserId": 1
      //           },
      //           {
      //             "CustomerReligionId": 4,
      //             "CustomerReligionName": "Hindu",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2017-04-06T19:55:33.333",
      //             "LastModifiedUserId": 1
      //           }
      //         ],
      //         "PageNo": 1,
      //         "PageSize": 0,
      //         "PageCount": 1,
      //         "Total": 4
      //       };
      // var res={
      //     "Result": [
      //       {
      //         "CustomerEthnicId": 1,
      //         "CustomerEthnicName": "Sunda",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:02:57.987",
      //         "LastModifiedUserId": 1
      //       },
      //       {
      //         "CustomerEthnicId": 2,
      //         "CustomerEthnicName": "Jawa",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:03:00.22",
      //         "LastModifiedUserId": 1
      //       },
      //       {
      //         "CustomerEthnicId": 3,
      //         "CustomerEthnicName": "Betawi",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:03:13.77",
      //         "LastModifiedUserId": 1
      //       },
      //       {
      //         "CustomerEthnicId": 4,
      //         "CustomerEthnicName": "Ambon",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:03:16.84",
      //         "LastModifiedUserId": 1
      //       },
      //       {
      //         "CustomerEthnicId": 5,
      //         "CustomerEthnicName": "Minang",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:03:21.59",
      //         "LastModifiedUserId": 1
      //       },
      //       {
      //         "CustomerEthnicId": 6,
      //         "CustomerEthnicName": "Papua",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:03:26.113",
      //         "LastModifiedUserId": 1
      //       }
      //     ],
      //     "PageNo": 1,
      //     "PageSize": 0,
      //     "PageCount": 1,
      //     "Total": 6
      //   };
// var res={
      //       "Result": [
      //         {
      //           "FleetId": 1,
      //           "Fleet": "Non Fleet",
      //           "StatusCode": null,
      //           "LastModifiedDate": null,
      //           "LastModifiedUserId": null
      //         },
      //         {
      //           "FleetId": 2,
      //           "Fleet": "Fleet",
      //           "StatusCode": null,
      //           "LastModifiedDate": null,
      //           "LastModifiedUserId": null
      //         }
      //       ],
      //       "PageNo": 1,
      //       "PageSize": 0,
      //       "PageCount": 1,
      //       "Total": 2
      //     };
      // var res={
      //       "Result": [
      //         {
      //           "CustomerLanguageId": 1,
      //           "CustomerLanguageName": "Indonesia",
      //           "StatusCode": 1,
      //           "LastModifiedDate": "2017-04-06T20:03:50.383",
      //           "LastModifiedUserId": 1
      //         },
      //         {
      //           "CustomerLanguageId": 2,
      //           "CustomerLanguageName": "english",
      //           "StatusCode": 1,
      //           "LastModifiedDate": "2017-04-06T20:03:55.153",
      //           "LastModifiedUserId": 1
      //         }
      //       ],
      //       "PageNo": 1,
      //       "PageSize": 0,
      //       "PageCount": 1,
      //       "Total": 2
      //     };
      // var res={
      //       "Result": [
      //         {
      //           "SectorBusinessId": 1,
      //           "SectorBusinessName": "Teknologi",
      //           "StatusCode": 1,
      //           "LastModifiedDate": "2017-04-06T19:56:35.537",
      //           "LastModifiedUserId": 1
      //         },
      //         {
      //           "SectorBusinessId": 2,
      //           "SectorBusinessName": "Retail",
      //           "StatusCode": 1,
      //           "LastModifiedDate": "2017-04-06T20:34:38.667",
      //           "LastModifiedUserId": 1
      //         },
      //         {
      //           "SectorBusinessId": 3,
      //           "SectorBusinessName": "Kosmetik dan Parfum",
      //           "StatusCode": 1,
      //           "LastModifiedDate": "2017-04-06T20:35:19.82",
      //           "LastModifiedUserId": 1
      //         },
      //         {
      //           "SectorBusinessId": 4,
      //           "SectorBusinessName": "Fashion",
      //           "StatusCode": 1,
      //           "LastModifiedDate": "2017-04-06T20:35:28.07",
      //           "LastModifiedUserId": 1
      //         }
      //       ],
      //       "PageNo": 1,
      //       "PageSize": 0,
      //       "PageCount": 1,
      //       "Total": 4
      //     };
      // var res = [
      //             {
      //               "CustomerPositionId": 1,
      //               "CustomerPositionName": "CEO",
      //               "StatusCode": 1,
      //               "LastModifiedDate": "2017-04-06T20:36:22.37",
      //               "LastModifiedUserId": 1
      //             },
      //             {
      //               "CustomerPositionId": 2,
      //               "CustomerPositionName": "Manager",
      //               "StatusCode": 1,
      //               "LastModifiedDate": "2017-04-06T20:36:24.597",
      //               "LastModifiedUserId": 1
      //             },
      //             {
      //               "CustomerPositionId": 3,
      //               "CustomerPositionName": "Direktur",
      //               "StatusCode": 1,
      //               "LastModifiedDate": "2017-04-06T20:36:34.9",
      //               "LastModifiedUserId": 1
      //             },
      //             {
      //               "CustomerPositionId": 4,
      //               "CustomerPositionName": "Sales",
      //               "StatusCode": 1,
      //               "LastModifiedDate": "2017-04-06T20:36:38.513",
      //               "LastModifiedUserId": 1
      //             },
      //             {
      //               "CustomerPositionId": 5,
      //               "CustomerPositionName": "Teknisi",
      //               "StatusCode": 1,
      //               "LastModifiedDate": "2017-04-06T20:36:44.36",
      //               "LastModifiedUserId": 1
      //             }
      //           ];
      // var res=[
      //             {
      //               "RangeGrossIncomeId": 1,
      //               "RangeGrossIncomeName": "0 - 1.000.000                 ",
      //               "StatusCode": 1,
      //               "LastModifiedDate": "2017-04-06T20:47:31.45",
      //               "LastModifiedUserId": 1
      //             },
      //             {
      //               "RangeGrossIncomeId": 2,
      //               "RangeGrossIncomeName": "1.000.000 - 5.000.000.000",
      //               "StatusCode": 1,
      //               "LastModifiedDate": "2017-04-06T20:47:44.037",
      //               "LastModifiedUserId": 1
      //             },
      //             {
      //               "RangeGrossIncomeId": 3,
      //               "RangeGrossIncomeName": "5.000.000 - 10.000.000",
      //               "StatusCode": 1,
      //               "LastModifiedDate": "2017-04-06T20:47:57.06",
      //               "LastModifiedUserId": 1
      //             }
      //           ];
      // var res=[
      //             {
      //               "AddressCategoryId": 1,
      //               "AddressCategoryDesc": "Rumah                                             ",
      //               "StatusCode": null,
      //               "LastModifiedDate": null,
      //               "LastModifiedUserId": null
      //             },
      //             {
      //               "AddressCategoryId": 2,
      //               "AddressCategoryDesc": "Kantor Pusat                                      ",
      //               "StatusCode": null,
      //               "LastModifiedDate": null,
      //               "LastModifiedUserId": null
      //             },
      //             {
      //               "AddressCategoryId": 3,
      //               "AddressCategoryDesc": "Kantor Cabang                                     ",
      //               "StatusCode": null,
      //               "LastModifiedDate": null,
      //               "LastModifiedUserId": null
      //             }
      //           ];
      //   var res={
      //         "Result": [
      //           {
      //             "ProvinceId": 2,
      //             "ProvinceName": "Aceh",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:11.85",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 3,
      //             "ProvinceName": "Bangka-Belitung",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:12.79",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 4,
      //             "ProvinceName": "Bengkulu",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:13.727",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 5,
      //             "ProvinceName": "Jambi",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:14.65",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 6,
      //             "ProvinceName": "Kepulauan Riau",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:15.597",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 7,
      //             "ProvinceName": "Lampung",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:16.537",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 8,
      //             "ProvinceName": "Riau",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:17.473",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 9,
      //             "ProvinceName": "Sumatera Barat",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:18.427",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 10,
      //             "ProvinceName": "Sumatera Selatan",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:19.367",
      //             "LastModifiedUserId": -1
      //           },
      //           {
      //             "ProvinceId": 11,
      //             "ProvinceName": "Sumatera Utara",
      //             "StatusCode": 1,
      //             "LastModifiedDate": "2016-10-21T15:12:20.417",
      //             "LastModifiedUserId": -1
      //           }
      //         ],
      //         "PageNo": 1,
      //         "PageSize": 0,
      //         "PageCount": 1,
      //         "Total": 34
      //       };
//         [
  //   {
  //     "DistrictId": 1,
  //     "DistrictName": "Ciawi",
  //     "RegencyId": 2,
  //     "StatusCode": 1,
  //     "LastModifiedDate": "2017-09-09T00:00:00",
  //     "LastModifiedUserId": -1,
  //     "Regency": null
  //   },
  //   {
  //     "DistrictId": 2,
  //     "DistrictName": "Cibinong",
  //     "RegencyId": 2,
  //     "StatusCode": 1,
  //     "LastModifiedDate": "2017-09-09T00:00:00",
  //     "LastModifiedUserId": -1,
  //     "Regency": null
  //   },
  //   {
  //     "DistrictId": 3,
  //     "DistrictName": "Cisarua",
  //     "RegencyId": 1,
  //     "StatusCode": 1,
  //     "LastModifiedDate": "2017-09-09T00:00:00",
  //     "LastModifiedUserId": -1,
  //     "Regency": null
  //   }
  // ]
  //     [
      //       {
      //         "VillageId": 1,
      //         "VillageName": "Banjarwaru",
      //         "DistrictId": 1,
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-09-09T00:00:00",
      //         "LastModifiedUserId": -1,
      //         "District": null
      //       },
      //       {
      //         "VillageId": 2,
      //         "VillageName": "Barnjarsari",
      //         "DistrictId": 2,
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-09-09T00:00:00",
      //         "LastModifiedUserId": -1,
      //         "District": null
      //       },
      //       {
      //         "VillageId": 3,
      //         "VillageName": "Banjarwangi",
      //         "DistrictId": 3,
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-09-09T00:00:00",
      //         "LastModifiedUserId": -1,
      //         "District": null
      //       }
      //     ]  
      //     "FamilyRelationId": 1,
//     "FamilyRelationName": "Suami",
//     "StatusCode": 1,
//     "LastModifiedDate": "2017-04-07T16:25:41.63",
//     "LastModifiedUserId": 1
//   },
//   {
//     "FamilyRelationId": 2,
//     "FamilyRelationName": "Istri",
//     "StatusCode": 1,
//     "LastModifiedDate": "2017-04-07T16:25:56.233",
//     "LastModifiedUserId": 1
//   },
//   {
//     "FamilyRelationId": 3,
//     "FamilyRelationName": "Anak",
//     "StatusCode": 1,
//     "LastModifiedDate": "2017-04-07T16:26:02.67",
//     "LastModifiedUserId": 1
//   },
//   {
//     "FamilyRelationId": 4,
//     "FamilyRelationName": "Ayah",
//     "StatusCode": 1,
//     "LastModifiedDate": "2017-04-07T16:26:15.283",
//     "LastModifiedUserId": 1
//   },
//   {
//     "FamilyRelationId": 5,
//     "FamilyRelationName": "Ibu",
//     "StatusCode": 1,
//     "LastModifiedDate": "2017-04-07T16:26:20.49",
//     "LastModifiedUserId": 1
//   }
// ]
 //       [
      //         {
      //           "BrandId": 3,
      //           "BrandName": "Honda",
      //           "StatusCode": 1,
      //           "LastModifiedDate": "2017-09-09T00:00:00",
      //           "LastModifiedUserId": -1
      //         },
      //         {
      //           "BrandId": 4,
      //           "BrandName": "Suzuki",
      //           "StatusCode": 1,
      //           "LastModifiedDate": "2017-09-09T00:00:00",
      //           "LastModifiedUserId": -1
      //         }
      //       ] 
      //     [
      //       {
      //         "VehicleCategoryId": 1,
      //         "VehicleCategoryName": "Sedan",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:39:48.82",
      //         "LastModifiedUserId": 1
      //       },
      //       {
      //         "VehicleCategoryId": 2,
      //         "VehicleCategoryName": "Truk",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:39:51.3",
      //         "LastModifiedUserId": 1
      //       },
      //       {
      //         "VehicleCategoryId": 3,
      //         "VehicleCategoryName": "Box",
      //         "StatusCode": 1,
      //         "LastModifiedDate": "2017-04-06T20:39:53.263",
      //         "LastModifiedUserId": 1
      //       }
      //     ]  
      //     [
      //     {
      //       "PreferenceId": 1,
      //       "PreferenceName": "Makanan Kesukaan ?",
      //       "StatusCode": 1,
      //       "LastModifiedDate": "2017-04-12T11:18:01.767",
      //       "LastModifiedUserId": 1
      //     }
      //   ]
      //     [
      //     {
      //       "SocialMediaId": 1,
      //       "SocialMediaName": "Twitter",
      //       "StatusCode": 1,
      //       "LastModifiedDate": "2017-04-12T11:23:43.767",
      //       "LastModifiedUserId": 1
      //     }
      //   ]
