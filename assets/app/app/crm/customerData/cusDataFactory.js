angular.module('app')
  .factory('CusData', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getData: function() {
        // var res=$http.get('/api/as/Stall');
        var defer = $q.defer();
             defer.resolve(
            {
                  "data":{
                    "acc1" : {
                      "idToyota" : "1234567891-2",
                      "peran" : 1,
                      "KTP" : 27346587634,
                      "NPWP" : 2537622389,
                      "GelarDepan" : "Prof, Dr, MR. H ",
                      "name" : "Berubah Hairusia",
                      "GelarBelakang" : "Sch, SSh, SBP",
                      "nickName" : "BeHa",
                      "PlaceBirth" : "Semarang",
                      "DateBirth" : "12-12-1212",
                      "JK" : 1,
                      "phoneStatus1" : 0,
                      "phoneStatus2" : 1,
                      "phone1" : 98263876,
                      "phone2" : 82937,
                      "email" : "email@email.com",
                      "agama" : 1,
                      "suku" : 1,
                      "bahasa" : 1,
                      "categoryPelanggan" : 1,
                      "fleet" : 1,
                      "salary" : 1,
                      "job" : "Tidur",
                      "company" : "Central",
                      "position" : "Direktur",
                      "departemen" : "Kira jalan"
                      
                    },
                    "acc2":{
                      "Married":1,
                      "dateMarried":"12-12-1991"
                    },
                    "acc3":{
                      "carCategory" : 1,
                      "carModel":1,
                      "carColour":1,
                      "nopol":"B 2334 PKK",
                      "tahunBeli":2015,
                      "price":293000000
                    },
              "Result": [
                {
                  "contact":1,
                  "used":1,
                  "ContactCategoryName":"Rumah",
                  "add":"Jalan Haji 78",
                  "phone":207348723,
                  "kelurahan":1,
                  "kelurahanName":"Haji",
                  "kecamatan":1,
                  "kecamatanName":"Desa Timur",
                  "rt":"03/08",
                  "KodePos":78676,
                  "kabupaten":1,
                  "kabupatenName":"Ujung Timur",
                  "provinsi":3,
                  "provinsiName":"Jawa Tengah",
                  "xstat":true
                },
                {
                  "contact":2,
                  "used":0,
                  "ContactCategoryName":"Kantor",
                  "add":"Jl Khong guan 89",
                  "phone":207348723,
                  "kelurahan":2,
                  "kelurahanName":"Korakal",
                  "kecamatan":3,
                  "kecamatanName":"Desa Tenggara",
                  "rt":"03/08",
                  "KodePos":78676,
                  "kabupaten":2,
                  "kabupatenName":"Ujung Tengah",
                  "provinsi":1,
                  "provinsiName":"Papua Barat",
                  "xstat":false
                },
                {
                  "contact":1,
                  "used":0,
                  "ContactCategoryName":"Rumah",
                  "add":"Jl Biak 99",
                  "phone":207348723,
                  "kelurahan":3,
                  "kelurahanName":"Lembayung",
                  "kecamatan":2,
                  "kecamatanName":"Desa Tengah",
                  "rt":"03/08",
                  "KodePos":78676,
                  "kabupaten":3,
                  "kabupatenName":"Ujung Tenggara",
                  "provinsi":2,
                  "provinsiName":"Jakarta",
                  "xstat":false
                }
                
              ],
              "ResultFamily": [
                {
                  "CategoryFamily":1,
                  "familyRelationship":"Suami",
                  "name":"Joko",
                  "gender":1,
                  "genderValue":"Pria",
                  "phone":28735472,
                  "address":"Susukan 67 Bogor",
                  "placeBirth":"Semarang",
                  "dateBirht":"Wed Mar 15 2017 07:00:00 GMT+0700 (WIB)",
                  "notes":"hahaha hihihi heheheh"},
                {
                  "CategoryFamily":2,
                  "familyRelationship":"Istri",
                  "name":"Joni",
                  "gender":2,
                  "genderValue":"Wanita",
                  "phone":28735472,
                  "address":"Susukan 67 Bogor",
                  "placeBirth":"Semarang",
                  "dateBirht":"03-09-2013",
                  "notes":"hahaha hihihi heheheh"},
                {
                  "CategoryFamily":1,
                  "familyRelationship":"Suami",
                  "name":"Joko Juga",
                  "gender":1,
                  "genderValue":"Pria",
                  "phone":28735472,
                  "address":"Susukan 67 Bogor",
                  "placeBirth":"Semarang",
                  "dateBirht":"03-09-2013",
                  "notes":"hahaha hihihi heheheh"},
                
              ],
              "ResultPreferensi": [
                {
                  "id":1,
                  "name":"Makanan Favorit",
                  "desc":"Bakso"
                },
                {
                  "id":2,
                  "name":"Minuman Favorit",
                  "desc":"Kuah Bakso"
                },
                {
                  "id":3,
                  "name":"Olahraga Favorit",
                  "desc":"Tidur"
                },
                {
                  "id":4,
                  "name":"Hobby",
                  "desc":"Makan"
                },
                
              ],
              "ResultMedsos": [
                {
                  "id":1,
                  "name":"Facebook",
                  "desc":"@huahaha",
                  "xnote":1
                },
                {
                  "id":2,
                  "name":"Twitter",
                  "desc":"@hihihi",
                  "xnote":0
                },
                {
                  "id":3,
                  "name":"Instagram",
                  "desc":"@hehehe",
                  "xnote":1
                },
                {
                  "id":4,
                  "name":"Youtube",
                  "desc":"-",
                  "xnote":0
                },
                
              ],
              "ResultVehicle": [
                {
                  "id":1,
                  "merk" : 1,
                  "vehicle":"Toyota",
                  "modelId":1,
                  "modelName":"Avanza",
                  "category":1,
                  "categoryName":"Mobil",
                  "type":1,
                  "vehicleTypeName":"All New 1.3 G A/T",
                  "colour":1,
                  "colourName":"Merah",
                  "nopol":"B 1250 GT",
                  "tahun":2015,
                  "price":200000000
                },
                {
                  "id":2,
                  "merk" : 2,
                  "vehicle":"Mazda",
                  "modelId":2,
                  "modelName":"Fortuner",
                  "category":1,
                  "categoryName":"Mobil",
                  "type":1,
                  "vehicleTypeName":"All New 1.3 G A/T",
                  "colour":2,
                  "colourName":"Transparan",
                  "nopol":"B 1111 GT",
                  "tahun":1984,
                  "price":280000000
                },
                {
                  "id":3,
                  "merk" : 1,
                  "vehicle":"Toyota",
                  "modelId":3,
                  "modelName":"Cayla",
                  "category":1,
                  "categoryName":"Mobil",
                  "type":1,
                  "vehicleTypeName":"All New 1.3 G A/T",
                  "colour":1,
                  "colourName":"Merah",
                  "nopo;":"B 1250 GT",
                  "tahun":2011,
                  "price":200000000
                },
                
              ],
              "fieldAction" : [
                {
                    "id":1,
                    "date" : "02/06/2016 10:20:00",
                    "description":"Potensi malfungsi pada Rear Corner Sensor"
                },
                {
                    "id":2,
                    "date" : "09/06/2016 10:20:00",
                    "description":"Potensi malfungsi pada lampu sen kiri belakang"
                },
                {
                    "id":3,
                    "date" : "15/06/2016 10:20:00",
                    "description":"Toyota Warranty Claim"
                },
                {
                    "id":4,
                    "date" : "29/06/2016 10:20:00",
                    "description":"Parts Warranty Claim"
                }
              ],
              "ssc" : [
                {
                    "id":1,
                    "date" : "29/06/2016 10:20:00",
                    "metode":"Call",
                    "xstatus":"Airbag vios 2008"
                },
                {
                    "id":2,
                    "date" : "29/06/2016 10:20:00",
                    "metode":"Call",
                    "xstatus":"Kebakaran vios 2008"
                },
                {
                    "id":4,
                    "date" : "29/06/2016 10:20:00",
                    "metode":"Call",
                    "xstatus":"Masuk Jurang vios 2008"
                }
              ],
              "csl" : [
                {
                    "id":1,
                    "date" : "02/06/2016 10:20:00",
                    "description":"CSL 1"
                },
                {
                    "id":2,
                    "date" : "09/06/2016 10:20:00",
                    "description":"CSL 2"
                },
                {
                    "id":3,
                    "date" : "15/06/2016 10:20:00",
                    "description":"CSL 3"
                },
                {
                    "id":4,
                    "date" : "29/06/2016 10:20:00",
                    "description":"CSL 4"
                }
              ],
              "idi" : [
                {
                    "id":1,
                    "date" : "02/06/2016 10:20:00",
                    "description":"Interview 1"
                },
                {
                    "id":2,
                    "date" : "09/06/2016 10:20:00",
                    "description":"Interview 2"
                },
                {
                    "id":3,
                    "date" : "15/06/2016 10:20:00",
                    "description":"Interview 3"
                }
              ],
              "complain" : [
                {
                    "id":1,
                    "date" : "02/06/2016 10:20:00",
                    "description":"Permintaan"
                },
                {
                    "id":2,
                    "date" : "09/06/2016 10:20:00",
                    "description":"Keluhan: Suara Mesin"
                }
              ],
              "cs" : [
                {
                    "id":1,
                    "date" : "02/06/2016 10:20:00",
                    "description":"Test Drive Survey"
                },
                {
                    "id":2,
                    "date" : "09/06/2016 10:20:00",
                    "description":"DEC CR1 "
                },
                {
                    "id":3,
                    "date" : "02/06/2016 10:20:00",
                    "description":"DEC h3-7"
                },
                {
                    "id":4,
                    "date" : "09/06/2016 10:20:00",
                    "description":"DEC Feedback "
                },
                {
                    "id":5,
                    "date" : "02/06/2016 10:20:00",
                    "description":"PSFU Survey"
                },
                {
                    "id":6,
                    "date" : "09/06/2016 10:20:00",
                    "description":"VOCAS "
                }
              ],
              "ResultLogHistory" : [
                {
                    "type":1,
                    "modul":"Sales",
                    "menu":"Prospecting",
                    "atribut":"Nomor Handphone 1",
                    "newVal":"08123456789",
                    "oldVal":"08123456756",
                    "userName":"Anton",
                    "update" : "02/06/2016 10:20:00"
                    
                },
                {
                    "type":2,
                    "modul":"Sales",
                    "menu":"Prospecting",
                    "atribut":"Nomor Handphone 1",
                    "newVal":"08123452322",
                    "oldVal":"08123456789",
                    "userName":"Tim",
                    "update" : "02/06/2016 10:20:00"
                },
                {
                    "type":1,
                    "modul":"After Sales",
                    "menu":"Reception",
                    "atribut":"Nomor Handphone 2",
                    "newVal":"08123456789",
                    "oldVal":"08123456789",
                    "userName":"Lampard",
                    "update" : "02/06/2016 10:20:00"
                },
                {
                    "type":1,
                    "modul":"Sales",
                    "menu":"Prospecting",
                    "atribut":"Nama Pelanggan",
                    "newVal":"Jono",
                    "oldVal":"Joni",
                    "userName":"Terry",
                    "update" : "02/06/2016 10:20:00"
                }
              ],
              "Start": 0,
              "Limit": 10,
              "Total": 3
            }}
              );
              console.log('ressss=>',defer.promise);
             return defer.promise;

        // console.log('hasil=>',res);
        //res.data.Result = null;
        // return res;
      },
      
      getGroupData: function(data) {
        console.log(' tambah data=>', data);

        var defer = $q.defer();
             defer.resolve(
            {
                  "data":{
              "Result": [
                {
                  "toyotaId":"1234567891-2",
                  "branchName":"Biak",
                  "name":"Sampurasun Individu",
                  "modelName":"Avanza",
                  "noPol":"B 78634 KKK",
                  "noRank":"CHS987345",
                  "typeCustomer":1
                  },
                {
                  "toyotaId":"1234567899-2",
                  "branchName":"Mamuju",
                  "name":"Horas Bah Instistusi",
                  "modelName":"Fortuner",
                  "noPol":"S 11225 SPK",
                  "noRank":"CHS344445",
                  "typeCustomer":2},
                {
                  "toyotaId":"1234567491-3",
                  "branchName":"Natuna",
                  "name":"Ikan",
                  "modelName":"GTR 2000X",
                  "noPol":"N 99999 SPK",
                  "noRank":"CHS344445",
                  "typeCustomer":1}
                
              ],
              "Start": 0,
              "Limit": 10,
              "Total": 3
            }}
              );
              console.log('ressss=>',defer.promise);
             return defer.promise;
      },
      getDataInstitusi: function(data) {
        
        var defer = $q.defer();
             defer.resolve(
            {
                  "data":{
                    "acc21":{
                      "toyotaId":"1234567899-2",
                      "peran":1,
                      "siup":"12.345.678",
                      "tdp":"12.345.678.9-012.345",
                      "npwp":"12.345.678.9-012.345",
                      "name":"PT Maju Mundur Kena",
                      "category":1,
                      "fleet":"Fleet",
                      "bisnis":1,
                      "nameResponsible":"Bambang Adi Nugroho",
                      "phone":8745683874,
                      "jabatan":1,
                      "departemen":"IT Development",
                      "carCategory" : 1,
                      "carModel":1,
                      "carColour":1,
                      "nopol":"B 2334 PKK",
                      "tahunBeli":2015,
                      "price":293000000
                    },
                    "acc3":{
                      "carCategory" : 1,
                      "carModel":1,
                      "carColour":1,
                      "nopol":"B 2334 PKK",
                      "tahunBeli":2015,
                      "price":293000000
                    },
                    "Result": [
                        {
                          "contact":3,
                          "used":1,
                          "ContactCategoryName":"Kantor Pusat",
                          "add":"Jalan Haji 78",
                          "phone":207348723,
                          "kelurahan":1,
                          "kelurahanName":"Haji",
                          "kecamatan":1,
                          "kecamatanName":"Desa Timur",
                          "rt":"03/08",
                          "KodePos":78676,
                          "kabupaten":1,
                          "kabupatenName":"Ujung Timur",
                          "provinsi":3,
                          "provinsiName":"Jawa Tengah",
                          "xstat":true
                        },
                        {
                          "contact":4,
                          "used":0,
                          "ContactCategoryName":"Kantor Cabang",
                          "add":"Jl Khong guan 89",
                          "phone":207348723,
                          "kelurahan":2,
                          "kelurahanName":"Korakal",
                          "kecamatan":3,
                          "kecamatanName":"Desa Tenggara",
                          "rt":"03/08",
                          "KodePos":78676,
                          "kabupaten":2,
                          "kabupatenName":"Ujung Tengah",
                          "provinsi":1,
                          "provinsiName":"Papua Barat",
                          "xstat":false
                        },
                        {
                          "contact":4,
                          "used":0,
                          "ContactCategoryName":"Kantor Cabang",
                          "add":"Jl Biak 99",
                          "phone":207348723,
                          "kelurahan":3,
                          "kelurahanName":"Lembayung",
                          "kecamatan":2,
                          "kecamatanName":"Desa Tengah",
                          "rt":"03/08",
                          "KodePos":78676,
                          "kabupaten":3,
                          "kabupatenName":"Ujung Tenggara",
                          "provinsi":2,
                          "provinsiName":"Jakarta",
                          "xstat":false
                        }
                        
                      ],
                    
                    "ResultVehicle": [
                      {
                        "id":1,
                        "merk" : 1,
                        "vehicle":"Toyota",
                        "modelId":1,
                        "modelName":"Avanza",
                        "category":1,
                        "categoryName":"Mobil",
                        "type":1,
                        "vehicleTypeName":"All New 1.3 G A/T",
                        "colour":1,
                        "colourName":"Merah",
                        "nopol":"B 1250 GT",
                        "tahun":2015,
                        "price":200000000
                      },
                      {
                        "id":2,
                        "merk" : 2,
                        "vehicle":"Mazda",
                        "modelId":2,
                        "modelName":"Fortuner",
                        "category":1,
                        "categoryName":"Mobil",
                        "type":1,
                        "vehicleTypeName":"All New 1.3 G A/T",
                        "colour":2,
                        "colourName":"Transparan",
                        "nopol":"B 1111 GT",
                        "tahun":1984,
                        "price":280000000
                      },
                      {
                        "id":3,
                        "merk" : 1,
                        "vehicle":"Toyota",
                        "modelId":3,
                        "modelName":"Cayla",
                        "category":1,
                        "categoryName":"Mobil",
                        "type":1,
                        "vehicleTypeName":"All New 1.3 G A/T",
                        "colour":1,
                        "colourName":"Merah",
                        "nopo;":"B 1250 GT",
                        "tahun":2011,
                        "price":200000000
                      },
                      
                    ],
                    "fieldAction" : [
                      {
                          "id":1,
                          "date" : "02/06/2016 10:20:00",
                          "description":"Potensi malfungsi pada Rear Corner Sensor"
                      },
                      {
                          "id":2,
                          "date" : "09/06/2016 10:20:00",
                          "description":"Potensi malfungsi pada lampu sen kiri belakang"
                      },
                      {
                          "id":3,
                          "date" : "15/06/2016 10:20:00",
                          "description":"Toyota Warranty Claim"
                      },
                      {
                          "id":4,
                          "date" : "29/06/2016 10:20:00",
                          "description":"Parts Warranty Claim"
                      }
                    ],
                    "ssc" : [
                      {
                          "id":1,
                          "date" : "29/06/2016 10:20:00",
                          "metode":"Call",
                          "xstatus":"Airbag vios 2008"
                      },
                      {
                          "id":2,
                          "date" : "29/06/2016 10:20:00",
                          "metode":"Call",
                          "xstatus":"Kebakaran vios 2008"
                      },
                      {
                          "id":4,
                          "date" : "29/06/2016 10:20:00",
                          "metode":"Call",
                          "xstatus":"Masuk Jurang vios 2008"
                      }
                    ],
                    "csl" : [
                      {
                          "id":1,
                          "date" : "02/06/2016 10:20:00",
                          "description":"CSL 1"
                      },
                      {
                          "id":2,
                          "date" : "09/06/2016 10:20:00",
                          "description":"CSL 2"
                      },
                      {
                          "id":3,
                          "date" : "15/06/2016 10:20:00",
                          "description":"CSL 3"
                      },
                      {
                          "id":4,
                          "date" : "29/06/2016 10:20:00",
                          "description":"CSL 4"
                      }
                    ],
                    "idi" : [
                      {
                          "id":1,
                          "date" : "02/06/2016 10:20:00",
                          "description":"Interview 1"
                      },
                      {
                          "id":2,
                          "date" : "09/06/2016 10:20:00",
                          "description":"Interview 2"
                      },
                      {
                          "id":3,
                          "date" : "15/06/2016 10:20:00",
                          "description":"Interview 3"
                      }
                    ],
                    "complain" : [
                      {
                          "id":1,
                          "date" : "02/06/2016 10:20:00",
                          "description":"Permintaan"
                      },
                      {
                          "id":2,
                          "date" : "09/06/2016 10:20:00",
                          "description":"Keluhan: Suara Mesin"
                      }
                    ],
                    "cs" : [
                      {
                          "id":1,
                          "date" : "02/06/2016 10:20:00",
                          "description":"Test Drive Survey"
                      },
                      {
                          "id":2,
                          "date" : "09/06/2016 10:20:00",
                          "description":"DEC CR1 "
                      },
                      {
                          "id":3,
                          "date" : "02/06/2016 10:20:00",
                          "description":"DEC h3-7"
                      },
                      {
                          "id":4,
                          "date" : "09/06/2016 10:20:00",
                          "description":"DEC Feedback "
                      },
                      {
                          "id":5,
                          "date" : "02/06/2016 10:20:00",
                          "description":"PSFU Survey"
                      },
                      {
                          "id":6,
                          "date" : "09/06/2016 10:20:00",
                          "description":"VOCAS "
                      }
                    ],
                    "ResultLogHistory" : [
                      {
                          "type":1,
                          "modul":"Sales",
                          "menu":"Prospecting",
                          "atribut":"Nomor Handphone 1",
                          "newVal":"08123456789",
                          "oldVal":"08123456756",
                          "userName":"Anton",
                          "update" : "02/06/2016 10:20:00"
                          
                      },
                      {
                          "type":2,
                          "modul":"Sales",
                          "menu":"Prospecting",
                          "atribut":"Nomor Handphone 1",
                          "newVal":"08123452322",
                          "oldVal":"08123456789",
                          "userName":"Tim",
                          "update" : "02/06/2016 10:20:00"
                      },
                      {
                          "type":1,
                          "modul":"After Sales",
                          "menu":"Reception",
                          "atribut":"Nomor Handphone 2",
                          "newVal":"08123456789",
                          "oldVal":"08123456789",
                          "userName":"Lampard",
                          "update" : "02/06/2016 10:20:00"
                      },
                      {
                          "type":1,
                          "modul":"Sales",
                          "menu":"Prospecting",
                          "atribut":"Nama Pelanggan",
                          "newVal":"Jono",
                          "oldVal":"Joni",
                          "userName":"Terry",
                          "update" : "02/06/2016 10:20:00"
                      }
                    ],
                    "Start": 0,
                    "Limit": 10,
                    "Total": 3
                  }}
              );
              console.log('ressss=>',defer.promise);
             return defer.promise;
      },
      create: function(data) {
        console.log(' tambah data=>', data);

        return $http.post('/api/as/Stall', [{
                                            StallParentId: (data.StallParentId==null?0:data.StallParentId),
                                            Name: data.Name,
                                            Type: 0}]);
      },
      update: function(data){
        console.log('rubah data=>', data);
        return $http.put('/api/as/Stall', [{
                                            StallId: data.StallId,
                                            StallParentId: (data.StallParentId==null?0:data.StallParentId),
                                            Name: data.Name,
                                            Type: 0}]);
      },
      delete: function(id) {
        console.log("delete id==>",id);
        return $http.delete('/api/as/Stall',{data:id,headers: {'Content-Type': 'application/json'}});
      }
    }
  });
