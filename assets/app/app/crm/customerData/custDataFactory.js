angular.module('app')
  .factory('CustData', function($http, CurrentUser, $q, bsNotify) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      
      
      getGroupData: function(data) {
        console.log(' tambah data=>', data);
        // var res=$http.get('/api/ct/GetCCustomerListSpesific/1/-/-/-/-/3177729764072/-/');
        var res=$http.get('/api/ct/GetCCustomerListSpecific/'+data+'');
        // api/ct/GetCCustomerListSpesific/1/-/-/-/-/3177729764072/-
        return res;
        
      },
      
      getGroupDataAdvanced: function(data) {
        console.log(' tambah data=>', data);
        // var res=$http.get('/api/ct/GetCCustomerListSpesific/1/-/-/-/-/3177729764072/-/');
        var res=$http.get('/api/ct/GetCCustomerListGroup'+data);
        // api/ct/GetCCustomerListSpesific/1/-/-/-/-/3177729764072/-
        return res;
      },
      // getDataInstitusi: function(data) {
        
        
      // },

      SentEmail: function(data){
        console.log("dataa==>",data);
          return $http.post('/api/as/PostUmailQueue/', data);
          // var promise = [];
          // var phoneNumber = {};
          // console.log('masuk sendSMS Service', JSON.stringify(data.ParamBody));
          // console.log('masuk sendSMS Service currentUser', currentUser);

          // function send(data) {
          // var res = $http.post('/api/as/PostUWSQueue/', [{
          //       // WSQueueId: 0,
          //       OutletId: currentUser.OrgId,
          //       WorkerId: currentUser.UserId,
          //       Url: '-', //Insert Service SMS
          //       Method: '-', //Insert Method
          //       AuthUser: null,
          //       AuthPassword: null,
          //       AuthApiKey: null,
          //       ParamBody: JSON.stringify(data.ParamBody),
          //       RetryCount: null,
          //       Response: null,
          //       Status: 1,
          //       InsertTime: new Date(),
          //       ExecuteTime: data.ExecuteTime,
          //       FetchTime: null,
          //       FinisihTime: null
          //     }]);
          // };
          // console.log('masuk sendSMS Service send', send(data));

          // _.map(data, function(v) {
          // console.log('masuk sendSMS Service v', v);
          // var a = send(v);
          // promise.push(a);
          // });
          // console.log('masuk sendSMS Service Promise', promise);

          // return $q.all(promise);
          

        },
 
      // Personal Info
      getPersonalInfo: function(id){
        console.log("CustomerId, ",id); 
        var res=$http.get('/api/ct/GetCCustomerListPersonal/'+id+ '/');
        console.log("getPersonalInfo======>",res);
        return res;
      },

      getInstitusiInfo: function(id){
        console.log("CustomerId, ",id); 
        var res=$http.get('/api/ct/GetCCustomerListInstitution/'+id+'/');
        return res;
      },

      getJobInfo: function(id){
        console.log("IndividuId, ",id); 
        var res=$http.get('/api/ct/GetCustomerJobs/'+id+'/');
        return res;
      },
      // FAMILY
      getListFamily: function(id) {
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetCListFamily/'+id+'/');
      
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
        },
      //Address
      GetListAddress: function(id) {
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetCCustomerAddress/'+id+'/');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
        },
      GetCustomerJobs: function(id) {
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetCustomerJobs/'+id+'/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
        },
      getListPreference: function(id) {
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetCListPreference/'+id+'/');

        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
        },
      getListSocialMedia: function(id) {
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetCListSocialMedia/'+id+'/');

        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
        },
      GetCListVehicle: function(id) {
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetCListVehicle/'+id+'/');
  
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
        },
      GetToyotaVehicle: function(id) {
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetVehicleListByCustId/'+id+'/');
        // var res=$http.get('/api/ct/GetVehicleListByCustId/'+id+'/');
  
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
        },
      
      GetCHistory: function(id){
        // console.log("ID===>",id);
        var res=$http.get('/api/ct/GetCHistoryDataChanged/'+id+'/');
        return res;
      },

      GetForgotPass: function(data){
        console.log("GetForgotPass===>",data);
        var res = $http.post('/api/ct/PostCCustomerToyotaId/', data);
        // .then(
        //   function(ress){
        //     console.log("ress", ress);
        //     return ress;

        //     // kirim ke sms atau email nya disini
        //   });
        // console.log("res1234", res);
        return res;
        
      },

      GetFieldAction: function(id) {
        var res =  $http.get('/api/ct/GetCFieldAction/'+id);
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc User factory=>", res);
        return res;
      },

      ValidatePhoneEmail: function(param){
        var res = $http.get('/api/ct/ValidatePhoneEmail/'+param);

        return res;
      },

      getMappingOutlet: function(TID, param) {
        // var res =  $http.get('/api/ct/GetCustomerOutletMapping/'+param.toyotaId+'/'+param.outletId);
        var res =  $http.get('/api/ct/GetCustomerOutletMapping/'+TID+'/'+param.outletId);
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc User factory=>", res);
        return res;
      },
      getHakAkses: function(param) {
        var res =  $http.get('/api/ct/CustomerDataAccessTab');
        // var resUsr =  $http.get('/api/as/VehicleUser');
        // console.log("resSvc User factory=>", res);
        return res;
      },
      
        
      create: function(data,name) {
        console.log(' tambah data=>', data);
        console.log(' tambah name=>', name);

        if (name == 'address') {
          console.log(' tambah data address=>', data);

          var obj = {};
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=1;
               obj.Modul=data.modul;
               obj.Attribute=data.Address;
               obj.NewValue=null;
               obj.OldValue=null;

          return $http.post('/api/ct/PostCCustomerAddress', [data]).then(
                      function(res){
                        console.log("==>",res);
                              // insert ke history
                            return $http.post('/api/ct/PostCHistoryDataChanged', [obj]);
                      }
                  )

          
          // return $http.post('/api/ct/PostCCustomerAddress/', [data]);
        } else if(name == 'family'){
          console.log(' tambah data family=>', data);

          // var jH = [];
          var obj = {};
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=1;
               obj.Modul=data.modul;
               obj.Attribute=data.Name;
               obj.NewValue=null;
               obj.OldValue=null;

               // jH.push(obj);
          console.log("history==>",obj);


          // $q.resolve(
                  return $http.post('/api/ct/PostCListFamily', [data]).then(
                      function(res){
                        console.log("==>",res);
                              // insert ke history
                            return $http.post('/api/ct/PostCHistoryDataChanged', [obj]);
                      }
                  )
              // );
          //return $http.post('/api/ct/PostCListFamily/', [data]);

        } else if(name == 'preference'){
          // PutCListPreference
          console.log(' tambah data preference=>', data);
          var obj = {};
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=1;
               obj.Modul=data.modul;
               obj.Attribute=data.Description;
               obj.NewValue=null;
               obj.OldValue=null;

               // jH.push(obj);
          console.log("history==>",obj);


          if (data.PreferenceId == undefined) {
              // $q.resolve(
                  return $http.post('/api/ct/PostCMasterPreference/', [data]).then(
                      function(res){
                        if(res.data.Response.ResponseCode == 98){
                          bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Data Gagal Ditambahkan, Data sudah tersedia",
                            timeout: 2000
                          });
                        }else{
                          console.log("ressss===>",res);
                            // var data1=res.data;
                            // console.log("ressss===>",res.data);
                            data.preferenceId = res.data;
                            console.log(data,"ressss===>",res.data);

                            $q.resolve(
                                  $http.post('/api/ct/PostCListPreference', [data]).then(
                                      function(res){
                                        console.log("==>",res);
                                              // insert ke history
                                            return $http.post('/api/ct/PostCHistoryDataChanged', [obj]);
                                      }
                                  )
                              );
                            // return $http.post('/api/ct/PostCListPreference/', [data]);
                          }
                      },function (err) {
                        console.log("errr",err);
                      }
                  )
              // );
              
              // var preference = cId.split("#");
              // var preferenceId = preference[0];
              // console.log(cId);
              // console.log(cId.$$state);
              // console.log(cId.$$state.value);
              // console.log(cId.$$state.value.data.ResponseMessage);

              //return $http.post('/api/ct/PostCListPreference/', [data]);  
              // return 1;
          } else{
                //$q.resolve(
                        return $http.post('/api/ct/PostCListPreference', [data]).then(
                            function(res){
                              console.log("==>",res);
                                    // insert ke history
                                  return $http.post('/api/ct/PostCHistoryDataChanged', [obj]);
                            }
                        )
                    //);
            // return $http.post('/api/ct/PostCListPreference/', [data]); gentlemen only ladies forbidden

          };
          

          
          
        } else if(name == 'socialMedia'){
          console.log(' tambah data medsos=>', data);
          var obj = {};
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=1;
               obj.Modul=data.modul;
               obj.Attribute=data.Description;
               obj.NewValue=null;
               obj.OldValue=null;

               // jH.push(obj);
          console.log("history==>",obj);

          if (data.SocialMediaId == undefined) {
              // $q.resolve(
                  return $http.post('/api/ct/PostCMasterSocialMedia', [data]).then(
                      function(res){
                        if(res.data.Response.ResponseCode == 98){
                          bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Data Gagal Ditambahkan, Data sudah tersedia",
                            timeout: 2000
                          });
                        }else{
                          console.log("ressss===>",res);
                            // var data1=res.data;
                            // console.log("ressss===>",res.data);
                            data.SocialMediaId = res.data;
                            console.log(data,"ressss===>",res.data);

                            $q.resolve(
                                  $http.post('/api/ct/PostCListSocialMedia', [data]).then(
                                      function(res){
                                        console.log("==>",res);
                                              // insert ke history
                                            return $http.post('/api/ct/PostCHistoryDataChanged', [obj]);
                                      }
                                  )
                              );
                            // return $http.post('/api/ct/PostCListSocialMedia/', [data]);
                                    }

                      }
                  )
              // );
          } else{
            // $q.resolve(
                    return $http.post('/api/ct/PostCListSocialMedia', [data]).then(
                            function(res){
                              console.log("==>",res);
                                    // insert ke history
                                  return $http.post('/api/ct/PostCHistoryDataChanged', [obj]);
                            }
                        )
                    // );
            // return $http.post('/api/ct/PostCListSocialMedia', [data]);  
          };
          
          // api/ct/PostCListSocialMedia
        } else if(name == 'nonToyota'){
          console.log(' tambah data non Toyota=>', data);
          var obj = {};
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=1;
               obj.Modul=data.modul;
               obj.Attribute=(data.Model+'( '+data.LicensePlate+' )');
               obj.NewValue=null;
               obj.OldValue=null;

               // jH.push(obj);
          console.log("history==>",obj);

          // $q.resolve(
                  return $http.post('/api/ct/PostCListVehicle', [data]).then(
                      function(res){
                        console.log("==>",res);
                              // insert ke history
                            return $http.post('/api/ct/PostCHistoryDataChanged', [obj]);
                      }
                  )
              // );

          // return $http.post('/api/ct/PostCListVehicle/', [data]);
          
        } ;

        // return $http.post('/api/ct/ct', [{
        //                                     StallParentId: (data.StallParentId==null?0:data.StallParentId),
        //                                     Name: data.Name,
        //                                     Type: 0}]);
      },
      update: function(data,name){
        console.log('rubah data=>', data);
        console.log('rubah name=>', name);

        if(name == undefined && data.acc1 != undefined){

          var pData = {"PersonalId": data.acc1[0].CustomerListPersonal[0].PersonalId,
          "CustomerId": data.acc1[0].CustomerListPersonal[0].CustomerId,
          "CustomerName": data.acc1[0].CustomerListPersonal[0].CustomerName,
          "CustomerNickName": data.acc1[0].CustomerListPersonal[0].CustomerNickName,
          "KTPKITAS": data.acc1[0].CustomerListPersonal[0].KTPKITAS,
          "BirthPlace": data.acc1[0].CustomerListPersonal[0].BirthPlace,
          "BirthDate": data.acc1[0].CustomerListPersonal[0].BirthDate,
          "Handphone1": data.acc1[0].CustomerListPersonal[0].Handphone1,
          "Handphone2": data.acc1[0].CustomerListPersonal[0].Handphone2,
          "Email": data.acc1[0].CustomerListPersonal[0].Email,
          "MaritalDate": data.acc1[0].CustomerListPersonal[0].MaritalDate,
          "CustomerGenderId": data.acc1[0].CustomerListPersonal[0].CustomerGenderId,
          "CustomerReligionId": data.acc1[0].CustomerListPersonal[0].CustomerReligionId,
          "CustomerEthnicId": data.acc1[0].CustomerListPersonal[0].CustomerEthnicId,
          "CustomerLanguageId": data.acc1[0].CustomerListPersonal[0].CustomerLanguageId,
          "RoleId": data.acc1[0].CustomerListPersonal[0].RoleId,
          "MaritalStatusId": data.acc1[0].CustomerListPersonal[0].MaritalStatusId,
          "FrontTitle": data.acc1[0].CustomerListPersonal[0].FrontTitle,
          "EndTitle": data.acc1[0].CustomerListPersonal[0].EndTitle,
          "PhoneStatus": data.acc1[0].CustomerListPersonal[0].PhoneStatus,"StatusCode":1};
          

          

          var cData = {"CustomerId": data.acc1[0].CustomerId,
                       "Npwp": data.acc1[0].Npwp, 
                       "FleetId": data.acc1[0].FleetId, 
                       "CustomerTypeId": data.acc1[0].CustomerTypeId,
                       "ToyotaId": data.acc1[0].ToyotaId,
                       "StatusCode":1,
                      //  "OutletId": data.acc1[0].OutleltId
                      "OutletId": data.acc1File.OutletId
                     };
          
          var jData = {};
          if (data.acc2 != undefined) {
              var jData = {"CustomerJobsId": data.acc2[0].CustomerJobsId,
                          "CustomerId": data.acc1[0].CustomerId,
                          "PersonalId": data.acc2[0].PersonalId,
                          "Job": data.acc2[0].Job,
                          "Company": data.acc2[0].Company,
                          "CustomerPositionId": data.acc2[0].CustomerPositionId,
                          "Department": data.acc2[0].Department,
                          "RangeGrossIncomeId": data.acc2[0].RangeGrossIncomeId,
                          "StatusCode":1,};  
          };

          console.log(' update data Personal=>', pData);
          console.log(' update data job=>', jData);
          console.log(' update data job=>', cData);

          var pH = [];
          // var jH = [];
          for (var key in pData) {
              console.log(pData[key], "====", data.acc1File.CustomerListPersonal[0][key] );
              console.log("KOK BANYAK");
            if (pData[key] != data.acc1File.CustomerListPersonal[0][key]) {
               var obj = {};

               
               obj.CustomerId=pData.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.acc1File.modul;
               obj.Attribute=key;
               // obj.NewValue=pData[key];
               // obj.OldValue=data.acc1File.CustomerListPersonal[0][key];

              //  if ((key == 'BirthDate') || (key == 'MaritalDate')) {
              //   var xn = new Date(pData[key]);
              //   var xo = new Date(data.acc1File.CustomerListPersonal[0][key]);
              //   console.log("===>",xn,"======",xo,"=====",xn < xo,"=====",xn > xo);
              //   if((xn < xo) || (xn > xo)){pH.push(obj);}
              //  }
               
               if ((key == 'BirthDate')) {
                console.log("Gua lelah boy");             
                obj.NewValue = pData[key];
                obj.OldValue = data.acc1File.CustomerListPersonal[0][key];
                pH.push(obj);
               }     
               
              
               else if ((key == 'MaritalDate')) {
                 console.log("Gua lelah cuy");             
                obj.NewValue = pData[key];  
                obj.OldValue = data.acc1File.CustomerListPersonal[0][key];
                pH.push(obj);
               }
               
               else if (key == "CustomerGenderId") {
                  // var index1 = data.master["statusGender"].findIndex(x => x.id==data.acc1File.CustomerListPersonal[0][key]);
                  // var index2 = data.master["statusGender"].findIndex(x => x.id==pData[key]);
                  var index1 = _.findIndex(data.master["statusGender"], function(o) { return o.id == data.acc1File.CustomerListPersonal[0][key]; });
                  var index2 = _.findIndex(data.master["statusGender"], function(o) { return o.id == pData[key]; });

                  //console.log("=====>1=",index1,"==>2=",index2,"==>3=",index3,"==>4=",index4);
                  obj.NewValue=((index2 == -1) ? null : data.master.statusGender[index2].name);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusGender[index1].name);  
                  pH.push(obj);
               }else if(key == "CustomerReligionId"){
                  //var index1 = data.master["statusReligion"].findIndex(x => x.CustomerReligionId==data.acc1File.CustomerListPersonal[0][key]);
                  //var index2 = data.master["statusReligion"].findIndex(x => x.CustomerReligionId==pData[key]);
                  var index1 = _.findIndex(data.master["statusReligion"], function(o) { return o.CustomerReligionId == data.acc1File.CustomerListPersonal[0][key]; });
                  var index2 = _.findIndex(data.master["statusReligion"], function(o) { return o.CustomerReligionId == pData[key]; });


                  obj.NewValue=((index2 == -1) ? null : data.master.statusReligion[index2].CustomerReligionName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusReligion[index1].CustomerReligionName);  
                  pH.push(obj);
                  // obj.NewValue=data.master.statusReligion[index2].CustomerReligionName;
                  // obj.OldValue=data.master.statusReligion[index1].CustomerReligionName; 
               }else if(key == "CustomerEthnicId"){
                  //var index1 = data.master["statusEthnic"].findIndex(x => x.CustomerEthnicId==data.acc1File.CustomerListPersonal[0][key]);
                  //var index2 = data.master["statusEthnic"].findIndex(x => x.CustomerEthnicId==pData[key]);
                  var index1 = _.findIndex(data.master["statusEthnic"], function(o) { return o.CustomerEthnicId == data.acc1File.CustomerListPersonal[0][key]; });
                  var index2 = _.findIndex(data.master["statusEthnic"], function(o) { return o.CustomerEthnicId == pData[key]; });

                  obj.NewValue=((index2 == -1) ? null : data.master.statusEthnic[index2].CustomerEthnicName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusEthnic[index1].CustomerEthnicName);
                  pH.push(obj);
                  // obj.NewValue=data.master.statusEthnic[index2].CustomerEthnicName;
                  // obj.OldValue=data.master.statusEthnic[index1].CustomerEthnicName; 
               }else if(key == "CustomerLanguageId"){
                  //var index1 = data.master["statusLanguage"].findIndex(x => x.CustomerLanguageId==data.acc1File.CustomerListPersonal[0][key]);
                  //var index2 = data.master["statusLanguage"].findIndex(x => x.CustomerLanguageId==pData[key]);
                  var index1 = _.findIndex(data.master["statusLanguage"], function(o) { return o.CustomerLanguageId == data.acc1File.CustomerListPersonal[0][key]; });
                  var index2 = _.findIndex(data.master["statusLanguage"], function(o) { return o.CustomerLanguageId == pData[key]; });

                  obj.NewValue=((index2 == -1) ? null : data.master.statusLanguage[index2].CustomerLanguageName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusLanguage[index1].CustomerLanguageName);
                  pH.push(obj);
                  // obj.NewValue=data.master.statusLanguage[index2].CustomerLanguageName;
                  // obj.OldValue=data.master.statusLanguage[index1].CustomerLanguageName; 
               }else if(key == "MaritalStatusId"){
                  //var index1 = data.master["statusMarital"].findIndex(x => x.MaritalStatusId==data.acc1File.CustomerListPersonal[0][key]);
                  //var index2 = data.master["statusMarital"].findIndex(x => x.MaritalStatusId==pData[key]);
                  var index1 = _.findIndex(data.master["statusMarital"], function(o) { return o.MaritalStatusId == data.acc1File.CustomerListPersonal[0][key]; });
                  var index2 = _.findIndex(data.master["statusMarital"], function(o) { return o.MaritalStatusId == pData[key]; });

                  //console.log(index1,"iniiiii==>",index2);
                  obj.NewValue=((index2 == -1) ? null : data.master.statusMarital[index2].MaritalStatusName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusMarital[index1].MaritalStatusName);
                  pH.push(obj);
                  // obj.NewValue=data.master.statusMarital[index2].MaritalStatusName;
                  // obj.OldValue=data.master.statusMarital[index1].MaritalStatusName; 
               } else{
                obj.NewValue=pData[key];
                obj.OldValue=data.acc1File.CustomerListPersonal[0][key];
                pH.push(obj);
               };
               
            };  
          }

          for (var key in jData) {
            console.log(jData[key], "====", data.acc2File[key] );
            console.log("THIS SONG");
            if (jData[key] != data.acc2File[key]) {
               var obj = {};
               
               obj.CustomerId=jData.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.acc2File.modul;
               obj.Attribute=key;
               // obj.NewValue=jData[key];
               // obj.OldValue=data.acc2File[key];

               if(key == "CustomerPositionId"){
                  //var index1 = data.master["statusPosition"].findIndex(x => x.CustomerPositionId==data.acc2File[key]);
                  //var index2 = data.master["statusPosition"].findIndex(x => x.CustomerPositionId==jData[key]);
                  var index1 = _.findIndex(data.master["statusPosition"], function(o) { return o.CustomerPositionId == data.acc2File[key]; });
                  var index2 = _.findIndex(data.master["statusPosition"], function(o) { return o.CustomerPositionId == jData[key]; });


                  obj.NewValue=((index2 == -1) ? null : data.master.statusPosition[index2].CustomerPositionName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusPosition[index1].CustomerPositionName);
                  pH.push(obj);
                  // obj.NewValue=data.master.statusPosition[index2].CustomerPositionName;
                  // obj.OldValue=data.master.statusPosition[index1].CustomerPositionName; 
               }else if(key == "Job"){
                 // var index1 = data.master["statusJob"].findIndex(x => x.JobId==data.acc2File[key]);
                 // var index2 = data.master["statusJob"].findIndex(x => x.JobId==jData[key]);
                  var index1 = _.findIndex(data.master["statusJob"], function(o) { return o.JobId == data.acc2File[key]; });
                  var index2 = _.findIndex(data.master["statusJob"], function(o) { return o.JobId == jData[key]; });
                  
                  obj.NewValue=((index2 == -1) ? null : data.master.statusJob[index2].JobName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusJob[index1].JobName);
                  pH.push(obj);
                  // obj.NewValue=data.master.statusJob[index2].JobName;
                  // obj.OldValue=data.master.statusJob[index1].JobName; 
               }else if(key == "RangeGrossIncomeId"){
                  // var index1 = data.master["statusIncome"].findIndex(x => x.RangeGrossIncomeId==data.acc2File[key]);
                  // var index2 = data.master["statusIncome"].findIndex(x => x.RangeGrossIncomeId==jData[key]);
                  var index1 = _.findIndex(data.master["statusIncome"], function(o) { return o.RangeGrossIncomeId == data.acc2File[key]; });
                  var index2 = _.findIndex(data.master["statusIncome"], function(o) { return o.RangeGrossIncomeId == jData[key]; });
                  
                  obj.NewValue=((index2 == -1) ? null : data.master.statusIncome[index2].RangeGrossIncomeName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusIncome[index1].RangeGrossIncomeName);
                  pH.push(obj);
                  // obj.NewValue=data.master.statusIncome[index2].RangeGrossIncomeName;
                  // obj.OldValue=data.master.statusIncome[index1].RangeGrossIncomeName; 
               } else{
                obj.NewValue=jData[key];
                obj.OldValue=data.acc2File[key];
                pH.push(obj);
               }

              //  if (cData[key] != 'StatusCode') {pH.push(obj);}; // comment dl jadi double nge push nya.. kl ada yg tau ini buat apa uncomment aja
            };  
          }

          for (var key in cData) {
            console.log(cData[key], "====", data.acc1File[key] );
            console.log("THIS SONG FOR YOU");
            if (cData[key] != data.acc1File[key]) {

               var obj = {};
               
               obj.CustomerId=cData.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.acc1File.modul;
               obj.Attribute=key;
               // obj.NewValue=cData[key];
               // obj.OldValue=data.acc1File[key];

               
               if(key == "FleetId"){
                  // var index1 = data.master["statusFleet"].findIndex(x => x.FleetId==data.acc1File[key]);
                  // var index2 = data.master["statusFleet"].findIndex(x => x.FleetId==cData[key]);
                  var index1 = _.findIndex(data.master["statusFleet"], function(o) { return o.FleetId == data.acc1File[key]; });
                  var index2 = _.findIndex(data.master["statusFleet"], function(o) { return o.FleetId == cData[key]; });
                  

                  obj.NewValue=((index2 == -1) ? null : data.master.statusFleet[index2].Fleet);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusFleet[index1].Fleet);
                  pH.push(obj);
                  // obj.NewValue=data.master.statusFleet[index2].Fleet;
                  // obj.OldValue=data.master.statusFleet[index1].Fleet; 
               } 
                else if ((key == 'OutletId')) {
                console.log("MIDLE",data.acc1File.OutletId);
                console.log("MISS YOU");             
                obj.NewValue = data.acc1File.OutletId;
                obj.OldValue = data.acc1File.OutletId;  
                pH.push(obj);
               }   
               
               else{
                obj.NewValue=cData[key];
                obj.OldValue=data.acc1File[key];
                pH.push(obj);
               }

              //  if (cData[key] != 'StatusCode') {pH.push(obj);}; // comment dl jadi double nge push nya.. kl ada yg tau ini buat apa uncomment aja
               
            };  
          }


          console.log(' update data Personal=>', pH);
          // console.log(' update data job=>', jH);

          
          
         // $q.resolve(
                  return $http.put('/api/ct/PutCListPersonal', [pData]).then(
                      function(res){
                            console.log("==>",res);
                              // insert ke history
                            $http.put('/api/ct/PutCustomerJobs', [jData]);
                            $http.put('/api/ct/PutCustomerList', [cData]);
                            $http.post('/api/ct/PostCHistoryDataChanged', pH);
                            //$q.resolve(
                            //     $http.put('/api/ct/PutCustomerJobs', [jData]).then(
                            //         function(res){
                            //           console.log("==>",res);

                            //               $http.put('/api/ct/PutCustomerList', [cData]).then(
                            //                 function(res){
                            //                   console.log("==>",res);

                            //                   // insert ke history
                            //                    $http.post('/api/ct/PostCHistoryDataChanged', pH);
                            //                 }
                            //               );
                            //         }
                            //    // )
                            // );
                      }
                  )
            //  );
          // return $http.put('/api/ct/PutCListPersonal', [sData]);
        } else if (name == undefined && data.xFileInstitusi != undefined) {
          console.log(' update data Institusi=>', data);

          var pData = {"CreatedUserId":data.CustomerListInstitution[0].CreatedUserId,
              "CustomerId":data.CustomerListInstitution[0].CustomerId,
              "InstitutionId":data.CustomerListInstitution[0].InstitutionId,
              "Name":data.CustomerListInstitution[0].Name,
              "PICAddress":data.CustomerListInstitution[0].PICAddress,
              "PICDOB":data.CustomerListInstitution[0].PICDOB,
              "PICDepartmentId":data.CustomerListInstitution[0].PICDepartmentId,
              "PICEmail":data.CustomerListInstitution[0].PICEmail,
              "PICGenderId":data.CustomerListInstitution[0].PICGenderId,
              "PICHp":data.CustomerListInstitution[0].PICHp,
              "PICKTPKITAS":data.CustomerListInstitution[0].PICKTPKITAS,
              "PICName":data.CustomerListInstitution[0].PICName,
              "PICNickName":data.CustomerListInstitution[0].PICNickName,
              "PICPositionId":data.CustomerListInstitution[0].PICPositionId,
              "PICRT":data.CustomerListInstitution[0].PICRT,
              "PICRW":data.CustomerListInstitution[0].PICRW,
              "PICVillageId":data.CustomerListInstitution[0].PICVillageId,
              "SIUP":data.CustomerListInstitution[0].SIUP,
              "SectorBusinessId":data.CustomerListInstitution[0].SectorBusinessId,
              "TDP":data.CustomerListInstitution[0].TDP}

          var cData = {"CustomerId": data.CustomerId,
                       "Npwp": data.Npwp, 
                       "FleetId": data.FleetId, 
                       "CustomerTypeId": data.CustomerTypeId,
                       "ToyotaId": data.ToyotaId,
                       "StatusCode":1,
                       "OutletId": data.OutleltId
                     };
          
          var pH = [];
          var jH = [];
          for (var key in pData) {
              console.log(pData[key], "====", data.xFileInstitusi.CustomerListInstitution[0][key] );
              console.log("YEEY");
            if (pData[key] != data.xFileInstitusi.CustomerListInstitution[0][key]) {
               var obj = {};
               
               obj.CustomerId=pData.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.modul;
               obj.Attribute=key;

               if(key == "SectorBusinessId"){
                  // var index1 = data.master["statusSektor"].findIndex(x => x.SectorBusinessId==data.xFileInstitusi.CustomerListInstitution[0][key]);
                  // var index2 = data.master["statusSektor"].findIndex(x => x.SectorBusinessId==pData[key]);
                  var index1 = _.findIndex(data.master["statusSektor"], function(o) { return o.SectorBusinessId == data.xFileInstitusi.CustomerListInstitution[0][key]; });
                  var index2 = _.findIndex(data.master["statusSektor"], function(o) { return o.SectorBusinessId == pData[key]; });
                  
                  obj.NewValue=((index2 == -1) ? null : data.master.statusSektor[index2].SectorBusinessName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusSektor[index1].SectorBusinessName);
                  // obj.NewValue=data.master.statusPosition[index2].CustomerPositionName;
                  // obj.OldValue=data.master.statusPosition[index1].CustomerPositionName; 
               } else if(key == "PICPositionId"){
                  // var index1 = data.master["statusPosition"].findIndex(x => x.CustomerPositionId==data.xFileInstitusi.CustomerListInstitution[0][key]);
                  // var index2 = data.master["statusPosition"].findIndex(x => x.CustomerPositionId==pData[key]);
                  var index1 = _.findIndex(data.master["statusPosition"], function(o) { return o.CustomerPositionId == data.xFileInstitusi.CustomerListInstitution[0][key]; });
                  var index2 = _.findIndex(data.master["statusPosition"], function(o) { return o.CustomerPositionId == pData[key]; });

                  obj.NewValue=((index2 == -1) ? null : data.master.statusPosition[index2].CustomerPositionName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusPosition[index1].CustomerPositionName);
                  // obj.NewValue=data.master.statusPosition[index2].CustomerPositionName;
                  // obj.OldValue=data.master.statusPosition[index1].CustomerPositionName; 
               } else if(key == "PICDepartmentId"){
                  // var index1 = data.master["statusDepartemen"].findIndex(x => x.DepartmentId==data.xFileInstitusi.CustomerListInstitution[0][key]);
                  // var index2 = data.master["statusDepartemen"].findIndex(x => x.DepartmentId==pData[key]);
                  var index1 = _.findIndex(data.master["statusDepartemen"], function(o) { return o.DepartmentId == data.xFileInstitusi.CustomerListInstitution[0][key]; });
                  var index2 = _.findIndex(data.master["statusDepartemen"], function(o) { return o.DepartmentId == pData[key]; });
                  
                  obj.NewValue=((index2 == -1) ? null : data.master.statusDepartemen[index2].DepartmentName);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusDepartemen[index1].DepartmentName);
                  // obj.NewValue=data.master.statusPosition[index2].CustomerPositionName;
                  // obj.OldValue=data.master.statusPosition[index1].CustomerPositionName; 
               } else{
                obj.NewValue=pData[key];
                obj.OldValue=data.xFileInstitusi.CustomerListInstitution[0][key];
               }


               // obj.NewValue=pData[key];
               // obj.OldValue=data.xFileInstitusi.CustomerListInstitution[0][key];

               pH.push(obj);
            };  
          }

          for (var key in cData) {
            console.log(cData[key], "====", data.xFileInstitusi[key] );
            console.log("I TELL YOU");
            if (cData[key] != data.xFileInstitusi[key]) {

               var obj = {};
               
               obj.CustomerId=cData.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.modul;
               obj.Attribute=key;

               if(key == "FleetId"){
                  // var index1 = data.master["statusFleet"].findIndex(x => x.FleetId==data.xFileInstitusi[key]);
                  // var index2 = data.master["statusFleet"].findIndex(x => x.FleetId==cData[key]);
                  var index1 = _.findIndex(data.master["statusFleet"], function(o) { return o.FleetId == data.xFileInstitusi[key]; });
                  var index2 = _.findIndex(data.master["statusFleet"], function(o) { return o.FleetId == cData[key]; });
                  
                  obj.NewValue=((index2 == -1) ? null : data.master.statusFleet[index2].Fleet);
                  obj.OldValue=((index1 == -1) ? null : data.master.statusFleet[index1].Fleet);
                  pH.push(obj);
                  // obj.NewValue=data.master.statusPosition[index2].CustomerPositionName;
                  // obj.OldValue=data.master.statusPosition[index1].CustomerPositionName; 
               } else{
                obj.NewValue=pData[key];
                obj.OldValue=data.xFileInstitusi.CustomerListInstitution[0][key];
                pH.push(obj);
               }

               // obj.NewValue=cData[key];
               // obj.OldValue=data.xFileInstitusi[key];

              //  if (cData[key] != 'StatusCode') {pH.push(obj);}; // comment dl jadi double nge push nya.. kl ada yg tau ini buat apa uncomment aja
               
            };  
          }

          console.log("pH Institusi===>",pH);
          return $http.put('/api/ct/PutCListInstitution', [pData]).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             // $http.post('/api/ct/PostCHistoryDataChanged', pH);
                             $http.put('/api/ct/PutCustomerList', [cData]).then(
                                            function(res){
                                              console.log("==>",res);

                                              // insert ke history
                                               $http.post('/api/ct/PostCHistoryDataChanged', pH);
                                            }
                                          );
                        }
                   // )
                );

          // return $http.put('/api/ct/PutCCustomerAddress/', [data]);

        } else if (name == 'address') {
          console.log(' update data address=>', data);

          var pH = [];
          var xFile = data.FileBC;
          for (var key in xFile) {
              console.log(data[key], "====", xFile[key] );
              console.log("WHAT YOU SAID");
            if (data[key] != xFile[key]) {
               var obj = {};
               
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.modul;
               obj.Attribute=key;
               obj.NewValue=data[key];
               obj.OldValue=xFile[key];

               pH.push(obj);
            };  
          }

          console.log("pH==>",pH);

          return $http.put('/api/ct/PutCCustomerAddress', [data]).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);
                        }
                   // )
                );

          // return $http.put('/api/ct/PutCCustomerAddress/', [data]);

        } else if(name == 'family'){
          console.log(' update data family=>', data);
          var pH = [];
          var xFile = data.FileBC;
          for (var key in xFile) {
              console.log(data[key], "====", xFile[key] );
              console.log("THIS TIME");
            if (data[key] != xFile[key]) {
               var obj = {};
               
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.modul;
               obj.Attribute=key;
               if (key == "GenderId") {
                  // var index1 = data["statusGender"].findIndex(x => x.id==xFile[key]);
                  // var index2 = data["statusGender"].findIndex(x => x.id==data[key]);
                  var index1 = _.findIndex(data["statusGender"], function(o) { return o.id == xFile[key]; });
                  var index2 = _.findIndex(data["statusGender"], function(o) { return o.id == data[key]; });

                  obj.NewValue=((index2 == -1) ? null : data.statusGender[index2].name);
                  obj.OldValue=((index1 == -1) ? null : data.statusGender[index1].name);
                  // obj.NewValue=data.statusGender[index2].name;
                  // obj.OldValue=data.statusGender[index1].name;  
               }else if(key == "FamilyRelationId"){
                  // var index1 = data["statusFamily"].findIndex(x => x.FamilyRelationId==xFile[key]);
                  // var index2 = data["statusFamily"].findIndex(x => x.FamilyRelationId==data[key]);
                  var index1 = _.findIndex(data["statusFamily"], function(o) { return o.FamilyRelationId == xFile[key]; });
                  var index2 = _.findIndex(data["statusFamily"], function(o) { return o.FamilyRelationId == data[key]; });

                  obj.NewValue=((index2 == -1) ? null : data.statusFamily[index2].FamilyRelationName);
                  obj.OldValue=((index1 == -1) ? null : data.statusFamily[index1].FamilyRelationName);
                  // obj.NewValue=data.statusFamily[index2].FamilyRelationName;
                  // obj.OldValue=data.statusFamily[index1].FamilyRelationName; 
               } else{
                obj.NewValue=data[key];
                obj.OldValue=xFile[key];
               };
               

               pH.push(obj);
            };  
          }

          console.log("pH==>",pH);

          return $http.put('/api/ct/PutCListFamily', [data]).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);
                        }
                   // )
                );

          // return $http.put('/api/ct/PutCListFamily', [data]);
        } else if(name == 'nonToyota'){
          console.log(' update data non Toyota=>', data);
          var pH = [];
          var xFile = data.FileBC;
          for (var key in xFile) {
              console.log(data[key], "====", xFile[key] );
              console.log("YEY EVERYBODY SINGING");
            if (data[key] != xFile[key]) {
               var obj = {};
               
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.modul;
               obj.Attribute=key;
               if (key == "BrandId") {
                  // var index1 = data["statusBrand"].findIndex(x => x.BrandId==xFile[key]);
                  // var index2 = data["statusBrand"].findIndex(x => x.BrandId==data[key]);
                  var index1 = _.findIndex(data["statusBrand"], function(o) { return o.BrandId == xFile[key]; });
                  var index2 = _.findIndex(data["statusBrand"], function(o) { return o.BrandId == data[key]; });
                  
                  obj.NewValue=((index2 == -1) ? null : data.statusBrand[index2].BrandName);
                  obj.OldValue=((index1 == -1) ? null : data.statusBrand[index1].BrandName);
                  // obj.NewValue=data.statusBrand[index2].BrandName;
                  // obj.OldValue=data.statusBrand[index1].BrandName;
                  
               }else if(key == "VehicleCategoryId"){
                  // var index1 = data["statusCategory"].findIndex(x => x.VehicleCategoryId==xFile[key]);
                  // var index2 = data["statusCategory"].findIndex(x => x.VehicleCategoryId==data[key]);
                  var index1 = _.findIndex(data["statusCategory"], function(o) { return o.VehicleCategoryId == xFile[key]; });
                  var index2 = _.findIndex(data["statusCategory"], function(o) { return o.VehicleCategoryId == data[key]; });
                  
                  obj.NewValue=((index2 == -1) ? null : data.statusCategory[index2].VehicleCategoryName);
                  obj.OldValue=((index1 == -1) ? null : data.statusCategory[index1].VehicleCategoryName);
                  // obj.NewValue=data.statusCategory[index2].VehicleCategoryName;
                  // obj.OldValue=data.statusCategory[index1].VehicleCategoryName; 
               } else{
                obj.NewValue=data[key];
                obj.OldValue=xFile[key];
               };

               pH.push(obj);
            };  
          }

          console.log("pH==>",pH);

          return $http.put('/api/ct/PutCListVehicle', [data]).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);

                        }
                   // )
                );
          // return $http.put('/api/ct/PutCListVehicle/', [data]);
          
        } else if(name == 'preference'){
          console.log(' update data preference=>', data);
          var pH = [];
          var xFile = data.FileBC;
          for (var key in xFile) {
              console.log(data[key], "====", xFile[key] );
              console.log("REASON");
            if (data[key] != xFile[key]) {
               var obj = {};
               
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.modul;
               obj.Attribute=key;
               obj.NewValue=data[key];
               obj.OldValue=xFile[key];

               pH.push(obj);
            };  
          }

          console.log("pH==>",pH);

          return $http.put('/api/ct/PutCListPreference', [data]).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);

                        }
                   // )
                );
          // return $http.put('/api/ct/PutCListPreference/', [data]);
        } else if(name == 'socialMedia'){
          console.log(' update data socialMedia=>', data);
          var pH = [];
          var xFile = data.FileBC;
          for (var key in xFile) {
              console.log(data[key], "====", xFile[key] );
              console.log("THAT I WANT");
            if (data[key] != xFile[key]) {
               var obj = {};
               
               obj.CustomerId=data.CustomerId;
               obj.ChangeTypeId=2;
               obj.Modul=data.modul;
               obj.Attribute=key;
               obj.NewValue=data[key];
               obj.OldValue=xFile[key];

               pH.push(obj);
            };  
          }

          console.log("pH==>",pH);

          return $http.put('/api/ct/PutCListSocialMedia', [data]).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);

                        }
                   // )
                );
          // return $http.put('/api/ct/PutCListSocialMedia/', [data]);
        
        }
        
      },
      updateMainAddress: function(x){
        console.log("===>",x);
        var res=$http.put('/api/ct/PutCMainAddress/'+x.CustomerId+'/'+x.CustomerAddressId+'/');
        return res;
      },
      delete: function(id,name) {
        console.log("delete id==>",id);
        if (name == 'address') {
          console.log(' delete data address=>', id);
          var i = id.length - 1;
          console.log(id.length,"total==>",i);
          var cv = id[i];
          var pH = [];
          for (var j = 0;j < cv.length; j++) {
              var obj = {};
              obj.CustomerId=cv[j].CustomerId;
               obj.ChangeTypeId=3;
               obj.Modul=cv[j].modul;
               obj.Attribute=cv[j].address;
               obj.NewValue=null;
               obj.OldValue=null;

               pH.push(obj);
          };
          console.log("history",pH);
          id.splice(i, 1);
          console.log("====>",id);
          return $http.delete('/api/ct/DeleteCCustomerAddress',{data:id,headers: {'Content-Type': 'application/json'}}).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);
                            // return $http.post('/api/ct/PostCHistoryDataChanged', jH);
                            // return $http.post('/api/ct/PostCListSocialMedia/', [data]);

                        }
                   // )
                );
          // return $http.delete('/api/ct/DeleteCCustomerAddress', id);
          // return $http.delete('/api/ct/DeleteCCustomerAddress',{data:id,headers: {'Content-Type': 'application/json'}});
          
        } else if(name == 'family'){
          console.log(' Delete data family=>', id);
          var i = id.length - 1;
          console.log(id.length,"total==>",i);
          var cv = id[i];
          var pH = [];
          for (var j = 0;j < cv.length; j++) {
              var obj = {};
              obj.CustomerId=cv[j].CustomerId;
               obj.ChangeTypeId=3;
               obj.Modul=cv[j].modul;
               obj.Attribute=cv[j].Name;
               obj.NewValue=null;
               obj.OldValue=null;

               pH.push(obj);
          };
          console.log("history",pH);
          id.splice(i, 1);
          console.log("====>",id);
          return $http.delete('/api/ct/DeleteCListFamily',{data:id,headers: {'Content-Type': 'application/json'}}).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);
                            // return $http.post('/api/ct/PostCHistoryDataChanged', jH);
                            // return $http.post('/api/ct/PostCListSocialMedia/', [data]);

                        }
                   // )
                );
          // return $http.delete('/api/ct/DeleteCListFamily', id);
          // return $http.delete('/api/ct/DeleteCListFamily',{data:id,headers: {'Content-Type': 'application/json'}});
        } else if(name == 'nonToyota'){
          console.log(' delete data non Toyota=>', id);
          var i = id.length - 1;
          console.log(id.length,"total==>",i);
          var cv = id[i];
          var pH = [];
          for (var j = 0;j < cv.length; j++) {
              var obj = {};
              obj.CustomerId=cv[j].CustomerId;
               obj.ChangeTypeId=3;
               obj.Modul=cv[j].modul;
               obj.Attribute=(cv[j].Model+'( '+cv[j].LicensePlate+' )');
               obj.NewValue=null;
               obj.OldValue=null;

               pH.push(obj);
          };
          console.log("history",pH);
          id.splice(i, 1);
          console.log("====>",id);
          return $http.delete('/api/ct/DeleteCListVehicle',{data:id,headers: {'Content-Type': 'application/json'}}).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);
                            // return $http.post('/api/ct/PostCHistoryDataChanged', jH);
                            // return $http.post('/api/ct/PostCListSocialMedia/', [data]);

                        }
                   // )
                );


          // return $http.delete('/api/ct/DeleteCListVehicle/', id);
          // return $http.delete('/api/ct/DeleteCListVehicle',{data:id,headers: {'Content-Type': 'application/json'}});
          // api/ct/DeleteCListSocialMedia
        } else if(name == 'preference'){
          console.log(' Delete preference=>', id);
          var i = id.length - 1;
          console.log(id.length,"total==>",i);
          var cv = id[i];
          var pH = [];
          for (var j = 0;j < cv.length; j++) {
              var obj = {};
              obj.CustomerId=cv[j].CustomerId;
               obj.ChangeTypeId=3;
               obj.Modul=cv[j].modul;
               obj.Attribute=cv[j].Description;
               obj.NewValue=null;
               obj.OldValue=null;

               pH.push(obj);
          };
          console.log("history",pH);
          id.splice(i, 1);
          console.log("====>",id);
          return $http.delete('/api/ct/DeleteCListPreference',{data:id,headers: {'Content-Type': 'application/json'}}).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);
                            // return $http.post('/api/ct/PostCHistoryDataChanged', jH);
                            // return $http.post('/api/ct/PostCListSocialMedia/', [data]);

                        }
                   // )
                );
          // return $http.delete('/api/ct/DeleteCListPreference/', id);
          // return $http.delete('/api/ct/DeleteCListPreference',{data:id,headers: {'Content-Type': 'application/json'}});
          // api/ct/DeleteCListSocialMedia
        } else if(name == 'socialMedia'){
          console.log(' Delete Sosial Media=>', id);
          var i = id.length - 1;
          console.log(id.length,"total==>",i);
          var cv = id[i];
          var pH = [];
          for (var j = 0;j < cv.length; j++) {
              var obj = {};
              obj.CustomerId=cv[j].CustomerId;
               obj.ChangeTypeId=3;
               obj.Modul=cv[j].modul;
               obj.Attribute=cv[j].SocialMediaAddress;
               obj.NewValue=null;
               obj.OldValue=null;

               pH.push(obj);
          };
          console.log("history",pH);
          id.splice(i, 1);
          console.log("====>",id);
          return $http.delete('/api/ct/DeleteCListSocialMedia',{data:id,headers: {'Content-Type': 'application/json'}}).then(
                        function(res){
                          console.log("==>",res);
                            // insert ke history
                             $http.post('/api/ct/PostCHistoryDataChanged', pH);

                        }
                   // )
                );
          // return $http.delete('/api/ct/DeleteCListSocialMedia/', id);
          // return $http.delete('/api/ct/DeleteCListSocialMedia',{data:id,headers: {'Content-Type': 'application/json'}});
          // 
        } ;
        // return $http.post('/api/ct/PostCListFamily', [data]);
        // return $http.delete('/api/as/Stall',{data:id,headers: {'Content-Type': 'application/json'}});
      }
    }
  });
