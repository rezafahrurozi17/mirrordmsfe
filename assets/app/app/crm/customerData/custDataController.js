angular.module('app')
  .controller('CustDataController', function($scope,$state, $http, CurrentUser, bsTransTree, CustData, bsNotify, bsAlert, ngDialog, CMaster, CFamily,CDataBasic,CAccessMatrix,MrsList, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    var resizeLayout = function() {
        console.log("Make sure this function called");
        $timeout(function() {
            // $("#layoutContainer_CustDataForm > .stretch").css("overflow-y","hidden");
            $("#layoutContainer_CustDataForm").height($(window).height());
            var a = $('div[name="CustDataForm"]').height();
            console.log("aaaaaaaaaaaaaaaaaaa ====",a);
            // $(".bs-listx > .bs-list > .panel-body > .ui-list-view-striped").height($(window).height() - 5);
            // $(".bs-listx > .bs-list > .panel-body > .ui-list-view-striped").style.setProperty("overflow","hidden","important");
         
            // $(".bs-listx > .bs-list > .panel-body > .ui-list-view-striped .ui-list-view-cell-content").each(function(){
            //   this.style.setProperty("position","relative");
            // });
        }, 100);
    };


    $scope.resize = function(){
      resizeLayout();
    }

    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = false;
      $scope.getSMSGateway();
      $scope.getHakAkses();
      resizeLayout();
      

    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    
    // $scope.user.OrgId = 260; // buat tes aja outlet TAM Wokshop Sunter II
    // $scope.user.RoleId = 1128; // buat tes aja Role Kepala Bengkel GR
    $scope.mCustData = {};
    $scope.acc1 = {};
    $scope.acc2 = {};
    $scope.selectedRows = [];
    $scope.selectedAddress = [];
    // $scope.selectedAddress = [];
    $scope.search2 = false;
    $scope.groupfilter = true;
    $scope.groupSearch = {};
    $scope.forgotPass = true;
    // $scope.alldata = false;
    $scope.show={mailStatus:true,phoneStatusValid1:true,phoneStatusValid2:true,mailStatusValid:true,alldataIn:false,alldata:false,modalPreference:false,modalSosial:false,tabW:false,advSearch:true,disableOtherTab:false,tab:{tabBasic:false,tabJob:false,tabFamily:false,tabVehicle:false,tabPreference:false,tabFA:false,tabHistory:false}};
    // $scope.alldataInstitusi = true;
    $scope.address = true;
    $scope.afamily = true;
    $scope.aInfoPreferensi = true;
    $scope.aSosMed = true;
    $scope.moPreferensi = "new";
    $scope.vehicle = true;
    $scope.KetPref = false;
    $scope.KetSosmed = false;
    $scope.infoInstitusi = false;
    $scope.infoIndividu = false;

    $scope.xenable = true;
    
    $scope.mData = {}; //Model
    $scope.xRole = {
      selected: []
    };
    $scope.gridOptions1 = {};
    $scope.gridOptions2 = {};
    $scope.grid33 = {};

    //---data scope
    $scope.mAddress={};
    $scope.mPreferensi={};
    $scope.newPref=false;
    $scope.newSosmed=false;
    $scope.mSosmed={};
    $scope.mVehicle={};

    $scope.valueChange = true;

    $scope.lmModel = {};
    $scope.Mlocation = [];
    $scope.modalMode='new';
    var dateFormat2='MM/yyyy';
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.SdateOptions = {
        startingDay: 1,
        format: dateFormat,
    };
    $scope.EdateOptions = {
        startingDay: 1,
        format: dateFormat,
    };
    $scope.serviceSdateOptions = {
      startingDay: 1,
      format: dateFormat,
    };
    $scope.serviceEdateOptions = {
        startingDay: 1,
        format: dateFormat,
    };
    $scope.dateOptions2 = {
        startingDay: 1,
        format: dateFormat2,
    };
    //$scope.modalPreference=false;
    //$scope.modalSosial = false;

    // $scope.ubah2 = false;
    // $scope.batal2 = true;x
    // $scope.simpan2 = true;
    // $scope.valueChange2 = true;

    // $scope.detailActionButtonSettings = [{actionType: 'edit', //Use 'Edit Action' of bsForm
    //                                   title: 'Ubah',icon: 'fa fa-fw fa-edit',color: 'rgb(213,51,55)'},];


    

    $scope.onBeforeCancel = function(hiya) {
         $timeout(function() {
            $("#layoutContainer_CustDataForm").height($(window).height());
        }, 1000)
    }


    function checkTime(i) {
      if (i < 10) {
          i = "0" + i;
      }
      return i;
    }
    function formatDate(date) {
      console.log("date==>",date);
        // var monthNames = [
        //   "January", "February", "March",
        //   "April", "May", "June", "July",
        //   "August", "September", "October",
        //   "November", "December"
        // ];
        // var monthIndex = date.getMonth();

        if (date != undefined) {
            var day = date.getDate();
            var month = date.getMonth()+1;
            var year = date.getFullYear();
            day = checkTime(day);
            month = checkTime(month);
            return year + '-' + month + '-' + day;  
        } else{
          return null
        };
        
      }
      function formatDate2(date) {
      console.log("date==>",date);
        var day = date.getDate();
        var month = date.getMonth()+1;
        var year = date.getFullYear();
        day = checkTime(day);
        month = checkTime(month);
        return day + '/' + month + '/' + year;
        
      }
    //----------------------------------
    // Detail Action Button
    //----------------------------------
    $scope.actionButtonSettingsDetail = [
        {
          // actionType: 'back', //Use 'Back Action' of bsForm
          title: 'Kembali',
            func: function(){
               $scope.formApi.setMode('grid');
               $scope.onBeforeCancel();
            }
          //icon: 'fa fa-fw fa-edit',
        },
        {
          enable:true,
          title: 'Simpan',
          icon: 'fa fa-fw fa-check',
          func: function(){
            console.log("detail action save=>",$scope.mCustData);
            // bsNotify.show({
            //   title: "Customer Data",
            //   content: "Data Berhasil di Simpan",
            //   type: 'success'
            // });
            $scope.mCustData.master={};
            $scope.mCustData.master.statusGender=[{id:1,name:'Laki - Laki'},{id:2,name:'Perempuan'}];
            $scope.mCustData.master.statusReligion=$scope.agamaData;
            $scope.mCustData.master.statusEthnic=$scope.sukuData;
            $scope.mCustData.master.statusLanguage=$scope.bahasaData;
            $scope.mCustData.master.statusMarital=$scope.marriedStatus;
            $scope.mCustData.master.statusFleet=$scope.fleetData;
            $scope.mCustData.master.statusJob=$scope.jobCategory;
            $scope.mCustData.master.statusPosition=$scope.positionData;
            $scope.mCustData.master.statusIncome=$scope.pendapatanData;
            $scope.mCustData.master.statusSektor=$scope.sektorBisnisData;
            $scope.mCustData.master.statusDepartemen=$scope.departmentData;
            console.log("DIDIEU YEUHHHHH ==>", $scope.mCustData.CustomerTypeId);
            console.log("CUSTDATA YEUHHHHH ==>", $scope.mCustData);
            console.log("ACC YEUHHHHH ==>", $scope.mCustData.acc1);
            if ($scope.mCustData.CustomerTypeId == 1 || $scope.mCustData.CustomerTypeId == 3) {


                var csphone = '';
                var csStatus = 0;

                
                  if($scope.mCustData.acc1[0].CustomerListPersonal[0].Handphone1 != $scope.mCustData.acc1File.CustomerListPersonal[0].Handphone1){
                    // csStatus = 1;
                    csphone = $scope.mCustData.acc1[0].CustomerListPersonal[0].Handphone1+'/-/-/';
                    CustData.ValidatePhoneEmail(csphone).then(function(res){
                     console.log("ValidatePhoneEmail==>",res);
                      if(res.data.Result[0].Valid != null){
                        // csStatus = 2;
                        $scope.show.phoneStatusValid1 = false;
                      }else{
                        csStatus =csStatus + 1;
                        $scope.show.phoneStatusValid1 = true;
                        $scope.updateProfile(csStatus);
                      }

                    });
                  }else {
                    //csphone = csphone+'-/';

                    csStatus =csStatus + 1;
                    $scope.show.phoneStatusValid1 = true;
                    $scope.updateProfile(csStatus);
                  }
                  if($scope.mCustData.acc1[0].CustomerListPersonal[0].Handphone2 != $scope.mCustData.acc1File.CustomerListPersonal[0].Handphone2){
                    // csStatus = 1;
                    csphone = '-/'+$scope.mCustData.acc1[0].CustomerListPersonal[0].Handphone2+'/-/';
                    CustData.ValidatePhoneEmail(csphone).then(function(res){
                    //  console.log("ValidatePhoneEmail==>",res);
                      if(res.data.Result[0].Valid != null){
                        // csStatus = 2;
                        $scope.show.phoneStatusValid2 = false;
                      }else{
                        // csStatus = 1;
                        csStatus =csStatus + 1;
                        $scope.show.phoneStatusValid2 = true;
                        $scope.updateProfile(csStatus);
                      }

                    });
                  }else {
                    // csphone = csphone+'-/';
                    csStatus =csStatus + 1;
                    $scope.show.phoneStatusValid2 = true;
                    $scope.updateProfile(csStatus);
                  }
                
               
                var xmail = angular.copy($scope.mCustData.acc1[0].CustomerListPersonal[0].Email);
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
                 if(xmail.match(mailformat)){
                      $scope.show.mailStatus = true;
                      if($scope.mCustData.acc1[0].CustomerListPersonal[0].Email != $scope.mCustData.acc1File.CustomerListPersonal[0].Email){
                        // csStatus = 1;
                        csphone = '-/-/'+$scope.mCustData.acc1[0].CustomerListPersonal[0].Email+'/';
                        CustData.ValidatePhoneEmail(csphone).then(function(res){
                        //  console.log("ValidatePhoneEmail==>",res);
                          if(res.data.Result[0].Valid != null){
                            // csStatus = 2;
                            $scope.show.mailStatusValid = false;

                          }else{
                            csStatus =csStatus + 1;
                            $scope.show.mailStatusValid = true;
                            $scope.updateProfile(csStatus);
                          }

                        });
                      }else {
                        // csphone = csphone+'-/';
                        csStatus =csStatus + 1;
                        $scope.show.mailStatusValid = true;
                        $scope.updateProfile(csStatus);
                      }
                 }else{
                      $scope.show.mailStatus = false;
                      // $scope.updateProfile(csStatus);
                      // csStatus = 2;

                 }  
             //  console.log("csphone=>",csphone,"csStatus=>",csStatus);
            }else{
              //institusi
              $scope.updateProfile(3);
            }
            // if ($scope.mCustData.CustomerTypeId == 2 || $scope.mCustData.CustomerTypeId > 3) {
            //   $scope.updateProfile(3);
            // }
          }
        },


        {
          enable:true,
          title: 'Batal',
          // icon: 'fa fa-fw fa-thumbs-up',
            func: function(){
              $scope.mData=$scope.mDataCopy;
              setEditMode(false);
              $scope.mCustData.acc1[0] = angular.copy($scope.mCustData.acc1File);
              $scope.mCustData.acc2[0] = angular.copy($scope.mCustData.acc2File);
              if($scope.backToMain){
                $scope.formApi.setMode('grid');
              }
            }
        },
        {
          title: 'Ubah',
          icon: 'fa fa-fw fa-edit',
            func: function(){
              console.log("detail action edit=>",$scope.mData);
              $scope.mDataCopy=angular.copy($scope.mData);            
              setEditMode(true);
              // $scope.actionButtonSettingsDetail[1].enable=true;
              // $scope.actionButtonSettingsDetail[2].enable=true;
              // $scope.actionButtonSettingsDetail[3].enable=false;
              $scope.backToMain=false;
            }
        },

    ];

    // $scope.dateO = function(){

    //           var cBD = formatDate($scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate);
    //           var mD = formatDate($scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate);
    //           var cBD2 = formatDate($scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate);
    //           var mD2 = formatDate($scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate);

    //           $scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate = cBD;
    //           $scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate = mD;
    //           $scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate = cBD2;
    //           $scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate = mD2;
    // }

    $scope.tabC = function(xdata){
      console.log("==>",xdata);
      
      console.log(xTabEdit , "===>",xdata, "===>",xEdit);
      //if(xTabEdit && (xdata == "Pribadi")) {console.log("1==>",xEdit&1); $scope.actionButtonSettingsDetail[3].visible= ((xEdit&1) ? true : false); };
      // if(xTabEdit && (xdata == "Pribadi")) {console.log("1==>",xEdit&1); $scope.actionButtonSettingsDetail[3].visible= true;};
      // if(xTabEdit && (xdata == "Pekerjaan")) {console.log("2==>",xEdit&2); $scope.actionButtonSettingsDetail[3].visible= true;};
      // if(xTabEdit && (xdata == "Keluarga")) {console.log("3==>",xEdit&4); $scope.actionButtonSettingsDetail[3].visible= true;};
      // if(xTabEdit && (xdata == "Kendaraan")) { $scope.actionButtonSettingsDetail[3].visible= true;};
      // if(xTabEdit && (xdata == "Preferensi")) {console.log("5==>",xEdit&16); $scope.actionButtonSettingsDetail[3].visible= true;};
      // if(xTabEdit && (xdata == "Field Action")) {console.log("6==>",xEdit&32); $scope.actionButtonSettingsDetail[3].visible= true;};
      // if(xTabEdit && (xdata == "Riwayat Perubahan Data")) {console.log("7==>",xEdit&64); $scope.actionButtonSettingsDetail[3].visible= true;};
    
      // ============================================= comment jadi pakai service aja cek nya ============================ start

      // if(xTabEdit && (xdata == "Pribadi")) {console.log("1==>",xEdit&1); $scope.actionButtonSettingsDetail[3].visible= true;};
      // if(xTabEdit && (xdata == "Pekerjaan")) {console.log("2==>",xEdit&2); $scope.actionButtonSettingsDetail[3].visible= ((xEdit&2) ? true : false);};
      // if(xTabEdit && (xdata == "Keluarga")) {console.log("3==>",xEdit&4); $scope.actionButtonSettingsDetail[3].visible= ((xEdit&4) ? true : false);};
      // //if(xTabEdit && (xdata == "Kendaraan")) { $scope.actionButtonSettingsDetail[3].visible= ((xEdit&8) ? true : false);};
      // // if(xTabEdit && (xdata == "Kendaraan")) {setEditMode(true); $scope.actionButtonSettingsDetail[3].visible= false;  $scope.actionButtonSettingsDetail[1].visible= false;  $scope.actionButtonSettingsDetail[2].visible= true;$scope.actionButtonSettingsDetail[0].visible=false};
      // if(xTabEdit && (xdata == "Kendaraan")) { $scope.actionButtonSettingsDetail[3].visible= true;  $scope.actionButtonSettingsDetail[1].visible= false;  $scope.actionButtonSettingsDetail[2].visible= false;$scope.actionButtonSettingsDetail[0].visible=true};
      // if(xTabEdit && (xdata == "Preferensi")) {console.log("5==>",xEdit&16); $scope.actionButtonSettingsDetail[3].visible= ((xEdit&16) ? true : false);};
      // if(xTabEdit && (xdata == "Field Action")) {console.log("6==>",xEdit&32); $scope.actionButtonSettingsDetail[3].visible= false;};
      // if(xTabEdit && (xdata == "Riwayat Perubahan Data")) {console.log("7==>",xEdit&64); $scope.actionButtonSettingsDetail[3].visible= false;};
        // ============================================= comment jadi pakai service aja cek nya ============================ end
      
        for (var i=0; i<$scope.HakAkses.length; i++){
          if ($scope.HakAkses[i].TabName == "Informasi Data Pribadi"){
            if((xdata == "Pribadi")) {
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
            };
          }
          if ($scope.HakAkses[i].TabName == "Informasi Pekerjaan"){
            if((xdata == "Pekerjaan")) {
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
            };
          }
          if ($scope.HakAkses[i].TabName == "Informasi Keluarga"){
            if((xdata == "Keluarga")) {
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
            };           
          }
          if ($scope.HakAkses[i].TabName == "Informasi Kendaraan"){
            if((xdata == "Kendaraan")) { 
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
            };                       
          }
          if ($scope.HakAkses[i].TabName == "Informasi Preferensi"){
            if((xdata == "Preferensi")) {
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
            };                                  
          }
          if ($scope.HakAkses[i].TabName == "Informasi Field Action"){
            if((xdata == "Field Action")) {
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
            };                                               
          }
          if ($scope.HakAkses[i].TabName == "Informasi Riwayat Perubahan"){
            if((xdata == "Riwayat Perubahan Data")) {
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
            };                                                          
          }
        }
        
    }

    
    var setEditMode = function(editing){
        // console.log("editing==>",editing);
        xTabEdit = !editing;
      $scope.actionButtonSettingsDetail[0].visible=!editing; //back
      $scope.actionButtonSettingsDetail[1].visible=editing;  //save
      $scope.actionButtonSettingsDetail[2].visible=editing;  //cancel
      $scope.actionButtonSettingsDetail[3].visible=!editing;
      // $scope.actionButtonSettingsDetail[4].visible=!editing;
      // $scope.actionButtonSettingsDetail[5].visible=!editing;
      $scope.show.disableOtherTab = editing;
      $scope.formApi.setFormReadOnly(!editing);
    }
    var xTabEdit = true;
    var xtab = 0;
    var xEdit = 0;
    $scope.outletRolex = {OutletId:$scope.user.OrgId,RoleId:$scope.user.RoleId};
    console.log('$scope.outletRolex',$scope.outletRolex);

        if ($scope.user.OrgId == 655) {
            if($scope.user.OrgId == 655){
                $scope.show.tab.tabBasic = true;
                $scope.show.tab.tabJob = true;
                $scope.show.tab.tabFamily = true;
                $scope.show.tab.tabVehicle = true;
                $scope.show.tab.tabPreference = true;
                $scope.show.tab.tabFA = true;
                $scope.show.tab.tabHistory = true;

                xEdit = 127;
            }
        } else{
            CAccessMatrix.getData($scope.outletRolex).then(function(res){

                    // $scope.detailActionButtonSettings.edit.enable=false;

                    console.log("xtaaab==>",res);
                    console.log("call me maybe",$scope.user.OrgId);
                    
                    if ($scope.user.OrgId != 1000038){
                     
                      xtab = res.data.Result[0].RightByte;
                      xEdit = res.data.Result[0].RightByteEdit;
                      console.log("taylor swift");
                    }
                    else{
                      xEdit = 1;
                      xEdit = 1;
                    }
                   
                    console.log("xtaaab==>",xtab);
                    if(xtab&1){$scope.show.tab.tabBasic = true};
                    if(xtab&2){$scope.show.tab.tabJob = true};
                    if(xtab&4){$scope.show.tab.tabFamily = true};
                    if(xtab&8){$scope.show.tab.tabVehicle = true};
                    if(xtab&16){$scope.show.tab.tabPreference = true};
                    if(xtab&32){$scope.show.tab.tabFA = true};
                    if(xtab&64){$scope.show.tab.tabHistory = true};
                    if (xEdit > 0) {
                          $scope.actionButtonSettingsDetail[0].visible=true; //back
                          $scope.actionButtonSettingsDetail[1].visible=false;  //save
                          $scope.actionButtonSettingsDetail[2].visible=false;  //cancel
                          $scope.actionButtonSettingsDetail[3].visible=!false;
                    };
                    

                    // $scope.detailActionButtonSettings = [{actionType: 'edit', //Use 'Edit Action' of bsForm
                    //                   title: 'Ubah',icon: 'fa fa-fw fa-edit',color: 'rgb(213,51,55)'},];
                    // console.log("ress===>",res);



                    // $scope.detailActionButtonSettings = [{actionType: 'Ubah', //Use 'Edit Action' of bsForm
                    //                   title: 'Ubah',icon: 'fa fa-fw fa-edit',color: 'rgb(213,51,55)'},]; 
                    // if (res.data.Result.length > 0) {
                    //         var cmenu = res.data.Result[0].RightByte;
                    //         if (res.data.Result[0].RightByteEdit > 0) {
                    //             $scope.detailActionButtonSettings = [{actionType: 'Ubah', //Use 'Edit Action' of bsForm
                    //                   title: 'Ubah',icon: 'fa fa-fw fa-edit',color: 'rgb(213,51,55)'},];  
                    //         };
                            
                    //     } else{
                    //         var cmenu = 0;
                    //         $scope.detailActionButtonSettings = [];
                    //     };

            })
        };
        
        
    //hak akses
    // if ($scope.user.OrgTypeId == 1) {
    //   $scope.show.advSearch = true;
    // }
    console.log($scope.user,"niiii==>",$scope.user.OrgTypeId);
    if ($scope.user.OrgTypeId == 1 || $scope.user.OrgId == 1000038) {$scope.show.advSearch = true;}
    else { $scope.show.advSearch = false;}   
    $scope.checkRights=function(bit){
        console.log('$scope.myRights',$scope.myRights);
        var p=$scope.myRights & Math.pow(2,bit);
        var res=(p==Math.pow(2,bit));
        return res;
    }



    $timeout(function(){
        // if($scope.checkRights(11)){
        //         xRep.push({id:1, name:"- Report RSSP Regular"});
        // }
        // $scope.report = xRep;
    })





    var dateFormat='dd/MM/yyyy';
    var dateFormatFilter='dd-MM-yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var condPhoneStatus=0;
    $scope.dateOptions = {
      startingDay: 1,
      format: dateFormat
      // disableWeekend: 1
    };

    $scope.dateOptionsFilter = {
      startingDay: 1,
      format: dateFormatFilter
      // disableWeekend: 1
    };

    $scope.buyStartDateOptions = {
        startingDay: 1,
        format: dateFormat,
        disableWeekend: 1,
        minDate: "2017-05-08"
    };
    $scope.buyEndDateOptions = {
        startingDay: 1,
        format: dateFormat,
        disableWeekend: 1
    };
    
    $scope.filterSearch = {flag:"",TID:"",filterValue:"",choice:false};
    $scope.filterField = [{key: "Nopol",desc: "Nomor Polisi",flag:1,value:""},
                          {key: "NoRank",desc: "Nomor Rangka",flag:2,value:""},
                          {key: "NoMesin",desc: "Nomor Mesin",flag:3,value:""},
                          {key: "TTL",desc: "Tanggal Lahir",flag:6,value:""},
                          {key: "Phone",desc: "Nomor Handphone",flag:4,value:""},
                          {key: "NoKTP",desc: "Nomor KTP/KITAS",flag:5,value:""}];
    $scope.mForgot = {show:false,flag:"",name:"",valueHp:"",valueEmail:""};
    $scope.filterForgot = [{key: "phone",desc: "Nomor Handphone",flag:1,value:""},{key: "Email",desc: "Alamat Email",flag:2,value:""}];
    //----------------------------------
    $scope.filterFieldChange=function(item){
        $scope.filterSearch.flag = item.flag;
        $scope.filterSearch.filterValue = "";
        $scope.filterFieldSelected = item.desc;

    }
    $timeout(function(){
        $scope.filterFieldChange($scope.filterField[0]);
    },0);
    //----------------------------------
    // DETAIL ACTION BUTTON
    // if ($scope.user.) {} else{};
    // $scope.detailActionButtonSettings = [
    // {
    //       actionType: 'edit', //Use 'Edit Action' of bsForm
    //       title: 'Ubah',icon: 'fa fa-fw fa-edit',color: 'rgb(213,51,55)'}
    //       ];
    // bsAlert.alert(title:{},);
    //----------------------------------
    // Action Button
    //----------------------------------
   
    

    $scope.listButtonSettingsGroupSearch={view:{enable:false},new:{enable:false},edit:{enable:false},delete:{enable:false}};
    $scope.listCustomButtonSettingsGroupSearch=[{enable:true,
                                              // title:"Jadikan Utama",
                                              icon:"fa fa-ambulance",
                                              func:function(row){
                                                    console.log("customButton",row);
                                                    $scope.ininovin = row.VIN;
                                                    console.log("no vin apac",  $scope.ininovin);
                                                    // var vd={VehicleId:row.VehicleId,crm:1};
                                                    // $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'view',vd,true,true);
                                                    // $scope.onShowDetail(row,'view');
                                                    // $scope.formApi.setMode('detail');
                                                    // $scope.onShowDetail(row,'view');
                                                    // setEditMode(false);
                                                    // console.log("customButton",row);
                                                    var vd={VehicleId:row.VehicleId,crm:1,linkView:1, VIN:$scope.ininovin };
                                                    $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'view',vd,true,true,1);
                                              }
                                            },{enable:true,
                                              // title:"Jadikan Utama",
                                              icon:"glyphicon glyphicon-user",
                                              // icon:"fa fa-external-link",
                                                
                                              func:function(row){
                                                    console.log("customButton",row);
                                                    // var vd={VehicleId:row.VehicleId,crm:1};
                                                    // $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'view',vd,true,true);
                                                    // $scope.onShowDetail(row,'view');
                                                    $scope.ininovin = row.VIN;
                                                    console.log("no vin apac",  $scope.ininovin);
                                                    $scope.formApi.setMode('detail');
                                                    setEditMode(false);
                                                    $scope.onShowDetail(row,'view');
                                              }
                                            }]
                                        // ,{enable:true,
                                        //   caption:"Jadikan Utama",
                                        //   icon:"glyphicon glyphicon-home",
                                        //   func:function(row){
                                        //         console.log("customButton",row);
    
                                        //         CustData.updateMainAddress(row);
                                        //   }
                                        // }
                                        ;
    $scope.listButtonSettings={view:{enable:false},new:{enable:true,icon:"glyphicon glyphicon-home"}};//[{view:{enable:false}},{new:{enable:true,icon:"glyphicon glyphicon-home"}}];
    $scope.listCustomButtonSettings=[{enable:true,
                                      caption:"Jadikan Utama",
                                      icon:"glyphicon glyphicon-home",
                                      func:function(row){
                                            console.log("customButton",row);
                                            $scope.ininovin = row.VIN;
                                            console.log("no vin apac",  $scope.ininovin);

                                            CustData.updateMainAddress(row).then(function(res){
                                              $scope.onShowDetail(detailData,'view');
                                            });
                                      }
                                    }];
    $scope.listButtonSettingsFamily={view:{enable:false},new:{enable:true,icon:"fa fa-group"}};
    $scope.listButtonSettingsToyota={new:{enable:false},view:{enable:false},edit:{enable:false},delete:{enable:false}};//[{view:{enable:false}},{edit:{enable:false}},{new:{enable:false,icon:"glyphicon glyphicon-home"}}];
    

    $scope.listCustomButtonSettingsToyota=[{enable:true,
                                          // title:"Jadikan Utama",
                                          icon:"fa fa-external-link",
                                          func:function(row){                                            
                                            console.log("no vin apa ini",  $scope.ininovin);
                                                console.log("customButton",row);
                                                var vd={VehicleId:row.VehicleId,crm:1,linkView:1,VIN : $scope.ininovin};
                                                $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'view',vd,true,true,1);

                                          }
                                      // skip-enable:true
                                    }];
    // $scope.listButtonSettingsNonToyota=((xEdit&8) ? {new:{enable:true},view:{enable:true},edit:{enable:true},delete:{enable:true}} : {new:{enable:false},view:{enable:false},edit:{enable:false},delete:{enable:false}});
    $scope.listButtonSettingsNonToyota={new:{enable:true},view:{enable:true},edit:{enable:true},delete:{enable:true}};


    $scope.listButtonSettingsFASSC={new:{enable:false},view:{enable:false},edit:{enable:false},delete:{enable:false}};
                                    
    $scope.listButtonSettingsHistory={view:{enable:false},edit:{enable:false},new:{enable:false},delete:{enable:false}}
    $scope.buttonSettingModal = {save:{text:'Check Toyota ID'}};
    //----------------------------------
    // Get Data
    //----------------------------------
    //
   // V
   var MasterPreference = [];
   var MasterSosialMedia = [];

    CMaster.GetCFamilyRelation().then(function(res){
        // console.log("Family Relation====>",res);
        $scope.catFamily = res.data.Result;
      })
      
    CMaster.GetCCustomerFleet().then(function(res){
        $scope.fleetData = res.data.Result;
        // console.log("Fleet====>",res);
      })
    
     
    CMaster.getReligion().then(function(res){
        // console.log("List Religion====>",res);
        $scope.agamaData = res.data.Result;
        
      })
    
    CMaster.getEthnic().then(function(res){
        // console.log("List Suku====>",res);
        $scope.sukuData = res.data.Result;
      })

    CMaster.getCStatusMarital().then(function(res){
        // console.log("List Suku====>",res);
        $scope.marriedStatus = res.data.Result;
      })

    CMaster.getCCategory().then(function(res){
        console.log("List kategory Customer====>",res);
        $scope.categoryData = res.data.Result;
      })
    
    CMaster.GetCLanguange().then(function(res){
        // console.log("List Bahasa====>",res);
        $scope.bahasaData = res.data.Result;
      })

      
    CMaster.GetCSectorBisnis().then(function(res){
        // console.log("List Sektor Bisnis====>",res);
        $scope.sektorBisnisData = res.data.Result;
      })
    
    CMaster.GetCPosition().then(function(res){
        // console.log("List Position====>",res);
        $scope.positionData = res.data.Result;
      })
     
    CMaster.GetCGrossIncome().then(function(res){
        // console.log("List pendapatanData====>",res);
        $scope.pendapatanData = res.data.Result;
      })
     
    

    CMaster.GetDepartment().then(function(res){
        // console.log("List CategoryAddress====>",res.data.Result);
        $scope.departmentData = res.data.Result;
    })
    
    CMaster.GetCProvince().then(function(res){
        console.log("List Province====>",res.data.Result);
        $scope.provinsiData = res.data.Result;
      })
    
//     CMaster.GetCRegency().then(function(res){
//         console.log("List Kota====>",res);
//         $scope.kabupatenData = res.data.Result;
//       })
    
//     CMaster.GetCDistrict().then(function(res){
//         console.log("List Kecamatan====>",res);
//         $scope.kecamatanData = res.data.Result;
//       })
      
// // GetCVillage
//     CMaster.GetCVillage().then(function(res){
//         console.log("List Kelurahan====>",res);
//         $scope.kelurahanData = res.data.Result;
//       })
    CMaster.GetCJobCategory().then(function(res){
        $scope.jobCategory = res.data.Result;
    })
 
    // CMaster.GetLocation().then(function(res){
    //     // console.log("List Province====>",res.data.Result);
    //     $scope.provinsiData = res.data.Result;         
    //   })
    CMaster.GetCMasterSocialMedia().then(function(res){
        // console.log("List SosialMedia====>",res);
        $scope.sosmedData = res.data.Result;
        MasterSosialMedia = angular.copy(res.data.Result);
      })
    
    CMaster.GetCMasterPreference().then(function(res){
        // console.log("List Preference====>",res);
        $scope.preferenceData = res.data.Result;
        MasterPreference = angular.copy(res.data.Result);
      })
      
    CMaster.GetCVhehicleBrand().then(function(res){
        // console.log("List Vehicle Brand====>",res);
        $scope.VehicleBrand = res.data.Result;
      })
    
    CMaster.GetCVhehicleCategory().then(function(res){
        // console.log("List Vehicle Brand====>",res);
        $scope.VehicleCategory = res.data.Result;
      })

    CMaster.GetCVehicleModel().then(function(res){
        // console.log("List Vehicle Brand====>",res);
        $scope.modelVehicle = res.data.Result;
      })
    $scope.gDealerData =[];
    $scope.outletData =[];
    if($scope.user.OrgId == 655 ){
      CAccessMatrix.getDealOut($scope.user).then(function(result) {
          // $scope.orgData = result.data.Result;
          $scope.gDealerData = result.data.Result;
          console.log("orgData=>",result.data);
          // $scope.ctlOrg.expand_all();
        });
        // CMaster.getDealOut().then(function(res){
        //     console.log("List gDealerData ====>",res);
        //     $scope.gDealerData = res.data.Result;
        //   })
    }
    //console.log("APA",$scope.groupSearch.dealerId);
    else if ($scope.user.OrgId == 1000038){
      CAccessMatrix.getDealOut($scope.user).then(function(result) {
        // $scope.orgData = result.data.Result;
        $scope.gDealerData = result.data.Result;
        console.log("orgData=>",result.data);
        // $scope.ctlOrg.expand_all();
      });
      CAccessMatrix.getOutletcrm($scope.groupSearch).then(function(result) {
        // $scope.orgData = result.data.Result;
        $scope.outletData = result.data.Result;
        console.log("$scope.outletData ",$scope.outletData );
        // $scope.ctlOrg.expand_all();
      });
      // CMaster.getDealOut().then(function(res){
      //     console.log("List gDealerData ====>",res);
      //     $scope.gDealerData = res.data.Result;
      //   })

    }

    $scope.getSMSGateway = function () {
            MrsList.getSMSGateway().then(function (resu) {
                console.log('resu smsgateway', resu.data.Result);
                var smsGateway = resu.data.Result[0].Name.split("|");
                console.log('smsGateway smsgateway', smsGateway);
                $scope.arrSmsGateway = smsGateway;
            });
        };
    $scope.getHakAkses = function () {
      CustData.getHakAkses().then(function (resu) {
            console.log('resu Hak Akses', resu);
            $scope.HakAkses = resu.data.Result;
        });
    };
    // $scope.sendSMS = function (row) {
    //         console.log('row mode sendSMS', row);
    //         // console.log('smsText', $scope.dataGrid.SMSMessage);
    //         var arr = [];
    //         var nextDate = new Date();
    //         nextDate.setHours(24, 0, 0, 0);
    //         console.log('nextDate', nextDate);
    //         // var text = "Halo Kamu yang disana!~!+_)+!$";
    //         // var asdtext = encodeURIComponent(text);
    //         // console.log('asdtext', asdtext);
    //         // var arrhp = [];
    //         _.map(row.Grid, function (val, key) {
    //             console.log('val row.grid >>', val);
    //             var arrUrl = [];
    //             _.map($scope.arrSmsGateway, function (o, k) {
    //                 if (k === 0) {
    //                     arrUrl[k] = o + val.Phone;
    //                 } else if (k === 1) {
    //                     arrUrl[k] = o + encodeURIComponent(row.SMSMessage);
    //                 }
    //             });
    //             console.log('$scope.arrSmsGateway >>', arrUrl);
    //             var obj = {};
    //             obj.ParamBody = { 'to': val.Phone, 'message': row.SMSMessage };
    //             obj.ExecuteTime = nextDate;
    //             obj.Phone = val.Phone;
    //             obj.Message = row.SMSMessage;
    //             obj.Url = arrUrl[0] + arrUrl[1];
    //             // arrhp.push(val.Phone);
    //             arr.push(obj);
    //         });
    //         console.log('arr', arr);
    //         // MrsList.sendSMS(arr).then(function (resu) {
    //         //     $scope.dataGrid.SMSMessage = "";
    //         //     console.log('smsText', resu.data);
    //         // });
    //     };

    var amenu =0;
    var tBasic =0;
    var tAdvanted = 0;
    CDataBasic.GetBasicDataOutlet().then(function(res) {
          // $scope.grid.data = result.data.Result;
          console.log("datDataBasic=>",res.data);
          for (var i = 0; i < res.data.Result.length ; i++) {
            if(res.data.Result[i].FlagState == 1){
                tBasic = tBasic + res.data.Result[i].RightByte;
            }else if(res.data.Result[i].FlagState == 2){
                tAdvanted = tAdvanted + res.data.Result[i].RightByte;
            };

          };
          // $scope.ctlOrg.expand_all();
        });
    
        
     $scope.selectProvince = function(row,lm){
      console.log($scope.lmModel,"row Province",row,lm);
        // $scope.kabupatenData = row.MCityRegency;
        lm.MVillage.MDistrict.CityRegencyId = '';
        lm.MVillage.DistrictId='';
        lm.VillageId='';
        CMaster.GetCRegency(row.ProvinceId).then(function(res){
                  // console.log("resss Regency==>",res);
                  $scope.kabupatenData = res.data.Result;
                  $scope.kecamatanData = [];
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
     }
     $scope.selectRegency = function(row,lm){
      console.log("row Regency",row,lm);
        // $scope.kecamatanData = row.MDistrict;
        lm.MVillage.DistrictId='';
        lm.VillageId='';
        CMaster.GetCDistrict(row.CityRegencyId).then(function(res){
                  // console.log("resss District==>",res);
                  $scope.kecamatanData = res.data.Result;
                  $scope.kelurahanData = [];
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
     }
     $scope.selectDistrict = function(row,lm){
      console.log("row District",row,lm);
        // $scope.kelurahanData = row.MVillage;
        lm.VillageId='';
        CMaster.GetCVillage(row.DistrictId).then(function(res){
                  // console.log("resss Village==>",res);
                  $scope.kelurahanData = res.data.Result;
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
     }
     $scope.listApiAddress = {};
     $scope.selectVillage = function(row, lm){
        // $scope.lmModel.MVillage = row.MVillage;  
        console.log('data kelurahan', $scope.kelurahanData)
        console.log('data kelurahan selected', $scope.lmModel.VillageId)
        console.log('data row', row)
        console.log('harusnya postal code', lm)
        lm.MVillage.PostalCode = row.PostalCode == 0 ? '-' : row.PostalCode;
        $scope.listApiAddress.addDetail(false, lm);

     }

    $scope.selectModel = function(selected){
        $scope.selectedModel = selected;
        console.log("$scope.selectedModel",$scope.selectedModel);
        var cType = [];
        for (var i = $scope.selectedModel.MVehicleType.length - 1; i >= 0; i--) {

          var resultAA = _.findWhere(cType, {Description: $scope.selectedModel.MVehicleType[i].Description});
          console.log(cType,"result search==>",resultAA);

          if (resultAA == undefined) {
              cType.push($scope.selectedModel.MVehicleType[i]);  
          };
          
        };
        // $scope.typeVehicle = $scope.selectedModel.MVehicleType;
        $scope.typeVehicle = cType;
        
    }

    $scope.selectDealer = function(selected){
        $scope.selectedDealer = selected;
        // console.log("$scope.selectedModel",$scope.selectedModel);
        // $scope.outletData = $scope.selectedDealer.Child;
        $scope.outletData = selected.listOutlet;
        console.log("apa", $scope.groupSearch.dealerId);
        CAccessMatrix.getOutletcrm($scope.groupSearch).then(function(result) {
          // $scope.orgData = result.data.Result;
          $scope.outletData = result.data.Result;
          console.log("$scope.outletData ",$scope.outletData );
          // $scope.ctlOrg.expand_all();
        });
    }
    

    // ------- dummy data ----
    // $scope.cars = ['Audi', 'BMW', 'Merc'];
    // $scope.bikes = ['Yamaha', 'Honda', 'Enfield'];
    //[{id:1,name:"Lancer"},{id:2,name:"Skyline"}];
    //{id:1,name:"Bus"},{id:2,name:"Truk"};
    $scope.merkVehicle = [{id:1,name:"Mitshubishi"},{id:2,name:"Honda"}];
    $scope.peran = [{id:1,name:"Pemilik"},{id:2,name:"Pemakai"}];
    // $scope.agamaData  = [{id:1,name:"Islam"},{id:2,name:"Kristen"},{id:2,name:"Buddha"}];
    // $scope.sukuData  = [{id:1,name:"Jawa"},{id:2,name:"Sunda"}];
    // $scope.bahasaData  = [{id:1,name:"Indonesia"},{id:2,name:"China"}];
    // $scope.fleetData  = [{FleetId:1,Fleet:"Fleet"},{FleetId:2,Fleet:"Non Fleet"}];
    // $scope.positionData = [{id:1,name:"Direktur"},{id:2,name:"Manager"},{id:3,name:"Supervisor"},{id:2,name:"Office Boy"}];
    // $scope.categoryData  = [{id:1,name:"Pengguna"},{id:2,name:"Pemakai"}];
    // $scope.categoryContact = [{id:1,name:"Rumah"},{id:2,name:"Kantor"}];
    // $scope.marriedStatus = [{id:1,name:"Kawin"},{id:2,name:"Belum Kawin"},,{id:3,name:"Cerai"}];
    // $scope.pendapatanData = [{id:1,name:"1 Milyar - 15 Milyar"},{id:2,name:"16 Milyar - 25 Milyar"}];
    // $scope.provinsiData = [{id:1,name:"Papua Barat"},{id:2,name:"Jakarta"},{id:3,name:"Jawa Tengah"}];
    // $scope.kabupatenData = [{id:1,name:"Ujung Timur"},{id:2,name:"Ujung Tengah"},{id:3,name:"Ujung Tenggara"}];
    // $scope.kecamatanData = [{id:1,name:"Desa Timur"},{id:2,name:"Desa Tengah"},{id:3,name:"Desa Tenggara"}];
    // $scope.kelurahanData = [{id:1,name:"Haji"},{id:2,name:"Korakal"},{id:3,name:"Lembayung"}];
    // $scope.catFamily = [{id:1,name:"Suami"},{id:2,name:"Istri"},{id:3,name:"Ayah"},{id:4,name:"Ibu"},{id:5,name:"Anak"}];
    $scope.informasiPreferensi = [{id:1,name:"Makanan Favorit"},{id:2,name:"Minuman Favorit"},{id:3,name:"Hobby"},{id:4,name:"Musik Favorit"},{id:5,name:"Olahraga Favorit"}];
    $scope.sosmed = [{id:1,name:"Facebook"},{id:2,name:"Twitter"},{id:3,name:"Youtube"}];
    $scope.merkVehicle = [{id:1,name:"Toyota"},{id:2,name:"Mazda"},{id:3,name:"Mitshubishi"}];
    $scope.categoryVehicle = [{id:1,name:"Mobil"},{id:2,name:"Truk"},{id:3,name:"Bus"}];
    // $scope.modelVehicle = [{id:1,name:"Avanza"},{id:2,name:"Fortuner"},{id:3,name:"Cayla"}];
    // $scope.typeVehicle = [{id:1,name:"All New 1.3 G A/T"},{id:2,name:"All New 1.5 G A/T"},{id:3,name:"All New 1.3 G M/T"}];
    $scope.colourVehicle = [{id:1,name:"Merah"},{id:2,name:"Transparan"},{id:3,name:"Hijau"}];
    
    $scope.updateProfile = function(csStatus){
      console.log("updateProfile==>",csStatus);

      if(csStatus == 3){
            if ($scope.mCustData.CustomerTypeId == 1 || $scope.mCustData.CustomerTypeId == 3){
                if ($scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate != null && $scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate != undefined){
                  $scope.BirthDate1 = new Date($scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate);               
                  var d1 = $scope.BirthDate1.getDate();
                  var dd1 = d1 < 10 ? '0' + d1 : d1;
                  var m1 = $scope.BirthDate1.getMonth() + 1;
                  var mm1 = m1 < 10 ? '0' + m1 : m1;
                  var y1 = $scope.BirthDate1.getFullYear();                
                  var cBD = y1 + "-" + mm1 + "-" + dd1;
                }
                else {
                  var cBD = null 
                }

                if ($scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate != null && $scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate != undefined){
                  $scope.MaritalDate1 = new Date($scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate);               
                  var d2 = $scope.MaritalDate1.getDate();
                  var dd2 = d2 < 10 ? '0' + d2 : d2;
                  var m2 = $scope.MaritalDate1.getMonth() + 1;
                  var mm2 = m2 < 10 ? '0' + m2 : m2;
                  var y2 = $scope.MaritalDate1.getFullYear();                
                  var mD = y2 + "-" + mm2 + "-" + dd2;
                }
                else {
                  var mD = null 
                }

                if ($scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate != null && $scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate != undefined){
                  $scope.BirthDate3 = new Date($scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate);               
                  var d3 = $scope.BirthDate3.getDate();
                  var dd3 = d3 < 10 ? '0' + d3 : d3;
                  var m3 = $scope.BirthDate3.getMonth() + 1;
                  var mm3 = m3 < 10 ? '0' + m3 : m3;
                  var y3 = $scope.BirthDate3.getFullYear();                
                  var cBD2 = y3 + "-" + mm3 + "-" + dd3;
                }
                else {
                  var cBD2 = null 
                }

                if ($scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate != null && $scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate != undefined){
                  $scope.MaritalDate4 = new Date($scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate);               
                  var d4 = $scope.MaritalDate4.getDate();
                  var dd4 = d4 < 10 ? '0' + d4 : d4;
                  var m4 = $scope.MaritalDate4.getMonth() + 1;
                  var mm4 = m4 < 10 ? '0' + m4 : m4;
                  var y4 = $scope.MaritalDate1.getFullYear();                
                  var mD2 = y4 + "-" + mm4 + "-" + dd4;
                }
                else {
                  var mD2 = null 
                }
               

                      // var cBD = formatDate($scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate);
                      // var mD = formatDate($scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate);
                      // var cBD2 = formatDate($scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate);
                      // var mD2 = formatDate($scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate);
                      // console.log("cBD utm==>",cBD);
                      console.log("cBD utm==>",cBD);
                      $scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate = cBD;
                      $scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate = mD;
                      $scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate = cBD2;
                      $scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate = mD2;
                      console.log("$scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate",$scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate);
                      console.log("$scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate",$scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate);
                      console.log("$scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate",$scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate);
                      console.log("$scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate",$scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate);

                      CustData.update($scope.mCustData).then(function(res){
                      $scope.onShowDetail(detailData,'view');
                      setEditMode(false);
                      if($scope.backToMain){
                        $scope.formApi.setMode('grid');
                      }      
                  });
            }else{

                  if ($scope.mCustData.acc1 != undefined && $scope.mCustData.acc1 != null ) {
                      if ($scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate != null && $scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate != undefined){
                        $scope.BirthDate1 = new Date($scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate);               
                        var d1 = $scope.BirthDate1.getDate();
                        var dd1 = d1 < 10 ? '0' + d1 : d1;
                        var m1 = $scope.BirthDate1.getMonth() + 1;
                        var mm1 = m1 < 10 ? '0' + m1 : m1;
                        var y1 = $scope.BirthDate1.getFullYear();                
                        var cBD = y1 + "-" + mm1 + "-" + dd1;
                      }
                      else {
                        var cBD = null 
                      }

                      if ($scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate != null && $scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate != undefined){
                        $scope.MaritalDate1 = new Date($scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate);               
                        var d2 = $scope.MaritalDate1.getDate();
                        var dd2 = d2 < 10 ? '0' + d2 : d2;
                        var m2 = $scope.MaritalDate1.getMonth() + 1;
                        var mm2 = m2 < 10 ? '0' + m2 : m2;
                        var y2 = $scope.MaritalDate1.getFullYear();                
                        var mD = y2 + "-" + mm2 + "-" + dd2;
                      }
                      else {
                        var mD = null 
                      }

                      if ($scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate != null && $scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate != undefined){
                        $scope.BirthDate3 = new Date($scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate);               
                        var d3 = $scope.BirthDate3.getDate();
                        var dd3 = d3 < 10 ? '0' + d3 : d3;
                        var m3 = $scope.BirthDate3.getMonth() + 1;
                        var mm3 = m3 < 10 ? '0' + m3 : m3;
                        var y3 = $scope.BirthDate3.getFullYear();                
                        var cBD2 = y3 + "-" + mm3 + "-" + dd3;
                      }
                      else {
                        var cBD2 = null 
                      }

                      if ($scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate != null && $scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate != undefined){
                        $scope.MaritalDate4 = new Date($scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate);               
                        var d4 = $scope.MaritalDate4.getDate();
                        var dd4 = d4 < 10 ? '0' + d4 : d4;
                        var m4 = $scope.MaritalDate4.getMonth() + 1;
                        var mm4 = m4 < 10 ? '0' + m4 : m4;
                        var y4 = $scope.MaritalDate1.getFullYear();                
                        var mD2 = y4 + "-" + mm4 + "-" + dd4;
                      }
                      else {
                        var mD2 = null 
                      }

                      $scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate = cBD;
                      $scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate = mD;
                      $scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate = cBD2;
                      $scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate = mD2;
                      console.log("$scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate",$scope.mCustData.acc1[0].CustomerListPersonal[0].BirthDate);
                      console.log("$scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate",$scope.mCustData.acc1[0].CustomerListPersonal[0].MaritalDate);
                      console.log("$scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate",$scope.mCustData.acc1File.CustomerListPersonal[0].BirthDate);
                      console.log("$scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate",$scope.mCustData.acc1File.CustomerListPersonal[0].MaritalDate);

                      

                  }

                  console.log("mCustData utm==>",$scope.mCustData);
                      CustData.update($scope.mCustData).then(function(res){
                            $scope.onShowDetail(detailData,'view');
                            setEditMode(false);

                            bsNotify.show({
                              title: "Customer Data",
                              content: "Data Berhasil di Simpan",
                              type: 'success'
                            });

                            if($scope.backToMain){
                              $scope.formApi.setMode('grid');
                            }      
                        });

                      
              }      
              // CustData.ValidatePhoneEmail(csphone).then(function(res){
              //   console.log("ValidatePhoneEmail==>",res);
              //   if(res.data.Result[0].Valid == null){
                  
              //   }

              // });
            }else //if(csStatus == 2)
            {
              console.log("failed save");
              // CustData.ValidatePhoneEmail(csphone).then(function(res){
              //   console.log("ValidatePhoneEmail==>",res);
              // });
            }
            // else{
            //   CustData.update($scope.mCustData).then(function(res){
            //       $scope.onShowDetail(detailData,'view');
            //       setEditMode(false);
            //       if($scope.backToMain){
            //         $scope.formApi.setMode('grid');
            //       }      
            //   });
            // };
    }
    $scope.CheckModal = function(){
      //console.log("checkSent",$scope.mForgot);
      var xParam = [];
      if($scope.mForgot.flag.flag == 1){
        xParam.push({Phone:$scope.mForgot.valueHP,Email:"-"});
       // console.log("xparam neeh ==>", xParam);
      }else{
        xParam.push({Phone:"-",Email:$scope.mForgot.valueEmail});
       // console.log("xparam neeh ==>", xParam);
      };

      CustData.GetForgotPass(xParam).then(function(res){
                 // console.log("res nih kudu sampe disini jangan undefined wae", res.data.Result);
              if(res.data.Result.length>0){
                bsAlert.alert({
                      title:"Kirim Toyota ID ?",
                      text:"Atas Nama : "+res.data.Result[0].CustomerName, // tambah disini
                      type:"question",
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Ya, Kirim Toyota ID !',
                      cancelButtonText: 'Bukan, Batalkan !',
                      confirmButtonClass: 'btn btn-success',
                      cancelButtonClass: 'btn btn-danger'
                      // buttonsStyling: false
                    }, function () {
                      //console.log("sampai kemarin paman datang");
                        if($scope.mForgot.flag.flag == 1){
                          var arr = [];
                          var nDate = new Date();
                          nDate.setHours(24, 0, 0, 0);
                          //console.log('nextDate', nDate);

                          var xphone = $scope.arrSmsGateway[0] + xParam[0].Phone;
                          var xmessage = $scope.arrSmsGateway[1] + encodeURIComponent('Yth. Pelanggan Toyota : '+res.data.Result[0].CustomerName+' ,Toyota ID Anda = '+res.data.Result[0].ToyotaId);
                          
                            var obj = {};   
                            obj.ParamBody = { 'to': xParam[0].Phone, 'message': 'Yth. Pelanggan Toyota : '+res.data.Result[0].CustomerName+' ,Toyota ID Anda = '+res.data.Result[0].ToyotaId };
                            obj.ExecuteTime = nDate;
                            obj.Phone = xParam[0].Phone;
                            obj.Message = 'Yth. Pelanggan Toyota : '+res.data.Result[0].CustomerName+' ,Toyota ID Anda = '+res.data.Result[0].ToyotaId;
                            obj.Url = xphone + xmessage;
                            // arrhp.push(val.Phone);

                            arr.push(obj);
                            //console.log("before sent",arr);

                            // CustData.SentSms(arr).then(function(res){
                              MrsList.sendSMS(arr).then(function(res){
                                $scope.mForgot.valueHP='';
                                // $scope.mForgot.valueEmail='';
                                $scope.mForgot.show = false;
                                bsAlert.alert({
                                  title:"Berhasil !",
                                  text:"Toyota ID telah dikirim.", // tambah disini
                                  type:"success",
                                  showCancelButton: false,
                                  confirmButtonText: 'OK'
                                })
                            });
                          }else{
                            //anitawulandari2506@gmail.com
                            var arr = [];
                            var obj = {};
                            obj.SendTo = $scope.mForgot.valueEmail;//"agus.st19@gmail.com";
                            obj.SendCc = $scope.mForgot.valueEmail;
                            obj.SendBcc = $scope.mForgot.valueEmail;
                            obj.Subject = "Permintaan Pengiriman Toyota ID";
                            obj.AsHtml = 1;
                            // obj.Message = "Toyota ID ANDA Bapak/Ibu/Saudara/Saudari  <b>"+res.data.Result[0].CustomerName+"</b> adalah <b><i>"+res.data.Result[0].ToyotaId+"</i></b> ,<br><br> TOLONG DIHAFALIN YA !!!!!!";
                            obj.Message = "<P>Kepada Yth. <b>"+res.data.Result[0].CustomerName+"</b> </P><P>Bersama ini Kami kirimkan No. Toyota ID Anda : <b><i>"+res.data.Result[0].ToyotaId+"</i></b> </P> ,<br><br> <P> Salam</P><P><b>TOYOTA</b></P>";
                            
                            //obj.Attachment = "C:\inetpub\ftproot\MailAttachment\fce03cde-6550-4026-8dc0-d9dd6bbe8881.jpg";
                            arr.push(obj);
                            //console.log("data==>",arr);
                            // xParam.push({Phone:"-",Email:$scope.mForgot.valueEmail});
                              CustData.SentEmail(arr).then(function(res){
                                  $scope.mForgot.valueEmail='';
                                  $scope.mForgot.show = false;
                                  bsAlert.alert({
                                    title:"Berhasil !",
                                    text:"Toyota ID telah dikirim.", // tambah disini
                                    type:"success",
                                    showCancelButton: false,
                                    confirmButtonText: 'OK'
                                  })
                              });
                          };
                        
                      
                    }, function (dismiss) {
                      //console.log("sampai kemarin paman datang2");
                      bsAlert.alert({
                        title:"Dibatalkan !",
                        text:"Toyota ID tidak dikirim.", // tambah disini
                        type:"error",
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                      })
                  });
              }else{
                bsAlert.alert({
                        title:"Data Tidak Ditemukan !",
                        text:"Pastikan Nomor Handphone atau Email yang diinput benar.", // tambah disini
                        type:"warning",
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                      });
              }
                  

                    
              // $scope.grid.data = angular.copy(res.data.Result);
              // $scope.loading=false;
              // console.log("======>",$scope.grid.data);
            },
            function(err) {
              console.log("err=>", err);
            }
          );
    }
      // CustData.getGroupData(xParam).then(function(res){
      //         $scope.grid.data = angular.copy(res.data.Result);
      //         $scope.loading=false;
      //         console.log("======>",$scope.grid.data);
      //       },
      //       function(err) {
      //         console.log("err=>", err);
      //       }
      //     );
    // }
    
    var gArray =["branchId","dealerId","name","modelId","typeId","dateStart","dateEnd","servicedateStart","servicedateEnd"];

    //  =========================== Validasi Date Start ==========================================
    $scope.filter = {DateStart:periode, DateEnd:periode,serviceDateStart:periode, serviceDateEnd:periode};
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(),today.getDate());  
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(),today.getDate()); 
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(),today.getDate());                     
    $scope.servicemaxDateS = new Date(today.getFullYear(),today.getMonth(),today.getDate());  
    $scope.servicemaxDateE = new Date(today.getFullYear(),today.getMonth(),today.getDate()); 
    $scope.serviceminDateE = new Date(today.getFullYear(),today.getMonth(),today.getDate());                     

    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);         
       
        $scope.EdateOptions.minDate = $scope.filter.DateStart; 
        $scope.EdateOptions.maxDate = new Date(dayOne.getFullYear()+1,dayOne.getMonth(),dayOne.getDate());
        console.log("$scope.EdateOptions.maxDate",$scope.EdateOptions.maxDate);        
    }

    $scope.changeEndDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        if (dayTwo > today){
            $scope.EdateOptions.minDate = $scope.filter.DateStart;            
            //$scope.filter.DateEnd = today;
        }
    }
    
    $scope.servicechangeStartDate = function(tgl){
      var today = new Date();
      var dayOne = new Date($scope.filter.serviceDateStart);
      var dayTwo = new Date($scope.filter.serviceDateEnd);
       
      $scope.serviceEdateOptions.minDate = $scope.filter.serviceDateStart;
      $scope.serviceEdateOptions.maxDate = new Date(dayOne.getFullYear()+1,dayOne.getMonth(),dayOne.getDate());
      console.log("$scope.serviceEdateOptions",$scope.serviceEdateOptions);
      console.log("$scope.serviceEdateOptions.servicemaxDate",$scope.serviceEdateOptions.servicemaxDate);        
    }

    $scope.servicechangeEndDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.serviceDateStart);
        var dayTwo = new Date($scope.filter.serviceDateEnd);
        console.log(" $scope.filter.serviceDateEnd", $scope.filter.serviceDateEnd);
        console.log(" $scope.serviceEdateOptions.serviceminDate", $scope.serviceEdateOptions.serviceminDate);
        
        if (dayTwo > today){
          $scope.serviceEdateOptions.serviceminDate = $scope.filter.serviceDateStart;            
          //$scope.filter.DateEnd = today;
      }
    }

  //  =========================== Validasi Date End ==========================================

    
    //==========================================================Getdata================================================================
    
   
    $scope.getData = function() {
      resizeLayout();

      // console.log("hereeeee==>",$scope.filterSearch);
      // console.log("get Role",$state.current);
      // console.log("user",$scope.user);
      
      if ($scope.filterSearch.choice) {          

           $scope.groupSearch.dateStart = $scope.filter.DateStart;
           $scope.groupSearch.dateEnd = $scope.filter.DateEnd;
           $scope.groupSearch.servicedateStart = $scope.filter.serviceDateStart;
           $scope.groupSearch.servicedateEnd = $scope.filter.serviceDateEnd;

           var xParam="";
           if ($scope.groupSearch.branchId != undefined && $scope.groupSearch.dealerId != undefined) {
             if ($scope.groupSearch.modelId != undefined || $scope.groupSearch.typeId != undefined){
              if ($scope.groupSearch.servicedateStart == undefined && $scope.groupSearch.servicedateEnd == undefined && $scope.groupSearch.dateStart == undefined && $scope.groupSearch.dateEnd == undefined) {
                  bsAlert.alert({
                    title:"Periode Kosong",
                    text:"harap isi salah satu periode terlebih dahulu",
                    type:"warning"    
                  })
              };
              if ($scope.groupSearch.dateStart != undefined ) {
                  //console.log(formatDate($scope.groupSearch.dateBuyStart));
                  // var c = formatDate($scope.groupSearch.dateStart);
                  // $scope.groupSearch.dateStart = c;
                  // var d = formatDate($scope.groupSearch.dateEnd);
                  // $scope.groupSearch.dateEnd = d;

                    var yearFirst =  $scope.groupSearch.dateStart.getFullYear();
                    var monthFirst =  $scope.groupSearch.dateStart.getMonth() + 1;
                    var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFirst =  $scope.groupSearch.dateStart.getDate();
                    var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                    var firstDate = yearFirst + '-' + monthFirsts + '-' + dayFirsts;                   
                    $scope.groupSearch.dateStart = firstDate;

              };
              if ($scope.groupSearch.dateEnd != undefined) {
                //console.log(formatDate($scope.groupSearch.dateBuyStart));
                // var c = formatDate($scope.groupSearch.dateStart);
                // $scope.groupSearch.dateStart = c;
                // var d = formatDate($scope.groupSearch.dateEnd);
                // $scope.groupSearch.dateEnd = d;

                  var yearFirst2 = $scope.groupSearch.dateEnd.getFullYear();
                  var monthFirst2 = $scope.groupSearch.dateEnd.getMonth() + 1;
                  var monthFirsts2 = monthFirst2 < 10 ? '0' + monthFirst2 : monthFirst2;
                  var dayFirst2 = $scope.groupSearch.dateEnd.getDate();
                  var dayFirsts2 = dayFirst2 < 10 ? '0' + dayFirst2 : dayFirst2;
                  var firstDate2 = yearFirst2 + '-' + monthFirsts2 + '-' + dayFirsts2;                  
                  $scope.groupSearch.dateEnd = firstDate2;

            };
              // if ($scope.groupSearch.dateEnd != undefined) {
              //    // console.log(formatDate($scope.groupSearch.dateBuyEnd));
              //     var c = formatDate($scope.groupSearch.dateEnd);
              //     $scope.groupSearch.dateEnd = c;
              // };
              if ($scope.groupSearch.servicedateStart != undefined ) {
                  //console.log(formatDate($scope.groupSearch.serviceStart));
                  // var c = formatDate($scope.groupSearch.servicedateStart);
                  // $scope.groupSearch.servicedateStart = c;
                  // var d = formatDate($scope.groupSearch.servicedateEnd);
                  // $scope.groupSearch.servicedateEnd = d;

                  var yearFirst =   $scope.groupSearch.servicedateStart.getFullYear();
                  var monthFirst =  $scope.groupSearch.servicedateStart.getMonth() + 1;
                  var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                  var dayFirst =  $scope.groupSearch.servicedateStart.getDate();
                  var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                  var firstDate = yearFirst + '-' + monthFirsts + '-' + dayFirsts;                   
                  $scope.groupSearch.servicedateStart = firstDate;


                 
              };
              if ( $scope.groupSearch.servicedateEnd != undefined) {
                //console.log(formatDate($scope.groupSearch.serviceStart));
                // var c = formatDate($scope.groupSearch.servicedateStart);
                // $scope.groupSearch.servicedateStart = c;
                // var d = formatDate($scope.groupSearch.servicedateEnd);
                // $scope.groupSearch.servicedateEnd = d;

                var yearFirst2 = $scope.groupSearch.servicedateEnd.getFullYear();
                var monthFirst2 = $scope.groupSearch.servicedateEnd.getMonth() + 1;
                var monthFirsts2 = monthFirst2 < 10 ? '0' + monthFirst2 : monthFirst2;
                var dayFirst2 = $scope.groupSearch.servicedateEnd.getDate();
                var dayFirsts2 = dayFirst2 < 10 ? '0' + dayFirst2 : dayFirst2;
                var firstDate2 = yearFirst2 + '-' + monthFirsts2 + '-' + dayFirsts2;                  
                $scope.groupSearch.servicedateEnd = firstDate2;
            };
              // if ($scope.groupSearch.servicedateEnd != undefined) {
              //     //console.log(formatDate($scope.groupSearch.serviceEnd));
              //     var c = formatDate($scope.groupSearch.servicedateEnd);
              //     $scope.groupSearch.servicedateEnd = c;
              // };
              if($scope.groupSearch.name == ""){
                $scope.groupSearch.name = "-";
              }
              // if( $scope.groupSearch.dateStart == undefined){
              //   $scope.groupSearch.dateStart = "-";
              // }
              // if( $scope.groupSearch.dateEnd == undefined){
              //   $scope.groupSearch.dateEnd = "-";
              // }
              // if( $scope.groupSearch.servicedateStart  == undefined){
              //   $scope.groupSearch.servicedateStart  = "-";
              // }
              // if(  $scope.groupSearch.servicedateEnd == undefined){
              //   $scope.groupSearch.servicedateEnd = "-";
              // }
              
             

              xParam = '/'+ $scope.groupSearch.branchId + '/' + $scope.groupSearch.dealerId + '/';  
                        
              for (var i=2; i <  9; i++) {
                if ($scope.groupSearch[gArray[i]] != undefined) {
                    xParam = xParam + $scope.groupSearch[gArray[i]] + '/';  
                }else{
                    xParam = xParam + '-/';  
                };
                
              };
              console.log("pencarian kelommpok",$scope.groupSearch);  
              console.log("xparaaam===>",xParam);
              console.log("$scope.groupSearch.servicedateEnd",$scope.groupSearch.servicedateEnd);
              console.log("$scope.groupSearch.servicedateStart",$scope.groupSearch.servicedateStart);
              console.log("$scope.groupSearch.dateStart",$scope.groupSearch.dateStart);
              console.log("$scope.groupSearch.dateEnd",$scope.groupSearch.dateEnd);
            CustData.getGroupDataAdvanced(xParam).then(function(res){
                $scope.gridGroupSearch = angular.copy(res.data.Result);
                $scope.loading=false;
                console.log("======>",$scope.gridGroupSearch);

                if($scope.gridGroupSearch.length<1){
                    bsAlert.alert({
                      title:"Data Tidak Ditemukan",
                      text:"",
                      type:"warning"
                    })
                };

              },
              function(err) {
                console.log("err=>", err);
              }
            );
          } else{
            bsAlert.alert({
              title:"Type atau Model Kosong",
              text:"harap isi salah satu terlebih dahulu",
              type:"warning"
            // },function(){

            // },function(){

            })
          };
           } else{
            bsAlert.alert({
              title:"Dealer Outlet Kosong",
              text:"harap isi terlebih dahulu",
              type:"warning"
            // },function(){

            // },function(){

            })
          // alert("outlet dealer kosong");
         };

           

      } else{
          console.log("pencarian spesifik",$scope.filterSearch);
          // var cc = $scope.filterSearch.filterValue.trim();
          // $scope.filterSearch.filterValue = cc;
          // console.log("pencarian spesifik",$scope.filterSearch);
          var xParam="";
          //TAM if (true) {
          // xParam = xParam+'-/';
          // } else{
          xParam = xParam + (($scope.filterSearch.TID == undefined || $scope.filterSearch.TID == null || $scope.filterSearch.TID == "") ? '-' : $scope.filterSearch.TID) + '/';
          console.log("a",xParam);
          if ($scope.filterSearch.flag == 6) {
              console.log(formatDate($scope.filterSearch.filterValue));
              var c = formatDate($scope.filterSearch.filterValue);
              $scope.filterSearch.filterValue = c;
          };
          // };
          for (var i = 1; i < 7; i++) {
              if ($scope.filterSearch.flag == i) {
                 if ($scope.filterSearch.filterValue == null ||$scope.filterSearch.filterValue == undefined ||$scope.filterSearch.filterValue == ""){
                  xParam=xParam+'-/';
                  console.log("Masuk1");
                 }   
                 else{
                  xParam=xParam+$scope.filterSearch.filterValue+'/';
                  console.log("Masuk2");
                 } 
              } else{
                xParam=xParam+'-/';
              };
              // console.log("xparam==>",xParam);
            
          };
          console.log("apa orgnya", $scope.user.OrgId); 
          if($scope.user.OrgId == 1000038){
          
          if(($scope.filterSearch.TID == "" || $scope.filterSearch.TID == undefined) && ($scope.filterSearch.filterValue == "" || $scope.filterSearch.filterValue == undefined)){
                    bsAlert.alert({
                      title:"Pencarian Tidak Dapat Dilakukan",
                      text:"Mohon Lengkapi Data Pencarian",
                      type:"warning"
                    });
          }else{
            if (($scope.filterSearch.TID == "" || $scope.filterSearch.TID == undefined) && $scope.user.OrgId == 655) {
                xParam='-'+xParam;
            }         
          
          CustData.getGroupData(xParam).then(function(res){
            console.log("ressss groupSearch===>",res.data.Result);
              $scope.gridGroupSearch = angular.copy(res.data.Result);
              $scope.loading=false;
              // console.log("======>",$scope.grid.data);
                // console.log("length=>",$scope.gridGroupSearch.length);
                  if($scope.gridGroupSearch.length<1){
                      bsAlert.alert({
                        title:"Data Tidak Ditemukan",
                        text:"",
                        type:"warning"
                      })
                  };

                  var outletToyotaId = {outletId:$scope.user.OrgId,toyotaId:''};
                  if (res.data.Result.length > 0) {
                    outletToyotaId = {outletId:$scope.user.OrgId,toyotaId:res.data.Result[0].ToyotaId};
                  }
            
                  console.log("outletToyotaId==>",outletToyotaId);
                  CustData.getMappingOutlet($scope.filterSearch.TID, outletToyotaId).then(function(res){
                    console.log("ToyotaId==>",res);
                        if (res.data.Result.length > 0) {
                            amenu = tAdvanted+tBasic;
                        } else{
                            amenu = tBasic;
                        };
                  })
                // }
              },
              function(err) {
                console.log("err=>", err);
              }
            );

          }
        }
        else {

        if(($scope.filterSearch.TID == "" || $scope.filterSearch.TID == undefined) && ($scope.filterSearch.filterValue == "" || $scope.filterSearch.filterValue == undefined)){
                    bsAlert.alert({
                      title:"Pencarian Tidak Dapat Dilakukan",
                      text:"Mohon Lengkapi Data Pencarian",
                      type:"warning"
                    });
          }else if((($scope.filterSearch.TID == "" || $scope.filterSearch.TID == undefined) || ($scope.filterSearch.filterValue == "" || $scope.filterSearch.filterValue == undefined)) && $scope.user.OrgId != 655){
                    bsAlert.alert({
                      title:"Pencarian Tidak Dapat Dilakukan",
                      text:"Mohon Lengkapi Data Pencarian",
                      type:"warning"
                    });
          }else if((($scope.filterSearch.filterValue == "" || $scope.filterSearch.filterValue == undefined)) && $scope.user.OrgId == 655){
                    bsAlert.alert({
                      title:"Pencarian Tidak Dapat Dilakukan",
                      text:"Mohon Lengkapi Data Pencarian",
                      type:"warning"
                    });
          }else{
            if (($scope.filterSearch.TID == "" || $scope.filterSearch.TID == undefined) && $scope.user.OrgId == 655) {
                xParam='-'+xParam;
            }         
          
          CustData.getGroupData(xParam).then(function(res){
            console.log("ressss groupSearch===>",res.data.Result);
              $scope.gridGroupSearch = angular.copy(res.data.Result);
              $scope.loading=false;
              // console.log("======>",$scope.grid.data);
                // console.log("length=>",$scope.gridGroupSearch.length);
                  if($scope.gridGroupSearch.length<1){
                      bsAlert.alert({
                        title:"Data Tidak Ditemukan",
                        text:"",
                        type:"warning"
                      })
                  };

                  var outletToyotaId = {outletId:$scope.user.OrgId,toyotaId:''};
                  if (res.data.Result.length > 0) {
                    outletToyotaId = {outletId:$scope.user.OrgId,toyotaId:res.data.Result[0].ToyotaId};
                  }
            
                  console.log("outletToyotaId==>",outletToyotaId);
                  CustData.getMappingOutlet($scope.filterSearch.TID, outletToyotaId).then(function(res){
                    console.log("ToyotaId==>",res);
                        if (res.data.Result.length > 0) {
                            amenu = tAdvanted+tBasic;
                        } else{
                            amenu = tBasic;
                        };
                  })
                // }
              },
              function(err) {
                console.log("err=>", err);
              }
            );

          }

        }
      };
      // CustData.getDataInstitusi().then(function(res) {
      // CustData.getGroupData().then(function(res){
      // // CustData.getData().then(function(res) {
      //   // console.log("=====>",res);
      //     // var gridData = res.data.Result;
      //     // gridData = bsTransTree.createTree(gridData, 'StallId', 'StallParentId');
      //     $scope.grid.data = angular.copy(res.data.Result);
      //     $scope.loading=false;
      //     console.log("======>",$scope.grid.data);
      //     // $scope.grid.data = bsTransTree.createUIGridTreeData(gridData);
      //     // console.log("gridata=>",gridData);
      //     // $scope.loading = false;
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // );
      
    }

    $scope.listSelectedRows = [];
    $scope.onListSelectRows = function(rows){
        // console.log("form controller=>",rows);
    }
    $scope.onSelectRows = function(rows) {
        // $scope.dis = rows.length;
        // console.log("test", $scope.dis);
        console.log("onSelectRows=>", rows);
    }


    $scope.personal = {};
    var detailData = {};
    $scope.amenu = {};
    $scope.gridFA=[];
    var FileNonToyota = [];
    var Fileaddress = [];
    var FilePreference = [];
    var FileMedsos = [];
    var FileFamily = [];

    $scope.onShowDetail = function(row,mode){
        $scope.amenu={};
        $scope.show.alldata = true;
        detailData = angular.copy(row);
        console.log("showDetail->",row,mode,$scope.show.alldata);

        $scope.show.tabW = false;
        $scope.tabInst = false;
        for (var i=0; i<$scope.HakAkses.length; i++){
          if ($scope.HakAkses[i].TabName == "Informasi Data Pribadi"){
            if (row.CustomerTypeId == 3 || row.CustomerTypeId == 1){
              $scope.show.tabW = $scope.HakAkses[i].AksesBit;
            } else if (row.CustomerTypeId == 2 || row.CustomerTypeId > 3){
              $scope.tabInst = $scope.HakAkses[i].AksesBit;
            }
          }
          if ($scope.HakAkses[i].TabName == "Informasi Pekerjaan"){
            // $scope.show.tab.tabJob = $scope.HakAkses[i].AksesBit;
            if (row.CustomerTypeId == 3 || row.CustomerTypeId == 1){
              $scope.show.tab.tabJob = $scope.HakAkses[i].AksesBit;
            } else if (row.CustomerTypeId == 2 || row.CustomerTypeId > 3){
              $scope.show.tab.tabJob = false;
            }
          }
          if ($scope.HakAkses[i].TabName == "Informasi Keluarga"){
            // $scope.show.tab.tabFamily = $scope.HakAkses[i].AksesBit;    
            if (row.CustomerTypeId == 3 || row.CustomerTypeId == 1){
              $scope.show.tab.tabFamily = $scope.HakAkses[i].AksesBit;
            } else if (row.CustomerTypeId == 2 || row.CustomerTypeId > 3){
              $scope.show.tab.tabFamily = false;
            }        
          }
          if ($scope.HakAkses[i].TabName == "Informasi Kendaraan"){
            $scope.show.tab.tabVehicle = $scope.HakAkses[i].AksesBit;                        
          }
          if ($scope.HakAkses[i].TabName == "Informasi Preferensi"){
            // $scope.show.tab.tabPreference = $scope.HakAkses[i].AksesBit;  
            if (row.CustomerTypeId == 3 || row.CustomerTypeId == 1){
              $scope.show.tab.tabPreference = $scope.HakAkses[i].AksesBit;
            } else if (row.CustomerTypeId == 2 || row.CustomerTypeId > 3){
              $scope.show.tab.tabPreference = false;
            }                                    
          }
          if ($scope.HakAkses[i].TabName == "Informasi Field Action"){
            $scope.show.tab.tabFA = $scope.HakAkses[i].AksesBit;                                                
          }
          if ($scope.HakAkses[i].TabName == "Informasi Riwayat Perubahan"){
            $scope.show.tab.tabHistory = $scope.HakAkses[i].AksesBit;                                                            
          }
        }

        for (var i=0; i<$scope.HakAkses.length; i++){
          if ($scope.HakAkses[i].TabName == "Informasi Data Pribadi"){
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
          }
          if ($scope.HakAkses[i].TabName == "Informasi Pekerjaan"){
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
          }
          if ($scope.HakAkses[i].TabName == "Informasi Keluarga"){
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
          }
          if ($scope.HakAkses[i].TabName == "Informasi Kendaraan"){
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
          }
          if ($scope.HakAkses[i].TabName == "Informasi Preferensi"){
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
          }
          if ($scope.HakAkses[i].TabName == "Informasi Field Action"){
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
          }
          if ($scope.HakAkses[i].TabName == "Informasi Riwayat Perubahan"){
              $scope.actionButtonSettingsDetail[0].visible = true; //kembali
              // $scope.actionButtonSettingsDetail[1].visible= false;  //simpan
              // $scope.actionButtonSettingsDetail[2].visible= false; // batal
              $scope.actionButtonSettingsDetail[3].visible = $scope.HakAkses[i].EditBit;  // ubah
          }
        }

        // ============================================= comment jadi pakai service aja cek nya ============================ start
        // if ($scope.user.OrgId == 655) {
        //     // if($scope.user.OrgId == 655){
        //         $scope.show.tab.tabBasic = true;
        //         $scope.show.tab.tabJob = true;
        //         $scope.show.tab.tabFamily = true;
        //         $scope.show.tab.tabVehicle = true;
        //         $scope.show.tab.tabPreference = true;
        //         $scope.show.tab.tabFA = true;
        //         $scope.show.tab.tabHistory = true;
        //     // }
        // } else{
        //         console.log("xtab==>",xtab,"amenu==>",amenu);
                
        //         $scope.show.tab.tabBasic = ((xtab&1 && amenu&1) ? true:false);
        //         $scope.show.tab.tabJob = ((xtab&2 && amenu&2) ? true:false);
        //         $scope.show.tab.tabFamily = ((xtab&4 && amenu&4) ? true:false);
        //         $scope.show.tab.tabVehicle = ((xtab&8 && amenu&8) ? true:false);
        //         $scope.show.tab.tabPreference = ((xtab&16 && amenu&16) ? true:false);
        //         //$scope.show.tab.tabFA = ((xtab&32 && amenu&32) ? true:false);
        //         $scope.show.tab.tabHistory = ((xtab&64 && amenu&64) ? true:false);
        //         // if(xtab&1 && amenu&1){$scope.show.tab.tabBasic = true};
        //         // if(xtab&2 && amenu&2){$scope.show.tab.tabJob = true};
        //         // if(xtab&4 && amenu&4){$scope.show.tab.tabFamily = true};
        //         // if(xtab&8 && amenu&8){$scope.show.tab.tabVehicle = true};
        //         // if(xtab&16 && amenu&16){$scope.show.tab.tabPreference = true};
        //         // if(xtab&32 && amenu&32){$scope.show.tab.tabFA = true};
        //         // if(xtab&64 && amenu&64){$scope.show.tab.tabHistory = true};
        // };
        // ============================================= comment jadi pakai service aja cek nya ============================ end


        // if (true) {} else{};

        
        
        if (row.CustomerTypeId == 3 || row.CustomerTypeId == 1) {
            // $scope.detailActionButtonSettings = [{actionType: 'Ubah', //Use 'Edit Action' of bsForm
            //                           title: 'Ubah',icon: 'fa fa-fw fa-edit',color: 'rgb(213,51,55)'},];  
            // $scope.detailActionButtonSettings = [];
            
            CMaster.GetCAddressCategory().then(function(res){
                // console.log("List CategoryAddress====>",res.data.Result);
                var cPull = [];
                var cAddress = res.data.Result;
                for(var i in cAddress){
                    if(cAddress[i].AddressType == 1 || cAddress[i].AddressType == null || cAddress[i].AddressType == undefined){
                      cPull.push(cAddress[i]);
                    }
                }
                $scope.categoryContact = cPull;
            })
            console.log("user==>",$scope.user);
            // var outletRole = {outletId:$scope.user.OrgId,roleId:$scope.user.RoleId};
            // var outletToyotaId = {outletId:$scope.user.OrgId,toyotaId:row.ToyotaId};
            // console.log("outletRole==>",outletRole);
            // console.log("outletToyotaId==>",outletToyotaId);
            // CustData.getMappingOutlet(outletToyotaId).then(function(res){
            //         console.log("ToyotaId==>",res);
            //             if (res.data.Result.length > 0) {
            //                 var amenu = tAdvanted;
            //             } else{
            //                 var amenu = tBasic;
            //             };
            //       })
            // CustData.GetMappingAccesMatrix(outletRole).then(function(res){
            //         console.log("ress===>",res);
            //         if (res.data.Result.length > 0) {
            //                 var cmenu = res.data.Result[0].RightByte;
            //                 if (res.data.Result[0].RightByteEdit > 0) {
            //                     $scope.detailActionButtonSettings = [{actionType: 'Ubah', //Use 'Edit Action' of bsForm
            //                           title: 'Ubah',icon: 'fa fa-fw fa-edit',color: 'rgb(213,51,55)'},];  
            //                 };
                            
            //             } else{
            //                 var cmenu = 0;
            //                 $scope.detailActionButtonSettings = [];
            //             };

            // })
            CustData.getPersonalInfo(row.CustomerOwnerId).then(function(res){

        // ============================================= comment jadi pakai service aja cek nya ============================ start
              // if ($scope.user.OutletId == res.data.Result[0].OutletId){
              //   if (res.data.Result[0].CustomerId != null && res.data.Result[0].CustomerId != undefined && res.data.Result[0].CustomerId != 0){
              //     $scope.show.tab.tabJob = true;
              //     $scope.show.tab.tabFamily = true;
              //     $scope.show.tab.tabVehicle = true;
              //     $scope.show.tab.tabPreference = true;
              //     $scope.show.tab.tabFA = true;
              //   } else {
              //     $scope.show.tab.tabJob = false;
              //     $scope.show.tab.tabFamily = false;
              //     $scope.show.tab.tabVehicle = false;
              //     $scope.show.tab.tabPreference = false;
              //     $scope.show.tab.tabFA = false;
              //   }
              // } else {
              //   $scope.show.tab.tabJob = false;
              //   $scope.show.tab.tabFamily = false;
              //   $scope.show.tab.tabVehicle = false;
              //   $scope.show.tab.tabPreference = false;
              //   $scope.show.tab.tabFA = false;
              // }
        // ============================================= comment jadi pakai service aja cek nya ============================ end


              console.log("personaaal==>",res);
              console.log("CustData==>",$scope.mCustData);
              $scope.mCustData.acc1 = res.data.Result;
              var txt = res.data.Result[0].ToyotaId;
              var txt1 = txt.split("", -1);
              $scope.mCustData.acc1[0].tyId = txt.slice(0,10) +"-"+ txt1.pop();

              // console.log("personal lagi nih ==>", $scope.mCustData.acc1);

              $scope.mCustData.acc1File = angular.copy(res.data.Result[0]);
              // console.log("ini==>",$scope.mCustData.acc1File);
              $scope.mCustData.acc1File.modul = $state.current.data.title;
              CustData.GetListAddress($scope.mCustData.acc1[0].CustomerId).then(function(res){
                    console.log("resss Address==>",res);
                    $scope.gridDataAddress.data = res.data.Result;
                    Fileaddress = angular.copy(res.data.Result);
                    // $scope.loading=false;
                    // console.log("======>",$scope.gridDataAddress.data);
                    },
                    function(err) {
                      console.log("errrrrrrrr=>", err);
                    }
                  )
              
              if (1==1) {
                  CustData.getJobInfo($scope.mCustData.acc1[0].CustomerListPersonal[0].PersonalId).then(function(res){
                    // console.log("Jobs==>",res);
                    $scope.mCustData.acc2 = res.data.Result;
                    $scope.mCustData.acc2File = angular.copy(res.data.Result[0]);
                    $scope.mCustData.acc2File.CustomerId = $scope.mCustData.acc1[0].CustomerId;
                    $scope.mCustData.acc2File.modul = $state.current.data.title;
                    
                  })
                  
              } 
              if(1==1) {
                  CustData.getListPreference($scope.mCustData.acc1[0].CustomerListPersonal[0].PersonalId).then(function(res){
                    // console.log("resss ListPreference==>",res);
                    $scope.gridDataPreferensi.data = res.data.Result;
                    FilePreference = angular.copy(res.data.Result);
                    // $scope.loading=false;
                    // console.log("======>",$scope.gridDataPreferensi.data);
                    },
                    function(err) {
                      console.log("errrrrrrrr=>", err);
                    }
                  );
              }
              if(1==1){
                CustData.getListFamily($scope.mCustData.acc1[0].CustomerId).then(function(res){
                   console.log("resss FAMILYYYY==>",res);
               //     for (var i =0; i < res.data.Result.length; i++) {
               //       res.data.Result[i]
               // };
                  $scope.gridDataFamily.data = res.data.Result;
                  FileFamily = angular.copy(res.data.Result);
                  // $scope.loading=false;
                  // console.log("======>",$scope.gridDataFamily.data);
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
              }
              if(1==1) {
                CustData.getListSocialMedia($scope.mCustData.acc1[0].CustomerListPersonal[0].PersonalId).then(function(res){
                  // console.log("resss ListSocialMedia==>",res);
                  $scope.gridDataMedsos.data = res.data.Result;
                  FileMedsos = angular.copy(res.data.Result);
                  // $scope.loading=false;
                  // console.log("======>",$scope.gridDataMedsos.data);
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
              }
              if(1==1) {
                CustData.GetToyotaVehicle($scope.mCustData.acc1[0].CustomerId).then(function(res){
                  console.log("resss ListToyota==>",res);

                  $scope.gridDataToyotaVehicle.data = res.data.Result;
                  // FileList= angular.copy(res.data.Result);
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
              }
              if(1==1) {
                CustData.GetCListVehicle($scope.mCustData.acc1[0].CustomerId).then(function(res){
                  console.log("resss ListNonToyota==>",res);

                  $scope.gridDataVehicle.data = res.data.Result;
                  FileNonToyota = angular.copy(res.data.Result);
                  // $scope.loading=false;
                  // console.log("======>",$scope.gridDataMedsos.data);
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
              }
              if(1==1) {
                CustData.GetFieldAction($scope.mCustData.acc1[0].CustomerId).then(function(res){
                  console.log("resss ListFieldAction==>",res);
                  $scope.gridFA.data = res.data.Result;              
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
              }
               
              if(1==1) {
                CustData.GetCHistory($scope.mCustData.acc1[0].CustomerId).then(function(res){
                  console.log("resss ListHistory==>",res);

                  $scope.gridDataLogHistory.data = res.data.Result;
                  // FileNonToyota = angular.copy(res.data.Result);
                  // $scope.loading=false;
                  // console.log("======>",$scope.gridDataMedsos.data);
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
              }

              

            })
            $scope.show.tabW=true;
            $scope.tabInst=false;
            $scope.show.alldataIn = true;
            $scope.filterSearch.flag.value="34";
            console.log("menuuuu",$scope.show.tab);
            // $scope.filterSearch.TID = 1;

            // console.log("======personaaal==>",$scope.mCustData);
            // $scope.cari();

            // var d = new Date(row.StartPeriod);
        // ============================================= comment jadi pakai service aja cek nya ============================ start
            // if($scope.user.OrgId == 1000038){
            //   $scope.show.tab.tabBasic = true;
            //   $scope.show.tab.tabJob = true;
            //   $scope.show.tab.tabFamily = true;
            //   $scope.show.tab.tabVehicle = true;
            //   $scope.show.tab.tabPreference = true;
            //   $scope.show.tab.tabHistory = true;
              
            //   if ($scope.gridFA.VIN = null){
            //     $scope.show.tab.tabFA = false;  
            //     console.log("menuuuu");          
            //   }
            //   else {
            //     $scope.show.tab.tabFA = true;               
            //   }
          
            // }
            // else{

            // }
        // ============================================= comment jadi pakai service aja cek nya ============================ end

        //   if ($scope.gridFA.VIN = null){             //if ini mungkin kepake simpen dl aja
            //     $scope.show.tab.tabFA = false;  
            //     console.log("menuuuu");          
            //   }
            //   else {
            //     $scope.show.tab.tabFA = true;               
            //   }
              
        } else if(row.CustomerTypeId == 2 || row.CustomerTypeId > 3){
          console.log("institusiiiii");
          // $scope.detailActionButtonSettings = [];
              // $scope.show.tab.tabBasic = true;
             
        // ============================================= comment jadi pakai service aja cek nya ============================ start              
              // if($scope.user.OrgId == 1000038){
              //   $scope.show.tab.tabBasic = true;
              //   $scope.show.tab.tabJob = false;
              //   $scope.show.tab.tabFamily = false;
              //   $scope.show.tab.tabVehicle = false;
              //   $scope.show.tab.tabPreference = false;
              //   $scope.show.tab.tabFA = false;
              //   $scope.show.tab.tabHistory = true;

              //   xEdit = 127;
              // }else{
              //   // bapuk
              //   $scope.show.tab.tabJob = false;
              //   $scope.show.tab.tabFamily = false;
              //   $scope.show.tab.tabVehicle = false;
              //   $scope.show.tab.tabPreference = false;
              //   $scope.show.tab.tabFA = false;
              // }
        // ============================================= comment jadi pakai service aja cek nya ============================ end

              // $scope.show.tab.tabHistory = false;
            CMaster.GetCAddressCategory().then(function(res){
                // console.log("List CategoryAddress====>",res.data.Result);
                var cPull = [];
                var cAddress = res.data.Result;
                for(var i in cAddress){
                    if(cAddress[i].AddressType == 2 || cAddress[i].AddressType == null || cAddress[i].AddressType == undefined){
                      cPull.push(cAddress[i]);
                    }
                }
                $scope.categoryContact = cPull;
            })
            CustData.getInstitusiInfo(row.CustomerOwnerId).then(function(res){
              console.log("instistusi==>",res);

        // ============================================= comment jadi pakai service aja cek nya ============================ start
              // if ($scope.user.OutletId == res.data.Result[0].OutletId){
              //   if (res.data.Result[0].CustomerId != null && res.data.Result[0].CustomerId != undefined && res.data.Result[0].CustomerId != 0){
              //     $scope.show.tab.tabJob = true;
              //     $scope.show.tab.tabFamily = true;
              //     $scope.show.tab.tabVehicle = true;
              //     $scope.show.tab.tabPreference = true;
              //     $scope.show.tab.tabFA = true;
              //   } else {
              //     $scope.show.tab.tabJob = false;
              //     $scope.show.tab.tabFamily = false;
              //     $scope.show.tab.tabVehicle = false;
              //     $scope.show.tab.tabPreference = false;
              //     $scope.show.tab.tabFA = false;
              //   }
              // } else {
              //   $scope.show.tab.tabJob = false;
              //   $scope.show.tab.tabFamily = false;
              //   $scope.show.tab.tabVehicle = false;
              //   $scope.show.tab.tabPreference = false;
              //   $scope.show.tab.tabFA = false;
              // }
        // ============================================= comment jadi pakai service aja cek nya ============================ end


              // console.log("id ow.CustomerOwnerId",row.CustomerOwnerId);
              $scope.mCustData = res.data.Result[0];
              $scope.mCustData.xFileInstitusi = angular.copy(res.data.Result[0]);
              $state.current.data.title = 'Customer Data';
              $scope.mCustData.modul = ($state.current.data.title + ' - institusi');
              $scope.mCustData.CustomerOwnerId = res.data.Result[0].CustomerId;
              var txt = res.data.Result[0].ToyotaId;
              var txt1 = txt != null ? txt.split("", -1) : "";
              $scope.mCustData.tytId = txt != null ? txt.slice(0,10) +"-"+ txt1.pop() : "";
              // $scope.mCustData[0].xFileInstitusi = [1,2,3,4,5,6];
              console.log("dataaaa institusi=>",$scope.mCustData)

              CustData.GetListAddress($scope.mCustData.CustomerId).then(function(res){
                    console.log("resss Address==>",res);
                    $scope.gridDataAddressInstitusi.data = res.data.Result;
                    Fileaddress = angular.copy(res.data.Result);
                    // $scope.loading=false;
                    console.log("======>",$scope.gridDataAddressInstitusi.data);
                    },
                    function(err) {
                      console.log("errrrrrrrr=>", err);
                    }
                  )

              if(1==1) {
                CustData.GetCHistory($scope.mCustData.CustomerId).then(function(res){
                  console.log("resss ListHistory==>",res);

                  $scope.gridDataLogHistory.data = res.data.Result;
                  // FileNonToyota = angular.copy(res.data.Result);
                  // $scope.loading=false;
                  // console.log("======>",$scope.gridDataMedsos.data);
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
              }

              if(1==1){
                CustData.GetFieldAction($scope.mCustData.CustomerId).then(function(res){
                  console.log("resss ListFieldAction==>",res);

                  $scope.gridFA = res.data.Result;
                  },
                  function(err) {
                    console.log("errrrrrrrr=>", err);
                  }
                );
              }
            })
            


            

              // $scope.acc21 = res.data.acc21;
              // $scope.acc3 = res.data.acc3;
              // $scope.gridDataAddressInstitusi.data = res.data.Result;
              // $scope.gridDataVehicle.data = res.data.ResultVehicle;
              // $scope.gridDataToyotaVehicle.data = res.data.ResultVehicle;
              // $scope.faData = res.data.fieldAction;
              // $scope.sscData = res.data.ssc;
              // $scope.cslData = res.data.csl;
              // $scope.idiData = res.data.idi;
              // $scope.complainData = res.data.complain;
              // $scope.csData = res.data.cs;
              // $scope.gridDataLogHistory.data = res.data.ResultLogHistory;    5271403527

            $scope.show.tabW=false;
            $scope.tabInst=true;
            $scope.show.alldataIn = false;
            // $scope.cariInstitusi();

            // var d = new Date(row.StartPeriod);
            // $scope.alldataIn = true;

            if(row.CustomerTypeId == 4){ //pemerintah
              $scope.hNPWP = false;
              $scope.hSIUP = true;
              $scope.hTDP = true;

            } else if(row.CustomerTypeId == 5){ //yayasan
              $scope.hNPWP = false;
              $scope.hSIUP = false;
              $scope.hSIP = true;
              $scope.hTDP = true;

            } else if(row.CustomerTypeId == 6){ //perusahaan
              $scope.hNPWP = false;
              $scope.hSIUP = false;
              $scope.hSIP = false;
              $scope.hTDP = false;

            }
        };

        
        // $scope.Criteria.prdid = row.GetsudoPeriodId;
        // $scope.Criteria.prdCode = row.GetsudoPeriodCode;
        // $scope.Criteria.prdName = row.GetsudoPeriodName;
        // $scope.Criteria.xYear = d.getFullYear();
        // console.log("criteria==>",$scope.Criteria);
        // $scope.startDateChange(row.StartPeriod);
        $scope.showDetailForm = (mode=='view' ? true:false);
        // $scope.gridDetail.data = [];


    }
    
    $scope.tabClick = function(xc){
      console.log("tab tabClick",xc);
    }
    $scope.onBeforeSaveMaster = function(xdata){
      console.log("lokasiii==>",$state.current.data.title);
      console.log("lokasiii==>",$state.current);

      // console.log("mdata==>",xData);

    }
    $scope.onBeforeNewAddress = function(){
      
      var cC = angular.copy($scope.categoryContact);
      console.log($scope.CustData,"newwww Address",cC);
      // var fd = $scope.CustData
      // console.log("newwww Address",$scope.mCustData.);
      // categoryContact
      // var endCategoryContact = [];
      // $scope.endCategoryContact = [];
      for (var j in $scope.gridDataAddress.data){
          $scope.gridDataAddress.data[j].AddressCategoryId;
          var index1 = _.findIndex(cC, function(o) { return o.AddressCategoryId == cC[j].AddressCategoryId; });
          if (index1 > -1) {cC.splice(index1, 1);};
          // obj.OldValue=((index1 == -1) ? null : data.statusCategory[index1].VehicleCategoryName);
                          
          // arrayOfObjects.splice(i, 1);
          
      }

      // for (var k in cC){
      //   if(cC[k].AddressType == ktype){
      //     endCategoryContact.push(cC[k]);
      //   }
      // }
      // $scope.categoryContact = cC;
      // $scope.endCategoryContact = endCategoryContact;
      console.log("categoryContact",$scope.CategoryContact);

        // for (var i = Things.length - 1; i >= 0; i--) {
        //   Things[i]
        // };
    }
    $scope.onBeforeNewPreference = function(){
        // FilePreference
        // MasterPreference;
        // _.remove(MasterPreference, {
        //     subTopicId: stToDelete
        // });
        console.log("before New Preference");
        console.log("Masuk 0");
        CMaster.GetCMasterPreference().then(function(res){
          // console.log("List Preference====>",res);
          $scope.preferenceData = res.data.Result;
          MasterPreference = angular.copy(res.data.Result);
        })
        $scope.editPref = false;
        $scope.newPref = false;
        $scope.KetPref = false;
        $scope.editPrefa = false;

    }
    $scope.onBeforeNewSocialMedia = function(){
      console.log("Masuk e");

      CMaster.GetCMasterSocialMedia().then(function(res){
        // console.log("List SosialMedia====>",res);
        $scope.sosmedData = res.data.Result;
        MasterSosialMedia = angular.copy(res.data.Result);
      })
      $scope.editSosmed = false;
      $scope.newSosmed = false;
      $scope.KetSosmed = false;
    }
    
    $scope.onBeforeEditPreference = function(row){
      console.log("Masuk 1");
      $scope.editPrefa = false;
      $scope.editPref = true;
      $scope.KetPref = false;
    }
    $scope.onBeforeEditSocialMedia = function(row){
      console.log("Masuk 2");
      $scope.editSosmed = true;
      $scope.newSosmed = false;
      $scope.KetSosmed = false;
    }
    $scope.onBeforeEditAddress = function(row){
      console.log("ediiit address==>",row);
      if (row.MVillage != null && row.MVillage.MDistrict != null && row.MVillage.MDistrict.MCityRegency != null && row.MVillage.MDistrict.MCityRegency.ProvinceId != null) {
         
        CMaster.GetCRegency(row.MVillage.MDistrict.MCityRegency.ProvinceId).then(function(res){
            console.log("resss Regency==>",res);
            $scope.kabupatenData = res.data.Result;
            },
            function(err) {
              console.log("errrrrrrrr=>", err);
            }
          );

          
          if (row.MVillage.MDistrict.CityRegencyId != null){
              CMaster.GetCDistrict(row.MVillage.MDistrict.CityRegencyId).then(function(res){
                console.log("resss District==>",res);
                $scope.kecamatanData = res.data.Result;
                },
                function(err) {
                  console.log("errrrrrrrr=>", err);
                }
              );
          }

          if (row.MVillage.DistrictId != null){
              CMaster.GetCVillage(row.MVillage.DistrictId).then(function(res){
                console.log("resss Village==>",res);
                $scope.kelurahanData = res.data.Result;
                },
                function(err) {
                  console.log("errrrrrrrr=>", err);
                }
              );
          }

      }
      
              
     
              
      
      
        //var cC = angular.copy($scope.categoryContact);
        //console.log("newwww Address",cC,type);
        // console.log("newwww Address",$scope.mCustData.);
        // categoryContact
        // var endCategoryContact = [];
        // // $scope.endCategoryContact = [];
        // for (var j in $scope.gridDataAddress.data){
        //     var idAddress = $scope.gridDataAddress.data[j].AddressCategoryId;;
        //     $scope.gridDataAddress.data[j].AddressCategoryId;
        //     var index1 = _.findIndex(cC, function(o) { return o.AddressCategoryId == cC[j].AddressCategoryId; });
            
        //     if (index1 > -1 && row.AddressCategoryId != idAddress) {cC.splice(index1, 1);};
        //     // obj.OldValue=((index1 == -1) ? null : data.statusCategory[index1].VehicleCategoryName);
                            
        //     // arrayOfObjects.splice(i, 1);
            
        // }

        // for (var k in cC){
        //   if(cC[k].AddressType == ktype){
        //     endCategoryContact.push(cC[k]);
        //   }
        // }
        // // $scope.categoryContact = cC;
        // $scope.endCategoryContact = endCategoryContact;
        // console.log("categoryContact",$scope.endCategoryContact);

      // console.log("ediiit address==>",actListEdit);

      // console.log("ediiit address==>",lmModel);

      // Mlocation
      // $scope.Mlocation = angular.copy($scope.provinsiData);
      // $scope.provinsiData = res.data.Result;
      // console.log("province==>",$scope.provinsiData);
      
      // var indexProv = $scope.provinsiData.findIndex(x => x.ProvinceId==row.MVillage.MDistrict.MCityRegency.ProvinceId);
      // if (indexProv == -1) {
      //     $scope.kabupatenData = [];
      //     $scope.kecamatanData = [];
      //     $scope.kelurahanData = [];
      // } else{
      //     $scope.kabupatenData = $scope.provinsiData[indexProv].MCityRegency;
      //     var indexCity = $scope.provinsiData[indexProv].MCityRegency.findIndex(x => x.CityRegencyId==row.MVillage.MDistrict.CityRegencyId);
      //     if (indexCity == -1) {
      //         $scope.kecamatanData = [];
      //         $scope.kelurahanData = [];
      //     } else{
      //         $scope.kecamatanData = $scope.provinsiData[indexProv].MCityRegency[indexCity].MDistrict;
      //         var indexVillage = $scope.provinsiData[indexProv].MCityRegency[indexCity].MDistrict.findIndex(x => x.DistrictId==row.MVillage.DistrictId);
      //         if (indexVillage == -1) {
      //             $scope.kelurahanData = [];
      //         } else{
      //             $scope.kelurahanData = $scope.provinsiData[indexProv].MCityRegency[indexCity].MDistrict[indexVillage].MVillage;
      //         };
      //     };
      // };
      // $scope.kabupatenData=((indexProv == -1) ? [] : $scope.provinsiData[indexProv].MCityRegency);

      // for (var i = 0; i < $scope.Mlocation.length; i++) {
      //   console.log($scope.Mlocation[i].ProvinceId ,"==", row.MVillage.MDistrict.MCityRegency.ProvinceId);
      //   if($scope.Mlocation[i].ProvinceId == row.MVillage.MDistrict.MCityRegency.ProvinceId){
      //       $scope.kabupatenData = $scope.Mlocation[i].MCityRegency;
      //       for (var j = 0; j < $scope.Mlocation[i].MCityRegency.length ; j++) {
      //             if($scope.Mlocation[i].MCityRegency[j].CityRegencyId == row.MVillage.MDistrict.CityRegencyId){
      //               $scope.kecamatanData = $scope.Mlocation[i].MCityRegency[j].MDistrict;
      //               for (var k =0; k < $scope.Mlocation[i].MCityRegency[j].MDistrict.length; k++) {
      //                 if($scope.Mlocation[i].MCityRegency[j].MDistrict[k].DistrictId == row.MVillage.DistrictId){
      //                   $scope.kelurahanData = $scope.Mlocation[i].MCityRegency[j].MDistrict[k].MVillage;
      //                   break;
      //                 }
      //               };
      //               break;
      //             }
      //           };
      //       break;    
      //   }
      // };
      
      
      


      // $scope.lmItem.IndividuId = 1;
    }
    $scope.onBeforeEditNonToyota = function(row){
      console.log("===>",row);
    }
    $scope.onValidateCancel = function(row,mode){
        console.log("onBeforeCancel");
        // $scope.show.alldata = true;
        $scope.showDetailForm = false;
        // $scope.showDetailPanel = false;
        $scope.getData();

    }
    $scope.onBeforeSaveFamily = function(row){
      // console.log($scope.mCustData.acc1[0].PersonalId,"onBeforeSaveFamily=>",row);
      // row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
      // row.PersonalId = $scope.mCustData.acc1[0].PersonalId;
      // row.modul = $state.current.data.title;

      // console.log("on Before Save Family",row);

      console.log($scope.mCustData.acc1[0].CustomerId,"row==>",row);
      if((row.CustomerDetailFamilyId == undefined) || (row.CustomerDetailFamilyId == null)){
          delete row["child"];
          // formatDate2(row.BirthDate);
          var cDate = formatDate(row.BirthDate);
          row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
          row.PersonalId = $scope.mCustData.acc1[0].CustomerListPersonal[0].PersonalId;
          row.BirthDate = cDate;
          $state.current.data.title = 'Customer Data';
          row.modul = ($state.current.data.title + ' - Family');
        }
      else{
        console.log("update");
        row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
        $state.current.data.title = 'Customer Data';
        row.modul = ($state.current.data.title + ' - Family');
        row.statusFamily = angular.copy($scope.catFamily);
        row.statusGender = [{id:1,name:'Laki - Laki'},{id:2,name:'Perempuan'}];
        // delete row["CreatedDate"];
        var crDate = formatDate(row["CreatedDate"]);
        var cDate = formatDate(row["BirthDate"]);
        var lDate = formatDate(new Date());

        row["CreatedDate"] = crDate;
        row["BirthDate"] = cDate;
        row["LastModifiedDate"] = lDate;
        // delete row["LastModifiedDate"];
        delete row["LastModifiedUserId"];
        // delete row["CreatedUserId"];
        delete row["CustomerFamilyRelation"];
        delete row["CustomerGender"];
        delete row["CustomerListPersonal"];
        delete row["CustomerListIndividu"];

        for (var i = 0; i < FileFamily.length; i++) {
            if(row.CustomerDetailFamilyId == FileFamily[i].CustomerDetailFamilyId){
              row.FileBC = FileFamily[i];
              var fDate = formatDate(row.FileBC["BirthDate"]);
              row.FileBC["BirthDate"] = fDate;
              delete row.FileBC["CreatedDate"];
              delete row.FileBC["LastModifiedDate"];
              delete row.FileBC["LastModifiedUserId"];
              delete row.FileBC["CreatedUserId"];
              delete row.FileBC["CustomerFamilyRelation"];
              delete row.FileBC["CustomerGender"];
              delete row.FileBC["CustomerListPersonal"];
              delete row["CustomerListIndividu"];
            }
        };
      }

    }
    $scope.onBeforeSavePreference = function(row){
      console.log("onBeforeSavePreference=>",row);
      // row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
      // row.PersonalId = $scope.mCustData.acc1[0].PersonalId;
      // row.modul = $state.current.data.title;

      // CustomerDetailPreferenceId
      if((row.CustomerDetailPreferenceId == undefined) || (row.CustomerDetailPreferenceId == null)){
          // delete row["child"];  
          var crDate = formatDate(new Date());
          row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
          row.PersonalId = $scope.mCustData.acc1[0].CustomerListPersonal[0].PersonalId;
          $state.current.data.title = 'Customer Data';
          row.modul = ($state.current.data.title + ' - Preference');
          row["CreatedDate"] = crDate;

        }
      else{
        console.log("update");
        row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
        row.modul = ($state.current.data.title + ' - Preference');
        row.statusPreference = angular.copy($scope.preferenceData);  
        var crDate = formatDate(row["CreatedDate"]);
        var lDate = formatDate(new Date());

        row["CreatedDate"] = crDate;
        row["LastModifiedDate"] = lDate;

        // delete row["CreatedDate"];
        delete row["CustomerPreference"];
        // delete row["LastModifiedDate"];
        delete row["CustomerListPersonal"];
        delete row["CreatedUserId"];
        delete row["LastModifiedUserId"];
        // delete row["CustomerFamilyRelation"];
        // delete row["CustomerGender"];

        for (var i = 0; i < FilePreference.length; i++) {
            if(row.CustomerDetailPreferenceId == FilePreference[i].CustomerDetailPreferenceId){
              row.FileBC = FilePreference[i];
              delete row.FileBC["CreatedDate"];
              delete row.FileBC["CustomerPreference"];
              delete row.FileBC["LastModifiedDate"];
              delete row.FileBC["CustomerListPersonal"];
              delete row.FileBC["CreatedUserId"];
              delete row.FileBC["LastModifiedUserId"];
            }
        };
      }
    }
    $scope.onBeforeSaveSocialMedia = function(row){
      console.log("onBeforeSaveSocialMedia=>",row);
      if((row.CustomerDetailSocialMediaId == undefined) || (row.CustomerDetailSocialMediaId == null)){
          // delete row["child"];
          row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
          row.PersonalId = $scope.mCustData.acc1[0].CustomerListPersonal[0].PersonalId;
          $state.current.data.title = 'Customer Data';
          row.modul = ($state.current.data.title + ' - Preferensi');
        }
      else{
        console.log("update");
        row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
        $state.current.data.title = 'Customer Data';
        row.modul = ($state.current.data.title + ' - Preferensi');
        // delete row["CreatedDate"];
        delete row["CustomerSocialMedia"];
        delete row["LastModifiedDate"];
        delete row["CustomerListPersonal"];
        delete row["CreatedUserId"];
        delete row["LastModifiedUserId"];
        // delete row["CustomerFamilyRelation"];
        // delete row["CustomerGender"];

        for (var i = 0; i < FileMedsos.length; i++) {
            if(row.CustomerDetailSocialMediaId == FileMedsos[i].CustomerDetailSocialMediaId){
              row.FileBC = FileMedsos[i];
              delete row.FileBC["CreatedDate"];
              delete row.FileBC["CustomerSocialMedia"];
              delete row.FileBC["LastModifiedDate"];
              delete row.FileBC["CustomerListPersonal"];
              delete row.FileBC["CreatedUserId"];
              delete row.FileBC["LastModifiedUserId"];
            }
        };
      }
    }
    // $scope.onBeforeSaveAddressInstitusi = function(row){
    //   console.log($scope.mInstitusiData.CustomerId,"row==>",row);
    //   if((row.CustomerAddressId == undefined) || (row.CustomerAddressId == null)){
    //       row.CustomerId = 1;
    //       row.MainAddress = 0;  
    //     }
    //   else{
    //     console.log("update");
    //     delete row["CustomerList"];
    //     delete row["District"];
    //     delete row["PostalCode"];
    //     delete row["Province"];
    //     delete row["Regency"];
    //     delete row["Village"];
    //     delete row["LastModifiedDate"];
    //     delete row["LastModifiedUserId"];
        
        
    //   } 
      

    //   // return true;
    // }
    $scope.onBeforeSaveAddress = function(row){
      console.log($scope.mCustData.acc1[0].CustomerId,"row==>",row);
      if((row.CustomerAddressId == undefined) || (row.CustomerAddressId == null)){
          row.CustomerId = angular.copy($scope.mCustData.acc1[0].CustomerId);
          row.MainAddress = 0;
          $state.current.data.title = 'Customer Data';
          row.modul = ($state.current.data.title + ' - Alamat');  
          delete row["MVillage"];

        }
      else{
        console.log("update");
        $state.current.data.title = 'Customer Data';
        row.modul = ($state.current.data.title + ' - Alamat');
        delete row["CustomerList"];
        delete row["MVillage"];
        delete row["LastModifiedDate"];
        delete row["LastModifiedUserId"];
        delete row["AddressCategory"];
        

        for (var i = 0; i < Fileaddress.length; i++) {
            if(row.CustomerAddressId == Fileaddress[i].CustomerAddressId){
              row.FileBC = Fileaddress[i];
              delete row.FileBC["CreatedDate"];
              delete row.FileBC["LastModifiedDate"];
              delete row.FileBC["LastModifiedUserId"];
              delete row.FileBC["CreatedUserId"];
              delete row.FileBC["MVillage"];
              delete row.FileBC["CustomerList"];
              delete row.FileBC["AddressCategory"];
            }
        };
        
        
      } 
      

      // return true;
    }
    $scope.onBeforeSaveAddressInstitusi = function(row){
      console.log($scope.mCustData.CustomerId,"row==>",row);
      if (row.RT !== null) row.RT = row.RT.trim();
      if (row.RW !== null) row.RW = row.RW.trim();
      if((row.CustomerAddressId == undefined) || (row.CustomerAddressId == null)){
          row.CustomerId = angular.copy($scope.mCustData.CustomerId);
          row.MainAddress = 0;
          $state.current.data.title = 'Customer Data';
          row.modul = ($state.current.data.title + ' - Alamat');  
          delete row["MVillage"];
        }
      else{
        console.log("update");
        $state.current.data.title = 'Customer Data';
        row.modul = ($state.current.data.title + ' - Alamat');
        delete row["CustomerList"];
        delete row["MVillage"];
        delete row["LastModifiedDate"];
        delete row["LastModifiedUserId"];
        delete row["AddressCategory"];

        for (var i = 0; i < Fileaddress.length; i++) {
            if(row.CustomerAddressId == Fileaddress[i].CustomerAddressId){
              row.FileBC = Fileaddress[i];
              delete row.FileBC["CreatedDate"];
              delete row.FileBC["LastModifiedDate"];
              delete row.FileBC["LastModifiedUserId"];
              delete row.FileBC["CreatedUserId"];
              delete row.FileBC["MVillage"];
              delete row.FileBC["CustomerList"];
              delete row.FileBC["AddressCategory"];
            }
        };
        
        
      } 
      

      // return true;
    }
    $scope.onAfterSaveAddressInstitusi = function(row){
      CustData.GetListAddress($scope.mCustData.CustomerId).then(function(res){
        console.log("resss Address==>",res);
        $scope.gridDataAddressInstitusi.data = res.data.Result;
        Fileaddress = angular.copy(res.data.Result);
        // $scope.loading=false;
        console.log("======>",$scope.gridDataAddressInstitusi.data);
        },
        function(err) {
          console.log("errrrrrrrr=>", err);
        }
      )
    }
    $scope.onBeforeSaveNonToyota = function(row,mode){
      console.log("on Before Save Non TOYOTA",row);
      console.log("on Before Save Non TOYOTA",mode);

      console.log($scope.mCustData.acc1[0].CustomerId,"row==>",row);
      if((row.CustomerDetailVehicleId == undefined) || (row.CustomerDetailVehicleId == null)){
          row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
          row.modul = ($state.current.data.title + ' - Kendaraan');
        }
      else{
        console.log("update");
        row.CustomerId = $scope.mCustData.acc1[0].CustomerId;
        row.modul = ($state.current.data.title + ' - Kendaraan');
        row.statusBrand = angular.copy($scope.VehicleBrand);
        row.statusCategory = angular.copy($scope.VehicleCategory);
        var crDate = formatDate(row["CreatedDate"]);
        var lDate = formatDate(new Date());

        row["CreatedDate"] = crDate;
        row["LastModifiedDate"] = lDate;
        // delete row["CreatedDate"];
        // delete row["LastModifiedDate"];

        delete row["LastModifiedUserId"];
        // delete row["CreatedUserId"];
        delete row["MBrand"];
        delete row["PCategoryVehicle"];

        for (var i = 0; i < FileNonToyota.length; i++) {
            if(row.CustomerDetailVehicleId == FileNonToyota[i].CustomerDetailVehicleId){
              row.FileBC = FileNonToyota[i];
              delete row.FileBC["CreatedDate"];
              delete row.FileBC["LastModifiedDate"];
              delete row.FileBC["LastModifiedUserId"];
              delete row.FileBC["CreatedUserId"];
              delete row.FileBC["MBrand"];
              delete row.FileBC["PCategoryVehicle"];
            }
        };
       
        
      }

    }

    $scope.onBeforeDeleteNonToyota = function(row){
      var FD = [];
      console.log("delete Non toyota==>",row);
      for (var i = 0; i < row.length; i++) {
        for (var j = 0; j < FileNonToyota.length; j++) {
            if(row[i] == FileNonToyota[j].CustomerDetailVehicleId){
              var xc = angular.copy(FileNonToyota[j]);
              xc.CustomerId = $scope.mCustData.acc1[0].CustomerId;
              xc.modul = ($state.current.data.title + ' - Kendaraan');
              FD.push(xc);
              
              break;
            }
        };
        
      };
      row.push(FD);
      
    }

    $scope.onBeforeDeleteAddress = function(row){
      var FD = [];
      console.log("delete Address==>",row);
      for (var i = 0; i < row.length; i++) {
        for (var j = 0; j < Fileaddress.length; j++) {
            if(row[i] == Fileaddress[j].CustomerAddressId){
              var xc = angular.copy(Fileaddress[j]);
              xc.CustomerId = $scope.mCustData.acc1[0].CustomerId;
              $state.current.data.title = 'Customer Data';
              xc.modul = ($state.current.data.title + ' - Address');
              FD.push(xc);
              
              break;
            }
        };
        
      };
      row.push(FD);
      
    }

    $scope.onBeforeDeleteAddressInstitusi = function(row){
      var FD = [];
      console.log("delete Address==>",row);
      for (var i = 0; i < row.length; i++) {
        for (var j = 0; j < Fileaddress.length; j++) {
            if(row[i] == Fileaddress[j].CustomerAddressId){
              var xc = angular.copy(Fileaddress[j]);
              xc.CustomerId = $scope.mCustData.CustomerId;
              $state.current.data.title = 'Customer Data';
              xc.modul = ($state.current.data.title + ' - Address');
              FD.push(xc);
              
              break;
            }
        };
        
      };
      row.push(FD);
      
    }

    $scope.onBeforeDeleteFamily = function(row){
      var FD = [];
      console.log("delete Family==>",row);
      for (var i = 0; i < row.length; i++) {
        for (var j = 0; j < FileFamily.length; j++) {
            if(row[i] == FileFamily[j].CustomerDetailFamilyId){
              var xc = angular.copy(FileFamily[j]);
              xc.CustomerId = $scope.mCustData.acc1[0].CustomerId;
              $state.current.data.title = 'Customer Data';
              xc.modul = ($state.current.data.title + ' - Family');
              FD.push(xc);
              
              break;
            }
        };
        
      };
      row.push(FD);
      
    }

    $scope.onBeforeDeletePreference = function(row){
      var FD = [];
      console.log("delete Preference==>",row);
      for (var i = 0; i < row.length; i++) {
        for (var j = 0; j < FilePreference.length; j++) {
            if(row[i] == FilePreference[j].CustomerDetailPreferenceId){
              var xc = angular.copy(FilePreference[j]);
              xc.CustomerId = $scope.mCustData.acc1[0].CustomerId;
              $state.current.data.title = 'Customer Data';
              xc.modul = ($state.current.data.title + ' - Preference');
              FD.push(xc);
              
              break;
            }
        };
        
      };
      row.push(FD);
      
    }

    $scope.onBeforeDeleteSosialMedia = function(row){
      var FD = [];
      console.log("delete Sosial Media==>",row);
      for (var i = 0; i < row.length; i++) {
        for (var j = 0; j < FileMedsos.length; j++) {
            if(row[i] == FileMedsos[j].CustomerDetailSocialMediaId){
              var xc = angular.copy(FileMedsos[j]);
              xc.CustomerId = $scope.mCustData.acc1[0].CustomerId;
              $state.current.data.title = 'Customer Data';
              xc.modul = ($state.current.data.title + ' - Sosial Media');
              FD.push(xc);
              
              break;
            }
        };
        
      };
      row.push(FD);
      
    }
    
    $scope.cariPass = function(){
      console.log("cariPass==>",$scope.mForgot);
      // ($scope.mForgot.name != "") && 
      if (($scope.mForgot.flag.value != "") && ($scope.mForgot.flag != "")) {
        // if ($scope.mForgot.name == "xxx") {
        if ($scope.mForgot.flag.value == "xxx") {
            
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px;text-align:center;">\
                        <div class="row">Peringatan</div>\
                        <div class="row">Tidak ada informasi Toyota Id </div>\
                        <div class="row">yang cocok sesuai informasi yang diberikan</div>\
                        <div class="row"></div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">OK</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CustDataController',
               });
        } else{
            // new
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Informasi Terverifikasi</div>\
                          <div class="row"><div class="col-md-5">  Nama Pelanggan </div><div class="col-md-7"> = <input type="text" ng-disabled="true" value="Bambang Tri"> </div></div>\
                          <div class="row"><div class="col-md-5"> '+$scope.mForgot.flag.desc  +' </div><div class="col-md-7"> = <input type="text"  ng-disabled="true" value="'+  $scope.mForgot.flag.value +'"> </div></div>\
                          <div class="row"> Mohon Di Periksa Ulang</div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="sendRecovery()">Kirim</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CustDataController',
               });
            
        };
      }else{
          var error=[];
                
            if ($scope.mForgot.name != "") {error.push("Nama Pelanggan kosong");};
            if ($scope.mForgot.flag.value != "") {error.push("Pilihan Pemulihan Akun Kosong");};
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );
        }
    }
    $scope.sendRecovery = function(){
      console.log("masuk recovery untuk sms gateway, email gateway");
      $scope.closeThisDialog(0);
    }
    $scope.changePhoneStatus = function(cid){

      console.log(cid,"===>",condPhoneStatus);
      if (cid == 1) {
          if (condPhoneStatus == 1) {
             condPhoneStatus = 0;
          } else if(condPhoneStatus == 2){
            $scope.acc1.phoneStatus2 = 0;
            condPhoneStatus = 1;
          } else if(condPhoneStatus == 0){
            condPhoneStatus = 1;
          };

      } else if(cid == 2){
        if (condPhoneStatus == 1) {
            $scope.acc1.phoneStatus1 = 0;
            condPhoneStatus = 2;
          } else if(condPhoneStatus == 2){
             condPhoneStatus = 0;
          } else if(condPhoneStatus == 0){
            condPhoneStatus = 2;
          };
      }
    }

    // $scope.cariInstitusi = function() {
    //     // console.log("cari=>",$scope.filterSearch);
      
    //         CustData.getDataInstitusi().then(function(res) {
    //           console.log("ressssInstitusi==>",res.data.acc21);

    //           $scope.acc21 = res.data.acc21;
    //           $scope.acc3 = res.data.acc3;
    //           $scope.gridDataAddressInstitusi.data = res.data.Result;
    //           $scope.gridDataVehicle.data = res.data.ResultVehicle;
    //           $scope.gridDataToyotaVehicle.data = res.data.ResultVehicle;
    //           $scope.faData = res.data.fieldAction;
    //           $scope.sscData = res.data.ssc;
    //           $scope.cslData = res.data.csl;
    //           $scope.idiData = res.data.idi;
    //           $scope.complainData = res.data.complain;
    //           $scope.csData = res.data.cs;
    //           $scope.gridDataLogHistory.data = res.data.ResultLogHistory;
    //           // $scope.grid.data = res.data.Result;

    //         }); 
    //         $scope.infoInstitusi = true;
    //         $scope.infoIndividu = false;
    //         $scope.search2 = true;
    //         $scope.alldata = false; 
          
        
    //   }

    $scope.cari = function() {
        console.log("cari=>",$scope.filterSearch);
        // $scope.filterSearch.TID = 

        if (($scope.filterSearch.TID != "") && ($scope.filterSearch.flag.value != "")) {
          

          if ($scope.filterSearch.TID == "xxx") {
              ngDialog.open({ template: '<p style="aligment:center"> Tidak Ditemukan Data Pelanggan Untuk <br> Toyota ID = ' + $scope.filterSearch.TID + ' <br>'+$scope.filterSearch.flag.desc + ' = '+ $scope.filterSearch.flag.value +' <br> Mohon Di Periksa Ulang</p>', plain: true });
          } else{
            CustData.getData().then(function(res) {
              console.log("ressss==>",res.data.acc1);

              // $scope.gridDataAddress.data = res.data.Result;
              $scope.acc1 = res.data.acc1;
              if ($scope.acc1.phoneStatus1 == 1) {
                  condPhoneStatus = 1;
              } else if($scope.acc1.phoneStatus2 == 1) {
                  condPhoneStatus = 2;
              } else{
                  condPhoneStatus = 0;
              };
              $scope.acc2 = res.data.acc2;
              $scope.acc3 = res.data.acc3;
              $scope.listDataCust = res.data.Result;
              // $scope.gridDataFamily.data = res.data.ResultFamily;
              // $scope.gridDataPreferensi.data = res.data.ResultPreferensi;
              // var xmedsos = res.data.ResultMedsos;
              for (i in res.data.ResultMedsos) {
                if(res.data.ResultMedsos[i].xnote == 1){
                  // console.log("ya....");
                  res.data.ResultMedsos[i].flag = "Ya"
                }else{
                  // console.log("tidak....");
                  res.data.ResultMedsos[i].flag = "Tidak"
                }
              }
             // $scope.gridDataMedsos.data = res.data.ResultMedsos;
              // $scope.gridDataVehicle.data = res.data.ResultVehicle;
              $scope.gridDataToyotaVehicle.data = res.data.ResultVehicle;
              console.log(" $scope.gridDataToyotaVehicle.data",  $scope.gridDataToyotaVehicle.data)
              $scope.faData = res.data.fieldAction;
              $scope.sscData = res.data.ssc;
              $scope.cslData = res.data.csl;
              $scope.idiData = res.data.idi;
              $scope.complainData = res.data.complain;
              $scope.csData = res.data.cs;
              $scope.gridDataLogHistory.data = res.data.ResultLogHistory;
              // $scope.grid.data = res.data.Result;


              console.log("add==>",$scope.gridDataAddress.data);

            }); 
            $scope.infoInstitusi = false;
            $scope.infoIndividu = true;
            $scope.search2 = true;
            $scope.alldata = false; 
          };
          


        } 
        else{
          var error=[];
                
            if ($scope.filterSearch.TID != "") {error.push("Toyota ID empty");};
            if ($scope.filterSearch.flag.value != "") {error.push("Filter Choice empty");};
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );  

        };
        
        
      }
    $scope.groupFilter = function(){
        console.log("groupFilter=>");
        $scope.groupfilter = false;
        CustData.getGroupData().then(function(res){
          console.log("ressss group filter==>",res.data.Result);
          $scope.gridDataGroupFilter.data = res.data.Result;
        });
        // $scope.forgotPass = false;
        // $scope.search2 = true;

    }
    // $scope.onListSave = function(np){
    //   if(ne == 1){
    //       // $scope.newPref = true;
    //       console.log("====>",$scope.mPref);

    //     }else if(ne == 2){
    //       // $scope.newSosmed = true;
    //       console.log("====>",$scope.mSosmed);
    //     }
      
    // }
    $scope.forget = function() {
        console.log("forgot=>");
        $scope.mForgot.valueHP='';
        $scope.mForgot.valueEmail='';
        $scope.mForgot.show=true;
        // $scope.forgotPass = false;
        // $scope.search2 = true;
        // $scope.dimmerForgot = true;
      }
    
    $scope.newPreferensi = function(ne){
        if(ne == 1){
          console.log("Masuk 3");
          $scope.editPref = true;
          $scope.editPrefa = true;
          $scope.newPref = true;
          $scope.KetPref = true;
        }else if(ne == 2){
          console.log("Masuk 4");
          $scope.editSosmed = true;
          $scope.newSosmed = true;
          $scope.KetSosmed = true;
        }
    }
    

     
    // $scope.onBeforeSaveMaster = function(){
    //   console.log("Save Master");
    // }  
    $scope.onBeforeCancelPreference = function(){
      console.log("cancel preference");
          $scope.editPref = false;
          $scope.newPref = false;
          $scope.KetPref = false;
    }
    $scope.onBeforeCancelSocialMedia = function(){
      console.log("cancel preference");
          $scope.editSosmed = false;
          $scope.newSosmed = false;
          $scope.KetSosmed = false;
    }
    $scope.onBeforeSave = function(){
      console.log("save preference");
    }
    // $scope.onBeforeSaveFamily = function(){
    //   console.log("on before save family");
    // }
    $scope.onAfterSaveFamily = function(){
      // console.log("on after save family",detailData);
      $scope.onShowDetail(detailData,'view');
    }
    
    $scope.onAfterSaveAddress = function(){     
      bsNotify.show({
        title: "Customer Data",
        content: "Data berhasil di update",
        type: 'success'
      });
      $scope.onShowDetail(detailData,'view');      
    }

    $scope.onAfterSavePreference = function(){
      $scope.onShowDetail(detailData,'view');
      console.log("Masuk 6", res);
      $scope.editPref = false;
      $scope.newPref = false;
      $scope.KetPref = false;
    }

    $scope.onAfterSaveSocialMedia = function(){
      $scope.onShowDetail(detailData,'view');
      console.log("Masuk 5", res);
      $scope.editSosmed = false;
      $scope.newSosmed = false;
      $scope.KetSosmed = false;
    }

    $scope.onAfterSaveNonToyotaVehicle = function(){
      $scope.onShowDetail(detailData,'view');
    }

    $scope.onAfterSaveHistory = function(){
      $scope.onShowDetail(detailData,'view');
    }
    // $scope.user = {
    //   name: 'John Doe',
    //   email: '',
    //   phone: '',
    //   address: 'Mountain View, CA',
    //   donation: 19.99
    // };

    $scope.buyStartDateChange = function(dt){
        console.log("change->","noh =>",dt);
        $scope.buyEndDateOptions.minDate = dt;
    }

    $scope.buyEndDateChange = function(dt){
        console.log("change->","noh =>",dt);
        $scope.buyStartDateOptions.maxDate = dt;
    }



    // ---- accordion 1 Address
    $scope.deleteThisRow = function(del){
      console.log("delete",del,"===>");
      if (del == 1) {
        // console.log("delete address",val);
        $scope.selectedRows = angular.copy($scope.gridDataAddress.selection.getSelectedRows());
        // $scope.selectedRows = angular.copy($scope.selectedAddress);
        console.log("selected=>",$scope.selectedRows);
        console.log("selected=>",$scope.selectedAddress);

        if ($scope.selectedRows.length > 1) {
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda ingin menghapus '+ $scope.selectedRows.length+' alamat yang terpilih ? </div>\
                          <div class="row"> Mohon Di Periksa Ulang</div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(1)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CustDataController',
               });
        } else{
          ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda yakin ingin menghapus alamat ini ?</div>\
                          <div class="row"><div class="col-md-5">  Category Alamat </div><div class="col-md-7"> = <input type="text" ng-disabled="true" value="'+ $scope.selectedRows[0].ContactCategoryName+'"> </div></div>\
                          <div class="row"><div class="col-md-5"> Alamat </div><div class="col-md-7"> = <input type="text"  ng-disabled="true" value="'+  $scope.selectedRows[0].add +'"> </div></div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(1)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CustDataController',
               });
        };

      } else if(del == 2){
        // console.log("delete family",val);
        $scope.selectedRows = angular.copy($scope.gridDataFamily.selection.getSelectedRows());
        console.log("selected=>",$scope.selectedRows);

        if ($scope.selectedRows.length > 1) {
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda ingin menghapus '+ $scope.selectedRows.length+' alamat yang terpilih ? </div>\
                          <div class="row"> Mohon Di Periksa Ulang</div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(2)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CustDataController',
               });
        } else{
          ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda yakin ingin menghapus Informasi Keluarga ini ?</div>\
                          <div class="row"><div class="col-md-5">  Hubungan Keluarga </div><div class="col-md-7"> = <input type="text" ng-disabled="true" value="'+ $scope.selectedRows[0].familyRelationship+'"> </div></div>\
                          <div class="row"><div class="col-md-5"> Nama </div><div class="col-md-7"> = <input type="text"  ng-disabled="true" value="'+  $scope.selectedRows[0].name +'"> </div></div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(2)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CustDataController',
               });
        };
      } else if(del == 3){
        console.log("delete preference",val);
        
      } else if(del == 4){
        console.log("delete sosmed",val);
      } else if(del == 5){
        // console.log("delete vehicle",val);
        $scope.selectedRows = angular.copy($scope.gridDataVehicle.selection.getSelectedRows());
        console.log("selected=>",$scope.selectedRows);

        if ($scope.selectedRows.length > 1) {
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda ingin menghapus '+ $scope.selectedRows.length+' Informasi Kendaraan ? </div>\
                          <div class="row"> Mohon Di Periksa Ulang</div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(5)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CustDataController',
               });
        } else{
          ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda yakin ingin menghapus Informasi Kendaraan ini ?</div>\
                          <div class="row"><div class="col-md-5">  Merk Kendaraan </div><div class="col-md-7"> = <input type="text" ng-disabled="true" value="'+ $scope.selectedRows[0].merk+'"> </div></div>\
                          <div class="row"><div class="col-md-5"> Nomor Polisi </div><div class="col-md-7"> = <input type="text"  ng-disabled="true" value="'+  $scope.selectedRows[0].nopol +'"> </div></div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(5)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CustDataController',
               });
        };
      };
    }

    $scope.deletedItem = function(del){
      console.log("deleted ===>",del);
      $scope.closeThisDialog(0);
    }

    

    $scope.master = function(cd){
      // if (ts == 1) {
          if (cd == 1) {
            $scope.ubah = true;
            $scope.batal = false;
            $scope.simpan = false;  
            $scope.valueChange = false;
          } else if (cd == 2) {
            $scope.ubah = false;
            $scope.batal = true;
            $scope.simpan = true;
            $scope.valueChange = true;  
          } else if (cd == 3){
            $scope.ubah = false;
            $scope.batal = true;
            $scope.simpan = true;  
            $scope.valueChange = true;
          };
             

      
    }
    
    $scope.toyotaIdLink = function(tid){
      console.log("toyotaIdLink==>",tid.typeCustomer);
      
      if (tid.typeCustomer == 1) {
        $scope.filterSearch.TID = angular.copy(tid.typeCustomer);
        $scope.filterSearch.flag.desc="Nomor Rangka";
        $scope.filterSearch.flag.flag=0;
        $scope.filterSearch.flag.key="NoRank";
        $scope.filterSearch.flag.value="www";
        $scope.cari();  

      } else {
        $scope.search2 = true;
        $scope.alldataInstitusi = false;

        CustData.getDataInstitusi().then(function(res) {
            console.log("ressss==>",res.data.Result);
            // $scope.acc1 = res.data.acc1;
            // $scope.acc2 = res.data.acc2;
            $scope.gridDataAddressInstitusi.data = res.data.Result;
            // $scope.gridDataFamily.data = res.data.ResultFamily;
            // $scope.gridDataPreferensi.data = res.data.ResultPreferensi;
            // // var xmedsos = res.data.ResultMedsos;
            // for (i in res.data.ResultMedsos) {
            //   if(res.data.ResultMedsos[i].xnote == 1){
            //     // console.log("ya....");
            //     res.data.ResultMedsos[i].flag = "Ya"
            //   }else{
            //     // console.log("tidak....");
            //     res.data.ResultMedsos[i].flag = "Tidak"
            //   }
            // }
            // $scope.gridDataMedsos.data = res.data.ResultMedsos;
            // $scope.gridDataVehicle.data = res.data.ResultVehicle;
            // $scope.faData = res.data.fieldAction;
            // $scope.sscData = res.data.ssc;
            // $scope.cslData = res.data.csl;
            // $scope.idiData = res.data.idi;
            // $scope.complainData = res.data.complain;
            // $scope.csData = res.data.cs;
            // $scope.gridDataLogHistory.data = res.data.ResultLogHistory;
            // // $scope.grid.data = res.data.Result;

          });
      };
      // console.log("====>",$scope.selectedRows);
      

    }
    $scope.rangkaLink = function(rl){
      console.log("toyotaIdLink==>",rl);
      $scope.cari();
    }

    $scope.detailCSL = function(x){
      console.log("detailll==>",x);


      alert("detaaaailllll==>",x);
    }


    // $scope.selectGridRow = function () {
    //   console.log("selected");
    // if ($scope.selectedItem[0].total != 0) {
    //         $location.path('items/' + $scope.selecteditem[0].id);
    // }};
    // $scope.deleteThisRow = function(ev) {
    // // Appending dialog to document.body to cover sidenav in docs app
    //   var confirm = $mdDialog.confirm()
    //         .title('Would you like to delete your debt?')
    //         .textContent('All of the banks have agreed to forgive you your debts.')
    //         .ariaLabel('Lucky day')
    //         .targetEvent(ev)
    //         .ok('Please do it!')
    //         .cancel('Sounds like a scam');

    //   $mdDialog.show(confirm).then(function() {
    //     // $scope.status = 'You decided to get rid of your debt.';
    //     alert("You decided to get rid of your debt.");
    //   }, function() {
    //     // $scope.status = 'You decided to keep your debt.';
    //     alert("You decided to keep your debt.");
    //   });
    // };
    //----------------------------------
    // Grid Setup
    //----------------------------------

  $scope.gridClickView = function(row){
        console.log("rowEntity",row);
        // alert("gridClickView=>",x);
        
        if (row.typeCustomer == 1) {
            $scope.filterSearch.flag.value="34";
            // $scope.filterSearch.TID = 1;
            $scope.cari();
            // var d = new Date(row.StartPeriod);
            $scope.alldata = false;  
        } else if(row.typeCustomer == 2){
            $scope.cariInstitusi();
            // var d = new Date(row.StartPeriod);
            $scope.alldata = false;
        };

        // $scope.showDetailForm = (mode=='view' ? true:false);
        $scope.showDetailForm = true;
        


  }
    var linkSaveCb = function(mode, model){
        console.log("mode",mode);
        console.log("mode",model);
    }
    var linkBackCb = function(){
      resizeLayout();

    }
    
  $scope.gridClickVehicle = function(x){
      console.log("gridClickEdit=>",x);
      $scope.formApi.showLinkView(linkSaveCb,linkBackCb,'new','true','true');
  }
  $scope.gridActionButtonTemplate =
              '<button class="ui icon inverted grey button"'+
                  'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                  ' onclick="this.blur()" uib-tooltip="View Customer" tooltip-placement="bottom"'+
                  //' ng-if="!grid.appScope.gridHideActionColumn && !row.groupHeader"'+
                  ' ng-click="grid.appScope.actView(row.entity)">'+
                  '<i class="fa fa-fw fa-lg fa-child"></i>'+
              '</button>'+
              '<button class="ui icon inverted grey button"'+
                  ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"'+
                  ' onclick="this.blur()" uib-tooltip="View Vehicle" tooltip-placement="bottom"'+
                  //' ng-if="grid.appScope.allowEdit && !grid.appScope.gridHideActionColumn && !grid.appScope.hideEditButton'+
                  //       ' && !row.groupHeader && !grid.appScope.defHideEditButtonFunc(row)"'+
                  ' ng-click="grid.appScope.$parent.gridClickVehicle(row.entity)">'+
                  '<i class="fa fa-fw fa-lg fa-taxi"></i>'+
              '</button>';
  // $scope.gridDataGroupFilter = {  
  $scope.grid = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: true,
    multiSelect : false,
    enableFullRowSelection: true,
    enableRowHeaderSelection: false,
    enableFiltering:true,
    enableSelectAll : false,
// grid.appScope.
    columnDefs: [
        {name: 'Toyota Id',field: 'ToyotaId'}, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
        {name: 'Kategori',field: 'Category'},
        {name: 'Nama Cabang',field: 'OutletName'},
        {name: 'Nama',field: 'Category'},
        {name: 'Model',field: 'VehicleModelName'},
        {name: 'Nomor Polisi',field: 'LicensePlate'},
        {name: 'Nomor Rangka',field: 'VIN'} //, cellTemplate:'<a ng-click="grid.appScope.rangkaLink(row.entity)">{{row.entity.noRank}}</a>' 
    // };{{row.entity}}
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridApi = gridApi;

        $scope.gridApi.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
              // console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  }

  
  var cAddress = '<div> <i ng-if="row.entity.xstat == 1" class="glyphicon glyphicon-record" style="color:green"></i> </div>';
  $scope.dimmerShow=false;
  $scope.gridDataAddress = {
    enableRowSelection: true,
    enableRowSelectionCheckBox: true,
    multiSelect: true,
    //enableFiltering:true,
    //enableSorting: true,

    columnDefs: [
        { name:'', field: 'xstat', width:'3%', type: 'boolean', cellTemplate: cAddress},//<input type="checkbox" ng-model="row.entity.xstat" disabled>
        // { name:'', fieldset: 'xstat', width:'3%', cellTemplate:'<div class></div>',icon:'glyphicon glyphicon-ok'},
        {name: 'Kategori',field: 'ContactCategoryName'},
        {name: 'Alamat',field: 'Address'},
        {name: 'No Telepon',field: 'Phone'},
        {name: 'RT/RW',field: 'RT'},
        {name: 'Provinsi',field: 'Province.ProvinceName'},
        {name: 'Kota/Kabupaten',field: 'Regency.RegencyName'},
        {name: 'kecamatan',field: 'kecamatanName'},
        {name: 'Kelurahan',field: 'kelurahanName'},
        {name: 'Kode Pos',field: 'KodePos'},
        // {name: 'Hapus', cellTemplate:
        // //      '<div class="grid-action-cell">'+
        // //      '<a ng-click="$event.stopPropagation(); deleteThisRow(row.entity);" href="#">Delete</a></div>'}
        // '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(1,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
        // {name: 'Ubah', cellTemplate:
        // // '<button class="btn btn-info" ng-click="console.log(\'scope=>\',scope.$parent);grid.appScope.EditThisRow(1,row.entity);$event.stopPropagation();"'+
        // //  ' ng-disabled="grid.appScope.valueChange">Ubah</button>'
        //   '<button class="btn btn-info" ng-click="alert(\'scope=>\');$event.stopPropagation();"'+
        //           '>Ubah</button>'
        // },
        {name: 'Ubah', cellTemplate:
          '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(1,row.entity)" >Ubah</button>'
        }
    // }; ng-disabled="grid.appScope.valueChange"
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridDataAddress = gridApi;

        $scope.gridDataAddress.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridDataAddress.selection.getSelectedRows();
              console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridDataAddress.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridDataAddress.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  };

  // $scope.gridDataAddress.onRegisterApi = function(gridApi){
  //     //set gridApi on scope
  //     $scope.gridDataAddress = gridApi;
  //     console.log("cek",$scope.gridApi);
  //     gridApi.selection.on.rowSelectionChanged($scope,function(row){
  //       var msg = 'row selected ' + row;
  //       console.log(msg,$scope.gridDataAddress.selection.getSelectedRows());
  //       console.log("cek",$scope.gridApi);
  //     });

  //     gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
  //       var msg = 'rows changed ' + rows.length;
  //       console.log(msg);
  //     });
  // };
  // $scope.gridDataAddressInstitusi = {
  $scope.gridDataAddressInstitusi = {
    enableRowSelection: true,
    enableRowSelectionCheckBox: true,
    multiSelect: true,
    //enableFiltering:true,
    //enableSorting: true,

    columnDefs: [
        { name:'', field: 'xstat', width:'3%', type: 'boolean', cellTemplate: '<input type="checkbox" ng-model="row.entity.xstat" disabled>'},
        {name: 'Kategori',field: 'ContactCategoryName'},
        {name: 'Alamat',field: 'Address'},
        {name: 'No Telepon',field: 'Phone'},
        {name: 'RT/RW',field: 'RT'},
        {name: 'Provinsi',field: 'provinsiName'},
        {name: 'kecamatan',field: 'kecamatanName'},
        {name: 'Kelurahan',field: 'kelurahanName'},
        {name: 'Kode Pos',field: 'KodePos'},
        // {name: 'Hapus', cellTemplate: 
        // //      '<div class="grid-action-cell">'+
        // //      '<a ng-click="$event.stopPropagation(); deleteThisRow(row.entity);" href="#">Delete</a></div>'}
        // '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(1,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
        {name: 'Ubah', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(1,row.entity)" ng-disabled="grid.appScope.valueChange">Ubah</button>'
      }    
    // };
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridDataAddressInstitusi = gridApi;

        $scope.gridDataAddressInstitusi.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridDataAddressInstitusi.selection.getSelectedRows();
              console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridDataAddressInstitusi.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridDataAddressInstitusi.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  };
  $scope.gridDataFamily = {
    enableRowSelection: true,
    enableRowSelectionCheckBox: true,
    multiSelect: true,
    //enableFiltering:true,
    //enableSorting: true,

    columnDefs: [
      { name:'Hubungan Keluarga', field: 'CustomerFamilyRelation.FamilyRelationName',width:'15%'},// , enableHiding : false 
      { name:'Nama',  field: 'Name'},
      { name:'Jenis Kelamin', field: 'CustomerGender.CustomerGenderName'},
      { name:'Nomor Handphone', field: 'Handphone'},
      { name:'Tempat Lahir', field: 'BirthPlace'},
      { name:'Tanggal Lahir', field: 'BirthDate'},
      { name:'Toyota Id', field: 'IndividuId'},
      // {name: 'Hapus' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(2,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
      {name: 'Ubah' , width: '10%', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(2,gridItem)" ng-disabled="grid.appScope.valueChange2">Ubah</button>'
      }
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridDataFamily = gridApi;

        $scope.gridDataFamily.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridDataFamily.selection.getSelectedRows();
              console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridDataFamily.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridDataFamily.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  };

  $scope.gridDataPreferensi = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: true,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: false,
    // enableFiltering:true,
    // enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Jenis Informasi Preferensi' , width: '40%',  field: 'name'},
      { name:'Keterangan Preferensi' , width: '50%', field: 'desc'},
      // {name: 'Hapus' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(3,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
        {name: 'Ubah' , width: '10%', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(3,gridItem)">Ubah</button>' // ng-disabled="grid.appScope.valueChange"
      }    
      
    ]
  };

  $scope.gridDataMedsos = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: true,
    multiSelect : true,
    // enableFullRowSelection: true,
    // enableRowHeaderSelection: false,
    // enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      // { name:'Sering Digunakan',  field: 'flag', width: '10%',
      // // cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
      // //     if (grid.getCellValue(row,col) === 'Yes') {
      // //       return 'yes';
      // //     }
      // //   }
      // cellTemplate: '<input type=\"checkbox\" value=\"grid.getCellValue(row,col)\" />'
      //   // }
      // },
      { name:'Media Sosial', field: 'name', width: '20%'},
      { name:'Alamat Media Sosial', field: 'desc',width: '30%'},
      { name:'Sering Digunakan', field: 'flag',cellClass: 'yes',width: '40%'},
      // {name: 'Hapus', width: '10%',cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(4,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
        {name: 'Ubah', width: '10%',cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(4,gridItem)">Ubah</button>' // ng-disabled="grid.appScope.valueChange"
      }    
      
    ]
  };

  $scope.gridDataToyotaVehicle = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: false,
    multiSelect : false,
    enableFullRowSelection: true,
    // enableRowHeaderSelection: false,
    // enableFiltering:true,
    // enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Merek' ,   field: 'Merk'},//width: '40%',
      { name:'Jenis' , field: 'Jenis'}, //width: '40%', 
      { name:'Model' , field: 'VehicleModelName'},//width: '40%', 
      { name:'Type' , field: 'Description'},//width: '40%', 
      { name:'Warna' ,  field: 'ColorName'},//width: '40%',
      { name:'Nomor Polisi' , field: 'LicensePlate'},//width: '40%', 
      { name:'Tahun Pembelian' , field: 'TahunPembelian'},//width: '40%', 
      { name:'Harga Beli' , field: 'Harga'},//width: '40%', 
      { name: 'Detail' , width: '10%', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.gridClickVehicle(row.entity)" >Detail</button>'
      } //  //ng-disabled="true"
      // { name: 'Ubah' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(5,row.entity)" >Ubah</button>'//ng-disabled="grid.appScope.valueChange"
      // }
      // ,
      // { name: 'Hapus' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(5,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
          
      
    ]

  };

  $scope.gridDataVehicle = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: false,
    multiSelect : true,
    enableFullRowSelection: true,
    // enableRowHeaderSelection: true,
    // enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Merek' ,   field: 'Merk'},//width: '40%',
      { name:'Jenis' , field: 'VehicleCategoryName'}, //width: '40%', 
      { name:'Model' , field: 'Model'},//width: '40%', 
      { name:'Type' , field: 'Description'},//width: '40%', //SAAT INPUT TIDAK ADA TIPE
      { name:'Warna' ,  field: 'Color'},//width: '40%',
      { name:'Nomor Polisi' , field: 'LicensePlate'},//width: '40%', 
      { name:'Tahun Pembelian' , field: 'YearBuy'},//width: '40%', 
      // { name:'Harga Beli' , field: 'Harga'},//width: '40%', 
      // { name: 'Detail' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-info" ng-click="grid.appScope.DetailThisRowVehicle(row.entity)" ng-disabled="true">Detail</button>'
      // },
      { name: 'Ubah' , width: '10%', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(5,row.entity)" >Ubah</button>'//ng-disabled="grid.appScope.valueChange"
      }
      // ,
      // { name: 'Hapus' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(5,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
          
      
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridDataVehicle = gridApi;

        $scope.gridDataVehicle.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridDataVehicle.selection.getSelectedRows();
              console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridDataVehicle.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridDataVehicle.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  };

  $scope.gridDataLogHistory = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: true,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: false,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Tipe' ,   field: 'type'},//width: '40%',
      { name:'Modul' , field: 'modul'}, //width: '40%', 
      { name:'Menu' , field: 'menu'},//width: '40%', 
      { name:'Atribut' , field: 'atribut'},//width: '40%', 
      { name:'New Value' ,  field: 'newVal'},//width: '40%',
      { name:'Old Value' , field: 'oldVal'},//width: '40%', 
      { name:'Username' , field: 'userName'},//width: '40%', 
      { name:'Kode Outlet' , field: 'outletKode'},//width: '40%', 
      { name:'Update DateTime' , field: 'update'}//width: '40%', 
          
      
    ]
  };


  
  });

