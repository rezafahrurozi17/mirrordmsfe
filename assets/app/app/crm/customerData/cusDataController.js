angular.module('app')
  .controller('CusDataController', function($scope, $http, CurrentUser, bsTransTree, CusData, bsNotify, ngDialog, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = false;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.acc1 = {};
    $scope.acc2 = {};
    $scope.selectedRows = [];
    $scope.search2 = false;
    $scope.groupfilter = true;
    $scope.forgotPass = true;
    $scope.alldata = true;
    $scope.alldataInstitusi = true;
    $scope.address = true;
    $scope.afamily = true;
    $scope.aInfoPreferensi = true;
    $scope.aSosMed = true;
    $scope.vehicle = true;
    $scope.infoInstitusi = false;
    $scope.infoIndividu = false;
    // $scope.condPhoneStatus1 = true;
    // $scope.condPhoneStatus2 = true;

    $scope.mData = {}; //Model
    $scope.xRole = {
      selected: []
    };
    $scope.gridOptions1 = {};
    $scope.gridOptions2 = {};
    $scope.grid33 = {};

    //---data scope
    $scope.mAddress={};
    $scope.mPreferensi={};
    $scope.newPref=false;
    $scope.newSosmed=false;
    $scope.mSosmed={};
    $scope.mVehicle={};

    $scope.ubah = false;
    $scope.batal = true;
    $scope.simpan = true;
    $scope.valueChange = true;
    // $scope.ubah2 = false;
    // $scope.batal2 = true;
    // $scope.simpan2 = true;
    // $scope.valueChange2 = true;

    

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var condPhoneStatus=0;
    $scope.dateOptions = {
      startingDay: 1,
      format: dateFormat,
      disableWeekend: 1
    };
    $scope.filterSearh = {flag:"",TID:""};
    $scope.filterValue = [{key: "Nopol",desc: "Nomor Polisi",flag:0,value:""},{key: "NoRank",desc: "Nomor Rangka",flag:0,value:""},{key: "NoMesin",desc: "Nomor Mesin",flag:0,value:""},{key: "TTL",desc: "Tanggal Lahir",flag:0,value:""},{key: "Phone",desc: "Nomor Handphone",flag:0,value:""},{key: "NoKTP",desc: "Nomor KTP",flag:0,value:""}];
    $scope.mForgot = {flag:"",name:""};
    $scope.filterForgot = [{key: "phone",desc: "Nomor Handphone",flag:0,value:""},{key: "Email",desc: "Alamat Email",flag:0,value:""}];
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.gridDataTree=[];

    // ------- dummy data ----
    // $scope.cars = ['Audi', 'BMW', 'Merc'];
    // $scope.bikes = ['Yamaha', 'Honda', 'Enfield'];
    //[{id:1,name:"Lancer"},{id:2,name:"Skyline"}];
    //{id:1,name:"Bus"},{id:2,name:"Truk"};
    $scope.merkVehicle = [{id:1,name:"Mitshubishi"},{id:2,name:"Honda"}];
    $scope.peran = [{id:1,name:"Pemilik"},{id:2,name:"Pemakai"}];
    $scope.agamaData  = [{id:1,name:"Islam"},{id:2,name:"Kristen"},{id:2,name:"Buddha"}];
    $scope.sukuData  = [{id:1,name:"Jawa"},{id:2,name:"Sunda"}];
    $scope.bahasaData  = [{id:1,name:"Indonesia"},{id:2,name:"China"}];
    $scope.fleetData  = [{id:1,name:"Fleet"},{id:2,name:"Non Fleet"}];
    $scope.positionData = [{id:1,name:"Direktur"},{id:2,name:"Manager"},{id:3,name:"Supervisor"},{id:2,name:"Office Boy"}];
    $scope.categoryData  = [{id:1,name:"Pengguna"},{id:2,name:"Pemakai"}];
    $scope.categoryContact = [{id:1,name:"Rumah"},{id:2,name:"Kantor"}];
    $scope.marriedStatus = [{id:1,name:"Kawin"},{id:2,name:"Belum Kawin"},,{id:3,name:"Cerai"}];
    $scope.pendapatanData = [{id:1,name:"1 Milyar - 15 Milyar"},{id:2,name:"16 Milyar - 25 Milyar"}];
    $scope.provinsiData = [{id:1,name:"Papua Barat"},{id:2,name:"Jakarta"},{id:3,name:"Jawa Tengah"}];
    $scope.kabupatenData = [{id:1,name:"Ujung Timur"},{id:2,name:"Ujung Tengah"},{id:3,name:"Ujung Tenggara"}];
    $scope.kecamatanData = [{id:1,name:"Desa Timur"},{id:2,name:"Desa Tengah"},{id:3,name:"Desa Tenggara"}];
    $scope.kelurahanData = [{id:1,name:"Haji"},{id:2,name:"Korakal"},{id:3,name:"Lembayung"}];
    $scope.catFamily = [{id:1,name:"Suami"},{id:2,name:"Istri"},{id:3,name:"Ayah"},{id:4,name:"Ibu"},{id:5,name:"Anak"}];
    $scope.informasiPreferensi = [{id:1,name:"Makanan Favorit"},{id:2,name:"Minuman Favorit"},{id:3,name:"Hobby"},{id:4,name:"Musik Favorit"},{id:5,name:"Olahraga Favorit"}];
    $scope.sosmed = [{id:1,name:"Facebook"},{id:2,name:"Twitter"},{id:3,name:"Youtube"}];
    $scope.merkVehicle = [{id:1,name:"Toyota"},{id:2,name:"Mazda"},{id:3,name:"Mitshubishi"}];
    $scope.categoryVehicle = [{id:1,name:"Mobil"},{id:2,name:"Truk"},{id:3,name:"Bus"}];
    $scope.modelVehicle = [{id:1,name:"Avanza"},{id:2,name:"Fortuner"},{id:3,name:"Cayla"}];
    $scope.typeVehicle = [{id:1,name:"All New 1.3 G A/T"},{id:2,name:"All New 1.5 G A/T"},{id:3,name:"All New 1.3 G M/T"}];
    $scope.colourVehicle = [{id:1,name:"Merah"},{id:2,name:"Transparan"},{id:3,name:"Hijau"}];
    
    
    $scope.getData = function() {
      console.log("hereeeee==>");
      // CusData.getDataInstitusi().then(function(res) {
      CusData.getGroupData().then(function(res){
      // CusData.getData().then(function(res) {
        // console.log("=====>",res);
          // var gridData = res.data.Result;
          // gridData = bsTransTree.createTree(gridData, 'StallId', 'StallParentId');
          $scope.grid.data = angular.copy(res.data.Result);
          $scope.loading=false;
          console.log("======>",$scope.grid.data);
          // $scope.grid.data = bsTransTree.createUIGridTreeData(gridData);
          // console.log("gridata=>",gridData);
          // $scope.loading = false;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      
    }

    $scope.onSelectRows = function(rows) {
        console.log("onSelectRows=>", rows);
        // $scope.selectedRows = rows;
      }
    // function sendReco(){
    //   console.log("test send reco");
    // }
    $scope.onShowDetail = function(row,mode){
        console.log("showDetail->",row,mode);
        if (row.typeCustomer == 1) {
            $scope.filterSearh.flag.value="34";
            $scope.filterSearh.TID = 1;
            $scope.cari();
            // var d = new Date(row.StartPeriod);
            $scope.alldata = false;  
        } else if(row.typeCustomer == 2){
            $scope.cariInstitusi();
            // var d = new Date(row.StartPeriod);
            $scope.alldata = false;
        };

        
        // $scope.Criteria.prdid = row.GetsudoPeriodId;
        // $scope.Criteria.prdCode = row.GetsudoPeriodCode;
        // $scope.Criteria.prdName = row.GetsudoPeriodName;
        // $scope.Criteria.xYear = d.getFullYear();
        // console.log("criteria==>",$scope.Criteria);
        // $scope.startDateChange(row.StartPeriod);
        $scope.showDetailForm = (mode=='view' ? true:false);
        // $scope.gridDetail.data = [];
    }
    $scope.onValidateCancel = function(row,mode){
        console.log("onBeforeCancel");
        $scope.alldata = true;
        $scope.showDetailForm = false;
        // $scope.showDetailPanel = false;
        $scope.getData();

    }
    // $scope.doCancel = function(row,mode){
    //     console.log("onCancel");
    //     $scope.alldata = true;
    //     $scope.showDetailForm = false;
    //     // $scope.showDetailPanel = false;
    //     $scope.getData();

    // }
    $scope.cariPass = function(){
      console.log("cariPass==>",$scope.mForgot);
      // ($scope.mForgot.name != "") && 
      if (($scope.mForgot.flag.value != "") && ($scope.mForgot.flag != "")) {
        // if ($scope.mForgot.name == "xxx") {
        if ($scope.mForgot.flag.value == "xxx") {
            
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px;text-align:center;">\
                        <div class="row">Peringatan</div>\
                        <div class="row">Tidak ada informasi Toyota Id </div>\
                        <div class="row">yang cocok sesuai informasi yang diberikan</div>\
                        <div class="row"></div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">OK</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CusDataController',
               });
        } else{
            // new
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Informasi Terverifikasi</div>\
                          <div class="row"><div class="col-md-5">  Nama Pelanggan </div><div class="col-md-7"> = <input type="text" ng-disabled="true" value="Bambang Tri"> </div></div>\
                          <div class="row"><div class="col-md-5"> '+$scope.mForgot.flag.desc  +' </div><div class="col-md-7"> = <input type="text"  ng-disabled="true" value="'+  $scope.mForgot.flag.value +'"> </div></div>\
                          <div class="row"> Mohon Di Periksa Ulang</div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="sendRecovery()">Kirim</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CusDataController',
               });
            // ngDialog.openConfirm ({
            //   template:'<p> Informasi Terverifikasi <br><br>Nama Pelanggan<br> <input type="text" disabled="true" value=" xxxxxx "> <br><br>Informasi Toyota Id akan dikirimkan melalui kontak berikut : <br> xxxx <br>  <input type="text" disabled="true" value=" xxxx"></p>\
            //          <div class="ngdialog-buttons">\
                       
            //            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="sendRecovery()">Yes</button>\
            //            </div>',
            //              plain: true,
            //              controller: 'CusDataController',
            //    });
        };
      }else{
          var error=[];
                
            if ($scope.mForgot.name != "") {error.push("Nama Pelanggan kosong");};
            if ($scope.mForgot.flag.value != "") {error.push("Pilihan Pemulihan Akun Kosong");};
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );
        }
    }
    $scope.sendRecovery = function(){
      console.log("masuk recovery untuk sms gateway, email gateway");
      $scope.closeThisDialog(0);
    }
    $scope.changePhoneStatus = function(cid){

      console.log(cid,"===>",condPhoneStatus);
      if (cid == 1) {
          if (condPhoneStatus == 1) {
             condPhoneStatus = 0;
          } else if(condPhoneStatus == 2){
            $scope.acc1.phoneStatus2 = 0;
            condPhoneStatus = 1;
          } else if(condPhoneStatus == 0){
            condPhoneStatus = 1;
          };

      } else if(cid == 2){
        if (condPhoneStatus == 1) {
            $scope.acc1.phoneStatus1 = 0;
            condPhoneStatus = 2;
          } else if(condPhoneStatus == 2){
             condPhoneStatus = 0;
          } else if(condPhoneStatus == 0){
            condPhoneStatus = 2;
          };
      }
    }

    $scope.cariInstitusi = function() {
        // console.log("cari=>",$scope.filterSearh);
      
            CusData.getDataInstitusi().then(function(res) {
              console.log("ressssInstitusi==>",res.data.acc21);

              $scope.acc21 = res.data.acc21;
              $scope.acc3 = res.data.acc3;
              $scope.gridDataAddressInstitusi.data = res.data.Result;
              $scope.gridDataVehicle.data = res.data.ResultVehicle;
              $scope.gridDataToyotaVehicle.data = res.data.ResultVehicle;
              $scope.faData = res.data.fieldAction;
              $scope.sscData = res.data.ssc;
              $scope.cslData = res.data.csl;
              $scope.idiData = res.data.idi;
              $scope.complainData = res.data.complain;
              $scope.csData = res.data.cs;
              $scope.gridDataLogHistory.data = res.data.ResultLogHistory;
              // $scope.grid.data = res.data.Result;

            }); 
            $scope.infoInstitusi = true;
            $scope.infoIndividu = false;
            $scope.search2 = true;
            $scope.alldata = false; 
          
        
      }

    $scope.cari = function() {
        console.log("cari=>",$scope.filterSearh);
        // $scope.filterSearh.TID = 

        if (($scope.filterSearh.TID != "") && ($scope.filterSearh.flag.value != "")) {
          

          if ($scope.filterSearh.TID == "xxx") {
              ngDialog.open({ template: '<p style="aligment:center"> Tidak Ditemukan Data Pelanggan Untuk <br> Toyota ID = ' + $scope.filterSearh.TID + ' <br>'+$scope.filterSearh.flag.desc + ' = '+ $scope.filterSearh.flag.value +' <br> Mohon Di Periksa Ulang</p>', plain: true });
          } else{
            CusData.getData().then(function(res) {
              console.log("ressss==>",res.data.acc1);

              $scope.acc1 = res.data.acc1;
              if ($scope.acc1.phoneStatus1 == 1) {
                  condPhoneStatus = 1;
              } else if($scope.acc1.phoneStatus2 == 1) {
                  condPhoneStatus = 2;
              } else{
                  condPhoneStatus = 0;
              };
              $scope.acc2 = res.data.acc2;
              $scope.acc3 = res.data.acc3;
              $scope.gridDataAddress.data = res.data.Result;
              $scope.gridDataFamily.data = res.data.ResultFamily;
              $scope.gridDataPreferensi.data = res.data.ResultPreferensi;
              // var xmedsos = res.data.ResultMedsos;
              for (i in res.data.ResultMedsos) {
                if(res.data.ResultMedsos[i].xnote == 1){
                  // console.log("ya....");
                  res.data.ResultMedsos[i].flag = "Ya"
                }else{
                  // console.log("tidak....");
                  res.data.ResultMedsos[i].flag = "Tidak"
                }
              }
              $scope.gridDataMedsos.data = res.data.ResultMedsos;
              $scope.gridDataVehicle.data = res.data.ResultVehicle;
              $scope.gridDataToyotaVehicle.data = res.data.ResultVehicle;
              $scope.faData = res.data.fieldAction;
              $scope.sscData = res.data.ssc;
              $scope.cslData = res.data.csl;
              $scope.idiData = res.data.idi;
              $scope.complainData = res.data.complain;
              $scope.csData = res.data.cs;
              $scope.gridDataLogHistory.data = res.data.ResultLogHistory;
              // $scope.grid.data = res.data.Result;

            }); 
            $scope.infoInstitusi = false;
            $scope.infoIndividu = true;
            $scope.search2 = true;
            $scope.alldata = false; 
          };
          


        } 
        else{
          var error=[];
                
            if ($scope.filterSearh.TID != "") {error.push("Toyota ID empty");};
            if ($scope.filterSearh.flag.value != "") {error.push("Filter Choice empty");};
            bsNotify.show(
                {
                    size: 'big',
                    type: 'danger',
                    title: "Ditemukan beberapa data yang salah",
                    content: error.join('<br>'),
                    number: error.length
                }
            );  

        };
        
        
      }
    $scope.groupFilter = function(){
        console.log("groupFilter=>");
        $scope.groupfilter = false;
        CusData.getGroupData().then(function(res){
          console.log("ressss group filter==>",res.data.Result);
          $scope.gridDataGroupFilter.data = res.data.Result;
        });
        // $scope.forgotPass = false;
        // $scope.search2 = true;

    }
    $scope.forget = function() {
        console.log("forgot=>");
        $scope.forgotPass = false;
        $scope.search2 = true;
      }
    
    $scope.newPreferensi = function(ne){
        if(ne == 1){
          $scope.newPref = true;
        }else if(ne == 2){
          $scope.newSosmed = true;
        }
    }
    $scope.Add = function(ad) {
        if (ad == 1) {
          // address
          console.log("Add Address Button=>");
          
          $scope.mAddress={};
          $scope.address=false;
          $scope.alldata=true;

        } else if(ad == 2){
          // add family
          console.log("Add Family Button=>");
          
          $scope.mFamily={};
          $scope.afamily=false;
          $scope.alldata=true;

        } else if(ad == 3){
          // add family
          console.log("Add Preference Button=>");
          
          $scope.mPreferensi={};
          $scope.aInfoPreferensi=false;
          $scope.alldata=true;

        } else if(ad == 4){
          // add family
          console.log("Add Sosmed Button=>");
          
          $scope.mSosmed={};
          $scope.aSosMed=false;
          $scope.alldata=true;

        } else if(ad == 5){
          // add family
          console.log("Add Vehicle Button=>");
          
          $scope.mVehicle={};
          $scope.vehicle=false;
          $scope.alldata=true;

        } else if (ad == 21) {
          // address
          console.log("Add Address Institusi Button=>");
          
          $scope.mAddress={};
          $scope.address=false;
          $scope.alldata=true;

        };
        

        // $scope.forgotPass = false;
        // $scope.search2 = true;
      }

    
    $scope.cancelPop = function(cp) {
        if (cp == 0) {
          // address
          console.log("Cancel forgot Password=>");

          $scope.forgotPass = true;
          $scope.search2 = false;

        } else if (cp == 1) {
          // address
          console.log("Cancel Address Button=>");

          $scope.mAddress={};
          $scope.address=true;
          $scope.alldata=false;

        } else if(cp == 2){
          // family
          console.log("Cancel Family Button=>");

          $scope.mFamily={};
          $scope.afamily=true;
          $scope.alldata=false;
        } else if(cp == 3){
          // preference
          // $scope.preferensiForm.$pristine=true;
          console.log("Cancel Preferen Button=>");

          $scope.mPreferensi={};
          $scope.aInfoPreferensi=true;
          $scope.alldata=false;
          $scope.newPref = false;
        } else if(cp == 4){
          // preference
          console.log("Cancel Sosmed Button=>");

          $scope.mSosmed={};
          $scope.aSosMed=true;
          $scope.alldata=false;
          $scope.newSosmed=false;
        } else if(cp == 5){
          // preference
          console.log("Cancel vehicle Button=>");

          $scope.mVehicle={};
          $scope.vehicle=true;
          $scope.alldata=false;
        };
        

        // $scope.forgotPass = false;
        // $scope.search2 = true;
      }
  $scope.savePop = function(sp) {
        // console.log("====>",$scope.preferensiForm);
        // $scope.preferensiForm.$validate();


        
        if (sp == 1) {
          // address
          console.log("Save Address Button=>",$scope.mAddress);
          // if ($scope.mAddress.contact == undefined || ) {
              // var error=[];
              // if ($scope.mAddress.contact == undefined || $scope.mAddress.contact == '') {error.push("Contact Category kosong");};
              // if ($scope.mAddress.phone == undefined || $scope.mAddress.phone == '') {error.push("Nomor Handphone kosong");};
              // if ($scope.mAddress.add == undefined || $scope.mAddress.add == '') {error.push("Alamat kosong");};
              // if ($scope.mAddress.rt == undefined || $scope.mAddress.rt == '') {error.push("Rt/Rw kosong");};
              // if ($scope.mAddress.provinsi == undefined || $scope.mAddress.provinsi == '') {error.push("Provinsi kosong");};
              // if ($scope.mAddress.kabupaten == undefined || $scope.mAddress.kabupaten == '') {error.push("Kabupaten kosong");};
              // if ($scope.mAddress.kecamatan == undefined || $scope.mAddress.kecamatan == '') {error.push("Kecamatan kosong");};
              // if ($scope.mAddress.kelurahan == undefined || $scope.mAddress.kelurahan == '') {error.push("Kelurahan kosong");};
              // if ($scope.mAddress.KodePos == undefined || $scope.mAddress.KodePos == '') {error.push("Kode Pos kosong");};
              //   if (error.length > 0) {
              //     bsNotify.show(
              //         {
              //             size: 'big',
              //             type: 'danger',
              //             title: "Ditemukan beberapa data yang salah",
              //             content: error.join('<br>'),
              //             number: error.length
              //         }
              //       );   
              //   } else{

                    ngDialog.openConfirm ({
                      template:'<fieldset style="border:3px solid #e2e2e2;padding:20px;text-align:center;">\
                                <div class="row">Data Sudah Tersimpan </div>\
                                <div class="row"></div>\
                             <div class="ngdialog-buttons">\
                               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">OK</button>\
                              </div></fieldset>'
                               ,
                                 plain: true,
                                 controller: 'CusDataController',
                       });

                    $scope.address=true;
                    $scope.alldata=false;        

                    $scope.mAddress = {};
                // };
                
          


        } else if(sp == 2){
          console.log("Save Family Button=>",$scope.mFamily);
            // var error=[];
            // if ($scope.mFamily.CategoryFamily == undefined || $scope.mFamily.CategoryFamily == '') {error.push("Category Family kosong");};
            // if ($scope.mFamily.name == undefined || $scope.mFamily.name == '') {error.push("Nama kosong");};
            // if ($scope.mFamily.gender == undefined || $scope.mFamily.gender == '') {error.push("Jenis Kelamin kosong");};
            // if ($scope.mFamily.phone == undefined || $scope.mFamily.phone == '') {error.push("Nomor Telephone kosong");};
            // if ($scope.mFamily.placeBirth == undefined || $scope.mFamily.placeBirth == '') {error.push("Tempat lahir kosong");};
            // if ($scope.mFamily.dateBirth == undefined || $scope.mFamily.dateBirth == '') {error.push("Tanggal Lahir kosong");};
            //   if (error.length > 0) {
            //       bsNotify.show(
            //           {
            //               size: 'big',
            //               type: 'danger',
            //               title: "Ditemukan beberapa data yang salah",
            //               content: error.join('<br>'),
            //               number: error.length
            //           }
            //         );   
            //     } else{

                    ngDialog.openConfirm ({
                      template:'<fieldset style="border:3px solid #e2e2e2;padding:20px;text-align:center;">\
                                <div class="row">Data Sudah Tersimpan </div>\
                                <div class="row"></div>\
                             <div class="ngdialog-buttons">\
                               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">OK</button>\
                              </div></fieldset>'
                               ,
                                 plain: true,
                                 controller: 'CusDataController',
                       });
                    if ($scope.mFamily.familyId == undefined) {
                      // new preference, add to preference master and value
                    } else{
                      //update only
                    };

                    $scope.afamily=true;
                    $scope.alldata=false;        

                    $scope.mFamily = {};
                // };
      } else if(sp == 3){
          console.log("Save Prefence Button=>",$scope.mPreferensi);
            // var error=[];
            // if ($scope.mPreferensi.preferensi == undefined || $scope.mPreferensi.preferensi == '') {error.push("Preferensi kosong");};
            // if ($scope.mPreferensi.desc == undefined || $scope.mPreferensi.desc == '') {error.push("Keterangan kosong");};
            //   if (error.length > 0) {
            //       bsNotify.show(
            //           {
            //               size: 'big',
            //               type: 'danger',
            //               title: "Ditemukan beberapa data yang salah",
            //               content: error.join('<br>'),
            //               number: error.length
            //           }
            //         );   
            //     } else{

                    ngDialog.openConfirm ({
                      template:'<fieldset style="border:3px solid #e2e2e2;padding:20px;text-align:center;">\
                                <div class="row">Data Sudah Tersimpan </div>\
                                <div class="row"></div>\
                             <div class="ngdialog-buttons">\
                               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">OK</button>\
                              </div></fieldset>'
                               ,
                                 plain: true,
                                 controller: 'CusDataController',
                       });
                    if ($scope.mPreferensi.preferensiType == undefined) {
                      // new preference, add to preference master and value
                    } else{
                      //update only
                    };
          
                    $scope.aInfoPreferensi=true;
                    $scope.alldata=false;        
                    $scope.newPref=false;
                    $scope.mPreferensi = {};
                // };
      } else if(sp == 4){
          console.log("Save Sosmed Button=>",$scope.mSosmed);
            // var error=[];
            // if ($scope.mSosmed.sosial == undefined || $scope.mSosmed.sosial == '') {error.push("Media Sosial kosong");};
            // if ($scope.mSosmed.desc == undefined || $scope.mSosmed.desc == '') {error.push("Keterangan kosong");};
            
            //   if (error.length > 0) {
            //       bsNotify.show(
            //           {
            //               size: 'big',
            //               type: 'danger',
            //               title: "Ditemukan beberapa data yang salah",
            //               content: error.join('<br>'),
            //               number: error.length
            //           }
            //         );   
            //     } else{
                    ngDialog.openConfirm ({
                      template:'<fieldset style="border:3px solid #e2e2e2;padding:20px;text-align:center;">\
                                <div class="row">Data Sudah Tersimpan </div>\
                                <div class="row"></div>\
                             <div class="ngdialog-buttons">\
                               <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">OK</button>\
                              </div></fieldset>'
                               ,
                                 plain: true,
                                 controller: 'CusDataController',
                       });

                    if ($scope.mSosmed.sosMedId == undefined) {
                      // new sosmed, add to sosmed master and value
                    } else{
                      //update only
                    };

                    $scope.aSosMed=true;
                    $scope.alldata=false;        
                    $scope.newSosmed=false;
                    $scope.mSosmed = {};
                // };
      } else if(sp == 5){
          console.log("Save Vehicle Button=>",$scope.mVehicle);
            var error=[];
            if ($scope.$scope.mVehicle.merk == undefined || $scope.$scope.mVehicle.merk == '') {error.push("Merk Kendaraan kosong");};
            if ($scope.$scope.mVehicle.category == undefined || $scope.$scope.mVehicle.category == '') {error.push("Kategori Kendaraan kosong");};
            
              if (error.length > 0) {
                  bsNotify.show(
                      {
                          size: 'big',
                          type: 'danger',
                          title: "Ditemukan beberapa data yang salah",
                          content: error.join('<br>'),
                          number: error.length
                      }
                    );   
                } else{
                    if ($scope.mVehicle.vehicleId == undefined) {
                      // new sosmed, add to sosmed master and value
                    } else{
                      //update only
                    };

                    $scope.vehicle=true;
                    $scope.alldata=false;        

                    $scope.mVehicle = {};
                };
      }

    }

    $scope.user = {
      name: 'John Doe',
      email: '',
      phone: '',
      address: 'Mountain View, CA',
      donation: 19.99
    };


    // ---- accordion 1 Address
    $scope.deleteThisRow = function(del){
      console.log("delete",del,"===>");
      if (del == 1) {
        // console.log("delete address",val);
        $scope.selectedRows = angular.copy($scope.gridDataAddress.selection.getSelectedRows());
        console.log("selected=>",$scope.selectedRows);

        if ($scope.selectedRows.length > 1) {
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda ingin menghapus '+ $scope.selectedRows.length+' alamat yang terpilih ? </div>\
                          <div class="row"> Mohon Di Periksa Ulang</div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(1)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CusDataController',
               });
        } else{
          ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda yakin ingin menghapus alamat ini ?</div>\
                          <div class="row"><div class="col-md-5">  Category Alamat </div><div class="col-md-7"> = <input type="text" ng-disabled="true" value="'+ $scope.selectedRows[0].ContactCategoryName+'"> </div></div>\
                          <div class="row"><div class="col-md-5"> Alamat </div><div class="col-md-7"> = <input type="text"  ng-disabled="true" value="'+  $scope.selectedRows[0].add +'"> </div></div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(1)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CusDataController',
               });
        };

      } else if(del == 2){
        // console.log("delete family",val);
        $scope.selectedRows = angular.copy($scope.gridDataFamily.selection.getSelectedRows());
        console.log("selected=>",$scope.selectedRows);

        if ($scope.selectedRows.length > 1) {
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda ingin menghapus '+ $scope.selectedRows.length+' alamat yang terpilih ? </div>\
                          <div class="row"> Mohon Di Periksa Ulang</div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(2)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CusDataController',
               });
        } else{
          ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda yakin ingin menghapus Informasi Keluarga ini ?</div>\
                          <div class="row"><div class="col-md-5">  Hubungan Keluarga </div><div class="col-md-7"> = <input type="text" ng-disabled="true" value="'+ $scope.selectedRows[0].familyRelationship+'"> </div></div>\
                          <div class="row"><div class="col-md-5"> Nama </div><div class="col-md-7"> = <input type="text"  ng-disabled="true" value="'+  $scope.selectedRows[0].name +'"> </div></div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(2)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CusDataController',
               });
        };
      } else if(del == 3){
        console.log("delete preference",val);
        
      } else if(del == 4){
        console.log("delete sosmed",val);
      } else if(del == 5){
        // console.log("delete vehicle",val);
        $scope.selectedRows = angular.copy($scope.gridDataVehicle.selection.getSelectedRows());
        console.log("selected=>",$scope.selectedRows);

        if ($scope.selectedRows.length > 1) {
            ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda ingin menghapus '+ $scope.selectedRows.length+' Informasi Kendaraan ? </div>\
                          <div class="row"> Mohon Di Periksa Ulang</div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(5)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CusDataController',
               });
        } else{
          ngDialog.openConfirm ({
              template:'<fieldset style="border:3px solid #e2e2e2;padding:20px"><div class="row">Apakah anda yakin ingin menghapus Informasi Kendaraan ini ?</div>\
                          <div class="row"><div class="col-md-5">  Merk Kendaraan </div><div class="col-md-7"> = <input type="text" ng-disabled="true" value="'+ $scope.selectedRows[0].merk+'"> </div></div>\
                          <div class="row"><div class="col-md-5"> Nomor Polisi </div><div class="col-md-7"> = <input type="text"  ng-disabled="true" value="'+  $scope.selectedRows[0].nopol +'"> </div></div>\
                     <div class="ngdialog-buttons">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="deletedItem(5)">Hapus</button>\
                      </div></fieldset>'
                       ,
                         plain: true,
                         controller: 'CusDataController',
               });
        };
      };
    }

    $scope.deletedItem = function(del){
      console.log("deleted ===>",del);
      $scope.closeThisDialog(0);
    }

    $scope.EditThisRow = function(edit,val){
      console.log("Edit==>",val)
      if (edit == 1) {
        console.log("Edit address",val);
        $scope.address=false;
        $scope.alldata=true;
        $scope.mAddress=angular.copy(val);

      } else if(edit == 2){
        console.log("Edit family",val);
        $scope.afamily=false;
        $scope.alldata=true;
        $scope.mFamily=angular.copy(val);
      } else if(edit == 3){
        console.log("Edit Preference",val);
        $scope.mPreferensi = {};
        $scope.mPreferensi.preferensiType = val.id;
        $scope.mPreferensi.preferensi = val.name;
        $scope.mPreferensi.desc = val.desc;
        $scope.aInfoPreferensi=false;
        $scope.alldata=true;
      } else if(edit == 4){
        console.log("Edit sosmed",val);
        $scope.mSosmed.sosMedId = val.id;
        $scope.mSosmed.sosial = val.name;
        $scope.mSosmed.desc = val.desc;
        $scope.aSosMed=false;
        $scope.alldata=true;
      } else if(edit == 5){
        console.log("Edit vehicle",val);
        // $scope.mVehicle.vehicleId = val.id;
        // $scope.mVehicle.merk = val.merkId;
        // $scope.mVehicle.category = val.categoryId;
        // $scope.mVehicle.model = val.modelId;
        // $scope.mVehicle.type = val.typeId;
        // $scope.mVehicle.colour = val.colourId;
        // $scope.mVehicle.nopol = val.nopolId;
        // $scope.mVehicle.tahun = val.tahunId;
        $scope.mVehicle = angular.copy(val);
        $scope.vehicle=false;
        $scope.alldata=true;
      };

      
    }

    $scope.master = function(cd){
      // if (ts == 1) {
          if (cd == 1) {
            $scope.ubah = true;
            $scope.batal = false;
            $scope.simpan = false;  
            $scope.valueChange = false;
          } else if (cd == 2) {
            $scope.ubah = false;
            $scope.batal = true;
            $scope.simpan = true;
            $scope.valueChange = true;  
          } else if (cd == 3){
            $scope.ubah = false;
            $scope.batal = true;
            $scope.simpan = true;  
            $scope.valueChange = true;
          };
             

      
    }
    
    $scope.toyotaIdLink = function(tid){
      console.log("toyotaIdLink==>",tid.typeCustomer);
      
      if (tid.typeCustomer == 1) {
        $scope.filterSearh.TID = angular.copy(tid.typeCustomer);
        $scope.filterSearh.flag.desc="Nomor Rangka";
        $scope.filterSearh.flag.flag=0;
        $scope.filterSearh.flag.key="NoRank";
        $scope.filterSearh.flag.value="www";
        $scope.cari();  

      } else {
        $scope.search2 = true;
        $scope.alldataInstitusi = false;

        CusData.getDataInstitusi().then(function(res) {
            console.log("ressss==>",res.data.Result);
            // $scope.acc1 = res.data.acc1;
            // $scope.acc2 = res.data.acc2;
            $scope.gridDataAddressInstitusi.data = res.data.Result;
            // $scope.gridDataFamily.data = res.data.ResultFamily;
            // $scope.gridDataPreferensi.data = res.data.ResultPreferensi;
            // // var xmedsos = res.data.ResultMedsos;
            // for (i in res.data.ResultMedsos) {
            //   if(res.data.ResultMedsos[i].xnote == 1){
            //     // console.log("ya....");
            //     res.data.ResultMedsos[i].flag = "Ya"
            //   }else{
            //     // console.log("tidak....");
            //     res.data.ResultMedsos[i].flag = "Tidak"
            //   }
            // }
            // $scope.gridDataMedsos.data = res.data.ResultMedsos;
            // $scope.gridDataVehicle.data = res.data.ResultVehicle;
            // $scope.faData = res.data.fieldAction;
            // $scope.sscData = res.data.ssc;
            // $scope.cslData = res.data.csl;
            // $scope.idiData = res.data.idi;
            // $scope.complainData = res.data.complain;
            // $scope.csData = res.data.cs;
            // $scope.gridDataLogHistory.data = res.data.ResultLogHistory;
            // // $scope.grid.data = res.data.Result;

          });
      };
      // console.log("====>",$scope.selectedRows);
      

    }
    $scope.rangkaLink = function(rl){
      console.log("toyotaIdLink==>",rl);
      $scope.cari();
    }

    $scope.detailCSL = function(x){
      console.log("detailll==>",x);


      alert("detaaaailllll==>",x);
    }


    // $scope.selectGridRow = function () {
    //   console.log("selected");
    // if ($scope.selectedItem[0].total != 0) {
    //         $location.path('items/' + $scope.selecteditem[0].id);
    // }};
    // $scope.deleteThisRow = function(ev) {
    // // Appending dialog to document.body to cover sidenav in docs app
    //   var confirm = $mdDialog.confirm()
    //         .title('Would you like to delete your debt?')
    //         .textContent('All of the banks have agreed to forgive you your debts.')
    //         .ariaLabel('Lucky day')
    //         .targetEvent(ev)
    //         .ok('Please do it!')
    //         .cancel('Sounds like a scam');

    //   $mdDialog.show(confirm).then(function() {
    //     // $scope.status = 'You decided to get rid of your debt.';
    //     alert("You decided to get rid of your debt.");
    //   }, function() {
    //     // $scope.status = 'You decided to keep your debt.';
    //     alert("You decided to keep your debt.");
    //   });
    // };
    //----------------------------------
    // Grid Setup
    //----------------------------------
  // $scope.gridDataGroupFilter = {  
  $scope.grid = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: true,
    multiSelect : false,
    enableFullRowSelection: true,
    enableRowHeaderSelection: false,
    enableFiltering:true,
    enableSelectAll : false,
// grid.appScope.
    columnDefs: [
        {name: 'Toyota Id',field: 'toyotaId'}, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
        {name: 'Nama Cabang',field: 'branchName'},
        {name: 'Nama',field: 'name'},
        {name: 'Model',field: 'modelName'},
        {name: 'Nomor Polisi',field: 'noPol'},
        {name: 'Nomor Rangka',field: 'noRank'} //, cellTemplate:'<a ng-click="grid.appScope.rangkaLink(row.entity)">{{row.entity.noRank}}</a>' 
    // };{{row.entity}}
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridApi = gridApi;

        $scope.gridApi.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
              // console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  }
  $scope.gridDataAddress = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: false,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: true,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
        { name:'', field: 'xstat', width:'3%', type: 'boolean', cellTemplate: '<input type="checkbox" ng-model="row.entity.xstat" disabled>'},
        {name: 'Kategori Rumah',field: 'ContactCategoryName'},
        {name: 'Alamat Rumah',field: 'add'},
        {name: 'No Telepon',field: 'phone'},
        {name: 'RT/RW',field: 'rt'},
        {name: 'Provinsi',field: 'provinsiName'},
        {name: 'kecamatan',field: 'kecamatanName'},
        {name: 'Kelurahan',field: 'kelurahanName'},
        {name: 'Kode Box',field: 'KodePos'},
        // {name: 'Hapus', cellTemplate: 
        // //      '<div class="grid-action-cell">'+
        // //      '<a ng-click="$event.stopPropagation(); deleteThisRow(row.entity);" href="#">Delete</a></div>'}
        // '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(1,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
        {name: 'Ubah', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(1,row.entity)" ng-disabled="grid.appScope.valueChange">Ubah</button>'
      }    
    // };
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridDataAddress = gridApi;

        $scope.gridDataAddress.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridDataAddress.selection.getSelectedRows();
              console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridDataAddress.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridDataAddress.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  };

  // $scope.gridDataAddress.onRegisterApi = function(gridApi){
  //     //set gridApi on scope
  //     $scope.gridDataAddress = gridApi;
  //     console.log("cek",$scope.gridApi);
  //     gridApi.selection.on.rowSelectionChanged($scope,function(row){
  //       var msg = 'row selected ' + row;
  //       console.log(msg,$scope.gridDataAddress.selection.getSelectedRows());
  //       console.log("cek",$scope.gridApi);
  //     });

  //     gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
  //       var msg = 'rows changed ' + rows.length;
  //       console.log(msg);
  //     });
  // };
  // $scope.gridDataAddressInstitusi = {
  $scope.gridDataAddressInstitusi = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: false,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: true,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
        { name:'', field: 'xstat', width:'3%', type: 'boolean', cellTemplate: '<input type="checkbox" ng-model="row.entity.xstat" disabled>'},
        {name: 'Kategori Rumah',field: 'ContactCategoryName'},
        {name: 'Alamat Rumah',field: 'add'},
        {name: 'No Telepon',field: 'phone'},
        {name: 'RT/RW',field: 'rt'},
        {name: 'Provinsi',field: 'provinsiName'},
        {name: 'kecamatan',field: 'kecamatanName'},
        {name: 'Kelurahan',field: 'kelurahanName'},
        {name: 'Kode Box',field: 'KodePos'},
        // {name: 'Hapus', cellTemplate: 
        // //      '<div class="grid-action-cell">'+
        // //      '<a ng-click="$event.stopPropagation(); deleteThisRow(row.entity);" href="#">Delete</a></div>'}
        // '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(1,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
        {name: 'Ubah', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(1,row.entity)" ng-disabled="grid.appScope.valueChange">Ubah</button>'
      }    
    // };
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridDataAddressInstitusi = gridApi;

        $scope.gridDataAddressInstitusi.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridDataAddressInstitusi.selection.getSelectedRows();
              console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridDataAddressInstitusi.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridDataAddressInstitusi.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  };
  $scope.gridDataFamily = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: false,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: true,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Nama',  field: 'name'},
      { name:'Jenis Kelamin', field: 'genderValue'},
      { name:'Nomor Handphone', field: 'phone'},
      { name:'Tempat Lahir', field: 'placeBirth'},
      { name:'Tanggal Lahir', field: 'dateBirht'},
      { name:'Keterangan Lain', field: 'notes'},
      // {name: 'Hapus' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(2,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
      {name: 'Ubah' , width: '10%', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(2,row.entity)" ng-disabled="grid.appScope.valueChange2">Ubah</button>'
      }
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridDataFamily = gridApi;

        $scope.gridDataFamily.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridDataFamily.selection.getSelectedRows();
              console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridDataFamily.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridDataFamily.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  };

  $scope.gridDataPreferensi = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: true,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: false,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Jenis Informasi Preferensi' , width: '40%',  field: 'name'},
      { name:'Keterangan Preferensi' , width: '50%', field: 'desc'},
      // {name: 'Hapus' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(3,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
        {name: 'Ubah' , width: '10%', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(3,row.entity)">Ubah</button>' // ng-disabled="grid.appScope.valueChange"
      }    
      
    ]
  };

  $scope.gridDataMedsos = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: true,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: false,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      // { name:'Sering Digunakan',  field: 'flag', width: '10%',
      // // cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
      // //     if (grid.getCellValue(row,col) === 'Yes') {
      // //       return 'yes';
      // //     }
      // //   }
      // cellTemplate: '<input type=\"checkbox\" value=\"grid.getCellValue(row,col)\" />'
      //   // }
      // },
      { name:'Media Sosial', field: 'name', width: '20%'},
      { name:'Alamat Media Sosial', field: 'desc',width: '30%'},
      { name:'Sering Digunakan', field: 'flag',cellClass: 'yes',width: '40%'},
      // {name: 'Hapus', width: '10%',cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(4,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
        {name: 'Ubah', width: '10%',cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(4,row.entity)">Ubah</button>' // ng-disabled="grid.appScope.valueChange"
      }    
      
    ]
  };

  $scope.gridDataToyotaVehicle = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: false,
    multiSelect : false,
    enableFullRowSelection: true,
    enableRowHeaderSelection: false,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Merk Mobil' ,   field: 'vehicle'},//width: '40%',
      { name:'Kategori Kendaraan' , field: 'categoryName'}, //width: '40%', 
      { name:'Model' , field: 'modelName'},//width: '40%', 
      { name:'Type' , field: 'vehicleTypeName'},//width: '40%', 
      { name:'Warna' ,  field: 'colourName'},//width: '40%',
      { name:'Nomor Polisi' , field: 'nopol'},//width: '40%', 
      { name:'Tahun Pembelian' , field: 'tahun'},//width: '40%', 
      { name:'Harga Beli' , field: 'price'},//width: '40%', 
      { name: 'Detail' , width: '10%', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.DetailThisRowVehicle(row.entity)" ng-disabled="true">Detail</button>'
      }
      // { name: 'Ubah' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(5,row.entity)" >Ubah</button>'//ng-disabled="grid.appScope.valueChange"
      // }
      // ,
      // { name: 'Hapus' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(5,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
          
      
    ]

  };

  $scope.gridDataVehicle = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: false,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: true,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Merk Mobil' ,   field: 'vehicle'},//width: '40%',
      { name:'Kategori Kendaraan' , field: 'categoryName'}, //width: '40%', 
      { name:'Model' , field: 'modelName'},//width: '40%', 
      { name:'Type' , field: 'vehicleTypeName'},//width: '40%', 
      { name:'Warna' ,  field: 'colourName'},//width: '40%',
      { name:'Nomor Polisi' , field: 'nopol'},//width: '40%', 
      { name:'Tahun Pembelian' , field: 'tahun'},//width: '40%', 
      { name:'Harga Beli' , field: 'price'},//width: '40%', 
      // { name: 'Detail' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-info" ng-click="grid.appScope.DetailThisRowVehicle(row.entity)" ng-disabled="true">Detail</button>'
      // },
      { name: 'Ubah' , width: '10%', cellTemplate: 
        '<button class="btn btn-info" ng-click="grid.appScope.EditThisRow(5,row.entity)" >Ubah</button>'//ng-disabled="grid.appScope.valueChange"
      }
      // ,
      // { name: 'Hapus' , width: '10%', cellTemplate: 
      //   '<button class="btn btn-warning" ng-click="grid.appScope.deleteThisRow(5,row.entity)" ng-disabled="grid.appScope.valueChange">Hapus</button>'},
          
      
    ],
    onRegisterApi : function(gridApi) {
        // set gridApi on $scope
        $scope.gridDataVehicle = gridApi;

        $scope.gridDataVehicle.selection.on.rowSelectionChanged($scope,function(row) {
              $scope.selectedRows = $scope.gridDataVehicle.selection.getSelectedRows();
              console.log("selected=>",$scope.selectedRows);
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
        $scope.gridDataVehicle.selection.on.rowSelectionChangedBatch($scope,function(row) {
              $scope.selectedRows = $scope.gridDataVehicle.selection.getSelectedRows();
              if($scope.onSelectRows){
                  $scope.onSelectRows($scope.selectedRows);
              }
        });
    }
  };

  $scope.gridDataLogHistory = {
    enableGridMenu: true,
    enableSorting: true,
    enableRowSelection: true,
    multiSelect : true,
    enableFullRowSelection: true,
    enableRowHeaderSelection: false,
    enableFiltering:true,
    enableSelectAll : false,

    columnDefs: [
      // { name:'Hubungan Keluarga', field: 'familyRelationship',width:'15%'},// , enableHiding : false 
      { name:'Tipe' ,   field: 'type'},//width: '40%',
      { name:'Modul' , field: 'modul'}, //width: '40%', 
      { name:'Menu' , field: 'menu'},//width: '40%', 
      { name:'Atribut' , field: 'atribut'},//width: '40%', 
      { name:'New Value' ,  field: 'newVal'},//width: '40%',
      { name:'Old Value' , field: 'oldVal'},//width: '40%', 
      { name:'Username' , field: 'userName'},//width: '40%', 
      { name:'Update DateTime' , field: 'update'}//width: '40%', 
          
      
    ]
  };


  // $scope.gridDataGroupFilter.

    // $scope.grid33.data = [{ContactCategory:1,address:"Jakarta",phone:207348723,kelurahan:"tujuh",kecamatan:"3",rtrw:"03/08",kodepos:78676,kabupaten:"jalan",provinsi:"salatiga"}];
    // $scope.grid33 = {
    //   enableSorting: true,
    //   enableRowSelection: true,
    //   multiSelect: true,
    //   enableSelectAll: true,
    //   //showTreeExpandNoChildren: true,
    //   // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
    //   // paginationPageSize: 15,
    //   columnDefs: [
    //     {name: 'Kategori Rumah',field: 'ContactCategory'},
    //     {name: 'Alamat Rumah',field: 'address'},
    //     {name: 'No Telepon',field: 'phone'},
    //     {name: 'RT/RW',field: 'rtrw'},
    //     {name: 'Kelurahan',field: 'kelurahan'},
    //     {name: 'Kode Box',field: 'kodepos'},
    //     {name: 'kecamatan',field: 'kecamatan'},
    //     {name: 'Provinsi',field: 'provinsi'}



    //     // {
    //     //   name: 'Stall Parent Id',
    //     //   field: 'StallParentId',
    //     //   width: '7%'
    //     // },
    //     // {
    //     //   name: 'Name',
    //     //   field: 'Name'
    //     // }
    //   ]
    // };

    // $scope.myData = [
    // {
    //     "firstName": "Cox",
    //     "lastName": "Carney",
    //     "company": "Enormo",
    //     "employed": true
    // },
    // {
    //     "firstName": "Lorraine",
    //     "lastName": "Wise",
    //     "company": "Comveyer",
    //     "employed": false
    // },
    // {
    //     "firstName": "Nancy",
    //     "lastName": "Waters",
    //     "company": "Fuelton",
    //     "employed": false
    // }
    // ];
    // $scope.gridOptions1 = {
    //   enableSorting: true,
    //   enableRowSelection: true,
    //   multiSelect: true,
    //   enableSelectAll: true,
    //   //showTreeExpandNoChildren: true,
    //   // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
    //   // paginationPageSize: 15,
    //   columnDefs: [
    //     {name: 'First hdjgfjhsd Name',field: 'lastName'},
    //     {name: 'Last Name',field: 'firstName'},
    //     {name: 'Company',field: 'company'},
    //     {name: 'Employed',field: 'employed'}
        
    //   ]
    // };
    // $scope.gridOptions1.data = angular.copy($scope.myData);
    // $scope.gridOptions2.data = angular.copy($scope.GridFamily);
    // $scope.grid1.data = angular.copy($scope.myData);

    // $scope.gridOptions2 = {
    //   enableSorting: true,
    //   enableRowSelection: true,
    //   multiSelect: true,
    //   enableSelectAll: true,
    //   //showTreeExpandNoChildren: true,
    //   // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
    //   // paginationPageSize: 15,
    //   columnDefs: [
    //     {name: 'First Name',field: 'firstName'},
    //     {name: 'Last Name',field: 'company'},
    //     {name: 'Company',field: 'lastName'},
    //     {name: 'Employed',field: 'employed'}
        
    //   ]
    // };

    //  $scope.gridOptions = {
    //     data: 'myData',
    //     enablePinning: true,
    //     columnDefs: [{ field: "name", width: 120, pinned: true },
    //                 { field: "age", width: 120 },
    //                 { field: "birthday", width: 120 },
    //                 { field: "salary", width: 120 }]
    // };

    

    // Stall.getData().then(
    //   function(res) {
    //     // $scope.parentData = res.data.Result;
    //     var temp = res.data.Result;
    //     $scope.parentData = [];
    //     var obj = {};
    //     for (var i in temp) {
    //       if (!obj[temp[i].StallId]) {
    //         obj[temp[i].StallId] = {};
    //         obj[temp[i].StallId]["Name"] = temp[i].Name;
    //         obj[temp[i].StallId]["StallId"] = temp[i].StallId;
    //         $scope.parentData.push(obj[temp[i].StallId]);
    //       }
    //     }

    //     $scope.loading = false;
    //     return res.data;
    //   }
    // );
    // $scope.onBeforeEdit = function() {
    //   var json = $scope.grid.data;
    //   var xjson = []
    //   for (var i = 0; i < json.length; i++) {
    //     if (json[i]["StallId"] !== $scope.mData.StallId) {
    //       xjson.push({
    //         'StallId': json[i].StallId,
    //         'Name': json[i].Name
    //       });
    //     }
    //   }
    //   $scope.parentData = xjson;
    // }
  });
