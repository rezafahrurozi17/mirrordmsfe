angular.module('app')
  .factory('CDataBasic', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getDealOut: function() {
        var res=$http.get('/api/fw/Organization/Tree?childOnly=1');
        
        console.log('hasil=>',res);

        //res.data.Result = null;
        return res;
      },

      GetBasicDataOutlet: function() {
        var res=$http.get('/api/ct/GetCustomerBasicTab/');
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      updateData: function(data) {

        console.log("data update",data);
        // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);
        
        return $http.put('/api/ct/PutCCustomerBasicTab', data);
      },
    
      
    }
  });


