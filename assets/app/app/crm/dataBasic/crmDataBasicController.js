angular.module('app')
  .controller('CrmDataBasicController', function($scope, $state, $http, CurrentUser,  bsNotify, ngDialog, CAccessMatrix,CDataBasic, Role, OrgChart, $timeout, Auth) {
    // $scope, $http, $sce,$filter,$timeout,$window,CurrentUser, Service, Role, ServiceRights, OrgChart, Auth

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
                    // $scope.getOrgData();
                    // $scope.getRoleData();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

//     //                   };
    $scope.orgData = []; //OrgChart Collection
    $scope.ctlOrg = {}; //OrgChart Tree Controller
    $scope.selOrg = null;  //Selected Org

    $scope.roleData = []; //Role Collection
    $scope.selRole = null; //Selected Role
    $scope.mAccesFilter = {};

    $scope.formApi = {};

    // var tabFile = [{tab:"Informasi Personal", key:1, OpenState : 1, edit: false},
    //                   {tab:"Informasi Pekerjaan", key:2, OpenState : 1, edit: false},
    //                   {tab:"Informasi Keluarga", key:4, OpenState : 2, edit: false},
    //                   {tab:"Informasi Kendaraan", key:8, OpenState : 2, edit: false},
    //                   {tab:"Informasi Preferensi", key:16, OpenState : null, edit: false},
    //                   {tab:"Informasi Field Action", key:32, OpenState : null, edit: false},
    //                   {tab:"Informasi Riwayat Pwrubahan", key:64, OpenState : 1, edit: false}];


//     // ---------------------------------------------------------------------------------
//     // --------------------------------- Role  -----------------------------------------
//     // ---------------------------------------------------------------------------------
    // $scope.getRoleData = function(){
    //   Role.getData().then(function(result) {
    //       $scope.roleData = result.data.Result;
    //       console.log("roleData=>",result.data);
    //       // $scope.ctlOrg.expand_all();
    // });

    // $scope.selectRole = function(role){
    //       console.log("sel role->",role);
    //       $scope.selRole = role;
    //       // $scope.getServiceRightsbyOrgRole(role);
    // }
    // }
//     // ---------------------------------------------------------------------------------
//     // --------------------------- Organization Chart  ---------------------------------
//     // ---------------------------------------------------------------------------------

//     //----------------------------------
//     // Backend Operation
    var bin = [1,2,4,8,16,32,64];
    $scope.getData = function(){
      // console.log("getDataBasic",$scope.mAccesFilter);

      var tot = 43;
      var tit = 1;

      CDataBasic.GetBasicDataOutlet().then(function(result) {
          $scope.grid.data = result.data.Result;
          $scope.changingFilterButton();

          console.log("datDataBasic=>",result.data);
          // $scope.ctlOrg.expand_all();
        });

      // $scope.grid.data = tabFile;
      
    }
    // ==== Append by anita - faisal =====
    $scope.changingFilterButton = function() {
        var tmpGridCols = [];
        var x = -1;
        for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
          console.log("$scope.grid.columnDefs[i]",$scope.grid.columnDefs[i]);
            if ($scope.grid.columnDefs[i].visible == undefined && $scope.grid.columnDefs[i].name !== "Kategori Data") {
                x++;
                tmpGridCols.push($scope.grid.columnDefs[i]);
                tmpGridCols[x].idx = i;
            }
        }
        console.log("tmpGridCols",tmpGridCols,$scope.formApi);
        $scope.formApi.changeDropdown(tmpGridCols);
    }
    // ===================================

    // $scope.getOrgData = function(){
    //   console.log("user==>",$scope.user);
    //     CAccessMatrix.getDealOut($scope.user).then(function(result) {
    //       $scope.orgData = result.data.Result;
    //       console.log("orgData=>",result.data);
    //       // $scope.ctlOrg.expand_all();
    //     });
    // }

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    $scope.saveAcces = function(){
      console.log("save==>",$scope.grid.data);
      CDataBasic.updateData($scope.grid.data).then(function(){
        
      })
      bsNotify.show({
        size: 'small',
        type: 'success',
        title: "Data berhasil disimpan"
            // ,
            // content: error.join('<br>'),
            // number: error.length
    });
      // updateData
    }

    // $scope.selectDealer = function(xD){
    //   console.log("dealeeer==>",xD);
    //   $scope.outletData = xD.Child;
    // }
       // var btnActionAccesTemplate0 = '<button class="ui icon inverted grey button"'+
       //                              ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:green;margin:1px 1px 0px 2px"'+
       //                              // ' onclick="this.blur()"'+
       //                              ' ng-click="grid.appScope.$parent.gridClickAcces(row.entity,grid.renderContainers.body.visibleRowCache.indexOf(row))">'+
       //                              '<i ng-class="'+
       //                                                '{ \'fa fa-fw fa-lg fa-toggle-on\' : row.entity.OpenState,'+
       //                                                '  \'fa fa-fw fa-lg fa-toggle-off\': !row.entity.OpenState,'+
       //                                                '}'+
       //                              '">'+
       //                              '</i>'+
       //                          '</button>';
      // var btnActionAccesTemplate = '<div ng-init="releaseAction=0"><input name="Release" ng-model="releaseAction" type="radio" value="0" style="width:20px">&nbsp;None&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="Release" type="radio" ng-model="releaseAction"  value="1" style="width:20px">&nbsp;Accept&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="Release" type="radio" ng-model="releaseAction"  value="2" style="width:20px">&nbsp;Decline</div>'
  
      var btnActionAccesTemplate = '<bsradiobox ng-model="row.entity.FlagState"'+
                                              'options="[{text:\'Basic\',value:1},{text:\'Advanced\',value:2}]"'+
                                              'layout="H"'+
                                              '>'+
                                  '</bsradiobox>';
      // var btnActionEditTemplate = '<button class="ui icon inverted grey button"'+
      //                               ' style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:green;margin:1px 1px 0px 2px"'+
      //                               // ' onclick="this.blur()"'+
      //                               ' ng-click="grid.appScope.$parent.gridClickUbah(row.entity,grid.renderContainers.body.visibleRowCache.indexOf(row))">'+
      //                               '<i ng-class="'+
      //                                                 '{ \'fa fa-fw fa-lg fa-toggle-on\' : row.entity.edit,'+
      //                                                 '  \'fa fa-fw fa-lg fa-toggle-off\': !row.entity.edit,'+
      //                                                 '}'+
      //                               '">'+
      //                               '</i>'+
      //                           '</button>';

      $scope.gridClickUbah = function(row,idx){
      console.log(idx,"click==>",row);
        // $scope.grid.
        
        if ($scope.grid.data[idx].edit) {
            for (var i = 0; i < $scope.grid.data.length; i++) {
              $scope.grid.data[i].edit = false;
            };
        } else{
            for (var i = 0; i < $scope.grid.data.length; i++) {
              $scope.grid.data[i].edit = true;
            };
        };
      
      }
      $scope.gridClickAcces = function(row,idx){
      console.log(idx,"click==>",row);
      // $scope.grid.
        if ($scope.grid.data[idx].OpenState) {
            $scope.grid.data[idx].OpenState = false;  
        } else{
            $scope.grid.data[idx].OpenState = true;  
        };
      
      }

    $scope.grid = {
        enableGridMenu: true,
        enableSorting: true,
        // enableRowSelection: true,
        // multiSelect : false,
        // enableFullRowSelection: true,
        // enableRowHeaderSelection: false,
        // enableFiltering:true,
        // enableSelectAll : false,
    // grid.appScope.
        columnDefs: [
            {name: 'Jenis Informasi',field: 'Description', width: '70%'}, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            // {name: 'Kategori',field: 'key'},
            { name:'Kategori Data', allowCellFocus: false, width:'30%', pinnedRight:true,
                                       enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                                       cellTemplate: btnActionAccesTemplate
              },
            // { name:'Advanced', allowCellFocus: false, width:100, pinnedRight:true,
            //                            enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
            //                            cellTemplate: btnActionEditTemplate
            //   },
             //, cellTemplate:'<a ng-click="grid.appScope.rangkaLink(row.entity)">{{row.entity.noRank}}</a>' 
        // };{{row.entity}}
        ],
        onRegisterApi : function(gridApi) {
            // set gridApi on $scope
            $scope.gridApi = gridApi;

            $scope.gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                  $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                  // console.log("selected=>",$scope.selectedRows);
                  if($scope.onSelectRows){
                      $scope.onSelectRows($scope.selectedRows);
                  }
            });
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                  $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                  if($scope.onSelectRows){
                      $scope.onSelectRows($scope.selectedRows);
                  }
            });
        }
      }




});