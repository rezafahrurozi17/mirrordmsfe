angular.module('app')
    .controller('MaintainPIController', function(PrintRpt, $rootScope, bsNotify, $scope, $http, CurrentUser, MaintainPIFactory, MaintainSOFactory, $timeout, uiGridConstants) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.CreateForm = false;
        $scope.MaintainForm = true;
        $scope.showKontak = true;
        $scope.showHandphone = true;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';
        $scope.selctedRow = [];

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });

        $scope.typeData2 = [
            { text: "DP", value: 1 },
            { text: "Full Payment", value: 2 }
        ];
        $scope.typeData = { data: [{ text: "DP", value: 1 }, { text: "Full Payment", value: 2 }]};
        
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainPI = null; //Model
        $scope.xRole = { selected: [] };
        var mount = new Date();
        var year = mount.getFullYear();
        var mounth = mount.getMonth();
        var lastdate = new Date(year, mounth+1, 0).getDate();
        $scope.disabledSimpanPI = false;
        $scope.firstDate = new Date(mount.setDate("01"));
        $scope.lastDate = new Date (mount.setDate(lastdate));
        $scope.filter = { PIDateStart: $scope.firstDate, PIDateEnd: $scope.lastDate, CustomerName: null };

        $scope.filter.withAccBit = false;

        $scope.dateOptionsStart = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.dateOptionsEnd = {
            minDate: new Date(),
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.PIDate = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        }

        $scope.dateOptionPengiriman = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };
		
		var Da_TodayDate=new Date();
		var da_awal_tanggal1 = new Date(Da_TodayDate.getFullYear(), Da_TodayDate.getMonth(), 1);
		$scope.filter.PIDateStart=da_awal_tanggal1;
		$scope.filter.PIDateEnd=Da_TodayDate;

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            alerts = [];
            $scope.gridData = [];
            if ($scope.filter.PIDateStart == null || $scope.filter.PIDateEnd == null) {
                bsNotify.show({
                    size: 'big',
                    type: 'succes',
                    timeout: 3000,
                    color: "#c00",
                    title: "Filter Tidak Boleh Kosong.!"
                });
            } else {
                $scope.filter.PIDateStart = $scope.changeFormatDate($scope.filter.PIDateStart);
                $scope.filter.PIDateEnd = $scope.changeFormatDate($scope.filter.PIDateEnd);
                
                MaintainPIFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        return res.data.Result;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
        }

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2)
                    // ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    // ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    // ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };
        $scope.changeFormatDate = function(item) {
            var tmpParam = item;
            tmpParam = new Date(tmpParam);
            var finalDate
            var yyyy = tmpParam.getFullYear().toString();
            var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpParam.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            
            return finalDate;
        }

        // norman
        $scope.Search = {};
        $scope.SearchSPK = function(SearchNospk) {
            try {
                SearchNospk.BillingDateStart = fixDate(SearchNospk.BillingDateStart);
                SearchNospk.BillingDateStart.slice(0, 10);

            } catch (e1) {
                SearchNospk.BillingDateStart = null;
            }
            try {
                SearchNospk.BillingDateEnd = fixDate(SearchNospk.BillingDateEnd);
                SearchNospk.BillingDateEnd.slice(0, 10);
            } catch (e1) {
                SearchNospk.BillingDateEnd = null;
            }
           
            
            SearchNospk.PICategoryId = $scope.filter.typeId;
            // SearchNospk.withAccBit = $scope.filter.withAccBit;
            SearchNospk.withAccBit =  $scope.AccessoriesAndItemJoined;
            console.log("mana anjay",SearchNospk);

            MaintainPIFactory.getNoSPK(SearchNospk).then(
                function(res) {
                    $scope.gridPI.data = res.data.Result;
                    $scope.loading = false;
                    //return res.data.Result;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

        }

        MaintainPIFactory.getBilling().then(
            function(res) {
                $scope.getBillingType = res.data.Result;
                return res.data;
            }
        );

        $scope.bilingType = function(BilingtypeId) {
            if (BilingtypeId != 1) {
                $scope.gridPI.multiSelect = false;
            } else {
                $scope.gridPI.multiSelect = true;
            }
        }

        $scope.opdstatus = false;

        $scope.radioPiutang = function(Billingtype){
            if(Billingtype == 2 || Billingtype == 3){
                $scope.SOBilling = "Billing"
                $scope.gridPI.columnDefs =  [
                    { name: 'Nama Pelanggan', field: 'CustomerName', width: '20%', enableCellEdit: false },
                    { name: 'No Billing', field: 'BillingCode', width: '16%', enableCellEdit: false },
                    { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false },
                    { name: 'Yang Pernah Ditagihkan', field: 'TotalYgPernahDitagih', cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    { name: 'Sisa Tagihan', field: 'SisaTagihan',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    { name: 'Outstanding AR', field: 'OutStandingAR',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    {
                        name: 'Nominal Tagihan *',
                        field: 'Invoice',
                        enableCellEdit: true,
                        cellFilter: 'currency:"Rp." : 0',
                        type: 'number',
                        cellClass: 'text-right',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellFilter: 'currency:"Rp."',
                        aggregationHideLabel: true,
                        // customtreeAggregationFinalizerFn: function(aggregation) {
                        //     aggregation.rendered = 'TotalTagihan : ' + aggregation.value
                        // }
                    },
                ]                
                $scope.filter.typeId = 2;
            }else{
                $scope.SOBilling = "SO"
                $scope.gridPI.columnDefs =  [
                    { name: 'Nama Pelanggan', field: 'CustomerName', width: '20%', enableCellEdit: false },
                    { name: 'No SO', field: 'SOCode', width: '16%', enableCellEdit: false },
                    { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false },
                    { name: 'Yang Pernah Ditagihkan', field: 'TotalYgPernahDitagih', cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    { name: 'Sisa Tagihan', field: 'SisaTagihan',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    { name: 'Outstanding TDP', field: 'OutStandingAR',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    {
                        name: 'Nominal Tagihan *',
                        field: 'Invoice',
                        enableCellEdit: true,
                        cellFilter: 'currency:"Rp." : 0',
                        type: 'number',
                        cellClass: 'text-right',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellFilter: 'currency:"Rp."',
                        aggregationHideLabel: true,
                        // customtreeAggregationFinalizerFn: function(aggregation) {
                        //     aggregation.rendered = 'TotalTagihan : ' + aggregation.value
                        // }
                    },
                ]
                
                //$scope.opdstatus = false;
            }
            $scope.gridPI.data = [];
            $scope.selctedRow = [];
        }

        $scope.kosongGrid = function(Billingtype) {
            console.log('Cek Filter', $scope.filter.typeId)
            if(Billingtype == 2 || Billingtype == 3){
                $scope.SOBilling = "Billing"               
                $scope.filter.typeId = 2;
                $scope.opdstatus = true;
                $scope.gridPI.columnDefs =  [
                    { name: 'Nama Pelanggan', field: 'CustomerName', width: '20%', enableCellEdit: false },
                    { name: 'No Billing', field: 'BillingCode', width: '16%', enableCellEdit: false },
                    { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false },
                    { name: 'Yang Pernah Ditagihkan', field: 'TotalYgPernahDitagih', cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    { name: 'Sisa Tagihan', field: 'SisaTagihan',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    { name: 'Outstanding AR', field: 'OutStandingAR',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                    {
                        name: 'Nominal Tagihan *',
                        field: 'Invoice',
                        enableCellEdit: true,
                        cellFilter: 'currency:"Rp." : 0',
                        type: 'number',
                        cellClass: 'text-right',
                        aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellFilter: 'currency:"Rp."',
                        aggregationHideLabel: true,
                        // customtreeAggregationFinalizerFn: function(aggregation) {
                        //     aggregation.rendered = 'TotalTagihan : ' + aggregation.value
                        // }
                    },
                ] 
                // bsNotify.show({
                //     title: "Peringatan",
                //     content: 'Tipe OPD atau Purna Jual hanya dapat melakukan pembayaran Full Payment.',
                //     type: 'warning'
                // });
            }else{
                if($scope.filter.typeId == 1){
                    $scope.SOBilling = "SO"
                    $scope.gridPI.columnDefs =  [
                        { name: 'Nama Pelanggan', field: 'CustomerName', width: '20%', enableCellEdit: false },
                        { name: 'No SO', field: 'SOCode', width: '16%', enableCellEdit: false },
                        { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false },
                        { name: 'Yang Pernah Ditagihkan', field: 'TotalYgPernahDitagih', cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                        { name: 'Sisa Tagihan', field: 'SisaTagihan',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                        { name: 'Outstanding TDP', field: 'OutStandingAR',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                        {
                            name: 'Nominal Tagihan *',
                            field: 'Invoice',
                            enableCellEdit: true,
                            cellFilter: 'currency:"Rp." : 0',
                            type: 'number',
                            cellClass: 'text-right',
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellFilter: 'currency:"Rp."',
                            aggregationHideLabel: true,
                            // customtreeAggregationFinalizerFn: function(aggregation) {
                            //     aggregation.rendered = 'TotalTagihan : ' + aggregation.value
                            // }
                        },
                    ]
                } else {
                    $scope.SOBilling = "Billing"
                    $scope.gridPI.columnDefs =  [
                        { name: 'Nama Pelanggan', field: 'CustomerName', width: '20%', enableCellEdit: false },
                        { name: 'No Billing', field: 'BillingCode', width: '16%', enableCellEdit: false },
                        { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false },
                        { name: 'Yang Pernah Ditagihkan', field: 'TotalYgPernahDitagih', cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                        { name: 'Sisa Tagihan', field: 'SisaTagihan',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                        { name: 'Outstanding AR', field: 'OutStandingAR',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                        {
                            name: 'Nominal Tagihan *',
                            field: 'Invoice',
                            enableCellEdit: true,
                            cellFilter: 'currency:"Rp." : 0',
                            type: 'number',
                            cellClass: 'text-right',
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellFilter: 'currency:"Rp."',
                            aggregationHideLabel: true,
                            // customtreeAggregationFinalizerFn: function(aggregation) {
                            //     aggregation.rendered = 'TotalTagihan : ' + aggregation.value
                            // }
                        },
                    ] 
                }
                $scope.opdstatus = false;
            }
            $scope.gridPI.data = [];
            $scope.selctedRow = [];
        }

        $scope.cetakanUrl = null;
        $scope.cetakanUrlSub3 = null;
        $scope.SPFBit = null;
        $scope.SPTBit = null;
        $scope.SPPBBit = null;
        $scope.SimpanPI = function() {
            MaintainPIFactory.getMasterCetakan().then(
                function (res) 
                {
                    $scope.cetakTemp = res.data.Result;
                    //$scope.cetakTemp.data = res.data.Result;
                    console.log('Temp =', $scope.cetakTemp)
                    //console.log('Temp2 =', $scope.cetakTemp.data)
                
            $scope.disabledSimpanPI = true;
            $scope.DataPI = {};
            $scope.DataPI.ProformaInvoiceCategoryId = $scope.filter.typeId;
            $scope.DataPI.InvoiceTargetId = $scope.filter.ProformaInvoiceTarget;
            $scope.DataPI.ProformaInvoiceDate = $scope.filter.DatePI;
            $scope.DataPI.CustomerId = $scope.selctedRow[0].CustomerId;
            try {
                $scope.DataPI.ProformaInvoiceDate = fixDate($scope.DataPI.ProformaInvoiceDate);
                $scope.DataPI.ProformaInvoiceDate.slice(0, 10);
            } catch (e1) {
                $scope.DataPI.ProformaInvoiceDate = null;
            }

            //if (typeof $scope.AccessoriesAndItemJoined == 'undefined') {
            if (typeof $scope.AccessoriesAndItemJoined == 'undefined' || typeof $scope.AccessoriesAndItemJoined == false ) {   
                $scope.DataPI.AccessoriesAndItemJoined = false;
            } else {
                $scope.DataPI.AccessoriesAndItemJoined = true;
            }
            $scope.DataPI.TotalInvoice = 0;
            for (var i in $scope.selctedRow) {
                $scope.DataPI.TotalInvoice = $scope.DataPI.TotalInvoice + $scope.selctedRow[i].Invoice;
            }

            $scope.DataPI.ListOfPIDetail = $scope.selctedRow;

            // DP + Finis  = 1 & 1
            // DP + OPD = 1 & 2
            // DP + Purna = 1 & 3

            // Full + Finis = 2 & 1
            // Full + OPD = 2 & 2
            // Full + Purna = 2 & 3

            //If tambahan
            // for(var i in $scope.cetakTemp){
            //     if($scope.cetakTemp[i].PrintSettingName == "Surat Permohonan Faktur" && $scope.cetakTemp[i].ActiveNonActive == 1 &&  $scope.DataPI.InvoiceTargetId == 2){
            //        console.log('Cek Faktur->', $scope.cetakTemp)
            //     } else
            //     if($scope.cetakTemp[i].PrintSettingName == "Surat Permohonan Transfer" && $scope.cetakTemp[i].ActiveNonActive == 1 &&  $scope.DataPI.InvoiceTargetId == 2){
            //         console.log('Cek Transfer->', $scope.cetakTemp)
            //     } else
            //     if($scope.cetakTemp[i].PrintSettingName == "Surat Pernyataan Penyerahan BPKB" && $scope.cetakTemp[i].ActiveNonActive == 1 &&  $scope.DataPI.InvoiceTargetId == 2){
            //         console.log('Cek BPKB->', $scope.cetakTemp)
            //     }
            // }
            $scope.SPFBit = res.data.Result[0].SPFBit;
            $scope.SPTBit = res.data.Result[0].SPTBit;
            $scope.SPPBBit = res.data.Result[0].SPPBBit;
            if($scope.DataPI.ProformaInvoiceCategoryId == 2 && $scope.DataPI.InvoiceTargetId == 2 && ($scope.SPFBit == 1 || $scope.SPTBit == 1 || $scope.SPPBBit == 1)){
                //console.log('Cek BPKB->', $scope.cetakTemp)
                $scope.cetakanUrlSub3 = 'sales/ProformaInvoiceSub3?ProformaInvoiceId=';
            }

            if ($scope.DataPI.ProformaInvoiceCategoryId == 1 && $scope.DataPI.ListOfPIDetail[0].BillingTypeId == 1) {
                $scope.cetakanUrl = 'sales/ProformaInvoiceFinishUnitDP?ProformaInvoiceId=';
            } else if ($scope.DataPI.ProformaInvoiceCategoryId == 1 && $scope.DataPI.ListOfPIDetail[0].BillingTypeId == 2) {
                $scope.cetakanUrl = 'sales/ProformaInvoiceOPDDP?ProformaInvoiceId=';
            } else if ($scope.DataPI.ProformaInvoiceCategoryId == 1 && $scope.DataPI.ListOfPIDetail[0].BillingTypeId == 3) {
                $scope.cetakanUrl = 'sales/ProformaInvoicePurnaJualDP?ProformaInvoiceId=';
            } else if ($scope.DataPI.ProformaInvoiceCategoryId == 2 && $scope.DataPI.ListOfPIDetail[0].BillingTypeId == 1) {
                $scope.cetakanUrl = 'sales/ProformaInvoiceFinishUnitFP?ProformaInvoiceId=';
            } else if ($scope.DataPI.ProformaInvoiceCategoryId == 2 && $scope.DataPI.ListOfPIDetail[0].BillingTypeId == 2) {
                $scope.cetakanUrl = 'sales/ProformaInvoiceOPDFP?ProformaInvoiceId=';
            } else if ($scope.DataPI.ProformaInvoiceCategoryId == 2 && $scope.DataPI.ListOfPIDetail[0].BillingTypeId == 3) {
                $scope.cetakanUrl = 'sales/ProformaInvoicePurnaJualFP?ProformaInvoiceId=';
            }

            //If tambahan
            // for(var i in $scope.cetakTemp.data){
            //     if($scope.cetakTemp[i].PrintSettingName == "Surat Permohonan Faktur" && $scope.cetakTemp[i].ActiveNonActive == 1 &&  $scope.DataPI.InvoiceTargetId == 2){
            //        console.log('Cek Faktur->', $scope.cetakTemp)
            //     }
            //     if($scope.cetakTemp[i].PrintSettingName == "Surat Permohonan Transfer" && $scope.cetakTemp[i].ActiveNonActive == 1 &&  $scope.DataPI.InvoiceTargetId == 2){
            //         console.log('Cek Transfer->', $scope.cetakTemp)
            //     }
            //     if($scope.cetakTemp[i].PrintSettingName == "Surat Pernyataan Penyerahan BPKB" && $scope.cetakTemp[i].ActiveNonActive == 1 &&  $scope.DataPI.InvoiceTargetId == 2){
            //         console.log('Cek BPKB->', $scope.cetakTemp)
            //     }
            // }

            var MaintainPiNominalTagihanSemuaLebihDari0=false;
            
                for(var i = 0; i < $scope.DataPI.ListOfPIDetail.length; ++i)
                {	
                    try
                    {
                        if($scope.DataPI.ListOfPIDetail[i].Invoice>0)
                        {
                            MaintainPiNominalTagihanSemuaLebihDari0=true;
                        }
                        else
                        {
                            MaintainPiNominalTagihanSemuaLebihDari0=false;
                        }
                    }
                    catch(ulala)
                    {
                        MaintainPiNominalTagihanSemuaLebihDari0=false;
                    }
                }
                
                if(MaintainPiNominalTagihanSemuaLebihDari0==true)
                {
                    MaintainPIFactory.savePI($scope.DataPI).then(
                        function(res) {
                            //$scope.gridPI.data = res.data.Result;
                            $scope.loading = false;
                            //return res.data.Result;
                            $scope.createForm = false;
                            $scope.MaintainForm = true;
                            bsNotify.show({
                                title: "Success Message",
                                content: 'Simpan Data PI Berhasil',
                                type: 'success'
                            });
                            $scope.disabledSimpanPI = false;
                            //cetak PI
                            //$scope.PrintFunction($scope.cetakanUrl + res.data.Result[0].ProformaInvoiceId);
                            if($scope.cetakanUrlSub3 != null){
                                var pdfblob = null;
                                PrintRpt.print($scope.cetakanUrlSub3 + res.data.Result[0].ProformaInvoiceId + '&SPFBit=' + $scope.SPFBit + '&SPTBit=' + $scope.SPTBit + '&SPPBBit=' + $scope.SPPBBit).success(function (res) {
                                    var contentType = 'application/pdf';
                                    var blob = new Blob([res], { type: contentType });
                                    //var blobURL = URL.createObjectURL(blob);
                                    //console.log("Cek pdf", blobURL);
                                    console.log("Res", res);
    
                                    saveAs(blob, 'ReportProformaInvoiceSubb3.pdf');
                                    //pdfblob = blobURL;
                    
                                        // if (pdfblob != null)
                                        //     {
                                        //         //printJS(pdfblob);
                                        //     }
                                        // else
                                        //     {
                                        //         console.log("error cetakan", pdfblob);
                                        //     }
                                })

                            }
                            //     .error(function(res) {
                            //         console.log("error cetakan", pdfblob);
                            // });
                            //---------------
                            var pdfFile = null;
                            //var pdftemp = []
                            PrintRpt.print($scope.cetakanUrl + res.data.Result[0].ProformaInvoiceId).success(function(res) {
                                    var file = new Blob([res], { type: 'application/pdf' });
                                    var fileURL = URL.createObjectURL(file);
                
                                    console.log("pdf", fileURL);
                                    //$scope.content = $sce.trustAsResourceUrl(fileURL);
                                    pdfFile = fileURL;

                                    if (pdfFile != null)
                                        {
                                            printJS(pdfFile);
                                        }
                                    else
                                        {
                                            console.log("error cetakan", pdfFile);
                                        }
                                    //--------------------------
                                    // if (pdfblob != null){
                                    //     pdftemp.push(pdfblob);
                                    // }
                                    // if (pdfFile != null){
                                    //     pdftemp.push(pdfFile);
                                    // }
                                    // if (pdftemp.length > 0)
                                    //     {
                                    //         printJS(pdftemp);
                                    //     }
                                    // else
                                    //     {
                                    //         console.log("error cetakan", pdfFile);
                                    //     }

                                })
                                .error(function(res) {
                                    console.log("error cetakan", pdfFile);
                                });
                                
                            // $scope.PrintFile($scope.cetakanUrlSub3 + res.data.Result[0].ProformaInvoiceId);
                            // var pdfFile2 = null;
                            // PrintRpt.print($scope.cetakanUrlSub3 + res.data.Result[0].ProformaInvoiceId + '&SPFBit=' + $scope.SPFBit + '&SPTBit=' + $scope.SPTBit + '&SPPBBit=' + $scope.SPPBBit).success(function (res) {
                            //     var file = new Blob([res], {type: 'application/pdf'});
                            //     var fileURL = URL.createObjectURL(file); 
                                        
                            //     pdfFile2 = fileURL;

                            //     if(pdfFile2 != null)
                            //     {
                            //         printJS(pdfFile2);
                            //     }	
                            //     else
                            //     {
                            //         bsNotify.show(
                            //             {
                            //                 title: "gagal",
                            //                 content: "Data gagal di print.",
                            //                 type: 'danger'
                            //             }
                            //         );
                            //     }	
                            // })
                            // .error(function (res) {
                            //     bsNotify.show(
                            //         {
                            //             title: "gagal",
                            //             content: "Terjadi error pada cetakan.",
                            //             type: 'danger'
                            //         }
                            //     ); 
                            // });

                            // if($scope.cetakanSuratPermohonanFaktur != null){    //If tambahan
                            //     $scope.PrintFunction($scope.cetakanSuratPermohonanFaktur + res.data.Result[0].ProformaInvoiceId);                                
                            // }
                            // if($scope.cetakanSuratPermohonanTransfer != null){
                            //     $scope.PrintFunction($scope.cetakanSuratPermohonanTransfer + res.data.Result[0].ProformaInvoiceId);                                
                            // }
                            // if($scope.cetakanSuratPernyataanPenyerahanBPKB != null){
                            //     $scope.PrintFunction($scope.cetakanSuratPernyataanPenyerahanBPKB + res.data.Result[0].ProformaInvoiceId);                                
                            // }
                            $scope.cetakanUrl = null;
                            $scope.cetakanUrlSub3 = null;
                            // $scope.cetakanSuratPermohonanFaktur = null;
                            // $scope.cetakanSuratPermohonanTransfer = null;
                            // $scope.cetakanSuratPernyataanPenyerahanBPKB = null;
                            $scope.opdstatus = false;
                            $scope.filter = {};
                            $scope.Search = {};
                            $scope.selctedRow = [];
							var Da_TodayDate=new Date();
							var da_awal_tanggal1 = new Date(Da_TodayDate.getFullYear(), Da_TodayDate.getMonth(), 1);
							$scope.filter.PIDateStart=da_awal_tanggal1;
							$scope.filter.PIDateEnd=Da_TodayDate;
                            $scope.getData();
                        },
                        function(err) {
                            console.log("err=>", err);
                            bsNotify.show({
                                title: "Gagal",
                                content: err.data.Message,
                                type: 'danger'
                            });
                            $scope.disabledSimpanPI = false;
                        }
                    );
                }
                else
                {
                    bsNotify.show(
                            {
                                title: "Peringatan",
                                content: "Tolong input nominal tagihan.",
                                type: 'warning'
                            }
                        );
                        $scope.disabledSimpanPI = false;
                }              
            });
        }
        // end norman

        $scope.btnKembaliFormMaintain = function() {
            $scope.createForm = false;
            $scope.MaintainForm = true;
            $scope.opdstatus = false;
            $scope.Search = {};
            //$scope.filter = {};
        }
		
		$scope.MaintainPI_BillingMulaiSampaiChanged = function() {
            try 
			{
				if($scope.Search.BillingDateStart>$scope.Search.BillingDateEnd)
				{
					$scope.Search.BillingDateEnd=$scope.Search.BillingDateStart;
				}
            } 
			catch (e1) 
			{

            }
        }
		
		$scope.MaintainPI_KuitansiMulaiSampaiChanged = function() {
            try 
			{
				if($scope.filter.PIDateStart>$scope.filter.PIDateEnd)
				{
					$scope.filter.PIDateEnd=$scope.filter.PIDateStart;
				}
            } 
			catch (e1) 
			{

            }
        }

        $scope.CetakPIfromGrid = function(PI) {
            MaintainPIFactory.getMasterCetakan().then(
                function (res) 
                {
                    $scope.cetakTemp = res.data.Result;
                    //$scope.cetakTemp.data = res.data.Result;
                    console.log('Temp =', $scope.cetakTemp)
                    //console.log('Temp2 =', $scope.cetakTemp.data)
                
            //console.log("tst", PI);
            if (PI.ProformaInvoiceStatusName == "Printed" || PI.ProformaInvoiceStatusName == "Created"){
                angular.element('.ui.small.modal.RequestPrint').modal('show');
                $scope.PIcetak = angular.copy(PI); 
            }else if(PI.ProformaInvoiceStatusName == "Printing Requested"){
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Kuitansi penagihan dalam pengajuan approval.",
                        type: 'warning'
                    }
                );
            }else if(PI.ProformaInvoiceStatusName == "Printing Approved"){
                if (PI.ProformaInvoiceCategoryId == 1 && PI.BillingTypeId == 1) {
                    $scope.cetakanUrl = 'sales/ProformaInvoiceFinishUnitDP?ProformaInvoiceId=';
                } else if (PI.ProformaInvoiceCategoryId == 1 && PI.BillingTypeId == 2) {
                    $scope.cetakanUrl = 'sales/ProformaInvoiceOPDDP?ProformaInvoiceId=';
                } else if (PI.ProformaInvoiceCategoryId == 1 && PI.BillingTypeId == 3) {
                    $scope.cetakanUrl = 'sales/ProformaInvoicePurnaJualDP?ProformaInvoiceId=';
                } else if (PI.ProformaInvoiceCategoryId == 2 && PI.BillingTypeId == 1) {
                    $scope.cetakanUrl = 'sales/ProformaInvoiceFinishUnitFP?ProformaInvoiceId=';
                } else if (PI.ProformaInvoiceCategoryId == 2 && PI.BillingTypeId == 2) {
                    $scope.cetakanUrl = 'sales/ProformaInvoiceOPDFP?ProformaInvoiceId=';
                } else if (PI.ProformaInvoiceCategoryId == 2 && PI.BillingTypeId == 3) {
                    $scope.cetakanUrl = 'sales/ProformaInvoicePurnaJualFP?ProformaInvoiceId=';
                }

                $scope.SPFBit = res.data.Result[0].SPFBit;
                $scope.SPTBit = res.data.Result[0].SPTBit;
                $scope.SPPBBit = res.data.Result[0].SPPBBit;
                if(PI.ProformaInvoiceCategoryId == 2 && PI.InvoiceTargetId == 2 && ($scope.SPFBit == 1 || $scope.SPTBit == 1 || $scope.SPPBBit == 1)){
                    $scope.cetakanUrlSub3 = 'sales/ProformaInvoiceSub3?ProformaInvoiceId=';
                }
                //$scope.PrintFunction($scope.cetakanUrl + PI.ProformaInvoiceId);
                if($scope.cetakanUrlSub3 != null){
                    var pdfblob = null;
                    PrintRpt.print($scope.cetakanUrlSub3 + PI.ProformaInvoiceId + '&SPFBit=' + $scope.SPFBit + '&SPTBit=' + $scope.SPTBit + '&SPPBBit=' + $scope.SPPBBit).success(function (res) {
                        var contentType = 'application/pdf';
                        var blob = new Blob([res], { type: contentType });
                        
                        console.log("Res", res);
    
                        saveAs(blob, 'ReportProformaInvoiceSubb3.pdf');
                       
                    })
                }
               
                var pdfFile = null;
                PrintRpt.print($scope.cetakanUrl + PI.ProformaInvoiceId).success(function(res) {
                    var file = new Blob([res], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);

                    console.log("pdf", fileURL);
                    //$scope.content = $sce.trustAsResourceUrl(fileURL);
                    pdfFile = fileURL;

                    if (pdfFile != null)
                        {
                            printJS(pdfFile);
                            $scope.getData();
                        }
                    else
                        {
                            console.log("error cetakan", pdfFile);
                        }
                })
                .error(function(res) {
                    console.log("error cetakan", pdfFile);
                });
                //$scope.getData();
            }
        })
        }

        $scope.ajuReprint = function(){
            // bsNotify.show(
            //     {
            //         title: "Peringatan",
            //         content: "Under Construction."+ $scope.PIcetak,
            //         type: 'warning'
            //     }
            // );
///////////////////// INI CR Jangan dihapus !!!!!!!!!!!!!!!!!!!!!!///////////////
            MaintainPIFactory.rePrint($scope.PIcetak).then(function(){
                angular.element('.ui.small.modal.RequestPrint').modal('hide');
                    bsNotify.show(
                        {
                            title: "Berhasil",
                            content: "Kuitansi berhasil diajukan untuk cetak ulang.",
                            type: 'success'
                        }
                    );
                $scope.getData();
            })
        }

        $scope.keluarModalPrint = function(){
            angular.element('.ui.modal.RequestPrint').modal('hide');
        }

        $scope.PINOClick = function(SelectPO) {
            var tempNOPI = SelectPO.ProformaInvoiceId;
            MaintainPIFactory.getNOPI('PIID=' + tempNOPI).then(
                function(res) {
                    $scope.DetailPI = res.data.Result[0];
                    $scope.CategoryPI = res.data.Result[0].ProformaInvoiceCategoryId;
                    $scope.gridPIDetail.data = res.data.Result[0].ListOfPIDetail;
                    $scope.gridPIDetailDP.data = res.data.Result[0].ListOfPIDetail;
                    if($scope.CategoryPI == 1){
                        setTimeout(function() {
                            angular.element('.ui.modal.ModalLihatPIDP').modal('refresh');
                        }, 0);
                        angular.element('.ui.modal.ModalLihatPIDP').modal('show');
                    } else {
                        setTimeout(function() {
                            angular.element('.ui.modal.ModalLihatPI').modal('refresh');
                        }, 0);
                        angular.element('.ui.modal.ModalLihatPI').modal('show');
                    }
                },
                function(err) {
                    console.log("err=>", err);
                    // bsNotify.show(
                    //     {
                    //         title: "Error Message",
                    //         content: 'Simpan Data PI Gagal',
                    //         type: 'danger'
                    //     }
                    // );
                }
            );

        }

        $scope.CloseDetailPIDP = function() {
            angular.element('.ui.modal.ModalLihatPIDP').modal('hide');
        }

        $scope.CloseDetailPI = function() {
            angular.element('.ui.modal.ModalLihatPI').modal('hide');
        }

        $scope.btnUbahPO = function(SelectPO) {
            $scope.BatalPI = {};
            $scope.PISelected = {};
            $scope.PISelected = SelectPO;

            setTimeout(function() {
                angular.element('.ui.small.modal.BatalPI').modal('refresh');
            }, 0);
            angular.element('.ui.small.modal.BatalPI').modal('show');
        }

        $scope.Batal_Clicked = function() {
            angular.element('.ui.small.modal.BatalPI').modal('hide');
        }

        $scope.Simpan_Clicked = function() {
            $scope.DataCancel = {};
            $scope.DataCancel.ProformaInvoiceId = $scope.PISelected.ProformaInvoiceId;
            $scope.DataCancel.CancelReason = $scope.BatalPI.Keterangan;
            MaintainPIFactory.cancelPI($scope.DataCancel).then(
                function(res) {
                    //$scope.gridPI.data = res.data.Result;
                    //$scope.loading = false;
                    //return res.data.Result;
					MaintainPIFactory.getData($scope.filter).then(
						function(res) {
							$scope.grid.data = res.data.Result;
							$scope.loading = false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil di cancel",
									type: 'success'
								}
							);
						},
						function(err) {
							console.log("err=>", err);
						}
					);
                    angular.element('.ui.small.modal.BatalPI').modal('hide');
                },
                function(err) {
                    var errMsg = "";
                      if(err.data.Message.split('#').length > 1){
                          errMsg = err.data.Message.split('#')[1];
                      }
                    console.log("err=>", err);
                    bsNotify.show({
                        title: "Gagal",
                        content: errMsg,
                        type: 'danger'
                    });
                }
            );
        }

        $scope.filterType = function(Selected) {
            $scope.typeSelected = Selected;
            if ($scope.typeSelected.POTypeId == 1 || $scope.typeSelected.POTypeId == 2 || $scope.typeSelected.POTypeId == 6) {
                $scope.showGridmaterial = true;
                $scope.showGridbirojasa = false;
            } else if ($scope.typeSelected.POTypeId == 3) {
                $scope.showGridmaterial = false;
                $scope.showGridbirojasa = true;
            } else if ($scope.typeSelected.POTypeId == 5) {
                $scope.noDocShow = true;
            } else {
                $scope.showGridmaterial = false;
                $scope.showGridbirojasa = false;
            }
        }

        MaintainPIFactory.getStatusPI().then(
            function(res) {
                $scope.getStatusPI = res.data.Result;

            },
            function(err) {
                console.log("err=>", err);
                // bsNotify.show(
                //     {
                //         title: "Error Message",
                //         content: 'Simpan Data PI Gagal',
                //         type: 'danger'
                //     }
                // );
            }
        );

        $scope.btnbuatPI = function() {
            $scope.PIDate.minDate = new Date();
            $scope.filter.DatePI = new Date();
            $scope.DataPI = {};
            MaintainPIFactory.getPITarget().then(
                function(res) {
                    for(var i in res.data.Result){
                        if (res.data.Result[i].InvoiceTargetName == "Nama di Billing"){
                            res.data.Result.splice(i,1);
                        }
                    }
                    $scope.getProformaInvoiceTarget = res.data.Result;
                    $scope.gridPI.data = [];
                    $scope.createForm = true;
                    $scope.MaintainForm = false;
                },
                function(err) {
                    console.log("err=>", err);
                    // bsNotify.show(
                    //     {
                    //         title: "Error Message",
                    //         content: 'Simpan Data PI Gagal',
                    //         type: 'danger'
                    //     }
                    // );
                }
            );
            $scope.filter.withAccBit = false;
            $scope.filter.typeId = null;
        }

        $scope.btnCetak = function() {
            $scope.CreateForm = false;
            $scope.MaintainForm = false;
            $scope.PrintForm = true;
        }

        $scope.btnTutupdokumen = function() {
            $scope.CreateForm = false;
            $scope.PrintForm = false;
            $scope.MaintainForm = true;
        }

        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.cte = function(item) {
            $scope.mMaintainPO = item;
            $scope.show_modal.show = !$scope.show_modal.show;
        }

        $scope.onListCancel = function(item) {
            $scope.show_modal = { show: false };
        }
        
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }
        

        var ClickNoPI = '<a style="color:blue;"  ng-click="grid.appScope.$parent.PINOClick(row.entity)"><p style="padding:5px 0 0 5px"><u>{{row.entity.ProformaInvoiceCode}}</u></p></a>';
        var actionClick= '<a ng-if="row.entity.ProformaInvoiceStatusName !=\'Cancel\' && row.entity.ProformaInvoiceCancelStatusName !=\'Diajukan\'" style="color:#777;" class="trlink ng-scope" uib-tooltip="Cetak" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.CetakPIfromGrid(row.entity)" tabindex="0">' +
        '<i class="fa fa-fw fa-lg fa fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>' +
        '<a ng-hide="row.entity.ProformaInvoiceStatusName == \'Cancel\' || row.entity.ProformaInvoiceCancelStatusName == \'Diajukan\' || row.entity.ProformaInvoiceStatusName == \'Printing Requested\'" style="color:#777;" class="trlink ng-scope" uib-tooltip="Batal Kuitansi" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.btnUbahPO(row.entity)" tabindex="0">' +
        '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a>'
        var HapusClick = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.$parent.clickhapusrowgrid(row.entity)">Hapus</u></span>'

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            //multiSelect: true,
            //enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Nama Pelanggan', field: 'CustomerName' },
                {
                    name: 'No Kuitansi',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: ClickNoPI
                },
                { name: 'Tanggal Kuitansi', cellFilter: 'date:\'dd-MM-yyyy\'', field: 'ProformaInvoiceDate', width: '15%' },
                { name: 'Status Kuitansi', field: 'ProformaInvoiceStatusName' },
                { name: 'Cancel Status', field: 'ProformaInvoiceCancelStatusName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    width: '10%',
                    enableColumnResizing: true,
                    cellTemplate: actionClick
                }
            ]
        };
        
        $scope.gridPI = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            showColumnFooter: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
			enableColumnResizing: true,
            columnDefs: [
                { name: 'Nama Pelanggan', field: 'CustomerName', width: '20%', enableCellEdit: false },
                { name: 'No Billing', field: 'BillingCode', width: '16%', enableCellEdit: false },
                { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false },
                { name: 'Yang Pernah Ditagihkan', field: 'TotalYgPernahDitagih', cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                { name: 'Sisa Tagihan', field: 'SisaTagihan',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                { name: 'Outstanding AR', field: 'OutStandingAR',cellFilter: 'currency:"Rp." : 0',cellClass: 'text-right', enableCellEdit: false },
                {
                    name: 'Nominal Tagihan *',
                    field: 'Invoice',
                    enableCellEdit: true,
                    cellFilter: 'currency:"Rp." : 0',
                    type: 'number',
                    cellClass: 'text-right',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerCellFilter: 'currency:"Rp."',
                    aggregationHideLabel: true,
                    // customtreeAggregationFinalizerFn: function(aggregation) {
                    //     aggregation.rendered = 'TotalTagihan : ' + aggregation.value
                    // }
                },
            ]
        };

        $scope.gridPIDetailDP = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            showColumnFooter: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'No.', width:'7%', cellTemplate: '<div class="ui-grid-cell-contents">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>' },
                { name: 'No SO ', field: 'SOCode', enableCellEdit: false },
                { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false, footerlClass: 'text-right',
                    footerCellFilter: "Total Tagihan :"  },
                {
                    name: 'Nominal Tagihan',
                    field: 'Invoice',
                    enableCellEdit: false,
                    cellFilter: 'currency:"Rp." : 0',
                    type: 'number',
                    width:'23%',
                    cellClass: 'text-right',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerlClass: 'text-right',
                    footerCellFilter: 'currency:"Total Tagihan : Rp." : 0 ',
                    aggregationHideLabel: true,
                    // customTreeAggregationFinalizerFn: function(aggregation) {
                    //     aggregation.rendered = ' ' + aggregation.value
                    // }
                },
            ]
        };

        $scope.gridPIDetail = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            showColumnFooter: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10,25,50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'No.', width:'7%', cellTemplate: '<div class="ui-grid-cell-contents">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>' },
                { name: 'Billing No  ', field: 'BillingCode', enableCellEdit: false },
                { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false, footerlClass: 'text-right',
                    footerCellFilter: "Total Tagihan :"  },
                {
                    name: 'Nominal Tagihan',
                    field: 'Invoice',
                    enableCellEdit: false,
                    cellFilter: 'currency:"Rp." : 0',
                    type: 'number',
                    width:'23%',
                    cellClass: 'text-right',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerlClass: 'text-right',
                    footerCellFilter: 'currency:"Total Tagihan : Rp." : 0 ',
                    aggregationHideLabel: true,
                    // customTreeAggregationFinalizerFn: function(aggregation) {
                    //     aggregation.rendered = ' ' + aggregation.value
                    // }
                },
            ]
        };

        $scope.PrintFunction = function(url) {
            var pdfFile = null;
            PrintRpt.print(url).success(function(res) {
                    var file = new Blob([res], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);

                    console.log("pdf", fileURL);
                    //$scope.content = $sce.trustAsResourceUrl(fileURL);
                    pdfFile = fileURL;

                    if (pdfFile != null)
                        printJS(pdfFile);
                    else
                        console.log("error cetakan", pdfFile);
                })
                .error(function(res) {
                    console.log("error cetakan", pdfFile);
                });
        };

        //Percobaan Print
        // $scope.PrintFile = function() {
        //     var pdfFile = null;
        //     PrintRpt.print("sales/ProformaInvoiceSub3?ProformaInvoiceId="+$scope.ProformaInvoiceId).success(function(res) {
        //             var file = new Blob([res], { type: 'application/pdf' });
        //             var fileURL = URL.createObjectURL(file);

        //             console.log("pdf", fileURL);
        //             //$scope.content = $sce.trustAsResourceUrl(fileURL);
        //             pdfFile = fileURL;

        //             if (pdfFile != null)
        //                 printJS(pdfFile);
        //             else
        //                 console.log("error cetakan", pdfFile);
        //         })
        //         .error(function(res) {
        //             console.log("error cetakan", pdfFile);
        //         });
        // };

        $scope.Tagihan = {};
        $scope.Tagihan.TotalTagihan = 0;
        $scope.gridPI.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {

                $scope.selctedRow = gridApi.selection.getSelectedRows();
                for (var i in $scope.selctedRow) {
                    $scope.Tagihan.TotalTagihan = $scope.Tagihan.TotalTagihan + parseInt($scope.selctedRow[i].NominalTagihan);

                }
            });

            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });

            gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                console.log("test", rowEntity);
                if (newValue<0) {
                    newValue = 0;
                    rowEntity[colDef.field] = newValue;   
                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: "Nilai Nominal Tagihan tidak boleh kurang dari 0.",
                            type: 'warning'
                        }
                    );           
                }else if(newValue>rowEntity.SisaTagihan){
                    newValue = 0;
                    rowEntity[colDef.field] = newValue;   
                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: "Nilai Nominal Tagihan tidak boleh lebih dari sisa tagihan.",
                            type: 'warning'
                        }
                    );      
                }  
                 
            });
        };
    });