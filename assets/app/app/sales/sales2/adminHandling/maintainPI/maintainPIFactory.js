angular.module('app')
    .factory('MaintainPIFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(filter) {
                for (key in filter) {
                    if (filter[key] == null || filter[key] == undefined)
                        delete filter[key];
                }
                var res = $http.get('/api/sales/AdminHandlingProformaInvoice/?start=1&limit=1000&' + $httpParamSerializer(filter));
                return res;
            },

            getNoSPK: function(filter) {
                console.log("==>" , filter)
                for (key in filter) {
                    if (filter[key] == null || filter[key] == undefined)
                        delete filter[key];
                }
                var NoSPK = $http.get('/api/sales/AdminHandlingProformaInvoice/GetBilling/?start=1&limit=1000&' + $httpParamSerializer(filter));
                return NoSPK;
            },

            getTypePO: function() {
                var tipe = $http.get('/api/sales/AdminHandlingPOType');
                return tipe;
            },

            getBilling: function() {
                var billing = $http.get('/api/sales/AdminHandlingBillingType');
                return billing;
            },

            getStatusPI: function() {
                var StatusPI = $http.get('/api/sales/AdminHandlingProformaInvoiceStatus');
                return StatusPI;
            },

            getPITarget: function() {
                var PITarget = $http.get('/api/sales/AdminHandlingProformaInvoiceTarget');
                return PITarget;
            },

            getStatusPO: function() {
                var status = $http.get('/api/sales/AdminHandlingPOStatus');
                return status;
            },

            getRequestPO: function() {
                var status = $http.get('/api/sales/AdminHandlingPOPRList');
                return status;
            },

            getPaymentType: function() {
                var res = $http.get('/api/sales/FFinancePaymentType');
                return res;
            },

            getNoRangka: function() {
                var res = $http.get('/api/sales/AdminHandlingPOInternalVehicle');
                return res;
            },

            getMaterialInternal: function(param) {
                var res = $http.get('/api/sales/AdminHandlingPOInternalDetailAccessories' + param);
                return res;
            },

            getNOPI: function(param) {
                var res = $http.get('/api/sales/AdminHandlingProformaInvoice/Detail?' + param);
                return res;
            },

            getMasterCetakan: function() {
                var res = $http.get('/api/sales/AdminHandlingProformaInvoice/GetCheckPenagihanLeasing');
                return res;
            },

            create: function(maintainPO) {
                return $http.post('/api/sales/AdminHandlingPOMaintenance', [maintainPO]);
            },

            savePI: function(DataPI) {
                var res = $http.post('/api/sales/AdminHandlingProformaInvoice', [DataPI]);
                return res;
            },

            cancelPI: function(DataPI) {
                return $http.put('/api/sales/AdminHandlingProformaInvoice/Cancel', [DataPI]);
            },

            update: function(maintainPO) {
                return $http.put('/api/sales/AdminHandlingPOMaintenance', [maintainPO]);
            },

            closePO: function(param) {
                return $http.put('/api/sales/AdminHandlingPOMaintenance/Close' + param);
            },

            rePrint: function(DataPI){
                return $http.put('/api/sales/AdminHandlingProformaInvoice/RequestPrint', [{
                    ProformaInvoiceId: DataPI.ProformaInvoiceId,
                    Note: DataPI.Note
                }]);
            },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });