angular.module('app')
.controller('BuatBillingDokumenController', function($scope, $http, CurrentUser, BuatBillingDokumenFactory,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];

    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mDokumen = null; //Model
    $scope.xRole={selected:[]};

	//----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        BuatBillingDokumenFactory.getData()
        .then(
            function(res){
                gridData = [];
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
				
				var x = [
					{
						"No":1,
                        "TypeDokumen":"TDP",
                        "Dokumen":"View",
                        "Status":"Upload"
						
					},
					
					{
						"No":2,
                        "TypeDokumen":"KTP",
                        "Dokumen":"Browse",
                        "Status":"Empty"
						
					},
					{
						"No":3,
                        "TypeDokumen":"NPWP",
                        "Dokumen":"View",
                        "Status":"Upload"
						
					}
				];
	
				
                //$scope.grid.data = res.data.Result;
				$scope.grid.data = x;
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
   
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
	
	var btnAction = '<a style="color:blue;"  ng-click=""><p style="padding:5px 0 0 5px" onclick="BatalSO()";><u>{{row.entity.Dokumen}}</u></p></a>';
    var btnHapus = '<a style="color:blue;"  ng-click=""><p style="padding:5px 0 0 5px" onclick="";><u>Bersihkan</u></p></a>';

	 //----------------------------------
    // Grid1 Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'#', field:'No', width:'4%'},
            { name:'Type Dokumen', field:'TypeDokumen'},
            { 
            		name:'Dokumen',  
            		allowCellFocus: false,
                    width: '15%',                    
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnAction
            },
            { name:'Status', field: 'Status'},
            { 
                    name:'Action', allowCellFocus: false,
                    width: '15%',                    
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnHapus
            }
        ]
    };		
});
