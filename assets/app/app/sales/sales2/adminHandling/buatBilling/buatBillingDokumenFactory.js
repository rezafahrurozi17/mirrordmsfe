angular.module('app')
  .factory('BuatBillingDokumenFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(lihatSODokumenFactory) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: lihatSODokumenFactory.SalesProgramName}]);
      },
      update: function(lihatSODokumenFactory){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: lihatSODokumenFactory.SalesProgramId,
                                            SalesProgramName: lihatSODokumenFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });