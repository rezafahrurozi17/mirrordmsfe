angular.module('app')
.controller('BuatBillingRincianHargaController', function($scope, $http, CurrentUser, BuatBillingRincianHargaFactory,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];

    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mBuatBillingRincianHarga = null; //Model    

	//----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        BuatBillingRincianHargaFactory.getData()
        .then(
            function(res){
                gridData = [];
				
				var x = [
					{
						"TypeModel":1,
                        "Keterangan":"Diskon",
                        "Currency":"USD",
                        "Nominal":"250000"
						
					},
					
					{
						"TypeModel":2,
                        "Keterangan":"Diskon",
                        "Currency":"USD",
                        "Nominal":"100000"
						
					},
					{
						"TypeModel":3,
                        "Keterangan":"Diskon",
                        "Currency":"IDR",
                        "Nominal":"1520000000"
						
					},
				];
	
				
                //$scope.grid.data = res.data.Result;
				$scope.grid.data = x;
                
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
   
   
	
	 //----------------------------------
    // Grid1 Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Type Model', field:'TypeModel'},
            { name:'Keterangan', field:'Keterangan'},
            { name:'Currency', field:'Currency'},
			{ name:'Nominal', field:'Nominal'}
			
        ]
    };
		
});

