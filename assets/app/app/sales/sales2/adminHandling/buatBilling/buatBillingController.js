angular.module('app')
.controller('BuatBillingController', function($scope, $http, CurrentUser, BuatBillingFactory,$timeout) {

	var listData = [{
                Id: "1",
                Name: "Individu"
            },
            {
                Id: "2",
                Name: "Perusahaan"
            }

        ];

    $scope.selectionItem = listData;

	var listType = [{
				BillingTypeId: "1",
				BillingType: "Finish unit"
            },
            {
				BillingTypeId: "2",
				BillingType: "OPD"
            },
            {
				BillingTypeId: "3",
				BillingType: "Purna Jual"
            }

        ];

    $scope.selectionBillingType = listType;

 	$scope.mKategori = $scope.selectionItem[0];
	
	$scope.KategoryPelanggan = function() {
		if ($scope.mKategori == 1) {
			kategoriIndividu();
		} 
		else if ($scope.mKategori == 2) {
			kategoriPerusahaan();
		}
	};

	$scope.SalesOrderType = function() { 
		if ($scope.mSOType == 1) {
			TabFinishUnit();
		} 
		else if ($scope.mSOType == 2) {
			TabOPD();
		}
		else if ($scope.mSOType == 3) {
			TabPurnaJual();
		}
	};

	$scope.clickBillingType = function() { 
		if ($scope.mBillingType == 1) {
			TabFinishUnit();
		} 
		else if ($scope.mBillingType == 2) {
			TabOPD();
		}
		else if ($scope.mBillingType == 3) {
			TabPurnaJual();
		}
	};

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mCreateSO = null; //Model
    $scope.xRole={selected:[]};

	//----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        BuatBillingFactory.getData()
        .then(
            function(res){
                gridData = [];
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
				
				var x = [
					{
						"IUtemId":1,
                        "KodeJasa":"J06A",
                        "NamaJasa":"Jasa Pengurusan STNK",
                        "Qty":"2"
						
					},
					
					{
						"IUtemId":2,
                        "KodeJasa":"J04A",
                        "NamaJasa":"Jasa Pengurusan STNK",
                        "Qty":"1"
						
					},
					{
						"IUtemId":3,
                        "KodeJasa":"J07A",
                        "NamaJasa":"Jasa Pengurusan STNK",
                        "Qty":"12"
						
					},
				];
	
				
                //$scope.grid.data = res.data.Result;
				$scope.grid.data = x;
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
   
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
	
	//----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Item No.',    field:'IUtemId'},
            { name:'Kode Jasa', field:'KodeJasa' },
			{ name:'Nama Jasa',  field: 'NamaJasa' },
			{ name:'Qty',  field: 'Qty' }
        ]
    };

 	
	$scope.intended_operation="None";
	$scope.LookSelected = function (CreateSOFactory) {

	   $scope.mProfileSalesProgram_SalesProgramName=SalesProgramName;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.ChangeSelected = function (SOId,SalesProgramName) {
	   $scope.intended_operation="Update_Ops";
	   $scope.mProfileSalesProgram_SalesProgramName=SalesProgramName;
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Batal_Button = true;
	   $scope.Submit_Button = true;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Batal_Clicked = function () {

	   $scope.mProfileSalesProgram_SalesProgramName=null;
	   
	   $scope.intended_operation="None";
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Add_Clicked = function () {
			
		   $scope.mProfileSalesProgram_SalesProgramName=null;
		   $scope.intended_operation="Insert_Ops";
		   
		   $scope.ShowParameter = !$scope.ShowParameter;
		   $scope.ShowTable = !$scope.ShowTable;
		   $scope.Submit_Button = true;
		   $scope.Batal_Button = true;
		   $scope.Add_Button = !$scope.Add_Button;
	};

	$scope.Simpan_Clicked = function () {
		//put httppost,delete,cll here
		if($scope.intended_operation=="Update_Ops")
		{
			alert("update");
		}
		else if($scope.intended_operation="Insert_Ops")
		{
			alert("insert");
		}
		
		//clear all input stuff first 
	   $scope.mProfileSalesProgram_SalesProgramName=null;
	   
	   $scope.ShowParameter = !$scope.ShowParameter;
	   $scope.ShowTable = !$scope.ShowTable;
	   $scope.Submit_Button = false;
	   $scope.Batal_Button = false;
	   $scope.Add_Button = !$scope.Add_Button;
	   $scope.intended_operation="None";
	};
		
});

