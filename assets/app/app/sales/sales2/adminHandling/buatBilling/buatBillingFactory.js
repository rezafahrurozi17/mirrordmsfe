angular.module('app')
  .factory('BuatBillingFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(lihatSOFactory) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: lihatSOFactory.SalesProgramName}]);
      },
      update: function(lihatSOFactory){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: lihatSOFactory.SalesProgramId,
                                            SalesProgramName: lihatSOFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });