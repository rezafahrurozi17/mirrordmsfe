angular.module('app')
  .factory('BuatBillingRincianHargaFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(lihatSORincianHargaFactory) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: lihatSORincianHargaFactory.SalesProgramName}]);
      },
      update: function(lihatSORincianHargaFactory){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: lihatSORincianHargaFactory.SalesProgramId,
                                            SalesProgramName: lihatSORincianHargaFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });