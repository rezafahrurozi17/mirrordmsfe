angular.module('app')
.controller('GoodReceiptController', function($scope,$filter, $http, bsNotify, CurrentUser, GoodReceiptFactory,uiGridConstants, MaintainPOFactory, $timeout, MobileHandling) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.MaintainForm = true;
    $scope.CreateviewForm = false;
    $scope.show_modal={show:false};
    $scope.modalMode='new';
    $scope.btnbatalGR = true;
    $scope.flagStatus = "SIMPAN";
	

    $scope.modal_model = [];
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
        //----------Begin Check Display---------
            
              $scope.bv=MobileHandling.browserVer();
              $scope.bver=MobileHandling.getBrowserVer();
              //console.log("browserVer=>",$scope.bv);
              //console.log("browVersion=>",$scope.bver);
              //console.log("tab: ",$scope.tab);
              if($scope.bv!=="Chrome"){
              if ($scope.bv=="Unknown") {
                if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
                {
                  $scope.removeTab($scope.tab.sref);
                }
              }        
              }
            
            //----------End Check Display-------
    });

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mGoodReceipt = null; //Model
    $scope.xRole = {selected:[]};
    $scope.filter = {POTypeId:null, GRStatus:null, GRDateStart:null, GRDateEnd:null};
    
    $scope.disabledGrid = true; //fitri
    $scope.disabledSimpanGRBtn = false;
    $scope.disabledSimpanModalGoodReceipt_AlasanBatalGR = false;
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalGoodReceipt_AlasanBatalGR').remove();
		  
		});
	
    $scope.dateOptionsStart = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    };

    $scope.dateOptionsEnd = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1,
    }

    

    //$scope.ViewGR = {};
   

    $scope.tanggalmin = function (dt){
        $scope.dateOptionsEnd.minDate = dt;
    }
	
	$scope.filter.GRDateEnd=new Date();
	var tampung_tanggal_buat_awal=new Date();
	$scope.filter.GRDateStart=tampung_tanggal_buat_awal.setDate(1);

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        alerts=[];
        $scope.gridData = [];
        if ($scope.filter.POTypeId == null || $scope.filter.GRDateStart == null || $scope.filter.GRDateEnd == null ) {
            //tadi ini juga dijagain diganti tgl 6 maret || $scope.filter.GRStatus == null
			bsNotify.show({
                size: 'big',
                type: 'succes',
                timeout: 3000,
                color: "#c00",
                title: "Mohon input filter Mandatory",
            });
        } 
        else{
            $scope.filter.GRDateStart=new Date($scope.filter.GRDateStart);
			console.log("getfilter", $scope.filter);
            GoodReceiptFactory.getData($scope.filter).then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    $scope.loading=false;
                    return res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }
    }

    GoodReceiptFactory.getDataStatusGR().then(
        function(res){
            $scope.getDataStatusGR = res.data.Result;
			//pas 28 feb andri suruh balikin lagi wong edan
			//$scope.filter.GRStatus=$scope.getDataStatusGR[0].GRStatusId;
            return res.data;
        }
    );

    GoodReceiptFactory.getDataCancelGR().then(
        function(res){
            $scope.getDataCancelGR = res.data.Result;
            return res.data;
        }
    );

    MaintainPOFactory.getTypePO().then(
        function(tipe){
            $scope.getTypePO = tipe.data.Result;
            $scope.getTypePO.splice(7,1);
            console.log("$scope.getTypePO 1===>",$scope.getTypePO);
			$scope.filter.POTypeId=$scope.getTypePO[0].POTypeId;

            $scope.getTypePOBuat = angular.copy(tipe.data.Result);
            for(var i in $scope.getTypePOBuat){
                if(($scope.getTypePOBuat[i].POTypeId == 6) ||
                    ($scope.getTypePOBuat[i].POTypeId == 7))
                {
                    $scope.getTypePOBuat.splice(i,1);
                }
            }

          //  return tipe.data;
        }
    );

    $scope.pilihType = function(selected){
        console.log('selected ===>', selected);
        $scope.tem = selected;
        $scope.ViewGR.VendorId = null;
        $scope.ViewGR.POId = null;
        $scope.DataVendor = [];
        $scope.DataPOCode = [];
        var getTypeId = "?PRTypeId=" + $scope.tem.POTypeId;
        console.log('getTypeId ===>', getTypeId);
        GoodReceiptFactory.getDataVendor(getTypeId).then(
            function(res){
                $scope.DataVendor = res.data.Result;
                return res.data;
            }
        );
    }
	


    $scope.pilihVendor = function(selected){
        $scope.Vend = selected;
        $scope.ViewGR.POId = null;
        $scope.DataPOCode = [];
        var getVendorId = "?POTypeId=" + $scope.tem.POTypeId + "&VendorId=" + $scope.Vend.VendorId;
        GoodReceiptFactory.getPOCode(getVendorId).then(
            function(res){
                $scope.DataPOCode = res.data.Result;
                return res.data;
            }
        );
    }

    $scope.btnbuatGR = function(){
        var Da_TodayDate = new Date();
        $scope.flagStatus = "CreateGR";
        $scope.ViewGR = [];
        $scope.DataVendor = [];
        $scope.DataPOCode = [];
        $scope.ViewGR.ReceivedDate = Da_TodayDate;
		
		var Da_Waktu_Sekarang=$filter('date')(new Date(), 'HH:mm');
			var RawSekarangTime_Get = Da_Waktu_Sekarang.split(':');
				$scope.ProcessedSekarangTime_Get = {    
				 value: new Date(Da_TodayDate.getFullYear(), Da_TodayDate.getMonth(), Da_TodayDate.getDate(), RawSekarangTime_Get[0], RawSekarangTime_Get[1])
				 };
		console.log("setan1",$scope.ProcessedSekarangTime_Get);
		console.log("setan2",Da_TodayDate);
		
		$scope.ViewGR.ReceivedTime = $scope.ProcessedSekarangTime_Get.value;
                
        $scope.gridCreateViewGoodReceipt.data = [];
        $scope.MaintainForm = false;
        $scope.TextboxLihatGR = false;
        $scope.CreateviewForm = true;
        $scope.SaveGR = true;
        $scope.lihatGR = true;
        $scope.DisabledTextViewGR = false;
        $scope.gridCreateViewGoodReceipt.columnDefs[7].enableCellEdit = true;
        $scope.gridCreateViewGoodReceipt.columnDefs[7].cellTooltip = 'Double click untuk mengisi kolom';
        //console.log(' $scope.ViewGR', $scope.ViewGR);
    }

    $scope.ChangeNumberPO = function(){
        
		if($scope.flagStatus == "CreateGR"){
            var getPO = "?POId=" + $scope.ViewGR.POId;
            GoodReceiptFactory.getDataPO(getPO).then(
                function(res){
                    $scope.getDataPO = res.data.Result;
                    console.log('$scope.getDataPO ===>', $scope.getDataPO);
                    if($scope.getDataPO[0].POReferenceId==1)
					{	
                        console.log('ini masuk yg ifff')
                        //ini aksesoris
						$scope.gridCreateViewGoodReceipt = {
							enableSorting: true,
							enableRowSelection: true,
							multiSelect: false,
							enableSelectAll: true,
							//showTreeExpandNoChildren: true,
							paginationPageSizes: [10, 25, 50],
							paginationPageSize: 10,
							//data: 'dataCreateViewGoodReceipt',
							columnDefs: [
								{ name:'GRId', field:'GRId', visible:false},
								{ name:'No. Item', field:'NO', width: '10%', cellTemplate: ' <span>{{rowRenderIndex+1}}</span>', visible:true},
								{ name:'No Rangka', field:'FrameNo', width: '15%' , enableCellEdit:false, visible:true}, 
								{ name:'No. Aksesoris', field:'AccessoriesCode', width: '15%', enableCellEdit:false, visible:true},          
                                { name:'Nama Aksesoris', field:'AccessoriesName', enableCellEdit:false, visible:true },
                                
								{ name:'No. Material', field:'MaterialCode', width: '15%', enableCellEdit:false, visible:false},          
                                { name:'Nama Material', field:'MaterialName', enableCellEdit:false, visible:false },		
                                	
								{ name:'Qty PO', field:'QtyPO', format: '{0:c}', type: 'number', enableCellEdit:false, visible:true},
								{ name:'Qty yang sudah diterima', format: '{0:c}', type: 'number', field:'QtyReceived', width: '20%', enableCellEdit:false, visible:true},
								{ name:'Qty yang diterima *', cellTooltip: 'Double click untuk mengisi kolom', field:'QtyReceive', format: '{0:c}', type:'number', max:99999, enableCellEdit:true, visible:true},
								{ name:'Satuan', field:'UomName', enableCellEdit:false , visible:true}    
							]
						};
						
						//$scope.gridApigridCreateViewGoodReceipt.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
						$scope.gridCreateViewGoodReceipt.data = $scope.getDataPO[0].ListOfPODetailPRView;
						for(var i = 0; i < $scope.gridCreateViewGoodReceipt.data.length; ++i)
						{	
							if($scope.gridCreateViewGoodReceipt.data[i].QtyReceived == null)
							{
								$scope.gridCreateViewGoodReceipt.data[i].QtyReceived =0;
							}
						}
						//$scope.gridApigridCreateViewGoodReceipt.grid.refresh();
					}
                    else
					{
                        console.log('ini masuk yg else')
						
						$scope.gridCreateViewGoodReceipt = {
							enableSorting: true,
							enableRowSelection: true,
							multiSelect: false,
							enableSelectAll: true,
							//showTreeExpandNoChildren: true,
							paginationPageSizes: [10, 25, 50],
							paginationPageSize: 10,
							//data: 'dataCreateViewGoodReceipt',
							columnDefs: [
								{ name:'GRId', field:'GRId', visible:false},
								{ name:'No. Item', field:'NO', width: '10%', cellTemplate: ' <span>{{rowRenderIndex+1}}</span>', visible:true},
                                { name:'No Rangka', field:'FrameNo', width: '15%' , enableCellEdit:false, visible:true}, 
                                

								{ name:'No. Aksesoris', field:'AccessoriesCode', width: '15%', enableCellEdit:false, visible:false},          
                                { name:'Nama Aksesoris', field:'AccessoriesName', enableCellEdit:false, visible:false },
                                

								{ name:'Kode Jasa', field:'MaterialCode', width: '15%', enableCellEdit:false, visible:true},          
                                { name:'Nama Jasa', field:'MaterialName', enableCellEdit:false, visible:true },			
                                

								{ name:'Qty PO', field:'QtyPO', format: '{0:c}', type: 'number', enableCellEdit:false, visible:true},
								{ name:'Qty yang sudah diterima', format: '{0:c}', type: 'number', field:'QtyReceived', width: '20%', enableCellEdit:false, visible:true},
								{ name:'Qty yang diterima *', cellTooltip: 'Double click untuk mengisi kolom', field:'QtyReceive', format: '{0:c}', type:'number', max:99999, enableCellEdit:true, visible:true},
								{ name:'Satuan', field:'UomName', enableCellEdit:false , visible:true}    
							]
						};
						//$scope.gridApigridCreateViewGoodReceipt.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
						$scope.gridCreateViewGoodReceipt.data = $scope.getDataPO[0].ListOfPODetailSummaryView;
						for(var i = 0; i < $scope.gridCreateViewGoodReceipt.data.length; ++i)
						{	
							if($scope.gridCreateViewGoodReceipt.data[i].QtyReceived == null)
							{
								$scope.gridCreateViewGoodReceipt.data[i].QtyReceived =0;
							}
						}
						//$scope.gridApigridCreateViewGoodReceipt.grid.refresh();
					}
					return res.data;
                }
            );
        }
        else{
            console.log("!= CREATE");
        }        
    }

    $scope.ViewNoGRClick = function(SelectData){
        $scope.ViewGR = SelectData;
		$scope.flagStatus = "LihatGR";
		
		var getPO = "?POId=" + $scope.ViewGR.POId;
            GoodReceiptFactory.getDataPO(getPO).then(
                function(res){
                    $scope.getDataPO = res.data.Result;
					if($scope.getDataPO[0].POTypeId==3||$scope.getDataPO[0].POTypeId==4)
					{	
						$scope.gridCreateViewGoodReceipt = {
							enableSorting: true,
							enableRowSelection: true,
							multiSelect: false,
							enableSelectAll: true,
							//showTreeExpandNoChildren: true,
							paginationPageSizes: [10, 25, 50],
							paginationPageSize: 10,
							//data: 'dataCreateViewGoodReceipt',
							columnDefs: [
								{ name:'GRId', field:'GRId', visible:false},
								{ name:'No. Item', field:'NO', width: '10%', cellTemplate: ' <span>{{rowRenderIndex+1}}</span>', visible:true},
								{ name:'No Rangka', field:'FrameNo', width: '15%' , enableCellEdit:false, visible:true}, 
								{ name:'Kode Jasa', field:'MaterialCode', width: '15%', enableCellEdit:false, visible:true},          
								{ name:'Nama Jasa', field:'MaterialName', enableCellEdit:false, visible:true },			
								{ name:'Qty PO', field:'QtyPO', format: '{0:c}', type: 'number', enableCellEdit:false, visible:true},
								{ name:'Qty yang sudah diterima', format: '{0:c}', type: 'number', field:'Qty', width: '20%', enableCellEdit:false, visible:true},
								{ name:'Qty yang diterima *', cellTooltip: 'Double click untuk mengisi kolom', field:'QtyReceived', format: '{0:c}', type:'number', max:99999, enableCellEdit:true, visible:true},
								{ name:'Satuan', field:'UomName', enableCellEdit:false , visible:true}    
							]
						};
						$scope.gridCreateViewGoodReceipt.data = $scope.getDataPO[0].ListOfPODetailSummaryView;
					}
					else
					{
						
						$scope.gridCreateViewGoodReceipt = {
							enableSorting: true,
							enableRowSelection: true,
							multiSelect: false,
							enableSelectAll: true,
							//showTreeExpandNoChildren: true,
							paginationPageSizes: [10, 25, 50],
							paginationPageSize: 10,
							//data: 'dataCreateViewGoodReceipt',
							columnDefs: [
								{ name:'GRId', field:'GRId', visible:false},
								{ name:'No. Item', field:'NO', width: '10%', cellTemplate: ' <span>{{rowRenderIndex+1}}</span>', visible:true},
								{ name:'No Rangka', field:'FrameNo', width: '15%' , enableCellEdit:false, visible:true}, 
								{ name:'Kode Material', field:'MaterialCode', width: '15%', enableCellEdit:false, visible:true},          
								{ name:'Nama Material', field:'MaterialName', enableCellEdit:false, visible:true },			
								{ name:'Qty PO', field:'QtyPO', format: '{0:c}', type: 'number', enableCellEdit:false, visible:true},
								{ name:'Qty yang sudah diterima', format: '{0:c}', type: 'number', field:'Qty', width: '20%', enableCellEdit:false, visible:true},
								{ name:'Qty yang diterima *', cellTooltip: 'Double click untuk mengisi kolom', field:'QtyReceived', format: '{0:c}', type:'number', max:99999, enableCellEdit:true, visible:true},
								{ name:'Satuan', field:'UomName', enableCellEdit:false , visible:true}    
							]
						};
						console.log("setan",SelectData.ListOfGRDetailView);
                        $scope.gridCreateViewGoodReceipt.data = $scope.getDataPO[0].ListOfPODetailSummaryView;
                        
					}
					return res.data;
                }
            );

        
        $scope.MaintainForm = false;
        $scope.SaveGR = false;
        $scope.CreateviewForm = true;
        $scope.lihatGR = true;
        $scope.TextboxLihatGR = true;
        $scope.DisabledTextViewGR = true;
        $scope.gridCreateViewGoodReceipt.columnDefs[7].enableCellEdit = false;
        $scope.gridCreateViewGoodReceipt.columnDefs[7].cellTooltip = '';
		
		//var RawReceivedTime_Get = ""+$scope.ViewGR.ReceivedDate.split(':');
				// $scope.ViewGR.ReceivedTime = {    
				 // value: new Date(2015, 10, 10, $scope.ViewGR.ReceivedDate.getHours(), $scope.ViewGR.ReceivedDate.getMinutes(), $scope.ViewGR.ReceivedDate.getSeconds())
				 // };
				 
		
		$scope.ViewGR.ReceivedTime=angular.copy($scope.ViewGR.ReceivedDate);
		
		
		var getTypeId = "?PRTypeId=" + SelectData.POTypeId;
        GoodReceiptFactory.getDataVendor(getTypeId).then(
            function(res){
                $scope.DataVendor = res.data.Result;
                //return res.data;
				
				var getVendorId = "?POTypeId=" + SelectData.POTypeId + "&VendorId=" + SelectData.VendorId;
				GoodReceiptFactory.getPOCode(getVendorId).then(
					function(res){
						$scope.DataPOCode = res.data.Result;
						//return res.data;
					}
				);
				
            }
        );
		
		

        //console.log("gridCreateViewGoodReceipt",  $scope.gridCreateViewGoodReceipt);
    }

    $scope.simpanGRBtn =  function(){
        $scope.disabledSimpanGRBtn = true;
        $scope.ViewGR.VendorId = $scope.getDataPO[0].VendorId;
        $scope.ViewGR.POTypeId = $scope.getDataPO[0].POTypeId;
        $scope.gridCreateViewGoodReceipt.data.QtyReceive = $scope.getDataPO[0].ListOfPODetailPRView.QtyReceive;
        $scope.gridCreateViewGoodReceipt.data.Qty = $scope.getDataPO[0].ListOfPODetailPRView.Qty;
        $scope.ViewGR.ListOfGRDetailView = $scope.gridCreateViewGoodReceipt.data;
		
		var DaTotalGoodReceiptQtyYangDiterimaValid=0;
		
		for(var i = 0; i < $scope.gridCreateViewGoodReceipt.data.length; ++i)
		{	
			try
			{
				DaTotalGoodReceiptQtyYangDiterimaValid+=$scope.gridCreateViewGoodReceipt.data[i].QtyReceive;
			}
			catch(ulala)
			{
				//$scope.gridCreateViewGoodReceipt.data[i].QtyReceive=0;
				//belom kepake
			}
		}
		
		if(DaTotalGoodReceiptQtyYangDiterimaValid>0)
		{
			var GoodReceiptPaksaTime = new Date($scope.ViewGR.ReceivedDate.getFullYear(), $scope.ViewGR.ReceivedDate.getMonth(), $scope.ViewGR.ReceivedDate.getDate(), $scope.ViewGR.ReceivedTime.getHours(), $scope.ViewGR.ReceivedTime.getMinutes(), 0);
			$scope.ViewGR.ReceivedDate=angular.copy(GoodReceiptPaksaTime);
			
			
			GoodReceiptFactory.create($scope.ViewGR).then(
				function(){
					bsNotify.show(
										{
											title: "Berhasil",
											content: "GR berhasil disimpan.",
											type: 'success'
										}
                                    );
                                    $scope.disabledSimpanGRBtn = false;
				},
				function(err){

					bsNotify.show(
										{
											title: "Gagal",
											content: "GR gagal disimpan.",
											type: 'danger'
										}
                                    );
                                    $scope.disabledSimpanGRBtn = false;
					
				}
			);
			$scope.MaintainForm = true;
			$scope.CreateviewForm = false;
		}
		else
		{
			bsNotify.show(
						{
							title: "Peringatan",
							content: "Pastikan semua qty yang diterima terisi dengan benar dan jumlahnya lebih besar dari 0.",
							type: 'warning'
						}
                    );
                    $scope.disabledSimpanGRBtn = false;
			
		}
		
        
    }

    $scope.TutupBtn = function(){
        $scope.CreateviewForm = false;
        $scope.MaintainForm = true;
        $scope.gridCreateViewGoodReceipt.data = [];
    }

    //----------------------------------
    // Begin Modal
    //----------------------------------
    $scope.btnBatalGR = function(){
        //$scope.show_modal.show =! $scope.show_modal.show;
		setTimeout(function() {
              angular.element('.ui.modal.ModalGoodReceipt_AlasanBatalGR').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalGoodReceipt_AlasanBatalGR').not(':first').remove();  
            }, 1);
        $scope.item = $scope.SelectedRows;
        $scope.item[0].ReasonCancelGRId = "";
        $scope.item[0].ReasonCancelNote = "";
    }
    
    $scope.SimpanModalGoodReceipt_AlasanBatalGR = function(item){
        $scope.disabledSimpanModalGoodReceipt_AlasanBatalGR = true;
        GoodReceiptFactory.update($scope.item).then(
            function(){
                // GoodReceiptFactory.getData().then(
                    // function(){
                        // $scope.grid.data = res.data.Result;
                    // });
					
				GoodReceiptFactory.getData($scope.filter).then(
					function(res){
						$scope.grid.data = res.data.Result;
						$scope.loading=false;
						return res.data.Result;
					},
					function(err){
						console.log("err=>",err);
					}
				);	
                
				bsNotify.show(
                    {
                        title: "Berhasil",
                        content: "GR berhasil dibatalkan.",
                        type: 'success'
                    }
                );
                $scope.disabledSimpanModalGoodReceipt_AlasanBatalGR = false;
            },
            function(err)
                {console.log("err=>",err);
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    timeout: 3000,
                    title: "GR gagal dibatalkan",
                });
                $scope.disabledSimpanModalGoodReceipt_AlasanBatalGR = false;
            }
        );
        //$scope.show_modal={show:false};
		angular.element('.ui.modal.ModalGoodReceipt_AlasanBatalGR').modal('hide');
    }
    
    // $scope.onListCancel = function(item){
        // $scope.show_modal={show:false};
    // }
	
	$scope.KembaliModalGoodReceipt_AlasanBatalGR = function(){
        // $scope.disabledSimpanModalGoodReceipt_AlasanBatalGR = false;
        angular.element('.ui.modal.ModalGoodReceipt_AlasanBatalGR').modal('hide');
    }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    
    $scope.onSelectRows = function(rows){
        $scope.SelectedRows = rows;
        console.log("onSelectRows=>",$scope.SelectedRows);
        if($scope.SelectedRows[0].GRStatusId == 2){
            $scope.btnbatalGR = true;
        }
        else{
            $scope.btnbatalGR = false;
        }
    }
	
    var NOGRClick = '<span style="color:blue"><p style="padding:5px 0 0 5px" ng-click="grid.appScope.$parent.ViewNoGRClick(row.entity)"><u>{{row.entity.GRCode}}</u></p></span>';;
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: false,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'GRId', field:'GRId', visible:false},
			{ name:'Nama Customer',  field: 'CustomerName'},
            { name:'No. PO',    field:'POCode', width: '15%'},
            { name:'Tanggal PO', field:'PODate', cellFilter: 'date:\'dd-MM-yyyy\''},          
            { name:'PO Status',  field: 'POStatusName'},
            {
                name: 'No GR',
                width: '15%',
                enableColumnMenu: false,
                enableSorting: true,
				field:'GRCode',
                enableColumnResizing: true,
                cellTemplate: NOGRClick                  
            },
            { name:'Tanggal Dokumen GR',  field: 'GRDate', cellFilter: 'date:\'dd-MM-yyyy\''},
            { name:'GR Status',  field: 'GRStatusName'},
            { name:'Vendor',  field: 'NameVendor'}        
        ]
    };

    $scope.gridCreateViewGoodReceipt = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: false,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10, 25, 50],
        paginationPageSize: 10,
        //data: 'dataCreateViewGoodReceipt',
        columnDefs: [
            { name:'GRId', field:'GRId', visible:false},
            { name:'No. Item', field:'NO', width: '10%', cellTemplate: ' <span>{{rowRenderIndex+1}}</span>', visible:true},
			{ name:'No Rangka', field:'FrameNo', width: '15%' , enableCellEdit:false, visible:true}, 
            { name:'No. Material', field:'AccessoriesCode', width: '15%', enableCellEdit:false, visible:true},          
            { name:'Nama Material', field:'AccessoriesName', enableCellEdit:false, visible:true },
			{ name:'No. Material', field:'MaterialCode', width: '15%', enableCellEdit:false, visible:true},          
            { name:'Nama Material', field:'MaterialName', enableCellEdit:false, visible:true },			
            { name:'Qty PO', field:'QtyPO', format: '{0:c}', type: 'number', enableCellEdit:false, visible:true},
            { name:'Qty yang sudah diterima', format: '{0:c}', type: 'number', field:'QtyReceived', width: '20%', enableCellEdit:false, visible:true},
            { name:'Qty yang diterima *', cellTooltip: 'Double click untuk mengisi kolom', field:'QtyReceive', format: '{0:c}', type:'number', max:99999, enableCellEdit:true, visible:true},
            { name:'Satuan', field:'UomName', enableCellEdit:false , visible:true}    
        ]
    };

    $scope.gridCreateViewGoodReceipt.onRegisterApi = function(gridApi){
        $scope.gridApigridCreateViewGoodReceipt = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
            $scope.selectedRow = gridApi.selection.getSelectedRows();
            console.log('select');
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
           $scope.selectedRow = gridApi.selection.getSelectedRows();
        });

        gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            var count = 0;
            var countError = 0;
            var TotalQty = rowEntity.QtyPO - rowEntity.QtyReceived; 
            if(newValue > TotalQty){
                rowEntity.QtyReceive = null;
                count++;
            }
            if(newValue < 0){
                rowEntity.QtyReceive = null;
                count++;
            }
           
            for(var i in $scope.gridCreateViewGoodReceipt.data) {
                
                if ($scope.gridCreateViewGoodReceipt.data[i].QtyReceive == null || typeof $scope.gridCreateViewGoodReceipt.data[i].QtyReceive == 'undefined') {
                    countError++;
                }
                // }else{
                //     QtyDiterima = $scope.gridCreateViewGoodReceipt.data[i].QtyReceived;
            }
            //     var TotalQty = $scope.gridCreateViewGoodReceipt.data[i].QtyPO - QtyDiterima ;

            //     if(newValue > TotalQty){
            //         $scope.gridCreateViewGoodReceipt.data[i].QtyReceive = null;
            //         count++;
            //     }else if(newValue < 0){
            //         $scope.gridCreateViewGoodReceipt.data[i].QtyReceive = null;
            //         count++;

            //     }
                    
                
            // }
			
			
            if(count > 0) {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: 'Quantity yang diinput salah',
                        type: 'warning'
                    }
                );
                // $scope.disabledGrid = false;
            }
            if(countError == 0){
                $scope.disabledGrid = false;
            }else{
                $scope.disabledGrid = true;
            }


			//var angka_beres=true;
			if(rowEntity.QtyReceive%1!=0 || rowEntity.QtyReceive.toString().indexOf(".")!=-1)
			{
				rowEntity.QtyReceive=0;
				bsNotify.show(
										{
											title: "Peringatan",
											content: "Anda hanya dapat mengisi angka bulat",
											type: 'warning'
										}
									);
				
			}
			// for(var x in $scope.gridCreateViewGoodReceipt.data) 
			// {
				// try
				// {
					// if($scope.gridCreateViewGoodReceipt.data[x].QtyReceive%1!=0 || $scope.gridCreateViewGoodReceipt.data[x].QtyReceive.toString().indexOf(".")!=-1)
					// {
						// $scope.gridCreateViewGoodReceipt.data[x].QtyReceive=0;
					// }
				// }
				// catch(ulala)
				// {
					// $scope.gridCreateViewGoodReceipt.data[x].QtyReceive=0;
				// }
				
			// }
              
        });
    };
});
