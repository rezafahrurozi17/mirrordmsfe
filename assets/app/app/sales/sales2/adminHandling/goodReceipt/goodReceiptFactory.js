angular.module('app')
  .factory('GoodReceiptFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
          for(key in filter){
            if(filter[key]==null || filter[key]==undefined)
              delete filter[key];
          }
          var res=$http.get('/api/sales/AdminHandlingGR/?start=1&limit=10000&'+$httpParamSerializer(filter));
          return res;
      },

      getDataPO: function(param) {
        console.log('param=>',param);
        var res=$http.get('/api/sales/AdminHandlingPOMaintenance' + param);
        return res;
      },
      
      getDataStatusGR: function() {
        var res=$http.get('/api/sales/AdminHandlingStatusGR');
        console.log('res=>',res);
        return res;
      },
      
      getDataCancelGR: function() {
        var res=$http.get('/api/sales/PCategoryReasonCancelGR');
        return res;
      },

      getDataVendor: function(param) {
        var res=$http.get('/api/sales/MVendorList' + param);
        return res;
      },

      getPOCode: function(param) {
        var res=$http.get('/api/sales/AdminHandlingPOList' + param);        
        return res;
      },

      create: function(MaintainGR) {
        
		
		
		
		var ulala=""+MaintainGR.ReceivedDate.getFullYear()+"-"+('0' + (MaintainGR.ReceivedDate.getMonth()+1)).slice(-2)+"-"+('0' + MaintainGR.ReceivedDate.getDate()).slice(-2)+"T"+('0' + (MaintainGR.ReceivedDate.getHours())).slice(-2)+":"+('0' + (MaintainGR.ReceivedDate.getMinutes())).slice(-2)+":00";
		var MaintainGR_Buat_Paksa_ReceivedDate=angular.copy(ulala);
	
        return $http.post('/api/sales/AdminHandlingGR', [{
               
                POId: MaintainGR.POId,
                NoSuratJalan: MaintainGR.NoSuratJalan,
                GRDate: MaintainGR.GRDate,
                ReceivedDate: MaintainGR_Buat_Paksa_ReceivedDate,
                Note: MaintainGR.Note,
                GRStatusId: MaintainGR.GRStatusId,
                POCode: MaintainGR.POCode,
                POTypeId: MaintainGR.POTypeId,
                POTypeName: MaintainGR.POTypeName,
                PODate: MaintainGR.PODate,
                POStatusId: MaintainGR.POStatusId,
                POStatusName: MaintainGR.POStatusName,
                GRStatusName: MaintainGR.GRStatusName,
                Note: MaintainGR.Note,
                VendorId: MaintainGR.VendorId,
                NameVendor: MaintainGR.NameVendor,
                ListOfGRDetailView:MaintainGR.ListOfGRDetailView,
                AccessoriesId: MaintainGR.AccessoriesId,
                AccessoriesCode: MaintainGR.AccessoriesCode,
                AccessoriesName: MaintainGR.AccessoriesName,
                Qty: MaintainGR.Qty,
                QtyReceive: MaintainGR.QtyReceive,
                UomId: MaintainGR.UomId,
                UomName: MaintainGR.UomName
        }]);
      },

      update: function(MaintainGR){
        return $http.put('/api/sales/AdminHandlingGR/Cancel', MaintainGR);
      },

      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });




