angular.module('app')
    .controller('MaintainBillingController', function ($filter, ComboBoxFactory, $scope,bsRights, $stateParams, $rootScope, PrintRpt, bsNotify, $http, CurrentUser, MaintainSOFactory, MaintainBillingFactory, $timeout, MobileHandling, WarnaFactory, uiGridConstants, uiGridGroupingConstants, TipeFactory) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.tab = $stateParams.tab;
        $scope.breadcrums = {};

        $scope.selectedFilterBelumBilling = {};
        $scope.maintainBillingForm = true;
        $scope.GridBelumBuatBilling = true;
        $scope.BelumBillingButtonSearch = true;
        $scope.GridSudahBuatBilling = false;
        $scope.ShowGridTungguApprovalBilling = false;
        $scope.disablebuttoneBuatBilling = true;
        $scope.buatBillingForm = false;
        $scope.TodayDate_Raw = new Date();
        $scope.TodayDate = $scope.TodayDate_Raw.getFullYear() + '-' + ('0' + ($scope.TodayDate_Raw.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate_Raw.getDate()).slice(-2);
        $scope.MaintainBillingShowAdvSearch = true;
        $scope.MaintainBillingModeSimpan = "";
        $scope.MaintainBillingUdaKalkulasi = "";
        $scope.disabledBtnSimpanCetak = false;

        MaintainBillingFactory.getDataLeasing().then(function (res) {
            $scope.leasing = res.data.Result;
        });
        // ==== value of bsRights
        // approve: boolean
        // custom1: boolean
        // custom2: boolean
        // custom3: boolean
        // custom4: boolean
        // custom5: boolean
        // retur: boolean
        // print2: boolean
        // cancel: boolean
        // delete: boolean
        // edit: boolean
        // new: boolean
        // print: boolean
        // reject: boolean
        // review: boolean
        // view: boolean 
        //  ==> example = val.approve 
        $scope.objRights = null;
        $scope.getRights = function(){
            $scope.objRights = bsRights.get("app.maintainbilling");
        }
        $scope.getRights();

        $scope.disableBank = true;
        //$scope.showInfoPengiriman = true;
        $scope.showCetakBatal = false;
        $scope.showCetak = false;
        $scope.showCetakNota = false;

        $scope.permohonanPembatalanBillingForm = false;
        $scope.Selectdatarow = {};
        $scope.selected_rows = [];
        $scope.DisabledTotalRincianHarga = true;
        $scope.flagStatus = "SIMPAN";

        // inisialisasi fitri
        $scope.onProsesClickButtonCetak =  false;
        $scope.onProsesClickButtonCetak=  false;
        // IfGotProblemWithModal();
        $scope.uploadFiles = [];

        var listData = [{
            Id: 1,
            Name: "Outstanding SO (Belum Billing)"
        }, {
            Id: 3,
            Name: "Outstanding SO (Menunggu Approval)"
        }, {
            Id: 2,
            Name: "SO Sudah Billing"
        }];

        $scope.optionsLeasingTenor = [];

        $scope.onNamaLeasingSelected = function (selectedLeasingName) {
            console.log('on model change ===>', selectedLeasingName);
            if (selectedLeasingName == undefined || selectedLeasingName == null) {
                $scope.getDataSO[0].LeasingId = null;
                $scope.getDataSO[0].LeasingName = null;
                $scope.getDataSO[0].LeasingLeasingTenorId = null;
                $scope.getDataSO[0].LeasingTenorTime = null;
                $scope.optionsLeasingTenor = [];
            } else {
                $scope.getDataSO[0].LeasingId = selectedLeasingName.LeasingId;
                $scope.getDataSO[0].LeasingName = selectedLeasingName.LeasingName;
                $scope.getDataSO[0].LeasingLeasingTenorId = null;
                $scope.getDataSO[0].LeasingTenorTime = null;
                $scope.optionsLeasingTenor = [];
                $scope.optionsLeasingTenor = angular.copy(selectedLeasingName.ListLeasingTenor);
                console.log(' $scope.optionsLeasingTenor ===>', $scope.optionsLeasingTenor);


                // MaintainBillingFactory.getDataTenor(selectedLeasingName.LeasingId).then(function (res) {
                //     for (var i = 0; i < res.data.Result.length; ++i) {
                //         if (res.data.Result[i].LeasingId == selectedLeasingName.LeasingId) {

                //             $scope.optionsLeasingTenor = res.data.Result[i].ListLeasingTenor;
                //             console.log('$scope.tenor', $scope.optionsLeasingTenor);
                //             break;
                //         }
                //     }
                    
                // });
            }
        }

        $scope.onTenorLeasingSelected = function (selectedTenorTIme) {
            console.log('on tenor change ===>', selectedTenorTIme);
            if (selectedTenorTIme == null || selectedTenorTIme == undefined) {
                $scope.getDataSO[0].LeasingLeasingTenorId = null
                $scope.getDataSO[0].LeasingTenorTime = null
            }else{
                $scope.getDataSO[0].LeasingLeasingTenorId = selectedTenorTIme.LeasingLeasingTenorId;
                $scope.getDataSO[0].LeasingTenorTime = selectedTenorTIme.LeasingTenorTime;
            }
        }


        $scope.selectionItem = listData;
        $scope.mDaftarSO = $scope.selectionItem;
        $scope.mDaftarSO.Id = 1;

        $scope.DaftarSO = function (selected) {
            if ($scope.mDaftarSO.Id == 1) {
                $scope.GridBelumBuatBilling = true;
                $scope.GridSudahBuatBilling = false;
                $scope.ShowGridTungguApprovalBilling = false;
                $scope.BelumBillingButtonSearch = true;
                $scope.SudahBillingButtonSearch = false;
                $scope.MenungguApprovalButtonSearch = false;
                $scope.disablebuttoneBuatBilling = true;
                console.log('belum billing');
                
            } else if ($scope.mDaftarSO.Id == 2) {
                $scope.GridBelumBuatBilling = false;
                $scope.GridSudahBuatBilling = true;
                $scope.ShowGridTungguApprovalBilling = false;
                $scope.BelumBillingButtonSearch = false;
                $scope.SudahBillingButtonSearch = true;
                $scope.MenungguApprovalButtonSearch = false;
                $scope.disablebuttoneBuatBilling = true;
                console.log('sudah billing');

            } else if ($scope.mDaftarSO.Id == 3) {
                $scope.GridBelumBuatBilling = false;
                $scope.GridSudahBuatBilling = false;
                $scope.ShowGridTungguApprovalBilling = true;
                $scope.BelumBillingButtonSearch = false;
                $scope.SudahBillingButtonSearch = false;
                $scope.MenungguApprovalButtonSearch = true;
                $scope.disablebuttoneBuatBilling = true;
                console.log('menunggu approval');

            }
            //$scope.selectedRow=[];
            //$scope.selectSudahBilling=[];
            $scope.gridApiBelumBilling.selection.clearSelectedRows();
            $scope.gridApiSudahBilling.selection.clearSelectedRows();
            $scope.showCetakNota = false;
            $scope.showCetakBatal = false;
            $scope.showCetak = false;
            $scope.disablebuttoneBuatBilling = true;
        };

        $scope.ChangeSales = function () {
            if ($scope.ViewData.SOTypeId == 3) {
                var getSalesId = "&SalesId=" + $scope.ViewData.EmployeeId;
                MaintainSOFactory.getNoRangka(getSalesId).then(
                    function (res) {
                        $scope.DataNorangka = res.data.Result;
                        return res.data;
                    }
                );
            }
        }

        $scope.pilihFrameNo = function (selected) {
            if ($scope.flagStatus == "InsertSO" || $scope.flagStatus == "UpdateSO") {
                $scope.resultFrame = selected;
                $scope.ViewData.FrameNo = $scope.resultFrame.FrameNo;
                $scope.ViewData.Name = $scope.resultFrame.CustomerName;
                $scope.ViewData.KTP = $scope.resultFrame.KTPKITAS;
                $scope.ViewData.SIUP = $scope.resultFrame.SIUP;
                $scope.ViewData.NPWP = $scope.resultFrame.NPWP;
                $scope.ViewData.Handphone = $scope.resultFrame.Handphone;
                $scope.ViewData.Address = $scope.resultFrame.Address;
                $scope.ViewData.ProspectId = $scope.resultFrame.ProspectId;
            }
            // $scope.resultFrame = selected;
            // $scope.ViewData.FrameNo = $scope.resultFrame.FrameNo;
            // $scope.ViewData.Name = $scope.resultFrame.CustomerName;
            // $scope.ViewData.KTP = $scope.resultFrame.KTPKITAS;
            // $scope.ViewData.SIUP = $scope.resultFrame.SIUP;
            // $scope.ViewData.NPWP = $scope.resultFrame.NPWP;
            // $scope.ViewData.Handphone = $scope.resultFrame.Handphone;
            // $scope.ViewData.Address = $scope.resultFrame.Address;
            // $scope.ViewData.ProspectId = $scope.resultFrame.ProspectId;
        }

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
            //----------Begin Check Display---------

            $scope.bv = MobileHandling.browserVer();
            $scope.bver = MobileHandling.getBrowserVer();       //console.log("browserVer=>",$scope.bv);
            //console.log("browVersion=>",$scope.bver);
            //console.log("tab: ",$scope.tab);

            if ($scope.bv !== "Chrome") { if ($scope.bv == "Unknown") { if (!alert("Menu ini tidak bisa dibuka pada perangkat Mobile.")) { $scope.removeTab($scope.tab.sref); } } }         //----------End Check Display-------
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainBilling = null; //Model
        $scope.xRole = { selected: [] };
        $scope.filter = { SOType: null, SODateStart: null, SODateEnd: null };

        $scope.MaintainBillingdateOptionsStart1 = {
            minDate: new Date(),
            startingDay: 1,
            format: "dd/MM/yyyy",
        };

        $scope.MaintainBillingdateOptionsStart2 = {
            minDate: new Date(),
            startingDay: 1,
            format: "dd/MM/yyyy",
        };

        $scope.MaintainBillingdateOptionsSampai1 = {
            minDate: new Date(),
            startingDay: 1,
            format: "dd/MM/yyyy",
        };

        $scope.MaintainBillingdateOptionsSampai2 = {
            minDate: new Date(),
            startingDay: 1,
            format: "dd/MM/yyyy",
        };

        $scope.dateOptionsStart = {
            startingDay: 1,
            format: "dd/MM/yyyy",

        };

        $scope.dateOptionsEnd = {
            startingDay: 1,
            format: "dd/MM/yyyy",

        };

        $scope.tanggalmin = function (dt) {
            $scope.dateOptionsEnd.minDate = dt;
        }
        var Da_TodayDate = new Date();
        var da_awal_tanggal1 = new Date(Da_TodayDate.getFullYear(), Da_TodayDate.getMonth(), 1);
        $scope.filter.SODateStart = da_awal_tanggal1;
        $scope.tanggalmin($scope.filter.SODateStart);
        $scope.filter.SODateEnd = Da_TodayDate;

        $scope.showAlert = function (message, number) {

            bsNotify.show(
                {
                    title: "Peringatan",
                    content: "Mohon Input Filter.",
                    type: 'warning'
                }
            );
        };

        //----------------------------------
        // Get Data
        //----------------------------------

        MaintainSOFactory.getFakturPajak().then(
            function (res) {
                $scope.CodePajak = res.data.Result;
                //$scope.ViewData = {};
                //$scope.ViewData.CInvoiceTypeId = $scope.CodePajak[0].CodeInvoiceTransactionTaxId;
                return res.data;
            }
        );

        TipeFactory.getData().then(
            function (res) {
                $scope.listTipe = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getOutlet().then(
            function (res) {
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getDataSalesman().then(
            function (res) {
                $scope.getDataSalesman = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getJPembayaran().then(
            function (res) {
                $scope.dataPembayaran = res.data.Result;
                return res.data;
            }
        );

        $scope.getData = function () {
            $scope.disablebuttoneBuatBilling = true;
            alerts = [];
            $scope.gridData = [];
            if ($scope.filter.SOType == null || $scope.filter.SODateStart == null || $scope.filter.SODateEnd == null) {
                $scope.showAlert(alerts.join('\n'), alerts.length);
                $timeout(function () {
                    $scope.loading = false;
                })
            } else {

                if ($scope.filter.SOType == 0) {
                    $scope.filter.SOType = null;
                }
                $scope.filter.SODateStart = $scope.changeFormatDate($scope.filter.SODateStart);
                $scope.filter.SODateEnd = $scope.changeFormatDate($scope.filter.SODateEnd);
                
                MaintainBillingFactory.getData($scope.filter).then(
                    function (res) {
                        $scope.GridBelumBilling.data = res.data.Result;
                        $scope.GridBelumBilling.dataTemp = res.data.Result;
                        $scope.loading = false;
                        return res.data.Result;
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );

                if ($scope.filter.SOType == null) {
                    $scope.filter.SOType = 0;
                }
            }
        }
        $scope.changeFormatDate = function(item) {
            var tmpParam = item;
            tmpParam = new Date(tmpParam);
            var finalDate
            var yyyy = tmpParam.getFullYear().toString();
            var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpParam.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            
            return finalDate;
        }

        $scope.ChangeSOType_MaintainSoDiMaintainBilling = function () {
            if ($scope.ViewData.SOTypeId == "2") //OPD
            {
                // $scope.ShowMenuTabs = true;

                // $scope.ShowtabContainer = true;
                // $scope.InfoPelanggan = true;
                // $scope.ShowPelangganOPD = true;
                // $scope.ShowPeranPelanggan = false;
                // $scope.JasaPengurusanDokumen = true;
                $scope.ShowNorangka_MaintainSoDiMaintainBilling = true;//tadinya false diresmiin true sama andry 28 juni 2019
                // $scope.Aksesoris = false;
                // $scope.Dokumen = true;
                // $scope.InfoPengiriman = true;
                // $scope.RincianHarga = true;
                // $scope.karoseriFinishUnit = false;
                // $scope.InfoLeasingFinishUnit = false;
                // $scope.InfoAsuransiFinishUnit = false;
                // $scope.finishUnitInfounityangakandibeli = false;
                // $scope.InfoMediatorFinishUnit = false;
                // $scope.InfoPemakaiFinishUnit = false;

            } else if ($scope.ViewData.SOTypeId == "3") {
                $scope.ShowMenuTabs = true;

                // $scope.ShowtabContainer = true;
                $scope.ShowNorangka_MaintainSoDiMaintainBilling = true;
                // $scope.ShowPeranPelanggan = false;
                // $scope.InfoPelanggan = true;
                // $scope.ShowPelangganOPD = true;
                // $scope.JasaPengurusanDokumen = false;
                // $scope.Aksesoris = true;
                // $scope.Dokumen = false;
                // $scope.InfoPengiriman = true;
                // $scope.RincianHarga = true;
                // $scope.karoseriFinishUnit = false;
                // $scope.InfoLeasingFinishUnit = false;
                // $scope.InfoAsuransiFinishUnit = false;
                // $scope.finishUnitInfounityangakandibeli = false;
                // $scope.InfoMediatorFinishUnit = false;
                // $scope.InfoPemakaiFinishUnit = false;
            } else if ($scope.ViewData.SOTypeId == "4") {

                // $scope.ShowMenuTabs = true;
                // $scope.ShowtabContainer = true;
                // $scope.InfoPelanggan = false;
                $scope.ShowNorangka_MaintainSoDiMaintainBilling = false;
                // $scope.JasaPengurusanDokumen = false;
                // $scope.Aksesoris = false;
                // $scope.Dokumen = false;
                // $scope.InfoPengiriman = true;
                // $scope.RincianHarga = true;
            }

            if ($scope.ViewData.SOTypeId == "3") {
                $scope.columnDefsgridAksesorisMaintainBilling[11].visible = true;
                $scope.gridAksesorisMaintainBilling_Api.grid.refresh();
            }
            else {
                $scope.columnDefsgridAksesorisMaintainBilling[11].visible = false;
                $scope.gridAksesorisMaintainBilling_Api.grid.refresh();
            }


        };

        $scope.getDatawesbilling = function () {
            alerts = [];
            $scope.gridData = [];
            if ($scope.filter.SOType == null || $scope.filter.SODateStart == null || $scope.filter.SODateEnd == null) {
                $scope.showAlert(alerts.join('\n'), alerts.length);
                $timeout(function () {
                    $scope.loading = false;
                })
            } else {

                if ($scope.filter.SOType == 0) {
                    $scope.filter.SOType = null;
                }

                MaintainBillingFactory.getDatawesbilling($scope.filter).then(
                    function (res) {
                        $scope.GridSudahBilling.data = res.data.Result;
                        $scope.GridSudahBilling.dataTemp = res.data.Result;
                        $scope.loading = false;
                        return res.data.Result;
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );

                if ($scope.filter.SOType == null) {
                    $scope.filter.SOType = 0;
                }
            }
        }

        $scope.SearchgetDataMenungguApprovalbilling = function () {
            alerts = [];
            $scope.gridData = [];
            if ($scope.filter.SOType == null || $scope.filter.SODateStart == null || $scope.filter.SODateEnd == null) {
                $scope.showAlert(alerts.join('\n'), alerts.length);
                $timeout(function () {
                    $scope.loading = false;
                })
            } else {

                if ($scope.filter.SOType == 0) {
                    $scope.filter.SOType = null;
                }

                MaintainBillingFactory.getDataYangMenungguApproval($scope.filter).then(
                    function (res) {
                        $scope.GridTungguApprovalBilling.data = res.data.Result;
                        $scope.GridTungguApprovalBilling.dataTemp = res.data.Result;
                        $scope.loading = false;
                        console.log('GridTungguApprovalBilling .data ==>', $scope.GridTungguApprovalBilling.data);
                        return res.data.Result;
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );

                if ($scope.filter.SOType == null) {
                    $scope.filter.SOType = 0;
                }
            }
        }

        $scope.ActRefreshMaintainBilling = function () {
            if ($scope.mDaftarSO.Id == 1) {
                $scope.getData();
            } else {
                $scope.getDatawesbilling();
            }
        }

        MaintainSOFactory.getSOType().then(
            function (res) {
                $scope.getSOType = res.data.Result;
                $scope.getSOTypeMaintainBilling = [{ SOTypeId: null, SOTypeName: null }];
                $scope.getSOTypeMaintainBilling.splice(0, 1);
                $scope.getSOTypeMaintainBilling.push({ SOTypeId: 0, SOTypeName: "All" });

                for (var i = 0; i < res.data.Result.length; i++) {
                    $scope.getSOTypeMaintainBilling.push({ SOTypeId: res.data.Result[i].SOTypeId, SOTypeName: res.data.Result[i].SOTypeName });
                }
                $scope.filter.SOType = 0;
                //return res.data;
            }
        );

        MaintainSOFactory.getCustomerCategory().then(
            function (res) {
                $scope.getCustomerCategory = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getDataBank().then(
            function (res) {
                $scope.getDataBank = res.data.Result;
                return res.data;
            }
        );

        MaintainBillingFactory.getDataCPosition().then(
            function (res) {
                $scope.CPosition = res.data.Result;
                return res.data;
            }
        );

        MaintainBillingFactory.getDataProvince().then(
            function (res) {
                $scope.getProvince = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getJPembayaran().then(
            function (res) {
                $scope.getmetodepembayaran = res.data.Result;
                return res.data;
            }
        );

        $scope.filterKabupaten = function (selected) {
            MaintainBillingFactory.getDataCity('?start=1&limit=100&filterData=ProvinceId|' + selected).then(function (res) { $scope.getCity = res.data.Result; });


        }

        $scope.filterKecamatan = function (selected) {
            MaintainBillingFactory.getDataKec('?start=1&limit=100&filterData=CityRegencyId|' + selected).then(function (res) { $scope.getKec = res.data.Result; });

        }

        $scope.filterKelurahan = function (selected) {
            MaintainBillingFactory.getDataKel('?start=1&limit=100&filterData=DistrictId|' + selected).then(function (res) { $scope.getKel = res.data.Result; });

        }

        $scope.DisabledKTP = false;
        $scope.DisabledKitas = false;

        $scope.DisabledKTPFunction = function (isi) {
            if (isi.SIUPNo != null && isi.TDPNo == null) {
                $scope.DisabledKTP = true;
            } else if (isi.SIUPNo == null && isi.TDPNo != null) {
                $scope.DisabledKTP = true;
            } else if (isi.SIUPNo != null && isi.TDPNo != null) {
                $scope.DisabledKTP = true;
            } else {
                $scope.DisabledKTP = false;
            }
        }

        $scope.DisabledKitasFunction = function (isi) {
            if (isi != null) {
                $scope.DisabledKitas = true;
            } else {
                $scope.DisabledKitas = false;
            }

        }

        $scope.MaintainBillingTanggalEfektifFakturChanged1 = function (date) {

            $scope.MaintainBillingdateOptionsSampai1.minDate = date;
            $scope.MinSampaiTanggalListBrosur_Raw1 = $scope.getDataSO[0].InvoiceStartEffectiveDate;
            //$scope.MinSampaiTanggalListBrosur1=$scope.MinSampaiTanggalListBrosur_Raw1.getFullYear()+'-'+('0' + ($scope.MinSampaiTanggalListBrosur_Raw1.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MinSampaiTanggalListBrosur_Raw1.getDate()).slice(-2);

            $scope.MaintainBillingdateOptionsSampai1.minDate = $scope.MinSampaiTanggalListBrosur_Raw1;
            if ($scope.getDataSO[0].InvoiceStartEffectiveDate >= $scope.getDataSO[0].InvoiceEndEffectiveDate) {
                $scope.getDataSO[0].InvoiceEndEffectiveDate = $scope.getDataSO[0].InvoiceStartEffectiveDate;
            }
        }

        $scope.MaintainBillingTanggalEfektifFakturChanged2 = function (date) {
            $scope.MaintainBillingTanggalEfektifFakturChanged2.minDate = date;
            $scope.MinSampaiTanggalListBrosur_Raw2 = $scope.getDataSO[0].InvoiceStartEffectiveDate;
            //$scope.MinSampaiTanggalListBrosur2=$scope.MinSampaiTanggalListBrosur_Raw2.getFullYear()+'-'+('0' + ($scope.MinSampaiTanggalListBrosur_Raw2.getMonth()+1)).slice(-2)+'-'+('0' + $scope.MinSampaiTanggalListBrosur_Raw2.getDate()).slice(-2);

            $scope.MaintainBillingdateOptionsSampai2.minDate = $scope.MinSampaiTanggalListBrosur_Raw2;
            if ($scope.getDataSO[0].InvoiceStartEffectiveDate >= $scope.getDataSO[0].InvoiceEndEffectiveDate) {
                $scope.getDataSO[0].InvoiceEndEffectiveDate = $scope.getDataSO[0].InvoiceStartEffectiveDate;
            }
        }



        $scope.btnBuatBilling = function () {
            console.log("iniiii",  $scope.selectedRow);

            console.log('------------------- tambah billlllllll --------------------------')
            $scope.dataPriceCheck = null
            $scope.adaPerubahan = 0;


            MaintainBillingFactory.getValidasiBilling($scope.selectedRow[0].SOId).then(
                function (res){
                    if(res.data.Result[0].isValidBilling==false){              
                        bsNotify.show({
                                title: "Peringatan",
                                content: res.data.Result[0].Message,
                                type: 'warning'   
                        });
                    } else if (res.data.Result[0].isValidBilling==true){
                        $scope.breadcrums.title = "Buat";
                        $scope.MaintainBillingUdaKalkulasi = "Belom";
                        $scope.formValidBuat.$setUntouched();
                        $scope.MaintainBillingdateOptionsStart1.minDate = $scope.TodayDate;
                        $scope.MaintainBillingdateOptionsStart2.minDate = $scope.TodayDate;
            
            
                        $scope.flagStatus = "InsertSO";
                        $scope.ShowbtnSimpan = true;
                        $scope.btnKembali = true;
                        $scope.btnCetak = true;
                        $scope.ViewBillingPelanggan = false;
                        $scope.ViewBillingDokumen = false;
                        $scope.ViewBillingPengiriman = false;
                        $scope.ViewBillingHarga = false;
                        $scope.ViewBillingNamaBilling = false;
                        $scope.btntambah = true;
                        $scope.Selectdata = $scope.selectedRow[0];
                        if ($scope.Selectdata.SOTypeName == "OPD" || $scope.Selectdata.SOTypeId == 2) {
                            $scope.BillingDokumen = true;
                            $scope.BillingInfoPelanggan = false;//tadinya false
                        } else if ($scope.Selectdata.SOTypeName == "Finish Unit" && $scope.Selectdata.SOTypeId == 1) {
                            $scope.BillingInfoPelanggan = true;
                            $scope.BillingDokumen = true;
                        } else if ($scope.Selectdata.SOTypeName == "Purna Jual" && $scope.Selectdata.SOTypeId == 3){
                            $scope.BillingInfoPelanggan = false;
                            $scope.BillingDokumen = false;
                        }
            
                        $scope.Selectdatarow.SoCode = $scope.Selectdata.SoCode;
                        $scope.Selectdatarow.SoDate = $scope.Selectdata.SoDate;
                        $scope.Selectdatarow.SOTypeId = $scope.Selectdata.SOTypeId;
                        $scope.Selectdatarow.CustomerTypeId = $scope.Selectdata.CustomerTypeId;
                        if ($scope.Selectdata.IsDPPaid == null || $scope.Selectdata.IsDPPaid == false) {
                            if ($scope.Selectdata.SOTypeId == 1) {
                                if ($scope.Selectdata.IsBillingBeforeDPPaidApproved != null) {
                                    $scope.getDataSOtobilling();
                                    console.log("Approved tanpa DP");
                                } else {
            
                                    bsNotify.show({
                                        title: "Peringatan",
                                        content: "Belum bayar DP.",
                                        type: 'warning'
                                    });
                                    $scope.disablebuttoneBuatBilling = true;
                                    $scope.maintainBillingForm = true;
                                    $scope.buatBillingForm = false;
                                }
                                if ($scope.Selectdata.BillingAFIParameter == 3 && $scope.Selectdata.IsAFICreated == false) {
            
                                    bsNotify.show({
                                        title: "Peringatan",
                                        content: "Belum ada AFI.",
                                        type: 'warning'
                                    });
                                    $scope.disablebuttoneBuatBilling = true;
                                    $scope.maintainBillingForm = true;
                                    $scope.buatBillingForm = false;
                                } else {
                                    console.log("Sudah AFI.!");
                                }
                            } else {
                                $scope.getDataSOtobilling();
                                console.log("OPD & Purna Jual");
                            }
                        } else {
                            $scope.getDataSOtobilling();
                            console.log("Get Data");
                        }
                    }

                }
            )
        }

        //tambahan
        $scope.pilihFrameNo = function (selected) {
            $scope.resultFrame = selected;
            $scope.ViewData.FrameNo = $scope.resultFrame.FrameNo;
        }


        $scope.getDataSOtobilling = function () {

            $scope.disablebuttoneBuatBilling = false;
            var getSO = "?SOId=" + $scope.Selectdata.SOId;
            MaintainBillingFactory.getDataSO(getSO).then(
                function (res) {
                    $scope.getDataSO = res.data.Result;
                    // $scope.getAccStandard = $scope.getDataSO.listofAccessoriesStandar;
                    console.log("getDataSO==>",$scope.getDataSO);
                    $scope.getAccStandard = $scope.getDataSO[0].listofAccessoriesStandar;
                    $scope.buatBillingForm = true;
                    $scope.maintainBillingForm = false;
                    console.log("getAccStandard==>",$scope.getAccStandard);
                    // $scope.onNamaLeasingSelected($scope.getDataSO[0].LeasingId);
                    // $scope.onTenorLeasingSelected($scope.getDataSO[0].LeasingLeasingTenorId);
                  



                    MaintainBillingFactory.getDataTenor($scope.getDataSO[0].LeasingId).then(function (res) {
                        for (var i = 0; i < res.data.Result.length; ++i) {
                            if (res.data.Result[i].LeasingId == $scope.getDataSO[0].LeasingId) {

                                $scope.optionsLeasingTenor = res.data.Result[i].ListLeasingTenor;
                                console.log('$scope.tenor', $scope.optionsLeasingTenor);
                                break;
                            }
                        }

                    });

                    //$scope.getDataSO[0].InvoiceStartEffectiveDate = new Date();
                    //$scope.getDataSO[0].InvoiceEndEffectiveDate = new Date();
                    $scope.gridDokumenPembetulanMaintainBilling = $scope.getDataSO[0].ListOfSODocumentView;


                    if ($scope.getDataSO[0].ARRefundLeasing == null && $scope.getDataSO[0].ARRefundInsurance == null){
                        $scope.getDataSO[0].ARRefundLeasing = 0;
                        $scope.getDataSO[0].ARRefundInsurance = 0;
                    }


                    $scope.getDataSO[0].IsCustomerEqualBuyer = $scope.getDataSO[0].IsCustomerEqualBuyer;
                    $scope.getDataSO[0].NameinBilling.CustomerName = angular.copy($scope.getDataSO[0].BuyerInformation.CustomerName);
                    $scope.getDataSO[0].NameinBilling.Npwp = angular.copy($scope.getDataSO[0].BuyerInformation.NPWPNo);
                    $scope.getDataSO[0].NameinBilling.CityRegencyId = angular.copy($scope.getDataSO[0].BuyerInformation.CityRegencyId);
                    // $scope.getDataSO[0].NameinBilling.PaymentTypeId = angular.copy($scope.getDataSO[0].ListOfSODetailPriceView[0].PaymentTypeId);
                    $scope.getDataSO[0].NameinBilling.CustomerPositionId = angular.copy($scope.getDataSO[0].BuyerInformation.CustomerPositionId);
                    $scope.getDataSO[0].NameinBilling.Address = angular.copy($scope.getDataSO[0].BuyerInformation.CustomerAddress);
                    $scope.getDataSO[0].NameinBilling.DistrictId = angular.copy($scope.getDataSO[0].BuyerInformation.DistrictId);
                    //$scope.getDataSO[0].NameinBilling.BankId = angular.copy($scope.getDataSO[0].BuyerInformation.BankId);
                    $scope.getDataSO[0].NameinBilling.Phone1 = angular.copy($scope.getDataSO[0].BuyerInformation.PhoneNumber);
                    $scope.getDataSO[0].NameinBilling.RT = angular.copy($scope.getDataSO[0].BuyerInformation.RT);
                    $scope.getDataSO[0].NameinBilling.RW = angular.copy($scope.getDataSO[0].BuyerInformation.RW);
                    $scope.getDataSO[0].NameinBilling.VillageId = angular.copy($scope.getDataSO[0].BuyerInformation.VillageId);
                    $scope.getDataSO[0].NameinBilling.Npwp = angular.copy($scope.getDataSO[0].BuyerInformation.NPWPNo);
                    $scope.getDataSO[0].NameinBilling.Email = angular.copy($scope.getDataSO[0].BuyerInformation.Email);
                    $scope.getDataSO[0].NameinBilling.ProvinceId = angular.copy($scope.getDataSO[0].BuyerInformation.ProvinceId);
                    $scope.getDataSO[0].NameinBilling.VillageId = angular.copy($scope.getDataSO[0].BuyerInformation.VillageId);

                    $scope.getDataSO[0].GrandTotalUnit = angular.copy($scope.getDataSO[0].ListInfoUnit[0].GrandTotalUnit);
                    console.log('ini data dari SO', $scope.getDataSO[0])


                    //tadi di bahah ini ga ada
                    if ($scope.getDataSO[0].SOTypeName == 'Finish Unit' && $scope.getDataSO[0].FundSourceName == 'Cash') {
                        $scope.getDataSO[0].ARRefundLeasing = 0;
                        $scope.flagSumberDana = 'Cash';
                    }
                    else {
                        $scope.flagSumberDana = 'Credit';
                    }
                    //disabled AR Refund Insurance
                    if($scope.getDataSO[0].InsuranceId == null || $scope.getDataSO[0].InsuranceId == undefined){
                        $scope.getDataSO[0].ARRefundInsurance = 0;
                    }

                    if ($scope.getDataSO[0].NameinBilling.Phone1 == null) {
                        $scope.getDataSO[0].NameinBilling.Phone1 = angular.copy($scope.getDataSO[0].BuyerInformation.HpNumber);
                    }

                    var BankLagi = "?BankId=" + $scope.getDataSO[0].BankId;
                    MaintainBillingFactory.getNoRek(BankLagi).then(
                        function (res) {
                            $scope.selectRek = res.data.Result;
                            $scope.getDataSO[0].NameinBilling.AccountNo = $scope.selectRek[0].AccountNo;
                            //return res.data;
                        }
                    );

                    //$scope.ViewBillingNamaBilling = true;
                    if ($scope.getDataSO[0].SOTypeId == 1) {
                        $scope.ShowInfoPelanggan = true;
                        $scope.BillingInfoPelanggan = true;
                        $scope.BillingDokumen = true;
                        $scope.DisabledInfoPengiriman = true;
                        if ($scope.getDataSO[0].CustomerTypeDesc == 'Individu') {
                            $scope.IndividuPemilik = true;
                            $scope.disabledPemilik = true;
                            $scope.InstansiPemilik = false;
                        } else {
                            $scope.IndividuPemilik = false;
                            $scope.disabledPemilik = true;
                            $scope.InstansiPemilik = true;
                        }
                    } else {
                        $scope.ShowInfoPelanggan = false;
                        $scope.BillingInfoPelanggan = false;
                        if ($scope.getDataSO[0].SOTypeId == 1 || $scope.getDataSO[0].SOTypeId == 2) {
                            $scope.BillingDokumen = true;
                        } else {
                            $scope.BillingDokumen = false;
                        }

                        $scope.showInfoPengiriman = false;
                        $scope.DisabledInfoPengiriman = false;

                    }

                    console.log('$scope.getDataSO ===>', $scope.getDataSO[0]);

                    
                    angular.element("#MaintainBillingBuatBillingTab_InfoPelanggan_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_Dokumen_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InfoPengiriman_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_RincianHarga_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_NamaBilling_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InformasiPembayaran_MaintainBilling").removeClass("active").addClass("");

                    angular.element("#MaintainBillingBuatBillingTab_InfoPengiriman_MaintainBilling").addClass("active");
                    angular.element("#MaintainBillingBuatBillingTab_Dokumen_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InfoPelanggan_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_RincianHarga_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_NamaBilling_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InformasiPembayaran_MaintainBilling").removeClass("active").addClass("");

                    $scope.MaintainBillingTanggalEfektifFakturChanged1($scope.getDataSO[0].InvoiceStartEffectiveDate);
                    $scope.MaintainBillingTanggalEfektifFakturChanged2($scope.getDataSO[0].InvoiceStartEffectiveDate);


                    //bawah ini function javascript
                    navigateTab('tabContainerBill', 'menuInfoPengirimanBill');

                    if ($scope.getDataSO[0].ListOfSOCustomerInformationView[2].SIUPNo != null && $scope.getDataSO[0].ListOfSOCustomerInformationView[2].TDPNo == null) {
                        $scope.DisabledKTP = true;
                    } else if ($scope.getDataSO[0].ListOfSOCustomerInformationView[2].SIUPNo == null && $scope.getDataSO[0].ListOfSOCustomerInformationView[2].TDPNo != null) {
                        $scope.DisabledKTP = true;
                    } else if ($scope.getDataSO[0].ListOfSOCustomerInformationView[2].SIUPNo != null && $scope.getDataSO[0].ListOfSOCustomerInformationView[2].TDPNo != null) {
                        $scope.DisabledKTP = true;
                    } else {
                        $scope.DisabledKTP = false;
                    }

                    if ($scope.getDataSO[0].ListOfSOCustomerInformationView[2].KTPNo != null) {
                        $scope.DisabledKitas = true;
                    } else {
                        $scope.DisabledKitas = false;
                    }


                    



                }
            );
        }

        $scope.cbSelected = function () {
            if ($scope.getDataSO[0].IsCustomerEqualBuyer) { // when checked
                $scope.cobadulu = $scope.getDataSO[0];
                $scope.getDataSO[0].IsCustomerEqualBuyer = true;

                $scope.getDataSO[0].NameinBilling.CustomerName = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].CustomerName);
                $scope.getDataSO[0].NameinBilling.Npwp = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].NPWPNo);
                $scope.getDataSO[0].NameinBilling.CityRegencyId = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].CityRegencyId);
                //$scope.getDataSO[0].NameinBilling.PaymentTypeId = angular.copy($scope.cobadulu.ListOfSODetailPriceView[0].PaymentTypeId);
                $scope.getDataSO[0].NameinBilling.CustomerPositionId = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].CustomerPositionId);
                $scope.getDataSO[0].NameinBilling.Address = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].CustomerAddress);
                $scope.getDataSO[0].NameinBilling.DistrictId = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].DistrictId);
                //$scope.getDataSO[0].NameinBilling.BankId = angular.copy($scope.cobadulu.ListOfSODetailPriceView[0].BankId);
                $scope.getDataSO[0].NameinBilling.Phone1 = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].PhoneNumber);
                $scope.getDataSO[0].NameinBilling.RT = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].RT);
                $scope.getDataSO[0].NameinBilling.RW = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].RW);
                $scope.getDataSO[0].NameinBilling.VillageId = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].VillageId);
                $scope.getDataSO[0].NameinBilling.Npwp = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].NPWPNo);
                $scope.getDataSO[0].NameinBilling.Email = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].Email);
                $scope.getDataSO[0].NameinBilling.ProvinceId = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].ProvinceId);
                $scope.getDataSO[0].NameinBilling.VillageId = angular.copy($scope.cobadulu.ListOfSOCustomerInformationView[0].VillageId);
                $scope.ViewBillingNamaBilling = true;
            } else {
                $scope.getDataSO[0].IsCustomerEqualBuyer = false;
                $scope.getDataSO[0].NameinBilling.CustomerName = "";
                $scope.getDataSO[0].NameinBilling.Npwp = "";
                $scope.getDataSO[0].NameinBilling.CityRegencyId = "";
                $scope.getDataSO[0].NameinBilling.PaymentTypeId = "";
                $scope.getDataSO[0].NameinBilling.CustomerPositionId = "";
                $scope.getDataSO[0].NameinBilling.Address = "";
                $scope.getDataSO[0].NameinBilling.DistrictId = "";
                $scope.getDataSO[0].NameinBilling.BankId = "";
                $scope.getDataSO[0].NameinBilling.Phone1 = "";
                $scope.getDataSO[0].NameinBilling.RT = "";
                $scope.getDataSO[0].NameinBilling.VillageId = "";
                $scope.getDataSO[0].NameinBilling.Npwp = "";
                $scope.getDataSO[0].NameinBilling.Email = "";
                $scope.getDataSO[0].NameinBilling.ProvinceId = "";
                $scope.getDataSO[0].NameinBilling.VillageId = "";
                $scope.ViewBillingNamaBilling = false;
            }
        };

        $scope.PilihMetode = function (selected) {
            $scope.Metod = selected;
            if ($scope.Metod.PaymentTypeName == "Cash") {
                $scope.disableBank = true;
            } else {
                $scope.disableBank = false;
            }
        }

        $scope.pilihBank = function (selected) {

            $scope.bankTemp = selected;
            var Bank = "?BankId=" + $scope.bankTemp.BankId;

            MaintainBillingFactory.getNoRek(Bank).then(
                function (res) {
                    $scope.selectRek = res.data.Result;
                    $scope.getDataSO[0].NameinBilling.AccountNo = $scope.selectRek[0].AccountNo;
                    return res.data;
                }
            );
        }

        $scope.ListMasterDokumen = function () {
            MaintainSOFactory.getDataDokumen().then(
                function (res) {
                    $scope.gridDataDokumenBill.data = res.data.Result;
                    $scope.loading = false;
                },
                function (err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.btnTambahDokumenBill = function () {
            angular.element('.ui.modal.DokumenBill').modal('show');
            $scope.ListMasterDokumen();
        }

        $scope.btnBatalDoc = function () {
            angular.element('.ui.modal.DokumenBill').modal('hide');
        }

        $scope.btnPilihDokumenBill = function () {
            angular.element('.ui.modal.DokumenBill').modal('hide');
            var sque = 0;
            if ($scope.gridDokumenPembetulanMaintainBilling.length == 0) {
                for (var i in $scope.selctedRow) {
                    sque++;
                    $scope.gridDokumenPembetulanMaintainBilling.push({ No: sque, DocumentId: $scope.selctedRow[i].DocumentId, DocumentType: $scope.selctedRow[i].DocumentType, DocumentURL: "", StatusDocumentName: "Empty" });
                }
            } else {
                for (var i in $scope.gridDokumenPembetulanMaintainBilling) {
                    for (var j in $scope.selctedRow) {
                        if ($scope.selctedRow[j].DocumentId == $scope.gridDokumenPembetulanMaintainBilling[i].DocumentId) {
                            $scope.selctedRow.splice(j, 1);
                        }
                    }
                }
                for (var i in $scope.selctedRow) {
                    sque++;
                    $scope.gridDokumenPembetulanMaintainBilling.push({ No: sque, DocumentId: $scope.selctedRow[i].DocumentId, DocumentType: $scope.selctedRow[i].DocumentType, DocumentURL: "", StatusDocumentName: "Empty" });
                }
            }
            $scope.selctedRow = [];
        }

        $scope.modalKonfirmasiPerubahanHargaMaintainBillingKembali = function () {
            angular.element('.ui.modal.modalKonfirmasiPerubahanHargaMaintainBilling').modal('hide');
            $scope.disabledBtnSimpanCetak = false;
            $scope.onProsesClickButtonCetak =false;
            $scope.adaPerubahan = 0;
        }

        $scope.ButtonSimpan = function () {

            console.log('--------------------------------- simpan billllll ---------------------------------------------------')
            $scope.onProsesClickButtonCetak= true; //fitri

            $scope.dataPriceCheck = null

            $scope.savingStateBilling = true;
            MaintainBillingSemuaAdaFile = true;
            $scope.getDataSO[0].NameinBilling.BankAccountNumber = $scope.getDataSO[0].NameinBilling.AccountNo;
            var MaintainBillingSemuaAdaFile = true;
            angular.forEach($scope.getDataSO[0].ListOfSODocumentView, function (Doc, DocIndx) {
                if (Doc.StatusDocumentName != "Upload") {
                    if (Doc.DocumentData != null) {
                        Doc.DocumentData.UpDocObj = btoa(Doc.DocumentData.UpDocObj);

                    } else {
                        console.log("UpDocObj => Kosong");
                        MaintainBillingSemuaAdaFile = false;
                    }
                }

            });


            if (($scope.getDataSO[0].ListOfSODocumentView.length <= 0 && ($scope.Selectdatarow.SOTypeName == "Swapping" || $scope.Selectdatarow.SOTypeName == "Purna Jual")) || MaintainBillingSemuaAdaFile == false) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Tolong upload dokumen",
                    type: 'warning'
                });
                $scope.onProsesClickButtonCetak= false; //fitri

                MaintainBillingSemuaAdaFile = false;
                $scope.savingStateBilling = false;
            } else {

                $scope.getDataSO[0].IsCustomerEqualBuyer = true;
                //$scope.cbSelected();
                $scope.MaintainBillingModeSimpan = "Biasa";

                if ($scope.getDataSO[0].SOTypeId == 1 && $scope.MaintainBillingUdaKalkulasi == "Belom") {
                    MaintainBillingFactory.getBillingCheck($scope.getDataSO[0].SOId).then(
                        function (res) {
                            $scope.MaintainBillingHasilCekPerubahanHarga = "";
                            if (res.data[0].PriceCheck != 0) {
                                $scope.dataPriceCheck = res.data[0]
                                $scope.MaintainBillingHasilCekPerubahanHarga = angular.copy(res.data[0].PriceCheck);
                                $scope.MaintainBillingUdaKalkulasi = "Belom";
                                setTimeout(function () {
                                    angular.element('.ui.modal.modalKonfirmasiPerubahanHargaMaintainBilling').modal('setting', { closable: false }).modal('show');
                                    angular.element('.ui.modal.modalKonfirmasiPerubahanHargaMaintainBilling').not(':first').remove();
                                }, 1);
                            }
                            else {
                                MaintainBillingFactory.create($scope.getDataSO).then(
                                    function () {
                                        try {
                                            if ($scope.mDaftarSO.Id == 1) {
                                                $scope.getData();
                                            } else if ($scope.mDaftarSO.Id == 2) {
                                                $scope.getDatawesbilling();
                                            } else {
                                                $scope.mDaftarSO.Id = 1;
                                                $scope.getData();
                                            }
                                        } catch (DaSalah) {
                                            $scope.mDaftarSO.Id = 1;
                                            $scope.getData();
                                        }

                                        bsNotify.show({
                                            title: "Berhasil",
                                            content: "Data berhasil disimpan.",
                                            type: 'success'
                                            
                                        });
                                        $scope.onProsesClickButtonCetak= false; //fitri
   
                                        $scope.MaintainBillingUdaKalkulasi = "Belom";
                                        $scope.buatBillingForm = false;
                                        $scope.maintainBillingForm = true;
                                        MaintainBillingSemuaAdaFile = false;
                                        $scope.savingStateBilling = false;
                                    },
                                    function (err) {
                                        // if(err.data.Message.indexOf("truncated") != -1)
                                        // {
                                        // bsNotify.show(
                                        // {
                                        // title: "error",
                                        // content: "Ukuran maksimal melebihi batas yang ditentukan",
                                        // type: 'danger'
                                        // }
                                        // );
                                        // }
                                        // else
                                        // {
                                        //err.data.Message

                                        // bsNotify.show({
                                        // title: "Error",
                                        // content: "Data gagal disimpan pastikan semua data mandatory diisi dan ada dokumen yang di upload ",
                                        // type: 'danger'
                                        // });

                                        bsNotify.show({
                                            title: "Gagal",
                                            content: err.data.Message,
                                            type: 'danger'
                                        });
                                        $scope.onProsesClickButtonCetak= false; //fitri
   
                                        //}

                                    }
                                );
                                MaintainBillingSemuaAdaFile = false;
                                $scope.savingStateBilling = false;
                            }



                        }
                    );
                }
                else if ($scope.getDataSO[0].SOTypeId == 1 && $scope.MaintainBillingUdaKalkulasi == "Uda") {
                    console.log('masuk sini 05');
                    // console.log('masuk sini 01',xxxxxx);
                    $scope.modalKonfirmasiPerubahanHargaMaintainBillingOkSimpan();
                }
                else {
                    console.log('masuk sini 04');
                    // console.log('masuk sini 01',xxxxxx);
                    MaintainBillingFactory.create($scope.getDataSO).then(
                        function () {
                            try {
                                if ($scope.mDaftarSO.Id == 1) {
                                    $scope.getData();
                                } else if ($scope.mDaftarSO.Id == 2) {
                                    $scope.getDatawesbilling();
                                } else {
                                    $scope.mDaftarSO.Id = 1;
                                    $scope.getData();
                                }
                            } catch (DaSalah) {
                                $scope.mDaftarSO.Id = 1;
                                $scope.getData();
                            }

                            bsNotify.show({
                                title: "Berhasil",
                                content: "Data berhasil disimpan.",
                                type: 'success'
                            });
                            $scope.onProsesClickButtonCetak= false; //fitri
   
                            $scope.MaintainBillingUdaKalkulasi = "Belom";
                            $scope.buatBillingForm = false;
                            $scope.maintainBillingForm = true;
                            MaintainBillingSemuaAdaFile = false;
                            $scope.savingStateBilling = false;
                        },
                        function (err) {
                            // if(err.data.Message.indexOf("truncated") != -1)
                            // {
                            // bsNotify.show(
                            // {
                            // title: "error",
                            // content: "Ukuran maksimal melebihi batas yang ditentukan",
                            // type: 'danger'
                            // }
                            // );
                            // }
                            // else
                            // {
                            //err.data.Message

                            // bsNotify.show({
                            // title: "Error",
                            // content: "Data gagal disimpan pastikan semua data mandatory diisi dan ada dokumen yang di upload ",
                            // type: 'danger'
                            // });

                            bsNotify.show({
                                title: "Gagal",
                                content: err.data.Message,
                                type: 'danger'
                            });
                            $scope.onProsesClickButtonCetak= false; //fitri
   

                            //}

                        }
                    );
                    MaintainBillingSemuaAdaFile = false;
                    $scope.savingStateBilling = false;
                }

            }

        }

        $scope.MaintainBilingSimpanUbah = function () {
            // $scope.onProsesClickButtonCetak =  true; //fitri
            $scope.savingStateBilling = true;
            $scope.dataPriceCheck = null

            console.log('$scope.savingStateBilling = true;', $scope.savingStateBillingxxxxxxx);
            $scope.getDataSO[0].NameinBilling.BankAccountNumber = $scope.getDataSO[0].NameinBilling.AccountNo;
            var MaintainBillingSemuaAdaFile = true;
            angular.forEach($scope.getDataSO[0].ListOfSODocumentView, function (Doc, DocIndx) {
                if (Doc.StatusDocumentName != "Upload") {
                    if (Doc.DocumentData != null) {
                        Doc.DocumentData.UpDocObj = btoa(Doc.DocumentData.UpDocObj);

                    } else {
                        console.log("UpDocObj => Kosong");
                        MaintainBillingSemuaAdaFile = false;
                        
                    }
                }

            });


            if (($scope.getDataSO[0].ListOfSODocumentView.length <= 0 && ($scope.Selectdatarow.SOTypeName == "Swapping" || $scope.Selectdatarow.SOTypeName == "Purna Jual")) || MaintainBillingSemuaAdaFile == false) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Tolong upload dokumen",
                    type: 'warning'
                });
                // $scope.onProsesClickButtonCetak =  false; //fitri
                $scope.savingStateBilling = false;
                
            } else {

                $scope.getDataSO[0].IsCustomerEqualBuyer = true;
                //$scope.cbSelected();
                $scope.MaintainBillingModeSimpan = "Ubah";

                if ($scope.getDataSO[0].SOTypeId == 1 && $scope.MaintainBillingUdaKalkulasi == "Belom") {
                    MaintainBillingFactory.getBillingCheck($scope.getDataSO[0].SOId).then(
                        function (res) {
                            $scope.MaintainBillingHasilCekPerubahanHarga = "";
                            if (res.data[0].PriceCheck != 0) {
                                $scope.dataPriceCheck = res.data[0]
                                $scope.MaintainBillingHasilCekPerubahanHarga = angular.copy(res.data[0].PriceCheck);
                                $scope.MaintainBillingUdaKalkulasi = "Belom";
                                setTimeout(function () {
                                    angular.element('.ui.modal.modalKonfirmasiPerubahanHargaMaintainBilling').modal('setting', { closable: false }).modal('show');
                                    angular.element('.ui.modal.modalKonfirmasiPerubahanHargaMaintainBilling').not(':first').remove();
                                }, 1);
                            }
                            else {
                                MaintainBillingFactory.update($scope.getDataSO).then(
                                    function () {
                                        try {
                                            if ($scope.mDaftarSO.Id == 1) {
                                                $scope.getData();
                                            } else if ($scope.mDaftarSO.Id == 2) {
                                                $scope.getDatawesbilling();
                                            } else {
                                                $scope.mDaftarSO.Id = 1;
                                                $scope.getData();
                                            }
                                        } catch (DaSalah) {
                                            $scope.mDaftarSO.Id = 1;
                                            $scope.getData();
                                        }

                                        bsNotify.show({
                                            title: "Berhasil",
                                            content: "Data berhasil disimpan.",
                                            type: 'success'
                                        });
                                        // $scope.onProsesClickButtonCetak =  false; //fitri
                                        $scope.MaintainBillingUdaKalkulasi = "Belom";
                                        $scope.buatBillingForm = false;
                                        $scope.maintainBillingForm = true;
                                        $scope.savingStateBilling = false;
                                    },
                                    function (err) {
                                        // if(err.data.Message.indexOf("truncated") != -1)
                                        // {
                                        // bsNotify.show(
                                        // {
                                        // title: "error",
                                        // content: "Ukuran maksimal melebihi batas yang ditentukan",
                                        // type: 'danger'
                                        // }
                                        // );
                                        // }
                                        // else
                                        // {
                                        //err.data.Message

                                        // bsNotify.show({
                                        // title: "Error",
                                        // content: "Data gagal disimpan pastikan semua data mandatory diisi dan ada dokumen yang di upload ",
                                        // type: 'danger'
                                        // });

                                        bsNotify.show({
                                            title: "Gagal",
                                            content: err.data.Message,
                                            type: 'danger'
                                        });
                                        // $scope.onProsesClickButtonCetak =  false; //fitri

                                        //}

                                    }
                                );
                                $scope.savingStateBilling = false;
                            }
                        }
                    );
                }
                else if ($scope.getDataSO[0].SOTypeId == 1 && $scope.MaintainBillingUdaKalkulasi == "Uda") {
                    $scope.modalKonfirmasiPerubahanHargaMaintainBillingOkSimpan();
                }
                else {
                    MaintainBillingFactory.update($scope.getDataSO).then(
                        function () {
                            try {
                                if ($scope.mDaftarSO.Id == 1) {
                                    $scope.getData();
                                } else if ($scope.mDaftarSO.Id == 2) {
                                    $scope.getDatawesbilling(); 
                                } else {
                                    $scope.mDaftarSO.Id = 1;
                                    $scope.getData();
                                }
                            } catch (DaSalah) {
                                $scope.mDaftarSO.Id = 1;
                                $scope.getData();
                            }

                            bsNotify.show({
                                title: "Berhasil",
                                content: "Data berhasil disimpan.",
                                type: 'success'
                            });
                            $scope.onProsesClickButtonCetak =  false; //fitri
                            $scope.MaintainBillingUdaKalkulasi = "Belom";
                            $scope.buatBillingForm = false;
                            $scope.maintainBillingForm = true;
                            $scope.savingStateBilling = false;
                        },
                        function (err) {
                            // if(err.data.Message.indexOf("truncated") != -1)
                            // {
                            // bsNotify.show(
                            // {
                            // title: "error",
                            // content: "Ukuran maksimal melebihi batas yang ditentukan",
                            // type: 'danger'
                            // }
                            // );
                            // }
                            // else
                            // {
                            //err.data.Message

                            // bsNotify.show({
                            // title: "Error",
                            // content: "Data gagal disimpan pastikan semua data mandatory diisi dan ada dokumen yang di upload ",
                            // type: 'danger'
                            // });

                            bsNotify.show({
                                title: "Gagal",
                                content: err.data.Message,
                                type: 'danger'
                            });
                            $scope.onProsesClickButtonCetak =  false; //fitri
     
                            //}

                        }
                    );
                    $scope.savingStateBilling = false;
                }

            }

        }

        $scope.savingStateBilling = false;
        $scope.btnSimpanCetak = function () {
            $scope.disabledBtnSimpanCetak = true;
            $scope.onProsesClickButtonCetak =  true;
            $scope.savingStateBilling = true;

            $scope.dataPriceCheck = null
            
            $scope.getDataSO[0].NameinBilling.BankAccountNumber = $scope.getDataSO[0].NameinBilling.AccountNo;
            var MaintainBillingSemuaAdaFile = true;
            angular.forEach($scope.getDataSO[0].ListOfSODocumentView, function (Doc, DocIndx) {
                if (Doc.StatusDocumentName != "Upload") {
                    if (Doc.DocumentData != null) {
                        Doc.DocumentData.UpDocObj = btoa(Doc.DocumentData.UpDocObj);
                    } else {
                        console.log("UpDocObj => Kosong");
                        MaintainBillingSemuaAdaFile = false;
                    }
                }

            });

            if (($scope.getDataSO[0].ARRefundLeasing == null || $scope.getDataSO[0].ARRefundLeasing == undefined || $scope.getDataSO[0].ARRefundLeasing == '') && 
                ($scope.getDataSO[0].ARRefundInsurance == null || $scope.getDataSO[0].ARRefundInsurance == undefined || $scope.getDataSO[0].ARRefundInsurance == '')) 
                {
                $scope.getDataSO[0].ARRefundLeasing = 0;
                $scope.getDataSO[0].ARRefundInsurance = 0;
            }


            if (($scope.getDataSO[0].ListOfSODocumentView.length <= 0 && ($scope.Selectdatarow.SOTypeName == "Swapping" || $scope.Selectdatarow.SOTypeName == "Purna Jual")) || MaintainBillingSemuaAdaFile == false) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Tolong upload dokumen",
                    type: 'warning'
                });
                $scope.disabledBtnSimpanCetak = false;
                $scope.onProsesClickButtonCetak =  false; //fitri
                $scope.savingStateBilling = false;
            } else {
                $scope.getDataSO[0].IsCustomerEqualBuyer = true;
                //$scope.cbSelected();
                $scope.MaintainBillingModeSimpan = "Cetak";

                console.log('$scope.MaintainBillingUdaKalkulasi', $scope.MaintainBillingUdaKalkulasi);
                console.log('$scope.getDataSO[0].SOTypeId', $scope.getDataSO[0].SOTypeId);
                if ($scope.getDataSO[0].SOTypeId == 1 && $scope.MaintainBillingUdaKalkulasi == "Belom") {
                    MaintainBillingFactory.getBillingCheck($scope.getDataSO[0].SOId).then(
                        function (res) {
                            $scope.MaintainBillingHasilCekPerubahanHarga = "";
                            if (res.data[0].PriceCheck != 0) {
                                $scope.dataPriceCheck = res.data[0]
                                $scope.MaintainBillingHasilCekPerubahanHarga = angular.copy(res.data[0].PriceCheck);
                                $scope.MaintainBillingUdaKalkulasi = "Belom";
                                setTimeout(function () {
                                    angular.element('.ui.modal.modalKonfirmasiPerubahanHargaMaintainBilling').modal('setting', { closable: false }).modal('show');
                                    angular.element('.ui.modal.modalKonfirmasiPerubahanHargaMaintainBilling').not(':first').remove();
                                }, 1);
                            }
                            else if ($scope.getDataSO[0].SOTypeId == 1 && $scope.MaintainBillingUdaKalkulasi == "Uda") {
                                console.log('masuk sini 01');
                                // console.log('masuk sini 01',xxxxxx);
                                $scope.modalKonfirmasiPerubahanHargaMaintainBillingOkSimpan();
                            }
                            else {
                                console.log('masuk sini 02');
                                // console.log('masuk sini 01',xxxxxx);
                                MaintainBillingFactory.create($scope.getDataSO).then(
                                    function () {
                                        bsNotify.show({
                                            title: "Berhasil",
                                            content: "Data berhasil di simpan.",
                                            type: 'success'
                                        });
                                        $scope.disabledBtnSimpanCetak = false;
                                        $scope.onProsesClickButtonCetak =  false; //fitri
                                        $scope.MaintainBillingUdaKalkulasi = "Belom";
                                        $scope.getData();
                                        $scope.buatBillingForm = false;
                                        $scope.maintainBillingForm = true;
                                        $scope.savingStateBilling = false;

                                        var getId = "?SOId=" + $scope.getDataSO[0].SOId;
                                        MaintainBillingFactory.getBillId(getId).then(
                                            function (res) {
                                                $scope.getBlillID = res.data.Result;
                                                //($scope.getBlillID[0].FrameNo == NULL || $scope.getBlillID[0].FrameNo == "" ? "":$scope.getBlillID[0].FrameNo)

                                                $scope.DaMaintainBillingFrameNoString = "";

                                                try {
                                                    if ($scope.getBlillID[0].FrameNo != null && $scope.getBlillID[0].FrameNo != "" && $scope.getBlillID[0].FrameNo.length > 0) {
                                                        $scope.DaMaintainBillingFrameNoString = "&FrameNo=" + $scope.getBlillID[0].FrameNo
                                                    } else {
                                                        $scope.DaMaintainBillingFrameNoString = "";
                                                    }
                                                } catch (ExceptionDaMaintainBillingFrameNoString) {
                                                    $scope.DaMaintainBillingFrameNoString = "";
                                                }


                                                if ($scope.getDataSO[0].SOTypeId == 1) {
                                                    $scope.ctkBill = "sales/FakturKendaraanBaru?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                                } else if ($scope.getDataSO[0].SOTypeId == 2) {
                                                    //$scope.ctkBill = "sales/FakturOPD?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                                    $scope.ctkBill = "sales/FakturOPD?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                                } else if ($scope.getDataSO[0].SOTypeId == 3) {
                                                    $scope.ctkBill = "sales/FakturPurnaJual?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                                } else {
                                                    //api/17&FrameNo=U299823123
                                                    $scope.ctkBill = "sales/FakturKendaraanSwapping?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                                }

                                                //Call PrintRpt
                                                var pdfFile = null;
                                                PrintRpt.print($scope.ctkBill).success(function (res) {
                                                    var file = new Blob([res], { type: 'application/pdf' });
                                                    var fileURL = URL.createObjectURL(file);
                                                    pdfFile = fileURL;
                                                    $scope.onProsesClickButtonCetak =  false; //fitri
       
                                                    if (pdfFile != null)
                                                        printJS(pdfFile);
                                                    else
                                                        console.log("error cetakan", pdfFile);
                                                })
                                                    .error(function (res) {
                                                        console.log("error cetakan", pdfFile);
                                                    });

                                                return res.data;
                                            },
                                            function (err) {
                                                bsNotify.show(
                                                    {
                                                        title: "Gagal",
                                                        content: err.data.Message,
                                                        type: 'danger'
                                                    }
                                                );
                                                $scope.disabledBtnSimpanCetak = false;
                                                $scope.onProsesClickButtonCetak =  false; //fitri
       
                                                
                                            }
                                        );
                                        $scope.savingStateBilling = false;
                                    },
                                    function (err) {

                                        bsNotify.show({
                                            title: "Gagal",
                                            content: err.data.Message,
                                            type: 'danger'
                                        });
                                        $scope.disabledBtnSimpanCetak = false;
                                        $scope.onProsesClickButtonCetak =  false; //fitri
       
                                    }
                                );
                                // $scope.onProsesClickButtonCetak =  false; //fitri //tidak boleh menaruh fungsi ini di luar asynchronous, karena akan langsung terpanggil false, yg seharusnya menyelesaikan asynchronous terlebih dahulu
                                $scope.savingStateBilling = false;
                            }
                        }
                    );
                    
                    $scope.savingStateBilling = false;
                }
                else {
                    console.log('masuk sini 03');
                    // console.log('masuk sini 01',xxxxxx);
                    MaintainBillingFactory.SimpanAbisCek($scope.getDataSO).then(
                        function () {
                            bsNotify.show({
                                title: "Berhasil",
                                content: "Data berhasil di simpan.",
                                type: 'success'
                            });
                            $scope.disabledBtnSimpanCetak = false;
                            $scope.onProsesClickButtonCetak =  false; //fitri
       
                            $scope.MaintainBillingUdaKalkulasi = "Belom";
                            $scope.getData();
                            $scope.buatBillingForm = false;
                            $scope.maintainBillingForm = true;
                            $scope.savingStateBilling = false;

                            var getId = "?SOId=" + $scope.getDataSO[0].SOId;
                            MaintainBillingFactory.getBillId(getId).then(
                                function (res) {
                                    $scope.getBlillID = res.data.Result;
                                    //($scope.getBlillID[0].FrameNo == NULL || $scope.getBlillID[0].FrameNo == "" ? "":$scope.getBlillID[0].FrameNo)

                                    $scope.DaMaintainBillingFrameNoString = "";

                                    try {
                                        if ($scope.getBlillID[0].FrameNo != null && $scope.getBlillID[0].FrameNo != "" && $scope.getBlillID[0].FrameNo.length > 0) {
                                            $scope.DaMaintainBillingFrameNoString = "&FrameNo=" + $scope.getBlillID[0].FrameNo
                                        } else {
                                            $scope.DaMaintainBillingFrameNoString = "";
                                        }
                                    } catch (ExceptionDaMaintainBillingFrameNoString) {
                                        $scope.DaMaintainBillingFrameNoString = "";
                                    }


                                    if ($scope.getDataSO[0].SOTypeId == 1) {
                                        $scope.ctkBill = "sales/FakturKendaraanBaru?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                    } else if ($scope.getDataSO[0].SOTypeId == 2) {
                                        //$scope.ctkBill = "sales/FakturOPD?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                        $scope.ctkBill = "sales/FakturOPD?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                    } else if ($scope.getDataSO[0].SOTypeId == 3) {
                                        $scope.ctkBill = "sales/FakturPurnaJual?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                    } else {
                                        //api/17&FrameNo=U299823123
                                        $scope.ctkBill = "sales/FakturKendaraanSwapping?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                    }

                                    //Call PrintRpt
                                    var pdfFile = null;
                                    PrintRpt.print($scope.ctkBill).success(function (res) {
                                        var file = new Blob([res], { type: 'application/pdf' });
                                        var fileURL = URL.createObjectURL(file);
                                        pdfFile = fileURL;
                                        $scope.onProsesClickButtonCetak =  false; //fitri
       
                                        if (pdfFile != null)
                                            printJS(pdfFile);
                                        else
                                            console.log("error cetakan", pdfFile);
                                    })
                                        .error(function (res) {
                                            console.log("error cetakan", pdfFile);
                                        });

                                    return res.data;
                                },
                                function (err) {
                                    console.log(err);
                                }
                            );
                            $scope.savingStateBilling = false;
                        },
                        function (err) {

                            bsNotify.show({
                                title: "Gagal",
                                content: err.data.Message,
                                type: 'danger'
                            });
                            $scope.disabledBtnSimpanCetak = false;
                            $scope.onProsesClickButtonCetak =  false; //fitri
       
                        }
                    );
                    $scope.savingStateBilling = false;
                }

            }

            $scope.savingStateBilling = false;
        }

        $scope.modalKonfirmasiPerubahanHargaMaintainBillingOkSimpan = function () {
            if ($scope.MaintainBillingModeSimpan == "Biasa") {
                MaintainBillingFactory.SimpanAbisCek($scope.getDataSO).then(
                    function () {
                        try {
                            if ($scope.mDaftarSO.Id == 1) {
                                $scope.getData();
                            } else if ($scope.mDaftarSO.Id == 2) {
                                $scope.getDatawesbilling();
                            } else {
                                $scope.mDaftarSO.Id = 1;
                                $scope.getData();
                            }
                        } catch (DaSalah) {
                            $scope.mDaftarSO.Id = 1;
                            $scope.getData();
                        }

                        bsNotify.show({
                            title: "Berhasil",
                            content: "Data berhasil disimpan.",
                            type: 'success'
                        });
                          $scope.onProsesClickButtonCetak =  false; //fitri
      
                        $scope.buatBillingForm = false;
                        $scope.maintainBillingForm = true;
                    },
                    function (err) {
                        // if(err.data.Message.indexOf("truncated") != -1)
                        // {
                        // bsNotify.show(
                        // {
                        // title: "error",
                        // content: "Ukuran maksimal melebihi batas yang ditentukan",
                        // type: 'danger'
                        // }
                        // );
                        // }
                        // else
                        // {
                        //err.data.Message

                        // bsNotify.show({
                        // title: "Error",
                        // content: "Data gagal disimpan pastikan semua data mandatory diisi dan ada dokumen yang di upload ",
                        // type: 'danger'
                        // });

                        bsNotify.show({
                            title: "Gagal",
                            content: err.data.Message,
                            type: 'danger'
                        });
                        $scope.onProsesClickButtonCetak =  false; //fitri
      

                        //}

                    }
                );
            }
            else if ($scope.MaintainBillingModeSimpan == "Ubah") {
                MaintainBillingFactory.SimpanAbisCekUbah($scope.getDataSO).then(
                    function () {
                        try {
                            if ($scope.mDaftarSO.Id == 1) {
                                $scope.getData();
                            } else if ($scope.mDaftarSO.Id == 2) {
                                $scope.getDatawesbilling();
                            } else {
                                $scope.mDaftarSO.Id = 1;
                                $scope.getData();
                            }
                        } catch (DaSalah) {
                            $scope.mDaftarSO.Id = 1;
                            $scope.getData();
                        }

                        bsNotify.show({
                            title: "Berhasil",
                            content: "Data berhasil disimpan.",
                            type: 'success'
                        });
                        $scope.onProsesClickButtonCetak =  false; //fitri
      
                        $scope.buatBillingForm = false;
                        $scope.maintainBillingForm = true;
                    },
                    function (err) {
                        // if(err.data.Message.indexOf("truncated") != -1)
                        // {
                        // bsNotify.show(
                        // {
                        // title: "error",
                        // content: "Ukuran maksimal melebihi batas yang ditentukan",
                        // type: 'danger'
                        // }
                        // );
                        // }
                        // else
                        // {
                        //err.data.Message

                        // bsNotify.show({
                        // title: "Error",
                        // content: "Data gagal disimpan pastikan semua data mandatory diisi dan ada dokumen yang di upload ",
                        // type: 'danger'
                        // });

                        bsNotify.show({
                            title: "Gagal",
                            content: err.data.Message,
                            type: 'danger'
                        });
                        $scope.onProsesClickButtonCetak =  false; //fitri
     
                        //}

                    }
                );
            }
            else if ($scope.MaintainBillingModeSimpan == "Cetak") {
                MaintainBillingFactory.SimpanAbisCek($scope.getDataSO).then(
                    function () {
                        bsNotify.show({
                            title: "Berhasil",
                            content: "Data berhasil di simpan.",
                            type: 'success'
                        });
                        $scope.onProsesClickButtonCetak =  false; //fitri
     
                        $scope.getData();
                        $scope.buatBillingForm = false;
                        $scope.maintainBillingForm = true;

                        var getId = "?SOId=" + $scope.getDataSO[0].SOId;
                        MaintainBillingFactory.getBillId(getId).then(
                            function (res) {
                                $scope.getBlillID = res.data.Result;
                                //($scope.getBlillID[0].FrameNo == NULL || $scope.getBlillID[0].FrameNo == "" ? "":$scope.getBlillID[0].FrameNo)

                                $scope.DaMaintainBillingFrameNoString = "";

                                try {
                                    if ($scope.getBlillID[0].FrameNo != null && $scope.getBlillID[0].FrameNo != "" && $scope.getBlillID[0].FrameNo.length > 0) {
                                        $scope.DaMaintainBillingFrameNoString = "&FrameNo=" + $scope.getBlillID[0].FrameNo
                                    } else {
                                        $scope.DaMaintainBillingFrameNoString = "";
                                    }
                                } catch (ExceptionDaMaintainBillingFrameNoString) {
                                    $scope.DaMaintainBillingFrameNoString = "";
                                }


                                if ($scope.getDataSO[0].SOTypeId == 1) {
                                    $scope.ctkBill = "sales/FakturKendaraanBaru?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                } else if ($scope.getDataSO[0].SOTypeId == 2) {
                                    //$scope.ctkBill = "sales/FakturOPD?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                    $scope.ctkBill = "sales/FakturOPD?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                } else if ($scope.getDataSO[0].SOTypeId == 3) {
                                    $scope.ctkBill = "sales/FakturPurnaJual?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                } else {
                                    //api/17&FrameNo=U299823123
                                    $scope.ctkBill = "sales/FakturKendaraanSwapping?BillingId=" + $scope.getBlillID[0].BillingId + $scope.DaMaintainBillingFrameNoString;
                                }

                                //Call PrintRpt
                                var pdfFile = null;
                                PrintRpt.print($scope.ctkBill).success(function (res) {
                                    var file = new Blob([res], { type: 'application/pdf' });
                                    var fileURL = URL.createObjectURL(file);
                                    pdfFile = fileURL;
                                    $scope.onProsesClickButtonCetak =  false; //fitri
     
                                    if (pdfFile != null)
                                        printJS(pdfFile);
                                    else
                                        console.log("error cetakan", pdfFile);
                                })
                                    .error(function (res) {
                                        console.log("error cetakan", pdfFile);
                                    });

                                return res.data;
                            },
                            function (err) {
                                console.log(err);
                            }
                        );
                    },
                    function (err) {

                        bsNotify.show({
                            title: "Gagal",
                            content: err.data.Message,
                            type: 'danger'
                        });
                        $scope.onProsesClickButtonCetak =  false; //fitri
     
                    }
                );
            }
        }

        $scope.modalRincianHargaPerubahanHargaMaintainBillingKembali = function () {
            angular.element('.ui.modal.modalRincianHargaPerubahanHargaMaintainBilling').modal('hide');
        }

        $scope.modalRincianHargaPerubahanHargaMaintainBillingKalkulasi = function () {
            if ($scope.getDataSO[0].BuyerInformation.NPWPNo == null || $scope.getDataSO[0].BuyerInformation.NPWPNo == undefined || $scope.getDataSO[0].BuyerInformation.NPWPNo == '') {
                $scope.IsNPWPTrue = 0;
            } else {
                $scope.IsNPWPTrue = 1;
            }


            console.log('$scope.IsNPWPTrue ===>', $scope.IsNPWPTrue);

            var param = "";
            param = {
                "Title": null,
                "HeaderInputs": {
                    "$VehicleTypeColorId$": $scope.getDataSO[0].VehicleTypeColorId,
                    "$OutletId$": $scope.user.OutletId,
                    "$VehicleYear$": $scope.getDataSO[0].ProductionYear,
                    "$KodeTPajak$": $scope.getDataSO[0].CodeInvoiceTransactionTaxId,
                    "$BBN$": $scope.getDataSO[0].BBN,
                    "$BBNAdj$": $scope.getDataSO[0].BBNAdjustment,
                    "$BBNSAdj$": $scope.getDataSO[0].BBNServiceAdjustment,
                    "$TotalACC$": $scope.MaintainBillingTotalKaroseriTerbaru + $scope.MaintainBillingTotalAccessoriesTerbaru, // + $scope.MaintainBillingTotalAccessoriesPaketTerbaru,
                    "$Diskon$": $scope.getDataSO[0].ListInfoUnit[0].Discount,
                    "$MCommision$": $scope.getDataSO[0].MediatorCommision,
                    "$DSubsidiDP$": $scope.getDataSO[0].DiscountSubsidiDP,
                    "$DSubsidiRate$": $scope.getDataSO[0].DiscountSubsidiRate,
                    "$NPWP$": $scope.IsNPWPTrue,
                    "$OnOffTheRoadId$": $scope.getDataSO[0].OnOffTheRoadId,
                    "$FreePPNBit$": $scope.getDataSO[0].FreePPNBit,
                    "$FreePPnBMBit$": $scope.getDataSO[0].FreePPnBMBit,
                    "$VehicleId$": $scope.getDataSO[0].VehicleId,
                    "$KaroseriPrice$": $scope.MaintainBillingTotalKaroseriTerbaru,
                    "$KaroseriVAT$": $scope.getDataSO[0].KaroseriVAT,
                    "$KaroseriPPN$": $scope.getDataSO[0].KaroseriPPN,
                    "$CustomerId$" : $scope.getDataSO[0].CustomerId,
                    "$ProspectCode$" : $scope.getDataSO[0].ProspectCode,
                    "$ProspectId$" : $scope.getDataSO[0].ProspectId,
                    
                    

                },
                "BodyRows": [],
                "Cells": [],
                "Codes": {}

            };

            var UrlPricing = '';
            //finish unit by branch
            if ($scope.getDataSO[0].ToyotaId == null || $scope.getDataSO[0].ToyotaId == '' || typeof $scope.getDataSO[0].ToyotaId == 'undefined') {
                UrlPricing = '/api/fe/PricingEngine?PricingId=20003&OutletId='
            }
            else {
                //special customer
                UrlPricing = '/api/fe/PricingEngine?PricingId=20006&OutletId='
            }


            $http.post(UrlPricing + $scope.user.OutletId + '&DisplayId=1', param)
                .then(function (res) {
                    $scope.MaintainBillingGrandTotalTerbaru = angular.copy(parseFloat(res.data.Codes["[HargaMobil]"]));
                    $scope.MaintainBillingTotalBBNTerbaru = angular.copy(parseFloat(res.data.Codes["[BBN]"]));
                    $scope.getDataSO[0].GrandTotal = angular.copy(parseFloat(res.data.Codes["[GrandTotal]"]))
                    // $scope.getDataSO[0].GrandTotal = $scope.getDataSO[0].TotalInclVAT + $scope.MaintainBillingTotalKaroseriTerbaru + $scope.MaintainBillingTotalAccessoriesTerbaru; //GrandTotal
                    $scope.getDataSO[0].VehicleVAT = angular.copy(parseFloat(res.data.Codes["[PPN]"]));
                    // $scope.getDataSO[0].TotalVAT=angular.copy(res.data.Codes["[PPN]"]);
                    $scope.getDataSO[0].DPP = angular.copy(parseFloat(res.data.Codes["[DPP]"]));
                    $scope.getDataSO[0].PPH22 = angular.copy(parseFloat(res.data.Codes["[PPH22]"]));

                    $scope.getDataSO[0].BBN = angular.copy(parseFloat(res.data.Codes["[BBN]"]));
                    $scope.getDataSO[0].TotalBBN = angular.copy(parseFloat(res.data.Codes["[TotalBBN]"]));

                    $scope.getDataSO[0].TotalDPP = $scope.getDataSO[0].DPP + $scope.getDataSO[0].KaroseriDPP + $scope.totalDPPAcc;
                    $scope.getDataSO[0].TotalVAT = $scope.getDataSO[0].VehicleVAT + $scope.getDataSO[0].KaroseriPPN + $scope.totalPPNAcc;

                    $scope.getDataSO[0].TotalNonVehicle = $scope.getDataSO[0].KaroseriDPP + $scope.totalDPPAcc + $scope.getDataSO[0].KaroseriPPN + $scope.totalPPNAcc;
                    $scope.getDataSO[0].AccessoriesDPP = $scope.totalDPPAcc;
                    $scope.getDataSO[0].AccessoriesVAT =  $scope.totalPPNAcc;
                    // $scope.getDataSO[0].AccessoriesBBN = $scope.totalBBNAcc;
                    // $scope.getDataSO[0].AccessoriesTotalBBN =  $scope.totalBBNAcc_All;

                    $scope.getDataSO[0].VehiclePrice = angular.copy(parseFloat(res.data.Codes["[HargaMobil]"]));
                    $scope.getDataSO[0].PriceAfterDiscount = angular.copy(parseFloat(res.data.Codes["[PriceAfterDiskon]"]));
                    $scope.getDataSO[0].TotalInclVAT = angular.copy(parseFloat(res.data.Codes["[TotalHarga]"]));


                    // ---------------- jika ada perubahan harga saja --------------------------- start
                    if ($scope.adaPerubahan == 1) {
                        $scope.getDataSO[0].TotalDPP = $scope.getDataSO[0].DPP + $scope.getDataSO[0].KaroseriDPPNew + $scope.totalDPPAccNew;
                        $scope.getDataSO[0].TotalVAT = Math.floor ($scope.getDataSO[0].VehicleVAT + $scope.getDataSO[0].KaroseriPPNNew + $scope.totalPPNAccNew);
                        // // $scope.getDataSO[0].GrandTotal = angular.copy(parseFloat(res.data.Codes["[GrandTotal]"])) // masih ragu


                    }


                    // ---------------- jika ada perubahan harga saja --------------------------- end



                    $scope.MaintainBillingUdaKalkulasi = "Uda";

                    $scope.onProsesClickButtonCetak =  false; //fitri
                    $scope.disabledBtnSimpanCetak = false;

                    console.log('======================ini Finall==========================');
                    console.log('grandtotalFinal ===> TotalInclVAT + KarosTerbaru + AccTerbaru')
                    console.log('2. (perubahan Aksesoris Pkg).GrandTotal ===>', $scope.getDataSO[0].TotalInclVAT + ' + ' + $scope.MaintainBillingTotalKaroseriTerbaru + ' + ' + $scope.MaintainBillingTotalAccessoriesTerbaru + ' = ' + $scope.getDataSO[0].GrandTotal);

                    console.log('TotalNonVehicle ===>', $scope.getDataSO[0].TotalNonVehicle);
                    console.log('AccessoriesDPP  ===>', $scope.getDataSO[0].AccessoriesDPP);
                    console.log('AccessoriesVAT  ===>', $scope.getDataSO[0].AccessoriesVAT);
                    console.log('getDataSO  ========>', $scope.getDataSO[0]);
                    console.log('======isi dari Grand Total======');
                    console.log('$scope.getDataSO[0].TotalInclVAT',$scope.getDataSO[0].TotalInclVAT);
                    console.log('$scope.MaintainBillingTotalKaroseriTerbaru',$scope.MaintainBillingTotalKaroseriTerbaru);
                    console.log('$scope.MaintainBillingTotalAccessoriesTerbaru',$scope.MaintainBillingTotalAccessoriesTerbaru);

                });
                
                // $scope.onProsesClickButtonCetak =  false; //fitri
    
        }

        $scope.modalKonfirmasiPerubahanHargaMaintainBillingOk = function () {
            console.log('------------------------- kalkulasi billllll --------------------------------')
            $scope.adaPerubahan = 1;

            setTimeout(function () {
                angular.element('.ui.modal.modalRincianHargaPerubahanHargaMaintainBilling').modal('setting', { closable: false }).modal('show');
                angular.element('.ui.modal.modalRincianHargaPerubahanHargaMaintainBilling').not(':first').remove();
            }, 1);

            console.log('$scope.getDataSO[0] Sebelum', $scope.getDataSO[0]);
            $scope.MaintainBillingGrandTotalSebelumnya = angular.copy($scope.getDataSO[0].VehiclePrice);

            $scope.MaintainBillingTotalAccessoriesSebelumnya = angular.copy($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].GrandTotalAcc);

            $scope.MaintainBillingGrandTotalTerbaru = angular.copy("");

            $scope.MaintainBillingTotalBBNTerbaru = angular.copy("");

            $scope.MaintainBillingTotalAccessoriesTerbaru = 0;

            $scope.MaintainBillingTotalAccessoriesPaketTerbaru = 0;


            $scope.MaintainBillingTotalKaroseriSebelumnya = $scope.getDataSO[0].KaroseriPrice;

            $scope.MaintainBillingTotalBBNSebelumnya = $scope.getDataSO[0].BBN;
            
            $scope.totalDPPAcc = 0;
            $scope.totalPPNAcc = 0;

            
            $scope.totalDPPAccNew = 0;
            $scope.totalPPNAccNew = 0;

            // $scope.totalBBNAcc = 0; //sebelumnya BBN
            // $scope.totalBBNAcc_All = 0; //Sebelumnya TotalBBN
            

            $scope.MaintainBillingTotalKaroseriTerbaru = 0;

            //aksesoris ada berubah
            if ($scope.MaintainBillingHasilCekPerubahanHarga == 2 || $scope.MaintainBillingHasilCekPerubahanHarga == 3 || $scope.MaintainBillingHasilCekPerubahanHarga == 6 || $scope.MaintainBillingHasilCekPerubahanHarga == 7) {
                var BillingUrlAccessoriesSpk = '/api/fe/PricingEngine?PricingId=20001&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                var BillingUrlAccessoriesPackageSpk = '/api/fe/PricingEngine?PricingId=20002&OutletId=' + $scope.user.OutletId + '&DisplayId=1';

                $scope.MaintainBillingTampunganAcessoriesBedaHarga = [];
                $scope.MaintainBillingTampunganAcessoriesPackageBedaHarga = [];

                if ($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories != []) {

                   
                    angular.forEach($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories, function (acc, accIdx) {
                        
                        

                        // var UrlTembakHargaTerbaruAcc = "?VehicleTypeId=" + $scope.getDataSO[0].ListInfoUnit[0].VehicleTypeId + "&AccessoriesCode=" + $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].AccessoriesCode;
                        // MaintainBillingFactory.getHargaAccessoriesSatuan(UrlTembakHargaTerbaruAcc).then(
                        //         function (res) {
                        //             $scope.newDiscountAcc  = res.data.Result[0].RetailPrice;

                                

                                
                        //     } 
                        // )


                        if ($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].Discount > 0) {
                            console.log('ini kalo ada diskon ===>', $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].Discount);
                            var param = {
                                "Title": "Accessories",
                                "HeaderInputs": {
                                    "$AccessoriesId$": acc.AccessoriesId,
                                    "$QTY$": acc.Qty,
                                    // "$Discount$": $scope.newDiscountAcc,
                                    "$Discount$": acc.Discount,
                                    "$VehicleTypeId$": $scope.getDataSO[0].VehicleTypeId,
                                    "$OutletId$": $scope.user.OutletId,
                                    "$AccessoriesCode$": acc.AccessoriesCode,
                                }
                            };
                        } else {
                            console.log('ini kalo tidak ada diskon ===>', $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].Discount);
                            var param = {
                                "Title": "Accessories",
                                "HeaderInputs": {
                                    "$AccessoriesId$": acc.AccessoriesId,
                                    "$QTY$": acc.Qty,
                                    "$Discount$": acc.Discount,
                                    "$VehicleTypeId$": $scope.getDataSO[0].VehicleTypeId,
                                    "$OutletId$": $scope.user.OutletId,
                                    "$AccessoriesCode$": acc.AccessoriesCode,
                                }
                            };
                        }


                        $http.post(BillingUrlAccessoriesSpk, param)
                            .then(function (res) {
                                console.log('res aksesoris berubah ===>', res);
                                //$scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx]
                                $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].Price = angular.copy(parseFloat(res.data.Codes["#RetailPrice#"]));
                                $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].DPP = angular.copy(parseFloat(res.data.Codes["[DPP]"]));
                                $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].PPN = angular.copy(parseFloat(res.data.Codes["[PPN]"]));
                                $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].TotalInclVAT = angular.copy(parseFloat(res.data.Codes["[TotalHargaAcc]"]));

                                // Rollback SPK Galang - sebaris doang nih
                                // if ($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].Discount > 0) {
                                // $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].Discount = angular.copy(parseFloat(res.data.Codes["#RetailPrice#"]));
                                // }else{
                                //     $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories[accIdx].Discount = angular.copy(parseFloat(res.data.Codes["[Discount]"]));
                                // }



                                $scope.MaintainBillingTotalAccessoriesTerbaru += angular.copy(parseFloat(res.data.Codes["[TotalHargaAcc]"]));


                                $scope.totalDPPAcc += angular.copy(parseFloat(res.data.Codes["[DPP]"])) * angular.copy(parseFloat(res.data.Codes["[QTY]"]));
                                $scope.totalPPNAcc += angular.copy(parseFloat(res.data.Codes["[PPN]"])) * angular.copy(parseFloat(res.data.Codes["[QTY]"]));

                                // $scope.totalBBNAcc += angular.copy(parseFloat(res.data.Codes["[BBN]"])) * angular.copy(parseFloat(res.data.Codes["[QTY]"]));
                                // $scope.totalBBNAcc_All += angular.copy(parseFloat(res.data.Codes["[TotalBBN]"])) * angular.copy(parseFloat(res.data.Codes["[QTY]"]));

                                var accSatuanNew = angular.copy(parseFloat(res.data.Codes["[TotalHargaAcc]"]));
                                console.log('accSatuanNew ===>', accSatuanNew);

                                $scope.MaintainBillingGrandTotalSebelumnya = parseFloat($scope.MaintainBillingGrandTotalSebelumnya);
                                $scope.getDataSO[0].KaroseriPrice = parseFloat($scope.getDataSO[0].KaroseriPrice);
                                $scope.getDataSO[0].GrandTotal = parseFloat($scope.getDataSO[0].GrandTotal);


                                $scope.getDataSO[0].GrandTotal = $scope.MaintainBillingGrandTotalSebelumnya + $scope.getDataSO[0].KaroseriPrice + $scope.MaintainBillingTotalAccessoriesTerbaru;
                                console.log('1. New Acc GrandTotal ===>', $scope.MaintainBillingGrandTotalSebelumnya + ' + ' + $scope.getDataSO[0].KaroseriPrice + ' + ' + $scope.MaintainBillingTotalAccessoriesTerbaru + ' = ' + $scope.getDataSO[0].GrandTotal);
                                $scope.modalRincianHargaPerubahanHargaMaintainBillingKalkulasi();

                            })
                            
                        
                    });
                    

                }

                
                if ($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage != []) {
                    angular.forEach($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage, function (accp, accIdx) {
                // if ($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].listofAccessoriesStandar[0].ListAccessoriesPackage != []) {
                //     angular.forEach($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].listofAccessoriesStandar[0].ListAccessoriesPackage, function (accp, accIdx) {


                        // var UrlTembakHargaTerbaruAccPkg = "?VehicleTypeId=" + $scope.getDataSO[0].ListInfoUnit[0].VehicleTypeId + "&AccessoriesPackageId=" + $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].AccessoriesPackageId;
                        // MaintainBillingFactory.getHargaAccessoriesPaket(UrlTembakHargaTerbaruAccPkg).then(
                        //     function (res) {
                        //         $scope.newDiscountAccPkg = res.data.Result[0].RetailPrice;
                        //     }
                        // );

                        if ($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].Discount > 0) {
                            console.log('ini kalo ada diskon AccPkg ===>', $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].Discount);    
                            var param = {
                                "Title": "AccessoriesPackage",
                                "HeaderInputs": {
                                    "$AccPackageId$": accp.AccessoriesPackageId,
                                    "$QTY$": accp.Qty,
                                    // "$Discount$": $scope.newDiscountAccPkg,
                                    "$Discount$": accp.Discount,
                                    "$OutletId$": $scope.user.OutletId,
                                }
                            };
                        }else{
                            console.log('ini kalo tidak ada diskon AccPkg ===>', $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].Discount);
                            var param = {
                                "Title": "AccessoriesPackage",
                                "HeaderInputs": {
                                    "$AccPackageId$": accp.AccessoriesPackageId,
                                    "$QTY$": accp.Qty,
                                    "$Discount$": accp.Discount,
                                    "$OutletId$": $scope.user.OutletId,
                                }
                            };
                        }

                        
                        
                        

                        $http.post(BillingUrlAccessoriesPackageSpk, param)
                            .then(function (res) {
                                console.log('res aksesoris package berubah ===>', res);

                                //$scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx]
                                $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].Price = angular.copy(parseFloat(res.data.Codes["#RetailPrice#"]));
                                $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].DPP = angular.copy(parseFloat(res.data.Codes["[DPP]"]));
                                $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].PPN = angular.copy(parseFloat(res.data.Codes["[PPN]"]));
                                $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].TotalInclVAT = angular.copy(parseFloat(res.data.Codes["[TotalHargaAcc]"]));

                                 // Rollback SPK Galang - sebaris doang nih
                                // if ($scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].Discount > 0) {
                                    // $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].Discount = angular.copy(parseFloat(res.data.Codes["#RetailPrice#"]));
                                // }else{
                                //     $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage[accIdx].Discount = angular.copy(parseFloat(res.data.Codes["[Discount]"]));
                                // }
                                


                                $scope.MaintainBillingTotalAccessoriesTerbaru += angular.copy(parseFloat(res.data.Codes["[TotalHargaAcc]"]));

                                $scope.totalDPPAcc = angular.copy(parseFloat(res.data.Codes["[DPP]"])) * angular.copy(parseFloat(res.data.Codes["[QTY]"]));
                                $scope.totalPPNAcc = angular.copy(parseFloat(res.data.Codes["[PPN]"])) * angular.copy(parseFloat(res.data.Codes["[QTY]"]));
                                
                                // $scope.totalBBNAcc = angular.copy(parseFloat(res.data.Codes["[BBN]"])) * angular.copy(parseFloat(res.data.Codes["[QTY]"]));
                                // $scope.totalBBNAcc_All = angular.copy(parseFloat(res.data.Codes["[TotalBBN]"])) * angular.copy(parseFloat(res.data.Codes["[QTY]"]));

                                var accPackageNew = angular.copy(parseFloat(res.data.Codes["[TotalHargaAcc]"]));
                                console.log('accPackageNew ===>', accPackageNew);



                                $scope.MaintainBillingGrandTotalSebelumnya = parseFloat($scope.MaintainBillingGrandTotalSebelumnya);
                                $scope.getDataSO[0].KaroseriPrice = parseFloat($scope.getDataSO[0].KaroseriPrice);
                                $scope.getDataSO[0].GrandTotal = parseFloat($scope.getDataSO[0].GrandTotal);

                                $scope.getDataSO[0].GrandTotal = $scope.MaintainBillingGrandTotalSebelumnya + $scope.getDataSO[0].KaroseriPrice + $scope.MaintainBillingTotalAccessoriesTerbaru;
                                console.log('2. New Acc Pkg GrandTotal ===>', $scope.MaintainBillingGrandTotalSebelumnya + ' + ' + $scope.getDataSO[0].KaroseriPrice + ' + ' + $scope.MaintainBillingTotalAccessoriesTerbaru + ' = ' + $scope.getDataSO[0].GrandTotal);
                                $scope.modalRincianHargaPerubahanHargaMaintainBillingKalkulasi();

                            })
                    });


                }
            }
            else {
                // $scope.MaintainBillingTotalAccessoriesTerbaru = angular.copy($scope.MaintainBillingTotalAccessoriesSebelumnya);
                $scope.MaintainBillingTotalAccessoriesTerbaru = angular.copy($scope.dataPriceCheck.HargaAksesorisNew); // HargaAksesorisNew harus nya uda termasuk total aksesoris & pake aksesoris


                $scope.totalDPPAcc = 0;
                $scope.totalPPNAcc = 0;
                $scope.totalDPPAccNew = 0;
                $scope.totalPPNAccNew = 0;
                // $scope.totalBBNAcc = 0;
                // $scope.totalBBNAcc_All = 0;
                // galang
                var ListAcc = $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessories;
                var ListAccPkg = $scope.getDataSO[0].ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage;

                if (ListAcc != null || ListAcc != undefined || ListAcc != "") {
                    for (var i = 0; i < ListAcc.length; i++) {
                        $scope.totalDPPAcc += parseFloat(ListAcc[i].DPP) * parseFloat(ListAcc[i].Qty);
                        $scope.totalPPNAcc += parseFloat(ListAcc[i].PPN) * parseFloat(ListAcc[i].Qty);


                        $scope.totalDPPAccNew += parseFloat(ListAcc[i].DPPNew) * parseFloat(ListAcc[i].Qty);
                        $scope.totalPPNAccNew += parseFloat(ListAcc[i].PPNNew) * parseFloat(ListAcc[i].Qty);

                        // $scope.totalBBNAcc += parseFloat(ListAcc[i].BBN) * parseFloat(ListAcc[i].Qty);
                        // $scope.totalBBNAcc_All += parseFloat(ListAcc[i].TotalBBN) * parseFloat(ListAcc[i].Qty);
                    }
                }

                

                if (ListAccPkg != null || ListAccPkg != undefined || ListAccPkg != "") {
                    for (var i = 0; i < ListAccPkg.length; i++) {
                        $scope.totalDPPAcc += parseFloat(ListAccPkg[i].DPP) * parseFloat(ListAccPkg[i].Qty);
                        $scope.totalPPNAcc += parseFloat(ListAccPkg[i].PPN) * parseFloat(ListAccPkg[i].Qty);

                        $scope.totalDPPAccNew += parseFloat(ListAccPkg[i].DPPNew) * parseFloat(ListAccPkg[i].Qty);
                        $scope.totalPPNAccNew += parseFloat(ListAccPkg[i].PPNNew) * parseFloat(ListAccPkg[i].Qty);

                        $scope.MaintainBillingTotalAccessoriesPaketTerbaru += parseFloat(ListAccPkg[i].TotalInclVATNew)

                        // $scope.totalBBNAcc += parseFloat(ListAccPkg[i].BBN) * parseFloat(ListAccPkg[i].Qty);
                        // $scope.totalBBNAcc_All += parseFloat(ListAccPkg[i].TotalBBN) * parseFloat(ListAccPkg[i].Qty);
                    }
                }
                console.log('ini harga Acc dan acc Package Lama')
                console.log('$scope.totalDPPAcc Lama===>', $scope.totalDPPAcc);
                console.log('$scope.totalPPNAcc Lama===>', $scope.totalPPNAcc);

                console.log('$scope.totalDPPAcc New===>', $scope.totalDPPAccNew);
                console.log('$scope.totalPPNAcc New===>', $scope.totalPPNAccNew);

                // console.log('$scope.totalBBNAcc Lama===>', $scope.totalBBNAcc);
                // console.log('$scope.totalBBNAcc_All Lama===>', $scope.totalBBNAcc_All);




             

                $scope.MaintainBillingGrandTotalSebelumnya = parseFloat($scope.MaintainBillingGrandTotalSebelumnya);
                $scope.getDataSO[0].KaroseriPrice = parseFloat($scope.getDataSO[0].KaroseriPrice);
                $scope.getDataSO[0].GrandTotal = parseFloat($scope.getDataSO[0].GrandTotal);

                $scope.getDataSO[0].GrandTotal = $scope.MaintainBillingGrandTotalSebelumnya + $scope.getDataSO[0].KaroseriPrice + $scope.MaintainBillingTotalAccessoriesTerbaru;
                console.log('3. GrandTotal tak ada prubahan Acc & Acc Pkg ===>', $scope.MaintainBillingGrandTotalSebelumnya + ' + ' + $scope.getDataSO[0].KaroseriPrice + ' + ' + $scope.MaintainBillingTotalAccessoriesTerbaru + ' = ' + $scope.getDataSO[0].GrandTotal);
                $scope.modalRincianHargaPerubahanHargaMaintainBillingKalkulasi();

            }

            //karoseri ada berubah
            if ($scope.MaintainBillingHasilCekPerubahanHarga == 4 || $scope.MaintainBillingHasilCekPerubahanHarga == 5 || $scope.MaintainBillingHasilCekPerubahanHarga == 6 || $scope.MaintainBillingHasilCekPerubahanHarga == 7) {
                if ($scope.getDataSO[0].KaroseriId != null) {
                    var link = '/api/fe/PricingEngine/?PricingId=20004&OutletId=' + $scope.user.OutletId + '&DisplayId=1';
                    var param = {
                        "Title": "Karoseri",
                        "HeaderInputs": {
                            "$KaroseriId$": $scope.getDataSO[0].KaroseriId,
                            "$QTY$": 1,
                            "$VehicleTypeId$": $scope.getDataSO[0].VehicleTypeId,
                            "$OutletId$": $scope.user.OutletId,
                        }
                    };

                    $http.post(link, param)
                        .then(function (res) {
                            console.log('res Karoseri berubah ===>', res);

                            //$scope.getDataSO[0].KaroseriId
                            $scope.getDataSO[0].KaroseriPrice = angular.copy(parseFloat(res.data.Codes["[HargaKar]"]));
                            $scope.getDataSO[0].KaroseriDPP = angular.copy(parseFloat(res.data.Codes["[DPP]"]));
                            $scope.getDataSO[0].KaroseriPPN = angular.copy(parseFloat(res.data.Codes["[PPN]"]));
                            $scope.getDataSO[0].KaroseriVAT = angular.copy(parseFloat(res.data.Codes["[PPN]"]));
                            $scope.getDataSO[0].KaroseriTotalInclVAT = angular.copy(parseFloat(res.data.Codes["[TotalHargaKar]"]));
                            $scope.MaintainBillingTotalKaroseriTerbaru = angular.copy(parseFloat(res.data.Codes["[TotalHargaKar]"]));


                            $scope.MaintainBillingGrandTotalSebelumnya = parseFloat($scope.MaintainBillingGrandTotalSebelumnya);
                            $scope.MaintainBillingTotalAccessoriesTerbaru = parseFloat($scope.MaintainBillingTotalAccessoriesTerbaru);
                            $scope.getDataSO[0].GrandTotal = parseFloat($scope.getDataSO[0].GrandTotal);


                            $scope.getDataSO[0].GrandTotal = $scope.MaintainBillingGrandTotalSebelumnya + $scope.MaintainBillingTotalKaroseriTerbaru + parseFloat($scope.MaintainBillingTotalAccessoriesTerbaru);


                            console.log('GrandTotal New Karo= GrandTotalSebelumnya + TotalKaroseriTerbaru + TotalAccessoriesTerbaru;')
                            console.log('1. (perubahan Karoseri) GrandTotal ===>', parseFloat($scope.MaintainBillingGrandTotalSebelumnya) + ' + ' + parseFloat($scope.MaintainBillingTotalKaroseriTerbaru) + ' + ' + parseFloat($scope.MaintainBillingTotalAccessoriesTerbaru) + ' = ' + parseFloat($scope.getDataSO[0].GrandTotal));

                            $scope.modalRincianHargaPerubahanHargaMaintainBillingKalkulasi();

                        });
                }


            }
            else {

                // biar ga async jika ada perubahan aksesoris dan karoseri bersamaan
                setTimeout(function () {
                    // $scope.MaintainBillingTotalKaroseriTerbaru = angular.copy($scope.MaintainBillingTotalKaroseriSebelumnya);
                    // $scope.MaintainBillingTotalBBNTerbaru = angular.copy($scope.MaintainBillingTotalBBNSebelumnya);

                    $scope.MaintainBillingTotalKaroseriTerbaru = angular.copy($scope.dataPriceCheck.HargaKaroseriNew);
                    $scope.MaintainBillingTotalBBNTerbaru = angular.copy($scope.dataPriceCheck.HargaBBNNew);
                    
                    $scope.MaintainBillingGrandTotalSebelumnya = parseFloat($scope.MaintainBillingGrandTotalSebelumnya);
                    // $scope.MaintainBillingTotalAccessoriesTerbaru = parseFloat($scope.MaintainBillingTotalAccessoriesTerbaru);
                    $scope.MaintainBillingTotalAccessoriesTerbaru = parseFloat($scope.dataPriceCheck.HargaAksesorisNew);

                    $scope.getDataSO[0].GrandTotal = parseFloat($scope.getDataSO[0].GrandTotal);


                    $scope.getDataSO[0].GrandTotal = $scope.MaintainBillingGrandTotalSebelumnya + $scope.MaintainBillingTotalKaroseriTerbaru + $scope.MaintainBillingTotalAccessoriesTerbaru;
                    console.log('GrandTotal Karo Tak ada Perubahan= GrandTotalSebelumnya + TotalKaroseriTerbaru + TotalAccessoriesTerbaru;')
                    console.log('2. (perubahan Karoseri) GrandTotal ===>', parseFloat($scope.MaintainBillingGrandTotalSebelumnya) + ' + ' + parseFloat($scope.MaintainBillingTotalKaroseriTerbaru) + ' + ' + parseFloat($scope.MaintainBillingTotalAccessoriesTerbaru) + ' = ' + parseFloat($scope.getDataSO[0].GrandTotal));
                    console.log('$scope.MaintainBillingTotalBBNTerbaru',$scope.MaintainBillingTotalBBNTerbaru);
                    console.log('$scope.MaintainBillingTotalBBNSebelumnya',$scope.MaintainBillingTotalBBNSebelumnya)
                    $scope.modalRincianHargaPerubahanHargaMaintainBillingKalkulasi();
                }, 1000);
                

            }







        }

        $scope.MaintainBillingBtnCetakNotaRetur = function () {
            var pdfFile = null;
            var da_url = "";
            if ($scope.selectSudahBilling[0].BillingCancelTaxInvoiceTypeName == "Nota Retur") {
                //cetakan nota retur
                da_url = "sales/NotaRetur?BillingId=" + $scope.selectSudahBilling[0].BillingId;
                PrintRpt.print(da_url).success(function (res) {
                    var file = new Blob([res], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);
                    pdfFile = fileURL;

                    if (pdfFile != null)
                        printJS(pdfFile);
                    else
                        console.log("error cetakan", pdfFile);
                })
                    .error(function (res) {
                        console.log("error cetakan", pdfFile);
                    });
            } else if ($scope.selectSudahBilling[0].BillingCancelTaxInvoiceTypeName == "BA Pembatalan faktur") {
                //ba pembatalan
                da_url = "sales/BAPembatalanFakturPajak?BillingId=" + $scope.selectSudahBilling[0].BillingId;
                PrintRpt.print(da_url).success(function (res) {
                    var file = new Blob([res], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);
                    pdfFile = fileURL;

                    if (pdfFile != null)
                        printJS(pdfFile);
                    else
                        console.log("error cetakan", pdfFile);
                })
                    .error(function (res) {
                        console.log("error cetakan", pdfFile);
                    });
            }
        }

        $scope.btnCetakBatal = function () {
            if ($scope.selectSudahBilling[0].SOTypeId == 1) {
                $scope.ctkBatalBill = "sales/PembatalanFakturKendaraanBaru?BillingId=" + $scope.selectSudahBilling[0].BillingId + "&FrameNo=" + $scope.selectSudahBilling[0].FrameNo;
            } else if ($scope.selectSudahBilling[0].SOTypeId == 2) {
                $scope.ctkBatalBill = "sales/PembatalanFakturOPD?BillingId=" + $scope.selectSudahBilling[0].BillingId + "&FrameNo=" + $scope.selectSudahBilling[0].FrameNo;
            } else if ($scope.selectSudahBilling[0].SOTypeId == 3) {
                $scope.ctkBatalBill = "sales/PembatalanFakturPurnaJual?BillingId=" + $scope.selectSudahBilling[0].BillingId + "&FrameNo=" + $scope.selectSudahBilling[0].FrameNo;
            } else {
                $scope.ctkBatalBill = 'sales/PembatalanFakturKendaraanBaruSwapping?BillingId=' + $scope.selectSudahBilling[0].BillingId + "&FrameNo=" + $scope.selectSudahBilling[0].FrameNo;
            }

            //Call PrintRpt
            var pdfFile = null;
            PrintRpt.print($scope.ctkBatalBill).success(function (res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);
                pdfFile = fileURL;

                if (pdfFile != null)
                    printJS(pdfFile);
                else
                    console.log("error cetakan", pdfFile);
            })
            .error(function (res) {
                console.log("error cetakan", pdfFile);
            });
        }

        $scope.btnCetakBilling = function () {
            if ($scope.selectSudahBilling[0].SOTypeId == 1) {
                $scope.ctkBill = "sales/FakturKendaraanBaru?BillingId=" + $scope.selectSudahBilling[0].BillingId + "&FrameNo=" + $scope.selectSudahBilling[0].FrameNo;
            } else if ($scope.selectSudahBilling[0].SOTypeId == 2) {
                $scope.ctkBill = "sales/FakturOPD?BillingId=" + $scope.selectSudahBilling[0].BillingId + "&FrameNo=" + $scope.selectSudahBilling[0].FrameNo;
            } else if ($scope.selectSudahBilling[0].SOTypeId == 3) {
                $scope.ctkBill = "sales/FakturPurnaJual?BillingId=" + $scope.selectSudahBilling[0].BillingId + "&FrameNo=" + $scope.selectSudahBilling[0].FrameNo;
            } else {
                $scope.ctkBill = "sales/FakturKendaraanSwapping?BillingId=" + $scope.selectSudahBilling[0].BillingId + "&FrameNo=" + $scope.selectSudahBilling[0].FrameNo;

                // console.log("Kusung");
            }

            //Call PrintRpt
            var pdfFile = null;
            PrintRpt.print($scope.ctkBill).success(function (res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    printJS(pdfFile);
                    $scope.getDatawesbilling();
                }
                else {
                    console.log("error cetakan", pdfFile);
                }

            })
            .error(function (res) {
                console.log("error cetakan", pdfFile);
            });
        }

        $scope.btnLihatBilling = function (selected) {
            $scope.breadcrums.title = "Lihat";
            $scope.MaintainBillingUdaKalkulasi = "Belom";
            $scope.flagStatus = "lihatBilling";
            $scope.maintainBillingForm = false;
            $scope.buatBillingForm = true;
            $scope.ShowbtnSimpan = false;
            $scope.btnKembali = true;
            $scope.ViewBillingPelanggan = true;
            $scope.ViewBillingDokumen = true;
            $scope.ViewBillingPengiriman = true;
            $scope.ViewBillingHarga = true;
            $scope.ViewBillingNamaBilling = true;
            $scope.btntambah = false;

            $scope.Selectdata = selected;
            $scope.Selectdatarow.SoCode = $scope.Selectdata.SoCode;
            $scope.Selectdatarow.SoDate = $scope.Selectdata.SoDate;
            $scope.Selectdatarow.SOTypeId = $scope.Selectdata.SOTypeId;
            $scope.Selectdatarow.CustomerTypeId = $scope.Selectdata.CustomerTypeId;

            var getSO = "?SOId=" + $scope.Selectdata.SOId + "&BillingId=" + $scope.Selectdata.BillingId;
            MaintainBillingFactory.getDetailSudahBilling(getSO).then(
                function (res) {
                    $scope.getDataSO = res.data.Result;
                    $scope.gridDokumenPembetulanMaintainBilling = $scope.getDataSO[0].ListOfSODocumentView;

                    if ($scope.getDataSO[0].SOTypeId == 1 || $scope.getDataSO[0].SOTypeId == 2) {
                        if ($scope.getDataSO[0].SOTypeId == 1) {
                            $scope.ShowInfoPelanggan = true;
                            $scope.BillingInfoPelanggan = true;
                        } else if ($scope.getDataSO[0].SOTypeId == 2) {
                            $scope.ShowInfoPelanggan = false;
                            $scope.BillingInfoPelanggan = false;
                        }


                        $scope.BillingDokumen = true;
                        if ($scope.getDataSO[0].CustomerTypeDesc == 'Individu') {
                            $scope.IndividuPemilik = true;
                            $scope.disabledPemilik = true;
                            $scope.InstansiPemilik = false;
                        } else {
                            $scope.IndividuPemilik = false;
                            $scope.disabledPemilik = true;
                            $scope.InstansiPemilik = true;
                        }
                    } else if ($scope.getDataSO[0].SOTypeId == 3) {
                        $scope.ShowInfoPelanggan = false;
                        $scope.BillingInfoPelanggan = false;
                        $scope.BillingDokumen = false;
                    } else {
                        $scope.ShowInfoPelanggan = false;
                        $scope.BillingInfoPelanggan = false;
                        $scope.BillingDokumen = false;
                        $scope.ShowbtnSimpan = false;
                    }

                    angular.element("#MaintainBillingBuatBillingTab_Dokumen_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InfoPengiriman_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_RincianHarga_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InfoPelanggan_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_NamaBilling_MaintainBilling").addClass("active");
                    angular.element("#MaintainBillingBuatBillingTab_InformasiPembayaran_MaintainBilling").removeClass("active").addClass("");


                    angular.element("#menuDokumenBill").removeClass("tab-pane fade active").addClass("tab-pane fade");
                    angular.element("#menuInfoPengirimanBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfoRincianHargaBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfoPelangganBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuNamaBilling").removeClass("tab-pane fade").addClass("tab-pane fade in active");
                    angular.element("#menuInformasiPembayaran").removeClass("tab-pane fade in active").addClass("tab-pane fade");

                    $scope.pilihBank($scope.getDataSO[0]);
                }
            );
        }

        $scope.btnLihatBillingNeedApproval = function (selected) {
            console.log('Detail Need Approval');
            $scope.breadcrums.title = "Lihat";
            $scope.MaintainBillingUdaKalkulasi = "Belom";
            $scope.flagStatus = "lihatBilling";
            $scope.maintainBillingForm = false;
            $scope.buatBillingForm = true;
            $scope.ShowbtnSimpan = false;
            $scope.btnKembali = true;
            $scope.ViewBillingPelanggan = true;
            $scope.ViewBillingDokumen = true;
            $scope.ViewBillingPengiriman = true;
            $scope.ViewBillingHarga = true;
            $scope.ViewBillingNamaBilling = true;
            $scope.btntambah = false;

            $scope.Selectdata = selected;
            $scope.Selectdatarow.SoCode = $scope.Selectdata.SoCode;
            $scope.Selectdatarow.SoDate = $scope.Selectdata.SoDate;
            $scope.Selectdatarow.SOTypeId = $scope.Selectdata.SOTypeId;
            $scope.Selectdatarow.CustomerTypeId = $scope.Selectdata.CustomerTypeId;

            var getSO = "?SOId=" + $scope.Selectdata.SOId + "&BillingId=" + $scope.Selectdata.BillingId;
            MaintainBillingFactory.getDetailNeedApproval(getSO).then(
                function (res) {
                    $scope.getDataSO = res.data.Result;
                    $scope.getAccStandard = $scope.getDataSO[0].listofAccessoriesStandar;
                    $scope.gridDokumenPembetulanMaintainBilling = $scope.getDataSO[0].ListOfSODocumentView;

                    $scope.LeasingTenorTimeUntukViewDoang = $scope.getDataSO[0].LeasingTenorTime;

                    if ($scope.getDataSO[0].SOTypeId == 1 || $scope.getDataSO[0].SOTypeId == 2) {
                        if ($scope.getDataSO[0].SOTypeId == 1) {
                            $scope.ShowInfoPelanggan = true;
                            $scope.BillingInfoPelanggan = true;
                        } else if ($scope.getDataSO[0].SOTypeId == 2) {
                            $scope.ShowInfoPelanggan = false;
                            $scope.BillingInfoPelanggan = false;
                        }


                        $scope.BillingDokumen = true;
                        if ($scope.getDataSO[0].CustomerTypeDesc == 'Individu') {
                            $scope.IndividuPemilik = true;
                            $scope.disabledPemilik = true;
                            $scope.InstansiPemilik = false;
                        } else {
                            $scope.IndividuPemilik = false;
                            $scope.disabledPemilik = true;
                            $scope.InstansiPemilik = true;
                        }
                    } else if ($scope.getDataSO[0].SOTypeId == 3) {
                        $scope.ShowInfoPelanggan = false;
                        $scope.BillingInfoPelanggan = false;
                        $scope.BillingDokumen = false;
                    } else {
                        $scope.ShowInfoPelanggan = false;
                        $scope.BillingInfoPelanggan = false;
                        $scope.BillingDokumen = false;
                        $scope.ShowbtnSimpan = false;
                    }

                    angular.element("#MaintainBillingBuatBillingTab_Dokumen_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InfoPengiriman_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_RincianHarga_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InfoPelanggan_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_NamaBilling_MaintainBilling").addClass("active");
                    angular.element("#MaintainBillingBuatBillingTab_InformasiPembayaran_MaintainBilling").removeClass("active").addClass("");


                    angular.element("#menuDokumenBill").removeClass("tab-pane fade active").addClass("tab-pane fade");
                    angular.element("#menuInfoPengirimanBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfoRincianHargaBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfoPelangganBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuNamaBilling").removeClass("tab-pane fade").addClass("tab-pane fade in active");
                    angular.element("#menuInformasiPembayaran").removeClass("tab-pane fade in active").addClass("tab-pane fade");

                    $scope.pilihBank($scope.getDataSO[0]);
                }
            );
        }


        $scope.MaintainBillingApproveSelected = function () {
            MaintainBillingFactory.Approve($scope.selctedRowGridTungguApprovalBilling).then(
                function () {
                    //$scope.getData();
                    $scope.SearchgetDataMenungguApprovalbilling();
                    bsNotify.show({
                        title: "Berhasil",
                        content: "Data berhasil diapprove.",
                        type: 'success'
                    });
                    $scope.selctedRowGridTungguApprovalBilling = [];
                },
                function (err) {
                    bsNotify.show({
                        title: "Gagal",
                        content: err.data.Message,
                        type: 'danger'
                    });
                    $scope.selctedRowGridTungguApprovalBilling = [];
                }
            );
        }

        $scope.btnBuatBillingKembali = function () {
            $scope.buatBillingForm = false;
            $scope.maintainBillingForm = true;


        }

        $scope.MaintainBilingUbahYgLihat = function () {
            $scope.btnKembali = false;
            $scope.flagStatus = "ubahBilling";
            $scope.breadcrums.title = "Ubah";
            $scope.MaintainBillingUdaKalkulasi = "Belom";

            $scope.ViewBillingPelanggan = false;
            $scope.ViewBillingDokumen = false;
            $scope.ViewBillingPengiriman = false;
            $scope.ViewBillingHarga = false;
            $scope.ViewBillingNamaBilling = false;

              //ini biar tenor nya muncul (get ulang)
              MaintainBillingFactory.getDataTenor($scope.getDataSO[0].LeasingId).then(function (res) {
                for (var i = 0; i < res.data.Result.length; ++i) {
                    if (res.data.Result[i].LeasingId == $scope.getDataSO[0].LeasingId) {

                        $scope.optionsLeasingTenor = res.data.Result[i].ListLeasingTenor;
                        console.log('$scope.tenor', $scope.optionsLeasingTenor);
                        break;
                    }
                }

            });


        }

        $scope.MaintainBilingBatalUbah = function () {
            $scope.btnKembali = true;
            $scope.flagStatus = "lihatBilling";
            $scope.breadcrums.title = "Lihat";

            $scope.ViewBillingPelanggan = true;
            $scope.ViewBillingDokumen = true;
            $scope.ViewBillingPengiriman = true;
            $scope.ViewBillingHarga = true;
            $scope.ViewBillingNamaBilling = true;

            var getSO = "?SOId=" + $scope.Selectdata.SOId + "&BillingId=" + $scope.Selectdata.BillingId;
            MaintainBillingFactory.getDetailSudahBilling(getSO).then(
                function (res) {
                    $scope.getDataSO = res.data.Result;
                    $scope.gridDokumenPembetulanMaintainBilling = $scope.getDataSO[0].ListOfSODocumentView;

                    if ($scope.getDataSO[0].SOTypeId == 1 || $scope.getDataSO[0].SOTypeId == 2) {
                        if ($scope.getDataSO[0].SOTypeId == 1) {
                            $scope.ShowInfoPelanggan = true;
                            $scope.BillingInfoPelanggan = true;
                        } else if ($scope.getDataSO[0].SOTypeId == 2) {
                            $scope.ShowInfoPelanggan = false;
                            $scope.BillingInfoPelanggan = false;
                        }


                        $scope.BillingDokumen = true;
                        if ($scope.getDataSO[0].CustomerTypeDesc == 'Individu') {
                            $scope.IndividuPemilik = true;
                            $scope.disabledPemilik = true;
                            $scope.InstansiPemilik = false;
                        } else {
                            $scope.IndividuPemilik = false;
                            $scope.disabledPemilik = true;
                            $scope.InstansiPemilik = true;
                        }
                    } else if ($scope.getDataSO[0].SOTypeId == 3) {
                        $scope.ShowInfoPelanggan = false;
                        $scope.BillingInfoPelanggan = false;
                        $scope.BillingDokumen = false;
                    } else {
                        $scope.ShowInfoPelanggan = false;
                        $scope.BillingInfoPelanggan = false;
                        $scope.BillingDokumen = false;
                        $scope.ShowbtnSimpan = false;
                    }

                    angular.element("#MaintainBillingBuatBillingTab_Dokumen_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InfoPengiriman_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_RincianHarga_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_InfoPelanggan_MaintainBilling").removeClass("active").addClass("");
                    angular.element("#MaintainBillingBuatBillingTab_NamaBilling_MaintainBilling").addClass("active");
                    angular.element("#MaintainBillingBuatBillingTab_InformasiPembayaran_MaintainBilling").removeClass("active").addClass("");


                    angular.element("#menuDokumenBill").removeClass("tab-pane fade active").addClass("tab-pane fade");
                    angular.element("#menuInfoPengirimanBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfoRincianHargaBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfoPelangganBill").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuNamaBilling").removeClass("tab-pane fade").addClass("tab-pane fade in active");
                    angular.element("#menuInformasiPembayaran").removeClass("tab-pane fade in active").addClass("tab-pane fade");

                    $scope.pilihBank($scope.getDataSO[0]);
                }
            );

        }

        $scope.btnLihatPermohonanPembatalanBilling = function (row) {
            $scope.breadcrums.title = "Lihat";
            //$scope.mDetailBatalBilling = row;ini tadinya ada
            $scope.maintainBillingForm = false;
            $scope.permohonanPembatalanBillingForm = true;

            var getSO = "?SOId=" + row.SOId + "&BillingId=" + row.BillingId;

            MaintainBillingFactory.getDetailSudahBilling(getSO).then(
                function (res) {
                    $scope.getDataSO = res.data.Result;
                    $scope.mDetailBatalBilling = $scope.getDataSO[0];//ini tadinya ga ada
                    $scope.getAccStandard = $scope.mDetailBatalBilling.listofAccessoriesStandar;
                    console.log("listofAccessoriesStandar inii-->",$scope.getAccStandard);
                    //$scope.pilihBank($scope.getDataSO[0]);
                }
            );
        }

        $scope.kembalilihatpembatalanbilling = function () {
            $scope.permohonanPembatalanBillingForm = false;
            $scope.maintainBillingForm = true;
        }

        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.ModalLeasing = function (NoSPK) {
            $scope.ViewDataModal = NoSPK;
            angular.element('.ui.modal.MLeasing').modal('show');
        }

        $scope.onListCancel = function (item) {
            angular.element('.ui.modal.MLeasing').modal('hide');
            angular.element('.ui.modal.DokumenBill').modal('hide');
        }

        //===============================================================================   
        // $rootScope.$on("KembalikeFormSebelumnya", function() {
        // $scope.backfromMaintain();
        // });
        $scope.MaintainBillingSearchToggle = function () {
            $scope.MaintainBillingShowAdvSearch = !$scope.MaintainBillingShowAdvSearch;
            if ($scope.MaintainBillingShowAdvSearch == false) {
                document.getElementById("DaMaintainBillingMaeninAtas_Belum").style.paddingTop = "4%";
                document.getElementById("DaMaintainBillingMaeninAtas_Sudah").style.paddingTop = "4%";
                document.getElementById("DaMaintainBillingMaeninAtas_SudahTunggu").style.paddingTop = "4%";
            } else if ($scope.MaintainBillingShowAdvSearch == true) {
                document.getElementById("DaMaintainBillingMaeninAtas_Belum").style.paddingTop = "19%";
                document.getElementById("DaMaintainBillingMaeninAtas_Sudah").style.paddingTop = "19%";
                document.getElementById("DaMaintainBillingMaeninAtas_SudahTunggu").style.paddingTop = "19%";
            }

        }


        $scope.backfromMaintain = function () {
            $scope.maintainBillingForm = true;
            $scope.CreateSO = false;
        }

        $scope.ViewSO = function (SOId) {
            //$rootScope.SOId = SOId;
            //console.log("rootScope scope from parent", $rootScope.SOId, SOId)
            //$rootScope.$emit("LihatSOdariBilling", {});
            $scope.breadcrums.title = "Lihat";
            $scope.CreateSO = true;
            $scope.maintainBillingForm = false;
            MaintainSOFactory.getSODetail(SOId).then(function (res) {
                $scope.NoSOCode = res.data.Result[0];
                $scope.lihatDetailSOMaintainBilling();
            });
        }

        $scope.ChangePelanggan = function () {
            if ($scope.ViewData.CustomerTypeId == "3") {
                $scope.IndividuShow = true;
                $scope.InstitusiShow = false;

                $scope.infoPembeliPerusahaan = false;
                $scope.infoPenanggungJawabPerusahaan = false;
                $scope.infoPemilikPerusahaan = false;
            } else {
                $scope.IndividuShow = false;
                $scope.InstitusiShow = true;
            }
        }

        $scope.lihatDetailSOMaintainBilling = function () {
            $scope.ViewData = $scope.NoSOCode;
            if ($scope.ViewData.StatusPDDName == null || $scope.ViewData.StatusPDDName == "") {
                $scope.ViewData.StatusPDDName = "Belum dibuat";
            }

            if ($scope.ViewData.ManualMatchingBit == true) {
                $scope.ViewData.ManualMatchingBit = "Manual";
            } else {
                $scope.ViewData.ManualMatchingBit = "Automatic";
            }
            WarnaFactory.getData("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.ViewData.VehicleTypeId).then(
                function (res) {
                    $scope.listWarna = res.data.Result;
                })

            $scope.flagStatus = "lihatSO";

            $scope.CreateSO = true;
            $scope.ShowMenuTabs = true;
            $scope.btnBackToMaintainSO = true;
            $scope.btnSimpanCreateSO = false;
            $scope.DisableSOText = true;
            $scope.MaintainForm = false;
            $scope.disableViewPelanggan = true;
            $scope.disableViewJasa = true;
            $scope.disableViewAksesoris = true;
            $scope.disableViewDokumen = true;
            $scope.DisabledRincianHarga = true;
            $scope.disabledInfoPengiriman = true;
            $scope.getPeranPelanggan = [];

            if ($scope.ViewData.SOTypeId == 3) {
                var getSalesId = "&SalesId=" + $scope.ViewData.EmployeeId;
                MaintainSOFactory.getNoRangka(getSalesId).then(
                    function (res) {
                        $scope.DataNorangka = res.data.Result;
                        for (var i = 0; i < $scope.DataNorangka.length; ++i) {
                            if ($scope.DataNorangka[i].VehicleId == $scope.ViewData.VehicleId) {
                                $scope.pilihFrameNo($scope.DataNorangka[i]);
                                break;
                            }
                        }
                    }
                );
            }

            $scope.deleteRows = true;
            for (var i in $scope.NoSOCode.ListOfSOCustomerInformationView) {
                $scope.getPeranPelanggan.push({
                    CustomerCorrespondentId: $scope.NoSOCode.ListOfSOCustomerInformationView[i].CustomerCorrespondentId,
                    CustomerCorrespondentName: $scope.NoSOCode.ListOfSOCustomerInformationView[i].CustomerCorrespondentName
                });
            }
            $scope.gridJasaPengurusanDokumenMaintainBilling.data = angular.copy($scope.NoSOCode.ListOfSODocumentCareDetailService);

            $scope.MaintainBillingLihatSoShowPaketAksesoris = false;
            if ($scope.ViewData.SOTypeName == "Finish Unit") {
                for (var i = 0; i < $scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessories.length; ++i) {
                    $scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessories[i].RetailPrice = $scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessories[i].Price;
                }
                $scope.gridAksesorisMaintainBilling.data = angular.copy($scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessories);
                $scope.MaintainBillingLihatSoShowPaketAksesoris = true;
                $scope.gridAksesorisPackageMaintainBilling.data = angular.copy($scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage);
            } else {
                $scope.gridAksesorisMaintainBilling.data = angular.copy($scope.NoSOCode.ListOfSODetailAccesoriesView);
            }


            $scope.gridDokumenMaintainBilling.data = angular.copy($scope.NoSOCode.ListOfSODocumentView);

            //$scope.ViewDataDelivery = $scope.NoSOCode.ListOfSODeliveryInfoView;
            // $scope.ViewDataInfoUnit = $scope.NoSOCode.ListOfSOUnitInfoView;
            $scope.ViewDataAsuransi = $scope.NoSOCode.ListOfSOInsuranceExtView;
            if ($scope.ViewData.SOTypeName == "Finish Unit") {
                $scope.ShowtabContainer = true;
                $scope.ShowSONumber = true;
                $scope.ShowProspectID = true;
                $scope.ShowProspectName = true;
                $scope.ShowSalesman = false;
                $scope.InfoPelanggan = true;
                $scope.ShowPeranPelanggan = true;
                $scope.disableTextInfoPelanggan = true;
                $scope.ShowPelangganOPD = false;
                $scope.ShowPelangganFinishUnit = true;
                $scope.JasaPengurusanDokumen = false;
                $scope.Dokumen = true;
                $scope.btntambah = false;
                $scope.InfoPengiriman = true;
                $scope.showDelivery = true;
                $scope.showOfftheRoad = true;
                $scope.showMatching = true;
                $scope.showPromiseDelivery = true;
                $scope.showEstimasiRentang = true;
                $scope.showStatusPDD = true;
                $scope.RincianHarga = true;
                $scope.Aksesoris = true;
                $scope.karoseriFinishUnit = true;
                $scope.InfoLeasingFinishUnit = true;
                $scope.disabledinfoLeasing = true;
                $scope.InfoAsuransiFinishUnit = true;
                $scope.finishUnitInfounityangakandibeli = true;
                $scope.finishUnitWarna = true;
                $scope.InfoMediatorFinishUnit = true;
                $scope.InfoPemakaiFinishUnit = true;
                $scope.showAlasanBatalSO = true;
                $scope.infoPembeli = true;
            } else {
                if ($scope.ViewData.SOTypeName == "OPD") {
                    $scope.ShowtabContainer = true;
                    $scope.ShowSONumber = true;
                    $scope.ShowSalesman = true;

                    $scope.ShowNorangka = true; //===No Rangka
                    $scope.ShowFrameNo = true;

                    var getSalesId = "&SalesId=" + $scope.ViewData.EmployeeId;
                    MaintainSOFactory.getNoRangka(getSalesId).then(
                        function (res) {
                            $scope.DataNorangka = res.data.Result;
                            for (var i = 0; i < $scope.DataNorangka.length; ++i) {
                                if ($scope.DataNorangka[i].VehicleId == $scope.ViewData.VehicleId) {
                                    $scope.pilihFrameNo($scope.DataNorangka[i]);
                                    break;
                                }
                            }
                        }
                    );

                    $scope.ShowProspectID = false;
                    $scope.ShowProspectName = false;
                    $scope.ShowPeranPelanggan = false;
                    $scope.disableTextInfoPelanggan = true;
                    $scope.InfoPelanggan = true;
                    $scope.ShowPelangganOPD = true;
                    $scope.ShowPelangganFinishUnit = false;
                    $scope.JasaPengurusanDokumen = true;
                    $scope.Dokumen = true;
                    $scope.btntambah = false;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.DisabledTotalRincianHarga = true;
                    $scope.Aksesoris = false;
                    $scope.menuRincianHarga = true;
                    $scope.InfoPelangganFinishUnit = false;
                    $scope.karoseriFinishUnit = false;
                    $scope.InfoLeasingFinishUnit = false;
                    $scope.InfoAsuransiFinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli = false;
                    $scope.finishUnitWarna = true;
                    $scope.InfoMediatorFinishUnit = false;
                    $scope.InfoPemakaiFinishUnit = false;
                    $scope.infoPembeli = false;
                } else if ($scope.ViewData.SOTypeName == "Purna Jual") {
                    $scope.ShowtabContainer = true;
                    $scope.ShowSONumber = true;
                    $scope.ShowProspectID = false;
                    $scope.ShowProspectName = false;
                    $scope.ShowPeranPelanggan = false;
                    $scope.disableTextInfoPelanggan = true;
                    $scope.InfoPelanggan = true;
                    $scope.ShowPelangganOPD = true;
                    $scope.ShowPelangganFinishUnit = false;
                    $scope.JasaPengurusanDokumen = false;
                    $scope.Dokumen = false;
                    $scope.btntambah = false;
                    $scope.Aksesoris = true;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.DisabledTotalRincianHarga = true;
                    $scope.InfoPelangganFinishUnit = false;
                    $scope.karoseriFinishUnit = false;
                    $scope.InfoLeasingFinishUnit = false;
                    $scope.InfoAsuransiFinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli = false;
                    $scope.finishUnitWarna = true;
                    $scope.InfoMediatorFinishUnit = false;
                    $scope.InfoPemakaiFinishUnit = false;
                    $scope.infoPembeli = false;
                } else if ($scope.ViewData.SOTypeName == "Swapping") {
                    $scope.ShowtabContainer = true;
                    $scope.InfoPelanggan = false;
                    $scope.Aksesoris = false;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.JasaPengurusanDokumen = false;
                    $scope.Dokumen = false;
                    $scope.finishUnitInfounityangakandibeli = true;
                    $scope.finishUnitWarna = true;
                    $scope.infoPembeli = false;
                    $scope.showAlasanBatalSO = true;

                    angular.element("#InfoPelangganSO").removeClass("active").addClass("");
                    angular.element("#JasaPengurusanDokumenSO").removeClass("active").addClass("");
                    angular.element("#DokumenSO").removeClass("active").addClass("");
                    angular.element("#AksesorisSO").removeClass("active").addClass("");
                    angular.element("#karoseriFinishUnitSO").removeClass("active").addClass("");
                    angular.element("#InfoPengirimanSO").addClass("active");
                    angular.element("#InfoLeasingFinishUnitSO").removeClass("active").addClass("");
                    angular.element("#InfoAsuransiFinishUnitSO").removeClass("active").addClass("");
                    angular.element("#finishUnitInfounityangakandibeliSO").removeClass("active").addClass("");
                    angular.element("#RincianHargaSO").removeClass("active").addClass("");
                    angular.element("#InfoMediatorFinishUnitSO").removeClass("active").addClass("");
                    angular.element("#InfoPemakaiFinishUnitSO").removeClass("active").addClass("");

                    angular.element("#menuInfoPelanggan_MaintainBilling").removeClass("tab-pane fade active").addClass("tab-pane fade");
                    angular.element("#menuJasaPengurusanDokumen_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuAksesoris_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuDokumen_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfoPengiriman_MaintainBilling").removeClass("tab-pane fade").addClass("tab-pane fade in active");
                    angular.element("#menuRincianHarga_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menukaroseri_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInsurance_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuinfoLeasing_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuMedia_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#infoPemakai_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfounityangakandibeli_MaintainBilling").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                }
            }
            console.log('ini data view',$scope.ViewData);
            $scope.changeInfoPelangganMaintainBilling($scope.ViewData.ListOfSOCustomerInformationView[0]);
            $scope.pilihBank();

        }

        $scope.pilihBank = function (selected) {
            $scope.bankTemp = selected;
            if ($scope.flagStatus == "InsertSO") {
                if ($scope.ViewData.BankId != null || $scope.ViewData.BankId !== undefined) {
                    var Bank = "?BankId=" + $scope.bankTemp.BankId;
                    MaintainBillingFactory.getNoRek(Bank).then(
                        function (res) {
                            $scope.selectRek = res.data.Result;
                            //$scope.ViewData.AccountNo = $scope.selectRek[0].AccountNo;
                            //tadi kaya diatas
                            $scope.ViewData.BankAccountNumber = $scope.selectRek[0].BankAccountNumber;

                            return res.data;
                        }
                    );
                }

            } else {
                if ($scope.ViewData.BankId != null || $scope.ViewData.BankId !== undefined) {
                    var Bank = "?BankId=" + $scope.ViewData.BankId;
                    MaintainBillingFactory.getNoRek(Bank).then(
                        function (res) {
                            $scope.selectRek = res.data.Result;
                            //$scope.ViewData.AccountNo = $scope.selectRek[0].AccountNo;
                            //tadi kaya diatas
                            $scope.ViewData.BankAccountNumber = $scope.selectRek[0].BankAccountNumber;
                            return res.data;
                        }
                    );
                }

            }
        }

        $scope.changeInfoPelangganMaintainBilling = function (selected) {
            if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pembeli") {
                $scope.infoPembeli = true;
                $scope.infoPemilik = false;
            } else if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pemilik") {
                $scope.infoPemilik = true;
                $scope.infoPembeli = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pembeli") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;
                $scope.infoPembeliPerusahaan = true;
                $scope.infoPenanggungJawabPerusahaan = false;
                $scope.infoPemilikPerusahaan = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Penanggung Jawab") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;
                $scope.infoPenanggungJawabPerusahaan = true;
                $scope.infoPembeliPerusahaan = false;
                $scope.infoPemilikPerusahaan = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pemilik") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;
                $scope.infoPemilikPerusahaan = true;
                $scope.infoPembeliPerusahaan = false;
                $scope.infoPenanggungJawabPerusahaan = false;
            } else {
                //kosong();
            }
        }

        $scope.clickhapusrowgrid = function (row) {
            if ($scope.flagStatus == "lihatBilling") {

                bsNotify.show({
                    title: "Peringatan",
                    content: "Tidak dapat menghapus data ketika lihat billing.",
                    type: 'warning'
                });
            } else {
                // var index = $scope.gridDokumenPembetulanMaintainBilling.indexOf(row.entity);
                // $scope.gridDokumenPembetulanMaintainBilling.splice(index, 1);
                $scope.gridDokumenPembetulanMaintainBilling.splice(row, 1);
            }
        }

        $scope.onSelectRows = function (rows) {
            $scope.selected_rows = rows;


        }

        $scope.status = false;
        $scope.statusDokumenSoMaintainBilling = false;

        $scope.viewDocument = function (row) {
            $scope.viewdocument = {};
            $scope.viewdocumentName = {};
            //$scope.viewdocument = row.StrBase64;
            //$scope.viewdocumentName = row.DocumentName;
            var tempGuid = row.DocumentURL;
            MaintainBillingFactory.getImage(tempGuid).then(
                function (res) {
                    $scope.viewdocument = res.data.UpDocObj;
                    $scope.viewdocumentName = res.data.FileName;
                })

            $scope.status = true;
            callModal();
        }

        $scope.viewDocumentMaintainBillingLihatSo = function (row) {
            MaintainSOFactory.getImage(row.DocumentURL).then(function (res) {
                $scope.viewdocument = res.data.UpDocObj;
                $scope.viewdocumentName = res.data.FileName;
                $scope.statusDokumenSoMaintainBilling = true;
                callModalYangGambarMaintainBilling();
            })
        }

        $scope.detailaccpacketMaintainBilling = function (data) {
            var number = angular.element('.ui.modal.modalAccPackageDetailMaintainBilling').length;
            setTimeout(function () {
                angular.element('.ui.modal.modalAccPackageDetailMaintainBilling').modal('refresh');
            }, 0);
            if (number > 0) {
                angular.element('.ui.modal.modalAccPackageDetailMaintainBilling').not(':first').remove();
            }
            $scope.gridAksesorisDetailMaintainBilling.data = data.ListDetail;
            angular.element('.ui.modal.modalAccPackageDetailMaintainBilling').modal('show');
        }

        $scope.modalAccPackageDetailMaintainBilling = function () {
            angular.element('.ui.modal.modalAccPackageDetailMaintainBilling').modal('hide');
        }


        function callModal() {
            angular.element('.ui.modal.dokumenViewbilling').modal({ closable: false }).modal('show');
        }

        function callModalYangGambarMaintainBilling() {
            angular.element('.ui.modal.dokumenViewSOMaintainBilling').modal({ closable: false }).modal('show');
        }

        $scope.keluarImg = function () {
            $scope.status = false;
            angular.element('.ui.modal.dokumenViewbilling').modal('hide');
        }

        $scope.keluarImgdokumenViewSOMaintainBilling = function () {
            $scope.statusDokumenSoMaintainBilling = false;
            angular.element('.ui.modal.dokumenViewSOMaintainBilling').modal('hide');
        }

        //----------------------------------
        // Grid Action Click
        //----------------------------------
        var detailClick = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.ModalLeasing(row.entity)">Detail</u></p></span>';
        var SONoClicked = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.ViewSO(row.entity.SOId)">{{row.entity.SoCode}}</u></p></span>';
        var ViewBilling = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.btnLihatBilling(row.entity)">{{row.entity.BillingCode}}</u></p></span>';

        var ViewBillingNeedApproval = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.btnLihatBillingNeedApproval(row.entity)">{{row.entity.BillingCode}}</u></p></span>';

        var NoBatalBilling = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.btnLihatPermohonanPembatalanBilling(row.entity)">{{row.entity.BillingCancelNo}}</u></p></span>';

        // var viewClick = '<span>\
        // <p style="padding:5px 0 0 5px"><u ng-if="row.entity.StatusDocumentName ==\'Empty\'">\
        // <button style="margin-top:-5px"><label style="padding-top:-5px;border:1px" class="btn ng-binding ladda-button"><span>Browse</span><input type="file" bs-uploadsingle="onFileSelect($files)" img-upload="row.entity.DocumentData" name="" id="pickfiles-nav1" style="display:none"></label></button><label style="margin-top:-5px;text-decoration:none !important" ng-show="row.entity.DocumentData!=null">{{row.entity.DocumentData.FileName}}</label>\
        // </u><u ng-if="row.entity.StatusDocumentName ==\'Upload\'"\
        // ng-click="grid.appScope.viewDocument(row.entity)" style="color:blue">View</u></p></span>';


        var viewClick = '<span>\
        <p style="padding:5px 0 0 5px"><u ng-if="row.entity.StatusDocumentName ==\'Empty\' "><button  style="float:left;margin-left:5px" onClick="document.getElementById(\'pickfiles-nav1\').click()"> Browse <input id="pickfiles-nav1"  type="file" maxbyte="10" formattype="[\'gif\',\'jpeg\',\'jpg\',\'png\',\'img\']" bs-uploadsingle="onFileSelect($files)" img-upload="row.entity.DocumentData" style="display:none"></button>  <label ng-show="row.entity.DocumentData.FileName!=null && row.entity.DocumentData.FileName!=undefined">{{row.entity.DocumentData.FileName}}</label></u>&nbsp<u ng-if="row.entity.StatusDocumentName ==\'Upload\'"\
            ng-click="grid.appScope.viewDocument(row.entity)" style="color:blue">View</u></p></span>';

        var HapusClick = '<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.flagStatus==\'lihatBilling\'" ng-click="grid.appScope.clickhapusrowgrid(row)" ></i>'

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.GridBelumBilling = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            enableColumnResizing: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,

            //data: 'dataBelumBilling',
            columnDefs: [
                { displayName: 'No. SPK/No. PO', name: 'No SPK/PO No', field: 'FormSPKNo', width: '10%' },
                { displayName: 'SO No', name: 'SO No', field: 'SoCode', cellTemplate: SONoClicked, width: '18%' },
                { displayName: 'SO Date', name: 'SO Date', field: 'SoDate', width: '9%', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { name: 'Nama Pelanggan', field: 'ProspectName', width: '20%' },
                { name: 'Sumber Dana', field: 'FundSourceName', width: '9%' },
                { name: 'Status Leasing', cellTemplate: detailClick, width: '9%' },
                { name: 'Total Incoming', cellFilter: 'currency:"Rp."', field: 'TotalFullPayment', width: '14%' },
                {
                    displayName: 'DP Lunas',
                    name: 'DP Lunas',
                    field: 'IsDPPaid',
                    cellTemplate: '<div align="center">\
                <input type="checkbox" name="Persentase" ng-checked="(row.entity.IsDPPaid==true ||row.entity.IsBillingBeforeDPPaidApproved==true)" ng-disabled="true">\
                <div>',
                    width: '8%'
                }
            ]
        };

        $scope.GridBelumBilling.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApiBelumBilling = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selectedRow = gridApi.selection.getSelectedRows();
                if ($scope.selectedRow != "") {
                    $scope.disablebuttoneBuatBilling = false;
                } else {
                    $scope.disablebuttoneBuatBilling = true;
                }
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selectedRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.GridSudahBilling = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            enableColumnResizing: true,
            enableColumnMenus: true,
            //enableHorizontalScrollbar : 0,
            enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            // data: 'dataSudahBilling',
            columnDefs: [
                { displayName: 'SO No', name: 'SO No', field: 'SoCode', cellTemplate: SONoClicked },
                { displayName: 'SO Date', name: 'SO Date', field: 'SoDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { name: 'Nama Pelanggan', field: 'ProspectName' },
                { name: 'Billing No', field: 'BillingCode', cellTemplate: ViewBillingNeedApproval },
                { name: 'VIN', field: 'VIN' },
                { name: 'Billing Status', field: 'BillingStatusName' },
                { name: 'No Batal Billing', cellTemplate: NoBatalBilling },
                { name: 'Status Batal Billing', field: 'BillingCancelStatusName' }
            ]
        };

        $scope.GridSudahBilling.onRegisterApi = function (gridApi) {
            $scope.gridApiSudahBilling = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selectSudahBilling = gridApi.selection.getSelectedRows();
                if ($scope.selectSudahBilling.length > 0) {
                    if ($scope.selectSudahBilling[0].BillingStatusName == "Created") {
                        $scope.showCetakBatal = false;
                        $scope.showCetak = true;
                        $scope.showCetakNota = false;
                    } else if ($scope.selectSudahBilling[0].BillingStatusName == "Printed") {
                        $scope.showCetakBatal = false;
                        $scope.showCetak = true;
                        $scope.showCetakNota = false;
                    } else if ($scope.selectSudahBilling[0].BillingStatusName == "Completed") {
                        $scope.showCetakBatal = false;
                        $scope.showCetak = true;
                        $scope.showCetakNota = false;
                    } else if ($scope.selectSudahBilling[0].BillingStatusName == "Cancel") {
                        $scope.showCetakBatal = true;
                        $scope.showCetak = false;
                        $scope.showCetakNota = true;
                    }

                    if ($scope.selectSudahBilling[0].BillingCancelTaxInvoiceTypeName == "Normal") {
                        $scope.showCetakNota = false;
                    }

                } else {
                    $scope.showCetakBatal = false;
                    $scope.showCetak = false;
                    $scope.showCetakNota = false;
                }
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selectSudahBilling = gridApi.selection.getSelectedRows();
            });
        };

        $scope.GridTungguApprovalBilling = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            enableColumnResizing: true,
            enableColumnMenus: true,
            //enableHorizontalScrollbar : 0,
            enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            // data: 'dataSudahBilling',
            // columnDefs: [
            //     { displayName: 'SO No', name: 'SO No', field: 'SoCode', cellTemplate: SONoClicked },
            //     { displayName: 'SO Date', name: 'SO Date', field: 'SoDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
            //     { name: 'Nama Pelanggan', field: 'ProspectName' },
            //     { name: 'Billing No', field: 'BillingCode' ,cellTemplate: ViewBillingNeedApproval },
            //     { name: 'Billing Status', field: 'BillingStatusName' },
            //     { name: 'No Batal Billing', cellTemplate: NoBatalBilling },
            //     { name: 'Status Batal Billing', field: 'BillingCancelStatusName' }
            // ]

            columnDefs: [
                { displayName: 'No. SPK/No. PO', name: 'No SPK/PO No', field: 'FormSPKNo', width: '10%' },
                { displayName: 'SO No', name: 'SO No', field: 'SoCode', cellTemplate: SONoClicked, width: '18%' },
                { name: 'Billing No', field: 'BillingCode', cellTemplate: ViewBillingNeedApproval, width: '10%' },
                { displayName: 'SO Date', name: 'SO Date', field: 'SoDate', width: '9%', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { name: 'Nama Pelanggan', field: 'ProspectName', width: '10%' },
                { name: 'Sumber Dana', field: 'FundSourceName', width: '9%' },
                { name: 'Status Leasing', cellTemplate: detailClick, width: '9%' },
                { name: 'Total Incoming', cellFilter: 'currency:"Rp."', field: 'TotalFullPayment', width: '14%' },
                {
                    displayName: 'DP Lunas',
                    name: 'DP Lunas',
                    field: 'IsDPPaid',
                    cellTemplate: '<div align="center">\
                <input type="checkbox" name="Persentase" ng-checked="(row.entity.IsDPPaid==true ||row.entity.IsBillingBeforeDPPaidApproved==true)" ng-disabled="true">\
                <div>',
                    width: '8%'
                }
            ]
        };

        $scope.GridTungguApprovalBilling.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApiGridTungguApprovalBilling = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selctedRowGridTungguApprovalBilling = gridApi.selection.getSelectedRows();
                $scope.selectedRow = gridApi.selection.getSelectedRows();
                if ($scope.selectedRow != "") {
                    $scope.disablebuttoneBuatBilling = false;
                } else {
                    $scope.disablebuttoneBuatBilling = true;
                }
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selctedRowGridTungguApprovalBilling = gridApi.selection.getSelectedRows();
            });
        };

        // $scope.gridDokumen = {
        // enableSorting: true,
        // enableRowSelection: true,
        // multiSelect: false,
        // enableColumnResizing: true,
        // enableSelectAll: true,
        // paginationPageSizes: [10, 25, 50],
        // paginationPageSize: 10,
        // columnDefs: [
        // { name: 'No', field: 'No', width: '4%', visible: false },
        // { name: 'Document Id', field: 'DocumentId', visible: false },
        // { name: 'Tipe Dokumen', field: 'DocumentType',width: '15%' },
        // {
        // name: 'Dokumen',
        // allowCellFocus: false,
        // enableColumnMenu: false,
        // enableSorting: false,
        // enableColumnResizing: true,
        // cellTemplate: viewClick
        // },
        // {
        // name: 'Status',
        // width: '10%',
        // field: 'StatusDocumentName',
        // cellTemplate: '<div ng-if="row.entity.DocumentData==null && row.entity.StatusDocumentName !=\'Upload\'"><p style="padding:5px 0 0 5px">Empty</p></div><div ng-if="row.entity.DocumentData!=null"><p style="padding:5px 0 0 5px">Upload</p></div><div ng-if="row.entity.StatusDocumentName ==\'Upload\'"><p style="padding:5px 0 0 5px">Upload</p></div>'
        // },
        // {
        // name: 'Action',
        // allowCellFocus: false,
        // width: '10%',
        // enableColumnMenu: false,
        // enableSorting: false,
        // enableColumnResizing: true,
        // cellTemplate: HapusClick
        // }
        // ]
        // };

        // $scope.gridDokumen.onRegisterApi = function(gridApi) {
        // $scope.gridApi = gridApi;
        // gridApi.selection.on.rowSelectionChanged($scope, function(row) {
        // $scope.selectRowgridDokumen = gridApi.selection.getSelectedRows();
        // });
        // gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
        // $scope.selectRowgridDokumen = gridApi.selection.getSelectedRows();
        // });
        // };

        $scope.gridDataDokumenBill = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Tipe Dokumen', field: 'DocumentId', visible: false },
                { name: 'Tipe Dokumen', field: 'DocumentType', width: '20%' },
                { name: 'Dokumen', field: 'DocumentName' }
            ]
        };

        $scope.gridDataDokumenBill.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridRincianHarga = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableColumnResizing: true,
            //data: 'dataInfoDokumen',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Type Model', field: 'TypeModel' },
                { name: 'Keterangan', field: 'Keterangan' },
                { name: 'Currency', field: 'Currency' },
                { name: 'Nominal', field: 'Nominal' }
            ]
        };

        var HapusClickMaintainBilling = '<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.flagStatus==\'lihatSO\'" ng-click="grid.appScope.clickhapusrowgrid(row)" ></i>';
        $scope.gridJasaPengurusanDokumenMaintainBilling = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'dataJasaPengurusanDokumen',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'ServiceId.', field: 'ServiceId', visible: false },
                { name: 'Item No.', field: 'ItemNo', visible: false },
                { name: 'Kode Jasa', field: 'ServiceCode' },
                { name: 'Nama Jasa', field: 'ServiceName' },
                { name: 'No PR', field: 'PRCode' },
                { name: 'Status PR', field: 'StatusPRName' },
                {
                    name: 'Qty',
                    field: 'Qty',
                    format: '{0:c}',
                    type: 'number',
                    visible: false
                },
                {
                    name: 'Admin Fee',
                    field: 'AdminFee',
                    format: '{0:c}',
                    type: 'number',
                    visible: false
                },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '15%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: HapusClickMaintainBilling
                }
            ]
        };

        //var HapusClick2MaintainBilling = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-hide="grid.appScope.flagStatus==\'lihatSO\'" ng-click="grid.appScope.clickhapusrowgrid2(row)">Hapus</u></span>'
        $scope.columnDefsgridAksesorisMaintainBilling = [
            { name: 'Item No', field: 'ItemNo', visible: false },
            { name: 'MaterialId', field: 'MaterialId', visible: false },
            { name: 'PartsId', field: 'AccessoriesId', width: '20%', visible: false },
            { name: 'No Material', field: 'AccessoriesCode' },
            { name: 'Nama Material', field: 'AccessoriesName' },
            { name: 'Qty', field: 'Qty', format: '{0:c}', type: 'number', visible: false },
            { name: 'Satuan', field: 'UomName' },
            {
                name: 'Harga Satuan',
                field: 'RetailPrice',
                cellClass: 'text-right-MaintainBilling',
                cellFilter: 'currency:"Rp."',
                treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                customTreeAggregationFinalizerFn: function (aggregation) {
                    aggregation.rendered = aggregation.value;
                }
            },
            { displayName: 'No PR', name: 'No PR', field: 'PRCode' },
            { name: 'Status PR', field: 'StatusPRName' },
            {
                name: 'Diskon',
                field: 'Discount',
                cellClass: 'text-right-MaintainSo', cellFilter: 'currency:"Rp."',
                treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                customTreeAggregationFinalizerFn: function (aggregation) {
                    aggregation.rendered = aggregation.value;
                },
                //format: '{0:c}',
                type: 'number',
                enableCellEdit: true
            },
            { name: 'Generate PR', field: 'GeneratePR', enableCellEdit: false, cellTemplate: '<p align="center"><input ng-if="!row.groupHeader" ng-disabled="true" class="ui-grid-cell-contents" type="checkbox" ng-checked="row.entity.GeneratePR == true"></p>', visible: true },
            {
                name: 'Action',
                allowCellFocus: false,
                width: '15%',
                enableColumnMenu: false,
                enableSorting: false,
                enableColumnResizing: true,
                cellTemplate: '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-hide="grid.appScope.flagStatus==\'lihatSO\'" ng-click="grid.appScope.clickhapusrowgrid2(row)">Hapus</u></span>'
            },
        ];

        $scope.gridAksesorisMaintainBilling = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            enableColumnResizing: true,
            //data: 'dataInfoAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: $scope.columnDefsgridAksesorisMaintainBilling
        };

        $scope.gridAksesorisMaintainBilling.onRegisterApi = function (gridApi) {
            $scope.gridAksesorisMaintainBilling_Api = gridApi;

        };
        var HapusClick1MaintainBilling = '<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.flagStatus==\'lihatSO\'" ng-click="grid.appScope.clickhapusrowgrid1(row)"></i>'
        var viewClickMaintainBilling = '<span>\
        <p style="padding:5px 0 0 5px"><u ng-if="row.entity.StatusDocumentName ==\'Empty\'">\
            <input type="file" bs-uploadsingle="onFileSelect($files)" img-upload="row.entity.DocumentData" name="" id="pickfiles-nav1" style="z-index: 1" accept="image/x-png,image/gif,image/jpeg">\
        </u>&nbsp<u ng-if="row.entity.StatusDocumentName ==\'Upload\'"\
            ng-click="grid.appScope.viewDocumentMaintainBillingLihatSo(row.entity)" style="color:blue">View</u></p></span>';
        $scope.gridDokumenMaintainBilling = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            //data: 'dataInfoDokumen',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'No', field: 'No', width: '7%', visible: false },
                { name: 'Tipe Dokumen', field: 'SODetailDocumentId', visible: false },
                { name: 'Document Id', field: 'DocumentId', visible: false },
                { name: 'Tipe Dokumen', field: 'DocumentType', width: '25%' },
                {
                    name: 'Dokumen',
                    field: 'DocumentURL',
                    allowCellFocus: false,
                    width: '35%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: viewClickMaintainBilling
                },
                {
                    name: 'Status',
                    field: 'StatusDocumentName',
                    width: '20%',
                    cellTemplate: '<div ng-if="row.entity.DocumentData==null && row.entity.StatusDocumentName !=\'Upload\'""><p style="padding:5px 0 0 5px">Empty</p></div><div ng-if="row.entity.DocumentData!=null"><p style="padding:5px 0 0 5px">Upload</p></div><div ng-if="row.entity.StatusDocumentName ==\'Upload\'"><p style="padding:5px 0 0 5px">Upload</p></div>'
                },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '15%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: HapusClick1MaintainBilling
                }
            ]
        };

        var accpackage = '<i ng-if="!row.groupHeader" title="Lihat Detil" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.detailaccpacketMaintainBilling(row.entity)" ></i>';

        $scope.gridAksesorisPackageMaintainBilling = {
            multiSelect: false,
            //enableSelectAll: false,
            enableColumnMenus: true,
            //enableRowSelection: false,
            enableSorting: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            //data: 'dataAksesoris',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                {
                    name: 'tipe model',
                    field: 'Description',
                    width: '40%',
                    pinnedLeft: true,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'material number', field: 'AccessoriesPackageCode' },
                { name: 'Nama Material', field: 'AccessoriesPackageName' },
                { name: 'qty', field: 'Qty', width: '10%' },
                //  { name: 'status', field: 'StatusAccName' },
                {
                    name: 'harga',
                    cellFilter: 'currency:"Rp."',
                    field: 'Price',
                    width: '20%',
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function (aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                {
                    name: 'Detil Aksesoris',
                    cellTemplate: accpackage

                }
            ],
        };

        $scope.FilterGridBelumBilling = function () {
            var inputfilter = $scope.textFilterBelumBilling;
            console.log('$scope.textFilterBelumBilling ===>', $scope.textFilterBelumBilling);

            var tempGrid = angular.copy($scope.GridBelumBilling.dataTemp);
            var objct = '{"' + $scope.selectedFilterBelumBilling + '":"' + inputfilter + '"}'
            if (inputfilter == "" || inputfilter == null) {
                $scope.GridBelumBilling.data = $scope.GridBelumBilling.dataTemp;
                console.log('$scope.GridBelumBilling.data if ===>', $scope.GridBelumBilling.data)
            } else {
                $scope.GridBelumBilling.data = $filter('filter')(tempGrid, JSON.parse(objct));
                console.log('$scope.GridBelumBilling.data else ===>', $scope.GridBelumBilling.data)
            }
        };

        $scope.FilterGridSudahBilling = function () {
            var inputfilter = $scope.textFilterSudahBilling;
            console.log('$scope.textFilterSudahBilling ===>', $scope.textFilterSudahBilling);


            var tempGrid = angular.copy($scope.GridSudahBilling.dataTemp);
            var objct = '{"' + $scope.selectedFilterSudahBilling + '":"' + inputfilter + '"}'
            if (inputfilter == "" || inputfilter == null) {
                $scope.GridSudahBilling.data = $scope.GridSudahBilling.dataTemp;
                console.log('$scope.GridSudahBilling.data if ===>', $scope.GridSudahBilling.data)

            } else {
                $scope.GridSudahBilling.data = $filter('filter')(tempGrid, JSON.parse(objct));
                console.log('$scope.GridSudahBilling.data else ===>', $scope.GridSudahBilling.data)

            }
        };

        $scope.FilterGridTungguApprovalBilling = function () {
            var inputfilter = $scope.textFilterTungguApprovalBilling;
            console.log('$scope.textFilterTungguApprovalBilling ===>', $scope.textFilterTungguApprovalBilling);


            var tempGrid = angular.copy($scope.GridTungguApprovalBilling.dataTemp);
            var objct = '{"' + $scope.selectedFilterTungguApprovalBilling + '":"' + inputfilter + '"}'
            if (inputfilter == "" || inputfilter == null) {
                $scope.GridTungguApprovalBilling.data = $scope.GridTungguApprovalBilling.dataTemp;
                console.log('$scope.GridTungguApprovalBilling.data if ===>', $scope.GridTungguApprovalBilling.data)

            } else {
                $scope.GridTungguApprovalBilling.data = $filter('filter')(tempGrid, JSON.parse(objct));
                console.log('$scope.GridTungguApprovalBilling.data else ===>', $scope.GridTungguApprovalBilling.data)

            }
        };

        $scope.gridAksesorisDetailMaintainBilling = {
            // enableSorting: true,
            enableRowSelection: true,
            enableColumnMenus: true,
            enableSorting: true,
            enableColumnResizing: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            //data: 'dataAksesoris',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { name: 'material number', field: 'AccessoriesCode' },
                { name: 'material name', field: 'AccessoriesName' },
                {
                    name: 'harga',
                    cellFilter: 'currency:"Rp."',
                    field: 'Price', attributes: '{style:"text-align:right;"}'
                }
            ],
        };


    });

app.directive('imageSetAspect', function ($scope) {
    return {
        scope: false,
        restrict: 'A',
        link: function ($scope, element, attrs) {
            element.bind('load', function () {
                // Use jquery on 'element' to grab image and get dimensions.
                scope.$apply(function () {
                    $scope.viewdocument.width = $(element).width();
                    $scope.viewdocument.height = $(element).height();
                });
            });
        }
    };
});