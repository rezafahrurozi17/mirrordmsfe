angular.module('app')
    .factory('MaintainBillingFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };

        return {
            getData: function(filter) {
                filter.SoDate = fixDate(filter.SoDate);
                var res = $http.get('/api/sales/BillingOutstandingSO/?start=1&limit=100000&' + $httpParamSerializer(filter));
                return res;
            },
			
			getDataLeasing: function () { 
				var res = $http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000'); 
				return res; 
			},

            getDatawesbilling: function(filter) {
                for (key in filter) {
                    if (filter[key] == null || filter[key] == undefined)
                        delete filter[key];
                }
                var res = $http.get('/api/sales/AdminHandlingBillingBilledSO/?start=1&limit=100000&' + $httpParamSerializer(filter));
                return res;
            },
			
			getDataYangMenungguApproval: function(filter) {
                for (key in filter) {
                    if (filter[key] == null || filter[key] == undefined)
                        delete filter[key];
                }
                var res = $http.get('/api/sales/AdminHandlingBillingNeedApproval/?start=1&limit=100000&' + $httpParamSerializer(filter));
                return res;
            },

            getDataSO: function(param) {
                var res = $http.get('/api/sales/BillingOutstandingSO/Detail' + param);
                return res;
            },

            getValidasiBilling: function(param){
                var res = $http.get('/api/sales/AdminHandlingValidasiBFDP/?soId=' + param);
                return res;
            },

            getDetailSudahBilling: function(param) {
                var res = $http.get('/api/sales/AdminHandlingBillingBilledSO/Detail' + param);
                return res;
            },


            getDetailNeedApproval: function (param) {
                var res = $http.get('/api/sales/AdminHandlingBillingNeedApproval/Detail' + param);
                return res;
            },

            getDataTenor: function (param) {
                var res = $http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000');
                return res;
            },

   
            getBillingIdForDetailSudahBilling: function(param) {
                var res = $http.get('/api/sales/AdminHandlingBillingController/GetBillingId/?SOId=' + param);
                return res;
            },

            getDataCPosition: function(param) {
                var res = $http.get('/api/sales/CCustomerPosition');
                return res;
            },

            getDataProvince: function(param) {
                var res = $http.get('/api/sales/MLocationProvince');
                return res;
            },

            getDataCity: function(param) {
                var res = $http.get('/api/sales/MLocationCityRegency' + param);
                return res;
            },

            getHargaAccessoriesSatuan: function (param ) {
                var res = $http.get('/api/sales/HargaAccessoriesSatuanByCode' + param);
                return res;
            },

            getHargaAccessoriesPaket: function (param) {
                var res = $http.get('/api/sales/HargaAccessoriesPaket' + param);
                return res;
            },

            getDataKec: function(param) {
                var res = $http.get('/api/sales/MLocationKecamatan' + param);
                return res;
            },

            getDataKel: function(param) {
                var res = $http.get('/api/sales/MLocationKelurahan' + param);
                return res;
            },

            getNoRek: function(param) {
                var res = $http.get('/api/sales/AdminHandling_BankAccount' + param);
                return res;
            },

            getBillId: function(param) {
                var res = $http.get('/api/sales/AdminHandlingBillingController/GetBillingId' + param);
                return res;
            },
			
			getBillingCheck: function (SOId) { 
				var res = $http.get('/api/sales/AdminHandlingBillingPriceCheck?SOId='+SOId); 
				return res; 
			},
			
			SimpanAbisCek: function(maintainBilling) {
                var SubmitmaintainBilling=angular.copy(maintainBilling);
				for (var i in SubmitmaintainBilling[0].ListOfSODocumentView){
                    if (typeof SubmitmaintainBilling[0].ListOfSODocumentView[i].StrBase64 === "undefined"){
                    }else{
                        delete SubmitmaintainBilling[0].ListOfSODocumentView[i].StrBase64;
                    }
                }
				var setan=fixDate(angular.copy(maintainBilling[0].InvoiceStartEffectiveDate));
				
				SubmitmaintainBilling[0].InvoiceStartEffectiveDate=angular.copy(setan);
                return $http.post('/api/sales/AdminHandlingBillingPriceChange', SubmitmaintainBilling);
            },
			
			SimpanAbisCekUbah: function(maintainBilling) {
                var SubmitmaintainBilling=angular.copy(maintainBilling);
				for (var i in SubmitmaintainBilling[0].ListOfSODocumentView){
                    if (typeof SubmitmaintainBilling[0].ListOfSODocumentView[i].StrBase64 === "undefined"){
                    }else{
                        delete SubmitmaintainBilling[0].ListOfSODocumentView[i].StrBase64;
                    }
                }
				var setan=fixDate(angular.copy(maintainBilling[0].InvoiceStartEffectiveDate));
				
				SubmitmaintainBilling[0].InvoiceStartEffectiveDate=angular.copy(setan);
                return $http.put('/api/sales/AdminHandlingBillingPriceChange', SubmitmaintainBilling);
            },

            create: function(maintainBilling) {
                var SubmitmaintainBilling=angular.copy(maintainBilling);
				for (var i in SubmitmaintainBilling[0].ListOfSODocumentView){
                    if (typeof SubmitmaintainBilling[0].ListOfSODocumentView[i].StrBase64 === "undefined"){
                    }else{
                        delete SubmitmaintainBilling[0].ListOfSODocumentView[i].StrBase64;
                    }
                }
				var setan=fixDate(angular.copy(maintainBilling[0].InvoiceStartEffectiveDate));
				
				SubmitmaintainBilling[0].InvoiceStartEffectiveDate=angular.copy(setan);
                return $http.post('/api/sales/AdminHandlingBilling', SubmitmaintainBilling);
            },
			
			update: function(maintainBilling) {
                var SubmitmaintainBilling=angular.copy(maintainBilling);
				for (var i in SubmitmaintainBilling[0].ListOfSODocumentView){
                    if (typeof SubmitmaintainBilling[0].ListOfSODocumentView[i].StrBase64 === "undefined"){
                    }else{
                        delete SubmitmaintainBilling[0].ListOfSODocumentView[i].StrBase64;
                    }
                }
				var setan=fixDate(angular.copy(maintainBilling[0].InvoiceStartEffectiveDate));
				
				SubmitmaintainBilling[0].InvoiceStartEffectiveDate=angular.copy(setan);
                return $http.put('/api/sales/AdminHandlingBilling', SubmitmaintainBilling);
            },
			
			Approve: function(YangMauDiApprove) {
                
                return $http.put('/api/sales/AdminHandlingBilling/UpdateStatus', YangMauDiApprove);
            },

            getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
            },

            delete: function(id) {
                return $http.delete('/sales/SOListBilling', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });