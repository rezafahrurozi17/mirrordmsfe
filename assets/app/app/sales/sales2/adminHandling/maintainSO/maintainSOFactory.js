angular.module('app')
  .factory('MaintainSOFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
  
    function fixDate(date) {
        if (date != null || date != undefined) {
            var fix = date.getFullYear() + '-' +
                ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                ('0' + date.getDate()).slice(-2) + 'T' + 
                ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
            return fix;
        } else {
            return null;
        }
    };
    if (_myCache == undefined)
    var _myCache = [];
    return {
        getDataLeasing: function () { var res = $http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000'); return res; },
        getDataTenor: function (param) { var res = $http.get('/api/sales/MProfileLeasingForMaster/?start=1&limit=1000000'); return res; },
        getDataInsuranceProduct: function (param) { var res = $http.get('/api/sales/MProfileInsuranceProduct/?insuranceId=' + param); return res },
        getDataInsuranceEkstension: function (param) { var res = $http.get('/api/sales/MProfileInsuranceInsuranceExtension/?insuranceId=' + param); return res },
        getDownPaymentAfterEdit: function(param) {
            var res=$http.get('/api/fe/SOCheckDPUpdate/?SOId='+param.SOId + '&SPKId=' + param.SpkId + '&pOutletId=' + param.OutletId+ '&NominalDP=' + param.NominalDP);
            return res;
        },
        getDataProvince: function () {
            var res = null;
            if (_myCache.length == 0) {
                var user = CurrentUser.user();
                var res = $http.get('/api/sales/MLocationProvince');
                res.then(function (res) {
                    _myCache = res.data.Result;
                    console.log('ini cache')
                });
            } else {
                res = {
                    then: function (cbOk, cbErr) {
                        cbOk({ data: { Result: angular.copy(_myCache) } });
                    }
                }
            }
            console.log('ini bukan cache')
            return res;
        },

        getDataKabupaten: function (param) { var res = $http.get('/api/sales/MLocationCityRegency' + param); return res; },

        getRegionCode: function(Id,ProvinceId) {
            var res = $http.get('/api/sales/AFIRegion/?Id=0&ProvinceId='+ProvinceId+'&CityRegencyId='+Id);
            return res;
        },
        
        getDataGender: function () { var res = $http.get('/api/sales/CCustomerGender'); return res; },

        getDataStatusPernikahan: function () { var res = $http.get('/api/sales/CCustomerStatusMarital'); return res; },

        getDataAgama: function () { var res = $http.get('/api/sales/CCustomerReligion'); return res; },

        getDataJabatan: function () {
            var res = $http.get('/api/sales/CCustomerPosition');
            return res;
        },
        
        getData: function(filter) {
            for(key in filter){
              if(filter[key]==null || filter[key]==undefined)
                delete filter[key];
            }
            var res=$http.get('/api/sales/AdminHandlingSOMaintain/?start=1&limit=10000&'+$httpParamSerializer(filter));
            return res;
        },
        
        getSOType: function() {
            var res=$http.get('/api/sales/AdminHandlingSOType');
            return res;
        },

        getJPembayaran: function() {
            var res=$http.get('/api/sales/FFinancePaymentType');
            return res;
        },
		
		getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
            },

        getCustomerCategory: function() {
            var res=$http.get('/api/sales/CCustomerCategory');
            return res;
        },

        getPeranPelanggan: function() {
            var res=$http.get('/api/sales/CCustomerCorrespondent');
            return res;
        },

        getDataSectorBisnis: function () {
            var res = $http.get('/api/sales/CCustomerSectorBusiness');
            return res;
        },

        getSOStatus: function() {
            var res=$http.get('/api/sales/AdminHandlingSOStatus');
            return res;
        },

        getVehicleType: function (param) {
            var res = $http.get('/api/sales/MUnitVehicleTypeTomas' + param);
            return res;
        },

        getDataModel: function () {
            var res = $http.get('/api/sales/MUnitVehicleModelTomas');
            return res;
        },

        getCancelreason: function() {
            var res=$http.get('/api/sales/AdminHandlingSOCancelReason');
            return res;
        },

        getOutlet: function() {
            var res=$http.get('/api/sales/MSitesOutlet?start=1&limit=1000');
            return res;
        },

        getDataJasaDokumen: function(param) {
            var res=$http.get('/api/sales/MProfileService?'+param);
            return res;
        },

        getDataDokumen: function() {
            var res=$http.get('/api/sales/PCategoryDocument?start=1&limit=100');
            return res;
        },

        getDataAksesoris: function(param) {
            var res=$http.get('/api/sales/AccessoriesSatuan/All' + param);
            return res;
        },

        getDataAksesorisBelokan: function (param) {
            var res = $http.get('/api/sales/AccessoriesSatuan/All' + param);
            return res;
        },

        getDataKaroseri: function(param) {
            var res=$http.get('/api/sales/MPartsKaroseri/SOPurnaJual?start=1&limit=100' + param);
            return res;
        },

        getDataBank: function() {
            var res=$http.get('/api/sales/AdminHandlingBank');
            return res;
        },

        getDataSalesman: function() {
            var res=$http.get('/api/sales/MProfileEmployee/SalesmanAll');
            return res;
        },

        getSODetail: function (param){
            var res = $http.get('/api/sales/AdminHandlingSOMaintain?SOId='+param);
            return res;
        },

        getNoRangka: function (param){
            var res = $http.get('/api/sales/AdminHandlingStockPurnaJual?start=1&limit=1000' + param);
            return res;
        },

        getFakturPajak: function (){
            var res = $http.get('/api/sales/FFinanceCodeInvoiceTransactionTax');
            return res;
        },

        getStatusBilling: function (param){
            var res = $http.get('/api/sales/AdminHandlingCheckBillingStatus' + param);
            return res;
        },

//-------------------------------------------------------------------------------------------------\\
        createSO: function(MaintainSO) {
            MaintainSO.SentDate = fixDate(MaintainSO.SentDate);
            MaintainSO.SentUnitDate = fixDate(MaintainSO.SentUnitDate);
            console.log("MaintainSO",MaintainSO)
            return $http.post('/api/sales/AdminHandlingSOMaintain', [{
            ProspectId: MaintainSO.ProspectId,
            SpkId: MaintainSO.SpkId,
            SpkDetailInfoUnitId: MaintainSO.SpkDetailInfoUnitId,
            SoCode: MaintainSO.SoCode,
            SoDate: MaintainSO.SoDate,
            ToyotaId: MaintainSO.ToyotaId,
            EmployeeId: MaintainSO.EmployeeId,
            Name: MaintainSO.Name,
			FreePPNBit: MaintainSO.FreePPNBit,
            Handphone:MaintainSO.Handphone,
            KTP:MaintainSO.KTP,
            SIUP:MaintainSO.SIUP,
            TDP:MaintainSO.TDP,
            
            VehicleYearExt: MaintainSO.VehicleYearExt,

            NPWP:MaintainSO.NPWP,
            Address:MaintainSO.Address,
            CInvoiceTypeId:MaintainSO.CInvoiceTypeId,
            ReceiverName:MaintainSO.ReceiverName,
            ReceiverAddress:MaintainSO.ReceiverAddress,
            ReceiverHP:MaintainSO.ReceiverHP,
            SentDate:MaintainSO.SentDate,
            Discount:MaintainSO.Discount,
            AFIValidDate:MaintainSO.AFIValidDate,
            NoteSOCancelReason:MaintainSO.NoteSOCancelReason,
            RRNNo:MaintainSO.RRNNo,
            FrameNo:MaintainSO.FrameNo,
            NamaPO:MaintainSO.NamaPO,
            VehicleTypeColorId:MaintainSO.VehicleTypeColorId,
            SOStatusId:MaintainSO.SOStatusId,
            SOTypeId:MaintainSO.SOTypeId,
            SOCancelReasonId:MaintainSO.SOCancelReasonId,
            CustomerCategoryId:MaintainSO.CustomerCategoryId,
            PaymentTypeId:MaintainSO.PaymentTypeId,
            DPP:MaintainSO.DPP,
            PPN:MaintainSO.PPN,
            PPH22:MaintainSO.PPH22,
            TotalVAT:MaintainSO.TotalVAT,
            TotalDPP:MaintainSO.TotalDPP, // CR4
            GrandTotal:MaintainSO.GrandTotal,
            MediatorName:MaintainSO.MediatorName,
            MediatorPhoneNo:MaintainSO.MediatorPhoneNo,
            MediatorKTPNo:MaintainSO.MediatorKTPNo,
            ListOfSODocumentCareDetailService:MaintainSO.ListOfSODocumentCareDetailService,
            ListOfSODocumentView:MaintainSO.ListOfSODocumentView,
            ListOfSODetailAccesoriesView:MaintainSO.ListOfSODetailAccesoriesView,
            ServiceBureauId:MaintainSO.ServiceBureauId,
            ServiceBureauCode:MaintainSO.ServiceBureauCode,
            ServiceBureauName:MaintainSO.ServiceBureauName,
            DocumentId:MaintainSO.DocumentId,
            DocumentType:MaintainSO.DocumentType,
            DocumentName:MaintainSO.DocumentName,
            AccessoriesId:MaintainSO.AccessoriesId,
            PostingDate:MaintainSO.PostingDate,
            SOTypeName:MaintainSO.SOTypeName,
            SOStatusName:MaintainSO.SOStatusName,
            ProspectName:MaintainSO.ProspectName,
            FormSPKNo:MaintainSO.FormSPKNo,
            spkCustomerId:MaintainSO.spkCustomerId,
            CustomerTypeId:MaintainSO.CustomerTypeId,
            CustomerTypeDesc:MaintainSO.CustomerTypeDesc,
            DeliveryCategoryId:MaintainSO.DeliveryCategoryId,
            DeliveryCategoryName:MaintainSO.DeliveryCategoryName,
            OnOffTheRoadId:MaintainSO.OnOffTheRoadId,
            OnOffTheRoadName:MaintainSO.OnOffTheRoadName,
            PromiseDeliveryMonthId:MaintainSO.PromiseDeliveryMonthId,
            PromiseDeliveryMonthName:MaintainSO.PromiseDeliveryMonthName,
            EstimateDate:MaintainSO.EstimateDate,
            SentUnitDate:MaintainSO.SentUnitDate,
            StatusPDDId:MaintainSO.StatusPDDId,
            StatusPDDName:MaintainSO.StatusPDDName,
            StatusMatchId:MaintainSO.StatusMatchId,
            StatusMatchName:MaintainSO.StatusMatchName,
            Description:MaintainSO.Description,
            colorName:MaintainSO.colorName,
            NumberPlateId:MaintainSO.NumberPlateId,
            NumberPlateName:MaintainSO.NumberPlateName,
            StatusStockName:MaintainSO.StatusStockName,
            ProductionYear:MaintainSO.ProductionYear,
            InsuranceId:MaintainSO.InsuranceId,
            NotePayment:MaintainSO.NotePayment,
            BankId:MaintainSO.BankId,
            BankName:MaintainSO.BankName,
			BankAccountNumber:MaintainSO.BankAccountNumber,
            InsuranceProductId:MaintainSO.InsuranceProductId,
            ProductName:MaintainSO.ProductName,
            InsuranceUserTypeId:MaintainSO.InsuranceUserTypeId,
            InsuranceUserTypeName:MaintainSO.InsuranceUserTypeName,
            PolisName:MaintainSO.PolisName,
            spkPaymentTypeId:MaintainSO.spkPaymentTypeId,
            PaymentTypeName:MaintainSO.PaymentTypeName,
            EstimasiBiayaAsuransi:MaintainSO.EstimasiBiayaAsuransi,
            WaktuPertanggungan:MaintainSO.WaktuPertanggungan,
            Karoseri:MaintainSO.Karoseri,
            KaroseriPrice:MaintainSO.KaroseriPrice,
            MaterialId:MaintainSO.MaterialId,
            MaterialTypeId:MaintainSO.MaterialTypeId,
            MaterialNo:MaintainSO.MaterialNo,
            MaterialName:MaintainSO.MaterialName,
            KaroseriStatus:MaintainSO.KaroseriStatus,
            EngineNoExt : MaintainSO.EngineNoExt,
            STNKNameExt : MaintainSO.STNKNameExt,
            LicensePlateExt : MaintainSO.LicensePlateExt,
            ColorUnitExt : MaintainSO.ColorUnitExt
            }]);
        },

        update: function(MaintainSO){
            MaintainSO.SentDate = fixDate(MaintainSO.SentDate);
            MaintainSO.SentUnitDate = fixDate(MaintainSO.SentUnitDate);
            console.log("test",MaintainSO);
            return $http.put('/api/sales/AdminHandlingSOMaintain', [MaintainSO]);
        },

        batalSO:function(MaintainSO)
        {
            return $http.put('/api/sales/AdminHandlingSOMaintain/Cancel', [MaintainSO]);
        },
		
		getDataVendorList: function() {
            var res = $http.get('/api/sales/MVendorOPD');
            return res;
        },

        delete: function(id) {
            return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
        },

        getDataInsurance: function() {
            var res = $http.get('/api/sales/MProfileInsurance');
            return res;
        },
    }
  });