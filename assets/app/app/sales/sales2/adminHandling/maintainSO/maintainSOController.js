angular.module('app')
    .controller('MaintainSOController', function($scope,$stateParams, $rootScope, $httpParamSerializer, $http, CurrentUser, bsNotify, MaintainQuotationFactory, MaintainSOFactory, MaintainBillingFactory,ModelFactory, WarnaFactory, TipeFactory, $timeout, MobileHandling, bsRights,uiGridConstants, uiGridGroupingConstants,ComboBoxFactory, ASPricingEngine) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.MaintainForm = true;
        $scope.CreateSO = false;
        $scope.ViewForm = false;
        $scope.PromiseDelivery = false; 
        $scope.SummarySOOPD = false;
        $scope.SummarySOPurnaJual = false;
        //$scope.show_modal = { show: false };
        $scope.btnSimpanCreateSO = false;
        $scope.show_modalDokumen = { show: false };
        $scope.show_modalJasaPengurusanDokumen = { show: false };
        $scope.show_modalInfoUnit = { show: false };
        $scope.disableinfoPemakai = true;
        $scope.modalMode = 'new';
        $scope.flagStatus = "SIMPAN";
        $scope.showSum = "typeSO";
        $scope.deleteRows = false;
        $scope.disabledBank = false;
        $scope.resultFrame = {};
        var PPNPerc = 0;

        function getMod(a, b) {
            return (a & Math.pow(2, b)) == Math.pow(2, b)
        }
        $scope.RightsEnableForMenu = {};

        $scope.modal_model = [];
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
            //----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------


            ASPricingEngine.getPPN(0, 1).then(function (res) {
                PPNPerc = res.data
            });
        });

        $scope.uploadFiles = [];
       // IfGotProblemWithModal();
        //----------------------------------
        // Radio-Button
        //----------------------------------
        var listMaterial = [{
            MaterialTypeId: 1,
                MaterialName: "Aksesoris"
        },      {
            MaterialTypeId: 2,
                MaterialName: "Karoseri"    
        }    ];
        $scope.materialType = listMaterial;

        //----------------------------------
        // Initialization
        //----------------------------------
            $scope.rightEnableByte = null;
            $scope.disabledSimpanSO = false;
            $scope.rights = bsRights.get("app.maintainso");
            $scope.disabledBtnCreateSo = false;
            //console.log("rights=>", $scope.rights);
            //$scope.RightsNew = rights.new;
            //console.log("RightsNew",$scope.RightsNew);

        // AMBIL DATA LEASING
        MaintainSOFactory.getDataLeasing().then(function (res) { $scope.leasing = res.data.Result; console.log("leasing--->", $scope.leasing);});
        
        $scope.filterTenor = function (selected) {
            console.log("leasing",$scope.leasing);
            console.log("$scope.ViewData.test",$scope.ViewData);
            console.log('selected leasing id',selected);
            console.log('ViewData.ListOfSOLeasingView[0]',$scope.ViewData.ListOfSOLeasingView[0]);
            console.log('ViewData',$scope.ViewData);
            console.log("$scope.temptenor",$scope.temptenor);
            for(var a in $scope.leasing){
                if($scope.leasing[a].LeasingId == selected){
                    
                $scope.temptenor = $scope.leasing[a].ListLeasingTenor;
                }
            }
            $scope.tenor = $scope.temptenor.filter(function (type) {
                return (type.LeasingId == selected);
            })
            console.log("$scope.tenor",$scope.tenor);
        }

        MaintainSOFactory.getDataInsurance().then(function (res) {



            $scope.insurance = res.data.Result;
            var ulala = angular.copy($scope.insurance[0]);
            $scope.insurance.unshift(ulala);
            $scope.insurance[0].InsuranceId = null;
            $scope.insurance[0].InsuranceName = "Tidak Pakai Asuransi";
            $scope.insurance[0].ListInsuranceProductView = null;
            $scope.insurance[0].ListMProfileInsuranceExtensionView = null;


        });

        $scope.user = CurrentUser.user();
        $scope.tab = $stateParams.tab;
		$scope.breadcrums = {};
        $scope.mMaintainSO = null; //Model
        $scope.filter = { SOStatusId: null, SOType: null, SODateStart: null, SODateEnd: null, SpkId: null };
        $scope.xRole = { selected: [] };
        $scope.getSOType = [];

        $scope.dateOptionsStart = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.dateOptionsEnd = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
        }

        $scope.tanggalmin = function(dt) {
            $scope.dateOptionsEnd.minDate = dt;
        }
		
		var Da_TodayDate=new Date();
		var da_awal_tanggal1 = new Date(Da_TodayDate.getFullYear(), Da_TodayDate.getMonth(), 1);
		$scope.filter.SODateStart=da_awal_tanggal1;
		$scope.tanggalmin($scope.filter.SODateStart);
		$scope.filter.SODateEnd=Da_TodayDate;

        $scope.showAlert = function(message, number) {
            $.bigBox({
                title: "Mohon Input Filter",
                color: "#c00",
                icon: "fa fa-shield fadeInLeft animated",
                timeout: 2000
            });
        };
		
		// MaintainSOFactory.getDataVendorList().then(
            // function (res) {
                // $scope.OptionsDataVendorMaintainSO = res.data.Result;
            // }
        // );

        $scope.temptenor = [];
        // $scope.filterTenor = function (selected) {
        //     console.log('selectedleasingid',selected);
        //     if (selected !=l
        //             for (var i = 0; i < res.data.Result.length; ++i) {
        //                 console.log('selected', selected);
        //                 if (res.data.Result[i].LeasingId == selected) {

        //                     $scope.tenor = res.data.Result[i].ListLeasingTenor;
        //                     console.log('$scope.tenor', $scope.tenor);
        //                     break;
        //                 }
        //             }


        //             setTimeout(function () {
        //                 $scope.selected_data.LeasingLeasingTenorId = $scope.selected_data.LeasingLeasingTenorId;
        //             }, 300);
        //         })
        //     }
        // }

        $scope.product = [];
        $scope.insuranceextension = [];
        $scope.filterProductSO = function (selected) {
            console.log("product--->>", $scope.product);
            console.log("insuranceextension--->>", $scope.insuranceextension);
            console.log("selected--->>", selected);
            if (selected != undefined || selected != null) {
                MaintainSOFactory.getDataInsuranceProduct(selected).then(function (res) { $scope.product = res.data.Result; });
                MaintainSOFactory.getDataInsuranceEkstension(selected).then(function (res) { $scope.insuranceextension = res.data.Result; });
            }
            
        }
       
        $scope.selectPembelibit = function() {
            
            $scope.ViewData.PemakaiPembeliBit = true;
            $scope.ViewData.PemakaiName = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerName);
            $scope.ViewData.PemakaiHP = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].HpNumber);
            $scope.ViewData.PemakaiAddress = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerAddress);

            console.log('$scope.ViewData.PemakaiPembeliBit',$scope.ViewData.PemakaiPembeliBit);
        }
        $scope.selectPemilikbit = function() {
            $scope.ViewData.PemakaiPembeliBit = true;
            $scope.ViewData.PemakaiName = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[1].CustomerName);
            $scope.ViewData.PemakaiHP = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[1].HpNumber);
            $scope.ViewData.PemakaiAddress = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[1].CustomerAddress);
            console.log('$scope.ViewData.PemakaiPembeliBit',$scope.ViewData.PemakaiPembeliBit);
        }

        $scope.unitLihat = function() {
            if ($scope.unitLihatView == true) {
                $scope.unitLihatView = false;
            } else {
                $scope.unitLihatView = true;
            }

            if ($scope.jasaLihatView == true) {
                $scope.jasaLihatView = false;
            } else {
                $scope.jasaLihatView = true;
            }

            if ($scope.PurnaLihatView == true) {
                $scope.PurnaLihatView = false;
            } else {
                $scope.PurnaLihatView = true;
            }
        }

        $scope.unitLihat_EditFinishUnit = function() {
            if ($scope.unitLihatView_EditFinishUnit == true) {
                $scope.unitLihatView_EditFinishUnit = false;
            } else {
                $scope.unitLihatView_EditFinishUnit = true;
            }

        }

        function checkBytes(u) {
            $scope.RightsEnableForMenu.allowNew = getMod(u, 1);
            $scope.RightsEnableForMenu.allowEdit = getMod(u, 2);
            $scope.RightsEnableForMenu.allowDelete = getMod(u, 3);
            $scope.RightsEnableForMenu.allowApprove = getMod(u, 4);
            $scope.RightsEnableForMenu.allowReject = getMod(u, 5);
            $scope.RightsEnableForMenu.allowReview = getMod(u, 6);
            $scope.RightsEnableForMenu.allowPrint = getMod(u, 7);
            $scope.RightsEnableForMenu.allowPart = getMod(u, 8);
            $scope.RightsEnableForMenu.allowConsumeable = getMod(u, 9);
            $scope.RightsEnableForMenu.allowGR = getMod(u, 10);
            $scope.RightsEnableForMenu.allowBP = getMod(u, 11);
            $scope.RightsEnableForMenu.allowUnmatch = getMod(u, 12);
            $scope.RightsEnableForMenu.allowSuggestmatch = getMod(u, 13);
            $scope.RightsEnableForMenu.allowMatch = getMod(u, 14);
        }

        $timeout(function () {
            console.log("rightEnableByte", $scope.rightEnableByte);
            checkBytes($scope.rightEnableByte);
            console.log("RIGHST =====>", $scope.RightsEnableForMenu);
        }, 1000)

        //----------------------------------
        // Get Data
        //----------------------------------
        MaintainSOFactory.getSOType().then(
            function(res) {
                //var da_thing=res.data.Result;
				$scope.getSOType = res.data.Result;
				$scope.getSOTypeFilter = [{ SOTypeId: null, SOTypeName: null }];
                $scope.getSOTypeFilter.splice(0, 1);
                $scope.getSOTypeFilter.push({ SOTypeId: 0, SOTypeName: "All" });
				for (var i = 0; i < res.data.Result.length; i++) {

                    $scope.getSOTypeFilter.push({ SOTypeId: res.data.Result[i].SOTypeId, SOTypeName: res.data.Result[i].SOTypeName });
                }
                //$scope.filter.SOType = 0;
				
                $scope.filter.SOType=$scope.getSOTypeFilter[0].SOTypeId;
                console.log(' $scope.filter.SOType ===>', $scope.filter.SOType);
                return res.data;
            }
        );
        // $scope.PopulateKategory();
       

        MaintainSOFactory.getSOStatus().then(
            function(res) {
				
				$scope.getSOstatus=[{OutletId:null,SOStatusId:null,SOStatusName:null,StatusCode:null}];
				$scope.getSOstatus.splice(0,1);
				$scope.getSOstatus.push({OutletId:0,SOStatusId:0,SOStatusName:"All",StatusCode:1});
				
				for(var i = 0; i < res.data.Result.length; i++)
				{
					$scope.getSOstatus.push({OutletId:res.data.Result[i].OutletId,SOStatusId:res.data.Result[i].SOStatusId,SOStatusName:res.data.Result[i].SOStatusName,StatusCode:res.data.Result[i].StatusCode});
				}
				
                //$scope.getSOstatus = res.data.Result;
				$scope.filter.SOStatus=$scope.getSOstatus[0].SOStatusId;
                return res.data;
            }
        );

        MaintainSOFactory.getFakturPajak().then(
            function(res) {
                $scope.CodePajak = res.data.Result;
                //$scope.ViewData = {};
                //$scope.ViewData.CInvoiceTypeId = $scope.CodePajak[0].CodeInvoiceTransactionTaxId;
                return res.data;
            }
        );

        MaintainSOFactory.getJPembayaran().then(
            function(res) {
                $scope.dataPembayaran = res.data.Result;
                return res.data;
            }
        );

        MaintainQuotationFactory.getTOP().then(function(res) {
            $scope.termOfPayment = res.data.Result;
            return res.data;
        });

        $scope.getData = function() {
            alerts = [];
            $scope.gridData = [];
            if ($scope.filter.SOType == null || $scope.filter.SODateStart == null || $scope.filter.SODateEnd == null) {
                $scope.showAlert(alerts.join('\n'), alerts.length);
                $timeout(function() {
                    $scope.loading = false;
                })
            } else {
				if($scope.filter.SOStatus==0)
				{
					$scope.filter.SOStatus=null;
                }
                
                $scope.filter.SODateStart = $scope.changeFormatDate($scope.filter.SODateStart);
                $scope.filter.SODateEnd = $scope.changeFormatDate($scope.filter.SODateEnd);
                
                MaintainSOFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        for (var j in $scope.grid.data) {
                            if ($scope.grid.data[j].ProspectName == null) {
                                $scope.grid.data[j].ProspectName = $scope.grid.data[j].Name;
                            }
                        }
                        $scope.loading = false;
						
						if($scope.filter.SOStatus==null)
						{
							$scope.filter.SOStatus=$scope.getSOstatus[0].SOStatusId;
						}
						
                        return res.data.Result;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
        }
        $scope.changeFormatDate = function(item) {
            var tmpParam = item;
            tmpParam = new Date(tmpParam);
            var finalDate
            var yyyy = tmpParam.getFullYear().toString();
            var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpParam.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            
            return finalDate;
        }

        // $scope.SelectedCodeInvoice = function (SelectedKodePajak) {
           // console.log('SelectedKodePajak',SelectedKodePajak);
            // if(SelectedKodePajak.Name == '08-Penyerahan oleh PKP yang mendapat fasilitas PPN dibebaskan(Duty Free)' || SelectedKodePajak.Name == '07-Penyerahan oleh PKP yang mendapat fasilitas PPN tidak dipungut'){
                // $scope.ViewData.FreePPNBit = true;
            // }else{
                // $scope.ViewData.FreePPNBit = false;
            // }
        // }
		$scope.SelectedCodeInvoice = function(Id) {
            //if (Id != undefined) {
                ComboBoxFactory.getKodePajakbyId(Id.CodeInvoiceTransactionTaxId).then(function(res) {
                    $scope.KodePajak = res.data;
                    if (($scope.KodePajak.Code == '01') || ($scope.KodePajak.Code == '08')) {
                        $scope.MaintainSODisableFreePPN = false;
                        
                        
                        $scope.ViewData.FreePPNBit = false;
                    } else if (($scope.KodePajak.Code != '01') || ($scope.KodePajak.Code != '08')) {
                        $scope.MaintainSODisableFreePPN = true;
                        
                        if ($scope.KodePajak.Code == '07') {
                            
                            $scope.ViewData.FreePPNBit = true;
                        } else if ($scope.KodePajak.Code != '07') {
                            
                            $scope.ViewData.FreePPNBit = false;
                        }
                    }
                    $scope.MaintainSOPaksaItungUlangRincian();
                    $scope.ViewData.TampungTotalVAT = 0;
                })
            //}
        }

        $scope.ChangeSOType = function() {
            
			for(var i = 0; i < $scope.getSOType.length; ++i)
			{	
				if($scope.getSOType[i].SOTypeId == $scope.ViewData.SOTypeId)
				{
					$scope.ViewData.SOTypeName=$scope.getSOType[i].SOTypeName;
					break;
				}
			}
			
			if ($scope.ViewData.SOTypeId == "1") {
                console.log('1 ===>', $scope.ViewData.SOTypeName);
                console.log('SO type Finish Unit');
                if($scope.flagStatus == "lihatSO")
				{
                    $scope.btnSimpanCreateSO = false;
				}
				else
				{
					$scope.btnSimpanCreateSO = true;
				}
            } else if ($scope.ViewData.SOTypeId == "2") {
                console.log('2 ===>', $scope.ViewData.SOTypeName);
                console.log('SO type OPD');
                $scope.ShowMenuTabs = true;
                $scope.ShowMenuTabs_FinishUnit = false;              
				if($scope.flagStatus == "lihatSO")
				{
					$scope.btnSimpanCreateSO = false;
				}
				else
				{
					$scope.btnSimpanCreateSO = true;
				}
                $scope.ShowtabContainer_FinishUnit = false;
                $scope.ShowtabContainer = true;
                $scope.InfoPelanggan = true;
                $scope.ShowPelangganOPD = true;
                $scope.ShowPeranPelanggan = false;
                $scope.JasaPengurusanDokumen = true;
                $scope.ShowNorangka = true;//tadinya ini false diresmiin tg 28 juni 2019
                $scope.Aksesoris = false;
                $scope.Dokumen = true;
                $scope.InfoPengiriman = true;
                $scope.RincianHarga = true;
                $scope.karoseriFinishUnit = false;
                $scope.InfoLeasingFinishUnit = false;
                $scope.InfoAsuransiFinishUnit = false;
                $scope.finishUnitInfounityangakandibeli = false;
                $scope.InfoMediatorFinishUnit = false;
                $scope.InfoPemakaiFinishUnit = false;

            } else if ($scope.ViewData.SOTypeId == "3") {
                console.log('3 ===>', $scope.ViewData.SOTypeName);
                console.log('SO type Purna Jual');
                $scope.ShowMenuTabs = true;
                $scope.ShowMenuTabs_FinishUnit = false;
                if($scope.flagStatus == "lihatSO")
				{
					$scope.btnSimpanCreateSO = false;
				}
				else
				{
					$scope.btnSimpanCreateSO = true;
				}
                $scope.ShowtabContainer_FinishUnit = false;
                $scope.ShowtabContainer = true;
                $scope.ShowNorangka = true;
                $scope.ShowPeranPelanggan = false;
                $scope.InfoPelanggan = true;
                $scope.ShowPelangganOPD = true;
                $scope.JasaPengurusanDokumen = false;
                $scope.Aksesoris = true;
                $scope.Dokumen = false;
                $scope.InfoPengiriman = true;
                $scope.RincianHarga = true;
                $scope.karoseriFinishUnit = false;
                $scope.InfoLeasingFinishUnit = false;
                $scope.InfoAsuransiFinishUnit = false;
                $scope.finishUnitInfounityangakandibeli = false;
                $scope.InfoMediatorFinishUnit = false;
                $scope.InfoPemakaiFinishUnit = false;
            } else if ($scope.ViewData.SOTypeId == "4") {
                console.log('4 ===>', $scope.ViewData.SOTypeName);
                console.log('SO type Swapping');
                if($scope.flagStatus == "lihatSO")
				{
					$scope.btnSimpanCreateSO = false;
				}
				else
				{
					$scope.btnSimpanCreateSO = true;
				}
                $scope.ShowMenuTabs_FinishUnit = false;
                $scope.ShowtabContainer_FinishUnit = false;
				$scope.ShowMenuTabs = true;
                $scope.ShowtabContainer = true;
                $scope.InfoPelanggan = false;
                $scope.ShowNorangka = false;
                $scope.JasaPengurusanDokumen = false;
                $scope.Aksesoris = false;
                $scope.Dokumen = false;
                $scope.InfoPengiriman = true;
                $scope.RincianHarga = true;
            }
			
			if($scope.ViewData.SOTypeId == "3")
			{
				$scope.columnDefsGridAksesoris[11].visible = true;
				$scope.gridApiAksesorisDepanMaintainSo.grid.refresh();
			// } else if($scope.ViewData.SOTypeId == "1" && $scope.flagStatus == "UpdateSO")
			// {
            //     console.log("ini diaaaaaa", $scope.columnDefsGridAksesoris[11].visible)
            //     console.log("ini diaaaaaa2", $scope.gridApiAksesorisDepanMaintainSo.grid)
			// 	$scope.columnDefsGridAksesoris[11].visible = true;
			// 	$scope.gridApiAksesorisDepanMaintainSo.grid.refresh();
			}
			else
			{
				$scope.columnDefsGridAksesoris[11].visible = false;
				$scope.gridApiAksesorisDepanMaintainSo.grid.refresh();
			}
        };

      
        $scope.ChangePelanggan = function() {
            if ($scope.ViewData.CustomerTypeId == "3") {
                $scope.IndividuShow = true;
                $scope.InstitusiShow = false;
				
				$scope.infoPembeliPerusahaan=false;
				$scope.infoPenanggungJawabPerusahaan=false;
				$scope.infoPemilikPerusahaan=false;
            } else {
                $scope.IndividuShow = false;
                $scope.InstitusiShow = true;
            }
        }

        $scope.ChangeSales = function() {
            if ($scope.ViewData.SOTypeId == 3 || $scope.ViewData.SOTypeId == 2) {
                if($scope.ViewData.CustomerTypeId==null || $scope.ViewData.CustomerTypeId=='' || typeof $scope.ViewData.CustomerTypeId=='undefined'){
                    $scope.ViewData.CustomerTypeId = 0;

                }

                console.log('$scope.ViewData ===>', $scope.ViewData.EmployeeId);
                var getSalesId = "&SalesId=" + $scope.ViewData.EmployeeId+ "&CustCategoryTypeId=" + $scope.ViewData.CustomerTypeId;
                MaintainSOFactory.getNoRangka(getSalesId).then(
                    function(res) {
                        $scope.DataNorangka = res.data.Result;
                        console.log('get FrameNo berdasarkan Salesman ===>', $scope.DataNorangka);
                        return res.data;//tadi yang if id 2 ga ada 28 juni 2019 diresmiin andry
                    }
                );
            }
        }

        $scope.pilihFrameNo = function(selected) {
            console.log("selected===>",selected);

            
            
            if (selected == undefined || selected == null || selected == "") {
                $scope.generateFrameNoByComboBox = false;
                $scope.VehicleId = null;
                console.log('generateFrameNoByComboBox ===> false');
            }else{
                $scope.generateFrameNoByComboBox = true;
                $scope.VehicleId = angular.copy(selected.VehicleId);
                console.log('generateFrameNoByComboBox ===> true');

            }
            if($scope.flagStatus == "InsertSO" || $scope.flagStatus == "UpdateSO")
            {
                $scope.resultFrame = selected;
                $scope.ViewData.FrameNo = $scope.resultFrame.FrameNo;
                $scope.ViewData.EngineNoExt = $scope.resultFrame.EngineNo;
                $scope.ViewData.Name = $scope.resultFrame.CustomerName;
                $scope.ViewData.KTP = $scope.resultFrame.KTPKITAS;
                $scope.ViewData.SIUP = $scope.resultFrame.SIUP;
                $scope.ViewData.NPWP = $scope.resultFrame.NPWP;
                $scope.ViewData.Handphone = $scope.resultFrame.Handphone;
                $scope.ViewData.Address = $scope.resultFrame.Address;
                $scope.ViewData.ProspectId = $scope.resultFrame.ProspectId;
                $scope.ViewData.VehicleTypeId = $scope.resultFrame.VehicleTypeId;
            }
            // $scope.resultFrame = selected;
            // $scope.ViewData.FrameNo = $scope.resultFrame.FrameNo;
            // $scope.ViewData.Name = $scope.resultFrame.CustomerName;
            // $scope.ViewData.KTP = $scope.resultFrame.KTPKITAS;
            // $scope.ViewData.SIUP = $scope.resultFrame.SIUP;
            // $scope.ViewData.NPWP = $scope.resultFrame.NPWP;
            // $scope.ViewData.Handphone = $scope.resultFrame.Handphone;
            // $scope.ViewData.Address = $scope.resultFrame.Address;
            // $scope.ViewData.ProspectId = $scope.resultFrame.ProspectId;
        }

        $scope.pilihPembayaran = function(selected) {
            $scope.resultPayment = selected;
            if ($scope.resultPayment.PaymentTypeName == "Cash") {
                $scope.disabledBank = true;
                $scope.ViewData.BankId = "";
                $scope.ViewData.AccountNo = "";
            } else {
                $scope.disabledBank = false;
            }
        }

        MaintainSOFactory.getCustomerCategory().then(
            function(res) {
                var tempCategory = res.data.Result;
                $scope.getCustomerCategory = tempCategory.filter(function(type) {
                    return (type.ParentId == 2 || type.CustomerTypeId == 3);
                })
            }
        );

        MaintainSOFactory.getPeranPelanggan().then(
            function(res) {
                $scope.getPeranPelanggan = res.data.Result;

                
                if ($scope.ViewData.CustomerTypeDesc == 'Individu'){
                    $scope.getPeranPelanggan.splice(2, 1);
                    return res.data;
                }else{
                    return res.data;
                }
                
                
            }
        );

        MaintainSOFactory.getCancelreason().then(
            function(res) {
                $scope.getCancelreason = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getOutlet().then(
            function(res) {
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getDataBank().then(
            function(res) {
                $scope.getDataBank = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getDataSalesman().then(
            function(res) {
                $scope.getDataSalesman = res.data.Result;
                return res.data;
            }
        );

        TipeFactory.getData().then(
            function(res) {
                $scope.listTipe = res.data.Result;
                return res.data;
            }
        );

        $scope.ListMasterBiroJasa = function() {

        

            if (($scope.selectedTipe.VehicleType == null || $scope.selectedTipe.VehicleType == undefined || $scope.selectedTipe.VehicleType == '') && ($scope.selectedModel.VehicleModel == null || $scope.selectedModel.VehicleModel == undefined || $scope.selectedModel.VehicleModel == '') ){
                bsNotify.show({
                    title: "Peringatan",
                    content: "Isi Model dan Tipe untuk melakukan pencarian ",
                    type: 'warning'
                });
            }else{

                MaintainSOFactory.getDataJasaDokumen('start=1&limit=1000&MaterialTypeId=3&VehicleTypeId=' + $scope.SelectedTipe.VehicleTypeId).then(
                    function (res) {
                        $scope.gridDataJBiroJasaSO.data = res.data.Result;
                        for (var i = 0; i < $scope.gridDataJBiroJasaSO.data.length; ++i) {
                            $scope.gridDataJBiroJasaSO.data[i].Qty = 1;
                        }
                        $scope.loading = false;
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );

            }
            
			// var da_shitty_param="";
			
			// if($scope.selectedVendorMaintainSo==null)
			// {
				// da_shitty_param='start=1&limit=1000&MaterialTypeId=3&VehicleTypeId='+ $scope.SelectedTipe.VehicleTypeId;
			// }
			// else
			// {
				// da_shitty_param='start=1&limit=1000&MaterialTypeId=3&VehicleTypeId='+ $scope.SelectedTipe.VehicleTypeId+'&ServiceBureauId='+$scope.selectedVendorMaintainSo;
			// }
			
			
        }

        $scope.btnTambahJasaDokumen = function() {
            $scope.selectedModel = [];
            $scope.selectedTipe = [];
            $scope.gridDataJBiroJasaSO.data = [];
			//$scope.selectedVendorMaintainSo = null;

            var numLength = angular.element('.ui.modal.PengurusanJasaDokumenSO').length;
            setTimeout(function () {
                angular.element('.ui.modal.PengurusanJasaDokumenSO').modal('refresh');
            }, 0);

            if (numLength > 1) {
                angular.element('.ui.modal.PengurusanJasaDokumenSO').not(':first').remove();
            }

            angular.element(".ui.modal.PengurusanJasaDokumenSO").modal("show");
            //angular.element('.ui.modal.PengurusanJasaDokumenSO').modal('show');
            
        }

        $scope.btnBatalPengurusanJasa = function() {
           
            angular.element('.ui.modal.PengurusanJasaDokumenSO').modal('hide');
            angular.element('.ui.modal.Dokumen').modal('hide');
            angular.element('.ui.modal.Aksesoris').modal('hide');
        }

        $scope.btnPilihPengurusanJasa = function() {
            angular.element('.ui.modal.PengurusanJasaDokumenSO').modal('hide');
			
            var sque = 0;
			
			if ($scope.gridJasaPengurusanDokumen.data.length == 0) {
					for (var i in $scope.selctedRow) {
						sque++;
						$scope.gridJasaPengurusanDokumen.data.push({
							ItemNo: sque,
							ServiceId: $scope.selctedRow[i].ServiceId,
							OutletId: $scope.user.OutletId,
							ServiceCode: $scope.selctedRow[i].ServiceCode,
							ServiceName: $scope.selctedRow[i].ServiceName,
							PRCode: $scope.selctedRow[i].PRCode,
							StatusPRName: $scope.selctedRow[i].StatusPRName,
							Qty: $scope.selctedRow[i].Qty,
							VehicleTypeId: $scope.selctedRow[i].VehicleTypeId,
							VehicleTypeDesc: $scope.selctedRow[i].VehicleTypeDesc+" - "+$scope.selctedRow[i].Katsu,
							AdminFee: $scope.selctedRow[i].AdminFee
						});
					}
				} else {
					for (var i in $scope.gridJasaPengurusanDokumen.data) {
						for (var j in $scope.selctedRow) {
							if ($scope.selctedRow[j].ServiceId == $scope.gridJasaPengurusanDokumen.data[i].ServiceId) {
								$scope.selctedRow.splice(j, 1);
							}
						}
					}
					for (var i in $scope.selctedRow) {
						sque++;
						$scope.gridJasaPengurusanDokumen.data.push({
							ItemNo: sque,
							ServiceId: $scope.selctedRow[i].ServiceId,
							OutletId: $scope.user.OutletId,
							ServiceCode: $scope.selctedRow[i].ServiceCode,
							ServiceName: $scope.selctedRow[i].ServiceName,
							PRCode: $scope.selctedRow[i].PRCode,
							StatusPRName: $scope.selctedRow[i].StatusPRName,
							Qty: $scope.selctedRow[i].Qty,
							VehicleTypeId: $scope.selctedRow[i].VehicleTypeId,
							VehicleTypeDesc: $scope.selctedRow[i].VehicleTypeDesc+" - "+$scope.selctedRow[i].Katsu,
							AdminFee: $scope.selctedRow[i].AdminFee
						});
					}
				}
            
            $scope.selctedRow = [];
            //console.log("Pilih", $scope.selctedRow);

            //Pricing Engine
            $scope.blabla = $scope.gridJasaPengurusanDokumen.data;
            $scope.urlPricing = '/api/fe/PricingEngine?PricingId=30401&OutletId=' + $scope.user.OutletId + '&DisplayId=' + $scope.user.OutletId + '30401';
            $scope.jsData = { Title: null, HeaderInputs: { $TotalDPP$: null, $TotalVAT$: null, $GrandTotal$: 0 } }
                //$scope.jsData.HeaderInputs.$OutletId$ = $scope.user.OutletId;
            $scope.jsData.HeaderInputs.$TotalDPP$ = $scope.TotalDpp;
            $scope.jsData.HeaderInputs.$TotalVAT$ = $scope.PPN;
            $scope.jsData.HeaderInputs.$GrandTotal$ = $scope.GrandTotal;
            $scope.GetDataFromJasa();
        }
		
		$scope.MaintainSOPaksaItungUlangRincian = function() {
			if($scope.flagStatus == "InsertSO" ||$scope.flagStatus == "UpdateSO")
			{
				setTimeout(function() {
					if($scope.ViewData.SOTypeId==2)
					{
						$scope.blabla = $scope.gridJasaPengurusanDokumen.data;
						//$scope.GetDataFromJasa();
						$scope.GetDataFromJasaPaksaanCentanganBaru();
					}
					else if($scope.ViewData.SOTypeId==3)
					{
						$scope.accesoris = $scope.gridAksesoris.data;
						$scope.GetDataFromAksesoris();
					}
					
                }, 1000);
			}
		}

        $scope.GetDataFromJasa = function() {
            $scope.ViewData.PPN = 0;
            $scope.ViewData.TotalDpp = 0;
            $scope.ViewData.GrandTotal = 0;
			$scope.ViewData.TotalVAT=0;
			
			var DaMaintainSoFreePpnThingy1=0;
			if($scope.ViewData.FreePPNBit==true)
			{
				DaMaintainSoFreePpnThingy1=1;
			}
			else
			{
				DaMaintainSoFreePpnThingy1=0;
			}
			
            angular.forEach($scope.blabla, function(acc, accIdx) {
				var param="";
				param = {
                    "Title": "Order Pengurusan Dokumen",
                    "HeaderInputs": {
                        "$ServiceId$": acc.ServiceId,
                        "$OutletId$": acc.OutletId,
                        "$QTY$": acc.Qty,
                        "$AdminFee$": acc.AdminFee,
						"$FreePPNBit$": DaMaintainSoFreePpnThingy1,
						"$ServicePriceInput$": -1,
						"$VehicleTypeId$": acc.VehicleTypeId
                    }
					//,"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
					//tadi yg service price ga ada

                };
				
				if($scope.gridJasaPengurusanDokumen.data[accIdx].StatusPRName=="Dibuat")
				{
					try
					{
						param = {
							"Title": "Order Pengurusan Dokumen",
							"HeaderInputs": {
								"$ServiceId$": acc.ServiceId,
								"$OutletId$": acc.OutletId,
								"$QTY$": acc.Qty,
								"$AdminFee$": acc.AdminFee,
								"$FreePPNBit$": DaMaintainSoFreePpnThingy1,
								"$ServicePriceInput$": $scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice,
								"$VehicleTypeId$": acc.VehicleTypeId
							}
							//,"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
							//tadi yg service price ga ada

						};
					}
					catch(eeee)
					{
						param = {
							"Title": "Order Pengurusan Dokumen",
							"HeaderInputs": {
								"$ServiceId$": acc.ServiceId,
								"$OutletId$": acc.OutletId,
								"$QTY$": acc.Qty,
								"$AdminFee$": acc.AdminFee,
								"$FreePPNBit$": DaMaintainSoFreePpnThingy1,
								"$ServicePriceInput$": $scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice,
								"$VehicleTypeId$": null
							}
							//,"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
							//tadi yg service price ga ada

						};
					}
				}

                $scope.JumlahSementara = 0;
                $http.post('/api/fe/PricingEngine?PricingId=30001&OutletId=' + $scope.user.OutletId + '&DisplayId=' + $scope.user.OutletId + '30001', param)
                    .then(function(res) {
                        $scope.blabla[accIdx].ServicePrice = res.data.Codes["#ServicePrice#"];
                        $scope.blabla[accIdx].AdminFee = res.data.Codes["$AdminFee$"];
                        $scope.blabla[accIdx].QTY = res.data.Codes["[QTY]"];
                        $scope.blabla[accIdx].DPP = res.data.Codes["[DPP]"];
                        $scope.blabla[accIdx].VAT = res.data.Codes["[PPN]"];
                        $scope.blabla[accIdx].PriceAfterTax = res.data.Codes["[PriceAfterTax]"];
                        $scope.blabla[accIdx].SubTotal = res.data.Codes["[SubTotal]"];
                        $scope.blabla[accIdx].Total = res.data.Codes["[Total]"];
                        $scope.blabla[accIdx].Selisih = res.data.Codes["[Selisih]"];
						//dhemit
						$scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice=angular.copy(res.data.Codes["[HargaService]"]);

                        $scope.ViewData.PPN = $scope.ViewData.PPN + parseInt(res.data.Codes["[PPN]"]);
                        // $scope.ViewData.TotalDpp = $scope.ViewData.TotalDpp + parseInt(res.data.Codes["[DPP]"]);
                        $scope.ViewData.TotalDpp = $scope.ViewData.TotalDpp + parseInt(res.data.Codes["[DPP]"] * parseInt(res.data.Codes["[QTY]"]));
                        
                        // $scope.ViewData.GrandTotal = $scope.ViewData.GrandTotal + parseInt(res.data.Codes["[Total]"]);
                        // $scope.ViewData.TotalVAT= $scope.ViewData.TotalVAT + parseInt(res.data.Codes["[PPN]"]);
                        $scope.ViewData.TampungTotalDpp =  $scope.ViewData.TotalDpp;
                        // $scope.ViewData.TampungTotalDpp = parseInt(res.data.Codes["[QTY]"]) * $scope.ViewData.TotalDpp;
                        // $scope.ViewData.TampungTotalVAT = parseInt(res.data.Codes["[QTY]"]) * $scope.ViewData.TotalVAT;
                        // Rounding CR4
                        // $scope.ViewData.TampungTotalVAT = Math.floor(0.1 * $scope.ViewData.TampungTotalDpp) ;
                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.TampungTotalVAT = 0 ;
                        }else{
                            // $scope.ViewData.TampungTotalVAT = Math.floor(0.1 * $scope.ViewData.TampungTotalDpp) ; 
                            $scope.ViewData.TampungTotalVAT = Math.floor((PPNPerc/100) * $scope.ViewData.TampungTotalDpp) ; 

                        }
                        $scope.ViewData.GrandTotal =  $scope.ViewData.TampungTotalVAT + $scope.ViewData.TampungTotalDpp;
                        $scope.ViewData.TotalVAT= $scope.ViewData.TampungTotalVAT;
                        
                        // $scope.ViewData.Pembulatan = $scope.ViewData.GrandTotal - $scope.ViewData.TampungTotalDpp - $scope.ViewData.TampungTotalVAT;
                        $scope.jumlahHarga = 0;
                        $scope.jumlahHarga = (parseInt(res.data.Codes["[HargaService]"] * parseInt(res.data.Codes["[QTY]"])));
                        console.log('$scope.jumlahHarga',$scope.jumlahHarga);
                        $scope.JumlahSementara = $scope.JumlahSementara + angular.copy($scope.jumlahHarga);
                         
                        console.log('$scope.JumlahSementara',$scope.JumlahSementara);

                        $scope.ViewData.TampungHarga = $scope.JumlahSementara;
                        console.log('$scope.ViewData.TampungHarga1',$scope.ViewData.TampungHarga);   
                        // $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.Pembulatan = 0;
                        }else{
                            $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        } 
                        
                    });
            });
        }
		
		$scope.GetDataFromJasaPaksaanCentanganBaru = function() {
            $scope.ViewData.PPN = 0;
            $scope.ViewData.TotalDpp = 0;
            $scope.ViewData.GrandTotal = 0;
			$scope.ViewData.TotalVAT=0;
			
			var DaMaintainSoFreePpnThingy1=0;
			if($scope.ViewData.FreePPNBit==true)
			{
				DaMaintainSoFreePpnThingy1=1;
			}
			else
			{
				DaMaintainSoFreePpnThingy1=0;
			}
			
            angular.forEach($scope.blabla, function(acc, accIdx) {
				var param="";
				
				
				try
				{
					param = {
						"Title": "Order Pengurusan Dokumen",
						"HeaderInputs": {
							"$ServiceId$": acc.ServiceId,
							"$OutletId$": acc.OutletId,
							"$QTY$": acc.Qty,
							"$AdminFee$": acc.AdminFee,
							"$FreePPNBit$": DaMaintainSoFreePpnThingy1,
							"$ServicePriceInput$": $scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice,
							"$VehicleTypeId$": acc.VehicleTypeId
						}
						//,"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
						//tadi yg service price ga ada

					};
				}
				catch(eeee)
				{
					param = {
						"Title": "Order Pengurusan Dokumen",
						"HeaderInputs": {
							"$ServiceId$": acc.ServiceId,
							"$OutletId$": acc.OutletId,
							"$QTY$": acc.Qty,
							"$AdminFee$": acc.AdminFee,
							"$FreePPNBit$": DaMaintainSoFreePpnThingy1,
							"$ServicePriceInput$": $scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice,
							"$VehicleTypeId$": null
						}
						//,"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
						//tadi yg service price ga ada

					};
				}

                $scope.JumlahSementara = 0;
                $http.post('/api/fe/PricingEngine?PricingId=30001&OutletId=' + $scope.user.OutletId + '&DisplayId=' + $scope.user.OutletId + '30001', param)
                    .then(function(res) {
                        $scope.blabla[accIdx].ServicePrice = res.data.Codes["#ServicePrice#"];
                        $scope.blabla[accIdx].AdminFee = res.data.Codes["$AdminFee$"];
                        $scope.blabla[accIdx].QTY = res.data.Codes["[QTY]"];
                        $scope.blabla[accIdx].DPP = res.data.Codes["[DPP]"];
                        $scope.blabla[accIdx].VAT = res.data.Codes["[PPN]"];
                        $scope.blabla[accIdx].PriceAfterTax = res.data.Codes["[PriceAfterTax]"];
                        $scope.blabla[accIdx].SubTotal = res.data.Codes["[SubTotal]"];
                        $scope.blabla[accIdx].Total = res.data.Codes["[Total]"];
                        $scope.blabla[accIdx].Selisih = res.data.Codes["[Selisih]"];
						//dhemit
						$scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice=angular.copy(res.data.Codes["[HargaService]"]);

                        $scope.ViewData.PPN = $scope.ViewData.PPN + parseInt(res.data.Codes["[PPN]"]);
                        // $scope.ViewData.TotalDpp = $scope.ViewData.TotalDpp + parseInt(res.data.Codes["[DPP]"]);
                        $scope.ViewData.TotalDpp = $scope.ViewData.TotalDpp + parseInt(res.data.Codes["[DPP]"] * parseInt(res.data.Codes["[QTY]"]));
                        // $scope.ViewData.GrandTotal = $scope.ViewData.GrandTotal + parseInt(res.data.Codes["[Total]"]);
                        // $scope.ViewData.TotalVAT= $scope.ViewData.TotalVAT + parseInt(res.data.Codes["[PPN]"]);
                        // $scope.ViewData.TampungTotalDpp = parseInt(res.data.Codes["[QTY]"]) * $scope.ViewData.TotalDpp;
                        $scope.ViewData.TampungTotalDpp = $scope.ViewData.TotalDpp;
                        // $scope.ViewData.TampungTotalVAT = parseInt(res.data.Codes["[QTY]"]) * $scope.ViewData.TotalVAT;
                        // Rounding CR4
                        // $scope.ViewData.TampungTotalVAT = Math.floor(0.1 * $scope.ViewData.TampungTotalDpp) ;
                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.TampungTotalVAT = 0 ;
                        }else{
                            // $scope.ViewData.TampungTotalVAT = Math.floor(0.1 * $scope.ViewData.TampungTotalDpp) ; 
                            $scope.ViewData.TampungTotalVAT = Math.floor((PPNPerc/100) * $scope.ViewData.TampungTotalDpp) ; 

                        }
                        $scope.ViewData.GrandTotal =  $scope.ViewData.TampungTotalVAT + $scope.ViewData.TampungTotalDpp;
                        $scope.ViewData.TotalVAT= $scope.ViewData.TampungTotalVAT;
                        // $scope.ViewData.Pembulatan = $scope.ViewData.GrandTotal - $scope.ViewData.TampungTotalDpp - $scope.ViewData.TampungTotalVAT;
                        $scope.jumlahHarga = 0;
                        $scope.jumlahHarga = (parseInt(res.data.Codes["[HargaService]"] * parseInt(res.data.Codes["[QTY]"])));
                        console.log('$scope.jumlahHarga',$scope.jumlahHarga);
                        $scope.JumlahSementara = $scope.JumlahSementara + angular.copy($scope.jumlahHarga);
                         
                        console.log('$scope.JumlahSementara',$scope.JumlahSementara);

                        $scope.ViewData.TampungHarga = $scope.JumlahSementara;
                        console.log('$scope.ViewData.TampungHarga1',$scope.ViewData.TampungHarga);
                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.Pembulatan = 0;
                        }else{
                            $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        }   
                       
                    });
            });
        }

        $scope.ListMasterAksesoris = function() {

            $scope.mAksesor = true;
            $scope.mKaroseri =false;

           
            var getaksesoris = "?start=1&limit=1000&VehicleTypeId=" + $scope.ViewData.VehicleTypeId;
            
            console.log('$scope.ViewData.VehicleTypeId cari aksesoris ===>', $scope.ViewData.VehicleTypeId);
            MaintainSOFactory.getDataAksesorisBelokan(getaksesoris).then(
                function(res) {
                    $scope.gridDataAksesoris.data = res.data.Result;

                    for (var i = 0; i < $scope.gridDataAksesoris.data.length; ++i) {
                        $scope.gridDataAksesoris.data[i].Qty = 1;
                    }

                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.ListMasterKaroseri = function() {

            $scope.mAksesor = false;
            $scope.mKaroseri = true;

           
            var getkaroseri = "&VehicleTypeId=" + $scope.resultFrame.VehicleTypeId;
        

            MaintainSOFactory.getDataKaroseri(getkaroseri).then(
                function(res) {
                    $scope.gridDataKaroseri.data = res.data.Result;

                    for (var i = 0; i < $scope.gridDataKaroseri.data.length; ++i) {
                        $scope.gridDataKaroseri.data[i].Qty = 1;
                    }

                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.onSelectModelChanged = function (ModelSelected) {
            console.log('on model change ===>', ModelSelected);

            if (ModelSelected == undefined || ModelSelected == null) {
                $scope.ViewData.VehicleModelName = "";
                $scope.ViewData.VehicleModelId = "";
                $scope.optionsTipe = [];
                $scope.ViewData.VehicleTypelName = "";
                $scope.ViewData.VehicleTypeId = "";

            } else {
                $scope.ViewData.VehicleModelName = ModelSelected.VehicleModelName;
                $scope.ViewData.VehicleModelId = ModelSelected.VehicleModelId;
                MaintainSOFactory.getVehicleType('?start=1&limit=100&filterData=VehicleModelId|' + ModelSelected.VehicleModelId).then(function (res) {
                    $scope.optionsTipe = [];
                    $scope.ViewData.VehicleTypelName = "";
                    $scope.ViewData.VehicleTypeId = "";
                    $scope.optionsTipe = res.data.Result;
                    return $scope.optionsTipe;
                });
            }

        }

        $scope.onSelectTipeChanged = function (TipeSelected) {
            console.log('TipeSelected ===>', TipeSelected);
            if (TipeSelected == undefined || TipeSelected == null) {
                $scope.ViewData.VehicleTypelName = "";
                $scope.ViewData.VehicleTypeId = "";

            } else {
                $scope.ViewData.VehicleTypelName = TipeSelected.Description;
                $scope.ViewData.VehicleTypeId = TipeSelected.VehicleTypeId;
            }
        }

        $scope.btnTambahAksesoris = function() {
           

            console.log('$scope.VehicleId ===>', $scope.VehicleId)
            if ($scope.VehicleId == null || $scope.VehicleId == undefined || $scope.VehicleId == "" ){
                $scope.generateFrameNoByComboBox = false;
                console.log('1')
                
                
            }else{
                $scope.generateFrameNoByComboBox = true;
                console.log('2')
                console.log('1')
                
            }

            if ($scope.ViewData.SOTypeName != "Finish Unit"){
                console.log('3');
                console.log('$scope.ViewData',$scope.ViewData);

                if ($scope.ViewData.FrameNo == null || $scope.ViewData.FrameNo == undefined || $scope.ViewData.FrameNo == "") {
                
                    bsNotify.show({
                                title: "Peringatan",
                                content: "Pilih Nomor Frame",
                                type: 'warning'
                            });
                }
                else {
                    console.log('4')
                    console.log('$scope.generateFrameNoByComboBox ===>', $scope.generateFrameNoByComboBox );
                    $scope.mAksesor = true;
                    $scope.mKaroseri = false;
            
                    if ($scope.generateFrameNoByComboBox == false){
                        $scope.manualFrameNumberField = true;
                        MaintainSOFactory.getDataModel().then(function (res) {
                            $scope.optionsModel = res.data.Result;
                            console.log('$scope.optionsModel ==>', $scope.optionsModel)
                            return $scope.optionsModel;
                        });
                    } else if ($scope.generateFrameNoByComboBox == true){
                        $scope.manualFrameNumberField = false;
                        if ($scope.ViewData.MaterialTypeId == 1) {
                            $scope.mAksesor = true;
                            $scope.mKaroseri = false;
                            // $scope.ListMasterAksesoris();
                        } else {
                            $scope.mAksesor = false;
                            $scope.mKaroseri = true;
                            // $scope.ListMasterKaroseri();
                        }
                    }else{
                        console.log('ini klik tambah yang tidak masuk kategori manapun');
                    }
    
                        
                    // }
                    // setTimeout(function() {
                    // angular.element('.ui.modal.Aksesoris').modal('refresh');
                    // }, 0);
                    console.log("masuk sini>>");
                    var numLength = angular.element('.ui.fullscreen.modal.Aksesoris').length;
                    setTimeout(function () {
                        angular.element('.ui.fullscreen.modal.Aksesoris').modal('refresh');
                    }, 0);
        
                    if (numLength > 1) {
                        angular.element('.ui.fullscreen.modal.Aksesoris').not(':first').remove();
                    }
        
                    //angular.element(".ui.fullscreen.modal.Aksesoris").modal("show");
                    angular.element('.ui.fullscreen.modal.Aksesoris').modal({observeChanges: true}).modal('show');
                    $scope.gridDataAksesoris.data = [];
                    $scope.gridDataKaroseri.data = [];
                } 

            }


            // else {
            //     console.log('4')
            //     console.log('$scope.generateFrameNoByComboBox ===>', $scope.generateFrameNoByComboBox );
            //     $scope.mAksesor = true;
            //     $scope.mKaroseri = false;
		
            //     if ($scope.generateFrameNoByComboBox == false){
            //         $scope.manualFrameNumberField = true;
            //         MaintainSOFactory.getDataModel().then(function (res) {
            //             $scope.optionsModel = res.data.Result;
            //             console.log('$scope.optionsModel ==>', $scope.optionsModel)
            //             return $scope.optionsModel;
            //         });
            //     } else if ($scope.generateFrameNoByComboBox == true){
            //         $scope.manualFrameNumberField = false;
            //         if ($scope.ViewData.MaterialTypeId == 1) {
            //             $scope.mAksesor = true;
            //             $scope.mKaroseri = false;
            //             // $scope.ListMasterAksesoris();
            //         } else {
            //             $scope.mAksesor = false;
            //             $scope.mKaroseri = true;
            //             // $scope.ListMasterKaroseri();
            //         }
            //     }else{
            //         console.log('ini klik tambah yang tidak masuk kategori manapun');
            //     }

					
			// 	// }
            //     // setTimeout(function() {
            //     // angular.element('.ui.modal.Aksesoris').modal('refresh');
            //     // }, 0);
            //     console.log("masuk sini>>");
            //     var numLength = angular.element('.ui.fullscreen.modal.Aksesoris').length;
            //     setTimeout(function () {
            //         angular.element('.ui.fullscreen.modal.Aksesoris').modal('refresh');
            //     }, 0);
    
            //     if (numLength > 1) {
            //         angular.element('.ui.fullscreen.modal.Aksesoris').not(':first').remove();
            //     }
    
            //     //angular.element(".ui.fullscreen.modal.Aksesoris").modal("show");
            //     angular.element('.ui.fullscreen.modal.Aksesoris').modal({observeChanges: true}).modal('show');
            //     $scope.gridDataAksesoris.data = [];
            //     $scope.gridDataKaroseri.data = [];
            // }

        }

        $scope.getMaterial = function(selected) {
            
			$scope.tMaterial = selected;
            if ($scope.tMaterial.MaterialTypeId == 1) {
                $scope.mAksesor = true;
                $scope.mKaroseri = false;
                $scope.ListMasterAksesoris();
            } else {
                $scope.mAksesor = false;
                $scope.mKaroseri = true;
                $scope.ListMasterKaroseri();
            }
        }

        $scope.btnPilihAksesoris = function() {
			if($scope.ViewData.MaterialTypeId==1)
			{
				$scope.tMaterial={MaterialTypeId: 1,MaterialName: "Aksesoris"};
			}
			else if($scope.ViewData.MaterialTypeId==2)
			{
				$scope.tMaterial={MaterialTypeId: 2,MaterialName: "Karoseri" };
			}
			
			
			
            angular.element('.ui.modal.Aksesoris').modal('hide');
            if ($scope.tMaterial.MaterialTypeId == 1) {
                var sque = 0;
                if ($scope.gridAksesoris.data.length == 0) {
                    for (var i in $scope.selctedRow) {
                        sque++;
                        $scope.gridAksesoris.data.push({
                            ItemNo: sque,
                            MaterialId: $scope.tMaterial.MaterialId,
							MaterialTypeId: angular.copy($scope.ViewData.MaterialTypeId),
                            AccessoriesId: $scope.selctedRow[i].PartsId,
                            AccessoriesCode: $scope.selctedRow[i].PartsCode,
                            AccessoriesName: $scope.selctedRow[i].PartsName,
                            UomName: $scope.selctedRow[i].UomName,
                            RetailPrice: $scope.selctedRow[i].RetailPrice,
                            Qty: $scope.selctedRow[i].Qty,
                            Discount: $scope.selctedRow[i].Discount,
							GeneratePR: $scope.selctedRow[i].GeneratePR
                        });
                    }
                } else {
                    for (var i in $scope.gridAksesoris.data) {
                        for (var j in $scope.selctedRow) {
                            if ($scope.selctedRow[j].PartsId == $scope.gridAksesoris.data[i].AccessoriesId) {
                                $scope.selctedRow.splice(j, 1);
                            }
                        }
                    }
                    for (var i in $scope.selctedRow) {
                        sque++;
                        $scope.gridAksesoris.data.push({
                            ItemNo: sque,
                            MaterialId: $scope.tMaterial.MaterialTypeId,
							MaterialTypeId: angular.copy($scope.ViewData.MaterialTypeId),
                            AccessoriesId: $scope.selctedRow[i].PartsId,
                            AccessoriesCode: $scope.selctedRow[i].PartsCode,
                            AccessoriesName: $scope.selctedRow[i].PartsName,
                            UomName: $scope.selctedRow[i].UomName,
                            RetailPrice: $scope.selctedRow[i].RetailPrice,
                            Qty: $scope.selctedRow[i].Qty,
                            Discount: $scope.selctedRow[i].Discount,
							GeneratePR: $scope.selctedRow[i].GeneratePR
                        });
                    }
                }
            } else {
				var sque = 0;
                if ($scope.gridAksesoris.data.length == 0) {
                    for (var i in $scope.KaroseriRow) {
                        sque++;
                        $scope.gridAksesoris.data.push({
                            ItemNo: sque,
                            MaterialId: $scope.tMaterial.MaterialTypeId,
							MaterialTypeId: angular.copy($scope.ViewData.MaterialTypeId),
                            AccessoriesId: $scope.KaroseriRow[i].KaroseriId,
                            AccessoriesCode: $scope.KaroseriRow[i].KaroseriCode,
                            AccessoriesName: $scope.KaroseriRow[i].KaroseriName,
                            RetailPrice: $scope.KaroseriRow[i].Price,
                            Qty: $scope.KaroseriRow[i].Qty,
                            Discount: 0,
							GeneratePR: $scope.KaroseriRow[i].GeneratePR
                        });
                    }
                } else {
                    for (var i in $scope.gridAksesoris.data) {
                        for (var j in $scope.KaroseriRow) {
                            if ($scope.KaroseriRow[j].KaroseriId == $scope.gridAksesoris.data[i].AccessoriesId) {
                                $scope.KaroseriRow.splice(j, 1);
                            }
                        }
                    }
                    for (var i in $scope.KaroseriRow) {
                        sque++;
                        $scope.gridAksesoris.data.push({
                            ItemNo: sque,
                            MaterialId: $scope.tMaterial.MaterialTypeId,
							MaterialTypeId: angular.copy($scope.ViewData.MaterialTypeId),
                            AccessoriesId: $scope.KaroseriRow[i].KaroseriId,
                            AccessoriesCode: $scope.KaroseriRow[i].KaroseriCode,
                            AccessoriesName: $scope.KaroseriRow[i].KaroseriName,
                            RetailPrice: $scope.KaroseriRow[i].Price,
                            Qty: $scope.KaroseriRow[i].Qty,
                            Discount: 0,
							GeneratePR: $scope.KaroseriRow[i].GeneratePR
                        });
                    }
                }
            }
            $scope.selctedRow = [];
            $scope.KaroseriRow = [];

            //Pricing Engine
            $scope.accesoris = $scope.gridAksesoris.data;
			
            $scope.urlPricing = '/api/fe/PricingEngine?PricingId=30501&OutletId=' + $scope.user.OutletId + '&DisplayId=' + $scope.user.OutletId + '30501';
            $scope.jsData = { Title: null, HeaderInputs: { $TotalDPP$: null, $TotalVAT$: null, $GrandTotal$: 0 } }
                //$scope.jsData.HeaderInputs.$OutletId$ = $scope.user.OutletId;
            $scope.jsData.HeaderInputs.$TotalDPP$ = $scope.TotalDpp;
            $scope.jsData.HeaderInputs.$TotalVAT$ = $scope.PPN;
            $scope.jsData.HeaderInputs.$GrandTotal$ = $scope.GrandTotal;
            $scope.GetDataFromAksesoris();
        }

        $scope.GetDataFromAksesoris = function() {
            $scope.ViewData.PPN = 0;
            $scope.ViewData.TotalDpp = 0;
            $scope.ViewData.GrandTotal = 0;
			$scope.ViewData.TotalVAT = 0;
            
			var DaMaintainSoFreePpnThingy2=0;
			if($scope.ViewData.FreePPNBit==true)
			{
				DaMaintainSoFreePpnThingy2=1;
			}
			else
			{
				DaMaintainSoFreePpnThingy2=0;
            }

            if($scope.generateFrameNoByComboBox == false){ 
                $scope.resultFrame.VehicleTypeId = $scope.ViewData.VehicleTypeId;
            }
            
            $scope.JumlahSementara = 0;
			angular.forEach($scope.accesoris, function(acc, accIdx) {
				var param = {
                    "Title": "Order Aksesoris",
                    "HeaderInputs": {
                        "$OutletId$": $scope.user.OutletId,
                        "$AccessoriesId$": acc.AccessoriesId,
                        "$AccessoriesCode$": acc.AccessoriesCode,
                        "$QTY$": acc.Qty,
                        "$Discount$": acc.Discount,
                        "$MaterialTypeId$": acc.MaterialTypeId,
						"$FreePPNBit$": DaMaintainSoFreePpnThingy2,
						"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
                    }
                };

                $scope.JumlahSementara = 0;
                $http.post('/api/fe/PricingEngine?PricingId=30101&OutletId=' + $scope.user.OutletId + '&DisplayId=' + $scope.user.OutletId + '30101', param)
                    .then(function(res) {
                        $scope.accesoris[accIdx].HargaJual = res.data.Codes["#RetailPrice#"];
                        $scope.accesoris[accIdx].Discount = res.data.Codes["[Discount]"];
                        $scope.accesoris[accIdx].Qty = res.data.Codes["[QTY]"];
                        $scope.accesoris[accIdx].DPP = res.data.Codes["[DPP]"];
                        $scope.accesoris[accIdx].VAT = res.data.Codes["[VAT]"];
                        $scope.accesoris[accIdx].PriceAfterDiscount = res.data.Codes["[PriceAfterDiscount]"];
                        $scope.accesoris[accIdx].PriceAfterTax = res.data.Codes["[PriceAfterTax]"];
                        $scope.accesoris[accIdx].Amount = res.data.Codes["[Amount]"];
                        $scope.accesoris[accIdx].Selisih = res.data.Codes["[Selisih]"];

                        $scope.ViewData.PPN = $scope.ViewData.PPN + parseInt(res.data.Codes["[VAT]"]);
                        console.log('masuk acc sini ga?', $scope.ViewData.TotalDpp);
                        $scope.ViewData.TotalDpp = $scope.ViewData.TotalDpp + parseInt(res.data.Codes["[DPP]"] * parseInt(res.data.Codes["[QTY]"]));
                        $scope.ViewData.TampungTotalDpp = $scope.ViewData.TotalDpp;
                        // $scope.ViewData.TampungTotalDpp = parseInt(res.data.Codes["[QTY]"]) * $scope.ViewData.TotalDpp;
                        // $scope.ViewData.GrandTotal = $scope.ViewData.GrandTotal + parseInt(res.data.Codes["[Amount]"]);
                        // $scope.ViewData.TotalVAT= $scope.ViewData.TotalVAT + parseFloat(res.data.Codes["[VAT]"]);
                        // $scope.ViewData.TampungTotalVAT = parseInt(res.data.Codes["[QTY]"]) * $scope.ViewData.TotalVAT;
                        // Rounding CR4 tambahin if untuk free ppn

                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.TampungTotalVAT = 0 ;
                        }else{
                            // $scope.ViewData.TampungTotalVAT = Math.floor(0.1 * $scope.ViewData.TampungTotalDpp) ; 
                            $scope.ViewData.TampungTotalVAT = Math.floor((PPNPerc/100) * $scope.ViewData.TampungTotalDpp) ; 

                        }
                        // $scope.ViewData.TampungTotalVAT = Math.floor(0.1 * $scope.ViewData.TampungTotalDpp) ;
                        // $scope.ViewData.TampungTotalVAT = 0 ;
                        $scope.ViewData.GrandTotal =  $scope.ViewData.TampungTotalVAT + $scope.ViewData.TampungTotalDpp;
                        $scope.ViewData.TotalVAT= $scope.ViewData.TampungTotalVAT;
                        
                        // $scope.ViewData.Pembulatan = $scope.ViewData.GrandTotal - $scope.ViewData.TampungTotalDpp - $scope.ViewData.TampungTotalVAT;
                        $scope.jumlahHarga = 0;
                        $scope.jumlahHarga = (parseInt(res.data.Codes["[HargaAcc]"] - parseInt(res.data.Codes["[Discount]"])))  * parseInt(res.data.Codes["[QTY]"]);
                        console.log('$scope.jumlahHarga',$scope.jumlahHarga);
                        $scope.JumlahSementara = $scope.JumlahSementara + angular.copy($scope.jumlahHarga);
                         
                        console.log('$scope.JumlahSementara',$scope.JumlahSementara);

                        $scope.ViewData.TampungHarga = $scope.JumlahSementara;
                        console.log('$scope.ViewData.TampungHarga1',$scope.ViewData.TampungHarga);   
                        // $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.Pembulatan = 0;
                        }else{
                            $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        } 
                        
                        
                        console.log('$scope.ViewData',$scope.ViewData);
                    });
            });
        }

        $scope.ListMasterDokumen = function() {
            MaintainSOFactory.getDataDokumen().then(
                function(res) {
                    $scope.gridDataDokumen.data = res.data.Result;
                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.btnTambahDokumen = function() {
            angular.element('.ui.modal.Dokumen').modal('show');
            $scope.ListMasterDokumen();
        }

        $scope.btnPilihDokumen = function() {
            angular.element('.ui.modal.Dokumen').modal('hide');
            var sque = 0;
            if ($scope.gridDokumenPembetulanMaintainSo.length == 0) {
                for (var i in $scope.selctedRow) {
                    sque++;
                    $scope.gridDokumenPembetulanMaintainSo.push({ No: sque, DocumentId: $scope.selctedRow[i].DocumentId, DocumentType: $scope.selctedRow[i].DocumentType, DocumentURL: "", StatusDocumentName: "Empty" });
                }
            } else {
                for (var i in $scope.gridDokumenPembetulanMaintainSo) {
                    for (var j in $scope.selctedRow) {
                        if ($scope.selctedRow[j].DocumentId == $scope.gridDokumenPembetulanMaintainSo[i].DocumentId) {
                            $scope.selctedRow.splice(j, 1);
                        }
                    }
                }
                for (var i in $scope.selctedRow) {
                    sque++;
                    $scope.gridDokumenPembetulanMaintainSo.push({ No: sque, DocumentId: $scope.selctedRow[i].DocumentId, DocumentType: $scope.selctedRow[i].DocumentType, DocumentURL: "", StatusDocumentName: "Empty" });
                }
            }
            $scope.selctedRow = [];
        }
        $scope.equalPemakaiPembeli = function () {
            if ($scope.selected_data.PemakaiPembeliBit) {
                $scope.selected_data.PemakaiPembeliBit = true;
                $scope.selected_data.PemakaiPemilikBit = false;
                if ($scope.selected_data.CustomerTypeId > 3) {
                    $scope.selected_data.PemakaiName = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceName);
                    $scope.selected_data.PemakaiAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].InstanceAddress);
                    // $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[0].PhoneNumber);
                } else {
                    $scope.selected_data.PemakaiHP = angular.copy($scope.selected_data.ListInfoPelanggan[0].HpNumber);
                    $scope.selected_data.PemakaiName = angular.copy($scope.selected_data.ListInfoPelanggan[0].CustomerName);
                    $scope.selected_data.PemakaiAddress = angular.copy($scope.selected_data.ListInfoPelanggan[0].CustomerAddress);
                }
            } else {
                $scope.selected_data.PemakaiPembeliBit = false;
                $scope.selected_data.PemakaiName = null;
                $scope.selected_data.PemakaiAddress = null;
                $scope.selected_data.PemakaiHP = null;
            }
        }

        $scope.changeInfoPelangganMaintainSo = function(selected) {
            $scope.ViewData.CustomerTypeDesc === '';
            selected.CustomerCorrespondentName === "";

            if($scope.flagStatus == "UpdateSO" && $scope.ViewData.SOTypeName == "Finish Unit"){
                if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pembeli") {
                    $scope.infoPembeli = false;
                    $scope.infoPemilik = false;
                    $scope.infoPembeliPerusahaan = false;  $scope.infoPemilikPerusahaan = false;

                    $scope.infoPembeli_individuFinishUnit = true;
                    $scope.infoPemilik_IndividuFinsihUnit = false;
                    $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
                } else if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pemilik"){
                    $scope.infoPembeli = false;
                    $scope.infoPemilik = false;
                    $scope.infoPembeliPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                    $scope.infoPembeli_individuFinishUnit = false;
                    $scope.infoPemilik_IndividuFinsihUnit = true;
                    $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
                } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pembeli"){
                    $scope.infoPembeli = false;
                    $scope.infoPemilik = false;
                    $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                    $scope.infoPembeli_individuFinishUnit = false;
                    $scope.infoPemilik_IndividuFinsihUnit = false;
                    $scope.infoPembeliPerusahaan_FinsihUnit = true; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
                } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Penanggung Jawab"){
                    $scope.infoPembeli = false;
                    $scope.infoPemilik = false;
                    $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                    $scope.infoPembeli_individuFinishUnit = false;
                    $scope.infoPemilik_IndividuFinsihUnit = false;
                    $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = true; $scope.infoPemilikPerusahaan_FinishUnit = false;
                } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pemilik"){
                    $scope.infoPembeli = false;
                    $scope.infoPemilik = false;
                    $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                    $scope.infoPembeli_individuFinishUnit = false;
                    $scope.infoPemilik_IndividuFinsihUnit = false;
                    $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = true;
                }
            }
            // ------- //
            if ($scope.flagStatus == "lihatSO" && $scope.ViewData.SOTypeName == "Finish Unit"){
                if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pembeli") {
                $scope.infoPembeli = true;
                $scope.infoPemilik = false;
                $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
                 } else if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pemilik") {
                $scope.infoPemilik = true;
                $scope.infoPembeli = false;
                $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
                } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pembeli") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;$scope.infoPembeliPerusahaan = true; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
                } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Penanggung Jawab") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;$scope.infoPenanggungJawabPerusahaan = true; $scope.infoPembeliPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
                } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pemilik") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;$scope.infoPemilikPerusahaan = true; $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
                    }
             }
            
        if ($scope.flagStatus != "UpdateSO" && $scope.ViewData.SOTypeName != "Finish Unit"){
            if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pembeli") {
                $scope.infoPembeli = true;
                $scope.infoPemilik = false;
                $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
            } else if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pemilik") {
                $scope.infoPemilik = true;
                $scope.infoPembeli = false;
                $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pembeli") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;$scope.infoPembeliPerusahaan = true; $scope.infoPenanggungJawabPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Penanggung Jawab") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;$scope.infoPenanggungJawabPerusahaan = true; $scope.infoPembeliPerusahaan = false; $scope.infoPemilikPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pemilik") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;$scope.infoPemilikPerusahaan = true; $scope.infoPembeliPerusahaan = false; $scope.infoPenanggungJawabPerusahaan = false;

                $scope.infoPembeli_individuFinishUnit = false;
                $scope.infoPemilik_IndividuFinsihUnit = false;
                $scope.infoPembeliPerusahaan_FinsihUnit = false; $scope.infoPenanggungJawabPerusahaan_FinishUnit = false; $scope.infoPemilikPerusahaan_FinishUnit = false;
            }                 
            } else {
                //kosong();
                console.log("iblis",$scope.ViewData.CustomerTypeDesc);
                console.log("ViewData.ListOfSOCustomerInformationView[0].CustomerCorrespondentId",$scope.ViewData.ListOfSOCustomerInformationView[0].CustomerCorrespondentId)
                console.log("$scope.getPeranPelanggan",$scope.getPeranPelanggan);
                console.log("$scope.ViewData",$scope.ViewData);
                console.log("$scope.ViewData.CustomerTypeDesc",$scope.ViewData.CustomerTypeDesc);
                console.log("$scope.flagStatus",$scope.flagStatus);
                console.log("$scope.ViewData.SOTypeName",$scope.ViewData.SOTypeName);
                if($scope.ViewData.CustomerTypeDesc === 'Individu' && $scope.flagStatus == "UpdateSO" && $scope.ViewData.SOTypeName == "Finish Unit"){
                    if($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerCorrespondentId == 1){

                        for(var c in $scope.kabupaten){
                            if($scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId == $scope.kabupaten[c].CityRegencyId){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId = $scope.kabupaten[c].CityRegencyId;
                            }
                        }
                        for(var c in $scope.kecamatan){
                            if($scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId == $scope.kecamatan[c].DistrictId){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = $scope.kecamatan[c].DistrictId;
                            }
                        }
                        for(var c in $scope.kelurahan){
                            if($scope.ViewData.ListOfSOCustomerInformationView[0].VillageId == $scope.kelurahan[c].VillageId){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = $scope.kelurahan[c].VillageId;
                            }
                        }
                    }else if($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerCorrespondentId == 3){
                        console.log("$scope.kabupaten1",$scope.kabupaten1);
                        for(var c in $scope.kabupaten1){
                            
                            if($scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId == $scope.kabupaten1[c].CityRegencyId){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = $scope.kabupaten1[c].CityRegencyId;
                            }
                        }
                        for(var c in $scope.kecamatan1){
                            if($scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId == $scope.kecamatan1[c].DistrictId){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = $scope.kecamatan1[c].DistrictId;
                            }
                        }
                        for(var c in $scope.kelurahan1){
                            if($scope.ViewData.ListOfSOCustomerInformationView[1].VillageId == $scope.kelurahan1[c].VillageId){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = $scope.kelurahan1[c].VillageId;
                            }
                        }
                    }
                }else {
                    if(($scope.ViewData.CustomerTypeDesc === 'Perusahaan' || $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||$scope.ViewData.CustomerTypeDesc === 'Yayasan') 
                            && $scope.flagStatus == "UpdateSO" && $scope.ViewData.SOTypeName == "Finish Unit"){
                    if($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerCorrespondentId == 1){
                        console.log("masukkkkk 1");
                        for(var c in $scope.kabupaten){
                            if($scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId == $scope.kabupaten[c].CityRegencyId){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId = $scope.kabupaten[c].CityRegencyId;
                            }
                        }
                        for(var c in $scope.kecamatan){
                            if($scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId == $scope.kecamatan[c].DistrictId){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = $scope.kecamatan[c].DistrictId;
                            }
                        }
                        for(var c in $scope.kelurahan){
                            if($scope.ViewData.ListOfSOCustomerInformationView[0].VillageId == $scope.kelurahan[c].VillageId){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = $scope.kelurahan[c].VillageId;
                            }
                        }
                    }else if($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerCorrespondentId == 2){
                        console.log("$scope.province",$scope.province);
                        // for(var a in $scope.province){
                        //     if($scope.province[a].ProvinceId == $scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId){
                        //         console.log("$scope.province[a].ProvinceId",$scope.province[a].ProvinceId);
                        //         $scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId = $scope.province[a].ProvinceId ;
                        //     }
                        // }
                        console.log("masukkkkk 2");
                        console.log("$scope.kabupaten7",$scope.kabupaten7);
                        for(var c in $scope.kabupaten7){
                            if($scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId == $scope.kabupaten7[c].CityRegencyId){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = $scope.kabupaten7[c].CityRegencyId;
                            }
                        }
                        for(var c in $scope.kecamatan7){
                            if($scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId == $scope.kecamatan7[c].DistrictId){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = $scope.kecamatan7[c].DistrictId;
                            }
                        }
                        for(var c in $scope.kelurahan7){
                            if($scope.ViewData.ListOfSOCustomerInformationView[1].VillageId == $scope.kelurahan7[c].VillageId){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = $scope.kelurahan7[c].VillageId;
                            }
                        }
                    }else if($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerCorrespondentId == 3){
                        console.log("masukkkkk 3");
                        for(var c in $scope.kabupaten1){
                            if($scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId == $scope.kabupaten1[c].CityRegencyId){
                                $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId = $scope.kabupaten1[c].CityRegencyId;
                            }
                        }
                        for(var c in $scope.kecamatan1){
                            if($scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId == $scope.kecamatan1[c].DistrictId){
                                $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId = $scope.kecamatan1[c].DistrictId;
                            }
                        }
                        for(var c in $scope.kelurahan1){
                            if($scope.ViewData.ListOfSOCustomerInformationView[2].VillageId == $scope.kelurahan1[c].VillageId){
                                $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = $scope.kelurahan1[c].VillageId;
                            }
                        }
                    }
                }
            }
                
            }
        }
        
        

       //$scope.isCalculateopd = false;

        $scope.btnbuatSO = function() {

            $scope.getSOType = [ { SOTypeId: 2, SOTypeName: "OPD" }, { SOTypeId: 3, SOTypeName: "Purna Jual" },{ SOTypeId: 4, SOTypeName: "Swapping" } ];

            console.log('List SOType ===>', $scope.getSOType);


            
            $scope.ViewData = {};
            $scope.ViewData.OutletId = $scope.user.OutletId;
            $scope.ViewData.isCalculateopd = false;
            $scope.ViewData.isCalculateacc = false;
            $scope.ViewData.SoDate = new Date();
            $scope.gridJasaPengurusanDokumen.data = [];
            $scope.gridAksesoris.data = [];
            $scope.gridDokumenPembetulanMaintainSo = [];
            $scope.MaintainForm = false;
            $scope.CreateSO = true;
            $scope.flagStatus = "InsertSO";
			$scope.breadcrums.title="Tambah";
            //$scope.ViewData.CInvoiceTypeId = $scope.CodePajak[0].CodeInvoiceTransactionTaxId;

            // Show/Disable combo/Text box
            $scope.disableViewPelanggan = false;
            $scope.disableViewJasa = false;
            $scope.disableViewAksesoris = false;
            $scope.disableViewDokumen = false;
            $scope.DisabledRincianHarga = false;
            $scope.DisableSOText = false;
            $scope.ShowNorangka = false;
            $scope.ShowSONumber = false;
            $scope.ShowSalesman = true;
            $scope.ShowProspectName = false;
            $scope.ShowProspectID = false;
            $scope.disabledInfoPengiriman = false;
            $scope.ShowPelangganOPD = true;
            $scope.showDelivery = false;
            $scope.showOfftheRoad = false;
            $scope.showMatching = false;
            $scope.showPromiseDelivery = false;
            $scope.showEstimasiRentang = false;
            $scope.showStatusPDD = false;
            $scope.ShowPeranPelanggan = false;
            $scope.DisabledRincianHarga = false;
            $scope.infoPembeli = false;

            // Show Button
            $scope.btnSimpanCreateSO = true;
            $scope.btnBackToMaintainSO = true;
            $scope.btntambah = true;

            // Show Tabs
            $scope.ShowMenuTabs_FinishUnit = false;
            $scope.ShowtabContainer_FinishUnit = false;
            $scope.ShowMenuTabs = true;
            $scope.ShowtabContainer = true;
            $scope.InfoPelanggan = true;
            $scope.ShowPelangganOPD = true;
            $scope.JasaPengurusanDokumen = true;
            $scope.Aksesoris = true;
            $scope.Dokumen = true;
            $scope.InfoPengiriman = true;
            $scope.RincianHarga = true;
            $scope.karoseriFinishUnit = false;
            $scope.InfoLeasingFinishUnit = false;
            $scope.InfoAsuransiFinishUnit = false;
            $scope.finishUnitInfounityangakandibeli = false;
            $scope.InfoMediatorFinishUnit = false;
            $scope.InfoPemakaiFinishUnit = false;
			
			for(var i = 0; i < $scope.CodePajak.length; ++i)
			{	
				if($scope.CodePajak[i].Name == "01-Penyerahan dalam negeri Oleh PKP")
				{
					$scope.ViewData.CInvoiceTypeId=$scope.CodePajak[i].CodeInvoiceTransactionTaxId;
					$scope.ViewData.FreePPNBit = false;
					break;
				}
			}
			
			ComboBoxFactory.getKodePajakbyId($scope.ViewData.CInvoiceTypeId).then(function(res) {
                    $scope.KodePajak = res.data;
                    if (($scope.KodePajak.Code == '01') || ($scope.KodePajak.Code == '08')) {
                        $scope.MaintainSODisableFreePPN = false;
                        
                        
                        $scope.ViewData.FreePPNBit = false;
                    } else if (($scope.KodePajak.Code != '01') || ($scope.KodePajak.Code != '08')) {
                        $scope.MaintainSODisableFreePPN = false;
                        
                        if ($scope.KodePajak.Code == '07') {
                            
                            $scope.ViewData.FreePPNBit = true;
                        } else if ($scope.KodePajak.Code != '07') {
                            
                            $scope.ViewData.FreePPNBit = false;
                        }
                    }
                });

            
            $scope.ViewData.EngineNoExt = '';
            $scope.ViewData.STNKNameExt = '';
            $scope.ViewData.LicensePlateExt = '';
            $scope.ViewData.ColorUnitExt = '';
            console.log('ViewData',$scope.ViewData);
        }

        $scope.btnCreateSO = function() {

            for(var i in $scope.ViewData.ListOfSOCustomerInformationView){
                // $scope.selected_data.ListInfoPelanggan[i].BirthDate = $scope.changeFormatDate($scope.selected_data.ListInfoPelanggan[i].BirthDate)
                if($scope.ViewData.ListOfSOCustomerInformationView[i].BirthDate != null || $scope.ViewData.ListOfSOCustomerInformationView[i].BirthDate != undefined){
                    $scope.ViewData.ListOfSOCustomerInformationView[i].BirthDate = $scope.changeFormatDate($scope.ViewData.ListOfSOCustomerInformationView[i].BirthDate) 
                }
           
            }

            if($scope.ViewData.SOTypeId == 3 && $scope.gridAksesoris.data.length >=1 && $scope.ViewData.isCalculateacc == false ){
                bsNotify.show({
                            title: "Peringatan",
                            content: "Harap kalkulasi terlebih dahulu.",
                            type: 'warning'
                        });
				
            }else if($scope.ViewData.SOTypeId == 3 && $scope.gridAksesoris.data.length == 0 && $scope.ViewData.isCalculateacc == false ){
                bsNotify.show({
                            title: "Peringatan",
                            content: "Harap pilih aksesoris yang akan dibuat SO.",
                            type: 'warning'
                        });
				
            }else if($scope.ViewData.SOTypeId == 2 && $scope.gridJasaPengurusanDokumen.data.length >=1 && $scope.ViewData.isCalculateopd == false ){
                
				bsNotify.show({
                            title: "Peringatan",
                            content: "Harap kalkulasi terlebih dahulu.",
                            type: 'warning'
                        });
            }else if($scope.ViewData.SOTypeId == 2 && $scope.gridJasaPengurusanDokumen.data.length == 0 && $scope.ViewData.isCalculateopd == false ){
                bsNotify.show({
                            title: "Peringatan",
                            content: "Harap pilih dokumen yang akan dibuat SO.",
                            type: 'warning'
                        });
				
            }else{
                $scope.ViewData.ListOfSODocumentView = $scope.gridDokumenPembetulanMaintainSo;
                $scope.ViewData.ListOfSODocumentCareDetailService = $scope.gridJasaPengurusanDokumen.data;
                $scope.ViewData.ListOfSODetailAccesoriesView = $scope.gridAksesoris.data;
				var ulala=angular.copy($scope.ViewData.SentUnitDate);
				if(ulala==null||(typeof ulala=="undefined"))
				{
					$scope.Paksa_Format_Tanggal_Pengiriman=null;
				}
				else
				{
					try
					{
						$scope.Paksa_Format_Tanggal_Pengiriman=""+ulala.getDate()+"/"+(ulala.getMonth()+1)+"/"+ulala.getFullYear()+"";
					}
					catch(ExceptionComment)
					{
						$scope.Paksa_Format_Tanggal_Pengiriman=null;
					}
				}

				
				
                if ($scope.ViewData.SOTypeId == 1) {
                    //$scope.simpanSO();
					if($scope.flagStatus == "InsertSO")
					{
						bsNotify.show(
							{
								title: "Peringatan",
								content: "Untuk SO Finish Unit tidak bisa dibuat dari sini. Silahkan melalui menu list database",
								type: 'warning'
							}
						);
					}
					else if($scope.flagStatus == "UpdateSO")
					{
						$scope.simpanSO();
					}
					
                } else if ($scope.ViewData.SOTypeId == 2) {
                    $scope.showSum = "typeSODoc";
                    $scope.ShowSUMBiroJasa = true;
                    $scope.ShowSUMAksesoris = false;
                    $scope.SummarySO = true;
                    $scope.CreateSO = false;
                } else if ($scope.ViewData.SOTypeId == 3) {
                    $scope.showSum = "typeSOPur";
                    $scope.ShowSUMBiroJasa = false;
                    $scope.ShowSUMAksesoris = true;
                    $scope.SummarySO = true;
                    $scope.CreateSO = false;
                }
            }                  
        }

        function setDefaultTab() {
            angular.element("#InfoPelangganSO").removeClass("active").addClass("");
            angular.element("#JasaPengurusanDokumenSO").removeClass("active").addClass("");
            angular.element("#DokumenSO").removeClass("active").addClass("");
            angular.element("#AksesorisSO").removeClass("active").addClass("");
            angular.element("#karoseriFinishUnitSO").removeClass("active").addClass("");
            angular.element("#InfoPengirimanSO").removeClass("active").addClass("");
            angular.element("#InfoLeasingFinishUnitSO").removeClass("active").addClass("");
            angular.element("#InfoAsuransiFinishUnitSO").removeClass("active").addClass("");
            angular.element("#finishUnitInfounityangakandibeliSO").removeClass("active").addClass("");
            angular.element("#RincianHargaSO").removeClass("active").addClass("");
            angular.element("#InfoMediatorFinishUnitSO").removeClass("active").addClass("");
            angular.element("#InfoPemakaiFinishUnitSO").removeClass("active").addClass("");
			angular.element("#InfoPelangganSO").addClass("active");
            
            angular.element("#menuInfoPelanggan").removeClass("tab-pane fade in active").addClass("tab-pane fade");
			angular.element("#menuJasaPengurusanDokumen").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuAksesoris").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuDokumen").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuInfoPengiriman").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuRincianHarga").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menukaroseri").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuInsurance").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuinfoLeasing").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuMedia").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoPemakai").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuInfounityangakandibeli").removeClass("tab-pane fade in active").addClass("tab-pane fade");
			angular.element("#menuInfoPelanggan").removeClass("tab-pane fade").addClass("tab-pane fade in active");

        }

        function setDefaultTab_FinishUnit() {
            angular.element("#InfoPelangganSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#JasaPengurusanDokumenSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#DokumenSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#AksesorisSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#karoseriFinishUnitSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#InfoPengirimanSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#InfoLeasingFinishUnitSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#InfoAsuransiFinishUnitSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#finishUnitInfounityangakandibeliSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#RincianHargaSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#InfoMediatorFinishUnitSO_FinishUnit").removeClass("active").addClass("");
            angular.element("#InfoPemakaiFinishUnitSO_FinishUnit").removeClass("active").addClass("");
			angular.element("#InfoPelangganSO_FinishUnit").addClass("active");
            
            angular.element("#menuInfoPelanggan_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
			angular.element("#menuJasaPengurusanDokumen_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuAksesoris_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuDokumen_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuInfoPengiriman_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuRincianHarga_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menukaroseri_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuInsurance_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuinfoLeasing_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuMedia_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#infoPemakai_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            angular.element("#menuInfounityangakandibeli_FinishUnit").removeClass("tab-pane fade in active").addClass("tab-pane fade");
			angular.element("#menuInfoPelanggan_FinishUnit").removeClass("tab-pane fade").addClass("tab-pane fade in active");

        }

        $scope.btnKembaliSOSummary = function() {
            $scope.SummarySO = false;
            $scope.CreateSO = true;
        }
		
		

        $scope.simpanSO = function() {
            $scope.disabledSimpanSO = true;
            $scope.kirimData = {};
            $scope.kirimData.SOId = '';
            $scope.kirimData.SpkId = '';
            $scope.kirimData.OutletId = '';
            $scope.kirimData.NominalDP = '';
            $scope.ViewData.BankAccountNumber=$scope.ViewData.AccountNo;
			angular.forEach($scope.ViewData.ListOfSODocumentView, function(Doc, DocIndx) {
                if (Doc.DocumentData != null) {
                    Doc.DocumentData.UpDocObj = btoa(Doc.DocumentData.UpDocObj);
                } else {
                    console.log("UpDocObj => Kosong");
                }
            });

            $scope.ViewData.ListOfSODocumentCareDetailService = $scope.gridJasaPengurusanDokumen.data;
            $scope.ViewData.ListOfSODocumentView = $scope.gridDokumenPembetulanMaintainSo;
            $scope.ViewData.ListOfSODetailAccesoriesView =angular.copy($scope.gridAksesoris.data);
			
			for(var i = 0; i < $scope.ViewData.ListOfSODocumentCareDetailService.length; ++i)
			{	
				$scope.ViewData.ListOfSODocumentCareDetailService[i].Amount=angular.copy($scope.ViewData.ListOfSODocumentCareDetailService[i]['Total']);
				delete $scope.ViewData.ListOfSODocumentCareDetailService[i].Total;
			}
			
			// for(var i = 0; i < $scope.ViewData.ListOfSODetailAccesoriesView.length; ++i)
			// {	
			// 	$scope.ViewData.ListOfSODetailAccesoriesView[i].MaterialTypeId=$scope.ViewData.MaterialTypeId;
			// }
			

			// if($scope.ViewData.SOTypeId==3)
			// {
				// $scope.ViewData.ListOfSODetailAccesoriesView.Qty=parseFloat($scope.ViewData.ListOfSODetailAccesoriesView.Qty);
				// $scope.ViewData.ListOfSODetailAccesoriesView.HargaJual=parseFloat($scope.ViewData.ListOfSODetailAccesoriesView.HargaJual);
				// $scope.ViewData.ListOfSODetailAccesoriesView.Discount=parseFloat($scope.ViewData.ListOfSODetailAccesoriesView.Discount);
				// $scope.ViewData.ListOfSODetailAccesoriesView.DPP=parseFloat($scope.ViewData.ListOfSODetailAccesoriesView.DPP);
				// $scope.ViewData.ListOfSODetailAccesoriesView.PPN=parseFloat($scope.ViewData.ListOfSODetailAccesoriesView.PPN);
				// $scope.ViewData.ListOfSODetailAccesoriesView.PriceAfterDiscount=parseFloat($scope.ViewData.ListOfSODetailAccesoriesView.PriceAfterDiscount);
				// $scope.ViewData.ListOfSODetailAccesoriesView.SubTotalAcc=parseFloat($scope.ViewData.ListOfSODetailAccesoriesView.SubTotalAcc);
				// $scope.ViewData.ListOfSODetailAccesoriesView.TotalHargaAcc=parseFloat($scope.ViewData.ListOfSODetailAccesoriesView.TotalHargaAcc);
			// }
			$scope.ViewData.TotalDPP=$scope.ViewData.TotalDpp;
            if ($scope.flagStatus == "UpdateSO") {
                if($scope.ViewData.DownPayment > $scope.ViewData.GrandTotal){
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai DP/TDP tidak boleh lebih besar dari Total Harga",
                        type: 'warning'
                    });
                }else if($scope.ViewData.DownPayment < $scope.ViewData.BookingFee){
                    bsNotify.show({
                        title: "Error Message",
                        content: "TDP Lebih Kecil Dari BookingFee",
                        type: 'danger'
                    });
                }else if($scope.ViewData.DownPayment < $scope.ViewData.tempDP){
                    // $scope.kirimData = {};
                    $scope.kirimData.SOId = angular.copy($scope.ViewData.SOId);
                    $scope.kirimData.SpkId = angular.copy($scope.ViewData.SpkId);
                    $scope.kirimData.OutletId = angular.copy($scope.ViewData.OutletId);
                    $scope.kirimData.NominalDP = angular.copy($scope.ViewData.DownPayment);
                    $scope.kirimData.GrandTotal = angular.copy($scope.ViewData.GrandTotal);
                    $scope.kirimData.BookingFee = angular.copy($scope.ViewData.BookingFee);
                    MaintainSOFactory.getDownPaymentAfterEdit($scope.kirimData).then(
                                function(res) {
                                    $scope.getDownPaymentAfterEdit = res.data;
                                    console.log('$scope.getDownPaymentAfterEdit',$scope.getDownPaymentAfterEdit);
                                    // return res.data;
                                    if($scope.getDownPaymentAfterEdit.Valid == false){
                                            bsNotify.show({
                                                title: "Peringatan",
                                                content: "Edit DP tidak dapat dilanjutkan, silahkan periksa Incoming DP dan Kuitansi Penagihan DP",
                                                type: 'warning'
                                            });
                                        }else if($scope.getDownPaymentAfterEdit.Valid == true){
                                            console.log("Valid true --->",$scope.getDownPaymentAfterEdit);
                                            MaintainSOFactory.update($scope.ViewData).then(
                                                function(res) {
                                                    
                                                    MaintainSOFactory.getData($scope.filter).then(
                                                        function(res) {
                                                            $scope.grid.data = res.data.Result;
                                                            for (var j in $scope.grid.data) {
                                                                if ($scope.grid.data[j].ProspectName == null) {
                                                                    $scope.grid.data[j].ProspectName = $scope.grid.data[j].Name;
                                                                }
                                                            }
                                                            $scope.loading = false;
                                                            
                                                            if($scope.filter.SOStatus==null)
                                                            {
                                                                $scope.filter.SOStatus=$scope.getSOstatus[0].SOStatusId;
                                                            }
                                                            bsNotify.show({
                                                                title: "Sukses",
                                                                content: "Data berhasil diubah",
                                                                type: 'success'
                                                            });
                                                            $scope.disabledSimpanSO = false;
                                                            $scope.MaintainForm = true;
                                                            $scope.CreateSO = false;
                                                            $scope.SummarySO = false;
                                                        },
                                                        function(err) {
                                                            console.log("err=>", err);
                                                        }
                                                    );
                                                    
                                                    
                                                },
                                                function(err) {
                                                    bsNotify.show({
                                                                title: "Gagal",
                                                                content: "Gagal Ubah Data",
                                                                type: 'danger'
                                                            });
                                                            $scope.disabledSimpanSO = false;
                                                }
                                            );
                                            
                                        }
                                }
                            );
                                   
                }else {
                    MaintainSOFactory.update($scope.ViewData).then(
                        function(res) {
                            
                            MaintainSOFactory.getData($scope.filter).then(
                                function(res) {
                                    $scope.grid.data = res.data.Result;
                                    for (var j in $scope.grid.data) {
                                        if ($scope.grid.data[j].ProspectName == null) {
                                            $scope.grid.data[j].ProspectName = $scope.grid.data[j].Name;
                                        }
                                    }
                                    $scope.loading = false;
                                    
                                    if($scope.filter.SOStatus==null)
                                    {
                                        $scope.filter.SOStatus=$scope.getSOstatus[0].SOStatusId;
                                    }
                                    bsNotify.show({
                                        title: "Sukses",
                                        content: "Data berhasil diubah",
                                        type: 'success'
                                    });
                                    $scope.disabledSimpanSO = false;
                                    $scope.MaintainForm = true;
                                    $scope.CreateSO = false;
                                    $scope.SummarySO = false;
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                            
                            
                        },
                        function(err) {
                            bsNotify.show({
                                        title: "Gagal",
                                        content: "Gagal Ubah Data",
                                        type: 'danger'
                                    });
                                    $scope.disabledSimpanSO = false;
                        }
                    );
                }
                
            } else if ($scope.flagStatus == "InsertSO") {
                MaintainSOFactory.createSO($scope.ViewData).then(
                    function(res) {
                        MaintainSOFactory.getData($scope.filter).then(
							function(res) {
								$scope.grid.data = res.data.Result;
								for (var j in $scope.grid.data) {
									if ($scope.grid.data[j].ProspectName == null) {
										$scope.grid.data[j].ProspectName = $scope.grid.data[j].Name;
									}
								}
								$scope.loading = false;
								
								if($scope.filter.SOStatus==null)
								{
									$scope.filter.SOStatus=$scope.getSOstatus[0].SOStatusId;
								}
								
								
								bsNotify.show({
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
                                });
                                $scope.disabledSimpanSO = false;
								$scope.MaintainForm = true;
								$scope.CreateSO = false;
								$scope.SummarySO = false;
							},
							function(err) {
								console.log("err=>", err);
							}
						);
						
						
                    },
                    function(err) {
                        
						bsNotify.show({
									title: "Gagal",
									content: "Gagal Simpan Data",
									type: 'danger'
                                });
                                $scope.disabledSimpanSO = false;
                    }
                );
            } else {
            }
        }

        $scope.NoSOCode = {};

        // $rootScope.$on("LihatSOdariBilling", function() {
            // $scope.KaloLihatSOdariBilling=true;
			// $scope.SOId = $rootScope.SOId;
            // $scope.lihatSO($scope.SOId);
        // });

        $scope.dataDP = function(){
            console.log("$scope.ViewData",$scope.ViewData);
            if($scope.ViewData.DownPayment == null || $scope.ViewData.DownPayment == undefined || $scope.ViewData.DownPayment == '' ){
                $scope.ViewData.DownPayment = 0;
            }
        }
        
        $scope.lihatSO = function(SOId) {
            MaintainSOFactory.getSODetail(SOId).then(function(res) {
                $scope.NoSOCode = res.data.Result[0];
				$scope.breadcrums.title="Lihat";
                $scope.lihatDetailSO();
            })
        }

        $scope.pilihWarna = function(selected) {
            //$scope.ViewData.ColorId = selected.ColorId;
            $scope.ViewData.ColorName = selected.ColorName;
        }

        // $scope.pilihNamaLeasing = function(selected) {
        //     //$scope.ViewData.ColorId = selected.ColorId;
        //     $scope.ViewData.ColorName = selected.ColorName;
        // }

		
		$scope.MaintainSoTanggalPengirimanNoSwap = function() {
			if($scope.flagStatus == "InsertSO")
			{
				if($scope.ViewData.SentUnitDate<=new Date())
				{
					$scope.ViewData.SentUnitDate=new Date();
				}
				
			}
		}

        $scope.pilihBank = function(selected) {
            $scope.bankTemp = selected;
            if ($scope.flagStatus == "InsertSO") {
                if ($scope.ViewData.BankId != null || $scope.ViewData.BankId !== undefined) {
                    var Bank = "?BankId=" + $scope.bankTemp.BankId;
                    MaintainBillingFactory.getNoRek(Bank).then(
                        function(res) {
                            $scope.selectRek = res.data.Result;
                            $scope.ViewData.AccountNo = $scope.selectRek[0].AccountNo;
                            return res.data;
                        }
                    );
                }

            } else {
                if ($scope.ViewData.BankId != null || $scope.ViewData.BankId !== undefined) {
                    var Bank = "?BankId=" + $scope.ViewData.BankId;
                    MaintainBillingFactory.getNoRek(Bank).then(
                        function(res) {
                            $scope.selectRek = res.data.Result;
                            $scope.ViewData.AccountNo = $scope.selectRek[0].AccountNo;
                            return res.data;
                        }
                    );
                }

            }
        }

        $scope.lihatDetailSO = function() {
            $scope.ViewData = $scope.NoSOCode;
            if ($scope.ViewData.StatusPDDName == null || $scope.ViewData.StatusPDDName == "") {
                $scope.ViewData.StatusPDDName = "Belum dibuat";
            }

            if ($scope.ViewData.ManualMatchingBit == true) {
                $scope.ViewData.ManualMatchingBit = "Manual";
            } else {
                $scope.ViewData.ManualMatchingBit = "Automatic";
            }
            WarnaFactory.getData("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.ViewData.VehicleTypeId).then(
                function(res) {
                    $scope.listWarna = res.data.Result;
                })

            $scope.flagStatus = "lihatSO";
            $scope.CreateSO = true;
            $scope.ShowMenuTabs = true;
            $scope.ShowMenuTabs_FinishUnit = false;
            $scope.btnBackToMaintainSO = true;
            $scope.btnSimpanCreateSO = false;
            $scope.DisableSOText = true;
            $scope.MaintainForm = false;
            $scope.disableViewPelanggan = true;
            $scope.disableViewJasa = true;
            $scope.disableViewAksesoris = true;
            $scope.disableViewDokumen = true;
            $scope.DisabledRincianHarga = true;
            $scope.disabledInfoPengiriman = true;
            $scope.getPeranPelanggan = [];
            $scope.deleteRows = true;
             // ISI DARI TAB INFO PENGIRIMAN //
             $scope.disabledNamaPenerimaNew = false;
             $scope.disabledAlamatNamaPenerimaNew = false;
             $scope.disabledHandphonePenerimaNew = false;
             $scope.disabledDeliveryNew = false;
             $scope.disabledOnOffRoadNew = false;
             $scope.disabledMatchingNew = false;
             $scope.disabledPromiseDeliveryNew = false;
             $scope.disabledEstimasiWaktuaNew = false;
             $scope.disabledTanggalPengirimanNew = false;
             $scope.disabledStatusPDDNew = false;
             //-----------------------//
            for (var i in $scope.NoSOCode.ListOfSOCustomerInformationView) {
                $scope.getPeranPelanggan.push({
                    CustomerCorrespondentId: $scope.NoSOCode.ListOfSOCustomerInformationView[i].CustomerCorrespondentId,
                    CustomerCorrespondentName: $scope.NoSOCode.ListOfSOCustomerInformationView[i].CustomerCorrespondentName
                });
            }
            $scope.gridJasaPengurusanDokumen.data = $scope.NoSOCode.ListOfSODocumentCareDetailService;
            
			$scope.MaintainSoLihatSoShowPaketAksesoris=false;
			if($scope.ViewData.SOTypeName=="Finish Unit")
			{
				for(var i = 0; i < $scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessories.length; ++i)
				{
					$scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessories[i].RetailPrice=$scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessories[i].Price;
				}
				$scope.gridAksesoris.data = angular.copy($scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessories);
				$scope.MaintainSoLihatSoShowPaketAksesoris=true;
				$scope.gridAksesorisPackageMaintainSo.data = angular.copy($scope.NoSOCode.ListInfoUnit[0].ListDetailUnit[0].ListAccessoriesPackage);
			}
			else
			{
				$scope.gridAksesoris.data = $scope.NoSOCode.ListOfSODetailAccesoriesView;
			}
			
            
			$scope.gridDokumenPembetulanMaintainSo = $scope.NoSOCode.ListOfSODocumentView;
            //$scope.ViewDataDelivery = $scope.NoSOCode.ListOfSODeliveryInfoView;
            // $scope.ViewDataInfoUnit = $scope.NoSOCode.ListOfSOUnitInfoView;
            $scope.ViewDataAsuransi = $scope.NoSOCode.ListOfSOInsuranceExtView;
            if ($scope.ViewData.SOTypeName == "Finish Unit") {
                // ISI DARI TAB INFO PENGIRIMAN //
                $scope.disabledNamaPenerimaNew = false;
                $scope.disabledAlamatNamaPenerimaNew = false;
                $scope.disabledHandphonePenerimaNew = false;
                $scope.disabledDeliveryNew = false;
                $scope.disabledOnOffRoadNew = false;
                $scope.disabledMatchingNew = false;
                $scope.disabledPromiseDeliveryNew = false;
                $scope.disabledEstimasiWaktuaNew = false;
                $scope.disabledTanggalPengirimanNew = false;
                $scope.disabledStatusPDDNew = false;
                //-----------------------//
                $scope.ShowtabContainer_FinishUnit = false;
                $scope.ShowtabContainer = true;
                $scope.ShowSONumber = true;
                $scope.ShowProspectID = true;
                $scope.ShowProspectName = true;
                $scope.ShowSalesman = false;
                $scope.InfoPelanggan = true;
                $scope.ShowPeranPelanggan = true;
                $scope.disableTextInfoPelanggan = true;
                $scope.ShowPelangganOPD = false;
                $scope.ShowPelangganFinishUnit = true;
                $scope.JasaPengurusanDokumen = false;
                $scope.Dokumen = true;
                $scope.btntambah = false;
                $scope.InfoPengiriman = true;
                $scope.showDelivery = true;
                $scope.showOfftheRoad = true;
                $scope.showMatching = true;
                $scope.showPromiseDelivery = true;
                $scope.showEstimasiRentang = true;
                $scope.showStatusPDD = true;
                $scope.RincianHarga = true;
                $scope.Aksesoris = true;
                $scope.karoseriFinishUnit = true;
                $scope.InfoLeasingFinishUnit = true;
                $scope.disabledinfoLeasing = true;
                $scope.InfoAsuransiFinishUnit = true;
                $scope.finishUnitInfounityangakandibeli = true;
                $scope.finishUnitWarna = true;
                $scope.InfoMediatorFinishUnit = true;
                $scope.InfoPemakaiFinishUnit = true;
                $scope.showAlasanBatalSO = true;
                $scope.infoPembeli = true;
				setDefaultTab();
            } else {
                if ($scope.ViewData.SOTypeName == "OPD") {
                    // ISI DARI TAB INFO PENGIRIMAN //
                    $scope.disabledNamaPenerimaNew = false;
                    $scope.disabledAlamatNamaPenerimaNew = false;
                    $scope.disabledHandphonePenerimaNew = false;
                    $scope.disabledDeliveryNew = false;
                    $scope.disabledOnOffRoadNew = false;
                    $scope.disabledMatchingNew = false;
                    $scope.disabledPromiseDeliveryNew = false;
                    $scope.disabledEstimasiWaktuaNew = false;
                    $scope.disabledTanggalPengirimanNew = false;
                    $scope.disabledStatusPDDNew = false;
                    //-----------------------//
                    $scope.ShowMenuTabs_FinishUnit = false;
                    $scope.ShowtabContainer_FinishUnit = false;
                    $scope.ShowtabContainer = true;
                    $scope.ShowSONumber = true;
                    $scope.ShowSalesman = true;
                    $scope.ShowProspectID = false;
                    $scope.ShowProspectName = false;
                    $scope.ShowPeranPelanggan = false;
                    $scope.disableTextInfoPelanggan = true;
                    $scope.InfoPelanggan = true;
                    $scope.ShowPelangganOPD = true;
                    $scope.ShowPelangganFinishUnit = false;
                    $scope.JasaPengurusanDokumen = true;
                    $scope.Dokumen = true;
                    $scope.btntambah = false;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.DisabledTotalRincianHarga = true;
                    $scope.Aksesoris = false;
                    $scope.menuRincianHarga = true;
                    $scope.InfoPelangganFinishUnit = false;
                    $scope.karoseriFinishUnit = false;
                    $scope.InfoLeasingFinishUnit = false;
                    $scope.InfoAsuransiFinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli = false;
                    $scope.finishUnitWarna = true;
                    $scope.InfoMediatorFinishUnit = false;
                    $scope.InfoPemakaiFinishUnit = false;
                    $scope.infoPembeli = false;
					setDefaultTab();
                } else if ($scope.ViewData.SOTypeName == "Purna Jual") {
                    // ISI DARI TAB INFO PENGIRIMAN //
                    $scope.disabledNamaPenerimaNew = false;
                    $scope.disabledAlamatNamaPenerimaNew = false;
                    $scope.disabledHandphonePenerimaNew = false;
                    $scope.disabledDeliveryNew = false;
                    $scope.disabledOnOffRoadNew = false;
                    $scope.disabledMatchingNew = false;
                    $scope.disabledPromiseDeliveryNew = false;
                    $scope.disabledEstimasiWaktuaNew = false;
                    $scope.disabledTanggalPengirimanNew = false;
                    $scope.disabledStatusPDDNew = false;
                    //-----------------------//
                    $scope.ShowtabContainer_FinishUnit = false;
                    $scope.ShowtabContainer = true;
                    $scope.ShowSONumber = true;
                    $scope.ShowProspectID = false;
                    $scope.ShowProspectName = false;
                    $scope.ShowPeranPelanggan = false;
                    $scope.disableTextInfoPelanggan = true;
                    $scope.InfoPelanggan = true;
                    $scope.ShowPelangganOPD = true;
                    $scope.ShowPelangganFinishUnit = false;
                    $scope.JasaPengurusanDokumen = false;
                    $scope.Dokumen = false;
                    $scope.btntambah = false;
                    $scope.Aksesoris = true;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.DisabledTotalRincianHarga = true;
                    $scope.InfoPelangganFinishUnit = false;
                    $scope.karoseriFinishUnit = false;
                    $scope.InfoLeasingFinishUnit = false;
                    $scope.InfoAsuransiFinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli = false;
                    $scope.finishUnitWarna = true;
                    $scope.InfoMediatorFinishUnit = false;
                    $scope.InfoPemakaiFinishUnit = false;
                    $scope.infoPembeli = false;
					setDefaultTab();
                } else if ($scope.ViewData.SOTypeName == "Swapping") {
                    // ISI DARI TAB INFO PENGIRIMAN //
                    $scope.disabledNamaPenerimaNew = false;
                    $scope.disabledAlamatNamaPenerimaNew = false;
                    $scope.disabledHandphonePenerimaNew = false;
                    $scope.disabledDeliveryNew = false;
                    $scope.disabledOnOffRoadNew = false;
                    $scope.disabledMatchingNew = false;
                    $scope.disabledPromiseDeliveryNew = false;
                    $scope.disabledEstimasiWaktuaNew = false;
                    $scope.disabledTanggalPengirimanNew = false;
                    $scope.disabledStatusPDDNew = false;
                    //-----------------------//
                    $scope.ShowtabContainer_FinishUnit = false;
                    $scope.InfoPelanggan = false;
                    $scope.Aksesoris = false;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.JasaPengurusanDokumen = false;
                    $scope.Dokumen = false;
                    $scope.finishUnitInfounityangakandibeli = true;
                    $scope.finishUnitWarna = true;
                    $scope.infoPembeli = false;
                    $scope.showAlasanBatalSO = true;

                    angular.element("#InfoPelangganSO").removeClass("active").addClass("");
                    angular.element("#JasaPengurusanDokumenSO").removeClass("active").addClass("");
                    angular.element("#DokumenSO").removeClass("active").addClass("");
                    angular.element("#AksesorisSO").removeClass("active").addClass("");
                    angular.element("#karoseriFinishUnitSO").removeClass("active").addClass("");
                    angular.element("#InfoPengirimanSO").addClass("active");
                    angular.element("#InfoLeasingFinishUnitSO").removeClass("active").addClass("");
                    angular.element("#InfoAsuransiFinishUnitSO").removeClass("active").addClass("");
                    angular.element("#finishUnitInfounityangakandibeliSO").removeClass("active").addClass("");
                    angular.element("#RincianHargaSO").removeClass("active").addClass("");
                    angular.element("#InfoMediatorFinishUnitSO").removeClass("active").addClass("");
                    angular.element("#InfoPemakaiFinishUnitSO").removeClass("active").addClass("");

                    angular.element("#menuInfoPelanggan").removeClass("tab-pane fade active").addClass("tab-pane fade");
                    angular.element("#menuJasaPengurusanDokumen").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuAksesoris").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuDokumen").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfoPengiriman").removeClass("tab-pane fade").addClass("tab-pane fade in active");
                    angular.element("#menuRincianHarga").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menukaroseri").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInsurance").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuinfoLeasing").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuMedia").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#infoPemakai").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                    angular.element("#menuInfounityangakandibeli").removeClass("tab-pane fade in active").addClass("tab-pane fade");
                }
            }
			$scope.changeInfoPelangganMaintainSo($scope.ViewData.ListOfSOCustomerInformationView[0]);
            // $scope.changeInfoPelangganMaintainSo_EditFinsihUnit($scope.ViewData.ListOfSOCustomerInformationView[0]);
            $scope.pilihBank();
			
			// for(var x = 0; x < $scope.DataNorangka.length; x++)
			// {	
				// if($scope.DataNorangka[i].FrameNo==$scope.ViewData.FrameNo)
				// {
					// $scope.ViewData.VehicleId=$scope.DataNorangka[i].VehicleId;
					// break;
				// }
			// }
        }

        $scope.btnUbahSO = function(selected) {
            // if ($scope.ViewData.SOTypeName == 'Finish Unit' && $scope.flagStatus == 'UpdateSO'){
            //     if($scope.ViewData.CustomerTypeId!='3' 
                
            //     ){

            //     }else if ($scope.ViewData.CustomerTypeId!='3' && (
            //         $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerName == null || $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerName == '' || $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerName == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[1].Email == null || $scope.ViewData.ListOfSOCustomerInformationView[1].Email == '' || $scope.ViewData.ListOfSOCustomerInformationView[1].Email == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[1].HpNumber == null || $scope.ViewData.ListOfSOCustomerInformationView[1].HpNumber == '' || $scope.ViewData.ListOfSOCustomerInformationView[1].HpNumber == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerPositionId == null || $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerPositionId == '' || $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerPositionId == undefined ||
                    
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerName == null || $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerName == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerName == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerAddress == null || $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerAddress == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerAddress == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId == null || $scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId == null || $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId == null || $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId == null || $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].RT == null || $scope.ViewData.ListOfSOCustomerInformationView[2].RT == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].RT == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].RW == null || $scope.ViewData.ListOfSOCustomerInformationView[2].RW == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].RW == undefined ||
            //         $scope.ViewData.ListOfSOCustomerInformationView[2].NPWPNo == null || $scope.ViewData.ListOfSOCustomerInformationView[2].NPWPNo == '' || $scope.ViewData.ListOfSOCustomerInformationView[2].NPWPNo == undefined ||
                    
            //         $scope.ViewData.VehicleTypeColorId == null || $scope.ViewData.VehicleTypeColorId == '' || $scope.ViewData.VehicleTypeColorId == undefined))
            //         {
            //         $scope.disabledBtnCreateSo = true;
            //     }
            // } else {
            
			MaintainSOFactory.getSODetail(selected.SOId).then(function(res) {
                
                $scope.ubahSOCode = res.data.Result[0];
				$scope.ubahSOCode_Lagi = res.data.Result[0];
				$scope.breadcrums.title="Ubah";
                console.log("ubah");
                
                $scope.ubahDetailSO();
            });

            setDefaultTab();

            // angular.element("#InfoPelangganSO").removeClass("active").addClass("");
            // angular.element("#finishUnitInfounityangakandibeliSO").removeClass("").addClass("active");

            // angular.element("#menuInfoPelanggan").removeClass("tab-pane fade in active").addClass("tab-pane fade");
            // angular.element("#menuInfounityangakandibeli").removeClass("tab-pane fade").addClass("tab-pane fade in active");
			
			if(selected.SOTypeName == "Finish Unit")
			{
				// navigateTab("tabContainer","menuInfounityangakandibeli");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuInfoPelanggan_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuJasaPengurusanDokumenSO_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuDokumen_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuAksesoris_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menukaroseri_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuInfoPengiriman_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuinfoLeasing_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuInsurance_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuInfounityangakandibeli_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuRincianHarga_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","menuMedia_FinishUnit");
                navigateTab_FinishUnit("tabContainer_FinishUnit","infoPemakai_FinishUnit");
			}
            // }
			
        }

        //----- TAB PEMBELI INDIVIDU ---- //
        $scope.filterKabupatenPembeliIndividu = function (selected, param) {
            try {
                console.log("selected prov===>",selected);
                // if(selected == null && param !== undefined){
                //     selected = param.ProvinceId;
                // }
                if(selected == null || selected == undefined || selected ==''){
                    if($scope.ViewData.CustomerTypeDesc == 'Individu'){
                        if(param == 1){
                            $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = null;  
                            $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;  
                        }else if(param == 0){
                            $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId = null;  
                            $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }
                    }else if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                                $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                            if(param == 0){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId = null;  
                                $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null; 
                                $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;  
                            }else if(param == 1){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = null;  
                                $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = null; 
                                $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                            }else if(param == 2){
                                $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId = null;  
                                $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId = null; 
                                $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = null; 
                            }
                    }                
                    
                }else{
                ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + selected).then(function (res) { 
                    if($scope.ViewData.CustomerTypeDesc == 'Individu'){
                        
                        if(param == 1){
                        
                            $scope.kabupaten1 = res.data.Result;
                            console.log("kabupaten1",$scope.kabupaten);
                            $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = null;  
                            $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                        }else if(param == 0){
    
                            $scope.kabupaten = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }
                    }


                    if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                            $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                            $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                        
                        if(param == 0){
                        
                            $scope.kabupaten = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId = null;  
                            $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }else if(param == 1){
    
                            $scope.kabupaten7 = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = null;  
                            $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                        }else if(param == 2){
    
                            $scope.kabupaten1 = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId = null;  
                            $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = null;
                        }
                    }
                    // $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId=null; 
                    // $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId=null; 
                    // $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId=null;
                    
                        // $scope.filterKecamatanPembeliIndividu(param.CityRegencyId, param);
                        // $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId= param.CityRegencyId;
                    // }
                });
            }
            } catch (err) {
            }

        }

        $scope.filterKecamatanPembeliIndividu = function (selected, param) {
            console.log("selected",selected);
            console.log("param",param);
            try {
                if(selected == null || selected == undefined || selected ==''){
                    if($scope.ViewData.CustomerTypeDesc == 'Individu'){
                        if(param == 1){
                            $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;  
                        }else if(param == 0){
                            $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }
                    }else if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                                $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                            if(param == 0){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null; 
                                $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;  
                            }else if(param == 1){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = null; 
                                $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                            }else if(param == 2){ 
                                $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId = null; 
                                $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = null; 
                            }
                    }                                  
                }else{
                MaintainSOFactory.getRegionCode(selected, $scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId).then(function (res) { 
                    $scope.regioncode = res.data.Result
                 });
                ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + selected).then(function (res) { 
                    console.log("$scope.ViewData.CustomerTypeDesc",$scope.ViewData.CustomerTypeDesc);
                    if($scope.ViewData.CustomerTypeDesc == 'Individu'){
                        if(param == 1){
                            
                            $scope.kecamatan1 = res.data.Result; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                        }else if(param == 0){
                            console.log("masuk individu 0");
                            
                            $scope.kecamatan = res.data.Result; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }
                    }


                    if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                            $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                            $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                        
                        if(param == 0){
                        
                            $scope.kecamatan = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }else if(param == 1){

                            $scope.kecamatan7 = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                        }else if(param == 2){
    
                            $scope.kecamatan1 = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = null;
                        }
                    }
                    // $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId=null; 
                    // $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId=null;
                    // if(param !== undefined){
                    //     $scope.filterKelurahanPembeliIndividu(param.DistrictId,param);
                    //     $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId=param.DistrictId;
                    // }
                });
            }
            } catch (err) {
            }

        }
        //FILTER UNTUK DIPASANG DI KECAMATAN
        $scope.filterKelurahanPembeliIndividu = function (selected, param) {
            console.log("selected kelurahan",selected);
            try {
                if(selected == null || selected == undefined || selected ==''){
                    if($scope.ViewData.CustomerTypeDesc == 'Individu'){
                        if(param == 1){
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;  
                        }else if(param == 0){ 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }
                    }else if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                                $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                                $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                            if(param == 0){ 
                                $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;  
                            }else if(param == 1){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                            }else if(param == 2){ 
                                $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = null;
                                $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = null; 
                            }
                    }                                  
                }else{
                ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + selected).then(function (res) { 
                    if($scope.ViewData.CustomerTypeDesc == 'Individu'){
                        if(param == 1){
                            
                            $scope.kelurahan1 = res.data.Result; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                        }else if(param == 0){
                            
                            $scope.kelurahan = res.data.Result; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }
                    }

                    if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                            $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                            $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                        
                        if(param == 0){
                        
                            $scope.kelurahan = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                        }else if(param == 1){
    
                            $scope.kelurahan7 = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                        }else if(param == 2){
    
                            $scope.kelurahan1 = res.data.Result;
                            $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = null; 
                            $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = null;
                        }
                    }
                    // $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId=null;
                    // if(param!== undefined){
                    //     $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId=param.VillageId;
                    // }
                 });
                }
            } catch (err) {

            }

        }
        //FILTER UNTUK DIPASANG DI KELURAHAN
        $scope.filterVillagePembeliIndividu = function (selected,param) {
            console.log("selected",selected);
            console.log("param",param);
            // try {
                // ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=VillageId|' + selected).then(function (res) { 
                    if (selected == null || selected == undefined || selected ==''){
                        if($scope.ViewData.CustomerTypeDesc == 'Individu'){
                            if(param == 1){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;  
                            }else if(param == 0){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;
                            }
                        }
                    } else {
                    if($scope.ViewData.CustomerTypeDesc == 'Individu'){
                        
                        if(param == 1){
                            
                            $scope.kodepos1 = [];
                            for(var d in $scope.kelurahan1){
                                if($scope.kelurahan1[d].VillageId == selected){
                                    $scope.kodepos1.push({PostalCode : $scope.kelurahan1[d].PostalCode});
                                    $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = $scope.kodepos1[0].PostalCode;
                                }
                            }
                            console.log("$scope.kodepos1",$scope.kodepos1);
                            
                            console.log("$scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode",$scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode);
                       
                        }else if(param == 0){
                            $scope.kodepos = [];
                            for(var a in $scope.kelurahan){
                                if($scope.kelurahan[a].VillageId == selected){
                                    $scope.kodepos.push({PostalCode : $scope.kelurahan[a].PostalCode});
                                    $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = $scope.kodepos[0].PostalCode;
                                }
                            }
                            
                            
                            console.log("$scope.kodepos",$scope.kodepos);
                            
                            console.log("$scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode",$scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode);
                        }
                    }
                }
                    if (selected == null || selected == undefined || selected ==''){
                        if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                            $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                            $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                            if(param == 0){
                                $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null;  
                            }else if(param == 1){
                                $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null;
                            }else if(param == 2){
                                $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = null;
                            }
                        }
                    } else {
                    if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                            $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
                            $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                        
                        if(param == 0){
                            $scope.kodepos = [];
                            for(var a in $scope.kelurahan){
                                if($scope.kelurahan[a].VillageId == selected){
                                    $scope.kodepos.push({PostalCode : $scope.kelurahan[a].PostalCode});
                                    $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = $scope.kodepos[0].PostalCode;
                                }
                            }                                                    
                            console.log("$scope.kodepos instansi",$scope.kodepos);                           
                            console.log("$scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode",$scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode);
                       
                        }else if(param == 1){
    
                            $scope.kodepos7 = [];
                            for(var d in $scope.kelurahan7){
                                if($scope.kelurahan7[d].VillageId == selected){
                                    $scope.kodepos7.push({PostalCode : $scope.kelurahan7[d].PostalCode});
                                    $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = $scope.kodepos7[0].PostalCode;
                                }
                            }
                            console.log("$scope.kodepos7",$scope.kodepos7);                         
                            console.log("$scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode",$scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode);
                        }else if(param == 2){
    
                            $scope.kodepos1 = [];
                            for(var d in $scope.kelurahan1){
                                if($scope.kelurahan1[d].VillageId == selected){
                                    $scope.kodepos1.push({PostalCode : $scope.kelurahan1[d].PostalCode});
                                    $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = $scope.kodepos1[0].PostalCode;
                                }
                            }
                            console.log("$scope.kodepos1",$scope.kodepos1);
                            
                            console.log("$scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode",$scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode);
                        }
                    }
                }
                // });
            // } catch (err) {

            // }

        }

        //FILTER UNTUK DIPASANG DI KODEPOS
        $scope.filterKodePosPembeliIndividu = function (selected) {
            // console.log("selected kode pos",selected);
            // try {
            //     ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=VillageId|' + selected).then(function (res) { 
            //         if($scope.ViewData.CustomerTypeDesc == 'Individu'){
            //             if(param == 1){
                            
            //                 $scope.kodepos1 = res.data.Result; 
            //                 $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null; 
            //             }else if(param == 0){
                            
            //                 $scope.kodepos = res.data.Result; 
            //                 $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null; 
            //             }
            //         }

            //         if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
            //                 $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
            //                 $scope.ViewData.CustomerTypeDesc === 'Yayasan'){
                        
            //             if(param == 0){
                        
            //                 $scope.kodepos = res.data.Result;
            //                 $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = null; 
            //             }else if(param == 1){
    
            //                 $scope.kodepos7 = res.data.Result;
            //                 $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = null; 
            //             }else if(param == 2){
    
            //                 $scope.kodepos1 = res.data.Result;
            //                 $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = null; 
            //             }
            //         }
            //     });
            // } catch (err) {

            // }

        }

        // function fixDate(date) {
        //     if (date != null || date != undefined) {
        //         var fix = date.getFullYear() + '-' +
        //             ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
        //             ('0' + date.getDate()).slice(-2)
        //         return fix;
        //     } else {
        //         return null;
        //     }
        // };

        $scope.changeFormatDate = function(item) {
            var tmpParam = item;
            tmpParam = new Date(tmpParam);
            var finalDate
            var yyyy = tmpParam.getFullYear().toString();
            var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpParam.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            
            return finalDate;
        }

        $scope.ubahDetailSO = function() {
            $scope.ViewData = $scope.ubahSOCode;
            console.log("$scope.ViewData",$scope.ViewData);
            console.log("ViewData.CustomerTypeId",$scope.ViewData.CustomerTypeId);;
            console.log("ViewData.PromiseDeliveryMonthName",$scope.ViewData.PromiseDeliveryMonthName)
            
            var ProvID0;
            var ProvID1;
            var ProvID2;

            var kec0;
            var kec1;
            var kec2;

            var kot0;
            var kot1;
            var kot2;
            
            var kel0;
            var kel1;
            var kel2;

            $scope.ViewData.CustomerCorrespondentId = 1;

            if($scope.ViewData.CustomerTypeDesc == 'Individu'){

                if($scope.ViewData.SOTypeName=="Finish Unit") {
                    $scope.kodepos = [{
                        PostalCode : $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode
                    }];
    
                    $scope.kodepos1 = [{
                        PostalCode : $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode
                    }];
    
                    console.log("$scope.kodepos",$scope.kodepos);
                    console.log("$scope.kodepos1",$scope.kodepos1);
    
                    $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode;
                    $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode;
    
                    ProvID0 = $scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId;
                    ProvID1 = $scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId;
                    
                    kot0 = $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId;
                    kot1 = $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId;
    
                    kec0 = $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId;
                    kec1 = $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId;
                    
                    ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + ProvID0).then(function (res) { 
                        $scope.kabupaten = res.data.Result;
        
                            for(var c in $scope.kabupaten){
                                if($scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId == $scope.kabupaten[c].CityRegencyId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId = $scope.kabupaten[c].CityRegencyId;
                                }
                                // $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = null;
                                // $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = null;
    
                            }
                       
                        ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + kot0).then(function (res) { 
                            $scope.kecamatan = res.data.Result; 
                            for(var c in $scope.kecamatan){
                                if($scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId == $scope.kecamatan[c].DistrictId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = $scope.kecamatan[c].DistrictId;
                                }
                            }
                            ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + kec0).then(function (res) { 
                                $scope.kelurahan = res.data.Result;
                                // if($scope.ViewData.CustomerTypeDesc == 'Individu' && selected.CustomerCorrespondentName === "Pembeli"){
                                    for(var c in $scope.kelurahan){
                                        if($scope.ViewData.ListOfSOCustomerInformationView[0].VillageId == $scope.kelurahan[c].VillageId){
                                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = $scope.kelurahan[c].VillageId;
                                        }
                                    }
                                // }
                             });
                        });
                        
                    });
        
                    ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + ProvID1).then(function (res) { 
                        $scope.kabupaten1 = res.data.Result;
        
                            for(var c in $scope.kabupaten1){
                                if($scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId == $scope.kabupaten1[c].CityRegencyId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = $scope.kabupaten1[c].CityRegencyId;
                                }
                            }
                       
                        ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + kot1).then(function (res) { 
                            $scope.kecamatan1 = res.data.Result; 
                            for(var c in $scope.kecamatan1){
                                if($scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId == $scope.kecamatan1[c].DistrictId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = $scope.kecamatan1[c].DistrictId;
                                }
                            }
                            ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + kec1).then(function (res) { 
                                $scope.kelurahan1 = res.data.Result;
                                // if($scope.ViewData.CustomerTypeDesc == 'Individu' && selected.CustomerCorrespondentName === "Pembeli"){
                                    for(var c in $scope.kelurahan1){
                                        if($scope.ViewData.ListOfSOCustomerInformationView[1].VillageId == $scope.kelurahan1[c].VillageId){
                                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = $scope.kelurahan1[c].VillageId;
                                        }
                                    }
                                // }
                             });
                        });
                    
                        
                    });
                }
                
            }else if($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
            $scope.ViewData.CustomerTypeDesc === 'Pemerintah' ||
            $scope.ViewData.CustomerTypeDesc === 'Yayasan'){

                if($scope.ViewData.SOTypeName=="Finish Unit") {
                    $scope.kodepos = [{
                        PostalCode : $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode
                    }];
    
                    $scope.kodepos7 = [{
                        PostalCode : $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode
                    }];
    
                    $scope.kodepos1 = [{
                        PostalCode : $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode
                    }];
    
                    $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode = $scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode;
                    $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode;
                    $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode;
    
                    ProvID0 = $scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId;
                    ProvID1 = $scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId;
                    ProvID2 = $scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId;
                    
                    kot0 = $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId;
                    kot1 = $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId;
                    kot2 = $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId;
    
                    kec0 = $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId;
                    kec1 = $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId;
                    kec2 = $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId;
    
                    
                    ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + ProvID0).then(function (res) { 
                        $scope.kabupaten= res.data.Result;
        
                            for(var c in $scope.kabupaten){
                                if($scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId == $scope.kabupaten[c].CityRegencyId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId = $scope.kabupaten[c].CityRegencyId;
                                }
                            }
                       
                        ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + kot0).then(function (res) { 
                            $scope.kecamatan = res.data.Result; 
                            for(var c in $scope.kecamatan){
                                if($scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId == $scope.kecamatan[c].DistrictId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId = $scope.kecamatan[c].DistrictId;
                                }
                            }
                            ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + kec0).then(function (res) { 
                                $scope.kelurahan = res.data.Result;
                                // if($scope.ViewData.CustomerTypeDesc == 'Individu' && selected.CustomerCorrespondentName === "Pembeli"){
                                    for(var c in $scope.kelurahan){
                                        if($scope.ViewData.ListOfSOCustomerInformationView[0].VillageId == $scope.kelurahan[c].VillageId){
                                            $scope.ViewData.ListOfSOCustomerInformationView[0].VillageId = $scope.kelurahan[c].VillageId;
                                        }
                                    }
                                // }
                             });
                        });
                    
                        
                    });
    
                    ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + ProvID1).then(function (res) { 
                        $scope.kabupaten7 = res.data.Result;
                        console.log("$scope.kabupaten7",$scope.kabupaten7);
                            for(var c in $scope.kabupaten){
                                if($scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId == $scope.kabupaten[c].CityRegencyId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = $scope.kabupaten[c].CityRegencyId;
                                }
                            }
                       
                        ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + kot1).then(function (res) { 
                            $scope.kecamatan7 = res.data.Result; 
                            for(var c in $scope.kecamatan){
                                if($scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId == $scope.kecamatan[c].DistrictId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = $scope.kecamatan[c].DistrictId;
                                }
                            }
                            ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + kec1).then(function (res) { 
                                $scope.kelurahan7 = res.data.Result;
                                // if($scope.ViewData.CustomerTypeDesc == 'Individu' && selected.CustomerCorrespondentName === "Pembeli"){
                                    for(var c in $scope.kelurahan){
                                        if($scope.ViewData.ListOfSOCustomerInformationView[1].VillageId == $scope.kelurahan[c].VillageId){
                                            $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = $scope.kelurahan[c].VillageId;
                                        }
                                    }
                                // }
                             });
                        });
                    
                        
                    });
    
                    ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + ProvID2).then(function (res) { 
                        $scope.kabupaten1 = res.data.Result;
        
                            for(var c in $scope.kabupaten){
                                if($scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId == $scope.kabupaten[c].CityRegencyId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId = $scope.kabupaten[c].CityRegencyId;
                                }
                            }
                       
                        ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + kot2).then(function (res) { 
                            $scope.kecamatan1 = res.data.Result; 
                            for(var c in $scope.kecamatan){
                                if($scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId == $scope.kecamatan[c].DistrictId){
                                    $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId = $scope.kecamatan[c].DistrictId;
                                }
                            }
                            ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + kec2).then(function (res) { 
                                $scope.kelurahan1 = res.data.Result;
                                // if($scope.ViewData.CustomerTypeDesc == 'Individu' && selected.CustomerCorrespondentName === "Pembeli"){
                                    for(var c in $scope.kelurahan){
                                        if($scope.ViewData.ListOfSOCustomerInformationView[2].VillageId == $scope.kelurahan[c].VillageId){
                                            $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = $scope.kelurahan[c].VillageId;
                                        }
                                    }
                                // }
                             });
                        });
                    
                        
                    });
                }

                
            }
            
        MaintainSOFactory.getPeranPelanggan().then(
            function(res) {
                $scope.getPeranPelanggan = res.data.Result;

                
                if ($scope.ViewData.CustomerTypeDesc == 'Individu'){
                    $scope.getPeranPelanggan.splice(1, 1);
                    return res.data;
                }else{
                    return res.data;
                }
                
                
            }
        );

            MaintainSOFactory.getDataProvince().then(function (res) { 
                $scope.province = res.data.Result; 
                if($scope.ViewData.SOTypeName=="Finish Unit") {
                    $scope.filterKabupatenPembeliIndividu($scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId);
                    console.log("ViewData.ListOfSOCustomerInformationView[0].ProvinceId",$scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId);
                }
                
            });
            
            $scope.filterKabupatenPembeliIndividu();
            MaintainSOFactory.getDataGender().then(function (res) { $scope.jeniskelamin = res.data.Result; });
            MaintainSOFactory.getDataStatusPernikahan().then(function (res) { $scope.maritalststus = res.data.Result; });
            MaintainSOFactory.getDataAgama().then(function (res) { $scope.agama = res.data.Result; });
            MaintainSOFactory.getDataJabatan().then(function (res) { $scope.jabatan = res.data.Result; });
            MaintainSOFactory.getDataSectorBisnis().then(function (res) { $scope.sectorbisnis = res.data.Result; });
            // // KOTA/KABUPATEN
            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[0].ProvinceId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId != "undefined") {
            //         ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId).then(function (res) {
            //             $scope.kabupaten0 = res.data.Result;
            //         });
            //     };
            // })

            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[1].ProvinceId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId != "undefined") {
            //         ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId).then(function (res) {
            //             $scope.kabupaten0 = res.data.Result;
            //         });
            //     };
            // })

            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[2].ProvinceId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId != "undefined") {
            //         ComboBoxFactory.getDataKabupaten('?start=1&limit=100&filterData=ProvinceId|' + $scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId).then(function (res) {
            //             $scope.kabupaten0 = res.data.Result;
            //         });
            //     };
            // })
            // //KECAMATAN
            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[0].CityRegencyId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId != "undefined") {
            //         ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId).then(function (res) {
            //             $scope.kecamatan0 = res.data.Result;
            //         });
            //     };
            // })

            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[1].CityRegencyId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId != "undefined") {
            //         ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId).then(function (res) {
            //             $scope.kecamatan0 = res.data.Result;
            //         });
            //     };
            // })
            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[2].CityRegencyId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId != "undefined") {
            //         ComboBoxFactory.getDataKecamatan('?start=1&limit=100&filterData=CityRegencyId|' + $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId).then(function (res) {
            //             $scope.kecamatan0 = res.data.Result;
            //         });
            //     };
            // })
            // //KELURAHAN
            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[0].DistrictId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId != "undefined") {
            //         ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId).then(function (res) {
            //             $scope.kelurahan0 = res.data.Result;
            //         });
            //     };
            // })

            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[1].DistrictId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId != "undefined") {
            //         ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId).then(function (res) {
            //             $scope.kelurahan0 = res.data.Result;
            //         });
            //     };
            // })

            // $scope.$watch("ViewData.ListOfSOCustomerInformationView[2].DistrictId", function (newValue, oldValue) {
            //     if ($scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId != null || typeof $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId != "undefined") {
            //         ComboBoxFactory.getDataKelurahan('?start=1&limit=100&filterData=DistrictId|' + $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId).then(function (res) {
            //             $scope.kelurahan0 = res.data.Result;
            //         });
            //     };
            // })
			
			if ($scope.ViewData.SOTypeId == 3 || $scope.ViewData.SOTypeId == 2) {
                var getSalesId = "&SalesId=" + $scope.ViewData.EmployeeId;
                MaintainSOFactory.getNoRangka(getSalesId).then(
                    function(res) {
                        $scope.DataNorangka = res.data.Result;
                        for(var i = 0; i < $scope.DataNorangka.length; ++i)
						{
							if($scope.DataNorangka[i].VehicleId==$scope.ViewData.VehicleId)
							{
								$scope.pilihFrameNo($scope.DataNorangka[i]);
								break;
							}
						}
                    }
                );
            }

            $scope.ViewData.tempDP = angular.copy($scope.ViewData.DownPayment);
			
            // if($scope.ViewData.DownPayment > ViewData.DownPayment){
            //     MaintainSOFactory.getDownPaymentAfterEdit().then(
            //         function(res) {
            //             $scope.getDownPaymentAfterEdit = res.data.Result;
            //             return res.data;
            //         }
            //     );
            // }
			
            WarnaFactory.getData("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.ViewData.VehicleTypeId).then(
                function(res) {
                    $scope.listWarna = res.data.Result;
					$scope.pilihBank();
					//$scope.btnPilihPengurusanJasa();
					$scope.gridJasaPengurusanDokumen.data = $scope.ubahSOCode.ListOfSODocumentCareDetailService;
					$scope.blabla = $scope.gridJasaPengurusanDokumen.data;
					
					$scope.urlPricing = '/api/fe/PricingEngine?PricingId=30401&OutletId=' + $scope.user.OutletId + '&DisplayId=' + $scope.user.OutletId + '30401';
					$scope.jsData = { Title: null, HeaderInputs: { $TotalDPP$: null, $TotalVAT$: null, $GrandTotal$: 0 } }
					//$scope.jsData.HeaderInputs.$OutletId$ = $scope.user.OutletId;
					$scope.jsData.HeaderInputs.$TotalDPP$ = $scope.ubahSOCode.TotalDPP;
					$scope.jsData.HeaderInputs.$TotalVAT$ = $scope.ubahSOCode.PPN;
					$scope.jsData.HeaderInputs.$GrandTotal$ = $scope.ubahSOCode.GrandTotal;
					
					//$scope.selectedTipe.VehicleType.VehicleTypeId=$scope.ubahSOCode.VehicleTypeId
					var DaMaintainSoFreePpnThingy1=0;
			if($scope.ViewData.FreePPNBit==true)
			{
				DaMaintainSoFreePpnThingy1=1;
			}
			else
			{
				DaMaintainSoFreePpnThingy1=0;
			}
            
            $scope.JumlahSementara = 0;
            angular.forEach($scope.blabla, function(acc, accIdx) {
				var param = {
                    "Title": "Order Pengurusan Dokumen",
                    "HeaderInputs": {
                        "$ServiceId$": acc.ServiceId,
                        "$OutletId$": acc.OutletId,
                        "$QTY$": acc.Qty,
                        "$AdminFee$": acc.AdminFee,
						"$FreePPNBit$": DaMaintainSoFreePpnThingy1,
						"$ServicePriceInput$": $scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice,
						"$VehicleTypeId$": acc.VehicleTypeId
                    }
					//,"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
					//tadi yg service price ga ada

                };

                $scope.JumlahSementara = 0;
                $http.post('/api/fe/PricingEngine?PricingId=30001&OutletId=' + $scope.user.OutletId + '&DisplayId=' + $scope.user.OutletId + '30001', param)
                    .then(function(res) {
                        $scope.blabla[accIdx].ServicePrice = res.data.Codes["#ServicePrice#"];
                        $scope.blabla[accIdx].AdminFee = res.data.Codes["$AdminFee$"];
                        $scope.blabla[accIdx].QTY = res.data.Codes["[QTY]"];
                        $scope.blabla[accIdx].DPP = res.data.Codes["[DPP]"];
                        $scope.blabla[accIdx].VAT = res.data.Codes["[PPN]"];
                        $scope.blabla[accIdx].PriceAfterTax = res.data.Codes["[PriceAfterTax]"];
                        $scope.blabla[accIdx].SubTotal = res.data.Codes["[SubTotal]"];
                        $scope.blabla[accIdx].Total = res.data.Codes["[Total]"];
						//dhemit
                        $scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice=angular.copy(res.data.Codes["[HargaService]"]);
                        

                        // $scope.ViewData.PPN = $scope.ViewData.PPN + parseInt(res.data.Codes["[PPN]"]);
                        // $scope.ViewData.TotalDpp = $scope.ubahSOCode_Lagi.TotalDPP;
                        // $scope.ViewData.GrandTotal = $scope.ViewData.GrandTotal + parseInt(res.data.Codes["[Total]"]);
                        // $scope.ViewData.TotalVAT= $scope.ViewData.TotalVAT + parseInt(res.data.Codes["[PPN]"]);
                        $scope.jumlahHarga = 0;
                        $scope.jumlahHarga = (parseInt(res.data.Codes["[HargaService]"] * parseInt(res.data.Codes["[QTY]"])));
                        console.log('$scope.jumlahHarga',$scope.jumlahHarga);
                        $scope.JumlahSementara = $scope.JumlahSementara + angular.copy($scope.jumlahHarga);
                         
                        console.log('$scope.JumlahSementara',$scope.JumlahSementara);

                        $scope.ViewData.TampungHarga = $scope.JumlahSementara;
                        console.log('$scope.ViewData.TampungHarga1',$scope.ViewData.TampungHarga);   
                        // $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.Pembulatan = 0;
                        }else{
                            $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        } 

                    });
					
            });
                })
            $scope.flagStatus = "UpdateSO";
            $scope.disableViewPelanggan = false;
            $scope.disableViewJasa = false;
            $scope.disableViewAksesoris = false;
            $scope.disableViewDokumen = false;
            $scope.DisabledRincianHarga = false;
            $scope.DisableSOText = true;

            $scope.filterTenor($scope.ViewData.ListOfSOLeasingView[0].LeasingId);

            if($scope.ViewData.PemakaiPembeliBit == true){
                $scope.ViewData.tempPemakai = '1';
                
            }else if($scope.ViewData.PemakaiPemilikBit == true){
                $scope.ViewData.tempPemakai = '2';
            }else{
                $scope.ViewData.tempPemakai = '0';
            }
            
			$scope.gridJasaPengurusanDokumen.data = $scope.ubahSOCode.ListOfSODocumentCareDetailService;
            
			$scope.ViewData.TotalDpp = $scope.ubahSOCode_Lagi.TotalDPP;
			$scope.ViewData.PPN = $scope.ubahSOCode_Lagi.PPN;    
            $scope.ViewData.GrandTotal = $scope.ubahSOCode_Lagi.GrandTotal;
            $scope.ViewData.TotalVAT= $scope.ubahSOCode_Lagi.TotalVAT;
            $scope.ViewData.TampungTotalVAT = $scope.ubahSOCode_Lagi.TotalVAT;
            $scope.ViewData.TampungTotalDpp = $scope.ubahSOCode_Lagi.TotalDPP
			
            
			$scope.gridAksesoris.data = $scope.ubahSOCode.ListOfSODetailAccesoriesView;
            $scope.gridDokumenPembetulanMaintainSo = $scope.ubahSOCode.ListOfSODocumentView;
            if ($scope.ViewData.SOTypeName == "Finish Unit") {               
                console.log('$scope.ViewData', $scope.ViewData)
                $scope.flagStatus = "UpdateSO";
                $scope.btnSimpanCreateSO = true;
                $scope.btnBackToMaintainSO = true;
                //------------code baru edit tab finsih unit------------//
                $scope.CreateSO = true;
                $scope.MaintainForm = false;
                $scope.ShowMenuTabs = false; //TAB UNTUK TAB CONTAINER SELAIN EDIT FINISH UNIT
                $scope.ShowtabContainer = false; //TAB UNTUK TAB CONTAINER SELAIN EDIT FINISH UNIT
                $scope.ShowMenuTabs_FinishUnit = true;
                $scope.ShowtabContainer_FinishUnit = true;
                // MUNCULIN TAB EDIT FINISH UNIT //
                $scope.InfoPelanggan_FinishUnit = true;
                $scope.Dokumen_FinishUnit = true;
                $scope.Aksesoris_FinishUnit = false;
                $scope.karoseriFinishUnit_FinishUnit = false;
                $scope.InfoPengiriman_FinishUnit = true;
                $scope.InfoLeasingFinishUnit_FinishUnit = true;
                $scope.InfoAsuransiFinishUnit_FinishUnit = true;
                $scope.finishUnitInfounityangakandibeli_FinishUnit = true;
                $scope.RincianHarga_FinishUnit = true;
                $scope.InfoMediatorFinishUnit_FinishUnit = true;
                $scope.InfoPemakaiFinishUnit_FinishUnit = true;
                // ------ISI DARI TAB INFO PELANGGAN ------//
                //PEMBELI INDIVIDU//
                $scope.disabledKTPPembeliIndividu = true;
                $scope.disabledProvinsiPembeliIndividu = true;
                $scope.disabledKodePosPembeliIndividu = true;
                $scope.disabledNoTeleponPembeliIndividu = true;
                $scope.disabledJenisKelaminPembeliIndividu = true;
                $scope.disabledJabatanPembeliIndividu = true;
                $scope.disabledTransaksiPajakPembeliIndividu = true;
                $scope.disabledNamaLengkapPembeliIndividu = true;
                $scope.disabledKotaKabPembeliIndividu = true;
                $scope.disabledRTPembeliIndividu = true;
                $scope.disabledNoHpPembeliIndividu = true;
                $scope.disabledStatusKawinPembeliIndividu = true;
                $scope.disabledTeleponKantorPembeliIndividu = true;
                $scope.disabledFreePPNPembeliIndividu = true;
                $scope.disabledNamaPanggilanPembeliIndividu = true;
                $scope.disabledKecamatanPembeliIndividu = true;
                $scope.disabledRWPembeliIndividu = true;
                $scope.disabledTempatLahirPembeliIndividu = true;
                $scope.disabledAgamaPembeliIndividu = true;
                $scope.disabledAlamatKantorPembeliIndividu = true;
                $scope.disabledFreePPNBitPembeliIndividu = true;
                $scope.disabledAlamatPembeliIndividu = true;
                $scope.disabledKelurahanPembeliIndividu = true;
                $scope.disabledEmailPembeliIndividu = true;
                $scope.disabledTglLahirPembeliIndividu = true;
                $scope.disabledPekerjaanPembeliIndividu = true;
                $scope.disabledNPWPPembeliIndividu = true;  
                // PEMILIK INDIVIDU
                $scope.disabledKTPPemilikNew = false;
                $scope.disabledKotaPemilikNew = false;
                $scope.disabledRTPemilikNew = false;
                $scope.disabledNoHpPemilikNew = false;
                $scope.disabledNamaLengkapPemilikNew = false;
                $scope.disabledKecamatanPemilikNew = false;
                $scope.disabledRWPemilikNew = false;
                $scope.disabledTglAfiPemilikNew = true;
                $scope.disabledAlamatPemilikNew = false;
                $scope.disabledKelurahanPemilikNew = false;
                $scope.disabledEmailPemilikNew = false;
                $scope.disabledProvinsiPemilikNew = false;
                $scope.disabledKodePosPemilikNew = true;
                $scope.disabledNoTlpPemilikNew = false;
                //PEMBELI INSTANSI//
                $scope.disabledSIUPInstansiNew = true;
                $scope.disabledProvinsiInstansiNew = true;
                $scope.disabledKodePosInstansiNew = true;
                $scope.disabledSektorBisnisInstansiNew = true;
                $scope.disabledTDPInstansiNew = true;
                $scope.disabledKotaInstansiNew = true;
                $scope.disabledRTInstansiNew = true;
                $scope.disabledNPWPInstansiNew = true;
                $scope.disabledKTPInstansiNew = true;
                $scope.disabledKecamatanInstansiNew = true;
                $scope.disabledNamaInstansiPembeliInstansi = true;
                $scope.disabledRWInstansiNew = true;
                $scope.disabledAlamatInstansiNew = true;
                $scope.disabledKelurahanInstansiNew = true;
                $scope.disabledNoTeleponInstansiNew = true;
                // PENANGGUNG JAWAB INSTANSI
                $scope.disabledNamaLengkapPJ = false;
                $scope.disabledProvinsiPJ = false;
                $scope.disabledKodePosPJ = true;
                $scope.disabledNoTeleponPJ = false;
                $scope.disabledDepartemenPJ = false;
                $scope.disabledNamaPanggilanPJ = false;
                $scope.disabledKotaPJ = false;
                $scope.disabledRTPJ = false;
                $scope.disabledNohpPJ = false;
                $scope.disabledJabatanPJ = false;
                $scope.disabledKTPPJ = false;
                $scope.disabledKecamatanPJ = false;
                $scope.disabledRWPJ = false;
                $scope.disabledTglLahirPJ = false;
                $scope.disabledAlamatInstansiPJ = false;
                $scope.disabledKelurahanPJ = false;
                $scope.disabledEmailPJ = false;
                $scope.disabledJenisKelaminPJ = false;
                // ISI TAB PEMILIK INSTANSI
                $scope.disabledSiupPemilikInstansi = false;
                $scope.disabledAlamatPemilikInstansi = false;
                $scope.disabledKelurahanPemilikInstansi = false;
                $scope.disabledNoTeleponPemilikInstansi = false;
                $scope.disabledTdpPemilikInstansi = false;
                $scope.disabledProvinsiPemilikInstansi = false;
                $scope.disabledKodePosPemilikInstansi = true;
                $scope.disabledNPWPPemilikInstansi = false;
                $scope.disabledKTPPemilikInstansi = false;
                $scope.disabledKotaPemilikInstansi = false;
                $scope.disabledRTPemilikInstansi = false;
                $scope.disabledAlamatKorespondenPemilikInstansi = false;
                $scope.disabledNamaPemilikInstansi = false;
                $scope.disabledKecamatanPemilikInstansi = false;
                $scope.disabledRWPemilikInstansi = false;
                // ISI DARI TAB INFO PENGIRIMAN //
                $scope.disabledNamaPenerimaNew = false;
                $scope.disabledAlamatNamaPenerimaNew = false;
                $scope.disabledHandphonePenerimaNew = false;
                $scope.disabledDeliveryNew = true;
                $scope.disabledOnOffRoadNew = true;
                $scope.disabledMatchingNew = true;
                $scope.disabledPromiseDeliveryNew = true;
                $scope.disabledEstimasiWaktuaNew = true;
                $scope.disabledTanggalPengirimanNew = true;
                $scope.disabledStatusPDDNew = true;
                // ISI DARI TAB INFO LEASING
                $scope.disabledNamaLeasingNew = false;
                $scope.disabledTenorNew = false;
                $scope.disabledNoTDPNew = true;
                // ISI DARI TAB  ASURANSI//
                $scope.disabledNamaAsuransiNew = false;
                $scope.disabledProdukNew = true;
                $scope.disabledPembayaranAsuransiNew = true;
                $scope.disabledKondisiPertanggunganNew = true;
                $scope.disabledJangkaWaktuNew = true;
                $scope.disabledNamaPolisNew = true;
                $scope.disabledJenisPenggunaanNew = true;
                $scope.disabledEStimasiBiayaAsuransiNNew = true;
                // ISI DARI TAB  MEDIATOR//
                $scope.disabledNamaMediatorNew = false;
                $scope.disabledTeleponMediatorNew = false;
                $scope.disabledKTPMediatorNew = false;
                // ISI DARI TAB  PEMAKAI//
                $scope.disabledNamaPemakaiNew = false;
                $scope.disabledTeleponPemakaiNew = false;
                $scope.disabledAlamatPemakaiNew = false;
                // ISI DARI TAB INFO UNIT YANG AKAN DIBELI
                $scope.disabledModelTipeNew = true;
                $scope.disabledStatusStokNew = true;
                $scope.disabledWarnaNew = false;
                $scope.disabledNoRangkaNew = true;
                $scope.disabledKategoriPlatNew = true;
                $scope.disabledTahunProduksiNew = true;
                $scope.disabledPermintaanPlatKhususNew = true;
                //ISI DARI TAB RINCIAN HARGA
                $scope.disabledProgramPenjualanNew = true;
                $scope.disabledSumberDanaNew = true;
                $scope.disabledTradeInNew = true;
                $scope.disabledJenisPembayaranNew= true;
                $scope.disabledCatatanNew= true;
                $scope.disabledMelaluiBankNew = true;
                $scope.disabledVirtualAccountNew= true;
                $scope.disabledBookingFeeNew = true;
                $scope.disabledDownPaymentNew = false;
                $scope.disabledRekeningDealerNew = true;
                // $scope.disabledDiskonNew= false;
                // $scope.disabledDiskonRateNew = false;
                // $scope.disabledDiskonDPNew= false;
                // $scope.disabledBBNAdjustmentNew = false;
                // $scope.disabledBBNServiceNew= false;

                $scope.disabledTotalDPPNew= true;
                $scope.disabledTotalPpnNew= true;
                $scope.disabledPembulatanNew= true;
                $scope.disabledGrandTotalNew= true;
                // ISI DARI TAB DOKUMEN
                $scope.btntambah = true;
                // $scope.ShowtabContainer = true;
                $scope.ShowSONumber = true;
                $scope.ShowProspectID = true;
                $scope.ShowProspectName = true;
                $scope.ShowSalesman = false;
                // $scope.InfoPelanggan = true;
                $scope.ShowPeranPelanggan_FinsihUnit = true;
                $scope.ShowPeranPelanggan= false;
                $scope.disableTextInfoPelanggan = true;
                $scope.ShowPelangganOPD = false;
                $scope.ShowPelangganFinishUnit = true;
                // $scope.JasaPengurusanDokumen = false;
                // $scope.Dokumen = true;
                // $scope.btntambah = false;
                // $scope.InfoPengiriman = true;
                $scope.showDelivery = true;
                $scope.showOfftheRoad = true;
                $scope.showMatching = true;
                $scope.showPromiseDelivery = true;
                $scope.showEstimasiRentang = true;
                $scope.showStatusPDD = true;
                // $scope.RincianHarga = true;
                // $scope.Aksesoris = true;
                // $scope.karoseriFinishUnit = true;
                // $scope.InfoLeasingFinishUnit = true;
                $scope.disabledinfoLeasing = true;
                // $scope.InfoAsuransiFinishUnit = true;
                // $scope.finishUnitInfounityangakandibeli = true;
                // $scope.finishUnitWarna = false;
                // $scope.InfoMediatorFinishUnit = true;
                // $scope.InfoPemakaiFinishUnit = true;
                $scope.showAlasanBatalSO = false;
                $scope.showAlasanBatalSO_FinsihUnit = false;
                $scope.infoPembeli = false;
                $scope.infoPembeli_individuFinishUnit = true;
                // $scope.infoPemilik_IndividuFinsihUnit = true;
                // $scope.infoPembeliPerusahaan_FinsihUnit = true;
                // $scope.infoPenanggungJawabPerusahaan_FinishUnit = true;
                // $scope.infoPemilikPerusahaan_FinishUnit = true;
                setDefaultTab_FinishUnit();
				// setDefaultTab();
                //----Code lama sebelum tab edit finish unit ditampilkan--//
                $scope.CreateSO = true;
                $scope.MaintainForm = false;
                //sampai sini//
                if ($scope.ViewData.FrameNo != null && $scope.ViewData.FrameNo != "") {
                    
					bsNotify.show({
									title: "Peringatan",
									content: "Tidak Dapat Merubah Tipe SO Finish Unit yang Sudah Terisi Nomor Rangka & Status Stock Matching!",
									type: 'warning'
								});
                                console.log('masuk sini01');
                                $scope.ShowMenuTabs_FinishUnit = true;
                                $scope.ShowtabContainer_FinishUnit = true;
                                $scope.ShowMenuTabs = false; //TAB UNTUK TAB CONTAINER SELAIN EDIT FINISH UNIT
                                $scope.ShowtabContainer = false; //TAB UNTUK TAB CONTAINER SELAIN EDIT FINISH UNIT
                                // CODE BARU MUNCULIN TAB EDIT FINISH UNIT //
                                $scope.InfoPelanggan_FinishUnit = true;
                                $scope.Dokumen_FinishUnit = true;
                                $scope.Aksesoris_FinishUnit = false;
                                $scope.karoseriFinishUnit_FinishUnit = false;
                                $scope.InfoPengiriman_FinishUnit = true;
                                $scope.InfoLeasingFinishUnit_FinishUnit = true;
                                $scope.InfoAsuransiFinishUnit_FinishUnit = true;
                                $scope.finishUnitInfounityangakandibeli_FinishUnit = true;
                                $scope.RincianHarga_FinishUnit = true;
                                $scope.InfoMediatorFinishUnit_FinishUnit = true;
                                $scope.InfoPemakaiFinishUnit_FinishUnit = true;
                               // ------ISI DARI TAB INFO PELANGGAN ------//
                                //PEMBELI INDIVIDU//
                                $scope.disabledKTPPembeliIndividu = true;
                                $scope.disabledProvinsiPembeliIndividu = true;
                                $scope.disabledKodePosPembeliIndividu = true;
                                $scope.disabledNoTeleponPembeliIndividu = true;
                                $scope.disabledJenisKelaminPembeliIndividu = true;
                                $scope.disabledJabatanPembeliIndividu = true;
                                $scope.disabledTransaksiPajakPembeliIndividu = true;
                                $scope.disabledNamaLengkapPembeliIndividu = true;
                                $scope.disabledKotaKabPembeliIndividu = true;
                                $scope.disabledRTPembeliIndividu = true;
                                $scope.disabledNoHpPembeliIndividu = true;
                                $scope.disabledStatusKawinPembeliIndividu = true;
                                $scope.disabledTeleponKantorPembeliIndividu = true;
                                $scope.disabledFreePPNPembeliIndividu = true;
                                $scope.disabledNamaPanggilanPembeliIndividu = true;
                                $scope.disabledKecamatanPembeliIndividu = true;
                                $scope.disabledRWPembeliIndividu = true;
                                $scope.disabledTempatLahirPembeliIndividu = true;
                                $scope.disabledAgamaPembeliIndividu = true;
                                $scope.disabledAlamatKantorPembeliIndividu = true;
                                $scope.disabledFreePPNBitPembeliIndividu = true;
                                $scope.disabledAlamatPembeliIndividu = true;
                                $scope.disabledKelurahanPembeliIndividu = true;
                                $scope.disabledEmailPembeliIndividu = true;
                                $scope.disabledTglLahirPembeliIndividu = true;
                                $scope.disabledPekerjaanPembeliIndividu = true;
                                $scope.disabledNPWPPembeliIndividu = true;  
                                // PEMILIK INDIVIDU
                                $scope.disabledKTPPemilikNew = false;
                                $scope.disabledKotaPemilikNew = false;
                                $scope.disabledRTPemilikNew = false;
                                $scope.disabledNoHpPemilikNew = false;
                                $scope.disabledNamaLengkapPemilikNew = false;
                                $scope.disabledKecamatanPemilikNew = false;
                                $scope.disabledRWPemilikNew = false;
                                $scope.disabledTglAfiPemilikNew = true;
                                $scope.disabledAlamatPemilikNew = false;
                                $scope.disabledKelurahanPemilikNew = false;
                                $scope.disabledEmailPemilikNew = false;
                                $scope.disabledProvinsiPemilikNew = false;
                                $scope.disabledKodePosPemilikNew = true;
                                $scope.disabledNoTlpPemilikNew = false;
                                //PEMBELI INSTANSI//
                                $scope.disabledSIUPInstansiNew = true;
                                $scope.disabledProvinsiInstansiNew = true;
                                $scope.disabledKodePosInstansiNew = true;
                                $scope.disabledSektorBisnisInstansiNew = true;
                                $scope.disabledTDPInstansiNew = true;
                                $scope.disabledKotaInstansiNew = true;
                                $scope.disabledRTInstansiNew = true;
                                $scope.disabledNPWPInstansiNew = true;
                                $scope.disabledKTPInstansiNew = true;
                                $scope.disabledKecamatanInstansiNew = true;
                                $scope.disabledNamaInstansiPembeliInstansi = true;
                                $scope.disabledRWInstansiNew = true;
                                $scope.disabledAlamatInstansiNew = true;
                                $scope.disabledKelurahanInstansiNew = true;
                                $scope.disabledNoTeleponInstansiNew = true;
                                // PENANGGUNG JAWAB INSTANSI
                                $scope.disabledNamaLengkapPJ = false;
                                $scope.disabledProvinsiPJ = false;
                                $scope.disabledKodePosPJ = true;
                                $scope.disabledNoTeleponPJ = false;
                                $scope.disabledDepartemenPJ = false;
                                $scope.disabledNamaPanggilanPJ = false;
                                $scope.disabledKotaPJ = false;
                                $scope.disabledRTPJ = false;
                                $scope.disabledNohpPJ = false;
                                $scope.disabledJabatanPJ = false;
                                $scope.disabledKTPPJ = false;
                                $scope.disabledKecamatanPJ = false;
                                $scope.disabledRWPJ = false;
                                $scope.disabledTglLahirPJ = false;
                                $scope.disabledAlamatInstansiPJ = false;
                                $scope.disabledKelurahanPJ = false;
                                $scope.disabledEmailPJ = false;
                                $scope.disabledJenisKelaminPJ = false;
                                // ISI TAB PEMILIK INSTANSI
                                $scope.disabledSiupPemilikInstansi = false;
                                $scope.disabledAlamatPemilikInstansi = false;
                                $scope.disabledKelurahanPemilikInstansi = false;
                                $scope.disabledNoTeleponPemilikInstansi = false;
                                $scope.disabledTdpPemilikInstansi = false;
                                $scope.disabledProvinsiPemilikInstansi = false;
                                $scope.disabledKodePosPemilikInstansi = true;
                                $scope.disabledNPWPPemilikInstansi = false;
                                $scope.disabledKTPPemilikInstansi = false;
                                $scope.disabledKotaPemilikInstansi = false;
                                $scope.disabledRTPemilikInstansi = false;
                                $scope.disabledAlamatKorespondenPemilikInstansi = false;
                                $scope.disabledNamaPemilikInstansi = false;
                                $scope.disabledKecamatanPemilikInstansi = false;
                                $scope.disabledRWPemilikInstansi = false;
                                // ISI DARI TAB INFO PENGIRIMAN //
                                $scope.disabledNamaPenerimaNew = false;
                                $scope.disabledAlamatNamaPenerimaNew = false;
                                $scope.disabledHandphonePenerimaNew = false;
                                $scope.disabledDeliveryNew = true;
                                $scope.disabledOnOffRoadNew = true;
                                $scope.disabledMatchingNew = true;
                                $scope.disabledPromiseDeliveryNew = true;
                                $scope.disabledEstimasiWaktuaNew = true;
                                $scope.disabledTanggalPengirimanNew = true;
                                $scope.disabledStatusPDDNew = true;
                                // ISI DARI TAB INFO LEASING
                                $scope.disabledNamaLeasingNew = false;
                                $scope.disabledTenorNew = false;
                                $scope.disabledNoTDPNew = true;
                                // ISI DARI TAB  ASURANSI//
                                $scope.disabledNamaAsuransiNew = false;
                                $scope.disabledProdukNew = true;
                                $scope.disabledPembayaranAsuransiNew = true;
                                $scope.disabledKondisiPertanggunganNew = true;
                                $scope.disabledJangkaWaktuNew = true;
                                $scope.disabledNamaPolisNew = true;
                                $scope.disabledJenisPenggunaanNew = true;
                                $scope.disabledEStimasiBiayaAsuransiNNew = true;
                                // ISI DARI TAB INFO UNIT YANG AKAN DIBELI
                                $scope.disabledModelTipeNew = true;
                                $scope.disabledStatusStokNew = true;
                                $scope.disabledWarnaNew = false;
                                $scope.disabledNoRangkaNew = true;
                                $scope.disabledKategoriPlatNew = true;
                                $scope.disabledTahunProduksiNew = true;
                                $scope.disabledPermintaanPlatKhususNew = true;
                                $scope.disabledProgramPenjualanNew = true;
                                $scope.disabledSumberDanaNew = true;
                                $scope.disabledTradeInNew = true;
                                $scope.disabledJenisPembayaranNew= true;
                                $scope.disabledCatatanNew= true;
                                $scope.disabledMelaluiBankNew = true;
                                $scope.disabledVirtualAccountNew= true;
                                $scope.disabledBookingFeeNew = true;
                                $scope.disabledDownPaymentNew = false;
                                $scope.disabledRekeningDealerNew = true;
                                // $scope.disabledDiskonNew= false;
                                // $scope.disabledDiskonRateNew = false;
                                // $scope.disabledDiskonDPNew= false;
                                // $scope.disabledBBNAdjustmentNew = false;
                                // $scope.disabledBBNServiceNew= false;

                                $scope.disabledTotalDPPNew= true;
                                $scope.disabledTotalPpnNew= true;
                                $scope.disabledPembulatanNew= true;
                                $scope.disabledGrandTotalNew= true;
                                // ISI DARI TAB  MEDIATOR//
                                $scope.disabledNamaMediatorNew = false;
                                $scope.disabledTeleponMediatorNew = false;
                                $scope.disabledKTPMediatorNew = false;
                                // ISI DARI TAB  PEMAKAI//
                                $scope.disabledNamaPemakaiNew = false;
                                $scope.disabledTeleponPemakaiNew = false;
                                $scope.disabledAlamatPemakaiNew = false;
                                // ISI DARI TAB DOKUMEN
                                $scope.btntambah = true;
                                // $scope.ShowtabContainer = true;
                                $scope.ShowSONumber = true;
                                $scope.ShowProspectID = true;
                                $scope.ShowProspectName = true;
                                $scope.ShowSalesman = false;
                                // $scope.InfoPelanggan = true;
                                $scope.ShowPeranPelanggan = false;
                                $scope.ShowPeranPelanggan_FinsihUnit = true;
                                $scope.disableTextInfoPelanggan = true;
                                $scope.ShowPelangganOPD = false;
                                $scope.ShowPelangganFinishUnit = true;
                                // $scope.JasaPengurusanDokumen = false;
                                // $scope.Dokumen = true;
                                // $scope.btntambah = false;
                                // $scope.InfoPengiriman = true;
                                $scope.showDelivery = true;
                                $scope.showOfftheRoad = true;
                                $scope.showMatching = true;
                                $scope.showPromiseDelivery = true;
                                $scope.showEstimasiRentang = true;
                                $scope.showStatusPDD = true;
                                // $scope.RincianHarga = true;
                                // $scope.Aksesoris = true;
                                // $scope.karoseriFinishUnit = true;
                                // $scope.InfoLeasingFinishUnit = true;
                                $scope.disabledinfoLeasing = true;
                                // $scope.InfoAsuransiFinishUnit = true;
                                // $scope.finishUnitInfounityangakandibeli = true;
                                $scope.finishUnitWarna = false;
                                // $scope.InfoMediatorFinishUnit = true;
                                // $scope.InfoPemakaiFinishUnit = true;
                                $scope.showAlasanBatalSO = false;
                                $scope.showAlasanBatalSO_FinsihUnit = false;
                                $scope.infoPembeli = false;
                                $scope.infoPembeli_individuFinishUnit = true;
                                // $scope.infoPemilik_IndividuFinsihUnit = true;
                                // $scope.infoPembeliPerusahaan_FinsihUnit = true;
                                // $scope.infoPenanggungJawabPerusahaan_FinishUnit = true;
                                // $scope.infoPemilikPerusahaan_FinishUnit = true;
                                // setDefaultTab();
                                //----Code lama sebelum tab edit finish unit ditampilkan--//
                                $scope.CreateSO = true;
                                $scope.MaintainForm = false;
                                setDefaultTab_FinishUnit();
                }  else {
               
                //----Code lama sebelum tab edit finish unit ditampilkan--//
                // console.log('masuk sini02');
                // $scope.CreateSO = true;
                // $scope.MaintainForm = false;
                // $scope.ShowMenuTabs = true;
                //     $scope.ShowtabContainer = true;
                //     $scope.ShowSONumber = true;
                //     $scope.ShowProspectID = true;
                //     $scope.ShowProspectName = true;
                //     $scope.ShowSalesman = false;
                //     $scope.InfoPelanggan = false;
                //     $scope.ShowPeranPelanggan = false;
                //     $scope.disableTextInfoPelanggan = true;
                //     $scope.ShowPelangganOPD = false;
                //     $scope.ShowPelangganFinishUnit = false;
                //     $scope.JasaPengurusanDokumen = false;
                //     $scope.Dokumen = false;
                //     $scope.btntambah = false;
                //     $scope.InfoPengiriman = false;
                //     $scope.disabledInfoPengiriman = false;
                //     $scope.showDelivery = false;
                //     $scope.showOfftheRoad = false;
                //     $scope.showMatching = false;
                //     $scope.showPromiseDelivery = false;
                //     $scope.showEstimasiRentang = false;
                //     $scope.showStatusPDD = false;
                //     $scope.RincianHarga = false;
                //     $scope.Aksesoris = false;
                //     $scope.karoseriFinishUnit = false;
                //     $scope.InfoLeasingFinishUnit = false;
                //     $scope.disabledinfoLeasing = true;
                //     $scope.InfoAsuransiFinishUnit = false;
                //     $scope.finishUnitInfounityangakandibeli = true;
                //     $scope.finishUnitWarna = false;
                //     $scope.InfoMediatorFinishUnit = false;
                //     $scope.InfoPemakaiFinishUnit = false;
                //     $scope.showAlasanBatalSO = true;
                }
            } else {
                $scope.CreateSO = true;
                $scope.btnSimpanCreateSO = true;
                $scope.btnBackToMaintainSO = true;
                $scope.MaintainForm = false;
                if ($scope.ViewData.SOTypeName == "OPD") {
                    // CODE BARU MUNCULIN TAB EDIT FINISH UNIT (dibuat false agar tidak muncul di selain finish unit//
                    $scope.InfoPelanggan_FinishUnit = false;
                    $scope.Dokumen_FinishUnit = false;
                    $scope.Aksesoris_FinishUnit = false;
                    $scope.karoseriFinishUnit_FinishUnit = false;
                    $scope.InfoPengiriman_FinishUnit = false;
                    $scope.InfoLeasingFinishUnit_FinishUnit = false;
                    $scope.InfoAsuransiFinishUnit_FinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli_FinishUnit = false;
                    $scope.RincianHarga_FinishUnit = false;
                    $scope.InfoMediatorFinishUnit_FinishUnit = false;
                    $scope.InfoPemakaiFinishUnit_FinishUnit = false;
                   // ISI DARI TAB INFO PENGIRIMAN //
                    $scope.disabledNamaPenerimaNew = false;
                    $scope.disabledAlamatNamaPenerimaNew = false;
                    $scope.disabledHandphonePenerimaNew = false;
                    $scope.disabledDeliveryNew = false;
                    $scope.disabledOnOffRoadNew = false;
                    $scope.disabledMatchingNew = false;
                    $scope.disabledPromiseDeliveryNew = false;
                    $scope.disabledEstimasiWaktuaNew = false;
                    $scope.disabledTanggalPengirimanNew = false;
                    $scope.disabledStatusPDDNew = false;
                    //-----------------------//
                    $scope.infoPembeli = false;
                    $scope.ShowMenuTabs = true;
                    $scope.ShowMenuTabs_FinishUnit = false;
                    $scope.ShowtabContainer = true;
                    $scope.ShowtabContainer_FinishUnit = false;
                    $scope.ShowSONumber = true;
                    $scope.ShowSalesman = true;
                    $scope.ShowProspectID = false;
                    $scope.ShowProspectName = false;
                    $scope.ShowPeranPelanggan = false;
                    $scope.disableTextInfoPelanggan = true;
                    $scope.InfoPelanggan = true;
                    $scope.ShowPelangganOPD = true;
                    $scope.ShowPelangganFinishUnit = false;
                    $scope.JasaPengurusanDokumen = true;
                    $scope.Dokumen = true;
                    $scope.btntambah = true;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.DisabledRincianHarga = false;
                    $scope.DisabledTotalRincianHarga = true;
                    $scope.Aksesoris = false;
                    $scope.menuRincianHarga = true;
                    $scope.InfoPelangganFinishUnit = false;
                    $scope.karoseriFinishUnit = false;
                    $scope.InfoLeasingFinishUnit = false;
                    $scope.InfoAsuransiFinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli = false;
                    $scope.finishUnitWarna = true;
                    $scope.InfoMediatorFinishUnit = false;
                    $scope.InfoPemakaiFinishUnit = false;
                } else if ($scope.ViewData.SOTypeName == "Purna Jual") {
                    $scope.ShowMenuTabs = true;
                    $scope.ShowMenuTabs_FinishUnit = false;
                    $scope.ShowtabContainer = true;
                    $scope.ShowtabContainer_FinishUnit = false;
                    $scope.ShowSONumber = true;
                    $scope.ShowProspectID = false;
                    $scope.ShowProspectName = false;
                    $scope.ShowPeranPelanggan = false;
                    $scope.disableTextInfoPelanggan = true;
                    $scope.InfoPelanggan = true;
                    $scope.ShowPelangganOPD = true;
                    $scope.ShowPelangganFinishUnit = false;
                    $scope.JasaPengurusanDokumen = false;
                    $scope.Dokumen = false;
                    $scope.btntambah = true;
                    $scope.Aksesoris = true;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.DisabledRincianHarga = false;
                    $scope.DisabledTotalRincianHarga = true;
                    $scope.InfoPelangganFinishUnit = false;
                    $scope.karoseriFinishUnit = false;
                    $scope.InfoLeasingFinishUnit = false;
                    $scope.InfoAsuransiFinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli = false;
                    $scope.finishUnitWarna = true;
                    $scope.InfoMediatorFinishUnit = false;
                    $scope.InfoPemakaiFinishUnit = false;
                } else if ($scope.ViewData.SOTypeName == "Swapping") {
                    $scope.InfoPelanggan = false;
                    $scope.Aksesoris = false;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.RincianHarga = false;
                    $scope.JasaPengurusanDokumen = false;
                    $scope.Dokumen = false;
                    $scope.finishUnitInfounityangakandibeli = true;
                    $scope.finishUnitUbahInfounityangakandibeli = true;
                }
            }
			
            // SENGAJA DI MATIKAN KARNA PAS UBAH SET LAGI JADI FALSE UNTUK SI CHECKBOX
			// ComboBoxFactory.getKodePajakbyId($scope.ViewData.CInvoiceTypeId).then(function(res) {
            //         $scope.KodePajak = res.data;
            //         if (($scope.KodePajak.Code == '01') || ($scope.KodePajak.Code == '08')) {
            //             $scope.MaintainSODisableFreePPN = false;
                        
            //             // if($scope.breadcrums.title == "Ubah"){
            //             //     $scope.ViewData.FreePPNBit = true;
            //             // }else{
            //                 $scope.ViewData.FreePPNBit = false;
            //             // }
            //         } else if (($scope.KodePajak.Code != '01') || ($scope.KodePajak.Code != '08')) {
            //             $scope.MaintainSODisableFreePPN = true;
                        
            //             if ($scope.KodePajak.Code == '07') {
                            
            //                 $scope.ViewData.FreePPNBit = true;
            //             } else if ($scope.KodePajak.Code != '07') {
                            
            //                 $scope.ViewData.FreePPNBit = false;
            //             }
            //         }
            //     })

                if($scope.ViewData.SOTypeName=="Finish Unit") {
                    $scope.changeInfoPelangganMaintainSo($scope.ViewData.ListOfSOCustomerInformationView[0]);
                }
        }

        $scope.BtnTutupCreateSO = function() {
            $scope.ViewData = {};
            $scope.infoPembeli_individuFinishUnit = false;
            $scope.infoPemilik_IndividuFinsihUnit = false;
            $scope.infoPembeliPerusahaan_FinsihUnit = false;
            $scope.infoPenanggungJawabPerusahaan_FinishUnit = false;
            $scope.infoPemilikPerusahaan_FinishUnit = false;
            //$scope.filter={SOStatusId:null, SOType:null, SODateStart:null, SODateEnd:null, SpkId:null};
            $scope.CreateSO = false;
            $scope.MaintainForm = true;
            $scope.MaintainSODisableFreePPN = false;
            setDefaultTab();
			// if($scope.KaloLihatSOdariBilling==true)
			// {
				// $rootScope.$emit('KembalikeFormSebelumnya', {});
			// }
			$scope.KaloLihatSOdariBilling=false;
            
        }

        $scope.btntutupLihatFinishUnit = function() {
            $scope.lihatTypeFinishUnit = false;
            $scope.MaintainForm = true;
        }

        $scope.clickhapusrowgrid = function(row) {
            if ($scope.flagStatus == "lihatSO") {
                
				bsNotify.show({
									title: "Peringatan",
									content: "Tidak Dapat Menghapus Data Ketika Lihat SO!",
									type: 'warning'
								});
            } else {
                var index = $scope.gridJasaPengurusanDokumen.data.indexOf(row.entity);
                $scope.gridJasaPengurusanDokumen.data.splice(index, 1);
				$scope.MaintainSOPaksaItungUlangRincian();
            }
        }

        $scope.clickhapusrowgrid1 = function(row) {
			if ($scope.flagStatus == "lihatSO") {
                bsNotify.show({
									title: "Peringatan",
									content: "Tidak Dapat Menghapus Data Ketika Lihat SO!",
									type: 'warning'
								});
            } else {
                // var index = $scope.gridDokumenPembetulanMaintainSo.indexOf(row.entity);
                // $scope.gridDokumenPembetulanMaintainSo.splice(index, 1);
				$scope.gridDokumenPembetulanMaintainSo.splice(row, 1);
            }
        }

        $scope.dataSama = function(){
            if ($scope.ViewData.STNKDataBit && $scope.ViewData.CustomerTypeDesc == 'Individu' && $scope.flagStatus == "UpdateSO") {
                $scope.ViewData.STNKDataBit = true;

                console.log('$scope.ViewData.STNKDataBit',$scope.ViewData.STNKDataBit)
                console.log('$scope.ViewData',$scope.ViewData)
                console.log('kabupaten',$scope.kabupaten)
                console.log('kabupaten1',$scope.kabupaten1)
                console.log('kelurahan',$scope.kelurahan)
                console.log('kelurahan1',$scope.kelurahan1)

                $scope.kabupaten1 = $scope.kabupaten;
                $scope.kecamatan1 = $scope.kecamatan;
                $scope.kelurahan1 = $scope.kelurahan;
                $scope.kodepos1 = $scope.kodepos;

                $scope.ViewData.ListOfSOCustomerInformationView[1].KTPNo = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].KTPNo);
                $scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId);
                $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode);
                $scope.ViewData.ListOfSOCustomerInformationView[1].PhoneNumber = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].PhoneNumber);
                $scope.ViewData.ListOfSOCustomerInformationView[0].CustomerGenderId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerGenderId);
                $scope.ViewData.ListOfSOCustomerInformationView[0].CustomerPositionId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerPositionId);
                // $scope.ViewData.CodeInvoiceTransactionTaxId = angular.copy($scope.ViewData.CodeInvoiceTransactionTaxId);
                
                
                $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerName = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerName);
                $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId);
                $scope.ViewData.ListOfSOCustomerInformationView[1].RT = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].RT);
                $scope.ViewData.ListOfSOCustomerInformationView[1].HpNumber = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].HpNumber);
                $scope.ViewData.ListOfSOCustomerInformationView[0].MaritalStatusId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].MaritalStatusId);              
                $scope.ViewData.ListOfSOCustomerInformationView[0].OfficePhone = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].OfficePhone);               
                // $scope.ViewData.ListOfSOCustomerInformationView[1].FreePPNBit = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].FreePPNBit);

                $scope.ViewData.ListOfSOCustomerInformationView[0].NickName = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].NickName);
                $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId);
                console.log("$scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId",$scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId);
                console.log("$scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId",$scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId);
                $scope.ViewData.ListOfSOCustomerInformationView[1].RW = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].RW);
                $scope.ViewData.ListOfSOCustomerInformationView[0].BirthPlace = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].BirthPlace);    
                $scope.ViewData.ListOfSOCustomerInformationView[0].CustomerReligionId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerReligionId);
                $scope.ViewData.ListOfSOCustomerInformationView[0].OfficeAddress = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].OfficeAddress);   
                // $scope.ViewData.FreePPnBMBit = angular.copy($scope.ViewData.FreePPnBMBit);     

                $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerAddress = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerAddress);
                $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].VillageId);
                console.log("$scope.ViewData.ListOfSOCustomerInformationView[1].VillageId",$scope.ViewData.ListOfSOCustomerInformationView[1].VillageId);
                console.log("$scope.ViewData.ListOfSOCustomerInformationView[0].VillageId",$scope.ViewData.ListOfSOCustomerInformationView[0].VillageId);
                $scope.ViewData.ListOfSOCustomerInformationView[1].Email = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].Email);
                $scope.ViewData.ListOfSOCustomerInformationView[0].BirthDate = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].BirthDate)
                $scope.ViewData.ListOfSOCustomerInformationView[0].Job = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].Job)
                $scope.ViewData.ListOfSOCustomerInformationView[1].NPWPNo = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].NPWPNo)           
                console.log("$scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode",$scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode);
                console.log("$scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode",$scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode);

                // $scope.ViewData.AFIValidDate = angular.copy($scope.ViewData.AFIValidDate); 
                

            } else {
                $scope.ViewData.STNKDataBit = false;
                $scope.ViewData.ListOfSOCustomerInformationView[1].KTPNo = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].CityRegencyId = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].RT = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].HpNumber = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerName = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].DistrictId = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].RW = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].CustomerAddress = "";              
                $scope.ViewData.ListOfSOCustomerInformationView[1].VillageId = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].Email = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].ProvinceId = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].PostalCode = "";
                $scope.ViewData.ListOfSOCustomerInformationView[1].PhoneNumber = "";
            }
            console.log("data sama kelurahan >>",$scope.ViewData.ListOfSOCustomerInformationView[0].VillageId);
            console.log("data sama kode pos >>",$scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode);
        }

        $scope.dataSamaInstansi = function(){
            if ($scope.ViewData.STNKDataBit) {
                $scope.ViewData.STNKDataBit = true;
                
                $scope.kabupaten1 = $scope.kabupaten;
                $scope.kecamatan1 = $scope.kecamatan;
                $scope.kelurahan1 = $scope.kelurahan;
                $scope.kodepos1 = $scope.kodepos;

                $scope.ViewData.ListOfSOCustomerInformationView[2].SIUPNo = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].SIUPNo);
                // $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerAddress = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CustomerAddress);
                $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId= angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].VillageId);
                $scope.ViewData.ListOfSOCustomerInformationView[2].PhoneNumber= angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].PhoneNumber);
                $scope.ViewData.ListOfSOCustomerInformationView[2].TDPNo = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].TDPNo);
                $scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].ProvinceId);
                $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].CityRegencyId);
                $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].PostalCode);
                $scope.ViewData.ListOfSOCustomerInformationView[2].NPWPNo = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].NPWPNo);
                $scope.ViewData.ListOfSOCustomerInformationView[2].RT = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].RT);    
                $scope.ViewData.ListOfSOCustomerInformationView[2].RW = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].RW);  
                $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerName = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].InstanceName);
                $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].DistrictId);

                $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerAddress = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].InstanceAddress); //hanya ada di pemilik          
                // $scope.ViewData.ListOfSOCustomerInformationView[2].InstanceName = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].InstanceName); // hanya ada di pembeli
                // $scope.ViewData.ListOfSOCustomerInformationView[2].InstanceAddress = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].InstanceAddress); // hanya ada di pembeli
                // $scope.ViewData.ListOfSOCustomerInformationView[2].SectorBusinessId = angular.copy($scope.ViewData.ListOfSOCustomerInformationView[0].SectorBusinessId); //hanya ada di pembeli
            } else {
                $scope.ViewData.STNKDataBit = false;
                $scope.ViewData.ListOfSOCustomerInformationView[2].SIUPNo = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerAddress = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].VillageId = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].PhoneNumber = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].TDPNo = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].ProvinceId = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].PostalCode = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].NPWPNo = "";              
                $scope.ViewData.ListOfSOCustomerInformationView[2].CityRegencyId = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].RT = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].CustomerName = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].DistrictId = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].RW = "";
                $scope.ViewData.ListOfSOCustomerInformationView[2].KTPNo = "";
            }
        }

        $scope.clickhapusrowgrid2 = function(row) {
            if ($scope.flagStatus == "lihatSO") {
                bsNotify.show({
									title: "Peringatan",
									content: "Tidak Dapat Menghapus Data Ketika Lihat SO!",
									type: 'warning'
								});
            } else {
                var index = $scope.gridAksesoris.data.indexOf(row.entity);
                $scope.gridAksesoris.data.splice(index, 1);
				$scope.MaintainSOPaksaItungUlangRincian();
            }
        }

        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.BatalSOModal = function(SelectData) {
            $scope.item = SelectData;
            $scope.item.SOCancelReasonId = "";
            $scope.item.NoteSOCancelReason = "";
            $scope.tmpInformation = "";
            //$scope.show_modal.show = !$scope.show_modal.show;
			
			setTimeout(function() {     
				//angular.element('.ui.modal.pilihNoRangkaBiroJasa').modal('hide');
				angular.element('.ui.modal.ModalMaintainSoBatal').modal('refresh'); 
			}, 0);
				
			setTimeout(function() {
				angular.element('.ui.modal.ModalMaintainSoBatal').modal('setting',{closable:false}).modal('show');
				angular.element('.ui.modal.ModalMaintainSoBatal').not(':first').remove();
			}, 1);

            var getBillingStat = "?SOId=" + $scope.item.SOId;
            MaintainSOFactory.getStatusBilling(getBillingStat).then(
                function(res) {
                    $scope.getStatBill = res.data.Result;
                    $scope.tmpInformation = res.data.Result[0].Information;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }
		
		$scope.MaintainSoBatal_Simpan_Awal = function() {
            
			if($scope.item.StatusMatchId==2)
			{
				//ini berarti match
				setTimeout(function() {
				  angular.element('.ui.modal.ModalMaintainSoBatalKaloMatching').modal('setting',{closable:false}).modal('show');
					angular.element('.ui.modal.ModalMaintainSoBatalKaloMatching').not(':first').remove();  
				}, 1);
			}
			else
			{
			// angular.element('.ui.modal.KonfirmasiBatalSO').modal('setting',{closable:false}).modal('show');
                angular.element('.ui.modal.ModalMaintainSoBatal').modal('setting',{closable:false}).modal('hide');
                $scope.MaintainSoBatal_Simpan();
			}
		}

        // $scope.KonfirmasiBatalSO = function(){
        //     angular.element('.ui.modal.KonfirmasiBatalSO').modal('setting',{closable:false}).modal('hide');
        // angular.element('.ui.modal.ModalMaintainSoBatal').modal('setting',{closable:false}).modal('hide');
        // }
		
		$scope.MaintainSoBatalKembaliLagi = function() {
			angular.element('.ui.modal.ModalMaintainSoBatalKaloMatching').modal('hide');
		}

        $scope.MaintainSoBatal_Simpan = function() {
            //$scope.show_modal = { show: false };
			// angular.element('.ui.modal.KonfirmasiBatalSO').modal('hide');
			// angular.element('.ui.modal.ModalMaintainSoBatalKaloMatching').modal('hide');
            if ($scope.getStatBill.BillingStatusId == 1 ||
                $scope.getStatBill.BillingStatusId == 2 ||
                $scope.getStatBill.BillingStatusId == 4) {
                
				bsNotify.show(
										{
											title: "Peringatan",
											content: "Tidak dapat membatalkan SO yang sudah dibuat Billing",
											type: 'warning'
										}
									);
            } else {
                MaintainSOFactory.batalSO($scope.item).then(
                    function() {
                        //$scope.show_modal = { show: false };
						angular.element('.ui.modal.KonfirmasiBatalSO').modal('hide');
						angular.element('.ui.modal.ModalMaintainSoBatalKaloMatching').modal('hide');
                        // bsNotify.show({
                            // size: 'big',
                            // type: 'succes',
                            // timeout: 3000,
                            // color: "#c00",
                            // title: "SO berhasil dibatalkan"
                        // });
						bsNotify.show(
										{
											title: "Berhasil",
											content: "SO berhasil dibatalkan",
											type: 'success'
										}
									);
                        MaintainSOFactory.getData($scope.filter).then(
                            function(res) {
                                $scope.grid.data = res.data.Result;
                                for (var j in $scope.grid.data) {
                                    if ($scope.grid.data[j].ProspectName == null) {
                                        $scope.grid.data[j].ProspectName = $scope.grid.data[j].Name;
                                    }
                                }
                                $scope.loading = false;
                                return res.data.Result;
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    },
                    function(err) {
                        var ErrMSG = "";
                        if(err.data.Message.split('#'). length > 1){
                            ErrMSG = err.data.Message.split('#')[1];
                            ErrMSG = ErrMSG.split('Error')[0];
                        }else{
                            ErrMSG = err.data.Message;
                        }
						bsNotify.show(
										{
											title: "Gagal",
											content: ErrMSG,
											type: 'danger'
										}
									);
                    }
                );
            }
        }
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalMaintainSoBatal').remove();
          angular.element('.ui.modal.KonfirmasiBatalSO').remove();
		  angular.element('.ui.modal.ModalMaintainSoBatalKaloMatching').remove();
		  angular.element('.ui.modal.modalAccPackageDetailMaintainSo').remove();
		  //ini 1 dulu kalo lemes uda dibikin pakein juga yg laen
		});

        $scope.MaintainSoBatal_Cancel = function() {
            //$scope.show_modal = { show: false };
			angular.element('.ui.modal.ModalMaintainSoBatal').modal('hide');
        }

        $scope.btnYaLepasNoRangka = function(item) {
            angular.element('.ui.small.modal.NotifikasiLepasNoRangka').modal('hide');
            angular.element('.ui.small.modal.NotifikasibatalSPK').modal('show');
        }

        $scope.btnYaBatalSPK = function(item) {
            angular.element('.ui.small.modal.NotifikasibatalSPK').modal('hide');
        }

        $scope.btnNoBatalSPK = function(item) {
            angular.element('.ui.small.modal.NotifikasiLepasNoRangka').modal('hide');
        }
		
		$scope.MaintainSoCentangAksesorisPasDiModal = function(barang) {
			if(barang.GeneratePR==false)
			{
				$scope.gridDataAksesoris.data[$scope.gridDataAksesoris.data.indexOf(barang)].GeneratePR = true;
			}
			else if(barang.GeneratePR==true)
			{
				$scope.gridDataAksesoris.data[$scope.gridDataAksesoris.data.indexOf(barang)].GeneratePR = false;
			}
		}
		
		$scope.MaintainSoCentangKaroseriPasDiModal = function(barang) {
			if(barang.GeneratePR==false)
			{
				$scope.gridDataKaroseri.data[$scope.gridDataKaroseri.data.indexOf(barang)].GeneratePR = true;
			}
			else if(barang.GeneratePR==true)
			{
				$scope.gridDataKaroseri.data[$scope.gridDataKaroseri.data.indexOf(barang)].GeneratePR = false;
			}
		}
		
		$scope.MaintainSoCentangAksesorisAbisDariModal = function(barang) {
			if(barang.GeneratePR==false)
			{
				$scope.gridAksesoris.data[$scope.gridAksesoris.data.indexOf(barang)].GeneratePR = true;
			}
			else if(barang.GeneratePR==true)
			{
				$scope.gridAksesoris.data[$scope.gridAksesoris.data.indexOf(barang)].GeneratePR = false;
			}
        }

        $scope.onListSaveInfoUnit = function(item) {
        }

        $scope.status = false;

        $scope.viewDocument = function(row) {      
            MaintainSOFactory.getImage(row.DocumentURL).then(function(res) {                
                $scope.viewdocument = res.data.UpDocObj;               
                $scope.viewdocumentName = res.data.FileName;               
                $scope.status = true;                
                callModal();                  
            })
        }
		
		$scope.detailaccpacketMaintainSo = function (data) {
            var number = angular.element('.ui.modal.modalAccPackageDetailMaintainSo').length;
            setTimeout(function () {
                angular.element('.ui.modal.modalAccPackageDetailMaintainSo').modal('refresh');
            }, 0);
            if (number > 0) {
                angular.element('.ui.modal.modalAccPackageDetailMaintainSo').not(':first').remove();
            }
            $scope.gridAksesorisDetailMaintainSo.data = data.ListDetail;
            angular.element('.ui.modal.modalAccPackageDetailMaintainSo').modal('show');
        }
		
		$scope.modalAccPackageDetailMaintainSo = function () {
            angular.element('.ui.modal.modalAccPackageDetailMaintainSo').modal('hide');
        }

        function callModal() {
            var number = angular.element('.ui.modal.dokumenViewSO').length;
            setTimeout(function () {
                angular.element('.ui.modal.dokumenViewSO').modal('refresh');
            }, 0);
            if (number > 0) {
                angular.element('.ui.modal.dokumenViewSO').not(':first').remove();
            }
            angular.element('.ui.modal.dokumenViewSO').modal('show');
            // angular.element('.ui.modal.dokumenViewSO').modal({ closable: false }).modal('show');
        }

        $scope.keluarImg = function() {
                $scope.status = false;
                angular.element('.ui.modal.dokumenViewSO').modal('hide');
            }
            /* End Test Modal */


        $scope.onSelectRows = function(rows) {
            $scope.selected_rows = rows;
        }

        ModelFactory.getData().then(function(res) {
            $scope.optionsModel = res.data.Result;
            return $scope.optionsModel;
        });

        TipeFactory.getData().then(function(res) {
            $scope.RawTipe = res.data.Result;
            return $scope.RawTipe;
        });

        $scope.filtertipe = function(selected) {
                       
            $scope.vehicleTypefilter = $scope.RawTipe.filter(function(type) { return (type.VehicleModelId == selected.VehicleModelId); })
        }

        $scope.SearchJasa = function () {
            $scope.ListMasterBiroJasa();
        }
        

        $scope.filterColor = function(selected) {
            $scope.SelectedTipe = {};
            $scope.SelectedTipe.VehicleTypeId = selected.VehicleTypeId;
                            
         }
		 
		 
		 
		 
		 
		// $scope.filterVendorMaintainSo = function(selected) {
            // $scope.selectedVendorMaintainSo = {};
            // $scope.selectedVendorMaintainSo = selected;
                            
         // } 


        //ng-hide : row.entity.StatusMatchId == 2 || row.entity.StatusMatchId == 3 ||row.entity.SOStatusId != 1 || row.entity.SOTypeName==\'Swapping\'
        //ng-if :          
        var btnAction = '<i title="Lihat" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.lihatSO(row.entity.SOId)"></i>	<i title="Ubah" class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="!grid.appScope.$parent.RightsEnableForMenu.allowEdit || row.entity.StatusMatchId == 3 ||row.entity.SOStatusId != 1 || row.entity.SOTypeName==\'Swapping\'" ng-click="grid.appScope.$parent.btnUbahSO(row.entity)"></i><i title="Batal SO" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-if="row.entity.SOStatusId ==\'1\' && row.entity.SwappingNonDMSBit == 0" ng-hide="!grid.appScope.$parent.RightsEnableForMenu.allowDelete" ng-click="grid.appScope.$parent.BatalSOModal(row.entity)" ></i>'

        var viewClick = '<span>\
        <p style="padding:5px 0 0 5px"><u ng-if="row.entity.StatusDocumentName ==\'Empty\' "><button  style="float:left;margin-left:5px"onClick="document.getElementById(\'pickfiles-nav1\').click()"> Browse <input id="pickfiles-nav1"  type="file" maxbyte="10" formattype="[\'gif\',\'jpeg\',\'jpg\',\'png\',\'img\']" bs-uploadsingle="onFileSelect($files)" img-upload="grid.appScope.gridDokumen.data[grid.appScope.gridDokumen.data.indexOf(row.entity)].DocumentData" style="display:none"></button>  <label ng-show="row.entity.DocumentData.FileName!=null && row.entity.DocumentData.FileName!=undefined">{{row.entity.DocumentData.FileName}}</label></u>&nbsp<u ng-if="row.entity.StatusDocumentName ==\'Upload\'"\
            ng-click="grid.appScope.viewDocument(row.entity)" style="color:blue">View</u></p></span>'

        var HapusClick = '<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.flagStatus==\'lihatSO\'" ng-click="grid.appScope.clickhapusrowgrid(row)" ></i>'
        var HapusClick1 = '<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.flagStatus==\'lihatSO\'" ng-click="grid.appScope.clickhapusrowgrid1(row)" ></i>'
        var HapusClick2 = '<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.flagStatus==\'lihatSO\'" ng-click="grid.appScope.clickhapusrowgrid2(row)" ></i>'
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            //paginationPageSize: 10,
            columnDefs: [
                { name: 'SO id', field: 'SoId', width: '7%', visible: false },
                { displayName:'No. SO' ,name: 'No. SO', field: 'SoCode', width: '15%' },
                { displayName:'Tanggal SO' ,name: 'Tanggal SO', field: 'SoDate', cellFilter: 'date:\'dd-MM-yyyy\'', width: '9%' },
                { displayName:'Tipe SO' ,name: 'Tipe SO', field: 'SOTypeName', width: '8%' },
                { displayName:'Status SO' ,name: 'Status SO', field: 'SOStatusName', width: '7%' },
                { displayName:'No. SPK' ,name: 'No. SPK', field: 'FormSPKNo', width: '15%' },
                { name: 'Kode Prospek', field: 'ProspectCode', width: '11%' },
                { name: 'Nama Prospek', field: 'ProspectName', width: '18%' },
				{ name: 'Sales', field:'Salesman', width: '18%'},
				{ name: 'Supervisor', field: 'Supervisor', width:'18%'},
				{ name: 'DP', field:'TotalAmountDP', width:'18%'},
                {
                    name: 'action',
                    allowCellFocus: false,
					enableFiltering: false,
                    width: '14%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnAction
                }
            ]
        };
		
		$scope.MaintainSoBisaEditgridJasaPengurusanDokumen = function () 
		{ 
			if($scope.flagStatus == "lihatSO" )
			{
				return false; 
			}
			else
			{
				return true; 
			}
			
		};

        $scope.gridJasaPengurusanDokumen = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'dataJasaPengurusanDokumen',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'ServiceId.', field: 'ServiceId', visible: false },
                { name: 'No', field: 'ItemNo', visible: false },
                { name: 'Kode Jasa', field: 'ServiceCode' , enableCellEdit: false,enableCellEditOnFocus:false },
                { name: 'Nama Jasa', field: 'ServiceName' , enableCellEdit: false,enableCellEditOnFocus:false },
				{ name: 'Model Tipe', field: 'VehicleTypeDesc' , enableCellEdit: false,enableCellEditOnFocus:false },
				{ name: 'No PR', field: 'PRCode' , enableCellEdit: false,enableCellEditOnFocus:false },
				{ name: 'Status PR', field: 'StatusPRName' , enableCellEdit: false,enableCellEditOnFocus:false },
                { name: 'Harga Jasa', cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {return 'canEdit';}, 
                    field: 'ServicePrice', cellFilter: 'currency:"Rp."', enableCellEdit: true,enableCellEditOnFocus:true ,cellEditableCondition: $scope.MaintainSoBisaEditgridJasaPengurusanDokumen},
                {
                    name: 'Qty',
                    field: 'Qty',
                    format: '{0:c}',
                    type: 'number',
                    visible: false
                },
                {
                    name: 'Biaya Admin',
                    field: 'AdminFee',
                    format: '{0:c}',
                    type: 'number',
                    visible: false
                },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '8%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: HapusClick
                }
            ]
        };
		
		$scope.gridJasaPengurusanDokumen.onRegisterApi = function(gridApi) {
            $scope.gridApiJasaPengurusanDokumen = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedRowJasaPengurusanDokumen = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectedRowJasaPengurusanDokumen = gridApi.selection.getSelectedRows();
            });

            $scope.gridApiJasaPengurusanDokumen.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                console.log('newValue ==>', newValue);
				if (newValue < 0) 
				{
                    
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai harga jasa tidak boleh kurang dari 0.",
                        type: 'warning'
                    });
                }
				
				$scope.ViewData.PPN = 0;
				$scope.ViewData.TotalDpp = 0;
				$scope.ViewData.GrandTotal = 0;
				$scope.ViewData.TotalVAT=0;
					
				var DaMaintainSoFreePpnThingy1=0;
				if($scope.ViewData.FreePPNBit==true)
				{
					DaMaintainSoFreePpnThingy1=1;
				}
				else
				{
					DaMaintainSoFreePpnThingy1=0;
				}
				
				
				angular.forEach($scope.blabla, function(acc, accIdx) {
				
				
				var param="";
				try
				{
					param = {
						"Title": "Order Pengurusan Dokumen",
						"HeaderInputs": {
							"$ServiceId$": acc.ServiceId,
							"$OutletId$": acc.OutletId,
							"$QTY$": acc.Qty,
							"$AdminFee$": acc.AdminFee,
							"$FreePPNBit$": DaMaintainSoFreePpnThingy1,
							"$ServicePriceInput$": $scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice,
							"$VehicleTypeId$": acc.VehicleTypeId
						}
						//,"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
						//tadi yg service price ga ada

					};
				}
				catch(eeee)
				{
					param = {
						"Title": "Order Pengurusan Dokumen",
						"HeaderInputs": {
							"$ServiceId$": acc.ServiceId,
							"$OutletId$": acc.OutletId,
							"$QTY$": acc.Qty,
							"$AdminFee$": acc.AdminFee,
							"$FreePPNBit$": DaMaintainSoFreePpnThingy1,
							"$ServicePriceInput$": $scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice,
							"$VehicleTypeId$": null
						}
						//,"$VehicleTypeId$": $scope.resultFrame.VehicleTypeId
						//tadi yg service price ga ada

					};
				}
				
                $scope.JumlahSementara = 0;
                $http.post('/api/fe/PricingEngine?PricingId=30001&OutletId=' + $scope.user.OutletId + '&DisplayId=' + $scope.user.OutletId + '30001', param)
                    .then(function(res) {
                        $scope.blabla[accIdx].ServicePrice = res.data.Codes["#ServicePrice#"];
                        $scope.blabla[accIdx].AdminFee = res.data.Codes["$AdminFee$"];
                        $scope.blabla[accIdx].QTY = res.data.Codes["[QTY]"];
                        $scope.blabla[accIdx].DPP = res.data.Codes["[DPP]"];
                        $scope.blabla[accIdx].VAT = res.data.Codes["[PPN]"];
                        $scope.blabla[accIdx].PriceAfterTax = res.data.Codes["[PriceAfterTax]"];
                        $scope.blabla[accIdx].SubTotal = res.data.Codes["[SubTotal]"];
                        $scope.blabla[accIdx].Total = res.data.Codes["[Total]"];
                        $scope.blabla[accIdx].Selisih = res.data.Codes["[Selisih]"];
						//dhemit
						$scope.gridJasaPengurusanDokumen.data[accIdx].ServicePrice=angular.copy(res.data.Codes["[HargaService]"]);
						
						

                        $scope.ViewData.PPN = $scope.ViewData.PPN + parseInt(res.data.Codes["[PPN]"]);
                        // $scope.ViewData.TotalDpp = $scope.ViewData.TotalDpp + parseInt(res.data.Codes["[DPP]"]);
                        $scope.ViewData.TotalDpp = $scope.ViewData.TotalDpp + parseInt(res.data.Codes["[DPP]"] * parseInt(res.data.Codes["[QTY]"]));

                        // $scope.ViewData.GrandTotal = $scope.ViewData.GrandTotal + parseInt(res.data.Codes["[Total]"]);
                        // $scope.ViewData.TotalVAT= $scope.ViewData.TotalVAT + parseInt(res.data.Codes["[PPN]"]);
                        $scope.ViewData.TampungTotalDpp = $scope.ViewData.TotalDpp;
                        // $scope.ViewData.TampungTotalDpp = parseInt(res.data.Codes["[QTY]"]) * $scope.ViewData.TotalDpp;
                        // $scope.ViewData.TampungTotalVAT = parseInt(res.data.Codes["[QTY]"]) * $scope.ViewData.TotalVAT;
                        // Rounding CR4
                        // $scope.ViewData.TampungTotalVAT = Math.floor(0.1 * $scope.ViewData.TampungTotalDpp) ;
                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.TampungTotalVAT = 0 ;
                        }else{
                            // $scope.ViewData.TampungTotalVAT = Math.floor(0.1 * $scope.ViewData.TampungTotalDpp) ; 
                            $scope.ViewData.TampungTotalVAT = Math.floor((PPNPerc/100) * $scope.ViewData.TampungTotalDpp) ; 

                        }
                        $scope.ViewData.GrandTotal =  $scope.ViewData.TampungTotalVAT + $scope.ViewData.TampungTotalDpp;
                        $scope.ViewData.TotalVAT= $scope.ViewData.TampungTotalVAT;
                        
                        // $scope.ViewData.Pembulatan = $scope.ViewData.GrandTotal - $scope.ViewData.TampungTotalDpp - $scope.ViewData.TampungTotalVAT;
                        $scope.jumlahHarga = 0;
                        $scope.jumlahHarga = (parseInt(res.data.Codes["[HargaService]"] * parseInt(res.data.Codes["[QTY]"])));
                        console.log('$scope.jumlahHarga',$scope.jumlahHarga);
                        $scope.JumlahSementara = $scope.JumlahSementara + angular.copy($scope.jumlahHarga);
                         
                        console.log('$scope.JumlahSementara',$scope.JumlahSementara);

                        $scope.ViewData.TampungHarga = $scope.JumlahSementara;
                        console.log('$scope.ViewData.TampungHarga1',$scope.ViewData.TampungHarga);   
                        // $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        if ($scope.ViewData.FreePPNBit == true) {
                            $scope.ViewData.Pembulatan = 0;
                        }else{
                            $scope.ViewData.Pembulatan = Math.abs( $scope.ViewData.GrandTotal -  $scope.ViewData.TampungHarga) ;
                        } 
						
                    });
            });
			
					
					
			});
				


        };

        $scope.gridDataJBiroJasaSO = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'ServiceId', field: 'ServiceId', visible: false },
                { name: 'Kode Jasa', field: 'ServiceCode', enableCellEdit: false },
                { name: 'Nama Jasa', field: 'ServiceName', enableCellEdit: false },
                // { name: 'Kode Jasa', field: 'ServiceCode', width: '25%', enableCellEdit: false },
                // { name: 'Nama Jasa', field: 'ServiceName', width: '45%', enableCellEdit: false },
				{
                    name: 'Qty',
                    field: 'Qty',
                    format: '{0:c}',
                    type: 'number',
                    enableCellEdit: true,
                    width: '10%'
                },
                
                {
                    name: 'Biaya Admin',
                    field: 'AdminFee',
                    format: '{0:c}',
                    type: 'number',
                    enableCellEdit: true,
                    width: '15%',
                    visible: false
                },
            ]
        };

        $scope.gridDataJBiroJasaSO.onRegisterApi = function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

		$scope.columnDefsGridAksesoris=[
                { name: 'Item No', field: 'ItemNo', visible: false },
                { name: 'MaterialId', field: 'MaterialId', visible: false },
                { name: 'PartsId', field: 'AccessoriesId', width: '20%', visible: false },
                { name: 'No Material', field: 'AccessoriesCode', width: '8%' },
                { name: 'Nama Material', field: 'AccessoriesName', width: '25%'},
                { name: 'Qty', field: 'Qty', format: '{0:c}', type: 'number', width: '4%', visible: true },
                { name: 'Satuan', field: 'UomName', width: '6%'},
            {
                name: 'Harga Satuan', field: 'RetailPrice', cellClass: 'text-right-MaintainSo', width: '10%', cellFilter: 'currency:"Rp."',
					treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    } 
				},
                { displayName: 'No PR', name: 'No PR', field: 'PRCode', width: '18%'},
                { name: 'Status PR', field: 'StatusPRName' ,width: '7%'},
                {
                    name: 'Diskon per Unit',
                    field: 'Discount',
                    cellClass: 'text-right-MaintainSo',cellFilter: 'currency:"Rp."',
					treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    } ,
                    //format: '{0:c}',
                    type: 'number',
                    width: '11%',
                    enableCellEdit: true
                },
				{ name: 'Generate PR', displayName: 'Generate PR', field: 'GeneratePR',enableCellEdit: false, cellTemplate: '<p align="center"><input ng-if="!row.groupHeader" ng-disabled="grid.appScope.flagStatus==\'lihatSO\'" class="ui-grid-cell-contents" type="checkbox" ng-checked="row.entity.GeneratePR == true" ng-click="grid.appScope.MaintainSoCentangAksesorisAbisDariModal(row.entity)"></p>', visible: true  },
                {
                    
                    name: 'Action',
                    allowCellFocus: false,
                    width: '6%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: '<i title="Hapus" class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-hide="grid.appScope.flagStatus==\'lihatSO\'" ng-click="grid.appScope.clickhapusrowgrid2(row)" ></i>'
                }
            ];
		
        $scope.gridAksesoris = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
			enableColumnResizing: true,
            //data: 'dataInfoAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: $scope.columnDefsGridAksesoris
        };
		
		$scope.gridAksesoris.onRegisterApi = function(gridApi) {
            $scope.gridApiAksesorisDepanMaintainSo = gridApi;
            
        };

        $scope.gridDataAksesoris = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'PartsId', field: 'PartsId', visible: false },
                { name: 'Kode Material', field: 'PartsCode', width: '11%', enableCellEdit: true },
                { name: 'Nama Material', field: 'PartsName', enableCellEdit: false },
                { name: 'Klasifikasi', field: 'Classification', width: '9%',enableCellEdit: false },
                {
                    name: 'Qty',
                    field: 'Qty',
                    width: '5%',
                    format: '{0:c}',
                    type: 'number',
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return 'canEdit'; },
                    enableCellEdit: true
                },
                { name: 'Satuan', field: 'UomName', width: '6%', enableCellEdit: false },
                {
                    name: 'Harga Satuan', width: '11%', field: 'RetailPrice',
                    cellClass: 'text-right-MaintainSo', cellFilter: 'currency:"Rp."',
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function (aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                {
                    name: 'Diskon per Unit',
                    field: 'Discount',
                    cellClass: 'text-right-MaintainSo', width: '11%', cellFilter: 'currency:"Rp."',
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function (aggregation) {
                        aggregation.rendered = aggregation.value;
                    },
                    //format: '{0:c}',
                    type: 'number',
                    enableCellEditOnFocus: true,
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) { return 'canEdit'; },
                    enableCellEdit: true
                },
                { name: 'Generate PR', field: 'GeneratePR', width: '8%', enableCellEdit: false, cellTemplate: '<p align="center"><input ng-if="!row.groupHeader" class="ui-grid-cell-contents" type="checkbox" ng-checked="row.entity.GeneratePR == true" ng-click="grid.appScope.MaintainSoCentangAksesorisPasDiModal(row.entity)"></p>' },
            ]
        };

        $scope.gridDataAksesoris.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });

            $scope.gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                console.log('new value===> ',newValue);
                console.log('rowEntity ===>', rowEntity);
                if (newValue > rowEntity.RetailPrice) {
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai harga diskon per-Unit tidak boleh melebihi Harga satuan.",
                        type: 'warning'
                    });

                    rowEntity.Discount = 0;
                }

                
            });


        };

        

        $scope.gridDataKaroseri = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'KaroseriId', field: 'KaroseriId', width: '20%', visible: false },
                { name: 'No Karoseri', field: 'KaroseriCode', width: '20%' },
                { name: 'Nama Karoseri', field: 'KaroseriName' },
                { name: 'Price', field: 'Price' },
                {
                    name: 'Qty',
                    field: 'Qty',
                    format: '{0:c}',
                    type: 'number',
                    enableCellEdit: true
                },
				{ name: 'Generate PR', field: 'GeneratePR', enableCellEdit: false , cellTemplate: '<p align="center"><input ng-if="!row.groupHeader" class="ui-grid-cell-contents" type="checkbox" ng-checked="row.entity.GeneratePR == true" ng-click="grid.appScope.MaintainSoCentangKaroseriPasDiModal(row.entity)"></p>'},
            ]
        };

        $scope.gridDataKaroseri.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.KaroseriRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.KaroseriRow = gridApi.selection.getSelectedRows();
            });
            
        };

        // $scope.gridDokumen = {
            // enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: false,
            // enableSelectAll: true,
            // //data: 'dataInfoDokumen',
            // //showTreeExpandNoChildren: true,
            // paginationPageSizes: [10, 25, 50],
            // paginationPageSize: 10,
            // columnDefs: [
                // { name: 'No', field: 'No', width: '7%', visible: false },
                // { name: 'Tipe Dokumen', field: 'SODetailDocumentId', visible: false },
                // { name: 'Document Id', field: 'DocumentId', visible: false },
                // { name: 'Tipe Dokumen', field: 'DocumentType', width: '15%' },
                // {
                    // name: 'Dokumen',
                    // field: 'DocumentURL',
                    // allowCellFocus: false,
                    // enableColumnMenu: false,
                    // enableSorting: false,
                    // enableColumnResizing: true,
                    // cellTemplate: viewClick
                // },
                // {
                    // name: 'Status',
                    // field: 'StatusDocumentName',
                    // width: '10%',
					// cellTemplate: '<div ng-if="row.entity.DocumentData==null && row.entity.StatusDocumentName !=\'Upload\'""><p style="padding:5px 0 0 5px">Empty</p></div><div ng-if="row.entity.DocumentData!=null"><p style="padding:5px 0 0 5px">Upload</p></div><div ng-if="row.entity.StatusDocumentName ==\'Upload\'"><p style="padding:5px 0 0 5px">Upload</p></div>'
                // },
                // {
                    // name: 'Action',
                    // allowCellFocus: false,
                    // width: '10%',
                    // enableColumnMenu: false,
                    // enableSorting: false,
                    // enableColumnResizing: true,
                    // cellTemplate: HapusClick1
                // }
            // ]
        // };

        // $scope.gridDokumen.onRegisterApi = function(gridApi) {
            // $scope.gridApi = gridApi;
            // gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                // $scope.selectRowgridDokumen = gridApi.selection.getSelectedRows();
            // });
            // gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                // $scope.selectRowgridDokumen = gridApi.selection.getSelectedRows();
            // });
        // };

        $scope.gridDataDokumen = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'ListMasterDokumen',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Tipe Dokumen', field: 'DocumentId', visible: false },
                { name: 'Tipe Dokumen', field: 'DocumentType', width: '20%' },
                { name: 'Dokumen', field: 'DocumentName' }
            ]
        };

        $scope.gridDataDokumen.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridRincianHarga = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            data: 'dataInfoRincianHarga',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Nama Jasa', field: 'ServiceBureauName' },
                { name: 'Nama Material', field: 'PartsName' },
                { name: 'Keterangan', field: 'Keterangan' },
                { name: 'Nominal', field: 'Nominal' }
            ]
        };
		
		var accpackage = '<i ng-if="!row.groupHeader" title="Lihat Detil" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.detailaccpacketMaintainSo(row.entity)" ></i>';
		
		$scope.gridAksesorisPackageMaintainSo = {
            multiSelect: false,
            //enableSelectAll: false,
            enableColumnMenus: true,
            //enableRowSelection: false,
            enableSorting: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            //data: 'dataAksesoris',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                {
                    name: 'Tipe model',
                    field: 'Description',
                    width: '40%',
                    pinnedLeft: true,
                    grouping: { groupPriority: 0 },
                    sort: { priority: 0, direction: 'asc' },
                    cellTemplate: '<div ng-if="!col.grouping || col.grouping.groupPriority === undefined || col.grouping.groupPriority === null || ( row.groupHeader && col.grouping.groupPriority === row.treeLevel )" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'
                },
                { name: 'No Material', field: 'AccessoriesPackageCode' },
                { name: 'Nama Material', field: 'AccessoriesPackageName' },
                { name: 'qty', field: 'Qty', width: '10%' },
                //  { name: 'status', field: 'StatusAccName' },
                {
                    name: 'harga',
                    cellFilter: 'currency:"Rp."',
                    field: 'Price',
                    width: '20%',
                    treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
                    customTreeAggregationFinalizerFn: function(aggregation) {
                        aggregation.rendered = aggregation.value;
                    }
                },
                {
                    name: 'Detil Aksesoris',
                    cellTemplate: accpackage

                }
            ],
        };
		
		$scope.gridAksesorisDetailMaintainSo = {
            // enableSorting: true,
            enableRowSelection: true,
            enableColumnMenus: true,
            enableSorting: true,
            enableColumnResizing: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            //data: 'dataAksesoris',
            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { name: 'material number', field: 'AccessoriesCode' },
                { name: 'material name', field: 'AccessoriesName' },
                {
                    name: 'harga',
                    cellFilter: 'currency:"Rp."',
                    field: 'Price', attributes: '{style:"text-align:right;"}'
                }
            ],
        };
		
    });

app.directive('imageSetAspect', function($scope) {
    return {
        scope: false,
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.bind('load', function() {
                // Use jquery on 'element' to grab image and get dimensions.
                scope.$apply(function() {
                    $scope.viewdocument.width = $(element).width();
                    $scope.viewdocument.height = $(element).height();
                });
            });
        }
    };
});