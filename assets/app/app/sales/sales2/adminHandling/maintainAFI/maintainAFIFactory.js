angular.module('app')
  .factory('MaintainAFIFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
       var data = [
           {
                "NoSPK":23674234,
                "NoSO":"2010940292016",
                "NoRangka":"19 10 2016",
                "NamaPelanggan":"LONO",
                "ModelKendaraan":"Credit",
                "Status":"Belum Aju AFI"  
            },
            {
                "NoSPK":23674234,
                "NoSO":"2010940292016",
                "NoRangka":"19 10 2016",
                "NamaPelanggan":"LONO",
                "ModelKendaraan":"Credit",
                "Status":"Belum Aju AFI"  
            },
            {
                "NoSPK":23674234,
                "NoSO":"2010940292016",
                "NoRangka":"19 10 2016",
                "NamaPelanggan":"LONO",
                "ModelKendaraan":"Credit",
                "Status":"Belum Aju AFI"                    
            },
        ];
        res = data;
        return res;
      },
      create: function(MaintainAFIFactory) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: MaintainAFIFactory.SalesProgramName}]);
      },
      update: function(MaintainAFIFactory){
        return $http.put('/api/fw/Role', [{
                                            SalesProgramId: MaintainAFIFactory.SalesProgramId,
                                            SalesProgramName: MaintainAFIFactory.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd