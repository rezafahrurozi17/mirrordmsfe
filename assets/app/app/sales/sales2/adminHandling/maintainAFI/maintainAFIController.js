angular.module('app')
.controller('MaintainAFIController', function($scope, $http, CurrentUser, MaintainAFIFactory,$timeout) {
    
    var listData = [{
                Id: "1",
                Name: "Belum aju AFI"
            },
            {
                Id: "2",
                Name: "Aju AFI ke HO/CAO"
            },
            {
                Id: "3",
                Name: "Aju AFI ke TAM"
            }

        ];

    $scope.selectionItemStatus = listData;

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mMaintainAFI = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------

    var gridData = [];
    $scope.getData = function() {
        $scope.grid.data=MaintainAFIFactory.getData();         
        $scope.loading=false;
    },

    
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            // { name:'No SPK/PO No',    field:'NoSPK', width:'7%', visible:false },
            { name:'No SPK', field:'NoSPK'},
            { name:'No SO', field:'NoSO'},
			{ name:'NO Rangka', field: 'NoRangka' },
			{ name:'Nama Pelanggan',  field: 'NamaPelanggan' },
			{ name:'Type Model Kendaraan',  field: 'ModelKendaraan' },
            { name: 'Status ', field: 'Status'}
        ]
    };
});
