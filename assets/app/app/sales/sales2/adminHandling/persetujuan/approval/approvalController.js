angular.module('app')
    .controller('ApprovalController', function($scope, bsNotify, $http, $httpParamSerializer, CurrentUser, ApprovalFactory, $timeout, MobileHandling) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.Approve = true;
            $scope.gridData = [];
			$scope.ApprovalAllCanApproveGotCannot=true;
            //----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mapproval = null; //Model
        $scope.filter = {ApprovalCategoryId : null};
        $scope.disabledBtnApproval = false;

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function(){
            ApprovalFactory.getData($scope.filter).then(
                function(res){
                    $scope.grid.data = res.data.Result;
                    $scope.loading=false;
					$scope.ApprovalAllCanApproveGotCannot=true;
                    return res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
					$scope.ApprovalAllCanApproveGotCannot=true;
                }
            );
        }

        var getPersetujuan = "?ApprovalCategoryCode=" + "AH";
        ApprovalFactory.getDataCategory(getPersetujuan).then(
            function(res){
                $scope.DataCategory = res.data.Result;
                return res.data;
            }
        );

        $scope.btnApproval = function(){
            $scope.disabledBtnApproval = true;
            ApprovalFactory.Approval($scope.selected_rows).then(
                function(res){
                    ApprovalFactory.getData().then(
                        function(res){
                            $scope.getData();
                            console.log("it's True");

							bsNotify.show(
								{
									title: "Berhasil",
									content: "Data Berhasil di Approve.",
									type: 'success'
								}
                            );
                            $scope.disabledBtnApproval = false;
                        }
                    );
                },
                function(err){
                    console.log("err=>",err.data);
                    
					
					bsNotify.show({
                            title: "Gagal",
                            content: "Error Message :"+err.data.Message,
                            type: 'danger'
                        });
                        $scope.disabledBtnApproval = false;
                }
            );
        }

        $scope.btnReject = function(){
            angular.element('.ui.modal.reject').modal('show');
        }

        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.mapproval = {};
        $scope.saveReject = function(item){
            
            $scope.selected_rows[0].RejectReason = item.RejectReason;
            console.log('$scope.selected_rows',$scope.selected_rows);
            console.log('$scope.selected_rows',$scope.selected_rowsxxxxxxxx);
            ApprovalFactory.reject($scope.selected_rows).then(
                function(res){
                    ApprovalFactory.getData().then(
                        function(res){
                            console.log("it's True");
                            
							bsNotify.show({
								title: "Berhasil",
								content: "SO berhasil di reject.",
								type: 'success'
							});
                        }
                    );
                },
                function(err){
                    console.log("err=>",err);
                }
            );
            angular.element('.ui.modal.reject').modal('hide');
        }
        
        $scope.kembaliReject = function(item){
            angular.element('.ui.modal.reject').modal('hide');
        }

        //----------------------------------
        // End Modal
        //----------------------------------

        // function roleFlattenAndSetLevel(node, lvl) {
        //     for (var i = 0; i < node.length; i++) {
        //         node[i].$$treeLevel = lvl;
        //         gridData.push(node[i]);
        //         if (node[i].child.length > 0) {
        //             roleFlattenAndSetLevel(node[i].child, lvl + 1)
        //         } 
        //         else {

        //         }
        //     }
        //     return gridData;
        // }

        $scope.selectRole = function(rows){
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows){
            console.log("onSelectRows=>", rows);
            $scope.selected_rows = rows;
			
			$scope.ApprovalAllCanApproveGotCannot=true;
			for(var i = 0; i < $scope.selected_rows.length; ++i)
			{
				if($scope.selected_rows[i].CanApprove==true)
				{
					$scope.ApprovalAllCanApproveGotCannot=false;
				}
				else if($scope.selected_rows[i].CanApprove==false)
				{
					$scope.ApprovalAllCanApproveGotCannot=true;
					break;
				}
			}
			
			if($scope.ApprovalAllCanApproveGotCannot==true)
			{
				// tadinya ini messagebox Salah satu data yang anda pilih tidak dapat anda approve tapi diminta ilang
			}
			
        }

        $scope.openReasonApproval = function (data){
                $scope.ApprovalReason = data;
                angular.element('.ui.small.modal.reasonApproval').modal('show');
             };
     
        $scope.closeReasonApproval = function (){
                 angular.element('.ui.small.modal.reasonApproval').modal('hide');
              };
        

       


        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            //paginationPageSize: 10,
            columnDefs: [                
                { name: 'S0 id', field: 'SOId',  visible:false},
                { name: 'Sales order no', width: '15%', field: 'SoCode'},
                { name: 'Sales order date', width: '15%', field: 'SoDate', cellFilter: 'date:\'dd-MM-yyyy\''},
                { name: 'Prospect id', width: '15%', field: 'ProspectCode'},
                { name: 'Nama prospect', width: '15%', field: 'ProspectName'},
                { name: 'Approval Type', field: 'ApprovalCategoryName'},
                { name: 'Alasan Permohonan', width: '10%', cellTemplate: '<div><a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat Alasan Pengajuan" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.$parent.openReasonApproval(row.entity.RequestReason)" tabindex="0"> ' +
                '<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
                '</a></div>'}
            ]
        };
    });