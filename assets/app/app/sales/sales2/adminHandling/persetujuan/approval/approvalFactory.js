angular.module('app')
    .factory('ApprovalFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(filter) {
                for(key in filter){
                  if(filter[key]==null || filter[key]==undefined)
                    delete filter[key];
                }
                var res=$http.get('/api/sales/AdminHandlingApprovalSO/?start=1&limit=100000&'+$httpParamSerializer(filter));
                return res;
            },

            getDataCategory: function(param) {
                var res = $http.get('/api/sales/SGlobalApprovalCategory' + param);
                return res;
            },

            Approval: function(approval) {
                return $http.put('/api/sales/AdminHandlingApprovalSO/Approve', approval);
            },

            reject: function(reject) {
                return $http.put('/api/sales/AdminHandlingApprovalSO/Reject', reject);
            },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });