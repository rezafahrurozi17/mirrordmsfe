angular.module('app')
    .factory('PersetujuanFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(filter) {
                for(key in filter){
                  if(filter[key]==null || filter[key]==undefined)
                    delete filter[key];
                }
                var res=$http.get('/api/sales/AdminHandlingApprUnapprovedSO/?start=1&limit=100000&'+$httpParamSerializer(filter));
                return res;
            },

            getDataPengajuan: function(param) {
                var res = $http.get('/api/sales/AdminHandlingApprSOApprovalRequestList' + param);
                return res;
            },

            getDataPajak: function() {
                var res = $http.get('/api/sales/PCategoryBillingCancelTaxInvoiceType');
                return res;
            },
			
			getJPembayaran: function() {
				var res=$http.get('/api/sales/FFinancePaymentType');
				return res;
			},

            getDataHistory: function(param) {
                var res = $http.get('/api/sales/AdminHandlingApprSOApprovalHistory' + param);
                return res;
            },

            create: function(persetujuan) {
                return $http.post('/api/sales/AdminHandlingApprSOApprovalRequestList', persetujuan);
            },

            update: function(persetujuan) {
                return $http.put('/api/fw/Role', [{
                    NoSPK: SpkTaking.NoSPK,
                    NamaSPK: SpkTaking.NamaSales,
                }]);
            },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd