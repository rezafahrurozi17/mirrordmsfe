angular.module('app')
    .controller('PersetujuanController', function($rootScope,$stateParams, bsNotify, $scope, $httpParamSerializer, $http, CurrentUser, PersetujuanFactory, MaintainSOFactory, MaintainBillingFactory, ComboBoxFactory, TipeFactory, $timeout, MobileHandling) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.persetujuanForm = true;
        $scope.pengajuanForm = false;
        $scope.historyPengajuanForm = false;
        $scope.ViewPengajuan = true;
        $scope.ShowSalesOrderForm = false;
        $scope.ViewHistoryPengajuan = true;
        $scope.DisabledTotalRincianHarga = true;
        $scope.disabledBtnDraftPengajuan = false;

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
            //----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------
        });

        IfGotProblemWithModal();
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
		$scope.tab = $stateParams.tab;
		$scope.breadcrums = {};
        $scope.mPersetujuan = null; //Model
        $scope.filter = { SODateStart: null, SODateEnd: null, SPKNo: null, SOCode: null };

        $scope.dateOptionsStart = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.dateOptionsEnd = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
        }

        $scope.tanggalmin = function(dt) {
            $scope.dateOptionsEnd.minDate = dt;
			if($scope.filter.SODateEnd<$scope.filter.SODateStart)
			{
				$scope.filter.SODateEnd=$scope.filter.SODateStart;
			}
        }
		
		$scope.PengajuanPersetujuanJagaSoDateTo = function() {
            if($scope.filter.SODateEnd<$scope.filter.SODateStart)
			{
				$scope.filter.SODateEnd=$scope.filter.SODateStart;
			}
        }

        $scope.showAlert = function(message, number) {
            $.bigBox({
                title: 'SO Date Tidak Boleh Kosong.!',
                content: message,
                color: "#c00",
                icon: "fa fa-shield fadeInLeft animated",
            });
        };
		
		var Da_TodayDate=new Date();
		var da_awal_tanggal1 = new Date(Da_TodayDate.getFullYear(), Da_TodayDate.getMonth(), 1);
		$scope.filter.SODateStart=da_awal_tanggal1;
		$scope.tanggalmin($scope.filter.SODateStart);
		$scope.filter.SODateEnd=Da_TodayDate;

        var listData = [{
                FundSourceId: 1,
                FundSourceName: "Cash"
            },
            {
                FundSourceId: 2,
                FundSourceName: "Transfer Bank"
            },
            {
                FundSourceId: 4,
                FundSourceName: "Credit Card/Debit"
            }
        ];

        $scope.selectionItem = listData;
		
		PersetujuanFactory.getJPembayaran().then(
            function(res) {
                $scope.dataPembayaran = res.data.Result;
                return res.data;
            }
        );
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {

            $scope.filter.SODateStart = $scope.changeFormatDate($scope.filter.SODateStart);
            $scope.filter.SODateEnd = $scope.changeFormatDate($scope.filter.SODateEnd);
                
            PersetujuanFactory.getData($scope.filter).then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }
        $scope.changeFormatDate = function(item) {
            var tmpParam = item;
            tmpParam = new Date(tmpParam);
            var finalDate
            var yyyy = tmpParam.getFullYear().toString();
            var mm = (tmpParam.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpParam.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            
            return finalDate;
        }

        MaintainSOFactory.getSOType().then(
            function(res) {
                $scope.getSOType = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getCustomerCategory().then(
            function(res) {
                $scope.getCustomerCategory = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getDataBank().then(
            function(res) {
                $scope.getDataBank = res.data.Result;
                return res.data;
            }
        );

        MaintainSOFactory.getOutlet().then(
            function(res) {
                $scope.getOutlet = res.data.Result;
                return res.data;
            }
        );

        PersetujuanFactory.getDataPajak().then(
            function(res) {
                $scope.dataPajak = res.data.Result;
                return res.data;
            }
        );

        TipeFactory.getData().then(
            function(res) {
                $scope.listTipe = res.data.Result;
                return res.data;
            }
        );
		
		$scope.changeInfoPelangganPersetujuan = function(selected) {
            console.log(selected);
            if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pembeli") {
                $scope.infoPembeli = true;
                $scope.infoPemilik = false;
            } else if ($scope.ViewData.CustomerTypeDesc === 'Individu' &&
                selected.CustomerCorrespondentName === "Pemilik") {
                $scope.infoPemilik = true;
                $scope.infoPembeli = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintahan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pembeli") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;
				$scope.infoPembeliPerusahaan = true; 
				$scope.infoPenanggungJawabPerusahaan = false; 
				$scope.infoPemilikPerusahaan = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintahan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Penanggung Jawab") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;
				$scope.infoPenanggungJawabPerusahaan = true; 
				$scope.infoPembeliPerusahaan = false; 
				$scope.infoPemilikPerusahaan = false;
            } else if (($scope.ViewData.CustomerTypeDesc === 'Perusahaan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Pemerintahan' ||
                    $scope.ViewData.CustomerTypeDesc === 'Yayasan') && selected.CustomerCorrespondentName === "Pemilik") {
                $scope.infoPemilik = false;
                $scope.infoPembeli = false;
				$scope.infoPemilikPerusahaan = true; 
				$scope.infoPembeliPerusahaan = false; 
				$scope.infoPenanggungJawabPerusahaan = false;
            } else {
                //kosong();
            }
        }

        $scope.ListPengajuan = function() {
            var getSO = "?SoId=" + $scope.ViewDataPengajuan.SoId;
            PersetujuanFactory.getDataPengajuan(getSO).then(
                function(res) {
                    $scope.ResultPengajuan = res.data.Result;
                    $scope.gridPengajuan.data = $scope.ResultPengajuan;
					$scope.selctedRow = [];
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        };

        $scope.btnPengajuan = function(Select) {
            $scope.persetujuanForm = false;
            $scope.pengajuanForm = true;
			$scope.breadcrums.title="Pengajuan";
            $scope.ViewDataPengajuan = Select;
            $scope.ListPengajuan();
        };

        $scope.btnDraftPengajuan = function() {
            $scope.disabledBtnDraftPengajuan = true;
            if($scope.selctedRow.length<1||$scope.selctedRow==[])
			{
				bsNotify.show(
						{
							title: "Peringatan",
							content: "Mohon Pilih Permohonan Persetujuan.",
							type: 'warning'
						}
                    );
                    $scope.disabledBtnDraftPengajuan = false;
			}
			else
			{
				if ($scope.selctedRow[0].RequestReason == "" || $scope.selctedRow[0].RequestReason == null) {

					
					bsNotify.show({
								title: "Peringatan",
								content: "Mohon Input Alasan Permohonan.",
								type: 'warning'
                            });
                            $scope.disabledBtnDraftPengajuan = false;
				} else {
					if ($scope.selctedRow[0].ApprovalCategoryCode == "AHBatalBilling") {
						$scope.pengajuanForm = false;
						$scope.persetujuanBatalBillingform = true;
						$scope.breadcrums.title="Pengajuan Batal Billing";


						MaintainBillingFactory.getBillingIdForDetailSudahBilling($scope.selctedRow[0].SOId).then(function(res) {
							var getSO = "?SOId=" + $scope.selctedRow[0].SOId + "&BillingId=" + res.data.Result[0].BillingId;
							MaintainBillingFactory.getDetailSudahBilling(getSO).then(
								function(res2) {
									$scope.Getbilling = res2.data.Result;
									return res2.data;
								}
							);

                        });
                        $scope.disabledBtnDraftPengajuan = false;


					} else {
						PersetujuanFactory.create($scope.selctedRow).then(
							function(res) {
								$scope.getData();

								bsNotify.show({
									title: "Berhasil",
									content: "Berhasil Diajukan.",
									type: 'success'
								});
								$scope.pengajuanForm = false;
                                $scope.persetujuanForm = true;
                                $scope.disabledBtnDraftPengajuan = false;
							},
							function(err) {
								
								
								bsNotify.show({
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
                                });
                                $scope.disabledBtnDraftPengajuan = false;
							}
						);
					}
				}
			}
			
        };

        $scope.btnKembaliDraftPengajuan = function() {
            $scope.persetujuanForm = true;
            $scope.pengajuanForm = false;
            $scope.gridData = [];
        };

        $scope.btnSimpanPengajuan = function() {
            $scope.tempPajak = { FakturPajakNo: $scope.Getbilling[0].FakturPajakNo, BillingCancelTaxInvoiceTypeId: $scope.Getbilling[0].BillingCancelTaxInvoiceTypeId }
            angular.merge($scope.selctedRow[0], $scope.tempPajak);
            PersetujuanFactory.create($scope.selctedRow).then(
                function(res) {
                    
					bsNotify.show({
									title: "Berhasil",
									content: "Behasil Diajukan",
									type: 'success'
								});

                    PersetujuanFactory.getData($scope.filter).then(
                        function(res) {
                            $scope.grid.data = res.data.Result;
                            $scope.loading = false;
                            $scope.pengajuanForm = false;
                            $scope.persetujuanForm = true;
                            $scope.persetujuanBatalBillingform = false;
                            return res.data.Result;
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );


                },
                function(err) {
                    
					bsNotify.show({
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								});
                }
            );
        }

        $scope.btnKembaliPengajuan = function() {
            $scope.pengajuanForm = true;
            $scope.persetujuanBatalBillingform = false;
            $scope.gridData = [];
        }

        $scope.btnHistoryPengajuan = function(Selected) {
            $scope.SelectRow = Selected;

            var getSO = "?SoId=" + $scope.SelectRow.SoId;
            PersetujuanFactory.getDataHistory(getSO).then(
                function(res) {
                    $scope.ResultHistory = res.data.Result;
                    $scope.gridhistorypengajuan.data = $scope.ResultHistory;
                },
                function(err) {
                    bsNotify.show({
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								});
                }
            );

            $scope.historyPengajuanForm = true;
            $scope.persetujuanForm = false;
        }

        $scope.btnTutupHistoryPengajuan = function() {
            $scope.historyPengajuanForm = false;
            $scope.ShowSalesOrderForm = false;
            $scope.persetujuanForm = true;
        }

        $scope.BtnTutupShowSO = function() {
            $scope.ShowSalesOrderForm = false;
            $scope.persetujuanForm = true;
        }

        $scope.NoSOCode = {};
        $scope.ViewSO = function(SOId) {
			$scope.breadcrums.title="Lihat";
            MaintainSOFactory.getSODetail(SOId).then(function(res) {
                $scope.NoSOCode = res.data.Result[0];
                $scope.GetDetailSO();
            })
        }

        $scope.GetDetailSO = function() {
            $scope.ViewData = $scope.NoSOCode;
            $scope.flagStatus = "lihatSO";
            ComboBoxFactory.getDataWarna("?start=1&limit=100&filterData=VehicleTypeId|" + $scope.ViewData.VehicleTypeId).then(
                function(res) {
                    $scope.listWarna = res.data.Result;
                })
            $scope.ShowSalesOrderForm = true;
            $scope.persetujuanForm = false;
            $scope.disableViewPelanggan = true;
            $scope.disableViewJasa = true;
            $scope.disableViewAksesoris = true;
            $scope.disableViewDokumen = true;
            $scope.DisabledRincianHarga = true;
            $scope.getPeranPelanggan = [];
            $scope.deleteRows = true;
            for (var i in $scope.NoSOCode.ListOfSOCustomerInformationView) {
                $scope.getPeranPelanggan.push({
                    CustomerCorrespondentId: $scope.NoSOCode.ListOfSOCustomerInformationView[i].CustomerCorrespondentId,
                    CustomerCorrespondentName: $scope.NoSOCode.ListOfSOCustomerInformationView[i].CustomerCorrespondentName
                });
            }
            $scope.gridJasaPengurusanDokumen.data = $scope.NoSOCode.ListOfSODocumentCareDetailService;
            $scope.gridAksesoris.data = $scope.NoSOCode.ListOfSODetailAccesoriesView;
            $scope.gridDokumen.data = $scope.NoSOCode.ListOfSODocumentView;
            $scope.ViewDataDelivery = $scope.NoSOCode.ListOfSODeliveryInfoView;
            $scope.ViewDataInfoUnit = $scope.NoSOCode.ListOfSOUnitInfoView;
            $scope.ViewDataAsuransi = $scope.NoSOCode.ListOfSOInsuranceExtView;
            $scope.disabledInfoPengiriman = true;
			if ($scope.ViewData.SOTypeName == "Finish Unit") {
                $scope.ShowSONumber = true;
                $scope.ShowProspectID = true;
                $scope.ShowProspectName = true;
                $scope.ShowSalesman = false;
                $scope.InfoPelanggan = true;
                $scope.ShowPeranPelanggan = true;
                $scope.disableTextInfoPelanggan = true;
                $scope.ShowPelangganOPD = false;
                $scope.ShowPelangganFinishUnit = true;
                $scope.JasaPengurusanDokumen = false;
                $scope.Dokumen = true;
                $scope.btntambah = false;
                $scope.InfoPengiriman = true;
                $scope.disabledInfoPengiriman = true;
                $scope.showDelivery = true;
                $scope.showOfftheRoad = true;
                $scope.showMatching = true;
                $scope.showPromiseDelivery = true;
                $scope.showEstimasiRentang = true;
                $scope.showStatusPDD = true;
                $scope.RincianHarga = true;
                $scope.Aksesoris = true;
                $scope.karoseriFinishUnit = true;
                $scope.InfoLeasingFinishUnit = true;
                $scope.disabledinfoLeasing = true;
                $scope.InfoAsuransiFinishUnit = true;
                $scope.finishUnitInfounityangakandibeli = true;
                $scope.finishUnitWarna = true;
                $scope.InfoMediatorFinishUnit = true;
                $scope.InfoPemakaiFinishUnit = true;
                $scope.showAlasanBatalSO = true;
                $scope.infoPembeli = true;
            } else {
                if ($scope.ViewData.SOTypeName == "OPD") {
                    $scope.ShowSONumber = true;
                    $scope.ShowSalesman = true;
                    $scope.ShowProspectID = false;
                    $scope.ShowProspectName = false;
                    $scope.ShowPeranPelanggan = false;
                    $scope.disableTextInfoPelanggan = true;
                    $scope.InfoPelanggan = true;
                    $scope.ShowPelangganOPD = true;
                    $scope.ShowPelangganFinishUnit = false;
                    $scope.JasaPengurusanDokumen = true;
                    $scope.Dokumen = true;
                    $scope.btntambah = false;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.DisabledTotalRincianHarga = true;
                    $scope.Aksesoris = false;
                    $scope.menuRincianHarga = true;
                    $scope.InfoPelangganFinishUnit = false;
                    $scope.karoseriFinishUnit = false;
                    $scope.InfoLeasingFinishUnit = false;
                    $scope.InfoAsuransiFinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli = false;
                    $scope.finishUnitWarna = true;
                    $scope.InfoMediatorFinishUnit = false;
                    $scope.InfoPemakaiFinishUnit = false;
                    $scope.infoPembeli = false;
                } else if ($scope.ViewData.SOTypeName == "Purna Jual") {
                    $scope.ShowSONumber = true;
                    $scope.ShowProspectID = false;
                    $scope.ShowProspectName = false;
                    $scope.ShowPeranPelanggan = false;
                    $scope.disableTextInfoPelanggan = true;
                    $scope.InfoPelanggan = true;
                    $scope.ShowPelangganOPD = true;
                    $scope.ShowPelangganFinishUnit = false;
                    $scope.JasaPengurusanDokumen = false;
                    $scope.Dokumen = false;
                    $scope.btntambah = false;
                    $scope.Aksesoris = true;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.DisabledTotalRincianHarga = true;
                    $scope.InfoPelangganFinishUnit = false;
                    $scope.karoseriFinishUnit = false;
                    $scope.InfoLeasingFinishUnit = false;
                    $scope.InfoAsuransiFinishUnit = false;
                    $scope.finishUnitInfounityangakandibeli = false;
                    $scope.finishUnitWarna = true;
                    $scope.InfoMediatorFinishUnit = false;
                    $scope.InfoPemakaiFinishUnit = false;
                    $scope.infoPembeli = false;
                } else if ($scope.ViewData.SOTypeName == "Swapping") {
                    $scope.InfoPelanggan = false;
                    $scope.Aksesoris = false;
                    $scope.InfoPengiriman = true;
                    $scope.RincianHarga = true;
                    $scope.JasaPengurusanDokumen = false;
                    $scope.Dokumen = false;
                    $scope.finishUnitUbahInfounityangakandibeli = true;
                    $scope.finishUnitWarna = true;
                    $scope.infoPembeli = false;
                }
            }
        }

        $scope.ChangePelanggan = function() {
            if ($scope.ViewData.CustomerTypeId == "3") {
                $scope.IndividuShow = true;
                $scope.InstitusiShow = false;
            } else {
                $scope.IndividuShow = false;
                $scope.InstitusiShow = true;
            }
        }

        $scope.viewDocument = function(row) {
            // $scope.viewdocument = row.StrBase64;
            // $scope.viewdocumentName = row.DocumentName;
            // $scope.status = true;
            // console.log("v.Doc => ", $scope.viewdocument);
            // console.log("v.DocName => ", $scope.viewdocumentName);
            // callModal();
            MaintainSOFactory.getImage(row.DocumentURL).then(
                function(res){
                    $scope.viewdocument = res.data.UpDocObj;
                    $scope.viewdocumentName = res.data.FileName; 
                    $scope.status = true;
                    callModal();
                }
            )
        }

        function callModal() {
            angular.element('.ui.modal.dokumenViewPersetujuan').modal({ closable: false }).modal('show');
        }

        $scope.keluarImg = function() {
            $scope.status = false;
            angular.element('.ui.modal.dokumenViewPersetujuan').modal('hide');
        }

        $scope.unitLihat = function() {
            // if ($scope.unitLihatView == true) {
                // $scope.unitLihatView = false;
            // } else {
                // $scope.unitLihatView = true;
            // }

            // if ($scope.jasaLihatView == true) {
                // $scope.jasaLihatView = false;
            // } else {
                // $scope.jasaLihatView = true;
            // }

            // if ($scope.PurnaLihatView == true) {
                // $scope.PurnaLihatView = false;
            // } else {
                // $scope.PurnaLihatView = true;
            // }
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        var SoNumberClick = '<span style="color:blue"><p style="padding:5px 0 0 5px" ng-click="grid.appScope.$parent.ViewSO(row.entity.SoId)"><u>{{row.entity.SoCode}}</u></p></span>';
        var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.$parent.btnPengajuan(row.entity)">Pengajuan</u>&nbsp<u ng-click="grid.appScope.$parent.btnHistoryPengajuan(row.entity)">History Pengajuan</u></span>'

        var viewClick = '<span>\
        <p><u ng-if="row.entity.StatusDocumentName ==\'Empty\'">\
            <input type="file" bs-uploadsingle="onFileSelect($files)" img-upload="row.entity.DocumentData" name="" id="pickfiles-nav1" style="z-index: 1">\
        </u>&nbsp<u ng-if="row.entity.StatusDocumentName ==\'Upload\'"\
            ng-click="grid.appScope.viewDocument(row.entity)" style="color:blue">View</u></span>'

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'SOId', field: 'SoId', width: '7%', visible: false },
                { displayName:'No. SPK' ,name: 'No. SPK', field: 'FormSPKNo' },
                { displayName:'No. SO' ,name: 'No. SO', field: 'SoCode', cellTemplate: SoNumberClick },
                { displayName:'Tanggal SO' ,name: 'Tanggal SO', field: 'SoDate', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Nama prospek/Pelanggan', field: 'ProspectName' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '20%',
                    pinnedRight: true,
					visible:true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnAction
                }
            ]
        };

        $scope.gridPengajuan = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            enableColumnResizing: true,
            columnDefs: [
                { name: 'ApprovalCategoryId', field: 'ApprovalCategoryId', visible: false },
                { name: 'Permohonan Persetujuan', field: 'ApprovalCategoryName' },
                { name: 'Alasan Permohonan *', field: 'RequestReason', cellTooltip: 'Double click untuk mengisi alasan', enableCellEdit: true }
            ]
        };

        $scope.gridPengajuan.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridhistorypengajuan = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableColumnResizing: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Permohonan Persetujuan', field: 'ApprovalCategoryName' },
                { name: 'Status', field: 'StatusApprovalName' },
                { name: 'Disetujui/Ditolak Oleh', field: 'ApproverName' },
                { name: 'Tanggal Setuju/Tolak', field: 'ApproveRejectDate', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Alasan Permohonan', field: 'RequestReason' },
                { name: 'Alasan Penolakan', field: 'RejectReason' }
            ]
        };

        $scope.gridJasaPengurusanDokumen = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'dataJasaPengurusanDokumen',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'ServiceId.', field: 'ServiceId', visible: false },
                { name: 'Item No.', field: 'ItemNo', visible: false },
                { name: 'Kode Jasa', field: 'ServiceCode' },
                { name: 'Nama Jasa', field: 'ServiceName' },
                { name: 'Qty', field: 'Qty', format: '{0:c}', type: 'number', enableCellEdit: true }
            ]
        };

        $scope.gridDokumen = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            //data: 'dataInfoDokumen',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'No', field: 'No', width: '7%', visible: false },
                { name: 'Tipe Dokumen', field: 'SODetailDocumentId', visible: false },
                { name: 'Document Id', field: 'DocumentId', visible: false },
                { name: 'Tipe Dokumen', field: 'DocumentType', width: '25%' },
                {
                    name: 'Dokumen',
                    field: 'DocumentURL',
                    allowCellFocus: false,
                    width: '35%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: viewClick
                },
                {
                    name: 'Status',
                    field: 'StatusDocumentName',
                    width: '20%'
                }
            ]
        };

        $scope.gridDokumen.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectRowgridDokumen = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectRowgridDokumen = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridAksesoris = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            //data: 'dataInfoAksesoris',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Item No', field: 'ItemNo', visible: false },
                { name: 'PartsId', field: 'PartsId', width: '20%', visible: false },
                { name: 'No. Material', field: 'AccessoriesCode' },
                { name: 'Nama Material', field: 'AccessoriesName' },
                { name: 'Qty', field: 'Qty', format: '{0:c}', type: 'number', enableCellEdit: true },
                { name: 'Satuan', field: 'UomName' },
                { name: 'Harga Satuan', field: 'Price' },
				{ displayName:'No. PR',name: 'No PR', field: 'PRCode' },
                { displayName:'Status PR',name: 'Status PR', field: 'StatusPRName' },
				{ name: 'Diskon', field: 'Discount' }
            ]
        };
    });

app.directive('imageSetAspect', function($scope) {
    return {
        scope: false,
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.bind('load', function() {
                // Use jquery on 'element' to grab image and get dimensions.
                scope.$apply(function() {
                    $scope.viewdocument.width = $(element).width();
                    $scope.viewdocument.height = $(element).height();
                    //console.log(scope.$parent.imageMeta);
                });
            });
        }
    };
});