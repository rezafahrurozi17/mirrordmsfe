angular.module('app')
  .factory('TOPFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
          for(key in filter){
            if(filter[key]==null || filter[key]==undefined)
              delete filter[key];
          }
          var res=$http.get('/api/sales/AdminHandlingTOPMaintain/?start=1&limit=100&'+$httpParamSerializer(filter));
          return res;
      },
	  
	  getFundSource: function() {
          var res=$http.get('/api/sales/FFinanceFundSource');
          return res;
      },

      create: function(tOPFactory) {
        return $http.post('/api/fw/AdminHandlingTOPMaintain', [{
                                            Id: tOPFactory.SOId,
                                            SODate: tOPFactory.SODate,
                                            Status: tOPFactory.StatusSO,
                                            NOSPK: tOPFactory.NOSPK,
                                            ProspectId:tOPFactory.ProspectId,
                                            ProspectName:tOPFactory.ProspectName
                                            }]);
      },
      
      update: function(tOP){
        return $http.put('/api/sales/AdminHandlingTOPMaintain', [tOP]);
      },

      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });




