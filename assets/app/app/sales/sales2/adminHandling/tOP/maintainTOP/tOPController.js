angular.module('app')
    .controller('TOPController', function($scope, $http,bsNotify, $httpParamSerializer, CurrentUser, TOPFactory, MaintainSOFactory, $timeout, MobileHandling) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.maintainTOP = true;    
    $scope.UbahTOP = false;
    $scope.show_modal={show:false};

    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
        //----------Begin Check Display---------
            
              $scope.bv=MobileHandling.browserVer();
              $scope.bver=MobileHandling.getBrowserVer();
              //console.log("browserVer=>",$scope.bv);
              //console.log("browVersion=>",$scope.bver);
              //console.log("tab: ",$scope.tab);
              if($scope.bv!=="Chrome"){
              if ($scope.bv=="Unknown") {
                if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
                {
                  $scope.removeTab($scope.tab.sref);
                }
              }        
              }
            
            //----------End Check Display-------
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mTOP = null; //Model
    $scope.xRole={selected:[]};
    $scope.filter={SPKDateStart:null, SPKDateEnd:null, SPKNo:null};

    $scope.dateOptionsStart = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    };

    $scope.dateOptionsEnd = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1,
    }

    $scope.tanggalmin = function (dt){
        $scope.dateOptionsEnd.minDate = dt;
    }

    $scope.showAlert = function(message, number){
        $.bigBox({
            title: 'SO Date Tidak Boleh Kosong.!',
            content: message,
            color: "#c00",
            icon: "fa fa-shield fadeInLeft animated",
            timeout: 2000
        });
    };
	
	var Da_Today_Date=new Date();
	$scope.filter.SPKDateStart=new Date(Da_Today_Date.setDate(1));
	$scope.filter.SPKDateEnd=new Date();

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
     $scope.getData = function() {
	 
		try
		{
			var da_SPKDateStart=$scope.filter.SPKDateStart;
			$scope.filter.SPKDateStart= da_SPKDateStart.getFullYear()+'-'
												+('0' + (da_SPKDateStart.getMonth()+1)).slice(-2)+'-'
												+('0' + da_SPKDateStart.getDate()).slice(-2);
		}
		catch(exceptionthingy)
		{
			$scope.filter.SPKDateStart=null;
		}
		
		try
		{
			var da_SPKDateEnd=$scope.filter.SPKDateEnd;
			$scope.filter.SPKDateEnd= da_SPKDateEnd.getFullYear()+'-'
												+('0' + (da_SPKDateEnd.getMonth()+1)).slice(-2)+'-'
												+('0' + da_SPKDateEnd.getDate()).slice(-2);
		}
		catch(exceptionthingy)
		{
			$scope.filter.SPKDateEnd=null;
		}
	 
        TOPFactory.getData($scope.filter).then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading = false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }

    MaintainSOFactory.getCustomerCategory().then(
        function(res){
            $scope.getCustomerCategory = res.data.Result;
            return res.data;
        }
    );

    $scope.btnUbah = function(selected){ //untuk grid awal saat ubah
        $scope.gridUbahTOP.data=[];
		$scope.editTOP = selected;
		console.log("yg bener==>", $scope.gridUbahTOP);
		for(var i=0;i<$scope.editTOP.ListOfTOPMaintainDetailView.length; i++)
		{
			$scope.editTOP.ListOfTOPMaintainDetailView[i].TOPKe=(i+1);
		}
		
		for(var i=0;i<$scope.editTOP.ListOfTOPMaintainDetailView.length; i++)
		{
			var da_buat_kurangin=0;
			for(var x=0;x<=i; x++)
			{
				da_buat_kurangin+=parseInt($scope.editTOP.ListOfTOPMaintainDetailView[x].Amount);
			}
			$scope.editTOP.ListOfTOPMaintainDetailView[i].Remaining=parseFloat($scope.editTOP.GrandTotal-da_buat_kurangin).toFixed(1);
		}
		
        $scope.gridUbahTOP.data = $scope.editTOP.ListOfTOPMaintainDetailView;
        $scope.maintainTOP = false;
        $scope.UbahTOP = true;
    }
	
	$scope.saveBtn = function(selected){
		
		TOPFactory.update($scope.editTOP).then(function () {
					//nanti di get
					TOPFactory.getData($scope.filter).then(
						function(res){
							$scope.grid.data = res.data.Result;
							$scope.loading = false;
							$scope.btnKembaliUbahTOP();
							return res.data.Result;
						},
						function(errorget){
							console.log("err=>",errorget);
						}
					);
				},
							function(err){
								if(err.data.Message=="Exception Error# xml detail kosong Error occured in procedure SAH_TOPPut_SP")
								{
									bsNotify.show(
										{
											title: "Peringatan",
											content: "TOP tidak boleh kosong",
											type: 'warning'
										}
									);
								}
								else
								{
									bsNotify.show(
										{
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										}
									);
								}
								
							});
		
		
		
    }
  
    $scope.btnKembaliUbahTOP = function(){
       $scope.UbahTOP = false;
       $scope.maintainTOP = true;
	   $scope.editTOP={};
	   $scope.gridUbahTOP.data=[];
    }

    //----------------------------------
    // Begin Modal
    //----------------------------------
    $scope.btnLihatTOP = function(selected)
    {
        $scope.lihatTOP = selected;
        $scope.lihatTOPDetail = selected.ListOfTOPMaintainDetailView;
        $scope.show_modal.show =! $scope.show_modal.show;
    }
	
	$scope.HapusChildTop = function(row)
    {
		
		var index = $scope.gridUbahTOP.data.indexOf(row); //variabel index ke-
		
        $scope.gridUbahTOP.data.splice(index, 1); //hapus index yg dipilih sebanyak 1 kali
		
		for(var i=0;i<$scope.gridUbahTOP.data.length; i++)
		{
			$scope.gridUbahTOP.data[i]["TOPKe"]=(i+1);
		}
		
		for(var i=0;i<$scope.gridUbahTOP.data.length; i++)
		{
			var da_buat_kurangin=0;
			for(var x=0;x<=i; x++)
			{
                da_buat_kurangin+=parseInt($scope.gridUbahTOP.data[x].Amount); 
                // da_buat_kurangin+=parseFloat($scope.gridUbahTOP.data[x].Amount).toFixed(1); 
                // da_buat_kurangin+=($scope.gridUbahTOP.data[x].Amount); 
                // da_buat_kurangin = da_buat_kurangin.toFixed(1)
			}
			$scope.gridUbahTOP.data[i].Remaining=parseFloat($scope.editTOP.GrandTotal-da_buat_kurangin).toFixed(1);
		}
		
		
    }
    
    $scope.onListSave = function(item)
    {
    }
    
    $scope.onListCancel = function(item)
    {
        $scope.show_modal={show:false};
    }

    $scope.btnTambahTOP = function(){
        angular.element('.ui.modal.insertTOP').modal('show');
		$scope.Top_TambahTop_Amount=0;
		$scope.Top_TambahTop_Due=0;
    }

    $scope.btnBatalInsertTOP = function(){
        angular.element('.ui.modal.insertTOP').modal('hide');
    }

    $scope.btnInsertTOP = function(){ //untuk grid setelah tambah data
        $scope.gridUbahTOP.data.push({TopId:null,
									TOPKe:null,
									Amount:$scope.Top_TambahTop_Amount,
									Due:$scope.Top_TambahTop_Due,
                                    Remaining:null}); // data yg baru ditambahkan masuk kesini
        // if ($scope.gridUbahTOP.data.Remaining>$scope.editTOP.GrandTotal){
        //    alert("Nilai Amount tidak bisa melebihi total SPK") 
        // }
					
		for(var i=0;i<$scope.gridUbahTOP.data.length; i++) 
		{
			$scope.gridUbahTOP.data[i]["TOPKe"]=(i+1);
		}
		
		
		for(var i=0;i<$scope.gridUbahTOP.data.length; i++)
		{
			var da_buat_kurangin=0;
			for(var x=0;x<=i; x++)
			{
                da_buat_kurangin+=parseInt($scope.gridUbahTOP.data[x].Amount); // 0 + data Amount yg ditambahkan
                // da_buat_kurangin+=parseFloat($scope.gridUbahTOP.data[x].Amount).toFixed(1); 
                // da_buat_kurangin+=$scope.gridUbahTOP.data[x].Amount; 
                // da_buat_kurangin = da_buat_kurangin.toFixed(1)
            }
            // $scope.gridUbahTOP.data[i].Remaining=($scope.editTOP.GrandTotal- da_buat_kurangin);
            $scope.gridUbahTOP.data[i].Remaining=parseFloat($scope.editTOP.GrandTotal- da_buat_kurangin).toFixed(1);   
        }
        console.log("yg salah==>", $scope.gridUbahTOP.data);						
		//console.log("ulala",$scope.gridUbahTOP.data);

		// $scope.gridApigridUbahTOP.grid.refresh();
		
		angular.element('.ui.modal.insertTOP').modal('hide');
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
	
    var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.$parent.btnLihatTOP(row.entity)">Lihat</u>&nbsp<u ng-click="grid.appScope.$parent.btnUbah(row.entity)">Ubah</u></span>'
    var btnHapus = '<a style="color:blue;"  ng-click=""><p style="padding:5px 0 0 5px" ng-click="grid.appScope.HapusChildTop(row.entity)";><u>Hapus</u></p></a>';
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: false,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Spk id', field:'SpkId', width:'7%', visible:false},
            { name:'No Spk', field:'FormSPKNo', width:'15%'},
            { name:'Spk Date', field:'SpkDate', width:'12%', cellFilter: 'date:\'dd-MM-yyyy\''},
			{ name:'Nama Prospect/pelanggan',  field: 'ProspectName'},
			{ name:'Jumlah kendaraan yang dipesan',  field: 'QtyTotalUnit', width:'20%'},
            {
                name: 'Action',
                allowCellFocus: false,
                width: '15%',
                pinnedRight: true,
                enableColumnMenu: false,
                enableSorting: false,
                enableColumnResizing: true,
                cellTemplate: btnAction                    
            }
        ]
    };

    $scope.gridUbahTOP = {
        enableSorting: false,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //enableColumnResizing: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10, 25,50],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'TopId', field:'TopId', width:'7%', visible:false,},
            { name:'TOP ke-', field:'TopKe', width:'10%',cellTemplate: '<div class="ui-grid-cell-contents">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>'},

            { name:'Amount', field:'Amount', width: '15%'},
            { name:'Jatuh Tempo', field:'Due', width: '15%'},
            { name:'Remaining', field:'Remaining', width: '15%'},
            {
                name: 'action',
                allowCellFocus: false,
                width: '15%',
                pinnedRight: true,
                enableColumnMenu: false,
                enableSorting: false,
                enableColumnResizing: true,
                cellTemplate: btnHapus                    
            }
        ]
    };
	
	$scope.gridUbahTOP.onRegisterApi = function(gridApi){
        $scope.gridApigridUbahTOP = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
            $scope.selectedRowgridUbahTOP = gridApi.selection.getSelectedRows();
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
           $scope.selectedRowgridUbahTOP = gridApi.selection.getSelectedRows();
        });

    };

    $scope.inputDue= function(valuesDue) {
        if(valuesDue == 0 & valuesDue.length == 1){
           $scope.Top_TambahTop_Due = 0;
        }else{
            var first = $scope.Top_TambahTop_Due.substring(0, 1);
            var length = $scope.Top_TambahTop_Due.length;
            var fixed = $scope.Top_TambahTop_Due.substring(1, length);
            if(first == 0){
                $scope.Top_TambahTop_Due = fixed;
            }
        }
    }

	$scope.inputAmount = function(valuesAmount) {
        if(valuesAmount == 0 & valuesAmount.length == 1){
           $scope.Top_TambahTop_Amount = 0;
        }else{
            var first = $scope.Top_TambahTop_Amount.substring(0, 1);
            var length = $scope.Top_TambahTop_Amount.length;
            var fixed = $scope.Top_TambahTop_Amount.substring(1, length);
            if(first == 0){
                $scope.Top_TambahTop_Amount = fixed;
            }
        }
    }
	
});
