angular.module('app')
    .controller('ApprovalPIController', function ($rootScope, bsNotify, $scope, $http, CurrentUser, ApprovalPIFactory, $timeout, uiGridConstants) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.CreateForm = false;
        $scope.MaintainForm = true;
        $scope.showKontak = true;
        $scope.showHandphone = true;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainPI = null; //Model
        $scope.xRole = { selected: [] };
        $scope.filter = { PIDateStart: null, PIDateEnd: null, CustomerName: null };

        $scope.dateOptionsStart = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.dateOptionsEnd = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.dateOptionPengiriman = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        $scope.typeApproval = [
            { text: "Batal Kwitansi Penagihan", value: 51 },
            { text: "Cetak Ulang Kwitansi Penagihan", value: 52 }
        ];
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function () {
            alerts = [];
            $scope.gridData = [];
            if ($scope.filter.PIDateStart == null || $scope.filter.PIDateEnd == null) {
                bsNotify.show({
                    type: 'danger',
                    timeout: 3000,
                    title: "Error Message",
                    content: 'Data Mandatory harus diisi !'
                });
            }
            else {
                try {
                    $scope.filter.PIDateStart = $scope.filter.PIDateStart.getFullYear() + '-' +
                        ('0' + ($scope.filter.PIDateStart.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + $scope.filter.PIDateStart.getDate()).slice(-2) + 'T' +
                        (($scope.filter.PIDateStart.getHours() < '10' ? '0' : '') + $scope.filter.PIDateStart.getHours()) + ':' +
                        (($scope.filter.PIDateStart.getMinutes() < '10' ? '0' : '') + $scope.filter.PIDateStart.getMinutes()) + ':' +
                        (($scope.filter.PIDateStart.getSeconds() < '10' ? '0' : '') + $scope.filter.PIDateStart.getSeconds());

                    $scope.filter.PIDateStart.slice(0, 10);
                } catch (e1) {
                    $scope.filter.PIDateStart = null;
                }

                try {
                    $scope.filter.PIDateEnd = $scope.filter.PIDateEnd.getFullYear() + '-' +
                        ('0' + ($scope.filter.PIDateEnd.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + $scope.filter.PIDateEnd.getDate()).slice(-2) + 'T' +
                        (($scope.filter.PIDateEnd.getHours() < '10' ? '0' : '') + $scope.filter.PIDateEnd.getHours()) + ':' +
                        (($scope.filter.PIDateEnd.getMinutes() < '10' ? '0' : '') + $scope.filter.PIDateEnd.getMinutes()) + ':' +
                        (($scope.filter.PIDateEnd.getSeconds() < '10' ? '0' : '') + $scope.filter.PIDateEnd.getSeconds());

                    $scope.filter.PIDateEnd.slice(0, 10);
                } catch (e1) {
                    $scope.filter.PIDateEnd = null;
                }

                ApprovalPIFactory.getData($scope.filter).then(
                    function (res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        //return res.data.Result;
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );
            }
        }

        ApprovalPIFactory.getStatusApproval().then(
            function (res) {
                $scope.getStatusPembatalan = res.data.Result;
                $scope.loading = false;
                //console.log('getStatusPembatalan',$scope.getStatusPembatalan);
                //return res.data.Result;
            },
            function (err) {
                console.log("err=>", err);
            }
        );

        // $scope.getStatusPembatalan = [{StatusId : 1,StatusName : "Diajukan"},{StatusId : 2,StatusName : "Approved"},{StatusId : 3,StatusName : "Rejected"}]

        $scope.status = {
            approve:true,
            reject:true
        };

        $scope.SelectedDataCancel = [];
        $scope.onSelectRows = function (rows) {
            $scope.SelectedDataCancel = angular.copy(rows);
            if($scope.SelectedDataCancel.length == 0
                || $scope.SelectedDataCancel[0].StatusApprovalName == "Disetujui"
                || $scope.SelectedDataCancel[0].StatusApprovalName == "Ditolak"){
                $scope.status = {approve:true, reject:true};
            }else{
                $scope.status = {approve:false, reject:false };
            }
            console.log("onSelectRows=>", rows);
        }

        $scope.DitolakCancelPI = function () {
            if ($scope.SelectedDataCancel.length == 0) {
                bsNotify.show({
                    type: 'warning',
                    timeout: 3000,
                    title: "Error Message",
                    content: 'Data Harus Dipilih Terlebih Dahulu !'
                });
            } else {
                $scope.ReasonCancel = {};
                setTimeout(function () {
                    angular.element('.ui.modal.AlasanBatalPI').modal('refresh');
                }, 0);
                angular.element('.ui.modal.AlasanBatalPI').modal('show');
            }
        }

        $scope.Batal_Clicked = function () {
            angular.element('.ui.modal.AlasanBatalPI').modal('hide');
        }

        $scope.ReasonCancel = {};
        $scope.ReasonCancel.ReasonName = null;

        $scope.Simpan_Clicked = function () {
            
            if ($scope.ReasonCancel.ReasonName == null || typeof $scope.ReasonCancel.ReasonName == "undefined") {
                bsNotify.show({
                    type: 'warning',
                    timeout: 3000,
                    title: "Error Message",
                    content: 'Alasan Harus Diisi !'
                });
            } else {
                $scope.DataReject = [];
                $scope.DataReject = angular.copy($scope.SelectedDataCancel);
                for(var i in $scope.SelectedDataCancel){
                    $scope.DataReject[i].RequestNote = $scope.ReasonCancel.ReasonName;
                }
                ApprovalPIFactory.savePIReject($scope.DataReject).then(
                    function (res) {
                        //$scope.getStatusPembatalan = res.data.Result;
                        $scope.loading = false;
                        //console.log('getStatusPembatalan',$scope.getStatusPembatalan);
                        //return res.data.Result;
                        angular.element('.ui.modal.AlasanBatalPI').modal('hide');
                        bsNotify.show({
                            type: 'success',
                            timeout: 3000,
                            title: "Message",
                            content: 'Data Berhasil Disimpan !'
                        });
                        $scope.getData();
                    },
                    function (err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            type: 'danger',
                            timeout: 3000,
                            title: "Error Message",
                            content: 'Gagal Approve Cancel Kwitansi Penagihan !'
                        });
                    }
                );
            }
        }


        $scope.DiterimaCancelPI = function () {
            if ($scope.SelectedDataCancel.length == 0) {
                bsNotify.show({
                    type: 'warning',
                    timeout: 3000,
                    title: "Error Message",
                    content: 'Data Harus Dipilih Terlebih Dahulu !'
                });
            } else {
                $scope.DataApprove = [];
                $scope.DataApprove = angular.copy($scope.SelectedDataCancel);
                ApprovalPIFactory.savePIApprove($scope.DataApprove).then(
                    function (res) {
                        //$scope.getStatusPembatalan = res.data.Result;
                        $scope.loading = false;
                        //console.log('getStatusPembatalan',$scope.getStatusPembatalan);
                        //return res.data.Result;
                        bsNotify.show({
                            type: 'success',
                            timeout: 3000,
                            title: "Message",
                            content: 'Data Berhasil Disimpan !'
                        });
                        $scope.getData();
                    },
                    function (err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            type: 'danger',
                            timeout: 3000,
                            title: "Error Message",
                            content: 'Gagal Approve Cancel Kwitansi Penagihan !'
                        });
                    }
                );
            }

        }



        var ClickNoPI = '<a style="color:blue;"  ng-click="grid.appScope.$parent.PINOClick(row.entity)"><p style="padding:5px 0 0 5px"><u>{{row.entity.ProformaInvoiceCode}}</u></p></a>';
        var actionClick = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.$parent.ModalClosePO(row.entity)">Cetak</u></span>&nbsp<u ng-click="grid.appScope.$parent.btnUbahPO(row.entity)">Batal</u></span>'
        var HapusClick = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.$parent.clickhapusrowgrid(row.entity)">Hapus</u></span>'

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Nama Pelanggan', field: 'CustomerName' },
                { name: 'No Kwitansi', field: 'ProformaInvoiceCode' },
                { name: 'Tanggal Kwitansi', cellFilter: 'date:\'dd-MM-yyyy\'', field: 'ProformaInvoiceDate', width: '15%' },
                { name: 'Tipe Approval', field:'ApprovalCategoryName'},
                { name: 'Status Approval', field: 'StatusApprovalName' }

            ]
        };

        $scope.gridPI = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            showColumnFooter: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 15,
            columnDefs: [
                { name: 'Nama Pelanggan', field: 'CustomerName', width: '20%', enableCellEdit: false },
                { name: 'Billing No', field: 'BillingCode', width: '20%', enableCellEdit: false },
                { name: 'No SPK', field: 'FormSPKNo', enableCellEdit: false },
                {
                    name: 'Nominal Tagihan', field: 'Invoice', enableCellEdit: true,
                    cellFilter: 'currency:"Rp."',
                    type: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    footerCellFilter: 'currency:"Rp."',
                    aggregationHideLabel: true,
                    customtreeAggregationFinalizerFn: function (aggregation) {
                        aggregation.rendered = 'TotalTagihan : ' + aggregation.value
                    }
                },
                { name: 'Sisa Tagihan', field: 'SisaTagihan', enableCellEdit: false },
                { name: 'Outstanding AR', field: 'OutStandingAR', enableCellEdit: false },
            ]
        };


        $scope.grid.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });

            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                $scope.selctedRow = gridApi.selection.getSelectedRows();
            });
        };
    });
