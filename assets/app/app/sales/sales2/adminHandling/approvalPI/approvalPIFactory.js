angular.module('app')
  .factory('ApprovalPIFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
          for(key in filter){
            if(filter[key]==null || filter[key]==undefined)
              delete filter[key];
          }
          var res=$http.get('/api/sales/AdminHandlingApprovalProformaInvoice/?start=1&limit=100&'+$httpParamSerializer(filter));
          return res;
      },

      getStatusApproval:function(){
        var StatusApproval = $http.get('/api/sales/PStatusApproval/'); 
        return StatusApproval;
      },

      savePIApprove: function(DataPIApprove) {
        return $http.put('/api/sales/AdminHandlingApprovalProformaInvoice/Approve', DataPIApprove);
      },

      savePIReject: function(DataPIReject) {
        return $http.put('/api/sales/AdminHandlingApprovalProformaInvoice/Reject', DataPIReject);
      },

      closePO: function(param){
        return $http.put('/api/sales/AdminHandlingPOMaintenance/Close' + param);
      },

      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });




