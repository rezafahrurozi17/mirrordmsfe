angular.module('app')
  .factory('MaintainPpatkFactory', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
        var param = $httpParamSerializer(filter);
		var res=$http.get('/api/sales/AdminHandlingPPATK/?'+param);
		//var res=$http.get('/api/sales/CCustomerReligion');
        return res;
      },
	  getDataBuat: function(param) {
		var res=$http.get('/api/sales/AdminHandlingPPATK/New/?'+param);
        return res;
      },
	  getDataInfoBilling: function(da_tanggal,CustomerTypeId,KTPKITAS,SIUP) {
		var res=$http.get('/api/sales/AdminHandlingPPATK/BillingList?BillingDate='+da_tanggal+'&CustomerTypeId='+CustomerTypeId+'&KTPKITAS='+KTPKITAS+'&SIUP='+SIUP);
        return res;
      },
	  getDataUbah: function(param) {
		var res=$http.get('/api/sales/AdminHandlingPPATK/?'+param);
        return res;
      },
	  
	  DonePpatk: function(param) {
		var res=$http.put('/api/sales/AdminHandlingPPATK/Done?PPATKId='+param);
        return res;
      },
	  
      create: function(MaintainPpatk) {
        return $http.post('/api/sales/AdminHandlingPPATK', [MaintainPpatk]);
      },
      update: function(MaintainPpatk){
        return $http.put('/api/sales/AdminHandlingPPATK', [MaintainPpatk]);
      },
	  
	  GetOptionsGelar: function() {
		var res=$http.get('/api/sales/AdminHandlingPPATK/Gelar');
        return res;
      },
	  
	  GetOptionsJenisLaporan: function () {
        
        var da_json = [
          { JenisLaporanId: 1, JenisLaporanName: "Baru" },
		  { JenisLaporanId: 2, JenisLaporanName: "Koreksi" },
        ];

        var res=da_json

        return res;
      },
	  
	  GetOptionsIdentitas: function () {
        
        var da_json = [
          { CustomerTypeId: 1, CustomerTypeName: "Perorangan" },
		  { CustomerTypeId: 2, CustomerTypeName: "Korporasi" },
        ];

        var res=da_json

        return res;
      },
	  
	  GetOptionsJenisDokumenIdentitas: function () {
        
        // var da_json = [
          // { POrDokumenIdentitasTypeId: 1, POrDokumenIdentitasTypeName: "KTP" },
		  // { POrDokumenIdentitasTypeId: 2, POrDokumenIdentitasTypeName: "SIM" },
		  // { POrDokumenIdentitasTypeId: 3, POrDokumenIdentitasTypeName: "Paspor" },
		  // { POrDokumenIdentitasTypeId: 4, POrDokumenIdentitasTypeName: "Lainnya" },
        // ];
        // var res=da_json
		var res=$http.get('/api/sales/AdminHandlingPPATK/JenisDokumenIdentitas');
        return res;

      },
	  
	  GetOptionsKewarganegaraan: function () {
        
        // var da_json = [
          // { POrKewarganegaraanTypeId: 1, POrKewarganegaraanTypeName: "WNI" },
		  // { POrKewarganegaraanTypeId: 2, POrKewarganegaraanTypeName: "WNA" },
        // ];

        // var res=da_json;
		var res=$http.get('/api/sales/AdminHandlingPPATK/Kewarganegaraan');

        return res;
      },
	  
	  GetOptionsInstrumentPembayaran: function () {
        
        // var da_json = [
          // { InstrumentPembayaranId: 1, InstrumentPembayaranName: "Uang Tunai" },
		  // { InstrumentPembayaranId: 2, InstrumentPembayaranName: "Non Tunai" },
        // ];

        // var res=da_json
		var res=$http.get('/api/sales/AdminHandlingPPATK/InstrumenPembayaran');

        return res;
      },
	  
	  GetOptionsCaraPembelian: function () {
        
        var da_json = [
          { CaraPembelianId: 1, CaraPembelianName: "Tunai" },
		  { CaraPembelianId: 2, CaraPembelianName: "Tunai Bertahap" },
        ];

        var res=da_json

        return res;
      },
	  
	  GetOptionsTipeTransaksi: function () {
        
        // var da_json = [
          // { TipeTransaksiId: 1, TipeTransaksiName: "Pembelian Properti" },
		  // { TipeTransaksiId: 2, TipeTransaksiName: "Pembelian Kendaraan Bermotor" },
		  // { TipeTransaksiId: 3, TipeTransaksiName: "Pembelian Permata dan Perhiasan/Logam Mulia" },
		  // { TipeTransaksiId: 4, TipeTransaksiName: "Pembelian Barang Seni dan Antik" },
		  // { TipeTransaksiId: 5, TipeTransaksiName: "Lelang" },
        // ];

        // var res=da_json
		var res=$http.get('/api/sales/AdminHandlingPPATK/TipeTransaksi');
        return res;
      },
	  
	  GetOptionsBeneficialOwner: function () {
        
        var da_json = [
          { KorpBeneficialOwnerExistId: 1, KorpBeneficialOwnerExistName: "Ada" },
		  { KorpBeneficialOwnerExistId: 2, KorpBeneficialOwnerExistName: "Tidak Ada" },

        ];

        var res=da_json

        return res;
      },
	  
	  GetOptionsBadanUsahaKorporasi: function () {
        
        // var da_json = [
          // { KorpBentukBadanUsahaId: 1, KorpBentukBadanUsahaName: "PT" },
		  // { KorpBentukBadanUsahaId: 2, KorpBentukBadanUsahaName: "CV" },
		  // { KorpBentukBadanUsahaId: 2, KorpBentukBadanUsahaName: "Lainnya" },

        // ];
        // var res=da_json
		var res=$http.get('/api/sales/AdminHandlingPPATK/BentukBadanUsahaKorporasi');
        return res;
      },
	  
	  getDataBank: function() {
            var res=$http.get('/api/sales/AdminHandlingBank');
            return res;
        },
	  
      delete: function(id) {
        return $http.delete('/api/sales/CCustomerReligion',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd