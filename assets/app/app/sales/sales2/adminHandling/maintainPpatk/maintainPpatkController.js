angular.module('app')
    .controller('MaintainPpatkController', function($scope, $http, CurrentUser, MaintainPpatkFactory,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mMaintainPpatk = null; //Model
	$scope.xRole={selected:[]};
	console.log('$scope.user',$scope.user);
	
	$scope.dateOptionsStart = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    };
	
	$scope.dateOptionsStartMaintainPpatk = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    };

    $scope.dateOptionsEndMaintainPpatk = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1,
    }
	
	MaintainPpatkFactory.getDataBank().then(
            function(res) {
                $scope.getDataBankMaintainPpatk = res.data.Result;
                return res.data;
            }
        );
		
	MaintainPpatkFactory.GetOptionsGelar().then(
            function(res) {
                $scope.getDataGelarMaintainPpatk = res.data.Result;
                return res.data;
            }
        );
	
	MaintainPpatkFactory.GetOptionsJenisDokumenIdentitas().then(
            function(res) {
                $scope.OptionsJenisDokumenIdentitas_MaintainPpatk = res.data.Result;
                return res.data;
            }
        );
	
	MaintainPpatkFactory.GetOptionsKewarganegaraan().then(
            function(res) {
                $scope.OptionsKewarganegaraan_MaintainPpatk = res.data.Result;
                return res.data;
            }
        );
		
	MaintainPpatkFactory.GetOptionsInstrumentPembayaran().then(
            function(res) {
                $scope.OptionsInstrumentPembayaran_MaintainPpatk = res.data.Result;
                return res.data;
            }
        );
		
	MaintainPpatkFactory.GetOptionsTipeTransaksi().then(
            function(res) {
                $scope.OptionsTipeTransaksi_MaintainPpatk = res.data.Result;
                return res.data;
            }
        );

	MaintainPpatkFactory.GetOptionsBadanUsahaKorporasi().then(
            function(res) {
                $scope.OptionsBadanUsahaKorporasi_MaintainPpatk = res.data.Result;
                return res.data;
            }
        );
	
	$scope.filter={ BillingStartDate:null, BillingEndDate:null};
	var Da_Today_Date=new Date();
	$scope.filter.BillingStartDate=new Date(Da_Today_Date.setDate(1));
	$scope.filter.BillingEndDate=new Date();
	
	$scope.MainMenuMaintainPpatk=true;
	$scope.TambahMaintainPpatk=false;
	$scope.MaintainPpatkOperation="";
	
	$scope.OptionsJenisLaporan_MaintainPpatk = MaintainPpatkFactory.GetOptionsJenisLaporan();
	$scope.OptionsIdentitas_MaintainPpatk = MaintainPpatkFactory.GetOptionsIdentitas();
	
	
	
	$scope.OptionsCaraPembelian_MaintainPpatk = MaintainPpatkFactory.GetOptionsCaraPembelian();
	
	$scope.OptionsBeneficialOwner_MaintainPpatk = MaintainPpatkFactory.GetOptionsBeneficialOwner();
	
	
    //----------------------------------
    // Get Data test
    //----------------------------------
     $scope.getData = function() {
        MaintainPpatkFactory.getData($scope.filter).then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                //return res.data;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }
	
	$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalInfoBillingMaintainPpatk').remove();
		});

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.MaintainPpatkDownloadXml = function(barang){
			var xmltext = "<ltkt>";
			
				xmltext+="<localId>";
				xmltext+="</localId>";
			
				xmltext+="<umum>";
					xmltext+="<tglLaporan>"+barang.TanggalPelaporan;
					xmltext+="</tglLaporan>";
					xmltext+="<namaPejabat>"+barang.NamaPBJ;
					xmltext+="</namaPejabat>";
					xmltext+="<laporan>";
					xmltext+="</laporan>";
					xmltext+="<noLtktKoreksi>"+barang.LTKTNo;
					xmltext+="</noLtktKoreksi>";
					xmltext+="<informasiLain>";
					xmltext+="</informasiLain>";
				xmltext+="</umum>";
				
				xmltext+="<perorangan>";
					xmltext+="<kepemilikan>";
					xmltext+="</kepemilikan>";
					
					// Informasi umum pihak terlapor perorangan sesuai dokumen identitas
					xmltext+="<noRekening>"+barang.NoRekening;
					xmltext+="</noRekening>";
					xmltext+="<gelar>"+barang.POrGelar;
					xmltext+="</gelar>";
					xmltext+="<namaLengkap>"+barang.POrNamaLengkap;
					xmltext+="</namaLengkap>";
					xmltext+="<tempatLahir>"+barang.POrBirthPlace;
					xmltext+="</tempatLahir>";
					xmltext+="<tglLahir>"+barang.POrBirthDate;
					xmltext+="</tglLahir>";
					xmltext+="<wargaNegara>"+barang.POrKewarganegaraanTypeId;
					xmltext+="</wargaNegara>";
					xmltext+="<idNegaraAsal>"+barang.POrNegaraAsalKodeNegara;
					xmltext+="</idNegaraAsal>";
					
					//Alamat domisili/tempat tinggal pihak terlapor perorangan
					xmltext+="<namaJalan>"+barang.POrDomAlamat;
					xmltext+="</namaJalan>";
					xmltext+="<rt>"+barang.POrDomRT;
					xmltext+="</rt>";
					xmltext+="<rw>"+barang.POrDomRW;
					xmltext+="</rw>";
					xmltext+="<idKelurahan>"+barang.POrDomKelurahanCode;
					xmltext+="</idKelurahan>";
					xmltext+="<idKecamatan>"+barang.POrDomKecamatanCode;
					xmltext+="</idKecamatan>";
					xmltext+="<kodePos>"+barang.POrDomKodePos;
					xmltext+="</kodePos>";
					xmltext+="<idKotaKab>"+barang.POrDomKotaKabCode;
					xmltext+="</idKotaKab>";
					xmltext+="<idPropinsi>"+barang.POrDomProvinsiCode;
					xmltext+="</idPropinsi>";
					xmltext+="<idNegara>"+barang.POrKewarganegaraanTypeId;
					xmltext+="</idNegara>";
					
					//Alamat sesuai dokumen identitas pihak terlapor perorangan untuk WNI (elemen <wargaNegara> = 1)
					xmltext+="<namaJalanIdentitas>"+barang.POrDokIdAlamat;
					xmltext+="</namaJalanIdentitas>";
					xmltext+="<rtIdentitas>"+barang.POrDokIdRT;
					xmltext+="</rtIdentitas>";
					xmltext+="<rwIdentitas>"+barang.POrDokIdRW;
					xmltext+="</rwIdentitas>";
					xmltext+="<idKelurahanIdentitas>"+barang.POrDokIdKelurahanCode;
					xmltext+="</idKelurahanIdentitas>";
					xmltext+="<idKecamatanIdentitas>"+barang.POrDokIdKecamatanCode;
					xmltext+="</idKecamatanIdentitas>";
					xmltext+="<kodePosIdentitas>"+barang.POrDokIdKodePos;
					xmltext+="</kodePosIdentitas>";
					xmltext+="<idKotaKabIdentitas>"+barang.POrDokIdKotaKabId;
					xmltext+="</idKotaKabIdentitas>";
					xmltext+="<idPropinsiIdentitas>"+barang.POrDokIdProvinsiCode;
					xmltext+="</idPropinsiIdentitas>";
					xmltext+="<idNegaraIdentitas>";
					xmltext+="</idNegaraIdentitas>";

					//Alamat di negara asal pihak terlapor perorangan untuk WNA (elemen <wargaNegara> = 2).
					xmltext+="<namaJalanNegaraAsal>"+barang.POrNegaraAsalAlamat;
					xmltext+="</namaJalanNegaraAsal>";
					xmltext+="<kodePosNegaraAsal>"+barang.POrNegaraAsalKodePos;
					xmltext+="</kodePosNegaraAsal>";
					xmltext+="<kotaNegaraAsal>"+barang.POrNegaraAsalKotaKabName;
					xmltext+="</kotaNegaraAsal>";
					xmltext+="<propinsiNegaraAsal>"+barang.POrNegaraAsalProvinsiName;
					xmltext+="</propinsiNegaraAsal>";
					xmltext+="<negaraAsal>"+barang.POrNegaraAsalNamaNegara;
					xmltext+="</negaraAsal>";

					// Dokumen identitas pihak terlapor perorangan. Salah satu elemen identitas <ktp> atau <sim> atau <passport> atau -->		
					//<kimsKitasKitap> atau <buktiLain> wajib diisi.
					if(barang.POrNomorIdentitasTypeId==1)
					{
						xmltext+="<ktp>"+barang.POrNomorIdentitas;
						xmltext+="</ktp>";
						xmltext+="<sim>";
						xmltext+="</sim>";
						xmltext+="<passport>";
						xmltext+="</passport>";
						xmltext+="<kimsKitasKitap>";
						xmltext+="</kimsKitasKitap>";
						xmltext+="<buktiLain>";
						xmltext+="</buktiLain>";
						xmltext+="<noBuktiLain>";
						xmltext+="</noBuktiLain>";
					}
					else if(barang.POrNomorIdentitasTypeId==2)
					{
						xmltext+="<ktp>";
						xmltext+="</ktp>";
						xmltext+="<sim>"+barang.POrNomorIdentitas;
						xmltext+="</sim>";
						xmltext+="<passport>";
						xmltext+="</passport>";
						xmltext+="<kimsKitasKitap>";
						xmltext+="</kimsKitasKitap>";
						xmltext+="<buktiLain>";
						xmltext+="</buktiLain>";
						xmltext+="<noBuktiLain>";
						xmltext+="</noBuktiLain>";
					}
					else if(barang.POrNomorIdentitasTypeId==3)
					{
						xmltext+="<ktp>";
						xmltext+="</ktp>";
						xmltext+="<sim>";
						xmltext+="</sim>";
						xmltext+="<passport>"+barang.POrNomorIdentitas;
						xmltext+="</passport>";
						xmltext+="<kimsKitasKitap>";
						xmltext+="</kimsKitasKitap>";
						xmltext+="<buktiLain>";
						xmltext+="</buktiLain>";
						xmltext+="<noBuktiLain>";
						xmltext+="</noBuktiLain>";
					}
					else if(barang.POrNomorIdentitasTypeId==4)
					{
						xmltext+="<ktp>";
						xmltext+="</ktp>";
						xmltext+="<sim>";
						xmltext+="</sim>";
						xmltext+="<passport>";
						xmltext+="</passport>";
						xmltext+="<kimsKitasKitap>";
						xmltext+="</kimsKitasKitap>";
						xmltext+="<buktiLain>"+barang.POrDokumenIdentitasTypeNameOther;
						xmltext+="</buktiLain>";
						xmltext+="<noBuktiLain>"+barang.POrNomorIdentitas;
						xmltext+="</noBuktiLain>";
					}

					xmltext+="<npwp>";
					xmltext+="</npwp>";
		
					//Pekerjaan pihak terlapor perorangan
					xmltext+="<pekerjaanUtama>"+barang.POrPekerjaanName;
					xmltext+="</pekerjaanUtama>";
					xmltext+="<jabatan>";
					xmltext+="</jabatan>";
					xmltext+="<penghasilan>";
					xmltext+="</penghasilan>";
					xmltext+="<tempatKerja>";
					xmltext+="</tempatKerja>";

					//Elemen-elemen di bawah ini diisi dengan teks bebas jika elemen-elemen diatas yang bersesuaian diiisi dengan kode -->		
					//yang dideskripsikan sebagai kode "teks bebas". Kode teks bebas dapat dilihat pada tabel referensi masing-masing, -->		
					//yang diawali dengan digit/angka 9.  -->
					xmltext+="<negaraAsalLain>";
					xmltext+="</negaraAsalLain>";
					xmltext+="<kotaDomisiliLain>";
					xmltext+="</kotaDomisiliLain>";
					xmltext+="<provDomisiliLain>";
					xmltext+="</provDomisiliLain>";
					xmltext+="<tempatKerja>";
					xmltext+="</tempatKerja>";
					xmltext+="<kotaIdLain>";
					xmltext+="</kotaIdLain>";
					xmltext+="<provIdLain>";
					xmltext+="</provIdLain>";
					xmltext+="<pekerjaanLain>";
					xmltext+="</pekerjaanLain>";

				xmltext+="</perorangan>";
				
				xmltext+="<korporasi>";
					xmltext+="<kepemilikan>";
					xmltext+="</kepemilikan>";
					xmltext+="<noRekening>"+barang.NoRekening;
					xmltext+="</noRekening>";
					xmltext+="<namaKorporasi>"+barang.KorpNama;
					xmltext+="</namaKorporasi>";
					xmltext+="<bentukBadan>"+barang.KorpBentukBadanUsahaId;
					xmltext+="</bentukBadan>";
					xmltext+="<bidangUsaha>"+barang.KorpBidangUsahaCode;
					xmltext+="</bidangUsaha>";
					xmltext+="<bidangUsahaLain>";
					xmltext+="</bidangUsahaLain>";
					xmltext+="<npwp>";
					xmltext+="</npwp>";
					
					//Alamat domisili/tempat usaha pihak terlapor korporasi
					xmltext+="<isLn>";
					xmltext+="</isLn>";
					xmltext+="<namaJalan>"+barang.KorpAlamat;
					xmltext+="</namaJalan>";
					xmltext+="<rt>"+barang.KorpRT;
					xmltext+="</rt>";
					xmltext+="<rw>"+barang.KorpRW;
					xmltext+="</rw>";
					xmltext+="<idKelurahan>"+barang.KorpKelurahanCode;
					xmltext+="</idKelurahan>";
					xmltext+="<idKecamatan>"+barang.KorpKecamatanCode;
					xmltext+="</idKecamatan>";
					xmltext+="<kodePos>"+barang.KorpKodePos;
					xmltext+="</kodePos>";
					xmltext+="<idKotaKab>"+barang.KorpKotaKabCode;
					xmltext+="</idKotaKab>";
					xmltext+="<idPropinsi>"+barang.KorpProvinsiCode;
					xmltext+="</idPropinsi>";
					xmltext+="<idNegara>"+barang.KorpNamaNegara;
					xmltext+="</idNegara>";

	
					//Alamat domisili/tempat usaha di luar negeri pihak terlapor korporasi
					xmltext+="<cityLn>"+barang.KorpNegaraAsalKotaKabName;
					xmltext+="</cityLn>";
					xmltext+="<stateLn>"+barang.KorpNegaraAsalProvinsiName;
					xmltext+="</stateLn>";
						
		
					//Elemen-elemen di bawah ini diisi dengan teks bebas jika elemen-elemen diatas yang bersesuaian diiisi dengan kode -->		
					//yang dideskripsikan sebagai kode "teks bebas". Kode teks bebas dapat dilihat pada tabel referensi masing-masing, -->		
					//yang diawali dengan digit/angka 9.  -->
					xmltext+="<kotaLain>";
					xmltext+="</kotaLain>";
					xmltext+="<provLain>";
					xmltext+="</provLain>";
					xmltext+="<negaraLain>";
					xmltext+="</negaraLain>";

				xmltext+="</korporasi>";
				
				xmltext+="<transaksi>";
					xmltext+="<tglTransaksi>"+barang.TransactionDate;
					xmltext+="</tglTransaksi>";
					xmltext+="<pjkTempatKejadian>";
					xmltext+="</pjkTempatKejadian>";
					xmltext+="<idKotaKab>";
					xmltext+="</idKotaKab>";
					xmltext+="<idPropinsi>";
					xmltext+="</idPropinsi>";

					//Elemen-elemen di bawah ini diisi dengan teks bebas jika elemen-elemen diatas yang bersesuaian diiisi dengan kode -->		
					//yang dideskripsikan sebagai kode "teks bebas". Kode teks bebas dapat dilihat pada tabel referensi masing-masing, -->		
					//dimana biasanya diawali dengan digit/angka 9.
					xmltext+="<kotaLain>";
					xmltext+="</kotaLain>";
					xmltext+="<provLain>";
					xmltext+="</provLain>";
					xmltext+="<stat>";
					xmltext+="</stat>";
					xmltext+="<noRekening>";
					xmltext+="</noRekening>";
		
					//Elemen <detailTransaksi> berisi satu TKT, baik dalam mata uang rupiah maupun dalam mata uang asing. -->		
					//Satu elemen <transaksi> setidaknya harus memuat satu elemen <detailTransaksi>.         -->		
					xmltext+="<detailTransaksi>";
						xmltext+="<idMataUang>";
						xmltext+="</idMataUang>";
						xmltext+="<nilaiTransaksi>";
						xmltext+="</nilaiTransaksi>";
						xmltext+="<kurs>";
						xmltext+="</kurs>";
						xmltext+="<jumlahValas>";
						xmltext+="</jumlahValas>";
					xmltext+="</detailTransaksi>";
					
					//Identitas pihak perorangan yang terkait dengan terlapor. -->		
					xmltext+="<terkaitPerorangan>";	
		
						//Informasi umum pihak perorangan yang terkait dengan terlapor sesuai dokumen identitas. -->	
						xmltext+="<gelar>";
						xmltext+="</gelar>";
						xmltext+="<namaLengkap>";
						xmltext+="</namaLengkap>";
						xmltext+="<tempatLahir>";
						xmltext+="</tempatLahir>";
						xmltext+="<tglLahir>";
						xmltext+="</tglLahir>";
						xmltext+="<wargaNegara>";
						xmltext+="</wargaNegara>";
						xmltext+="<idNegaraAsal>";
						xmltext+="</idNegaraAsal>";	
		
						//Alamat domisili/tempat tinggal pihak perorangan yang terkait dengan terlapor. -->	
						xmltext+="<namaJalan>";
						xmltext+="</namaJalan>";
						xmltext+="<rt>";
						xmltext+="</rt>";
						xmltext+="<rw>";
						xmltext+="</rw>";
						xmltext+="<idKecamatan>";
						xmltext+="</idKecamatan>";
						xmltext+="<idKelurahan>";
						xmltext+="</idKelurahan>";
						xmltext+="<kodePos>";
						xmltext+="</kodePos>";
						xmltext+="<idKotaKab>";
						xmltext+="</idKotaKab>";
						xmltext+="<idPropinsi>";
						xmltext+="</idPropinsi>";
						xmltext+="<idNegara>";
						xmltext+="</idNegara>";	
							
						//Alamat sesuai dokumen identitas pihak perorangan yang terkait dengan terlapor untuk WNI. -->	
						xmltext+="<namaJalanIdentitas>";
						xmltext+="</namaJalanIdentitas>";	
						xmltext+="<rtIdentitas>";
						xmltext+="</rtIdentitas>";	
						xmltext+="<rwIdentitas>";
						xmltext+="</rwIdentitas>";	
						xmltext+="<idNegaraIdentitas>";
						xmltext+="</idNegaraIdentitas>";	
						xmltext+="<idPropinsiIdentitas>";
						xmltext+="</idPropinsiIdentitas>";	
						xmltext+="<idKotaKabIdentitas>";
						xmltext+="</idKotaKabIdentitas>";	
						xmltext+="<idKecamatanIdentitas>";
						xmltext+="</idKecamatanIdentitas>";	
						xmltext+="<idKelurahanIdentitas>";
						xmltext+="</idKelurahanIdentitas>";
						xmltext+="<kodePosIdentitas>";
						xmltext+="</kodePosIdentitas>";	
							
						//Alamat di negara asal pihak perorangan yang terkait dengan terlapor untuk WNA. -->	
						xmltext+="<namaJalanNegaraAsal>";
						xmltext+="</namaJalanNegaraAsal>";	
						xmltext+="<kodePosNegaraAsal>";
						xmltext+="</kodePosNegaraAsal>";	
						xmltext+="<kotaNegaraAsal>";
						xmltext+="</kotaNegaraAsal>";	
						xmltext+="<propinsiNegaraAsal>";
						xmltext+="</propinsiNegaraAsal>";	
						xmltext+="<negaraAsal>";
						xmltext+="</negaraAsal>";	
							
						//Dokumen identitas pihak perorangan yang terkait dengan terlapor. Salah satu elemen identitas <ktp> atau <sim>  -->	
						//atau <passport> atau <kimsKitasKitap> atau <buktiLain> wajib diisi.      -->	
						xmltext+="<ktp>";
						xmltext+="</ktp>";
						xmltext+="<sim>";
						xmltext+="</sim>";
						xmltext+="<passport>";
						xmltext+="</passport>";
						xmltext+="<kimsKitasKitap>";
						xmltext+="</kimsKitasKitap>";
						xmltext+="<buktiLain>";
						xmltext+="</buktiLain>";
						xmltext+="<noBuktiLain>";
						xmltext+="</noBuktiLain>";
						xmltext+="<npwp>";
						xmltext+="</npwp>";
							
						//Pekerjaan pihak perorangan yang terkait dengan terlapor. -->	
						xmltext+="<pekerjaanUtama>";
						xmltext+="</pekerjaanUtama>";
						xmltext+="<jabatan>";
						xmltext+="</jabatan>";
						xmltext+="<penghasilan>";
						xmltext+="</penghasilan>";
						xmltext+="<tempatKerja>";
						xmltext+="</tempatKerja>";
							
						//Informasi transaksi yang terkait dengan terlapor. -->	
						xmltext+="<tujuanTransaksi>";
						xmltext+="</tujuanTransaksi>";
						xmltext+="<sumberDana>";
						xmltext+="</sumberDana>";
						xmltext+="<namaBankLain>";
						xmltext+="</namaBankLain>";
						xmltext+="<noRekeningTujuan>";
						xmltext+="</noRekeningTujuan>";
							
						//Elemen-elemen di bawah ini diisi dengan teks bebas jika elemen-elemen diatas yang bersesuaian diiisi dengan kode -->	
						//yang dideskripsikan sebagai kode "teks bebas". Kode teks bebas dapat dilihat pada tabel referensi masing-masing, -->	
						//yang diawali dengan digit/angka 9.  -->	
						xmltext+="<negaraAsalLain>";
						xmltext+="</negaraAsalLain>";
						xmltext+="<kotaDomisiliLain>";
						xmltext+="</kotaDomisiliLain>";
						xmltext+="<provDomisiliLain>";
						xmltext+="</provDomisiliLain>";
						xmltext+="<kotaIdLain>";
						xmltext+="</kotaIdLain>";
						xmltext+="<provIdLain>";
						xmltext+="</provIdLain>";
						xmltext+="<pekerjaanLain>";
						xmltext+="</pekerjaanLain>";
	
					xmltext+="</terkaitPerorangan>";

					xmltext+="<terkaitKorporasi>";
						
						//Informasi umum pihak korporasi yang terkait dengan terlapor sesuai dokumen perizinan. -->	
						xmltext+="<namaKorporasi>";
						xmltext+="</namaKorporasi>";
						xmltext+="<bentukBadan>";
						xmltext+="</bentukBadan>";
						xmltext+="<bidangUsaha>";
						xmltext+="</bidangUsaha>";
						xmltext+="<bidangUsahaLain>";
						xmltext+="</bidangUsahaLain>";
						xmltext+="<npwp>";
						xmltext+="</npwp>";
							
						// Alamat domisili/tempat usaha pihak korporasi yang terkait dengan terlapor sesuai dokumen perizinan. -->	
						xmltext+="<isLn>";
						xmltext+="</isLn>";
						xmltext+="<namaJalan>";
						xmltext+="</namaJalan>";
						xmltext+="<rt>";
						xmltext+="</rt>";
						xmltext+="<rw>";
						xmltext+="</rw>";
						xmltext+="<idKelurahan>";
						xmltext+="</idKelurahan>";
						xmltext+="<idKecamatan>";
						xmltext+="</idKecamatan>";
						xmltext+="<kodePos>";
						xmltext+="</kodePos>";
						xmltext+="<idKotaKab>";
						xmltext+="</idKotaKab>";
						xmltext+="<idPropinsi>";
						xmltext+="</idPropinsi>";
						xmltext+="<idNegara>";
						xmltext+="</idNegara>";
	
							
						//Alamat domisili/tempat usaha di luar negeri pihak korporasi yang terkait dengan terlapor. -->	
						xmltext+="<cityLn>";
						xmltext+="</cityLn>";
						xmltext+="<stateLn>";
						xmltext+="</stateLn>";
							
						//Informasi transaksi yang terkait dengan terlapor. -->	
						xmltext+="<tujuanTransaksi>";
						xmltext+="</tujuanTransaksi>";
						xmltext+="<sumberDana>";
						xmltext+="</sumberDana>";
						xmltext+="<namaBankLain>";
						xmltext+="</namaBankLain>";
						xmltext+="<noRekeningTujuan>";
						xmltext+="</noRekeningTujuan>";
						
						//Elemen-elemen di bawah ini diisi dengan teks bebas jika elemen-elemen diatas yang bersesuaian diiisi dengan kode -->	
						//yang dideskripsikan sebagai kode "teks bebas". Kode teks bebas dapat dilihat pada tabel referensi masing-masing, -->	
						//dimana biasanya diawali dengan digit/angka 9.  -->	
						xmltext+="<kotaLain>";
						xmltext+="</kotaLain>";
						xmltext+="<provLain>";
						xmltext+="</provLain>";
						xmltext+="<negaraLain>";
						xmltext+="</negaraLain>";
					xmltext+="</terkaitKorporasi>";


				xmltext+="</transaksi>";
			
			xmltext+="</ltkt>";
			var pom = document.createElement('a');

			var filename = "file.xml";
			var pom = document.createElement('a');
			var bb = new Blob([xmltext], {type: 'text/plain'});

			pom.setAttribute('href', window.URL.createObjectURL(bb));
			pom.setAttribute('download', filename);

			pom.dataset.downloadurl = ['text/plain', pom.download, pom.href].join(':');
			pom.draggable = true; 
			pom.classList.add('dragout');

			pom.click();
        }
	
	$scope.BuatPpatk = function(rows){
		$scope.MaintainPpatkOperation="Insert";
		$scope.MainMenuMaintainPpatk=false;
		$scope.TambahMaintainPpatk=true;
		$scope.MaintainPpatkUbah=false;
		$scope.MaintainPpatkDefaultTab();
		
		var da_date=rows.TransactionDate.getFullYear()+'-'+('0' + (rows.TransactionDate.getMonth()+1)).slice(-2)+'-'+('0' + rows.TransactionDate.getDate()).slice(-2); 
		
		var da_param='CustomerTypeId='+rows.CustomerTypeId+'&KTP='+rows.KTPKITAS+'&SIUP='+rows.SIUP+'&TransactionDate='+da_date;
		
		MaintainPpatkFactory.getDataBuat(da_param).then(function(res){
            $scope.ViewData=angular.copy(res.data.Result[0]);
        });
		
	}
	
	$scope.UbahPpatk = function(rows){
		$scope.MaintainPpatkOperation="Update";
		$scope.MainMenuMaintainPpatk=false;
		$scope.TambahMaintainPpatk=true;
		$scope.MaintainPpatkUbah=true;
		$scope.MaintainPpatkDefaultTab();
		
		var da_date=rows.TransactionDate.getFullYear()+'-'+('0' + (rows.TransactionDate.getMonth()+1)).slice(-2)+'-'+('0' + rows.TransactionDate.getDate()).slice(-2); 
		
		var da_param='PPATKId='+rows.PPATKId;
		
		MaintainPpatkFactory.getDataUbah(da_param).then(function(res){
            $scope.ViewData=angular.copy(res.data.Result[0]);
        });
		
	}
	
	$scope.MaintainPpatkIdentitasBrubah = function(selected){
		$scope.MaintainPpatkDefaultTab();
		
	}
	
	$scope.MaintainPpatkSimpan = function(){
		if($scope.ViewData.POrKewarganegaraanTypeId==1){
			$scope.ViewData.POrNegaraAsalAlamat=null;
			$scope.ViewData.POrNegaraAsalKodeNegara=null;
			$scope.ViewData.POrNegaraAsalNamaNegara=null;
			$scope.ViewData.POrNegaraAsalProvinsiName=null;
			$scope.ViewData.POrNegaraAsalKotaKabName=null;
			$scope.ViewData.POrNegaraAsalKodePos=null;
			$scope.ViewData.POrBirthPlace=null;
			$scope.ViewData.POrBirthDate=null;
			$scope.ViewData.POrPekerjaanCode=null;
			$scope.ViewData.POrPekerjaanName=null;
			$scope.ViewData.POrTelpArea1=null;
			$scope.ViewData.POrTelp1=null;
			$scope.ViewData.POrTelpArea2=null;
			$scope.ViewData.POrTelp2=null;
			$scope.ViewData.POrTelpArea3=null;
			$scope.ViewData.POrTelp3=null;
		}
		
		$scope.ViewData.TransactionDate=$scope.ViewData.TransactionDate.getFullYear()+'-'
												  +('0' + ($scope.ViewData.TransactionDate.getMonth()+1)).slice(-2)+'-'
												  +('0' + $scope.ViewData.TransactionDate.getDate()).slice(-2)+'T'
												  +(($scope.ViewData.TransactionDate.getHours()<'10'?'0':'')+ $scope.ViewData.TransactionDate.getHours())+':'
												  +(($scope.ViewData.TransactionDate.getMinutes()<'10'?'0':'')+ $scope.ViewData.TransactionDate.getMinutes())+':'
												  +(($scope.ViewData.TransactionDate.getSeconds()<'10'?'0':'')+ $scope.ViewData.TransactionDate.getSeconds());
		
		if($scope.MaintainPpatkOperation=="Insert")
		{
			MaintainPpatkFactory.create($scope.ViewData).then(
				function(res) {
					MaintainPpatkFactory.getData($scope.filter).then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							$scope.MaintainPpatkKembaliKeMain();
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);
				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		else if($scope.MaintainPpatkOperation=="Update")
		{
			MaintainPpatkFactory.update($scope.ViewData).then(
				function(res) {
					MaintainPpatkFactory.getData($scope.filter).then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							$scope.MaintainPpatkKembaliKeMain();
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);
				},            
						function(err){
							
							bsNotify.show(
								{
									title: "Gagal",
									content: err.data.Message,
									type: 'danger'
								}
							);
						}
			)
		}
		
	}
	
	$scope.MaintainPpatkDefaultTab = function(){
		angular.element("#MaintainPpatk_IdentitasPenggunaJasa_Perorangan").removeClass("active").addClass("");
        angular.element("#MaintainPpatk_RincianTransaksiDanMetodePembayaran").removeClass("active").addClass("");
		angular.element("#MaintainPpatk_PihakPelapor").removeClass("active").addClass("");
		angular.element("#MaintainPpatk_IdentitasPenggunaJasa_Korporasi").removeClass("active").addClass("");
		angular.element("#MaintainPpatk_PemberiKuasa").removeClass("active").addClass("");
		
		
		angular.element("#menu_MaintainPpatk_IdentitasPenggunaJasa_Perorangan").removeClass("active").addClass("");
        angular.element("#menu_MaintainPpatk_RincianTransaksiDanMetodePembayaran").removeClass("active").addClass("");
		angular.element("#menu_MaintainPpatk_PihakPelapor").removeClass("active").addClass("");
		angular.element("#menu_MaintainPpatk_IdentitasPenggunaJasa_Korporasi").removeClass("active").addClass("");
		angular.element("#menu_MaintainPpatk_PemberiKuasa").removeClass("active").addClass("");
		
		
		angular.element("#MaintainPpatk_RincianTransaksiDanMetodePembayaran").addClass("active");
		angular.element("#menu_MaintainPpatk_RincianTransaksiDanMetodePembayaran").addClass("active");
		navigateTabMaintainPpatk('tabContainerMaintainPpatk','menu_MaintainPpatk_RincianTransaksiDanMetodePembayaran');
	}
	
	$scope.MaintainPpatkInstrumenPembayaranBrubah = function(selected){
		if($scope.ViewData.InstrumentPembayaranId!=2)
		{
			$scope.ViewData.InstrumentPembayaranNonTunaiName=null;
			//$scope.ViewData.InstrumentPembayaranNonTunaiName=null;
			$scope.ViewData.NoRekening=null;
		}
		
		
	}
	
	$scope.MaintainPpatkJenisDokumenIdentitasBrubah_Perorangan = function(selected){
		$scope.MaintainPpatkJenisDokumenIdentitasBrubah_Perorangan_Lainnya_NotSelected=false;
		if(selected.PPATKJenisDokumenIdentitasName!="Lainnya" || (typeof selected.PPATKJenisDokumenIdentitasName=="undefined"))
		{
			$scope.ViewData.POrDokumenIdentitasTypeNameOther=null;
			$scope.MaintainPpatkJenisDokumenIdentitasBrubah_Perorangan_Lainnya_NotSelected=true;
		}
	}
	
	$scope.MaintainPpatkJenisDokumenIdentitasBrubah_Korporasi = function(selected){
		
		$scope.MaintainPpatkJenisDokumenIdentitasBrubah_Korporasi_Lainnya_NotSelected=false;
		if(selected.PPATKJenisDokumenIdentitasName!="Lainnya" || (typeof selected.PPATKJenisDokumenIdentitasName=="undefined"))
		{
			$scope.ViewData.KorpBenOwDokumenIdentitasTypeNameOther=null;
			$scope.MaintainPpatkJenisDokumenIdentitasBrubah_Korporasi_Lainnya_NotSelected=true;
		}
	}
	
	$scope.MaintainPpatkKewarganegaraanBrubah = function(selected){
		if($scope.ViewData.POrKewarganegaraanTypeId!=2)
		{
			$scope.ViewData.POrKodeWNA=null;
			$scope.ViewData.POrNamaWNA=null;
		}
		
		if($scope.ViewData.KorpBenOwKewarganegaraanTypeId!=2)
		{
			$scope.ViewData.KorpBenOwKodeWNA=null;
			$scope.ViewData.KorpBenOwNamaWNA=null;
		}
	}
	
	$scope.MaintainPpatkBadanUsahaKorporasiBrubah = function(selected){
		$scope.MaintainPpatkBadanUsahaKorporasi_Disable=true;
		if(selected.PPATKBentukBadanUsahaKorporasiName=="Lainnya")
		{
			$scope.MaintainPpatkBadanUsahaKorporasi_Disable=false;
		}
	}
	
	$scope.CetakPpatk = function(rows){
	
	}
	
	$scope.DonePpatk = function(rows){
		MaintainPpatkFactory.DonePpatk(rows.PPATKId).then(function(res){
            MaintainPpatkFactory.getData($scope.filter).then(
				function(res){
					bsNotify.show(
						{
							title: "Berhasil",
							content: "PPATK Telah Selesai.",
							type: 'success'
						}
					);
					$scope.grid.data = res.data.Result;
					$scope.loading=false;
					//return res.data;
				},
				function(err){
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
        });
	}
	
	$scope.MaintainPpatkKembaliKeMain = function(){
		$scope.MainMenuMaintainPpatk=true;
		$scope.TambahMaintainPpatk=false;
	}
	
	$scope.PpatkInfoBilling = function(rows){
		setTimeout(function() {
            angular.element('.ui.modal.ModalInfoBillingMaintainPpatk').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalInfoBillingMaintainPpatk').not(':first').remove();  
        }, 1);
		
		console.log("iblis",rows);
		var da_date=rows.TransactionDate.getFullYear()+'-'+('0' + (rows.TransactionDate.getMonth()+1)).slice(-2)+'-'+('0' + rows.TransactionDate.getDate()).slice(-2); 
		MaintainPpatkFactory.getDataInfoBilling(da_date,rows.CustomerTypeId,rows.KTPKITAS,rows.SIUP).then(function(res){
            $scope.InfoBillingGrid_MaintainPpatk.data=res.data.Result;
        });
	}
	
	$scope.BatalInfoBilling_MaintainPpatk = function(){
		angular.element('.ui.modal.ModalInfoBillingMaintainPpatk').modal('hide');
	}
	
    $scope.selectRole = function(rows){
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
	
    $scope.onSelectRows = function(rows){
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Nama Prospect/Pelanggan', field:'Nama' },
			{ name:'Billing Date', field:'TransactionDate',cellTemplate:'<a style="color:blue;"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.$parent.PpatkInfoBilling(row.entity)">{{row.entity.TransactionDate| date:"dd/MM/yyyy"}}</u></p></a>' },
			{ name:'Amount Billing', field:'JumlahNominal' },
			{ name:'Status PPATK', field:'PPATKStatusName' },
			{ name:'Action', visible: true,enableFiltering: false,cellTemplate:'<p style="padding:5px 0 0 5px"><a ng-show="row.entity.StatusPPATK==null" style="color:blue;"><u ng-click="grid.appScope.$parent.BuatPpatk(row.entity)">Buat PPATK</u></a>&nbsp;&nbsp;<a ng-show="row.entity.StatusPPATK==1" style="color:blue;"><u ng-click="grid.appScope.$parent.UbahPpatk(row.entity)">Ubah PPATK</u></a>&nbsp;&nbsp;<a ng-show="row.entity.StatusPPATK==1" style="color:blue;"><u ng-click="grid.appScope.$parent.CetakPpatk(row.entity)">Cetak PPATK</u></a>&nbsp;&nbsp;<a ng-show="row.entity.StatusPPATK==2" style="color:blue;"><u ng-click="grid.appScope.$parent.DonePpatk(row.entity)">Done</u></a></p>' },
        ]
    };
	
	$scope.InfoBillingGrid_MaintainPpatk = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Cabang', field: 'Cabang' },
                { name: 'No Billing', field: 'BillingCode' },
                { displayName:'Total (Termasuk PPN)' ,name: 'Total (Termasuk PPN)', field: 'GrandTotal' , cellFilter: 'currency : \'Rp.\' : fractionSize'}
            ]
        };
});


