angular.module('app')
    .controller('MaintainPOAksesorisStandarController', function($rootScope,$stateParams, PrintRpt, bsNotify, $scope, $http, CurrentUser, MaintainPOAksesorisStandarFactory,GoodReceiptFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.CreateForm = false;
        $scope.MaintainForm = true;
        $scope.showKontak = true;
        $scope.simpanShow = true;
        $scope.cetakShow = true;
        $scope.showHandphone = true;
        //$scope.POTypeId = null;
        $scope.POTypeName = null;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';
        $scope.OperationMaintainPo = "";
        var MaintainPoFrom = "";
		//$scope.gridApiPembelianMaterialMaintainPo="setan";
		
		function fixDate(date) {
            try {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } catch (ex) {
                return null;
            }
        }
		
		
		MaintainPOAksesorisStandarFactory.getMaintainPoQuotationNo().then(
            function(res) {
                $scope.optionMaintainPo_QuotationNo = res.data.Result;
               // return tipe.data;
            }
        );
       
        $scope.pageArray = [10,25,50];
        $scope.filter = {POType: null, PODateStart: $scope.firstDate, PODateEnd: new Date(), POStatus: null};
        //var PoReferenceRaw=[];
        var PoMaterialRaw = [];

        console.log("filter value", $scope.filter);

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = [];
            var mount = new Date();
            $scope.firstDate = new Date(mount.setDate("01"));
            $scope.filter = {POType: null, PODateStart: $scope.firstDate, PODateEnd: new Date(), POStatus: null};
        });

        $scope.$on('$destroy', function() {
            angular.element('.ui.modal.ModalMaintainPoBuatTambahChild').remove();

        });

        $scope.changeTgl = function (tgl) {
            $scope.dateOptionsEnd.minDate = $scope.filter.PODateStart;
            if (tgl == null || tgl == undefined) {
                $scope.filter.PODateEnd = null;
            } else {
                if ($scope.filter.PODateStart < $scope.filter.PODateEnd) {
                } else {
                    $scope.filter.PODateEnd = $scope.filter.PODateStart;
                }
            }
        }

        MaintainPOAksesorisStandarFactory.getTypePOAccStandar().then(
            function(tipe) {
                $scope.getTypePOAccStandar = tipe.data.Result;
                console.log("getTypePOAccStandar",$scope.getTypePOAccStandar)
                $scope.filter.POTypeName = "Aksesoris Standard";
                var filtered = $scope.getTypePOAccStandar.filter(function(item) { 
                    return item.POTypeName == "Aksesoris Standard";
                });
                console.log("filtered",filtered);
                $scope.filter.POType = filtered[0].POTypeId;
                // $scope.filter.POType = 9;
                // $scope.ListPO[0].POTypeName = "Aksesoris Standard";
            //    return tipe.data;
            }
        );

        MaintainPOAksesorisStandarFactory.getDaNoDokumen().then(
            function(NoDokumen) {
                $scope.getNoDokumen = NoDokumen.data.Result;
               // return NoDokumen.data;
            }
        );

        // MaintainPOAksesorisStandarFactory.getPCategoryMaterialType().then(
        // function(ulala){
        // PoMaterialRaw = ulala.data.Result;
        // return ulala.data;
        // }
        // );

        $scope.DaTipePoResetShowAll = function() {
            MaintainPOAksesorisStandarFactory.getTypePOAccStandar().then(
                function(tipe) {
                    $scope.getTypePOAccStandar = tipe.data.Result;
                    //return tipe.data;
                }
            );
        }

        MaintainPOAksesorisStandarFactory.getStatusPOAcc().then(
            function(status) {
                $scope.getStatusPOAcc = status.data.Result;
                console.log("getStatusPOAcc",$scope.getStatusPOAcc);
                $scope.filter.POStatus = 5;
               // return status.data;
            }
        )

        MaintainPOAksesorisStandarFactory.getPaymentType().then(
            function(res) {
                $scope.getPaymentType = res.data.Result;
               // return res.data;
            }
        );

        $scope.unitLihatMaintainPo = function() {
            if ($scope.unitLihatViewMaintainPo == true) {
                $scope.unitLihatViewMaintainPo = false;
            } else {
                $scope.unitLihatViewMaintainPo = true;
            }
        }

        // MaintainPOAksesorisStandarFactory.getNoRangka().then(
            // function(res) {
                // $scope.getNoRangka = res.data.Result;
               // // return res.data;
            // }
        // );

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
		$scope.tab = $stateParams.tab;
		$scope.breadcrums = {};
        $scope.mMaintainPO = null; //Model
        $scope.disabledBtnSimpan = false;
        $scope.disabledButtonSimpanCetak = false;
        //$scope.xRole = { selected: [] };
        //$scope.filter = { PODateStart: null, PODateEnd: null, POType: null, POStatus: null };

        $scope.dateOptionsStart = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1
        };

        // $scope.dateOptions = {
        //      minDate = new Date()
        // };

        $scope.dateOptionsEnd = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
        };

        $scope.dateOptionPengiriman = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
        };


        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            alerts = [];
            $scope.gridData = [];
            if ($scope.filter.POType == null || $scope.filter.PODateStart == null || $scope.filter.PODateEnd == null) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Filter Tidak Boleh Kosong.!",
                    type: 'warning'
                });
            } else {

                try {
                    var da_PODateStart = $scope.filter.PODateStart;
                    $scope.filter.PODateStart = da_PODateStart.getFullYear() + '-' +
                        ('0' + (da_PODateStart.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + da_PODateStart.getDate()).slice(-2);
                } catch (exceptionthingy) {
                    $scope.filter.PODateStart = null;
                }

                try {
                    var da_PODateEnd = $scope.filter.PODateEnd;
                    $scope.filter.PODateEnd = da_PODateEnd.getFullYear() + '-' +
                        ('0' + (da_PODateEnd.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + da_PODateEnd.getDate()).slice(-2);
                } catch (exceptionthingy) {
                    $scope.filter.PODateEnd = null;
                }

                MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
                    function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                       // return res.data.Result;
                    },
                    function(err) {
                        
						bsNotify.show({
							title: "gagal",
							content: err.data.Message,
							type: 'danger'
						});
                    }
                );
            }
        }



        $scope.btnKembaliFormMaintain = function() { 
            $scope.MaintainPoAccView = false;
            $scope.CreateForm = false;
            $scope.MaintainForm = true;
			if(MaintainPoFrom == "SPK")
			{
				if($scope.OperationMaintainPo == "InsertKelar")
				{
					$rootScope.PaksaSearchPrDariPo_PRTypeId=angular.copy($scope.ListPO[0].PRTypeId);
					$rootScope.PaksaSearchPrDariPo_VendorId=angular.copy($scope.ListPO[0].VendorId);
					$rootScope.DariPrKePoAbisInsert=true;
				}
				$rootScope.$emit('KembalidariPOValid', {});
			}
            
            $scope.OperationMaintainPo = "";
            $scope.DaTipePoResetShowAll();
        }

        $scope.PONOClick = function(SelectPO) {
            
			
			
			// $scope.gridPembelianMaterial.columnDefs[5].enableCellEdit=false;
			// $scope.gridPembelianMaterial.columnDefs[6].enableCellEdit=false;
			// $scope.gridApiPembelianMaterialMaintainPo.grid.refresh();
			
			$scope.showPONO = true;
			// // $scope.POTypeId = SelectPO.POTypeId;
            // $scope.POTypeId = $scope.ListPO[7];
            $scope.POTypeName = SelectPO.POTypeName;
			$scope.breadcrums.title="Lihat";
			$scope.OperationMaintainPo = "Lihat";
            $scope.ListPO = [];
            $scope.ListPO.push(SelectPO);
            $scope.showGridPembelianMaterial = false;
            $scope.showGridPembelianMaterialJasa = false;
            console.log("test detail PO", $scope.ListPO);

            $scope.gridListPR.data = $scope.ListPO[0].ListOfPODetailPRView;
            $scope.gridListBirojasa.data = $scope.ListPO[0].ListOfPODetailPRView;
            $scope.gridListDaftarPembelianUnitMaintainPo.data = $scope.ListPO[0].ListOfPODetailPRView;
            $scope.gridPembelianMaterialJasa.data = $scope.ListPO[0].ListOfPODetailSummaryView;
            $scope.gridPembelianMaterial.data = $scope.ListPO[0].ListOfPODetailSummaryView;
            $scope.DisabledViewPO = true;
			$scope.showPOStatus = true;

            $scope.DataReference = []; //'pr'  'buat po'  'semua' 
            var DaMaintainPOReferenceAction = "&POAction=semua";
            var DaMaintainPoParam = "/?POTypeId=" + $scope.ListPO[0].POTypeId + DaMaintainPOReferenceAction;
            MaintainPOAksesorisStandarFactory.getPOReference(DaMaintainPoParam).then(
                function(res) {
                    if(res.data.Result.length!=0)
					{
						for (var i = 0; i < res.data.Result.length; ++i) {
							$scope.DataReference.push(res.data.Result[i]);
						}
					}
					
                }
            );

            $scope.MaintainPoDaKolomBawah();


            var DaPoTypeId = "?PRTypeId=" + SelectPO.POTypeId;
            MaintainPOAksesorisStandarFactory.getVendor(DaPoTypeId).then(
                function(res) {
                    $scope.getVendor = res.data.Result;
                    //console.log("RES", $scope.getVendor);
                }
            );

            MaintainPOAksesorisStandarFactory.getDataDetailForUbah(SelectPO.POId + "&POTypeId=" + SelectPO.POTypeId).then(
                function(res) {
                    $scope.CreateForm = true;
                    $scope.MaintainForm = false;
                    $scope.simpanShow = false;
                    $scope.cetakShow = false;
                    $scope.ListPO = [];
                    $scope.ListPO.push(res.data.Result[0]);
                    console.log($scope.ListPO);
                    $scope.gridListPR.data = $scope.ListPO[0].ListOfPODetailPRView;
                    $scope.gridListBirojasa.data = $scope.ListPO[0].ListOfPODetailPRView;
                    $scope.gridListDaftarPembelianUnitMaintainPo.data = $scope.ListPO[0].ListOfPODetailPRView;
                    $scope.gridPembelianMaterial.data = $scope.ListPO[0].ListOfPODetailSummaryView;
                    $scope.gridPembelianMaterialJasa.data = $scope.ListPO[0].ListOfPODetailSummaryView;
                    $scope.MaintainPoMaeninFieldLagi();
                }
            );
        }
		
		$scope.MaintainPoDaSelectVendor = function(selected) {
			if(MaintainPoFrom == "SPK" && selected!=null && (typeof selected!="undefined"))
			{
				// $scope.gridListBirojasa.data=[];
				$scope.gridPembelianMaterial.data=[];
                var da_shit_so_code='';
                var listacc='';
                var listPRid = '';


                // if ($scope.selectedListPR.length < 1 || $scope.selectedListPR == [] || $scope.selectedListPR == undefined){
                //     bsNotify.show({
                //         title: "Notifikasi",
                //         content: "Anda harus memilih aksesoris terlebih dahulu",
                //         type: 'warning',
                //         timeout: 9000

                //     });
                // }

				//aksesoris
				for(var i = 0; i < $scope.ListPO.length.length; ++i)
				{	
					da_shit_so_code+=""+$scope.ListPO[i].SoCode+" ";
				}
				
				for (var i in $scope.gridListPR.data){
                    listacc+=""+$scope.gridListPR.data[i].AccessoriesId+" ";
                }

                for (var i in $scope.gridListPR.data) {
                    listPRid += "" + $scope.gridListPR.data[i].PRId + " ";
                }

                //OPD
                for(var i = 0; i < $scope.ListPO.length.length; ++i)
				{	
					da_shit_so_code+=""+$scope.ListPO[i].SoCode+" ";
				}
				
				for (var i in $scope.gridListBirojasa.data){
                    listacc+=""+$scope.gridListBirojasa.data[i].AccessoriesId+" ";
                }

                for (var i in $scope.gridListBirojasa.data) {
                    listPRid += "" + $scope.gridListBirojasa.data[i].PRId + " ";
                }

                // // OPD
                // for(var i = 0; i < $scope.ListPO.length.length; ++i)
				// {
                //     da_shit_so_code+=""+$scope.ListPO[i].SoCode+" ";
				// }
                // for (var i in $scope.gridListBirojasa.data){
                //     listacc+=""+$scope.gridListBirojasa.data[i].AccessoriesId+" ";
                // }

                // for (var i in $scope.gridListBirojasa.data) {
                //     listPRid += "" + $scope.gridListBirojasa.data[i].PRId + " ";
                // }



                //galang
                MaintainPOAksesorisStandarFactory.getAbisVendorYangDropdown($scope.ListPO[0].POTypeId, selected, da_shit_so_code, listacc, listPRid).then(
					function(res) {

						//$scope.getVendor = res.data.Result;
						// console.log("RES", $scope.ListPR);
						
						var tampungan_raw = res.data.Result;
                        var Grand_Total_Pas_VendorJasaDipilihDariSpk=0;
                        
                       

                        
                        $scope.listAccTidakAda = [];
                        $scope.hasiltampungan = "";
                        $scope.tampungArrayPR = [];

                        if ($scope.ListPO[0].PRTypeId == 1 ||$scope.ListPO[0].PRTypeId == 2||$scope.ListPO[0].PRTypeId == 4 || $scope.ListPO[0].PRTypeId == 5 ){
                            $scope.tampungArrayPR = angular.copy($scope.gridListPR.data);  
                        }else if($scope.ListPO[0].PRTypeId == 3){
                            $scope.tampungArrayPR = angular.copy($scope.gridListBirojasa.data);
                        };
                        var tmpDatasama = [];
                            for (var i in $scope.tampungArrayPR){ //tampungan dari apa yg dipilih di depan/grid
                                // for (var j in tampungan_raw){
                                    // if(tampungan_raw.length < 1){
                            
                                    //     $scope.listAccTidakAda.push($scope.tampungArrayPR[i]);
                                    //     console.log('listAccTidakAda',$scope.listAccTidakAda);
                                    // }else
                                    // if($scope.tampungArrayPR[i].AccessoriesCode != tampungan_raw[i].AccessoriesCode ){
                                    //     $scope.listAccTidakAda.push($scope.tampungArrayPR[i]);
                                    //     // tampungArrayPR.splice(i, 1);
                                    //     console.log('listAccTidakAda',$scope.listAccTidakAda);
        
                                    // }else{
                                    //     $scope.tampungArrayPR.splice(i, 1);
                                    // }
                                    for(var j in tampungan_raw){ //tampungan dari backend
                                        if($scope.tampungArrayPR[i].AccessoriesCode == tampungan_raw[j].AccessoriesCode){
                                            $scope.tampungArrayPR[i].isSame = true;
                                            tmpDatasama.push(tampungan_raw[j]);


                                        }
                                        
                                    }
                                        if($scope.tampungArrayPR[i].isSame == undefined){
                                            $scope.listAccTidakAda.push($scope.tampungArrayPR[i]);
                                            
                                        }
                                // } 
                            }

                        

                        

                        
                        
                        // $scope.gridListBirojasa.data = tampungan_raw;
                        // for(var a)
                        // console.log('gridListBirojasa ===>', $scope.gridListBirojasa.data)

                        
                       
                        // $scope.gridPembelianMaterial.data = tampungan_raw;
                        $scope.gridPembelianMaterial.data = tmpDatasama;

                        for (var i = 0; i < $scope.gridPembelianMaterial.data.length; i++) {
                            $scope.gridPembelianMaterial.data[i].MaterialId = $scope.gridPembelianMaterial.data[i].AccessoriesId 
                            $scope.gridPembelianMaterial.data[i].MaterialCode = $scope.gridPembelianMaterial.data[i].AccessoriesCode
                            $scope.gridPembelianMaterial.data[i].MaterialName = $scope.gridPembelianMaterial.data[i].AccessoriesName
                            $scope.gridPembelianMaterial.data[i].TotalPrice = ($scope.gridPembelianMaterial.data[i].Price - $scope.gridPembelianMaterial.data[i].Discount) * $scope.gridPembelianMaterial.data[i].Qty;
                            Grand_Total_Pas_VendorJasaDipilihDariSpk += (($scope.gridPembelianMaterial.data[i].Price - $scope.gridPembelianMaterial.data[i].Discount) * $scope.gridPembelianMaterial.data[i].Qty);
                            
                        }

				
						
						
                        $scope.ListPO[0].GrandTotal=angular.copy(Grand_Total_Pas_VendorJasaDipilihDariSpk);
                        
                        if($scope.listAccTidakAda.length > 0){
                            for (var i in $scope.listAccTidakAda)
                            bsNotify.show({
                                title: "Notifikasi",
                                content: $scope.listAccTidakAda[i].AccessoriesName + " tidak terdaftar pada vendor ini " ,
                                type: 'warning',
                                timeout: 9000
                                
                            });
                        }
					}
				);
			}
			
		}

        $scope.btnLihatPO = function(SelectPO) {
            // $scope.gridPembelianMaterial.columnDefs[5].enableCellEdit=true;
			// $scope.gridPembelianMaterial.columnDefs[6].enableCellEdit=true;
			// $scope.gridApiPembelianMaterialMaintainPo.grid.refresh();
			
			// $scope.CreateForm = true;
            $scope.breadcrums.title="Lihat";

            $scope.disabledTipePOView = true;
            $scope.disabledNoPOView = true;
            $scope.disabledNamaVendorView = true;
            $scope.disabledTipeMaterialView = true;
            $scope.disabledTglPengirimanView = true;
            $scope.disabledNoRangkaView = true;
            $scope.disabledTOPView = true;
            $scope.disabledMetodePembayaranView = true;
            $scope.disabledGrandTotalView = true;
            $scope.disabledCatatanView =  true;

            $scope.MaintainPoAccView = true;
            $scope.ShowViewPoAcc = true;
            $scope.MaintainForm = false;
            $scope.simpanShow = true;
            $scope.cetakShow = true;
            $scope.showGridPembelianMaterial = false;
            $scope.showGridPembelianMaterialJasa = false;
			$scope.showPONO = true;

            $scope.ListPO = [];
            $scope.ListPO.push(SelectPO);
			$scope.breadcrums.title="Ubah";
            
            console.log("test data", $scope.ListPO, $scope.dateOptionPengiriman.minDate);
            $scope.gridListPR.data = $scope.ListPO[0].ListOfPODetailPRView;
            $scope.gridListBirojasa.data = $scope.ListPO[0].ListOfPODetailPRView;
            $scope.gridListDaftarPembelianUnitMaintainPo.data = $scope.ListPO[0].ListOfPODetailPRView;
            $scope.gridPembelianMaterial.data = $scope.ListPO[0].ListOfPODetailSummaryView;    
            $scope.DisabledViewPO = false;
            $scope.disabledTextboxPO = true;
            $scope.OperationMaintainPo = "Update";
            MaintainPoFrom = "PoUpdate";
            //DisabledTypePO
            // 1  Aksesoris, ref 1  PR,  status created, grid tidak bisa ditambah
            // 1  Aksesoris, ref 2  Internal,  status created
            // 2  Karoseri   ref 1  PR,  status created, grid tidak bisa ditambah
            // 3  OPD        ref 1  PR,  status created, grid tidak bisa ditambah
            // 4  Ekspedisi  ref  3 Other,  status created
            // 5  Purna Jual  ref 1  PR,  status created, grid tidak bisa ditambah
            // if(){

            // }


            $scope.DisabledTypePO = false;
            $scope.MaintainPoDisableVendor = true;
            $scope.DisabledTypePO1 = true;
            $scope.DisabledTypePO2 = true;
            $scope.DisabledTypePO3 = true;
			$scope.showPOStatus = true;
            
            

            // $scope.DataReference = [];
            // var DaMaintainPOReferenceAction = "&POAction=semua";
            // var DaMaintainPoParam = "/?POTypeId=" + $scope.ListPO[0].POTypeId + DaMaintainPOReferenceAction;
            // MaintainPOAksesorisStandarFactory.getPOReference(DaMaintainPoParam).then(
            //     function(res) {
            //         $scope.DataReference = res.data.Result
            //         // for (var i = 0; i < res.data.Result.length; ++i) {
            //         //     $scope.DataReference.push(res.data.Result[i]);
            //         // }
            //     }
            // );

            // $scope.MaintainPoDaKolomBawah();


            // var DaPoTypeId = "?PRTypeId=" + SelectPO.POTypeId;
            // MaintainPOAksesorisStandarFactory.getVendor(DaPoTypeId).then(
            //     function(res) {
            //         $scope.getVendor = res.data.Result;
            //         //console.log("RES", $scope.getVendor);
            //     }
            // );
            

            MaintainPOAksesorisStandarFactory.getDataDetailForUbah(SelectPO.POId + "&POTypeId=" + SelectPO.POTypeId).then(
                function(res) {
                    $scope.ListPO = [];
                    $scope.ListPO.push(res.data.Result[0]);
                    console.log("$scope.ListPO-->",$scope.ListPO);
                    // $scope.ListPO.GrandTotal = res.data.Result;
                    console.log("$scope.ListPO.GrandTotal",$scope.ListPO.GrandTotal);
            // for(var i in res.data.Result[0].POAccessoriesDetailView){
            //    $scope.ListPO.GrandTotal = $scope.ListPO.GrandTotal +  res.data.Result[0].POAccessoriesDetailView[i].HargaTotal;
            // }
            console.log("$scope.ListPO.GrandTotal",$scope.ListPO.GrandTotal);
                    // if($scope.ListPO[0].SentDate < new Date()){
                    //     $scope.dateOptionPengiriman.minDate = new Date();
                    // }else{
                    //     $scope.dateOptionPengiriman.minDate = new Date($scope.ListPO[0].SentDate);
                    // }
                    // $scope.gridListPR.data = $scope.ListPO[0].ListOfPODetailPRView;
                    // $scope.gridListBirojasa.data = $scope.ListPO[0].ListOfPODetailPRView;
                    // $scope.gridListDaftarPembelianUnitMaintainPo.data = $scope.ListPO[0].ListOfPODetailPRView;
                    // $scope.gridPembelianMaterial.data = $scope.ListPO[0].ListOfPODetailSummaryView;
                    // $scope.gridPembelianMaterialJasa.data = $scope.ListPO[0].ListOfPODetailSummaryView;                          
                    $scope.gridAcc.data = res.data.Result[0].POAccessoriesDetailView;    //grid view acc standar   
                    console.log("grid view acc standar",$scope.ListPO[0].POAccessoriesDetailView); 
                    
					
					for(var i = 0; i < $scope.getTypePOAccStandar.length; ++i)
					{	
						if($scope.getTypePOAccStandar[i].POTypeId == $scope.ListPO[0].POTypeId)
						{
							$scope.filterType($scope.getTypePOAccStandar[i]);//ini tadi ga ada
						}
					}
					
					
					// $scope.MaintainPoMaeninFieldLagi();
                }
            );

        }

        $scope.MaintainPoMaeninFieldLagi = function() {
            $scope.noDocShow = false;
            $scope.showKontak = false;
            $scope.showHandphone = false;
            $scope.showNorangka = false;

            $scope.getTypeMaterial = [];
            var DaMaintainPOReferenceMaterialTypeAction = "&POReferenceId=" + $scope.ListPO[0].POReferenceId;
            var DaMaintainPoMaterialParam = "/?POTypeId=" + $scope.ListPO[0].POTypeId + DaMaintainPOReferenceMaterialTypeAction;
            MaintainPOAksesorisStandarFactory.getPCategoryMaterialType(DaMaintainPoMaterialParam).then(
                function(res) {
                    for (var i = 0; i < res.data.Result.length; ++i) {
                        $scope.getTypeMaterial.push(res.data.Result[i]);
                    }
                }
            );

            if ($scope.ListPO[0].POReferenceId == 3 || $scope.ListPO[0].POReferenceId == 8 || $scope.ListPO[0].POReferenceId == 7) {
                $scope.noDocShow = true;
                $scope.showKontak = true;
                $scope.showHandphone = true;
                $scope.showNorangka = false;

                

                if ($scope.ListPO[0].POReferenceId != 7){
                    $scope.showNamaTujuan = true;
                    $scope.suratJalan = true;
                    $scope.alamatTujuan = true;
                }

				if($scope.ListPO[0].POReferenceId == 8)
				{
					$scope.noDocShow = false;
				}
            } else if ($scope.ListPO[0].POReferenceId == 2) {
                $scope.noDocShow = true;
                $scope.showHandphone = false;
                $scope.showKontak = false;
                $scope.showNorangka = true;
            } else if ($scope.ListPO[0].POReferenceId == 1) {
                $scope.noDocShow = false;
                $scope.showHandphone = true;
                $scope.showKontak = true;
                $scope.showNorangka = false;
            } else if ($scope.ListPO[0].POReferenceId == 4) {
                $scope.noDocShow = true;
                $scope.showHandphone = false;
                $scope.showKontak = false;
                $scope.showNorangka = true;
            } else if ($scope.ListPO[0].POReferenceId == 5 || $scope.ListPO[0].POReferenceId == 6) {
                $scope.noDocShow = false;
                $scope.showHandphone = false;
                $scope.showKontak = false;
                $scope.showNorangka = true;
            }
        }

        $scope.btnBatalModalMaintainPoBuatTambahChild = function() {
            angular.element('.ui.modal.ModalMaintainPoBuatTambahChild').modal('hide');
        }

        $scope.btnPilihMaintainPoBuatTambahChild = function() {
            angular.element('.ui.modal.ModalMaintainPoBuatTambahChild').modal('hide');
            //nanti masukin buat tambah
            if ($scope.ListPO[0].POTypeId == 1 || $scope.ListPO[0].POTypeId == 2|| $scope.ListPO[0].POTypeId == 5) {
                //MaintainPoAksesorisRow
                $scope.gridPembelianMaterial.data = [];
                for (var i = 0; i < $scope.MaintainPoAksesorisRow.length; ++i) {
                    //nanti push
                    $scope.gridPembelianMaterial.data.push({
                        PRId: $scope.MaintainPoAksesorisRow[i].PRId,
						MaterialId: $scope.MaintainPoAksesorisRow[i].MaterialId,
                        MaterialCode: $scope.MaintainPoAksesorisRow[i].MaterialCode,
                        MaterialName: $scope.MaintainPoAksesorisRow[i].MaterialName,
                        Qty: $scope.MaintainPoAksesorisRow[i].Qty,
                        UomId: $scope.MaintainPoAksesorisRow[i].UomId,
                        UomName: $scope.MaintainPoAksesorisRow[i].UomName,
                        Price: $scope.MaintainPoAksesorisRow[i].Price,
                        Discount: $scope.MaintainPoAksesorisRow[i].Discount,
                        PPN: $scope.MaintainPoAksesorisRow[i].PPN,
                        TotalPrice: $scope.MaintainPoAksesorisRow[i].TotalPrice,
						SOCode: $scope.MaintainPoAksesorisRow[i].SOCode,
						SOId: $scope.MaintainPoAksesorisRow[i].SOId
                    });
                }

                try {
                    $scope.ListPO[0].GrandTotal = 0;

                    for (var i = 0; i < $scope.MaintainPoAksesorisRow.length; ++i) {
                        $scope.ListPO[0].GrandTotal += $scope.MaintainPoAksesorisRow[i].TotalPrice;
                    }

                } catch (exceptionthingy) {
                    $scope.ListPO[0].GrandTotal = 0;
                }
            } else if ($scope.ListPO[0].POTypeId == 3 || $scope.ListPO[0].POTypeId == 4) {
                // MaintainPoOpdDanEkspedisiRow
                $scope.gridPembelianMaterial.data = [];
                for (var i = 0; i < $scope.MaintainPoOpdDanEkspedisiRow.length; ++i) {
                    //nanti push
                    $scope.gridPembelianMaterial.data.push({
                        PRId: $scope.MaintainPoOpdDanEkspedisiRow[i].PRId,
						MaterialId: $scope.MaintainPoOpdDanEkspedisiRow[i].MaterialId,
                        MaterialCode: $scope.MaintainPoOpdDanEkspedisiRow[i].MaterialCode,
                        MaterialName: $scope.MaintainPoOpdDanEkspedisiRow[i].MaterialName,
                        Qty: $scope.MaintainPoOpdDanEkspedisiRow[i].Qty,
                        UomId: $scope.MaintainPoOpdDanEkspedisiRow[i].UomId,
                        UomName: $scope.MaintainPoOpdDanEkspedisiRow[i].UomName,
                        Price: $scope.MaintainPoOpdDanEkspedisiRow[i].Price,
                        Discount: $scope.MaintainPoOpdDanEkspedisiRow[i].Discount,
                        PPN: $scope.MaintainPoOpdDanEkspedisiRow[i].PPN,
                        TotalPrice: $scope.MaintainPoOpdDanEkspedisiRow[i].TotalPrice
                    });

                }

                try {
                    $scope.ListPO[0].GrandTotal = 0;

                    for (var i = 0; i < $scope.MaintainPoOpdDanEkspedisiRow.length; ++i) {
                        $scope.ListPO[0].GrandTotal += $scope.MaintainPoOpdDanEkspedisiRow[i].TotalPrice;
                    }

                } catch (exceptionthingy) {
                    $scope.ListPO[0].GrandTotal = 0;
                }
            }

        }

        $scope.AddDaNewMaintainPoChild = function(setan) {
            angular.element('.ui.modal.ModalMaintainPoBuatTambahChild').modal('setting', { closable: false }).modal('show');
            angular.element('.ui.modal.ModalMaintainPoBuatTambahChild').not(':first').remove();
            var da_intended_Frame_No = "";

            for (var i = 0; i < $scope.getNoRangka.length; ++i) {
                if ($scope.getNoRangka[i].VehicleId == setan.VehicleId) {
                    da_intended_Frame_No = $scope.getNoRangka[i].FrameNo;
                }
            }

            var getaksesoris = "start=1&limit=10000"+"&VIN=" + da_intended_Frame_No+"&VendorId="+$scope.ListPO[0].VendorId;
            var getaksesoris_PurnaJual = "start=1&limit=10000"+"&VIN=" + da_intended_Frame_No+"&VendorId="+$scope.ListPO[0].VendorId+"&MaterialTypeId="+$scope.ListPO[0].MaterialTypeId;
			if($scope.ListPO[0].POTypeId == 5)
			{
				$scope.columnDefsgridDataAksesorisMaintainPo[8].visible = true;
				$scope.gridApi_gridDataAksesorisMaintainPo.grid.refresh();
				
				MaintainPOAksesorisStandarFactory.getDataAksesorisBuatInternal(getaksesoris_PurnaJual).then(
					function(res) {
						$scope.gridDataAksesorisMaintainPo.data = res.data.Result;
						// for (var i = 0; i < $scope.gridDataAksesorisMaintainPo.data.length; ++i) {
						//     $scope.gridDataAksesorisMaintainPo.data[i].Qty = 1;
						// }
						$scope.loading = false;
					},
					function(err) {
						console.log("err=>", err);
					}
				);
			}
			else
			{
				if($scope.ListPO[0].POTypeId == 1 || $scope.ListPO[0].POTypeId == 2)
				{
					$scope.columnDefsgridDataAksesorisMaintainPo[8].visible = false;
					$scope.gridApi_gridDataAksesorisMaintainPo.grid.refresh();
				}
				
				
				MaintainPOAksesorisStandarFactory.getDataAksesoris(getaksesoris).then(
					function(res) {
						$scope.gridDataAksesorisMaintainPo.data = res.data.Result;
						// for (var i = 0; i < $scope.gridDataAksesorisMaintainPo.data.length; ++i) {
						//     $scope.gridDataAksesorisMaintainPo.data[i].Qty = 1;
						// }
						$scope.loading = false;
					},
					function(err) {
						console.log("err=>", err);
					}
				);
			}
			
			

            var da_intended_Vehicle_Type_Id = "";
            try {
                for (var i = 0; i < $scope.getNoDokumen.length; ++i) {
                    if ($scope.getNoDokumen[i].DeliveryNoteId == setan.DeliveryNoteId) {
                        da_intended_Vehicle_Type_Id = $scope.getNoDokumen[i].VehicleTypeId;
                    }
                }

                if (typeof setan.DeliveryNoteId == "undefined") {
                    da_intended_Vehicle_Type_Id = 0;
                }
            } catch (ExceptionComment) {
                da_intended_Vehicle_Type_Id = 0;
            }
			
			if($scope.ListPO[0].POReferenceId==8)
			{
				for(var i = 0; i < $scope.optionMaintainPo_QuotationNo.length; ++i)
				{	
					if($scope.optionMaintainPo_QuotationNo[i].QuotationNo ==$scope.ListPO[0].QuotationNo)
					{
						
						da_intended_Vehicle_Type_Id = angular.copy($scope.optionMaintainPo_QuotationNo[i].VehicleTypeId);
						break;
					}
				}
			}

            var getJasaOpdDanEkspedisi = "Start=1&Limit=1000000&VehicleTypeId=" + da_intended_Vehicle_Type_Id+"&VendorId=" + $scope.ListPO[0].VendorId+"&MaterialTypeId=" + $scope.ListPO[0].MaterialTypeId;
            MaintainPOAksesorisStandarFactory.getDataJasaOpdDanEkspedisi(getJasaOpdDanEkspedisi).then(
                function(res) {
                    $scope.gridDataOpdDanEkspedisi.data = res.data.Result;

                    for (var i = 0; i < $scope.gridDataOpdDanEkspedisi.data.length; ++i) {
                        $scope.gridDataOpdDanEkspedisi.data[i].Qty = 1;
                    }

                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

        }

        $scope.filterType = function(Selected) {
            $scope.typeSelected = Selected;
			
			$scope.POTypeId =$scope.typeSelected.POTypeId;
            $scope.POTypeName =$scope.typeSelected.POTypeName;
            
            //FYI PoTypeid
            // PoTypeid = 1      aksesoris
            // PoTypeid = 2      karoseri
            // PoTypeid = 3      OPD
            // PoTypeid = 4      ekspedisi
            // PoTypeid = 5      purna jual
            // PoTypeid = 6      swapping
            // PoTypeid = 7      finish unit
            


            //uda opd ke pr
            //uda purna jual ke pr
            //uda ekspedisi ke other terus sekarang ada tam sama quotation
            //uda aksesoris ke pr kalo dari maintain pr kalo dari maintain po ke internal
            //uda karoseri ke pr
            //uda finish unit ke DO
            //uda swapping ke (different company at pdc) (different company at pds)

            //karoseri sama kaya purna jual
            //purna jual sama kaya aksesoris yang pr
            //karoseri punya data nongol kodekaroseri sama karoseri



            //kalo pr ada 2 kalo bukan ada 1 kecuali finish unit getTypePOdo graphnya laen swapping graphnya laen

            $scope.DataReference = [];
            var DaMaintainPOReferenceAction = "";
            if (MaintainPoFrom == "&POAction=PoUpdate") {
                DaMaintainPOReferenceAction = "&POAction=semua";
            } else if (MaintainPoFrom == "SPK") {
                DaMaintainPOReferenceAction = "&POAction=pr";
            } else if (MaintainPoFrom == "POInsert") {
                DaMaintainPOReferenceAction = "&POAction=buat po";
            }

            var DaMaintainPoParam = "/?POTypeId=" + $scope.typeSelected.POTypeId + DaMaintainPOReferenceAction;
            MaintainPOAksesorisStandarFactory.getPOReference(DaMaintainPoParam).then(
                function(res) {
                    for (var i = 0; i < res.data.Result.length; ++i) {
                        $scope.DataReference.push(res.data.Result[i]);
                    }
                }
            );

            $scope.MaintainPoDaKolomBawah();

            var DaPoTypeId = "?PRTypeId=" + $scope.typeSelected.POTypeId;
            console.log(DaPoTypeId);
            MaintainPOAksesorisStandarFactory.getVendor(DaPoTypeId).then(
                function(res) {
                    $scope.getVendor = res.data.Result;
                }
            );
			
			if($scope.ListPO[0].POTypeId == 5)
			{
                $scope.showNamaTujuan = false;
                $scope.suratJalan = false;
                $scope.alamatTujuan = false;

				MaintainPOAksesorisStandarFactory.getNoRangkaPurnaJual().then(
					function(res) {
						$scope.getNoRangka = res.data.Result;
					   // return res.data;
					}
				);
			}
			else
			{
                $scope.showNamaTujuan = false;
                $scope.suratJalan = false;
                $scope.alamatTujuan = false;
                
				MaintainPOAksesorisStandarFactory.getNoRangka().then(
					function(res) {
						$scope.getNoRangka = res.data.Result;
					   // return res.data;
					}
				);
			}
        }

        $scope.showGridPembelianMaterial = false;
        $scope.showGridPembelianMaterialJasa = false;

        $scope.MaintainPoDaKolomBawah = function() {
            console.log($scope.ListPO[0]);
            if($scope.ListPO[0].POTypeName == "Aksesoris" || $scope.ListPO[0].POTypeId == 1 || $scope.ListPO[0].PRTypeName == "Aksesoris" || 
            $scope.ListPO[0].POTypeName == "Karoseri" || $scope.ListPO[0].POTypeId == 4 || $scope.ListPO[0].PRTypeName == "Karoseri" || 
            $scope.ListPO[0].POTypeName == "Purna Jual" || $scope.ListPO[0].PRTypeName == "Purna Jual" || $scope.ListPO[0].POTypeId == 3 || $scope.ListPO[0].POTypeName == "OPD"){
                //tadi yang 3 sama opd ga ada
				$scope.showGridPembelianMaterial = true;
            }else{
                $scope.showGridPembelianMaterialJasa = false;
            }
            
            $scope.showGridDaftarPembelianUnitMaintainPo = false;
            $scope.showGridRincianHarga = false;

            if ($scope.ListPO[0].POReferenceId == 1) {
                $scope.showGridmaterial = false;
                $scope.showGridbirojasa = false;

                if ($scope.ListPO[0].POTypeId == 1 || $scope.ListPO[0].POTypeId == 5 || $scope.ListPO[0].POTypeId == 2) {
                    $scope.showGridmaterial = true;
                    $scope.showGridbirojasa = false;
                } else if ($scope.ListPO[0].POTypeId == 3) {
                    $scope.showGridmaterial = false;
                    $scope.showGridbirojasa = true;
                }

            } else if ($scope.ListPO[0].POReferenceId != 1 && $scope.ListPO[0].POReferenceId == 4) {
                $scope.showGridmaterial = false;
                $scope.showGridbirojasa = false;
                $scope.showGridPembelianMaterial = false;
                $scope.showGridPembelianMaterialJasa = false;
                $scope.showGridDaftarPembelianUnitMaintainPo = true;
                //khusus buat finish unit do disini tambah buat urus 1 grafik
            } else if ($scope.ListPO[0].POReferenceId != 1 && ($scope.ListPO[0].POReferenceId == 5 || $scope.ListPO[0].POReferenceId == 6)) {
                $scope.showGridmaterial = false;
                $scope.showGridbirojasa = false;
                $scope.showGridPembelianMaterial = false;
                $scope.showGridPembelianMaterialJasa = false;
                $scope.showGridRincianHarga = true;
                //khusus buat swapping disini tambah buat urus 1 grafik
            } else if ($scope.ListPO[0].POReferenceId != 1 && $scope.ListPO[0].POReferenceId != 4) {
                $scope.showGridmaterial = false;
                $scope.showGridbirojasa = false;
				if($scope.ListPO[0].POReferenceId == 8)
				{
					$scope.noDocShow = false;
				}
				console.log("jalan");
                //ini yg tipenya laen ga nongol semua
            } else {
                $scope.showGridmaterial = false;
                $scope.showGridbirojasa = false;
				if($scope.ListPO[0].POReferenceId == 8)
				{
					$scope.noDocShow = false;
				}
				console.log("jalan");
                //ini cuma error catching
            }
        }

        $scope.filter_reference = function(Selected) {
            $scope.referenceSelected = Selected;
            console.log('$scope.referenceSelected ===>', $scope.referenceSelected);
            $scope.getTypeMaterial = [];
            var DaMaintainPOReferenceMaterialTypeAction = "&POReferenceId=" + $scope.referenceSelected.POReferenceId;
            var DaMaintainPoMaterialParam = "/?POTypeId=" + $scope.ListPO[0].POTypeId + DaMaintainPOReferenceMaterialTypeAction;
            MaintainPOAksesorisStandarFactory.getPCategoryMaterialType(DaMaintainPoMaterialParam).then(
                function(res) {
                    if(res.data.Result.length!=0)
					{
						for (var i = 0; i < res.data.Result.length; ++i) {
							$scope.getTypeMaterial.push(res.data.Result[i]);
						}
					}
					
                }
            );

            $scope.noDocShow = false;
            $scope.showKontak = false;
            $scope.showHandphone = false;
            $scope.showNorangka = false;

            if ($scope.referenceSelected.POReferenceId == 3 || 
                $scope.referenceSelected.POReferenceId == 8 || 
                $scope.referenceSelected.POReferenceId == 7) {
                $scope.noDocShow = true;
                $scope.showKontak = true;
                $scope.showHandphone = true;
                $scope.showNorangka = false;
                
				
				if($scope.referenceSelected.POReferenceId == 8)
				{
                    $scope.noDocShow = false;
                    $scope.showNamaTujuan = true;
                    $scope.suratJalan = true;
                    $scope.alamatTujuan = true;
                }
                
                if ($scope.referenceSelected.POReferenceId == 3) {
                    $scope.showNamaTujuan = true;
                    $scope.suratJalan = true;
                    $scope.alamatTujuan = true;
                }
				
            } else if ($scope.referenceSelected.POReferenceId == 2) {
                $scope.noDocShow = true;
                $scope.showHandphone = false;
                $scope.showKontak = false;
                $scope.showNorangka = true;
				if($scope.ListPO[0].POTypeId==5)
				{
					$scope.showGridPembelianMaterial=true;
				}
				
            } else if ($scope.referenceSelected.POReferenceId == 1) {
                $scope.noDocShow = false;
                $scope.showHandphone = true;
                $scope.showKontak = true;
                $scope.showNorangka = false;
               
            } else if ($scope.referenceSelected.POReferenceId == 4) {
                $scope.noDocShow = true;
                $scope.showHandphone = false;
                $scope.showKontak = false;
                $scope.showNorangka = true;
            } else if ($scope.referenceSelected.POReferenceId == 5 || 
                $scope.referenceSelected.POReferenceId == 6) {
                $scope.noDocShow = false;
                $scope.showHandphone = false;
                $scope.showKontak = false;
                $scope.showNorangka = true;
                
            }
        }

        $scope.SelectMaterial = function(selected) {
            $scope.FrameSelected = selected;
            var Da_DeliveryNoteId = 0;

            if ($scope.OperationMaintainPo == "Insert" && $scope.ListPO[0].POTypeId == 1 && $scope.ListPO[0].POReferenceId == 2) {
                for (var i = 0; i < $scope.getNoRangka.length; ++i) {
                    if (selected.VehicleId == $scope.getNoRangka[i].VehicleId) {
                        Da_DeliveryNoteId = $scope.getNoRangka[i].DeliveryNoteId;
                        break;
                    }
                }

                for (var i = 0; i < $scope.getNoDokumen.length; ++i) {
                    if (Da_DeliveryNoteId == $scope.getNoDokumen[i].DeliveryNoteId) {
                        $scope.ListPO[0].DeliveryNoteId = $scope.getNoDokumen[i].DeliveryNoteId;
                        break;
                    }
                }
            }

            // var getFrameNO = "?VIN=" + $scope.FrameSelected.FrameNo;
            // MaintainPOAksesorisStandarFactory.getMaterialInternal(getFrameNO).then(
            // function(res){
            // $scope.getVIN = res.data.Result;
            // console.log("VIN", $scope.getVIN);
            // for (var i in $scope.getVIN) {
            // $scope.gridPembelianMaterial.data.push({
            // PRId: $scope.getVIN[i].PRId, 
            // MaterialId: $scope.getVIN[i].AccessoriesId, 
            // MaterialCode:$scope.getVIN[i].AccessoriesCode, 
            // MaterialName:$scope.getVIN[i].AccessoriesName, 
            // Stock:$scope.getVIN[i].Stock, 
            // UomName:$scope.getVIN[i].UomName, 
            // Price:$scope.getVIN[i].Price,
            // TotalPrice:"",
            // PPN:"",
            // Discount:""
            // });
            // }
            // }
            // );
        }

        $scope.buatPOdariSPK = function() {
            $scope.ListPO = $rootScope.mMaintainPO;
			
			//tadi 1 line ini dibawah ga ada
            $scope.ListPO[0].VendorId=null;
            $scope.ListPO[0].PODate=null;
			//$scope.MaintainPoDaSelectVendor($scope.ListPO[0].VendorId);
            
			$scope.gridListPR.data = [];
            $scope.gridListBirojasa.data = [];
            $scope.gridListDaftarPembelianUnitMaintainPo.data = [];
            $scope.gridPembelianMaterial.data = [];
            $scope.OperationMaintainPo = "Insert";
            MaintainPoFrom = "SPK";
            $scope.dateOptionsStart.minDate = new Date();
            $scope.dateOptionPengiriman.minDate = new Date();

            $scope.DataReference = [];
            var DaMaintainPOReferenceAction = "&POAction=pr";
            var DaMaintainPoParam = "/?POTypeId=" + $scope.ListPO[0].PRTypeId + DaMaintainPOReferenceAction;
            MaintainPOAksesorisStandarFactory.getPOReference(DaMaintainPoParam).then(
                function(res) {
                    for (var i = 0; i < res.data.Result.length; ++i) {
                        $scope.DataReference.push(res.data.Result[i]);
                    }
                }
            );

            var getId = "?PRTypeId=" + $scope.ListPO[0].PRTypeId;
            console.log(getId);
            MaintainPOAksesorisStandarFactory.getVendor(getId).then(
                function(res) {
                    $scope.getVendor = res.data.Result;
                }
            );

            for (var i in $scope.ListPO) {

                var sPPN = 1 / 10;
                console.log(sPPN);
                $scope.ListPO[i].POTypeId = $scope.ListPO[i].PRTypeId;
                $scope.ListPO[i].VendorId = $scope.ListPO[i].VendorId;
                $scope.ListPO[i].POReferenceId = 1;

                $scope.ListPO[i]["TotalPrice"] = $scope.ListPO[i]["Qty"] * $scope.ListPO[i]["Price"];
                $scope.ListPO[i]["PPN"] = $scope.ListPO[i]["Price"] * sPPN;
                console.log("PPN", $scope.ListPO[i]["PPN"]);
                if($scope.ListPO[0].PRTypeId == 1 || $scope.ListPO[0].PRTypeId == 2|| $scope.ListPO[0].PRTypeId == 4|| $scope.ListPO[0].PRTypeId == 5){ // buat aksesoris , Karesori, Purna Jual
                    $scope.gridListPR.data.push({
                        PRId: $scope.ListPO[i].PRId,
                        AccessoriesId: $scope.ListPO[i].AccessoriesId,
                        SoCode: $scope.ListPO[i].SoCode,
                        PRCode: $scope.ListPO[i].PRCode,
                        AccessoriesCode: $scope.ListPO[i].AccessoriesCode,
                        AccessoriesName: $scope.ListPO[i].AccessoriesName,
                        Qty: $scope.ListPO[i].Qty,
                        UomId: $scope.ListPO[i].UomId,
                        UomName: $scope.ListPO[i].UomName
                    });
                }else if($scope.ListPO[0].PRTypeId == 3){ // buat OPD
                    $scope.gridListBirojasa.data.push({
                        PRId: $scope.ListPO[i].PRId,
                        AccessoriesId: $scope.ListPO[i].AccessoriesId,
                        SoCode: $scope.ListPO[i].SoCode,
                        PRCode: $scope.ListPO[i].PRCode,
                        AccessoriesCode: $scope.ListPO[i].AccessoriesCode,
                        AccessoriesName: $scope.ListPO[i].AccessoriesName,
                        Qty: $scope.ListPO[i].Qty,
                        UomId: $scope.ListPO[i].UomId,
                        UomName: $scope.ListPO[i].UomName
                        
                    });
                }

				//ini tadinya idup
                // $scope.gridListBirojasa.data.push({
                    // PRId: $scope.ListPO[i].PRId,
                    // AccessoriesId: $scope.ListPO[i].AccessoriesId,
                    // SoCode: $scope.ListPO[i].SoCode,
                    // PRCode: $scope.ListPO[i].PRCode,
                    // AccessoriesCode: $scope.ListPO[i].AccessoriesCode,
                    // AccessoriesName: $scope.ListPO[i].AccessoriesName,
                    // Qty: $scope.ListPO[i].Qty,
                    // UomId: $scope.ListPO[i].UomId,
                    // UomName: $scope.ListPO[i].UomName
                // });

                // $scope.gridListDaftarPembelianUnitMaintainPo.data.push({
                // MaterialName: $scope.ListPO[i].MaterialName, 
                // FrameNo: $scope.ListPO[i].AccessoriesId, 
                // });ini paksaan belom kepake

				//ini tadinya idup
                // $scope.gridPembelianMaterial.data.push({
                    // PRId: $scope.ListPO[i].PRId,
					// MaterialId: $scope.ListPO[i].AccessoriesId,
                    // MaterialCode: $scope.ListPO[i].AccessoriesCode,
                    // MaterialName: $scope.ListPO[i].AccessoriesName,
                    // Qty: $scope.ListPO[i].Qty,
                    // UomId: $scope.ListPO[i].UomId,
                    // UomName: $scope.ListPO[i].UomName,
                    // Price: $scope.ListPO[i].Price,
                    // TotalPrice: $scope.ListPO[i].TotalPrice,
                    // PPN: $scope.ListPO[i].PPN,
                    // Discount: $scope.ListPO[i].Discount
                // });
            }

            // var DaUniqueAccessoriesId = [];
            // var gridPembelianMaterialRaw = [];
            // var newArr = $scope.gridPembelianMaterial.data.filter(function(el) {
                // if (DaUniqueAccessoriesId.indexOf(el.MaterialId) === -1) {
                    // // If not present in array, then add it
                    // DaUniqueAccessoriesId.push(el.MaterialId);
                    // gridPembelianMaterialRaw.push(el);
                    // return true;
                // } else {
                    // // Already present in array, don't add it
                    // return false;
                // }
            // });

            // $scope.gridPembelianMaterial.data = gridPembelianMaterialRaw;

            // for (var gundam = 0; gundam < DaUniqueAccessoriesId.length; gundam++) {
                // var Da_Paksaan_Qty = 0;

                // for (var macross = 0; macross < $scope.gridListPR.data.length; macross++) {
                    // if (DaUniqueAccessoriesId[gundam] == $scope.gridListPR.data[macross].AccessoriesId) {
                        // Da_Paksaan_Qty += $scope.gridListPR.data[macross].Qty;
                    // }
                // }
                // $scope.gridPembelianMaterial.data[gundam].Qty = Da_Paksaan_Qty;

                // $scope.gridPembelianMaterial.data[gundam].PPN = ($scope.gridPembelianMaterial.data[gundam].Price - $scope.gridPembelianMaterial.data[gundam].Discount) * 0.1;

                // //var semprul = ($scope.gridPembelianMaterial.data[gundam].Price - $scope.gridPembelianMaterial.data[gundam].Discount) * (1.1) * (Da_Paksaan_Qty);
				// var semprul = ($scope.gridPembelianMaterial.data[gundam].Price - $scope.gridPembelianMaterial.data[gundam].Discount) * (Da_Paksaan_Qty);
                // $scope.gridPembelianMaterial.data[gundam].TotalPrice = semprul;

            // }

            var Da_Paksa_Total = 0;
            for (var i = 0; i < $scope.gridPembelianMaterial.data.length; ++i) {
                Da_Paksa_Total += $scope.gridPembelianMaterial.data[i].TotalPrice;
            }
            $scope.ListPO[0].GrandTotal = Da_Paksa_Total;

            $scope.MaintainPoDaKolomBawah();
            $scope.MaintainPoMaeninFieldLagi();
            $scope.DisabledTypePO = false;
            $scope.DisabledTypePO1 = true;
            $scope.DisabledTypePO2 = true;
            $scope.DisabledTypePO3 = true;
			
			//ini dibawah tadi ga ada
			//$scope.MaintainPoDisableVendor = true;
			//tgl 4 maret 2019 minta diidupin lagi sama pak jok atas perintah andry
        }

        // $scope.getUniqueArrayMaintainPo = function (array) {
        // var result = [];
        // for(var x = 0; x < array.length; x++){
        // if(result.indexOf(array[x].MaterialId) == -1)
        // result.push(array[x].MaterialId);
        // }
        // return result;
        // };

        $rootScope.$on("buatPOSPK", function() {
            $rootScope.DariPrKePoAbisInsert=false;
            $scope.ListPR = angular.copy($rootScope.selectedDataPR);
			$scope.buatPOdariSPK();
        });

        $scope.btnbuatPO = function() {
            $scope.breadcrums.title="Tambah";
			$scope.ListPO = [];
            $scope.gridListPR.data = [];
            $scope.gridListDaftarPembelianUnitMaintainPo.data = [];
            $scope.gridPembelianMaterial.data = [];
            $scope.gridListBirojasa.data = [];
            $scope.CreateForm = true;
            $scope.MaintainForm = false;
            $scope.showPONO = false;
            $scope.showPOStatus = false;
            $scope.DisabledViewPO = false;
            $scope.DisabledTypePO = false;
            $scope.DisabledTypePO1 = false;
            $scope.DisabledTypePO2 = false;
            $scope.DisabledTypePO3 = false;
			$scope.MaintainPoDisableVendor = false;
            $scope.simpanShow = true;
            $scope.cetakShow = true;
            $scope.OperationMaintainPo = "Insert";
            MaintainPoFrom = "POInsert";

            //console.log("PO Type", $scope.POTypeId);
            
            $scope.getTypePOAccStandar = [];
            MaintainPOAksesorisStandarFactory.getTypePOAccStandar().then(
                function(tipe) {
                    var getTypePOToFilter = tipe.data.Result;

                    for (var i = 0; i < getTypePOToFilter.length; ++i) {
                        if (getTypePOToFilter[i].POTypeName == "Aksesoris" || getTypePOToFilter[i].POTypeName == "Ekspedisi" || getTypePOToFilter[i].POTypeName == "Purna Jual") {
                            $scope.getTypePOAccStandar.push(getTypePOToFilter[i]);
                        }
                    }
                    return tipe.data;
                }
            );
        }
		
		$scope.MaintainPoPaksaQuotationId = function(selected) {
			try
			{
				$scope.ListPO[0].QuotationId=angular.copy(selected.QuotationId);
			}
			catch(exception)
			{
				$scope.ListPO[0].QuotationId=null;
			}
			
        }
        
        // $scope.cekvalidasi = function() {
        //     console.log('ListPO[0]',$scope.ListPO[0]);
        // }

        $scope.btnSimpan = function() {
            $scope.disabledBtnSimpan = true;
            $scope.ListPO[0].ListOfPODetailSummaryView = $scope.gridPembelianMaterial.data;
            $scope.ListPO[0].ListOfPODetailPRView = $scope.gridListPR.data; //aksesoris
            if ($scope.ListPO[0].POTypeId == 3 ){
                $scope.ListPO[0].ListOfPODetailPRView = $scope.gridListBirojasa.data;
            }else{
                $scope.ListPO[0].ListOfPODetailPRView = $scope.gridListPR.data;
            }
            $scope.ListPO[0].ContactName = $scope.ListPO[0].ContactName;
            $scope.ListPO[0].HPNumber = $scope.ListPO[0].HPNumber;
            $scope.ListPO[0].FrameNo = $scope.ListPO[0].FrameNo;
            $scope.ListPO[0].SentDate = fixDate($scope.ListPO[0].SentDate);
            $scope.ListPO[0].PaymentTypeId = $scope.ListPO[0].PaymentTypeId;
            $scope.ListPO[0].TermOfPayment = $scope.ListPO[0].TermOfPayment;
            $scope.ListPO[0].POCode = $scope.ListPO[0].POCode;
			
			if($scope.ListPO[0].POReferenceId == 8)
			{
				for(var i = 0; i < $scope.optionMaintainPo_QuotationNo.length; ++i)
				{	
					if($scope.optionMaintainPo_QuotationNo[i].QuotationNo ==$scope.ListPO[0].QuotationNo)
					{
						//$scope.ListPO[0].QuotationReceiveId=angular.copy($scope.optionsTipeAktivitas[i].QuotationReceiveId);
						$scope.ListPO[0].QuotationId=angular.copy($scope.optionMaintainPo_QuotationNo[i].QuotationId);
						break;
					}
				}
			}


			
			
			if ($scope.ListPO[0].POTypeId == 1 ||  $scope.ListPO[0].POTypeId == 4) {
                if ($scope.ListPO[0].ListOfPODetailSummaryView.length < 1 && $scope.ListPO[0].POReferenceId == 2 && $scope.ListPO[0].POTypeId == 1 && ((typeof $scope.ListPO[0].VehicleId == "undefined") || $scope.ListPO[0].VehicleId == null)) {
                    bsNotify.show({
                        title: "Notifikasi",
                        content: "Anda Harus Mengisi no rangka",
                        type: 'warning'
                    });
                    $scope.disabledBtnSimpan = false;
                } else if ($scope.ListPO[0].POReferenceId == 3 && $scope.ListPO[0].ListOfPODetailSummaryView.length < 1 && $scope.ListPO[0].POTypeId == 4 && ((typeof $scope.ListPO[0].DeliveryNoteId == "undefined") || $scope.ListPO[0].DeliveryNoteId == null)) {
                    bsNotify.show({
                        title: "Notifikasi",
                        content: "Anda Harus Mengisi no Dokumen",
                        type: 'warning'
                    });
                    $scope.disabledBtnSimpan = false;
                } else if ($scope.ListPO[0].POReferenceId == 8 && $scope.ListPO[0].ListOfPODetailSummaryView.length < 1 && $scope.ListPO[0].POTypeId == 4 && ((typeof $scope.ListPO[0].QuotationNo == "undefined") || $scope.ListPO[0].QuotationNo == null)) {
                    bsNotify.show({
                        title: "Notifikasi",
                        content: "Anda Harus Mengisi no Quotation",
                        type: 'warning'
                    });
                    $scope.disabledBtnSimpan = false;
                } else {
                    if ($scope.OperationMaintainPo == "Insert") {
                        MaintainPOAksesorisStandarFactory.create($scope.ListPO[0]).then(
                            function(res) {
								MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
									function(res) {
										$scope.grid.data = res.data.Result;
										$scope.loading = false;
										$scope.OperationMaintainPo = "InsertKelar";
										bsNotify.show({
											title: "Berhasil",
											content: "PO berhasil dibuat.",
											type: 'success'
                                        });
                                        $scope.disabledBtnSimpan = false;
										$scope.btnKembaliFormMaintain();
										$scope.selected_rows = [];
									},
									function(err) {
										bsNotify.show({
											title: "gagal",
											content: err.data.Message,
											type: 'danger'
                                        });
                                        $scope.disabledBtnSimpan = false;
									}
								);
                                
                            },
                            function(err) {
                                bsNotify.show({
									title: "gagal",
									content: err.data.Message,
									type: 'danger'
                                });
                                $scope.disabledBtnSimpan = false;
                            }
                        );
                    } else if ($scope.OperationMaintainPo == "Update") {
                        MaintainPOAksesorisStandarFactory.update($scope.ListPO[0]).then(
                            function(res) {
								MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
									function(res) {
										$scope.grid.data = res.data.Result;
										$scope.loading = false;
										
										bsNotify.show(
											{
												title: "Sukses",
												content: "PO berhasil dibuat",
												type: 'success'
											}
                                        );
                                        $scope.disabledBtnSimpan = false;
										$scope.btnKembaliFormMaintain();
									},
									function(err) {
										bsNotify.show({
											title: "gagal",
											content: err.data.Message,
											type: 'danger'
                                        });
                                        $scope.disabledBtnSimpan = false;
									}
								);
                                
                            },
                            function(err) {
                                bsNotify.show({
											title: "gagal",
											content: err.data.Message,
											type: 'danger'
                                        });
                                        $scope.disabledBtnSimpan = false;
                            }
                        );
                    }
                }
            } else {
                if ($scope.OperationMaintainPo == "Insert") {
                    MaintainPOAksesorisStandarFactory.create($scope.ListPO[0]).then(
                        function(res) {
							MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
								function(res) {
									$scope.grid.data = res.data.Result;
									$scope.loading = false;

									bsNotify.show(
										{
											title: "Sukses",
											content: "PO berhasil dibuat",
											type: 'success'
										}
                                    );
                                    $scope.disabledBtnSimpan = false;
									$scope.selected_rows = [];
									$scope.btnKembaliFormMaintain();
								},
								function(err) {
									
									bsNotify.show(
										{
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										}
                                    );
                                    $scope.disabledBtnSimpan = false;
								}
							);
                            
                        },
                        function(err) {
                            bsNotify.show(
										{
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										}
                                    );
                                    $scope.disabledBtnSimpan = false;
                        }
                    );
                } else if ($scope.OperationMaintainPo == "Update") {
                    MaintainPOAksesorisStandarFactory.update($scope.ListPO[0]).then(
                        function(res) {
                            MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
								function(res) {
									$scope.grid.data = res.data.Result;
									$scope.loading = false;
									
									bsNotify.show(
										{
											title: "Sukses",
											content: "PO berhasil dibuat",
											type: 'success'
										}
                                    );
                                    $scope.disabledBtnSimpan = false;
									$scope.btnKembaliFormMaintain();
								},
								function(err) {
									bsNotify.show(
										{
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										}
                                    );
                                    $scope.disabledBtnSimpan = false;
								}
							);
							
                        },
                        function(err) {
                            bsNotify.show(
										{
											title: "Gagal",
											content: err.data.Message,
											type: 'danger'
										}
                                    );
                                    $scope.disabledBtnSimpan = false;
                        }
                    );
                }
            }
        }

        $scope.buttonSimpanCetak = function() {
            $scope.disabledButtonSimpanCetak = true;
            $scope.ListPO[0].ListOfPODetailSummaryView = $scope.gridPembelianMaterial.data;
            $scope.ListPO[0].ListOfPODetailPRView = $scope.gridListPR.data;
            if ($scope.ListPO[0].POTypeId == 3){
                $scope.ListPO[0].ListOfPODetailPRView = $scope.gridListBirojasa.data;
            }else{
                $scope.ListPO[0].ListOfPODetailPRView = $scope.gridListPR.data;
            }

            $scope.ListPO[0].ContactName = $scope.ListPO[0].ContactName;
            $scope.ListPO[0].HPNumber = $scope.ListPO[0].HPNumber;
            $scope.ListPO[0].FrameNo = $scope.ListPO[0].FrameNo;
            $scope.ListPO[0].SentDate = fixDate($scope.ListPO[0].SentDate);
            $scope.ListPO[0].PaymentTypeId = $scope.ListPO[0].PaymentTypeId;
            $scope.ListPO[0].TermOfPayment = $scope.ListPO[0].TermOfPayment;
            $scope.ListPO[0].POCode = $scope.ListPO[0].POCode;
			console.log("iblis1",$scope.ListPO);
			console.log("iblis21",$scope.getNoRangka);

            $scope.MaintainPoDaIntendedFrameNo = "";
            try {
                for (var xyz = 0; xyz < $scope.getNoRangka.length; ++xyz) {
                    if ($scope.getNoRangka[xyz].VehicleId == $scope.ListPO[0].VehicleId) {
                        $scope.MaintainPoDaIntendedFrameNo = $scope.getNoRangka[xyz].FrameNo;
                    }
                }
				
				console.log("iblis22",$scope.MaintainPoDaIntendedFrameNo);
            } catch (ExceptionComment) {
                console.log("iblis23",ExceptionComment);
				$scope.MaintainPoDaIntendedFrameNo = "";
            }

            if ($scope.ListPO[0].POTypeId == 1 || $scope.ListPO[0].POTypeId == 4) {
                if ($scope.ListPO[0].ListOfPODetailSummaryView.length < 1 && $scope.ListPO[0].POReferenceId == 2 && $scope.ListPO[0].POTypeId == 1 && ((typeof $scope.ListPO[0].VehicleId == "undefined") || $scope.ListPO[0].VehicleId == null)) {
                    bsNotify.show({
                        title: "Notifikasi",
                        content: "Anda Harus Mengisi no rangka",
                        type: 'warning'
                    });
                    $scope.disabledButtonSimpanCetak = false;
                } else if ($scope.ListPO[0].ListOfPODetailSummaryView.length < 1 && $scope.ListPO[0].POTypeId == 4 && ((typeof $scope.ListPO[0].DeliveryNoteId == "undefined") || $scope.ListPO[0].DeliveryNoteId == null)) {
                    bsNotify.show({
                        title: "Notifikasi",
                        content: "Anda Harus Mengisi no Dokumen",
                        type: 'warning'
                    });
                    $scope.disabledButtonSimpanCetak = false;
                } else {
                    if ($scope.OperationMaintainPo == "Insert") {
                        console.log("iblis3",$scope.ListPO);
						MaintainPOAksesorisStandarFactory.create($scope.ListPO[0]).then(
                            function(res) {
                                $scope.Ctk = res.data.Result;
                                $scope.selected_rows = [];
                                if ($scope.Ctk[0].POTypeName == "OPD") {
                                    $scope.ctkUrl1 = "sales/OrderPengurusanDokumen?POId=" + $scope.Ctk[0].POId;
                                } else
                                if ($scope.Ctk[0].POTypeName == "Aksesoris") {

                                    if ($scope.ListPO[0].POReferenceId == 1) {
                                        //pr
                                        $scope.ctkUrl1 = "sales/OrderPembelianAksesorisOptional?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                    } else if ($scope.ListPO[0].POReferenceId == 2) {
                                        //internal
                                        $scope.ctkUrl1 = "sales/InternalOrderAksesoris?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                    }
                                } else
                                if ($scope.Ctk[0].POTypeName == "Karoseri") {
                                    $scope.ctkUrl1 = "sales/OrderPembelianAksesorisOptional?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                } else
                                if ($scope.Ctk[0].POTypeName == 'Ekspedisi') {
                                    $scope.ctkUrl1 = "sales/OrderPemesananEkspedisi?POId=" + $scope.Ctk[0].POId;
                                } else
                                if ($scope.Ctk[0].POTypeName == "Purna Jual") {
                                    $scope.ctkUrl1 = "sales/OrderPembelianPurnaJual?POId=" + $scope.Ctk[0].POId;
                                }
                                // if($scope.Ctk[0].POTypeName == 'Swapping'){
                                //     $scope.ctkUrl1 = "sales/FakturKendaraanSwapping?BillingId=" + $scope.ListPO[0].FrameNo + "&FrameNo=" + $scope.ListPO[0].FrameNo;
                                // }else
                                // if($scope.Ctk[0].POTypeName == 'Finish Unit'){
                                //     $scope.ctkUrl1 = "sales/FakturKendaraanBaru?BillingId=" + $scope.ListPO[0].FrameNo + "&FrameNo=" + $scope.ListPO[0].FrameNo;
                                // }


                                //Call PrintRpt
                                var pdfFile = null;
                                //console.log('$scope.ctkUrl1skrgbgt',$scope.ctkUrl1xxxxxxxxxxxxx);
                                PrintRpt.print($scope.ctkUrl1).success(function(res) {
                                        var file = new Blob([res], { type: 'application/pdf' });
                                        var fileURL = URL.createObjectURL(file);

                                        //$scope.content = $sce.trustAsResourceUrl(fileURL);
                                        pdfFile = fileURL;
										
										MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
											function(res) {
												$scope.grid.data = res.data.Result;
												$scope.loading = false;
												$scope.btnKembaliFormMaintain();

												
												bsNotify.show(
													{
														title: "Sukses",
														content: "PO berhasil dibuat",
														type: 'success'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
											},
											function(err) {
												bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
											}
										);

                                        if (pdfFile != null)
										{
                                            printJS(pdfFile);
										}	
                                        else
										{
                                            console.log("error cetakan", pdfFile);
										}	
                                    })
                                    .error(function(res) {
                                        console.log("error cetakan", pdfFile);
                                    });
									
                                
                            },
                            function(err) {
                                bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
                            }
                        );
                    } else if ($scope.OperationMaintainPo == "Update") {
                        MaintainPOAksesorisStandarFactory.update($scope.ListPO[0]).then(
                            function(res) {
                                $scope.Ctk = res.data.Result;
                                if ($scope.Ctk[0].POTypeName == 'OPD') {
                                    $scope.ctkUrl1 = "sales/OrderPengurusanDokumen?POId=" + $scope.Ctk[0].POId;
                                } else
                                if ($scope.Ctk[0].POTypeName == 'Aksesoris') {
                                    if ($scope.ListPO[0].POReferenceId == 1) {
                                        //pr
                                        $scope.ctkUrl1 = "sales/OrderPembelianAksesorisOptional?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                    } else if ($scope.ListPO[0].POReferenceId == 2) {
                                        //internal
                                        $scope.ctkUrl1 = "sales/InternalOrderAksesoris?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                    }

                                } else
                                if ($scope.Ctk[0].POTypeName == 'Karoseri') {
                                    $scope.ctkUrl1 = "sales/OrderPembelianAksesorisOptional?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                } else
                                if ($scope.ListPO[0].POTypeName == 'Ekspedisi') {
                                    $scope.ctkUrl1 = "sales/OrderPemesananEkspedisi?POId=" + $scope.Ctk[0].POId;
                                } else
                                if ($scope.Ctk[0].POTypeName == 'Purna Jual') {
                                    $scope.ctkUrl1 = "sales/OrderPembelianPurnaJual?POId=" + $scope.Ctk[0].POId;
                                }
                                // if($scope.ListPO[0].POTypeName == 'Swapping'){
                                //     $scope.ctkUrl1 = "sales/FakturKendaraanSwapping?BillingId=" + $scope.ListPO[0].FrameNo + "&FrameNo=" + $scope.ListPO[0].FrameNo;
                                // }else
                                // if($scope.ListPO[0].POTypeName == 'Finish Unit'){
                                //     $scope.ctkUrl1 = "sales/FakturKendaraanBaru?BillingId=" + $scope.ListPO[0].FrameNo + "&FrameNo=" + $scope.ListPO[0].FrameNo;
                                // }


                                //Call PrintRpt
                                var pdfFile = null;
                                //console.log('$scope.ctkUrl1skrgbgt',$scope.ctkUrl1xxxxxxxxxxxxx);
                                PrintRpt.print($scope.ctkUrl1).success(function(res) {
                                        var file = new Blob([res], { type: 'application/pdf' });
                                        var fileURL = URL.createObjectURL(file);
                                        //$scope.content = $sce.trustAsResourceUrl(fileURL);
                                        pdfFile = fileURL;
										
										MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
											function(res) {
												$scope.grid.data = res.data.Result;
												$scope.loading = false;
												$scope.btnKembaliFormMaintain();

												
												bsNotify.show(
													{
														title: "Sukses",
														content: "PO berhasil dibuat",
														type: 'success'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
											},
											function(err) {
												bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
											}
										);

                                        if (pdfFile != null)
										{
                                            printJS(pdfFile);
										}	
                                        else
										{
                                            console.log("error cetakan", pdfFile);
										}	
                                    })
                                    .error(function(res) {
                                        console.log("error cetakan", pdfFile);
                                    });
									
									
                                
                            },
                            function(err) {
                                bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
                            }
                        );
                    }
                }
            } else {
                if ($scope.OperationMaintainPo == "Insert") {
                    MaintainPOAksesorisStandarFactory.create($scope.ListPO[0]).then(
                        function(res) {
                            $scope.Ctk = res.data.Result;
                            $scope.selected_rows = [];
                            if ($scope.Ctk[0].POTypeName == "OPD") {
                                $scope.ctkUrl1 = "sales/OrderPengurusanDokumen?POId=" + $scope.Ctk[0].POId;
                            } else
                            if ($scope.Ctk[0].POTypeName == "Aksesoris") {

                                if ($scope.ListPO[0].POReferenceId == 1) {
                                    //pr
                                    $scope.ctkUrl1 = "sales/OrderPembelianAksesorisOptional?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                } else if ($scope.ListPO[0].POReferenceId == 2) {
                                    //internal
                                    $scope.ctkUrl1 = "sales/InternalOrderAksesoris?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                }
                            } else
                            if ($scope.Ctk[0].POTypeName == "Karoseri") {
                                $scope.ctkUrl1 = "sales/OrderPembelianAksesorisOptional?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                            } else
                            if ($scope.Ctk[0].POTypeName == 'Ekspedisi') {
                                $scope.ctkUrl1 = "sales/OrderPemesananEkspedisi?POId=" + $scope.Ctk[0].POId;
                            } else
                            if ($scope.Ctk[0].POTypeName == "Purna Jual") {
                                $scope.ctkUrl1 = "sales/OrderPembelianPurnaJual?POId=" + $scope.Ctk[0].POId;
                            }
                            // if($scope.Ctk[0].POTypeName == 'Swapping'){
                            //     $scope.ctkUrl1 = "sales/FakturKendaraanSwapping?BillingId=" + $scope.ListPO[0].FrameNo + "&FrameNo=" + $scope.ListPO[0].FrameNo;
                            // }else
                            // if($scope.Ctk[0].POTypeName == 'Finish Unit'){
                            //     $scope.ctkUrl1 = "sales/FakturKendaraanBaru?BillingId=" + $scope.ListPO[0].FrameNo + "&FrameNo=" + $scope.ListPO[0].FrameNo;
                            // }


                            //Call PrintRpt
                            var pdfFile = null;
                            //console.log('$scope.ctkUrl1skrgbgt',$scope.ctkUrl1xxxxxxxxxxxxx);
                            PrintRpt.print($scope.ctkUrl1).success(function(res) {
                                    var file = new Blob([res], { type: 'application/pdf' });
                                    var fileURL = URL.createObjectURL(file);

                                    //$scope.content = $sce.trustAsResourceUrl(fileURL);
                                    pdfFile = fileURL;
									
									MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
										function(res) {
											$scope.grid.data = res.data.Result;
											$scope.loading = false;
											$scope.btnKembaliFormMaintain();

											
											bsNotify.show(
													{
														title: "Sukses",
														content: "PO berhasil dibuat",
														type: 'success'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
										},
										function(err) {
											bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
										}
									);

                                    if (pdfFile != null)
									{
                                        printJS(pdfFile);
									}
                                    else
									{
                                        console.log("error cetakan", pdfFile);
									}
                                })
                                .error(function(res) {
                                    console.log("error cetakan", pdfFile);
                                });
                            
								
							
							
                        },
                        function(err) {
                            bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
                        }
                    );
                } else if ($scope.OperationMaintainPo == "Update") {
                    MaintainPOAksesorisStandarFactory.update($scope.ListPO[0]).then(
                        function(res) {
                            $scope.Ctk = res.data.Result;
                            if ($scope.Ctk[0].POTypeName == 'OPD') {
                                $scope.ctkUrl1 = "sales/OrderPengurusanDokumen?POId=" + $scope.Ctk[0].POId;
                            } else
                            if ($scope.Ctk[0].POTypeName == 'Aksesoris') {
                                if ($scope.ListPO[0].POReferenceId == 1) {
                                    //pr
                                    $scope.ctkUrl1 = "sales/OrderPembelianAksesorisOptional?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                } else if ($scope.ListPO[0].POReferenceId == 2) {
                                    //internal
                                    $scope.ctkUrl1 = "sales/InternalOrderAksesoris?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                                }

                            } else
                            if ($scope.Ctk[0].POTypeName == 'Karoseri') {
                                $scope.ctkUrl1 = "sales/OrderPembelianAksesorisOptional?POId=" + $scope.Ctk[0].POId + "&FrameNo=" + $scope.MaintainPoDaIntendedFrameNo;
                            } else
                            if ($scope.ListPO[0].POTypeName == 'Ekspedisi') {
                                $scope.ctkUrl1 = "sales/OrderPemesananEkspedisi?POId=" + $scope.Ctk[0].POId;
                            } else
                            if ($scope.Ctk[0].POTypeName == 'Purna Jual') {
                                $scope.ctkUrl1 = "sales/OrderPembelianPurnaJual?POId=" + $scope.Ctk[0].POId;
                            }
                            // if($scope.ListPO[0].POTypeName == 'Swapping'){
                            //     $scope.ctkUrl1 = "sales/FakturKendaraanSwapping?BillingId=" + $scope.ListPO[0].FrameNo + "&FrameNo=" + $scope.ListPO[0].FrameNo;
                            // }else
                            // if($scope.ListPO[0].POTypeName == 'Finish Unit'){
                            //     $scope.ctkUrl1 = "sales/FakturKendaraanBaru?BillingId=" + $scope.ListPO[0].FrameNo + "&FrameNo=" + $scope.ListPO[0].FrameNo;
                            // }


                            //Call PrintRpt
                            var pdfFile = null;
                            //console.log('$scope.ctkUrl1skrgbgt',$scope.ctkUrl1xxxxxxxxxxxxx);
                            PrintRpt.print($scope.ctkUrl1).success(function(res) {
                                    var file = new Blob([res], { type: 'application/pdf' });
                                    var fileURL = URL.createObjectURL(file);

                                    //console.log("pdf", fileURL);
                                    //$scope.content = $sce.trustAsResourceUrl(fileURL);
                                    pdfFile = fileURL;

                                    if (pdfFile != null)
									{
                                        printJS(pdfFile);
									}	
                                    else
									{
                                        console.log("error cetakan", pdfFile);
									}
									MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
										function(res) {
											$scope.grid.data = res.data.Result;
											$scope.loading = false;
											$scope.btnKembaliFormMaintain();

											
											bsNotify.show(
													{
														title: "Sukses",
														content: "PO berhasil dibuat",
														type: 'success'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
										},
										function(err) {
											bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
										}
									);
                                })
                                .error(function(res) {
                                    console.log("error cetakan", pdfFile);
                                });
								
								
                            
                        },
                        function(err) {
                            bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
                                                );
                                                $scope.disabledButtonSimpanCetak = false;
                        }
                    );
                }
            }
        }

        $scope.btnTutupdokumen = function() {
            $scope.CreateForm = false;
            $scope.PrintForm = false;
            $scope.MaintainForm = true;
        }

        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.ModalClosePO = function(item) {
            $scope.mMaintainPO = item;
            $scope.show_modal.show = !$scope.show_modal.show;
        }

        $scope.onListSave = function() {
            var StatPO = "?POId=" + $scope.mMaintainPO.POId + "&ReasonClosePO=" + $scope.mMaintainPO.ReasonClosePO;
            MaintainPOAksesorisStandarFactory.closePO(StatPO).then(
                function(res) {
                    
					MaintainPOAksesorisStandarFactory.getData($scope.filter).then(
						function(res) {
							$scope.grid.data = res.data.Result;
							$scope.loading = false;
							$scope.show_modal = { show: false };
							
							bsNotify.show(
													{
														title: "Sukses",
														content: "Close PO berhasil",
														type: 'success'
													}
												);
						},
						function(err) {
							bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
												);
						}
					);
					
					
                },
                function(err) {
                    $scope.show_modal = { show: false };
					bsNotify.show(
													{
														title: "Gagal",
														content: err.data.Message,
														type: 'danger'
													}
												);
                }
            );
            
        }

        $scope.onListCancel = function(item) {
            $scope.show_modal = { show: false };
        }

        $scope.closemodalwarning = function() {

        }

        $scope.clickhapusrowgrid = function(row) {
            //var index = $scope.gridPembelianMaterial.data.indexOf(row.entity);
            //$scope.gridPembelianMaterial.data.splice(index, 1);
            //kalo ga pake index pake PRId
            for (var i = 0; i < $scope.gridListPR.data.length; ++i) {
                if ($scope.gridListPR.data[i].PRId == row.entity.PRId) {
                    $scope.gridListPR.data.splice(i, 1);
                }
            }

            for (var i = 0; i < $scope.gridListBirojasa.data.length; ++i) {
                if ($scope.gridListBirojasa.data[i].PRId == row.entity.PRId) {
                    $scope.gridListBirojasa.data.splice(i, 1);
                }
            }
            //abis bisa apus di ni 2 tabel baru dibawahini bikin logika buat tabel baru gridPembelianMaterial


            // for (var i = 0; i < $scope.gridPembelianMaterial.data.length; ++i) {
                // if ($scope.gridPembelianMaterial.data[i].MaterialId == row.entity.AccessoriesId) {
                    // $scope.gridPembelianMaterial.data[i].Qty -= row.entity.Qty;
                    // var Da_Total_Price_Paksa = ($scope.gridPembelianMaterial.data[i].Price - $scope.gridPembelianMaterial.data[i].Discount) * (1.1) * ($scope.gridPembelianMaterial.data[i].Qty);
                    // $scope.gridPembelianMaterial.data[i].TotalPrice = Da_Total_Price_Paksa;

                    // $scope.gridPembelianMaterial.data[i].PPN = ($scope.gridPembelianMaterial.data[i].Price - $scope.gridPembelianMaterial.data[i].Discount) * 0.1;
                // }
            // }
			//refi sama andri minta 4 maret 2019 jadi ini dibawah

			for (var i = 0; i < $scope.gridPembelianMaterial.data.length; ++i) {
                if ($scope.gridPembelianMaterial.data[i].PRId == row.entity.PRId) {
                    $scope.gridPembelianMaterial.data.splice(i, 1);
                }
            }


            var Da_total = 0;
            for (var i = 0; i < $scope.gridPembelianMaterial.data.length; ++i) {
                Da_total += $scope.gridPembelianMaterial.data[i].TotalPrice;
            }
            $scope.ListPO[0].GrandTotal = Da_total;



        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        var ClickNoPO = '<a style="color:blue;" ng-click="grid.appScope.$parent.PONOClick(row.entity)"><p style="padding:5px 0 0 5px"><u>{{row.entity.POCode}}</u></p></a>';
        var actionClick = '<span><p style="padding:5px 0 0 5px" ><u tooltip-placement="bottom" uib-tooltip="Lihat PO"  ng-click="grid.appScope.$parent.btnLihatPO(row.entity)"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:10px;"></i></u>&nbsp;<u tooltip-placement="bottom" uib-tooltip="Close PO" ng-if="row.entity.POTypeId != 6 &&(row.entity.POStatusName!=\'Completed\' && row.entity.POStatusName!=\'Closed\')" ng-click="grid.appScope.$parent.ModalClosePO(row.entity)"><i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:10px;"></i></u></span>'
        //var HapusClick = '<span style="color:blue"><p style="padding:5px 0 0 5px"><u ng-click="grid.appScope.clickhapusrowgrid(row)" >Hapus</u></span>'
        var HapusClick = '<div ><a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Hapus Material" tooltip-placement="right" onclick="this.blur()" ng-hide="grid.appScope.OperationMaintainPo == \'Lihat\' || grid.appScope.OperationMaintainPo == \'Update\' ||row.entity.TombolDetail == false" ng-click="grid.appScope.clickhapusrowgrid(row)" tabindex="0"> ' +
        '<i class="fa fa-fw fa-lg fa-times" style="padding:8px 8px 8px 0px;margin-left:8px;"></i>' +
        '</a></div>'
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: $scope.pageArray,
            //paginationPageSize: 10,
            columnDefs: [
                { name: 'POId', field: 'POId', visible: false },
                { displayName:'Tanggal PO' ,name: 'Tanggal PO', field: 'PODate', cellFilter: 'date:\'dd/MM/yyyy\'' },
                {
                    displayName:'PO No' ,
					field: 'POCode',
					name: 'PO No',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: true,
                    enableColumnResizing: true,
                    // cellTemplate: ClickNoPO
                },
                { name: 'Vendor', field: 'Name', width: '15%' },
                { displayName:'Status PO' ,name: 'Status PO', field: 'POStatusName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    enableColumnMenu: false,
                    enableSorting: false,
                    width: '13%',
                    enableColumnResizing: true,
                    cellTemplate: actionClick
                }
            ]
        };

        $scope.gridListPR = {
            // enableSorting: true,
            // enableRowSelection: false,
            // multiSelect: false,
            // enableSelectAll: false,
			enableColumnResizing: true,
            //data: 'ListMasterDokumen',
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSizes: $scope.pageArray,
            //paginationPageSize: 10,
            columnDefs: [
                { name: 'PRId', field: 'PRId', visible: false },
                { name: 'AccessoriesId', field: 'AccessoriesId', visible: false },
                { displayName:'No. SO' ,name: 'no so', field: 'SoCode' },
                { displayName:'No. PR' ,name: 'no pr', field: 'PRCode' },
                { displayName:'Kode Jasa' ,name: 'Kode Jasa', field: 'AccessoriesCode' },
                { displayName:'Nama Material' ,name: 'Nama Material', field: 'AccessoriesName' },
                { displayName:'Satuan' ,name: 'UOM', field: 'UomName' },
                { name: 'Qty', width: '7%', field: 'Qty' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '8%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: HapusClick
                }
            ]
        };

        $scope.gridListPR.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedListPR = gridApi.selection.getSelectedRows();
                console.log('$scope.selected satu" ListPR ===>', $scope.selectedListPR);
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectedListPR = gridApi.selection.getSelectedRows();
                console.log('$scope.selected Batch ListPR ===>',$scope.selectedListPR);
                //galang
            });
        };

        $scope.gridListDaftarPembelianUnitMaintainPo = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnResizing: true,
            //data: 'ListMasterDokumen',
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSizes: $scope.pageArray,
            columnDefs: [
                { name: 'Model Tipe', field: 'AccessoriesName' },
                { name: 'Nomor Rangka', field: 'FrameNo' }
            ]
        };

        $scope.gridListDaftarPembelianUnitMaintainPo.onRegisterApi = function(gridApi) {
            $scope.gridApiDaftarPembelianUnitMaintainPo = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedListDaftarPembelianUnitMaintainPo = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectedListDaftarPembelianUnitMaintainPo = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridListBirojasa = {
            // enableSorting: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
			enableColumnResizing: true,
            //data: 'ListMasterDokumen',
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSizes: $scope.pageArray,
            columnDefs: [
                { name: 'PRId', field: 'PRId', visible: false },
                { name: 'AccessoriesId', field: 'AccessoriesId', visible: false },
                { displayName:'No. SO' ,name: 'No SO', field: 'SoCode' },
                { displayName:'No. PR' ,name: 'No PR', field: 'PRCode', width: '20%' },
                { name: 'Kode Jasa', field: 'AccessoriesCode' },
                { name: 'Nama Jasa', field: 'AccessoriesName' },
                { name: 'Qty', field: 'Qty' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '15%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: HapusClick
                }
            ]
        };
        
        $scope.gridAcc = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: $scope.pageArray,
            //paginationPageSize: 10,
            columnDefs: [
                { name: 'PODetailId', field: 'PODetailId', width: '20%', visible: false },
                { name: 'Kode Aksesoris', field: 'AccessoriesCode', width: '20%', enableCellEdit: false },
                { name: 'Nama Aksesoris', field: 'AccessoriesName', enableCellEdit: false },
                { name: 'Satuan', field: 'Satuan', enableCellEdit: false },
                // { name: 'Harga', field: 'Price', cellFilter: 'currency:\'Rp. \':2', enableCellEdit: false },
                {
                    name: 'Qty',
                    field: 'Qty',
                    format: '{0:c}',
                    type: 'number',  
                    enableCellEdit: false
                },
                { displayName: ' DPP', name: 'DPP', field: 'DPP', cellFilter: 'currency:\'Rp. \':2', enableCellEdit: false },
                { displayName: 'PPN', name: 'PPN', field: 'PPN', cellFilter: 'currency:\'Rp. \':2', enableCellEdit: false },
                { name: 'Harga Total', field: 'HargaTotal', cellFilter: 'currency:\'Rp. \':2', enableCellEdit: false },
                // { displayName:'No SO' ,name: 'No SO', field: 'SOCode', enableCellEdit: false, visible: true }
            ]
        };

        $scope.gridListBirojasa.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedBirojasa = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectedBirojasa = gridApi.selection.getSelectedRows();
            });
        };
		
		$scope.MaintainPoBisaEditgridPembelianMaterial = function () 
		{ 
			if($scope.OperationMaintainPo=="Lihat" || $scope.OperationMaintainPo=="Update")
			{
				return false; 
			}
			else
			{
				return true; 
			}
			
		};

        $scope.gridPembelianMaterial = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnResizing: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Material Id', field: 'MaterialId', visible: false },
                { displayName: 'Kode Material/Jasa',name: 'Kode Material', field: 'MaterialCode'},
                { displayName:'Nama Material' ,name: 'Nama Jasa', field: 'MaterialName', enableCellEdit: false },
                { name: 'Qty', field: 'Qty', enableCellEdit: false , width: '7%'},
                { name: 'Satuan', field: 'UomName', enableCellEdit: false , width: '7%'},
                { name: 'Harga Satuan', field: 'Price', cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {return 'canEdit';}, enableCellEdit: true,cellEditableCondition: $scope.MaintainPoBisaEditgridPembelianMaterial, cellFilter: 'currency:\'Rp. \':2' },
                { name: 'Diskon', field: 'Discount', cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {return 'canEdit';}, enableCellEdit: true,cellEditableCondition: $scope.MaintainPoBisaEditgridPembelianMaterial, format: '{0:c}', type: 'number', cellFilter: 'currency:\'Rp. \':2'},
               // { displayName:'PPN' ,name: 'Ppn', field: 'PPN', enableCellEdit: false, cellFilter: 'currency:\'Rp. \':2' },
                { name: 'Harga Total', field: 'TotalPrice', enableCellEdit: false, cellFilter: 'currency:\'Rp. \':2' },
				{ name: 'Keterangan', field: 'Keterangan', enableCellEdit: false , width: '17%'},
            ]
        };

        $scope.gridPembelianMaterial.onRegisterApi = function(gridApi) {
            $scope.gridApiPembelianMaterialMaintainPo = gridApi;
			
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedRowPembelianMaterial = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectedRowPembelianMaterial = gridApi.selection.getSelectedRows();
            });

            gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                
                if(colDef.field=="Discount")
				{
					if(newValue==null || (typeof newValue=="undefined"))
					{
						newValue=0;
						
					}
					var strings = angular.copy(newValue);
					
					var tempString = strings.toString();
					console.log(strings);
					var len = tempString.length;

					if (newValue < 0) {
						newValue = 0;
						rowEntity[colDef.field] = newValue;
						bsNotify.show({
							title: "Peringatan",
							content: "Nilai diskon tidak boleh kurang dari 0.",
							type: 'warning'
						});     
						rowEntity.TotalPrice = angular.copy(rowEntity.Price);    
					} else if (tempString.length > 18 && newValue <= rowEntity.Price){ 
						var tempVal = tempString.slice(0,18);
						rowEntity[colDef.field] = parseInt(tempVal);
						bsNotify.show({
							title: "Peringatan",
							content: "Nilai diskon tidak boleh lebih dari 18 digit.",
							type: 'warning'
						}); 
						rowEntity.TotalPrice = angular.copy(rowEntity.Price);  
					} else if(newValue > rowEntity.Price){
						newValue = 0;
						rowEntity[colDef.field] = newValue;
						bsNotify.show({
							title: "Peringatan",
							content: "Nilai diskon tidak boleh lebih dari Harga Satuan.",
							type: 'warning'
						});
						rowEntity.TotalPrice = angular.copy(rowEntity.Price);  
					}else{
						var Da_Total_Price = (rowEntity['Price'] - newValue) * (rowEntity['Qty']);
						rowEntity['TotalPrice'] = Da_Total_Price;
						var Da_total = 0;
		
						for (var i = 0; i < $scope.gridPembelianMaterial.data.length; ++i) {
							Da_total += $scope.gridPembelianMaterial.data[i].TotalPrice;
						}
		
						$scope.ListPO[0].GrandTotal = Da_total; 
						rowEntity['PPN'] = (rowEntity['Price'] - rowEntity['Discount']) * 0.1;
					}
				}
				
				if(colDef.field=="Price")
				{
					if(newValue==null || (typeof newValue=="undefined"))
					{
						newValue=0;
						
					}
					if (newValue < 0) {
						newValue = oldValue;
						rowEntity[colDef.field] = newValue;
						bsNotify.show({
							title: "Peringatan",
							content: "Nilai harga satuan tidak boleh kurang dari 0.",
							type: 'warning'
						});
					}
					else if(newValue < rowEntity.Discount){
						//tadinya ada sama dengan diatas
						newValue = oldValue;
						rowEntity[colDef.field] = newValue;
						bsNotify.show({
							title: "Peringatan",
							content: "Nilai Harga Satuan tidak boleh kurang/sama dengan diskon.",
							type: 'warning'
						});
					}
					else
					{
						var Da_Total_Price = (newValue - rowEntity['Discount']) * (rowEntity['Qty']);
						rowEntity['TotalPrice'] = Da_Total_Price;
		
						var Da_total = 0;
		
						for (var i = 0; i < $scope.gridPembelianMaterial.data.length; ++i) {
							Da_total += $scope.gridPembelianMaterial.data[i].TotalPrice;
						}
		
						$scope.ListPO[0].GrandTotal = Da_total; 
						rowEntity['PPN'] = (newValue - rowEntity['Discount']) * 0.1;
					}
				}
				
				
               

            });


        };

        $scope.gridPembelianMaterialJasa = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnResizing: true,
            //data: 'ListMasterDokumen',
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSizes: $scope.pageArray,
            columnDefs: [
                { name: 'Material Id', field: 'MaterialId', visible: false },
                { displayName: 'Kode Material',name: 'Kode Jasa', field: 'MaterialCode', visible: true },
                { displayName:'Nama Jasa' ,name: 'Nama Jasa', field: 'MaterialName', enableCellEdit: false },
                { name: 'Qty', field: 'Qty', enableCellEdit: false },
                { name: 'Satuan', field: 'UomName', enableCellEdit: false },
                { name: 'Harga Satuan', field: 'Price', enableCellEdit: false, cellFilter: 'currency:\'Rp. \':2' },
                { name: 'Diskon', field: 'Discount', format: '{0:c}', type: 'number', cellFilter: 'currency:\'Rp. \':2'},
               // { displayName:'PPN' ,name: 'Ppn', field: 'PPN', enableCellEdit: false, cellFilter: 'currency:\'Rp. \':2' },
                { name: 'Harga Total', field: 'TotalPrice', enableCellEdit: false, cellFilter: 'currency:\'Rp. \':2' }
            ]
        };

        $scope.gridPembelianMaterialJasa.onRegisterApi = function(gridApi) {
            $scope.gridApiPembelianMaterialJasa = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedRowPembelianMaterial = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.selectedRowPembelianMaterial = gridApi.selection.getSelectedRows();
            });

            $scope.gridApiPembelianMaterialJasa.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                console.log(rowEntity);
                var strings = angular.copy(newValue);
                var tempString = strings.toString();
                console.log(strings);
                var len = tempString.length;

                if (newValue < 0) {
                    newValue = 0;
                    rowEntity[colDef.field] = newValue;
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai diskon tidak boleh kurang dari 0.",
                        type: 'warning'
                    });     
                    rowEntity.TotalPrice = angular.copy(rowEntity.Price);    
                } else if (tempString.length > 18 && newValue <= rowEntity.Price){ 
                    var tempVal = tempString.slice(0,18);
                    rowEntity[colDef.field] = parseInt(tempVal);
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai diskon tidak boleh lebih dari 18 digit.",
                        type: 'warning'
                    }); 
                    rowEntity.TotalPrice = angular.copy(rowEntity.Price);  
                } else if(newValue > rowEntity.Price){
                    newValue = 0;
                    rowEntity[colDef.field] = newValue;
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai diskon tidak boleh lebih dari Harga Satuan.",
                        type: 'warning'
                    });
                    rowEntity.TotalPrice = angular.copy(rowEntity.Price);  
                }else{
                    var Da_Total_Price = (rowEntity['Price'] - newValue) * (rowEntity['Qty']);
                    rowEntity['TotalPrice'] = Da_Total_Price;
    
                    var Da_total = 0;
    
                    for (var i = 0; i < $scope.gridPembelianMaterialJasa.data.length; ++i) {
                        Da_total += $scope.gridPembelianMaterialJasa.data[i].TotalPrice;
                    }
    
                    $scope.ListPO[0].GrandTotal = Da_total; 
                    rowEntity['PPN'] = (rowEntity['Price'] - rowEntity['Discount']) * 0.1;
                }
               

            });


        };
		
		$scope.columnDefsgridDataAksesorisMaintainPo=[
                { name: 'MaterialId', field: 'MaterialId', width: '20%', visible: false },
                { name: 'Aksesoris No', field: 'MaterialCode', width: '20%', enableCellEdit: false },
                { name: 'Aksesoris Name', field: 'MaterialName', enableCellEdit: false },
                { name: 'Satuan', field: 'UomName', enableCellEdit: false },
                { name: 'Harga', field: 'Price', cellFilter: 'currency:\'Rp. \':2', enableCellEdit: false },
                {
                    name: 'Qty',
                    field: 'Qty',
                    format: '{0:c}',
                    type: 'number',  
                    enableCellEdit: false
                },
                { name: 'Ppn', field: 'PPN', cellFilter: 'currency:\'Rp. \':2', enableCellEdit: false },
                { name: 'Harga Total', field: 'TotalPrice', cellFilter: 'currency:\'Rp. \':2', enableCellEdit: false },
				{ displayName:'No SO' ,name: 'No SO', field: 'SOCode', enableCellEdit: false, visible: true }
            ];

        $scope.gridDataAksesorisMaintainPo = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
			enableColumnResizing: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSizes: $scope.pageArray,
            columnDefs: $scope.columnDefsgridDataAksesorisMaintainPo
        };

        $scope.gridDataAksesorisMaintainPo.onRegisterApi = function(gridApi) {
            $scope.gridApi_gridDataAksesorisMaintainPo = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.MaintainPoAksesorisRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.MaintainPoAksesorisRow = gridApi.selection.getSelectedRows();
            });
        };

        $scope.gridDataOpdDanEkspedisi = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'ListMasterAksesoris',
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSizes: $scope.pageArray,
            
            columnDefs: [
                { name: 'MaterialId', field: 'MaterialId', width: '20%', visible: false },
                { name: 'Kode Material/Jasa', field: 'MaterialCode', width: '20%', visible: true , enableCellEdit: false},
                { name: 'Nama Jasa', field: 'MaterialName', width: '20%', visible: true , enableCellEdit: false},
                { name: 'Qty', field: 'Qty', width: '20%', visible: true , enableCellEdit: false},
                { name: 'Satuan', field: 'UomName', width: '20%', visible: true , enableCellEdit: false},
                { name: 'Harga', field: 'Price', width: '20%', visible: true, cellFilter: 'currency:\'Rp. \':2' },
                { name: 'Discount', field: 'Discount', width: '20%', visible: true, format: '{0:c}',
                type: 'number', cellFilter: 'currency:\'Rp. \':2' },
               // { name: 'PPN', field: 'PPN', width: '20%', visible: true, cellFilter: 'currency:\'Rp. \':2' },
                { name: 'Harga Total', field: 'TotalPrice', width: '20%', enableCellEdit: true, cellFilter: 'currency:\'Rp. \':2' }
            ]
        };

        ///date:\'dd/MM/yyyy\'

        $scope.gridDataOpdDanEkspedisi.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.MaintainPoOpdDanEkspedisiRow = gridApi.selection.getSelectedRows();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                $scope.MaintainPoOpdDanEkspedisiRow = gridApi.selection.getSelectedRows();
            });

            gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {

                if (newValue < 0) {
                    newValue = 0;
                    rowEntity[colDef.field] = newValue;
                    bsNotify.show({
                        title: "Peringatan",
                        content: "Nilai target tidak boleh kurang dari 0.",
                        type: 'warning'
                    });          
                } else {
                   
                }


            });
        };
    });