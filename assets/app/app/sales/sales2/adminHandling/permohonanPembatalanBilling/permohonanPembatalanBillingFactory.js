angular.module('app')
  .factory('PermohonanPembatalanBillingFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/fw/Role');
        //console.log('res=>',res);
        return res;
      },
      create: function(permohonanPembatalanBillingFactory) {
        return $http.post('/api/fw/Role', [{
                                            Id: permohonanPembatalanBillingFactory.SOId,
										                        SODate: permohonanPembatalanBillingFactory.SODate,
                                            Status: permohonanPembatalanBillingFactory.StatusSO,
                                            NOSPK: permohonanPembatalanBillingFactory.NOSPK,
                                            ProspectId:permohonanPembatalanBillingFactory.ProspectId,
                                            ProspectName:permohonanPembatalanBillingFactory.ProspectName
                                            }]);
      },
      update: function(permohonanPembatalanBillingFactory){
        return $http.put('/api/fw/Role', [{
                                            SOId: permohonanPembatalanBillingFactory.SOId,
										                        SODate: permohonanPembatalanBillingFactory.SODate,
										                        Type: permohonanPembatalanBillingFactory.SOType,
                                            Status: permohonanPembatalanBillingFactory.StatusSO,
                                            NOSPK: permohonanPembatalanBillingFactory.NOSPK,
                                            ProspectId:permohonanPembatalanBillingFactory.ProspectId,
                                           ProspectName:permohonanPembatalanBillingFactory.ProspectName
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });




