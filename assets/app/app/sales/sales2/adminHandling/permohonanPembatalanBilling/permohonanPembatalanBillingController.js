angular.module('app')
.controller('PermohonanPembatalanBillingController', function($scope, $http, CurrentUser, PermohonanPembatalanBillingFactory,$timeout) {
    
    var listBillingType = [{
                Id: "1",
                Name: "Finish-Unit"
            },
            {
                Id: "2",
                Name: "OPD"
            },
            {
                Id: "3",
                Name: "Purna Jual"
            }

        ];

    $scope.selectionType = listBillingType;

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mPermohonanPembatalanBilling = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function() {
        PermohonanPembatalanBillingFactory.getData()
        .then(
            function(res){
                gridData = [];
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
				
				var x = [
					{
						"TypeModel":"Avanza 13 E/MT",
                        "Keterangan":"Harga Jual",
                        "Currency":"IDR",
                        "Nominal":"135.000.000"
					},
					{
						"TypeModel":"",
                        "Keterangan":"BBN",
                        "Currency":"IDR",
                        "Nominal":"0"
					},
					{
                        "TypeModel":"",
                        "Keterangan":"Diskon",
                        "Currency":"IDR",
                        "Nominal":"1000000"
                    },
                    {
                        "TypeModel":"",
                        "Keterangan":"DPP",
                        "Currency":"IDR",
                        "Nominal":"0"
                    },
                    {
                        "TypeModel":"",
                        "Keterangan":"VAT",
                        "Currency":"IDR",
                        "Nominal":"0"
                    },
                    {
                        "TypeModel":"",
                        "Keterangan":"Harga Setelah Pajak",
                        "Currency":"IDR",
                        "Nominal":"134000000"
                    },
                    {
                        "TypeModel":"talang Air",
                        "Keterangan":"Harga Jual",
                        "Currency":"IDR",
                        "Nominal":"0"
                    }
				];
	
				
                //$scope.grid.data = res.data.Result;
				$scope.grid.data = x;
                console.log("MaintainSO=>",res.data.Result);
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
	
	
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            // { name:'No SPK/PO No',    field:'NoSPK', width:'7%', visible:false },
            { name:'Type Model', field:'TypeModel'},
            { name:'Keterangan', field:'Keterangan'},
            { name:'Currency', field:'Currency'},
            { name:'Nominal', field:'Nominal'}
        ]
    };
});
