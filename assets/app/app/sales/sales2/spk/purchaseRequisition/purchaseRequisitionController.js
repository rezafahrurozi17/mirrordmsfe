angular.module('app')
    .controller('PurchaseRequisitionController', function (bsNotify, $rootScope, $scope, $http, $httpParamSerializer, CurrentUser, MaintainPOFactory, PurchaseRequisitionFactory, $timeout) {

        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
        });

        //----------------------------------
        // Initialization
        //----------------------------------

        $scope.user = CurrentUser.user();
        $scope.selected_rows = [];
        $scope.mpurchaseRequisition = null; //Model
        //$scope.filter = { PRTypeId: null, sOCode: null, accessoriesName: null, VendorId: null };
        $scope.filter = { PRTypeId: null, sOCode: null, accessoriesName: null };
		$scope.num = 0;
        $scope.numview = 1;
        $scope.len = null;
        $scope.MaintainPR = true;

        $scope.$on('$destroy', function () {
            angular.element('.ui.small.modal.batalPR').remove();

        });


        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.next = function () {
            $scope.num = $scope.num + 1;
            $scope.numview = $scope.numview + 1;
        }

        $scope.prev = function () {
            $scope.num = $scope.num - 1;
            $scope.numview = $scope.numview - 1;
        }

        $rootScope.$on("KembalidariPOValid", function () {
            if($rootScope.DariPrKePoAbisInsert==true)
			{
				$scope.filter.PRTypeId=angular.copy($rootScope.PaksaSearchPrDariPo_PRTypeId);
				$scope.filter.VendorId=angular.copy($rootScope.PaksaSearchPrDariPo_VendorId);
			}
			$scope.backfromPO();
        });

        $scope.backfromPO = function () {
            $scope.MaintainPR = true;
            $scope.formMaintainPO = false;
            $scope.getData();
        }

        $scope.childmethod = function () {
            $rootScope.$emit("buatPOSPK", {});
        }



        $scope.buatPO = function () {
            var count = 0;

            // angular.forEach($scope.selected_rows, function (Row, RowIdx) {
                // Row.VendorId = $scope.filter.VendorId;
            // });

            if (count >= 1) {
                bsNotify.show({
                    title: "Peringatan",
                    content: "Terdapat vendor yang sama.",
                    type: 'warning'
                });
            } else {
                $rootScope.mMaintainPO = $scope.selected_rows;
                $scope.childmethod();
                $scope.formMaintainPO = true;
                $scope.MaintainPR = false;
            }
        }

        PurchaseRequisitionFactory.getCategoryPR().then(
            function (tipe) {
                var Filter_Category_Pr=tipe.data.Result;
				for(var i = 0; i < Filter_Category_Pr.length; ++i)
				{	
					if(Filter_Category_Pr[i].PRTypeName=="Ekspedisi")
					{
						Filter_Category_Pr.splice(i, 1);
					}
				}
				$scope.getDataPR = Filter_Category_Pr;
				//console.log("setan",tipe.data.Result);
                // return tipe.data;
            }
        );


        PurchaseRequisitionFactory.getDataSOCode().then(
            function (res) {
                $scope.listSo = res.data.Result;
            }
        );

        $scope.BatalPR = function () {
            angular.element('.ui.small.modal.batalPR').modal('show');
            angular.element('.ui.small.modal.batalPR').not(':first').remove();
            $scope.batal = angular.copy($scope.selected_rows);
            for (var i in $scope.batal) {
                $scope.batal[i] = { PRId: $scope.batal[i].PRId, NoteCancelPR: "", CancelPRId: null };
            }
            $scope.len = $scope.batal.length;
            $scope.batal[$scope.num];
        }

        var gridData = [];
        $scope.getData = function () {
            if ($scope.filter.PRTypeId == null) {
                $scope.loading = false;
				bsNotify.show({
                    title: "Peringatan",
                    content: "Silahkan pilih tipe PR.",
                    type: 'warning'
                });
            } else {
                $scope.loading = true;
                PurchaseRequisitionFactory.getData($scope.filter).then(
                    function (res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                    },
                    function (err) {
                        $scope.loading = false;
                        console.log("err=>", err);
                    }
                );
            }
			
			// else if ($scope.filter.VendorId == null) {
                // $scope.loading = false;
				// bsNotify.show({
                    // title: "Peringatan",
                    // content: "Silahkan pilih vendor.",
                    // type: 'warning'
                // });
            // } 

        }
        $scope.getDataVendorList = function () {
            if ($scope.filter.PRTypeId == null || $scope.filter.PRTypeId == undefined) {
                $scope.filter.VendorId = null;
            } else {
                PurchaseRequisitionFactory.getDataVendorList($scope.filter).then(
                    function (res) {
                        $scope.filter.VendorId=null;
						$scope.DataVendor=[];
                        $scope.DataVendor = res.data.Result;
                    }
                );
            }

        }

        $scope.SimpanBatalPR = function () {
            //console.log("asdafaf",$scope.batal);
            PurchaseRequisitionFactory.update($scope.batal).then(
                function (res) {
                    angular.element('.ui.small.modal.batalPR').modal('hide');
                    bsNotify.show({
                        title: "Berhasil",
                        content: "PR berhasil dibatalkan.",
                        type: 'success'
                    });
                    $scope.getData();
                },
                function (err) {
                    console.log("err=>", err);
                    var message = err.data.Message;
                    console.log("asdasd", message);
                    if (message.toLowerCase().includes("exception") == true || message.toLowerCase().includes("error") == true) {
                        bsNotify.show({
                            title: "Gagal",
                            content: "PR gagal dibatalkan.",
                            type: 'danger'
                        });
                    } else {
                        bsNotify.show({
                            title: "Gagal",
                            content: message,
                            type: 'danger'
                        });
                    }

                }
            )
        }

        $scope.keluarBatalPR = function () {
            angular.element('.ui.small.modal.batalPR').modal('hide');
        }

        PurchaseRequisitionFactory.getDataCategoryCancle().then(function (res) {
            $scope.reasonCancel = res.data.Result;
        })


        // $scope.selectRole = function(rows) {
        //     console.log("onSelectRows=>", rows);
        //     $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        // }

        $scope.onSelectRows = function (rows) {
            console.log("onSelectRows=>", rows);
            $scope.selected_rows = rows;
			$scope.AdaOpdDiPrDipilih=false;
			
			for(var i = 0; i < $scope.selected_rows.length; ++i)
			{	
				if($scope.selected_rows[i].PRTypeName == "OPD")
				{
					$scope.AdaOpdDiPrDipilih=true;
					break;
				}
			}
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                //{ name: 'id', field: 'Id', visible: false },
                { displayName: 'Tipe PR', name: 'tipe pr', width:'10%', field: 'PRTypeName' },
                { displayName: 'No. PR', name: 'no pr', field: 'PRCode' },
                { displayName: 'Tanggal PR', name:'tanggal pr', width:'10%', field: 'PRDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { displayName: 'Nama Pembeli',name:'nama pembeli', field: 'CustomerName' },
                { displayName: 'No. SO', name:'no so',field: 'SoCode' },
                { displayName: 'Kode Material', name:'kode material', field: 'AccessoriesCode' },
                { displayName: 'Nama Material', name: 'nama material', field: 'AccessoriesName' },
                { displayName: 'Status', name: 'status', width:'10%', field: 'StatusPRName' }
            ]
        };

    });