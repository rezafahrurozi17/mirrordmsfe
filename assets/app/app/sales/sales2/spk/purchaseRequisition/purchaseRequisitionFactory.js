angular.module('app')
    .factory('PurchaseRequisitionFactory', function($http, $httpParamSerializer, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(filter) {
                var param = $httpParamSerializer(filter);
                if (param == undefined || param == null || param == "") {
                    param = "";
                } else {
                    param = "&" + param;
                }
                // var res = $http.get('/api/sales/SPKPRDetail/Filter/?start=1&limit=100' + param);
                var res = $http.get('/api/sales/SPKPRDetail/Filter/?start=1&limit=100' + param);
                
                return res;
            },
            getDataSOCode: function() {
                var res = $http.get('/api/sales/ListSOCode');
                return res;
            },
            getDataCategoryCancle: function() {
                var res = $http.get('/api/sales/PCategoryCancelPR');
                return res;
            },
            getDataAksesoris: function() {
                var res = $http.get('/api/sales/AccessoriesSatuan');
                return res;
            },
            // create: function(SpkTaking) {
            //     return $http.post('/api/fw/Role', [{
            //         NamaSales: SpkTaking.NamaSales,

            //     }]);
            // },
            getCategoryPR: function() {
                var res = $http.get('/api/sales/PCategoryPRType');
                return res;
            },
            update: function(SpkTaking) {
                return $http.put('/api/sales/SAHPRBatal', SpkTaking);
            },
            // delete: function(id) {
            //     return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            // },
            getDataAksesoris: function() {
                var res = $http.get('/api/sales/AccessoriesSatuan');
                return res;
            },

            getDataVendorList: function(filter) {
                var param = $httpParamSerializer(filter);
                var res = $http.get('/api/sales/MVendorList/?' + param);
                return res;
            },
        }
    });