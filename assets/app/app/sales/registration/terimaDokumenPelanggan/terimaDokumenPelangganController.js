angular.module('app')
    .controller('TerimaDokumenPelangganController', function($scope, $http, CurrentUser, TerimaDokumenPelangganFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];

            TerimaDokumenPelangganFactory.getStatus().then(function(res) {
                $scope.status = res.data.Result;
            });

            TerimaDokumenPelangganFactory.getOutletCabang().then(function(res) {
                $scope.outletCabang = res.data.Result;
            });
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log("token", $scope.user);
        $scope.mterimaDokumenPelanggan = null; //Model
        $scope.gridDokumenPelanggan = true;
        //$scope.xRole = { selected: [] };
        $scope.filter = { Id: 0, OutletId: null, TerimaDokumenStatusId: null, SuratDistribusiNo: null };

        $scope.listContainer = true;
        $scope.listDetail = false;
        $scope.rowsDocument = null;
        $scope.formData = false;
        $scope.noRangka = false;
        $scope.num = 0;
        $scope.numview = 1;
        //$scope.selctedRow = [{NoRangka:null,NamaPelanggan:null,VehicleModel:null}];

        //$scope.frameno = false;
        //----------------------------------
        // Get Data
        //----------------------------------

        var gridData = [];
        $scope.getData = function() {
            TerimaDokumenPelangganFactory.getData($scope.filter).then(function(res) {
                    $scope.grid.data = res.data.Result;
                    //console.log("", $scope.grid.data);
                    angular.forEach($scope.grid.data, function(Doc, DocIndx) {
                        angular.forEach(Doc.listFrameNo, function(Frame, FrameIndx) {
                            angular.forEach(Frame.listDetail, function(Detail, DetailIndx) {
                                if (Detail.ReceivedDate == null || Detail.ReceivedDate == undefined) {
                                    Detail.ReceivedDateBit = false;
                                } else {
                                    Detail.ReceivedDateBit = true;
                                }

                            });
                        });
                    });
                },
                function(err) {
                    console.log("err=>", err);
                }

            )
        };

        // $scope.gridNoRangka.

        // $scope.getDataRangka = function() {
        //     $scope.gridNoRangka.data = MaintainSuratDistribusiFactory.getDataNoRangka();
        // }

        $scope.rowselected = {};
        $scope.rowsDocument = [];
        $scope.rowsDocuments = [];

        $scope.UpdateDokumen = function(row) {
            angular.element('.ui.modal.formDocumenUpload').modal('refresh').modal('show');
            $scope.rowselected = row;
            console.log("rowsselected", $scope.rowselected);

            $scope.rowsDocument = row.listFrameNo;

            $scope.len = $scope.rowsDocument.length;
            console.log("rows", $scope.rowsDocument);
            $scope.rowsDocument[$scope.num];
        }

        $scope.simpanupdate = function() {
            console.log("$scope.rowsDocument", $scope.rowsDocument);
            // for (var i in $scope.rowsDocument) {
            //     if ($scope.rowsDocument[i].SuratDistribusiAjuAFIKeTAMStatusCheck == true) {
            //         $scope.rowsDocument[i].SuratDistribusiAjuAFIKeTAMStatusId = 1;
            //     } else {
            //         $scope.rowsDocument[i].SuratDistribusiAjuAFIKeTAMStatusId = 2;
            //     }
            // }

            $scope.rowselected.listFrameNo = $scope.rowsDocument;
            console.log("rowupdate", $scope.rowselected);
            TerimaDokumenPelangganFactory.update($scope.rowselected).then(function(res) {
                angular.element('.ui.modal.formDocumenUpload').modal('refresh').modal('hide');
                //  $scope.gridDokumenPelanggan = true, $scope.DetailDokumen = false;
                $scope.getData();
            })
        }

        $scope.simpanupdatesingle = function() {
            $scope.temprowsDocuments = [];
            $scope.temprowsDocuments.push($scope.rowsDocuments);
            console.log("$scope.temprowsDocuments", $scope.temprowsDocuments)
            $scope.rowselected.listFrameNo = $scope.temprowsDocuments;
            console.log("rowupdate", $scope.rowselected)
            TerimaDokumenPelangganFactory.update($scope.rowselected).then(function(res) {
                angular.element('.ui.modal.formDocumenUploadsingle').modal('refresh').modal('hide');
                $scope.gridDokumenPelanggan = true, $scope.DetailDokumen = false;
                $scope.getData();
            })
        }

        var i = 0;

        $scope.next = function() {
            $scope.num = $scope.num + 1;
            $scope.numview = $scope.num + 1;

        }

        $scope.prev = function() {
            $scope.num = $scope.num - 1;
            $scope.numview = $scope.num - 1;
        }


        $scope.listRangka = function(row) {
            console.log("test docbit", row);
            angular.element('.ui.modal.formDocumenUploadsingle').modal('show');
            // console.log("selected rows", row);
            $scope.rowsDocuments = row;
            console.log("rows", $scope.rowsDocuments);
            //$scope.rowsDocument[0];
        }

        $scope.lihatDetail = function(row) {
            $scope.gridDokumenPelanggan = false, $scope.DetailDokumen = true;
            $scope.gridDetailDokumen.data = row.listFrameNo;
            $scope.rowselected = row;
        }

        $scope.back = function() {
            $scope.gridDokumenPelanggan = true, $scope.DetailDokumen = false;
        }

        $scope.close = function() {
            angular.element(".ui.modal.formDocumenUpload").modal("hide");
            angular.element('.ui.modal.formDocumenUploadsingle').modal('hide');

        }

        $scope.viewImage = function(dokumen) {
            console.log("dokumen", dokumen);
            // TerimaDokumenPelangganFactory.getImage().then(function(res) {
            //     console.log("test", res.data.Result);
            // })
        }

        $scope.checkdate = function(doc) {
            console.log("dokumen", doc);
            if (doc.ReceivedDateBit == false) {
                doc.ReceivedDate = new Date();
            } else {
                doc.ReceivedDate = null;
            }
        }

        $scope.checkifDate = function(doc) {
            if (doc.ReceivedDate == null) {
                doc.ReceivedDateBit = false;
            } else {
                doc.ReceivedDateBit = true;
            }
        }

        var actionbutton = '<a style="color:blue;" onclick=""; ng-click="">' +
            '<p style="padding:5px 0 0 5px">' +
            '<u tooltip-placement="bottom" uib-tooltip="Lihat Detil" ng-click="grid.appScope.$parent.lihatDetail(row.entity)">Lihat Detil</u>&nbsp' +
            '<u tooltip-placement="bottom" uib-tooltip="Update Dokumen" ng-click="grid.appScope.$parent.UpdateDokumen(row.entity)">Update Dokumen</u>&nbsp' +
            '</p>' +
            '</a>';

        var actionbuttondocument = '<a style="color:blue;" onclick=""; ng-click="">' +
            '<p style="padding:5px 0 0 5px">' +
            '<u tooltip-placement="bottom" uib-tooltip="Update Documen" ng-click="grid.appScope.listRangka(row.entity)">Update Documen</u>&nbsp' +
            '</p>' +
            '</a>';


        var checkdisterima = '<p align="center" style="margin: 5px 0 0 0">' +
            '<i ng-if="row.entity.TerimaBit == true" class="fa fa-check-square" style="font-size: 15px" aria-hidden="true"></i>' +
            '<i ng-if="row.entity.TerimaBit == false" class="fa fa-square-o" style="font-size: 15px" aria-hidden="true"></i>' +
            '</p>'

        var checklengkap = '<p align="center" style="margin: 5px 0 0 0">' +
            '<i ng-if="row.entity.LengkapBit == true" class="fa fa-check-square" style="font-size: 15px" aria-hidden="true"></i>' +
            '<i ng-if="row.entity.LengkapBit == false" class="fa fa-square-o" style="font-size: 15px" aria-hidden="true"></i>' +
            '</p>'

        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        $scope.test = function() {
            console.log("asdasdsa", $scope.rowsDocuments);
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------  
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'no distribusi', field: 'SuratDistribusiNo' },
                { name: 'cabang', field: 'OutletName' },
                { name: 'qty diterima/qty no rangka', field: 'QtyDokumenPerFrameNo' },
                { name: 'status', field: 'TerimaDokumenStatusName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbutton
                }
            ]
        };

        $scope.pageArray = [10,25,50];

        $scope.gridDetailDokumen = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes:  $scope.pageArray,
            // paginationPageSize: 15,
            columnDefs: [
                // { name: 'Id', field: 'Id', }
                { name: 'Id', field: 'Id', width: '15%', visible: false },
                { name: 'diterima', cellTemplate: checkdisterima },
                { name: 'lengkap', cellTemplate: checklengkap },
                { name: 'no rangka', field: 'FrameNo' },
                { name: 'nama pelanggan', field: 'CustomerName' },
                { name: 'model kendaraan', field: 'Description' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionbuttondocument
                }
            ]
        };


    });