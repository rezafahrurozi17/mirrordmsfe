angular.module('app')
  .factory('MaintainPDDFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
               

                var res = $http.get('/api/sales/PDDMaintain'+param);
              

                return res;
            },

            getKalkulasiPDD: function(param) {
              
                var res = $http.get('/api/sales/PDDKalkulasi/'+param);
              //  console.log("res Factory", res);


                return res;
            },
      getHistoryPDD: function(param) {
               
         var res = $http.get('/api/sales/PDDHistory/'+param); 
         return res;
      },

      getResonRevision: function() {
              
         var res = $http.get('/api/sales/PCategoryReasonRevision/');
         //  console.log("res Factory", res);
         return res;
      },
      createPDDCash: function(KalkulasiPDD) {
          
        return $http.post('/api/sales/PDDMaintain', KalkulasiPDD);
      },

      revisiPDDCash: function(RevisiKalkulasiPDD) {
        
		  
        return $http.put('/api/sales/PDDMaintain', RevisiKalkulasiPDD);
      },

      kalkulasiTLSDate: function(KalkulasiTLSDate) {
          
        return $http.post('/api/sales/TLSCalculateDeliveryRequest', KalkulasiTLSDate);
      },

      confirmPDDDateTLS: function(ConfirmPDDDate) {
          
        return $http.post('/api/sales/TLSConfirmDeliveryRequest', ConfirmPDDDate);
      },
      
      
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd