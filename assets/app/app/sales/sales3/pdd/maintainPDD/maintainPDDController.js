angular.module('app')
    .controller('MaintainPDDController', function ($scope, $rootScope, $http, $filter, CurrentUser, MaintainPDDFactory, $timeout, bsNotify, bsRights) {
        IfGotProblemWithModal();
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mMaintainPDD = null; //Model
        $scope.xRole = { selected: [] };
        $scope.disabledCekSimpanCashNorangka = false;
        $scope.disabledSimpanPDDCredit = false;

        $scope.dateOptions = {
            // formatYear: 'yyyy-mm-dd',
            // startingDay: 1,
            // minMode: 'year'
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
            minDate: new Date(),
        };

        $scope.dateOptions5 = {
            // formatYear: 'yyyy-mm-dd',
            // startingDay: 1,
            // minMode: 'year'
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
            minDate: new Date(),
        };

        $scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 30;
        $scope.mytime.setMinutes('00');

        $scope.mytime2 = new Date();
        $scope.hstep = 1;
        $scope.mstep = 30;
        $scope.mytime2.setMinutes('00');



        $scope.filterData = {
            defaultFilter: [
                { name: 'Nama Pelanggan', value: 'CustomerName' },
                { name: 'No SPK', value: 'FormSPKNo' },
                { name: 'No Rangka', value: 'FrameNo' }
            ],
            advancedFilter: [
                { name: 'Nama Pelanggan', value: 'CustomerName', typeMandatory: 0 },
                { name: 'No SPK', value: 'FormSPKNo', typeMandatory: 0 },
                { name: 'No Rangka', value: 'FrameNo', typeMandatory: 0 }
            ]
        };

        $scope.options = {
            hstep: [1, 2, 3],
            mstep: [1, 5, 10, 15, 25, 30]
        };

        $scope.flagPDD = 'Buat';
        $scope.checkSentWithSTNK = true;
        $scope.buatPDDFunction = function (selected_data) {
            $scope.flagPDD = 'Buat';
            $scope.daftarPDD = true;
            $scope.tambahPDD = true;
            angular.element('.ui.modal.MaintainPDDForm').modal('hide');
            $scope.disabledLihatPDD = true;
            if (selected_data.OnOffTheRoadId == 2) {
                $scope.disabledNoPolisi = true;
                $scope.disabledSTNK = true;
                $scope.checkSentWithSTNK = false;
                $scope.selected_data.SentWithSTNK = false;
            } else {
                $scope.disabledNoPolisi = false;
                $scope.disabledSTNK = false;
                $scope.checkSentWithSTNK = true;
                $scope.selected_data.SentWithSTNK = true;
            }
        }
        $scope.revisiPDDFunction = function (DataRevisi) {
            $scope.flagPDD = 'Revisi';
            // $scope.daftarPDD=true;
            // $scope.tambahPDD=true;
            $scope.PilihanRevisi = {};
            if(DataRevisi.StatusPDC=='Sudah Dikirim PDC'){
                setTimeout(function () {
                    angular.element('.ui.modal.ModalRevisionAlert').modal('refresh');
                }, 0);
                angular.element('.ui.modal.ModalRevisionAlert').modal('show');
            }else{
                angular.element('.ui.modal.MaintainPDDForm').modal('hide');
                angular.element('.ui.modal.ModalFormRevision').modal('show');
            }
            
            $scope.SelectedDataRevisi = DataRevisi;
        


        }
        $scope.konfirmasiPDDFunction = function (SelectedKonfirmasi) {

            $scope.daftarPDD = true;
            $scope.tambahPDD = true;
            if (SelectedKonfirmasi.OnOffTheRoadId == 2) {
                $scope.disabledNoPolisi = true;
                $scope.disabledSTNK = true;
            } else {
                $scope.disabledNoPolisi = false;
                $scope.disabledSTNK = false;
            }

            angular.element('.ui.modal.MaintainPDDForm').modal('hide');
            $scope.disabledLihatPDD = true;
        }

        $scope.kembaliListPDD = function () {

            $rootScope.$emit('RefreshDirective', {})
            $scope.daftarPDD = false;
            $scope.tambahPDD = false;
            $scope.DetilKalkulasiPDDCash = false;
            angular.element('#MaintainPDDForm').modal('show');
        }
        $scope.disabledNoPolisi = false;
        $scope.disabledSTNK = false;
        // $scope.getRights = function(){
        //     var rights = bsRights.get("app.maintainpdd");
        //     console.log("rights=>",rights);
        // }

        $scope.lihatPDDFunction = function () {
            $scope.daftarPDD = true;
            $scope.DetilKalkulasiPDDCash = true;
            $scope.dtdetilpdd = $scope.selected_data.SentUnitDate

            console.log('aaa', $scope.selected_data.SentUnitDate);
            //$scope.options.minDate = new Date($scope.selected_data.SentUnitDate);
            var rights = bsRights.get("app.maintainpdd");
            var allowNew = rights.new;

            
            angular.element('.ui.modal.MaintainPDDForm').modal('hide');

            $scope.EtaCustomerPlus = $scope.updated_data.SentUnitTime;

            if ($scope.selected_data.StatusPDDName != 'Butuh Konfirmasi') {
                $scope.HighlightTheCalendarAtLihat();
            }
            else if ($scope.selected_data.StatusPDDName == 'Butuh Konfirmasi') {
                $scope.HighlightTheCalendarAtLihatRRN();
            }

            $scope.EtaCustomerPlus.setTime($scope.updated_data.SentUnitTime.getTime() + (1 * 60 * 60 * 1000));

            return this;


        }

        $scope.HighlightTheCalendarAtLihatRRN = function () {
            $scope.events = [];
            $scope.Color_Da_CalendarAtLihat = [{ date: null, status: '' }];
            //$scope.Color_Da_CalendarAtLihat=[{date:null,status: 'fifthDate'}];
            $scope.Color_Da_CalendarAtLihat.splice(0, 1);

            $scope.a_awal = new Date($scope.selected_data.DocCompRecDate);
            $scope.b_akhir = new Date($scope.selected_data.SentUnitDate);

            // $scope.a_awal = new Date($scope.bind_data.data[0].SentUnitDateWithoutWDP);
            // $scope.b_akhir = new Date($scope.bind_data.data[0].SentUnitDate);

            var oneDayCalculation = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
            var diffDaysRRN = Math.round(Math.abs(($scope.b_akhir.getTime() - $scope.a_awal.getTime()) / (oneDayCalculation)));
            var diffDaysRRNForLoop = diffDaysRRN + 1;

            for (var i = 0; i < diffDaysRRNForLoop; i++) {
                var date_to_insert = new Date(angular.copy($scope.a_awal).setDate(angular.copy($scope.a_awal).getDate() + i));//ini tadinya i bukan 1 daripada a semua pake angular copy

                date_to_insert = date_to_insert.getFullYear() + '-' + ('0' + (date_to_insert.getMonth() + 1)) + '-' + ('0' + (date_to_insert.getDate())).slice(-2) + '';
                $scope.Color_Da_CalendarAtLihat.push({ date: date_to_insert, status: 'fifthDate' });
            }





            for (var ii = 0; ii < $scope.Color_Da_CalendarAtLihat.length; ii++) {
                $scope.events.push($scope.Color_Da_CalendarAtLihat[ii]);
                $scope.refreshCalendar = false;
                $timeout(function () { $scope.refreshCalendar = true }, 0);
            }



        }

        $scope.HighlightTheCalendarAtLihat = function () {
            $scope.events = [];
            $scope.Color_Da_CalendarAtLihat = [{ date: null, status: '' }];
            //$scope.Color_Da_CalendarAtLihat=[{date:null,status: 'secondDate'}];
            $scope.Color_Da_CalendarAtLihat.splice(0, 1);


            $scope.Color_Da_CalendarAtLihat.push({ date: $scope.selected_data.DocCompRecDate, status: 'dafirstDate' });

            if ($scope.selected_data.FundSourceName == "Cash" && $scope.selected_data.FrameNo != null) {

                $scope.Color_Da_CalendarAtLihat.push({ date: $scope.selected_data.DPDate, status: 'secondDate' });
                $scope.Color_Da_CalendarAtLihat.push({ date: $scope.selected_data.FullPaymentDate, status: 'thirdDate' });
            }
            else if ($scope.selected_data.FundSourceName == "Credit" && $scope.selected_data.FrameNo != null) {

                $scope.Color_Da_CalendarAtLihat.push({ date: $scope.selected_data.TDPDate, status: 'secondDate' });
            }


            $scope.Color_Da_CalendarAtLihat.push({ date: $scope.selected_data.STNKDoneDate, status: 'fourthDate' });
            $scope.Color_Da_CalendarAtLihat.push({ date: $scope.selected_data.SentUnitDate, status: 'fifthDate' });
            $scope.Color_Da_CalendarAtLihat.push({ date: $scope.selected_data.ApprovalLeasingDate, status: 'sixDate' });

            for (var ii = 0; ii < $scope.Color_Da_CalendarAtLihat.length; ii++) {
                $scope.events.push($scope.Color_Da_CalendarAtLihat[ii]);
                $scope.refreshCalendar = false;
                $timeout(function () { $scope.refreshCalendar = true }, 0);
            }



        }

        $scope.kalkulasiPDDNorangka = function () {

            var tglDOc = $scope.testDate.date.getFullYear() + '-'
                + ('0' + ($scope.testDate.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDate.date.getDate()).slice(-2) + 'T'
                + (($scope.testDate.date.getHours() < '10' ? '0' : '') + $scope.testDate.date.getHours()) + ':'
                + (($scope.testDate.date.getMinutes() < '10' ? '0' : '') + $scope.testDate.date.getMinutes()) + ':'
                + (($scope.testDate.date.getSeconds() < '10' ? '0' : '') + $scope.testDate.date.getSeconds());

            tglDOc.slice(0, 10);

            var tglDP = $scope.testDate2.date.getFullYear() + '-'
                + ('0' + ($scope.testDate2.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDate2.date.getDate()).slice(-2) + 'T'
                + (($scope.testDate2.date.getHours() < '10' ? '0' : '') + $scope.testDate2.date.getHours()) + ':'
                + (($scope.testDate2.date.getMinutes() < '10' ? '0' : '') + $scope.testDate2.date.getMinutes()) + ':'
                + (($scope.testDate2.date.getSeconds() < '10' ? '0' : '') + $scope.testDate2.date.getSeconds());


            tglDP.slice(0, 10);

            var tglFUllPayment = $scope.testDate3.date.getFullYear() + '-'
                + ('0' + ($scope.testDate3.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDate3.date.getDate()).slice(-2) + 'T'
                + (($scope.testDate3.date.getHours() < '10' ? '0' : '') + $scope.testDate3.date.getHours()) + ':'
                + (($scope.testDate3.date.getMinutes() < '10' ? '0' : '') + $scope.testDate3.date.getMinutes()) + ':'
                + (($scope.testDate3.date.getSeconds() < '10' ? '0' : '') + $scope.testDate3.date.getSeconds());

            tglFUllPayment.slice(0, 10);


            var PLODDate = $scope.selected_data.PLOD.getFullYear() + '-'
                + ('0' + ($scope.selected_data.PLOD.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.selected_data.PLOD.getDate()).slice(-2) + 'T'
                + (($scope.selected_data.PLOD.getHours() < '10' ? '0' : '') + $scope.selected_data.PLOD.getHours()) + ':'
                + (($scope.selected_data.PLOD.getMinutes() < '10' ? '0' : '') + $scope.selected_data.PLOD.getMinutes()) + ':'
                + (($scope.selected_data.PLOD.getSeconds() < '10' ? '0' : '') + $scope.selected_data.PLOD.getSeconds());

            PLODDate.slice(0, 10);



            var RevisiBit = false;

            if ($scope.flagPDD == 'Revisi') {
                RevisiBit = true;
                $scope.disabledPengirimanUnit = true;
            } else {
                RevisiBit = false;
                $scope.disabledPengirimanUnit = false;
            }

            var choosePolice = false;
            var directPDC = false;
            var sentSTNK = false;
            if ($scope.selected_data.ChoosePoliceNo == null || $scope.selected_data.ChoosePoliceNo == false) {
                choosePolice = false;
            } else {
                choosePolice = true;
            }

            if ($scope.selected_data.DirectDeliveryPDC == null || $scope.selected_data.DirectDeliveryPDC == false) {
                directPDC = false;
            } else {
                directPDC = true;
            }

            if ($scope.selected_data.SentWithSTNK == null || $scope.selected_data.SentWithSTNK == false) {
                sentSTNK = false;
            } else {
                sentSTNK = true;
            }


            var getlKalkulasi = "?SPKId=" + $scope.selected_data.SpkId + "&SoId=" + $scope.selected_data.SoId + "&RRNNo=" + $scope.selected_data.RRNNo + "&DocCompRecDate=" + tglDOc + "&DPDate=" + tglDP + "&FullPaymentDate=" + tglFUllPayment + "&TLSSendDate=" + $scope.TLSDate.earliestPDD + "&DirectDeliveryFromPDC=" + directPDC + "&ChoosePoliceNo=" + choosePolice + "&SentWithSTNK=" + sentSTNK + "&PLOD=" + PLODDate + "&RevisiBit=" + RevisiBit + "&StatusFormABit=" + $scope.TLSDate.statusFormA;
            MaintainPDDFactory.getKalkulasiPDD(getlKalkulasi).then(
                function (res) {

                    $scope.bind_data.data = res.data.Result;
                    $scope.DateStart();

                    // $scope.firstDateChange();
                    // $scope.secondDateChange();
                    // $scope.thirdDateChange();
                    var date01 = $scope.bind_data.data[0]['DocCompRecDate'];
                    var date02 = $scope.bind_data.data[0]['DPDate'];
                    var date03 = $scope.bind_data.data[0]['FullPaymentDate'];

                    $scope.testDate1 = {
                        date: date01,
                        status: 'firstDate'
                    };

                    $scope.events.push($scope.testDate1);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);

                    $scope.testDate2 = {
                        date: date02,
                        status: 'secondDate'
                    };

                    $scope.events.push($scope.testDate2);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);

                    $scope.testDate3 = {
                        date: date03,
                        status: 'thirdDate'
                    };

                    $scope.events.push($scope.testDate3);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);
                    

                    $scope.fourthDateChange();
                    //$scope.fifthDateChange();
                    var date05 = $scope.bind_data.data[0]['SentUnitDate'];
                    var pengirimanSTNK = $scope.bind_data.data[0]['SentWithSTNK'];

                    if (pengirimanSTNK == true) {
                        $scope.dateOptions5 = {
                            // formatYear: 'yyyy-mm-dd',
                            // startingDay: 1,
                            // minMode: 'year'
                            startingDay: 1,
                            format: "dd/MM/yyyy",
                            disableWeekend: 1,
                            minDate: new Date(date05),
                        };

                        $scope.testDate5 = {
                            date: date05,
                            status: 'fifthDate'
                        };

                        $scope.events.push($scope.testDate5);
                        $scope.KalkulasiPDDCash = true;
                        $scope.MainPDD = false;

                    } else {
                        $scope.dateOptions5 = {
                            // formatYear: 'yyyy-mm-dd',
                            // startingDay: 1,
                            // minMode: 'year'
                            startingDay: 1,
                            format: "dd/MM/yyyy",
                            disableWeekend: 1,
                            minDate: new Date(),
                        };
                        $scope.testDate5 = {
                            date: date05,
                            status: 'fifthDate'
                        };

                        $scope.events.push($scope.testDate5);
                        $scope.KalkulasiPDDCash = true;
                        $scope.MainPDD = false;
                    }
                    // $scope.testDate5 = {
                    //     date: date05,
                    //     status: 'fifthDate'
                    // };

                    // $scope.events.push($scope.testDate5);
                    // $scope.KalkulasiPDDCash = true;
                    // $scope.MainPDD = false;
                },
                function (err) {

                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: err.data.Message,
                            type: 'warning'
                        }
                    );
                }
            )

        };

        $scope.simpanPDDCash = function () {
            var timeEta = $scope.mytime;
            var dateEta = $scope.testDate5.date;

            if ($scope.testDate5.date < $scope.bind_data.data[0]['SentUnitDate']) {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Pengiriman Unit paling cepat bisa dilakukan pada tanggal : " + fixDate($scope.bind_data.data[0]['SentUnitDate']),
                        type: 'warning'
                    }
                );
                $scope.disabledCekSimpanCashNorangka = false;
            }
            else {

                $scope.KalkulasiTLSDate.RequestedPDD = null;
                $scope.KalkulasiTLSDate.RequestedPDD = angular.copy($scope.testDate5.date);

                var HariTLS = $scope.bind_data.data[0].LeadTime;

                $scope.KalkulasiTLSDate.RequestedPDD = new Date($scope.KalkulasiTLSDate.RequestedPDD.setDate($scope.KalkulasiTLSDate.RequestedPDD.getDate() - HariTLS));

                try {
                    $scope.KalkulasiTLSDate.RequestedPDD = $scope.KalkulasiTLSDate.RequestedPDD.getFullYear() + '-'
                        + ('0' + ($scope.KalkulasiTLSDate.RequestedPDD.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.KalkulasiTLSDate.RequestedPDD.getDate()).slice(-2) + 'T'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getHours() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getHours()) + ':'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getMinutes() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getMinutes()) + ':'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getSeconds() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getSeconds());
                }
                catch (e1) {
                    $scope.KalkulasiTLSDate.RequestedPDD = null;
                }

                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].EarliestPDD = $scope.TLSDate.earliestPDD };

                $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-'
                    + ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-'
                    + ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T'
                    + (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':'
                    + (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':'
                    + (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());

                for (var i in $scope.bind_data.data) {
                    $scope.bind_data.data[i].SentUnitDate = dateEta;
                    try {
                        $scope.bind_data.data[i].SentUnitDate = $scope.bind_data.data[i].SentUnitDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].SentUnitDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].SentUnitDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].SentUnitDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].SentUnitDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].SentUnitDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getSeconds());
                    }
                    catch (e1) {
                        $scope.bind_data.data[i].SentUnitDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':'
                            + (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());
                    }
                    catch (e1) {
                        $scope.bind_data.data[i].SentUnitTime = null;
                    }

                    try {
                        $scope.bind_data.data[i].DocCompRecDate = $scope.bind_data.data[i].DocCompRecDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].DocCompRecDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].DocCompRecDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].DocCompRecDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].DocCompRecDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].DocCompRecDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getSeconds());
                    }
                    catch (e1) {
                        $scope.bind_data.data[i].DocCompRecDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].DPDate = $scope.bind_data.data[i].DPDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].DPDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].DPDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].DPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].DPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].DPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getSeconds());
                    }
                    catch (e2) {
                        $scope.bind_data.data[i].DPDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].TDPDate = $scope.bind_data.data[i].TDPDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].TDPDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].TDPDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].TDPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].TDPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].TDPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getSeconds());
                    }
                    catch (e3) {
                        $scope.bind_data.data[i].TDPDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].FullPaymentDate = $scope.bind_data.data[i].FullPaymentDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].FullPaymentDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].FullPaymentDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].FullPaymentDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].FullPaymentDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].FullPaymentDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getSeconds());
                    }
                    catch (e4) {
                        $scope.bind_data.data[i].FullPaymentDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].STNKDoneDate = $scope.bind_data.data[i].STNKDoneDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].STNKDoneDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].STNKDoneDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].STNKDoneDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].STNKDoneDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].STNKDoneDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getSeconds());
                    }
                    catch (e4) {
                        $scope.bind_data.data[i].STNKDoneDate = null;
                    }
                };
                if ($scope.flagPDD == 'Buat') {
                    var timeEta = $scope.mytime;
                    for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                    for (var i in $scope.bind_data.data) {
                        $scope.bind_data.data[i].ReasonRevisionId = IdRevisi;
                        $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':'
                            + (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());

                    };


                    MaintainPDDFactory.confirmPDDDateTLS($scope.KalkulasiTLSDate).then(
                        function (res) {
                            $scope.confirmPDD = res.data;

                            MaintainPDDFactory.createPDDCash($scope.bind_data.data).then(
                                function (res) {

                                    MaintainPDDFactory.getData('?start=1&limit=5').then(
                                        function (res) {
                                            $scope.bind_data.data = res.data.Result;
                                            $scope.KalkulasiPDDCash = false;
                                            $scope.daftarPDD = false;
                                            $rootScope.$emit('RefreshDirective', {})
                                        }

                                    );

                                    bsNotify.show(
                                        {
                                            title: "Sukses",
                                            content: "PDD Berhasil disimpan.",
                                            type: 'success'
                                        }
                                    );
                                    $scope.disabledCekSimpanCashNorangka = false;
                                },
                                function (err) {

                                    bsNotify.show(
                                        {
                                            title: "Gagal",
                                            content: "Simpan PDD gagal.",
                                            type: 'danger'
                                        }
                                    );
                                    $scope.disabledCekSimpanCashNorangka = false;
                                }
                            )
                        },
                        function (err) {

                            bsNotify.show(
                                {
                                    title: "Gagal",
                                    content: err.data.Message,
                                    type: 'danger'
                                }
                            );
                            $scope.disabledCekSimpanCashNorangka = false;
                        }

                    )


                }
                else
                    if ($scope.flagPDD == 'Revisi') {

                        var IdRevisi = $scope.PilihanRevisi.ReasonRevisionId;
                        var timeEta = $scope.mytime;
                        for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                        for (var i in $scope.bind_data.data) {
                            $scope.bind_data.data[i].ReasonRevisionId = IdRevisi;
                            $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-'
                                + ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-'
                                + ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T'
                                + (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':'
                                + (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':'
                                + (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());

                        };

                        MaintainPDDFactory.confirmPDDDateTLS($scope.KalkulasiTLSDate).then(
                            function (res) {
                                $scope.confirmPDD = res.data;

                                MaintainPDDFactory.revisiPDDCash($scope.bind_data.data).then(
                                    function (res) {


                                        MaintainPDDFactory.getData('?start=1&limit=5').then(
                                            function (res) {
                                                $scope.bind_data.data = res.data.Result;
                                                $scope.KalkulasiPDDCash = false;
                                                $scope.daftarPDD = false;
                                                $rootScope.$emit('RefreshDirective', {})
                                            }

                                        );
                                        //$rootScope.$emit('RefreshDirective', {})
                                        bsNotify.show(
                                            {
                                                title: "Sukses",
                                                content: "PDD Berhasil disimpan.",
                                                type: 'success'
                                            }
                                        );
                                        $scope.disabledCekSimpanCashNorangka = false;
                                    },
                                    function (err) {

                                        bsNotify.show(
                                            {
                                                title: "Gagal",
                                                content: "Revisi PDD gagal.",
                                                type: 'danger'
                                            }
                                        );
                                        $scope.disabledCekSimpanCashNorangka = false;

                                    }
                                )
                            },
                            function (err) {

                                bsNotify.show(
                                    {
                                        title: "Gagal",
                                        content: err.data.Message,
                                        type: 'danger'
                                    }
                                )
                                $scope.disabledCekSimpanCashNorangka = false;
                            }



                        )

                    }

                // $scope.KalkulasiPDDCash = false;
                // $scope.daftarPDD = false;

            }
        };

        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = ('0' + date.getDate()).slice(-2) + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    date.getFullYear()
                return fix;
            } else {
                return null;
            }
        };

        $scope.simpanPDDCredit = function () {
            $scope.disabledSimpanPDDCredit = true;
            var timeEta = $scope.mytime2;
            var dateEta = $scope.testDate5.date;
            console.log('timeEta', timeEta);
            if ($scope.testDate5.date < $scope.bind_data.data[0]['SentUnitDate']) {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Pengiriman Unit paling cepat bisa dilakukan pada tanggal : " + fixDate($scope.bind_data.data[0]['SentUnitDate']),
                        type: 'warning'
                    }
                );
                $scope.disabledSimpanPDDCredit = false;
            }
            else {
                $scope.KalkulasiTLSDate.RequestedPDD = null;
                $scope.KalkulasiTLSDate.RequestedPDD = angular.copy($scope.testDate5.date);
                var HariTLS = $scope.bind_data.data[0].LeadTime;
                $scope.KalkulasiTLSDate.RequestedPDD = new Date($scope.KalkulasiTLSDate.RequestedPDD.setDate($scope.KalkulasiTLSDate.RequestedPDD.getDate() - HariTLS));

                try {
                    $scope.KalkulasiTLSDate.RequestedPDD = $scope.KalkulasiTLSDate.RequestedPDD.getFullYear() + '-'
                        + ('0' + ($scope.KalkulasiTLSDate.RequestedPDD.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.KalkulasiTLSDate.RequestedPDD.getDate()).slice(-2) + 'T'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getHours() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getHours()) + ':'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getMinutes() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getMinutes()) + ':'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getSeconds() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getSeconds());
                }
                catch (e1) {
                    $scope.KalkulasiTLSDate.RequestedPDD = null;
                }

                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].EarliestPDD = $scope.TLSDate.earliestPDD };
                // $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-'
                //     + ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-'
                //     + ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T'
                //     + (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':'
                //     + (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':'
                //     + (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());


                for (var i in $scope.bind_data.data) {
                    $scope.bind_data.data[i].SentUnitDate = dateEta;
                    try {
                        $scope.bind_data.data[i].SentUnitDate = $scope.bind_data.data[i].SentUnitDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].SentUnitDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].SentUnitDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].SentUnitDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].SentUnitDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].SentUnitDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitDate.getSeconds());
                    }
                    catch (e1) {
                        $scope.bind_data.data[i].SentUnitDate = null;
                    }
                    try {
                        $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':'
                            + (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());
                    }
                    catch (e1) {
                        $scope.bind_data.data[i].SentUnitTime = null;
                    }

                    try {
                        $scope.bind_data.data[i].DocCompRecDate = $scope.bind_data.data[i].DocCompRecDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].DocCompRecDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].DocCompRecDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].DocCompRecDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].DocCompRecDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].DocCompRecDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getSeconds());
                    }
                    catch (e1) {
                        $scope.bind_data.data[i].DocCompRecDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].DPDate = $scope.bind_data.data[i].DPDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].DPDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].DPDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].DPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].DPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].DPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getSeconds());
                    }
                    catch (e2) {
                        $scope.bind_data.data[i].DPDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].TDPDate = $scope.bind_data.data[i].TDPDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].TDPDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].TDPDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].TDPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].TDPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].TDPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getSeconds());
                    }
                    catch (e3) {
                        $scope.bind_data.data[i].TDPDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].FullPaymentDate = $scope.bind_data.data[i].FullPaymentDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].FullPaymentDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].FullPaymentDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].FullPaymentDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].FullPaymentDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].FullPaymentDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getSeconds());
                    }
                    catch (e4) {
                        $scope.bind_data.data[i].FullPaymentDate = null;
                    }

                    try {
                        $scope.bind_data.data[i].STNKDoneDate = $scope.bind_data.data[i].STNKDoneDate.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].STNKDoneDate.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].STNKDoneDate.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].STNKDoneDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getHours()) + ':'
                            + (($scope.bind_data.data[i].STNKDoneDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].STNKDoneDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].STNKDoneDate.getSeconds());
                    }
                    catch (e4) {
                        $scope.bind_data.data[i].STNKDoneDate = null;
                    }
                };
                if ($scope.flagPDD == 'Buat') {
                    var timeEta = $scope.mytime2;
                    for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                    for (var i in $scope.bind_data.data) {
                        $scope.bind_data.data[i].ReasonRevisionId = IdRevisi;
                        $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-'
                            + ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T'
                            + (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':'
                            + (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':'
                            + (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());


                    };

                    MaintainPDDFactory.confirmPDDDateTLS($scope.KalkulasiTLSDate).then(
                        function (res) {
                            $scope.confirmPDD = res.data;

                            MaintainPDDFactory.createPDDCash($scope.bind_data.data).then(
                                function (res) {


                                    MaintainPDDFactory.getData('?start=1&limit=5').then(
                                        function (res) {
                                            $scope.bind_data.data = res.data.Result;
                                            $scope.KalkulasiPDDKredit = false;
                                            $scope.daftarPDD = false;
                                            $rootScope.$emit('RefreshDirective', {})
                                        }

                                    );
                                    //$rootScope.$emit('RefreshDirective', {})
                                    bsNotify.show(
                                        {
                                            title: "Sukses",
                                            content: "PDD Berhasil disimpan.",
                                            type: 'success'
                                        }
                                    );
                                    $scope.disabledSimpanPDDCredit = false;
                                },
                                function (err) {

                                    bsNotify.show(
                                        {
                                            title: "Gagal",
                                            content: "Simpan PDD gagal.",
                                            type: 'danger'
                                        }
                                    );
                                    $scope.disabledSimpanPDDCredit = false;
                                }
                            )
                        },
                        function (err) {

                            bsNotify.show(
                                {
                                    title: "Gagal",
                                    content: err.data.Message,
                                    type: 'danger'
                                }
                            );
                            $scope.disabledSimpanPDDCredit = false;
                        }

                    )


                }
                else
                    if ($scope.flagPDD == 'Revisi') {

                        var IdRevisi = $scope.PilihanRevisi.ReasonRevisionId;

                        var timeEta = $scope.mytime2;
                        console.log('timeEta', timeEta);
                        for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].SentUnitTime = timeEta };
                        for (var i in $scope.bind_data.data) {
                            $scope.bind_data.data[i].ReasonRevisionId = IdRevisi;
                            $scope.bind_data.data[i].SentUnitTime = $scope.bind_data.data[i].SentUnitTime.getFullYear() + '-'
                                + ('0' + ($scope.bind_data.data[i].SentUnitTime.getMonth() + 1)).slice(-2) + '-'
                                + ('0' + $scope.bind_data.data[i].SentUnitTime.getDate()).slice(-2) + 'T'
                                + (($scope.bind_data.data[i].SentUnitTime.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getHours()) + ':'
                                + (($scope.bind_data.data[i].SentUnitTime.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getMinutes()) + ':'
                                + (($scope.bind_data.data[i].SentUnitTime.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].SentUnitTime.getSeconds());


                        };


                        MaintainPDDFactory.confirmPDDDateTLS($scope.KalkulasiTLSDate).then(
                            function (res) {
                                $scope.confirmPDD = res.data;

                                MaintainPDDFactory.revisiPDDCash($scope.bind_data.data).then(
                                    function (res) {


                                        MaintainPDDFactory.getData('?start=1&limit=5').then(
                                            function (res) {
                                                $scope.bind_data.data = res.data.Result;
                                                $scope.KalkulasiPDDKredit = false;
                                                $scope.daftarPDD = false;
                                                $rootScope.$emit('RefreshDirective', {})
                                            }

                                        )
                                        //$rootScope.$emit('RefreshDirective', {})
                                        bsNotify.show(
                                            {
                                                title: "Sukses",
                                                content: "PDD Berhasil disimpan.",
                                                type: 'success'
                                            }
                                        );
                                        $scope.disabledSimpanPDDCredit = false;
                                    },
                                    function (err) {

                                        bsNotify.show(
                                            {
                                                title: "Gagal",
                                                content: "Revisi PDD gagal.",
                                                type: 'danger'
                                            }
                                        );
                                        $scope.disabledSimpanPDDCredit = false;
                                    }
                                )
                            },
                            function (err) {

                                bsNotify.show(
                                    {
                                        title: "Gagal",
                                        content: err.data.Message,
                                        type: 'danger'
                                    }
                                );
                                $scope.disabledSimpanPDDCredit = false;
                            }

                        )

                    }

                // $scope.KalkulasiPDDKredit = false;
                // $scope.daftarPDD = false;

            }
        };

        $scope.simpanRRNCash = function () {
            if ($scope.flagPDD == 'Buat') {

                $scope.KalkulasiTLSDate.RequestedPDD = null;
                //$scope.KalkulasiTLSDate.RequestedPDD = $scope.bind_data.data[0].SentUnitDate;
                $scope.KalkulasiTLSDate.RequestedPDD = angular.copy($scope.bind_data.data[0].SentUnitDate);
                var HariTLS = $scope.bind_data.data[0].LeadTime;
                $scope.KalkulasiTLSDate.RequestedPDD = new Date($scope.KalkulasiTLSDate.RequestedPDD.setDate($scope.KalkulasiTLSDate.RequestedPDD.getDate() - HariTLS));

                try {
                    $scope.KalkulasiTLSDate.RequestedPDD = $scope.KalkulasiTLSDate.RequestedPDD.getFullYear() + '-'
                        + ('0' + ($scope.KalkulasiTLSDate.RequestedPDD.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.KalkulasiTLSDate.RequestedPDD.getDate()).slice(-2) + 'T'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getHours() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getHours()) + ':'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getMinutes() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getMinutes()) + ':'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getSeconds() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getSeconds());
                }
                catch (e1) {
                    $scope.KalkulasiTLSDate.RequestedPDD = null;
                }

                for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].EarliestPDD = $scope.TLSDate.earliestPDD };
                try {
                    $scope.bind_data.data.DocCompRecDate = $scope.bind_data.data.DocCompRecDate.getFullYear() + '-'
                        + ('0' + ($scope.bind_data.data.DocCompRecDate.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.bind_data.data.DocCompRecDate.getDate()).slice(-2) + 'T'
                        + (($scope.bind_data.data.DocCompRecDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data.DocCompRecDate.getHours()) + ':'
                        + (($scope.bind_data.data.DocCompRecDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data.DocCompRecDate.getMinutes()) + ':'
                        + (($scope.bind_data.data.DocCompRecDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data.DocCompRecDate.getSeconds());
                }
                catch (e1) {
                    $scope.bind_data.data.DocCompRecDate = null;
                }

                try {
                    $scope.bind_data.data.DPDate = $scope.bind_data.data.DPDate.getFullYear() + '-'
                        + ('0' + ($scope.bind_data.data.DPDate.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.bind_data.data.DPDate.getDate()).slice(-2) + 'T'
                        + (($scope.bind_data.data.DPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data.DPDate.getHours()) + ':'
                        + (($scope.bind_data.data.DPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data.DPDate.getMinutes()) + ':'
                        + (($scope.bind_data.data.DPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data.DPDate.getSeconds());
                }
                catch (e2) {
                    $scope.bind_data.data.DPDate = null;
                }

                try {
                    $scope.bind_data.data.TDPDate = $scope.bind_data.data.TDPDate.getFullYear() + '-'
                        + ('0' + ($scope.bind_data.data.TDPDate.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.bind_data.data.TDPDate.getDate()).slice(-2) + 'T'
                        + (($scope.bind_data.data.TDPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data.TDPDate.getHours()) + ':'
                        + (($scope.bind_data.data.TDPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data.TDPDate.getMinutes()) + ':'
                        + (($scope.bind_data.data.TDPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data.TDPDate.getSeconds());
                }
                catch (e3) {
                    $scope.bind_data.data.TDPDate = null;
                }

                try {
                    $scope.bind_data.data.FullPaymentDate = $scope.bind_data.data.FullPaymentDate.getFullYear() + '-'
                        + ('0' + ($scope.bind_data.data.FullPaymentDate.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.bind_data.data.FullPaymentDate.getDate()).slice(-2) + 'T'
                        + (($scope.bind_data.data.FullPaymentDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data.FullPaymentDate.getHours()) + ':'
                        + (($scope.bind_data.data.FullPaymentDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data.FullPaymentDate.getMinutes()) + ':'
                        + (($scope.bind_data.data.FullPaymentDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data.FullPaymentDate.getSeconds());
                }
                catch (e4) {
                    $scope.bind_data.data.FullPaymentDate = null;
                }

                MaintainPDDFactory.confirmPDDDateTLS($scope.KalkulasiTLSDate).then(
                    function (res) {

                        MaintainPDDFactory.createPDDCash($scope.bind_data.data).then(
                            function (res) {
                                MaintainPDDFactory.getData('?start=1&limit=5').then(
                                    function (res) {
                                        $scope.bind_data.data = res.data.Result;
                                        $scope.KalkulasiRRNCash = false;
                                        $scope.daftarPDD = false;
                                    }

                                )
                                bsNotify.show(
                                    {
                                        title: "Sukses",
                                        content: "PDD Berhasil disimpan.",
                                        type: 'success'
                                    }
                                );

                            },
                            function (err) {

                                bsNotify.show(
                                    {
                                        title: "Gagal",
                                        content: "Input PDD gagal.",
                                        type: 'danger'
                                    }
                                );
                            }
                        )
                    },
                    function (err) {

                        bsNotify.show(
                            {
                                title: "Gagal",
                                content: err.data.Message,
                                type: 'danger'
                            }
                        );
                    }
                )
            }
            else
                if ($scope.flagPDD == 'Revisi') {

                    $scope.KalkulasiTLSDate.RequestedPDD = null;
                    //$scope.KalkulasiTLSDate.RequestedPDD = $scope.bind_data.data[0].SentUnitDate;
                    $scope.KalkulasiTLSDate.RequestedPDD = angular.copy($scope.bind_data.data[0].SentUnitDate);
                    var HariTLS = $scope.bind_data.data[0].LeadTime;
                    $scope.KalkulasiTLSDate.RequestedPDD = new Date($scope.KalkulasiTLSDate.RequestedPDD.setDate($scope.KalkulasiTLSDate.RequestedPDD.getDate() - HariTLS));

                    try {
                        $scope.KalkulasiTLSDate.RequestedPDD = $scope.KalkulasiTLSDate.RequestedPDD.getFullYear() + '-'
                            + ('0' + ($scope.KalkulasiTLSDate.RequestedPDD.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.KalkulasiTLSDate.RequestedPDD.getDate()).slice(-2) + 'T'
                            + (($scope.KalkulasiTLSDate.RequestedPDD.getHours() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getHours()) + ':'
                            + (($scope.KalkulasiTLSDate.RequestedPDD.getMinutes() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getMinutes()) + ':'
                            + (($scope.KalkulasiTLSDate.RequestedPDD.getSeconds() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getSeconds());
                    }
                    catch (e1) {
                        $scope.KalkulasiTLSDate.RequestedPDD = null;
                    }

                    var IdRevisi = $scope.PilihanRevisi.ReasonRevisionId;
                    for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].EarliestPDD = $scope.TLSDate.earliestPDD };
                    for (var i in $scope.bind_data.data) {
                        $scope.bind_data.data[i].ReasonRevisionId = IdRevisi;
                        // try {
                        //     $scope.bind_data.data[i].DocCompRecDate = $scope.bind_data.data[i].DocCompRecDate.getFullYear() + '-'
                        //         + ('0' + ($scope.bind_data.data[i].DocCompRecDate.getMonth() + 1)).slice(-2) + '-'
                        //         + ('0' + $scope.bind_data.data[i].DocCompRecDate.getDate()).slice(-2) + 'T'
                        //         + (($scope.bind_data.data[i].DocCompRecDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getHours()) + ':'
                        //         + (($scope.bind_data.data[i].DocCompRecDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getMinutes()) + ':'
                        //         + (($scope.bind_data.data[i].DocCompRecDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DocCompRecDate.getSeconds());
                        // }
                        // catch (e1) {
                        //     $scope.bind_data.data[i].DocCompRecDate = null;
                        // }

                        // try {
                        //     $scope.bind_data.data[i].DPDate = $scope.bind_data.data[i].DPDate.getFullYear() + '-'
                        //         + ('0' + ($scope.bind_data.data[i].DPDate.getMonth() + 1)).slice(-2) + '-'
                        //         + ('0' + $scope.bind_data.data[i].DPDate.getDate()).slice(-2) + 'T'
                        //         + (($scope.bind_data.data[i].DPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getHours()) + ':'
                        //         + (($scope.bind_data.data[i].DPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getMinutes()) + ':'
                        //         + (($scope.bind_data.data[i].DPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].DPDate.getSeconds());
                        // }
                        // catch (e2) {
                        //     $scope.bind_data.data[i].DPDate = null;
                        // }

                        // try {
                        //     $scope.bind_data.data[i].TDPDate = $scope.bind_data.data[i].TDPDate.getFullYear() + '-'
                        //         + ('0' + ($scope.bind_data.data[i].TDPDate.getMonth() + 1)).slice(-2) + '-'
                        //         + ('0' + $scope.bind_data.data[i].TDPDate.getDate()).slice(-2) + 'T'
                        //         + (($scope.bind_data.data[i].TDPDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getHours()) + ':'
                        //         + (($scope.bind_data.data[i].TDPDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getMinutes()) + ':'
                        //         + (($scope.bind_data.data[i].TDPDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].TDPDate.getSeconds());
                        // }
                        // catch (e3) {
                        //     $scope.bind_data.data[i].TDPDate = null;
                        // }

                        // try {
                        //     $scope.bind_data.data[i].FullPaymentDate = $scope.bind_data.data[i].FullPaymentDate.getFullYear() + '-'
                        //         + ('0' + ($scope.bind_data.data[i].FullPaymentDate.getMonth() + 1)).slice(-2) + '-'
                        //         + ('0' + $scope.bind_data.data[i].FullPaymentDate.getDate()).slice(-2) + 'T'
                        //         + (($scope.bind_data.data[i].FullPaymentDate.getHours() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getHours()) + ':'
                        //         + (($scope.bind_data.data[i].FullPaymentDate.getMinutes() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getMinutes()) + ':'
                        //         + (($scope.bind_data.data[i].FullPaymentDate.getSeconds() < '10' ? '0' : '') + $scope.bind_data.data[i].FullPaymentDate.getSeconds());
                        // }
                        // catch (e4) {
                        //     $scope.bind_data.data[i].FullPaymentDate = null;
                        // }
                    };

                    MaintainPDDFactory.confirmPDDDateTLS($scope.KalkulasiTLSDate).then(
                        function (res) {

                            MaintainPDDFactory.revisiPDDCash($scope.bind_data.data).then(
                                function (res) {
                                    MaintainPDDFactory.getData('?start=1&limit=5').then(
                                        function (res) {
                                            $scope.bind_data.data = res.data.Result;


                                            $scope.KalkulasiRRNCash = false;
                                            $scope.daftarPDD = false;
                                        }

                                    )
                                    bsNotify.show(
                                        {
                                            title: "Sukses",
                                            content: "PDD Berhasil disimpan.",
                                            type: 'success'
                                        }
                                    );

                                },
                                function (err) {

                                    bsNotify.show(
                                        {
                                            title: "Gagal",
                                            content: "Input PDD gagal.",
                                            type: 'danger'
                                        }
                                    );
                                }
                            )

                        },
                        function (err) {

                            bsNotify.show(
                                {
                                    title: "Gagal",
                                    content: err.data.Message,
                                    type: 'danger'
                                }
                            );
                        }
                    )
                }

        };

        $scope.simpanRRNCredit = function () {
            if ($scope.flagPDD == 'Buat') {

                $scope.KalkulasiTLSDate.RequestedPDD = null;
                //$scope.KalkulasiTLSDate.RequestedPDD = $scope.bind_data.data[0].SentUnitDate;
                $scope.KalkulasiTLSDate.RequestedPDD = angular.copy($scope.bind_data.data[0].SentUnitDate);
                var HariTLS = $scope.bind_data.data[0].LeadTime;
                $scope.KalkulasiTLSDate.RequestedPDD = new Date($scope.KalkulasiTLSDate.RequestedPDD.setDate($scope.KalkulasiTLSDate.RequestedPDD.getDate() - HariTLS));

                try {
                    $scope.KalkulasiTLSDate.RequestedPDD = $scope.KalkulasiTLSDate.RequestedPDD.getFullYear() + '-'
                        + ('0' + ($scope.KalkulasiTLSDate.RequestedPDD.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.KalkulasiTLSDate.RequestedPDD.getDate()).slice(-2) + 'T'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getHours() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getHours()) + ':'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getMinutes() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getMinutes()) + ':'
                        + (($scope.KalkulasiTLSDate.RequestedPDD.getSeconds() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getSeconds());
                }
                catch (e1) {
                    $scope.KalkulasiTLSDate.RequestedPDD = null;
                }

                MaintainPDDFactory.confirmPDDDateTLS($scope.KalkulasiTLSDate).then(
                    function (res) {
                        MaintainPDDFactory.createPDDCash($scope.bind_data.data).then(
                            function (res) {
                                MaintainPDDFactory.getData('?start=1&limit=5').then(
                                    function (res) {
                                        $scope.bind_data.data = res.data.Result;
                                        $scope.KalkulasiRRNKredit = false;
                                        $scope.daftarPDD = false;


                                    }

                                )
                                bsNotify.show(
                                    {
                                        title: "Sukses",
                                        content: "PDD Berhasil disimpan.",
                                        type: 'success'
                                    }
                                );

                            },
                            function (err) {

                                bsNotify.show(
                                    {
                                        title: "Gagal",
                                        content: "Input PDD gagal.",
                                        type: 'danger'
                                    }
                                );
                            }
                        )
                    },
                    function (err) {

                        bsNotify.show(
                            {
                                title: "Gagal",
                                content: err.data.Message,
                                type: 'danger'
                            }
                        );
                    }
                )
            }
            else
                if ($scope.flagPDD == 'Revisi') {

                    var IdRevisi = $scope.PilihanRevisi.ReasonRevisionId;
                    $scope.KalkulasiTLSDate.RequestedPDD = null;

                    $scope.KalkulasiTLSDate.RequestedPDD = angular.copy($scope.bind_data.data[0].SentUnitDate);
                    var HariTLS = $scope.bind_data.data[0].LeadTime;
                    $scope.KalkulasiTLSDate.RequestedPDD = new Date($scope.KalkulasiTLSDate.RequestedPDD.setDate($scope.KalkulasiTLSDate.RequestedPDD.getDate() - HariTLS));

                    try {
                        $scope.KalkulasiTLSDate.RequestedPDD = $scope.KalkulasiTLSDate.RequestedPDD.getFullYear() + '-'
                            + ('0' + ($scope.KalkulasiTLSDate.RequestedPDD.getMonth() + 1)).slice(-2) + '-'
                            + ('0' + $scope.KalkulasiTLSDate.RequestedPDD.getDate()).slice(-2) + 'T'
                            + (($scope.KalkulasiTLSDate.RequestedPDD.getHours() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getHours()) + ':'
                            + (($scope.KalkulasiTLSDate.RequestedPDD.getMinutes() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getMinutes()) + ':'
                            + (($scope.KalkulasiTLSDate.RequestedPDD.getSeconds() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.RequestedPDD.getSeconds());
                    }
                    catch (e1) {
                        $scope.KalkulasiTLSDate.RequestedPDD = null;
                    }

                    for (var i in $scope.bind_data.data) { $scope.bind_data.data[i].EarliestPDD = $scope.TLSDate.earliestPDD };
                    for (var i in $scope.bind_data.data) {
                        $scope.bind_data.data[i].ReasonRevisionId = IdRevisi;

                    };

                    MaintainPDDFactory.confirmPDDDateTLS($scope.KalkulasiTLSDate).then(
                        function (res) {
                            MaintainPDDFactory.revisiPDDCash($scope.bind_data.data).then(
                                function (res) {
                                    MaintainPDDFactory.getData('?start=1&limit=5').then(
                                        function (res) {
                                            $scope.bind_data.data = res.data.Result;
                                            $scope.KalkulasiRRNKredit = false;
                                            $scope.daftarPDD = false;


                                        }

                                    )
                                    bsNotify.show(
                                        {
                                            title: "Sukses",
                                            content: "PDD Berhasil disimpan.",
                                            type: 'success'
                                        }
                                    );

                                },
                                function (err) {

                                    bsNotify.show(
                                        {
                                            title: "Gagal",
                                            content: "Revisi PDD gagal.",
                                            type: 'danger'
                                        }
                                    );
                                }
                            )

                        },
                        function (err) {

                            bsNotify.show(
                                {
                                    title: "Gagal",
                                    content: err.data.Message,
                                    type: 'danger'
                                }
                            );
                        }
                    )

                }
        };


        $scope.cancelPDDCashNorangka = function () {

            $scope.MainPDD = true;
            $scope.KalkulasiPDDCash = false;
            $scope.DateStart();

        }

        $scope.cancelBuatPDD = function () {
            $scope.tambahPDD = true;
            $scope.MainPDD = false;
            $scope.MainPDDKredit = false;
            $scope.MainRRNCash = false;
            $scope.MainRRNKredit = false;
            $scope.DetilKalkulasiPDDCash = false;
            $scope.DateStart();

        }

        $scope.kalkulasiPDDNorangkaCredit = function () {

            //var tglDOc = ($scope.testDate.date).toJSON().slice(0, 10);
            //var tglTDP = ($scope.testDate2.date).toJSON().slice(0, 10);
            // var PLODDate = ($scope.selected_data.PLOD).toJSON().slice(0, 10);
            var RevisiBit = false;

            var tglDOc = $scope.testDate.date.getFullYear() + '-'
                + ('0' + ($scope.testDate.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDate.date.getDate()).slice(-2) + 'T'
                + (($scope.testDate.date.getHours() < '10' ? '0' : '') + $scope.testDate.date.getHours()) + ':'
                + (($scope.testDate.date.getMinutes() < '10' ? '0' : '') + $scope.testDate.date.getMinutes()) + ':'
                + (($scope.testDate.date.getSeconds() < '10' ? '0' : '') + $scope.testDate.date.getSeconds());

            tglDOc.slice(0, 10);

            var tglTDP = $scope.testDate2.date.getFullYear() + '-'
                + ('0' + ($scope.testDate2.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDate2.date.getDate()).slice(-2) + 'T'
                + (($scope.testDate2.date.getHours() < '10' ? '0' : '') + $scope.testDate2.date.getHours()) + ':'
                + (($scope.testDate2.date.getMinutes() < '10' ? '0' : '') + $scope.testDate2.date.getMinutes()) + ':'
                + (($scope.testDate2.date.getSeconds() < '10' ? '0' : '') + $scope.testDate2.date.getSeconds());

            tglTDP.slice(0, 10);

            var PLODDate = $scope.selected_data.PLOD.getFullYear() + '-'
                + ('0' + ($scope.selected_data.PLOD.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.selected_data.PLOD.getDate()).slice(-2) + 'T'
                + (($scope.selected_data.PLOD.getHours() < '10' ? '0' : '') + $scope.selected_data.PLOD.getHours()) + ':'
                + (($scope.selected_data.PLOD.getMinutes() < '10' ? '0' : '') + $scope.selected_data.PLOD.getMinutes()) + ':'
                + (($scope.selected_data.PLOD.getSeconds() < '10' ? '0' : '') + $scope.selected_data.PLOD.getSeconds());

            PLODDate.slice(0, 10);


            if ($scope.flagPDD == 'Revisi') {
                RevisiBit = true;
                $scope.disabledPengirimanUnit = true;
            } else {
                RevisiBit = false;
                $scope.disabledPengirimanUnit = false;
            }
            //
            var choosePolice = false;
            var directPDC = false;
            var sentSTNK = false;

            if ($scope.selected_data.ChoosePoliceNo == null || $scope.selected_data.ChoosePoliceNo == false) {
                choosePolice = false;
            } else {
                choosePolice = true;
            }

            if ($scope.selected_data.DirectDeliveryPDC == null || $scope.selected_data.DirectDeliveryPDC == false) {
                directPDC = false;
            } else {
                directPDC = true;
            }

            if ($scope.selected_data.SentWithSTNK == null || $scope.selected_data.SentWithSTNK == false) {
                sentSTNK = false;
            } else {
                sentSTNK = true;
            }


            var getKalkulasiCredit = "?SPKId=" + $scope.selected_data.SpkId + "&SoId=" + $scope.selected_data.SoId + "&RRNNo=" + $scope.selected_data.RRNNo + "&DocCompRecDate=" + tglDOc + "&TDPDate=" + tglTDP + "&TLSSendDate=" + $scope.TLSDate.earliestPDD + "&DirectDeliveryFromPDC=" + directPDC + "&ChoosePoliceNo=" + choosePolice + "&SentWithSTNK=" + sentSTNK + "&PLOD=" + PLODDate + "&RevisiBit=" + RevisiBit + "&StatusFormABit=" + $scope.TLSDate.statusFormA;
            MaintainPDDFactory.getKalkulasiPDD(getKalkulasiCredit).then(
                function (res) {

                    $scope.bind_data.data = res.data.Result;
                    $scope.DateStart();

                    // $scope.firstDateChange();
                    // $scope.secondDateChange();
                    // $scope.thirdDateChange();

                    var date01 = $scope.bind_data.data[0]['DocCompRecDate'];
                    var date02 = $scope.bind_data.data[0]['TDPDate'];
                    // var date03 = $scope.bind_data.data[0]['FullPaymentDate'];

                    $scope.testDate1 = {
                        date: date01,
                        status: 'firstDate'
                    };

                    $scope.events.push($scope.testDate1);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);

                    $scope.testDate2 = {
                        date: date02,
                        status: 'secondDate'
                    };

                    $scope.events.push($scope.testDate2);
                    $scope.refreshCalendar = false;
                    $timeout(function () { $scope.refreshCalendar = true }, 0);
                    // $scope.testDate3 = {
                    //     date: date03,
                    //     status: 'thirdDate'
                    // };

                    $scope.fourthDateChange();
                    $scope.sixDateChange();
                    var date05 = $scope.bind_data.data[0]['SentUnitDate'];
                    var pengirimanSTNK = $scope.bind_data.data[0]['SentWithSTNK'];

                    if (pengirimanSTNK == true) {
                        $scope.dateOptions5 = {
                            // formatYear: 'yyyy-mm-dd',
                            // startingDay: 1,
                            // minMode: 'year'
                            startingDay: 1,
                            format: "dd/MM/yyyy",
                            disableWeekend: 1,
                            minDate: new Date(date05),
                        };

                        $scope.testDate5 = {
                            date: date05,
                            status: 'fifthDate'
                        };

                        $scope.events.push($scope.testDate5);
                        $scope.KalkulasiPDDKredit = true;
                        $scope.MainPDDKredit = false;

                    } else {
                        $scope.dateOptions5 = {
                            // formatYear: 'yyyy-mm-dd',
                            // startingDay: 1,
                            // minMode: 'year'
                            startingDay: 1,
                            format: "dd/MM/yyyy",
                            disableWeekend: 1,
                            minDate: new Date(),
                        };
                        $scope.testDate5 = {
                            date: date05,
                            status: 'fifthDate'
                        };

                        $scope.events.push($scope.testDate5);
                        $scope.KalkulasiPDDKredit = true;
                        $scope.MainPDDKredit = false;
                    }

                    // $scope.testDate5 = {
                    //     date: date05,
                    //     status: 'fifthDate'
                    // };

                    // $scope.events.push($scope.testDate5);
                    // $scope.KalkulasiPDDKredit = true;
                    // $scope.MainPDDKredit = false;
                },
                function (err) {

                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: err.data.Message,
                            type: 'warning'
                        }
                    );
                }
            )


        };
        $scope.cancelPDDNorangkaCredit = function () {

            $scope.MainPDDKredit = true;
            $scope.KalkulasiPDDKredit = false;
            $scope.DateStart();

        }

        $scope.kalkulasiRRNCash = function () {
            // var tglDOc = ($scope.testDateRRN.date).toJSON().slice(0, 10);
            // var tglDP = ($scope.testDateRRN2.date).toJSON().slice(0, 10);
            // var tglFUllPayment = ($scope.testDateRRN3.date).toJSON().slice(0, 10);


            var tglDOc = $scope.testDateRRN.date.getFullYear() + '-'
                + ('0' + ($scope.testDateRRN.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDateRRN.date.getDate()).slice(-2) + 'T'
                + (($scope.testDateRRN.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN.date.getHours()) + ':'
                + (($scope.testDateRRN.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN.date.getMinutes()) + ':'
                + (($scope.testDateRRN.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN.date.getSeconds());

            var tglDP = $scope.testDateRRN2.date.getFullYear() + '-'
                + ('0' + ($scope.testDateRRN2.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDateRRN2.date.getDate()).slice(-2) + 'T'
                + (($scope.testDateRRN2.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN2.date.getHours()) + ':'
                + (($scope.testDateRRN2.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN2.date.getMinutes()) + ':'
                + (($scope.testDateRRN2.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN2.date.getSeconds());

            var tglFUllPayment = $scope.testDateRRN3.date.getFullYear() + '-'
                + ('0' + ($scope.testDateRRN3.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDateRRN3.date.getDate()).slice(-2) + 'T'
                + (($scope.testDateRRN3.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN3.date.getHours()) + ':'
                + (($scope.testDateRRN3.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN3.date.getMinutes()) + ':'
                + (($scope.testDateRRN3.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN3.date.getSeconds());

            tglDOc.slice(0, 10);
            tglDP.slice(0, 10);
            tglFUllPayment.slice(0, 10);

            var PLODDate = $scope.selected_data.PLOD.getFullYear() + '-'
                + ('0' + ($scope.selected_data.PLOD.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.selected_data.PLOD.getDate()).slice(-2) + 'T'
                + (($scope.selected_data.PLOD.getHours() < '10' ? '0' : '') + $scope.selected_data.PLOD.getHours()) + ':'
                + (($scope.selected_data.PLOD.getMinutes() < '10' ? '0' : '') + $scope.selected_data.PLOD.getMinutes()) + ':'
                + (($scope.selected_data.PLOD.getSeconds() < '10' ? '0' : '') + $scope.selected_data.PLOD.getSeconds());

            PLODDate.slice(0, 10);

            var RevisiBit = false;

            if ($scope.flagPDD == 'Revisi') {
                RevisiBit = true;
                $scope.disabledPengirimanUnit = true;
            } else {
                RevisiBit = false;
                $scope.disabledPengirimanUnit = false;
            }

            var choosePolice = false;
            var directPDC = false;
            var sentSTNK = false;

            if ($scope.selected_data.ChoosePoliceNo == null || $scope.selected_data.ChoosePoliceNo == false) {
                choosePolice = false;
            } else {
                choosePolice = true;
            }

            if ($scope.selected_data.DirectDeliveryPDC == null || $scope.selected_data.DirectDeliveryPDC == false) {
                directPDC = false;
            } else {
                directPDC = true;
            }

            if ($scope.selected_data.SentWithSTNK == null || $scope.selected_data.SentWithSTNK == false) {
                sentSTNK = false;
            } else {
                sentSTNK = true;
            }


            var getlKalkulasi = "?SPKId=" + $scope.selected_data.SpkId + "&SoId=" + $scope.selected_data.SoId + "&RRNNo=" + $scope.selected_data.RRNNo + "&DocCompRecDate=" + tglDOc + "&DPDate=" + tglDP + "&FullPaymentDate=" + tglFUllPayment + "&TLSSendDate=" + $scope.TLSDate.earliestPDD + "&DirectDeliveryFromPDC=" + directPDC + "&ChoosePoliceNo=" + choosePolice + "&SentWithSTNK=" + sentSTNK + "&PLOD=" + PLODDate + "&RevisiBit=" + RevisiBit + "&StatusFormABit=" + $scope.TLSDate.statusFormA;
            MaintainPDDFactory.getKalkulasiPDD(getlKalkulasi).then(
                function (res) {

                    $scope.bind_data.data = res.data.Result;


                    $scope.events = [];
                    $scope.Color_Da_CalendarAtLihat = [{ date: null, status: '' }];
                    //$scope.Color_Da_CalendarAtLihat=[{date:null,status: 'fifthDate'}];
                    $scope.Color_Da_CalendarAtLihat.splice(0, 1);

                    $scope.a_awal = new Date($scope.bind_data.data[0].SentUnitDateWithoutWDP);
                    $scope.b_akhir = new Date($scope.bind_data.data[0].SentUnitDate);
                    var oneDayCalculation = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                    var diffDaysRRN = Math.round(Math.abs(($scope.b_akhir.getTime() - $scope.a_awal.getTime()) / (oneDayCalculation)));
                    var diffDaysRRNForLoop = diffDaysRRN + 1;

                    for (var i = 0; i < diffDaysRRNForLoop; i++) {
                        var date_to_insert = new Date(angular.copy($scope.a_awal).setDate(angular.copy($scope.a_awal).getDate() + i));//ini tadinya i bukan 1 daripada a semua pake angular copy

                        date_to_insert = date_to_insert.getFullYear() + '-' + ('0' + (date_to_insert.getMonth() + 1)) + '-' + ('0' + (date_to_insert.getDate())).slice(-2) + '';
                        $scope.Color_Da_CalendarAtLihat.push({ date: date_to_insert, status: 'fifthDate' });
                    }

                    for (var ii = 0; ii < $scope.Color_Da_CalendarAtLihat.length; ii++) {
                        $scope.events.push($scope.Color_Da_CalendarAtLihat[ii]);
                        $scope.refreshCalendar = false;
                        $timeout(function () { $scope.refreshCalendar = true }, 0);
                    }


                    $scope.KalkulasiRRNCash = true;
                    $scope.MainRRNCash = false;

                },
                function (err) {

                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: err.data.Message,
                            type: 'warning'
                        }
                    );
                }
            )




        }

        $scope.kalkulasiRRNCredit = function () {
            // var tglDOc = ($scope.testDate.date).toJSON().slice(0, 10);
            // var tglTDP = ($scope.testDate2.date).toJSON().slice(0, 10);

            var tglDOc = $scope.testDateRRN.date.getFullYear() + '-'
                + ('0' + ($scope.testDateRRN.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDateRRN.date.getDate()).slice(-2) + 'T'
                + (($scope.testDateRRN.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN.date.getHours()) + ':'
                + (($scope.testDateRRN.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN.date.getMinutes()) + ':'
                + (($scope.testDateRRN.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN.date.getSeconds());

            var tglTDP = $scope.testDateRRN2.date.getFullYear() + '-'
                + ('0' + ($scope.testDateRRN2.date.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.testDateRRN2.date.getDate()).slice(-2) + 'T'
                + (($scope.testDateRRN2.date.getHours() < '10' ? '0' : '') + $scope.testDateRRN2.date.getHours()) + ':'
                + (($scope.testDateRRN2.date.getMinutes() < '10' ? '0' : '') + $scope.testDateRRN2.date.getMinutes()) + ':'
                + (($scope.testDateRRN2.date.getSeconds() < '10' ? '0' : '') + $scope.testDateRRN2.date.getSeconds());

            tglDOc.slice(0, 10);
            tglTDP.slice(0, 10);

            //var tglFUllPayment = ($scope.testDate3.date).toJSON().slice(0, 10);

            var PLODDate = $scope.selected_data.PLOD.getFullYear() + '-'
                + ('0' + ($scope.selected_data.PLOD.getMonth() + 1)).slice(-2) + '-'
                + ('0' + $scope.selected_data.PLOD.getDate()).slice(-2) + 'T'
                + (($scope.selected_data.PLOD.getHours() < '10' ? '0' : '') + $scope.selected_data.PLOD.getHours()) + ':'
                + (($scope.selected_data.PLOD.getMinutes() < '10' ? '0' : '') + $scope.selected_data.PLOD.getMinutes()) + ':'
                + (($scope.selected_data.PLOD.getSeconds() < '10' ? '0' : '') + $scope.selected_data.PLOD.getSeconds());

            PLODDate.slice(0, 10);

            var RevisiBit = false;

            if ($scope.flagPDD == 'Revisi') {
                RevisiBit = true;
                $scope.disabledPengirimanUnit = true;
            } else {
                RevisiBit = false;
                $scope.disabledPengirimanUnit = false;
            }

            var choosePolice = false;
            var directPDC = false;
            var sentSTNK = false;

            if ($scope.selected_data.ChoosePoliceNo == null || $scope.selected_data.ChoosePoliceNo == false) {
                choosePolice = false;
            } else {
                choosePolice = true;
            }

            if ($scope.selected_data.DirectDeliveryPDC == null || $scope.selected_data.DirectDeliveryPDC == false) {
                directPDC = false;
            } else {
                directPDC = true;
            }

            if ($scope.selected_data.SentWithSTNK == null || $scope.selected_data.SentWithSTNK == false) {
                sentSTNK = false;
            } else {
                sentSTNK = true;
            }


            var getlKalkulasi = "?SPKId=" + $scope.selected_data.SpkId + "&SoId=" + $scope.selected_data.SoId + "&RRNNo=" + $scope.selected_data.RRNNo + "&DocCompRecDate=" + tglDOc + "&TDPDate=" + tglTDP + "&TLSSendDate=" + $scope.TLSDate.earliestPDD + "&DirectDeliveryFromPDC=" + directPDC + "&ChoosePoliceNo=" + choosePolice + "&SentWithSTNK=" + sentSTNK + "&PLOD=" + PLODDate + "&RevisiBit=" + RevisiBit + "&StatusFormABit=" + $scope.TLSDate.statusFormA;
            MaintainPDDFactory.getKalkulasiPDD(getlKalkulasi).then(
                function (res) {

                    $scope.bind_data.data = res.data.Result;


                    $scope.events = [];
                    $scope.Color_Da_CalendarAtLihat = [{ date: null, status: '' }];
                    //$scope.Color_Da_CalendarAtLihat=[{date:null,status: 'fifthDate'}];
                    $scope.Color_Da_CalendarAtLihat.splice(0, 1);

                    $scope.a_awal = new Date($scope.bind_data.data[0].SentUnitDateWithoutWDP);
                    $scope.b_akhir = new Date($scope.bind_data.data[0].SentUnitDate);
                    var oneDayCalculation = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                    var diffDaysRRN = Math.round(Math.abs(($scope.b_akhir.getTime() - $scope.a_awal.getTime()) / (oneDayCalculation)));
                    var diffDaysRRNForLoop = diffDaysRRN + 1;

                    for (var i = 0; i < diffDaysRRNForLoop; i++) {
                        var date_to_insert = new Date(angular.copy($scope.a_awal).setDate(angular.copy($scope.a_awal).getDate() + i));//ini tadinya i bukan 1 daripada a semua pake angular copy

                        date_to_insert = date_to_insert.getFullYear() + '-' + ('0' + (date_to_insert.getMonth() + 1)) + '-' + ('0' + (date_to_insert.getDate())).slice(-2) + '';
                        $scope.Color_Da_CalendarAtLihat.push({ date: date_to_insert, status: 'fifthDate' });
                    }





                    for (var ii = 0; ii < $scope.Color_Da_CalendarAtLihat.length; ii++) {
                        $scope.events.push($scope.Color_Da_CalendarAtLihat[ii]);
                        $scope.refreshCalendar = false;
                        $timeout(function () { $scope.refreshCalendar = true }, 0);
                    }

                    $scope.KalkulasiRRNKredit = true;
                    $scope.MainRRNKredit = false;
                },
                function (err) {

                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: err.data.Message,
                            type: 'warning'
                        }
                    );
                }
            )




        }

        $scope.cancelkalkulasiRRNCash = function () {

            $scope.MainRRNCash = true;
            $scope.KalkulasiRRNCash = false;

        }

        $scope.cancelkalkulasiRRNCredit = function () {

            $scope.MainRRNKredit = true;
            $scope.KalkulasiRRNKredit = false;

        }

        $scope.openModalRevision = function () {
            $scope.flagPDD = 'Revisi';

            angular.element('.ui.modal.ModalFormRevision').modal('show');
        }

        $scope.OK_Clicked = function () {
            angular.element('.ui.modal.ModalRevisionAlert').modal('hide');
        }

        $scope.Lanjut_Clicked = function (selected_dataRevisi) {
            $scope.flagPDD = 'Revisi';


            $scope.disabledLihatPDD = true;
            if (selected_dataRevisi.OnOffTheRoadId == 2) {
                $scope.disabledNoPolisi = true;
                $scope.disabledSTNK = true;
            } else {
                $scope.disabledNoPolisi = false;
                $scope.disabledSTNK = false;
            }

            //console.log('$scope.PilihanRevisi',$scope.PilihanRevisi);

            if (selected_dataRevisi.CountRevisi >= 3 & $scope.PilihanRevisi.ReasonRevisionName != 'Revisi tanggal pengiriman unit(Permintaan pelanggan)') {

                angular.element('.ui.modal.ModalRevisionAlert').modal('show');
            }
            else {
                angular.element('.ui.modal.ModalFormRevision').modal('hide');
                $scope.disabledLihatPDD = true;
                $scope.daftarPDD = true;
                $scope.tambahPDD = true;
                $scope.disabledLihatPDD = true;
                $scope.DetilKalkulasiPDDCash = false;
            }


        }
        $scope.Batal_Clicked = function () {
            angular.element('.ui.modal.ModalFormRevision').modal('hide');
        }

        $scope.openHistoryPDD = function () {
            var getHistory = "?start=1&limit=100" + "&PDDId=" + $scope.selected_data.PDDId;
            MaintainPDDFactory.getHistoryPDD(getHistory).then(
                function (res) {
                    $scope.HistoryPDD = res.data.Result;
                }
            )
            $scope.HistoryPDD = true;
            $scope.DetilKalkulasiPDDCash = false;


        }

        $scope.cancelHistoryPDD = function () {
            $scope.HistoryPDD = false;
            $scope.DetilKalkulasiPDDCash = true;


        }


        $scope.selected_data = {};
        $scope.selected_data.DirectDeliveryPDC = false;
        //$scope.selected_data.ChoosePoliceNo = false;
        $scope.selected_data.SentWithSTNK = false;

        $scope.kalkulasiPDD = function () {
            $scope.KalkulasiTLSDate = {};
            $scope.KalkulasiTLSDate.FrameNumber = "";
            $scope.KalkulasiTLSDate.RRN = "";
            $scope.KalkulasiTLSDate.DTPLOD = "";
            $scope.KalkulasiTLSDate.PaketAksesorisTAM = "";
            $scope.KalkulasiTLSDate.DeliveryCategory = "";

            if ($scope.selected_data.DirectDeliveryPDC == false || $scope.selected_data.DirectDeliveryPDC == null|| $scope.selected_data.DirectDeliveryPDC == undefined) {
                $scope.KalkulasiTLSDate.DeliveryCategory = 1;
            } else {
                $scope.KalkulasiTLSDate.DeliveryCategory = 3;
            }
            console.log('$scope.selected_data.DirectDeliveryPDC >>',$scope.selected_data.DirectDeliveryPDC);

            if ($scope.selected_data.FrameNo == null || $scope.selected_data.FrameNo == '') {
                $scope.KalkulasiTLSDate.FrameNumber = "";
                $scope.KalkulasiTLSDate.RRN = $scope.selected_data.RRNNo;
                $scope.KalkulasiTLSDate.DTPLOD = $scope.selected_data.PLOD;

                try {
                    $scope.KalkulasiTLSDate.DTPLOD = $scope.KalkulasiTLSDate.DTPLOD.getFullYear() + '-'
                        + ('0' + ($scope.KalkulasiTLSDate.DTPLOD.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.KalkulasiTLSDate.DTPLOD.getDate()).slice(-2) + 'T'
                        + (($scope.KalkulasiTLSDate.DTPLOD.getHours() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.DTPLOD.getHours()) + ':'
                        + (($scope.KalkulasiTLSDate.DTPLOD.getMinutes() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.DTPLOD.getMinutes()) + ':'
                        + (($scope.KalkulasiTLSDate.DTPLOD.getSeconds() < '10' ? '0' : '') + $scope.KalkulasiTLSDate.DTPLOD.getSeconds());
                }
                catch (e1) {
                    $scope.KalkulasiTLSDate.DTPLOD = null;
                }


            } else {
                $scope.KalkulasiTLSDate.FrameNumber = $scope.selected_data.FrameNo;
                $scope.KalkulasiTLSDate.RRN = "";
                $scope.KalkulasiTLSDate.DTPLOD = "";
            }

            if ($scope.selected_data.DirectDeliveryPDC == false || $scope.selected_data.DirectDeliveryPDC == null) {
                $scope.selected_data.DirectDeliveryPDC == false;
            } else {
                $scope.selected_data.DirectDeliveryPDC == true;
            }

            if ($scope.selected_data.ChoosePoliceNo == false || $scope.selected_data.ChoosePoliceNo == null) {
                $scope.selected_data.ChoosePoliceNo == false;
            } else {
                $scope.selected_data.ChoosePoliceNo == true;
            }

            if ($scope.selected_data.SentWithSTNK == false || $scope.selected_data.SentWithSTNK == null) {
                $scope.selected_data.SentWithSTNK == false;
            } else {
                $scope.selected_data.SentWithSTNK == true;
            }


            MaintainPDDFactory.kalkulasiTLSDate($scope.KalkulasiTLSDate).then(
                function (res) {
                    $scope.TLSDate = res.data;

                    $scope.TLSDate.earliestPDD = $scope.TLSDate.earliestPDD.getFullYear() + '-'
                        + ('0' + ($scope.TLSDate.earliestPDD.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.TLSDate.earliestPDD.getDate()).slice(-2) + 'T'
                        + (($scope.TLSDate.earliestPDD.getHours() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getHours()) + ':'
                        + (($scope.TLSDate.earliestPDD.getMinutes() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getMinutes()) + ':'
                        + (($scope.TLSDate.earliestPDD.getSeconds() < '10' ? '0' : '') + $scope.TLSDate.earliestPDD.getSeconds());

                    $scope.TLSDate.earliestPDD.slice(0, 10);

                    if ($scope.selected_data.FrameNo == null && $scope.selected_data.FundSourceName == 'Cash') {
                        $scope.tambahPDD = false;
                        $scope.MainPDD = false;
                        $scope.MainRRNKredit = false;
                        $scope.MainRRNCash = true;

                        if ($scope.flagPDD == 'Revisi') {
                            $scope.testDateRRN = {
                                date: new Date($scope.selected_data.DocCompRecDate),
                                status: 'firstDate'
                            };
                            $scope.testDateRRN2 = {
                                date: new Date($scope.selected_data.DPDate),
                                status: 'secondDate'
                            };
                            $scope.testDateRRN3 = {
                                date: new Date($scope.selected_data.FullPaymentDate),
                                status: 'thirdDate'
                            };
                        }
                    }
                    if ($scope.selected_data.FrameNo == null && $scope.selected_data.FundSourceName == 'Credit') {
                        $scope.tambahPDD = false;
                        $scope.MainRRNKredit = true;

                        if ($scope.flagPDD == 'Revisi') {
                            $scope.testDateRRN = {
                                date: new Date($scope.selected_data.DocCompRecDate),
                                status: 'firstDate'
                            };
                            $scope.testDateRRN2 = {
                                date: new Date($scope.selected_data.TDPDate),
                                status: 'secondDate'
                            };

                        }
                    }
                    if ($scope.selected_data.FrameNo != null && $scope.selected_data.FundSourceName == 'Cash') {
                        $scope.tambahPDD = false;
                        $scope.MainPDD = true;

                        if ($scope.flagPDD == 'Revisi') {
                            $scope.testDate = {
                                date: new Date($scope.selected_data.DocCompRecDate),
                                status: 'firstDate'
                            };
                            $scope.testDate2 = {
                                date: new Date($scope.selected_data.DPDate),
                                status: 'secondDate'
                            };
                            $scope.testDate3 = {
                                date: new Date($scope.selected_data.FullPaymentDate),
                                status: 'thirdDate'
                            };
                        }
                    }
                    if ($scope.selected_data.FrameNo != null && $scope.selected_data.FundSourceName == 'Credit') {
                        $scope.tambahPDD = false;
                        $scope.MainPDDKredit = true;

                        if ($scope.flagPDD == 'Revisi') {
                            $scope.testDate = {
                                date: new Date($scope.selected_data.DocCompRecDate),
                                status: 'firstDate'
                            };
                            $scope.testDate2 = {
                                date: new Date($scope.selected_data.TDPDate),
                                status: 'secondDate'
                            };

                        }
                    }

                },
                function (err) {

                    bsNotify.show(
                        {
                            title: "Gagal",
                            content: err.data.Message,
                            type: 'danger'
                        }
                    );
                }



            )

        }
        MaintainPDDFactory.getResonRevision().then(
            function (res) {
                $scope.listRevision = res.data.Result;
            }
        )


        //Begin Calendar
        $scope.refreshCalendar = true;
        $scope.today = function () {
            $scope.dt1 = new Date();
            $scope.dt2 = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt1 = null;
            $scope.dt2 = null;
        };

        $scope.options = {
            customClass: getDayClass,
			startingDay: 1,
            minDate: new Date(),
            showWeeks: true
        };

        // Disable weekend selection
        $scope.disabledWeekend = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };


        $scope.toggleMin = function () {
            $scope.options.minDate = $scope.options.minDate ? null : new Date();
        };

        $scope.toggleMin();

        $scope.setDate = function (year, month, day) {
            $scope.dt1 = new Date(year, month, day);
            $scope.dt2 = new Date(year, month, day);
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date(tomorrow);
        afterTomorrow.setDate(tomorrow.getDate() + 1);


        $scope.testDate = {
            date: new Date(),
            status: 'firstDate'
        };

        $scope.testDate2 = {
            date: new Date(),
            status: 'secondDate'
        };
        $scope.testDate3 = {
            date: new Date(),
            status: 'thirdDate'
        };
        $scope.testDate4 = {
            date: new Date(),
            status: 'fourthDate'
        };
        $scope.testDate5 = {
            date: new Date(),
            status: 'fifthDate'
        };
        $scope.testDate6 = {
            date: new Date(),
            status: 'sixDate'
        };
        $scope.testDateRRN = {
            date: new Date(),
            status: 'firstDate'
        };
        $scope.testDateRRN2 = {
            date: new Date(),
            status: 'secondDate'
        };
        $scope.testDateRRN3 = {
            date: new Date(),
            status: 'thirdDate'
        };

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        //End Calendar

        //Begin DatePicker
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.firstDateChange = function () {
            var date01 = $scope.testDate.date;
            $scope.testDate = {
                date: date01,
                status: 'firstDate'
            };

            $scope.events.push($scope.testDate);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        //atas ini tadinya bukan dd/MM/yyyy dd-MMMM-yyyy
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };

        $scope.secondDateChange = function () {

            $scope.events.push($scope.testDate2);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };


        $scope.popup2 = {
            opened: false
        };

        $scope.open3 = function () {
            $scope.popup3.opened = true;
        };

        $scope.thirdDateChange = function () {

            $scope.events.push($scope.testDate3);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.popup3 = {
            opened: false
        };

        $scope.fourthDateChange = function () {

            var date04 = $scope.bind_data.data[0]['STNKDoneDate'];

            $scope.testDate4 = {
                date: date04,
                status: 'fourthDate'
            };

            $scope.events.push($scope.testDate4);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.sixDateChange = function () {

            var date06 = $scope.bind_data.data[0]['ApprovalLeasingDate'];

            $scope.testDate6 = {
                date: date06,
                status: 'sixDate'
            };

            $scope.events.push($scope.testDate6);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.open5 = function () {
            $scope.popup5.opened = true;
        };



        $scope.fifthDateChange = function () {

            if ($scope.testDate5.date < $scope.bind_data.data[0]['SentUnitDate']) {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Pengiriman Unit paling cepat bisa dilakukan pada tanggal : " + fixDate($scope.bind_data.data[0]['SentUnitDate']),
                        type: 'warning'
                    }
                );
            }
            $scope.events.push($scope.testDate5);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };


        $scope.popup5 = {
            opened: false
        };

        $scope.cancelDateChange = function () {

            var index = $scope.events.indexOf($scope.testDate4);
            $scope.events.splice(index, 1);

            index = $scope.events.indexOf($scope.testDate5);
            $scope.events.splice(index, 1);


            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        //atas ini tadinya bukan dd/MM/yyyy dd-MMMM-yyyy
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.DateStart = function () {

            $scope.testDate = {
                date: new Date(),
                status: 'firstDate'
            };

            $scope.testDate2 = {
                date: new Date(),
                status: 'secondDate'
            };
            $scope.testDate3 = {
                date: new Date(),
                status: 'thirdDate'
            };
            $scope.testDate4 = {
                date: new Date(),
                status: 'fourthDate'
            };
            $scope.testDate5 = {
                date: new Date(),
                status: 'fifthDate'
            };
            $scope.testDateRRN = {
                date: new Date(),
                status: 'firstDate'
            };
            $scope.testDateRRN2 = {
                date: new Date(),
                status: 'secondDate'
            };
            $scope.testDateRRN3 = {
                date: new Date(),
                status: 'thirdDate'
            };

            $scope.events.splice($scope.testDate);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        //atas ini tadinya bukan dd/MM/yyyy dd-MMMM-yyyy
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];


        $scope.openRRN1 = function () {
            $scope.popupRRN1.opened = true;
        };

        $scope.firstDateRRNChange = function () {

            $scope.events.push($scope.testDateRRN);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };


        $scope.popupRRN1 = {
            opened: false
        };

        $scope.openRRN2 = function () {
            $scope.popupRRN2.opened = true;
        };

        $scope.secondDateRRNChange = function () {

            $scope.events.push($scope.testDateRRN2);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };

        $scope.popupRRN2 = {
            opened: false
        };
        $scope.openRRN3 = function () {
            $scope.popupRRN3.opened = true;
        };

        $scope.thirdDateRRNChange = function () {

            $scope.events.push($scope.testDateRRN3);
            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };


        $scope.popupRRN3 = {
            opened: false
        };

        $scope.kalkulasiDateRRNCash = function () {
            var now = new Date('2017-03-23T11:47:00.06');

            for (var d = new Date('2017-03-10T11:47:00.06'); d <= now; d.setDate(d.getDate() + 1)) {
                $scope.events.push({ date: new Date(d), status: 'fifthDate' });

            }


            $scope.refreshCalendar = false;
            $timeout(function () { $scope.refreshCalendar = true }, 0);
        };


        //End DatePicker

        $scope.cekSimpanCashNorangka = function () {
            $scope.disabledCekSimpanCashNorangka = true;
            if ($scope.testDate5 < $scope.bind_data.data[0]['FullPaymentDate']) {

                setTimeout(function () {
                    angular.element('.ui.modal.SavePDDNoRangka').modal('refresh');
                }, 0);
                angular.element('.ui.modal.SavePDDNoRangka').modal('show');

            }
            else {
                angular.element('.ui.modal.SavePDDNoRangka').modal('hide');
                $scope.simpanPDDCash();
            }
        }

        $scope.cekSimpanCashRRN = function () {
            if ($scope.bind_data.data[0]['SentUnitDate'] < $scope.bind_data.data[0]['FullPaymentDate']) {

                setTimeout(function () {
                    angular.element('.ui.modal.SavePDDRRN').modal('refresh');
                }, 0);
                angular.element('.ui.modal.SavePDDRRN').modal('show');

            }
            else {
                $scope.simpanRRNCash();
                angular.element('.ui.modal.SavePDDRRN').modal('hide');

            }
        }

        $scope.YaSaveNorangka = function () {
            $scope.simpanPDDCash();
            angular.element('.ui.modal.SavePDDNoRangka').modal('hide');
        }

        $scope.TidakSaveNorangka = function () {
            angular.element('.ui.modal.SavePDDNoRangka').modal('hide');
        }

        $scope.YaSaveRRN = function () {
            $scope.simpanRRNCash();
            angular.element('.ui.modal.SavePDDRRN').modal('hide');
        }

        $scope.TidakSaveRRN = function () {
            angular.element('.ui.modal.SavePDDRRN').modal('hide');
        }

    });
