angular.module('app')
    .controller('ReportSalesController', function($scope, $http, CurrentUser, ReportSales, bsNotify, Region, GetsudoPeriod, OAPPeriod, MaintainSOFactory, ModelFactory, FindStockFactory, $timeout) {
        
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        // $scope.showPreview = true;
        
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mreportFilter= {DateFrom:'',DateTo:'',GroupDealerId:'',AreaId:'',OutletId:'',EmployeeId:'',VehicleModelId:''}; //Model
    $scope.mreportFilter.visibility= {
        marketPolreg:0,
        rsSugestion:0,
        stockBase:0,
        cl:0,
        outletShare:0,
        rsAdjusment:0,
        stockRatio:0,
        market:0,
        rsTargetMarket:0,
        wsSugestion:0,
        os:0,
        retailSales:0,
        rsOapRap:0,
        wsAdjusment:0,
        spk:0,
        outletShare:0,
        gapVsTarget:0,
        gapVsTargetMarket:0,
        gapVsTargetOutletShare:0,
        minMax:0,
        gapVsOutletContribution:0,
        outerSalesContribution:0,
        gapVsTargetOutletSalesContribution:0
    }; //Model

    $scope.date123 = function(selected){
        console.log("From", mreportFilter.DateFrom);
    }
    // var outletData = [];

    var outletObj = null;
    var outletData = [];
    var outletDataInt=[];
    var areaProvince = [];
    var provinceCityData = [];
    var orgTreeData = [];
    var orgTreeDataInt = [];
    $scope.areaData = [];
    $scope.provinceData = [];
    $scope.orgData= [];
    $scope.olYear = [];

    $scope.checkRights=function(bit){
        console.log('$scope.myRights',$scope.myRights);
        var p=$scope.myRights & Math.pow(2,bit);
        var res=(p==Math.pow(2,bit));
        return res;
    }

    var arrVisibility = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var arrVisibilityOAP = [0,0,0];
    
    $scope.dateOptionsStart = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1
    };

    $scope.dateOptionsEnd = {
        startingDay: 1,
        format: "dd/MM/yyyy",
        disableWeekend: 1,
    }

    $scope.tanggalmin = function (dt){
        //console.log("1",dt)
        $scope.dateOptionsEnd.minDate = dt;
        //console.log("2",$scope.mintgl)
    }
   
    //----------------------------------

    $scope.onSelectRows = function(rows){
    }

    var xRep = [];
    
    $timeout(function(){
        if($scope.checkRights(11)){
                xRep.push({id:1, name:"- Report Admin"});
        }
        if($scope.checkRights(12)){
                xRep.push({id:5, name:"- Report Sales"});
        }
        if($scope.checkRights(13)){
                xRep.push({id:2, name:"- Report SPK"});
        }
        console.log("reeep==>",xRep);
        $scope.report = xRep;
    })
    
    OAPPeriod.getData().then(
        function(res){
            var temp = res.data.Result;
            $scope.olYear = [];
            var obj={};
            for (var i in temp) {
                if (!obj[temp[i].Year]) {
                    obj[temp[i].Year] = {};
                    obj[temp[i].Year]["year"] = temp[i].Year;
                    obj[temp[i].Year]["id"] = temp[i].Year;
                    $scope.olYear.push(obj[temp[i].Year]);
                }                     
            }
            $scope.loading=false;
            return res.data;
        }
    )
    
    ReportSales.getDataDealer().then(
        function(res){
            $scope.getDealer = res.data.Result;
            return res.data;
        }
    );

    MaintainSOFactory.getOutlet().then(
        function(res){
            $scope.getOutlet = res.data.Result;
            return res.data;
        }
    );

    MaintainSOFactory.getDataSalesman().then(
        function(res){
            $scope.getSalesman = res.data.Result;
            return res.data;
        }
    );

    FindStockFactory.getDataArea().then(
        function(res){
            $scope.getArea = res.data.Result;
            return res.data;
        }
    );

    ModelFactory.getData().then(function(res){
        $scope.optionsModel = res.data.Result;
         return res.data;
    });

    // function getAreaProvinceFromHO(HO){
    //     $scope.provinceData = [];
    //     $scope.areaData = [];
    //     var dataSrc = HO.child;
    //     for(var i in dataSrc){
    //         var provinceObj = _.find(areaProvince,{ProvinceId: dataSrc[i].ProvinceId});
    //         var newObj = {ProvinceId:provinceObj.ProvinceId, ProvinceName:provinceObj.ProvinceName};
    //         if(_.findIndex($scope.provinceData, newObj)<0)
    //             $scope.provinceData.push(newObj);
    //         $scope.provinceData = _.sortBy($scope.provinceData, ['ProvinceName']);

    //         var areaObj = _.find(areaProvince,function(o){
    //             return dataSrc[i].AreaId.indexOf(o.AreaId)>=0;
    //         });
    //         var newObj = {AreaId:areaObj.AreaId, Description:areaObj.Description};
    //         if(_.findIndex($scope.areaData, newObj)<0)
    //             $scope.areaData.push(newObj);
    //         $scope.areaData = _.sortBy($scope.areaData, ['Description']);
    //     }
    // }
    function filterArray(data,filter){
        var res = [];
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(filter[key]==null || filter[key]==undefined) continue;
                if(Array.isArray(data[i][key]) && !Array.isArray(filter[key])){
                    if(data[i][key].indexOf(filter[key])<0){
                        valid=false; break;
                    }
                }else if(!Array.isArray(data[i][key]) && Array.isArray(filter[key])){
                    if(filter[key].indexOf(data[i][key])<0){
                        valid=false; break;
                    }
                }else if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }
    // var noAuto = false;
    // var xno = false;
    // var xc = 2;
    // function filterArea(filter){
    //         if(xno){filter=null};
    //     $scope.areaData = [];
    //     var dataSrc = [];
    //     if(filter==undefined || filter==null){
    //         dataSrc = areaProvince;
    //     }else{
    //         dataSrc = filterArray(areaProvince, filter);
    //     }
    //     for(var i=0; i<dataSrc.length; i++){
    //         var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
    //         if(_.findIndex($scope.areaData, areaObj)<0)
    //             $scope.areaData.push(areaObj);
    //     }
    //     if($scope.areaData.length==1){
    //         $scope.mreportFilter.areaId = $scope.areaData[0].AreaId;
    //     }
    // }

    // function filterProvince(filter){
    //     if(xno){filter=null};
    //     $scope.provinceData = [];
    //     var dataSrc = [];
    //     if(filter==undefined || filter==null){
    //         dataSrc = areaProvince;
    //     }else{
    //         dataSrc = filterArray(areaProvince, filter);
    //     }
    //     for(var i=0; i<dataSrc.length; i++){
    //         var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
    //         if(_.findIndex($scope.provinceData, provinceObj)<0)
    //             $scope.provinceData.push(provinceObj);
    //     }
    //     if($scope.provinceData.length==1){
    //         $scope.mreportFilter.provinceId = $scope.provinceData[0].ProvinceId;
    //     }
        
    //     console.log("prov",xc);
    //     xno=false;
    // }

    // function filterDealer(filter){
    //     $scope.orgData = [];
    //     var dataSrc = [];
    //     var xdataSrc = [];
        
    //     if(xno){filter=null};
    //     if(filter==undefined || filter==null){
    //         dataSrc = orgTreeData;
            
    //     }else{
    //         for(var i in orgTreeData){
    //             var inserted = false;
    //             for(var chIdx in orgTreeData[i].child){
    //                 var valid = true;
    //                 for(key in filter){
    //                     if(filter[key]==null || filter[key]==undefined) continue;
    //                     if(Array.isArray(orgTreeData[i].child[chIdx][key])){
    //                         if(orgTreeData[i].child[chIdx][key].indexOf(filter[key])<0){
    //                             valid=false; break;
    //                         }
    //                     }else if(orgTreeData[i].child[chIdx][key]!=filter[key]){
    //                         valid=false; break;
    //                     }
    //                 }
    //                 if(valid){
    //                     if(!inserted){
    //                         var parent = angular.copy(orgTreeData[i]);
    //                         parent.child = [];
    //                         parent.listOutlet = [];
    //                         dataSrc.push(parent);
    //                         inserted=true;
    //                     }
    //                     dataSrc[dataSrc.length-1].child.push(orgTreeData[i].child[chIdx]);
    //                 }
    //             }
    //         }
    //     }
    //     $scope.orgData = dataSrc;
    // }
    
    var outletSel;
    // $scope.changeFilterCombo = function(selected,sender){
    //     if (($scope.mreportFilter.areaId==null || $scope.mreportFilter.areaId=='') && ($scope.mreportFilter.provinceId==null || $scope.mreportFilter.provinceId=='')) {
    //         outletObj = null;    
    //     } 
        
    //     console.log("filterCombo==>",selected);
    //     $timeout(function(){
    //         if(sender.name=='outlet' || sender.name=='outlet2'){
    //             outletSel = selected;
    //             if($scope.mreportFilter.outletId!=null && $scope.mreportFilter.outletId!=undefined){
    //                 $scope.mreportFilter.dealerId=selected.GroupDealerId;
    //                 if(selected!=null && ('HeadOfficeId' in selected)){
    //                         getAreaProvinceFromHO(selected);
    //                         outletObj = _.find(outletData,{OutletId:selected.HeadOfficeId});
    //                         return;
    //                     }
    //                 else if(selected!=null)
    //                     {
    //                         outletObj = _.find(outletData,{OutletId:$scope.mreportFilter.outletId});
    //                         if(selected.OutletId==selected.parent.HeadOfficeId){
    //                             var parentObj = _.find(orgTreeData,{HeadOfficeId:selected.OutletId});
    //                             getAreaProvinceFromHO(parentObj);
    //                             return;
    //                         }
    //                     }
    //                 else
    //                     outletObj = null;
    //             }else{
    //                 outletObj = null;
    //                 $scope.mreportFilter.dealerId=null;
    //             }
    //         }else if(outletObj==null || outletObj.parent.HeadOfficeId!=outletObj.OutletId){
    //             // 
    //             filterDealer({ProvinceId:$scope.mreportFilter.provinceId, AreaId:$scope.mreportFilter.areaId});

    //         }
    //         if(sender.name!='province'){
    //             if($scope.mreportFilter.areaId!=null && $scope.mreportFilter.areaId!=undefined && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
    //                 filterProvince({AreaId:$scope.mreportFilter.areaId, ProvinceId:outletObj.ProvinceId});
    //             else if(($scope.mreportFilter.areaId==null || $scope.mreportFilter.areaId==undefined) && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
    //                 filterProvince({AreaId:outletObj.AreaId, ProvinceId:outletObj.ProvinceId});
    //             else{
    //                 filterProvince({AreaId:$scope.mreportFilter.areaId});
    //                 }
               
    //         }

    //         if(sender.name!='area'){
    //             if($scope.mreportFilter.provinceId!=null && $scope.mreportFilter.provinceId!=undefined && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
    //                 filterArea({ProvinceId:$scope.mreportFilter.provinceId, AreaId:outletObj.AreaId});
    //             else if(($scope.mreportFilter.provinceId==null || $scope.mreportFilter.provinceId==undefined) && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
    //                 filterArea({ProvinceId:outletObj.ProvinceId, AreaId:outletObj.AreaId});
    //             else{
    //                 filterArea({ProvinceId:$scope.mreportFilter.provinceId});
    //             }
    //         }
    //     })
    // }
    

    // $scope.selectOutletInternal = function(selected){
    //     // outletSel = selected;
    //     console.log("outlet==>",selected);
    //     if(selected!=null){
    //         if (selected.level == 1) {
    //             console.log("dealer==>",selected.GroupDealerId,"HO==>",selected.HeadOfficeId);
    //             $scope.mreportFilter.dealerId = selected.GroupDealerId;
    //             $scope.mreportFilter.outletIdInternal = selected.HeadOfficeId;
    //         } else{
    //             console.log("dealer==>",selected.GroupDealerId,"outlet==>",selected.OutletId);
    //             $scope.mreportFilter.dealerId = selected.GroupDealerId;
    //         };
    //     }
    // };
    
    // $scope.selectProduct = function(xProduct){
    //     $scope.mreportFilter.modelId=xProduct.VehicleModelId;
    //     $scope.productTypeData = xProduct.child;
    // }

    // $scope.selectType = function(xType){
    //     $scope.mreportFilter.typeId=xType.VehicleTypeId;
    //     $scope.productColorModel = xType.child;
    // }
    var coloum=[];

    $scope.selectReport = function(rId){
            
        xc=0;
        xno=true;
        
        $scope.mreportFilter.DateFrom=null;
        $scope.mreportFilter.DateTo=null; 
        $scope.mreportFilter.GroupDealerId=null;
        $scope.mreportFilter.AreaId=null;
        $scope.mreportFilter.OutletId=null;
        $scope.mreportFilter.areaId=null;
        
        $scope.mreportFilter.SupervisorId=null;
        $scope.mreportFilter.EmployeeId=null;
        $scope.mreportFilter.VehicleModelId=null;
        
        $scope.listMonth=[];
        
    console.log("=====>>>",rId,$scope.mreportFilter);
    }

    // Get Data
    //----------------------------------
    function b64toBlob(b64Data, contentType) {
        contentType = contentType || '';
        sliceSize = 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
    }

    $scope.getData = function() {
        console.log("param==>",$scope.mreportFilter);
        // if ($scope.mreportFilter.reportId == 1) {
        //     // Report RSSP
        //     arrVisibility[0]=(($scope.mreportFilter.visibility.marketPolreg == 1) ? 1:0);
        //     arrVisibility[1]=(($scope.mreportFilter.visibility.outletShare == 1) ? 1:0);
        //     arrVisibility[2]=(($scope.mreportFilter.visibility.rsTargetMarket == 1) ? 1:0);
        //     arrVisibility[3]=(($scope.mreportFilter.visibility.rsOapRap == 1) ? 1:0);
        //     arrVisibility[4]=(($scope.mreportFilter.visibility.stockBase == 1) ? 1:0);
        //     arrVisibility[5]=(($scope.mreportFilter.visibility.rsSugestion == 1) ? 1:0);
        //     arrVisibility[6]=(($scope.mreportFilter.visibility.rsAdjusment == 1) ? 1:0);
        //     arrVisibility[7]=(($scope.mreportFilter.visibility.wsSugestion == 1) ? 1:0);
        //     arrVisibility[8]=(($scope.mreportFilter.visibility.wsAdjusment == 1) ? 1:0);
        //     arrVisibility[9]=(($scope.mreportFilter.visibility.stockRatio == 1) ? 1:0);
        //     arrVisibility[10]=(($scope.mreportFilter.visibility.os == 1) ? 1:0);
        //     arrVisibility[11]=(($scope.mreportFilter.visibility.cl == 1) ? 1:0);
        //     arrVisibility[12]=(($scope.mreportFilter.visibility.spk == 1) ? 1:0);
        //     arrVisibility[13]=(($scope.mreportFilter.visibility.minMax == 1) ? 1:0);
        //     arrVisibility[14]=(($scope.mreportFilter.visibility.outerSalesContribution == 1) ? 1:0);
        //     arrVisibility[15]=(($scope.mreportFilter.visibility.gapVsTarget == 1) ? 1:0);
        //     arrVisibility[16]=(($scope.mreportFilter.visibility.gapVsTargetMarket == 1) ? 1:0);
        //     arrVisibility[17]=(($scope.mreportFilter.visibility.gapVsTargetOutletShare == 1) ? 1:0);
        //     arrVisibility[18]=(($scope.mreportFilter.visibility.gapVsTargetOutletSalesContribution == 1) ? 1:0);
        //     var ax = 0;
        //     for (var i = 0; i < arrVisibility.length; i++) {
        //         if (arrVisibility[i] == 1) {
        //             ax = 1;
        //             break;
        //         }
        //     };
        //     var xFilter = arrVisibility.toString();
        //     xFilter = xFilter.replace(/,/g, "|");

        //     $scope.mreportFilter.Filter=xFilter;
        //     if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null || $scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null || ax==0) {
        //         var error=[];
        //         if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
        //         if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};    
        //         if (ax == 0) {error.push("Filter Field Masih Kosong");};
        //         bsNotify.show(
        //             {
        //                 size: 'big',
        //                 type: 'danger',
        //                 title: "Ditemukan beberapa data yang salah",
        //                 content: error.join('<br>'),
        //                 number: error.length
        //             }
        //         );  
        //     } else{
        //         $scope.sm_show2=true;
        //         ReportFilter.getRptRssp($scope.mreportFilter).then(
        //             function(res){

        //                 var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
        //                 var b64Data = res.data;

        //                 var blob = b64toBlob(b64Data, contentType);
        //                 var blobUrl = URL.createObjectURL(blob);

        //                 $scope.sm_show2=false;
        //                 saveAs(blob, 'Report_RSSP_Regular' + '.xlsx');

        //                 return res;
        //             },
        //             function(err){
        //                 console.log("err=>",err);
        //             }
        //         );
        //     };
            
        // } 
        // else if($scope.mreportFilter.reportId == 2) {
        //     if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null || $scope.mreportFilter.modelId == '' || $scope.mreportFilter.modelId == null || $scope.mreportFilter.monthId == '' || $scope.mreportFilter.monthId == null) {
        //         var error=[];
                
        //         if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};
        //         if ($scope.mreportFilter.modelId == '' || $scope.mreportFilter.modelId == null) {error.push("Model kosong");};
        //         if ($scope.mreportFilter.monthId == '' || $scope.mreportFilter.monthId == null) {error.push("Month kosong");};
        //         // if ($scope.mreportFilter.allowance != '' || $scope.mreportFilter.allowance != null || $scope.mreportFilter.allowance < 20) {error.push("Allowance Kosong atau Kurang dari 20");};    
        //         bsNotify.show(
        //             {
        //                 size: 'big',
        //                 type: 'danger',
        //                 title: "Ditemukan beberapa data yang salah",
        //                 content: error.join('<br>'),
        //                 number: error.length
        //             }
        //         );  
        //     } 
        //     else{
        //         $scope.sm_show2=true;
        //         ReportFilter.getRptProfiling($scope.mreportFilter).then(
        //             function(res){
        //                 var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
        //                 var b64Data = res.data;

        //                 var blob = b64toBlob(b64Data, contentType);
        //                 var blobUrl = URL.createObjectURL(blob);

        //                 $scope.sm_show2=false;
        //                 saveAs(blob, 'Report_RSSP_Profiling' + '.xlsx');

        //                 return res;
        //             },
        //             function(err){
        //                 console.log("err=>",err);
        //             }
        //         );
        //     }
        // } 
        // else if($scope.mreportFilter.reportId == 3) {
        //     // Report OAP
        //     arrVisibilityOAP[1]=(($scope.mreportFilter.visibility.market == 1) ? 1:0);
        //     arrVisibilityOAP[0]=(($scope.mreportFilter.visibility.retailSales == 1) ? 1:0);
        //     arrVisibilityOAP[2]=(($scope.mreportFilter.visibility.outletShare == 1) ? 1:0);
        //     var xFilter = arrVisibilityOAP.toString();
        //     xFilter = xFilter.replace(/,/g, "|");
        //     $scope.mreportFilter.Filter=xFilter;

        //     if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null || $scope.mreportFilter.xyear == '' || $scope.mreportFilter.xyear == null) {
        //         var error=[];
        //         if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
        //         if ($scope.mreportFilter.xyear == '' || $scope.mreportFilter.xyear == null) {error.push("Tahun Periode kosong");};
        //         bsNotify.show(
        //             {
        //                 size: 'big',
        //                 type: 'danger',
        //                 title: "Ditemukan beberapa data yang salah",
        //                 content: error.join('<br>'),
        //                 number: error.length
        //             }
        //         );  
        //     } else{
        //             $scope.sm_show2=true;
        //             ReportFilter.getRptOAP($scope.mreportFilter).then(
        //                 function(res){
        //                     // console.log("===>",res);
        //                     //with function
        //                     var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
        //                     var b64Data = res.data;

        //                     var blob = b64toBlob(b64Data, contentType);
        //                     var blobUrl = URL.createObjectURL(blob);
        //                     // window.open(blobUrl);
        //                     $scope.sm_show2=false;
        //                     saveAs(blob, 'Report_OAP' + '.xlsx');

        //                     return res;
        //                 },
        //                 function(err){
        //                     console.log("err=>",err);
        //                 }
        //             );
        //     }
        // } 
        // else if($scope.mreportFilter.reportId == 4) {
        //     // Report RAP
        //     arrVisibilityOAP[1]=(($scope.mreportFilter.visibility.market == 1) ? 1:0);
        //     arrVisibilityOAP[0]=(($scope.mreportFilter.visibility.retailSales == 1) ? 1:0);
        //     arrVisibilityOAP[2]=(($scope.mreportFilter.visibility.outletShare == 1) ? 1:0);
        //     var xFilter = arrVisibilityOAP.toString();
        //     xFilter = xFilter.replace(/,/g, "|");
        //     // console.log(arrVisibilityOAP,"xFilter==>",xFilter);
        //     $scope.mreportFilter.Filter=xFilter;

        //     if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null || $scope.mreportFilter.xyear == '' || $scope.mreportFilter.xyear == null) {
        //         var error=[];
        //         if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
        //         if ($scope.mreportFilter.xyear == '' || $scope.mreportFilter.xyear == null) {error.push("Tahun Periode kosong");};
        //         bsNotify.show(
        //             {
        //                 size: 'big',
        //                 type: 'danger',
        //                 title: "Ditemukan beberapa data yang salah",
        //                 content: error.join('<br>'),
        //                 number: error.length
        //             }
        //         );  
        //     } else{
        //             $scope.sm_show2=true;
        //             ReportFilter.getRptRAP($scope.mreportFilter).then(
        //                 function(res){
        //                     // console.log("===>",res);
        //                     //with function
        //                     var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
        //                     var b64Data = res.data;

        //                     var blob = b64toBlob(b64Data, contentType);
        //                     var blobUrl = URL.createObjectURL(blob);
        //                     // window.open(blobUrl);
        //                     $scope.sm_show2=false;
        //                     saveAs(blob, 'Report_RAP' + '.xlsx');

        //                     return res;
        //                 },
        //                 function(err){
        //                     console.log("err=>",err);
        //                 }
        //             );
        //     }
        // } 
        // else if($scope.mreportFilter.reportId == 5) {
        //     // Report RSSP Fleet
        //     if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null || $scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null ) {
        //         var error=[];
        //         if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
        //         if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};    
        //         bsNotify.show(
        //             {
        //                 size: 'big',
        //                 type: 'danger',
        //                 title: "Ditemukan beberapa data yang salah",
        //                 content: error.join('<br>'),
        //                 number: error.length
        //             }
        //         );  
        //     } else{
        //             $scope.sm_show2=true;
        //             ReportFilter.getRptFleet($scope.mreportFilter).then(
        //                 function(res){
        //                     // console.log("===>",res);
        //                     //with function
        //                     var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
        //                     var b64Data = res.data;

        //                     var blob = b64toBlob(b64Data, contentType);
        //                     var blobUrl = URL.createObjectURL(blob);
        //                     // window.open(blobUrl);
        //                     $scope.sm_show2=false;
        //                     saveAs(blob, 'Report_RSSP_Fleet' + '.xlsx');

        //                     return res;
        //                 },
        //                 function(err){
        //                     console.log("err=>",err);
        //                 }
        //             );
        //     }
        // } 
            // Report RSSP Internal Order
            // if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null ) {
            //     var error=[];
            //     if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};    
            //     bsNotify.show(
            //         {
            //             size: 'big',
            //             type: 'danger',
            //             title: "Ditemukan beberapa data yang salah",
            //             content: error.join('<br>'),
            //             number: error.length
            //         }
            //     );  
            // } 
            // else{
                    // $scope.sm_show2=true;
                    // ReportAdmin.getRptAdmin($scope.mreportFilter).then(
                    //     function(res){
                    //         var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                    //         var b64Data = res.data;

                    //         var blob = b64toBlob(b64Data, contentType);
                    //         var blobUrl = URL.createObjectURL(blob);
                    //         $scope.sm_show2=false;
                    //         saveAs(blob, 'Report_RSSP_Internal_Order' + '.xlsx');

                    //         return res;
                    //     },
                    //     function(err){
                    //         console.log("err=>",err);
                    //     }
                    // );
            // }

            // Report Achievement Evaluation
            // if ( $scope.mreportFilter.DateFrom == '' || $scope.mreportFilter.DateFrom == null || $scope.mreportFilter.DateTo == '' || $scope.mreportFilter.DateTo == null ) {
            //     var error=[];
            //     if ($scope.mreportFilter.DateFrom == '' || $scope.mreportFilter.DateFrom == null) {error.push("Date Start kosong");};    
            //     if ($scope.mreportFilter.DateTo == '' || $scope.mreportFilter.DateTo == null) {error.push("Date End kosong");};  
                  
            //     bsNotify.show(
            //         {
            //             size: 'big',
            //             type: 'danger',
            //             title: "Ditemukan beberapa data yang salah",
            //             content: error.join('<br>'),
            //             number: error.length
            //         }
            //     );  
            // } 
            // else{
                $scope.sm_show2=true;
                ReportSales.getRptAdmin($scope.mreportFilter).then(
                    function(res){
                        var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                        var b64Data = res.data;

                        var blob = b64toBlob(b64Data, contentType);
                        var blobUrl = URL.createObjectURL(blob);
                        $scope.sm_show2=false;
                        saveAs(blob, 'Report_Achievement_Evaluation' + '.xlsx');
                        return res;
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
            // }
    }    
});
