angular.module('app')
  .factory('ReportSales', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getRptAdmin: function(xData){
        var xparam='';
        
        if (xData.GroupDealerId != '' && xData.GroupDealerId != undefined) {
          xparam = xparam +'&GroupDealerId='+xData.GroupDealerId;
        };
        if (xData.AreaId != '' && xData.AreaId != undefined) {
          xparam = xparam + '&AreaId='+xData.AreaId;
        };
        if (xData.OutletId != '' && xData.OutletId != undefined) {
          xparam = xparam + '&OutletId='+xData.OutletId;
        }
        if (xData.SupervisorId != '' && xData.SupervisorId != undefined) {
          xparam = xparam + '&SupervisorId='+xData.SupervisorId;
        };
        if (xData.EmployeeId != '' && xData.EmployeeId != undefined) {
          xparam = xparam + '&EmployeeId='+xData.EmployeeId;
        };
        if (xData.VehicleModelId != '' && xData.VehicleModelId != undefined) {
          xparam = xparam + '&VehicleModelId='+xData.VehicleModelId;
        };

        var d = new Date();
        var datestring = d.getFullYear() + "-" +("0"+(d.getMonth()+1)).slice(-2) + "-" +("0" + d.getDate()).slice(-2) + " T00:00:00";
        var datestringE = d.getFullYear() + "-" +("0"+(d.getMonth()+1)).slice(-2) + "-" +("0" + d.getDate()).slice(-2) + " T00:00:00";
        //var datestringE = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2);
        xData.DateFrom = datestring;  
        xData.DateTo = datestringE;  

        console.log(" Sales ===>",'/api/rpt/ReportSales?'+xparam);
        var res =  $http.get('/api/rpt/ReportSales?'+'DateFrom='+xData.DateFrom+'&DateTo='+xData.DateTo+xparam ); // , { responseType: 'arraybuffer' }
        return res;
      },
      
      getDataDealer: function() {
          var res=$http.get('/api/sales/MSitesGroupDealer');
          return res;
      },

      getData: function(xfilter) {
        console.log("get data ===>",xfilter);
        var res = 'CQAGAAAAAAAAAAAAAAABAAAAAQAAAAAAAAAAEAAA/';
        return res;
      }
    }
});