angular.module('app')
    .controller('ReportSummarySalesController', function($scope, $http, CurrentUser,ReportSummarySales,bsNotify, Region,GetsudoPeriod, OrgChart,OAPPeriod, ProductModel,$timeout) {
        
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
         // $scope.showPreview = true;
        
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mreportFilter= {reportId:'',islandId:'',provinceId:'',cityId:'',areaId:'',dealerId:'',outletId:'',outletIdInternal:'',modelId:'',typeId:'',colourId:'',xyear:'',GetsudoPeriodId:'',date1:'',date2:'',dateS:'',dateE:'',allowance:'',Filter:'',outletHO:null,dealerHO:null,monthId:null}; //Model
    $scope.mreportFilter.visibility= {
        marketPolreg:0,
        rsSugestion:0,
        stockBase:0,
        cl:0,
        outletShare:0,
        rsAdjusment:0,
        stockRatio:0,
        market:0,
        rsTargetMarket:0,
        wsSugestion:0,
        os:0,
        retailSales:0,
        rsOapRap:0,
        wsAdjusment:0,
        spk:0,
        outletShare:0,
        gapVsTarget:0,
        gapVsTargetMarket:0,
        gapVsTargetOutletShare:0,
        minMax:0,
        gapVsOutletContribution:0,
        outerSalesContribution:0,
        gapVsTargetOutletSalesContribution:0
    }; //Model

    // var outletData = [];

    var outletObj = null;
    var outletData = [];
    var outletDataInt=[];
    var areaProvince = [];
    var provinceCityData = [];
    var orgTreeData = [];
    var orgTreeDataInt = [];
    $scope.areaData = [];
    $scope.provinceData = [];
    $scope.orgData= [];
    $scope.olYear = [];

    $scope.checkRights=function(bit){
        console.log('$scope.myRights',$scope.myRights);
        var p=$scope.myRights & Math.pow(2,bit);
        var res=(p==Math.pow(2,bit));
        return res;
    }

    var arrVisibility = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var arrVisibilityOAP = [0,0,0];
    
    // var dateFormat='dd/MM/yyyy';
    var dateFormat='MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        minMode:"month",
        // mode:"'month'",
        disableWeekend: 1
    };
    $scope.endDateOptions = {
        startingDay: 1,
        format: dateFormat,
        minMode:"month",
        // mode:"'month'",
        disableWeekend: 1
    };
    $scope.startDate = function(dt){
        $scope.endDateOptions.minDate = dt;
        var d = new Date(dt);
        // var datestring = d.getFullYear() + "-" +("0"+(d.getMonth()+1)).slice(-2) + "-" +("0" + d.getDate()).slice(-2) + " T00:00:00";
        var datestring = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2);
        $scope.mreportFilter.dateS = datestring;
        console.log("start==>",$scope.mreportFilter.dateS);
    }
    $scope.endDate = function(dt){
        var d = new Date(dt);
        // var datestringE = d.getFullYear() + "-" +("0"+(d.getMonth()+1)).slice(-2) + "-" +("0" + d.getDate()).slice(-2) + " T00:00:00";
        var datestringE = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2);
        $scope.mreportFilter.dateE = datestringE;  
        console.log("End==>",$scope.mreportFilter.dateE); 
    }
   
    //----------------------------------

    $scope.onSelectRows = function(rows){
        // console.log("onSelectRows=>",rows);
    }
    
    var xRep = [];
    
    // $timeout(function(){
    //     if($scope.checkRights(11)){
    //         // console.log('masuk11');
    //         // $scope.report.concat([
    //             xRep.push({id:1, name:"- Report RSSP"},
    //                     {id:2, name:"- RSSP Profiling"},
    //                     {id:6, name:"- RSSP Internal Order"},                    
    //                     {id:7, name:"- Achievement Evaluation"});
    //     }
    //     // console.log("reeep==>",xRep);
    //     if($scope.checkRights(12)){
    //         // console.log('masuk12');
    //         // $scope.report.concat([
    //             xRep.push({id:3, name:"- Report OAP"},{id:4, name:"- Report RAP"});
    //     }
    //     if($scope.checkRights(13)){
    //         // console.log('masuk13');
    //         // $scope.report.concat([
    //             xRep.push({id:5, name:"- RSSP Fleet"});
    //     }

    //     // console.log("reeep==>",xRep);
    //     $scope.report = xRep;
    // })
    $timeout(function(){
        if($scope.checkRights(11)){
                xRep.push({id:1, name:"- Report RSSP Regular"});
        }
        if($scope.checkRights(12)){
                xRep.push({id:5, name:"- Report RSSP Fleet"});
        }
        if($scope.checkRights(13)){
                xRep.push({id:2, name:"- Report RSSP Profiling"});
        }
        if($scope.checkRights(14)){
                xRep.push({id:3, name:"- Report OAP"},{id:4, name:"- Report RAP"});
        }
        if($scope.checkRights(15)){
                xRep.push({id:6, name:"- Report RSSP Internal Order"});
        }
        if($scope.checkRights(16)){
                xRep.push({id:7, name:"- Report Achievement Evaluation"});
        }
        console.log("reeep==>",xRep);
        $scope.report = xRep;
    })
    OAPPeriod.getData().then(
        function(res){
            // $scope.yearData = res.data.Result;
            var temp = res.data.Result;
            $scope.olYear = [];
            var obj={};
            for (var i in temp) {
                if (!obj[temp[i].Year]) {
                    obj[temp[i].Year] = {};
                    obj[temp[i].Year]["year"] = temp[i].Year;
                    obj[temp[i].Year]["id"] = temp[i].Year;
                    $scope.olYear.push(obj[temp[i].Year]);
                }                     
            }
            
            // console.log("$scope.yearData : "+JSON.stringify($scope.olYear));
            $scope.loading=false;
            return res.data;
        }
    )
    // $scope.report = [{id:1, name:"- Report RSSP"},
    //                 {id:2, name:"- RSSP Profiling"},
    //                 {id:3, name:"- Report OAP"},
    //                 {id:4, name:"- Report RAP"},
    //                 {id:5, name:"- RSSP Fleet"},
    //                 {id:6, name:"- RSSP Internal Order"},
    //                 {id:7, name:"- Achievement Evaluation"}];    

    // Region.getDataTree().then(
    //     function(res){
    //         // console.log("Region tree===>",res);
    //         // console.log("Area=>",res.data);
    //                 var xd1 = res.data.Result;
    //                 var xres= [];
    //                 for (var i = 0; i < xd1.length; i++) {
    //                     for (var j = 0; j < xd1[i].child.length; j++) {

    //                         xres.push(xd1[i].child[j]);
    //                     };    
    //                 };
    //                 // console.log("region===>",xres);
    //                 $scope.listRegion = xres;
    //         // $scope.listRegion = res.data.Result;
    //         if($scope.listRegion.length==1){
    //             $scope.mreportFilter.orgId = $scope.listRegion[0].id;
    //         }
    //         $scope.loading=false;
    //         return res.data;
    //     }
    // );
    // Region.getDataArea().then(
    //             function(res){
    //                 console.log("Area=>",res.data);
    //                 // $scope.areaData = xres;
    //                 // $scope.areaData = res.data.Result;
    //             },
    //             function(err){
    //                 console.log("err=>",err);
    //             }
    // );
    // Region.getAreaProvince().then(
    //     function(res){
    //         areaProvince = res.data.Result;
    //         console.log("areaprovince===>",areaProvince);
    //         filterArea();
    //         filterProvince();
    //         $scope.loading=false;
    //     }
    // );
    
    OrgChart.getDataTree().then(
        
        function(res){
            var otint={};
            orgTreeData = res.data.Result;
            $scope.orgData = res.data.Result;
            for(i in $scope.orgData){
                for(ch in $scope.orgData[i].child){
                    outletData.push($scope.orgData[i].child[ch]);
                    if($scope.orgData[i].child[ch].OutletId < 0){
                        // orgTreeDataInt.push($scope.orgData[i]);
                        otint = $scope.orgData[i];
                        outletDataInt.push($scope.orgData[i].child[ch]);
                    }
                }

            }
            otint.child=outletDataInt;
            orgTreeDataInt.push(otint)
            $scope.orgDataInternal=orgTreeDataInt;
            console.log("Outlet Internal==>",orgTreeDataInt);
            if($scope.orgData.length==1){
                if ($scope.orgData[0].child.length > 1) {
                    console.log("HO",$scope.orgData[0]);
		    $scope.mreportFilter.outletHO = $scope.orgData[0].HeadOfficeId;
            	    $scope.mreportFilter.dealerHO = $scope.orgData[0].GroupDealerId;
                    // $scope.mreportFilter.dealerId = $scope.orgData[0].GroupDealerId;
                 //   $scope.mreportFilter.dealerId = $scope.orgData[0].id;
                } else{
                    console.log("BRANCH");
                    $scope.mreportFilter.outletId = $scope.orgData[0].child[0].id;
                    $scope.mreportFilter.outletIdInternal = $scope.orgData[0].child[0].id;
                          
                };
                
                //$scope.selectOutlet()
            }

            Region.getAreaProvince().then(
                function(res){
                    var areaProvRes = res.data.Result;
                    for(var i in areaProvRes){
                        var filteredOutlet = _.find(outletData, function(o){
                            return o.AreaId.indexOf(areaProvRes[i].AreaId)>=0 && o.ProvinceId==areaProvRes[i].ProvinceId;
                        });
                        if(filteredOutlet != null)
                            areaProvince.push(areaProvRes[i]);
                    }

                    filterArea();
                    filterProvince();

                    $scope.loading=false;
                }
            );
            $scope.loading=false;
            return res.data;
        }
    );

    ProductModel.getDataTree().then(
        function(res){
            // console.log("Product==>",res);
            $scope.productModelData = res.data.Result;
            $scope.loading=false;
            return res.data;
        }
    );
    GetsudoPeriod.getData().then(
            function(res){
                // console.log("dataSudoPeriod=>",res.data);
                $scope.getSudoPrdData=res.data.Result;
                // var lYear=[];

                // var xd = res.data.Result;
                // var temp={};
                // var j=0;
                // for (var i = 0; i < xd.length; i++) {
                //     var d = new Date(xd[i].StartPeriod);
                //     var dYear = d.getFullYear();
                //     if (!temp[dYear]) {
                //         var cl={};
                //         cl.id=dYear;
                //         cl.year=dYear;
                //         lYear.push(cl)
                //         temp[dYear]=dYear;
                //         //j++
                //         console.log("temp==>",temp);
                //     }
                // };
                // $scope.olYear = lYear;
                // consol.log("listYear==>",$scope.lYear);
                $scope.loading=false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    $scope.selectPeriod = function(period){
        $scope.listMonth = [];
        var xlistMonth = [];
        var month=["January","February","March","April","May","June","July","August","September","October","November","December"];
        // console.log("select period==>",period);
        var dr1 = new Date(period.StartPeriod);
        
        for (var i = 1; i < 6; i++) {
            var cs = {};
            dr1.setMonth(dr1.getMonth() + 1);

            cs.id=dr1.getFullYear()+""+("0"+(dr1.getMonth()+1)).slice(-2);
            cs.tx=month[dr1.getMonth()] + " " + dr1.getFullYear();
        //    console.log("select period + 8==>",dr1);
        //    console.log("select period + 8==>",cs);

            xlistMonth.push(cs);
        };
        $scope.listMonth = xlistMonth;
        console.log("list Month==>",$scope.listMonth);
    }

    // auto refill
    

    function getAreaProvinceFromHO(HO){
        $scope.provinceData = [];
        $scope.areaData = [];
        var dataSrc = HO.child;
        for(var i in dataSrc){
            var provinceObj = _.find(areaProvince,{ProvinceId: dataSrc[i].ProvinceId});
            var newObj = {ProvinceId:provinceObj.ProvinceId, ProvinceName:provinceObj.ProvinceName};
            if(_.findIndex($scope.provinceData, newObj)<0)
                $scope.provinceData.push(newObj);
            $scope.provinceData = _.sortBy($scope.provinceData, ['ProvinceName']);

            var areaObj = _.find(areaProvince,function(o){
                return dataSrc[i].AreaId.indexOf(o.AreaId)>=0;
            });
            var newObj = {AreaId:areaObj.AreaId, Description:areaObj.Description};
            if(_.findIndex($scope.areaData, newObj)<0)
                $scope.areaData.push(newObj);
            $scope.areaData = _.sortBy($scope.areaData, ['Description']);
        }
    }
    function filterArray(data,filter){
        var res = [];
        // for(var i in data){
        //     var valid=true;
        //     for(var key in filter){
        //         // console.log(data[i][key],"key==>",key,"====>",filter[key][key]);
        //         if(data[i][key]!=filter[key][key]){
        //             valid=false; break;
        //         }
        //     }
        //     if(valid) res.push(data[i]);
        // }
        for(var i in data){
            var valid=true;
            for(var key in filter){
                if(filter[key]==null || filter[key]==undefined) continue;
                if(Array.isArray(data[i][key]) && !Array.isArray(filter[key])){
                    if(data[i][key].indexOf(filter[key])<0){
                        valid=false; break;
                    }
                }else if(!Array.isArray(data[i][key]) && Array.isArray(filter[key])){
                    if(filter[key].indexOf(data[i][key])<0){
                        valid=false; break;
                    }
                }else if(data[i][key]!=filter[key]){
                    valid=false; break;
                }
            }
            if(valid) res.push(data[i]);
        }
        return res;
    }
    var noAuto = false;
    var xno = false;
    var xc = 2;
    function filterArea(filter){
            if(xno){filter=null};
        $scope.areaData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var areaObj = {AreaId: dataSrc[i].AreaId, Description: dataSrc[i].Description};
            if(_.findIndex($scope.areaData, areaObj)<0)
                $scope.areaData.push(areaObj);
        }
        if($scope.areaData.length==1){
            // console.log("areaData=>",$scope.areaData);
            $scope.mreportFilter.areaId = $scope.areaData[0].AreaId;
        }
        // console.log("areaData=>",$scope.areaData);
    }
    function filterProvince(filter){
        if(xno){filter=null};
        // console.log("filter province==>",filter);
        $scope.provinceData = [];
        var dataSrc = [];
        if(filter==undefined || filter==null){
            dataSrc = areaProvince;
        }else{
            dataSrc = filterArray(areaProvince, filter);
        }
        for(var i=0; i<dataSrc.length; i++){
            var provinceObj = {ProvinceId: dataSrc[i].ProvinceId, ProvinceName: dataSrc[i].ProvinceName};
            if(_.findIndex($scope.provinceData, provinceObj)<0)
                $scope.provinceData.push(provinceObj);
        }
        if($scope.provinceData.length==1){
            // console.log("provinceData=>",$scope.provinceData);
            $scope.mreportFilter.provinceId = $scope.provinceData[0].ProvinceId;
        }
        
        console.log("prov",xc);
        // if(xc>=1){xno=false;};
        // xc=xc+1;
        xno=false;
        
        // console.log("provinceData=>",$scope.provinceData);
    }
    function filterDealer(filter){
        $scope.orgData = [];
        var dataSrc = [];
        var xdataSrc = [];
        // console.log(orgTreeData,'filterDealer=',filter);
        // console.log(xno,"filter dealer==>",filter);
        if(xno){filter=null};
        // console.log("filter dealer2==>",filter);
        if(filter==undefined || filter==null){
            dataSrc = orgTreeData;
            
        }else{
            for(var i in orgTreeData){
                var inserted = false;
                for(var chIdx in orgTreeData[i].child){
                    var valid = true;
                    for(key in filter){
                        if(filter[key]==null || filter[key]==undefined) continue;
                        if(Array.isArray(orgTreeData[i].child[chIdx][key])){
                            if(orgTreeData[i].child[chIdx][key].indexOf(filter[key])<0){
                                valid=false; break;
                            }
                        }else if(orgTreeData[i].child[chIdx][key]!=filter[key]){
                            valid=false; break;
                        }
                    }
                    if(valid){
                        if(!inserted){
                            var parent = angular.copy(orgTreeData[i]);
                            parent.child = [];
                            parent.listOutlet = [];
                            dataSrc.push(parent);
                            inserted=true;
                        }
                        dataSrc[dataSrc.length-1].child.push(orgTreeData[i].child[chIdx]);
                    }
                    
                }
            }
        }
        // if ($scope.mreportFilter.reportId == 6) {
        //         for (var i = dataSrc.length - 1; i > -1; i--) {
        //             console.log("dealer==>",dataSrc[i].GroupDealerCode);
        //             if (dataSrc[i].GroupDealerCode == "TAM") {
        //                 xdataSrc = dataSrc[i];        
        //             } 
        //         };
                
        //     } 
        $scope.orgData = dataSrc;
        // console.log('$scope.orgData',$scope.orgData);

    }
    
    var outletSel;
    $scope.changeFilterCombo = function(selected,sender){
        if (($scope.mreportFilter.areaId==null || $scope.mreportFilter.areaId=='') && ($scope.mreportFilter.provinceId==null || $scope.mreportFilter.provinceId=='')) {
            outletObj = null;    
        } 
        
        console.log("filterCombo==>",selected);
        $timeout(function(){
            if(sender.name=='outlet' || sender.name=='outlet2'){
                outletSel = selected;
                if($scope.mreportFilter.outletId!=null && $scope.mreportFilter.outletId!=undefined){
                    $scope.mreportFilter.dealerId=selected.GroupDealerId;
                    if(selected!=null && ('HeadOfficeId' in selected)){
                            getAreaProvinceFromHO(selected);
                            outletObj = _.find(outletData,{OutletId:selected.HeadOfficeId});
                            return;
                        }
                    // else if(selected!=null){
                    //                         outletObj = _.find(outletData,{OutletId:$scope.mreportFilter.outletId});}
                    // else
                    //     {outletObj = null;}
                    else if(selected!=null)
                        {
                            outletObj = _.find(outletData,{OutletId:$scope.mreportFilter.outletId});
                            if(selected.OutletId==selected.parent.HeadOfficeId){
                                var parentObj = _.find(orgTreeData,{HeadOfficeId:selected.OutletId});
                                getAreaProvinceFromHO(parentObj);
                                return;
                            }
                        }
                    else
                        outletObj = null;
                }else{
                    outletObj = null;
                    $scope.mreportFilter.dealerId=null;
                }
                
            }else if(outletObj==null || outletObj.parent.HeadOfficeId!=outletObj.OutletId){
                // 
                filterDealer({ProvinceId:$scope.mreportFilter.provinceId, AreaId:$scope.mreportFilter.areaId});

            }
            if(sender.name!='province'){
                if($scope.mreportFilter.areaId!=null && $scope.mreportFilter.areaId!=undefined && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterProvince({AreaId:$scope.mreportFilter.areaId, ProvinceId:outletObj.ProvinceId});
                else if(($scope.mreportFilter.areaId==null || $scope.mreportFilter.areaId==undefined) && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterProvince({AreaId:outletObj.AreaId, ProvinceId:outletObj.ProvinceId});
                else{
                    filterProvince({AreaId:$scope.mreportFilter.areaId});
                    }
               
            }

            if(sender.name!='area'){
                if($scope.mreportFilter.provinceId!=null && $scope.mreportFilter.provinceId!=undefined && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterArea({ProvinceId:$scope.mreportFilter.provinceId, AreaId:outletObj.AreaId});
                else if(($scope.mreportFilter.provinceId==null || $scope.mreportFilter.provinceId==undefined) && outletObj!=null && (outletObj.OutletId != outletObj.parent.HeadOfficeId))
                    filterArea({ProvinceId:outletObj.ProvinceId, AreaId:outletObj.AreaId});
                else{
                    filterArea({ProvinceId:$scope.mreportFilter.provinceId});
                }
               
            }
            // filterDealer({ProvinceId:$scope.mreportFilter.provinceId, AreaId:$scope.mreportFilter.areaId});
        })
    }
    // $scope.changeArea = function(selected){
    //     if(selected==null ){
    //         // console.log("selected==null");
    //         filterProvince();
    //         filterDealer($scope.mreportFilter.provinceId==null || $scope.mreportFilter.provinceId==""?null:{ProvinceId:$scope.mreportFilter.provinceId});
    //     }else{
    //         // console.log("selected!==null");
    //         filterProvince({AreaId:selected});
    //         filterDealer($scope.mreportFilter.provinceId==null || $scope.mreportFilter.provinceId==""?{AreaId:selected}:{AreaId:selected,ProvinceId:$scope.mreportFilter.provinceId});
    //     }
    // }
    // $scope.changeProvince = function(selected){
    //     if(selected==null){
    //         // console.log("selected==null");
    //         filterArea();
    //         filterDealer($scope.mreportFilter.areaId==null || $scope.mreportFilter.areaId==""?null:{AreaId:$scope.mreportFilter.areaId});
    //     }else{
    //         // console.log("selected!==null",selected.ProvinceId);
    //         // selected.provinceId;
    //         var xp = $scope.listRegion;
    //         for (var i = 0; i < xp.length; i++) {
    //             // console.log("check===>",xp[i].ProvinceId,selected.ProvinceId);
    //             if (xp[i].ProvinceId == selected.ProvinceId) {
    //                 $scope.lcity=xp[i].child;
    //                 break;
    //             }
    //         };
            
    //         // console.log("selected",selected);
    //         filterArea({ProvinceId:selected});
    //         filterDealer($scope.mreportFilter.areaId==null || $scope.mreportFilter.areaId==""?{ProvinceId:selected}:{ProvinceId:selected,AreaId:{AreaId:$scope.mreportFilter.areaId}});
    //     }
    // }

    $scope.selectOutletInternal = function(selected){
        // outletSel = selected;
        console.log("outlet==>",selected);
        if(selected!=null){
            if (selected.level == 1) {
                console.log("dealer==>",selected.GroupDealerId,"HO==>",selected.HeadOfficeId);
                $scope.mreportFilter.dealerId = selected.GroupDealerId;
                $scope.mreportFilter.outletIdInternal = selected.HeadOfficeId;
            } else{
                console.log("dealer==>",selected.GroupDealerId,"outlet==>",selected.OutletId);
                $scope.mreportFilter.dealerId = selected.GroupDealerId;
            };
            // outletObj = _.find(outletData,{OutletId:selected.id});
            // $scope.mreportFilter.areaId = outletObj.AreaId[0];
            // $scope.mreportFilter.provinceId = outletObj.ProvinceId;
            // $scope.mreportFilter.dealerId = outletObj.GroupDealerId;


        }
    };
    // $scope.selectRegion = function(xRegion){
    //     // console.log("select region==>",xRegion);
    //     $scope.mreportFilter.provinceId=xRegion.ProvinceId;
    //     $scope.lcity = xRegion.child;
    //     // if (xRegion.level == 1) {
    //     //     $scope.mreportFilter.islandId=xRegion.IslandId;
    //     // } else if(xRegion.level == 2){
    //     //     $scope.mreportFilter.provinceId=xRegion.ProvinceId;
    //     //     $scope.mreportFilter.cityId='';
    //     // } else if(xRegion.level == 3){
    //     //     $scope.mreportFilter.provinceId=xRegion.ProvinceId;
    //     //     $scope.mreportFilter.cityId=xRegion.CityRegencyId;
    //     // };

    //     // $scope.outletData = xDealer.child;
    // }
    // $scope.selectCity = function(xType){
    //     $scope.mreportFilter.cityId=xType.CityRegencyId;
    // }
    // $scope.selectDealer = function(xDealer){
    //     // console.log("select dealeer==>",xDealer);
    //     $scope.outletData = xDealer.child;
    // }

    $scope.selectProduct = function(xProduct){
        $scope.mreportFilter.modelId=xProduct.VehicleModelId;
        $scope.productTypeData = xProduct.child;
        // console.log("select dealeer==>",xProduct);
        // if (xProduct.level == 1) {
        //     $scope.mreportFilter.modelId=xProduct.VehicleModelId;
        // } else if(xProduct.level == 2){
        //     $scope.mreportFilter.typeId=xProduct.VehicleTypeId;
        //     $scope.mreportFilter.modelId=xProduct.VehicleModelId;
        //     console.log("colour==>",xProduct.listColor);
        //     $scope.productColorModel = xProduct.listColor;
        // } else if(xProduct.level == 3){
        //     $scope.mreportFilter.colourId=xProduct.ColorId;
        // };
        // $scope.outletData = xDealer.child;
    }

    $scope.selectType = function(xType){
        $scope.mreportFilter.typeId=xType.VehicleTypeId;
        $scope.productColorModel = xType.child;
    }
    var coloum=[];
    // ng-if = "variabel == ?"

    $scope.selectReport = function(rId){
            
            xc=0;
            xno=true;
            $scope.mreportFilter.islandId=null;
            
            $scope.mreportFilter.cityId=null;
            $scope.mreportFilter.outletId=null; 
            $scope.mreportFilter.outletIdInternal=null;
            $scope.mreportFilter.dealerId=null;
            $scope.mreportFilter.provinceId=null;
            $scope.mreportFilter.areaId=null;
            
            $scope.mreportFilter.modelId=null;
            $scope.mreportFilter.typeId=null;
            $scope.mreportFilter.colourId=null;
            $scope.mreportFilter.xyear=null;
            $scope.mreportFilter.GetsudoPeriodId=null;
            $scope.mreportFilter.date1=null;
            $scope.mreportFilter.date2=null;
            $scope.mreportFilter.dateS=null;
            $scope.mreportFilter.dateE=null;
            $scope.mreportFilter.allowance=null;
            $scope.mreportFilter.Filter=null;
            $scope.mreportFilter.monthId=null;
            

            $scope.listMonth=[];
            
        console.log("=====>>>",rId,$scope.mreportFilter);
        
     
    }
    // Get Data
    //----------------------------------
    function b64toBlob(b64Data, contentType) {
          contentType = contentType || '';
          sliceSize = 512;

          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
          }
            
          var blob = new Blob(byteArrays, {type: contentType});
          return blob;
        }

    $scope.getData = function() {
        console.log("param==>",$scope.mreportFilter);
        if ($scope.mreportFilter.reportId == 1) {
            // Report RSSP
            arrVisibility[0]=(($scope.mreportFilter.visibility.marketPolreg == 1) ? 1:0);
            arrVisibility[1]=(($scope.mreportFilter.visibility.outletShare == 1) ? 1:0);
            arrVisibility[2]=(($scope.mreportFilter.visibility.rsTargetMarket == 1) ? 1:0);
            arrVisibility[3]=(($scope.mreportFilter.visibility.rsOapRap == 1) ? 1:0);
            arrVisibility[4]=(($scope.mreportFilter.visibility.stockBase == 1) ? 1:0);
            arrVisibility[5]=(($scope.mreportFilter.visibility.rsSugestion == 1) ? 1:0);
            arrVisibility[6]=(($scope.mreportFilter.visibility.rsAdjusment == 1) ? 1:0);
            arrVisibility[7]=(($scope.mreportFilter.visibility.wsSugestion == 1) ? 1:0);
            arrVisibility[8]=(($scope.mreportFilter.visibility.wsAdjusment == 1) ? 1:0);
            arrVisibility[9]=(($scope.mreportFilter.visibility.stockRatio == 1) ? 1:0);
            arrVisibility[10]=(($scope.mreportFilter.visibility.os == 1) ? 1:0);
            arrVisibility[11]=(($scope.mreportFilter.visibility.cl == 1) ? 1:0);
            arrVisibility[12]=(($scope.mreportFilter.visibility.spk == 1) ? 1:0);
            arrVisibility[13]=(($scope.mreportFilter.visibility.minMax == 1) ? 1:0);
            arrVisibility[14]=(($scope.mreportFilter.visibility.outerSalesContribution == 1) ? 1:0);
            arrVisibility[15]=(($scope.mreportFilter.visibility.gapVsTarget == 1) ? 1:0);
            arrVisibility[16]=(($scope.mreportFilter.visibility.gapVsTargetMarket == 1) ? 1:0);
            arrVisibility[17]=(($scope.mreportFilter.visibility.gapVsTargetOutletShare == 1) ? 1:0);
            arrVisibility[18]=(($scope.mreportFilter.visibility.gapVsTargetOutletSalesContribution == 1) ? 1:0);
            var ax = 0;
            for (var i = 0; i < arrVisibility.length; i++) {
                if (arrVisibility[i] == 1) {
                    ax = 1;
                    break;
                }
            };
            var xFilter = arrVisibility.toString();
            xFilter = xFilter.replace(/,/g, "|");
            // console.log(arrVisibility,"xFilter==>",xFilter);
            $scope.mreportFilter.Filter=xFilter;
            if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null || $scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null || ax==0) {
                var error=[];
                if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
                if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};    
                if (ax == 0) {error.push("Filter Field Masih Kosong");};
                bsNotify.show(
                    {
                        size: 'big',
                        type: 'danger',
                        title: "Ditemukan beberapa data yang salah",
                        content: error.join('<br>'),
                        number: error.length
                    }
                );  
            } else{
                $scope.sm_show2=true;
                ReportFilter.getRptRssp($scope.mreportFilter).then(
                    function(res){
                        // console.log("===>",res);
                        //with function
                        var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                        var b64Data = res.data;

                        var blob = b64toBlob(b64Data, contentType);
                        var blobUrl = URL.createObjectURL(blob);
                        // window.open(blobUrl);
                        $scope.sm_show2=false;
                        saveAs(blob, 'Report_RSSP_Regular' + '.xlsx');


                        return res;
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
            };
            
        } else if($scope.mreportFilter.reportId == 2) {
            
            // Report RSSP Profiling
            // monthId areaId=1&vehiclemodelId=28&provinceId=10&dealeId=1&GetsudoPeriodId=5&outletId=53&allowance=1 $scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null ||  || $scope.mreportFilter.allowance < 20 || $scope.mreportFilter.allowance == '' || $scope.mreportFilter.allowance == null
            if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null || $scope.mreportFilter.modelId == '' || $scope.mreportFilter.modelId == null || $scope.mreportFilter.monthId == '' || $scope.mreportFilter.monthId == null) {
                var error=[];
                
                if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};
                if ($scope.mreportFilter.modelId == '' || $scope.mreportFilter.modelId == null) {error.push("Model kosong");};
                if ($scope.mreportFilter.monthId == '' || $scope.mreportFilter.monthId == null) {error.push("Month kosong");};
                // if ($scope.mreportFilter.allowance != '' || $scope.mreportFilter.allowance != null || $scope.mreportFilter.allowance < 20) {error.push("Allowance Kosong atau Kurang dari 20");};    
                bsNotify.show(
                    {
                        size: 'big',
                        type: 'danger',
                        title: "Ditemukan beberapa data yang salah",
                        content: error.join('<br>'),
                        number: error.length
                    }
                );  
            } else{
                    $scope.sm_show2=true;
                    ReportFilter.getRptProfiling($scope.mreportFilter).then(
                        function(res){
                            // console.log("===>",res);
                            //with function
                            var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                            var b64Data = res.data;

                            var blob = b64toBlob(b64Data, contentType);
                            var blobUrl = URL.createObjectURL(blob);
                            // window.open(blobUrl);
                            // XLSXReaderService.readFile(blob, $scope.showPreview).then(function(xlsxData) {
                            //     $scope.sheets = xlsxData.sheets;
                            // });
                            // $scope.sm_show2=true;
                            $scope.sm_show2=false;
                            saveAs(blob, 'Report_RSSP_Profiling' + '.xlsx');

                            return res;
                        },
                        function(err){
                            console.log("err=>",err);
                        }
                    );
            }
        } else if($scope.mreportFilter.reportId == 3) {
            // Report OAP
            arrVisibilityOAP[1]=(($scope.mreportFilter.visibility.market == 1) ? 1:0);
            arrVisibilityOAP[0]=(($scope.mreportFilter.visibility.retailSales == 1) ? 1:0);
            arrVisibilityOAP[2]=(($scope.mreportFilter.visibility.outletShare == 1) ? 1:0);
            var xFilter = arrVisibilityOAP.toString();
            xFilter = xFilter.replace(/,/g, "|");
            // console.log(arrVisibilityOAP,"xFilter==>",xFilter);
            $scope.mreportFilter.Filter=xFilter;

            if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null || $scope.mreportFilter.xyear == '' || $scope.mreportFilter.xyear == null) {
                var error=[];
                if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
                if ($scope.mreportFilter.xyear == '' || $scope.mreportFilter.xyear == null) {error.push("Tahun Periode kosong");};
                bsNotify.show(
                    {
                        size: 'big',
                        type: 'danger',
                        title: "Ditemukan beberapa data yang salah",
                        content: error.join('<br>'),
                        number: error.length
                    }
                );  
            } else{
                    $scope.sm_show2=true;
                    ReportFilter.getRptOAP($scope.mreportFilter).then(
                        function(res){
                            // console.log("===>",res);
                            //with function
                            var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                            var b64Data = res.data;

                            var blob = b64toBlob(b64Data, contentType);
                            var blobUrl = URL.createObjectURL(blob);
                            // window.open(blobUrl);
                            $scope.sm_show2=false;
                            saveAs(blob, 'Report_OAP' + '.xlsx');

                            return res;
                        },
                        function(err){
                            console.log("err=>",err);
                        }
                    );
            }
        } else if($scope.mreportFilter.reportId == 4) {
            // Report RAP
            arrVisibilityOAP[1]=(($scope.mreportFilter.visibility.market == 1) ? 1:0);
            arrVisibilityOAP[0]=(($scope.mreportFilter.visibility.retailSales == 1) ? 1:0);
            arrVisibilityOAP[2]=(($scope.mreportFilter.visibility.outletShare == 1) ? 1:0);
            var xFilter = arrVisibilityOAP.toString();
            xFilter = xFilter.replace(/,/g, "|");
            // console.log(arrVisibilityOAP,"xFilter==>",xFilter);
            $scope.mreportFilter.Filter=xFilter;

            if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null || $scope.mreportFilter.xyear == '' || $scope.mreportFilter.xyear == null) {
                var error=[];
                if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
                if ($scope.mreportFilter.xyear == '' || $scope.mreportFilter.xyear == null) {error.push("Tahun Periode kosong");};
                bsNotify.show(
                    {
                        size: 'big',
                        type: 'danger',
                        title: "Ditemukan beberapa data yang salah",
                        content: error.join('<br>'),
                        number: error.length
                    }
                );  
            } else{
                    $scope.sm_show2=true;
                    ReportFilter.getRptRAP($scope.mreportFilter).then(
                        function(res){
                            // console.log("===>",res);
                            //with function
                            var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                            var b64Data = res.data;

                            var blob = b64toBlob(b64Data, contentType);
                            var blobUrl = URL.createObjectURL(blob);
                            // window.open(blobUrl);
                            $scope.sm_show2=false;
                            saveAs(blob, 'Report_RAP' + '.xlsx');

                            return res;
                        },
                        function(err){
                            console.log("err=>",err);
                        }
                    );
            }
        } else if($scope.mreportFilter.reportId == 5) {
            // Report RSSP Fleet
            if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null || $scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null ) {
                var error=[];
                if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
                if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};    
                bsNotify.show(
                    {
                        size: 'big',
                        type: 'danger',
                        title: "Ditemukan beberapa data yang salah",
                        content: error.join('<br>'),
                        number: error.length
                    }
                );  
            } else{
                    $scope.sm_show2=true;
                    ReportFilter.getRptFleet($scope.mreportFilter).then(
                        function(res){
                            // console.log("===>",res);
                            //with function
                            var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                            var b64Data = res.data;

                            var blob = b64toBlob(b64Data, contentType);
                            var blobUrl = URL.createObjectURL(blob);
                            // window.open(blobUrl);
                            $scope.sm_show2=false;
                            saveAs(blob, 'Report_RSSP_Fleet' + '.xlsx');

                            return res;
                        },
                        function(err){
                            console.log("err=>",err);
                        }
                    );
            }
        } else if($scope.mreportFilter.reportId == 6) {
            // Report RSSP Internal Order
            if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null ) {
                var error=[];
                // $scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null ||
                // if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
                if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};    
                bsNotify.show(
                    {
                        size: 'big',
                        type: 'danger',
                        title: "Ditemukan beberapa data yang salah",
                        content: error.join('<br>'),
                        number: error.length
                    }
                );  
            } else{
                    $scope.sm_show2=true;
                    ReportFilter.getRptInternal($scope.mreportFilter).then(
                        function(res){
                            // console.log("===>",res);
                            //with function
                            var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                            var b64Data = res.data;

                            var blob = b64toBlob(b64Data, contentType);
                            var blobUrl = URL.createObjectURL(blob);
                            // window.open(blobUrl);
                            $scope.sm_show2=false;
                            saveAs(blob, 'Report_RSSP_Internal_Order' + '.xlsx');

                            return res;
                        },
                        function(err){
                            console.log("err=>",err);
                        }
                    );
            }
        } else if($scope.mreportFilter.reportId == 7) {
            // Report Achievement Evaluation
            // || $scope.mreportFilter.date1 == '' || $scope.mreportFilter.date1 == null || $scope.mreportFilter.date2 == '' || $scope.mreportFilter.date2 == null $scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null ||
            if ( $scope.mreportFilter.dateS == '' || $scope.mreportFilter.dateS == null || $scope.mreportFilter.dateE == '' || $scope.mreportFilter.dateE == null ) {
                var error=[];
                // if ($scope.mreportFilter.areaId == '' || $scope.mreportFilter.areaId == null) {error.push("Area kosong");};
                if ($scope.mreportFilter.dateS == '' || $scope.mreportFilter.dateS == null) {error.push("Date Start kosong");};    
                if ($scope.mreportFilter.dateE == '' || $scope.mreportFilter.dateE == null) {error.push("Date End kosong");};  
                // if ($scope.mreportFilter.GetsudoPeriodId == '' || $scope.mreportFilter.GetsudoPeriodId == null) {error.push("GetSudo Period kosong");};    
                  
                bsNotify.show(
                    {
                        size: 'big',
                        type: 'danger',
                        title: "Ditemukan beberapa data yang salah",
                        content: error.join('<br>'),
                        number: error.length
                    }
                );  
            } else{
                    $scope.sm_show2=true;
                    ReportFilter.getRptAchievement($scope.mreportFilter).then(
                        function(res){
                            // console.log("===>",res);
                            //with function
                            var contentType = 'application/vnd.ms-excel;charset=charset=utf-8';
                            var b64Data = res.data;

                            var blob = b64toBlob(b64Data, contentType);
                            var blobUrl = URL.createObjectURL(blob);
                            // window.open(blobUrl);
                            $scope.sm_show2=false;
                            saveAs(blob, 'Report_Achievement_Evaluation' + '.xlsx');

                            return res;
                        },
                        function(err){
                            console.log("err=>",err);
                        }
                    );
            }
        };
      
        
    }
        

        // $scope.grid = {
        //                 enableSorting: true,
        //                 enableRowSelection: true,
        //                 multiSelect: true,
        //                 enableSelectAll: true,
        //                 // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        //                 // paginationPageSize: 15,
        //                 columnDefs:  [
        //                     // { name:'id',    field:'id', width:'7%' },
        //                     { name:$scope.field.f1, field:'Ocode' },
        //                     { name:'Outlet Name', field:'Oname'}, //, cellFilter: dateFilter 
        //                     { name:'Katashiki', field:'Katashiki'},
        //                     { name:'Suffix', field:'Suffix'},
        //                     { name:'Clr. Code', field:'Ccode'},
        //                     { name:'Model', field:'Model'}, 
        //                     { name:'Type', field:'Type'}, 
        //                     { name:'Colour', field:'Colour'}, 
        //                     { name:'C', field:'WS'}, 
        //                     { name:'N+1', field:'N1'}, 
        //                     { name:'N+2', field:'N2'}, 
        //                     { name:'N+3', field:'N3'}, 
        //                     { name:'N+4', field:'N4'}, 
        //                     { name:'N+5', field:'N5'}, 
        //                     { name:'N+6', field:'N6'}, 
        //                     { name:'N+7', field:'N7'}, 
        //                     { name:'N+8', field:'N8'}, 
        //                 ]
        //             };



    // $scope.rptNames = function() {
    //     $scope.rptName = $scope.report;
    //     // Region.getDataProvince().then(
    //     //         function(res){
    //     //             console.log("province=>",res.data);
    //     //             $scope.provinceData = res.data.Result;
    //     //         },
    //     //         function(err){
    //     //             console.log("err=>",err);
    //     //         }
    //     //     );
    // }

    /*
    */
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // btnActionEditTemplate = '_sys/templates/uigridCellEditButtonTemplate.html';
    

    
    
    
});
