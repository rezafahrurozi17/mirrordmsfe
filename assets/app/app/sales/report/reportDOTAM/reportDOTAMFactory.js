angular.module('app')
  .factory('ReportDOTAM', function($http, CurrentUser, $q) {
    var currentUser = CurrentUser.user;
    return {
      getRptRssp: function(xData){
        var xparam='';
        
        if (xData.provinceId != '' && xData.provinceId != undefined) {
          xparam = xparam +'&provinceId='+xData.provinceId;
        };
        if (xData.dealerId != '' && xData.dealerId != undefined) {
          xparam = xparam + '&dealerId='+xData.dealerId;
        };
        if (xData.outletId != '' && xData.outletId != undefined) {
          xparam = xparam + '&outletId='+xData.outletId;
        }else if (xData.outletHO != '' && xData.outletHO != undefined && xData.outletHO != null){
          xparam = xparam + '&outletId='+xData.outletHO;
	};
        if (xData.modelId != '' && xData.modelId != undefined) {
          xparam = xparam + '&vehiclemodelId='+xData.modelId;
        };
        if (xData.typeId != '' && xData.typeId != undefined) {
          xparam = xparam + '&vehicleTypeId='+xData.typeId;
        };
        if (xData.colourId != '' && xData.colourId != undefined) {
          xparam = xparam + '&ColorId='+xData.colourId;
        };

        
        // RSSPReport?areaId={areaId}&dealeId={dealeId}&vehiclemodelId={vehiclemodelId}&provinceId={provinceId}&outletId={outletId}&ColorId={ColorId}&PcFilter=[0|1|0|0,0,0,0]
        console.log(" RSSP ===>",xData);
        console.log(" RSSP ===>",'/api/rpt/RSSPReport?areaId='+xData.areaId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+xparam+'&PcFilter='+xData.Filter);
        
         var res =  $http.get('/api/rpt/RSSPReport?areaId='+xData.areaId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+xparam+'&PcFilter='+xData.Filter );
        // var res =  $http.get('/api/rpt/RSSPReport?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealerId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&PcFilter='+xData.Filter ); // , { responseType: 'arraybuffer' }'&ColorId='+xData.colourId+
          
        return res;
      },

      getRptProfiling: function(xData){
        var xparam='';
        if (xData.areaId != '' && xData.areaId != undefined) {
          xparam = xparam +'&areaId='+xData.areaId;
        };
        if (xData.provinceId != '' && xData.provinceId != undefined) {
          xparam = xparam +'&provinceId='+xData.provinceId;
        };
        if (xData.dealerId != '' && xData.dealerId != undefined) {
          xparam = xparam + '&dealerId='+xData.dealerId;
        };
        // if (xData.allowance != '' && xData.allowance != undefined) {
        //   xparam = xparam + '&allowance='+xData.allowance;
        // };
        if (xData.outletId != '' && xData.outletId != undefined) {
          xparam = xparam + '&outletId='+xData.outletId;
        }else if (xData.outletHO != '' && xData.outletHO != undefined && xData.outletHO != null){
          xparam = xparam + '&outletId='+xData.outletHO;
	};

        // console.log(" Profiling ===>",xData);areaId='+xData.areaId+'&areaId='+xData.areaId+'&'&allowance='+xData.allowance+'&allowance='+xData.allowance+
        console.log(" Profiling ===>",'/api/rpt/ReportRSSPProfiling/?GetsudoPeriodId='+xData.GetsudoPeriodId+'&vehiclemodelId='+xData.modelId+'&TargetPeriod='+xData.monthId+xparam);
        // console.log(" param ==>",'/api/rpt/ReportRSSPProfiling/?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealeId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&allowance='+xData.allowance);
        var res =  $http.get('/api/rpt/ReportRSSPProfiling/?GetsudoPeriodId='+xData.GetsudoPeriodId+'&vehiclemodelId='+xData.modelId+'&TargetPeriod='+xData.monthId+xparam); // , { responseType: 'arraybuffer' }
        // var res =  $http.get('/api/rpt/ReportRSSPProfiling/?areaId='+xData.areaId+'&vehiclemodelId=28&provinceId='+xData.provinceId+'&dealeId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&allowance='+xData.allowance ); // , { responseType: 'arraybuffer' }
          
        return res;
      },

      getRptOAP: function(xData){
        // api/OAPRAPReport?Year={Year}&areaId={areaId}&CityRegencyId={CityRegencyId}&dealerId={dealerId}&TypeDokumenId={TypeDokumenId}&provinceId={provinceId}&outletId={outletId}
        var xparam='';
        if (xData.provinceId != '' && xData.provinceId != undefined) {
          xparam = xparam +'&provinceId='+xData.provinceId;
        };
        if (xData.dealerId != '' && xData.dealerId != undefined) {
          xparam = xparam + '&dealerId='+xData.dealerId;
        };
        if (xData.outletId != '' && xData.outletId != undefined) {
          xparam = xparam + '&outletId='+xData.outletId;
        }else if (xData.outletHO != '' && xData.outletHO != undefined && xData.outletHO != null){
          xparam = xparam + '&outletId='+xData.outletHO;
	};
        if (xData.cityId != '' && xData.cityId != undefined) {
          xparam = xparam + '&CityRegencyId='+xData.cityId;
        };
        // if (xData.xyear != '' && xData.xyear != undefined) {
        //   xparam = xparam + '&year='+xData.xyear;
        // };

        // console.log(" OAP ===>",xData);
        console.log(" OAP ===>",'/api/rpt/OAPReport?areaId='+xData.areaId+'&TypeDokumenId=1&year='+xData.xyear+xparam+'&PcFilter='+xData.Filter);
        // console.log(" param ==>",'/api/rpt/ReportRSSPProfiling/?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealeId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&allowance='+xData.allowance);
        var res =  $http.get('/api/rpt/OAPReport?areaId='+xData.areaId+'&TypeDokumenId=1&year='+xData.xyear+xparam+'&PcFilter='+xData.Filter ); // , { responseType: 'arraybuffer' }
        // var res =  $http.get('/api/rpt/OAPReport?areaId='+xData.areaId+'&CityRegencyId='+xData.cityId+'&provinceId='+xData.provinceId+'&dealerId='+xData.dealerId+'&outletId='+xData.outletId+'&TypeDokumenId=1&year='+xData.xyear+'&PcFilter='+xData.Filter ); // , { responseType: 'arraybuffer' }
          
        return res;
      },

      getRptRAP: function(xData){
        var xparam='';
        if (xData.provinceId != '' && xData.provinceId != undefined) {
          xparam = xparam +'&provinceId='+xData.provinceId;
        };
        if (xData.dealerId != '' && xData.dealerId != undefined) {
          xparam = xparam + '&dealerId='+xData.dealerId;
        };
        if (xData.outletId != '' && xData.outletId != undefined) {
          xparam = xparam + '&outletId='+xData.outletId;
        }else if (xData.outletHO != '' && xData.outletHO != undefined && xData.outletHO != null){
          xparam = xparam + '&outletId='+xData.outletHO;
	};
        if (xData.cityId != '' && xData.cityId != undefined) {
          xparam = xparam + '&CityRegencyId='+xData.cityId;
        };
        console.log(" RAP ===>",xData);
        // console.log(" param ==>",'/api/rpt/ReportRSSPProfiling/?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealeId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&allowance='+xData.allowance);
        var res =  $http.get('/api/rpt/RAPReport?areaId='+xData.areaId+'&TypeDokumenId=2&year='+xData.xyear+xparam+'&PcFilter='+xData.Filter ); // , { responseType: 'arraybuffer' }
        // var res =  $http.get('/api/rpt/RAPReport?areaId='+xData.areaId+'&CityRegencyId='+xData.cityId+'&provinceId='+xData.provinceId+'&dealerId='+xData.dealerId+'&outletId='+xData.outletId+'&TypeDokumenId=2&year='+xData.xyear+'&PcFilter='+xData.Filter ); // , { responseType: 'arraybuffer' } 
        
        return res;
      },

      getRptFleet: function(xData){
        // RSSPDetailFleet?GetsudoPeriodId={GetsudoPeriodId}&areaId={areaId}&vehiclemodelId={vehiclemodelId}&provinceId={provinceId}&dealerId={dealerId}&outletId={outletId}&ColorId={ColorId}  
        var xparam='';
        if (xData.provinceId != '' && xData.provinceId != undefined) {
          xparam = xparam +'&provinceId='+xData.provinceId;
        };
        if (xData.dealerId != '' && xData.dealerId != undefined) {
          xparam = xparam + '&dealerId='+xData.dealerId;
        };
        if (xData.outletId != '' && xData.outletId != undefined) {
          xparam = xparam + '&outletId='+xData.outletId;
        }else if (xData.outletHO != '' && xData.outletHO != undefined && xData.outletHO != null){
          xparam = xparam + '&outletId='+xData.outletHO;
          // xparam = xparam + '&dealerId='+xData.dealerId;
	      };
        if (xData.modelId != '' && xData.modelId != undefined) {
          xparam = xparam + '&vehiclemodelId='+xData.modelId;
        };
        if (xData.typeId != '' && xData.typeId != undefined && xData.typeId != null ) {
          xparam = xparam + '&vehicleTypeId='+xData.typeId;
        };
        if (xData.colourId != '' && xData.colourId != undefined) {
          xparam = xparam + '&ColorId='+xData.colourId;
        };
        // console.log(" Fleet ===>",xData);
        console.log(" Fleet ===>",'/api/rpt/RSSPDetailFleet?areaId='+xData.areaId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+xparam);
        // console.log(" param ==>",'/api/rpt/ReportRSSPProfiling/?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealeId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&allowance='+xData.allowance);
        var res =  $http.get('/api/rpt/RSSPDetailFleet?areaId='+xData.areaId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+xparam); // , { responseType: 'arraybuffer' } +xData.colourId 
        // var res =  $http.get('/api/rpt/RSSPDetailFleet?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealerId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&ColorId=2531'); // , { responseType: 'arraybuffer' } +xData.colourId 
          
        return res;
      },
        
      getRptInternal: function(xData){
        // RSSPDetailOrderReport?GetsudoPeriodId={GetsudoPeriodId}&dealerId={dealerId}&areaId={areaId}&ColorId={ColorId}&vehiclemodelId={vehiclemodelId}&provinceId={provinceId}&outletId={outletId}
        var xparam='';
        if (xData.areaId != '' && xData.areaId != undefined) {
          xparam = xparam +'&areaId='+xData.areaId;
        };
        if (xData.provinceId != '' && xData.provinceId != undefined) {
          xparam = xparam +'&provinceId='+xData.provinceId;
        };
        if (xData.dealerId != '' && xData.dealerId != undefined) {
          xparam = xparam + '&dealerId='+xData.dealerId;
        };
        if (xData.outletIdInternal != '' && xData.outletIdInternal != undefined) {
          xparam = xparam + '&outletId='+xData.outletIdInternal;
        }else if (xData.outletHO != '' && xData.outletHO != undefined && xData.outletHO != null){
          xparam = xparam + '&outletId='+xData.outletHO;
	};
        if (xData.modelId != '' && xData.modelId != undefined) {
          xparam = xparam + '&vehiclemodelId='+xData.modelId;
        };
        if (xData.typeId != '' && xData.typeId != undefined && xData.typeId != null ) {
          xparam = xparam + '&vehicleTypeId='+xData.typeId;
        };
        if (xData.colourId != '' && xData.colourId != undefined) {
          xparam = xparam + '&ColorId='+xData.colourId;
        };
        // console.log(" Internal ===>",xData); areaId='+xData.areaId+'& areaId='+xData.areaId+'&
        console.log(" Internal ===>",'/api/rpt/RSSPDetailOrderReport?GetsudoPeriodId='+xData.GetsudoPeriodId+xparam);
        // console.log(" param ==>",'/api/rpt/ReportRSSPProfiling/?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealeId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&allowance='+xData.allowance);
        var res =  $http.get('/api/rpt/RSSPDetailOrderReport/?GetsudoPeriodId='+xData.GetsudoPeriodId+xparam); 
        //var res =  $http.get('/api/rpt/RSSPDetailOrderReport?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealerId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&ColorId=2531'); // , { responseType: 'arraybuffer' } +xData.colourId 
          
        return res;
      },  

      getRptAchievement: function(xData){
        // AchievementEvaluationReport?StartDate={StartDate}&EndDate={EndDate}&areaId={areaId}&CityRegencyId={CityRegencyId}&groupdealerId={groupdealerId}&vehiclemodelId={vehiclemodelId}&provinceId={provinceId}&outletId={outletId}
        var xparam='';
        if (xData.areaId != '' && xData.areaId != undefined && xData.areaId != null) {
          xparam = xparam +'&areaId='+xData.areaId;
        };
        if (xData.provinceId != '' && xData.provinceId != undefined) {
          xparam = xparam +'&provinceId='+xData.provinceId;
        };
        if (xData.dealerId != '' && xData.dealerId != undefined) {
          xparam = xparam + '&groupdealerId='+xData.dealerId;
        }else if (xData.dealerHO != '' && xData.dealerHO != undefined && xData.dealerHO != null){
            xparam = xparam + '&dealerId='+xData.dealerHO;
        };
        ;
        if (xData.outletId != '' && xData.outletId != undefined) {
          xparam = xparam + '&outletId='+xData.outletId;
        }
        else if (xData.outletHO != '' && xData.outletHO != undefined && xData.outletHO != null){
          xparam = xparam + '&outletId='+xData.outletHO;
	       }
         ;
        if (xData.modelId != '' && xData.modelId != undefined) {
          xparam = xparam + '&vehiclemodelId='+xData.modelId;
        };
        // console.log(" Achievement ===>",xData);StartDate='+xData.dateS+'&EndDate='+xData.dateE
        console.log(" Achievement ===>",'/api/rpt/AchievementEvaluationReport?'+xparam);
        // console.log(" param ==>",'/api/rpt/ReportRSSPProfiling/?areaId='+xData.areaId+'&vehiclemodelId='+xData.modelId+'&provinceId='+xData.provinceId+'&dealeId='+xData.dealerId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+'&outletId='+xData.outletId+'&allowance='+xData.allowance);
        // var res =  $http.get('/api/rpt/AchievementEvaluationReport?areaId='+xData.areaId+'&GetsudoPeriodId='+xData.GetsudoPeriodId+xparam ); // , { responseType: 'arraybuffer' }
        var res =  $http.get('/api/rpt/AchievementEvaluationReport?'+'StartDate='+xData.dateS+'&EndDate='+xData.dateE+xparam ); // , { responseType: 'arraybuffer' }
          
        return res;
      }, 
      
      getData: function(xfilter) {
        // var res=$http.get('/branchOrder/oap');
        console.log("get data ===>",xfilter);

        var res = 'CQAGAAAAAAAAAAAAAAABAAAAAQAAAAAAAAAAEAAA/';
        return res;
      }
    }
  });