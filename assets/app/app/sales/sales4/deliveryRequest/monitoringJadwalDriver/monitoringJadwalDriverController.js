angular.module('app')
    .controller('MonitoringJadwalDriverController', function ($scope,$filter, $http, CurrentUser, MonitoringJadwalDriverFactory, $timeout) {
        IfGotProblemWithModal();
		
		$scope.TodayDate_RawPengirimanUnit=new Date();
		$scope.GeserPengirimanUnit=new Date();
		$scope.GeserPengirimanKeBengkel=new Date();
		$scope.GeserPengambilanUnitSelesaiRepair=new Date();
		
		$scope.TodayDatePengirimanUnit=$scope.TodayDate_RawPengirimanUnit.getFullYear()+'-'+('0' + ($scope.TodayDate_RawPengirimanUnit.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_RawPengirimanUnit.getDate()).slice(-2);
		$scope.TodayDatePlus1PengirimanUnit=$scope.TodayDate_RawPengirimanUnit.getFullYear()+'-'+('0' + ($scope.TodayDate_RawPengirimanUnit.getMonth()+1)).slice(-2)+'-'+('0' + ($scope.TodayDate_RawPengirimanUnit.getDate()+1)).slice(-2);
		$scope.TodayDatePlus2PengirimanUnit=$scope.TodayDate_RawPengirimanUnit.getFullYear()+'-'+('0' + ($scope.TodayDate_RawPengirimanUnit.getMonth()+1)).slice(-2)+'-'+('0' + ($scope.TodayDate_RawPengirimanUnit.getDate()+2)).slice(-2);


		$scope.TodayDate_RawPengirimanKeBengkel=new Date();
		$scope.TodayDatePengirimanKeBengkel=$scope.TodayDate_RawPengirimanKeBengkel.getFullYear()+'-'+('0' + ($scope.TodayDate_RawPengirimanKeBengkel.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_RawPengirimanKeBengkel.getDate()).slice(-2);
		
		$scope.TodayDate_RawPengambilanUnitSelesaiRepair=new Date();
		$scope.TodayDatePengambilanUnitSelesaiRepair=$scope.TodayDate_RawPengambilanUnitSelesaiRepair.getFullYear()+'-'+('0' + ($scope.TodayDate_RawPengambilanUnitSelesaiRepair.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_RawPengambilanUnitSelesaiRepair.getDate()).slice(-2);
		
		$scope.user = CurrentUser.user();
        $scope.mMonitoringJadwalDriver = null; //Model
        $scope.xRole = { selected: [] };
		
		$scope.MonitoringJadwalDriverPengirimanUnitGeserKurang1 = function () {
			$scope.GeserPengirimanUnit.setDate($scope.GeserPengirimanUnit.getDate()-1);
		}
		
		$scope.MonitoringJadwalDriverPengirimanUnitGeserTambah1 = function () {
			$scope.GeserPengirimanUnit.setDate($scope.GeserPengirimanUnit.getDate()+1);
		}
		
		$scope.MonitoringJadwalDriverPengirimanKeBengkelGeserKurang1 = function () {
			$scope.GeserPengirimanKeBengkel.setDate($scope.GeserPengirimanKeBengkel.getDate()-1);
		}
		
		$scope.MonitoringJadwalDriverPengirimanKeBengkelGeserTambah1 = function () {
			$scope.GeserPengirimanKeBengkel.setDate($scope.GeserPengirimanKeBengkel.getDate()+1);
		}
		
		$scope.MonitoringJadwalDriverPengambilanUnitSelesaiRepairGeserKurang1 = function () {
			$scope.GeserPengambilanUnitSelesaiRepair.setDate($scope.GeserPengambilanUnitSelesaiRepair.getDate()-1);
		}
		
		$scope.MonitoringJadwalDriverPengambilanUnitSelesaiRepairGeserTambah1 = function () {
			$scope.GeserPengambilanUnitSelesaiRepair.setDate($scope.GeserPengambilanUnitSelesaiRepair.getDate()+1);
		}
		
		var chart1 = AmCharts.makeChart( "chartdivPengirimanUnit", {
					"type": "gantt",
					"theme": "light",
					"marginRight": 70,
					"period": "hh",
					"dataDateFormat":"YYYY-MM-DD",
					"balloonDateFormat": "JJ:NN",
					"columnWidth": 0.5,
					"valueAxis": {
						"type": "time",
						"labelFunction":function(value) 
						{	
							
							return value;
						}
					},
					"brightnessStep": 10,
					"graph": {
						"fillAlphas": 1,
						"balloonText": "<b>[[task]]</b>: [[open]] [[value]]"
					},
					"rotate": true,
					"categoryField": "category",
					"segmentsField": "segments",
					"colorField": "color",
					"startDate": "2015-01-01",
					"startField": "start",
					"endField": "end",
					"durationField": "duration",
					"dataProvider": [ {
						"category": "John",
						"segments": [ {
							"start": 7.5,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Smith",
						"segments": [ {
							"start": 10,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Ben",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Mike",
						"segments": [ {
							"start": 9,
							"duration": 6,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 4,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Lenny",
						"segments": [ {
							"start": 8,
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Scott",
						"segments": [ {
							"start": 15,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Julia",
						"segments": [ {
							"start": 9,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 1,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 8,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Bob",
						"segments": [ {
							"start": 9,
							"duration": 8,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 7,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Kendra",
						"segments": [ {
							"start": 11,
							"duration": 8,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Tom",
						"segments": [ {
							"start": 9,
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 5,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Kyle",
						"segments": [ {
							"start": 6,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Anita",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Jack",
						"segments": [ {
							"start": 8,
							"duration": 10,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Kim",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 3,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Aaron",
						"segments": [ {
							"start": 18,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Alan",
						"segments": [ {
							"start": 17,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Ruth",
						"segments": [ {
							"start": 13,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Simon",
						"segments": [ {
							"start": 10,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 17,
							"duration": 4,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					} ],
					"valueScrollbar": {
						"autoGridCount":true
					},
					"chartCursor": {
						"cursorColor":"#55bb76",
						"valueBalloonsEnabled": false,
						"cursorAlpha": 0,
						"valueLineAlpha":0.5,
						"valueLineBalloonEnabled": true,
						"valueLineEnabled": true,
						"zoomable":false,
						"valueZoomable":true
					},
					"export": {
						"enabled": true
					 }
				} );
		
		var chart2 = AmCharts.makeChart( "chartdivPengirimanKeBengkel", {
					"type": "gantt",
					"theme": "light",
					"marginRight": 70,
					"period": "hh",
					"dataDateFormat":"YYYY-MM-DD",
					"balloonDateFormat": "JJ:NN",
					"columnWidth": 0.5,
					"valueAxis": {
						"type": "date"
					},
					"brightnessStep": 10,
					"graph": {
						"fillAlphas": 1,
						"balloonText": "<b>[[task]]</b>: [[open]] [[value]]"
					},
					"rotate": true,
					"categoryField": "category",
					"segmentsField": "segments",
					"colorField": "color",
					"startDate": "2015-01-01",
					"startField": "start",
					"endField": "end",
					"durationField": "duration",
					"dataProvider": [ {
						"category": "John",
						"segments": [ {
							"start": 7,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Smith",
						"segments": [ {
							"start": 10,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Ben",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Mike",
						"segments": [ {
							"start": 9,
							"duration": 6,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 4,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Lenny",
						"segments": [ {
							"start": 8,
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Scott",
						"segments": [ {
							"start": 15,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Julia",
						"segments": [ {
							"start": 9,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 1,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 8,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Bob",
						"segments": [ {
							"start": 9,
							"duration": 8,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 7,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Kendra",
						"segments": [ {
							"start": 11,
							"duration": 8,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Tom",
						"segments": [ {
							"start": 9,
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 5,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Kyle",
						"segments": [ {
							"start": 6,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Anita",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Jack",
						"segments": [ {
							"start": 8,
							"duration": 10,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Kim",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 3,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Aaron",
						"segments": [ {
							"start": 18,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Alan",
						"segments": [ {
							"start": 17,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Ruth",
						"segments": [ {
							"start": 13,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Simon",
						"segments": [ {
							"start": 10,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 17,
							"duration": 4,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					} ],
					"valueScrollbar": {
						"autoGridCount":true
					},
					"chartCursor": {
						"cursorColor":"#55bb76",
						"valueBalloonsEnabled": false,
						"cursorAlpha": 0,
						"valueLineAlpha":0.5,
						"valueLineBalloonEnabled": true,
						"valueLineEnabled": true,
						"zoomable":false,
						"valueZoomable":true
					},
					"export": {
						"enabled": false
					 }
				} );
		
		var chart3 = AmCharts.makeChart( "chartdivPengambilanUnitSelesaiRepair", {
					"type": "gantt",
					"theme": "light",
					"marginRight": 70,
					"period": "hh",
					"dataDateFormat":"YYYY-MM-DD",
					"balloonDateFormat": "JJ:NN",
					"columnWidth": 0.5,
					"valueAxis": {
						"type": "date"
					},
					"brightnessStep": 10,
					"graph": {
						"fillAlphas": 1,
						"balloonText": "<b>[[task]]</b>: [[open]] [[value]]"
					},
					"rotate": true,
					"categoryField": "category",
					"segmentsField": "segments",
					"colorField": "color",
					"startDate": "2015-01-01",
					"startField": "start",
					"endField": "end",
					"durationField": "duration",
					"dataProvider": [ {
						"category": "John",
						"segments": [ {
							"start": 7,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Smith",
						"segments": [ {
							"start": 10,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Ben",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Mike",
						"segments": [ {
							"start": 9,
							"duration": 6,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 4,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Lenny",
						"segments": [ {
							"start": 8,
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Scott",
						"segments": [ {
							"start": 15,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Julia",
						"segments": [ {
							"start": 9,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 1,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 8,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Bob",
						"segments": [ {
							"start": 9,
							"duration": 8,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 7,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Kendra",
						"segments": [ {
							"start": 11,
							"duration": 8,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Tom",
						"segments": [ {
							"start": 9,
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 5,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Kyle",
						"segments": [ {
							"start": 6,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Anita",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 16,
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Jack",
						"segments": [ {
							"start": 8,
							"duration": 10,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						} ]
					}, {
						"category": "Kim",
						"segments": [ {
							"start": 12,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 3,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Aaron",
						"segments": [ {
							"start": 18,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					}, {
						"category": "Alan",
						"segments": [ {
							"start": 17,
							"duration": 2,
							"color": "#46615e",
							"task": "Task #1"
						}, {
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 2,
							"color": "#8dc49f",
							"task": "Task #3"
						} ]
					}, {
						"category": "Ruth",
						"segments": [ {
							"start": 13,
							"duration": 2,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"duration": 1,
							"color": "#8dc49f",
							"task": "Task #3"
						}, {
							"duration": 4,
							"color": "#46615e",
							"task": "Task #1"
						} ]
					}, {
						"category": "Simon",
						"segments": [ {
							"start": 10,
							"duration": 3,
							"color": "#727d6f",
							"task": "Task #2"
						}, {
							"start": 17,
							"duration": 4,
							"color": "#FFE4C4",
							"task": "Task #4"
						} ]
					} ],
					"valueScrollbar": {
						"autoGridCount":true
					},
					"chartCursor": {
						"cursorColor":"#55bb76",
						"valueBalloonsEnabled": false,
						"cursorAlpha": 0,
						"valueLineAlpha":0.5,
						"valueLineBalloonEnabled": true,
						"valueLineEnabled": true,
						"zoomable":false,
						"valueZoomable":true
					},
					"export": {
						"enabled": false
					 }
				} );
		
		$scope.TabelMonitoringJadwalDriver_PengirimanUnit = {
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'No SPK', field: 'FrameNo' },
                { name: 'Nama Pelanggan', field: 'CustomerName' },
				{ name: 'Salesman', field: 'SentDate' },
				{ name: 'Model', field: '' },
				{ name: 'Tipe', field: 'Description' },
                { name: 'Warna', field: 'DriverPDSName' },
				{ name: 'No Rangka', field: 'FrameNo' },
				{ name: 'Tgl Pengiriman', field: 'DriverPDSName' },
				{ name: 'Jam Pengiriman', field: 'DriverPDSName' },
				{ name: 'Tujuan Pengiriman', field: 'TujuanPengirimanName' },

            ]
        };
		
		$scope.TabelMonitoringJadwalDriver_PengirimanKeBengkel = {
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'No SPK', field: 'FrameNo' },
                { name: 'Nama Pelanggan', field: 'CustomerName' },
				{ name: 'Salesman', field: 'SentDate' },
				{ name: 'Model', field: '' },
				{ name: 'Tipe', field: 'Description' },
                { name: 'Warna', field: 'DriverPDSName' },
				{ name: 'No Rangka', field: 'FrameNo' },
				{ name: 'Tgl Pengiriman', field: 'DriverPDSName' },
				{ name: 'Jam Pengiriman', field: 'DriverPDSName' },
				{ name: 'Tanggal SPK Diterima', field: 'DriverPDSName' },

            ]
        };
		
		$scope.TabelMonitoringJadwalDriver_PengambilanUnitSelesaiRepair = {
            enableSorting: true,
            enableFiltering: true,
            columnDefs: [
                { name: 'No SPK', field: 'FrameNo' },
                { name: 'Nama Pelanggan', field: 'CustomerName' },
				{ name: 'Salesman', field: 'SentDate' },
				{ name: 'Model', field: '' },
				{ name: 'Tipe', field: 'Description' },
                { name: 'Warna', field: 'DriverPDSName' },
				{ name: 'No Rangka', field: 'FrameNo' },
				{ name: 'Tgl Pengiriman', field: 'DriverPDSName' },
				{ name: 'Jam Pengiriman', field: 'DriverPDSName' },
				{ name: 'Tgl Selesai Repair', field: 'DriverPDSName' },

            ]
        };
    });
