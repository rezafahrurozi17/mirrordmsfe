angular.module('app')
  .factory('MonitoringJadwalDriverFactory', function($http, $httpParamSerializer, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getListTaskDriver: function(filter) {
		var param = $httpParamSerializer(filter);
		var res=$http.get('/api/sales/SDRDeliveryNote/Filter/?'+param);
        return res;
      },

      create: function(DeliveryNote) {
        return $http.post('/api/sales/SDRDeliveryNote', [{
											DRId: DeliveryNote.DRId,
											FrameNo: DeliveryNote.FrameNo,
											VehicleTypeColorId: DeliveryNote.VehicleTypeColorId,
											TujuanPengirimanId: DeliveryNote.TujuanPengirimanId,
											JenisTugasId: DeliveryNote.JenisTugasId,
											DriverCategoryId: DeliveryNote.DriverCategoryId,
											DriverId: DeliveryNote.DriverId,
											OutDate: DeliveryNote.OutDate.getFullYear()+'-'
											+('0' + (DeliveryNote.OutDate.getMonth()+1)).slice(-2)+'-'
											+('0' + DeliveryNote.OutDate.getDate()).slice(-2)+'T'
											+((DeliveryNote.OutDate.getHours()<'10'?'0':'')+ DeliveryNote.OutDate.getHours())+':'
											+((DeliveryNote.OutDate.getMinutes()<'10'?'0':'')+ DeliveryNote.OutDate.getMinutes())+':'
											+((DeliveryNote.OutDate.getSeconds()<'10'?'0':'')+ DeliveryNote.OutDate.getSeconds()),
											
											SentDate: DeliveryNote.SentDate,
											SentTime: DeliveryNote.SentTime,
											DirectDeliveryBit: DeliveryNote.DirectDeliveryBit,
											VehicleModelId: DeliveryNote.VehicleModelId,
											VehicleTypeId: DeliveryNote.VehicleTypeId,
											VehicleTypeColorId: DeliveryNote.VehicleTypeColorId,
											ColorId: DeliveryNote.ColorId,
											DeliveryCaroseriesDestinationId: DeliveryNote.DeliveryCaroseriesDestinationId,
											DeliveryCaroseriesVendorId: DeliveryNote.DeliveryCaroseriesVendorId,
											SentAddress: DeliveryNote.SentAddress,
											DateBackEstimated: DeliveryNote.DateBackEstimated.getFullYear()+'-'
											+('0' + (DeliveryNote.DateBackEstimated.getMonth()+1)).slice(-2)+'-'
											+('0' + DeliveryNote.DateBackEstimated.getDate()).slice(-2)+'T'
											+((DeliveryNote.DateBackEstimated.getHours()<'10'?'0':'')+ DeliveryNote.DateBackEstimated.getHours())+':'
											+((DeliveryNote.DateBackEstimated.getMinutes()<'10'?'0':'')+ DeliveryNote.DateBackEstimated.getMinutes())+':'
											+((DeliveryNote.DateBackEstimated.getSeconds()<'10'?'0':'')+ DeliveryNote.DateBackEstimated.getSeconds()),
											
											}]);
      },
	  
      update: function(AlokasiJadwalDriver){
        return $http.put('/api/sales/SDRDeliveryNote_ListTaskDriverView', [{
                                            DeliveryNoteId : AlokasiJadwalDriver.DeliveryNoteId,
                                            TujuanPengirimanId: AlokasiJadwalDriver.TujuanPengirimanId,
											DriverCategoryId: AlokasiJadwalDriver.DriverCategoryId,
											DriverId: AlokasiJadwalDriver.DriverId,
											DirectDeliveryBit: AlokasiJadwalDriver.DirectDeliveryBit

											}]);
      },
	  
      delete: function(id) {
        return $http.delete('/api/sales/SDRDeliveryNote',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd