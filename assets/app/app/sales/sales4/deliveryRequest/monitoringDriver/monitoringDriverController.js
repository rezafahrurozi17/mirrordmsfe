angular.module('app')
    .controller('MonitoringDriverController', function ($scope,$filter, $http,$filter, CurrentUser, MonitoringDriverFactory, $timeout) {
        IfGotProblemWithModal();
		
		$scope.MonitoringDriverCalendar=true;
		$scope.MonitoringDriverListTask=false;
		$scope.CategoryDriverSelesai=false;
		
		$scope.TodayDate_Raw=new Date();
		$scope.TodayDate=$scope.TodayDate_Raw.getFullYear()+'-'+('0' + ($scope.TodayDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_Raw.getDate()).slice(-2);
		$scope.CalendarDetailChoice=null;
		
		$scope.user = CurrentUser.user();
        $scope.mMonitoringDriver = null; //Model
        $scope.xRole = { selected: [] };
		
		$scope.optionsDirectDelivery = MonitoringDriverFactory.getOptionDirectDelivery();
		$scope.optionsDirectDeliverySearch = MonitoringDriverFactory.getOptionDirectDelivery();
		$scope.optionsDirectDeliverySearch.push( {"DirectDeliveryBit": null, "DirectDeliveryName": "ALL" });
		$scope.MonitoringDriverListTask_SearchDirectDeliveryBit=null;
		
		MonitoringDriverFactory.getOptionTujuanPengiriman().then(function (res) {
			$scope.optionsTujuanPengiriman = res.data.Result;
			$scope.optionsTujuanPengirimanSearch = res.data.Result;
			$scope.optionsTujuanPengirimanSearch.push( {"TujuanPengirimanId": null, "TujuanPengirimanName": "ALL" });
			$scope.MonitoringDriverListTask_SearchTujuanPengirimanId=null;
			
		});
		
		MonitoringDriverFactory.getRadioCategoryDriver().then(function (res) {
			$scope.optionsCategoryDriver = res.data.Result;
			$scope.CategoryDriverSelesai=true;
			
		});
		
		MonitoringDriverFactory.getDriverListThingy().then(function (res) {
			$scope.optionsDriverThingy = res.data.Result;
			
		});
		
		$scope.SearchWithCalendarChanged = function (calendarThingy) {
			var da_DateToSearch=calendarThingy;
			var da_DateToSearchKirim=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1))+'-'+('0' +da_DateToSearch.getDate()).slice(-2)+'';
			$scope.dt1=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1))+'-'+('0' +da_DateToSearch.getDate()).slice(-2)+'';
			var buttonmonthback=calendarThingy.getMonth()+1;
			$scope.MonthBack=calendarThingy;
			
			$scope.MonitoringDriverCalendarTop=false;
			$scope.MonitoringDriverCalendarDetail=true;
			
			
			if($scope.CategoryDriverSelesai==true)
			{//.setDate(tomorrow.getDate() + 1);
				$scope.CalendarDetailChoice=$scope.dt1;//.setDate(tomorrow.getDate() + 1);
				$scope.CalendarDetailChoicePlus1=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1))+'-'+('0' +(da_DateToSearch.getDate()+1)).slice(-2)+'';
				$scope.CalendarDetailChoicePlus2=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1))+'-'+('0' +(da_DateToSearch.getDate()+2)).slice(-2)+'';
				$scope.CalendarDetailChoicePlus3=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1))+'-'+('0' +(da_DateToSearch.getDate()+3)).slice(-2)+'';
				$scope.CalendarDetailChoicePlus4=da_DateToSearch.getFullYear()+'-'+('0' +(da_DateToSearch.getMonth()+1))+'-'+('0' +(da_DateToSearch.getDate()+4)).slice(-2)+'';
			
				$scope.MonitoringDriverIsiCalendar=[];
				//nanti bikin sesuai kalender yang di klik
				MonitoringDriverFactory.getDataCalendar($scope.CalendarDetailChoice).then(function (res) {
					$scope.MonitoringDriverIsiCalendar = res.data.Result;
					
					
					$scope.TabelListJadwalColumn=[{ name: "null", field: "null" }];
					$scope.TabelListJadwalColumn.splice(0,1);
					$scope.TabelListJadwalColumn.push( {name: "Jam", field: "Jam" });
					for(var i = 0; i < $scope.optionsCategoryDriver.length; ++i)
					{
						$scope.TabelListJadwalColumn.push( {name: $scope.optionsCategoryDriver[i].DriverCategoryName, field: $scope.optionsCategoryDriver[i].DriverCategoryName });
					}
					$scope.TabelListJadwal.columnDefs=$scope.TabelListJadwalColumn;
					//$scope.TabelListJadwal.data=[];
					$scope.TabelListJadwal.data[0]={"Jam":"08:00:00"};
					$scope.TabelListJadwal.data[1]={"Jam":"09:00:00"};
					$scope.TabelListJadwal.data[2]={"Jam":"10:00:00"};
					$scope.TabelListJadwal.data[3]={"Jam":"11:00:00"};
					$scope.TabelListJadwal.data[4]={"Jam":"12:00:00"};
					$scope.TabelListJadwal.data[5]={"Jam":"13:00:00"};
					$scope.TabelListJadwal.data[6]={"Jam":"14:00:00"};
					$scope.TabelListJadwal.data[7]={"Jam":"15:00:00"};
					$scope.TabelListJadwal.data[8]={"Jam":"16:00:00"};
					$scope.TabelListJadwal.data[9]={"Jam":"17:00:00"};
					$scope.TabelListJadwal.data[10]={"Jam":"18:00:00"};
					$scope.TabelListJadwal.data[11]={"Jam":"19:00:00"};
					$scope.TabelListJadwal.data[12]={"Jam":"20:00:00"};
					$scope.TabelListJadwal.data[13]={"Jam":"21:00:00"};
					$scope.TabelListJadwal.data[14]={"Jam":"22:00:00"};
					$scope.TabelListJadwal.data[15]={"Jam":"23:00:00"};
					$scope.TabelListJadwal.data[16]={"Jam":"24:00:00"};
					
					//kalo ga ada for dibawah ini nanti jad undefined
					for(var q = 0; q < $scope.TabelListJadwal.data.length; ++q)
					{
						for(var w = 1; w < $scope.TabelListJadwalColumn.length; ++w)
						{
							$scope.TabelListJadwal.data[q][$scope.TabelListJadwalColumn[w].name]="";
						}
					}
					
					//ini buat populate
					for(var i = 0; i < $scope.MonitoringDriverIsiCalendar.length; ++i)
					{
						for(var x = 0; x < $scope.TabelListJadwal.data.length; ++x)
						{
							if($scope.MonitoringDriverIsiCalendar[i].SentTime.toString()==$scope.TabelListJadwal.data[x]['Jam'].toString())
							{
								$scope.TabelListJadwal.data[x][$scope.MonitoringDriverIsiCalendar[i].DriverCategoryName]+=$scope.MonitoringDriverIsiCalendar[i].CustomerName+"-"+$scope.MonitoringDriverIsiCalendar[i].FrameNo+"\n";
							}
						}
					}
				});
				
			}
			
			
			//yang kirim ke factory pake da_DateToSearchKirim
			

        }
		
		$scope.MonitoringDriverToListTask = function () {
			$scope.MonitoringDriverCalendar=false;
			$scope.MonitoringDriverListTask=true;
			
			MonitoringDriverFactory.getDataCalendar($scope.CalendarDetailChoice).then(function (res) {
				//$scope.TabelListTaskDriver.data = res.data.Result;
				$scope.TabelListTaskDriver = res.data.Result;
			});
			$scope.MonitoringDriverListTask_SearchSentDate=new Date($scope.CalendarDetailChoice);
        }
		
		$scope.MonitoringDriverKembaliKeCalendar = function () {
			$scope.MonitoringDriverCalendar=true;
			$scope.MonitoringDriverListTask=false;
        }
		
		$scope.HideCalendarDetailShowCalendar = function () {
			$scope.MonitoringDriverCalendarTop=true;
			$scope.MonitoringDriverCalendarDetail=false;
        }
		
		$scope.MonitoringDriverAssign = function (selekdata) {
			$scope.MonitoringDriverListTask=false;
			$scope.MonitoringDriverDetailPage=true;
			$scope.TheDriverDetail=angular.copy(selekdata);
			$scope.DisableTheDetailEditing=false;
			angular.element('.ui.modal.ModalMonitoringDriverListTask').modal('hide');
        }
		
		$scope.MonitoringDriverDetail = function (selekdata) {
			$scope.MonitoringDriverListTask=false;
			$scope.MonitoringDriverDetailPage=true;
			$scope.TheDriverDetail=angular.copy(selekdata);
			$scope.DisableTheDetailEditing=true;
			angular.element('.ui.modal.ModalMonitoringDriverListTask').modal('hide');
        }
		
		$scope.MonitoringDriverKembaliKeListTask = function () {
			$scope.MonitoringDriverListTask=true;
			$scope.MonitoringDriverDetailPage=false;
        }
		
		$scope.CalendarDetailChoicePlus1Clicked = function (da_kalender) {
			//alert(new Date($scope.CalendarDetailChoicePlus1));
			$scope.dt1=da_kalender;

			//if($scope.CategoryDriverSelesai==true)
			//{
				$scope.MonitoringDriverIsiCalendar=[];
				//nanti bikin sesuai kalender yang di klik
				MonitoringDriverFactory.getDataCalendar(da_kalender).then(function (res) {
					$scope.MonitoringDriverIsiCalendar = res.data.Result;
					
					
					$scope.TabelListJadwalColumn=[{ name: "null", field: "null" }];
					$scope.TabelListJadwalColumn.splice(0,1);
					$scope.TabelListJadwalColumn.push( {name: "Jam", field: "Jam" });
					for(var i = 0; i < $scope.optionsCategoryDriver.length; ++i)
					{
						$scope.TabelListJadwalColumn.push( {name: $scope.optionsCategoryDriver[i].DriverCategoryName, field: $scope.optionsCategoryDriver[i].DriverCategoryName });
					}
					$scope.TabelListJadwal.columnDefs=$scope.TabelListJadwalColumn;
					//$scope.TabelListJadwal.data=[];
					$scope.TabelListJadwal.data[0]={"Jam":"08:00:00"};
					$scope.TabelListJadwal.data[1]={"Jam":"09:00:00"};
					$scope.TabelListJadwal.data[2]={"Jam":"10:00:00"};
					$scope.TabelListJadwal.data[3]={"Jam":"11:00:00"};
					$scope.TabelListJadwal.data[4]={"Jam":"12:00:00"};
					$scope.TabelListJadwal.data[5]={"Jam":"13:00:00"};
					$scope.TabelListJadwal.data[6]={"Jam":"14:00:00"};
					$scope.TabelListJadwal.data[7]={"Jam":"15:00:00"};
					$scope.TabelListJadwal.data[8]={"Jam":"16:00:00"};
					$scope.TabelListJadwal.data[9]={"Jam":"17:00:00"};
					$scope.TabelListJadwal.data[10]={"Jam":"18:00:00"};
					$scope.TabelListJadwal.data[11]={"Jam":"19:00:00"};
					$scope.TabelListJadwal.data[12]={"Jam":"20:00:00"};
					$scope.TabelListJadwal.data[13]={"Jam":"21:00:00"};
					$scope.TabelListJadwal.data[14]={"Jam":"22:00:00"};
					$scope.TabelListJadwal.data[15]={"Jam":"23:00:00"};
					$scope.TabelListJadwal.data[16]={"Jam":"24:00:00"};
					
					//kalo ga ada for dibawah ini nanti jad undefined
					for(var q = 0; q < $scope.TabelListJadwal.data.length; ++q)
					{
						for(var w = 1; w < $scope.TabelListJadwalColumn.length; ++w)
						{
							$scope.TabelListJadwal.data[q][$scope.TabelListJadwalColumn[w].name]="";
						}
					}
					
					//ini buat populate
					for(var i = 0; i < $scope.MonitoringDriverIsiCalendar.length; ++i)
					{
						for(var x = 0; x < $scope.TabelListJadwal.data.length; ++x)
						{
							if($scope.MonitoringDriverIsiCalendar[i].SentTime.toString()==$scope.TabelListJadwal.data[x]['Jam'].toString())
							{
								$scope.TabelListJadwal.data[x][$scope.MonitoringDriverIsiCalendar[i].DriverCategoryName]+=$scope.MonitoringDriverIsiCalendar[i].CustomerName+"-"+$scope.MonitoringDriverIsiCalendar[i].FrameNo+"\n";
							}
						}
					}
					
					$scope.CalendarDetailChoice=da_kalender;//.setDate(tomorrow.getDate() + 1);
					$scope.CalendarDetailChoicePlus1=new Date($scope.CalendarDetailChoice);
					$scope.CalendarDetailChoicePlus1.setDate($scope.CalendarDetailChoicePlus1.getDate() + 1);
					$scope.CalendarDetailChoicePlus1=$scope.CalendarDetailChoicePlus1.getFullYear()+'-'+('0' +($scope.CalendarDetailChoicePlus1.getMonth()+1))+'-'+('0' +($scope.CalendarDetailChoicePlus1.getDate())).slice(-2)+'';
				
					$scope.CalendarDetailChoicePlus2=new Date($scope.CalendarDetailChoice);
					$scope.CalendarDetailChoicePlus2.setDate($scope.CalendarDetailChoicePlus2.getDate() + 2);
					$scope.CalendarDetailChoicePlus2=$scope.CalendarDetailChoicePlus2.getFullYear()+'-'+('0' +($scope.CalendarDetailChoicePlus2.getMonth()+1))+'-'+('0' +($scope.CalendarDetailChoicePlus2.getDate())).slice(-2)+'';
				
					$scope.CalendarDetailChoicePlus3=new Date($scope.CalendarDetailChoice);
					$scope.CalendarDetailChoicePlus3.setDate($scope.CalendarDetailChoicePlus3.getDate() + 3);
					$scope.CalendarDetailChoicePlus3=$scope.CalendarDetailChoicePlus3.getFullYear()+'-'+('0' +($scope.CalendarDetailChoicePlus3.getMonth()+1))+'-'+('0' +($scope.CalendarDetailChoicePlus3.getDate())).slice(-2)+'';
				
					$scope.CalendarDetailChoicePlus4=new Date($scope.CalendarDetailChoice);
					$scope.CalendarDetailChoicePlus4.setDate($scope.CalendarDetailChoicePlus4.getDate() + 4);
					$scope.CalendarDetailChoicePlus4=$scope.CalendarDetailChoicePlus4.getFullYear()+'-'+('0' +($scope.CalendarDetailChoicePlus4.getMonth()+1))+'-'+('0' +($scope.CalendarDetailChoicePlus4.getDate())).slice(-2)+'';
				});
			//}
        }
		
		$scope.MonitoringDriverSaveAssign = function () {
			MonitoringDriverFactory.update($scope.TheDriverDetail).then(function () {
				MonitoringDriverFactory.getDataCalendar($scope.CalendarDetailChoice).then(function (res) {
					//$scope.TabelListTaskDriver.data = res.data.Result;
					$scope.TabelListTaskDriver = res.data.Result;
					$scope.MonitoringDriverKembaliKeListTask();
				});
			});
        }
		
		$scope.MonitoringDriverListTask_Search_Btn_Clicked = function () {
			
			var Da_TipePengirimanId=null;
			var Da_TujuanPengirimanId=0;
			var Da_FrameNo="";
			
			try
			{
				if((typeof $scope.MonitoringDriverListTask_SearchDirectDeliveryBit!="undefined"))
				{
					Da_TipePengirimanId=$scope.MonitoringDriverListTask_SearchDirectDeliveryBit;
				}
				else
				{
					Da_TipePengirimanId=null;
					$scope.MonitoringDriverListTask_SearchDirectDeliveryBit=null;
				}
			}
			catch(Except)
			{
				Da_TipePengirimanId=null;
				$scope.MonitoringDriverListTask_SearchDirectDeliveryBit=null;
			}
			
			try
			{
				if((typeof $scope.MonitoringDriverListTask_SearchTujuanPengirimanId!="undefined"))
				{
					Da_TujuanPengirimanId=$scope.MonitoringDriverListTask_SearchTujuanPengirimanId;
				}
				else
				{
					Da_TujuanPengirimanId=null;
					$scope.MonitoringDriverListTask_SearchTujuanPengirimanId=null;
				}
			}
			catch(Except)
			{
				Da_TujuanPengirimanId=null;
				$scope.MonitoringDriverListTask_SearchTujuanPengirimanId=null;
			}
			
			if((typeof $scope.MonitoringDriverListTask_SearchFrameNo!="undefined"))
			{
				Da_FrameNo=$scope.MonitoringDriverListTask_SearchFrameNo;
			}
			else
			{
				Da_FrameNo="";
			}

			var Da_SentDate=$scope.MonitoringDriverListTask_SearchSentDate.getFullYear()+'-'+('' +($scope.MonitoringDriverListTask_SearchSentDate.getMonth()+1))+'-'+('0' +$scope.MonitoringDriverListTask_SearchSentDate.getDate()).slice(-2)+'';

			
			MonitoringDriverFactory.getListTaskDriver(Da_TipePengirimanId,Da_TujuanPengirimanId,Da_SentDate,Da_FrameNo).then(function (res) {
					//$scope.TabelListTaskDriver.data = res.data.Result;
					$scope.TabelListTaskDriver = res.data.Result;
				});
        }
		
		$scope.MonitoringDriverListTaskBukaModal = function (selekAction) {
			//IfGotProblemWithModal();
			$scope.Da_Selek_MonitoringDriver=angular.copy(selekAction);
			
			angular.element('.ui.modal.ModalMonitoringDriverListTask').modal('setting',{closable:false}).modal('show');
        }
		
		$scope.MonitoringDriverListTaskTutupModal = function () {
			angular.element('.ui.modal.ModalMonitoringDriverListTask').modal('hide');
			$scope.TheDriverDetail={};
			$scope.Da_Selek_MonitoringDriver={};
			
        }
		
		
		
		//Begin Calendar
        $scope.refreshCalendar = true;
        $scope.today = function () {
            $scope.dt1 = new Date();
			$scope.SearchWithCalendarChanged($scope.dt1);
			$scope.refreshCalendar = false;
			$timeout(function () { $scope.refreshCalendar = true }, 0);
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt1 = null;
        };

        $scope.options = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.toggleMin = function () {
            $scope.options.minDate = $scope.options.minDate ? null : new Date();
        };

        $scope.toggleMin();

        $scope.setDate = function (year, month, day) {
            $scope.dt1 = new Date(year, month, day);
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date(tomorrow);
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        

        $scope.testDate = {
            date: new Date(2017, 2, 11),
            status: 'firstDate'
        };

        $scope.testDate2 = {
            date: new Date(2017, 2, 15),
            status: 'firstDate'
        };

       

        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        $scope.events.push($scope.testDate);


        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        //End Calendar
		$scope.MonitoringDriverCalendarTop=true;
		$scope.MonitoringDriverCalendarDetail=false;
		
		var btnActionPilihTaskDriver = '<a style="color:blue;"  ng-click="grid.appScope.MonitoringDriverAssign(row.entity)">Assign</a>    <a style="color:blue;" ng-click="grid.appScope.MonitoringDriverDetail(row.entity)">Detail</a>';
		
		
		
		$scope.TabelListTaskDriver=[];
		
		$scope.TabelListJadwalColumn=[];
		$scope.TabelListJadwal = {
            enableSorting: true,
            enableFiltering: true,
            columnDefs: $scope.TabelListJadwalColumn
        };
		
		
    });
