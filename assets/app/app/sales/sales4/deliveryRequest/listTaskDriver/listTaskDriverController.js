angular.module('app')
    .controller('ListTaskDriverController', function ($rootScope,$scope, $http, CurrentUser, ListTaskDriverFactory, $timeout, bsNotify) {

		$scope.filterData = {
            defaultFilter: [
                { name: 'Nama Pelanggan', value: 'CustomerName' },
               	{ name: 'Tujuan Pengiriman', value: 'TujuanPengirimanName' },
				{ name: 'Jenis Tugas', value: 'NamaJenisTugas' },
				{ name: 'No. Rangka', value: 'FrameNo' },
				{ name: 'Nama Pemilik', value: 'CustomerOwnerName' }
            ],
            advancedFilter: [
                { name: 'Nama Pelanggan', value: 'CustomerName', typeMandatory: 0 },
                { name: 'Tujuan Pengiriman', value: 'TujuanPengirimanName', typeMandatory: 0 },
				{ name: 'Jenis Tugas', value: 'NamaJenisTugas', typeMandatory: 0 },
				{ name: 'No. Rangka', value: 'FrameNo' },
				{ name: 'Nama Pemilik', value: 'CustomerOwnerName' }
            ]
		};

		
		
		$scope.ListTaskDriverMain=true;
		$scope.ListTaskDriverDetail=false;

		$scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 30;
		$scope.mytime.setMinutes('00');
		
		// $scope.mytimeDriver2 = new Date();
        // $scope.hstep = 1;
        // $scope.mstep = 15;
		
        ListTaskDriverFactory.getRadioCategoryDriver().then(function (res) {
			$scope.optionsCategoryDriver = res.data.Result;
		});

        ListTaskDriverFactory.getOptionTujuanPengiriman().then(function (res) {
			$scope.optionsTujuanPengiriman = res.data.Result;
		});
		
		
		
		$scope.ListTaskDriverBackToMain = function () {
			$scope.DisableTheDetailEditing=true;
			$scope.ListTaskDriverMain=true;
			$scope.ListTaskDriverDetail=false;
			
		}
		
		$scope.SelectDriver = function (selected) {
			
			// if(selected.DriverCategoryName == 'PDS'){
				// ListTaskDriverFactory.getDriverListThingy().then(function (res) {
					// $scope.optionsDriverThingy = res.data.Result;
					// $scope.optionsDriverThingy = $scope.optionsDriverThingy.filter(function (type) { return (type.DriverOutsource == false); })
				// });
				
			// }else {
				// ListTaskDriverFactory.getDriverListThingy().then(function (res) {
					// $scope.optionsDriverThingy = res.data.Result;
					// $scope.optionsDriverThingy = $scope.optionsDriverThingy.filter(function (type) { return (type.DriverOutsource == true); })
				// });
			// }
			
			//nanti diganti lagi tunggu keresmian
			if(selected.DriverCategoryName == 'Outsource'){
				ListTaskDriverFactory.getDriverListThingy().then(function (res) {
					$scope.optionsDriverThingy = res.data.Result;
					$scope.optionsDriverThingy = $scope.optionsDriverThingy.filter(function (type) { return (type.DriverOutsource == true); })
				});
				
			}else {
				ListTaskDriverFactory.getDriverListThingy().then(function (res) {
					$scope.optionsDriverThingy = res.data.Result;
					$scope.optionsDriverThingy = $scope.optionsDriverThingy.filter(function (type) { return (type.DriverOutsource == false); })
				});
			}
		}
		
		$scope.ListTaskDriverChangedOutTime = function (brubah_wujud) {
			console.log("setan1",brubah_wujud);
			$scope.mytime=angular.copy(brubah_wujud);
			console.log("setan2",$scope.mytime);
		}
		
		$scope.ListTaskDriverSaveAssign = function () {
			$scope.selected_data.OutTime = angular.copy($scope.mytime);
			try {
			$scope.selected_data.OutTime = $scope.selected_data.OutTime.getFullYear() + '-'
			+ ('0' + ($scope.selected_data.OutTime.getMonth() + 1)).slice(-2) + '-'
			+ ('0' + $scope.selected_data.OutTime.getDate()).slice(-2) + 'T'
			+ (($scope.selected_data.OutTime.getHours() < '10' ? '0' : '') + $scope.selected_data.OutTime.getHours()) + ':'
			+ (($scope.selected_data.OutTime.getMinutes() < '10' ? '0' : '') + $scope.selected_data.OutTime.getMinutes()) + ':00';
			
			
			
			}
			catch (e1) {
				$scope.selected_data.OutTime = null;
			}
			
			
			try {
			$scope.selected_data.DateBackEstimatedTime = $scope.selected_data.DateBackEstimatedTime.getFullYear() + '-'
			+ ('0' + ($scope.selected_data.DateBackEstimatedTime.getMonth() + 1)).slice(-2) + '-'
			+ ('0' + $scope.selected_data.DateBackEstimatedTime.getDate()).slice(-2) + 'T'
			+ (($scope.selected_data.DateBackEstimatedTime.getHours() < '10' ? '0' : '') + $scope.selected_data.DateBackEstimatedTime.getHours()) + ':'
			+ (($scope.selected_data.DateBackEstimatedTime.getMinutes() < '10' ? '0' : '') + $scope.selected_data.DateBackEstimatedTime.getMinutes()) + ':00';
			}
			catch (e1) {
				$scope.selected_data.DateBackEstimatedTime = null;
			}
			
		
			if ($scope.selected_data.NamaJenisTugas == 'Pengiriman') {
				$scope.selected_data.DateBackEstimatedTime = null;
			}else
			if ($scope.selected_data.NamaJenisTugas == 'Pengambilan') {
				$scope.selected_data.OutTime = null;
			}
			console.log("setan3",$scope.selected_data.OutTime);

			ListTaskDriverFactory.update($scope.selected_data).then(function () {
						ListTaskDriverFactory.getData("").then(
							function(res){
								$rootScope.$emit("RefreshDirective", {});
								$scope.bind_data.data = res.data.Result;
								$scope.loading=false;
								bsNotify.show(
									{
										title: "Berhasil",
										content: "Data berhasil disimpan.",
										type: 'success'
									}
								);
								$scope.ListTaskDriverBackToMain();
								return res.data;
							},
							function (err) {
							   
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							}
						);
					},
							function (err) {
							   
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
							});
		
			
        }

        $scope.lihatDetailListTaskDriver = function () {
			$scope.DisableTheDetailEditing=true;
			$scope.ListTaskDriverMain=false;
			$scope.ListTaskDriverDetail=true;
			if($scope.selected_data.OutTime == null){
				$scope.mytime = new Date();
				$scope.mytime.setHours('00');
				$scope.mytime.setMinutes('00');
			}else{
				$scope.mytime = new Date($scope.selected_data.OutTime);
			}
			
			if($scope.selected_data.DriverCategoryName == 'Outsource'){
				ListTaskDriverFactory.getDriverListThingy().then(function (res) {
					$scope.optionsDriverThingy = res.data.Result;
					$scope.optionsDriverThingy = $scope.optionsDriverThingy.filter(function (type) { return (type.DriverOutsource == true); })
				});
				
			}else {
				ListTaskDriverFactory.getDriverListThingy().then(function (res) {
					$scope.optionsDriverThingy = res.data.Result;
					$scope.optionsDriverThingy = $scope.optionsDriverThingy.filter(function (type) { return (type.DriverOutsource == false); })
				});
			}
			
			angular.element('.ui.modal.ListTaskDriverForm').modal('hide');
        }
		
		 $scope.AssignListTaskDriver = function () {
			$scope.DisableTheDetailEditing=false;
			$scope.ListTaskDriverMain=false;
			$scope.ListTaskDriverDetail=true;
			$scope.mytime = new Date();
			$scope.mytime.setMinutes('00');
			angular.element('.ui.modal.ListTaskDriverForm').modal('hide');
			
			if($scope.selected_data.DriverCategoryName == 'Outsource'){
				ListTaskDriverFactory.getDriverListThingy().then(function (res) {
					$scope.optionsDriverThingy = res.data.Result;
					$scope.optionsDriverThingy = $scope.optionsDriverThingy.filter(function (type) { return (type.DriverOutsource == true); })
				});
				
			}else {
				ListTaskDriverFactory.getDriverListThingy().then(function (res) {
					$scope.optionsDriverThingy = res.data.Result;
					$scope.optionsDriverThingy = $scope.optionsDriverThingy.filter(function (type) { return (type.DriverOutsource == false); })
				});
			}
			
        }
        

    });

