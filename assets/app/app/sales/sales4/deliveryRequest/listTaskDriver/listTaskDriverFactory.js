angular.module('app')
  .factory('ListTaskDriverFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
                

                var res=$http.get('/api/sales/SDRDeliveryNote_ListTaskDriverView' + param);
				return res;
            },
	  
	  getDriverListThingy: function() {
        var res=$http.get('/api/sales/MProfileDriverPDS');
        //console.log('res=>',res);
        return res;
      },
	  
	  getRadioCategoryDriver: function() {
        var res=$http.get('/api/sales/PCategoryDriver');
        //console.log('res=>',res);
        return res;
      },
	  
	  getOptionTujuanPengiriman: function() {
        var res=$http.get('/api/sales/PCategoryTujuanPengiriman');
        //console.log('res=>',res);
        return res;
      },
	  
	  getOptionDirectDelivery: function () {
        
        var da_json = [
          { DirectDeliveryBit: true, DirectDeliveryName: "Direct Delivery From PDC" },
		  { DirectDeliveryBit: false, DirectDeliveryName: "Normal Delivery" }
        ];
        var res=da_json
        return res;
      },
	  
      create: function(SampleUITemplateFactory) {
        return $http.post('/api/fw/Role', [{
                                            //AppId: 1,
                                            SalesProgramName: SampleUITemplateFactory.SalesProgramName}]);
      },
      update: function(ListTaskDriver){
        return $http.put('/api/sales/SDRDeliveryNote_ListTaskDriverView', [ListTaskDriver]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd