angular.module('app')
  .factory('DeliveryNoteFactory', function($http, $httpParamSerializer, CurrentUser) {
		var currentUser = CurrentUser.user;

			function fixDate (date) {
			if (date != null || date != undefined) {
				var fix = date.getFullYear() + '-' +
					('0' + (date.getMonth() + 1)).slice(-2) + '-' +
					('0' + date.getDate()).slice(-2) + 'T' +
					((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
					((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
					((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
				return fix;
			} else {
				return null;
			}
		};
		
		


  return {
			getData: function(filter) {
					var param = $httpParamSerializer(filter);
					//var res=$http.get('/api/sales/SDRDeliveryNote/?start=1&limit=100&'+param);
					var res=$http.get('/api/sales/SDRDeliveryNote/Filter/?'+param);
					return res;
			},
				
			getDataVehicleModel : function (){
					var res=$http.get('/api/sales/MUnitVehicleModelTomas');
					return res;
			},
				
				getDataVehicleType : function (){
					var res=$http.get('/api/sales/MUnitVehicleTypeTomas');
					return res;
				},
				
				getDataVehicleTypeColors : function (){
						var res=$http.get('/api/sales/MUnitVehicleTypeColorJoinTomas');
						return res;
				},
				
				getDataFrameNo: function() {
						var res=$http.get('/api/sales/GetDaftarNoRangka/?start=1&limit=100000&VehicleModelId=null&VehicleTypeId=null&ColorId=null');
						return res;
				},
				
				getDataFrameNoSearch: function(da_VehicleModelId,da_VehicleTypeId,da_ColorId,da_FrameNo) {
						var res=$http.get('/api/sales/GetDaftarNoRangka?start=1&limit=100000'+da_VehicleModelId+da_VehicleTypeId+da_ColorId+da_FrameNo);
						
						return res;
				},
				
				getOptionTujuanPengiriman: function() {
						var res=$http.get('/api/sales/PCategoryTujuanPengiriman');
						
						return res;
				},
				
				getOptionJenisTugas: function() {
						//var res=$http.get('/api/sales/PCategoryTujuanPengiriman');
						var da_json = [
								{ JenisTugasId: 1, JenisTugasName: "Pengiriman" },
								{ JenisTugasId: 2, JenisTugasName: "Pengambilan" }
						];
						var res=da_json
						return res;
				},
				
				
				getRadioPengirimanUnit: function() {
						var res=$http.get('/api/sales/PCategoryCaroseriesDestination');
						
						return res;
				},
				
				getRadioCategoryDriver: function() {
						var res=$http.get('/api/sales/PCategoryDriver');
						
						return res;
				},
				
				getOptionsVendor: function(VendorTypeId) {
						var res=$http.get('/api/sales/MVendor?filterdata=BusinessUnitId|'+VendorTypeId);
						
						return res;
				},
				
				create: function(DeliveryNote) {
					var Da_DateBackEstimated=null;
					try
					{
						Da_DateBackEstimated= DeliveryNote.DateBackEstimated.getFullYear()+'-'
																	+('0' + (DeliveryNote.DateBackEstimated.getMonth()+1)).slice(-2)+'-'
																	+('0' + DeliveryNote.DateBackEstimated.getDate()).slice(-2)+'T'
																	+((DeliveryNote.DateBackEstimated.getHours()<'10'?'0':'')+ DeliveryNote.DateBackEstimated.getHours())+':'
																	+((DeliveryNote.DateBackEstimated.getMinutes()<'10'?'0':'')+ DeliveryNote.DateBackEstimated.getMinutes())+':'
																	+((DeliveryNote.DateBackEstimated.getSeconds()<'10'?'0':'')+ DeliveryNote.DateBackEstimated.getSeconds());
					}
					catch(e1)
					{
						Da_DateBackEstimated=null;
					}
					console.log('DeliveryNote ===>',DeliveryNote);

					// DeliveryNote.OutDate = fixdate(DeliveryNote.OutDate);

					return $http.post('/api/sales/SDRDeliveryNote',[DeliveryNote])
									// DRId: DeliveryNote.DRId,
									// FrameNo: DeliveryNote.FrameNo,
									// VehicleTypeColorId: DeliveryNote.VehicleTypeColorId,
									// TujuanPengirimanId: DeliveryNote.TujuanPengirimanId,
									// JenisTugasId: DeliveryNote.JenisTugasId,
									// DriverCategoryId: DeliveryNote.DriverCategoryId,
									// DriverId: DeliveryNote.DriverId,
									// OutDate: fixdate(DeliveryNote.OutDate),
									// SentDate: DeliveryNote.SentDate,
									// SentTime: DeliveryNote.SentTime,
									// DirectDeliveryBit: DeliveryNote.DirectDeliveryBit,
									// VehicleModelId: DeliveryNote.VehicleModelId,
									// VehicleTypeId: DeliveryNote.VehicleTypeId,
									// VehicleTypeColorId: DeliveryNote.VehicleTypeColorId,
									// ColorId: DeliveryNote.ColorId,
									// DeliveryCaroseriesDestinationId: DeliveryNote.DeliveryCaroseriesDestinationId,
									// DeliveryCaroseriesVendorId: DeliveryNote.DeliveryCaroseriesVendorId,
									// SentAddress: DeliveryNote.SentAddress,
									// DateBackEstimated: Da_DateBackEstimated,
									
									// }]);
				},

				update: function(DeliveryNote){
					return $http.put('/api/sales/SDRDeliveryNote', [DeliveryNote])
									// OutletId : DeliveryNote.OutletId,
									// DeliveryNoteId: DeliveryNote.DeliveryNoteId,
									// DeliveryNoteNo: DeliveryNote.DeliveryNoteNo,
									// DRId: DeliveryNote.DRId,
									// FrameNo: DeliveryNote.FrameNo,
									// VehicleTypeColorId: DeliveryNote.VehicleTypeColorId,
									// TujuanPengirimanId: DeliveryNote.TujuanPengirimanId,
									// JenisTugasId: DeliveryNote.JenisTugasId,
									// DriverCategoryId: DeliveryNote.DriverCategoryId,
									// DriverId: DeliveryNote.DriverId,
									// OutDate: fixDate(DeliveryNote.OutDate),
									// ReceiverName: DeliveryNote.ReceiverName,
									// SentDate: DeliveryNote.SentDate,
									// SentTime: DeliveryNote.SentTime,
									// DirectDeliveryBit: DeliveryNote.DirectDeliveryBit,
									// VehicleModelId: DeliveryNote.VehicleModelId,
									// VehicleTypeId: DeliveryNote.VehicleTypeId,
									// VehicleTypeColorId: DeliveryNote.VehicleTypeColorId,
									// ColorId: DeliveryNote.ColorId,
									// DeliveryCaroseriesDestinationId: DeliveryNote.DeliveryCaroseriesDestinationId,
									// DeliveryCaroseriesVendorId: DeliveryNote.DeliveryCaroseriesVendorId,
									// SentAddress: DeliveryNote.SentAddress,
									// DateBackEstimated: fixDate(DeliveryNote.DateBackEstimated),											
									// }]);
				},

				delete: function(id) {
						return $http.delete('/api/sales/SDRDeliveryNote',{data:id,headers: {'Content-Type': 'application/json'}});
				},

  }
  });
 //ddd