angular.module('app')
    .controller('DeliveryNoteController', function ($scope,$stateParams, $http, CurrentUser, DeliveryNoteFactory, $timeout,PrintRpt,bsNotify) {
        //IfGotProblemWithModal();
		//----------------------------------
        // Start-Up
        //----------------------------------
		$scope.TodayDate_Raw=new Date();
		$scope.TodayDate=$scope.TodayDate_Raw.getFullYear()+'-'+('0' + ($scope.TodayDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_Raw.getDate()).slice(-2);
		
		$scope.ShowAddDeliveryNoteBtn=true;
		$scope.ShowDeliveryNoteMain=true;
		$scope.ShowDeliveryNoteDetail=false;
		$scope.ShowDeliveryNoteCetak=false;
		$scope.DeliveryNoteOperation="";

		$scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
		};
		
		$scope.dateOptions02 = {
																											// formatYear: 'yyyy-mm-dd',
																											// startingDay: 1,
																											// minMode: 'year'
																											startingDay: 1,
																											format: "dd/MM/yyyy",
																											disableWeekend: 0,
																											minDate: new Date(),
																									};
		
		$scope.mDeliveryNoteSearch={
																															VehicleModelId:null, 
																															VehicleTypeId:null, 
																															ColorId:null, 
																															TujuanPengirimanId:null, 
																															OutDate:null
																													};
		
		$scope.mDeliveryNoteSearch.OutDate=new Date();
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.ModalFrame').remove();
		});
		
		$scope.$on('$viewContentLoaded', function () {
						//$scope.loading=true; tadinya true
						$scope.loading = false;
						$scope.gridData = [];
		});
		//----------------------------------
		// Initialization
		//----------------------------------
		$scope.user = CurrentUser.user();
		$scope.tab = $stateParams.tab;
		$scope.breadcrums = {};
		$scope.mDeliveryNote = null; //Model
		$scope.xRole = { selected: [] };
		$scope.disabledSaveTheDeliveryNote = false;

		//----------------------------------
		// Get Data
		//----------------------------------
		var gridData = [];
        
		$scope.getData = function() {
			try
			{
				var da_OutDate=$scope.mDeliveryNoteSearch.OutDate;
				$scope.mDeliveryNoteSearch.OutDate= da_OutDate.getFullYear()+'-'
													+('0' + (da_OutDate.getMonth()+1)).slice(-2)+'-'
													+('0' + da_OutDate.getDate()).slice(-2);
			}
			catch(exceptionthingy2)
			{
				$scope.mDeliveryNoteSearch.OutDate=null;
			}
			
									DeliveryNoteFactory.getData($scope.mDeliveryNoteSearch).then(
					function(res){
						$scope.grid.data = res.data.Result;
						$scope.loading=false;
						
						$scope.ShowKaroseriFoot=false;
						return res.data;
					},
					function(err){
						
					}
				);
		}
		
		function roleFlattenAndSetLevel(node, lvl) {
						for (var i = 0; i < node.length; i++) {
										node[i].$$treeLevel = lvl;
										gridData.push(node[i]);
										if (node[i].child.length > 0) {
														roleFlattenAndSetLevel(node[i].child, lvl + 1)
										} else {

										}
						}
						return gridData;
		}

		$scope.selectRole = function (rows) {
			$timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
		}


		$scope.onSelectRows = function (rows) {
			console.log("onSelectRows=>", rows);
		}
		
		DeliveryNoteFactory.getDataVehicleModel().then(function (res){
			$scope.VehicleModel = res.data.Result;
		});
         
		DeliveryNoteFactory.getDataVehicleType().then(function (res){
				$scope.VehicleType = res.data.Result;
		});

		DeliveryNoteFactory.getDataVehicleTypeColors().then(function (res){
				$scope.VehicleColor = res.data.Result;
		});
		
		$scope.filtertipe = function(selected){
				$scope.vehicleTypefilter = $scope.VehicleType.filter(function(type){ return (type.VehicleModelId == selected); })
		}

		$scope.filterColor = function(selected){
				$scope.vehicleTypecolorfilter = $scope.VehicleColor.filter(function(type){ return (type.VehicleTypeId == selected); })
		}
		
		$scope.ResetSearch = function(){
   DeliveryNoteFactory.getDataFrameNo().then(function (res) {
				$scope.gridFrameNo.data = res.data.Result;
				$scope.FrameWarnaSearch=null;
				$scope.FrameTipeSearch=null;
				$scope.FrameModelSearch=null;
				$scope.FrameNoSearch="";
			});
  }
		

		$scope.fixDate = function (date) {
			if (date != null || date != undefined) {
				var fix = date.getFullYear() + '-' +
					('0' + (date.getMonth() + 1)).slice(-2) + '-' +
					('0' + date.getDate()).slice(-2) + 'T' +
					((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
					((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
					((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
				return fix;
			} else {
				return null;
			}
		};

		$scope.addNewDeliveryNote = function(){
			$scope.mDeliveryNote={};
			$scope.ShowAddDeliveryNoteBtn=false;
			$scope.ShowDeliveryNoteMain=false;
			$scope.ShowDeliveryNoteDetail=true;
			$scope.DeliveryNoteOperation="Insert";
			$scope.breadcrums.title="Tambah";
			$scope.dateOptions02.minDate = new Date();
		}
		
		$scope.editDeliveryNote = function(selected_deliverynote){
			$scope.mDeliveryNote=angular.copy(selected_deliverynote);
			$scope.ShowAddDeliveryNoteBtn=false;
			$scope.ShowDeliveryNoteMain=false;
			$scope.ShowDeliveryNoteDetail=true;
			$scope.DeliveryNoteOperation="Update";
			$scope.breadcrums.title="Ubah";
			$scope.TujuanPengirimanSelectedChanged();
		}
		
		$scope.BataladdNewDeliveryNote = function(){
			$scope.mDeliveryNote={};
			$scope.ShowAddDeliveryNoteBtn=true;
			$scope.ShowDeliveryNoteMain=true;
			$scope.ShowDeliveryNoteDetail=false;
			$scope.DeliveryNoteOperation="";
		}
		
		$scope.SaveTheDeliveryNote = function(){
			$scope.disabledSaveTheDeliveryNote = true;
			$scope.DaDeliveryNoteErrorCheck=false;
			try
			{
				
				if($scope.mDeliveryNote['FrameNo'] != null && $scope.mDeliveryNote['TujuanPengirimanId'] != null &&  $scope.mDeliveryNote['OutDate']!= null )
				{	
					
					if($scope.ShowKaroseriFoot==false)
					{	
						if($scope.mDeliveryNote.TujuanPengirimanName == "Repair")
						{
							
							if((typeof $scope.mDeliveryNote['DeliveryCaroseriesVendorId']!="undefined") && $scope.mDeliveryNote['DateBackEstimated']!=null && (typeof $scope.mDeliveryNote['DateBackEstimated']!="undefined"))
							{
								$scope.DaDeliveryNoteErrorCheck=true;
							}
							else
							{
								$scope.DaDeliveryNoteErrorCheck=false;
							}
						}
						else if($scope.mDeliveryNote.TujuanPengirimanName == "Pameran")
						{
							
							if($scope.mDeliveryNote['DateBackEstimated']!=null && (typeof $scope.mDeliveryNote['DateBackEstimated']!="undefined"))
							{
								$scope.DaDeliveryNoteErrorCheck=true;
							}
							else
							{
								$scope.DaDeliveryNoteErrorCheck=false;
							}
						}
						else
						{	
							if($scope.mDeliveryNote['OutDate']!=null)
							{
								$scope.DaDeliveryNoteErrorCheck=true;
								
							}
							
							
						}
					}
					else if($scope.ShowKaroseriFoot==true)
					{
						if((typeof $scope.mDeliveryNote['DeliveryCaroseriesVendorId']!="undefined")&& (typeof $scope.mDeliveryNote['DateBackEstimated']!="undefined") &&(typeof $scope.mDeliveryNote['DeliveryCaroseriesDestinationId']!="undefined")&& $scope.mDeliveryNote['DeliveryCaroseriesDestinationId']!=null && $scope.mDeliveryNote['DateBackEstimated']!=null)
						{
							$scope.DaDeliveryNoteErrorCheck=true;
						}
					}
				}	
			}
			catch(ada_exception)
			{
				//alert(ada_exception);
				bsNotify.show(
						{
							title: "Error Message",
							content: "Pastikan Semua Data Terisi Dengan Benar!",
							type: 'warning'
						}
					);
					$scope.disabledSaveTheDeliveryNote = false;
			}
			
		
			if($scope.DaDeliveryNoteErrorCheck==true)
			{
				
				if($scope.DeliveryNoteOperation=="Insert")
				{

					$scope.mDeliveryNote.OutDate = $scope.fixDate($scope.mDeliveryNote.OutDate);
					DeliveryNoteFactory.create($scope.mDeliveryNote).then(function () {
						DeliveryNoteFactory.getData($scope.mDeliveryNoteSearch).then(
							function(res){
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								
								$scope.ShowKaroseriFoot=false;
								$scope.BataladdNewDeliveryNote();
								bsNotify.show(
									{
										title: "Message",
										content: "Data Berhasil Disimpan!",
										type: 'success'
									}
								);
								$scope.disabledSaveTheDeliveryNote = false;
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledSaveTheDeliveryNote = false;
							}
						);
					},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledSaveTheDeliveryNote = false;
							});
				}
				else if($scope.DeliveryNoteOperation=="Update")
				{
					$scope.mDeliveryNote.OutDate = $scope.fixDate($scope.mDeliveryNote.OutDate);
					$scope.mDeliveryNote.DateBackEstimated = $scope.fixDate($scope.mDeliveryNote.DateBackEstimated); 

					DeliveryNoteFactory.update($scope.mDeliveryNote).then(function () {
						
						DeliveryNoteFactory.getData($scope.mDeliveryNoteSearch).then(
							function(res){
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								
								$scope.ShowKaroseriFoot=false;
								$scope.BataladdNewDeliveryNote();
								bsNotify.show(
									{
										title: "Message",
										content: "Data Berhasil Disimpan!",
										type: 'success'
									}
								);
								$scope.disabledSaveTheDeliveryNote = false;
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledSaveTheDeliveryNote = false;
							}
						);
					},
							function(err){
								bsNotify.show(
									{
										title: "Gagal",
										content: err.data.Message,
										type: 'danger'
									}
								);
								$scope.disabledSaveTheDeliveryNote = false;
							});
				}
			}
			else
			{
				bsNotify.show(
						{
							title: "Error Message",
							content: "Pastikan Semua Data Terisi Dengan Benar!",
							type: 'warning'
						}
					);
					$scope.disabledSaveTheDeliveryNote = false;
			}
			
		}
		
		$scope.SearchDaFrame = function(){
				//DeliveryNoteFactory.getDataFrameNoSearch($scope.FrameModelSearch,$scope.FrameTipeSearch,$scope.FrameWarnaSearch,$scope.FrameNoSearch).then(function (res) {
			if(((typeof $scope.FrameNoSearch=="undefined")||$scope.FrameNoSearch==null||$scope.FrameNoSearch=="")&&((typeof $scope.FrameModelSearch=="undefined")||$scope.FrameModelSearch==null))
			{
				bsNotify.show(
						{
							title: "Error Message",
							content: "Nomor Rangka atau model kendaraan harus diisi",
							type: 'warning'
						}
					);
			}
			else
			{
				var DeliveryNoteModelSearchFrameNo="";
				var DeliveryNoteTipeSearchFrameNo="";
				var DeliveryNoteWarnaSearchFrameNo="";
				var DeliveryNoteFrameNoSearchFrameNo="";
				
				try
				{
					if($scope.FrameModelSearch.toString().length>0)
					{
						DeliveryNoteModelSearchFrameNo="&VehicleModelId="+$scope.FrameModelSearch;
					}
					else
					{
						DeliveryNoteModelSearchFrameNo="&VehicleModelId=null";
					}
				}
				catch(e1)
				{
					DeliveryNoteModelSearchFrameNo="&VehicleModelId=null";
				}
				
				try
				{
					if($scope.FrameTipeSearch.toString().length>0)
					{
						DeliveryNoteTipeSearchFrameNo="&VehicleTypeId="+$scope.FrameTipeSearch;
					}
					else
					{
						DeliveryNoteTipeSearchFrameNo="&VehicleTypeId=null";
					}
				}
				catch(e2)
				{
					DeliveryNoteTipeSearchFrameNo="&VehicleTypeId=null";
				}
				
				try
				{
					if($scope.FrameWarnaSearch.toString().length>0)
					{
						DeliveryNoteWarnaSearchFrameNo="&ColorId="+$scope.FrameWarnaSearch;
					}
					else
					{
						DeliveryNoteWarnaSearchFrameNo="&ColorId=null";
					}
				}
				catch(e3)
				{
					DeliveryNoteWarnaSearchFrameNo="&ColorId=null";
				}
				
				try
				{
					if($scope.FrameNoSearch.toString().length>0)
					{
						DeliveryNoteFrameNoSearchFrameNo="&filterData=FrameNo|"+$scope.FrameNoSearch;
					}
					else
					{
						DeliveryNoteFrameNoSearchFrameNo="&filterData=null";
					}
				}
				catch(e4)
				{
					DeliveryNoteFrameNoSearchFrameNo="&filterData=null";
				}
				
				DeliveryNoteFactory.getDataFrameNoSearch(DeliveryNoteModelSearchFrameNo,DeliveryNoteTipeSearchFrameNo,DeliveryNoteWarnaSearchFrameNo,DeliveryNoteFrameNoSearchFrameNo).then(function (res) {			
				$scope.gridFrameNo.data = res.data.Result;
					
				});
			}
  }
		
		$scope.TujuanPengirimanSelectedChanged = function () {
			// DeliveryNoteFactory.getOptionJenisTugas().then(function (res) {
				// $scope.optionsJenisTugas = res.data.Result;	
				// return $scope.optionsJenisTugas;
			// });
			$scope.DisableDeliveryNoteJenisTugas=false;
			$scope.optionsJenisTugas = DeliveryNoteFactory.getOptionJenisTugas();
		
			$scope.DeliveryNoteNotAllowChangeAddress=true;
			for(var i = 0; i < $scope.optionsTujuanPengiriman.length; ++i)
			{
				if($scope.optionsTujuanPengiriman[i].TujuanPengirimanId == $scope.mDeliveryNote.TujuanPengirimanId)
				{
					if($scope.optionsTujuanPengiriman[i].TujuanPengirimanName =="Karoseri" ||$scope.optionsTujuanPengiriman[i].TujuanPengirimanName =="Repair"||$scope.optionsTujuanPengiriman[i].TujuanPengirimanName =="Pameran")
					{
						$scope.DeliveryNoteShowEstimasiTanggalKembali=true;
					}
					else
					{
						$scope.mDeliveryNote.DateBackEstimated=null;
						$scope.mDeliveryNote.DeliveryCaroseriesVendorId=null;
						$scope.DeliveryNoteShowEstimasiTanggalKembali=false;
					}
					
					
					if($scope.optionsTujuanPengiriman[i].TujuanPengirimanName =="Karoseri" )
					{
						$scope.DeliveryNoteNotAllowChangeAddress=true;
						//get dropdown tipe karoseri then set yang 0
						DeliveryNoteFactory.getOptionsVendor(1).then(function (res) {
							$scope.optionsCabang = res.data.Result;
							
						}).then(function () {
							$scope.ShowKaroseriFoot=true;
							//$scope.mDeliveryNote.DeliveryCaroseriesVendorId=$scope.optionsCabang[0];
							//$scope.mDeliveryNote.SentAddress=$scope.optionsCabang[0].VendorAddress;
						});
						
						
						
					}
					else
					{
						if($scope.optionsTujuanPengiriman[i].TujuanPengirimanName =="Repair" )
						{
							$scope.DeliveryNoteNotAllowChangeAddress=true;
						}
						else
						{
							$scope.DeliveryNoteNotAllowChangeAddress=false;
						}
						
						if($scope.optionsTujuanPengiriman[i].TujuanPengirimanName =="Customer" )
						{
							for(var z = 0; z < $scope.optionsJenisTugas.length; ++z)
							{
								if($scope.optionsJenisTugas[z].JenisTugasName !="Pengiriman")
								{
									//$scope.DisableDeliveryNoteJenisTugas=true;
									//$scope.mDeliveryNote.JenisTugasId=$scope.optionsJenisTugas[z].JenisTugasId;
									$scope.optionsJenisTugas.splice(z,1);
								}
							}
						}
						
						$scope.mDeliveryNote.DeliveryCaroseriesDestinationId={};
						//alert('repair');
						//get dropdown tipe bengkel then set yang 0
						DeliveryNoteFactory.getOptionsVendor(2).then(function (res) {
							$scope.optionsCabang = res.data.Result;
							
						}).then(function () {
							$scope.mDeliveryNote.DeliveryCaroseriesDestinationId=null;
							$scope.mDeliveryNote.DeliveryCaroseriesDriverCategoryId=null;
							//$scope.mDeliveryNote.DeliveryCaroseriesVendorId=$scope.optionsCabang[0];
							//$scope.mDeliveryNote.SentAddress=$scope.optionsCabang[0].VendorAddress;
							$scope.ShowKaroseriFoot=false;
						});
						
						
					}
				}
			}
  }
		
		$scope.CabangKaroseriSelectionChanged = function () {
			for(var i = 0; i < $scope.optionsCabang.length; ++i)
			{
				if($scope.optionsCabang[i].VendorId == $scope.mDeliveryNote.DeliveryCaroseriesVendorId)
				{
					$scope.mDeliveryNote.SentAddress=angular.copy($scope.optionsCabang[i].VendorAddress);
					break;
				}
			}
			if((typeof $scope.mDeliveryNote['DeliveryCaroseriesVendorId']=="undefined"))
			{
				$scope.mDeliveryNote.SentAddress="";
			}
			
  }
		
		$scope.SelectedDriverCategoryRadio = function () {
			//alert($scope.mDeliveryNote.DriverCategoryId);
  }
		
		DeliveryNoteFactory.getRadioPengirimanUnit().then(function (res) {
			$scope.RadioPengirimanUnit = res.data.Result;
			
		});
		
		DeliveryNoteFactory.getRadioCategoryDriver().then(function (res) {
			$scope.RadioCategoryDriver = res.data.Result;
			
		});
		
		DeliveryNoteFactory.getOptionTujuanPengiriman().then(function (res) {
			$scope.optionsTujuanPengiriman = res.data.Result;
			// $scope.optionsTujuanPengiriman = $scope.optionsTujuanPengiriman.filter(function (type) { return (type.TujuanPengirimanName != 'Swapping'); })
			
		});
		
		// DeliveryNoteFactory.getOptionJenisTugas().then(function (res) {
			// $scope.optionsJenisTugas = res.data.Result;	
			// return $scope.optionsJenisTugas;
		// });
		$scope.DisableDeliveryNoteJenisTugas=false;
		$scope.optionsJenisTugas = DeliveryNoteFactory.getOptionJenisTugas();
		
		$scope.lookupFrameNo = function () {
			DeliveryNoteFactory.getDataFrameNo().then(function (res) {

				$scope.gridFrameNo.data = res.data.Result;
				$scope.FrameWarnaSearch=null;
				$scope.FrameTipeSearch=null;
				$scope.FrameModelSearch=null;
				$scope.FrameNoSearch="";
			});
			angular.element('.ui.modal.ModalFrame').modal('setting',{closable:false}).modal('show');
			angular.element('.ui.modal.ModalFrame').not(':first').remove();
  }
		
		$scope.BatalLookupFrame = function () {
			angular.element('.ui.modal.ModalFrame').modal('hide');
  }
		
		$scope.cetakDeliveryNote = function(SelectedData) {
			//$scope.ShowDeliveryNoteMain=false;
			//$scope.ShowDeliveryNoteCetak=true;
			$scope.buat_cetak=angular.copy(SelectedData);
			//$scope.ShowAddDeliveryNoteBtn=false;
			$scope.breadcrums.title="Cetak";
			$scope.DeliveryNoteCetakPanda();
  }
		
		$scope.DeliveryNoteCetakPanda = function() {

			//Call PrintRpt
			var pdfFile = null;
			PrintRpt.print("sales/PrintDeliveryNote?DeliveryNoteId="+$scope.buat_cetak.DeliveryNoteId+"&FrameNo="+$scope.buat_cetak.FrameNo).success(function (res) {
				var file = new Blob([res], {type: 'application/pdf'});
				var fileURL = URL.createObjectURL(file);
						
				console.log("pdf", fileURL);
				//$scope.content = $sce.trustAsResourceUrl(fileURL);
				pdfFile = fileURL;

				if(pdfFile != null)
					printJS(pdfFile);
				else
					console.log("error cetakan", pdfFile);  
			})
			.error(function (res) {
				console.log("error cetakan", pdfFile);  
				});
		}
		
		$scope.KembaliKeMainList = function(SelectedData) {
			$scope.breadcrums.title="";
			$scope.ShowDeliveryNoteMain=true;
			$scope.ShowDeliveryNoteCetak=false;
			$scope.ShowAddDeliveryNoteBtn=true;
  }

       
		//----------------------------------
		// Grid Setup
		//----------------------------------
		$scope.grid = {
						enableSorting: true,
						enableRowSelection: true,
						multiSelect: true,
						enableSelectAll: true,
						//showTreeExpandNoChildren: true,
						// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
						// paginationPageSize: 15,
						columnDefs: [
										{ name: 'Delivery Id', field: 'DeliveryNoteId',  visible: false },
										{ name: 'Nomor Dokumen',  field: 'DeliveryNoteNo' },
										{ name: 'No. Rangka',  field: 'FrameNo' },
										{ name: 'Model',   field: 'VehicleModelName' },
										{ name: 'Tipe',   field: 'Description' },
										{ name: 'Warna', field: 'ColorName' },
										{ name: 'Tujuan Pengiriman',   field: 'TujuanPengirimanName' },
										{ name: 'Tanggal Keluar',  field: 'OutDate',cellFilter: 'date:\'yyyy-MM-dd\'' },
										{ name: 'Driver', field: 'DriverPDSName' },
										{ name: 'Location',  field: 'LastLocation' },
										{ name: 'Address Unit', field: 'LastAddressLocation' },
										{ name: 'Estimasi Tanggal Kembali',   field: 'DateBackEstimated',cellFilter: 'date:\'yyyy-MM-dd\'' },
										{ name: 'Cetak', width:'6%', cellTemplate:'<i title="Cetak" class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.cetakDeliveryNote(row.entity)" ></i><i title="Ubah" class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.editDeliveryNote(row.entity)" ></i>' },
										
						]
		};
		
		var btnActionPilihFrameNo = '<a style="color:blue;"  ng-click=""> \
                                    <p style="padding:5px 0 0 5px" ng-click="grid.appScope.pilihFrameNo(row.entity)"> \
                                        <u>Pilih</u> \
                                    </p> \
                                  </a>';
		
		$scope.pilihFrameNo = function (FrameNoThingy) {
   $scope.mDeliveryNote.FrameNo=angular.copy(FrameNoThingy.FrameNo);
			$scope.mDeliveryNote.VehicleModelName=angular.copy(FrameNoThingy.VehicleModelName);
			$scope.mDeliveryNote.Description=angular.copy(FrameNoThingy.Description);
			$scope.mDeliveryNote.ColorName=angular.copy(FrameNoThingy.ColorName);
			
			$scope.mDeliveryNote.VehicleModelId=angular.copy(FrameNoThingy.VehicleModelId);
			$scope.mDeliveryNote.VehicleTypeId=angular.copy(FrameNoThingy.VehicleTypeId);
			$scope.mDeliveryNote.VehicleTypeColorId=angular.copy(FrameNoThingy.VehicleTypeColorId);
			$scope.mDeliveryNote.ColorId=angular.copy(FrameNoThingy.ColorId);
			$scope.mDeliveryNote.DRId=angular.copy(FrameNoThingy.DRId);
			
			$scope.mDeliveryNote.TanggalPDD=angular.copy(FrameNoThingy.TanggalPDD);
			$scope.mDeliveryNote.TanggalRepair=angular.copy(FrameNoThingy.TanggalRepair);
			$scope.mDeliveryNote.Address=angular.copy(FrameNoThingy.Address);
			$scope.mDeliveryNote.TanggalKembali=angular.copy(FrameNoThingy.TanggalKembali);
			
			$scope.mDeliveryNote.OutDate=angular.copy(FrameNoThingy.TanggalPDD);
			$scope.mDeliveryNote.ReceiverName = angular.copy(FrameNoThingy.ReceiveName);
			$scope.mDeliveryNote.LastLocation = angular.copy(FrameNoThingy.LastLocation);
			$scope.mDeliveryNote.LastAddressLocation = angular.copy(FrameNoThingy.LastAddressLocation);

			console.log('$scope.mDeliveryNote =====>', $scope.mDeliveryNote);

   angular.element('.ui.modal.ModalFrame').modal('hide');
		}
		
		$scope.TujuanPengirimanValue = function (selected) {
			$scope.SelectTujuanPengiriman = selected;
			$scope.mDeliveryNote.TujuanPengirimanName=selected.TujuanPengirimanName;
			
			if($scope.DeliveryNoteOperation=="Insert")
			{
				$scope.mDeliveryNote.OutDate=null;
				$scope.mDeliveryNote.SentAddress="";
				$scope.mDeliveryNote.DateBackEstimated=null;
				$scope.mDeliveryNote.SentAddress=null;
				if(selected.TujuanPengirimanName =="Customer")
				{
					$scope.mDeliveryNote.OutDate=angular.copy($scope.mDeliveryNote.TanggalPDD);
					$scope.mDeliveryNote.SentAddress=angular.copy($scope.mDeliveryNote.Address);
				}
				else if(selected.TujuanPengirimanName =="Repair")
				{
					$scope.mDeliveryNote.OutDate=angular.copy($scope.mDeliveryNote.TanggalRepair);
					$scope.mDeliveryNote.DateBackEstimated=angular.copy($scope.mDeliveryNote.TanggalKembali);
				}
			}
		}
		
		$scope.gridFrameNo = {
			enableSorting: true,
			enableFiltering: true,
			enableColumnResizing: true,
			// enableRowSelection: true,
			// multiSelect: true,
			// enableSelectAll: true,
			//showTreeExpandNoChildren: true,
			// paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
			// paginationPageSize: 15,

			//ada kemungkinan ganti prospect code
				columnDefs: 
						[
									{ name: 'No. Rangka', field: 'FrameNo' , width: '22.5%'},
									{ name: 'Model', field: 'VehicleModelName', width: '22.5%' },
									{ name: 'Tipe', field: 'Description', width: '22.5%' },
									{ name: 'Warna', field: 'ColorName', width: '22.5%' },
									{
													name: 'action',
													allowCellFocus: false,
													width: '12.5%',
													pinnedRight: true,
													enableColumnMenu: false,
													enableSorting: false,
													enableColumnResizing: true,
													cellTemplate: btnActionPilihFrameNo
									}
						]
		};



	});
