angular.module('app')
    .controller('DaftarUnitMasukNewController', function ($scope, $http, CurrentUser, DaftarUnitMasukNewFactory, $timeout, $rootScope, bsNotify, LocalService) {
        //#region SampleUploadImage
        $scope.uploadFiles = [];
        $scope.isImage = 1;
        IfGotProblemWithModal();
        $scope.DaftarUnitMasukNewLihatDetailDariList = true;
        $scope.DaftarUnitMasukNewHistoryDariList = true;

        //#endregion SampleUploadImage
        $scope.filterData = {
            defaultFilter: [
                { name: 'Nama Pelanggan', value: 'NameCustomer' },
                { name: 'Cabang Pemilik', value: 'OwnerBranchName' },
                { name: 'No Rangka', value: 'FrameNo' },
                { name: 'Status', value: 'StatusDRName' }
            ],
            advancedFilter: [
                { name: 'Nama Pelanggan', value: 'NameCustomer', typeMandatory: 0 },
                { name: 'Cabang Pemilik', value: 'OwnerBranchName', typeMandatory: 0 },
                { name: 'No Rangka', value: 'FrameNo', typeMandatory: 0 },
                { name: 'Status', value: 'StatusDRName', typeMandatory: 0 }
            ]
        };

        $scope.dateOptions = {
            // formatYear: 'yyyy-mm-dd',
            // startingDay: 1,
            // minMode: 'year'
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
            minDate: new Date(),
        };

        $scope.tinggi =   window.screen.availHeight;
                $scope.lebar =   window.screen.availWidth;

                $scope.gambar = {
                        "max-width": $scope.lebar - 40,
                        "max-height": $scope.tinggi - 40
                }

        $scope.selesaiWashingDryingFunction = function () {


            angular.element('.ui.modal.DaftarUnitMasukForm').modal('hide');
            //angular.element('.ui.modal.ModalSelesaiWashingDrying').modal('show');
            setTimeout(function () {
                angular.element(".ui.modal.ModalSelesaiWashingDrying").modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalSelesaiWashingDrying').modal('show');



        }

        $scope.selesaiRepairFunction = function () {


            angular.element('.ui.modal.DaftarUnitMasukForm').modal('hide');
            //angular.element('.ui.modal.ModalSelesaiWashingDrying').modal('show');
            setTimeout(function () {
                angular.element(".ui.modal.ModalSelesaiRepair").modal('refresh');
            }, 0);
            angular.element('.ui.modal.ModalSelesaiRepair').modal('show');



        }

        $scope.Ya_Clicked = function (SelectedDataWashing) {
            angular.element('.ui.modal.ModalSelesaiWashingDrying').modal('hide');
            SelectedDataWashing.StatusDRId = {};
            SelectedDataWashing.StatusDRId = 9;
            SelectedDataWashing.AlreadyWashDryBit = null;
            SelectedDataWashing.AlreadyWashDryBit = true;

            var UpdateWashing = {};
            UpdateWashing.OutletId = SelectedDataWashing.OutletId;
            UpdateWashing.DRId = SelectedDataWashing.DRId;
            UpdateWashing.StatusDRId = SelectedDataWashing.StatusDRId;
            UpdateWashing.FrameNo = SelectedDataWashing.FrameNo;


            //var dataGetIn = angular.merge({}, $scope.SelectedKendaraan, $scope.SelectedFundSource)
            DaftarUnitMasukNewFactory.putGRStatus(UpdateWashing).then(
                function (res) {

                    DaftarUnitMasukNewFactory.getData('?start=1&limit=5').then(
                        function (res) {
                            $scope.bind_data.data = res.data.Result;
                        }
                        

                    )
                    bsNotify.show(
                        {
                            title: "Sukses",
                            content: "Update data berhasil.",
                            type: 'success'
                        }
                    );
                },
                function (err) {
                    bsNotify.show(
                        {
                            title: "Gagal",
                            content: "Update data gagal.",
                            type: 'danger'
                        }
                    );

                }
            )

        }

        $scope.Ya_ClickedRepair = function (SelectedDataRepair) {
            angular.element('.ui.modal.ModalSelesaiRepair').modal('hide');
            SelectedDataRepair.StatusDRId = {};
            SelectedDataRepair.StatusDRId = 5;
            //SelectedDataRepair.AlreadyWashDryBit = null;
            //SelectedDataRepair.AlreadyWashDryBit = true;
            var kirimDR = {};
            kirimDR.StatusDRId = {};
            kirimDR.FrameNo = {};
            kirimDR.StatusDRId = SelectedDataRepair.StatusDRId;
            kirimDR.FrameNo = SelectedDataRepair.FrameNo;
            //var dataGetIn = angular.merge({}, $scope.SelectedKendaraan, $scope.SelectedFundSource)
            DaftarUnitMasukNewFactory.putGRStatus(kirimDR).then(
                function (res) {

                    DaftarUnitMasukNewFactory.getData('/?start=1&limit=5').then(
                        function (res) {
                            $scope.bind_data.data = res.data.Result;
                        }

                    )
                },
                function (err) {
                    bsNotify.show(
                        {
                            title: "Gagal",
                            content: "Update data gagal.",
                            type: 'danger'
                        }
                    );
                }
            )

        }

        $scope.SimpanDetailChecklist = function (SelectedDataChecklist) {
            setTimeout(function () {
                angular.element(".ui.modal.loadingImgDR").modal('refresh');
            }, 0);
            angular.element('.ui.modal.loadingImgDR').modal('show');

            SelectedDataChecklist.InputProblem = {};
            SelectedDataChecklist.InputProblem.SDRPhotoBuktiKerusakan = {};
            SelectedDataChecklist.ListDetailCheckList = [];

            if ($scope.SelectedProblem == null) {

                SelectedDataChecklist.InputProblem = null;
            } else {
                SelectedDataChecklist.InputProblem.DRId = $scope.selected_data.DRId;
                SelectedDataChecklist.InputProblem.CategoryReparasiId = $scope.SelectedProblem.CategoryReparasiId;
                SelectedDataChecklist.InputProblem.KeteranganKerusakan = $scope.SelectedProblem.KeteranganKerusakan;
                SelectedDataChecklist.InputProblem.NeedInsuranceBit = $scope.SelectedProblem.NeedInsuranceBit;
                SelectedDataChecklist.InputProblem.BackDateEstimated = $scope.SelectedProblem.BackDateEstimated;
                SelectedDataChecklist.InputProblem.OutDate = $scope.SelectedProblem.OutDate;
                SelectedDataChecklist.InputProblem.SDRPhotoBuktiKerusakan.PhotoBuktiUpload = [];
                SelectedDataChecklist.InputProblem.SDRPhotoBuktiKerusakan.PhotoBuktiUpload.UpDocObj = null;
                SelectedDataChecklist.InputProblem.SDRPhotoBuktiKerusakan.PhotoBuktiUpload.FileName = null;


                var tempUP = angular.copy($scope.uploadFiles);
                angular.forEach(tempUP, function (dok, dokindex) {
                    dok.UpDocObj = btoa(dok.UpDocObj);
                });

                SelectedDataChecklist.InputProblem.SDRPhotoBuktiKerusakan.PhotoBuktiUpload = tempUP;


            }


            for (var i in $scope.selectedItems) {
                SelectedDataChecklist.ListDetailCheckList.push($scope.selectedItems[i]);
            }
            console.log('aa',$scope.selectedItems.length);
            console.log('bb',$scope.sumCeklist);
            

            if ($scope.selectedItems.length != $scope.sumCeklist){
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Ada checklist inspeksi yang belum terisi",
                        type: 'warning'
                    }
                );
            }

            // scope.sssssss = 00;
            DaftarUnitMasukNewFactory.saveDetailChecklist(SelectedDataChecklist).then(
                function (res) {

                    DaftarUnitMasukNewFactory.getData('/?start=1&limit=5').then(
                        function (res) {
                            $scope.bind_data.data = res.data.Result;
                            $scope.batalkanmulaiInspeksiFunction();
                            $scope.InputDataProblem = {};
                        }

                    )

                    bsNotify.show(
                        {
                            title: "Sukses",
                            content: "Simpan inspeksi berhasil.",
                            type: 'success'
                        }
                    );
                },
                function (err) {
                    bsNotify.show(
                        {
                            title: "Gagal",
                            content: "Simpan inspeksi gagal.",
                            type: 'danger'
                        }
                    );

                }
            )

            angular.element('.ui.modal.loadingImgDR').modal('hide');

        }
        $scope.Tidak_Clicked = function () {
            angular.element('.ui.modal.ModalSelesaiWashingDrying').modal('hide');

        }

        $scope.Tidak_ClickedRepair = function () {
            angular.element('.ui.modal.ModalSelesaiRepair').modal('hide');

        }

        $scope.goodReceivedFunction = function () {
            $scope.goodReceivedForm = true;
            $scope.daftarUnitMasuk = true;
            angular.element('.ui.modal.DaftarUnitMasukForm').modal('hide');


        }
        $scope.SimpanGoodReceived = function (SelectedDataGetIn) {
            SelectedDataGetIn.StatusDRId = {};
            SelectedDataGetIn.StatusDRId = 2;

            //var dataGetIn = angular.merge({}, $scope.SelectedKendaraan, $scope.SelectedFundSource)
            DaftarUnitMasukNewFactory.putGRStatus(SelectedDataGetIn).then(
                function (res) {


                    DaftarUnitMasukNewFactory.postGR(SelectedDataGetIn).then(
                        function (res) {
                            //$scope.bind_data.data = res.data.Result;
                            $rootScope.$emit('RefreshDirective', {})
                        }

                    )

                },
                function (err) {
                    bsNotify.show(
                        {
                            title: "Gagal",
                            content: "Update data gagal.",
                            type: 'danger'
                        }
                    );

                }
            )
            $scope.goodReceivedForm = false;
            $scope.daftarUnitMasuk = false;

        }

        $scope.batalkanGoodReceived = function () {
            $scope.goodReceivedForm = false;
            $scope.daftarUnitMasuk = false;

        }

        $scope.mulaiInspeksiFunction = function (InspectionData) {
            $scope.mulaiInspeksiForm = true;

            $scope.inputNonProblem = true;
            $scope.inputProblem = false;

            $scope.daftarUnitMasuk = true;
            $scope.InputDataProblem = null;
            $scope.SelectedProblem = null;
            $scope.uploadFiles = [];
            angular.element('.ui.modal.DaftarUnitMasukForm').modal('hide');
            DaftarUnitMasukNewFactory.getDataChecklist('start=1&limit=1000').then(
                function (res) {

                    $scope.MasterChecklist = res.data.Result;
                    console.log('ceklist',$scope.MasterChecklist);
                    $scope.DetailCeklist = $scope.MasterChecklist[0];

                    $scope.sumCeklist = 0;
                    for(var i in $scope.MasterChecklist){
                        $scope.sumCeklist = $scope.sumCeklist + $scope.MasterChecklist[i].ListItemInspection.length;
                        
                    }

                    DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[0].UrlPhoto).then(function (res) {
                        var saveImage = JSON.stringify(res.data);
                        LocalService.unset('ImageDR1');
                        // LocalService.unset('ImageDR2');
                        // LocalService.unset('ImageDR3');
                        // LocalService.unset('ImageDR4');
                        // LocalService.unset('ImageDR5');
                        // LocalService.unset('ImageDR6');
                        // LocalService.unset('ImageDR7');
                        // LocalService.unset('ImageDR8');
                        // LocalService.unset('ImageDR9');
                        // LocalService.unset('ImageDR10');
                        LocalService.set('ImageDR1', saveImage);
                        $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR1'));
                        console.log('tempImageChecklist', $scope.tempImageChecklist);
                        $scope.imageChecklist = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                    })






                    $scope.countItem = 0;
                    $scope.countItemSimpan = 0;

                    for (var x in InspectionData.ListDetailCheckList) {
                        $scope.countItemSimpan++;
                    }

                    for (var i in $scope.MasterChecklist) {
                        for (var j in $scope.MasterChecklist[i].ListItemInspection) {
                            $scope.countItem++;
                        }
                    }


                    for (var i in $scope.MasterChecklist) {
                        for (var j in $scope.MasterChecklist[i].ListItemInspection) {
                            for (var k in InspectionData.ListDetailCheckList) {
                                if (InspectionData.ListDetailCheckList[k].ChecklistItemInspectionId == $scope.MasterChecklist[i].ListItemInspection[j].ChecklistItemInspectionId) {

                                    $scope.MasterChecklist[i].ListItemInspection[j].checked = true;
                                }
                            }

                        }
                    }

                },
                function (err) {

                }
            )

        }

        $scope.LihatDetailDaftarUnitMasuk = function (RowLihatDetail) {

            $scope.DataLihatDetailDaftarUnitMasuk = {};
            $scope.DaftarUnitMasukNewLihatDetailDariList = false;

            $scope.DataLihatDetailDaftarUnitMasuk = angular.copy(RowLihatDetail);
            $scope.LihatInspeksiForm = true;
            $scope.historyInspeksi = false;

            console.log('xxx', RowLihatDetail);
            $scope.History = {};
            $scope.History.EmployeeName = {};
            $scope.History.InspectionDate = {};


            $scope.History.EmployeeName = angular.copy(RowLihatDetail.EmployeeName);
            $scope.History.InspectionDate = angular.copy(RowLihatDetail.InspectionDate);

            $scope.DataLihatDetailDaftarUnitMasuk = angular.copy($scope.HistoryPunyaInputProblem[0]);

            console.log('yyy', $scope.DataLihatDetailDaftarUnitMasuk);

            DaftarUnitMasukNewFactory.getDataChecklist('start=1&limit=1000').then(
                function (res) {

                    $scope.MasterChecklist2 = res.data.Result;

                    $scope.DetailCeklist2 = $scope.MasterChecklist2[0];
                    DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[0].UrlPhoto).then(function (res) {
                        var saveImage = JSON.stringify(res.data);
                        LocalService.unset('ImageDR1');
                        // LocalService.unset('ImageDR2');
                        // LocalService.unset('ImageDR3');
                        // LocalService.unset('ImageDR4');
                        // LocalService.unset('ImageDR5');
                        // LocalService.unset('ImageDR6');
                        // LocalService.unset('ImageDR7');
                        // LocalService.unset('ImageDR8');
                        // LocalService.unset('ImageDR9');
                        // LocalService.unset('ImageDR10');
                        LocalService.set('ImageDR1', saveImage);
                        $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR1'));
                        console.log('tempImageChecklist', $scope.tempImageChecklist);
                        $scope.imageChecklist = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                    })


                    for (var i in $scope.MasterChecklist2) {
                        for (var j in $scope.MasterChecklist2[i].ListItemInspection) {
                            for (var k in RowLihatDetail.ListDetailCheckList) {
                                if (RowLihatDetail.ListDetailCheckList[k].ChecklistItemInspectionId == $scope.MasterChecklist2[i].ListItemInspection[j].ChecklistItemInspectionId) {

                                    $scope.MasterChecklist2[i].ListItemInspection[j].checked = true;
                                }
                            }

                        }
                    }


                },
                function (err) {

                }
            )

            //$scope.DataLihatDetailDaftarUnitMasuk = {};
            if (typeof $scope.DataLihatDetailDaftarUnitMasuk == 'undefined') {
                $scope.DataLihatDetailDaftarUnitMasuk = {};
                $scope.DataLihatDetailDaftarUnitMasuk.SDRPhotoBuktiKerusakan = [];
            } else {
                $scope.DataLihatDetailDaftarUnitMasuk.SDRPhotoBuktiKerusakan = [];

                for (var i = 0; i < $scope.HistoryPunyaInputProblem.length; ++i) {
                    $scope.DataLihatDetailDaftarUnitMasuk.SDRPhotoBuktiKerusakan[i] = {};
                    $scope.DataLihatDetailDaftarUnitMasuk.SDRPhotoBuktiKerusakan[i].UpDocObj = {};
                    $scope.DataLihatDetailDaftarUnitMasuk.SDRPhotoBuktiKerusakan[i].FileName = {};
                    $scope.DataLihatDetailDaftarUnitMasuk.SDRPhotoBuktiKerusakan[i].FileName = $scope.HistoryPunyaInputProblem[i].PhotoName;
                    $scope.DataLihatDetailDaftarUnitMasuk.SDRPhotoBuktiKerusakan[i].UpDocObj = $scope.HistoryPunyaInputProblem[i].PhotoData;
                }
            }


        }

        $scope.viewImage = function (data) {
            DaftarUnitMasukNewFactory.getImage(data.UrlPhoto).then(function (res) {
                $scope.image = { documentname: data.FileName, viewdocument: res.data.UpDocObj, viewdocumentName: res.data.FileName };
                setTimeout(function () {
                    angular.element(".ui.modal.viewImageDR").modal('refresh');
                }, 0);
                angular.element('.ui.modal.viewImageDR').modal('show');
            })
        }

        $scope.keluarDokumen = function () {
            angular.element('.ui.modal.viewImageDR').modal('hide');
        }

        $scope.DetailFunction = function (RowLihatDetail) {
            angular.element('.ui.modal.DaftarUnitMasukForm').modal('hide');
            $scope.DaftarUnitMasukNewLihatDetailDariList = true;
            $scope.uploadFiles = [];

            $scope.LihatInspeksiForm = true;
            $scope.historyInspeksi = false;
            $scope.daftarUnitMasuk = true;


            $scope.DataLihatDetailDaftarUnitMasuk = angular.copy($scope.selected_data.InputProblem);
            $scope.History = {};
            $scope.History.EmployeeName = {};
            $scope.History.InspectionDate = {};


            console.log('aaaaa', RowLihatDetail);
            var TempLength = RowLihatDetail.ListDetailCheckList.length;
            var TempHistory = RowLihatDetail.ListDetailCheckList[TempLength - 1];

            if (TempLength == 0) {
                $scope.History.EmployeeName = '';
                $scope.History.InspectionDate = '';
            } else {
                $scope.History.EmployeeName = angular.copy(TempHistory.StaffPDSName);
                $scope.History.InspectionDate = angular.copy(TempHistory.InspectionDate);
            }



            DaftarUnitMasukNewFactory.getDataChecklist('start=1&limit=1000').then(
                function (res) {

                    $scope.MasterChecklist2 = res.data.Result;

                    $scope.DetailCeklist2 = $scope.MasterChecklist2[0];

                    DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[0].UrlPhoto).then(function (res) {
                        var saveImage = JSON.stringify(res.data);
                        LocalService.unset('ImageDR1');
                        // LocalService.unset('ImageDR2');
                        // LocalService.unset('ImageDR3');
                        // LocalService.unset('ImageDR4');
                        // LocalService.unset('ImageDR5');
                        // LocalService.unset('ImageDR6');
                        // LocalService.unset('ImageDR7');
                        // LocalService.unset('ImageDR8');
                        // LocalService.unset('ImageDR9');
                        // LocalService.unset('ImageDR10');
                        LocalService.set('ImageDR1', saveImage);
                        $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR1'));
                        console.log('tempImageChecklist', $scope.tempImageChecklist);
                        $scope.imageChecklist = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                    })


                    if (RowLihatDetail.InputProblem != null) {
                        $scope.BuktiKerusakan = RowLihatDetail.InputProblem.SDRPhotoBuktiKerusakan;
                    } else {
                        $scope.BuktiKerusakan = null;
                    }



                    for (var i in $scope.MasterChecklist2) {
                        for (var j in $scope.MasterChecklist2[i].ListItemInspection) {
                            for (var k in RowLihatDetail.ListDetailCheckList) {
                                if (RowLihatDetail.ListDetailCheckList[k].ChecklistItemInspectionId == $scope.MasterChecklist2[i].ListItemInspection[j].ChecklistItemInspectionId) {

                                    $scope.MasterChecklist2[i].ListItemInspection[j].checked = true;
                                }
                            }

                        }
                    }

                    if (RowLihatDetail.InputProblem != null) {
                        for (var i in RowLihatDetail.InputProblem.SDRPhotoBuktiKerusakan) {
                            $scope.uploadFiles.push({
                                UpDocObj: RowLihatDetail.InputProblem.SDRPhotoBuktiKerusakan[i].PhotoData,
                                FileName: RowLihatDetail.InputProblem.SDRPhotoBuktiKerusakan[i].PhotoDataName
                            }
                            );


                        }
                    } else {
                        RowLihatDetail.InputProblem = null;
                    }


                },
                function (err) {

                }
            )

            resetStep2();

        }

        $scope.batalkanmulaiInspeksiFunction = function () {
            $scope.mulaiInspeksiForm = false;
            $scope.daftarUnitMasuk = false;
            $scope.LihatInspeksiForm = false;
            resetStep();

        }

        $scope.batalkanlihatInspeksiFunction = function () {
            if ($scope.DaftarUnitMasukNewLihatDetailDariList == true) {
                $scope.mulaiInspeksiForm = false;
                $scope.daftarUnitMasuk = false;
                $scope.LihatInspeksiForm = false;
            }
            else if ($scope.DaftarUnitMasukNewLihatDetailDariList == false) {
                $scope.historyInspeksi = true;
                $scope.LihatInspeksiForm = false;
            }
            resetStep();
            resetStep2();
        }


        $scope.InputDataProblem = {};
        $scope.modalInputProblem = function () {
            //$scope.InputDataProblem = {};

            $scope.inputProblem = true;
            $scope.inputNonProblem = false;
        }
        $scope.batalInputProblem = function (selectProblem) {
            $scope.inputProblem = false;
            $scope.inputNonProblem = true;
            $scope.SelectedProblem = selectProblem;

        }

        var resetStep = function () {
            $scope.step = 1;
            $scope.step01 = false;
            $scope.step02 = false;
            $scope.step03 = false;
            $scope.step04 = false;
            $scope.step05 = false;
            $scope.step06 = false;
            $scope.step07 = false;
            $scope.step08 = false;
            $scope.step09 = false;
            $scope.step10 = false;
            $scope.selectedItems = [];
        }

        var resetStep2 = function () {
            $scope.step2 = 1;
            $scope.step201 = false;
            $scope.step202 = false;
            $scope.step203 = false;
            $scope.step204 = false;
            $scope.step205 = false;
            $scope.step206 = false;
            $scope.step207 = false;
            $scope.step208 = false;
            $scope.step209 = false;
            $scope.step210 = false;
            $scope.selectedItems = [];
        }



        $scope.selectedItems = [];



        $scope.toggleChecked = function (data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItems.indexOf(data);
                $scope.selectedItems.splice(index, 1);

            } else {
                data.checked = true;
                $scope.selectedItems.push(data);

            }

        };


        $scope.ApproveData = function () {
            for (var i = 0; i < $scope.selectedItems.length; i++) {
                $scope.selectedItems[i].ApprovalFlag = 1;
                delete $scope.selectedItems[i].checked;
            }

            $scope.selectedItems = [];

        };

        $scope.RejectData = function () {
            for (var i = 0; i < $scope.selectedItems.length; i++) {
                $scope.selectedItems[i].ApprovalFlag = 0;
                delete $scope.selectedItems[i].checked;
            }

            $scope.selectedItems = [];

        };
        $scope.historyInspeksiFunction = function () {
            $scope.historyInspeksi = true;
            $scope.daftarUnitMasuk = true;
            $scope.DaftarUnitMasukNewHistoryDariList = true;
            angular.element('.ui.modal.DaftarUnitMasukForm').modal('hide');

            DaftarUnitMasukNewFactory.getDataHistoryInspeksi($scope.selected_data.DRId).then(function (res) {
                $scope.HistoryPunyaInputProblem = res.data.Result[0].InputProblem;
                $scope.gridHistoryInspeksi.data = res.data.Result;

            });

        }

        $scope.historyInspeksiFunctionDariInspectionChecklist = function () {
            $scope.historyInspeksi = true;
            $scope.daftarUnitMasuk = true;
            $scope.mulaiInspeksiForm = false;
            $scope.DaftarUnitMasukNewHistoryDariList = false;
            angular.element('.ui.modal.DaftarUnitMasukForm').modal('hide');
            $scope.HistoryPunyaInputProblem = null;
            DaftarUnitMasukNewFactory.getDataHistoryInspeksi($scope.selected_data.DRId).then(function (res) {
                $scope.HistoryPunyaInputProblem = res.data.Result[0].InputProblem;
                $scope.gridHistoryInspeksi.data = res.data.Result[0].ListSDRDetailCheckListDetail;

            });
            console.log("ulala");

        }

        $scope.batalkanHistoryInspeksi = function () {
            if ($scope.DaftarUnitMasukNewHistoryDariList == true) {
                $scope.historyInspeksi = false;
                $scope.daftarUnitMasuk = false;
            }
            else if ($scope.DaftarUnitMasukNewHistoryDariList == false) {
                $scope.historyInspeksi = false;
                $scope.mulaiInspeksiForm = true;
                $scope.inputNonProblem = true;
                $scope.inputProblem = false;
            }
        }

        $scope.step = 1;

        $scope.nextStep = function () {
            stepFunction();

        }

        $scope.step2 = 1;

        $scope.nextStep2 = function () {
            stepFunction2();

        }


        var stepFunction = function () {

            if ($scope.step + 1 == 2) {
                $scope.DetailCeklist = $scope.MasterChecklist[1];
                if (typeof $scope.MasterChecklist[2] != 'undefined') {
                    if ($scope.MasterChecklist[1].UrlPhoto != null) {
                        //$scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR2'));
                        DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[1].UrlPhoto).then(function (res) {
                            var saveImage = JSON.stringify(res.data);
                            LocalService.unset('ImageDR2');
                            LocalService.set('ImageDR2', saveImage);

                            $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR2'));
                            $scope.imageChecklist2 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                        })



                        //console.log('tempImageChecklist',$scope.tempImageChecklist);

                    } else {
                        $scope.imageChecklist2 = { viewdocument: null };
                    }


                    $scope.step02 = true;
                    $scope.step01 = true;
                    $scope.step = 2;
                } else {
                    $scope.step02 = true;
                    $scope.step01 = true;
                    $scope.step = 2;
                }

            } else
                if ($scope.step + 1 == 3) {
                    $scope.DetailCeklist = $scope.MasterChecklist[2];
                    if (typeof $scope.MasterChecklist[2] != 'undefined') {
                        if ($scope.MasterChecklist[2].UrlPhoto != null) {
                            DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[2].UrlPhoto).then(function (res) {
                                var saveImage = JSON.stringify(res.data);
                                LocalService.unset('ImageDR3');
                                LocalService.set('ImageDR3', saveImage);

                                $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR3'));
                                $scope.imageChecklist3 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                            })

                            //console.log('tempImageChecklist',$scope.tempImageChecklist);

                        } else {
                            $scope.imageChecklist3 = { viewdocument: null };
                        }


                        $scope.step02 = false;
                        $scope.step03 = true;
                        $scope.step = 3;
                    } else {
                        $scope.step02 = false;
                        $scope.step03 = true;
                        $scope.step = 3;
                    }

                } else
                    if ($scope.step + 1 == 4) {
                        $scope.DetailCeklist = $scope.MasterChecklist[3];
                        if (typeof $scope.MasterChecklist[3] != 'undefined') {
                            DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[3].UrlPhoto).then(function (res) {
                                var saveImage = JSON.stringify(res.data);
                                LocalService.unset('ImageDR4');
                                LocalService.set('ImageDR4', saveImage);

                                $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR4'));
                                $scope.imageChecklist4 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                            })

                            //console.log('tempImageChecklist',$scope.tempImageChecklist);

                            $scope.step03 = false;
                            $scope.step04 = true;
                            $scope.step = 4;
                        } else {
                            $scope.step03 = false;
                            $scope.step04 = true;
                            $scope.step = 4;
                        }

                    } else
                        if ($scope.step + 1 == 5) {
                            $scope.DetailCeklist = $scope.MasterChecklist[4];
                            if (typeof $scope.MasterChecklist[4] != 'undefined') {
                                DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[4].UrlPhoto).then(function (res) {
                                    var saveImage = JSON.stringify(res.data);
                                    LocalService.unset('ImageDR5');
                                    LocalService.set('ImageDR5', saveImage);

                                    $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR5'));
                                    $scope.imageChecklist5 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                                })

                                //console.log('tempImageChecklist',$scope.tempImageChecklist);

                                $scope.step04 = false;
                                $scope.step05 = true;
                                $scope.step = 5;
                            } else {
                                $scope.step04 = false;
                                $scope.step05 = true;
                                $scope.step = 5;
                            }

                        } else
                            if ($scope.step + 1 == 6) {
                                $scope.DetailCeklist = $scope.MasterChecklist[5];
                                if (typeof $scope.MasterChecklist[5] != 'undefined') {
                                    DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[5].UrlPhoto).then(function (res) {
                                        var saveImage = JSON.stringify(res.data);
                                        LocalService.unset('ImageDR6');
                                        LocalService.set('ImageDR6', saveImage);

                                        $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR6'));
                                        $scope.imageChecklist6 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                                    })

                                    //console.log('tempImageChecklist',$scope.tempImageChecklist);

                                    $scope.step05 = false;
                                    $scope.step06 = true;
                                    $scope.step = 6;
                                } else {
                                    $scope.step05 = false;
                                    $scope.step06 = true;
                                    $scope.step = 6;
                                }

                            } else
                                if ($scope.step + 1 == 7) {
                                    $scope.DetailCeklist = $scope.MasterChecklist[6];
                                    if (typeof $scope.MasterChecklist[6] != 'undefined') {
                                        DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[6].UrlPhoto).then(function (res) {
                                            var saveImage = JSON.stringify(res.data);
                                            LocalService.unset('ImageDR7');
                                            LocalService.set('ImageDR7', saveImage);

                                            $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR7'));

                                            //console.log('tempImageChecklist',$scope.tempImageChecklist);
                                            $scope.imageChecklist7 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                                        })



                                        $scope.step06 = false;
                                        $scope.step07 = true;
                                        $scope.step = 7;
                                    } else {
                                        $scope.step06 = false;
                                        $scope.step07 = true;
                                        $scope.step = 7;
                                    }

                                } else
                                    if ($scope.step + 1 == 8) {
                                        $scope.DetailCeklist = $scope.MasterChecklist[7];
                                        if (typeof $scope.MasterChecklist[7] != 'undefined') {
                                            DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[7].UrlPhoto).then(function (res) {
                                                var saveImage = JSON.stringify(res.data);
                                                LocalService.unset('ImageDR8');
                                                LocalService.set('ImageDR8', saveImage);

                                                $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR8'));

                                                //console.log('tempImageChecklist',$scope.tempImageChecklist);
                                                $scope.imageChecklist8 = { viewdocument: $scope.tempImageChecklist.UpDocObj };

                                            })


                                            $scope.step07 = false;
                                            $scope.step08 = true;
                                            $scope.step = 8;
                                        } else {
                                            $scope.step07 = false;
                                            $scope.step08 = true;
                                            $scope.step = 8;
                                        }

                                    } else
                                        if ($scope.step + 1 == 9) {
                                            $scope.DetailCeklist = $scope.MasterChecklist[8];
                                            if (typeof $scope.MasterChecklist[8] != 'undefined') {
                                                DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[8].UrlPhoto).then(function (res) {
                                                    var saveImage = JSON.stringify(res.data);
                                                    LocalService.unset('ImageDR9');
                                                    LocalService.set('ImageDR9', saveImage);

                                                    $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR9'));

                                                    //console.log('tempImageChecklist',$scope.tempImageChecklist);
                                                    $scope.imageChecklist9 = { viewdocument: $scope.tempImageChecklist.UpDocObj };

                                                })


                                                $scope.step08 = false;
                                                $scope.step09 = true;
                                                $scope.step = 9;
                                            } else {
                                                $scope.step08 = false;
                                                $scope.step09 = true;
                                                $scope.step = 9;
                                            }

                                        } else
                                            if ($scope.step + 1 == 10) {
                                                $scope.DetailCeklist = $scope.MasterChecklist[9];
                                                // console.log('aa',$scope.MasterChecklist);
                                                if (typeof $scope.MasterChecklist[9] != 'undefined') {
                                                    DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist[9].UrlPhoto).then(function (res) {
                                                        var saveImage = JSON.stringify(res.data);
                                                        LocalService.unset('ImageDR10');
                                                        LocalService.set('ImageDR10', saveImage);

                                                        $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR10'));

                                                        //console.log('tempImageChecklist',$scope.tempImageChecklist);
                                                        $scope.imageChecklist10 = { viewdocument: $scope.tempImageChecklist.UpDocObj };

                                                    })
                                                    $scope.step09 = false;
                                                    $scope.step10 = true;
                                                    $scope.step = 10;
                                                } else {
                                                    $scope.step09 = false;
                                                    $scope.step10 = true;
                                                    $scope.step = 10;

                                                }

                                            } else
                                                if ($scope.step == 1) {
                                                    resetStep();
                                                }

        }

        var stepFunction2 = function () {

            if ($scope.step2 + 1 == 2) {
                $scope.DetailCeklist2 = $scope.MasterChecklist2[1];
                if (typeof $scope.MasterChecklist2[1] != 'undefined') {
                    if ($scope.MasterChecklist2[1].UrlPhoto != null) {
                        //$scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR2'));
                        DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[1].UrlPhoto).then(function (res) {
                            var saveImage = JSON.stringify(res.data);
                            LocalService.unset('ImageDR2');
                            LocalService.set('ImageDR2', saveImage);

                            $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR2'));
                            $scope.imageChecklist2 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                        })



                        //console.log('tempImageChecklist',$scope.tempImageChecklist);

                    } else {
                        $scope.imageChecklist2 = { viewdocument: null };
                    }
                    $scope.step202 = true;
                    $scope.step201 = true;
                    $scope.step2 = 2;
                } else {
                    $scope.step202 = true;
                    $scope.step201 = true;
                    $scope.step2 = 2;
                }

            } else
                if ($scope.step2 + 1 == 3) {
                    $scope.DetailCeklist2 = $scope.MasterChecklist2[2];
                    if (typeof $scope.MasterChecklist2[2] != 'undefined') {
                        if ($scope.MasterChecklist2[2].UrlPhoto != null) {
                            DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[2].UrlPhoto).then(function (res) {
                                var saveImage = JSON.stringify(res.data);
                                LocalService.unset('ImageDR3');
                                LocalService.set('ImageDR3', saveImage);

                                $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR3'));
                                $scope.imageChecklist3 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                            })



                            //console.log('tempImageChecklist',$scope.tempImageChecklist);


                        } else {
                            $scope.imageChecklist3 = { viewdocument: null };
                        }

                        $scope.step202 = false;
                        $scope.step203 = true;
                        $scope.step2 = 3;
                    } else {
                        $scope.step202 = false;
                        $scope.step203 = true;
                        $scope.step2 = 3;
                    }

                } else
                    if ($scope.step2 + 1 == 4) {
                        $scope.DetailCeklist2 = $scope.MasterChecklist2[3];
                        if (typeof $scope.MasterChecklist2[3] != 'undefined') {
                            DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[3].UrlPhoto).then(function (res) {
                                var saveImage = JSON.stringify(res.data);
                                LocalService.unset('ImageDR4');
                                LocalService.set('ImageDR4', saveImage);

                                $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR4'));
                                $scope.imageChecklist4 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                            })

                            $scope.step203 = false;
                            $scope.step204 = true;
                            $scope.step2 = 4;
                        } else {
                            $scope.step203 = false;
                            $scope.step204 = true;
                            $scope.step2 = 4;
                        }

                    } else
                        if ($scope.step2 + 1 == 5) {
                            $scope.DetailCeklist2 = $scope.MasterChecklist2[4];
                            if (typeof $scope.MasterChecklist2[4] != 'undefined') {
                                DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[4].UrlPhoto).then(function (res) {
                                    var saveImage = JSON.stringify(res.data);
                                    LocalService.unset('ImageDR5');
                                    LocalService.set('ImageDR5', saveImage);

                                    $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR5'));
                                    $scope.imageChecklist5 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                                })
                                $scope.step204 = false;
                                $scope.step205 = true;
                                $scope.step2 = 5;
                            } else {
                                $scope.step204 = false;
                                $scope.step205 = true;
                                $scope.step2 = 5;
                            }

                        } else
                            if ($scope.step2 + 1 == 6) {
                                $scope.DetailCeklist2 = $scope.MasterChecklist2[5];
                                if (typeof $scope.MasterChecklist2[5] != 'undefined') {
                                    DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[5].UrlPhoto).then(function (res) {
                                        var saveImage = JSON.stringify(res.data);
                                        LocalService.unset('ImageDR6');
                                        LocalService.set('ImageDR6', saveImage);

                                        $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR6'));
                                        $scope.imageChecklist6 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                                    })
                                    $scope.step205 = false;
                                    $scope.step206 = true;
                                    $scope.step2 = 6;
                                } else {
                                    $scope.step205 = false;
                                    $scope.step206 = true;
                                    $scope.step2 = 6;
                                }

                            } else
                                if ($scope.step2 + 1 == 7) {
                                    $scope.DetailCeklist2 = $scope.MasterChecklist2[6];
                                    if (typeof $scope.MasterChecklist2[6] != 'undefined') {
                                        DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[6].UrlPhoto).then(function (res) {
                                            var saveImage = JSON.stringify(res.data);
                                            LocalService.unset('ImageDR7');
                                            LocalService.set('ImageDR7', saveImage);

                                            $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR7'));

                                            //console.log('tempImageChecklist',$scope.tempImageChecklist);
                                            $scope.imageChecklist7 = { viewdocument: $scope.tempImageChecklist.UpDocObj };
                                        })
                                        $scope.step206 = false;
                                        $scope.step207 = true;
                                        $scope.step2 = 7;
                                    } else {
                                        $scope.step206 = false;
                                        $scope.step207 = true;
                                        $scope.step2 = 7;
                                    }

                                } else
                                    if ($scope.step2 + 1 == 8) {
                                        $scope.DetailCeklist2 = $scope.MasterChecklist2[7];
                                        if (typeof $scope.MasterChecklist2[7] != 'undefined') {
                                            DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[7].UrlPhoto).then(function (res) {
                                                var saveImage = JSON.stringify(res.data);
                                                LocalService.unset('ImageDR8');
                                                LocalService.set('ImageDR8', saveImage);

                                                $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR8'));

                                                //console.log('tempImageChecklist',$scope.tempImageChecklist);
                                                $scope.imageChecklist8 = { viewdocument: $scope.tempImageChecklist.UpDocObj };

                                            })
                                            $scope.step207 = false;
                                            $scope.step208 = true;
                                            $scope.step2 = 8;
                                        } else {
                                            $scope.step207 = false;
                                            $scope.step208 = true;
                                            $scope.step2 = 8;
                                        }

                                    } else
                                        if ($scope.step2 + 1 == 9) {
                                            $scope.DetailCeklist2 = $scope.MasterChecklist2[8];
                                            if (typeof $scope.MasterChecklist2[8] != 'undefined') {
                                                DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[8].UrlPhoto).then(function (res) {
                                                    var saveImage = JSON.stringify(res.data);
                                                    LocalService.unset('ImageDR9');
                                                    LocalService.set('ImageDR9', saveImage);

                                                    $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR9'));

                                                    //console.log('tempImageChecklist',$scope.tempImageChecklist);
                                                    $scope.imageChecklist9 = { viewdocument: $scope.tempImageChecklist.UpDocObj };

                                                })
                                                $scope.step208 = false;
                                                $scope.step209 = true;
                                                $scope.step2 = 9;
                                            } else {
                                                $scope.step208 = false;
                                                $scope.step209 = true;
                                                $scope.step2 = 9;
                                            }

                                        } else
                                            if ($scope.step2 + 1 == 10) {
                                                $scope.DetailCeklist2 = $scope.MasterChecklist2[9];
                                                if (typeof $scope.MasterChecklist2[9] != 'undefined') {
                                                    DaftarUnitMasukNewFactory.getImage($scope.MasterChecklist2[9].UrlPhoto).then(function (res) {
                                                        var saveImage = JSON.stringify(res.data);
                                                        LocalService.unset('ImageDR10');
                                                        LocalService.set('ImageDR10', saveImage);

                                                        $scope.tempImageChecklist = JSON.parse(LocalService.get('ImageDR10'));

                                                        //console.log('tempImageChecklist',$scope.tempImageChecklist);
                                                        $scope.imageChecklist10 = { viewdocument: $scope.tempImageChecklist.UpDocObj };

                                                    })
                                                    $scope.step209 = false;
                                                    $scope.step210 = true;
                                                    $scope.step2 = 10;
                                                } else {
                                                    $scope.step209 = false;
                                                    $scope.step210 = true;
                                                    $scope.step2 = 10;
                                                }

                                            } else
                                                if ($scope.step2 == 1) {
                                                    resetStep2();
                                                }

        }

        $scope.kembaliStep = function () {
            $scope.step = $scope.step - 1;

            if ($scope.step == 1) {
                $scope.DetailCeklist = $scope.MasterChecklist[0];
                $scope.step02 = false;
                $scope.step01 = false;
                resetStep();
            } else
                if ($scope.step == 2) {
                    $scope.DetailCeklist = $scope.MasterChecklist[1];
                    $scope.step03 = false;
                    $scope.step02 = true;
                    $scope.step = 2;
                } else
                    if ($scope.step == 3) {
                        $scope.DetailCeklist = $scope.MasterChecklist[2];
                        $scope.step04 = false;
                        $scope.step03 = true;
                        $scope.step = 3;
                    } else
                        if ($scope.step == 4) {
                            $scope.DetailCeklist = $scope.MasterChecklist[3];
                            $scope.step05 = false;
                            $scope.step04 = true;
                            $scope.step = 4;
                        } else
                            if ($scope.step == 5) {
                                $scope.DetailCeklist = $scope.MasterChecklist[4];
                                $scope.step06 = false;
                                $scope.step05 = true;
                                $scope.step = 5;
                            } else
                                if ($scope.step == 6) {
                                    $scope.DetailCeklist = $scope.MasterChecklist[5];
                                    $scope.step07 = false;
                                    $scope.step06 = true;
                                    $scope.step = 6;
                                } else
                                    if ($scope.step == 7) {
                                        $scope.DetailCeklist = $scope.MasterChecklist[6];
                                        $scope.step08 = false;
                                        $scope.step07 = true;
                                        $scope.step = 7;
                                    } else
                                        if ($scope.step == 8) {
                                            $scope.DetailCeklist = $scope.MasterChecklist[7];
                                            $scope.step09 = false;
                                            $scope.step08 = true;
                                            $scope.step = 8;
                                        } else
                                            if ($scope.step == 9) {
                                                $scope.DetailCeklist = $scope.MasterChecklist[8];
                                                $scope.step10 = false;
                                                $scope.step09 = true;
                                                $scope.step = 9;
                                            }
                                            else
                                                if ($scope.step == 10) {
                                                    $scope.DetailCeklist = $scope.MasterChecklist[9];
                                                    $scope.step10 = false;
                                                    $scope.step09 = true;
                                                    $scope.step = 10;
                                                }
        }

        $scope.kembaliStep2 = function () {
            $scope.step2 = $scope.step2 - 1;

            if ($scope.step2 == 1) {
                $scope.DetailCeklist2 = $scope.MasterChecklist2[0];
                $scope.step202 = false;
                $scope.step201 = false;
                resetStep2();
            } else
                if ($scope.step2 == 2) {
                    $scope.DetailCeklist2 = $scope.MasterChecklist2[1];
                    $scope.step203 = false;
                    $scope.step202 = true;
                    $scope.step2 = 2;
                } else
                    if ($scope.step2 == 3) {
                        $scope.DetailCeklist2 = $scope.MasterChecklist2[2];
                        $scope.step204 = false;
                        $scope.step203 = true;
                        $scope.step2 = 3;
                    } else
                        if ($scope.step2 == 4) {
                            $scope.DetailCeklist2 = $scope.MasterChecklist2[3];
                            $scope.step205 = false;
                            $scope.step204 = true;
                            $scope.step2 = 4;
                        } else
                            if ($scope.step2 == 5) {
                                $scope.DetailCeklist2 = $scope.MasterChecklist2[4];
                                $scope.step206 = false;
                                $scope.step205 = true;
                                $scope.step2 = 5;
                            } else
                                if ($scope.step2 == 6) {
                                    $scope.DetailCeklist2 = $scope.MasterChecklist2[5];
                                    $scope.step207 = false;
                                    $scope.step206 = true;
                                    $scope.step2 = 6;
                                } else
                                    if ($scope.step2 == 7) {
                                        $scope.DetailCeklist2 = $scope.MasterChecklist2[6];
                                        $scope.step208 = false;
                                        $scope.step207 = true;
                                        $scope.step2 = 7;
                                    } else
                                        if ($scope.step2 == 8) {
                                            $scope.DetailCeklist2 = $scope.MasterChecklist2[7];
                                            $scope.step209 = false;
                                            $scope.step208 = true;
                                            $scope.step2 = 8;
                                        } else
                                            if ($scope.step2 == 9) {
                                                $scope.DetailCeklist2 = $scope.MasterChecklist2[8];
                                                $scope.step210 = false;
                                                $scope.step209 = true;
                                                $scope.step2 = 9;
                                            }
                                            else
                                                if ($scope.step2 == 10) {
                                                    $scope.DetailCeklist2 = $scope.MasterChecklist2[9];
                                                    $scope.step210 = false;
                                                    $scope.step209 = true;
                                                    $scope.step2 = 10;
                                                }
        }

        var btnActionPilihHistory = '<a style="color:blue;"  ng-click=""> \
                                    <p style="padding:5px 0 0 5px" ng-click="grid.appScope.LihatDetailDaftarUnitMasuk(row.entity)"> \
                                        <u>Lihat Hasil Inspeksi</u> \
                                    </p> \
                                  </a>';

        //Grid History Ispeksi
        $scope.gridHistoryInspeksi = {
            enableSorting: true,
            enableFiltering: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'History Id', field: 'HistoryId', visible: false },
                { name: 'Tanggal Inspeksi', field: 'InspectionDate', cellFilter: 'date:\'dd/MM/yyyy\'' },
                { displayName: 'Staff PDS',name: 'Staff PDS', field: 'EmployeeName' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnActionPilihHistory
                }

            ]
        };
    });

angular.module('app').directive('myDirective', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind('change', function () {
                var formData = new FormData();
                formData.append('file', element[0].files[0]);

                // optional front-end logging 
                var fileObject = element[0].files[0];
                scope.fileLog = {
                    'lastModified': fileObject.lastModified,
                    'lastModifiedDate': fileObject.lastModifiedDate,
                    'name': fileObject.name,
                    'size': fileObject.size,
                    'type': fileObject.type
                };

                scope.mAssetBrosur.BrochureUrl = "/imgs/content/Master/" + scope.fileLog.name;
                scope.$apply();


            });

        }
    };
});	
