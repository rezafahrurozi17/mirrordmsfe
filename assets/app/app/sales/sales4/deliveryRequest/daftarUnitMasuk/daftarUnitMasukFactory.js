angular.module('app')
  .factory('DaftarUnitMasukNewFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
                var res = $http.get('/api/sales/SDRList'+param);
				        return res;
            },

      getDataChecklist: function(param) {
        var res=$http.get('/api/sales/MActivityChecklistItemMasterDetail?'+param);
      
        return res;
      },

      putGRStatus: function(PutGrGetIn){
        console.log('daridbPUT',PutGrGetIn);
        return $http.put('/api/sales/SDRStatus', [PutGrGetIn]);
      },

      postGR: function(PostGR){
        console.log('daridb',PostGR);
        return $http.post('/api/sales/SDRGR', [PostGR]);
      },


      getDataHistoryInspeksi: function (DRId) {
        var res = $http.get('/api/sales/SDRDetailCheckList/?start=1&limit=100&filterData=DRId|'+DRId);
        
		    return res;
      },

      saveDetailChecklist: function(DataChecklist) {
         console.log('ceklistdaridb',DataChecklist);
        return $http.post('/api/sales/SDRDetailChecklist', [DataChecklist]);
      },

      getImage: function(GUID) {
        var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
        return res;
    }


    }
  });
 //ddd