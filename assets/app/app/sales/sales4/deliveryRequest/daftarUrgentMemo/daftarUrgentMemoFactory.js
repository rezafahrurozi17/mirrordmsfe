angular.module('app')
    .factory('DaftarUrgentMemoFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(param) {
                var res = $http.get('/api/sales/SDRList_DaftarUrgentMemo' + param);
                
                return res;
            },

            GetFrameNoSearchDropdownOptions: function() {

                var da_json = [
                    { SearchOptionId: "1", SearchOptionName: "No Rangka" },
                    { SearchOptionId: "2", SearchOptionName: "Model" },
                    { SearchOptionId: "3", SearchOptionName: "Tipe" }
                ];

                var res = da_json

                return res;
            },

            GetSPKNoSearchDropdownOptions: function() {

                var da_json = [
                    { SearchOptionId: "1", SearchOptionName: "No SPK" },
                    { SearchOptionId: "2", SearchOptionName: "No Rangka" },
                    { SearchOptionId: "3", SearchOptionName: "Nama Pelanggan" }
                ];

                var res = da_json

                return res;
            },

            getDataSPK: function() {
                var res = $http.get('/api/sales/SDRLookupSPK');

                return res;
            },

            getDataSPKSearch: function(SearchBy, SearchValue) {
                //Ori var res=$http.get('/api/sales/SDRLookupSPK/?start=1&limit=10000&filterData='+SearchBy+'|'+SearchValue);
                var res = $http.get('/api/sales/SDRLookupSPK/Filter/?' + SearchBy + '=' + SearchValue); //Edited Fikri
                //console.log('res=>',res);
                return res;
            },


            getOptionTujuanPengiriman: function() {
                var res = $http.get('/api/sales/PCategoryReason');
                //console.log('res=>',res);
                return res;
            },

            getDataFrameNo: function() {
                var res = $http.get('/api/sales/SDRLookupNoRangka');
                //console.log('res=>',res);
                return res;
            },

            getDataFrameNoSearch: function(SearchBy, SearchValue) {
                var res = $http.get('/api/sales/SDRLookupNoRangka/?start=1&limit=10000&filterData=' + SearchBy + '|' + SearchValue);
                //console.log('res=>',res);
                return res;
            },

            getDataCountQuota: function() {
                var res = $http.get('/api/sales/SDRQuotaUrgentMemo');
                //console.log('res=>',res);
                return res;
            },

            TLSUrgentMemo: function(TLSData) {
                return $http.post('/api/sales/TLSSendUrgentMemo', TLSData);
              },
            
              getOptionLocation: function() {
                var res = $http.get('/api/sales/SDRUrgentMemoLocation');
                //console.log('res=>',res);
                return res;
            },

            create: function(DaftarUrgentMemo) {
                var Da_EstimateBranchReceivedDate = "";
                var Da_PromiseDelDate = "";
                var Da_BackDateEstimated = "";

                try {
                    Da_EstimateBranchReceivedDate = DaftarUrgentMemo.EstimateBranchReceivedDate.getFullYear() + '-' +
                        ('0' + (DaftarUrgentMemo.EstimateBranchReceivedDate.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + DaftarUrgentMemo.EstimateBranchReceivedDate.getDate()).slice(-2) + 'T' +
                        ((DaftarUrgentMemo.EstimateBranchReceivedDate.getHours() < '10' ? '0' : '') + DaftarUrgentMemo.EstimateBranchReceivedDate.getHours()) + ':' +
                        ((DaftarUrgentMemo.EstimateBranchReceivedDate.getMinutes() < '10' ? '0' : '') + DaftarUrgentMemo.EstimateBranchReceivedDate.getMinutes()) + ':' +
                        ((DaftarUrgentMemo.EstimateBranchReceivedDate.getSeconds() < '10' ? '0' : '') + DaftarUrgentMemo.EstimateBranchReceivedDate.getSeconds());
                } catch (e1) {
                    Da_EstimateBranchReceivedDate = null;
                }

                try {
                    Da_PromiseDelDate = DaftarUrgentMemo.PromiseDelDate.getFullYear() + '-' +
                        ('0' + (DaftarUrgentMemo.PromiseDelDate.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + DaftarUrgentMemo.PromiseDelDate.getDate()).slice(-2) + 'T' +
                        ((DaftarUrgentMemo.PromiseDelDate.getHours() < '10' ? '0' : '') + DaftarUrgentMemo.PromiseDelDate.getHours()) + ':' +
                        ((DaftarUrgentMemo.PromiseDelDate.getMinutes() < '10' ? '0' : '') + DaftarUrgentMemo.PromiseDelDate.getMinutes()) + ':' +
                        ((DaftarUrgentMemo.PromiseDelDate.getSeconds() < '10' ? '0' : '') + DaftarUrgentMemo.PromiseDelDate.getSeconds());
                } catch (e2) {
                    Da_PromiseDelDate = null;
                }

                try {
                    Da_BackDateEstimated = DaftarUrgentMemo.BackDateEstimated.getFullYear() + '-' +
                        ('0' + (DaftarUrgentMemo.BackDateEstimated.getMonth() + 1)).slice(-2) + '-' +
                        ('0' + DaftarUrgentMemo.BackDateEstimated.getDate()).slice(-2) + 'T' +
                        ((DaftarUrgentMemo.BackDateEstimated.getHours() < '10' ? '0' : '') + DaftarUrgentMemo.BackDateEstimated.getHours()) + ':' +
                        ((DaftarUrgentMemo.BackDateEstimated.getMinutes() < '10' ? '0' : '') + DaftarUrgentMemo.BackDateEstimated.getMinutes()) + ':' +
                        ((DaftarUrgentMemo.BackDateEstimated.getSeconds() < '10' ? '0' : '') + DaftarUrgentMemo.BackDateEstimated.getSeconds());
                } catch (e3) {
                    Da_BackDateEstimated = null;
                }
                return $http.post('/api/sales/SDRList_DaftarUrgentMemo', [{
                    //AppId: 1,
                    OutletId: DaftarUrgentMemo.OutletId,

                    SOId: DaftarUrgentMemo.SOId,
                    SpkId: DaftarUrgentMemo.SpkId,
                    FrameNo: DaftarUrgentMemo.FrameNo,
                    PDDId: DaftarUrgentMemo.PDDId,
                    VehicleTypeColorId: DaftarUrgentMemo.VehicleTypeColorId,

                    EstimateBranchReceivedDate: Da_EstimateBranchReceivedDate,

                    PromiseDelDate: Da_PromiseDelDate,

                    ReasonSubmission: DaftarUrgentMemo.ReasonSubmission,
                    SentAddress: DaftarUrgentMemo.SentAddress,

                    BackDateEstimated: Da_BackDateEstimated,

                    BackToPDCBit: DaftarUrgentMemo.BackToPDCBit,
                    ReasonId: DaftarUrgentMemo.ReasonId,
                }]);
            },
            update: function(DaftarUrgentMemo) {
                return $http.put('/api/sales/SDRList_DaftarUrgentMemo', [{
                    SalesProgramId: DaftarUrgentMemo.SalesProgramId,
                    SalesProgramName: DaftarUrgentMemo.SalesProgramName
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/sales/SDRList_DaftarUrgentMemo', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd