angular.module('app')
    .controller('DaftarUrgentMemoController', function ($scope, $http, CurrentUser, DaftarUrgentMemoFactory, $timeout, bsNotify) {
        IfGotProblemWithModal();
        $scope.denganSPK = false;
        $scope.tanpaSPK = false;
        $scope.DaftarUrgentMemoForm = true;
        $scope.user = CurrentUser.user();

        if ($scope.user.RoleName == "KACAB") {
            $scope.ShowAddUrgentMemoBtn = true;
        } else {
            $scope.ShowAddUrgentMemoBtn = false;
        }

        $scope.TambahUrgentMemoForm = false;
        var modeTambahMemo = [
            { modeId: "1", modeName: "Dengan SPK" },
            { modeId: "2", modeName: "Tanpa SPK" },

        ];

        $scope.dateOptions = {
            // formatYear: 'yyyy-mm-dd',
            // startingDay: 1,
            // minMode: 'year'
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
            minDate: new Date(),
        };

        $scope.ModeTambahUrgentMemo = modeTambahMemo;

        $scope.TodayDate_Raw = new Date();
        $scope.TodayDate = $scope.TodayDate_Raw.getFullYear() + '-' + ('0' + ($scope.TodayDate_Raw.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate_Raw.getDate()).slice(-2);

        $scope.bind_data = {
            data: [],
            filter: []
        };
        $scope.filterData = {
            defaultFilter: [
                { name: 'No SPK', value: 'FormSPKNo' },
                { name: 'No Rangka', value: 'FrameNo' }
            ],
            advancedFilter: [
                { name: 'StatusTaskName', value: 'StatusTaskId', typeMandatory: 0 },
                { name: 'ActivityTypeName', value: 'ActivityTypeId', typeMandatory: 0 },
                { name: 'StartDate', value: 'StartDate', typeMandatory: 0 }
            ]
        };

        //var AlasanPengajuan = [
        //	{ AlasanPengajuanId: "1", AlasanPengajuanName: "Event" },
        //	{ AlasanPengajuanId: "2", AlasanPengajuanName: "Dll" },
        //];
        $scope.DisabledLocation = false;
        $scope.optionFrameNoSearchCriteria = DaftarUrgentMemoFactory.GetFrameNoSearchDropdownOptions();

        $scope.optionSPKNoSearchCriteria = DaftarUrgentMemoFactory.GetSPKNoSearchDropdownOptions();

        DaftarUrgentMemoFactory.getOptionTujuanPengiriman().then(function (res) {
            $scope.OptionsAlasanPengajuan = res.data.Result;

        }).then(function () {
            for (var i = 0; i < $scope.OptionsAlasanPengajuan.length; ++i) {
                if ($scope.OptionsAlasanPengajuan[i].ReasonName == "Customer") {
                    $scope.OptionsAlasanPengajuan.splice(i, 1);
                }
            }
        });

        $scope.Tujuan = {};
        $scope.Tujuan.ReasonName = null;
        $scope.AlasanPengajuanFunction = function (selectedData) {
            $scope.Tujuan = selectedData;
            
        }
        
        $scope.TLSDataUrgentMemo = {};
        $scope.LokasiValue = function (selected) {
           
            if(typeof selected != 'undefined'){
                $scope.DisabledLocation = true;
                $scope.TLSDataUrgentMemo.EventLocationCode = selected.LocationCode;
                $scope.TLSDataUrgentMemo.EventLocationName = selected.LocationName;
                $scope.TLSDataUrgentMemo.EventLocationAddress = selected.Address;
                $scope.selected_data.LocationName = selected.LocationName;
                $scope.selected_data.SentAddress = selected.Address;
            }
            else
            if(typeof selected == 'undefined'){
                $scope.DisabledLocation = false;
                $scope.selected_data.LocationName = '';
                $scope.selected_data.SentAddress = '';
                
            }
        }

        $scope.modeTambahMemo = function (selectedmodetambah) {
            $scope.selected_data = {};
            $scope.selected_data.CurrentQuota = 0;
            $scope.selected_data.BackToPDCBit = false;
            $scope.ModeTambah = selectedmodetambah;

            if (selectedmodetambah == '1') {
                $scope.denganSPK = true;
                $scope.tanpaSPK = false;
            } else
                if (typeof selectedmodetambah == "undefined") {
                    $scope.denganSPK = false;
                    $scope.tanpaSPK = false;
                } else {
                    $scope.denganSPK = false;
                    $scope.tanpaSPK = true;
                }
        }

        DaftarUrgentMemoFactory.getOptionLocation().then(function (res) {
            $scope.OptionsLocation = res.data.Result;
           
        });

        $scope.lookupSPK = function () {

            DaftarUrgentMemoFactory.getDataSPK("").then(function (res) {
                $scope.gridNoSPK.data = res.data.Result;

            });

            angular.element('.ui.modal.ModalFormNoSPK').modal('setting', { closable: false }).modal('show');
        }
        $scope.BatalLookupSPK_Clicked = function () {
            angular.element('.ui.modal.ModalFormNoSPK').modal('hide');

        }
        $scope.Quota = {};
        $scope.addNewUrgentMemo = function () {

            DaftarUrgentMemoFactory.getDataCountQuota().then(function (res) {
                $scope.Quota = res.data.Result[0];
                
            });


            $scope.selected_data = {};
            $scope.DaftarUrgentMemoForm = false;
            $scope.ShowAddUrgentMemoBtn = false;
            $scope.TambahUrgentMemoForm = true;
            $scope.selected_data.CurrentQuota = 0;
            $scope.selected_data.BackToPDCBit = false;

        }

        $scope.BatalTambah = function () {
            $scope.selected_data = {};
            $scope.DaftarUrgentMemoForm = true;
            $scope.ShowAddUrgentMemoBtn = true;
            $scope.TambahUrgentMemoForm = false;
        }

        $scope.SaveTambah = function () {

            $scope.DaErrorCheck = false;
            try {
                if ((typeof $scope.mModeTambahUrgentMemo != "undefined") && $scope.mModeTambahUrgentMemo.toString().length > 0) {
                    if ($scope.mModeTambahUrgentMemo == 1) {
                        //dengan
						$scope.selected_data['PromiseDelTime']=angular.copy($scope.selected_data.PromiseDelDate);
                        if ($scope.selected_data['FormSPKNo'].toString().length > 0 && (typeof $scope.selected_data['PromiseDelDate'] != "undefined") && $scope.selected_data['PromiseDelDate'] != null && $scope.selected_data['ReasonSubmission'].toString().length > 0 && (typeof $scope.selected_data['PromiseDelTime'] != "undefined") && $scope.selected_data['PromiseDelTime'] != null) {
                            $scope.DaErrorCheck = true;
                        }
                    } else if ($scope.mModeTambahUrgentMemo == 2) {
                        //tanpa
                        if ($scope.selected_data['FrameNo'].toString().length > 0 && (typeof $scope.selected_data['ReasonId'] != "undefined") && $scope.selected_data['SentAddress'].toString().length > 0 && (typeof $scope.selected_data['PromiseDelDate'] != "undefined") && $scope.selected_data['PromiseDelDate'] != null && (typeof $scope.selected_data['BackDateEstimated'] != "undefined") && $scope.selected_data['BackDateEstimated'] != null && (typeof $scope.selected_data['PromiseDelTime'] != "undefined") && $scope.selected_data['PromiseDelTime'] != null) {
                            $scope.DaErrorCheck = true;
                        }
                    }
                }


            } catch (erorthingy) {
                $scope.DaErrorCheck = false;
				// bsNotify.show(
                    // {
                        // title: "Peringatan",
                        // content: "Pastikan Semua Data Terisi Dengan Benar!",
                        // type: 'warning'
                    // }
                // );
            }

            if ($scope.DaErrorCheck == true) {
                
                $scope.TLSDataUrgentMemo.FrameNumber = null;
                $scope.TLSDataUrgentMemo.ReasonUrgentMemo = null;
                // $scope.Tujuan.TujuanPengirimanName = null;
                if ($scope.ModeTambah == '1') {
                    $scope.TLSDataUrgentMemo.FrameNumber = $scope.selected_data.FrameNo;
                    $scope.TLSDataUrgentMemo.DeliveryRequestType = 1;
                    $scope.TLSDataUrgentMemo.ReasonUrgentMemo = $scope.selected_data.ReasonSubmission;
                    $scope.Tujuan.ReasonName = null;
                    $scope.TLSDataUrgentMemo.DriverReturnType = null;
                    
                }
                
                  
                   
                    if ($scope.Tujuan.ReasonName == "Event") {
                        $scope.TLSDataUrgentMemo.DeliveryRequestType = 2;
                        $scope.TLSDataUrgentMemo.DriverReturnType = 1;
                    }else
                    if ($scope.Tujuan.ReasonName == "Karoseri") {
                        $scope.TLSDataUrgentMemo.DeliveryRequestType = 3;
                        $scope.TLSDataUrgentMemo.DriverReturnType = 1;
                    } else {
                        $scope.TLSDataUrgentMemo.DeliveryRequestType = 1;
                    }
                    
                    $scope.TLSDataUrgentMemo.FrameNumber = $scope.selected_data.FrameNo;
                    $scope.TLSDataUrgentMemo.DeliveryRequestDate = $scope.selected_data.SentUnitTime;
                    $scope.TLSDataUrgentMemo.PickUpDateTime = null;
					
					if($scope.tanpaSPK==true)
					{
						$scope.TLSDataUrgentMemo.VehicleReturnDate = $scope.selected_data.BackDateEstimated.getFullYear() + '-'
                        + ('0' + ($scope.selected_data.BackDateEstimated.getMonth() + 1)).slice(-2) + '-'
                        + ('0' + $scope.selected_data.BackDateEstimated.getDate()).slice(-2) + 'T'
                        + (($scope.selected_data.BackDateEstimated.getHours() < '10' ? '0' : '') + $scope.selected_data.BackDateEstimated.getHours()) + ':'
                        + (($scope.selected_data.BackDateEstimated.getMinutes() < '10' ? '0' : '') + $scope.selected_data.BackDateEstimated.getMinutes()) + ':'
                        + (($scope.selected_data.BackDateEstimated.getSeconds() < '10' ? '0' : '') + $scope.selected_data.BackDateEstimated.getSeconds());

                        $scope.TLSDataUrgentMemo.VehicleReturnDate = $scope.TLSDataUrgentMemo.VehicleReturnDate.slice(0, 19);
					}
                    

                    //$scope.TLSDataUrgentMemo.VehicleReturnDate = $scope.selected_data.BackDateEstimated;
                    $scope.TLSDataUrgentMemo.DriverId = null;
                    $scope.TLSDataUrgentMemo.DriverName = null;
                    $scope.TLSDataUrgentMemo.EventLocationName = $scope.selected_data.LocationName;
                    $scope.TLSDataUrgentMemo.EventLocationAddress = $scope.selected_data.SentAddress;


                var Da_Urgent_Memo_Quota = parseInt($scope.selected_data.CurrentQuota);
                if (Da_Urgent_Memo_Quota >= 1) {
					//if($scope.tanpaSPK == true)
					//{
						var paksaBuatTls=angular.copy($scope.selected_data.PromiseDelDate);
						paksaBuatTls.setHours($scope.selected_data.PromiseDelTime.getHours());
						paksaBuatTls.setMinutes($scope.selected_data.PromiseDelTime.getMinutes());
						
						
						$scope.TLSDataUrgentMemo.PickUpDateTime=""+paksaBuatTls.getFullYear()+"-"+('0' + (paksaBuatTls.getMonth()+1)).slice(-2)+"-"+('0' + paksaBuatTls.getDate()).slice(-2)+"T"+('0' + (paksaBuatTls.getHours())).slice(-2)+":"+('0' + (paksaBuatTls.getMinutes())).slice(-2)+":00";
						//$scope.TLSDataUrgentMemo.PickUpDateTime=angular.copy(paksaBuatTls);
					//}

                    DaftarUrgentMemoFactory.TLSUrgentMemo($scope.TLSDataUrgentMemo).then(function (res) {
                       
                        DaftarUrgentMemoFactory.create($scope.selected_data).then(function(res) {
                            DaftarUrgentMemoFactory.getData("").then(function(res) {
                                $scope.bind_data.data = res.data.Result;
								bsNotify.show(
										{
											title: "Berhasil",
											content: "Data berhasil tersimpan.",
											type: 'success'
										}
									);
                                $scope.loading = false;
                            }).then(function() {
                                $scope.BatalTambah();
                            });
                        })
                    },
                        function (err) {
                           
                            bsNotify.show(
                                {
                                    title: "Error Message",
                                    content: err.data.Message,
                                    type: 'danger'
                                }
                            );
                        })
                } else {
                    bsNotify.show(
                        {
                            title: "Peringatan",
                            content: "Anda Tidak Punya Kuota!",
                            type: 'warning'
                        }
                    );
                    //alert('Anda Tidak Punya Kuota!');
                }
            } else {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Pastikan Semua Data Terisi Dengan Benar!",
                        type: 'warning'
                    }
                );
            }
        }

        $scope.lookupNoRangka = function () {
            DaftarUrgentMemoFactory.getDataFrameNo().then(function (res) {
                $scope.gridNoRangka.data = res.data.Result;

            });

            angular.element('.ui.modal.ModalFormNoRangka').modal('setting', { closable: false }).modal('show');

        }

        $scope.BatalLookupNoRangka_Clicked = function () {
            angular.element('.ui.modal.ModalFormNoRangka').modal('hide');

        }

        $scope.pilihNoSPK = function (FormSPKNoThingy) {
            $scope.selected_data.CurrentQuota = angular.copy(FormSPKNoThingy.CurrentQuota);
            $scope.selected_data.FormSPKNo = angular.copy(FormSPKNoThingy.FormSPKNo);
            $scope.selected_data.CustomerName = angular.copy(FormSPKNoThingy.CustomerName);
            $scope.selected_data.FrameNo = angular.copy(FormSPKNoThingy.FrameNo);

            $scope.selected_data.VehicleModelId = angular.copy(FormSPKNoThingy.VehicleModelId);
            $scope.selected_data.VehicleTypeId = angular.copy(FormSPKNoThingy.VehicleTypeId);
            $scope.selected_data.VehicleTypeColorId = angular.copy(FormSPKNoThingy.VehicleTypeColorId);
            $scope.selected_data.ColorId = angular.copy(FormSPKNoThingy.ColorId);

            $scope.selected_data.VehicleModelName = angular.copy(FormSPKNoThingy.VehicleModelName);
            $scope.selected_data.Description = angular.copy(FormSPKNoThingy.Description);
            $scope.selected_data.ColorName = angular.copy(FormSPKNoThingy.ColorName);
            $scope.selected_data.SentUnitTime = angular.copy(FormSPKNoThingy.SentUnitTime);

            $scope.selected_data.SOId = angular.copy(FormSPKNoThingy.SOId);
            $scope.selected_data.SpkId = angular.copy(FormSPKNoThingy.SpkId);
            $scope.selected_data.PDDId = angular.copy(FormSPKNoThingy.PDDId);

            angular.element('.ui.modal.ModalFormNoSPK').modal('hide');
        }

        $scope.pilihNoRangka = function (FrameNoThingy) {
           
            $scope.selected_data.CurrentQuota = angular.copy(FrameNoThingy.CurrentQuota);
            $scope.selected_data.FrameNo = angular.copy(FrameNoThingy.FrameNo);
            $scope.selected_data.VehicleModelName = angular.copy(FrameNoThingy.VehicleModelName);
            $scope.selected_data.Description = angular.copy(FrameNoThingy.Description);
            $scope.selected_data.ColorName = angular.copy(FrameNoThingy.ColorName);

            $scope.selected_data.SentUnitTime = angular.copy(FrameNoThingy.SentUnitTime);
            $scope.selected_data.VehicleModelId = angular.copy(FrameNoThingy.VehicleModelId);
            $scope.selected_data.VehicleTypeId = angular.copy(FrameNoThingy.VehicleTypeId);
            $scope.selected_data.VehicleTypeColorId = angular.copy(FrameNoThingy.VehicleTypeColorId);
            $scope.selected_data.ColorId = angular.copy(FrameNoThingy.ColorId);

            angular.element('.ui.modal.ModalFormNoRangka').modal('hide');
        }

        $scope.ResetModalFormNoRangka = function () {
            DaftarUrgentMemoFactory.getDataFrameNo().then(function (res) {
                $scope.gridNoRangka.data = res.data.Result;
				$scope.ModalFormNoRangkaSearchValue="";

            });
        }

        $scope.SearchModalFormNoRangka = function () {

            if ($scope.FrameNoSearchCriteria == 1) {
                $scope.DaFrameNoSearchBy = "FrameNo";
            } else if ($scope.FrameNoSearchCriteria == 2) {
                $scope.DaFrameNoSearchBy = "VehicleModelName";
            } else if ($scope.FrameNoSearchCriteria == 3) {
                $scope.DaFrameNoSearchBy = "Description";
            } else {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: 'Please Select Search Criteria',
                        type: 'warning'
                    }
                );
            }

            DaftarUrgentMemoFactory.getDataFrameNoSearch($scope.DaFrameNoSearchBy, $scope.ModalFormNoRangkaSearchValue).then(function (res) {
                $scope.gridNoRangka.data = res.data.Result;
            });
        }

        $scope.ResetModalFormNoSPK = function () {
            $scope.ModalFormNoSPKSearchValue = '';
            $scope.SPKNoSearchCriteria = {};
            DaftarUrgentMemoFactory.getDataSPK().then(function (res) {
                $scope.gridNoSPK.data = res.data.Result;

            });
        }

        $scope.SearchModalFormNoSPK = function () {

            if ($scope.SPKNoSearchCriteria == 1) {
                $scope.DaSPKNoSearchBy = "FormSPKNo";
            } else if ($scope.SPKNoSearchCriteria == 2) {
                $scope.DaSPKNoSearchBy = "FrameNo";
            } else if ($scope.SPKNoSearchCriteria == 3) {
                $scope.DaSPKNoSearchBy = "CustomerName";
            } else {
                bsNotify.show(
                    {
                        title: "Peringatan",
                        content: "Please Select Search Criteria",
                        type: 'warning'
                    }
                );
                //alert('Please Select Search Criteria');
            }

            DaftarUrgentMemoFactory.getDataSPKSearch($scope.DaSPKNoSearchBy, $scope.ModalFormNoSPKSearchValue).then(function (res) {
                $scope.gridNoSPK.data = res.data.Result;
            });
        }

        $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
            if (col.filters[0].term) {
                return 'header-filtered'
            } else {
                return '';
            }
        };

        $scope.lihatUrgentMemoFunction = function () {
            $scope.ShowAddUrgentMemoBtn = false;
            if ($scope.selected_data.FormSPKNo == null) {
                $scope.detiltanpaSPK = true;
                $scope.DaftarUrgentMemoForm = false;

                angular.element('.ui.modal.DaftarUrgentMemoForm').modal('hide');

            } else {
                $scope.detildenganSPK = true;
                $scope.DaftarUrgentMemoForm = false;

                angular.element('.ui.modal.DaftarUrgentMemoForm').modal('hide');
            }

        }

        $scope.BatalLihatDetailUrgentMemo = function () {
            $scope.detiltanpaSPK = false;
            $scope.detildenganSPK = false;
            $scope.selected_data = {};
            $scope.ShowAddUrgentMemoBtn = true;
            $scope.DaftarUrgentMemoForm = true;

        }




        var btnActionPilihNoSPK = '<a style="color:blue;"  ng-click=""> \
                                    <p style="padding:5px 0 0 5px" ng-click="grid.appScope.pilihNoSPK(row.entity)"> \
                                        <u>Pilih</u> \
                                    </p> \
                                  </a>';

        var btnActionPilihNoRangka = '<a style="color:blue;"  ng-click=""> \
                                    <p style="padding:5px 0 0 5px" ng-click="grid.appScope.pilihNoRangka(row.entity)"> \
                                        <u>Pilih</u> \
                                    </p> \
                                  </a>';

        //Grid No.SPK
        $scope.gridNoSPK = {
            enableSorting: true,
            enableFiltering: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'SPK Id', field: 'SPKId', visible: false },
                { name: 'No. SPK', field: 'FormSPKNo', enableFiltering: true, headerCellClass: $scope.highlightFilteredHeader, width: '25%' },
                { name: 'No. Rangka', field: 'FrameNo', enableFiltering: true, headerCellClass: $scope.highlightFilteredHeader, width: '25%' },
                { name: 'Nama Pelanggan', field: 'CustomerName', enableFiltering: true, headerCellClass: $scope.highlightFilteredHeader, width: '25%' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '25%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnActionPilihNoSPK
                }

            ]
        };

        //Grid No.Rangka
        $scope.gridNoRangka = {
            enableSorting: true,
            enableFiltering: true,
            // enableRowSelection: true,
            // multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'No. Rangka', field: 'FrameNo', width: '22.5%' },
                { name: 'Model', field: 'VehicleModelName', width: '22.5%' },
                { name: 'Tipe', field: 'Description', width: '22.5%' },
                { name: 'Warna', field: 'ColorName', width: '22.5%' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '10%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnActionPilihNoRangka
                }

            ]
        };
    });

