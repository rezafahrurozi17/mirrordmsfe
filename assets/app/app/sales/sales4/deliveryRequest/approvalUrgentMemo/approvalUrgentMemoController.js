angular.module('app')
.controller('ApprovalUrgentMemoController', function($scope, $http, CurrentUser, ApprovalUrgentMemoFactory,$timeout) {
	
	
	
	
	$scope.$on('$viewContentLoaded', function () {
            //$scope.loading=true; tadinya true
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        //$scope.mApprovalUrgentMemo = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
		ApprovalUrgentMemoFactory.getData().then(
				function(res){
					$scope.grid.data = res.data.Result;
					$scope.loading=false;
					console.log("data dari Factory",res.data.Result);
					return res.data;
				},
				function(err){
					console.log("err=>",err);
				}
			);
		}
		
        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function (rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function (rows) {
            console.log("onSelectRows=>", rows);
        }
		
		$scope.TidakSetujuApprovalUrgentMemo = function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalUrgentMemo').modal('setting',{closable:false}).modal('show');
		};
		
		$scope.BatalAlasanApprovalUrgentMemo = function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalUrgentMemo').modal('hide');
			$scope.mApprovalUrgentMemo.ApprovalNote={};
		}
		
		$scope.SetujuApprovalUrgentMemo = function () {
			ApprovalUrgentMemoFactory.SetujuApprovalDropSalesSupervisor($scope.mApprovalUrgentMemo).then(function () {
					ApprovalUrgentMemoFactory.getData().then(
						function(res){
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							console.log("data dari Factory",res.data.Result);
							$scope.BatalAlasanApprovalUrgentMemo();
							return res.data;
						},
						function(err){
							console.log("err=>",err);
						}
					);
				});

		};
		
		$scope.SubmitAlasanApprovalUrgentMemo = function () {
			ApprovalUrgentMemoFactory.TidakSetujuApprovalDropSalesSupervisor($scope.mApprovalUrgentMemo).then(function () {
						ApprovalUrgentMemoFactory.getData().then(
							function(res){
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								console.log("data dari Factory",res.data.Result);
								$scope.BatalAlasanApprovalUrgentMemo();
								return res.data;
							},
							function(err){
								console.log("err=>",err);
							}
						);
					});
		}	
	
	$scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'SPK No', field: 'FormSPKNo', width: '7%', visible: true },
				{ name: 'Nama Pemilik', field: 'CustomerCorrespondentName' },
				{ name: 'Prospect Code', field: 'ProspectCode'},
				{ name: 'Besaran Diskon', field: 'Diskon' },
				{ name: 'Tanggal Aju', field: 'SpkDate' },
				{ name: 'Setuju', cellTemplate:'<a ng-click="grid.appScope.$parent.SetujuApprovalUrgentMemo(row.entity)" >Setuju</a>' },
                { name: 'Tidak Setuju', cellTemplate:'<a ng-click="grid.appScope.$parent.TidakSetujuApprovalUrgentMemo(row.entity)" >Tidak Setuju</a>' },
            ]
        };	
	
	
	
	

});



 
// angular.module('app').factory('SampleUITemplateFactory', function ($http) {
      // return {
          // getData: function () {
              // //var res = $http.get('file:///C:/Git/DMS-Sales-Prototype-Mike/DMS.Website/assets/app/app/sales/master/profileSalesProgram/the_thing.json');
				// var res = null;
			  // //uda bisa sampe sini ternyata
              // return res;
          // }
      // }
  // });
