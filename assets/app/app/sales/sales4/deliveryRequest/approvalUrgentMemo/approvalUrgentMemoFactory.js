angular.module('app')
  .factory('ApprovalUrgentMemoFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function() {
		var res=$http.get('/api/sales/CProspectListApproval');
		return res;
        
      },
	  
	  
	  
      create: function(ApprovalUrgentMemo) {
        return $http.post('/api/sales/CProspectListApproval', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalUrgentMemo.SalesProgramName}]);
      },
      update: function(ApprovalUrgentMemo){
        return $http.put('/api/sales/CProspectListApproval', [{
                                            SalesProgramId: ApprovalUrgentMemo.SalesProgramId,
                                            SalesProgramName: ApprovalUrgentMemo.SalesProgramName}]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/CProspectListApproval',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd