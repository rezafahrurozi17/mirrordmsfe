angular.module('app')
    .controller('DaftarUnitRepairController', function($scope, PrintRpt, bsNotify, $http, CurrentUser, DaftarUnitRepairFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.show = { inputProblem: false, grid: true };
        $scope.filter = { StatusUnit: null,TanggalPengiriman: null };
        $scope.dateOptions = {
            format: 'dd/MM/yyyy'
        }
        $scope.StatusUnitModel = null;
		$scope.filter.TanggalPengiriman=new Date();
        //----------------------------------
        // Get Data test
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            DaftarUnitRepairFactory.getData($scope.filter)
                .then(
                    function(res) {
                        gridData = [];
                        $scope.grid.data = res.data.Result;
                        
                        $scope.loading = false;
                    },
                    function(err) {
                        
                    }
                );
        }

        DaftarUnitRepairFactory.getStatusUnit().then(function (res){
            $scope.StatusUnitRes = res.data.Result;
        });

        $scope.filterStatus = function(selected){
            $scope.statusFilter = $scope.StatusUnitRes.filter(function(type){ return (type.ReparationId == selected); })
        }

        $scope.onSelectRows = function(rows) {
            
        }

        // var actionDaftarRepair = '<span> \
                                    // <a> \
                                        // <u ng-if="row.entity.StatusDRName ==\'Repair\' && row.entity.NeedInsuranceBit == true " ng-click="grid.appScope.$parent.followUpPerbaikan(row.entity)">Follow Up Perbaikan</u> \
                                  // </a> \
                                  // </span>';
		var actionDaftarRepair = '<i ng-if="row.entity.StatusDRName ==\'Repair\' && row.entity.NeedInsuranceBit == true " title="Follow Up Perbaikan" class="fa fa-fw fa-lg fa-wrench" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.followUpPerbaikan(row.entity)" ></i>';						  


        $scope.followUpPerbaikan = function(row) {
            $scope.show = { inputProblem: true, grid: false };
            $scope.mFollowup = angular.copy(row);
        }

        $scope.printUpdate = function() {
            DaftarUnitRepairFactory.update($scope.mFollowup).then(function(res) {
                    bsNotify.show({
                        title: "Success Message",
                        content: "Data Berhasil Disimpan",
                        type: 'success'
                    });
                    $scope.kembali();
                    $scope.btnPrintMaintainTs($scope.mFollowup.DRId);
                    $scope.getData();
                },
                function(err) {
                    bsNotify.show({
                        title: "Error Message",
                        content: "Data Gagal Disimpan",
                        type: 'success'
                    });
                });
        }

        $scope.kembali = function() {
            $scope.filter = { StatusUnit: null,TanggalPengiriman: null };
            $scope.StatusUnitModel = null;
            $scope.show = { inputProblem: false, grid: true };
            $scope.getData();
        }

        $scope.keluarDokumen = function() {
            angular.element('.ui.modal.viewImage').modal('hide');
        }

        $scope.viewImage = function(data) {
            DaftarUnitRepairFactory.getImage(data.UrlPhoto).then(function(res) {
                $scope.image = { documentname: data.FileName, viewdocument: res.data.UpDocObj, viewdocumentName: res.data.FileName };
                setTimeout(function() {
                    angular.element(".ui.modal.viewImage").modal('refresh');
                }, 0);
                angular.element('.ui.modal.viewImage').modal('show');
            })
        }

        $scope.btnPrintMaintainTs = function(TsId) {
                var pdfFile = null;

                PrintRpt.print("sales/CetakFormKlaimAsuransi?DRId=" + TsId).success(function(res) {
                        var file = new Blob([res], { type: 'application/pdf' });
                        var fileURL = URL.createObjectURL(file);

                       
                        pdfFile = fileURL;

                        if (pdfFile != null)
                            printJS(pdfFile);
                        else
                            console.log("error cetakan", pdfFile);
                    })
                    .error(function(res) {
                        console.log("error cetakan", pdfFile);
                    });
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Unit Repair Id', field: 'DRId', width: '7%', visible: false },
                { name: 'Nama Pelanggan', field: 'NamaPelanggan' },
                //{ name: 'Cabang Pemilik', field: 'OutletName' },
                { name: 'No. SPK', field: 'FormSPKNo' },
                { name: 'No. PO', field: 'POCode' },
                { name: 'No Rangka', field: 'FrameNo' },
                { name: 'Tanggal Keluar Kendaraan', field: 'OutDate', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Status Unit', field: 'CategoryReparasiName' },
                { name: 'Status Perbaikan', field: 'StatusRepairName' },
                { name: 'Estimasi Tanggal Kembali', field: 'BackDateEstimated', cellFilter: 'date:\'dd-MM-yyyy\'' },
                {
                    name: 'action',
                    allowCellFocus: false,
					enableFiltering: false,
					visible:true,
                    width: '15%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: actionDaftarRepair
                }

            ]
        };
    });