angular.module('app')
    .factory('DaftarUnitRepairFactory', function($http, CurrentUser, $httpParamSerializer) {
        var currentUser = CurrentUser.user;

        function fixDate(date) {
            // console.log("test Date", date);
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };
        return {
            getData: function(filter) {
                if (filter.TanggalPengiriman == undefined && filter.StatusUnit == undefined){
                    var res = $http.get('/api/sales/GetSDRListRepair?start=1&limit=10000');
                }
                else if (filter.TanggalPengiriman == undefined){
                    var param1 = '&filterData=CategoryReparasiId|' + filter.StatusUnit;
                    var res = $http.get('/api/sales/GetSDRListRepair?start=1&limit=10000' +param1);
                }
                else if (filter.StatusUnit == undefined){
                    filter.TanggalPengiriman = fixDate(filter.TanggalPengiriman);
                    var param = '&' + $httpParamSerializer(filter);
                    var res = $http.get('/api/sales/GetSDRListRepair?start=1&limit=10000' + param);

                }
                else{
                    filter.TanggalPengiriman = fixDate(filter.TanggalPengiriman);
                    var param = '&' + $httpParamSerializer(filter);
                    var param1 = '&filterData=CategoryReparasiId|' + filter.StatusUnit;
                    var res = $http.get('/api/sales/GetSDRListRepair?start=1&limit=10000' + param+param1);
                }
                //console.log('res=>',res);
                return res;
            },
            update: function(objct) {
                objct.BackDateEstimated = fixDate(objct.BackDateEstimated);
                return $http.put('/api/sales/SDRListRepair', [{
                    DRId: objct.DRId,
                    OutletId: objct.OutletId,
                    InputProblemId: objct.InputProblemId,
                    StatusRepairId: objct.StatusRepairId,
                    BackDateEstimated: objct.BackDateEstimated
                }]);
            },
            getImage: function(GUID) {
                var res = $http.get('/api/sales/FTP/?strGuid=' + GUID);
                return res;
            },
            getStatusUnit: function(){
                var res = $http.get('/api/sales/PCategoryReparation');
                return res;
            },

        }
    });
//ddd