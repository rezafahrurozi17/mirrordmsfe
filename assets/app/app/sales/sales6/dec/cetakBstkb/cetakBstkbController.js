angular.module('app')
    .controller('CetakBstkbController', function($scope,$filter, $http,PrintRpt, CurrentUser, CetakBstkbFactory, $timeout,bsNotify, MobileHandling) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.ListCetakBSTKBShow = true;
        $scope.PreviewBSTKBShow = false;
        $scope.show_modal={show:false};
        $scope.modalMode='new';

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
            //----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mcetakBstkb = null; //Model
        $scope.disabledBtnCetakThankYou = false;

        //----------------------------------
        // Get Data
        //----------------------------------
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.CetakBSTKB').remove();

		});
		
        var gridData = [];
        $scope.getData = function(){
            CetakBstkbFactory.getData().then(
                function(res)
                {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;
                },
                function(err)
                {
                    bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
                }
            );
        }        

        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.LihatDetail = function(Select)
        {	
            $scope.detail = angular.copy(Select);
			console.log("setan0",Select);
            $scope.thankyou = angular.copy(Select);
			
			var raw_date=""+$scope.thankyou.SentUnitTime;
			var time = raw_date.split(':');
			var date = new Date();
			date.setHours(time[0]);
			date.setMinutes(time[1]);
			$scope.thankyou.SentUnitTime = date;
			
			
			//$scope.thankyou.SentUnitTime=new Date($scope.thankyou.SentUnitTime);
			
			console.log("setan1",$scope.thankyou);
            if($scope.thankyou.StatusFullPayment == "True"){
                $scope.thankyou.StatusFullPayment = "Sudah Dibayar";
            }
            else{
                $scope.thankyou.StatusFullPayment = "Belum Dibayar";
            }
            console.log("setan2",$scope.thankyou);
			angular.element('.ui.modal.CetakBSTKB').modal('show');
			angular.element('.ui.modal.CetakBSTKB').not(':first').remove(); 
        }
        
        $scope.btnCetakBSTKB = function()
        {
            angular.element('.ui.modal.CetakBSTKB').modal('hide');
            //$scope.PreviewBSTKBShow = true;
            //$scope.ListCetakBSTKBShow = false;
			
			//Call PrintRpt
			var pdfFile = null;

			PrintRpt.print("sales/CetakBSTKB?FrameNo="+$scope.detail.FrameNo).success(function (res) {
				var file = new Blob([res], {type: 'application/pdf'});
				var fileURL = URL.createObjectURL(file);
						
				pdfFile = fileURL;

				if(pdfFile != null)
				{
                    CetakBstkbFactory.update($scope.detail).then(
                      function(res){
                          printJS(pdfFile);
                          console.log("cek bit sukses");
                          
                          CetakBstkbFactory.getData().then(
                            function(res){
                                $scope.grid.data = res.data.Result;
                            }
                          );
                         },
                      function(err){
                          bsNotify.show({
                                    title: "gagal",
                                    content: "Error pada cetakan.",
                                    type: 'danger'
                                }
                            );
                      }
                    );
                }
				else
				{
					bsNotify.show(
						{
							title: "gagal",
							content: "Error pada cetakan.",
							type: 'danger'
						}
					);
				}  
			})
			.error(function (res) {
				bsNotify.show(
						{
							title: "gagal",
							content: "Error pada cetakan.",
							type: 'danger'
						}
					);  
				});
        }

        $scope.btnCancelBSTKB = function()
        {
            angular.element('.ui.modal.CetakBSTKB').modal('hide');
        }

        $scope.ClosePreviewBSTKB = function(){
            $scope.PreviewBSTKBShow = false;
            $scope.ListCetakBSTKBShow = true;
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }

        $scope.selectRole = function(rows) {
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
        }
		
        // var btnAction = '<i title="Lihat Detail" class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatDetail(row.entity)" ></i>'
        var btnAction = 
        '<div style="padding:5px 5px 5px 5px"><u style="color:blue;" ng-click="grid.appScope.$parent.LihatDetail(row.entity)">BSTKB</u>&nbsp'
        + '<u style="color:blue" ng-click="grid.appScope.$parent.CetakThankYouModal(row.entity)">Thank You Letter</u></div>';

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Id', field: 'Id', visible: false },
                { name: 'Rencana Pengiriman', field: 'SentUnitDate', width: '10%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Nama Pelanggan', field: 'CustomerName', width: '17%' },
                { name: 'Sales', field: 'EmployeeName', width: '17%' },
                { name: 'No. Spk', field: 'FormSPKNo', width: '14%' },
                { name: 'No. Rangka', field: 'FrameNo', width: '13%' },
				{ name: 'Model', field: 'VehicleModelName', width: '13%' },
				{ name: 'Tipe', field: 'Description', width: '15%' },
				{ name: 'Warna', field: 'ColorName', width: '15%'},
                {
                   name: 'Action',
                   allowCellFocus: false,
                   width: 200,
                   pinnedRight: true,
                   enableColumnMenu: false,
                   enableSorting: false,
                   enableColumnResizing: false,
                   cellTemplate: btnAction
                }
            ]
        }

        $scope.CetakThankYouModal = function(Select){
            $scope.thankyou = angular.copy(Select);
			
			var raw_date=""+$scope.thankyou.SentUnitTime;
			var time = raw_date.split(':');
			var date = new Date();
			date.setHours(time[0]);
			date.setMinutes(time[1]);
			$scope.thankyou.SentUnitTime = date;
			
            if($scope.thankyou.StatusFullPayment == "True"){
                $scope.thankyou.StatusFullPayment = "Sudah Dibayar";
            }
            else{
                $scope.thankyou.StatusFullPayment = "Belum Dibayar";
            }
            
			angular.element('.ui.modal.CetakThankYouLetter').modal('show');
			angular.element('.ui.modal.CetakThankYouLetter').not(':first').remove(); 
        }

        $scope.btnCancelThankYou = function()
        {
            $scope.disabledBtnCetakThankYou = false;
            angular.element('.ui.modal.CetakThankYouLetter').modal('hide');
        }

        $scope.btnCetakThankYou = function()
        {
            $scope.disabledBtnCetakThankYou = true;
            var pdfFile = null;
			PrintRpt.print("sales/ThankYouLetter?DECId="+$scope.thankyou.DECId).success(function (res) {
				var file = new Blob([res], {type: 'application/pdf'});
				var fileURL = URL.createObjectURL(file);
						
				pdfFile = fileURL;

				if(pdfFile != null)
				{
					printJS(pdfFile);
				}	
				else
				{
					bsNotify.show(
						{
							title: "gagal",
							content: "Data gagal di print.",
							type: 'danger'
						}
                    );
                    $scope.disabledBtnCetakThankYou = false;
				}	
			})
			.error(function (res) {
				bsNotify.show(
					{
						title: "gagal",
						content: "Terjadi error pada cetakan.",
						type: 'danger'
					}
                ); 
                $scope.disabledBtnCetakThankYou = false;
				});
        }
    });