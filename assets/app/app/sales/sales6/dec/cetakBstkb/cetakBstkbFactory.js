angular.module('app')
    .factory('CetakBstkbFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;

        
        function toTimeSpan(date) {
            if (date != null || date != undefined) {
                var fix = 
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());
                return fix;
            } else {
                return null;
            }
        };


        return {
            getData: function() {
                var res = $http.get('/api/sales/ListBSTKB');
                return res;
            },

            create: function(CetakBstkb) {
                return $http.post('/api/fw/Role', [{
                    NamaSales: CetakBstkb.NamaSales,
                }]);
            },

            update: function(CetakBstkb) {
                console.log('CetakBstkb',CetakBstkb);
                
                // CetakBstkb.SentUnitTime = toTimeSpan(CetakBstkb.SentUnitTime);
                return $http.put('/api/sales/ListBSTKB', [CetakBstkb]);
            },

            delete: function(id) {
                return $http.delete('/api/fw/Role', { data:id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });