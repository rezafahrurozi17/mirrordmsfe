angular.module('app')
    .controller('FollowUpDecController', function($scope, $http,bsNotify, CurrentUser, FollowUpDecFactory, $timeout, MobileHandling) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.FollowUpDEC = true;
        $scope.surveyAfterDEC = false;

        $scope.status = null;

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            //----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------

        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mfollowUpDec = null; //Model

        //----------------------------------
        // Get Data
        //----------------------------------

        var gridData = [];
        $scope.getData = function() {
            FollowUpDecFactory.getData().then(function(res) {
                $scope.grid.data = res.data.Result;
                $scope.loading = false;
            });

        }

        $scope.LihatDetail = function(row) {
            $scope.status = "LihatDetail";
            $scope.mfollowUpDec = row;
            

            
            
            $scope.FollowUpDEC = false;
            $scope.surveyAfterDEC = true;


        }

        $scope.FollowUP = function(row) {
            $scope.mfollowUpDec = row;
            

            
            $scope.FollowUpDEC = false;
            $scope.surveyAfterDEC = true;

        }

        $scope.btnKembali = function() {
            $scope.FollowUpDEC = true;
            $scope.surveyAfterDEC = false;
            $scope.status = "";
        }

        $scope.btnSimpan = function() {
            FollowUpDecFactory.create($scope.mfollowUpDec).then(function(res) {
                $scope.getData();
                $scope.loading = false;
                $scope.FollowUpDEC = true;
                $scope.surveyAfterDEC = false;
                $scope.status = "";
            },
            function(err) {
				bsNotify.show({
						title: "Gagal",
						content: "Gagal Simpan Data",
						type: 'danger'
				});
                        
            }
			);
        }

        $scope.onSelectRows = function(rows) {
            //console.log("onSelectRows=>", rows);
        }

        $scope.toggleChecked = function(data) {
            if (data.IsTrue) {
                data.IsTrue = false;
            } else {
                data.IsTrue = true;
            }
        };


        var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-if="row.entity.FollowUpDECStatusName == \'Sudah\'" ng-click="grid.appScope.$parent.LihatDetail(row.entity)">Lihat Detail</u>&nbsp<u ng-if="(row.entity.FollowUpDECStatusName == null) || row.entity.FollowUpDECStatusName == \'Belum\'" ng-click="grid.appScope.$parent.FollowUP(row.entity)">Follow Up</u></span>'
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,

            columnDefs: [
                { name: 'id', field: 'Id', visible: false },
                { displayName:'toyota ID', name: 'toyota ID', field: 'ToyotaId', allowCellFocus: false, width: '10%' },
                { displayName:'nama pelanggan', name: 'nama pelanggan', field: 'CustomerName', allowCellFocus: false, width: '17%' },
                { displayName:'nama penerima', name: 'nama penerima', field: 'ReceiverName', allowCellFocus: false, width: '17%' },
                { displayName:'no spk', name: 'no spk', field: 'FormSPKNo', allowCellFocus: false, width: '13%' },
                { displayName:'no rangka', name: 'no rangka', field: 'FrameNo',allowCellFocus: false, width: '13%' },
                { displayName:'tanggal pengiriman', name: 'tanggal pengiriman', field: 'SentUnitDate', allowCellFocus: false, width: '10%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { displayName:'status follow up', name: 'status follow up', field: 'FollowUpDECStatusName', allowCellFocus: false, width: '10%' },
                {
                    name: 'Action',
                    allowCellFocus: false,
                    width: '10%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnAction
                }
            ]
        };
    });