angular.module('app')
    .factory('FollowUpDecFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/sales/DECFollowUp');
                return res;
            },

            create: function(objArray) {
                return $http.post('/api/sales/DECFollowUp', [objArray]);
            }
        }
    });