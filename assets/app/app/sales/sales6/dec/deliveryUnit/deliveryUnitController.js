angular.module('app')
.controller('TemplateDigitalSignatureController', function($scope, $http, $filter, CurrentUser, TemplateDigitalSignatureFactory,$timeout) {
    

    $scope.boundingBox = {
        width: 300,
        height: 300
    };  
    
});

 
///////////////////////////////////
////BEGIN SIGNATURE-PAD.JS/////////
///////////////////////////////////
!function(a,b){"function"==typeof define&&define.amd?define([],function(){return a.SignaturePad=b()}):"object"==typeof exports?module.exports=b():a.SignaturePad=b()}(this,function(){/*!
 * Signature Pad v1.5.3 | https://github.com/szimek/signature_pad
 * (c) 2016 Szymon Nowak | Released under the MIT license
 */
var a=function(a){"use strict";var b=function(a,b){var c=this,d=b||{};this.velocityFilterWeight=d.velocityFilterWeight||.7,this.minWidth=d.minWidth||.5,this.maxWidth=d.maxWidth||2.5,this.dotSize=d.dotSize||function(){return(this.minWidth+this.maxWidth)/2},this.penColor=d.penColor||"black",this.backgroundColor=d.backgroundColor||"rgba(0,0,0,0)",this.onEnd=d.onEnd,this.onBegin=d.onBegin,this._canvas=a,this._ctx=a.getContext("2d"),this.clear(),this._handleMouseDown=function(a){1===a.which&&(c._mouseButtonDown=!0,c._strokeBegin(a))},this._handleMouseMove=function(a){c._mouseButtonDown&&c._strokeUpdate(a)},this._handleMouseUp=function(a){1===a.which&&c._mouseButtonDown&&(c._mouseButtonDown=!1,c._strokeEnd(a))},this._handleTouchStart=function(a){if(1==a.targetTouches.length){var b=a.changedTouches[0];c._strokeBegin(b)}},this._handleTouchMove=function(a){a.preventDefault();var b=a.targetTouches[0];c._strokeUpdate(b)},this._handleTouchEnd=function(a){var b=a.target===c._canvas;b&&(a.preventDefault(),c._strokeEnd(a))},this._handleMouseEvents(),this._handleTouchEvents()};b.prototype.clear=function(){var a=this._ctx,b=this._canvas;a.fillStyle=this.backgroundColor,a.clearRect(0,0,b.width,b.height),a.fillRect(0,0,b.width,b.height),this._reset()},b.prototype.toDataURL=function(){var a=this._canvas;return a.toDataURL.apply(a,arguments)},b.prototype.fromDataURL=function(a){var b=this,c=new Image,d=window.devicePixelRatio||1,e=this._canvas.width/d,f=this._canvas.height/d;this._reset(),c.src=a,c.onload=function(){b._ctx.drawImage(c,0,0,e,f)},this._isEmpty=!1},b.prototype._strokeUpdate=function(a){var b=this._createPoint(a);this._addPoint(b)},b.prototype._strokeBegin=function(a){this._reset(),this._strokeUpdate(a),"function"==typeof this.onBegin&&this.onBegin(a)},b.prototype._strokeDraw=function(a){var b=this._ctx,c="function"==typeof this.dotSize?this.dotSize():this.dotSize;b.beginPath(),this._drawPoint(a.x,a.y,c),b.closePath(),b.fill()},b.prototype._strokeEnd=function(a){var b=this.points.length>2,c=this.points[0];!b&&c&&this._strokeDraw(c),"function"==typeof this.onEnd&&this.onEnd(a)},b.prototype._handleMouseEvents=function(){this._mouseButtonDown=!1,this._canvas.addEventListener("mousedown",this._handleMouseDown),this._canvas.addEventListener("mousemove",this._handleMouseMove),a.addEventListener("mouseup",this._handleMouseUp)},b.prototype._handleTouchEvents=function(){this._canvas.style.msTouchAction="none",this._canvas.style.touchAction="none",this._canvas.addEventListener("touchstart",this._handleTouchStart),this._canvas.addEventListener("touchmove",this._handleTouchMove),this._canvas.addEventListener("touchend",this._handleTouchEnd)},b.prototype.on=function(){this._handleMouseEvents(),this._handleTouchEvents()},b.prototype.off=function(){this._canvas.removeEventListener("mousedown",this._handleMouseDown),this._canvas.removeEventListener("mousemove",this._handleMouseMove),a.removeEventListener("mouseup",this._handleMouseUp),this._canvas.removeEventListener("touchstart",this._handleTouchStart),this._canvas.removeEventListener("touchmove",this._handleTouchMove),this._canvas.removeEventListener("touchend",this._handleTouchEnd)},b.prototype.isEmpty=function(){return this._isEmpty},b.prototype._reset=function(){this.points=[],this._lastVelocity=0,this._lastWidth=(this.minWidth+this.maxWidth)/2,this._isEmpty=!0,this._ctx.fillStyle=this.penColor},b.prototype._createPoint=function(a){var b=this._canvas.getBoundingClientRect();return new c(a.clientX-b.left,a.clientY-b.top)},b.prototype._addPoint=function(a){var b,c,e,f,g=this.points;g.push(a),g.length>2&&(3===g.length&&g.unshift(g[0]),f=this._calculateCurveControlPoints(g[0],g[1],g[2]),b=f.c2,f=this._calculateCurveControlPoints(g[1],g[2],g[3]),c=f.c1,e=new d(g[1],b,c,g[2]),this._addCurve(e),g.shift())},b.prototype._calculateCurveControlPoints=function(a,b,d){var e=a.x-b.x,f=a.y-b.y,g=b.x-d.x,h=b.y-d.y,i={x:(a.x+b.x)/2,y:(a.y+b.y)/2},j={x:(b.x+d.x)/2,y:(b.y+d.y)/2},k=Math.sqrt(e*e+f*f),l=Math.sqrt(g*g+h*h),m=i.x-j.x,n=i.y-j.y,o=l/(k+l),p={x:j.x+m*o,y:j.y+n*o},q=b.x-p.x,r=b.y-p.y;return{c1:new c(i.x+q,i.y+r),c2:new c(j.x+q,j.y+r)}},b.prototype._addCurve=function(a){var b,c,d=a.startPoint,e=a.endPoint;b=e.velocityFrom(d),b=this.velocityFilterWeight*b+(1-this.velocityFilterWeight)*this._lastVelocity,c=this._strokeWidth(b),this._drawCurve(a,this._lastWidth,c),this._lastVelocity=b,this._lastWidth=c},b.prototype._drawPoint=function(a,b,c){var d=this._ctx;d.moveTo(a,b),d.arc(a,b,c,0,2*Math.PI,!1),this._isEmpty=!1},b.prototype._drawCurve=function(a,b,c){var d,e,f,g,h,i,j,k,l,m,n,o=this._ctx,p=c-b;for(d=Math.floor(a.length()),o.beginPath(),f=0;d>f;f++)g=f/d,h=g*g,i=h*g,j=1-g,k=j*j,l=k*j,m=l*a.startPoint.x,m+=3*k*g*a.control1.x,m+=3*j*h*a.control2.x,m+=i*a.endPoint.x,n=l*a.startPoint.y,n+=3*k*g*a.control1.y,n+=3*j*h*a.control2.y,n+=i*a.endPoint.y,e=b+i*p,this._drawPoint(m,n,e);o.closePath(),o.fill()},b.prototype._strokeWidth=function(a){return Math.max(this.maxWidth/(a+1),this.minWidth)};var c=function(a,b,c){this.x=a,this.y=b,this.time=c||(new Date).getTime()};c.prototype.velocityFrom=function(a){return this.time!==a.time?this.distanceTo(a)/(this.time-a.time):1},c.prototype.distanceTo=function(a){return Math.sqrt(Math.pow(this.x-a.x,2)+Math.pow(this.y-a.y,2))};var d=function(a,b,c,d){this.startPoint=a,this.control1=b,this.control2=c,this.endPoint=d};return d.prototype.length=function(){var a,b,c,d,e,f,g,h,i=10,j=0;for(a=0;i>=a;a++)b=a/i,c=this._point(b,this.startPoint.x,this.control1.x,this.control2.x,this.endPoint.x),d=this._point(b,this.startPoint.y,this.control1.y,this.control2.y,this.endPoint.y),a>0&&(g=c-e,h=d-f,j+=Math.sqrt(g*g+h*h)),e=c,f=d;return j},d.prototype._point=function(a,b,c,d,e){return b*(1-a)*(1-a)*(1-a)+3*c*(1-a)*(1-a)*a+3*d*(1-a)*a*a+e*a*a*a},b}(document);return a});
///////////////////////////////////
/////END  SIGNATURE-PAD.JS/////////
///////////////////////////////////
 
angular.module('signature', []); 

angular.module('signature').directive('signaturePad', ['$interval', '$timeout', '$window',
  function ($interval, $timeout, $window) {
      'use strict';
      var signaturePad, element, EMPTY_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC';

      return {
          restrict: 'EA',
          replace: true,
          template: '<div class="signature" style="width: 100%; max-width:{{width}}px; height: 100%; max-height:{{height}}px;"><canvas style="display: block; margin: 0 auto;width: 100%; max-width:{{width}}px; height: 200px; max-height:200px" ng-mouseup="onMouseup()" ng-mousedown="notifyDrawing({ drawing: true })"></canvas></div>', 
          scope: {
              accept: '=?',
              clear: '=?',
              dataurl: '=?',
              height: '@',
              width: '@',
              notifyDrawing: '&onDrawing',
          },
          controller: [
            '$scope',
            function ($scope) {
                $scope.accept = function () {
                    return {

                        isEmpty: $scope.dataurl === EMPTY_IMAGE,
                        dataUrl: $scope.dataurl
                    };
                };

                $scope.onMouseup = function () {
                    $scope.updateModel();

                    // notify that drawing has ended
                    $scope.notifyDrawing({ drawing: false });
                };

                $scope.updateModel = function () {
                    /*
                     defer handling mouseup event until $scope.signaturePad handles
                     first the same event
                     */
                    $timeout().then(function () {
                        $scope.dataurl = $scope.signaturePad.isEmpty() ? EMPTY_IMAGE : $scope.signaturePad.toDataURL();
                    });
                };

                $scope.clear = function () {
                    $scope.signaturePad.clear();
                    $scope.dataurl = EMPTY_IMAGE;
                };

                $scope.$watch("dataurl", function (dataUrl) {
                    if (!dataUrl || $scope.signaturePad.toDataURL() === dataUrl) {
                        return;
                    }

                    $scope.setDataUrl(dataUrl);
                });
            }
          ],
          link: function (scope, element, attrs) {
              var canvas = element.find('canvas')[0];
              var parent = canvas.parentElement;
              var scale = 0;
              var ctx = canvas.getContext('2d');

              var width = parseInt(scope.width, 10);
              var height = parseInt(scope.height, 10);

              canvas.width = width;
              canvas.height = height;

              scope.signaturePad = new SignaturePad(canvas);

              scope.setDataUrl = function (dataUrl) {
                  ctx.setTransform(1, 0, 0, 1, 0, 0);
                  ctx.scale(1, 1);

                  scope.signaturePad.clear();
                  scope.signaturePad.fromDataURL(dataUrl);

                  $timeout().then(function () {
                      ctx.setTransform(1, 0, 0, 1, 0, 0);
                      ctx.scale(1 / scale, 1 / scale);
                  });
              };

              var calculateScale = function () {
                  var scaleWidth = Math.min(parent.clientWidth / width, 1);
                  var scaleHeight = Math.min(parent.clientHeight / height, 1);

                  var newScale = Math.min(scaleWidth, scaleHeight);

                  if (newScale === scale) {
                      return;
                  }

                  var newWidth = width * newScale;
                  var newHeight = height * newScale;
                  canvas.style.height = Math.round(newHeight) + "px";
                  canvas.style.width = Math.round(newWidth) + "px";

                  scale = newScale;
                  ctx.setTransform(1, 0, 0, 1, 0, 0);
                  ctx.scale(1 / scale, 1 / scale);
              };

              var resizeIH = $interval(calculateScale, 200);
              scope.$on('$destroy', function () {
                  $interval.cancel(resizeIH);
                  resizeIH = null;
              });

              angular.element($window).bind('resize', calculateScale);
              scope.$on('$destroy', function () {
                  angular.element($window).unbind('resize', calculateScale);
              });

              calculateScale();

              element.on('touchstart', onTouchstart);
              element.on('touchend', onTouchend);

              function onTouchstart() {
                  scope.$apply(function () {
                      // notify that drawing has started
                      scope.notifyDrawing({ drawing: true });
                  });
              }

              function onTouchend() {
                  scope.$apply(function () {
                      // updateModel
                      scope.updateModel();

                      // notify that drawing has ended
                      scope.notifyDrawing({ drawing: false });
                  });
              }
          }
      };
  }
]); 
 


// Backward compatibility
angular.module('ngSignaturePad', ['signature']);

//=============================================================================================================\\
angular.module('app')
    .controller('DeliveryUnitController', function($scope, $http, CurrentUser, DeliveryUnitFactory, $timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.daftarPengirimanShow = true;
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });

    $scope.uploadFiles=[];
	$scope.signature={};
    

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.filterData = {
        defaultFilter: [
            { name: 'Nama Pelanggan', value: 'CustomerName' },
            { name: 'No SPK', value: 'FormSPKNo' },
            { name: 'No Rangka', value: 'FrameNo' },
            { name: 'Status', value: 'StatusDECName' }
        ],
        advancedFilter: [
            { name: 'Nama Pelanggan', value: 'CustomerName', typeMandatory: 0 },
            { name: 'No SPK', value: 'FormSPKNo', typeMandatory: 0 },
            { name: 'No Rangka', value: 'FrameNo', typeMandatory: 0 },
            { name: 'Status', value: 'StatusDECName', typeMandatory: 0 }
        ]
    };

    $scope.user = CurrentUser.user();
    $scope.mDeliveryUnit = null; //Model
    $scope.xRole={selected:[]};
    $scope.disabledSimpanDariSign = false;

    //----------------------------------
    // Get Data
    //----------------------------------  
    $scope.mulaiDECModal = function(){
        angular.element('.ui.modal.daftarPengiriman').modal('hide');
        console.log("Sini",$scope.selected_data)
        $scope.daftarPengirimanShow = false;
        $scope.dataPelangganShow = true;
        //var getList = "?VehicleTypeId=" + $scope.selected_data.VehicleTypeId;
        
        DeliveryUnitFactory.decValidation($scope.selected_data.SpkId,$scope.selected_data.FrameNo).then(
            function(res){
                if(res.data.Result[0].isValidDEC == false){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: res.data.Result[0].Pesan,
                    });
                    $scope.disable_next = true
                }else{
                    $scope.disable_next = false

                    DeliveryUnitFactory.getDataChecklist().then(
                        function(res){
                            $scope.getDataInterior = res.data.Result;
                            return res.data;
                        }
                    );
            
                    DeliveryUnitFactory.getDataChecklist2().then(
                        function(res){
                            $scope.getDataDokumen = res.data.Result;
                            return res.data;
                        }
                    );

                }
               
            }
        );
    }

    $scope.kembaliDataPelanggan = function(){
        $scope.daftarPengirimanShow = true;
        $scope.dataPelangganShow = false;
    }

    $scope.NextCeklistDEC = function(){
        $scope.ceklistDEC = true;
        $scope.dataPelangganShow = false;
    }

    $scope.kembaliCeklistDEC = function(){
        $scope.ceklistDEC = false;
        $scope.dataPelangganShow = true;
    }
	
	$scope.kembaliCeklistBintang = function(){
		$scope.KuisionerDEC = true;
        $scope.ceklistDEC2 = false;
		$scope.SignatureDEC = false;
        $scope.FeedbackDEC = false;
    }

    $scope.NextCeklistDEC2 = function(){
        $scope.ceklistDEC2 = true;
        $scope.ceklistDEC = false;
    }

    $scope.kembaliCeklistDEC2 = function(){
        $scope.ceklistDEC2 = false;
        $scope.ceklistDEC = true;
    }

    $scope.NextCeklistKuisionerDEC = function(){
        
		
		if($scope.selected_data.UploadPhoto.length>0)
		{
			$scope.KuisionerDEC = true;
			$scope.ceklistDEC2 = false;
		}
		else if($scope.selected_data.UploadPhoto.length==0)
		{
			bsNotify.show(
								{
									title: "Anda Harus Memasukkan Gambar",
									content: "Anda Harus Memasukkan Gambar",
									type: 'warning'
								}
							);
		}

    }

    $scope.kembaliCeklistKuisionerDEC = function(){
          $scope.KuisionerDEC = false;
        $scope.ceklistDEC2 = true;
    }

    $scope.NextCeklistFeedbackDEC = function(){
        $scope.KuisionerDEC = false;
        $scope.FeedbackDEC = true;
    }

    $scope.kembaliCeklistFeedbackDEC = function(){
        $scope.KuisionerDEC = true;
        $scope.FeedbackDEC = false;
    }

    $scope.NextCeklistSignatureDEC = function(){
        $scope.SignatureDEC = true;
        $scope.FeedbackDEC = false;
    }

    $scope.kembaliCeklistSignatureDEC = function(){
        $scope.FeedbackDEC = true;
        $scope.SignatureDEC = false;
    }

    $scope.selectedItems = [];
    $scope.toggleChecked = function(data) {
        if (data.checked) {
            data.checked = false;
            var index = $scope.selectedItems.indexOf(data);
            $scope.selectedItems.splice(index, 1);
        } else {
            data.checked = true;
            $scope.selectedItems.push(data);   
        }
    };

    $scope.selectedItems1 = [];
    $scope.toggleChecked1 = function(data) {
        if (data.checked) {
            data.checked = false;
            var index = $scope.selectedItems1.indexOf(data);
            $scope.selectedItems1.splice(index, 1);
        } else {
            data.checked = true;
            $scope.selectedItems1.push(data);   
        }
    };

    $scope.afterGetData = function() {
        console.log('1');

        for(var i = 0; i < $scope.bind_data.data.length; i++)
        {
            console.log('2');
            var Da_SentUnitDate=angular.copy($scope.bind_data.data[i].SentUnitDate);

            var Da_Waktu_Sekarang = $filter('date')(angular.copy(""+$scope.bind_data.data[i].SentUnitTime), 'HH:mm');
            
            var RawSekarangTime_Get = Da_Waktu_Sekarang.split(':');
            Da_SentUnitDate.setHours(RawSekarangTime_Get[0]);  
            Da_SentUnitDate.setMinutes(RawSekarangTime_Get[1]);
            $scope.bind_data.data[i].nongol_senttime=angular.copy(Da_SentUnitDate);
        }
            
    }
    
	
	$scope.CreateGuid = function() {
            function _p8(s) {
                var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
            }
            return _p8() + _p8(true) + _p8(true) + _p8();
        }

    $scope.simpanDariSign = function(){
        $scope.disabledSimpanDariSign = true;
        for (var i in $scope.selectedItems) {
            $scope.selected_data.DetailCheckList.push({
                GroupChecklistDECId: $scope.selectedItems[i].GroupChecklistDECId,
                ChecklistItemDECId: $scope.selectedItems[i].ChecklistItemDECId,
                CheckListResult: $scope.selectedItems[i].checked
            });
        }

        for (var i in $scope.selectedItems1) {
            $scope.selected_data.DetailCheckList.push({
                GroupChecklistDECId: $scope.selectedItems1[i].GroupChecklistDECId,
                ChecklistItemDECId: $scope.selectedItems1[i].ChecklistItemDECId,
                CheckListResult: $scope.selectedItems1[i].checked
            });
        }
		
		//$scope.selected_data.UploadPhoto[0].UpDocObj= btoa($scope.selected_data.UploadPhoto[0].UpDocObj);
		$scope.selected_data.UploadPhoto[0].UploadDataPhoto={};
		$scope.selected_data.UploadPhoto[0].UploadDataPhoto.UpDocObj= btoa($scope.selected_data.UploadPhoto[0].UpDocObj);
		$scope.selected_data.UploadPhoto[0].UploadDataPhoto.FileName=$scope.selected_data.UploadPhoto[0].FileName;
		$scope.selected_data.UploadPhoto[0].PhotoName=$scope.selected_data.UploadPhoto[0].FileName;
		
		delete $scope.selected_data.UploadPhoto[0].UpDocObj;
		$scope.selected_data.StatusRatingId= $scope.DeliveryUnitRate;
		$scope.selected_data.UploadDigitalSignUrl= {};
		$scope.selected_data.UploadDigitalSignUrl.UpDocObj= btoa($scope.signature.dataUrl);
		$scope.selected_data.UploadDigitalSignUrl.FileName=$scope.CreateGuid();

		console.log("setan",$scope.selected_data);
		
		DeliveryUnitFactory.create($scope.selected_data).then(
            function(res){
                DeliveryUnitFactory.getData("").then(
                    function(res){
                        $scope.bind_data.data = res.data.Result;

						$scope.dataPelangganShow = false;
						$scope.ceklistDEC = false;
						$scope.ceklistDEC2 = false;
						$scope.SignatureDEC = false;
						$scope.FeedbackDEC = false;
						$scope.KuisionerDEC = false;
						$scope.daftarPengirimanShow = true;	
						bsNotify.show(
								{
									title: "Simpan Berhasil",
									content: "Simpan Berhasil",
									type: 'success'
								}
                            );
                            $scope.disabledSimpanDariSign = false;
                    });
				
            },
            function(err)
            {
				bsNotify.show(
						{
							title: "Simpan Gagal",
							content: "Simpan Gagal",
							type: 'warning'
						}
                    );
                    $scope.disabledSimpanDariSign = false;
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    
    $scope.selectRole = function(rows){
        //console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }

    $scope.onSelectRows = function(rows){
        //console.log("onSelectRows=>",rows);
    }

    $scope.DeliveryUnitRate = 0;
    $scope.max = 5;
    $scope.isReadonly = false;

    $scope.hoveringOver = function(value) {
      $scope.overStar = value;
      $scope.percent = 100 * (value / $scope.max);
    };

    $scope.ratingStates = [
      {
          stateOn : 'glyphicon-star', 
          stateOff: 'glyphicon-star-empty'
      }
    ];
});