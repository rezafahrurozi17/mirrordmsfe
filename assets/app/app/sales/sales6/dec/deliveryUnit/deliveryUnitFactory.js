angular.module('app')
  .factory('DeliveryUnitFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(param) {
        var res = $http.get('/api/sales/DeliveryUnit'+param);       
        return res;
      },

      getDataChecklist: function() {
          var res = $http.get('/api/sales/InteriorItemCheckListDEC');
          return res;
      },

      getDataChecklist2: function() {
        var res = $http.get('/api/sales/DokumenItemCheckListDEC');
        return res;
      },

      decValidation: function(spkId,frameNo){
        console.log("[decValidation]");
        var res = $http.get('/api/sales/DECValidasiDeliveryUnit', {
            params: {
                spkId: spkId,
                frameNo: frameNo
            }
        });
        //console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      create: function(deliveryUnit) {
        return $http.post('/api/sales/DeliveryUnit', [deliveryUnit]);
      },

      update: function(deliveryUnit){
        return $http.put('/api/fw/Role', [deliveryUnit]);
      },

      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });