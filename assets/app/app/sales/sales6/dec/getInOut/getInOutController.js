
angular.module('app')
    .controller('GetInOutController', function($scope, $http, CurrentUser, GetInOutFactory, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.gateInOutShow = true;
        $scope.getInScanShow = false;
        $scope.getOutScanShow = false;

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        //$scope.mfollowUpDec = null; //Model

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.scanGetInBtn = function(){
            $scope.gateInOutShow = false;
            $scope.getOutScanShow = false;
            $scope.getInScanShow = true;
        }

        $scope.scanGetOutBtn = function(){
            $scope.gateInOutShow = false;
            $scope.getInScanShow = false;
            $scope.getOutScanShow = true;
        }
});


