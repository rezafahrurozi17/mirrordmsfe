angular.module('app')
    .factory('GetInOutFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/sales/ListGateIn');
                //console.log('res=>',res);
                return res;
            },
            create: function(GetInOut) {
                return $http.post('/api/fw/Role', [{
                    NamaSales: GetInOut.NamaSales,

                }]);
            },
            update: function(GetInOut) {
                return $http.put('/api/fw/Role', [{
                    NoSPK: GetInOut.NoSPK,
                    NamaSPK: GetInOut.NamaSales,

                    //pid: role.pid,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd