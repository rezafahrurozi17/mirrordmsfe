angular.module('app')
  .factory('ListDeliveryUnitFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
          var res=$http.get('/api/sales/ListDeliveryUnitAdmin');
          return res;
      },

      getDataAdministrasi: function() {
          var res = $http.get('/api/sales/MActivityChecklistAdminPayment');
          console.log('res=>',res);
          return res;
      },

      create: function(listDelivery) {
        //return $http.post('/api/sales/SDECDetailCheckList', [listDelivery]);
		return $http.post('/api/sales/ListDeliveryUnitAdmin', [listDelivery]);
      },

      update: function(listDelivery){
        return $http.put('/api/sales/ListDeliveryUnitAdmin', [listDelivery]);
      },

      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });