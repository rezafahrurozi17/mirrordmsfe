angular.module('app')
    .controller('ListDeliveryUnitController', function($window, $scope, $http, PrintRpt, CurrentUser, ListDeliveryUnitFactory, $timeout, bsNotify, MobileHandling) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.ShowListDelivery = true;
        $scope.ShowCeklistAdministrasi = false;
        $scope.ShowListDeliveryUnitPrintPreview = false;

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            //----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------
        });


        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mListDeliveryUnit = null; //Model
        $scope.xRole = { selected: [] };
        $scope.disabledBtnSimpan = false;

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            ListDeliveryUnitFactory.getData().then(
                function(res) {
                    $scope.grid.data = res.data.Result;
                    $scope.loading = false;
                    return res.data.Result;
                },
                function(err) {
                    bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
                }
            );
        }

        $scope.MasukPengecekan = function(Selectdata) {
            $scope.selectedItems=[];
			$scope.ShowCeklistAdministrasi = true;
            $scope.ShowListDelivery = false;
            $scope.ViewDetail = Selectdata;
            var getList = "?VehicleTypeId=" + $scope.ViewDetail.VehicleTypeId;
            ListDeliveryUnitFactory.getDataAdministrasi().then(
                function(res) {
                    $scope.getAdministrasi = res.data.Result;
					for(var i = 0; i < $scope.ViewDetail.DetailList.length; ++i)
					{
						for(var x = 0; x < $scope.getAdministrasi.length; ++x)
						{
							if($scope.ViewDetail.DetailList[i].ChecklistItemDECId==$scope.getAdministrasi[x].ChecklistItemDECId && $scope.ViewDetail.DetailList[i].CheckListResult==true)
							{
								//$scope.getAdministrasi[x].checked=true;
								$scope.toggleChecked($scope.getAdministrasi[x]);
							}
						}
					}
					console.log("setan1",$scope.getAdministrasi);
					console.log("setan2",$scope.selectedItems);
                    return res.data;
                }
            );
        }

        $scope.btnkembaliCheckListAdmin = function() {
            $scope.ShowListDelivery = true;
            $scope.ShowCeklistAdministrasi = false;
        }

        $scope.selectedItems = [];

        $scope.toggleChecked = function(data) {
            if (data.checked) {
                data.checked = false;
                var index = $scope.selectedItems.indexOf(data);
                $scope.selectedItems.splice(index, 1);
            } else {
                data.checked = true;
                $scope.selectedItems.push(data);
            }
			
			if($scope.selectedItems.length<=0)
			{
				$scope.ViewDetail.DokumenLengkapBit=false;
			}
        };

        $scope.btnSimpan = function() {
            $scope.disabledBtnSimpan = true;
            $scope.ViewDetail.DetailList = [];
            for (var i in $scope.selectedItems) {
                // if($scope.ViewDetail.DokumenLengkapBit == true)
				// { var statusAdmin = 1; }
                // else 
				// { var statusAdmin = 2; }
                $scope.ViewDetail.DetailList.push({
                    ChecklistAdminPaymentId: $scope.selectedItems[i].ChecklistAdminPaymentId,
                    CheckListResult: $scope.selectedItems[i].checked,
                    
                });
            }

            // console.log('tua => ', $scope.ViewDetail);
            // console.log('bit => ', $scope.StatusAdministrasiBit);

            ListDeliveryUnitFactory.create($scope.ViewDetail).then(
                function(res) {
                    ListDeliveryUnitFactory.getData().then(
                        function(res) {
                            $scope.grid.data = res.data.Result;
                            $scope.btnkembaliCheckListAdmin();
                            bsNotify.show({
                                title: "Simpan Berhasil",
                                content: "Simpan Berhasil",
                                type: 'success'
                            });
                            $scope.disabledBtnSimpan = false;
                        });
                },
                function(err) {
                    bsNotify.show({
                        title: "Simpan Gagal",
                        content: "Simpan Gagal",
                        type: 'warning'
                    });
                    $scope.disabledBtnSimpan = false;
                }
            );
        }

        $scope.ClosePreviewListDeliveryUnit = function() {
            $scope.ShowListDelivery = true;
            $scope.ShowListDeliveryUnitPrintPreview = false;
        }

        $scope.btnCetakListDeliveryUnit = function() {
            //Call PrintRpt $scope.Cetak.ToyotaId	$scope.Cetak.CustomerName
            $scope.ListDeliveryUnitGambarnya = {};
            var pdfFile = null;
            PrintRpt.print("sales/PrintToyotaId?ToyotaId=" + $scope.Cetak.ToyotaId + "&CustomerName=" + $scope.Cetak.CustomerName + "").success(function(res) {
                    $scope.ListDeliveryUnitGambarnya = "data:image/PNG;base64," + UlalaToBase64(res);
                })
                .error(function(res) {
                    bsNotify.show(
						{
							title: "gagal",
							content: "Error cetakan..",
							type: 'danger'
						}
					);
                });
        }

        $scope.btnCetakListDeliveryUnitDonlot = function() {
            ListDeliveryUnitFactory.update($scope.Cetak).then(
                function(res) {
                    ListDeliveryUnitFactory.getData().then(
                        function(res) {
                            $scope.grid.data = res.data.Result;
                        });
                },
                function(err) {
                    bsNotify.show(
						{
							title: "gagal",
							content: "Data gagal di update.",
							type: 'danger'
						}
					);
                }
            );

            $scope.DaListDeliveryUnitDonlotToyotaId = null;
            $scope.DaListDeliveryUnitDonlotToyotaId = $scope.ListDeliveryUnitGambarnya;
            $scope.TheFileTipeToDownload = $scope.DaListDeliveryUnitDonlotToyotaId.split(';')[0];
            var blob2 = dataURItoBlob($scope.DaListDeliveryUnitDonlotToyotaId);
            var blob = new Blob([blob2], { type: $scope.TheFileTipeToDownload.substring(5) });
            ////change download.pdf to the name of whatever you want your file to be
            saveAs(blob, "TheToyotaID.png");

        };

        $scope.btnCetakToyotaId = function(Selectdata) {

            $scope.ShowListDelivery = false;
            $scope.ShowListDeliveryUnitPrintPreview = true;
            $scope.Cetak = Selectdata;
            $scope.Cetak.PrintToyotaId = true;

            $scope.btnCetakListDeliveryUnit();
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }

        $scope.selectRole = function(rows) {
            //console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            //console.log("onSelectRows=>", rows);
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        // var btnAction = '<span style="color:blue"><p style="padding:5px 0 0 5px" >'
                    // + '<u ng-if="row.entity.StatusAdministrasi ==\'Belum Siap\'" ng-click="grid.appScope.$parent.MasukPengecekan(row.entity)">Masuk Pengecekan</u>&nbsp'
                    // + '<u ng-if="row.entity.StatusAdministrasi ==\'Sudah Siap\'" ng-click="grid.appScope.$parent.btnCetakToyotaId(row.entity)">Cetak Toyota ID</u></span>&nbsp'
                    // + '<u ng-if="row.entity.StatusAdministrasi ==\'Menunggu Approval\'"></u></span>';
					
		var btnAction = '<span ><p style="padding:5px 0 0 5px" >'
                    + '<u ng-if="(row.entity.StatusAdministrasi ==\'Belum Siap\'||row.entity.DokumenLengkapBit==false) && row.entity.FormSPKNo != null" ng-click="grid.appScope.$parent.MasukPengecekan(row.entity)"><i title="Masuk Pengecekan" class="fa fa-fw fa-lg fa-list-alt"></i></u>&nbsp'
                    + '<u ng-if="row.entity.StatusAdministrasi ==\'Sudah Siap\'  && row.entity.DokumenLengkapBit==true" ng-click="grid.appScope.$parent.btnCetakToyotaId(row.entity)"><i title="Cetak Toyota ID" class="fa fa-fw fa-lg fa-print"></i></u></span>&nbsp'
                    + '<u style="color:blue" ng-if="row.entity.StatusAdministrasi ==\'Menunggu Approval\'"></u></span>';



        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 15, 30, 40, 50, 60, 70, 80, 90, 100, 200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'DEC Id', field: 'DECId', width: '5%', visible: false },
                { name: 'Janji pengiriman', field: 'SentUnitDate', width: '15%', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { name: 'Nama pelanggan', field: 'CustomerName', width: '15%' },
                { name: 'Salesman', field: 'EmployeeName', width: '15%' },
                { displayName: 'No. SPK',name: 'No SPK', field: 'FormSPKNo', width: '15%' },
                { name: 'No rangka', field: 'FrameNo', width: '15%' },
                { name: 'Model', field: 'VehicleModelName', width: '15%' },
                { displayName: 'Tipe',name: 'Tipe', field: 'Description', width: '15%' },
                { name: 'Warna', field: 'ColorName', width: '15%' },
                { name: 'Status Administrasi', field: 'StatusAdministrasi', width: '15%' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '13%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: btnAction
                }
            ]
        };
    });