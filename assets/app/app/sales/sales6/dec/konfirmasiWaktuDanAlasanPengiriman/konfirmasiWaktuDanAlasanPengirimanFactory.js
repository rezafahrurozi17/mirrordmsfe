angular.module('app')
  .factory('KonfirmasiWaktuDanAlasanPengirimanFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
        getData: function(param) {
            var res = $http.get('/api/sales/DECList'+param);
            return res;
        },

        getDataKetPengecualian: function() {
            var res = $http.get('/api/sales/DECDetailDeliveryExcReason');
            return res;
        },

        create: function(KonfirmasiWaktu) {
        return $http.post('/api/sales/DECList', [{                                           
            NamaAlasanPengecualianPengiriman: KonfirmasiWaktu.NamaAlasanPengecualianPengiriman,
            InputByAdmin: KonfirmasiWaktu.InputByAdmin,
            InputBySalesman: KonfirmasiWaktu.InputBySalesman}]);
        },

        update: function(KonfirmasiWaktu){
            return $http.put('/api/sales/DECList', [KonfirmasiWaktu]);
        },

        delete: function(id) {
            return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
        },
    }
  });