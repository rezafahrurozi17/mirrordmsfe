angular.module('app')
    .controller('KonfirmasiWaktuDanAlasanPengirimanController', function($scope, $http,$rootScope, CurrentUser, KonfirmasiWaktuDanAlasanPengirimanFactory,$timeout,bsNotify, $filter) {
		
		//----------------------------------
    	// Initialization
		//----------------------------------
		$scope.disabledSimpan = false;
		
		//----------------------------------
        // Start-Up
        //----------------------------------
    	$scope.ShowListUnitDelivery = true;
		$scope.show_modal={show:false};
		
		$scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 30;
		$scope.mytime.setMinutes('00');
		
		$scope.mytime2 = new Date();
        $scope.hstep = 1;
        $scope.mstep = 30;
		$scope.mytime2.setMinutes('00');
		
		$scope.filterData = {
            defaultFilter: [
                { name: 'Nama Pelanggan', value: 'CustomerName' },
                { name: 'No SPK', value: 'FormSPKNo' },
                { name: 'Tipe Pengiriman', value: 'DirectDeliveryStatus' }
            ],
            advancedFilter: [
                { name: 'Nama Pelanggan', value: 'CustomerName', typeMandatory: 0 },
                { name: 'No SPK', value: 'FormSPKNo', typeMandatory: 0 },
                { name: 'Tipe Pengiriman', value: 'DirectDeliveryStatus', typeMandatory: 0 }
            ]
		};				

	   	$scope.List_Untuk_Delivery_Action_Selected = function (Id) {
			$scope.List_Untuk_Delivery_Selected_Id = Id;
		};

		$scope.TodayDate_Raw = new Date();
        $scope.TodayDate = $scope.TodayDate_Raw.getFullYear() + '-' + ('0' + ($scope.TodayDate_Raw.getMonth() + 1)).slice(-2) + '-' + ('0' + $scope.TodayDate_Raw.getDate()).slice(-2);

		$scope.dateOptionsDECmin1 = {
            // formatYear: 'yyyy-mm-dd',
            // startingDay: 1,
            // minMode: 'year'
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 1,
            //minDate: new Date(),
        };
		
		KonfirmasiWaktuDanAlasanPengirimanFactory.getDataKetPengecualian().then(
	        function(res){
				$scope.DataKetPengecualian = res.data.Result;
				// console.log("DECFAC", $scope.DataKetPengecualian);
	            return res.data;
	        }
	    );

		$scope.KesiapanUnitModal = function(){
			angular.element('.ui.modal.KonfirmasiWaktu').modal('hide');
			$scope.ShowListUnitDelivery = false;
			$scope.ShowDetailWaktuDanLokasiPengiriman = false;
			$scope.ShowUnitReadiness = true;
		}
		
		$scope.KonfirmasiWaktu_Paksa_Tanggal = function () {
			for(var i = 0; i < $scope.bind_data.data.length; ++i) //ini tetep tadi ga di otak atik
			 {
            console.log('2');
            var Da_SentUnitDate=angular.copy($scope.bind_data.data[i].SentUnitDate);

			var Da_Waktu_Sekarang = $filter('date')(angular.copy(""+$scope.bind_data.data[i].SentUnitTime), 'HH:mm');
            
            var RawSekarangTime_Get = Da_Waktu_Sekarang.split(':');
            Da_SentUnitDate.setHours(RawSekarangTime_Get[0]);  
            Da_SentUnitDate.setMinutes(RawSekarangTime_Get[1]);
			$scope.bind_data.data[i].nongol_senttime=angular.copy(Da_SentUnitDate);
			var resultSplit = $scope.bind_data.data[i].SentUnitTime.split(":") // untuk memisahkan (split), suatu string dengan acuan ":" ex =11:55:59 
			$scope.bind_data.data[i].SentUnitTime = resultSplit[0] + ":" + resultSplit[1]; // mengambil index pertama dan kedua, index 1 nya berarti menitnya ya? jam , berarti yg index 0 buat yg mana
        }
			// ==>>>>> ini tadinya isi function nya {
			// 	var raw_date=""+$scope.bind_data.data[i].SentUnitTime;
			// 	var time = raw_date.split(':');
			// 	var date = new Date();
			// 	date.setHours(time[0]);
			// 	date.setMinutes(time[1]);
			// 	$scope.bind_data.data[i].SentUnitTime_Akalin = date;
				
			// }
		}

		$scope.KonfirmasiWaktuModal = function(){
			if($scope.selected_data.DirectDeliveryStatus = "Direct Delivery"){
				$scope.selected_data.DirectDeliveryStatus = true;
			}
			else{
				$scope.selected_data.DirectDeliveryStatus = false;
			}
			//var Eta1 = angular.copy($scope.selected_data.ETACustomer1);
			//var Eta2 = angular.copy($scope.selected_data.ETACustomer1);
			var jam = $scope.selected_data.ETACustomer1.slice(0, 2);
			var menit = $scope.selected_data.ETACustomer1.slice(3, 5);

			//var Eta3 = angular.copy($scope.selected_data.ETACustomer2);
			//var Eta4 = angular.copy($scope.selected_data.ETACustomer2);
			var jam2 = $scope.selected_data.ETACustomer2.slice(0, 2);
			var menit2 = $scope.selected_data.ETACustomer2.slice(3, 5);


			console.log('jam',jam);
			console.log('menit',menit);  
			//$scope.mytime = new date($scope.selected_data.ETACustomer1);
			$scope.mytime = new Date();
			$scope.mytime.setHours(jam);
			$scope.mytime.setMinutes(menit);

			$scope.mytime2 = new Date();
			$scope.mytime2.setHours(jam2);
			$scope.mytime2.setMinutes(menit2);

			angular.element('.ui.modal.KonfirmasiWaktu').modal('hide');
			$scope.ShowListUnitDelivery = false;
			$scope.ShowUnitReadiness = false;
			$scope.ShowDetailWaktuDanLokasiPengiriman = true;
			$scope.btnSimpanShow = false;
			$scope.btnUbahShow = true;

			$scope.selectedItems = [];
			
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_PengirimanDenganPengecualian_EnableDisable=true;
			for(var i = 0; i < $scope.DataKetPengecualian.length; ++i)
            {    
				$scope.DataKetPengecualian[i].checked=false;
                for(var z = 0; z < $scope.selected_data.DECDetailDeliveryExcReasonView.length; ++z)
                {
                    if($scope.DataKetPengecualian[i].DeliveryExcReasonId==$scope.selected_data.DECDetailDeliveryExcReasonView[z].DeliveryExcReasonId)
                    {
                        $scope.DataKetPengecualian[i].checked=false;
                        $scope.toggleChecked($scope.DataKetPengecualian[i]);
                        break;
                    }
                }
                
            }
            $scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_PengirimanDenganPengecualian_EnableDisable=false;

			
			
			
		}

		

		$scope.selectedItems = [];
		$scope.toggleChecked = function (data) {
			// if($scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_PengirimanDenganPengecualian_EnableDisable)
			// {
				console.log('before',$scope.selectedItems);
		    	if (angular.isUndefined(data.checked) || data.checked) {
		        	data.checked = false;
		        	var index = $scope.selectedItems.indexOf(data);
		        	$scope.selectedItems.splice(index, 1);
		    	} else {
		        	data.checked = true;
		        	$scope.selectedItems.push(data);
				}
				console.log('after',$scope.selectedItems);
			// }
		};

		$scope.Go_To_Unit_Readiness = function (Id) {
			for(var i = 0; i < $scope.List_Untuk_Delivery_To_Repeat.length; ++i)
			{
				if($scope.List_Untuk_Delivery_To_Repeat[i].Id == Id)
				{
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_UnitReadiness_NamaPelanggan=$scope.List_Untuk_Delivery_To_Repeat[i].ProspectName;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_UnitReadiness_NoSpk=$scope.List_Untuk_Delivery_To_Repeat[i].FormSPKNo;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_UnitReadiness_NoRangka=$scope.List_Untuk_Delivery_To_Repeat[i].No_Rangka;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_UnitReadiness_TanggalJanjiPengiriman=$scope.List_Untuk_Delivery_To_Repeat[i].Tanggal_Pengiriman;
				}
			}
		};
		
		$scope.Go_To_Konfirmasi_Waktu_Dan_Lokasi = function (Id) {
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NoSpk_EnableDisable=false;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NamaPenerima_EnableDisable=false;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable=false;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_WaktuPengiriman_EnableDisable=false;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_AlamatPengiriman_EnableDisable=false;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_DirectDariPDCC_EnableDisable=false;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_PengirimanDenganPengecualian_EnableDisable=false;
			$scope.Kebutuhan_FisikDisabled = false;
			
			for(var i = 0; i < $scope.List_Untuk_Delivery_To_Repeat.length; ++i)
			{
				if($scope.List_Untuk_Delivery_To_Repeat[i].Id == Id)
				{
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NoSpk=$scope.List_Untuk_Delivery_To_Repeat[i].No_SPK;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NoRangka=$scope.List_Untuk_Delivery_To_Repeat[i].No_Rangka;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NamaPelanggan=$scope.List_Untuk_Delivery_To_Repeat[i].Nama_Pelanggan;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NamaPenerima=$scope.List_Untuk_Delivery_To_Repeat[i].Nama_Penerima;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_Model=$scope.List_Untuk_Delivery_To_Repeat[i].Model;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_Tipe=$scope.List_Untuk_Delivery_To_Repeat[i].Tipe;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_Warna=$scope.List_Untuk_Delivery_To_Repeat[i].Warna;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman=$scope.List_Untuk_Delivery_To_Repeat[i].Tanggal_Pengiriman;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_WaktuPengiriman=$scope.List_Untuk_Delivery_To_Repeat[i].Waktu_Pengiriman;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_AlamatPengiriman=$scope.List_Untuk_Delivery_To_Repeat[i].Alamat_Pengiriman;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_DirectDariPDCC=$scope.List_Untuk_Delivery_To_Repeat[i].DirectDariPDCC;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_Driver=$scope.List_Untuk_Delivery_To_Repeat[i].Driver;
					$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_PengirimanDenganPengecualian=false;
				}
			}
		};
		
		$scope.BackToListUntukDelivery = function (Id) {
			$scope.ShowListUnitDelivery = true;
			$scope.ShowUnitReadiness = false;
			$scope.ShowDetailWaktuDanLokasiPengiriman = false;
			// $scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NamaPenerima_EnableDisable=false;
			// $scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable=false;
			// $scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_WaktuPengiriman_EnableDisable=false;
			// $scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_AlamatPengiriman_EnableDisable=false;
			// $scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_DirectDariPDCC_EnableDisable=false;
			// $scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_PengirimanDenganPengecualian_EnableDisable=false;
			$scope.Kebutuhan_FisikDisabled = false;
		};

		$scope.Ubah = function () {
			$scope.btnUbahShow = false;
			$scope.btnSimpanShow = true;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_NamaPenerima_EnableDisable=true;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_TanggalPengiriman_EnableDisable=true;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_WaktuPengiriman_EnableDisable=true;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_AlamatPengiriman_EnableDisable=true;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_DirectDariPDCC_EnableDisable=true;
			$scope.Dec_KonfirmasiWaktuDanAlasanPengiriman_DetailWaktiDanLokasiPengiriman_PengirimanDenganPengecualian_EnableDisable=true;
			$scope.Kebutuhan_FisikDisabled = true;
		};
		
		$scope.Batal = function () {
			$scope.Go_To_Konfirmasi_Waktu_Dan_Lokasi($scope.List_Untuk_Delivery_Selected_Id);//ini refresh
		};
		
		$scope.Simpan = function () {
			$scope.disabledSimpan = true;

	  		$scope.selected_data.DECDetailDeliveryExcReasonView = $scope.selectedItems;
			try {
				$scope.selected_data.SentUnitDate = $scope.selected_data.SentUnitDate.getFullYear() + '-' +
					('0' + ($scope.selected_data.SentUnitDate.getMonth() + 1)).slice(-2) + '-' +
					('0' + $scope.selected_data.SentUnitDate.getDate()).slice(-2) + 'T' +
					(($scope.selected_data.SentUnitDate.getHours() < '10' ? '0' : '') + $scope.selected_data.SentUnitDate.getHours()) + ':' +
					(($scope.selected_data.SentUnitDate.getMinutes() < '10' ? '0' : '') + $scope.selected_data.SentUnitDate.getMinutes()) + ':' +
					(($scope.selected_data.SentUnitDate.getSeconds() < '10' ? '0' : '') + $scope.selected_data.SentUnitDate.getSeconds());

					$scope.selected_data.SentUnitDate.slice(0, 10);
			} catch (e1) {
				$scope.selected_data.SentUnitDate = null;
			}

			$scope.selected_data.ETACustomer1 = $scope.mytime;
			$scope.selected_data.ETACustomer2 = $scope.mytime2;

			try {
				$scope.selected_data.ETACustomer1 = $scope.selected_data.ETACustomer1.getFullYear() + '-' +
					('0' + ($scope.selected_data.ETACustomer1.getMonth() + 1)).slice(-2) + '-' +
					('0' + $scope.selected_data.ETACustomer1.getDate()).slice(-2) + 'T' +
					(($scope.selected_data.ETACustomer1.getHours() < '10' ? '0' : '') + $scope.selected_data.ETACustomer1.getHours()) + ':' +
					(($scope.selected_data.ETACustomer1.getMinutes() < '10' ? '0' : '') + $scope.selected_data.ETACustomer1.getMinutes()) + ':' +
					(($scope.selected_data.ETACustomer1.getSeconds() < '10' ? '0' : '') + $scope.selected_data.ETACustomer1.getSeconds());
					 $scope.selected_data.ETACustomer1=$scope.selected_data.ETACustomer1.slice(-8);
					// $scope.selected_data.ETACustomer1.slice(0, 10);
					// $scope.selected_data.ETACustomer1.slice(11, 19);
			} catch (e1) {
				$scope.selected_data.ETACustomer1 = null;
			}

			try {
				$scope.selected_data.ETACustomer2 = $scope.selected_data.ETACustomer2.getFullYear() + '-' +
					('0' + ($scope.selected_data.ETACustomer2.getMonth() + 1)).slice(-2) + '-' +
					('0' + $scope.selected_data.ETACustomer2.getDate()).slice(-2) + 'T' +
					(($scope.selected_data.ETACustomer2.getHours() < '10' ? '0' : '') + $scope.selected_data.ETACustomer2.getHours()) + ':' +
					(($scope.selected_data.ETACustomer2.getMinutes() < '10' ? '0' : '') + $scope.selected_data.ETACustomer2.getMinutes()) + ':' +
					(($scope.selected_data.ETACustomer2.getSeconds() < '10' ? '0' : '') + $scope.selected_data.ETACustomer2.getSeconds());

					$scope.selected_data.ETACustomer2=$scope.selected_data.ETACustomer2.slice(-8);
					// $scope.selected_data.ETACustomer2.slice(0, 10);
					// $scope.selected_data.ETACustome21.slice(11, 19);

			} catch (e1) {
				$scope.selected_data.ETACustomer2 = null;
			}

			KonfirmasiWaktuDanAlasanPengirimanFactory.update($scope.selected_data).then(
	            function(res){
					bsNotify.show(
						{
							title: "Simpan Sukses",
							content: "Konfirmasi Waktu Dan Alasan Pengiriman Sukses",
							type: 'success'
						}
					);
					$scope.disabledSimpan = false;
					$scope.BackToListUntukDelivery();
					$rootScope.$emit("RefreshDirective", {});
	            },
	            function(err){
					bsNotify.show(
						{
							title: "Simpan Gagal",
							content: err.data.Message,
							type: 'warning'
						}
					);
					$scope.disabledSimpan = false;
					$scope.BackToListUntukDelivery();
	            }
	        );
		};
});
