angular.module('app')
  .factory('ApprovalPengirimanDenganPengecualianFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();//tadinya ini kurung ga ada
    return {
      getData: function() {
		var res=$http.get('/api/sales/DECApprovalList');
		return res;
        
      },
	  
	  
	  
      create: function(ApprovalPengirimanDenganPengecualian) {
        return $http.post('/api/sales/CProspectListApproval', [{
                                            //AppId: 1,
                                            SalesProgramName: ApprovalPengirimanDenganPengecualian.SalesProgramName}]);
      },
      update: function(ApprovalPengirimanDenganPengecualian){
        return $http.put('/api/sales/CProspectListApproval', [{
                                            SalesProgramId: ApprovalPengirimanDenganPengecualian.SalesProgramId,
                                            SalesProgramName: ApprovalPengirimanDenganPengecualian.SalesProgramName}]);
      },
	  
	  SetujuApprovalPengirimanDenganPengecualian: function(ApprovalPengirimanDenganPengecualian){
		return $http.put('/api/sales/DECApprovalList/Approver', [ApprovalPengirimanDenganPengecualian]);									
      },
	  
	  TidakSetujuApprovalPengirimanDenganPengecualian: function(ApprovalPengirimanDenganPengecualian){
        return $http.put('/api/sales/DECApprovalList/Reject', [{
                                            OutletId: ApprovalPengirimanDenganPengecualian.OutletId,
											ApprovalDECId: ApprovalPengirimanDenganPengecualian.ApprovalDECId,
											DECId: ApprovalPengirimanDenganPengecualian.DECId,
                                            RejectReason: ApprovalPengirimanDenganPengecualian.RejectReason}]);
		//return $http.put('/api/sales/DECApprovalList/Reject', [ApprovalPengirimanDenganPengecualian]);									
      },
	  
      delete: function(id) {
        return $http.delete('/api/sales/CProspectListApproval',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd