angular.module('app')
.controller('ApprovalPengirimanDenganPengecualianController', function($scope, $http,$filter, CurrentUser, ApprovalPengirimanDenganPengecualianFactory,$timeout,bsNotify) {
	$scope.ApprovalPengirimanDenganPengecualianMain=true;
	IfGotProblemWithModal();
	
	
	
	$scope.$on('$viewContentLoaded', function () {
            //$scope.loading=true; tadinya true
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        //$scope.mApprovalPengirimanDenganPengecualian = null; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
		ApprovalPengirimanDenganPengecualianFactory.getData().then(
				function(res){
					for(var i = 0; i < res.data.Result.length; ++i)
					{
						var ToBeTruncated=res.data.Result[i]['DECDetailDeliveryExcReasonView'][0].DeliveryExcReasonName;
						res.data.Result[i]['ApprovalPengirimanDenganPengecualianTruncatedAlasan']=ToBeTruncated.substring(0,16)+"...";
					}
				
					$scope.grid.data = res.data.Result;
					$scope.loading=false;
					return res.data;
				},
				function(err){
					bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
				}
			);
		}
		
        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function (rows) {
            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function (rows) {
        }
		
		$scope.ActionApprovalPengirimanDenganPengecualian = function (SelectedData) {
			$scope.mApprovalPengirimanDenganPengecualian=angular.copy(SelectedData);
			angular.element('.ui.modal.ModalActionApprovalPengirimanDenganPengecualian').modal('setting',{closable:false}).modal('show');
		}
		
		$scope.BatalActionApprovalPengirimanDenganPengecualian = function () {
			angular.element('.ui.modal.ModalActionApprovalPengirimanDenganPengecualian').modal('hide');
			$scope.mApprovalPengirimanDenganPengecualian={};
		}
		
		$scope.SelectApprovalPengirimanDenganPengecualian = function () {
			if($scope.user.RoleName=="KACAB")
			{
				$scope.UserIsKacab=true;
			}
			else
			{
				$scope.UserIsKacab=false;
			}
			$scope.ApprovalPengirimanDenganPengecualianMain=false;
			$scope.ApprovalPengirimanDenganPengecualianDetail=true;
			
			
			
			//var sdf = new SimpleDateFormat("hh:mm:ss");
			//var datekiri = sdf.parse($scope.mApprovalPengirimanDenganPengecualian.ETACustomer);
			
			//var datebuntut = new Date(datekiri.getTime() + 60 * 60 * 1000);
			//$scope.mApprovalPengirimanDenganPengecualian.ETACustomerBuntut=datebuntut.toString();
			
			
			var da_eta_buntut_string=$scope.mApprovalPengirimanDenganPengecualian.ETACustomer.toString();
			var collectionDate = '2002-04-26T'+da_eta_buntut_string;
			
			$scope.mApprovalPengirimanDenganPengecualian.ETACustomerBuntut=new Date(collectionDate);
			$scope.mApprovalPengirimanDenganPengecualian.ETACustomerBuntut.setHours($scope.mApprovalPengirimanDenganPengecualian.ETACustomerBuntut.getHours()+1);
			$scope.mApprovalPengirimanDenganPengecualian.ETACustomerBuntut=$filter('date')($scope.mApprovalPengirimanDenganPengecualian.ETACustomerBuntut, 'HH:mm:ss');
			
			angular.element('.ui.modal.ModalActionApprovalPengirimanDenganPengecualian').modal('hide');
		}
		
		$scope.BatalApprovalPengirimanDenganPengecualian = function () {
			$scope.mApprovalPengirimanDenganPengecualian ={};
			$scope.ApprovalPengirimanDenganPengecualianMain=true;
			$scope.ApprovalPengirimanDenganPengecualianDetail=false;
		}
		
		$scope.TidakSetujuApprovalPengirimanDenganPengecualian = function () {
			if($scope.user.RoleName=="KACAB")
			{
				if($scope.mApprovalPengirimanDenganPengecualian.StatusApprovalName=="Diajukan")
				{
					angular.element('.ui.modal.ModalAlasanPenolakanApprovalPengirimanDenganPengecualian').modal('setting',{closable:false}).modal('show');
				}
				else
				{
					bsNotify.show(
						{
							title: "peringatan",
							content: "Data sudah disetujui/ditolak.",
							type: 'warning'
						}
					);
					angular.element('.ui.modal.ModalActionApprovalPengirimanDenganPengecualian').modal('hide');
					$scope.BatalAlasanApprovalPengirimanDenganPengecualian();
					$scope.BatalApprovalPengirimanDenganPengecualian();
				}
				
			}
			else
			{
				alert('Fitur Ini Hanya Untuk Kepala Cabang!');
			}
			
		};
		
		$scope.BatalAlasanApprovalPengirimanDenganPengecualian = function () {
			angular.element('.ui.modal.ModalAlasanPenolakanApprovalPengirimanDenganPengecualian').modal('hide');
			//$scope.mApprovalPengirimanDenganPengecualian={};
		}
		
		$scope.SetujuApprovalPengirimanDenganPengecualian = function () {
		
			if($scope.user.RoleName=="KACAB")
			{
				if($scope.mApprovalPengirimanDenganPengecualian.StatusApprovalName=="Diajukan")
				{
					ApprovalPengirimanDenganPengecualianFactory.SetujuApprovalPengirimanDenganPengecualian($scope.mApprovalPengirimanDenganPengecualian).then(function () {
						ApprovalPengirimanDenganPengecualianFactory.getData().then(
							function(res){
								for(var i = 0; i < res.data.Result.length; ++i)
								{
									var ToBeTruncated=res.data.Result[i]['DECDetailDeliveryExcReasonView'][0].DeliveryExcReasonName;
									res.data.Result[i]['ApprovalPengirimanDenganPengecualianTruncatedAlasan']=ToBeTruncated.substring(0,16)+"...";
								}
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								$scope.BatalApprovalPengirimanDenganPengecualian();
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "gagal",
										content: "Data gagal disetujui.",
										type: 'danger'
									}
								);
							}
						);
					});
				}
				else
				{
					bsNotify.show(
						{
							title: "peringatan",
							content: "Data sudah disetujui/ditolak.",
							type: 'warning'
						}
					);
					$scope.BatalAlasanApprovalPengirimanDenganPengecualian();
				}
				
			}
			else
			{
				alert('Fitur Ini Hanya Untuk Kepala Cabang!');
			}

			angular.element('.ui.modal.ModalActionApprovalPengirimanDenganPengecualian').modal('hide');
		};
		
		$scope.SubmitAlasanApprovalPengirimanDenganPengecualian = function () {
			ApprovalPengirimanDenganPengecualianFactory.TidakSetujuApprovalPengirimanDenganPengecualian($scope.mApprovalPengirimanDenganPengecualian).then(function () {
						ApprovalPengirimanDenganPengecualianFactory.getData().then(
							function(res){
								for(var i = 0; i < res.data.Result.length; ++i)
								{
									var ToBeTruncated=res.data.Result[i]['DECDetailDeliveryExcReasonView'][0].DeliveryExcReasonName;
									res.data.Result[i]['ApprovalPengirimanDenganPengecualianTruncatedAlasan']=ToBeTruncated.substring(0,16)+"...";
								}
								$scope.grid.data = res.data.Result;
								$scope.loading=false;
								$scope.BatalAlasanApprovalPengirimanDenganPengecualian();
								$scope.BatalApprovalPengirimanDenganPengecualian();
								return res.data;
							},
							function(err){
								bsNotify.show(
									{
										title: "gagal",
										content: "Data tidak ditemukan.",
										type: 'danger'
									}
								);
							}
						);
					});
		
			angular.element('.ui.modal.ModalActionApprovalPengirimanDenganPengecualian').modal('hide');
		}	
	
	$scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'Nama Pelanggan', field: 'CustomerName', visible: true },
				{ name: 'Salesman', field: 'EmployeeName' },
				{ name: 'No SPK/No Rangka', cellTemplate:'<label><font size="1">{{row.entity.FormSPKNo}}</font> - </label><label><font size="1">{{row.entity.FrameNo}}</font></label>'},
				{ name: 'Tanggal/Janji Pengiriman', cellTemplate:'<label>{{row.entity.SentUnitDate| date:\'dd-MM-yyyy\'}} {{row.entity.SentUnitTime.toString().substring(0,5)}}</label>' },
				{ name: 'Alasan', field: 'ApprovalPengirimanDenganPengecualianTruncatedAlasan' },
				{ name: 'Status', cellTemplate:'<label>{{row.entity.StatusApprovalName}}</label><span ng-click="grid.appScope.$parent.ActionApprovalPengirimanDenganPengecualian(row.entity)" class="glyphicon glyphicon-option-vertical" ></span>' },
            ]
        };	
	
	
	
	

});
