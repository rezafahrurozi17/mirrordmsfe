angular.module('app')
    .factory('ListDecFactory', function($http, CurrentUser,$httpParamSerializer) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(filter) {
                var param = $httpParamSerializer(filter);
				var res = $http.get('/api/sales/GetReceptionBSTKB?'+ param);
                return res;
            },
			GetStatusDECDropdown_ListDEC: function () {
				
				var da_json = [
				  { StatusDECId: null, StatusDECName: "All" },
				  { StatusDECId: 1, StatusDECName: "Completed" },
				  { StatusDECId: 2, StatusDECName: "Not Completed" }
				];

				var res=da_json

				return res;
			  },
            create: function(ListDec) {
                return $http.post('/api/sales/ReceptionBSTKB', [{
                    
                }]);
            },
            update: function(ListDec) {
                return $http.put('/api/sales/ReceptionBSTKB', [ListDec]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
        }
    });
//ddd