angular.module('app')
    .controller('ListDecController', function($scope, $http, CurrentUser, ListDecFactory, $timeout,PrintRpt,bsNotify, MobileHandling) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.ShowListDEC = true;
        $scope.show_modal={show:false};
        $scope.show_modal2={show:false};
        $scope.modalMode='new';
		$scope.filter={StatusDECId:null, ActualPDD:null};
		$scope.optionStatusDecName = ListDecFactory.GetStatusDECDropdown_ListDEC();
		$scope.TodayDate_Raw=new Date();
		$scope.TodayDate=$scope.TodayDate_Raw.getFullYear()+'-'+('0' + ($scope.TodayDate_Raw.getMonth()+1)).slice(-2)+'-'+('0' + $scope.TodayDate_Raw.getDate()).slice(-2);
		$scope.filter.ActualPDD=new Date();
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            //----------Begin Check Display---------
			    
			      $scope.bv=MobileHandling.browserVer();
			      $scope.bver=MobileHandling.getBrowserVer();
			      //console.log("browserVer=>",$scope.bv);
			      //console.log("browVersion=>",$scope.bver);
			      //console.log("tab: ",$scope.tab);
			      if($scope.bv!=="Chrome"){
			      if ($scope.bv=="Unknown") {
			        if(!alert("Menu ini tidak bisa dibuka pada perangkat Mobile."))
			        {
			          $scope.removeTab($scope.tab.sref);
			        }
			      }        
			      }
			    
			    //----------End Check Display-------
        });
        
        function getMod(a, b) {
            return (a & Math.pow(2, b)) == Math.pow(2, b)
        }
        $scope.RightsEnableForMenu = {};

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.rightEnableByte = null;
        $scope.user = CurrentUser.user();
        $scope.mlistDec = null; //Model
        //----------------------------------
        
        function checkBytes(u) {
            $scope.RightsEnableForMenu.allowNew = getMod(u, 1);
            $scope.RightsEnableForMenu.allowEdit = getMod(u, 2);
            $scope.RightsEnableForMenu.allowDelete = getMod(u, 3);
            $scope.RightsEnableForMenu.allowApprove = getMod(u, 4);
            $scope.RightsEnableForMenu.allowReject = getMod(u, 5);
            $scope.RightsEnableForMenu.allowReview = getMod(u, 6);
            $scope.RightsEnableForMenu.allowPrint = getMod(u, 7);
            $scope.RightsEnableForMenu.allowPart = getMod(u, 8);
            $scope.RightsEnableForMenu.allowConsumeable = getMod(u, 9);
            $scope.RightsEnableForMenu.allowGR = getMod(u, 10);
            $scope.RightsEnableForMenu.allowBP = getMod(u, 11);
            $scope.RightsEnableForMenu.allowUnmatch = getMod(u, 12);
            $scope.RightsEnableForMenu.allowSuggestmatch = getMod(u, 13);
            $scope.RightsEnableForMenu.allowMatch = getMod(u, 14);
        }

        $timeout(function () {
            console.log("rightEnableByte", $scope.rightEnableByte);
            checkBytes($scope.rightEnableByte);
            console.log("RIGHST =====>", $scope.RightsEnableForMenu);
        }, 1000)


        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function() {
            ListDecFactory.getData($scope.filter).then(
                function(res) {
                    $scope.grid.data = res.data.Result; 
                    //$scope.grid.data = x;
                    $scope.loading = false;
                },
                function(err) {
                    bsNotify.show(
						{
							title: "gagal",
							content: "Data tidak ditemukan.",
							type: 'danger'
						}
					);
                }
            );
        }
		
		$scope.$on('$destroy', function() {
		  angular.element('.ui.modal.TerimaBSTKB').remove();
		  angular.element('.ui.modal.CetakThankYou').remove();
		});

		$scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
        };	
      
        //----------------------------------
        // Begin Modal
        //----------------------------------
        $scope.TerimaBSTKBClick = function(SelectData)
        { 
            $scope.TerimaBSTKB = SelectData;
            if($scope.TerimaBSTKB.BillingStatusId == 5){
                bsNotify.show(
                    {
                        title: "Perhatian",
                        content: "Status Billing masih Menunggu Approval, mohon lakukan approval dahulu. Approval dapat dilakukan oleh ADH di menu Maintain Billing.",
                        type: 'danger'         
                    }   
                    
                );
                console.log("ini==>", SelectData);
                return false;              
            } else {
                if($scope.TerimaBSTKB.StatusDECId != 1)
                {
                    bsNotify.show(
                        {
                            title: "Perhatian",
                            content: "Salesman belum melakukan DEC, pastikan DEC sudah dilakukan oleh Salesman.",
                            type: 'danger'         
                        }                                           
                    ); 
                    return false;  
                }
            }
            angular.element('.ui.modal.TerimaBSTKB').modal('show');
			angular.element('.ui.modal.TerimaBSTKB').not(':first').remove();
        }
        
        $scope.btnTerimaBSTKB = function()
        {
            if ($scope.TerimaBSTKB.TerimaBSTKBStatusId == 2 )
            {
                $scope.TerimaBSTKB.TerimaBSTKBStatusId = 1
                $scope.TerimaBSTKB.TerimaBSTKBStatusName = "Ya"
            }

            ListDecFactory.update($scope.TerimaBSTKB).then(
                function(res){
                    $scope.getData();
                    bsNotify.show(
						{
							title: "Sukses",
							content: "Data berhasil di simpan.",
							type: 'success'
						}
					);
                },

                function(err)
                {
					bsNotify.show(
						{
							title: "gagal",
							content: "Data gagal di update.",
							type: 'danger'
						}
					);
                }
            );

            angular.element('.ui.modal.TerimaBSTKB').modal('hide');
            // angular.element('.ui.modal.CetakThankYou').modal('show');
			// angular.element('.ui.modal.CetakThankYou').not(':first').remove();
        }

        $scope.CetakThankYouModal = function(SelectedData){

            $scope.TerimaBSTKB = SelectedData;

            if ($scope.TerimaBSTKB.TerimaBSTKBStatusId == 2 )
            {
                $scope.TerimaBSTKB.TerimaBSTKBStatusId = 1
                $scope.TerimaBSTKB.TerimaBSTKBStatusName = "Ya"
            }

            angular.element('.ui.modal.TerimaBSTKB').modal('hide');
            angular.element('.ui.modal.CetakThankYou').modal('show');
			angular.element('.ui.modal.CetakThankYou').not(':first').remove();
        }
        
        $scope.onListCancel = function()
        {
            angular.element('.ui.modal.TerimaBSTKB').modal('hide');
            angular.element('.ui.modal.CetakThankYou').modal('hide');
            $scope.ShowListDEC = true;
            $scope.ShowPreviewMail = false;
        }

        $scope.btnCetakThankYou = function()
        {
            //angular.element('.ui.modal.CetakThankYou').modal('hide');
            //$scope.ShowListDEC = false;
            //$scope.ShowPreviewMail = true;
			$scope.btncetakPreviewListDecPanda();
        }
		
		$scope.btncetakPreviewListDecPanda = function()
        {
			//Call PrintRpt
			var pdfFile = null;
			PrintRpt.print("sales/ThankYouLetter?DECId="+$scope.TerimaBSTKB.DECId).success(function (res) {
				var file = new Blob([res], {type: 'application/pdf'});
				var fileURL = URL.createObjectURL(file);
						
				pdfFile = fileURL;

				if(pdfFile != null)
				{
					printJS(pdfFile);
				}	
				else
				{
					bsNotify.show(
						{
							title: "gagal",
							content: "Data gagal di print.",
							type: 'danger'
						}
					);
				}	
			})
			.error(function (res) {
				bsNotify.show(
					{
						title: "gagal",
						content: "Terjadi error pada cetakan.",
						type: 'danger'
					}
				); 
				});
        }
        
        $scope.selectRole = function(rows) {
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }

        $scope.onSelectRows = function(rows) {
            //console.log("onSelectRows=>", rows);
        }

        statusCek = '<div align="center">'
            + '<input type="checkbox" ng-disabled="true" ng-checked="row.entity.TerimaBSTKBStatusName==\'Ya\'">'
            + '</div>';

        // btnAction = '<div ng-if="(row.entity.TerimaBSTKBStatusName!=\'Ya\' && row.entity.StatusDECName!=\'Belum DEC\') || (row.entity.SpkId==null && row.entity.TerimaBSTKBStatusName!=\'Ya\')"><span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.$parent.TerimaBSTKBClick(row.entity)">Terima BSTKB</u></span> </div>'
        // + '<div ng-if="row.entity.TerimaBSTKBStatusName==\'Ya\'"><span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.$parent.CetakThankYouModal(row.entity)">Cetak Thank You Letter</u></span> </div>';

		btnAction = '<div ng-hide="!grid.appScope.$parent.RightsEnableForMenu.allowEdit || row.entity.TerimaBSTKBStatusName==\'Ya\'"><span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.$parent.TerimaBSTKBClick(row.entity)">Terima BSTKB</u></u></span> </div>';
        // + '<div><span style="color:blue"><p style="padding:5px 0 0 5px" ><u ng-click="grid.appScope.$parent.CetakThankYouModal(row.entity)">Cetak Thank You Letter</u></span> </div>';
		
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,

            columnDefs: [
                { name: 'id', field: 'DECId', visible: false },
                { name: 'toyota id', displayName:'Toyota ID', field: 'ToyotaId', width: '12%' },
                { name: 'nama pelanggan', field: 'CustomerName', width: '17%' },
                { name: 'nama penerima', field: 'ReceiverName', width: '17%' },
                { name: 'no spk', displayName:'No. SPK', field: 'FormSPKNo', width: '14%' },
                { name: 'no rangka', field: 'FrameNo', width: '14%' },
                { 
                    name: 'terima BSTKB',
                    displayName:'Terima BSTKB',
                    allowCellFocus: false,
                    width: '11%',
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
                    cellTemplate: statusCek
                },
                { name: 'status dec',  displayName:'Status DEC', field: 'StatusDECName', width: '11%'},
                { name: 'tanggal dec',  displayName:'Tanggal DEC', field: 'ActualPDD', width: '11%', cellFilter: 'date:\'dd-MM-yyyy\''},
                { name: 'jam selesai dec',  displayName:'Jam Selesai DEC', field: 'ActualPDD', width: '10%', cellFilter: 'date:\'h:mm a\''},
                { name: 'nama salesman', field: 'NamaSalesman', width: '17%' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: '15%',
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: true,
					enableFiltering: false,
					visible:true,
                    cellTemplate: btnAction
                }
            ]
        };
    });