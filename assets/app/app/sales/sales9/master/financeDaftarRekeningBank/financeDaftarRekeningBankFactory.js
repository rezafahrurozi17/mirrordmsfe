angular.module('app')
  .factory('FinanceDaftarRekeningBankFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/FFinanceListAccountBank');
        //console.log('res=>',res);
		
		var res2 = [
					{
						 "DaftarRekeningBankId" : 1,
                         "NamaDaftarRekeningBank" : "orang1",
						"NoDaftarRekeningBank" : "123",
						"bank_id" : "1",
						"bank_name" : "BANK BCA"
					},
					{
						"DaftarRekeningBankId" : 2,
                         "NamaDaftarRekeningBank" : "orang1",
						"NoDaftarRekeningBank" : "123",
						"bank_id" : "2",
						"bank_name" : "BANK BNI"
					}
				];
        return res;
      },
      create: function(FinanceDaftarRekeningBank) {
        return $http.post('/api/sales/FFinanceListAccountBank', [{
                                            AccountBankName : FinanceDaftarRekeningBank.AccountBankName,
											AccountBankNo : FinanceDaftarRekeningBank.AccountBankNo,
											BankId : FinanceDaftarRekeningBank.BankId,
                                            }]);
      },
      update: function(FinanceDaftarRekeningBank){
        return $http.put('/api/sales/FFinanceListAccountBank', [{
                                            AccountBankId : FinanceDaftarRekeningBank.AccountBankId,
											OutletId : FinanceDaftarRekeningBank.OutletId,
											AccountBankName : FinanceDaftarRekeningBank.AccountBankName,
                                            AccountBankNo : FinanceDaftarRekeningBank.AccountBankNo,
											BankId : FinanceDaftarRekeningBank.BankId,
                                          }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/FFinanceListAccountBank',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd