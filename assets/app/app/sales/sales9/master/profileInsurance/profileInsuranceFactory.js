angular.module('app')
  .factory('ProfileInsuranceFactory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        var res=$http.get('/api/sales/MProfileInsuranceForMaster');
        //console.log('res=>',res);
    
        return res;
      },
      create: function(ProfileInsurance) {
        console.log(ProfileInsurance)
        return $http.post('/api/sales/MProfileInsurance', [{
                                            InsuranceName:ProfileInsurance.InsuranceName,
											StatusActive:ProfileInsurance.StatusActive,
											ListMProfileInsuranceExtensionView:ProfileInsurance.ListMProfileInsuranceExtensionView,
                                            }]);
      },
      update: function(ProfileInsurance){
        return $http.put('/api/sales/MProfileInsurance', [{
                                            InsuranceId:ProfileInsurance.InsuranceId,
											OutletId:ProfileInsurance.OutletId,
											InsuranceName:ProfileInsurance.InsuranceName,
											StatusActive:ProfileInsurance.StatusActive,
											ListMProfileInsuranceExtensionView:ProfileInsurance.ListMProfileInsuranceExtensionView,

                                            //pid: role.pid,
                                            }]);
      },
      delete: function(id) {
        return $http.delete('/api/sales/MProfileInsurance',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
 //ddd