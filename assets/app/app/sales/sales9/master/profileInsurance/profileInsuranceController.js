angular.module('app')
    .controller('ProfileInsuranceController', function($scope, $http, CurrentUser, ProfileInsuranceFactory,ProfileAsuransiPerluasanJaminan,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mProfileInsurance = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
	
	$scope.OperationProfileInsurance="";
	$scope.ProfileInsuranceMain=true;
	$scope.ProfileInsuranceDetail=false;

    $scope.getData = function() {
        ProfileAsuransiPerluasanJaminan.getData()
        .then(
            function(res){
                gridData = [];
                $scope.optionsProfileAsuransiPerluasanJaminan =  res.data.Result;
                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
		ProfileInsuranceFactory.getData()
        .then(
            function(res){
                gridData = [];
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
            },
            function(err){
                bsNotify.show(
					{
						title: "gagal",
						content: "Data tidak ditemukan.",
						type: 'danger'
					}
				);
            }
        );
    }
	
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
	
	$scope.RemoveProfileAsuransiPerluasanJaminan = function (index) {
		$scope.mProfileInsurance.ListMProfileInsuranceExtensionView.splice(index, 1);

    }
	
	$scope.ProfileInsuranceWarnainTabel = function(setan) {
		var ulala=setan+1;
		if(ulala%2!=0)
		{
			return "ProfileInsuranceWarnainYgGanjil";
		}
	}
	
	$scope.TambahProfileInsurance = function() {
		$scope.ProfileInsuranceMain=false;
		$scope.ProfileInsuranceDetail=true;
		$scope.OperationProfileInsurance="Insert";
		$scope.mProfileInsurance="";
		$scope.mProfileInsurance={};
		$scope.mProfileInsurance_Field_EnableDisable=false;
		
		$scope.ShowBtnSimpanProfileInsurance = true;
		$scope.ShowBtnKembaliProfileInsurance = false;
    }
	
	$scope.UpdateProfileInsurance = function(SelectedData) {
			$scope.ProfileInsuranceMain=false;
			$scope.ProfileInsuranceDetail=true;
			$scope.OperationProfileInsurance="Update";
			
			$scope.mProfileInsurance=angular.copy(SelectedData);
			$scope.mProfileInsurance_Field_EnableDisable=false;

			$scope.ShowBtnSimpanProfileInsurance = true;
			$scope.ShowBtnKembaliProfileInsurance = false;
			$scope.mProfileInsurance_Field_EnableDisable = false;
        }
		
	$scope.LihatProfileInsurance = function(SelectedData) {
			$scope.ProfileInsuranceMain=false;
			$scope.ProfileInsuranceDetail=true;
			$scope.OperationProfileInsurance="Lihat";
			
			$scope.mProfileInsurance=angular.copy(SelectedData);
			$scope.mProfileInsurance_Field_EnableDisable=true;

			$scope.ShowBtnSimpanProfileInsurance = false;
			$scope.ShowBtnKembaliProfileInsurance = true;
			$scope.mProfileInsurance_Field_EnableDisable = true;

			
        }

	$scope.KembaliProfileInsurance = function() {
			$scope.ProfileInsuranceDetail=false;
			$scope.ProfileInsuranceMain=true;
			$scope.DropdownProfileAsuransiPerluasanJaminan="";
			$scope.mProfileInsurance="";
			$scope.mProfileInsurance={};
        }	
	
	$scope.AddProfileAsuransiPerluasanJaminan = function () {
		if((typeof $scope.The_Perluasan!="undefined")&& $scope.The_Perluasan!=null)
		{
			try
			{
				var ProfileInsurancePerluasanJaminanUniqueCheck=true;
				for(var i = 0; i < $scope.mProfileInsurance.ListMProfileInsuranceExtensionView.length; ++i)
				{	
					if($scope.mProfileInsurance.ListMProfileInsuranceExtensionView[i].InsuranceExtensionId==$scope.The_Perluasan.InsuranceExtensionId)
					{
						ProfileInsurancePerluasanJaminanUniqueCheck=false;
						bsNotify.show(
							{
								title: "peringatan",
								content: "Data yang anda pilih duplikat.",
								type: 'warning'
							}
						);
						break;
					}
				}
				if(ProfileInsurancePerluasanJaminanUniqueCheck==true)
				{
					$scope.mProfileInsurance.ListMProfileInsuranceExtensionView.push($scope.The_Perluasan);
				}
				
			}
			catch(err)
			{
				$scope.mProfileInsurance.ListMProfileInsuranceExtensionView=[{ InsuranceExtensionId: null,InsuranceExtensionName:null}];
				$scope.mProfileInsurance.ListMProfileInsuranceExtensionView.splice(0, 1);
				$scope.mProfileInsurance.ListMProfileInsuranceExtensionView.push($scope.The_Perluasan);
			}
		}
		else
		{
			bsNotify.show(
					{
						title: "peringatan",
						content: "Anda harus memilih nama perluasan jaminan.",
						type: 'warning'
					}
				);
		}
		
    }
	
	$scope.SimpanProfileInsurance = function () {

		
		if($scope.OperationProfileInsurance=="Insert")
		{
			ProfileInsuranceFactory.create($scope.mProfileInsurance).then(
				function(res) {
					ProfileInsuranceFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							$scope.KembaliProfileInsurance();
							//return res.data;
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							$scope.KembaliProfileInsurance();
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		else if($scope.OperationProfileInsurance=="Update")
		{
			ProfileInsuranceFactory.update($scope.mProfileInsurance).then(
				function(res) {
					ProfileInsuranceFactory.getData().then(
						function(res){
							gridData = [];
							$scope.grid.data = res.data.Result;
							$scope.loading=false;
							bsNotify.show(
								{
									title: "Sukses",
									content: "Data berhasil disimpan",
									type: 'success'
								}
							);
							//return res.data;
							$scope.KembaliProfileInsurance();
						},
						function(err){
							bsNotify.show(
								{
									title: "Gagal",
									content: "Data tidak ditemukan.",
									type: 'danger'
								}
							);
						}
					);


				},            
						function(err){
							$scope.KembaliProfileInsurance();
							bsNotify.show(
								{
									title: "Gagal",
									content: "Sudah ada item dengan nama yang sama",
									type: 'danger'
								}
							);
						}
			)
		}
		
        
        $scope.formApi.setMode("grid");
    }
	
	$scope.deleteManualInsurance = function (id) {
		console.log('iddelete',id);
		var arrayDelete = [];
		for(var i in id) {
			arrayDelete.push(id[i].InsuranceId)
		}
		console.log('arraydel',arrayDelete);
		ProfileInsuranceFactory.delete(arrayDelete).then(
			function (res) {
				ProfileInsuranceFactory.getData().then(
					function (res) {
						gridData = [];
						$scope.grid.data = res.data.Result;
						$scope.loading = false;
						bsNotify.show(
							{
								title: "Sukses",
								content: "Data berhasil dihapus",
								type: 'success'
							}
						);
						return res.data;
					},
					function (err) {
						bsNotify.show(
							{
								title: "Gagal",
								content: "Data tidak ditemukan",
								type: 'danger'
							}
						);
					}
				);
			},
			function (err) {
				bsNotify.show(
					{
						title: "Gagal",
						content: err.data.Message,
						type: 'danger'
					}
				);
			}
		)
	}

	$scope.SetSelectedPerluasan = function (selectedperluasan) {
        $scope.The_Perluasan=selectedperluasan;
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Insurance Id',field:'InsuranceId', width:'7%', visible:false },
   			{ name:'Nama Asuransi',  field: 'InsuranceName' },
			{ name:'Profile Asuransi Perluasan Jaminan',  field: 'ListMProfileInsuranceExtensionView' , visible:false},
			{ name:'Status',  field: 'StatusActive',cellTemplate:'<p style="padding:5px 0 0 5px"><label ng-if="row.entity.StatusActive==false">Non Aktif</label><label ng-if="row.entity.StatusActive==true">Aktif</label></p>' },
			{ name:'Action',visible:true,enableFiltering: false, field: '', width: '20%' , cellTemplate:'<i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.LihatProfileInsurance(row.entity)" title="Lihat" ></i>	<i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;" ng-click="grid.appScope.$parent.UpdateProfileInsurance(row.entity)" title="Ubah"></i>'},
        ]
    };
});
