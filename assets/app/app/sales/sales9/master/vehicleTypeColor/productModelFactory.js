angular.module('app')
  .factory('ProductModel', function($http, $q, CurrentUser, bsTransTree) {
    if(_myCache==undefined)
      var _myCache = [];
    var currentUser = CurrentUser.user;

    return {
      getData: function(){

      },
      getDataTree: function() {
        var res = null;
        if(_myCache.length==0){
            res=$http.get('/api/param/VehicleTypeColorDatas/GetModelTree/?start=1&limit=200');
            res.then(function(res){
              var treeIn = bsTransTree.translate(res.data.Result, {
                id:['VehicleModelId','VehicleTypeId','ColorId'],
                code:['VehicleModelCode','CC','ColorCode'],
                name:['VehicleModelName','Description','ColorName'],
                child:{isChild:true, fld:['listVehicleType','listColor']}
              });
              _myCache = treeIn;
            });
        }else{
          res = {
            then:function(cbOk,cbErr){
              cbOk({data:{Result:angular.copy(_myCache)}});
            }
          }
        }
        return res;
      },
      create: function(prdMdl) {
        return $http.post('/master/productModel/create', prdMdl);
      },
      update: function(prdMdl){
        return $http.post('/master/productModel/update', prdMdl);
      },
      delete: function(id) {
        return $http.post('/master/productModel/delete',{id:id});
      },
    }
  });