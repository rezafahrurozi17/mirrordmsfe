angular.module('app')
    .controller('VehicleTypeColorController', function($scope, $http, CurrentUser, ProductModel, SuffixType, VehicleTypeColor, $timeout, $window, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });

    $scope.$on('tabChange', function(){
        console.log("tabChange");
        $("#content").click();
        resizeLayout();
        //$scope.updateDisplay();
    });
    $scope.$on('tabChange', function(){
        resizeLayout();
    });
    $timeout(function () {
        // Set height initially
        resizeLayout();
    });
    var resizeLayout=function() {
        $timeout(function(){
            console.log("resize override..");
            var staticHeight=
                         $("#page-footer").outerHeight()
                        + $(".nav.nav-tabs").outerHeight()
                        + $(".ui.breadcrumb").outerHeight() + 85;
            var advsearch=$(".advsearch");
            var advsearchHeight=$(".advsearch").outerHeight();
            if(!advsearchHeight) {
                advsearchHeight = 0;
            }
            $("#layoutContainer_VehicleModelTemplate").height($(window).height() - staticHeight - advsearchHeight);
        });
    };
    angular.element($window).bind('resize', resizeLayout);
    $scope.$on('$destroy', function() {
        angular.element($window).unbind('resize', resizeLayout);
    });

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mVehicleTypeColor = {}; //Model
    $scope.cVehicleTypeColor = null; //Collection
    $scope.xVehicleTypeColor = {};
    $scope.xVehicleTypeColor.selected=[];
    $scope.editviewinput = true;
		$scope.startDateOptions = {
			startingDay: 1,
			format: 'dd/MM/yyyy',
			//disableWeekend: 1
        };
			$scope.endOrderDateOptions = {
				startingDay: 1,
				format: 'dd/MM/yyyy',
			}
			$scope.startSpkDateOptions = {
				startingDay: 1,
				format: 'dd/MM/yyyy',
				//disableWeekend: 1
				};
				$scope.endSpkDateOptions = {
					startingDay: 1,
					format: 'dd/MM/yyyy',
				}
   

    $scope.startOrderDateChange = function (date){
        $scope.endOrderDateOptions.minDate = date;
        $scope.startSpkDateOptions.minDate = date;
    }

    $scope.startSpkDateChange = function (date){
        $scope.endSpkDateOptions.minDate = date;
    }

    

    $scope.filter = {vehicleModelId:null, vehicleTypeId:null, colorId:null, startValidDate:null, endValidDate:null, suffixTypeId:null, startSPKDate:null, endSPKDate:null};

	var Da_Today_Date=new Date();

	$scope.filter.startValidDate=new Date(Da_Today_Date.setDate(1));
	$scope.filter.endValidDate=new Date();

	$scope.filter.startSPKDate=new Date(Da_Today_Date.setDate(1));
	$scope.filter.endSPKDate=new Date();


    $scope.enbElement = true;
    $scope.disEl = false;

    // var dateFilter='date:"dd/MM/yyyy HH:mm"';
    var dateFilter='date:"dd/MM/yyyy"';
    
    $scope.onBeforeEdit = function(xrow, param){
        $timeout(function () {
            // Set height initially
            resizeLayout();
        });
    }

    $scope.onShowDetail = function(xrow, mode){
        // resizeLayout();   
        //console.log(xrow);

        $scope.mEditView = xrow;

        $scope.$watch("mVehicleTypeColor.EndValidDate", function (newValue, oldValue) {
            $scope.endOrderDateOptions.minDate = $scope.mVehicleTypeColor.StartValidDate;
            $scope.startSpkDateOptions.minDate = $scope.mVehicleTypeColor.EndValidDate;
        });

//         $scope.$watch("mVehicleTypeColor.StartSPKDate", function (newValue, oldValue) {
//             $scope.endSpkDateOptions.minDate = $scope.mVehicleTypeColor.StartSPKDate;
//             //$scope.startSpkDateOptions.minDate = $scope.mVehicleTypeColor.EndValidDate;
//         });
        $scope.$watch("mVehicleTypeColor.StartSPKDate", function (newValue, oldValue) {
            $scope.endSpkDateOptions.minDate = $scope.mVehicleTypeColor.StartSPKDate;
            //$scope.startSpkDateOptions.minDate = $scope.mVehicleTypeColor.EndValidDate;
        });
        
        console.log( $scope.mEditView, $scope.mVehicleTypeColor);


//         $scope.mVehicleTypeColor.productModel = _.find($scope.productModelData,function(o){
//             return o.VehicleModelId == xrow.VehicleModelId;
//         });
//         $scope.mVehicleTypeColor.productType = _.find($scope.mVehicleTypeColor.productModel.listVehicleType,function(o){
//             return o.VehicleTypeId == xrow.VehicleTypeId;
//         });

//         var noFndColor = _.find($scope.mVehicleTypeColor.productType.listColor,function(o){
//             return o.ColorId == xrow.ColorId;
//         });
//         if (noFndColor==undefined || noFndColor==null) {
//             $scope.mVehicleTypeColor.productType.listColor.push({
//                 ColorId : $scope.mVehicleTypeColor.ColorId,
//                 ColorCode : xrow.ColorCode,
//                 ColorName : xrow.ColorName
//             });
//         }

        // var katObj = _.find($scope.productModelData,function(o){
        //     return o.KatashikiCode == $scope.mVehicleTypeColor.KatashikiCode;
        // });
        // var sufObj = _.find($scope.productModelData,function(o){
        //     return o.SuffixCode == $scope.mVehicleTypeColor.SuffixCode;
        // });
        $scope.typeData = [];
        $scope.colorData =[];
        $scope.katashikiData = [];
        $scope.suffixData = [];
        $scope.assemblyTypeData = [];
        $scope.formulaData = [];

        $scope.typeData.push({Description:xrow.Description,VehicleTypeId:xrow.VehicleTypeId});
        $scope.colorData.push({ColorId:xrow.ColorId,ColorCode:xrow.ColorCode,ColorName:xrow.ColorName});
        $scope.katashikiData.push({KatashikiCode:xrow.KatashikiCode});
        $scope.suffixData.push({SuffixCode:xrow.SuffixCode});
        $scope.assemblyTypeData.push({AssemblyType:$scope.mVehicleTypeColor.AssemblyType});
        $scope.formulaData.push({FormulaHitung:$scope.mVehicleTypeColor.FormulaHitung});
        console.log($scope.mVehicleTypeColor);

        if (mode=='edit') {
            console.log('edit mode..');
            $scope.enbElement = true;
            $scope.disEl = false;
            $scope.editviewinput = true;
        } else {
            $scope.enbElement = false;
            $scope.disEl = true;
            $scope.editviewinput = true;
            // prepareData();
        }        
    }

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        console.log($scope.filter);
        // if ($scope.filter.vehicleModelId==null || $scope.filter.vehicleTypeId==null) {
        //     bsNotify.show({
        //         size: 'big',
        //         type: 'danger',
        //         title: "Mohon Input Filter",
        //         // content: error.join('<br>'),
        //         // number: error.length
        //     });
        // }else{            
            var temp = angular.copy($scope.filter);
            VehicleTypeColor.getData(temp).then(
            // VehicleTypeColor.getData($scope.filter).then(
                function(res){
                    $scope.grid.data = res.data.Result;                
                    $scope.loading=false;
                    resizeLayout();   
                    return res.data;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        // }
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    // $scope.filproductModel = function(selected){
    //     $scope.filter.productModel = selected;    
    //     $scope.filter.vehicleTypeId = null;
    //     $scope.filter.colorId = null;
    // }
    
    $scope.filproductType = function(selected){        
        if(selected){
            $scope.filter.productType = selected;
        }else{
            $scope.filter.vehicleTypeId = null;
            $scope.filter.productType = null;
        }
        $scope.filter.colorId = null;
    };

    $scope.startDateOptions = {
        startingDay: 1,
        format: 'dd/MM/yyyy',
        //disableWeekend: 1
    };
    $scope.endDateOptions = {
        startingDay: 1,
        format: 'dd/MM/yyyy',
        //disableWeekend: 1
    };

    ProductModel.getDataTree().then(
        function(res){
            $scope.productModelData = res.data.Result;    
            // console.log("$scope.productModelData : ",$scope.productModelData);        
            return res.data.Result;
        }
    );
    SuffixType.getData().then(
        function(res){
            // $scope.suffixTypeData = res.data.Result;
            // $scope.suffixTypeDataEdit = res.data.Result;
            $scope.suffixTypeData = res.data.Result;
            $scope.suffixTypeDataEdit = [];
            for (var i in res.data.Result) {
                if (res.data.Result[i].StatusCode == 1)
                    $scope.suffixTypeDataEdit.push(res.data.Result[i]);
            }
            $scope.suffixTypeData.unshift({
                SuffixTypeId : -1,
                SuffixTypeName : 'Belum diset Suffix Type', 
                SuffixTypeCode : '', 
                SuffixTypeDescription : ''
            });
            $scope.loading=false;
            return res.data;
        },
        function(err){
            console.log("err=>",err);
        }
    );

    //----------------------------------
    // Grid Setup
    //----------------------------------    

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            // { name:'Model Id',    field:'VehicleModelId', width:'7%' , visible: false},
            { name:'Model', field:'VehicleModelName' },
            { name:'Tipe', field:'Type' },
            { name:'Kode Warna',  field: 'ColorCode' },
            { name:'Warna', field:'ColorName' },
            { name:'Start Order',  field: 'StartValidDate', cellFilter: dateFilter },
            { name:'End Order', field:'EndValidDate', cellFilter: dateFilter },
            { name:'Start SPK',  field: 'StartSPKDate', cellFilter: dateFilter },
            { name:'End SPK', field:'EndSPKDate', cellFilter: dateFilter },
            { name:'Suffix Type Code', field:'SuffixTypeCode' },
            // { name:' ', allowCellFocus: false, width:100, pinnedRight:true,enableColumnMenu:false,enableSorting: false, cellTemplate: btnActionEditTemplate}
        ]
    };
    //----------------------------------
    // Form Fields Setup
    //  Remark All below if not using formly
    //----------------------------------
    // var vm=this;
    // vm.model = $scope.mRole;
    // vm.fields = [
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'title',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Name',
    //                                         placeholder: 'Name',
    //                                         required: true,
    //                                         minlength:3,
    //                                         //maxlength:7,
    //                                         icon:'fa fa-child'
    //                     }
    //                 },
    //                 {
    //                     className: 'input-icon-right',
    //                     type: 'input',
    //                     key: 'desc',
    //                     templateOptions: {
    //                                         type:'text',
    //                                         label: 'Description',
    //                                         placeholder: 'Description',
    //                                         required: true,
    //                                         icon:'glyphicon glyphicon-pencil'
    //                     }
    //                 },
    // ];
});